package com.ftd.osp.mercuryinterface;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.mercuryinterface.constants.MercuryConstants;
import com.ftd.osp.mercuryinterface.dao.MercuryDAO;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;
import com.ftd.osp.mercuryinterface.util.MercuryValidator;
import com.ftd.osp.mercuryinterface.util.MessageUtilities;
import com.ftd.osp.mercuryinterface.util.SocketClient;
import com.ftd.osp.mercuryinterface.vo.BatchMessageVO;
import com.ftd.osp.mercuryinterface.vo.BatchResponseVO;
import com.ftd.osp.mercuryinterface.vo.BatchSendVO;
import com.ftd.osp.mercuryinterface.vo.EfosTimeVO;
import com.ftd.osp.mercuryinterface.vo.LastBatchVO;
import com.ftd.osp.mercuryinterface.vo.MercuryMessageVO;
import com.ftd.osp.mercuryinterface.vo.MercuryOpsVO;
import com.ftd.osp.mercuryinterface.vo.SocketConnectionVO;
import com.ftd.osp.mercuryinterface.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.math.BigDecimal;

import java.net.SocketTimeoutException;

import java.sql.Connection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;

/**
 * This class contains the majority of the logic used to do Mercury processing.
 * This class is invoked by either a MDB or by a servlet.  
 * 
 * Once this process is started it runs continously (at the end of each process
 * this process sends out a JMS message to start itself up again).
 * 
 * This process reads the database to determine what messages need to be sent
 * to Box X, sends the messages, and then processes any reponses that Box X
 * sends back.
 * 
 * 
 * Note: This application uses the DELAY property in JMS messages.  This property is 
 * Oracle specific.
 */

public class MercuryProcessor 
{
 
  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.osp.mercuryinterface.MercuryProcessor";

  private static int SAK_SIZE = 600;
  
  private static String VIEW_QUEUE_YES = "Y";
  private static String VIEW_QUEUE_NO = "N";
  private static final String ACK_RESPONSE_GOOD = "GOOD";
  private static final String ACK_RESPONSE_BAD = "BAD";
  private static final String CCAS_PREFIX = "CREDIT CARD #: ";
  private static final Pattern CCAS_PATTERN = Pattern.compile(CCAS_PREFIX+"\\d{12,24}");  //"CREDIT CARD #: \d{12,24}"
  private static final Pattern SAK_PATTERN = Pattern.compile("<[a-zA-Z]{3}+\\d{10}+>\\z"); //<FTD0123456789>
  
  private static String FTD_MESSAGE = "FTD";

  /**
   * Constructor
   */
  public MercuryProcessor()
  {
      logger =  new Logger(LOGGER_CATEGORY);
  }
  
  
  /** 
   * The main method which does the mercury processing.
   * 
   * @param Connection database connection
   * @param String The mercury suffix to run
   */
  public void doMercuryProcessing(Connection conn,String suffix) throws Exception
  {  
 
      logger.debug("Start processing. SUFFIX=" + suffix);

      //get initial context.  Need for JMS messageing
      InitialContext context = null;
      context = new InitialContext();           
 
      //get config file info
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      String jmsEFOSdelay = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"EFOS_DELAY");                   
      String stopOnAutoRet = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"STOP_ON_AUTO_RET");                   
      
      //dao
      MercuryDAO dao = new MercuryDAO(conn);
  
      //Get MercuryOps row for this suffix
      MercuryOpsVO mercuryOpsVO = dao.getMercuryOpsBySuffix(suffix);
      
      //Get the LastBatch for this suffix
      LastBatchVO lastBatchVO = dao.getLastBatchBySuffix(suffix);
      
      //Check if current time is during the EFOS shutdown time frame. 
      if(isEFOSDown()){      

          logger.debug("We are within the scheduled EFOS downtown.");

          //send message with delay
          MessageToken token = new MessageToken();
          token.setMessage(suffix);
          token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,jmsEFOSdelay ,"int");
          CommonUtilites.sendJMSMessage(context,token);   
     
          //exit procedure
          return;
      }
      
      //if suffix is in use exit
      if(mercuryOpsVO.isInUse()) {
          logger.info("Suffix is in use. This process will stop. Suffix:" + mercuryOpsVO.getSuffix());
          return;
      }
      
      //if suffix is not available exit
      if(!mercuryOpsVO.isAvailable()){
         logger.info("Suffix is not available.  This process will stop. Suffix:" + mercuryOpsVO.getSuffix());
         return;
      }
      
      //Update IN_USE flag to show that this suffix is being used right now
      mercuryOpsVO.setInUse(true);
      dao.updateMercuryOPS(mercuryOpsVO);       

      List messageList = null;
      BatchSendVO batchVO = null;
      String batchString = null;
      Calendar lastTransTime = Calendar.getInstance();
      lastTransTime.setTime(mercuryOpsVO.getLastTransmission());
      
      //If we did not get a good response from the last ack, then we need to do 
      //the ask before anything else.
      if( mercuryOpsVO.isAckAsk() ) 
      {
        messageList = dao.getUnSakedMessagesForSuffix(suffix);
        batchVO = createBatch(conn,mercuryOpsVO,messageList);
      }
      else if( mercuryOpsVO.isResendBatch() ) 
      {
        messageList = dao.getUnSakedMessagesForSuffix(suffix);
        batchVO = createBatch(conn,mercuryOpsVO,messageList);
        batchString = lastBatchVO.getLastBatchSent();  
      }
      else if( !hadGoodTransmissionToday(lastTransTime) )
      {
        //Send empty batches until we hav a good transmission for the current day
        messageList = new ArrayList();//empty message list
        batchVO = createBatch(conn,mercuryOpsVO,messageList);
        batchString = batchVO.getBatchString();
        logger.debug("There are " + messageList.size() + " message(s) to send.");

        lastBatchVO.setLastBatchSent(batchString);
        dao.updateLastBatch(lastBatchVO);
        logger.debug("lastBatchVO written");
      }
      else
      {
        //get messages to process from DB
        messageList = dao.getMercuryMessageBySuffixStatus(mercuryOpsVO.getSuffix(),MercuryConstants.MERCURY_OPEN,mercuryOpsVO.isSendNewOrders(),mercuryOpsVO.getMaxBatchSize());            
        //create batch to be sent out
        batchVO = createBatch(conn,mercuryOpsVO,messageList);
        batchString = batchVO.getBatchString();
        logger.debug("There are " + messageList.size() + " message(s) to send.");

        lastBatchVO.setLastBatchSent(batchString);
        dao.updateLastBatch(lastBatchVO);
        logger.debug("lastBatchVO written");
      }      
    
      BatchResponseVO batchResponseVO = null;

      //Get list of Box servers and open socket connection
      List serverList = getServerList();
      SocketClient client = new SocketClient(serverList);
      //if socket could not be opened
      if(!client.open(isSendingEFOSAlert())){

          //treat socket being down the same as EFOS being down. 
          doEfosDown(conn,messageList,mercuryOpsVO); 

          //exit
          return;          
      }
      
      //try to send and receive data
      String receivedData = "";
      try{ 
        if( mercuryOpsVO.isAckAsk() ) 
        {
          sendAckAsk(client, mercuryOpsVO, dao);
          
          if( !mercuryOpsVO.isInUse() ) {
            //send message 
            MessageToken token = new MessageToken();
            token.setMessage(suffix);
            token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,"0","int");
            CommonUtilites.sendJMSMessage(context,token);   
          }
          
          //All done...will send batches on next cycle
          return;
        }
        else 
        {
          if( mercuryOpsVO.isResendBatch() ) 
          {
            logger.info("Resending batch: " + maskFOLCreditCardNumbers(batchString)); 
          }
          else 
          {
            logger.info("Sending batch: " + maskFOLCreditCardNumbers(batchString)); 
          }       
          receivedData = client.send(batchString,false);
          logger.info("Received: " + receivedData);
        }
      }
      catch(Exception e)
      {
        //Send system message & log message
        String message = "Error occurred while communicating with Box X.  Message status set back to MO.  Process will try and run again.  Suffix: " + mercuryOpsVO.getSuffix();
        CommonUtilites.sendSystemMessage(message);
        logger.error(e);
      
        //set message status back to Mercury Open
        setMessageStatus(conn,messageList,MercuryConstants.MERCURY_OPEN);
        
        //turn off in use status
        mercuryOpsVO.setInUse(false);
        dao.updateMercuryOPS(mercuryOpsVO);   
        
        //send message with efos  delay
        MessageToken token = new MessageToken();
        token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,jmsEFOSdelay ,"int");
        token.setMessage(suffix);
        CommonUtilites.sendJMSMessage(context,token);   
        
        return;
      }
      
      //try to parse data 
      try{
    
          //Parse out response
          batchResponseVO = MessageUtilities.parseReturnString(receivedData,batchVO.getMessageList());
      }
      catch(Exception e) {
        //Leave message status as MQ            
        //Leave suffix status at IN_USE="Y"    
        mercuryOpsVO.setInUse(true);
        dao.updateMercuryOPS(mercuryOpsVO);       
        //Do not send another JMS message

        //send out system message
        String message = "Error parsing response from Box X. Suffix: " + suffix + "  Process will be stopped.  Suffix IN_USE status will be 'Y'.  Messages will be left with a status of 'MQ'";      
        logger.error(e);
        CommonUtilites.sendSystemMessage(message);  
        
        //exit
        return;
      } 
      
      //send acknowledgement
      try{
          //send acknowledgement if no errors
          if(!batchResponseVO.isMessageError() && !batchResponseVO.isBatchError()){
            if( !sendAck(client, mercuryOpsVO, dao) && !mercuryOpsVO.isInUse() ) 
            {
              //send message 
              MessageToken token = new MessageToken();
              token.setMessage(suffix);
              token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,"0","int");
              CommonUtilites.sendJMSMessage(context,token);  
              return;
            }
          }
      }
      catch(Exception e) {   
        //Leave suffix status at IN_USE="Y"    
        mercuryOpsVO.setInUse(true);
        dao.updateMercuryOPS(mercuryOpsVO);   
        
        //send out system message
        String message = "Error while sending acknowledgement to Box X. Suffix: " + suffix + "  Process will be stopped.  Suffix IN_USE status will be 'Y'.  Messages will be left with a status of 'MQ'";      
        logger.error(message,e);
        CommonUtilites.sendSystemMessage(message);  
        
        return;
      } finally {
        if( client!=null ) 
        {
          client.close();
        }
      }

      
      //Process the batch response (Processes errors, do the updates for the SAKS,
      //insert new messages.
      batchResponseVO = processBatchResponse(conn,mercuryOpsVO.getSuffix(), batchVO, batchResponseVO);

      //if we recevied an error message indicating that efos is down
      if(batchResponseVO.isEfosDown()){
        doEfosDown(conn,messageList,mercuryOpsVO);        
        //exit
        return;
      }

      //Retrieve the settings again from MERCURY_OPS in case something has changed
      mercuryOpsVO = dao.getMercuryOpsBySuffix(suffix);
        
      //if we did not receive a SAK for each message sent out
      if(batchResponseVO.isMissingSAKs())
      {
        //Set the suffix inuse flag set to Y
        mercuryOpsVO.setInUse(true);
        dao.updateMercuryOPS(mercuryOpsVO);
        
        //All messages that have not received SAKS will have a status of MQ
        
        //Send system message
        String message = "Missing SAK responses.  Suffix IN_USE flag will be left at Y.  Messages without SAKS will have a status of MQ.  Processing will stop on this suffix.  Suffix:" + mercuryOpsVO.getSuffix();
        CommonUtilites.sendSystemMessage(message); 
        
        //exit
        return;
      }
        
      //One or more saks contained a <BAD0000000000> instead of a mercury id
      if(batchResponseVO.isBadSAKs())
      {
        //Set the suffix inuse flag set to Y
        mercuryOpsVO.setInUse(true);
        dao.updateMercuryOPS(mercuryOpsVO);
        
        //All messages that have not received SAKS will have a status of MQ
        
        //Send system message
        String message = "Bad SAK responses.  Suffix IN_USE flag will be left at Y.  Messages without SAKS will have a status of MQ.  Processing will stop on this suffix.  Suffix:" + mercuryOpsVO.getSuffix();
        CommonUtilites.sendSystemMessage(message); 
        
        //exit
        return;
      }

      //Reset IN USE flag
      mercuryOpsVO.setInUse(false);      
      
      //update activity time if any messages were sent our or received
      if((batchVO.getMessageList() != null && batchVO.getMessageList().size() > 0) ||
        (batchResponseVO.getMessages() != null && batchResponseVO.getMessages().size() >0) ||
        (batchResponseVO.getResponses() != null && batchResponseVO.getResponses().size() >0))
        {
          mercuryOpsVO.setLastActivitiy(new java.util.Date());
        }
      
      //Update tranmission time if an error did occur with the entire batch.
      //If there a message an error then the transmission time will be updated
      if(!batchResponseVO.isBatchError()){
        mercuryOpsVO.setLastTransmission(new java.util.Date());
      }

      //check if an auto-retrieval message was received
      if(batchResponseVO.isAutoRetrieveReceived())
      {
        String autoRetMessage = null;;
        if(stopOnAutoRet != null && stopOnAutoRet.equals("Y"))
        {
            autoRetMessage = "Auto retrieval was received.  Suffix will be stopped.  Available flag being set to N.  Suffix:" + mercuryOpsVO.getSuffix();            

            //set available flag for suffix to off if auto retrieval was recieved
            mercuryOpsVO.setAvailable(false);            
        }
        else
        {
            autoRetMessage = "Auto retrieval was received.  Suffix will NOT be stopped.  Suffix:" + mercuryOpsVO.getSuffix();            
        }
      

        
        //send system message
        CommonUtilites.sendSystemMessage(autoRetMessage);         
      }

      //update
      dao.updateMercuryOPS(mercuryOpsVO);      

      //Start the processing over, unless this suffix is no longer available      
      if(mercuryOpsVO.isAvailable()){          
          
          String jmsDelay = "0";
          
          //Get the JMS delay time.  Delay is greater if EFOS is down or if
          //we got an error with the batch as a whole.
          if(batchResponseVO.isBatchError()){    
            logger.debug("Error in batch.");
            jmsDelay = jmsEFOSdelay;
          }
          else if(isEFOSDown()){    
            logger.debug("We are within the scheduled EFOS downtown.");
            jmsDelay = jmsEFOSdelay;
          }
          else
          {
            jmsDelay = Integer.toString(mercuryOpsVO.getSleepTime());
          }
          
          //send message with proper delay
          MessageToken token = new MessageToken();
          token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,jmsDelay ,"int");
          token.setMessage(suffix);
          CommonUtilites.sendJMSMessage(context,token);
          
      }
      else
      {
        //this suffix is no longer available, do not send a new JMS message
        logger.debug("Suffix is no longer available.  Process ending. Suffix:" + mercuryOpsVO.getSuffix());
      }     
  }
  

  /*
   * This method gets a list of all the Box X servers to which we can send messages to.
   * The list is sorted in the order in which each server should be used.  For 
   * example if the application will first try to communicate with Box using the
   * first server in the list.  If the application cannot connect to the server
   * then it will attempt to connect to the next server in the list.
   * 
   * While running in FAILOVER mode the list will always be in the order as
   * it exists in the config file.  This means that the first server will 
   * always be used, unless for some reason it is down.
   * 
   * While running in LOADBALANCE mode the list will be in a random order. 
   * This order will change each time a message is sent out.  This causes
   * the messages to be processed equally accross all the boxes listed in the
   * config file.
   */
  private List getServerList() throws Exception
  {
      int i = 0;     
      
      //get the connection mode
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
      String connectionMode = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"CONNECTION_MODE");      
      
      List socketList = new ArrayList();
      
      //loop through all the connections that exist in the config file
      boolean moreServers = true;
      while(moreServers)
      {
        
        //look for server in file
        i++;
        String ipAddress = configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"BOX_X_IPADDRESS_" + i);
        String port = configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"BOX_X_PORT_" + i);    

        //if server was found add it to the list of servers
        if(ipAddress != null && ipAddress.length() > 0){
          SocketConnectionVO socketVO = new SocketConnectionVO();      
          socketVO.setIPAddress(ipAddress);
          socketVO.setPort(port);    
          socketList.add(socketVO);       
        }
        else
         {
            moreServers = false;
         }//end if address null
      }//end while loop
        
      
      //if running in load balance mode randomly mix up the order
      List orderedList = null;
      if(connectionMode.equals("LOADBALANCE")){
          Random generator = new Random();
          orderedList = new ArrayList();
          while(socketList.size() > 0)
          {
            
            if(socketList.size() > 1){
                i = generator.nextInt(socketList.size());                
                orderedList.add(socketList.get(i));
                socketList.remove(i);
            }
            else
            {
                orderedList.add(socketList.get(0));
                socketList.remove(0);              
            }
          }
      }
      else
      {
        //else, we are in FAILOVER mode so just leave the list as is
        orderedList = socketList;
      }

      return orderedList;
      

  }
  
  /*
   * This method checs if a successful transmission was made today.
   * The method compares the last transmission date in the DB with the
   * current date.
   * 
   * @parms Calendar The last transmission date for the current suffix
   * @returns boolean.  TRUE=We received a valid transmission today
   */
  private boolean hadGoodTransmissionToday(Calendar lastTran)
  {
     boolean hadActivity = false;
      
     // get today.  Set time to 00:00..beginning of day.
     Calendar today = Calendar.getInstance();
     today.set(Calendar.HOUR_OF_DAY,0);//set hour to 0...beginning of day
     today.set(Calendar.MINUTE,0);
     
     //check if we got any activity today
     if(lastTran.after(today))
     {
       hadActivity = true;
     }         
    return hadActivity;         
  }
  
  /*
   * This method contains the processing that must take place when efos is down.  This proc is called
   * during scheduled and unscheduled efos outages.
   * 
   * @param conn Database connection
   * @param List of message where were sent out in last batch (empty if we are during the scheduled outage0
   * @param MercuryOpsVO MERCURY_OPS data for this suffix
   */
  private void doEfosDown(Connection conn, List messageList,MercuryOpsVO mercuryOPS) throws Exception
  {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      String jmsEFOSdelay = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"EFOS_DELAY");                     
  
      //Reset message status from the last batch to MO (We sent out a batch,
      //got a response saying efos was down, so now we put the message statuses
      //back to MO)
      setMessageStatus(conn,messageList,MercuryConstants.MERCURY_OPEN);     
         
      //get efos times
      EfosTimeVO efosTimeVO = getEFOSTimes();
         
      //get curreny day and time
      Calendar now = Calendar.getInstance();
      
      //get last transmission time
      Calendar lastTransTime = Calendar.getInstance();
      lastTransTime.setTime(mercuryOPS.getLastTransmission());
         
      //If a good transmision was sent to day, or we are after x time send system message
      //(Basically we are sending a system message if this an unscheduled efos outage, or if
      //efos is coming up very late. )
      if(lastTransTime.after(efosTimeVO.getEfosEndTime()) || 
            now.after(efosTimeVO.getEfosAlertTime()))
      {
        String message = "EFOS/BoxX is down.  Process will continue to run and check for system availability.  Suffix:" + mercuryOPS.getSuffix();
        CommonUtilites.sendSystemMessage(message);
      }
          
      //reset inuse flag 
      MercuryDAO dao = new MercuryDAO(conn);
      mercuryOPS.setInUse(false);      
      dao.updateMercuryOPS(mercuryOPS);
          
      //send message with efos delay    
      MessageToken token = new MessageToken();
      token.setMessage(mercuryOPS.getSuffix());
      token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,jmsEFOSdelay ,"int");
      CommonUtilites.sendJMSMessage(new InitialContext(),token);              
  }
  
  
  /*
   * This method controls the process flow for processing the response from BOX X.
   * Errors, Responses, and new messages are processed.
   * 
   * @param Connection Database connection
   * @param BatchSendVO Data related to the batch that was sent out
   * @param BatchResponseVO Date related to the batch reponse
   * 
   * @returns BatchResponseVO Data related to the batch response
   */
  private BatchResponseVO processBatchResponse(Connection conn,String suffix,BatchSendVO batchVO,BatchResponseVO batchResponseVO)
    throws Exception
  {
      //if errors exists
      if(batchResponseVO.isBatchError() || batchResponseVO.isMessageError())
      {      
        logger.debug("Error(s) occured in batch.");      
        //reset statuses to MO, except for message in error, set that one to ME
        batchResponseVO = processErrors(conn,suffix,batchVO.getMessageList(),batchResponseVO);

      }
      else
      {     
        logger.debug("Batch processed without any problems.");       
      
        //Log warnings
        processWarnings(batchResponseVO.getWarnings());

        //process returned responses
        logger.debug("Processing Received Responses.");
        batchResponseVO =  processResponses(conn,batchVO.getMessageList(),batchResponseVO, suffix);
        
        //process returned messages
        logger.debug("Processing Received Messages.");
        processReturnedMessages(conn,batchResponseVO.getMessages(), suffix);
        
      }
    
    return batchResponseVO;
  }
  
  

  /*
   * Set the status of all messages in list
   * 
   * @param Connection database connection
   * @param List List of messages
   * @param String Status to set messages to
   */
   private void setMessageStatus(Connection conn,List messageList,String status) throws Exception
   {
      //null check
      if(messageList == null)
      {
        return;
      }
      
      //get dao
      MercuryDAO dao = new MercuryDAO(conn);
      
      //for each message
      Iterator iter = messageList.iterator();
      while(iter.hasNext())
      {
        MercuryMessageVO vo = (MercuryMessageVO)iter.next();
        vo.setMercuryStatus(status);
        vo.setTransmissionDate(new java.util.Date());
        dao.updateMercuryStatus(vo);
      }
   }

  /*
   * This method sets the status message of all the messages to 'MO' (mercury open) which were
   * not flagged as having an error.  The messages in error are set to have a status of 'ME'.
   * 
   * *Note*. If any message is in error EFOS does not process any orders in the batch.  That is why
   * the message status of all the messages are being updated here.
   */
   private BatchResponseVO processErrors(Connection conn, String suffix,List sentMessages,BatchResponseVO batchResponseVO)
      throws Exception
   {
     //Status of all sent messages to MO(mercury open)
     setMessageStatus(conn,sentMessages,MercuryConstants.MERCURY_OPEN);
     
     //If there is a particular message in error then set it's status to ME(mecury error)
     if(batchResponseVO.isMessageError())
     {
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryMessageVO messageVO = new MercuryMessageVO();
       messageVO.setMercuryID(batchResponseVO.getErrorMercuryID());
       messageVO.setMercuryStatus(MercuryConstants.MERCURY_ERROR);
       messageVO.setSakText(CommonUtilites.truncate(batchResponseVO.getErrorMessageText(),SAK_SIZE));
       messageVO.setTransmissionDate(new java.util.Date());
       dao.updateMercuryStatus(messageVO, suffix);
  
       //insert into hp table
       dao.insertMercuryInboundMDB(messageVO.getMercuryID());
       
       //send sytem error
       String errorText = "Error with message.  Setting message status to 'ME'.  ID:" + batchResponseVO.getErrorMercuryID() + " Error:" + batchResponseVO.getErrorMessageText();
       CommonUtilites.sendSystemMessage(errorText);       

     }
 
     //check for efos down error message
     if(batchResponseVO.getErrorText() != null &&
          batchResponseVO.getErrorText().indexOf(MercuryConstants.EFOS_DOWN_ERROR) >= 0)
          {
            batchResponseVO.setEfosDown(true);
          }
          
    //check if Box X is still processing last transmission
     if(batchResponseVO.getErrorText() != null &&
          batchResponseVO.getErrorText().indexOf(MercuryConstants.BOXX_BUSY_ERROR) >= 0)
          {
            batchResponseVO.setBoxXBusy(true);
            String errorText = "Signon Has Been Rejected By EFOS.  This may mean that BOX X is still processing the last transaction.  Message status set to 'MO'.  Processing will continue.  Suffix:" + suffix;
            CommonUtilites.sendSystemMessage(errorText);                          
          }
          
    //if error occured with batch that is not related to EFOS being down or BOX X being busy
    if(batchResponseVO.isBatchError() && !batchResponseVO.isEfosDown() && !batchResponseVO.isBoxXBusy())
    {
       String errorText = "Error in batch.  Message status set to 'MO'.  Processing will continue.  Suffix:" + suffix + " Error:" + batchResponseVO.getErrorText();
       CommonUtilites.sendSystemMessage(errorText);                                
    }
    
     return batchResponseVO;
   }

//  /**
//   * This method process all the responses returned from the batch.
//   * For each response we UPDATE the mercury message table and insert
//   * a new record into the mercury hp table.
//   * 
//   * @param Connection dat1base connection
//   * @param List List of messages sent out with batch
//   * @param BatchResponseVO data related to response
//   */
//  private BatchResponseVO processResponses(Connection conn, List outGoingMessages,BatchResponseVO batchResponseVO) throws Exception
//  {
//
//    List responseList = batchResponseVO.getResponses();
//
//    //if there are any responses
//    if(responseList != null && responseList.size() > 0){
//
//
//        Iterator iter = responseList.iterator();
//        
//        MercuryDAO dao = new MercuryDAO(conn);
//        
//        int i = 0;
//        
//        //for each message in the list
//        while(iter.hasNext())
//        {
//        
//          //update message in DB
//          MercuryMessageVO messageVO = (MercuryMessageVO)iter.next();
//
//          //get the corresponding outgoing message
//          MercuryMessageVO outgoingMessage = null;
//          if(i < outGoingMessages.size())
//          {
//            outgoingMessage = (MercuryMessageVO)outGoingMessages.get(i);
//            //if this is the response(SAK) or a FTD message then set merucry Message # = order #
//            if(outgoingMessage.getMessageType().equals("FTD"))
//            {
//                messageVO.setMercuryOrderNumber(messageVO.getMercuryMessageNumber());
//            }
//          }
//
//          //set status to complete
//          messageVO.setMercuryStatus(MercuryConstants.MERCURY_CLOSED);
//        
//          //Check the contents of the SAK message.  If it SAK indicates that the 
//          //message rejects then give the message a reject status
//          if(messageVO.getSakText() != null)
//          {
//              String beginningText = messageVO.getSakText().substring(0,MercuryConstants.SAK_REJECT_TEXT.length());
//              if(beginningText.equalsIgnoreCase(MercuryConstants.SAK_REJECT_TEXT))
//              {
//                  messageVO.setMercuryStatus(MercuryConstants.MERCURY_REJECT);
//                  
//                  //first check if we want to flag any items as view queue
//                  if(CommonUtilites.isViewQueueFlagOn()){
//                  
//                          //If message type is FTD and SAK message indicates that the florist
//                          //could not deliver the order then set the view queue status
//                          if(outgoingMessage != null && outgoingMessage.getMessageType().equals("FTD"))
//                          {
//                              if(messageVO.getSakText().indexOf(MercuryConstants.SAK_CANNOT_FILL_TEXT) != 0)
//                              {
//                                  messageVO.setViewQueue(VIEW_QUEUE_YES);
//                              }//end if contains text
//                          }//end if FTD message
//                  }//end if view queue flag on
//              }//end if reject message
//          }//end if sak contains text
//
//          logger.debug("Updating Message: " + messageVO.getMercuryID());
//          dao.updateMercuryMessage(messageVO);
//          
//          //insert data into HP table (this is how the HP knows that something needs to be done with message)
//          dao.insertMercuryHP(messageVO.getMercuryID(),new java.util.Date());          
//
//          //increment counter
//          i++;
//
//        }//end loop    
//    }
//    
//    //If the number of responses received does not match the number of SAKS sent
//    //out then set error flag
//    if(batchResponseVO.getResponses().size() < outGoingMessages.size())
//    {
//      batchResponseVO.setMissingSAKs(true);
//    }
//    
//    return batchResponseVO;    
//  }

  /**
   * This method process all the responses returned from the batch.
   * For each response we UPDATE the mercury message table and insert
   * a new record into the mercury hp table.
   * 
   * @param Connection dat1base connection
   * @param List List of messages sent out with batch
   * @param BatchResponseVO data related to response
   */
  private BatchResponseVO processResponses(Connection conn, List outGoingMessages,BatchResponseVO batchResponseVO, String suffix) throws Exception
  {
    assert outGoingMessages!=null; 
    assert conn!=null;
    assert batchResponseVO!=null;
    
    List responseList = batchResponseVO.getResponses();
    boolean bSakMismatchError = false;
    boolean bBadSak = false;

    //if there are any responses
    if(responseList != null && responseList.size() > 0){
        Iterator iter = responseList.iterator();
        MercuryDAO dao = new MercuryDAO(conn);
        int i = 0;
        String sakText;
        String returnedMercuryID;
        
        //for each message in the list
        while(iter.hasNext())
        {
          //update message in DB
          MercuryMessageVO messageVO = (MercuryMessageVO)iter.next();
          
          sakText = messageVO.getSakText();

          //get the corresponding outgoing message
          MercuryMessageVO outgoingMessage = null;
          if(i < outGoingMessages.size())
          {
            //Get the returned mercury_id
            returnedMercuryID = getMercuryIdFromSak(messageVO.getSakText());
            
            //First try to locate the message by position in list
            outgoingMessage = (MercuryMessageVO)outGoingMessages.get(i);
            
            //If the mercury id's don't match then loop through and find it
            if( returnedMercuryID==null ) 
            {
              logger.error("SAK returned with no mercury id.  Target message mercury id is "+outgoingMessage.getMercuryID());
              bSakMismatchError=true;
            }
            else if( returnedMercuryID.equalsIgnoreCase("BAD0000000000") )
            {
              logger.error("SAK returned with \"BAD0000000000\" mercury id.  Target message mercury id is "+outgoingMessage.getMercuryID());
              bBadSak=true;
            }
            else if( !returnedMercuryID.equals(outgoingMessage.getMercuryID()) ) 
            {
              logger.error("Mercury ID ("+returnedMercuryID+") in SAK does not match mercury ID ("+outgoingMessage.getMercuryID()+") on order");  
              bSakMismatchError=true;
            }
            
            //if this is the response(SAK) or a FTD message then set merucry Message # = order #
            if(outgoingMessage.getMessageType().equals("FTD"))
            {
                messageVO.setMercuryOrderNumber(messageVO.getMercuryMessageNumber());
            }
          }

          //set status to complete
          messageVO.setMercuryStatus(MercuryConstants.MERCURY_CLOSED);
        
          //default view_queue flag to 'N'
          messageVO.setViewQueue(VIEW_QUEUE_NO);

          //Check the contents of the SAK message.  If it SAK indicates that the 
          //message rejects then give the message a reject status
          messageVO.setViewQueue(VIEW_QUEUE_NO);
          if(messageVO.getSakText() != null)
          {
              String beginningText = messageVO.getSakText().substring(0,MercuryConstants.SAK_REJECT_TEXT.length());
              if(beginningText.equalsIgnoreCase(MercuryConstants.SAK_REJECT_TEXT))
              {
                  messageVO.setMercuryStatus(MercuryConstants.MERCURY_REJECT);
                  
                  //first check if we want to flag any items as view queue
                  if(CommonUtilites.isViewQueueFlagOn()){
                  
                          //If message type is FTD and SAK message indicates that the florist
                          //could not deliver the order then set the view queue status
                          if(outgoingMessage != null && outgoingMessage.getMessageType().equals("FTD"))
                          {
                              if(messageVO.getComments() != null && messageVO.getComments().indexOf(MercuryConstants.SAK_CANNOT_FILL_TEXT) >= 0)
                              {
                                  messageVO.setViewQueue(VIEW_QUEUE_YES);
                              }//end if contains text
                          }//end if FTD message
                  }//end if view queue flag on
              }//end if reject message
          }//end if sak contains text

          // modify message to contain new fields for Phase III
          messageVO.setDirection(MercuryMessageVO.OUTBOUND);
          messageVO.setSuffix(suffix);
          String msgNumber = messageVO.getMercuryMessageNumber().trim();
          int length = msgNumber.length();
          
          if(messageVO.getMessageType().equals(FTD_MESSAGE)) 
          {
            // set the order sequence to the last 4 digits of the message number
            messageVO.setOrderSequence(msgNumber.substring(length - 4));
          } else {
            // set the admin sequence to the last 4 digits of the message number
            messageVO.setAdminSequence(msgNumber.substring(length - 4));
          }
          
          logger.debug("Updating Message: " + messageVO.getMercuryID());
          dao.updateMercuryMessage(messageVO);
          
          //insert data into HP table (this is how the HP knows that something needs to be done with message)
          dao.insertMercuryInboundMDB(messageVO.getMercuryID());          

          //increment counter
          i++;

        }//end loop    
    }
    
    //If the number of responses received does not match the number of SAKS sent
    //out then set error flag or there was an error matching saks
    if(bSakMismatchError || batchResponseVO.getResponses().size() < outGoingMessages.size())
    {
      batchResponseVO.setMissingSAKs(true);
    }
    
    if(bBadSak)
    {
      batchResponseVO.setBadSAKs(true);
    }
    
    return batchResponseVO;    
  }


  /*
   * This method process all the messages returned from the batch.
   * Messages are inserted into the mercury message table and into the
   * mercury hp table.
   * 
   * @param Connection Database connectin
   * @param List List of messages recieved.
   */
  private void processReturnedMessages(Connection conn, List messages, String suffix) throws Exception
  {
 
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
    String folFloristID = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"FOL_FLORIST_ID");                   

    //return if null
    if(messages == null)
    {
      return;
    }
    
    Iterator iter = messages.iterator();
    
    MercuryDAO dao = new MercuryDAO(conn);
    List mercuryOrderList = new ArrayList();
    
    //for each message in the list
    while(iter.hasNext())
    {
      MercuryMessageVO messageVO = (MercuryMessageVO)iter.next();
      String mercuryHPInsert = "Y";

      //set status to closed
      messageVO.setMercuryStatus(MercuryConstants.MERCURY_CLOSED);
      
      //if mercury id is empty, set the mercury id equal to the message #
      if(messageVO.getMercuryID() == null || messageVO.getMercuryID().length() == 0)
      {
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
        String today = sdf.format(new Date());
        messageVO.setMercuryID(messageVO.getMercuryMessageNumber() + "-" + today);
      }
      
      //if the message is an ASK or ANS set price to null if ASK ANSWER code is empty
      if( (messageVO.getMessageType().equals("ASK") || messageVO.getMessageType().equals("ANS")) &&
          (messageVO.getAskAnswerCode() == null || messageVO.getAskAnswerCode().length() == 0))
          {
            messageVO.setPrice(null);
          }      
            
      //default view queue status to N
      messageVO.setViewQueue(VIEW_QUEUE_NO);            
           
      //if GEN message check if this a suspend message
      if(messageVO.getMessageType().equals(MercuryConstants.GEN_MESSAGE))
      {
          try 
          {
              //check for suspend text in message
              if(messageVO.getComments() != null && messageVO.getComments().toUpperCase().indexOf(MercuryConstants.SUSPEND_TEXT) >= 0)
              {
                  dao.updateSuspend(messageVO.getSendingFlorist(),"Y",messageVO.getMercuryMessageNumber(),null);
                  mercuryHPInsert = "N";
              }
              //check for resume text in message
              if(messageVO.getComments() != null && messageVO.getComments().toUpperCase().indexOf(MercuryConstants.RESUME_TEXT) >= 0)
              {
                  dao.updateSuspend(messageVO.getSendingFlorist(),"N",null,messageVO.getMercuryMessageNumber());
                  mercuryHPInsert = "N";
              }
          } catch (Exception e) 
          {
              //log error
              logger.error(e);
              //send out sytem message
              String message = "Error processing suffix("+suffix+")  :" + e.toString();

              try{
                  CommonUtilites.sendSystemMessage(message);
              }
              catch(Exception ex)
              {
                  logger.error("Could not send system message:" + ex.toString());
              }
          }
      }
           
      //first check if the view queue flag is on
      if(CommonUtilites.isViewQueueFlagOn()) {    
          if(messageVO.getFillingFlorist() == null || !messageVO.getFillingFlorist().startsWith(folFloristID)) {
              //If message is a REJect or FORward message and the comments do NOT contain an 
              //'Unable to contact' message then set the view queue status to Y
              if(messageVO.getMessageType().equals(MercuryConstants.REJ_MESSAGE) || messageVO.getMessageType().equals(MercuryConstants.FOR_MESSAGE)) 
              {
                  messageVO.setViewQueue(VIEW_QUEUE_YES);
                  if (messageVO.getComments() != null && messageVO.getComments().indexOf(MercuryConstants.COMMENTS_UNABLE_CONTACT_TEXT) >= 0) 
                  {
                      messageVO.setViewQueue(VIEW_QUEUE_NO);
                  } else 
                  {
                      if (messageVO.getComments() != null && messageVO.getComments().indexOf(MercuryConstants.COMMENTS_SELECTED_FILLING) >= 0) 
                      {
                          messageVO.setViewQueue(VIEW_QUEUE_NO);
                      }
                  }
              }
          }//end if not FOL florist
      }//end view queue flag on        

      //fix sending florist for EFOS generated CON messages
      if(messageVO.getMessageType().equals(MercuryConstants.CON_MESSAGE))
      {
        logger.debug("Sending Florist = " + messageVO.getSendingFlorist());
        logger.debug("Reference Number = " + messageVO.getReferenceNumber());
        
        if(messageVO.getSendingFlorist().indexOf(MercuryConstants.MERCURY_SYSTEM) >=0) 
        {
          logger.info("Modifying auto-generated CON message");
          
          // look up the original filling florist from the ftd record
          String originalFlorist = (dao.getMercuryMessageByMessageNumber(messageVO.getMercuryOrderNumber()).getFillingFlorist());
          // make the CON look like it came from the florist instead of Mercury
          messageVO.setSendingFlorist(originalFlorist);
        }
      }

      //if REJ message, check for duplicate REJ message
      if(messageVO.getMessageType().equals(MercuryConstants.REJ_MESSAGE))
      {
          String mercuryOrderNumber = messageVO.getMercuryOrderNumber();
          if (mercuryOrderNumber != null) {
              if (mercuryOrderList.contains(mercuryOrderNumber)) {
                  // Another REJ for same order is in this batch
                  mercuryHPInsert = "N";
              } else {
                  mercuryOrderList.add(mercuryOrderNumber);
              }
          }
      }

      logger.debug("Inserting Message: " + messageVO.getMercuryID());
      
      // populate new fields for Phase III
      messageVO.setDirection(MercuryMessageVO.INBOUND);
      messageVO.setSuffix(suffix);
      String msgNumber = messageVO.getMercuryMessageNumber().trim();
      int messageNumberLength = msgNumber.length();
      String operator = messageVO.getOperator().trim();
      int operatorLength = operator.length();
      
      if(messageVO.getMessageType().equals(FTD_MESSAGE)) {
        // order sequence = last 4 characters of message number
        messageVO.setOrderSequence(msgNumber.substring(messageNumberLength - 4));
        
        // admin sequence = last 4 characters of operator
        messageVO.setAdminSequence(operator.substring(operatorLength - 4));
      } else 
      {
        // admin sequence = last 4 characters of message number
        messageVO.setAdminSequence(msgNumber.substring(messageNumberLength - 4));
      }
      
      MercuryMessageVO truncatedMessage = truncateFields(messageVO);
      dao.insertMercuryMessage(truncatedMessage);

      //insert data into HP table (this is how the HP knows that something needs to be done with message)
      if (mercuryHPInsert != null && mercuryHPInsert.equals("Y")) {
          dao.insertMercuryInboundMDB(messageVO.getMercuryID()); 
      }
    }
  }

  /*
   * This method logs all warning messages
   */
  private void processWarnings(List warnings)
  {
    //exit if list if empty
    if(warnings != null)
    {    
        //loop through all messages
        Iterator iter = warnings.iterator();
        while(iter.hasNext())
        {
          String warning = (String)iter.next();
          logger.warn(warning);
        }
    }

  }

      
  /*
   *  This method creates a batch to be sent out for the given suffix.
   *  
   *  @param Connection database connection
   *  @param MercuryOpsVO MercuryOPS data for the suffix
   *  @param List Messages to be put into batch
   *  
   *  @returns BatchSendVO The Batch to be sent out
   */
   private BatchSendVO createBatch(Connection conn,MercuryOpsVO mercuryOpsVO,List messageList) throws Exception
   {
   
      BatchSendVO batchVO = new BatchSendVO();
  
      List messagesInBatch = new ArrayList();
      
      MercuryDAO dao = new MercuryDAO(conn);
      
      //set flags and counters 
      int messageCount = 0;      
      boolean batchFull = false;
      
      //get value(s) from config file    
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      long maxBatchSizeWithoutTags = Long.parseLong(configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"MAX_BATCH_SIZE_WITHOUT_TAGS"));             
      long maxBatchSizeWithTags = Long.parseLong(configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"MAX_BATCH_SIZE_WITH_TAGS"));             
      long maxMessageLengthWithTags  = Long.parseLong(configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"MAX_MESSAGE_LENGTH_WITH_TAGS"));             
     
      long batchLengthWithoutTags = 0;
     
      String defaultSendingFlorist  = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE,"DEFAULT_SENDING_FLORIST");             
     
      String sendingFlorist = "";
     
      //create object which will contain batch to send to Box X
      StringBuffer batch = new StringBuffer();
      
      boolean emptyBatch;
      //If no messages were passed in (empty batch)
      if(messageList == null || messageList.size() <= 0)
      {
        emptyBatch=true;
      }
      else
      {
        emptyBatch=false;
      }
      logger.info("Empty Batch: "+emptyBatch);
      //We have messages to send out
      if(!emptyBatch) 
      {      
          Iterator messageIter = messageList.iterator();
      
          //Loop until (message count >= max amount of messages per batch) 
          //and (batch full flag = false) and (there are more messages left in the list).
          while( (messageCount < mercuryOpsVO.getMaxBatchSize()) && !batchFull && messageIter.hasNext())
          {
                      //get message
                      MercuryMessageVO messageVO = (MercuryMessageVO)messageIter.next();       

                      //added 11/24/04 - emueller.
                      messageVO = massageData(messageVO);
            
                      //validate the message   
                      MercuryValidator validator = new MercuryValidator();
                      ValidationResponse response = validator.validate(messageVO);
                      //If invalid 
                      if(!response.isValid()){
           
                          //log error & upate message status to ME
                          String errorMessage = "MercuryID:" + messageVO.getMercuryID() + " " + response.getMessage();
                          logger.error(errorMessage);                          
                          messageVO.setMercuryStatus(MercuryConstants.MERCURY_ERROR);
                          messageVO.setTransmissionDate(new java.util.Date());
                          messageVO.setSakText(CommonUtilites.truncate(response.getMessage(),SAK_SIZE));
                          dao.updateMercuryStatus(messageVO, messageVO.getOutboundID());
                          
                          //insert into hp table so that HP knows about error
                          dao.insertMercuryInboundMDB(messageVO.getMercuryID());
                          
                          //send system message
                          CommonUtilites.sendSystemMessage(errorMessage);
                      }
                      else
                      {
                          //Build a batch message from the mercury message
                          BatchMessageVO batchMessage = MessageUtilities.buildMessage(messageVO);
          
                          //If the message length (with tags) exceeds the max message length
                          if(batchMessage.getMessage().length() > maxMessageLengthWithTags){
                              
                              //log error, sent message status to ME, send system message
                              String errorMessage = "Mercury message " + messageVO.getMercuryID() + " exceeded length of " + maxMessageLengthWithTags;
                              logger.error(errorMessage);
                              CommonUtilites.sendSystemMessage(errorMessage);
                              messageVO.setMercuryStatus(MercuryConstants.MERCURY_ERROR);
                              messageVO.setTransmissionDate(new java.util.Date());
                              messageVO.setSakText(CommonUtilites.truncate(errorMessage,SAK_SIZE));
                              dao.updateMercuryStatus(messageVO, messageVO.getOutboundID());    
                              
                              //insert into hp table
                              dao.insertMercuryInboundMDB(messageVO.getMercuryID());
                              
                          }
                          else{                 
                           
                              //Check if the message can fit into the batch
                              if(  ((batch.length() + batchMessage.getMessage().length()) > maxBatchSizeWithTags) ||
                                   ((batchLengthWithoutTags + batchMessage.getLengthWithoutTags()) > maxBatchSizeWithoutTags)){
                                              
                                    //this message cannot fit into the batch                  
                                    batchFull = true;
                            
                                } 
                                else{ //Else, this message fits into the batch
                                
                                    //save the sending florst
                                    sendingFlorist = messageVO.getSendingFlorist();
                                
                                    //set message status to MQ
                                    messageVO.setMercuryStatus(MercuryConstants.MERCURY_QUEUED);
                                    messageVO.setTransmissionDate(new java.util.Date());
                                    dao.updateMercuryStatus(messageVO);                  
          
                                    //keep a total of the batch length without tags
                                    batchLengthWithoutTags = batchLengthWithoutTags + batchMessage.getLengthWithoutTags();

                                    //add message header to mercury message
                                    String batchMessageHeader = "MESSAGE NUMBER(" + (messageCount+1) + ")";
                                    String batchWithHeader = MessageUtilities.buildTag(batchMessageHeader, batchMessage.getMessage());
                            
                                    //keep list of all messages placed in batch
                                    messagesInBatch.add(messageVO);                                    
                                    
                                    //Add message to batch
                                    batch.append(batchWithHeader);
                                    
                                    //incremetn message count
                                    messageCount++;
                                }//end if - can message fit into batch                 
                        }//end if response is valid                                  
                    }//end if response is valid
       }//end message loop          
      }//end if messages were found   
      
      //if sending florist is empty set it to the default
      if ( sendingFlorist.equals("") ) {
        sendingFlorist = defaultSendingFlorist;
      }
      
      //build sending florist header
      String floristHeader = MessageUtilities.buildTag("SENDING FLORIST CODE", sendingFlorist + mercuryOpsVO.getSuffix());
      
      //build total mesage header
      String messageTotalHeader  = MessageUtilities.buildTag("TOTAL MESSAGES", Integer.toString(messageCount));
      
      //create final batch string
      String finalBatch;
      
      if(emptyBatch)
      {
        //We are not sending anything.  Send message to indicate that we want to retrieve messages
        finalBatch = messageTotalHeader + floristHeader;
      }
      else
      {
        finalBatch = messageTotalHeader + floristHeader + batch.toString();
      }
      finalBatch = MessageUtilities.buildHeaderAscii(finalBatch.length()) + finalBatch;
      
      //popluate vo and return it
      batchVO.setBatchString(finalBatch);
      batchVO.setMessageList(messagesInBatch);
      
      return batchVO;
   }
   
  /**
   * Creates the batch string to send to get the last ack response processed.
   * @param mercuryOpsVO Value Object of the mercury ops record 
   * @return The batch string to be sent.
   * @throws Exception 
   */
  private String createAskAckString(MercuryOpsVO mercuryOpsVO) throws Exception
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String defaultSendingFlorist = configUtil.getProperty(MercuryConstants.MERCURY_PROPERTY_FILE, "DEFAULT_SENDING_FLORIST");
    
    StringBuffer sb = new StringBuffer();
    sb.append(MessageUtilities.buildTag("SENDING FLORIST CODE", defaultSendingFlorist + mercuryOpsVO.getSuffix()));
    sb.append("[MARV BYTE;1;TEXT]?");
    
    return sb.toString();
  }
  
  
  /*
   * Massage the data that came from the database.
   * 
   * Created: 11/24/40 - Ed Mueller
   * 
   */
   public static MercuryMessageVO massageData(MercuryMessageVO message)
   {
       //trim the price ask-answer code
       if(message.getAskAnswerCode() != null)
       {
           message.setAskAnswerCode(message.getAskAnswerCode().trim());
       }
       
       //if second choice is null or empty spaces on an FTD message change it to NONE
       if(message.getMessageType() != null && message.getMessageType().equals(MercuryConstants.FTD_MESSAGE))
       {
           if(message.getSecondChoice() == null || message.getSecondChoice().trim().length() == 0)
           {
               message.setSecondChoice("NONE");
           }
       }//end type
       
       return message;
   }

  
  
  /*
   * This method checks if the current time is within the EFOS downtime.
   */
   private boolean isEFOSDown() throws Exception
   {
     boolean efosDown = false;
     
     EfosTimeVO efosTimeVO = getEFOSTimes();
     
     //get current time
     Calendar currentTime = Calendar.getInstance();  
     
     //if the efos outage time goes past midnight (starts one night, ends the next morning)
     if(efosTimeVO.getEfosStartTime().after(efosTimeVO.getEfosEndTime()))
     {
        //efos outtage goes past midnight
     
       //check if we are in the efos down time that started last night
       if(currentTime.before(efosTimeVO.getEfosEndTime()))
       {
         efosDown = true;
       }

       //check if we are in the efos down time that starts tonight
       if(currentTime.after(efosTimeVO.getEfosStartTime()))
       {
         efosDown = true;
       }       
       
     }
     else
     {
       //efos outage starts and ends on same day
       
       //check if we are during outage
       if(currentTime.after(efosTimeVO.getEfosStartTime()) && currentTime.before(efosTimeVO.getEfosEndTime()))
       {
         efosDown = true;
       }             
     }

     return efosDown;
   }
  
  
  /*
   * This method checks if the current time is within the the timeframe
   * that an alert (system message) should be sent if EFOS is down.
   */
   private boolean isSendingEFOSAlert() throws Exception
   {
     boolean efosAlert = true;
     
     EfosTimeVO efosTimeVO = getEFOSTimes();
     
     //get current time
     Calendar currentTime = Calendar.getInstance();  

     //if the efos outage time goes past midnight (starts one night, ends the next morning)
     if(efosTimeVO.getEfosStartTime().after(efosTimeVO.getEfosAlertTime()))
     {
        //efos outtage goes past midnight
     
       //check if we are in the efos down time that started last night
       if(currentTime.before(efosTimeVO.getEfosAlertTime()))
       {
         efosAlert = false;
       }

       //check if we are in the efos down time that starts tonight
       if(currentTime.after(efosTimeVO.getEfosStartTime()))
       {
         efosAlert = false;
       }       
       
     }
     else
     {
       //efos outage starts and ends on same day
       
       //check if we are during outage
       if(currentTime.after(efosTimeVO.getEfosStartTime()) && currentTime.before(efosTimeVO.getEfosAlertTime()))
       {
         efosAlert = false;
       }             
     }

     return efosAlert;
   }  
  
  
  /*
   * This method gets the efos times from the config file
   */
  private EfosTimeVO getEFOSTimes() throws Exception
  {
    EfosTimeVO efosTimeVO = new EfosTimeVO();
    
    //Get efos times from config file
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();   
    
    //get current day
    Calendar today = Calendar.getInstance();
        
    //EFOS has different down times on Sunday
    if(today.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
        efosTimeVO.setEfosStartTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_SUNDAY_DOWN_START_TIME")));             
        efosTimeVO.setEfosEndTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_SUNDAY_DOWN_END_TIME")));             
        efosTimeVO.setEfosAlertTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_SUNDAY_ALERT_TIME")));             
    }
    else
    {
        efosTimeVO.setEfosStartTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_DOWN_START_TIME")));             
        efosTimeVO.setEfosEndTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_DOWN_END_TIME")));             
        efosTimeVO.setEfosAlertTime(getTimeFromString(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"EFOS_ALERT_TIME")));                   
    }
    
    return efosTimeVO;
  }
  
  /*
   * Returns a calender object for the given time string.
   * 
   * @param String Time in the format of HH:MM:SS (24 hour clock)
   * @return Calender   * 
   */
   private Calendar getTimeFromString(String timeString) throws Exception
   {
   
     //check for nulls
     if(timeString == null)
     {
       throw new Exception("Time string in property file is null.");
     }

     Calendar theTime = null;
     try{
         theTime = Calendar.getInstance();    
      
         String hours = timeString.substring(0,2);
         String minutes = timeString.substring(3,5);
         
         theTime.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hours));
         theTime.set(Calendar.MINUTE,Integer.parseInt(minutes));
     }
     catch(Throwable t)
     {
       throw new Exception("Time in config file is invalid.");
     }
     return theTime;

   }
  private String truncate(String in, int length, String name) 
  {
    if(in != null) 
    {
      if(in.length() > length){
        logger.warn("Truncating " + name + ":" + in + " TO " + in.substring(0,length -1));
        return in.substring(0,length);
      }
    }
    return in;
  }
  private MercuryMessageVO truncateFields(MercuryMessageVO in)
  {
    MercuryMessageVO out = new MercuryMessageVO();
    int ADDRESS_LENGTH = 2000;
    out.setAddress(truncate(in.getAddress(), ADDRESS_LENGTH, "ADDRESS"));

    int REASON_LENGTH = 63;
    out.setADJReasonCode(truncate(in.getADJReasonCode(), ADDRESS_LENGTH, "ADJ REASON CODE"));

    int ADMIN_SEQUENCE_LENGTH = 4;
    out.setAdminSequence(truncate(in.getAdminSequence(), ADMIN_SEQUENCE_LENGTH, "ADMIN SEQUENCE"));
    
    int ORDER_SEQUENCE_LENGTH = 4;
    out.setOrderSequence(truncate(in.getOrderSequence(), ORDER_SEQUENCE_LENGTH, "ORDER_SEQUENCE"));
    
    int MESSAGE_DIRECTION_LENGTH = 8;
    out.setDirection(truncate(in.getDirection(), MESSAGE_DIRECTION_LENGTH, "DIRECTION"));
    
    int SUFFIX_LENGTH = 2;
    out.setSuffix(truncate(in.getSuffix(), SUFFIX_LENGTH, "SUFFIX"));

    int VIEW_QUEUE_LENGTH = 1;
    out.setViewQueue(truncate(in.getViewQueue(), VIEW_QUEUE_LENGTH, "VIEW QUEUE"));
    
    int TO_MESSAGE_DATE_LENGTH = 63;
    out.setRetrievalToDate(truncate(in.getRetrievalToDate(), TO_MESSAGE_DATE_LENGTH, "RETRIEVAL TO DATE"));
    
    int TO_MESSAGE_NUMBER_LENGTH = 63;
    out.setRetrievalToMessage(truncate(in.getRetrievalToMessage(), TO_MESSAGE_NUMBER_LENGTH, "RETRIEVAL TO MESSAGE"));
    
    int FROM_MESSAGE_DATE_LENGTH = 63;
    out.setRetrievalFromDate(truncate(in.getRetrievalFromDate(), FROM_MESSAGE_DATE_LENGTH, "RETRIEVAL FROM DATE"));
    
    int FROM_MESSAGE_NUMBER_LENGTH = 63;
    out.setRetrievalFromMessage(truncate(in.getRetrievalFromMessage(), FROM_MESSAGE_NUMBER_LENGTH, "RETRIEVAL FROM MESSAGE"));
    
    int ROF_NUMBER_LENGTH = 63;
    out.setRofNumber(truncate(in.getRofNumber(), ROF_NUMBER_LENGTH, "ROF"));
    
    int COMBINED_REPORT_NUMBER_LENGTH = 63;
    out.setCombinedReportNumber(truncate(in.getCombinedReportNumber(), COMBINED_REPORT_NUMBER_LENGTH, "COMBINED REPORT NUMBER"));
    
    int RETRIEVAL_FLAG_LENGTH = 1;
    out.setRetrievalFlag(truncate(in.getRetrievalFlag(), RETRIEVAL_FLAG_LENGTH, "RETRIEVA FLAG"));
    
    int SORT_VALUE_LENGTH = 100;
    out.setSortValue(truncate(in.getSortValue(), SORT_VALUE_LENGTH, "SORT VALUE"));
    
    int ASK_ANSWER_CODE_LENGTH = 1;
    out.setAskAnswerCode(truncate(in.getAskAnswerCode(), ASK_ANSWER_CODE_LENGTH, "ASK ANSWER CODE"));
    
    int ZIP_CODE_LENGTH = 10;
    out.setZipCode(truncate(in.getZipCode(), ZIP_CODE_LENGTH, "ZIP CODE"));
    
    int PRODUCT_ID_LENGTH = 10;
    out.setProductID(truncate(in.getProductID(), PRODUCT_ID_LENGTH, "PRODUCT ID"));
    
    int REFERENCE_NUMBER_LENGTH = 100;
    out.setReferenceNumber(truncate(in.getReferenceNumber(), REFERENCE_NUMBER_LENGTH, "REFERENCE NUMBER"));
    
    int SAK_TEXT_LENGTH = 600;
    out.setSakText(truncate(in.getSakText(), SAK_TEXT_LENGTH, "SAK"));
    
    int COMMENTS_LENGTH = 4000;
    out.setComments(truncate(in.getComments(), COMMENTS_LENGTH, "COMMENTS"));
    
    int OPERATOR_LENGTH = 2000;
    out.setOperator(truncate(in.getOperator(), OPERATOR_LENGTH, "OPERATOR"));
    
    int PRIORITY_LENGTH = 10;
    out.setPriority(truncate(in.getPriority(), PRIORITY_LENGTH, "PRIORITY"));
    
    int SPECIAL_INSTRUCTIONS_LENGTH = 4000;
    out.setSpecialInstructions(truncate(in.getSpecialInstructions(), SPECIAL_INSTRUCTIONS_LENGTH, "SPECIAL INSTRUCTIONS"));
    
    int OCCASION_LENGTH = 63;
    out.setOccasion(truncate(in.getOccasion(), OCCASION_LENGTH, "OCCASION"));
    
    int CARD_MESSAGE_LENGTH = 4000;
    out.setCardMessage(truncate(in.getCardMessage(), CARD_MESSAGE_LENGTH, "CARD MESSAGE"));
    
    int SECOND_CHOICE_LENGTH = 2000;
    out.setSecondChoice(truncate(in.getSecondChoice(), SECOND_CHOICE_LENGTH, "SECOND CHOICE"));
    
    int FIRST_CHOICE_LENGTH = 2000;
    out.setFirstChoice(truncate(in.getFirstChoice(), FIRST_CHOICE_LENGTH, "FIRST CHOICE"));
    
    int DELIVERY_DATE_TEXT_LENGTH = 63;
    out.setDeliveryDateText(truncate(in.getDeliveryDateText(), DELIVERY_DATE_TEXT_LENGTH, "DELIVERY DATE TEXT"));
    
    int PHONE_NUMBER_LENGTH = 63;
    out.setPhoneNumber(truncate(in.getPhoneNumber(), PHONE_NUMBER_LENGTH, "PHONE NUMBER"));
    
    int CITY_STATE_ZIP_LENGTH = 2000;
    out.setCityStateZip(truncate(in.getCityStateZip(), CITY_STATE_ZIP_LENGTH, "CSZ"));

    int RECIPIENT_LENGTH = 2000;
    out.setRecipient(truncate(in.getRecipient(), RECIPIENT_LENGTH, "RECIPIENT"));
    
    int FILLING_FLORIST_LENGTH = 2000;
    out.setFillingFlorist(truncate(in.getFillingFlorist(), FILLING_FLORIST_LENGTH, "FILLING FLORIST"));
    
    int SENDING_FLORIST_LENGTH = 2000;
    out.setSendingFlorist(truncate(in.getSendingFlorist(), SENDING_FLORIST_LENGTH, "SENDING FLORIST"));
    
    int OUTBOUND_ID_LENGTH = 2;
    out.setOutboundID(truncate(in.getOutboundID(), OUTBOUND_ID_LENGTH, "OUTBOUND ID"));
    
    int MSG_TYPE_LENGTH = 3;
    out.setMessageType(truncate(in.getMessageType(), MSG_TYPE_LENGTH, "MESSAGE TYPE"));
    
    int MERCURY_STATUS_LENGTH = 2;
    out.setMercuryStatus(truncate(in.getMercuryStatus(), MERCURY_STATUS_LENGTH, "MERCURY STATUS"));
    
    int MERCURY_ORDER_NUMBER_LENGTH = 20;
    out.setMercuryOrderNumber(truncate(in.getMercuryOrderNumber(), MERCURY_ORDER_NUMBER_LENGTH, "MERCURY ORDER NUMBER"));
    
    int MERCURY_MESSAGE_NUMBER_LENGTH = 11;
    out.setMercuryMessageNumber(truncate(in.getMercuryMessageNumber(), MERCURY_MESSAGE_NUMBER_LENGTH, "MERCURY MESSAGE NUMBER"));
    
    int MERCURY_ID_LENGTH = 20;
    out.setMercuryID(truncate(in.getMercuryID(), MERCURY_ID_LENGTH, "MERCURY ID"));
    
    // set non string fields
    out.setCRSEQ(in.getCRSEQ());
    out.setCTSEQ(in.getCTSEQ());
    out.setDeliveryDate(in.getDeliveryDate());
    out.setOrderDate(in.getOrderDate());
    String overUnderString = FieldUtils.formatBigDecimalAsCurrency(getBigDec(new Double(in.getOverUnderCharge())),2,false);      
    //EFOS doesn't like commas
    overUnderString = overUnderString.replace(",", "");
    Double newOverUnderChg = new Double(overUnderString);
    out.setOverUnderCharge(newOverUnderChg.doubleValue());
    out.setPrice(in.getPrice());
    out.setTransmissionDate(in.getTransmissionDate());
    
    
    return out;
  }
  public static void main(String[] args)
  {
      String x = "[TOTAL MESSAGES;1;TEXT]1[MESSAGE NUMBER(1);879;TEXT][MERC MESSAGE TYPE;3;TEXT]FTD[MESSAGE TYPE;9;TEXT]FTD ORDER[CTSEQ;4;TEXT]6753[MESSAGE HEADER;41;TEXT] ";
      x = x + ">>SRETRANSMISSION OF   ";
      x = x + ">>NOUTGOING ORDER[SENDING FLORIST CODE;9;TEXT]90-8400AA[SENDING FLORIST CODE DESCRIPTION;93;TEXT]90-8400AA  FTD.COM/1-800-SEND-FTD ";
      x = x + "                 DOWNERS GROVE, IL 60515       800-554-0993[ORDER NUMBER;11;TEXT]M0903Z-2643[MERCURY MESSAGE NUMBER;11;TEXT]M0903Z-2643[FILLING FLORIST CODE;9;TEXT]90-8590AA[FILLING FLORIST CODE DESCRIPTION;9;TEXT]90-8590AA[RECIPIENT NAME;13;TEXT]CAROL AVENSON[RECIPIENT STREET ADDRESS;51;TEXT]944 JEANNETTE ST ";
      x = x + "                                  [RECIPIENT CITY STATE ZIP;21;TEXT]DES PLAINES, IL 60016[RECIPIENT PHONE;12;TEXT]847/824-4738[DELIVERY DATE;10;TEXT]08/19/2004[OCCASION;14;TEXT]OTHER OCCASION[SPECIAL INSTRUCTIONS;42;TEXT] ";
      x = x + "Additional Devlivery Instructions: - THU ";
      x = x + "[OPERATOR;31;TEXT]  WED SEP 08 04 11:55A     5175[TOTAL RESPONSES;1;TEXT]0[MORE MESSAGES WAITING;2;TEXT]NO ";
    
      try{
    
       //   MercuryProcessor mp = new MercuryProcessor();

          MercuryMessageVO test = new MercuryMessageVO();
          test.setPrice(new Double(1.2));
          test.setAskAnswerCode("  ");
          test.setMessageType("FTD");
          test.setSecondChoice(null);

          MercuryProcessor.massageData(test);
          
      }
      catch(Exception e)
      {
        System.out.println(e);
      }
  }
  
  /**
   * Sends an acknowledgement to Box X.  Validates the response received from Box X
   * @param sendString String to be sent to Box X
   * @param client socket to Box X
   * @param mercuryOpsVO Statistics for the suffix being used
   * @param dao Data access object
   * @return success or failure
   * @throws Exception 
   */
  private boolean sendAck(String sendString, SocketClient client, MercuryOpsVO mercuryOpsVO, MercuryDAO dao) throws Exception
  {
    boolean retval = false;
      
    //get amount of time to wait while data
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
    int timeoutTemp = Integer.parseInt(configUtil.getFrpGlobalParm(MercuryConstants.MERCURY_CONFIG_CONTEXT,"ACK_SOCKET_READ_TIMEOUT"));

    
    //convert seconds to milliseconds
    timeoutTemp = timeoutTemp * 1000;
    
    int oldTimeout = client.getSocketTimeout();
    client.setSocketTimeout(timeoutTemp);
    String ackResponse = null;
    
    try {
      logger.debug(sendString);
      ackResponse = client.send(sendString,true); 
      retval = validateAckResponse(mercuryOpsVO,ackResponse); 
      
    } catch (SocketTimeoutException ste ) {
      String message = "Socket timeout when sending ack/ack ask to Box X. Suffix: " + mercuryOpsVO.getSuffix() + " Messages will be left with a status of 'MQ'";      
      logger.error(message);
      mercuryOpsVO.setAckAsk(true);
      mercuryOpsVO.setResendBatch(false);
      mercuryOpsVO.setAckCount(mercuryOpsVO.getAckCount()+1);
      retval = false;
      
    } finally {
      mercuryOpsVO.setInUse(false);  
      if( mercuryOpsVO.getAckCount()>0 ) 
      {
        String strValue;
        int maxAttempts;
        try 
        {
          strValue = dao.getFrpGlobalParm("MERCURY_INTERFACE","MAX_ACK_COUNT");
          maxAttempts = Integer.parseInt(strValue);
        } catch (Exception e) 
        {
          logger.warn("Parameter MERCURY_INTERFACE/MAX_ACK_COUNT is not set up in FRP.GLOBAL_PARMS correctly");
          maxAttempts=3;
        }
        
        if( mercuryOpsVO.getAckCount()>=maxAttempts ) 
        {
          String message = "Ack of transmission for suffix "+mercuryOpsVO.getSuffix()+" has failed " + mercuryOpsVO.getAckCount() + " times.  Process will be stopped.";
          logger.error(message);
          CommonUtilites.sendSystemMessage(message);  
          mercuryOpsVO.setInUse(true);  //prevents from trying any more
        }
      }
      
      dao.updateMercuryOPS(mercuryOpsVO);
      client.setSocketTimeout(oldTimeout);
    }
    
    return retval;
  }
  
  /**
   * Builds the ACK string and forwards it to be sent to Box X.
   * @param client Socket used to send ACK to Box X.
   * @param mercuryOpsVO Merury ops value object
   * @param dao Data access object
   * @return response from sendAck(String sendString, SocketClient client, 
   * MercuryOpsVO mercuryOpsVO, MercuryDAO dao)
   * @throws Exception 
   */
  private boolean sendAck(SocketClient client, MercuryOpsVO mercuryOpsVO, MercuryDAO dao) throws Exception
  {
    logger.debug("Sending Acknowledgement.");
    String acknowledgement = "[CLIENT RESPONSE]ACKNOWLEDGED";
    acknowledgement = MessageUtilities.buildHeaderAscii(acknowledgement.length()) + acknowledgement;
    return sendAck(acknowledgement, client, mercuryOpsVO, dao);
  }
  
  /**
   * Builds the ackAsk string and forwards it to be sent to Box X.
   * @param client Socket used to send ackAsk to Box X.
   * @param mercuryOpsVO Merury ops value object
   * @param dao Data access object
   * @return response from sendAck(String sendString, SocketClient client, 
   * MercuryOpsVO mercuryOpsVO, MercuryDAO dao)
   * @throws Exception 
   */
  private boolean sendAckAsk(SocketClient client, MercuryOpsVO mercuryOpsVO, MercuryDAO dao) throws Exception 
  {
    logger.debug("Sending Acknowledgement Ask.");
    String ask = this.createAskAckString(mercuryOpsVO);
    ask = MessageUtilities.buildHeaderAscii(ask.length()) + ask;
      
    return sendAck(ask,client,mercuryOpsVO,dao);
  }

  /**
   * Validates the ACK response received from Box X and sets the ackCount, 
   * ackAsk, and resendBatch flags based on the response received.
   * @param mercuryOpsVO Mercury Ops value object 
   * @param ackResponse Response received from Box X
   * @return True if the 
   * @throws Exception 
   */
  private boolean validateAckResponse(MercuryOpsVO mercuryOpsVO, String ackResponse) throws Exception
  { 
    if( ackResponse!=null ) 
    {
      ackResponse = ackResponse.trim();
    }
    
    boolean retval;
    logger.info("Acknowledgement response: " + ackResponse);
    String suffix = mercuryOpsVO.getSuffix();
    if (ackResponse == null)
    {
      String message = "Null response received when sending acknowledgement to Box X. Suffix:" + suffix + ".  Messages will be left with a status of 'MQ'";
      logger.error(message);
      mercuryOpsVO.setAckAsk(true);
      mercuryOpsVO.setResendBatch(false);
      mercuryOpsVO.setAckCount(mercuryOpsVO.getAckCount() + 1);
      retval = false;
    }
    else if (ackResponse.equalsIgnoreCase(ACK_RESPONSE_GOOD))
    {
      mercuryOpsVO.setAckAsk(false);
      mercuryOpsVO.setResendBatch(false);
      mercuryOpsVO.setAckCount(0);
      retval = true;
    }
    else if (ackResponse.equalsIgnoreCase(ACK_RESPONSE_BAD))
    {
      String message = "\"Bad\" response received when sending acknowledgement to Box X. Suffix:" + suffix + ".  Messages will be left with a status of 'MQ'";
      logger.error(message);
      mercuryOpsVO.setAckAsk(false);
      mercuryOpsVO.setResendBatch(true);
      mercuryOpsVO.setAckCount(0);
      retval = false;
    }
    else 
    {
      String message = "Unknown response received when sending acknowledgement to Box X. Suffix: " + suffix + ".  Messages will be left with a status of 'MQ'";
      logger.error(message);
      mercuryOpsVO.setAckAsk(true);
      mercuryOpsVO.setResendBatch(false);
      mercuryOpsVO.setAckCount(mercuryOpsVO.getAckCount() + 1);
      retval = false;
    }
    return retval;
  }
  
  /**
   * Search the source string for credit card numbers and replace all but the 
   * last four digits of the credit card number with �X�.
   * @param message Text to be searched.
   * @return Passed in message with the credit card masked.
   */
  private static String maskFOLCreditCardNumbers(String message)
  {
    Matcher matcher = CCAS_PATTERN.matcher(message);
    int startIdx;
    int endIdx;
    String strCC;
    String strTmp;
    while (matcher.find())
    {
      strCC = matcher.group();
      if (strCC != null)
      {
        strTmp = strCC.substring(CCAS_PREFIX.length(), strCC.length());
        startIdx = matcher.start();
        endIdx = matcher.end();
        int strLength = strTmp.length();
        String newValue = "XXXXXXXXXXXXXXXXXXXXXXXX";
        newValue += strTmp.substring(strLength - 4);
        StringBuffer sb = new StringBuffer();
        sb.append(message.substring(0, startIdx));
        sb.append(CCAS_PREFIX);
        sb.append(newValue.substring(newValue.length() - strLength));
        sb.append(message.substring(endIdx));
        message = sb.toString();
      }
    }
    return message;
  }
  
  /**
   * Parses out the mercury_id from the passed sak text
   * @param sakText Text to search through. 
   * @return Mercury ID parsed from the passed sak text
   */
  private static String getMercuryIdFromSak(String sakText)
  {
    String retval = null;
    Matcher matcher = SAK_PATTERN.matcher(sakText);
    if (matcher.find())
    {
      String strTmp = matcher.group();
      retval = strTmp.substring(1, strTmp.length() - 1);
    }
    return retval;
  }
  
    static private BigDecimal getBigDec(Double dbl)
    {
        BigDecimal ret = null;
        
        if(dbl != null)
        {
            ret = new BigDecimal(dbl.toString());
        }
        
        return ret;
    }
}