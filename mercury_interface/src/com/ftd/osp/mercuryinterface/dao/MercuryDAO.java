package com.ftd.osp.mercuryinterface.dao;


import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;
import com.ftd.osp.mercuryinterface.vo.LastBatchVO;
import com.ftd.osp.mercuryinterface.vo.MercuryMessageVO;
import com.ftd.osp.mercuryinterface.vo.MercuryOpsVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.*;

import javax.naming.InitialContext;


import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains data access methods used within the Mercury Interface project.
 */
public class MercuryDAO 
{
  private Connection conn;
  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.osp.mercuryinterface.MercuryDAO";
  
  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  
  /**
   * Constructor
   * @param Connection
   */
  public MercuryDAO(Connection conn)
  {
    this.conn = conn;
    logger =  new Logger(LOGGER_CATEGORY);
  }
  
  /**
   * Clear the mercury queue
   * 
   */
  public void clearQueue() throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("CLEAR_MERCURY_QUEUE");
      Map paramMap = new HashMap();
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String status = (String) dataAccessUtil.execute(dataRequest);

      if(status.equals("N"))
      {
          throw new Exception("Queue could not be cleared.");
      }

  }   

  /**
   * Returns the MercuryOps rows that are available
   * 
   * @param String suffic
   * @return MercuryOpsVO
   */
  public MercuryOpsVO getMercuryOpsBySuffix(String suffix) throws Exception
  {
      List suffixList = getMercuryOpsBySuffixAvailable(suffix,null);
      MercuryOpsVO vo = null;
      
      //list will only contain 1 value if any
      if(suffixList !=null && suffixList.size() > 0)
      {
        vo = (MercuryOpsVO)suffixList.get(0);
      }

      return vo;
  }

  /**
   * Returns the MercuryOps rows that are available
   * 
   * @param String suffic
   * @return MercuryOpsVO
   */
  public List getMercuryOpsByAvailable(boolean available) throws Exception
  {
      return getMercuryOpsBySuffixAvailable(null,getBoolean(available));
  }
      
 
  /**
   * Returns the MercuryOps rows that are available
   * 
   * @param String suffic
   * @return MercuryOpsVO
   */
  public List getMercuryOpsBySuffixAvailable(String suffix,String available) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("VIEW_MERCURY_OPS");
      Map paramMap = new HashMap();
      paramMap.put("IN_SUFFIX",suffix); 
      paramMap.put("IN_AVAILABLE_FLAG",available); 
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      Document xmlDoc = (Document)dataAccessUtil.execute(dataRequest);
   
      List list = new ArrayList();

      String xpath = "SUFFIXS/SUFFIX";
      NodeList nl = DOMUtil.selectNodes(xmlDoc, xpath);
      if(nl.getLength() > 0){
        for (int i = 0; i < nl.getLength(); i++)
        {
            Element node = (Element)nl.item(i);

            MercuryOpsVO vo = new MercuryOpsVO();

            vo.setSuffix(node.getAttribute("suffix"));
            vo.setInUse(getBoolean(node.getAttribute("in_use_flag")));
            vo.setLastTransmission(formatStringToUtilDate(node.getAttribute("last_transmission")));    
            vo.setLastActivitiy(formatStringToUtilDate(node.getAttribute("last_message_activity")));    
            vo.setAvailable(getBoolean(node.getAttribute("available_flag")));
            vo.setSendNewOrders(getBoolean(node.getAttribute("send_new_orders")));
            vo.setSleepTime(getInt(node.getAttribute("sleep_time")));
            vo.setMaxBatchSize(getInt(node.getAttribute("batch_size")));
            vo.setAckAsk(getBoolean(node.getAttribute("ack_ask")));
            vo.setAckCount(getInt(node.getAttribute("ack_count")));
            vo.setResendBatch(getBoolean(node.getAttribute("resend_batch")));
            
            list.add(vo);

        }
      }

      
     return list;    
  }     
 

  public MercuryMessageVO getMercuryMessageByMessageNumber(String messageNumber) throws Exception
  {
    MercuryMessageVO vo = new MercuryMessageVO();
    boolean isEmpty = true;
    DataRequest dataRequest = new DataRequest();

    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_MERCURY_MESSAGE_NUMBER", messageNumber);
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("GET_MERCURY_BY_MESSAGE_NUMBER");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    ResultSet output = (ResultSet) dataAccessUtil.execute(dataRequest);
    while (output.next())
    {
      isEmpty = false;
      vo.setMercuryID(output.getString("MERCURY_ID"));
      vo.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
      vo.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
      vo.setMercuryStatus(output.getString("MERCURY_STATUS"));
      vo.setMessageType(output.getString("MSG_TYPE"));
      vo.setOutboundID(output.getString("OUTBOUND_ID"));
      vo.setSendingFlorist(output.getString("SENDING_FLORIST"));
      vo.setFillingFlorist(output.getString("FILLING_FLORIST"));
      vo.setOrderDate(output.getDate("ORDER_DATE"));
      vo.setRecipient(output.getString("RECIPIENT"));
      vo.setAddress(output.getString("ADDRESS"));
      vo.setCityStateZip(output.getString("CITY_STATE_ZIP"));
      vo.setPhoneNumber(output.getString("PHONE_NUMBER"));
      vo.setDeliveryDate(output.getDate("DELIVERY_DATE"));
      vo.setDeliveryDateText(output.getString("DELIVERY_DATE_TEXT"));
      vo.setFirstChoice(output.getString("FIRST_CHOICE"));
      vo.setSecondChoice(output.getString("SECOND_CHOICE"));
      vo.setPrice(new Double(output.getDouble("PRICE")));
      vo.setCardMessage(output.getString("CARD_MESSAGE"));
      vo.setOccasion(output.getString("OCCASION"));
      vo.setSpecialInstructions(output.getString("SPECIAL_INSTRUCTIONS"));
      vo.setPriority(output.getString("PRIORITY"));
      vo.setOperator(output.getString("OPERATOR"));
      vo.setComments(output.getString("COMMENTS"));
      vo.setSakText(output.getString("SAK_TEXT"));
      vo.setCTSEQ(output.getInt("CTSEQ"));
      vo.setCRSEQ(output.getInt("CRSEQ"));
      vo.setTransmissionDate(output.getDate("TRANSMISSION_TIME"));
      vo.setReferenceNumber(output.getString("REFERENCE_NUMBER"));
      vo.setProductID(output.getString("PRODUCT_ID"));
      vo.setZipCode(output.getString("ZIP_CODE"));
      vo.setAskAnswerCode(output.getString("ASK_ANSWER_CODE"));
      vo.setSortValue(output.getString("SORT_VALUE"));
      vo.setRetrievalFlag(output.getString("RETRIEVAL_FLAG"));
      vo.setRetrievalToMessage(output.getString("TO_MESSAGE_NUMBER"));
      vo.setRetrievalFromMessage(output.getString("FROM_MESSAGE_NUMBER"));
      vo.setRetrievalFromDate(output.getString("FROM_MESSAGE_DATE"));
      vo.setRetrievalToDate(output.getString("TO_MESSAGE_DATE"));
      vo.setViewQueue(output.getString("VIEW_QUEUE"));
      vo.setSuffix(output.getString("SUFFIX"));
      vo.setDirection(output.getString("MESSAGE_DIRECTION"));
      vo.setOrderSequence(output.getString("ORDER_SEQ"));
      vo.setAdminSequence(output.getString("ADMIN_SEQ"));
      vo.setCombinedReportNumber(output.getString("COMBINED_REPORT_NUMBER"));
      vo.setRofNumber(output.getString("ROF_NUMBER"));
      vo.setOverUnderCharge(output.getDouble("OVER_UNDER_CHARGE"));
      vo.setADJReasonCode(output.getString("ADJ_REASON_CODE"));
    }

    if (isEmpty)
      return null;
    else
      return vo;

  }

  /**
   * Get a list of Mercury Messages based on suffic and status
   * @param String suffix
   * @param String status
   * @boolean true=return null suffixs also
   */  
  public List getMercuryMessageBySuffixStatus(String suffix,String status,boolean getNull,int recordLimit) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("VIEW_MERCURY_FOR_OUTBOUND");
      Map paramMap = new HashMap();
      paramMap.put("IN_MERCURY_STATUS",status); 
      paramMap.put("IN_OUTBOUND_ID",suffix); 
      paramMap.put("IN_GET_NULL_FLAG",getBoolean(getNull)); 
      paramMap.put("IN_RECORD_LIMIT",new Integer(recordLimit));       
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      List messages = new ArrayList();;
 
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }

      CachedResultSet rs = (CachedResultSet)outputs.get("RegisterOutParameter1");
       
      //if something was found
      while(rs.next())
      {        
      
        MercuryMessageVO vo = new MercuryMessageVO();

        vo.setMercuryID((String)rs.getObject(1));
        vo.setMercuryMessageNumber((String)rs.getObject(2));
        vo.setMercuryOrderNumber((String)rs.getObject(3));
        vo.setMercuryStatus((String)rs.getObject(4));
        vo.setMessageType((String)rs.getObject(5));
        vo.setOutboundID((String)rs.getObject(6));
        vo.setSendingFlorist((String)rs.getObject(7));
        vo.setFillingFlorist((String)rs.getObject(8));
        vo.setOrderDate(getUtilDate(rs.getObject(9)));
        vo.setRecipient((String)rs.getObject(10));
        vo.setAddress((String)rs.getObject(11));
        vo.setCityStateZip((String)rs.getObject(12));
        vo.setPhoneNumber((String)rs.getObject(13));
        vo.setDeliveryDate(getUtilDate(rs.getObject(14)));
        vo.setDeliveryDateText((String)rs.getObject(15));
        vo.setFirstChoice((String)rs.getObject(16));
        vo.setSecondChoice((String)rs.getObject(17));
        vo.setPrice(getDoubleAmount(rs.getObject(18)));
        vo.setCardMessage((String)rs.getObject(19));
        vo.setOccasion((String)rs.getObject(20));
        vo.setSpecialInstructions((String)rs.getObject(21));
        vo.setPriority((String)rs.getObject(22));
        vo.setOperator((String)rs.getObject(23));
        vo.setComments((String)rs.getObject(24));
        vo.setSakText((String)rs.getObject(25));
        vo.setCTSEQ(getInt(rs.getObject(26)));
        vo.setCRSEQ(getInt(rs.getObject(27)));
        vo.setTransmissionDate(getUtilDate(rs.getObject(28)));
        vo.setReferenceNumber((String)rs.getObject(29));
        vo.setProductID((String)rs.getObject(30));
        vo.setZipCode((String)rs.getObject(31));
        vo.setAskAnswerCode((String)rs.getObject(32));
        vo.setSortValue((String)rs.getObject(33));
        vo.setRetrievalFlag((String)rs.getObject(34));
        vo.setCombinedReportNumber((String)rs.getObject(35));
        vo.setRofNumber((String)rs.getObject(36));
        vo.setADJReasonCode((String)rs.getObject(37));
        vo.setOverUnderCharge(getAmount(rs.getObject(38)));
        vo.setRetrievalFromMessage((String)rs.getObject(39));
        vo.setRetrievalFromDate((String)rs.getObject(40));
        vo.setRetrievalToMessage((String)rs.getObject(41));
        vo.setRetrievalToDate((String)rs.getObject(42));

        messages.add(vo);

      }
      
     return messages;    
  }    
  
  /**
   * Updates the MercuryOps row for the give suffix.
   * 
   * @param String suffic
   * @return MercuryOpsVO
   */
  public void updateMercuryOPS(MercuryOpsVO vo) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("UPDATE_MERCURY_OPS");
      Map paramMap = new HashMap();
      paramMap.put("IN_SUFFIX",vo.getSuffix()); 
      paramMap.put("IN_IN_USE_FLAG",getBoolean(vo.isInUse())); 
      paramMap.put("IN_LAST_TRANSMISSION",CommonUtilites.getTimestamp(vo.getLastTransmission())); 
      paramMap.put("IN_LAST_MESSAGE_ACTIVITY",CommonUtilites.getTimestamp(vo.getLastActivitiy())); 
      paramMap.put("IN_AVAILABLE_FLAG",getBoolean(vo.isAvailable())); 
      paramMap.put("IN_SEND_NEW_ORDERS",getBoolean(vo.isSendNewOrders())); 
      paramMap.put("IN_SLEEP_TIME",new Integer(vo.getSleepTime())); 
      paramMap.put("IN_BATCH_SIZE",new Integer(vo.getMaxBatchSize())); 
      paramMap.put("IN_ACK_ASK",getBoolean(vo.isAckAsk()));
      paramMap.put("IN_ACK_COUNT",new Integer(vo.getAckCount()));
      paramMap.put("IN_RESEND_BATCH",getBoolean(vo.isResendBatch()));
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      

  }   
 
 
 
  public void updateMercuryStatus(MercuryMessageVO vo) throws Exception 
  {
    updateMercuryStatus(vo, null);
  }
   /**
   * Update a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void updateMercuryStatus(MercuryMessageVO vo, String suffix) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("UPDATE_MERCURY_STATUS_TIMEST");
      Map paramMap = new HashMap();   
     
      paramMap.put("IN_MERCURY_ID",vo.getMercuryID());
      paramMap.put("IN_MERCURY_STATUS",vo.getMercuryStatus());
      paramMap.put("IN_TRANSMISSION_TIME",CommonUtilites.getTimestamp(vo.getTransmissionDate()));
      paramMap.put("IN_SAK_TEXT",vo.getSakText());
      paramMap.put("IN_SUFFIX", suffix);
      
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      
    
  }
 
 
  /**
   * Update a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void updateMercuryMessage(MercuryMessageVO vo) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      //mare sure all messages can fit in the DB
      vo = trucateFields(vo);    
    
      dataRequest.setStatementID("UPDATE_MERCURY");
      Map paramMap = new HashMap();   
     
      paramMap.put("IN_MERCURY_ID",vo.getMercuryID());
      paramMap.put("IN_MERCURY_MESSAGE_NUMBER",vo.getMercuryMessageNumber());
      paramMap.put("IN_MERCURY_STATUS",vo.getMercuryStatus());
      paramMap.put("IN_SAK_TEXT",vo.getSakText());
      paramMap.put("IN_CTSEQ",new Long(vo.getCTSEQ()));
      paramMap.put("IN_CRSEQ",new Long(vo.getCRSEQ()));
      paramMap.put("IN_TRANSMISSION_TIME",CommonUtilites.getTimestamp(vo.getTransmissionDate()));
      paramMap.put("IN_MERCURY_ORDER_NUMBER",vo.getMercuryOrderNumber());
      paramMap.put("IN_VIEW_QUEUE",vo.getViewQueue());
      paramMap.put("IN_MESSAGE_DIRECTION", vo.getDirection());
      paramMap.put("IN_SUFFIX", vo.getSuffix());
      paramMap.put("IN_ORDER_SEQ", vo.getOrderSequence());
      paramMap.put("IN_ADMIN_SEQ", vo.getAdminSequence());

      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      
    
  }
 
 
   /**
   * insert a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void insertMercuryMessage(MercuryMessageVO vo) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      //mare sure all messages can fit in the DB
      vo = trucateFields(vo);
    
      
      Map paramMap = new HashMap();   
     
      paramMap.put("IN_MERCURY_ID",vo.getMercuryID());
      paramMap.put("IN_MERCURY_MESSAGE_NUMBER",vo.getMercuryMessageNumber());
      paramMap.put("IN_MERCURY_ORDER_NUMBER",vo.getMercuryOrderNumber());
      paramMap.put("IN_MERCURY_STATUS",vo.getMercuryStatus());
      paramMap.put("IN_MSG_TYPE",vo.getMessageType());
      paramMap.put("IN_OUTBOUND_ID",vo.getOutboundID());
      paramMap.put("IN_SENDING_FLORIST",vo.getSendingFlorist());
      paramMap.put("IN_FILLING_FLORIST",vo.getFillingFlorist());
      paramMap.put("IN_ORDER_DATE",CommonUtilites.getTimestamp(vo.getOrderDate()));
      paramMap.put("IN_RECIPIENT",vo.getRecipient());
      paramMap.put("IN_ADDRESS",vo.getAddress());
      paramMap.put("IN_CITY_STATE_ZIP",vo.getCityStateZip());
      paramMap.put("IN_PHONE_NUMBER",vo.getPhoneNumber());
      paramMap.put("IN_DELIVERY_DATE",CommonUtilites.getTimestamp(vo.getDeliveryDate()));
      paramMap.put("IN_DELIVERY_DATE_TEXT",vo.getDeliveryDateText());
      paramMap.put("IN_FIRST_CHOICE",vo.getFirstChoice());
      paramMap.put("IN_SECOND_CHOICE",vo.getSecondChoice());      
      paramMap.put("IN_CARD_MESSAGE",vo.getCardMessage());
      paramMap.put("IN_OCCASION",vo.getOccasion());
      paramMap.put("IN_SPECIAL_INSTRUCTIONS",vo.getSpecialInstructions());
      paramMap.put("IN_PRIORITY",vo.getPriority());
      paramMap.put("IN_OPERATOR",vo.getOperator());
      paramMap.put("IN_COMMENTS",vo.getComments());
      paramMap.put("IN_SAK_TEXT",vo.getSakText());
      paramMap.put("IN_CTSEQ",new Long(vo.getCTSEQ()));
      paramMap.put("IN_CRSEQ",new Long(vo.getCRSEQ()));
      paramMap.put("IN_TRANSMISSION_TIME",CommonUtilites.getTimestamp(vo.getTransmissionDate()));
      paramMap.put("IN_REFERENCE_NUMBER",vo.getReferenceNumber());
      paramMap.put("IN_PRODUCT_ID",vo.getProductID());
      paramMap.put("IN_ZIP_CODE",vo.getZipCode());
      paramMap.put("IN_ASK_ANSWER_CODE",vo.getAskAnswerCode());
      paramMap.put("IN_SORT_VALUE",vo.getSortValue());
      paramMap.put("IN_RETRIEVAL_FLAG",vo.getRetrievalFlag());
      paramMap.put("IN_COMBINED_REPORT_NUMBER",vo.getCombinedReportNumber());
      paramMap.put("IN_ROF_NUMBER",vo.getRofNumber());
      paramMap.put("IN_ADJ_REASON_CODE",vo.getADJReasonCode());
      paramMap.put("IN_OVER_UNDER_CHARGE",Double.toString(vo.getOverUnderCharge()));
      paramMap.put("IN_FROM_MESSAGE_NUMBER",vo.getRetrievalFromMessage());
      paramMap.put("IN_FROM_MESSAGE_DATE",vo.getRetrievalFromDate());
      paramMap.put("IN_TO_MESSAGE_NUMBER",vo.getRetrievalToMessage());
      paramMap.put("IN_TO_MESSAGE_DATE",vo.getRetrievalToDate());       
      paramMap.put("IN_PRICE",doubleToString(vo.getPrice()));
      paramMap.put("IN_VIEW_QUEUE",vo.getViewQueue());      
      paramMap.put("IN_MESSAGE_DIRECTION", vo.getDirection());
      paramMap.put("IN_COMP_ORDER", new String());
      paramMap.put("IN_REQUIRE_CONFIRMATION", new String("N"));
      paramMap.put("IN_SUFFIX", vo.getSuffix());
      paramMap.put("IN_ORDER_SEQ", vo.getOrderSequence());
      paramMap.put("IN_ADMIN_SEQ", vo.getAdminSequence());
      paramMap.put("IN_OLD_PRICE",  new String());
      paramMap.put("IN_ORIG_COMPLAINT_COMM_TYPE_ID", null);
      paramMap.put("IN_NOTI_COMPLAINT_COMM_TYPE_ID", null);

      dataRequest.setStatementID("INSERT_MERCURY");


      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      
    
  }


   /**
   * insert a Mercury Inbound JMS message into the OJMS.MERCURY_INBOUNT table.
   * 
   * @param mercuryID
   */
  public void insertMercuryInboundMDB(String mercuryID) throws Exception
  {
	  //Send JMS message
	  logger.info("Sending Mercury Message to MERCURY INBOUND QUEUE. MercuryId="+mercuryID);
	  InitialContext initialContext = new InitialContext();
	  
      MessageToken token = new MessageToken();
      token.setMessage(mercuryID);
      
      token.setJMSCorrelationID((String)token.getMessage());
      token.setStatus("MERCURYINBOUND");

      Dispatcher dispatcher = Dispatcher.getInstance();
      dispatcher.dispatchTextMessage(initialContext,token);
      logger.info("Message sent.");
      
  }

   /**
   * insert a row in the Mercury Message table.
   * 
   * @param MercuryMessageVO
   */
  public void updateSuspend(String floristID,String suspendInd,String susID,String resID) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("UPDATE_FLORIST_GOTO_SUSPEND");
      Map paramMap = new HashMap();   
     
      paramMap.put("IN_FLORIST_ID",floristID);
      paramMap.put("IN_SUSPEND_INDICATOR",suspendInd);
      paramMap.put("IN_GOTO_FLORIST_SUS_MERC_ID",susID);
      paramMap.put("IN_GOTO_FLORIST_RES_MERC_ID",resID);

      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      
    
  }


     /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {
    
    
      java.util.Date theDate = null;
      boolean test  = false;
      if(time != null){
              if(test || time instanceof Timestamp)
              {
                Timestamp timestamp = (Timestamp)time;
                theDate = new java.util.Date(timestamp.getTime());
              }
              else
              {
                theDate = (java.util.Date)time;
                //theDate = new java.util.Date(timestamp.getTime());                
               }
      }


      return theDate;
    }
    
    /*
     * Convert a database boolean to a java boolean
     */
     private boolean getBoolean(String dbBool)
     {
       return (dbBool == null || dbBool.equalsIgnoreCase("N") ? false : true);
     }

    /*
     * Convert a java boolean to a DB boolean
     */
     private String getBoolean(boolean bool)
     {
       return (bool ? "Y" : "N");
     }
  
    private double getAmount(Object obj)
    {
      double value = 0.0;
      if(obj != null)
      {
        value = Double.parseDouble(obj.toString());
      }

      return value;
    }  
    
    private Double getDoubleAmount(Object obj)
    {
      Double value = null;
      if(obj != null)
      {
        value = new Double(obj.toString());
      }

      return value;
    }  
    
    private String doubleToString(Double value)
    {
        String ret = null;
    
        if(value != null)
        {
            ret = value.toString();
        }
        
        return ret;
    }
  
    private int getInt(Object obj)
    {
      int value = 0;
      if(obj != null)
      {
        value = Integer.parseInt(obj.toString());
      }

      return value;
    }    
 
/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				nd converts it to a Util date of yyyy-mm-dd.
 * @param String data string
 * @return java.sql.Date  (returns null if invalid date)
 * @throws ParseException
 */
public static java.util.Date formatStringToUtilDate(String strDate) 
                throws ParseException{

    String inDateFormat = "";
    int dateLength = 0;
    int firstSep = 0;
    int lastSep = 0;

    java.util.Date returnDate = null;

    if ((strDate != null) && (!strDate.trim().equals(""))) {

		    // set input date format
		    dateLength = strDate.length();
		    int firstTimeSep = strDate.indexOf(":");
		    int firstDashSep = strDate.indexOf("-");
        if(dateLength > 20){
            inDateFormat = "yyyy-MM-dd HH:mm:ss.S";                 
        }        
		    else if ( firstTimeSep > 0) {
			    inDateFormat = "yyyy-MM-dd hh:mm:ss";
		    } 
        else if(firstDashSep > 0){
			    int lastDashSep = strDate.lastIndexOf("-");          
          //check how many in years
          if(strDate.length() - lastDashSep == 4)
          {//4 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yyyy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yyyy";              
            }            
          }
          else
          {//2 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yy";              
            }            
          }
        }
        else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
    		SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );

		   returnDate = sdfInput.parse(strDate);
    }

	return returnDate;
}

/**
 * Truncate fields before inserting into DB.
 * This is to insure the insert will be ok.
 */
 private MercuryMessageVO trucateFields(MercuryMessageVO vo) throws Exception
 {
      vo.setMercuryID(truncateField(vo.getMercuryID(),"IN_MERCURY_ID",vo.getMercuryID(),20));
      vo.setMercuryMessageNumber(truncateField(vo.getMercuryID(),"IN_MERCURY_MESSAGE_NUMBER",vo.getMercuryMessageNumber(),11));
      vo.setMercuryOrderNumber(truncateField(vo.getMercuryID(),"IN_MERCURY_ORDER_NUMBER",vo.getMercuryOrderNumber(),11));
      vo.setMercuryStatus(truncateField(vo.getMercuryID(),"IN_MERCURY_STATUS",vo.getMercuryStatus(),2));
      vo.setMessageType(truncateField(vo.getMercuryID(),"IN_MSG_TYPE",vo.getMessageType(),3));
      vo.setOutboundID(truncateField(vo.getMercuryID(),"IN_OUTBOUND_ID",vo.getOutboundID(),2));
      vo.setSendingFlorist(truncateField(vo.getMercuryID(),"IN_SENDING_FLORIST",vo.getSendingFlorist(),2000));
      vo.setFillingFlorist(truncateField(vo.getMercuryID(),"IN_FILLING_FLORIST",vo.getFillingFlorist(),2000));
      vo.setRecipient(truncateField(vo.getMercuryID(),"IN_RECIPIENT",vo.getRecipient(),2000));
      vo.setAddress(truncateField(vo.getMercuryID(),"IN_ADDRESS",vo.getAddress(),2000));
      vo.setCityStateZip(truncateField(vo.getMercuryID(),"IN_CITY_STATE_ZIP",vo.getCityStateZip(),2000));
      vo.setPhoneNumber(truncateField(vo.getMercuryID(),"IN_PHONE_NUMBER",vo.getPhoneNumber(),63));
      vo.setDeliveryDateText(truncateField(vo.getMercuryID(),"IN_DELIVERY_DATE_TEXT",vo.getDeliveryDateText(),63));
      vo.setFirstChoice(truncateField(vo.getMercuryID(),"IN_FIRST_CHOICE",vo.getFirstChoice(),2000));
      vo.setSecondChoice(truncateField(vo.getMercuryID(),"IN_SECOND_CHOICE",vo.getSecondChoice(),2000));
      vo.setCardMessage(truncateField(vo.getMercuryID(),"IN_CARD_MESSAGE",vo.getCardMessage(),4000));
      vo.setOccasion(truncateField(vo.getMercuryID(),"IN_OCCASION",vo.getOccasion(),63));
      vo.setSpecialInstructions(truncateField(vo.getMercuryID(),"IN_SPECIAL_INSTRUCTIONS",vo.getSpecialInstructions(),4000));
      vo.setPriority(truncateField(vo.getMercuryID(),"IN_PRIORITY",vo.getPriority(),10));
      vo.setOperator(truncateField(vo.getMercuryID(),"IN_OPERATOR",vo.getOperator(),2000));
      vo.setComments(truncateField(vo.getMercuryID(),"IN_COMMENTS",vo.getComments(),4000));
      vo.setSakText(truncateField(vo.getMercuryID(),"IN_SAK_TEXT",vo.getSakText(),600));
      vo.setReferenceNumber(truncateField(vo.getMercuryID(),"IN_REFERENCE_NUMBER",vo.getReferenceNumber(),100));
      vo.setProductID(truncateField(vo.getMercuryID(),"IN_PRODUCT_ID",vo.getProductID(),10));
      vo.setZipCode(truncateField(vo.getMercuryID(),"IN_ZIP_CODE",vo.getZipCode(),10));
      vo.setAskAnswerCode(truncateField(vo.getMercuryID(),"IN_ASK_ANSWER_CODE",vo.getAskAnswerCode(),1));
      vo.setSortValue(truncateField(vo.getMercuryID(),"IN_SORT_VALUE",vo.getSortValue(),100));
      vo.setRetrievalFlag(truncateField(vo.getMercuryID(),"IN_RETRIEVAL_FLAG",vo.getRetrievalFlag(),1));
      vo.setCombinedReportNumber(truncateField(vo.getMercuryID(),"IN_COMBINED_REPORT_NUMBER",vo.getCombinedReportNumber(),63));
      vo.setRofNumber(truncateField(vo.getMercuryID(),"IN_ROF_NUMBER",vo.getRofNumber(),63));
      vo.setADJReasonCode(truncateField(vo.getMercuryID(),"IN_ADJ_REASON_CODE",vo.getADJReasonCode(),63));
      vo.setRetrievalFromMessage(truncateField(vo.getMercuryID(),"IN_FROM_MESSAGE_NUMBER",vo.getRetrievalFromMessage(),63));
      vo.setRetrievalToMessage(truncateField(vo.getMercuryID(),"IN_TO_MESSAGE_NUMBER",vo.getRetrievalToMessage(),63));   

    return vo;
 }

  /*
   * Truncate a value.
   * If the value is over the max allowed size a system message is sent out.
   */
  public String truncateField(String mercuryMessageID,String fieldName, String value, int maxLength)
    throws Exception
  {
    if(value != null && value.length() > maxLength)
    {
    
      String message = "Mercury Message field excedes database max.  Value truncated.  Mercury Message=" + mercuryMessageID + ", Field=" + fieldName + ", Size=" + value.length() + ", Max size allowed=" + maxLength;
      CommonUtilites.sendSystemMessage(message);

      value = value.substring(0,maxLength);
    }
    
    return value;
  }
  
  
  /**
   * Get a list of Mercury Messages based on suffic that are in am 'MQ' status
   * @param String suffix
   * @return a list of MercuryMessageVO populate with values from the database
   * @throws Exception 
   */
  public List getUnSakedMessagesForSuffix(String suffix) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("VIEW_UNSAKED_MSGS");
    Map paramMap = new HashMap();
    paramMap.put("IN_OUTBOUND_ID", suffix);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    List messages = new ArrayList();
    
    CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
    //if something was found
    while (rs.next())
    {
      MercuryMessageVO vo = new MercuryMessageVO();
      vo.setMercuryID((String)rs.getObject(1));
      vo.setMercuryMessageNumber((String)rs.getObject(2));
      vo.setMercuryOrderNumber((String)rs.getObject(3));
      vo.setMercuryStatus((String)rs.getObject(4));
      vo.setMessageType((String)rs.getObject(5));
      vo.setOutboundID((String)rs.getObject(6));
      vo.setSendingFlorist((String)rs.getObject(7));
      vo.setFillingFlorist((String)rs.getObject(8));
      vo.setOrderDate(getUtilDate(rs.getObject(9)));
      vo.setRecipient((String)rs.getObject(10));
      vo.setAddress((String)rs.getObject(11));
      vo.setCityStateZip((String)rs.getObject(12));
      vo.setPhoneNumber((String)rs.getObject(13));
      vo.setDeliveryDate(getUtilDate(rs.getObject(14)));
      vo.setDeliveryDateText((String)rs.getObject(15));
      vo.setFirstChoice((String)rs.getObject(16));
      vo.setSecondChoice((String)rs.getObject(17));
      vo.setPrice(getDoubleAmount(rs.getObject(18)));
      vo.setCardMessage((String)rs.getObject(19));
      vo.setOccasion((String)rs.getObject(20));
      vo.setSpecialInstructions((String)rs.getObject(21));
      vo.setPriority((String)rs.getObject(22));
      vo.setOperator((String)rs.getObject(23));
      vo.setComments((String)rs.getObject(24));
      vo.setSakText((String)rs.getObject(25));
      vo.setCTSEQ(getInt(rs.getObject(26)));
      vo.setCRSEQ(getInt(rs.getObject(27)));
      vo.setTransmissionDate(getUtilDate(rs.getObject(28)));
      vo.setReferenceNumber((String)rs.getObject(29));
      vo.setProductID((String)rs.getObject(30));
      vo.setZipCode((String)rs.getObject(31));
      vo.setAskAnswerCode((String)rs.getObject(32));
      vo.setSortValue((String)rs.getObject(33));
      vo.setRetrievalFlag((String)rs.getObject(34));
      vo.setCombinedReportNumber((String)rs.getObject(35));
      vo.setRofNumber((String)rs.getObject(36));
      vo.setADJReasonCode((String)rs.getObject(37));
      vo.setOverUnderCharge(getAmount(rs.getObject(38)));
      vo.setRetrievalFromMessage((String)rs.getObject(39));
      vo.setRetrievalFromDate((String)rs.getObject(40));
      vo.setRetrievalToMessage((String)rs.getObject(41));
      vo.setRetrievalToDate((String)rs.getObject(42));
      messages.add(vo);
    }
    return messages;
  }   
  
  /**
   * Retrieves the MERCURY.MERCURY_BATCH record for the passes suffix
   * @param suffix suffix
   * @return LsatBatchVO representing the MERCURY.MERCURY_BATCH record returned by the database;
   * @throws Exception 
   */
  public LastBatchVO getLastBatchBySuffix(String suffix) throws Exception
  {
    LastBatchVO vo = null;
    String lastString = null;
    
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("VIEW_LAST_BATCH");
    Map paramMap = new HashMap();
    paramMap.put("IN_SUFFIX", suffix);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
    
    //if something was found
    if (rs.next()) 
    {
      vo = new LastBatchVO();
      vo.setSuffix((String)rs.getObject("suffix"));
      lastString = rs.getString("last_sent1");
      if (rs.getString("last_sent2") != null) 
      {
        lastString = lastString + rs.getString("last_sent2");
      }
      if (rs.getString("last_sent3") != null) 
      {
        lastString = lastString + rs.getString("last_sent3");
      }
      vo.setLastBatchSent(lastString);
    }
    else 
    {
      //throw new Exception("Could not find the last batch record for suffix "+suffix+".");
      // this exception causes errors with new suffixes, just log the event and move on
      logger.error("Could not find the last batch record for suffix "+suffix+".");
      vo = new LastBatchVO();
      vo.setSuffix(suffix);
    }
    return vo;
  }
  
  /**
   * Updates the record in the MERCURY.MERCURY_BATCH table for the target suffix
   * @param vo LastBatchVO representing the MERCURY.MERCURY_BATCH record to update
   * @throws Exception 
   */
  public void updateLastBatch(LastBatchVO vo) throws Exception
  {
    if( vo==null ) 
    {
      return;
    }

    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    
    dataRequest.setStatementID("UPDATE_LAST_BATCH");
    Map paramMap = new HashMap();
    paramMap.put("IN_SUFFIX",vo.getSuffix());
    
    String inString = vo.getLastBatchSent();
    String[] outString = splitString(inString);
    paramMap.put("IN_BATCH_SENT1", outString[0]);
    paramMap.put("IN_BATCH_SENT2", outString[1]);
    paramMap.put("IN_BATCH_SENT3", outString[2]);
    
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

  }
  
  /**
   * This new method will split a string that could be up to 9,999 characters into 
   * three strings of 4,000 characters max for database storage
   * @param String to split (9,999 characters max)
   * @return String array of 4,000 characters max each
   * @throws Exception 
   */
  public String[] splitString(String inString) throws Exception
  {
    String[] retString = new String[3];
    int maxLength = 4000;
    int inLength = inString.length();
    
    if (inLength <= maxLength) {
      retString[0]=inString.substring(0,inLength);
    } else {
      retString[0]=inString.substring(0, maxLength);
      if (inLength <= maxLength * 2) {
        retString[1]=inString.substring(maxLength, inLength);
      } else 
      {
        retString[1]=inString.substring(maxLength, maxLength*2);
        retString[2]=inString.substring(maxLength*2, inLength);
      }
    }
    
    return retString;
  }
  
  /**
   * This new method will call .MISC_PKG.GET_GLOBAL_PARM_VALUE to retrieve the 
   * value stored in the FRP.GLOBAL_PARMS table for the context and key passed.
   * @param context to search for
   * @param key to search for
   * @return value for the passed context and key
   * @throws Exception 
   */
  public String getFrpGlobalParm(String context, String key) throws Exception
  {
    String value = null;
    
    // Try cache first
    GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
    if (parmHandler != null)
    {
      value = parmHandler.getFrpGlobalParm(context, key);
    }
    else 
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_GLOBAL_PARAM");
      Map paramMap = new HashMap();
      paramMap.put("IN_CONTEXT", context);
      paramMap.put("IN_PARAM", key);
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String rs = (String)dataAccessUtil.execute(dataRequest);
      value = rs;
      dataRequest.reset();
    }
    return value;
  }
  
}
