package com.ftd.rulesaction;

import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.constant.OrderConstants;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.orderservice.exception.OrderServiceException;
import com.ftd.orderservice.service.OrderService;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the Send FTD message action 
 * 
 * @author kdatchan
 *
 */
@Component
public class SendFTDMsgAction extends RulesAction {

	private static final Logger LOGGER = Logger.getLogger(SendFTDMsgAction.class);
	
	@Autowired
	@Qualifier(value="orderService")
	private OrderService orderService;
	
	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;
	
	@Autowired
	private DataSource ds;
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Increments the reject retry count by 1.
	 * <br/>
	 * 2. Changes the order status to PENDING.
	 * <br/>
	 * 3. Updates the order detail by invoking the new Order Service.
	 * <br/>
	 * 4. Clears the florist by invoking the new Order Service.
	 * <br/>
	 * 5. Sends a FTD message by invoking the legacy Order service.
	 * <br/>
	 * 6. Tracks the result into rules tracking table
	 * <br/>
	 * 7. If there are any exceptions, then roll back the entire transaction
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {

		Mercury associatedMessage;
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		
		if (mercuryMessage == null) {
			LOGGER.error("Mercury mercury message can't be null. Exiting the process");
			throw new RulesActionException("Mercury mercury message can't be null. Exiting the process");			
		}
		
		OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
		
		if (orderDetails == null) {
			LOGGER.error("Order details object is expected and can't be null. Exiting the process");
			throw new RuntimeException("Order details object is expected and can't be null. Exiting the process");
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Send FTD message action processing has STARTED for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Setting order detail id: ").append(orderDetails.getOrderDetailId()).append(" OP_STATUS to PENDING...").toString());	
		}
		
    
    try {
    	
    	associatedMessage = mercuryService.getMercuryByOrderNumber(mercuryMessage.getMercuryOrderNumber(), mercuryMessage.getReferenceNumber());
    	
    	orderDetails.setRejectRetryCount(orderDetails.getRejectRetryCount() + 1);
      orderDetails.setOpStatus(OrderConstants.ORDER_STATUS_PENDING);
      orderDetails.setUpdatedBy(RulesActionConstants.UPDATED_BY_MARS);
    	
  		if (LOGGER.isDebugEnabled()) {
  			LOGGER.debug(new StringBuffer("Updating and clear the florist id order detail id: ").append(orderDetails.getOrderDetailId()).toString());	
  		}
			orderService.updateOrderDetail(orderDetails);
			orderService.clearFlorist(String.valueOf(orderDetails.getOrderDetailId()));	
		} 
    catch (OrderServiceException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
    
    orderDetails.setFloristId(null);
    
    LOGGER.info(new StringBuffer("Sending automated FTD message for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
    
    boolean status = processOrder(orderDetails, associatedMessage);
    if(!status) {
    	throw new RulesActionException("Sending a new FTD message failed. Old order service failed to process the order.");
    }
    
    try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Send FTD Message action in DB");
			}
			MarsVO mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);
			OrderDetails oldOrderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
			this.logTrackingResults(mars, ruleFired, RulesActionConstants.SEND_FTD_MSG_ACTION, RulesActionConstants.ACTION_SUCCESS, oldOrderDetails);
		}
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to persist the tracking results into DB", e);
		}
    
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Send FTD message action processing has ENDED for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
		}
	}
	
	private Connection getConnection() {
		return DataSourceUtils.getConnection(ds);
	}
	
	private boolean processOrder(OrderDetails orderDetails, Mercury associatedMessage) throws RulesActionException {
		try {
			com.ftd.op.order.service.OrderService oldOrderService = new com.ftd.op.order.service.OrderService(getConnection());
			OrderDetailVO orderDetailVO =  RulesActionUtil.getOrderDetailVO(orderDetails, associatedMessage);
			return oldOrderService.processOrder(orderDetailVO, false, false);
		}
		catch(Exception e) {
			LOGGER.error("Exception caught while trying to call the Legacy Order Service. Exiting the process", e);
			throw new RulesActionException(e);
		}
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

}
