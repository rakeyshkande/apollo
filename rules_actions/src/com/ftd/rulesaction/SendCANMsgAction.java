package com.ftd.rulesaction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the Send CAN message action 
 * 
 * @author kdatchan
 *
 */
@Component
public class SendCANMsgAction extends RulesAction {

	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;
	
	private Logger LOGGER = Logger.getLogger(SendCANMsgAction.class);
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Sends a cancel message by invoking the Mercury service.
	 * <br/>
	 * 2. Tracks the result into rules tracking table
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {

		Mercury associatedMessage;
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		
		if (mercuryMessage == null) {
			LOGGER.error("Mercury message can't be null. Exiting the process");
			throw new RulesActionException("Mercury message can't be null. Exiting the process");			
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Send CAN message action processing has STARTED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
		
		try {
			LOGGER.info(new StringBuffer("Sending automated CAN message to the florist for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
			associatedMessage = mercuryService.getMercuryByOrderNumber(mercuryMessage.getMercuryOrderNumber(), mercuryMessage.getReferenceNumber());
			Mercury canMessage = RulesActionUtil.prepareCANMessage(mercuryMessage, associatedMessage);
			mercuryService.sendCANMessage(canMessage);
		} 
		catch (MercuryServiceException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Send CAN Message action in DB");
			}
			MarsVO mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);
			OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
			this.logTrackingResults(mars, ruleFired, RulesActionConstants.SEND_CAN_MSG_ACTION, RulesActionConstants.ACTION_SUCCESS, orderDetails);
		}
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to persist the tracking results into DB", e);
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Send CAN message action processing has ENDED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
		
	}

	public MercuryService getMercuryService() {
		return mercuryService;
	}

	public void setMercuryService(MercuryService mercuryService) {
		this.mercuryService = mercuryService;
	}

}
