package com.ftd.rulesaction;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.MercuryRulesTracker;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * An abstract class that is used to persist the
 * mercury inbound details into the rules tracking table. 
 * 
 * @author kdatchan
 *
 */
@Component
public abstract class RulesAction {

	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;
	
	protected abstract void execute(Map<String, Object> ruleFacts, String ruleFired) throws RulesActionException;

	/**
	 * Suspends the current transaction and opens a new
	 * transaction and persists the rules tracking detail into the DB
	 * 
	 * @param mars
	 * @param ruleFired
	 * @param actionTaken
	 * @param status
	 * @param orderDetails TODO
	 * @throws MercuryServiceException
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	protected void logTrackingResults(MarsVO mars, String ruleFired, String actionTaken, String status, OrderDetails orderDetails) throws MercuryServiceException {
		MercuryRulesTracker mercuryRulesTracker = RulesActionUtil.prepareRulesTracker(mars, ruleFired, actionTaken, status);
		if (orderDetails != null) {
			mercuryRulesTracker.setExternalOrderNumber(orderDetails.getExternalOrderNumber());
			mercuryRulesTracker.setFloristId(orderDetails.getFloristId());
		}
		if(mars.getActionDescription()!=null && !mars.getActionDescription().equals("EMPTYSTRING")){
			mercuryRulesTracker.setActionDescription(mars.getActionDescription());
		}
		mercuryService.saveMercuryRulesTracker(mercuryRulesTracker);
	}

}
