/**
 * 
 */
package com.ftd.rulesaction.util;

import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.naming.InitialContext;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.ftd.constant.MercuryConstants;
import com.ftd.core.domain.AddOn;
import com.ftd.core.domain.Comments;
import com.ftd.core.domain.FloristCodifications;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.MercuryRulesTracker;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.op.mercury.util.ProxyFlorist;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.vo.AddOnVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * An utility class that helps in constructing the objects
 * that are used by the core classes
 * 
 * @author kdatchan
 *
 */
public class RulesActionUtil {
	
	private static final Logger LOGGER = Logger.getLogger(RulesActionUtil.class);
	
	/**
	 * Prepares the mercury message for ANS message type
	 * 
	 * @param mercuryMessage
	 * @param associatedMessage
	 * @return Answer Mercury
	 */
	public static Mercury prepareANSMessage(Mercury mercuryMessage, Mercury associatedMessage) {
		
		Mercury ansMessage = null;
		if (mercuryMessage != null) {
			
			if (associatedMessage == null) {
				LOGGER.error("Associated mercury message can't be null. Exiting the process");
				return null;
			}

      StringBuffer comments = new StringBuffer(MercuryConstants.AUTO_ANSWER_MESSAGE_COMMENT_TEXT)
      														.append(" PRICE WILL CHANGE FROM ")
      														.append(associatedMessage.getPrice())
      														.append(" TO ")
      														.append(mercuryMessage.getPrice())
      														.append(".");

      ansMessage = new Mercury();
			ansMessage.setOperator(getOperatorId());
      ansMessage.setComments(comments.toString());
			ansMessage.setMercuryOrderNumber(mercuryMessage.getMercuryOrderNumber());
			ansMessage.setMercuryStatus(MercuryConstants.MERCURY_OPEN);
			ansMessage.setOldPrice(associatedMessage.getPrice());
			ansMessage.setPrice(mercuryMessage.getPrice());
			ansMessage.setAskAnswerCode(MercuryConstants.ASK_ANSWER_CODE_P);
			ansMessage.setMessageDirection(MercuryConstants.OUTBOUND);
			ansMessage.setFillingFlorist(mercuryMessage.getSendingFlorist());
			ansMessage.setSendingFlorist(associatedMessage.getSendingFlorist());
			setFillingFlorist(ansMessage, mercuryMessage.getSendingFlorist());
		}
		
		return ansMessage;
	}
	
	/**
	 * Prepares the mercury message for CAN message type
	 * 
	 * @param mercuryMessage
	 * @param associatedMessage
	 * @return Cancel Mercury
	 */
	public static Mercury prepareCANMessage(Mercury mercuryMessage, Mercury associatedMessage) {
		
		Mercury canMessage = null;
		if (mercuryMessage != null) {
			
			if (associatedMessage == null) {
				LOGGER.error("Associated mercury message can't be null. Exiting the process");
				return null;
			}

      String comments = MercuryConstants.AUTO_CANCEL_MESSAGE_COMMENT_TEXT;
      canMessage = new Mercury();
      canMessage.setOperator(getOperatorId());
      canMessage.setComments(comments);
      canMessage.setMercuryOrderNumber(mercuryMessage.getMercuryOrderNumber());
      canMessage.setMercuryStatus(MercuryConstants.MERCURY_OPEN);
			canMessage.setMessageDirection(MercuryConstants.OUTBOUND);
			canMessage.setSendingFlorist(associatedMessage.getSendingFlorist());
			setFillingFlorist(canMessage, null);
		}
		
		return canMessage;
	}
	
	/**
	 * Generates a random operator id.
	 * 
	 * @return OperatorId
	 */
	private static String getOperatorId() {
		
		String alphaOperator = RandomStringUtils.randomAlphabetic(RulesActionConstants.OPERATOR_RANDOM_ALPHA);
		String numericOperator = RandomStringUtils.randomNumeric(RulesActionConstants.OPERATOR_RANDOM_NUMERIC);
		
		return (new StringBuffer(RulesActionConstants.OPERATOR_FIRST_CHAR).append(alphaOperator).append(numericOperator).toString().toUpperCase());
	}
	
	/**
	 * Prepares the Order Details
	 * 
	 * @param orderDetails
	 * @param associatedMessage
	 * @return OrderDetailVO
	 */
	public static OrderDetailVO getOrderDetailVO(OrderDetails orderDetails, Mercury mercuryMessage) {
		OrderDetailVO orderDetailVO = new OrderDetailVO();
		
		if (orderDetails == null) {
			LOGGER.error("Order details object is expected and can't be null. Exiting the process");
			return null;
		}
		
		orderDetailVO.setAribaAmsProjectCode(orderDetails.getAribaAmsProjectCode());
		orderDetailVO.setAribaCostCenter(orderDetails.getAribaCostCenter());
		orderDetailVO.setAribaPoNumber(orderDetails.getAribaPoNumber());
		orderDetailVO.setAribaUnspscCode(orderDetails.getAribaUnspscCode());
		orderDetailVO.setAVSAddressId(orderDetails.getAvsAddressId());
		orderDetailVO.setCardMessage(orderDetails.getCardMessage());
		orderDetailVO.setCardSignature(orderDetails.getCardSignature());
		orderDetailVO.setCarrierDelivery(orderDetails.getCarrierDelivery());
		orderDetailVO.setCarrierId(orderDetails.getCarrierId());
		orderDetailVO.setColor1(orderDetails.getColor1());
		orderDetailVO.setColor2(orderDetails.getColor2());
		orderDetailVO.setDeliveryConfirmationStatus(orderDetails.getDeliveryConfirmationStatus());
		orderDetailVO.setDeliveryDate(orderDetails.getDeliveryDate());
		orderDetailVO.setDeliveryDateRangeEnd(orderDetails.getDeliveryDateRangeEnd());
		orderDetailVO.setDerived_vip_flag(orderDetails.getDerivedVipFlag());
		orderDetailVO.setExternalOrderNumber(orderDetails.getExternalOrderNumber());
		orderDetailVO.setFloristId(orderDetails.getFloristId());
		orderDetailVO.setHpOrderNumber(orderDetails.getHpOrderNumber());;
		orderDetailVO.setLpOrderIndicator(orderDetails.getLpOrderIndicator());
		orderDetailVO.setMilesPoints(orderDetails.getMilesPoints());
		orderDetailVO.setMilesPointsRedeemed(orderDetails.getMilesPointsRedeemed());
		orderDetailVO.setMorningDeliveryOrder(orderDetails.getMorningDeliveryOrder() == "Y" ? true : false);
		orderDetailVO.setOccasion(orderDetails.getOccasion());
		orderDetailVO.setOpStatus(orderDetails.getOpStatus());
		orderDetailVO.setOrderDetailId(orderDetails.getOrderDetailId());
		orderDetailVO.setOrderDispCode(orderDetails.getOrderDispCode());
		orderDetailVO.setOrderGuid(orderDetails.getOrderGuid());
		orderDetailVO.setOrigBillBilled(orderDetails.getOrigBillBilled() == "Y" ? true : false);
		orderDetailVO.setPcGroupId(orderDetails.getPcGroupId());
		orderDetailVO.setPersonalGreetingId(orderDetails.getPersonalGreetingId());
		orderDetailVO.setPersonalizationData(orderDetails.getPersonalizationData());
		orderDetailVO.setProductId(orderDetails.getProductId());
		orderDetailVO.setQuantity(orderDetails.getQuantity());
		orderDetailVO.setRecipientId(orderDetails.getRecipientId());
		orderDetailVO.setRejectRetryCount(orderDetails.getRejectRetryCount());
		orderDetailVO.setReleaseInfoIndicator(orderDetails.getReleaseInfoIndicator());
		orderDetailVO.setSameDayGift(orderDetails.getSameDayGift());
		orderDetailVO.setScrubbedBy(orderDetails.getScrubbedBy());
		orderDetailVO.setScrubbedOn(orderDetails.getScrubbedOn());
		orderDetailVO.setSecondChoiceProduct(orderDetails.getSecondChoiceProduct());
		orderDetailVO.setShipDate(orderDetails.getShipDate());
		orderDetailVO.setShipMethod(orderDetails.getShipMethod());
		orderDetailVO.setSizeIndicator(orderDetails.getSizeIndicator());
		orderDetailVO.setSourceCode(orderDetails.getSourceCode());
		orderDetailVO.setSpecialInstructions(orderDetails.getSpecialInstructions());
		orderDetailVO.setSubcode(orderDetails.getSubcode());
		orderDetailVO.setSubstitutionIndicator(orderDetails.getSubstitutionIndicator());
		orderDetailVO.setVendorId(orderDetails.getVendorId());
		orderDetailVO.setVenusMethodOfPayment(orderDetails.getVenusMethodOfPayment());
		orderDetailVO.setZipQueueCount(orderDetails.getZipQueueCount());
		orderDetailVO.setAddOnList(getAddOnVO(orderDetails.getAddOnList()));
		orderDetailVO.setCurrentFtd(getMercuryVOFromMercury(mercuryMessage));
		return orderDetailVO;
	}
	
	/**
	 * Constructs AddonList for the Order Details object
	 * 
	 * @param addOnList
	 * @return
	 */
	private static ArrayList<AddOnVO> getAddOnVO(List<AddOn> addOnList) {
		ArrayList<AddOnVO> addOnVOList = new ArrayList<AddOnVO>();
		AddOnVO addOnVO = null;
		if (addOnList != null && !addOnList.isEmpty()) {
			for (AddOn addOn : addOnList) {
				addOnVO = new AddOnVO();
				addOnVO.setAddOnCode(addOn.getAddOnCode());
				addOnVO.setAddOnId(addOn.getAddOnId());
				addOnVO.setAddOnQuantity(addOn.getAddOnQuantity());
				addOnVO.setCreatedBy(addOn.getCreatedBy());
				addOnVO.setCreatedOn(addOn.getCreatedOn());
				addOnVO.setDesciption(addOn.getDesciption());
				addOnVO.setOrderDetailId(addOn.getOrderDetailId());
				addOnVO.setPrice(addOn.getPrice().doubleValue());
				addOnVO.setUpdatedBy(addOn.getUpdatedBy());
				addOnVO.setUpdatedOn(addOn.getUpdatedOn());
				addOnVOList.add(addOnVO);
			}
		}
		
		return addOnVOList;
	}
	
	/**
	 * Prepares order comments
	 * 
	 * @param orderDetails
	 * @param comments
	 * @return
	 */
	public static Comments prepareOrderComment(OrderDetails orderDetails, Comments comments) {
		
		if(orderDetails == null) {
			LOGGER.error("Order details is expected and can't be null. Exiting the process");
			return null;
		}
		
		Comments orderComments = new Comments();
		
		if(comments != null) {
			orderComments.setComment(comments.getComment());
		}
		
		try {
			orderComments.setCommentOrigin(getCommentOrigin());
		}
		catch (Exception e) {
			LOGGER.error("Exception caught when getting the comment origin for Mars from global parms", e);
			orderComments.setCommentOrigin(RulesActionConstants.COMMENT_ORIGIN_MARS);
		}
		orderComments.setCommentType(RulesActionConstants.COMMENT_TYPE_ORDER);
		orderComments.setCreatedBy(getOperatorId());
		orderComments.setCustomerId(String.valueOf(orderDetails.getRecipientId()));
		orderComments.setOrderDetailId(String.valueOf((orderDetails.getOrderDetailId())));
		orderComments.setOrderGuid(orderDetails.getOrderGuid());
		
		return orderComments;
	}

	/**
	 * Gets the soft florist block days from the global param
	 * 
	 * @return
	 * @throws Exception
	 */
	public static int getSoftBlockFloristDays() throws Exception {
    String daysString = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      daysString = configUtil.getFrpGlobalParm(RulesActionConstants.ORDER_PROCESSING_CONTEXT,
      		RulesActionConstants.FLORIST_SOFT_BLOCK_DAYS);
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the commentOrigin", e);
    	throw new Exception("Exception caught when getting the commentOrigin", e);
    }
    
    if (daysString != null) {
    	return Integer.parseInt(daysString);
    }
    
		return 0;
	}

	public static Date getSoftFloristBlockEndDate(int softBlockDays) {
		Calendar now = Calendar.getInstance();
    
    // get today at 6am
    Calendar blockEndDate = new GregorianCalendar(now.get(Calendar.YEAR),now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH),6,00 );

    // calculate end date. today + soft block days = expiry date 6am
    blockEndDate.add(Calendar.DAY_OF_YEAR, softBlockDays);
    
    return blockEndDate.getTime();
	}
	
	/**
	 * An utility method to send JMS message into queue
	 * 
	 * @param context
	 * @param messageToken
	 * @param queue
	 * @throws Exception
	 */
  public static void sendJMSMessage(InitialContext context, MessageToken messageToken, String queue)
      throws Exception {

    // make the correlation id the same as the message
    messageToken.setJMSCorrelationID((String) messageToken.getMessage());

    Dispatcher dispatcher = Dispatcher.getInstance();
    messageToken.setStatus(queue);
    dispatcher.dispatchTextMessage(context, messageToken);
  }
  
  /**
   * Gets the order comment origin from the global params
   * 
   * @return
   * @throws Exception
   */
	public static String getCommentOrigin() throws Exception {
    String commentOrigin = null;

    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      commentOrigin = configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT,
      		RulesActionConstants.COMMENT_ORIGIN);
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the commentOrigin", e);
    	throw new Exception("Exception caught when getting the commentOrigin", e);
    }
    
		return commentOrigin;
	}
	
	private static void setFillingFlorist(Mercury mercuryMessage, String floristId) {
    ProxyFlorist proxy = ProxyFlorist.getInstance();

    try {
			if (!proxy.isProxyFlorist()) {
				mercuryMessage.setFillingFlorist(floristId);
			} 
			else {
			  mercuryMessage.setComments(breakString(new StringBuffer(mercuryMessage.getComments()).append("  Florist ").append(floristId).append(" replaced by proxy").toString(), 54));
				mercuryMessage.setFillingFlorist(proxy.getRandomProxyFlorist());
			}
		} 
    catch (Exception e) {
    	LOGGER.error("Exception caught while trying to set proxy florist. Suppressing this exception in order to continue the process");
		}
	}
	
	private static String breakString(String inString, int lineLength) {
		
		if (inString == null) {
			return null;
		}
		inString = inString.replaceAll("\n", " ");
		String outputString = new String();
		if (inString.length() == 0)
			return inString;

		// append a blank space so the substring method can always find a space at
		// the end
		String remainingString = inString;

		try {
			while (lineLength < remainingString.length()) {
				outputString += remainingString.substring(0,
						remainingString.indexOf(" ", lineLength))
						+ "\n";
				remainingString = remainingString.substring(
						remainingString.indexOf(" ", lineLength) + 1,
						remainingString.length()).trim();
			}
		} catch (Exception e) {
			// fall out of loop on exception
		}
		return outputString + remainingString;
	}
	
	/**
	 * Prepares the mercury rules tracker object using the 
	 * dependent objects
	 * 
	 * @param mars
	 * @param ruleFired
	 * @param actionTaken
	 * @param status
	 * @return
	 */
	public static MercuryRulesTracker prepareRulesTracker(MarsVO mars, String ruleFired, String actionTaken, String status) {
		MercuryRulesTracker rulesTracker = null;
		
		if (mars != null) {
			rulesTracker = new MercuryRulesTracker();
			rulesTracker.setAskpThreshold(mars.getAskpThreshold());
			rulesTracker.setAutoRespBeforeCutoff(mars.getAutoResponseCutoffStop());
			rulesTracker.setActionStatus(RulesActionConstants.ACTION_FAILED);
			if (ruleFired != null) {
				rulesTracker.setActionStatus(status);	
			}
			rulesTracker.setActionTaken(actionTaken);
			rulesTracker.setCustomerAddress(mars.getCustomerAddress());
			rulesTracker.setCustomerCity(mars.getCustomerCity());
			rulesTracker.setCustomerCountry(mars.getCustomerCountry());
			rulesTracker.setCustomerFirstName(mars.getCustomerFirstName());
			rulesTracker.setCustomerLastName(mars.getCustomerLastName());
			rulesTracker.setCustomerPhoneNumber(mars.getCustomerPhoneNumber());
			rulesTracker.setCustomerState(mars.getCustomerState());
			rulesTracker.setCustomerZipCode(mars.getCustomerZipCode());
			rulesTracker.setCreatedBy(RulesActionConstants.SYS);
			rulesTracker.setDeliveryCity(mars.getDeliveryCity());
			rulesTracker.setDeliveryState(mars.getDeliveryState());
			rulesTracker.setDeliveryDate(mars.getDeliveryDate());
			rulesTracker.setDeliveryZipCode(mars.getDeliveryZipCode());
			rulesTracker.setFloristBlockDays(mars.getFloristSoftBlockDays());
			rulesTracker.setGlobalRejectRetryLimit(mars.getGlobalRejectRetryLimit());
			rulesTracker.setMarsOrderBounceCount(mars.getMarsOrderBounceCount());
			rulesTracker.setMercuryId(mars.getMercuryId());
			rulesTracker.setMercuryNumber(mars.getMercuryNumber());
			rulesTracker.setMessageDirection(mars.getDirection());
			rulesTracker.setMessageText(mars.getMessageText());
			rulesTracker.setMessageType(mars.getMessageType());
			rulesTracker.setNewMercuryPrice(mars.getNewMercuryPrice());
			rulesTracker.setOccasion(mars.getOccasion());
			rulesTracker.setOrderBounceCount(mars.getOrderBounceCount());
			rulesTracker.setOrderDate(mars.getOrderDate());
			rulesTracker.setOrderDetailId(mars.getOrderDetailId());
			rulesTracker.setOrderOrigin(mars.getOrderOrigin());
			rulesTracker.setOriginalMercuryPrice(mars.getOriginalMercuryPrice());
			rulesTracker.setPhoenixEligibleFlag(mars.getPhoenixEligible());
			rulesTracker.setPreferredPartnerId(mars.getPreferredPartnerId());
			rulesTracker.setProductId(mars.getProductId());
			rulesTracker.setRecipientAddress(mars.getRecipientAddress());
			rulesTracker.setRecipientCity(mars.getRecipientCity());
			rulesTracker.setRecipientCountry(mars.getRecipientCountry());
			rulesTracker.setRecipientFirstName(mars.getRecipientFirstName());
			rulesTracker.setRecipientLastName(mars.getRecipientLastName());
			rulesTracker.setRecipientPhoneNumber(mars.getRecipientPhoneNumber());
			rulesTracker.setRecipientState(mars.getRecipientState());
			rulesTracker.setRecipientZipCode(mars.getRecipientZipCode());
			rulesTracker.setRulesFired(ruleFired);
			rulesTracker.setRuleType(RulesActionConstants.MERCURY_INBOUND);
			rulesTracker.setSakText(mars.getSakText());
			rulesTracker.setSourceCode(mars.getSourceCode());
			rulesTracker.setTimeZone(mars.getTimeZone());
			rulesTracker.setUpdatedBy(RulesActionConstants.SYS);
			rulesTracker.setCutOff(mars.getCutOffTime());
			rulesTracker.setCodificationId(mars.getCodifiedId());
			rulesTracker.setCodifiedMinimum(mars.getCodifiedMinimum());
		}
		return rulesTracker;
	}
	
	private static MercuryVO getMercuryVOFromMercury(Mercury mercuryMessage) {
		MercuryVO mercuryVO = null;
		
		if (mercuryMessage != null) {
			mercuryVO = new MercuryVO();
			mercuryVO.setAddress(mercuryMessage.getAddress());
			mercuryVO.setAdjReasonCode(mercuryMessage.getAdjReasonCode());
			mercuryVO.setAdminSequence(mercuryMessage.getAdminSeq());
			mercuryVO.setApproval_identity_id(mercuryMessage.getApprovalIdentityId());
			mercuryVO.setAskAnswerCode(mercuryMessage.getAskAnswerCode());
			mercuryVO.setCardMessage(mercuryMessage.getCardMessage());
			mercuryVO.setCityStateZip(mercuryMessage.getCityStateZip());
			mercuryVO.setCombinedReportNumber(mercuryMessage.getCombinedReportNumber());
			mercuryVO.setComments(mercuryMessage.getComments());
			mercuryVO.setComplaintCommNotificationTypeId(mercuryMessage.getComplaintCommNotificationTypeId());
			mercuryVO.setComplaintCommOriginTypeId(mercuryMessage.getComplaintCommOriginTypeId());
			mercuryVO.setCompOrder(mercuryMessage.getCompOrder());
			mercuryVO.setCrseq(mercuryMessage.getCrseq());
			mercuryVO.setCtseq(mercuryMessage.getCtseq());
			mercuryVO.setDeliveryDate(mercuryMessage.getDeliveryDate());
			mercuryVO.setDeliveryDateText(mercuryMessage.getDeliveryDateText());
			mercuryVO.setDirection(mercuryMessage.getMessageDirection());
			mercuryVO.setFillingFlorist(mercuryMessage.getFillingFlorist());
			mercuryVO.setFirstChoice(mercuryMessage.getFirstChoice());
//			mercuryVO.setFromMessageDate(mercuryMessage.getFromMessageDate());
			mercuryVO.setFromMessageNumber(mercuryMessage.getFromMessageNumber());
			mercuryVO.setMercuryId(mercuryMessage.getMercuryId());
			mercuryVO.setMercuryMessageNumber(mercuryMessage.getFromMessageNumber());
			mercuryVO.setMercuryOrderNumber(mercuryMessage.getMercuryOrderNumber());
			mercuryVO.setMercuryStatus(mercuryMessage.getMercuryStatus());
			mercuryVO.setMessageType(mercuryMessage.getMessageType());
			mercuryVO.setOccasion(mercuryMessage.getOccasion());
			if (mercuryMessage.getOldPrice() != null) {
				mercuryVO.setOldPrice(mercuryMessage.getOldPrice().doubleValue());	
			}
			mercuryVO.setOperator(mercuryMessage.getOperator());
			mercuryVO.setOrderDate(mercuryMessage.getOrderDate());
			mercuryVO.setOrderSequence(mercuryMessage.getOrderSeq());
			mercuryVO.setOutboundId(mercuryMessage.getOutboundId());
			if (mercuryMessage.getOverUnderCharge() != null) {
				mercuryVO.setOverUnderCharge(mercuryMessage.getOverUnderCharge().doubleValue());	
			}
			mercuryVO.setPhoneNumber(mercuryMessage.getPhoneNumber());
			mercuryVO.setPriority(mercuryMessage.getPriority());
			if (mercuryMessage.getPrice() != null) {
				mercuryVO.setPrice(mercuryMessage.getPrice().doubleValue());	
			}
			mercuryVO.setProductId(mercuryMessage.getProductId());
			mercuryVO.setRecipient(mercuryMessage.getRecipient());
			mercuryVO.setReferenceNumber(mercuryMessage.getReferenceNumber());
			mercuryVO.setRequireConfirmation(mercuryMessage.getRequireConfirmation());
			mercuryVO.setRetrievalFlag(mercuryMessage.getRetrievalFlag());
			mercuryVO.setRofNumber(mercuryMessage.getRofNumber());
			mercuryVO.setSakText(mercuryMessage.getSakText());
			mercuryVO.setSecondChoice(mercuryMessage.getSecondChoice());
			mercuryVO.setSendingFlorist(mercuryMessage.getSendingFlorist());
			mercuryVO.setSortValue(mercuryMessage.getSortValue());
			mercuryVO.setSpecialInstructions(mercuryMessage.getSpecialInstructions());
			mercuryVO.setSuffix(mercuryMessage.getSuffix());
//			mercuryVO.setToMessageDate(mercuryMessage.getToMessageDate());
			mercuryVO.setToMessageNumber(mercuryMessage.getToMessageNumber());
			mercuryVO.setTransmissionDate(mercuryMessage.getTransmissionDate());
			mercuryVO.setViewQueue(mercuryMessage.getViewQueue());
			mercuryVO.setZipCode(mercuryMessage.getZipCode());
		}
		return mercuryVO;
	}
	
	public static Date getSoftBlockDefaultEndDate() throws Exception {
    Date endDate = null;
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      endDate = df.parse(configUtil.getFrpGlobalParm(RulesActionConstants.LMD_CONTEXT, RulesActionConstants.VIEW_QUEUE_DEFAULT_END_DATE));
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the View Queue Soft Block End Date from the global control", e);
    	throw new RulesActionException("Unable to get the View Queue Soft Block End Date from the global control. Param Name: " + RulesActionConstants.VIEW_QUEUE_DEFAULT_END_DATE + ". Please update with a valid value.");
    }
    
		return endDate;
	}
	
	public static Date getZipBlockEndDate() throws Exception {
    Date endDate = null;
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      endDate = df.parse(configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT, RulesActionConstants.VIEW_QUEUE_END_DATE_ZIPCODE));
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the View Queue Block Zipcode End Date from the global control", e);
    	throw new RulesActionException("Unable to get the View Queue Block Zipcode End Date from the global control. Param Name: " + RulesActionConstants.VIEW_QUEUE_END_DATE_ZIPCODE + ". Please update with a valid value.");
    }
    
		return endDate;
	}
	
	public static Date getCityBlockEndDate() throws Exception {
	    Date endDate = null;
	    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    ConfigurationUtil configUtil;
	    try {
	      configUtil = ConfigurationUtil.getInstance();
	      endDate = df.parse(configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT, RulesActionConstants.VIEW_QUEUE_END_DATE_CITY));
	    }
	    catch (Exception e) {
	    	LOGGER.error("Exception caught when getting the View Queue Block City End Date from the global control", e);
	    	throw new RulesActionException("Unable to get the View Queue Block City End Date from the global control. Param Name: " + RulesActionConstants.VIEW_QUEUE_END_DATE_CITY + ". Please update with a valid value.");
	    }
	    
			return endDate;
		}
	
	public static Date getCodificationBlockEndDate() throws Exception {
    Date endDate = null;
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    ConfigurationUtil configUtil;
    try {
      configUtil = ConfigurationUtil.getInstance();
      endDate = df.parse(configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT, RulesActionConstants.VIEW_QUEUE_END_DATE_PRODUCT));
    }
    catch (Exception e) {
    	LOGGER.error("Exception caught when getting the View Queue Block Codification End Date from the global control", e);
    	throw new RulesActionException("Unable to get the View Queue Block Codification End Date from the global control. Param Name: " + RulesActionConstants.VIEW_QUEUE_END_DATE_PRODUCT + ". Please update with a valid value.");
    }
    
		return endDate;
	}
	
	public static String getViewQueueZipCutoff() throws Exception 
	{
		String viewQueueZipCutoff = null;
		ConfigurationUtil configUtil;
		
		try 
		{
			configUtil = ConfigurationUtil.getInstance();
			viewQueueZipCutoff = configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT, RulesActionConstants.VIEW_QUEUE_ZIP_CUTOFF);
			// Simple way to validate whether the cutoff is a number value or not
			Long.valueOf(viewQueueZipCutoff);
			if (viewQueueZipCutoff == null) {
				throw new Exception();
			}
		}
		catch (Exception e) 
		{
			LOGGER.error("Exception caught when getting the View Queue Block Zip Cutoff from the global control", e);
			throw new RulesActionException("Unable to get the View Queue Block Zip Cutoff from the global control. Param Name: " + RulesActionConstants.VIEW_QUEUE_ZIP_CUTOFF + ". Please update with a valid value.");
		}
    
		return viewQueueZipCutoff;
	}

	public static List<String> getCodificationGroup(String codificationGroupString) {

		if (codificationGroupString != null) {
			String[] codificationArr = codificationGroupString.split("/");
			return (codificationArr != null) ? Arrays.asList(codificationArr) : null;
		}
		 
		return null;
	}
	
	public static List<String> getCodificationList(List<FloristCodifications> floristCodifications) {
		
		List<String> codificationList = null;
		if (floristCodifications != null) {
			codificationList = new ArrayList<String>();
			for (FloristCodifications floristCodification : floristCodifications) {
				codificationList.add(floristCodification.getCodificationId());
			}
		}
		return codificationList;
	}

	public static List<String> getCommonCodification(
			List<String> codificationGroup, List<String> codificationList) {
		List<String> commonCodification = null;
		
		if (codificationList != null && !codificationList.isEmpty() && codificationGroup != null && !codificationGroup.isEmpty()) {
			commonCodification = new ArrayList<String>();
			for (String codificationId : codificationGroup) {
				if (codificationList.contains(codificationId)) {
					commonCodification.add(codificationId);
				}
			}
		}
		return commonCodification;
	}
	
	public static boolean isDateSame(Date date1, Date date2) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		if (df.format(date1).equalsIgnoreCase(df.format(date2))) {
  		LOGGER.info("Block end date is already present in the florist zips table. So skipping the update");
  		return true;
  	}
		return false;
	}
	
	public static boolean isEndDateLTStartDate(Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if (cal.getTime().after(endDate)) {
  		LOGGER.info("End date is before the Start date.");
  		return true;
  	}
		return false;
	}
	
	public static void sendSystemMessageWrapper(String message, String messageSource, String type) throws RulesActionException {

		LOGGER.error(message);
		Connection conn = null;

		try {
			String messageId = "";
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(messageSource);
			sysMessage.setType(type);
			sysMessage.setSubject("MARS");
			sysMessage.setMessage(message);
			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			conn = getConnection();

			messageId = sysMessenger.send(sysMessage, conn);

			if (messageId == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
				LOGGER.error(msg);
			}
		} 
		catch (Exception e) {
			LOGGER.error("Could not send system message: " + e.getMessage(), e);
		} 
		finally {
			try {
				if (conn != null) {
					conn.close();	
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}

	}
	
	/**
	 * Get the DB connection based on the datasource value
	 * 
	 * @return Connection
	 * @throws MarsException
	 */
  public static Connection getConnection() throws RulesActionException {

    ConfigurationUtil config;
		try {
			config = ConfigurationUtil.getInstance();
	    String dbConnection = config.getProperty("mars_config.xml","MARS_DS");
	    return DataSourceUtil.getInstance().getConnection(dbConnection);
		} 
		catch (IOException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new RulesActionException(e);
		} 
		catch (SAXException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new RulesActionException(e);
		} 
		catch (ParserConfigurationException e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new RulesActionException(e);
		} catch (Exception e) {
			LOGGER.error("Exception caught while trying to get the connection. Exception message: " + e.getMessage(), e);
			throw new RulesActionException(e);
		}
  }
}
