package com.ftd.rulesaction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.constant.FloristConstants;
import com.ftd.core.domain.Florist;
import com.ftd.core.domain.FloristZip;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.exception.FloristServiceException;
import com.ftd.floristservice.service.FloristService;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the Update Florist Zip Cutoff Action
 * 
 * @author vsunil
 * 
 */
@Component
public class UpdateFloristZipCutoffAction extends RulesAction {

	@Autowired
	@Qualifier(value = "floristService")
	private FloristService floristService;

	@Autowired
	@Qualifier(value = "mercuryService")
	private MercuryService mercuryService;

	private static final Logger LOGGER = Logger
			.getLogger(UpdateFloristZipCutoffAction.class);

	/**
	 * Participates if there is already an ongoing transaction, else creates a new
	 * transaction and does the processing. <br/>
	 * 1. Updates florist zip cutoff-time by invoking the Florist service. <br/>
	 * 2. Tracks the result into rules tracking table
	 * 
	 */
	@Transactional(rollbackFor = RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired)
			throws RulesActionException {
		String floristId = null;
		String zipCode = null;
		String vqZipCutoffTime = null;
		String comments = null;
		Florist florist = null;
		String floristLockedStatus = "N";
		String csrLockedStatus = "N";

		if (map == null) {
			LOGGER
					.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException(
					"Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}

		Mercury mercuryMessage = (Mercury) map
				.get(RulesActionConstants.MERCURY_OBJ);

		if (mercuryMessage == null) {
			LOGGER
					.error("Mercury message object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"Mercury message object is expected and can't be null. Exiting the process");
		}

		OrderDetails orderDetails = (OrderDetails) map
				.get(RulesActionConstants.ORDER_DETAILS_OBJ);

		if (orderDetails == null) {
			LOGGER
					.error("OrderDetails object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"OrderDetails object is expected and can't be null. Exiting the process");
		}

		Mercury associatedMessage = (Mercury) map
				.get(RulesActionConstants.ASSOCIATED_MERCURY_OBJ);

		if (associatedMessage == null) {
			LOGGER
					.error("AssociatedMessage object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"AssociatedMessage object is expected and can't be null. Exiting the process");
		}

		MarsVO marsVO = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);

		if (marsVO == null) {
			LOGGER
					.error("MarsVO object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"MarsVO object is expected and can't be null. Exiting the process");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER
					.debug(new StringBuffer(
							"Update Florist Zip Cutoff action processing has STARTED for mercury id: ")
							.append(mercuryMessage.getMercuryId()).toString());
		}

		florist = new Florist();
		florist.setFloristId(associatedMessage.getFillingFlorist());
		florist.setBlockedByUserId(FloristConstants.FLORIST_BLOCK_USER_ID);

		try {
			floristId = associatedMessage.getFillingFlorist();
			zipCode = associatedMessage.getZipCode();
			
      try {
      	vqZipCutoffTime = RulesActionUtil.getViewQueueZipCutoff();	
      }
      catch (RulesActionException rae){
      	try {
      		RulesActionUtil.sendSystemMessageWrapper(rae.getMessage(), RulesActionConstants.NOPAGE, "ERROR");	
      	}
      	catch (Exception e) {
      		LOGGER.error(e);
      	}
      }
      
      if (vqZipCutoffTime == null) {
      	throw new RulesActionException();
      }

			LOGGER
					.info(new StringBuffer("Fetching florist zip info for florist id: ")
							.append(floristId).append(" zipcode: ").append(zipCode)
							.toString());
			FloristZip floristZip = floristService.getFloristZip(floristId, zipCode);
			
			if(floristZip == null)
    		{
    			LOGGER.error(
    						new StringBuffer("FloristZip object is null. Florist: ")
    							.append(floristId)
    							.append(" might not be associated to zipcode: ")
    							.append(zipCode)
    							.append(". Exiting the process.").toString()
    						);
    			
    			throw new RulesActionException(
						    					new StringBuffer("floristZip object is null. Florist: ")
												.append(floristId)
												.append(" might not be associated to zipcode: ")
												.append(zipCode)
												.append(". Exiting the process.").toString()
												);
    		}
			
			LOGGER.info("Insert the data into CSR viewed id table ");
			csrLockedStatus = mercuryService.insertCSRView(
					FloristConstants.FLORIST_BLOCK_USER_ID_MARS,
					mercuryMessage.getMercuryId(),
					RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);
			
			if (vqZipCutoffTime != null && vqZipCutoffTime.equalsIgnoreCase(floristZip.getCutoffTime())) {
				LOGGER.info("Cutoff is already present in the florist zips table. So skipping the update");	
			}

			if (vqZipCutoffTime != null && !vqZipCutoffTime.isEmpty()
					&& !(vqZipCutoffTime.equalsIgnoreCase(floristZip.getCutoffTime()))) {
				LOGGER.info(new StringBuffer("Locking florist id: ").append(florist
						.getFloristId()));
				floristService.lockFlorist(florist, FloristConstants.Y_STRING);

				if (florist.getLockedFlag() != null
						&& FloristConstants.Y_STRING.equalsIgnoreCase(florist
								.getLockedFlag())) {
					floristLockedStatus = florist.getLockedFlag();
					comments = new StringBuffer("Cutoff was changed from ")
							.append(floristZip.getCutoffTime()).append(" to ")
							.append(vqZipCutoffTime).append("for Postal Code ")
							.append(floristZip.getZipCode())
							.append(" by MARS auto View Queue handling (")
							.append(orderDetails.getOrderDetailId())
							.append("). Text matched: ").append(marsVO.getFloristComments())
							.append(".").toString();

					floristZip.setCutoffTime(vqZipCutoffTime);

					LOGGER.info(new StringBuffer(
							"Updating florist zip cut off for florist id: ")
							.append(floristId).toString());
					floristService.updateFloristCutoffZip(floristZip,
							RulesActionConstants.UPDATED_BY_MARS, comments);
				}
				else {
					LOGGER.info("Florist is locked by another user. User Id: " + florist.getLockedByUser() + " So MARS won't process the View Queue actions");
					throw new RulesActionException();
				}
			}
		} catch (FloristServiceException e) {
			LOGGER
					.error(
							"Exception caught while trying to execute update florist zip cutoff action",
							e);
			throw new RulesActionException(
					"Exception caught while trying to execute update florist zip cutoff action. Exception message: "
							+ e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		} finally {

			if (florist != null
					&& FloristConstants.Y_STRING.equalsIgnoreCase(floristLockedStatus)) {
				LOGGER.info(new StringBuffer("Unlocking florist id: ").append(florist
						.getFloristId()));
				try {
					floristService.unlockFlorist(florist, FloristConstants.N_STRING);
				} catch (Exception e) {
					LOGGER.error("Exception caught while unlocking the florist Id", e);
					StringBuffer sb = new StringBuffer(
							"Unable to unlock the florist id: ");
					sb.append(florist.getFloristId()).append(". Error Message: ")
							.append(e.getMessage());
					RulesActionUtil.sendSystemMessageWrapper(sb.toString(), "MARS_PAGE",
							"ERROR");
				}
			}

			if (FloristConstants.Y_STRING.equalsIgnoreCase(csrLockedStatus)) {
				LOGGER.info("Delete the data from CSR viewed id table ");
				try {
					mercuryService.deleteCSRView(
							FloristConstants.FLORIST_BLOCK_USER_ID_MARS,
							mercuryMessage.getMercuryId(),
							RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);
				} catch (Exception e) {
					LOGGER
							.error(
									"Exception caught while removing the CSR ID from CSR viewed id table",
									e);
					StringBuffer sb = new StringBuffer(
							"Unable to delete CSR Id/Entity Id/Entity Type: ");
					sb.append(FloristConstants.FLORIST_BLOCK_USER_ID_MARS).append("/")
							.append(mercuryMessage.getMercuryId()).append("/")
							.append(RulesActionConstants.CSR_ENTITY_TYPE_MERCURY)
							.append(". Error Message: ").append(e.getMessage());
					RulesActionUtil.sendSystemMessageWrapper(sb.toString(), "MARS_PAGE",
							"ERROR");
				}
			}
		}

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Update Florist Zip Cutoff action in DB");
			}
			MarsVO mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);

			this.logTrackingResults(mars, ruleFired,
					RulesActionConstants.UPDATE_FLORIST_ZIP_CUTOFF_ACTION,
					RulesActionConstants.ACTION_SUCCESS, orderDetails);
		} catch (Exception e) {
			LOGGER
					.error(
							"Exception caught while trying to persist the tracking results into DB",
							e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER
					.debug(new StringBuffer(
							"Update Florist Zip Cutoff action processing has ENDED for mercury id: ")
							.append(mercuryMessage.getMercuryId()).toString());
		}

	}

	public FloristService getFloristService() {
		return floristService;
	}

	public void setFloristService(FloristService floristService) {
		this.floristService = floristService;
	}
}
