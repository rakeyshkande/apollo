package com.ftd.rulesaction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.Comments;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * A spring bean to process the Approve ASKP action code
 * 
 * @author kdatchan
 *
 */
@Component
public class ApproveASKPAction extends RulesAction {

	@Autowired
	private SendANSMsgAction sendAnsAction;
	
	@Autowired
	private UpdateOrderCommentsAction updateOrderComments;
	
	private Logger LOGGER = Logger.getLogger(ApproveASKPAction.class);
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Sends an Answer message by invoking the appropriate action handler class.
	 * <br/>
	 * 2. Updates order comments by invoking the appropriate action handler class.
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}

		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
		Comments comments = (Comments) map.get(RulesActionConstants.COMMENTS_OBJ);
		
		if(mercuryMessage == null && orderDetails == null && comments == null) {
			LOGGER.error("Mercury message, Order Details and Order comments are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message, Order Details and Order comments are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Approve ASKP action processing has STARTED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
		
		LOGGER.info(new StringBuffer("Sending ANS message for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		sendAnsAction.execute(map, ruleFired);
		
		LOGGER.info(new StringBuffer("Updating order comments for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		updateOrderComments.execute(map, ruleFired);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Approve ASKP action processing has ENDED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}

	}

	public SendANSMsgAction getSendAnsAction() {
		return sendAnsAction;
	}

	public void setSendAnsAction(SendANSMsgAction sendAnsAction) {
		this.sendAnsAction = sendAnsAction;
	}

	public UpdateOrderCommentsAction getUpdateOrderComments() {
		return updateOrderComments;
	}

	public void setUpdateOrderComments(UpdateOrderCommentsAction updateOrderComments) {
		this.updateOrderComments = updateOrderComments;
	}

}	

