/**
 * 
 */
package com.ftd.rulesaction;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * A spring bean that processes all the action codes sent the caller.
 * 
 * @author kdatchan
 *
 */
@Component
public class MercuryRulesActionHandler {

	@Autowired
	private ApproveASKPAction approveAskPAction;

	@Autowired
	private RejectASKPAction rejectAskPAction;

	@Autowired
	private SendFTDMsgAction sendFtdAction;

	@Autowired
	private SendCANMsgAction sendCanAction;

	@Autowired
	private SendANSMsgAction sendAnsAction;

	@Autowired
	private UpdateOrderCommentsAction updateOrderComments;

	@Autowired
	private ExistingLogicAction existingLogic;

	@Autowired
	private QueueMsgAction queueMsg;

	@Autowired
	private BlockFloristCityAction blockFloristCityAction;

	private static final Logger LOGGER = Logger
			.getLogger(MercuryRulesActionHandler.class);

	/**
	 * Suspends the current transaction and works on a new transaction to
	 * process the action codes. Iterates the action codes and process them
	 * sequentially
	 * 
	 * @param ruleFacts
	 * @param ruleFiredMap
	 * @param actionMetaData
	 * @throws RulesActionException
	 */
	@Transactional(rollbackFor = RulesActionException.class, propagation = Propagation.REQUIRES_NEW)
	public void handleActions(Map<String, Object> ruleFacts,
			Map<String, List<String>> ruleFiredMap,
			Map<String, Object> actionMetaData) throws RulesActionException {

		/*
		 * If there are no action codes then put the messages back to the
		 * existing logic
		 */
		if (ruleFiredMap == null || ruleFiredMap.isEmpty()) {
			existingLogic.execute(ruleFacts, null);
			return;
		}

		Set<String> rulesFired = ruleFiredMap.keySet();
		boolean actionCodeExecuted = false;

		for (String ruleFired : rulesFired) {
			List<String> actionCodes = (List<String>) ruleFiredMap
					.get(ruleFired);

			List<String> actionDescriptions = (List<String>) ruleFiredMap
					.get(ruleFired + "ACTION_DESCRIPTION");
			MarsVO marsVO = (MarsVO) ruleFacts
					.get(RulesActionConstants.MARS_OBJ);
			if (actionDescriptions != null && !actionDescriptions.isEmpty()) {
				marsVO.setActionDescription(actionDescriptions.get(0));
			} else {
				marsVO.setActionDescription(null);
			}

			if (actionCodes != null
					&& !ruleFired.contains("ACTION_DESCRIPTION")) {
				for (String actionCode : actionCodes) {

					LOGGER.info("The actionCode is : " + actionCode);

					if (RulesActionConstants.APPROVE_ASKP_ACTION
							.equalsIgnoreCase(actionCode)) {
						approveAskPAction.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.REJECT_ASKP_ACTION
							.equalsIgnoreCase(actionCode)) {
						rejectAskPAction.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.SEND_ANS_MSG_ACTION
							.equalsIgnoreCase(actionCode)) {
						sendAnsAction.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.SEND_FTD_MSG_ACTION
							.equalsIgnoreCase(actionCode)) {
						sendFtdAction.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.SEND_CAN_MSG_ACTION
							.equalsIgnoreCase(actionCode)) {
						sendCanAction.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.UPDATE_ORDER_COMMENTS_ACTION
							.equalsIgnoreCase(actionCode)) {
						updateOrderComments.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					}

					else if (RulesActionConstants.QUEUE_MSG_ACTION
							.equalsIgnoreCase(actionCode)) {
						queueMsg.execute(ruleFacts, ruleFired);
						actionCodeExecuted = true;
					} else if (RulesActionConstants.BLOCK_FLORIST_CITY_PERMANENT
							.equalsIgnoreCase(actionCode)) {
						blockFloristCityAction.executeRules(ruleFacts,
								ruleFired, false);
						actionCodeExecuted = true;
					}

					else {
						if (!(RulesActionConstants.BLOCK_FLORIST_CODIFICATION
								.equalsIgnoreCase(actionCode)
								|| RulesActionConstants.BLOCK_FLORIST_CODIFICATION_GROUP
										.equalsIgnoreCase(actionCode)
								|| RulesActionConstants.SOFT_BLOCK_FLORIST_ACTION
										.equalsIgnoreCase(actionCode)
								|| RulesActionConstants.REMOVE_FROM_VIEW_QUEUE
										.equalsIgnoreCase(actionCode)
								|| RulesActionConstants.BLOCK_FLORIST_CITY_TEMPORARY
										.equalsIgnoreCase(actionCode)
								|| RulesActionConstants.BLOCK_FLORIST_ZIP
										.equalsIgnoreCase(actionCode) || RulesActionConstants.UPDATE_FLORIST_ZIP_CUTOFF_ACTION
									.equalsIgnoreCase(actionCode))) {
							existingLogic.execute(ruleFacts, ruleFired);
							actionCodeExecuted = true;
						}
					}
				}
			}
		}

		if (!actionCodeExecuted) {
			existingLogic.execute(ruleFacts, null);
		}

	}

	public ApproveASKPAction getApproveAskPAction() {
		return approveAskPAction;
	}

	public void setApproveAskPAction(ApproveASKPAction approveAskPAction) {
		this.approveAskPAction = approveAskPAction;
	}

	public RejectASKPAction getRejectAskPAction() {
		return rejectAskPAction;
	}

	public void setRejectAskPAction(RejectASKPAction rejectAskPAction) {
		this.rejectAskPAction = rejectAskPAction;
	}

	public SendFTDMsgAction getSendFtdAction() {
		return sendFtdAction;
	}

	public void setSendFtdAction(SendFTDMsgAction sendFtdAction) {
		this.sendFtdAction = sendFtdAction;
	}

	public SendCANMsgAction getSendCanAction() {
		return sendCanAction;
	}

	public void setSendCanAction(SendCANMsgAction sendCanAction) {
		this.sendCanAction = sendCanAction;
	}

	public SendANSMsgAction getSendAnsAction() {
		return sendAnsAction;
	}

	public void setSendAnsAction(SendANSMsgAction sendAnsAction) {
		this.sendAnsAction = sendAnsAction;
	}

	public UpdateOrderCommentsAction getUpdateOrderComments() {
		return updateOrderComments;
	}

	public void setUpdateOrderComments(
			UpdateOrderCommentsAction updateOrderComments) {
		this.updateOrderComments = updateOrderComments;
	}

	public ExistingLogicAction getExistingLogic() {
		return existingLogic;
	}

	public void setExistingLogic(ExistingLogicAction existingLogic) {
		this.existingLogic = existingLogic;
	}

	public QueueMsgAction getQueueMsg() {
		return queueMsg;
	}

	public void setQueueMsg(QueueMsgAction queueMsg) {
		this.queueMsg = queueMsg;
	}

}
