package com.ftd.rulesaction;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.constant.FloristConstants;
import com.ftd.core.domain.Florist;
import com.ftd.core.domain.FloristBlocks;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.exception.FloristServiceException;
import com.ftd.floristservice.service.FloristService;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the Soft Florist Block action 
 * 
 * @author kdatchan
 *
 */
@Component
public class SoftBlockFloristAction extends RulesAction {

	@Autowired
	@Qualifier(value="mercuryService")
	private MercuryService mercuryService;
	
	@Autowired
	@Qualifier(value="floristService")
	private FloristService floristService;
	

	
	private static final Logger LOGGER = Logger.getLogger(SoftBlockFloristAction.class);
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Soft blocks the florist by invoking the Florist service.
	 * <br/>
	 * 2. Tracks the result into rules tracking table
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {

		Mercury associatedMessage;
		MarsVO mars = null;
		Florist florist = null;
		String floristLockedStatus = "N";
		String csrLockedStatus = "N";
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Set<String> newBlockDateStringSet = null;
		Set<String> traversedBlockedDateSet = null;
		Boolean isGoToFlorist = false;
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		
		if (mercuryMessage == null) {
			LOGGER.error("Mercury message object is expected and can't be null. Exiting the process");
			throw new RulesActionException("Mercury message object is expected and can't be null. Exiting the process");
		}
		
		String sendingFlorist = mercuryMessage.getSendingFlorist();
		String mercuryOrderNumber = mercuryMessage.getMercuryOrderNumber();
		
		try {
			isGoToFlorist = floristService.getGoToFLoristStatus(sendingFlorist);
		} catch (FloristServiceException e1) {
			LOGGER.error("Error occured while getting the GoTo flag for the florist : "+sendingFlorist,e1);
		}
		
		
		mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);
		
		LOGGER.info("GoTo status for the florist is : "+isGoToFlorist);
		
		if(isGoToFlorist){

			LOGGER.info("Skipping the Soft block action for GoTo florist. Sending email now!!");
			
			String recipient;
			String messageTypeText = null;
			
			if(mars.getMessageType().equals("REJ")){
				messageTypeText = "reject";
			}else if(mars.getMessageType().equals("FOR")){
				messageTypeText = "forward";
			}
			try {
				 ConfigurationUtil configUtil;
				 DataSource db = CommonUtils.getDataSource();
		         Connection conn = db.getConnection();
		         configUtil = ConfigurationUtil.getInstance();
				
				
				String subject = configUtil.getContentWithFilter(conn, RulesActionConstants.MARS_SBLOCK_EMAIL_CONTEXT,
						RulesActionConstants.MARS_SBLOCK_EMAIL_CONTENT, RulesActionConstants.GOTO_SBLOCK_SUBJECT, null);
				String body = configUtil.getContentWithFilter(conn, RulesActionConstants.MARS_SBLOCK_EMAIL_CONTEXT,
						RulesActionConstants.MARS_SBLOCK_EMAIL_CONTENT, RulesActionConstants.GOTO_SBLOCK_BODY, null);

				subject = FieldUtils.replaceAll(subject, "~florist_id~", sendingFlorist);
				subject = FieldUtils.replaceAll(subject, "&ndash;", "-");
				subject = FieldUtils.replaceAll(subject, "~msgType~", mars.getMessageType());
				body = FieldUtils.replaceAll(body, "~membercode~", sendingFlorist);
				body = FieldUtils.replaceAll(body, "~ordernumber~", mars.getExternalOrderNumber());
				body = FieldUtils.replaceAll(body, "~mercuryordernumber~", mercuryOrderNumber);
				body = FieldUtils.replaceAll(body, "~softblockcondition~", mars.getFloristComments());
				body = FieldUtils.replaceAll(body, "~msgTypeTxt~", messageTypeText);
				
				recipient = configUtil.getFrpGlobalParm(RulesActionConstants.MARS_CONTEXT,
				  		RulesActionConstants.GOTO_SBLOCK_EMAIL_RECIPIENT);
				
				floristService.sendmail(subject,body,recipient);
			} catch (Exception e1) {
				LOGGER.error("Error caught while trying to send the email",e1);
			}
			
			
			return;
		
		}
		else{
			
			try {
				associatedMessage = mercuryService.getMercuryByOrderNumber(mercuryMessage.getMercuryOrderNumber(), mercuryMessage.getReferenceNumber());
			} 
			catch (MercuryServiceException e) {
				LOGGER.error(e.getMessage(), e);
				throw new RulesActionException(e);
			}
			catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				throw new RulesActionException(e);
			}
			
			if (associatedMessage == null) {
				LOGGER.error("Associated mercury message object is expected and can't be null. Exiting the process");
				throw new RulesActionException("Associated mercury message object is expected and can't be null. Exiting the process");
			}

			

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(new StringBuffer("Soft Block Florist action processing has STARTED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
			}
			
	    try {
	    	Calendar cal = Calendar.getInstance();
	    	cal.setTime(new Date());
	    	cal.set(Calendar.HOUR_OF_DAY, 0);
	    	cal.set(Calendar.MINUTE, 0);
	    	cal.set(Calendar.SECOND, 0);
	    	cal.set(Calendar.MILLISECOND, 0);
	    	
	    	Date blockEndDate = null;
	      
	      try {
	      	blockEndDate = RulesActionUtil.getSoftBlockDefaultEndDate();	
	      }
	      catch (RulesActionException rae){
	      	try {
	      		RulesActionUtil.sendSystemMessageWrapper(rae.getMessage(), RulesActionConstants.NOPAGE, "ERROR");	
	      	}
	      	catch (Exception e) {
	      		LOGGER.error(e);
	      	}
	      }
	    	
	    	florist = new Florist();
	      florist.setFloristId(mercuryMessage.getSendingFlorist());
	      florist.setBlockedByUserId(FloristConstants.FLORIST_BLOCK_USER_ID_MARS);
	      florist.setBlockType(FloristConstants.FLORIST_SOFT_BLOCK_TYPE);
	      florist.setBlockStartDate(cal.getTime());
	      florist.setBlockEndDate(blockEndDate);
	    	
	    	List<FloristBlocks> floristBlocks = floristService.getFloristBlocks(mercuryMessage.getSendingFlorist());
	    	
	    	if (floristBlocks != null && !floristBlocks.isEmpty()) {
	    		newBlockDateStringSet = new LinkedHashSet<String>();
	    		traversedBlockedDateSet = new LinkedHashSet<String>();
	    		while (cal.getTime().getTime() <= blockEndDate.getTime()) {
	      		for (int i = 0; i<floristBlocks.size(); i++) {
	      	    FloristBlocks fb = floristBlocks.get(i);
	      	    if (fb != null && fb.getBlockStartDate() != null && fb.getBlockEndDate() != null) {
	      	    	if ((cal.getTime().getTime() >= fb.getBlockStartDate().getTime()) && (cal.getTime().getTime() <= fb.getBlockEndDate().getTime())) {
	      	    		traversedBlockedDateSet.add(df.format(cal.getTime()));
	      	    		newBlockDateStringSet.remove(df.format(cal.getTime()));
	      	    		continue;
	      	    	}
	      	    	if (!traversedBlockedDateSet.contains(df.format(cal.getTime()))) {
	      	    		newBlockDateStringSet.add(df.format(cal.getTime()));
	      	    		florist.setBlockStartDate(null);
	      	        florist.setBlockEndDate(null);
	    	    		}
	      	    }
	          }
	      		cal.add(Calendar.DATE, 1);
	      	}
	    	}
	    	
	      LOGGER.info("Insert the data into CSR viewed id table ");
	      csrLockedStatus = mercuryService.insertCSRView(FloristConstants.FLORIST_BLOCK_USER_ID_MARS, mercuryMessage.getMercuryId(), RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);
	          	    	
	    	LOGGER.info(new StringBuffer("Locking florist id: ").append(florist.getFloristId()));
				floristService.lockFlorist(florist, FloristConstants.Y_STRING);
				
				if (florist.getLockedFlag() != null && FloristConstants.Y_STRING.equalsIgnoreCase(florist.getLockedFlag())) {
					LOGGER.info(new StringBuffer("Applying soft florist block for florist id: ").append(florist.getFloristId()));
					floristLockedStatus = FloristConstants.Y_STRING;
					StringBuffer sb = new StringBuffer("by MARS auto View Queue handling (");
					sb.append(mercuryMessage.getReferenceNumber()).append("). Text Matched: ").append(mars.getFloristComments());
					if (newBlockDateStringSet != null) {
						List<String> newBlockDateStringList = new ArrayList<String>(newBlockDateStringSet);
						for(int i = 0; i < newBlockDateStringList.size(); i++) {
							if (newBlockDateStringSet.size() == 1) {
								florist.setBlockStartDate(df.parse(newBlockDateStringList.get(i)));
								florist.setBlockEndDate(df.parse(newBlockDateStringList.get(i)));
								floristService.updateFloristBlocks(florist, FloristConstants.Y_STRING, sb.toString());
								florist.setBlockStartDate(null);
								florist.setBlockEndDate(null);
							}
							else if(i == (newBlockDateStringSet.size() - 1)) {
								Calendar tempCal = Calendar.getInstance();
								tempCal.setTime(df.parse(newBlockDateStringList.get(i)));
								tempCal.add(Calendar.DATE, 1);
								if (florist.getBlockStartDate() == null) {
									florist.setBlockStartDate(df.parse(newBlockDateStringList.get(i)));
								}
								if (tempCal.getTime().getTime() != df.parse(newBlockDateStringList.get(i)).getTime()) {
									florist.setBlockEndDate(df.parse(newBlockDateStringList.get(i)));
									floristService.updateFloristBlocks(florist, FloristConstants.Y_STRING, sb.toString());
									florist.setBlockStartDate(null);
									florist.setBlockEndDate(null);
									break;
								}
							}
							else {
								Calendar tempCal = Calendar.getInstance();
								tempCal.setTime(df.parse(newBlockDateStringList.get(i)));
								tempCal.add(Calendar.DATE, 1);
								if (florist.getBlockStartDate() == null) {
									florist.setBlockStartDate(df.parse(newBlockDateStringList.get(i)));
								}
								if (tempCal.getTime().getTime() != df.parse(newBlockDateStringList.get(i+1)).getTime()) {
									florist.setBlockEndDate(df.parse(newBlockDateStringList.get(i)));
									floristService.updateFloristBlocks(florist, FloristConstants.Y_STRING, sb.toString());
									florist.setBlockStartDate(null);
									florist.setBlockEndDate(null);
								}
							}
						}	
					}
					if (florist.getBlockStartDate() != null && florist.getBlockEndDate() != null) {
						if (newBlockDateStringSet != null && newBlockDateStringSet.isEmpty() && traversedBlockedDateSet != null) {
							LOGGER.info("Florist block already exists. So skipping the update.");
						}
						if (newBlockDateStringSet == null && traversedBlockedDateSet == null) {
							floristService.updateFloristBlocks(florist, FloristConstants.Y_STRING, sb.toString());
						}
					}
				}
				else {
					LOGGER.info("Florist is locked by another user. User Id: " + florist.getLockedByUser() + " So MARS won't process the View Queue actions");
					RulesActionUtil.sendSystemMessageWrapper("Florist is locked by another user. User Id: " + florist.getLockedByUser() + " So MARS won't process the View Queue actions", RulesActionConstants.NOPAGE, "ERROR");
					throw new RulesActionException();
				}
			} 
	    catch (FloristServiceException e) {
				LOGGER.error("Exception caught while trying to execute soft florist block action", e);
				throw new RulesActionException(e);
			}
			catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				throw new RulesActionException(e);
			}
	    finally {
	    	
	    	if (florist != null && FloristConstants.Y_STRING.equalsIgnoreCase(floristLockedStatus)) {
	    		LOGGER.info(new StringBuffer("Unlocking florist id: ").append(florist.getFloristId()));
	    		try {
	  				floristService.unlockFlorist(florist, FloristConstants.N_STRING);
	  			} 
	    		catch (Exception e) {
						LOGGER.error("Exception caught while unlocking the florist Id", e);
						StringBuffer sb = new StringBuffer("Unable to unlock the florist id: ");
						sb.append(florist.getFloristId())
							.append(". Error Message: ").append(e.getMessage());
						RulesActionUtil.sendSystemMessageWrapper(sb.toString(), "MARS_PAGE", "ERROR");
					}
	    	}
				
				if (FloristConstants.Y_STRING.equalsIgnoreCase(csrLockedStatus)) {
					LOGGER.info("Delete the data from CSR viewed id table ");
					try {
						mercuryService.deleteCSRView(FloristConstants.FLORIST_BLOCK_USER_ID_MARS, mercuryMessage.getMercuryId(), RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);
					}
					catch (Exception e) {
						LOGGER.error("Exception caught while removing the CSR ID from CSR viewed id table", e);
						StringBuffer sb = new StringBuffer("Unable to delete CSR Id/Entity Id/Entity Type: ");
						sb.append(FloristConstants.FLORIST_BLOCK_USER_ID_MARS).append("/")
							.append(mercuryMessage.getMercuryId()).append("/")
							.append(RulesActionConstants.CSR_ENTITY_TYPE_MERCURY)
							.append(". Error Message: ").append(e.getMessage());
						RulesActionUtil.sendSystemMessageWrapper(sb.toString(), "MARS_PAGE", "ERROR");
					}
				}
	    }
	    
	    try {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Tracking Soft Block Florist action in DB");
				}
				
				OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
				this.logTrackingResults(mars, ruleFired, RulesActionConstants.SOFT_BLOCK_FLORIST_ACTION, RulesActionConstants.ACTION_SUCCESS, orderDetails);
			}
			catch (Exception e) {
				LOGGER.error("Exception caught while trying to persist the tracking results into DB", e);
			}
	    
			
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(new StringBuffer("Soft Block Florist action processing has ENDED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
			}

			
		}
		
		
	}

	public MercuryService getMercuryService() {
		return mercuryService;
	}

	public void setMercuryService(MercuryService mercuryService) {
		this.mercuryService = mercuryService;
	}

	public FloristService getFloristService() {
		return floristService;
	}

	public void setFloristService(FloristService floristService) {
		this.floristService = floristService;
	}

}
