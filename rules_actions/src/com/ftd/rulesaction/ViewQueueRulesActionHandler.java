/**
 * 
 */
package com.ftd.rulesaction;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * A spring bean that processes all the action codes sent the caller.
 * 
 * @author kdatchan
 *
 */
@Component
public class ViewQueueRulesActionHandler {

	@Autowired
	private SoftBlockFloristAction softBlockFlorist;

	@Autowired
	private BlockFloristCityAction blockFloristCityAction;

	@Autowired
	private BlockFloristCodificationAction floristCodificationAction;

	@Autowired
	private BlockFloristCodificationGroupAction floristCodificationGroupAction;

	@Autowired
	private BlockFloristZipAction floristZipAction;

	@Autowired
	private UpdateFloristZipCutoffAction updateFloristZipCutoffAction;

	@Autowired
	private UpdateViewQueueAction updateViewQueue;

	/**
	 * Suspends the current transaction and works on a new transaction to
	 * process the action codes. Iterates the action codes and process them
	 * sequentially
	 * 
	 * @param ruleFacts
	 * @param ruleFiredMap
	 * @param actionMetaData
	 * @throws RulesActionException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handleActions(Map<String, Object> ruleFacts,
			Map<String, List<String>> ruleFiredMap,
			Map<String, Object> actionMetaData) {

		if (ruleFiredMap == null || ruleFiredMap.isEmpty()) {
			return;
		}

		Set<String> rulesFired = ruleFiredMap.keySet();

		for (String ruleFired : rulesFired) {
			List<String> actionCodes = (List<String>) ruleFiredMap
					.get(ruleFired);

			List<String> actionDescriptions = (List<String>) ruleFiredMap
					.get(ruleFired + "ACTION_DESCRIPTION");
			MarsVO marsVO = (MarsVO) ruleFacts
					.get(RulesActionConstants.MARS_OBJ);
			if (actionDescriptions != null && !actionDescriptions.isEmpty()) {
				marsVO.setActionDescription(actionDescriptions.get(0));
			} else {
				marsVO.setActionDescription(null);
			}

			if (actionCodes != null) {
				try {
					for (String actionCode : actionCodes) {
						if (RulesActionConstants.SOFT_BLOCK_FLORIST_ACTION
								.equalsIgnoreCase(actionCode)) {
							softBlockFlorist.execute(ruleFacts, ruleFired);
						}

						else if (RulesActionConstants.REMOVE_FROM_VIEW_QUEUE
								.equalsIgnoreCase(actionCode)) {
							updateViewQueue.execute(ruleFacts, ruleFired);
						}

						else if (RulesActionConstants.BLOCK_FLORIST_CODIFICATION
								.equalsIgnoreCase(actionCode)) {
							floristCodificationAction.execute(ruleFacts,
									ruleFired);
						}

						else if (RulesActionConstants.BLOCK_FLORIST_CODIFICATION_GROUP
								.equalsIgnoreCase(actionCode)) {
							floristCodificationGroupAction.execute(ruleFacts,
									ruleFired);
						}

						else if (RulesActionConstants.BLOCK_FLORIST_ZIP
								.equalsIgnoreCase(actionCode)) {
							floristZipAction.execute(ruleFacts, ruleFired);
						}

						else if (RulesActionConstants.UPDATE_FLORIST_ZIP_CUTOFF_ACTION
								.equalsIgnoreCase(actionCode)) {
							updateFloristZipCutoffAction.execute(ruleFacts,
									ruleFired);
						} else if (RulesActionConstants.BLOCK_FLORIST_CITY_TEMPORARY
								.equalsIgnoreCase(actionCode)) {
							blockFloristCityAction.executeRules(ruleFacts,
									ruleFired, true);
						}
					}
				} catch (RulesActionException ex) {
					return;
				}
			}
		}

	}

	public UpdateViewQueueAction getUpdateViewQueue() {
		return updateViewQueue;
	}

	public void setUpdateViewQueue(UpdateViewQueueAction updateViewQueue) {
		this.updateViewQueue = updateViewQueue;
	}

	public SoftBlockFloristAction getSoftBlockFlorist() {
		return softBlockFlorist;
	}

	public void setSoftBlockFlorist(SoftBlockFloristAction softBlockFlorist) {
		this.softBlockFlorist = softBlockFlorist;
	}

	public BlockFloristCodificationAction getFloristCodificationAction() {
		return floristCodificationAction;
	}

	public void setFloristCodificationAction(
			BlockFloristCodificationAction floristCodificationAction) {
		this.floristCodificationAction = floristCodificationAction;
	}

	public BlockFloristCodificationGroupAction getFloristCodificationGroupAction() {
		return floristCodificationGroupAction;
	}

	public void setFloristCodificationGroupAction(
			BlockFloristCodificationGroupAction floristCodificationGroupAction) {
		this.floristCodificationGroupAction = floristCodificationGroupAction;
	}

	public BlockFloristZipAction getFloristZipAction() {
		return floristZipAction;
	}

	public void setFloristZipAction(BlockFloristZipAction floristZipAction) {
		this.floristZipAction = floristZipAction;
	}

	public UpdateFloristZipCutoffAction getUpdateFloristZipCutoffAction() {
		return updateFloristZipCutoffAction;
	}

	public void setUpdateFloristZipCutoffAction(
			UpdateFloristZipCutoffAction updateFloristZipCutoffAction) {
		this.updateFloristZipCutoffAction = updateFloristZipCutoffAction;
	}
}
