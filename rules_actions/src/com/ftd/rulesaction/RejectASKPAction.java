package com.ftd.rulesaction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.Comments;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * A spring bean to process the Reject ASKP action code
 * 
 * @author kdatchan
 *
 */
@Component
public class RejectASKPAction extends RulesAction {

	private static final Logger LOGGER = Logger.getLogger(RejectASKPAction.class);
	
	@Autowired
	private SendFTDMsgAction sendFtdAction;
	
	@Autowired
	private SendCANMsgAction sendCanAction;
	
	@Autowired
	private UpdateOrderCommentsAction updateOrderComments;
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Sends a FTD message by invoking the appropriate action handler class.
	 * <br/>
	 * 2. Sends a CAN message by invoking the appropriate action handler class.
	 * <br/>
	 * 3. Updates order comments by invoking the appropriate action handler class.
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
		Comments comments = (Comments) map.get(RulesActionConstants.COMMENTS_OBJ);
		
		if (mercuryMessage == null || orderDetails == null || comments == null) {
			LOGGER.error("Mercury message/Order details/COmments objects are required and can't be null. Exiting the process");
			throw new RulesActionException("Mercury message/Order details/COmments objects are required and can't be null. Exiting the process");
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Execute Reject ASKP action processing has STARTED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
		
		LOGGER.info(new StringBuffer("Sending automated FTD message for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		sendFtdAction.execute(map, ruleFired);
		
		LOGGER.info(new StringBuffer("Sending automated CAN message to the florist for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		sendCanAction.execute(map, ruleFired);
		
		LOGGER.info(new StringBuffer("Updating order comments for incoming mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		updateOrderComments.execute(map, ruleFired);
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Execute Reject ASKP action processing has ENDED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
	}

	public SendFTDMsgAction getSendFtdAction() {
		return sendFtdAction;
	}

	public void setSendFtdAction(SendFTDMsgAction sendFtdAction) {
		this.sendFtdAction = sendFtdAction;
	}

	public SendCANMsgAction getSendCanAction() {
		return sendCanAction;
	}

	public void setSendCanAction(SendCANMsgAction sendCanAction) {
		this.sendCanAction = sendCanAction;
	}

	public UpdateOrderCommentsAction getUpdateOrderComments() {
		return updateOrderComments;
	}

	public void setUpdateOrderComments(UpdateOrderCommentsAction updateOrderComments) {
		this.updateOrderComments = updateOrderComments;
	}


}
