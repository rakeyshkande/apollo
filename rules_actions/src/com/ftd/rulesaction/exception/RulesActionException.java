/**
 * 
 */
package com.ftd.rulesaction.exception;

/**
 * @author kdatchan
 *
 */
public class RulesActionException extends Exception
{
	private static final long serialVersionUID = 1L;

	public RulesActionException() {
		super();
	}

	public RulesActionException(String message, Throwable cause) {
		super(message, cause);
	}

	public RulesActionException(String message) {
		super(message);
	}

	public RulesActionException(Throwable cause) {
		super(cause);
	}
	
}
