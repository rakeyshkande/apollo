package com.ftd.rulesaction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.Comments;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.orderservice.exception.OrderServiceException;
import com.ftd.orderservice.service.OrderService;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the update order comments action 
 * 
 * @author kdatchan
 *
 */
@Component
public class UpdateOrderCommentsAction extends RulesAction {

	private static final Logger LOGGER = Logger.getLogger(UpdateOrderCommentsAction.class);
	
	@Autowired
	@Qualifier(value="orderService")
	private OrderService orderService;
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Updates order comments by invoking the Order service.
	 * <br/>
	 * 2. Tracks the result into rules tracking table
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {

		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
		Comments comments = (Comments) map.get(RulesActionConstants.COMMENTS_OBJ);
		
		if(orderDetails == null) {
			LOGGER.error("Order details is expected and can't be null. Exiting the process");
			throw new RulesActionException("Order details is expected and can't be null. Exiting the process");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Update Order Comments action processing has STARTED for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
		}
		
		Comments orderComments = RulesActionUtil.prepareOrderComment(orderDetails, comments);
		try {
			LOGGER.info(new StringBuffer("Updating order comments for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
			orderService.updateComments(orderComments);
		} 
		catch (OrderServiceException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Update Order Comments action in DB");
			}
			MarsVO mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);
			this.logTrackingResults(mars, ruleFired, RulesActionConstants.UPDATE_ORDER_COMMENTS_ACTION, RulesActionConstants.ACTION_SUCCESS, orderDetails);
		}
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to persist the tracking results into DB", e);
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Update Order Comments action processing has ENDED for order detail id: ").append(orderDetails.getOrderDetailId()).toString());
		}
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
}
