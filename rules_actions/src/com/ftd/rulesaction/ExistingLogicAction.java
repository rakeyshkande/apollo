/**
 * 
 */
package com.ftd.rulesaction;

import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;


/**
 * A spring bean to process the Execute existing logic action
 * 
 * @author kdatchan
 *
 */
@Component
public class ExistingLogicAction extends RulesAction {
	
	private static final Logger LOGGER = Logger.getLogger(ExistingLogicAction.class);
	
	/**
	 * Participates if there is already an ongoing transaction, 
	 * else creates a new transaction and does the processing.
	 * <br/>
	 * 1. Sends a JMS message to OJMS.MERCURY_INBOUND queue.
	 * 
	 */
	@Transactional(rollbackFor=RulesActionException.class)
	public void execute(Map<String, Object> map, String ruleFired) throws RulesActionException {
		
		if(map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}
		
		Mercury mercuryMessage = (Mercury) map.get(RulesActionConstants.MERCURY_OBJ);
		
		if (mercuryMessage == null) {
			LOGGER.error("Mercury message is required to send the message to MERCURY_INBOUND queue");
			throw new RulesActionException("Mercury message is required to send the message to MERCURY_INBOUND queue");
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Execute Existing Logic action processing has STARTED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
		
		InitialContext context;
		try {
			context = new InitialContext();
	    MessageToken token = new MessageToken();
	    token.setMessage(mercuryMessage.getMercuryId());
	    RulesActionUtil.sendJMSMessage(context, token, RulesActionConstants.MERCURY_INBOUND_QUEUE);
		} 
		catch (NamingException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		} 
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}
		
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Execute Existing Logic action in DB");
			}
			MarsVO mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);
			OrderDetails orderDetails = (OrderDetails) map.get(RulesActionConstants.ORDER_DETAILS_OBJ);
			this.logTrackingResults(mars, ruleFired, RulesActionConstants.EXECUTE_EXISTING_LOGIC_ACTION, RulesActionConstants.ACTION_SUCCESS, orderDetails);
		}
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to persist the tracking results into DB", e);
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer("Execute Existing Logic action processing has ENDED for mercury id: ").append(mercuryMessage.getMercuryId()).toString());
		}
	}

}
