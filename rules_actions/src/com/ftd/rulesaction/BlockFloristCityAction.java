package com.ftd.rulesaction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftd.constant.FloristConstants;
import com.ftd.core.domain.Florist;
import com.ftd.core.domain.FloristCity;
import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.exception.FloristServiceException;
import com.ftd.floristservice.service.FloristService;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;
import com.ftd.rulesaction.util.RulesActionUtil;

/**
 * A spring bean to process the Block Florist City action
 * 
 * @author gboddu
 *
 */
@Component
public class BlockFloristCityAction extends RulesAction {

	@Autowired
	@Qualifier(value = "mercuryService")
	private MercuryService mercuryService;

	@Autowired
	@Qualifier(value = "floristService")
	private FloristService floristService;

	@Autowired
	private UpdateViewQueueAction updateViewQueue;

	private static final Logger LOGGER = Logger
			.getLogger(BlockFloristCityAction.class);

	/**
	 * Participates if there is already an ongoing transaction, else creates a
	 * new transaction and does the processing. <br/>
	 * 1. Blocks the City for the florist by invoking the Florist service. <br/>
	 * 2. Tracks the result into rules tracking table
	 * 
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void executeRules(Map<String, Object> map, String ruleFired,
			boolean includeBlockEndDate) throws RulesActionException {

		Mercury associatedMessage;
		MarsVO mars;
		Florist florist = null;
		String floristLockedStatus = "N";
		String csrLockedStatus = "N";
		boolean isCitySearchOrder = true;
		String isViewQueueOn = "N";
		String action = null;
		String skipErosFlorist = null;
		ConfigurationUtil configUtil;
		
		
		if (map == null) {
			LOGGER.error("Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
			throw new RulesActionException(
					"Mercury message and Order Details are expected for processing Approve ASKP and it can't be null. Exiting the process");
		}

		Mercury mercuryMessage = (Mercury) map
				.get(RulesActionConstants.MERCURY_OBJ);

		if (mercuryMessage == null) {
			LOGGER.error("Mercury message object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"Mercury message object is expected and can't be null. Exiting the process");
		}
		
		try{
			configUtil = ConfigurationUtil.getInstance();
			skipErosFlorist = configUtil.getFrpGlobalParm(
				RulesActionConstants.MARS_CONTEXT,
				RulesActionConstants.SKIP_EROS_FLORIST);
		
		if(mercuryMessage.getSendingFlorist().equalsIgnoreCase(skipErosFlorist)){
			LOGGER.error("REJ message received for the Skip EROS florist. Exiting the process");
		}
		}catch(Exception e){
			LOGGER.error("Unable to get the Skip Eros florist from global parameters. Exiting block florist city process");
			return;
		}

		try {
			isCitySearchOrder = floristService
					.isCitySearchedOrder(mercuryMessage
							.getMercuryOrderNumber());
		} catch (FloristServiceException e2) {
			LOGGER.error("Unable to get City Search Functionality for the message. Exiting block florist city process");
			return;
		}

		LOGGER.info("Is city searched order : " + isCitySearchOrder);

		if (!isCitySearchOrder) {
			LOGGER.error("The message was not sent by using the city search functionality. Exiting block florist city process");
			return;
		}

		try {
					configUtil = ConfigurationUtil.getInstance();
					isViewQueueOn = configUtil.getFrpGlobalParm(
					RulesActionConstants.LMD_CONTEXT,
					RulesActionConstants.VIEW_QUEUE_SWITCH);
		} catch (Exception e1) {
			LOGGER.error("Unable to get the View Queue Swith from global parameters. Exiting block florist city process");
			return;
		}

		LOGGER.info("View Queue Switch : " + isViewQueueOn);

		if (isViewQueueOn.equals("Y") && !includeBlockEndDate) {
			LOGGER.error("VIEW QUEUE SWITCH is turned on. Cannot block the florist city permanently");
			return;
		} else if (isViewQueueOn.equals("N") && includeBlockEndDate) {
			LOGGER.error("VIEW QUEUE SWITCH is turned off. Cannot block the florist city temporarily");
			return;
		}

		if (includeBlockEndDate) {
			action = RulesActionConstants.BLOCK_FLORIST_CITY_TEMPORARY;
		} else {
			action = RulesActionConstants.BLOCK_FLORIST_CITY_PERMANENT;
		}

		try {
			associatedMessage = mercuryService.getMercuryByOrderNumber(
					mercuryMessage.getMercuryOrderNumber(),
					mercuryMessage.getReferenceNumber());
		} catch (MercuryServiceException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		}

		if (associatedMessage == null) {
			LOGGER.error("Associated mercury message object is expected and can't be null. Exiting the process");
			throw new RulesActionException(
					"Associated mercury message object is expected and can't be null. Exiting the process");
		}

		mars = (MarsVO) map.get(RulesActionConstants.MARS_OBJ);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer(
					"Block Florist City action processing has STARTED for mercury id: ")
					.append(mercuryMessage.getMercuryId()).toString());
		}

		try {
			Date blockStartDate = new Date();
			Date blockEndDate = null;

			if (includeBlockEndDate) {
				try {
					blockEndDate = RulesActionUtil.getCityBlockEndDate();
				} catch (RulesActionException rae) {
					try {
						RulesActionUtil.sendSystemMessageWrapper(
								rae.getMessage(), RulesActionConstants.NOPAGE,
								"ERROR");
					} catch (Exception e) {
						LOGGER.error(e);
					}
				}

				if (blockEndDate == null) {
					throw new RulesActionException();
				}

				if (RulesActionUtil.isEndDateLTStartDate(blockEndDate)) {
					StringBuilder message = new StringBuilder(
							"Block End date: ");
					message.append(blockEndDate)
							.append(" should not be before today's date. Please re-configure the Global Param: ")
							.append(RulesActionConstants.VIEW_QUEUE_END_DATE_CITY)
							.append(" properly.");
					try {
						RulesActionUtil.sendSystemMessageWrapper(
								message.toString(),
								RulesActionConstants.NOPAGE, "ERROR");
					} catch (Exception e) {
						LOGGER.error(e);
					}
					throw new RulesActionException();
				}

			}

			FloristCity floristCity = floristService
					.getFloristCity(associatedMessage.getFillingFlorist());//mercuryMessage.getSendingFlorist());
			florist = new Florist();
			florist.setFloristId(associatedMessage.getFillingFlorist());
			florist.setBlockedByUserId(FloristConstants.FLORIST_BLOCK_USER_ID_MARS);

			if (floristCity == null) {

				mercuryMessage.setViewQueue("N");
				map.put(RulesActionConstants.MERCURY_OBJ, mercuryMessage);

				LOGGER.info("City is not associated to the Florist, removing from VIEW QUEUE");

				updateViewQueue.execute(map, ruleFired);
				// return;
				throw new RulesActionException(
						"City is not associated to the Florist. This request will be processed by the existing system");
			}

			if (floristCity != null && includeBlockEndDate
					&& floristCity.getBlockEndDate() != null
					&& blockEndDate != null) {
				if (RulesActionUtil.isDateSame(blockEndDate,
						floristCity.getBlockEndDate())) {
					LOGGER.info("Block end date is already present in the florist City table. So skipping the update");
					return;
				}
			}

			floristCity.setBlockEndDate(blockEndDate);

			if (floristCity.getBlockStartDate() == null) {
				floristCity.setBlockStartDate(blockStartDate);
			}

			LOGGER.info("Insert the data into CSR viewed id table ");
			csrLockedStatus = mercuryService.insertCSRView(
					FloristConstants.FLORIST_BLOCK_USER_ID_MARS,
					mercuryMessage.getMercuryId(),
					RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);

			LOGGER.info(new StringBuffer("Locking florist id: ")
					.append(floristCity.getFloristId()));
			floristService.lockFlorist(florist, FloristConstants.Y_STRING);

			if (florist.getLockedFlag() != null
					&& FloristConstants.Y_STRING.equalsIgnoreCase(florist
							.getLockedFlag())) {
				LOGGER.info(new StringBuffer("Blocking city for florist id: ")
						.append(florist.getFloristId()));
				floristLockedStatus = florist.getLockedFlag();
				StringBuffer sb = null;
				if (includeBlockEndDate) {
					DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
					sb = new StringBuffer("Block End Date "
							+ df.format(blockEndDate)
							+ " was added by MARS auto View Queue handling ("
							+ mercuryMessage.getReferenceNumber() + ").\n");
				} else {
					sb = new StringBuffer(
							"Permanent block was added by MARS auto handling ("
									+ mercuryMessage.getReferenceNumber()
									+ ").\n");
				}
				sb.append(
						"City " + floristCity.getCity()
								+ " was blocked by MARS auto handling ("
								+ mercuryMessage.getReferenceNumber()
								+ "). Text Matched: ").append(
						mars.getFloristComments());
				floristService.updateFloristCity(floristCity,
						FloristConstants.Y_STRING,
						FloristConstants.FLORIST_BLOCK_USER_ID_MARS,
						sb.toString());
			} else {
				LOGGER.info("Florist is locked by another user. User Id: "
						+ florist.getLockedByUser()
						+ " So MARS won't process the View Queue actions");
				RulesActionUtil.sendSystemMessageWrapper("Florist is locked by another user. User Id: "
						+ florist.getLockedByUser()
						+ " So MARS won't process the View Queue actions",
						"MARS_PAGE", "ERROR");
				throw new RulesActionException();
				
			}
		} catch (FloristServiceException e) {
			LOGGER.error(
					"Exception caught while trying to execute block florist city action",
					e);
			throw new RulesActionException(e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RulesActionException(e);
		} finally {

			if (florist != null
					&& FloristConstants.Y_STRING
							.equalsIgnoreCase(floristLockedStatus)) {
				LOGGER.info(new StringBuffer("Unlocking florist id: ")
						.append(florist.getFloristId()));
				try {
					floristService.unlockFlorist(florist,
							FloristConstants.N_STRING);
				} catch (Exception e) {
					LOGGER.error(
							"Exception caught while unlocking the florist Id",
							e);
					StringBuffer sb = new StringBuffer(
							"Unable to unlock the florist id: ");
					sb.append(florist.getFloristId())
							.append(". Error Message: ").append(e.getMessage());
					RulesActionUtil.sendSystemMessageWrapper(sb.toString(),
							"MARS_PAGE", "ERROR");
				}
			}

			if (FloristConstants.Y_STRING.equalsIgnoreCase(csrLockedStatus)) {
				LOGGER.info("Delete the data from CSR viewed id table ");
				try {
					mercuryService.deleteCSRView(
							FloristConstants.FLORIST_BLOCK_USER_ID_MARS,
							mercuryMessage.getMercuryId(),
							RulesActionConstants.CSR_ENTITY_TYPE_MERCURY);
				} catch (Exception e) {
					LOGGER.error(
							"Exception caught while removing the CSR ID from CSR viewed id table",
							e);
					StringBuffer sb = new StringBuffer(
							"Unable to delete CSR Id/Entity Id/Entity Type: ");
					sb.append(FloristConstants.FLORIST_BLOCK_USER_ID_MARS)
							.append("/")
							.append(mercuryMessage.getMercuryId())
							.append("/")
							.append(RulesActionConstants.CSR_ENTITY_TYPE_MERCURY)
							.append(". Error Message: ").append(e.getMessage());
					RulesActionUtil.sendSystemMessageWrapper(sb.toString(),
							"MARS_PAGE", "ERROR");
				}
			}
		}

		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tracking Block Florist City action in DB");
			}
			OrderDetails orderDetails = (OrderDetails) map
					.get(RulesActionConstants.ORDER_DETAILS_OBJ);
			this.logTrackingResults(mars, ruleFired, action,
					RulesActionConstants.ACTION_SUCCESS, orderDetails);
		} catch (Exception e) {
			LOGGER.error(
					"Exception caught while trying to persist the tracking results into DB",
					e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuffer(
					"Block Florist City action processing has ENDED for mercury id: ")
					.append(mercuryMessage.getMercuryId()).toString());
		}

	}

	public MercuryService getMercuryService() {
		return mercuryService;
	}

	public void setMercuryService(MercuryService mercuryService) {
		this.mercuryService = mercuryService;
	}

	public FloristService getFloristService() {
		return floristService;
	}

	public void setFloristService(FloristService floristService) {
		this.floristService = floristService;
	}

	@Override
	protected void execute(Map<String, Object> ruleFacts, String ruleFired)
			throws RulesActionException {
		// TODO Auto-generated method stub

	}

}
