/**
 * 
 */
package com.ftd.rulesaction.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.OrderDetails;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.service.FloristService;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.orderservice.exception.OrderServiceException;
import com.ftd.orderservice.service.OrderService;
import com.ftd.rulesaction.UpdateFloristZipCutoffAction;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * @author kdatchan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:rules-action-context.xml")
public class ChangeZipCutoffActionTest extends BaseTest
{
	@Autowired
	private UpdateFloristZipCutoffAction updateFloristZipCutoffAction;
	
	@Autowired
	private FloristService floristService;
	
	@Autowired
	private MercuryService mercuryService;
	
	@Autowired
	private OrderService orderService;
	
	@Test
	public void testUpdateFloristZips() {
		try {
			Mercury mercury = mercuryService.getMercuryById("Z5439I-0139-02042016");
			Mercury associateMercury = mercuryService.getMercuryByOrderNumber(mercury.getMercuryOrderNumber(), mercury.getReferenceNumber());
			OrderDetails orderDetails = orderService.getOrderDetailById(Long.valueOf(mercury.getReferenceNumber()));
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(RulesActionConstants.MERCURY_OBJ, mercury);
			map.put(RulesActionConstants.MARS_OBJ, getMars());
			map.put(RulesActionConstants.ORDER_DETAILS_OBJ, orderDetails);
			map.put(RulesActionConstants.ASSOCIATED_MERCURY_OBJ, associateMercury);
			updateFloristZipCutoffAction.execute(map, null);
		} 
		catch (RulesActionException e) {
			e.printStackTrace();
		} 
		catch (MercuryServiceException e) {
			e.printStackTrace();
		} 
		catch (NumberFormatException e) {
			e.printStackTrace();
		} 
		catch (OrderServiceException e) {
			e.printStackTrace();
		}
	}
	
	private MarsVO getMars() {
		MarsVO marsVO = new MarsVO();
		marsVO.setMercuryId("Z5439I-0139-02042016");
		marsVO.setCodifiedId("FCM");
		
		return marsVO;
	}
	
	
}
