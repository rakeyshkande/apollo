/**
 * 
 */
package com.ftd.rulesaction.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.core.domain.Mercury;
import com.ftd.core.domain.request.MarsVO;
import com.ftd.floristservice.service.FloristService;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.rulesaction.BlockFloristZipAction;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * @author kdatchan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:rules-action-context.xml")
public class BlockFloristZipActionTest extends BaseTest
{
	@Autowired
	private BlockFloristZipAction blockFloristZipAction;
	
	@Autowired
	private FloristService floristService;
	
	@Autowired
	private MercuryService mercuryService;
	
	
	@Test
	public void testUpdateFloristZips() {
		try {
			Mercury mercury = mercuryService.getMercuryById("Z5439I-0139-02042016");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(RulesActionConstants.MERCURY_OBJ, mercury);
			map.put(RulesActionConstants.MARS_OBJ, getMars());
			blockFloristZipAction.execute(map, null);
		} catch (RulesActionException e) {
			e.printStackTrace();
		} catch (MercuryServiceException e) {
			e.printStackTrace();
		}
	}
	
	private MarsVO getMars() {
		MarsVO marsVO = new MarsVO();
		marsVO.setMercuryId("Z5439I-0139-02042016");
		marsVO.setCodifiedId("FCM");
		
		return marsVO;
	}
	
	
}
