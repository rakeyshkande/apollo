/**
 * 
 */
package com.ftd.rulesaction.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.constant.MercuryConstants;
import com.ftd.core.domain.Mercury;
import com.ftd.rulesaction.ApproveASKPAction;
import com.ftd.rulesaction.constant.RulesActionConstants;
import com.ftd.rulesaction.exception.RulesActionException;

/**
 * @author skatam
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:rules-action-context.xml")
public class ApproveASKPActionTest 
{
	@Autowired
	private ApproveASKPAction approveASKPAction;
	
//	@Test
//	public void test_processASKPmercury() {
//		String mercuryMsgId ="";
//		mercuryService.processASKPMessage(mercuryMsgId);
//	}
	
	@Test
	public void testSendAnswerMessage() {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(RulesActionConstants.MERCURY_OBJ, getmercury());
			approveASKPAction.execute(map, null);
		} catch (RulesActionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Hi");
	}
	
	private Mercury getmercury() {
		Mercury mercury = new Mercury();
		mercury.setMercuryStatus(MercuryConstants.MERCURY_OPEN);
		mercury.setMessageType(MercuryConstants.FTD);
		mercury.setMercuryOrderNumber("A5490B-8579");
  	mercury.setComments("Test Message");
  	mercury.setSendingFlorist("90-8400AA");
  	mercury.setOldPrice(new BigDecimal(12.99));
  	mercury.setPrice(new BigDecimal(15.99));
  	mercury.setOperator("DK12");
  	mercury.setAskAnswerCode("");
  	mercury.setMessageDirection(MercuryConstants.OUTBOUND);
		mercury.setTransmissionDate(new Date());
		mercury.setViewQueue(MercuryConstants.N_STRING);
		mercury.setRequireConfirmation(MercuryConstants.N_STRING);
		return mercury;
	}
}
