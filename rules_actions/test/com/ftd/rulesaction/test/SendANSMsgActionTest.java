/**
 * 
 */
package com.ftd.rulesaction.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.constant.MercuryConstants;
import com.ftd.core.domain.Mercury;
import com.ftd.rulesaction.SendANSMsgAction;
import com.ftd.rulesaction.constant.RulesActionConstants;

/**
 * @author skatam
 * @author kdatchan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:rules-action-context.xml")
public class SendANSMsgActionTest extends BaseTest
{
	@Autowired
	private SendANSMsgAction sendANSMsgAction;
	
	@Test
	public void testSendAnswerMessage() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(RulesActionConstants.MERCURY_OBJ, getmercury());
		sendANSMsgAction.execute(map, null);
	}
	
	private Mercury getmercury() {
		Mercury mercury = new Mercury();
		mercury.setMercuryStatus(MercuryConstants.MERCURY_OPEN);
		mercury.setMessageType(MercuryConstants.FTD);
		mercury.setMercuryOrderNumber("A5490B-8579");
  	mercury.setComments("Test Message");
  	mercury.setSendingFlorist("90-8400AA");
  	mercury.setOldPrice(new BigDecimal("12.99"));
  	mercury.setPrice(new BigDecimal("15.99"));
  	mercury.setOperator("DK12");
  	mercury.setAskAnswerCode("");
  	mercury.setMessageDirection(MercuryConstants.OUTBOUND);
		mercury.setTransmissionDate(new Date());
		mercury.setViewQueue(MercuryConstants.N_STRING);
		mercury.setRequireConfirmation(MercuryConstants.N_STRING);
		return mercury;
	}
}
