/**
 * 
 */
package com.ftd.rulesaction.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ftd.core.domain.Mercury;
import com.ftd.mercuryservice.exception.MercuryServiceException;
import com.ftd.mercuryservice.service.MercuryService;
import com.ftd.rulesaction.SendCANMsgAction;
import com.ftd.rulesaction.constant.RulesActionConstants;

/**
 * @author kdatchan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:rules-action-context.xml")
public class SendCANMsgActionTest extends BaseTest 
{
	@Autowired
	private SendCANMsgAction sendCANMsgAction;
	
	@Autowired
	private MercuryService mercuryService;
		
	@Test
	public void testSendCancelMessage() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(RulesActionConstants.MERCURY_OBJ, getmercury());
		sendCANMsgAction.execute(map, null);
	}
	
	private Mercury getmercury() throws MercuryServiceException {
		Mercury mercury = mercuryService.getMercuryById("B0143V-7177-12152010");
		return mercury;
	}
}
