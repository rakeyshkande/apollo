package com.ftd.novator.giftcert.common;

public interface XMLInterface 
{

  public String toXML() throws IllegalAccessException, ClassNotFoundException;
}