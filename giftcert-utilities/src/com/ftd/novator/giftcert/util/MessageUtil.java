package com.ftd.novator.giftcert.util;
import com.ftd.novator.giftcert.dao.NovatorGiftCertDao;
import com.ftd.novator.giftcert.vo.GiftCertMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


public class MessageUtil 
{

    private Logger logger = 
        new Logger("com.ftd.novator.giftcert.util.MessageUtil");
    Connection dbConn = null;

    public MessageUtil()
    {
        super();
    }
    public MessageUtil(Connection conn)
    {
        super();
        dbConn = conn;
    }
    
    public boolean isMsgRequired (String gccNumber) throws Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering isMsgRequired");
            logger.debug("gccNumber : " + gccNumber);
        }
        boolean msgRequired = false;
        
        try{
            NovatorGiftCertDao novGiftCertDAO = new NovatorGiftCertDao(dbConn);
            String redemptionType = novGiftCertDAO.getGiftCertRedemptionType(gccNumber);
            if(redemptionType.equals("I"))
            {
                msgRequired = true;
            }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting isMsgRequired");
            }
        }
        
        return msgRequired;
    }
    
    public void dispatchMessage(String message)
        throws NamingException, JMSException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering dispatchMessage");
            logger.debug("message : " + message);
        }

        try{
            MessageToken messageToken = new MessageToken();
            messageToken.setStatus("NOVATOR_GIFT_CERT");    
            messageToken.setMessage(message);   
        
            Dispatcher dispatcher = Dispatcher.getInstance();

            /* enlist the JMS transaction, with the other 
             * application transaction 
            */
            dispatcher.dispatchTextMessage(new InitialContext(), messageToken); 
           
        
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchMessage");
            }
        }
    }
    
    public void dispatchMessages(List giftCertMessageVOList)
        throws NamingException, JMSException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering dispatchMessage(List)");
        }

        try{
        
        if (giftCertMessageVOList != null && giftCertMessageVOList.size() > 0) {
            // Construct a list of MessageTokens.
            List tokenList = new ArrayList(giftCertMessageVOList.size());
            for (Iterator i = giftCertMessageVOList.iterator(); i.hasNext();) 
            {
              GiftCertMessageVO message = (GiftCertMessageVO) i.next();
              logger.debug("message : " + message.toXML());
              MessageToken messageToken = new MessageToken();
              messageToken.setStatus("NOVATOR_GIFT_CERT");
              messageToken.setMessage(message.toXML()); 
              tokenList.add(messageToken);
            }  
        
            Dispatcher dispatcher = Dispatcher.getInstance();

            /* enlist the JMS transaction, with the other 
             * application transaction 
            */
            dispatcher.dispatchTextMessages(new InitialContext(), tokenList); 
           
        }
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting dispatchMessage(List)");
            }
        }
    }

}