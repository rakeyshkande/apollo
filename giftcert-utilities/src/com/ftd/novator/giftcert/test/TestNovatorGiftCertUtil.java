package com.ftd.accountingreporting.test;

import com.ftd.novator.giftcert.util.MessageUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class TestNovatorGiftCertUtil  extends TestCase
{
  public TestNovatorGiftCertUtil(String test)
  {
    super(test);
  }

   public void testDoInvoke() {
      try {
         /* Create Connection object. */
              
                Connection conn = getConn();
        MessageUtil msgUtil = new MessageUtil(conn);
        boolean msgReq = msgUtil.isMsgRequired("1159554088");
        System.out.println(msgReq);
        conn.close();
         }catch (Throwable e) {
            e.printStackTrace();
        }
  }
  
     public void testDispatchMessage() {
      try {
         /* Create Connection object. */
              

        MessageUtil msgUtil = new MessageUtil();
        msgUtil.dispatchMessage("test");

         }catch (Throwable e) {
            e.printStackTrace();
        }
  }
public Connection getConn()
{
Connection connection=null;
    try{
    String driver_ = "oracle.jdbc.driver.OracleDriver";
            String database_ = "jdbc:Oracle:thin:@stheno-dev.ftdi.com:1522:DEV5";
            String user_ = "osp";
            String password_ = "osp";

            Class.forName(driver_);
            connection = DriverManager.getConnection(database_, user_, password_);
    }catch(Exception e){}
    return connection;
    }
  public static Test suite() {
    	TestSuite suite= new TestSuite();
      
      suite.addTest(new TestNovatorGiftCertUtil("testDispatchMessage"));
      return suite;
 }
 
 public static void main(String[] args){
    junit.textui.TestRunner.run(suite());
  }


}