package com.ftd.novator.giftcert.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * This is value object class which models the Gift response XML 
 * message from Novator. 
 * @author Charles Fox
 */
public class NovatorGiftCertDao 
{
    private Connection dbConnection = null;

    private Logger logger = 
        new Logger("com.ftd.accountingreporting.novator.dao.GiftCertDao");
        
    public NovatorGiftCertDao(Connection conn)
    {
        super();
        dbConnection = conn;
    }

    /**
     *  Call CLEAN.GIFT_CERTIFICATE_COUPON_PKG.GET_GCCREQ_BY_REQNUM_OR_GCCNUM(null, couponNumber)
     * @param couponNumber - String
     * @return GiftCertVO
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     */
    public String getGiftCertRedemptionType(String gccNumber)
        throws IOException, SAXException, SQLException, 
            ParserConfigurationException
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getGiftCertRedemptionType");
            logger.debug("Coupon Number : " + gccNumber);
        }
        
        DataRequest request = new DataRequest();
        String redemptionType = "";
        try {
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_REQUEST_NUMBER", null);
            inputParams.put("IN_GC_COUPON_NUMBER", gccNumber);

            // build DataRequest object
            request.setConnection(dbConnection);
            request.setInputParams(inputParams);
            request.setStatementID("GET_GCCREQ_BY_REQNUM_OR_GCCNUM");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();
            CachedResultSet outputs = (CachedResultSet) dau.execute(request);
            
            while(outputs.next())
            { 
                redemptionType = outputs.getString("order_source");
            }

        } finally {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getGiftCertRedemptionType");
            } 
        } 
        return redemptionType;
    }
}