<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ARAgingReportAll" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="ARAGINGREPORTALL" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_Member_Status" datatype="character" precision="10"
     initialValue="blank" label="P Member Status" defaultWidth="0"
     defaultHeight="0"/>
    <userParameter name="P_Currency_Code" datatype="character" precision="10"
     initialValue="blank" label="P Currency Code" defaultWidth="0"
     defaultHeight="0"/>
    <userParameter name="P_Curr_Amnt_Due" datatype="character" precision="10"
     initialValue="blank" label="P Curr Amnt Due" defaultWidth="0"
     defaultHeight="0"/>
    <userParameter name="P_Report_Title" datatype="character" width="35"
     precision="10" initialValue="blank"
     validationTrigger="p_report_titlevalidtrigger" label="P Report Title"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Month" datatype="character" precision="10"
     label="P Month" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Year" datatype="character" precision="10"
     label="P Year" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Territory" datatype="character" width="5"
     precision="10" initialValue="blank" label="P Territory" defaultWidth="0"
     defaultHeight="0"/>
    <userParameter name="P_REGION" datatype="character" width="5"
     initialValue="blank" label="P Region" defaultWidth="0" defaultHeight="0"
    />
    <userParameter name="P_SEARCH_TYPE" datatype="character" width="3"
     precision="10" label="P Search Type" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_COLLECTION" datatype="character" width="5"
     precision="10" initialValue="blank" label="P Collection" defaultWidth="0"
     defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="number" precision="10"
     label="P Member" defaultWidth="0" defaultHeight="0" display="no"/>
    <dataSource name="Q_AR_AGING">
      <select>
      <![CDATA[SELECT  substr(WW.MAILING_NAME,1,30) as Mailing_Name,
                AB.FIELD_ACCT_REPRESENTATIVE_CODE,
                AB.DIVISION_MANAGER_CODE,
                AA.COLLECTION_MANAGER,
	shop_phone_convert( PB.AREA_CODE || PB.PHONE_NUMBER ) as PHONE_NUMBER,
	AA.ADDRESS_NUMBER,
                AA.Member_Status,
	AD.address_line_1,
	AD.CITY,
                AD.State,
                AD.Postal_Code,
                AA.OPEN_AMOUNT,
                AA.FUTURE_AMOUNT_DUE,
	AA.CURRENT_AMOUNT_DUE,
	AA.AGING_1_AMOUNT_1,
	AA.AGING_1_AMOUNT_2,
	AA.AGING_1_AMOUNT_3,
	AA.AGING_1_AMOUNT_4,
	AA.AGING_1_AMOUNT_5,
	AA.AGING_2_AMOUNT_1,
	AA.AGING_2_AMOUNT_2,
	AA.AGING_2_AMOUNT_3,
	AA.AGING_2_AMOUNT_4,
	AA.AGING_2_AMOUNT_5,
	(AA.AGING_2_AMOUNT_6 + AA.AGING_2_AMOUNT_7) AGING_2_AMOUNT_6_7
FROM
	JDE_AR_AGING_BALANCES_MONTHLY AA, JDE_ADDRESS_BOOK AB, 
	JDE_PHONE_BOOK PB, JDE_ADDRESSES AD, JDE_WHOS_WHO WW
WHERE
                        AA.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
	AND AB.ADDRESS_NUMBER = PB.ADDRESS_NUMBER
	AND AB.ADDRESS_NUMBER = AD.ADDRESS_NUMBER
                AND AB.ADDRESS_NUMBER = WW.ADDRESS_NUMBER
                AND WW.LINE_ID = 0
	AND PB.PHONE_NUMBER_TYPE = 'S'
	AND AA.PARENT_CHILD_RELATIONSHIP <> 'P'
	AND UPPER(:P_Report_Title) <> 'ALL'
                AND ( AA.REPORT_EFFECTIVE_DATE >= TO_DATE(:P_Month ||'/01/'||:P_Year,'MM/DD/YYYY') AND 
                           AA.REPORT_EFFECTIVE_DATE < ADD_MONTHS(TO_DATE(:P_Month || '/01/' || :P_Year,'MM/DD/YYYY'),1) )
	AND (( UPPER(:P_Currency_Code) = 'USD' AND AA.CURRENCY_CODE = UPPER(:P_Currency_Code) 
                AND AA.COMPANY IN ('01200','04500') )
	  OR (UPPER(:P_Currency_Code) = 'CAD'
                AND AA.CURRENCY_CODE = UPPER(:P_Currency_Code) AND AA.COMPANY IN ('00000') ) )
	AND ((UPPER(:P_Curr_Amnt_Due) = 'DEBIT' AND AA.OPEN_AMOUNT > 0 ) 
	  OR  (UPPER(:P_Curr_Amnt_Due) = 'CREDIT' AND AA.OPEN_AMOUNT < 0 )
                  OR  (UPPER(:P_Curr_Amnt_Due) = 'BOTH'  AND AA.OPEN_AMOUNT <> 0 ) )
             	AND AB.SEARCH_TYPE = UPPER(:P_Search_Type)
	AND ((UPPER(:P_Region) = 'ALL' or UPPER (:P_Territory) = 'ALL' or UPPER(:P_Collection) = 'ALL' 
                  OR  ((UPPER(:P_Region) = 'BLANK' or UPPER(:P_Region) = AB.DIVISION_MANAGER_CODE
                  OR  ( UPPER(:P_Region) = 'NA' and AB.DIVISION_MANAGER_CODE = ' ') ) 
	AND   (UPPER(:P_Territory) = 'BLANK' or UPPER(:P_Territory) = AB.FIELD_ACCT_REPRESENTATIVE_CODE
                  OR   (UPPER(:P_Territory) = 'NA' and AB.FIELD_ACCT_REPRESENTATIVE_CODE = ' ') )
                AND   (UPPER(:P_Collection) = 'BLANK' or UPPER(:P_Collection) = AA.COLLECTION_MANAGER))))
              	AND ((UPPER(:P_Member_Status) = 'ACTIVE' AND AA.MEMBER_STATUS = '   ')
	  OR  (UPPER(:P_Member_Status) = 'NONACTIVE' AND AA.MEMBER_STATUS IN ('N', 'D', 'S', 'C', 'F', 'V'))
                  OR  (UPPER(:P_Member_Status) = 'ALL'  )
	    )
	order by AA.OPEN_AMOUNT desc
]]>
      </select>
      <displayInfo x="1.22461" y="0.07654" width="1.48535" height="0.19995"/>
      <group name="G_AR_AGING">
        <displayInfo x="0.97974" y="0.72632" width="1.97791" height="4.87402"
        />
        <dataItem name="address_number" oracleDatatype="number"
         columnOrder="22" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1" defaultLabel="Address Number">
          <dataDescriptor expression="AA.ADDRESS_NUMBER"
           descriptiveExpression="ADDRESS_NUMBER" order="6"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="Member_Status" datatype="vchar2" columnOrder="38"
         width="3" defaultWidth="30000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="AA.Member_Status"
           descriptiveExpression="MEMBER_STATUS" order="7" width="3"/>
        </dataItem>
        <dataItem name="CITY" datatype="vchar2" columnOrder="41" width="25"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="City" breakOrder="none">
          <dataDescriptor expression="AD.CITY" descriptiveExpression="CITY"
           order="9" width="25"/>
        </dataItem>
        <dataItem name="State" datatype="character"
         oracleDatatype="aFixedChar" columnOrder="42" width="3"
         defaultWidth="30000" defaultHeight="10000" columnFlags="0"
         defaultLabel="State" breakOrder="none">
          <dataDescriptor expression="AD.State" descriptiveExpression="STATE"
           order="10" oracleDatatype="aFixedChar" width="3"/>
        </dataItem>
        <dataItem name="MAILING_NAME" datatype="vchar2" columnOrder="37"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Mailing Name" breakOrder="none">
          <dataDescriptor expression="substr ( WW.MAILING_NAME , 1 , 30 )"
           descriptiveExpression="MAILING_NAME" order="1" width="30"/>
        </dataItem>
        <dataItem name="DIVISION_MANAGER_CODE" datatype="vchar2"
         columnOrder="45" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Division Manager Code"
         breakOrder="none">
          <dataDescriptor expression="AB.DIVISION_MANAGER_CODE"
           descriptiveExpression="DIVISION_MANAGER_CODE" order="3" width="3"/>
        </dataItem>
        <dataItem name="FIELD_ACCT_REPRESENTATIVE_CODE" datatype="vchar2"
         columnOrder="44" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Field Acct Representative Code"
         breakOrder="none">
          <dataDescriptor expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE"
           descriptiveExpression="FIELD_ACCT_REPRESENTATIVE_CODE" order="2"
           width="3"/>
        </dataItem>
        <dataItem name="COLLECTION_MANAGER" datatype="vchar2" columnOrder="46"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Collection Manager" breakOrder="none">
          <dataDescriptor expression="AA.COLLECTION_MANAGER"
           descriptiveExpression="COLLECTION_MANAGER" order="4" width="10"/>
        </dataItem>
        <dataItem name="Postal_Code" datatype="vchar2" columnOrder="43"
         width="12" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Postal Code" breakOrder="none">
          <dataDescriptor expression="AD.Postal_Code"
           descriptiveExpression="POSTAL_CODE" order="11" width="12"/>
        </dataItem>
        <dataItem name="address_line_1" datatype="vchar2" columnOrder="23"
         width="40" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Address Line 1" breakOrder="none">
          <dataDescriptor expression="AD.address_line_1"
           descriptiveExpression="ADDRESS_LINE_1" order="8" width="40"/>
        </dataItem>
        <dataItem name="PHONE_NUMBER" datatype="vchar2" columnOrder="34"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Phone Number" breakOrder="none">
          <dataDescriptor
           expression="shop_phone_convert ( PB.AREA_CODE || PB.PHONE_NUMBER )"
           descriptiveExpression="PHONE_NUMBER" order="5" width="4000"/>
        </dataItem>
        <dataItem name="OPEN_AMOUNT" oracleDatatype="number" columnOrder="40"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Open Amount" breakOrder="none">
          <dataDescriptor expression="AA.OPEN_AMOUNT"
           descriptiveExpression="OPEN_AMOUNT" order="12"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="FUTURE_AMOUNT_DUE" oracleDatatype="number"
         columnOrder="39" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Future Amount Due" breakOrder="none">
          <dataDescriptor expression="AA.FUTURE_AMOUNT_DUE"
           descriptiveExpression="FUTURE_AMOUNT_DUE" order="13"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="CURRENT_AMOUNT_DUE" oracleDatatype="number"
         columnOrder="24" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Current Amount Due" breakOrder="none">
          <dataDescriptor expression="AA.CURRENT_AMOUNT_DUE"
           descriptiveExpression="CURRENT_AMOUNT_DUE" order="14"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_1_AMOUNT_1" oracleDatatype="number"
         columnOrder="25" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 1 Amount 1"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_1_AMOUNT_1"
           descriptiveExpression="AGING_1_AMOUNT_1" order="15"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_1_AMOUNT_2" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 1 Amount 2"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_1_AMOUNT_2"
           descriptiveExpression="AGING_1_AMOUNT_2" order="16"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_1_AMOUNT_3" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 1 Amount 3"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_1_AMOUNT_3"
           descriptiveExpression="AGING_1_AMOUNT_3" order="17"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_1_AMOUNT_4" oracleDatatype="number"
         columnOrder="28" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 1 Amount 4"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_1_AMOUNT_4"
           descriptiveExpression="AGING_1_AMOUNT_4" order="18"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_1_AMOUNT_5" oracleDatatype="number"
         columnOrder="29" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 1 Amount 5"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_1_AMOUNT_5"
           descriptiveExpression="AGING_1_AMOUNT_5" order="19"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_1" oracleDatatype="number"
         columnOrder="35" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 2 Amount 1"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_2_AMOUNT_1"
           descriptiveExpression="AGING_2_AMOUNT_1" order="20"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_2" oracleDatatype="number"
         columnOrder="30" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 2 Amount 2"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_2_AMOUNT_2"
           descriptiveExpression="AGING_2_AMOUNT_2" order="21"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_3" oracleDatatype="number"
         columnOrder="31" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 2 Amount 3"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_2_AMOUNT_3"
           descriptiveExpression="AGING_2_AMOUNT_3" order="22"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_4" oracleDatatype="number"
         columnOrder="32" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 2 Amount 4"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_2_AMOUNT_4"
           descriptiveExpression="AGING_2_AMOUNT_4" order="23"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_5" oracleDatatype="number"
         columnOrder="33" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging 2 Amount 5"
         breakOrder="none">
          <dataDescriptor expression="AA.AGING_2_AMOUNT_5"
           descriptiveExpression="AGING_2_AMOUNT_5" order="24"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_2_AMOUNT_6_7" oracleDatatype="number"
         columnOrder="36" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Aging 2 Amount 6 7" breakOrder="none">
          <dataDescriptor
           expression="( AA.AGING_2_AMOUNT_6 + AA.AGING_2_AMOUNT_7 )"
           descriptiveExpression="AGING_2_AMOUNT_6_7" order="25"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <formula name="CF_TRC" source="cf_trcformula" datatype="character"
         width="20" precision="10" defaultWidth="0" defaultHeight="0"
         columnFlags="16" breakOrder="none">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </formula>
      </group>
    </dataSource>
    <summary name="CS_Aging1Amt2" source="AGING_1_AMOUNT_2" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging1amt2">
      <displayInfo x="3.34546" y="1.72913" width="1.10413" height="0.23962"/>
    </summary>
    <summary name="CS_Aging1Amt1" source="AGING_1_AMOUNT_1" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging1amt1">
      <displayInfo x="3.35596" y="1.32288" width="1.07288" height="0.22925"/>
    </summary>
    <summary name="CS_Aging2Amt1" source="AGING_2_AMOUNT_1" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt1">
      <displayInfo x="3.36548" y="3.17712" width="1.10413" height="0.21875"/>
    </summary>
    <summary name="CS_Aging1Amt3" source="AGING_1_AMOUNT_3" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging1amt3">
      <displayInfo x="3.36633" y="2.05212" width="1.08325" height="0.22913"/>
    </summary>
    <summary name="CS_Aging1Amt4" source="AGING_1_AMOUNT_4" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging1amt4">
      <displayInfo x="3.36633" y="2.44788" width="1.11462" height="0.23962"/>
    </summary>
    <summary name="CS_Aging1Amt5" source="AGING_1_AMOUNT_5" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging1amt5">
      <displayInfo x="3.36633" y="2.78125" width="1.11462" height="0.22913"/>
    </summary>
    <summary name="CS_Aging2Amt2" source="AGING_2_AMOUNT_2" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt2">
      <displayInfo x="3.37598" y="3.48950" width="1.10425" height="0.25000"/>
    </summary>
    <summary name="CS_Balance_Total" source="OPEN_AMOUNT" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Balance Total">
      <displayInfo x="3.37671" y="0.08325" width="1.40625" height="0.25012"/>
    </summary>
    <summary name="CS_Current_amt_Due_Bal" source="CURRENT_AMOUNT_DUE"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Current Amt Due Bal">
      <displayInfo x="3.37671" y="0.51038" width="1.63538" height="0.22925"/>
    </summary>
    <summary name="CS_Future_Amt_Due_Bal" source="FUTURE_AMOUNT_DUE"
     function="sum" width="22" precision="15" scale="2" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Future Amt Due Bal">
      <displayInfo x="3.37671" y="0.95825" width="1.63538" height="0.21875"/>
    </summary>
    <summary name="CS_Aging2Amt3" source="AGING_2_AMOUNT_3" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt3">
      <displayInfo x="3.40735" y="3.84387" width="1.08325" height="0.25195"/>
    </summary>
    <summary name="CS_Aging2Amt5" source="AGING_2_AMOUNT_5" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt5">
      <displayInfo x="4.51099" y="1.71887" width="1.05151" height="0.26038"/>
    </summary>
    <summary name="CS_Aging2Amt4" source="AGING_2_AMOUNT_4" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt4">
      <displayInfo x="4.52185" y="1.33337" width="1.01978" height="0.23950"/>
    </summary>
    <summary name="CS_Aging2Amt67" source="AGING_2_AMOUNT_6_7" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Aging2amt67">
      <displayInfo x="4.54187" y="2.09363" width="1.01038" height="0.26038"/>
    </summary>
    <formula name="CF_TITLE" source="cf_titleformula" datatype="character"
     width="100" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="5.55310" y="0.04175" width="1.23950" height="0.20825"/>
    </formula>
    <formula name="CF_Title_2" source="cf_title_2formula0017"
     datatype="character" width="60" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.57288" y="0.37500" width="1.21875" height="0.19995"/>
    </formula>
    <formula name="CF_Title_3" source="cf_title_2formula" datatype="character"
     width="100" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="5.59460" y="0.71863" width="1.19702" height="0.20837"/>
    </formula>
    <formula name="CF_trc_hg" source="cf_trc_hgformula" datatype="character"
     precision="10" defaultWidth="0" defaultHeight="0" columnFlags="16"
     breakOrder="none">
      <displayInfo x="5.61462" y="1.02100" width="1.16663" height="0.19995"/>
    </formula>
    <formula name="CF_No_Data_Msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.63672" y="1.36450" width="1.16675" height="0.20837"/>
    </formula>
    <summary name="CS_Record_Count" source="address_number" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="5.68884" y="1.77087" width="1.17700" height="0.20837"/>
    </summary>
  </data>
  <layout>
  <section name="header" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.00000" height="6.50000"/>
  </section>
  <section name="trailer" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.00000" height="6.50000"/>
  </section>
  <section name="main" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.93750" height="7.18750">
      <location x="0.00000" y="0.75000"/>
      <frame name="M_G_address_number_GRPFR">
        <geometryInfo x="0.00000" y="0.00000" width="13.87500"
         height="2.50000"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_address_number" source="G_AR_AGING"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.25000" width="13.81250"
           height="0.81250"/>
          <generalLayout pageProtect="yes" verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_address_number" source="address_number"
           alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="0.00000" y="0.31250" width="0.43750"
             height="0.12500"/>
          </field>
          <field name="F_Member_Status" source="Member_Status"
           spacing="single" alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="0.50000" y="0.31250" width="0.18750"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_ALPHA_NAME" source="MAILING_NAME" alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="0.75000" y="0.31250" width="1.87500"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="Balance" source="OPEN_AMOUNT"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="3.25000" y="0.31250" width="0.81250"
             height="0.18750"/>
          </field>
          <field name="F_Future_Amount_Due" source="FUTURE_AMOUNT_DUE"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="4.13892" y="0.31250" width="0.73608"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="Current_Balance" source="CURRENT_AMOUNT_DUE"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="4.95142" y="0.31250" width="0.86108"
             height="0.18750"/>
          </field>
          <field name="F_AGING_1_AMOUNT_1" source="AGING_1_AMOUNT_1"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="5.87500" y="0.31250" width="0.87500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_1_AMOUNT_2" source="AGING_1_AMOUNT_2"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="6.81250" y="0.31250" width="0.75000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_1_AMOUNT_3" source="AGING_1_AMOUNT_3"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="7.62500" y="0.31250" width="0.68750"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_1_AMOUNT_4" source="AGING_1_AMOUNT_4"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="8.37500" y="0.31250" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_1_AMOUNT_5" source="AGING_1_AMOUNT_5"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="9.06250" y="0.31250" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_1" source="AGING_2_AMOUNT_1"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="9.75000" y="0.31250" width="0.68750"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_2" source="AGING_2_AMOUNT_2"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="10.50000" y="0.31250" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_3" source="AGING_2_AMOUNT_3"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="11.18750" y="0.31250" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_4" source="AGING_2_AMOUNT_4"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="11.87500" y="0.31250" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_5" source="AGING_2_AMOUNT_5"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="12.56250" y="0.31250" width="0.56250"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_AGING_2_AMOUNT_7" source="AGING_2_AMOUNT_6_7"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="13.25000" y="0.31250" width="0.50000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_address_line_1" source="address_line_1"
           alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="0.75000" y="0.43750" width="1.87500"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_PHONE_NUMBER" source="PHONE_NUMBER" alignment="start"
            >
            <font face="Arial" size="6"/>
            <geometryInfo x="0.75000" y="0.68750" width="0.81250"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <field name="F_City" source="CITY" alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="0.75000" y="0.56250" width="1.06250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_State" source="State" alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="1.87500" y="0.56250" width="0.18750"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Zip" source="Postal_Code" alignment="start">
            <font face="Arial" size="6"/>
            <geometryInfo x="2.12500" y="0.56250" width="0.43750"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_TRC" source="CF_TRC" alignment="center">
            <font face="Arial" size="6"/>
            <geometryInfo x="2.75000" y="0.31250" width="0.31250"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"
             formatTrigger="f_trcformattrigger"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <frame name="M_Grand_Totals">
          <geometryInfo x="0.06250" y="1.12500" width="13.81250"
           height="0.43750"/>
          <advancedLayout formatTrigger="m_grand_totalsformattrigger"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_1">
            <geometryInfo x="0.18750" y="1.31250" width="0.40625"
             height="0.16663"/>
            <advancedLayout formatTrigger="b_1formattrigger"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Totals:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_BALANCE_DUE" source="CS_Balance_Total"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="3.18750" y="1.31250" width="0.87500"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_FUTURE_AMT_BALANCE" source="CS_Future_Amt_Due_Bal"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="4.12500" y="1.31250" width="0.75000"
             height="0.18750"/>
          </field>
          <field name="F_Current_Amt_Due_Bal" source="CS_Current_amt_Due_Bal"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="4.93750" y="1.31250" width="0.87500"
             height="0.18750"/>
          </field>
          <field name="F_30days" source="CS_Aging1Amt1"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="5.87500" y="1.31250" width="0.87500"
             height="0.18750"/>
          </field>
          <field name="F_60days" source="CS_Aging1Amt2"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="6.81250" y="1.31250" width="0.75000"
             height="0.18750"/>
          </field>
          <field name="F_90days" source="CS_Aging1Amt3"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="7.62500" y="1.31250" width="0.68750"
             height="0.18750"/>
          </field>
          <field name="F_120days" source="CS_Aging1Amt4"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="8.37500" y="1.31250" width="0.62500"
             height="0.18750"/>
          </field>
          <field name="F_150days" source="CS_Aging1Amt5"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="9.06250" y="1.31250" width="0.62500"
             height="0.18750"/>
          </field>
          <field name="F_364days" source="CS_Aging2Amt1"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="9.75000" y="1.31250" width="0.68750"
             height="0.18750"/>
          </field>
          <field name="F_760days" source="CS_Aging2Amt2"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="10.50000" y="1.31250" width="0.62500"
             height="0.18750"/>
          </field>
          <field name="F_1095days" source="CS_Aging2Amt3"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="11.18750" y="1.31250" width="0.62500"
             height="0.18750"/>
          </field>
          <field name="F_1460days" source="CS_Aging2Amt4"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="11.87500" y="1.31250" width="0.62500"
             height="0.18750"/>
          </field>
          <field name="F_1825days" source="CS_Aging2Amt5"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="12.56250" y="1.31250" width="0.56250"
             height="0.18750"/>
          </field>
          <field name="F_1825plusdays" source="CS_Aging2Amt67"
           formatMask="LNNNGNNNGNNNGNN0D00" alignment="end">
            <font face="Arial" size="5"/>
            <geometryInfo x="13.25000" y="1.31250" width="0.50000"
             height="0.18750"/>
          </field>
          <line name="B_7" arrow="none">
            <geometryInfo x="3.12500" y="1.26038" width="10.75000"
             height="0.00000"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             fillBackgroundColor="gray" linePattern="solid"/>
            <points>
              <point x="3.12500" y="1.26038"/>
              <point x="13.87500" y="1.26038"/>
            </points>
          </line>
          <field name="F_Member_count" source="CS_Record_Count"
           formatMask="NNN,NN0" alignment="center">
            <font face="Arial" size="5"/>
            <geometryInfo x="2.18750" y="1.31250" width="0.56250"
             height="0.18750"/>
          </field>
        </frame>
        <text name="B_9">
          <textSettings justify="center"/>
          <geometryInfo x="5.00000" y="1.68750" width="3.56250"
           height="0.31250"/>
          <advancedLayout formatTrigger="b_9formattrigger"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[&<CF_No_Data_Msg>
]]>
            </string>
          </textSegment>
        </text>
        <text name="B_11">
          <textSettings justify="center"/>
          <geometryInfo x="6.37500" y="2.12500" width="1.10413"
           height="0.31250"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[End of Report
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[V1.2]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_G_address_number_HDR">
          <geometryInfo x="0.00000" y="0.00000" width="13.87500"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="anchoringObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_AGING_1_AMOUNT_1">
            <textSettings justify="end"/>
            <geometryInfo x="5.87500" y="0.06250" width="0.87500"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[1-30]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_2">
            <textSettings justify="end"/>
            <geometryInfo x="6.81250" y="0.06250" width="0.75000"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[31-60]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_3">
            <textSettings justify="end"/>
            <geometryInfo x="7.62500" y="0.06250" width="0.68750"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[61-90]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_4">
            <textSettings justify="center"/>
            <geometryInfo x="8.62500" y="0.06250" width="0.43750"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[91-120]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_5">
            <textSettings justify="center"/>
            <geometryInfo x="9.25000" y="0.06250" width="0.50000"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[121-150]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_6">
            <textSettings justify="center"/>
            <geometryInfo x="10.00000" y="0.06250" width="0.56250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[151-364]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_1_AMOUNT_7">
            <textSettings justify="center"/>
            <geometryInfo x="10.68750" y="0.06250" width="0.56250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[365-780]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_2_AMOUNT_2">
            <textSettings justify="center"/>
            <geometryInfo x="11.37500" y="0.06250" width="0.56250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[881-1095]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_2_AMOUNT_3">
            <textSettings justify="center"/>
            <geometryInfo x="12.06250" y="0.06250" width="0.56250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[1096-1460]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_2_AMOUNT_4">
            <textSettings justify="center"/>
            <geometryInfo x="12.68750" y="0.06250" width="0.56250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[1461-1825]]>
              </string>
            </textSegment>
          </text>
          <text name="B_AGING_2_AMOUNT_5">
            <textSettings justify="center"/>
            <geometryInfo x="13.37500" y="0.06250" width="0.50000"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[1825+]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Balance_Amount_Due">
            <textSettings justify="end"/>
            <geometryInfo x="3.25000" y="0.06250" width="0.81250"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Balance]]>
              </string>
            </textSegment>
          </text>
          <line name="B_6" arrow="none"
           stretchWithFrame="M_G_address_number_HDR">
            <geometryInfo x="3.12500" y="0.18750" width="10.75000"
             height="0.00000"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             linePattern="solid"/>
            <points>
              <point x="3.12500" y="0.18750"/>
              <point x="13.87500" y="0.18750"/>
            </points>
          </line>
          <text name="B_4">
            <geometryInfo x="0.00000" y="0.06250" width="0.43750"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="darkgreen"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Member]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <geometryInfo x="1.00000" y="0.06250" width="1.00000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="darkgreen"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Name/Address]]>
              </string>
            </textSegment>
          </text>
          <text name="B_5">
            <geometryInfo x="0.50000" y="0.06250" width="0.31250"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="r100g25b0"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Status]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Current_Amount_Due">
            <textSettings justify="end"/>
            <geometryInfo x="4.93750" y="0.06250" width="0.87500"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Current]]>
              </string>
            </textSegment>
          </text>
          <text name="B_Future_Amount_Due">
            <textSettings justify="end"/>
            <geometryInfo x="4.12500" y="0.06250" width="0.75000"
             height="0.12500"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Future]]>
              </string>
            </textSegment>
          </text>
          <line name="B_8" arrow="none">
            <geometryInfo x="0.00000" y="0.18750" width="3.12524"
             height="0.00000"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="anchoringObject"/>
            <visualSettings lineWidth="1" linePattern="transparent"
             lineBackgroundColor="black"/>
            <points>
              <point x="0.00000" y="0.18750"/>
              <point x="3.12524" y="0.18750"/>
            </points>
          </line>
          <text name="B_12">
            <textSettings justify="center"/>
            <geometryInfo x="2.62500" y="0.06250" width="0.50000"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[&<cf_trc_hg>]]>
              </string>
            </textSegment>
          </text>
        </frame>
      </frame>
    </body>
    <margin>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Arial" size="8"/>
        <geometryInfo x="0.56250" y="0.00000" width="0.83337" height="0.18750"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_10">
        <textSettings justify="center"/>
        <geometryInfo x="4.75000" y="0.50000" width="4.50000" height="0.16663"
        />
        <textSegment>
          <font face="Arial" size="8"/>
          <string>
          <![CDATA[&<CF_Title_3>]]>
          </string>
        </textSegment>
      </text>
      <text name="B_2">
        <textSettings justify="center"/>
        <geometryInfo x="4.75000" y="0.06250" width="4.50000" height="0.18750"
        />
        <visualSettings fillBackgroundColor="green"/>
        <textSegment>
          <font face="Arial" size="8"/>
          <string>
          <![CDATA[&<CF_TITLE>]]>
          </string>
        </textSegment>
      </text>
      <text name="B_14">
        <textSettings justify="center"/>
        <geometryInfo x="4.75000" y="0.25000" width="4.50000" height="0.14587"
        />
        <textSegment>
          <font face="Arial" size="8"/>
          <string>
          <![CDATA[&<CF_Title_2>]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <parameterForm height="12.00000">
  <text name="PB_1" pageNumber="1">
    <geometryInfo x="1.20142" y="0.15979" width="1.62476" height="0.22205"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="12" bold="yes"/>
      <string>
      <![CDATA[Report Parameters]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_2" pageNumber="1">
    <geometryInfo x="0.86816" y="0.49304" width="2.29138" height="0.18042"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="10" bold="yes"/>
      <string>
      <![CDATA[Enter values for the parameters]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Member_Status" pageNumber="1">
    <geometryInfo x="0.24316" y="0.82629" width="1.04138" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Member Status]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Currency_Code" pageNumber="1">
    <geometryInfo x="0.24316" y="1.15955" width="1.03101" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Currency Code]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Curr_Amnt_Due" pageNumber="1">
    <geometryInfo x="0.24316" y="1.49280" width="1.02051" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Curr Amnt Due]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Report_Title" pageNumber="1">
    <geometryInfo x="0.24316" y="1.82605" width="0.86426" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Report Title]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Month" pageNumber="1">
    <geometryInfo x="0.24316" y="2.15930" width="0.53101" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Month]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Year" pageNumber="1">
    <geometryInfo x="0.24316" y="2.49255" width="0.43726" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Year]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_Territory" pageNumber="1">
    <geometryInfo x="0.24316" y="2.82581" width="0.65601" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Territory]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_REGION" pageNumber="1">
    <geometryInfo x="0.24316" y="3.15906" width="0.58301" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Region]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_SEARCH_TYPE" pageNumber="1">
    <geometryInfo x="0.24316" y="3.49231" width="0.91638" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Search Type]]>
      </string>
    </textSegment>
  </text>
  <text name="PB_P_COLLECTION" pageNumber="1">
    <geometryInfo x="0.24316" y="3.82556" width="0.74976" height="0.14929"/>
    <visualSettings lineWidth="1"/>
    <textSegment>
      <font face="MS Sans Serif" size="8" bold="yes"/>
      <string>
      <![CDATA[P Collection]]>
      </string>
    </textSegment>
  </text>
  <field name="PF_P_Member_Status" source="P_Member_Status" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="0.78430" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Currency_Code" source="P_Currency_Code" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="1.11755" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Curr_Amnt_Due" source="P_Curr_Amnt_Due" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="1.45081" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Report_Title" source="P_Report_Title" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="1.78406" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Month" source="P_Month" pageNumber="1" alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="2.11731" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Year" source="P_Year" pageNumber="1" alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="2.45056" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_Territory" source="P_Territory" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="2.78381" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_REGION" source="P_REGION" pageNumber="1" alignment="start"
    >
    <font size="0"/>
    <geometryInfo x="1.50000" y="3.11707" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_SEARCH_TYPE" source="P_SEARCH_TYPE" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="3.45032" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  <field name="PF_P_COLLECTION" source="P_COLLECTION" pageNumber="1"
   alignment="start">
    <font size="0"/>
    <geometryInfo x="1.50000" y="3.78357" width="2.00000" height="0.26428"/>
    <visualSettings lineWidth="1" linePattern="solid"/>
  </field>
  </parameterForm>
  <programUnits>
    <function name="p_report_titlevalidtrigger">
      <textSource>
      <![CDATA[function P_Report_TitleValidTrigger return boolean is
begin
  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="formattitle">
      <textSource>
      <![CDATA[FUNCTION FormatTitle RETURN character IS
BEGIN
  return(:P_Report_Title + ' Aging Report');
END;]]>
      </textSource>
    </function>
    <function name="cf_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TITLEFormula return Char is
 lv_title varchar2(80);
 lv_date varchar2(10);
begin
   lv_date := (:P_Month ||'/'|| :P_Year);
	 IF :P_Report_Title = 'FTD_Member' then
	   If :P_Curr_Amnt_Due = 'Debit' then
    	 lv_title := ('FTD MEMBER DEBIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	     :P_Curr_Amnt_Due = 'Credit' then
		    lv_title := ('FTD MEMBER CREDIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	     :P_Curr_Amnt_Due = 'Both' then
		    lv_title := ('FTD MEMBER AGING REPORT FOR'||' '|| lv_date);
	   end if;
   end if;
     
   IF :P_Report_Title = 'All_Members' then
	   If :P_Curr_Amnt_Due = 'Debit' then
    	 lv_title := ('ALL MEMBERS DEBIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	     :P_Curr_Amnt_Due = 'Credit' then
		    lv_title := ('ALL MEMBERS CREDIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	     :P_Curr_Amnt_Due = 'Both' then
		    lv_title := ('ALL MEMBERS AGING REPORT FOR'||' '|| lv_date);
	   end if;
   end if;
   
   If :P_Report_Title = 'FTD_Non-Active_Member' then
	   If :P_Curr_Amnt_Due = 'Debit' then
         lv_title := ('FTD NON-ACTIVE MEMBER DEBIT AGING REPORT FOR'||' '|| lv_date);
     elsif
        :P_Curr_Amnt_Due = 'Credit' then
           lv_title := ('FTD NON-ACTIVE MEMBER CREDIT AGING REPORT FOR'||' '|| lv_date);
     elsif
	      :P_Curr_Amnt_Due = 'Both' then
           lv_title := ('FTD NON-ACTIVE MEMBER AGING REPORT FOR'||' '|| lv_date);
     end if;   
   end if;
	 
	 If :P_Report_Title = 'VNS_Aging' then
	   If :P_Curr_Amnt_Due = 'Debit' then
	     lv_title := ('VNS DEBIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	     :P_Curr_Amnt_Due = 'Credit' then
	        lv_title := ('VNS CREDIT AGING REPORT FOR'||' '|| lv_date);
	   elsif
	 	 :P_Curr_Amnt_Due = 'Both' then
	        lv_title := ('VNS AGING REPORT FOR'||' '|| lv_date);
	   end if;   
   end if;
	          
   IF :P_Report_Title = 'Other_Aging' then
	   If :P_Curr_Amnt_Due = 'Debit' then
       	 lv_title := ('OTHER DEBIT AGING REPORT FOR'||' '|| lv_date);
  	   elsif	
         :P_Curr_Amnt_Due = 'Credit' then
      	    lv_title := ('OTHER CREDIT AGING REPORT FOR'||' '|| lv_date);
  	   elsif  
         :P_Curr_Amnt_Due = 'Both' then
      	    lv_title := ('OTHER AGING REPORT FOR'||' '|| lv_date);
  	   end if; 
     end if;	 
 RETURN (lv_title);
END;
]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_MsgFormula return Char is
 lv_Message varchar2(80);
begin
  if :cs_record_count = 0 then
   lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;
  return lv_message;
end;]]>
      </textSource>
    </function>
    <function name="b_9formattrigger">
      <textSource>
      <![CDATA[function B_9FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count != '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_1formattrigger">
      <textSource>
      <![CDATA[function B_1FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="m_grand_totalsformattrigger">
      <textSource>
      <![CDATA[function M_Grand_TotalsFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_title_2formula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_2Formula return Char is
  lv_title3 varchar2 (80) null;
  temp_title varchar2(80);
begin
 If :p_territory = 'All' then
 	 lv_title3 := 'All Territories';
 elsif :p_region = 'All' then
 	 lv_title3 := 'All Regions';
 elsif :p_collection = 'All' then
 	 lv_title3 := 'All Collections';
 elsif :p_territory != 'NA' and :p_region != 'NA' then
 	 temp_title := title_name1 (:p_territory, :p_region, :p_collection);
 end if;
 
 If :p_territory != 'blank' and :p_territory != 'NA ' and :p_territory != 'All' then
 	 lv_title3 := 'Territory # '||:p_territory ||' - '|| temp_title;
 elsif :p_region != 'blank' and :p_region != 'NA' and :p_region != 'All' then
   lv_title3 := 'Region # ' ||:p_region|| ' - ' ||temp_title;
 elsif :p_collection != 'blank' and :p_collection != 'All' then
   lv_title3 := 'Collection # ' ||:p_collection|| ' - ' ||temp_title;
 end if;
 
 If :p_territory = 'NA' then
 	 lv_title3 := 'Not Assigned Territories';
 elsif :p_region = 'NA' then
 	 lv_title3 := 'Not Assigned Regions';
 end if;
       
 return lv_title3;
end;
]]>
      </textSource>
    </function>
    <function name="f_trcformattrigger">
      <textSource>
      <![CDATA[function F_TRCFormatTrigger return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_trcformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TRCFormula return Char is
    lv_terr_reg_coll varchar2(10) null;
begin
   if :p_territory = 'All' then
   	 lv_terr_reg_coll := :field_acct_representative_code;
   elsif :p_region = 'All' then
   	 lv_terr_reg_coll := :division_manager_code;
   elsif :p_collection = 'All' then
   	 lv_terr_reg_coll := :collection_manager;
   end if;
return (lv_terr_reg_coll);   
end;]]>
      </textSource>
    </function>
    <function name="cf_trc_hgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_trc_hgFormula return Char is
   lv_terr_reg_coll_hdg varchar2(10) null;
begin
   if :p_territory ='All' then
   	 lv_terr_reg_coll_hdg := 'Territory';
   end if;	 
   if :p_region = 'All' then
   	 lv_terr_reg_coll_hdg := 'Region';
   end if;	    
   If :p_collection = 'All' then
   	 lv_terr_reg_coll_hdg := 'Collection';
   end if;
return (lv_terr_reg_coll_hdg);   

end;]]>
      </textSource>
    </function>
    <function name="cf_title_2formula0017" returnType="character">
      <textSource>
      <![CDATA[function CF_TITLE_2FORMULA0017 return Char is
  lv_title2 varchar2 (80);
begin
 If :p_currency_code = 'USD' then
 	 lv_title2 := 'US Dollars';
 else
 	 lv_title2 := 'Canadian Dollars';
 end if; 

 return lv_title2;
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate defaultReportType="tabular" templateName="rwbeige"
   sectionTitle="Aging Report"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>



</head>


<body>

<!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MGaddressnumberGRPFR">
<table>
<caption> Aging Report </caption>
 <!-- Header -->
 <thead>
  <tr>
   <th <rw:id id="HBaddressnumber" asArray="no"/>> Address Number </th>
   <th <rw:id id="HBaddressline1" asArray="no"/>> Address Line 1 </th>
   <th <rw:id id="HBCITY" asArray="no"/>> City </th>
   <th <rw:id id="HBSTATE" asArray="no"/>> State </th>
   <th <rw:id id="HBCOUNTRY" asArray="no"/>> Country </th>
   <th <rw:id id="HBPOSTALCODE" asArray="no"/>> Postal Code </th>
   <th <rw:id id="HBCURRENTAMOUNTDUE" asArray="no"/>> Current Amount Due </th>
   <th <rw:id id="HBAGING1AMOUNT1" asArray="no"/>> Aging 1 Amount 1 </th>
   <th <rw:id id="HBAGING1AMOUNT2" asArray="no"/>> Aging 1 Amount 2 </th>
   <th <rw:id id="HBAGING1AMOUNT3" asArray="no"/>> Aging 1
Amount 3 </th>
   <th <rw:id id="HBAGING1AMOUNT4" asArray="no"/>> Aging 1
Amount 4 </th>
   <th <rw:id id="HBAGING1AMOUNT5" asArray="no"/>> Aging 1
Amount 5 </th>
   <th <rw:id id="HBAGING1AMOUNT6" asArray="no"/>> Aging 1
Amount 6 </th>
   <th <rw:id id="HBAGING1AMOUNT7" asArray="no"/>> Aging 1
Amount 7 </th>
   <th <rw:id id="HBAGING2AMOUNT2" asArray="no"/>> Aging 2
Amount 2 </th>
   <th <rw:id id="HBAGING2AMOUNT3" asArray="no"/>> Aging 2
Amount 3 </th>
   <th <rw:id id="HBAGING2AMOUNT4" asArray="no"/>> Aging 2
Amount 4 </th>
   <th <rw:id id="HBAGING2AMOUNT5" asArray="no"/>> Aging 2
Amount 5 </th>
   <th <rw:id id="HBAGING2AMOUNT6" asArray="no"/>> Aging 2
Amount 6 </th>
   <th <rw:id id="HBAGING2AMOUNT7" asArray="no"/>> Aging 2
Amount 7 </th>
   <th <rw:id id="HBPHONENUMBER" asArray="no"/>> Phone
Number </th>
  </tr>
 </thead>
 <!-- Body -->
 <tbody>
  <rw:foreach id="R_G_address_number_1" src="G_address_number">
   <tr>
    <td <rw:headers id="HFaddressnumber" src="HBaddressnumber"/>><rw:field id="F_address_number" src="address_number" nullValue="&nbsp;"> F_address_number </rw:field></td>
    <td <rw:headers id="HFaddressline1" src="HBaddressline1"/>><rw:field id="F_address_line_1" src="address_line_1" nullValue="&nbsp;"> F_address_line_1 </rw:field></td>
    <td <rw:headers id="HFCITY" src="HBCITY"/>><rw:field id="F_CITY" src="CITY" nullValue="&nbsp;"> F_CITY </rw:field></td>
    <td <rw:headers id="HFSTATE" src="HBSTATE"/>><rw:field id="F_STATE" src="STATE" nullValue="&nbsp;"> F_STATE </rw:field></td>
    <td <rw:headers id="HFCOUNTRY" src="HBCOUNTRY"/>><rw:field id="F_COUNTRY" src="COUNTRY" nullValue="&nbsp;"> F_COUNTRY </rw:field></td>
    <td <rw:headers id="HFPOSTALCODE" src="HBPOSTALCODE"/>><rw:field id="F_POSTAL_CODE" src="POSTAL_CODE" nullValue="&nbsp;"> F_POSTAL_CODE </rw:field></td>
    <td <rw:headers id="HFCURRENTAMOUNTDUE" src="HBCURRENTAMOUNTDUE"/>><rw:field id="F_CURRENT_AMOUNT_DUE" src="CURRENT_AMOUNT_DUE" nullValue="&nbsp;"> F_CURRENT_AMOUNT_DUE </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT1" src="HBAGING1AMOUNT1"/>><rw:field id="F_AGING_1_AMOUNT_1" src="AGING_1_AMOUNT_1" nullValue="&nbsp;"> F_AGING_1_AMOUNT_1 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT2" src="HBAGING1AMOUNT2"/>><rw:field id="F_AGING_1_AMOUNT_2" src="AGING_1_AMOUNT_2" nullValue="&nbsp;"> F_AGING_1_AMOUNT_2 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT3" src="HBAGING1AMOUNT3"/>><rw:field id="F_AGING_1_AMOUNT_3" src="AGING_1_AMOUNT_3" nullValue="&nbsp;"> F_AGING_1_AMOUNT_3 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT4" src="HBAGING1AMOUNT4"/>><rw:field id="F_AGING_1_AMOUNT_4" src="AGING_1_AMOUNT_4" nullValue="&nbsp;"> F_AGING_1_AMOUNT_4 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT5" src="HBAGING1AMOUNT5"/>><rw:field id="F_AGING_1_AMOUNT_5" src="AGING_1_AMOUNT_5" nullValue="&nbsp;"> F_AGING_1_AMOUNT_5 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT6" src="HBAGING1AMOUNT6"/>><rw:field id="F_AGING_1_AMOUNT_6" src="AGING_1_AMOUNT_6" nullValue="&nbsp;"> F_AGING_1_AMOUNT_6 </rw:field></td>
    <td <rw:headers id="HFAGING1AMOUNT7" src="HBAGING1AMOUNT7"/>><rw:field id="F_AGING_1_AMOUNT_7" src="AGING_1_AMOUNT_7" nullValue="&nbsp;"> F_AGING_1_AMOUNT_7 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT2" src="HBAGING2AMOUNT2"/>><rw:field id="F_AGING_2_AMOUNT_2" src="AGING_2_AMOUNT_2" nullValue="&nbsp;"> F_AGING_2_AMOUNT_2 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT3" src="HBAGING2AMOUNT3"/>><rw:field id="F_AGING_2_AMOUNT_3" src="AGING_2_AMOUNT_3" nullValue="&nbsp;"> F_AGING_2_AMOUNT_3 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT4" src="HBAGING2AMOUNT4"/>><rw:field id="F_AGING_2_AMOUNT_4" src="AGING_2_AMOUNT_4" nullValue="&nbsp;"> F_AGING_2_AMOUNT_4 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT5" src="HBAGING2AMOUNT5"/>><rw:field id="F_AGING_2_AMOUNT_5" src="AGING_2_AMOUNT_5" nullValue="&nbsp;"> F_AGING_2_AMOUNT_5 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT6" src="HBAGING2AMOUNT6"/>><rw:field id="F_AGING_2_AMOUNT_6" src="AGING_2_AMOUNT_6" nullValue="&nbsp;"> F_AGING_2_AMOUNT_6 </rw:field></td>
    <td <rw:headers id="HFAGING2AMOUNT7" src="HBAGING2AMOUNT7"/>><rw:field id="F_AGING_2_AMOUNT_7" src="AGING_2_AMOUNT_7" nullValue="&nbsp;"> F_AGING_2_AMOUNT_7 </rw:field></td>
    <td <rw:headers id="HFPHONENUMBER" src="HBPHONENUMBER"/>><rw:field id="F_PHONE_NUMBER" src="PHONE_NUMBER" nullValue="&nbsp;"> F_PHONE_NUMBER </rw:field></td>
   </tr>
  </rw:foreach>
 </tbody>
</table>
</rw:dataArea> <!-- id="MGaddressnumberGRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->




</body>
</html>

<!--
</rw:report> 
-->