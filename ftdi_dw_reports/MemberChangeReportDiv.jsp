<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="MemberChangeReportDiv" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="MEMBERCHANGEREPORTDIV" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE1" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE2" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_MEMBER_CHANGE_DIV">
      <select>
      <![CDATA[SELECT ALL /* + ORDERED */  TI.FBC_NAME,
AB.FIELD_ACCT_REPRESENTATIVE_CODE,
MC.FTD_LONG_NUMBER, MC.SHOP_NAME,
AB.DIVISION_MANAGER_CODE, AR.AMOUNT_DUE,
MC.ADDRESS_LINE_1, CI.INCOMING_RANK, CI.OUTGOING_RANK,
MC.CITY, MC.STATE, MC.ZIP_CODE, 
substr(OP.OWNER_PRESIDENT_NAME,1,30) ,
shop_phone_convert(MC.SHOP_PHONE) as SHOP_PHONE,
MC.MEMBERSHIP_TYPE, MC.REASON_FOR_CHANGE,
MC.MEMBER_DATE, MC.NONMEMBER_DATE, MC.BOOK_DATE, MC.NEWSLETTER_DATE, MC.DATE_LAST_UPDATED
FROM
JDE_MEMBER_CHANGES MC,
JDE_ADDRESS_BOOK AB,
JDE_CUSTOMER_INFORMATION CI,
OWNER_PRESIDENT_NAMES OP,
JDE_AR_AGING_BALANCES AR,
TERRITORY_INFO TI
WHERE((:P_TERRITORY = 'All' or :P_DIVISION = 'All')  
       OR (:P_DIVISION != 'blank' and :P_DIVISION = AB.DIVISION_MANAGER_CODE)  
       OR (:P_TERRITORY != 'blank' and :P_TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE)) 
      AND(:P_MEMBER = 'blank' or :P_MEMBER = MC.FTD_LONG_NUMBER)
            AND ((:P_DATE1 = 'blank' or  MC.DATE_LAST_UPDATED >=  TO_DATE(:P_DATE1, 'MM/DD/YYYY'))
            AND (:P_DATE2 = 'blank' or MC.DATE_LAST_UPDATED <= TO_DATE(:P_DATE2, 'MM/DD/YYYY')))                         
            AND MC.FTD_LONG_NUMBER = AB.LONG_ADDRESS_NUMBER
            AND CI.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
            AND AB.ADDRESS_NUMBER = OP.ADDRESS_NUMBER (+)
            AND AB.ADDRESS_NUMBER = AR.ADDRESS_NUMBER (+)
            AND 'P' !=  AR.PARENT_CHILD_RELATIONSHIP (+)
            AND '00000' = AR.COMPANY (+) 
            AND TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
            AND MC.MEMBERSHIP_TYPE IN ('VT','VSP','SAV','REN','OPN','NEW','GAN','DRP') 
ORDER BY MC.MEMBERSHIP_TYPE, AB.LONG_ADDRESS_NUMBER]]>
      </select>
      <displayInfo x="0.29443" y="0.08337" width="2.28125" height="0.19995"/>
      <group name="G_MEMBERSHIP_TYPE">
        <displayInfo x="0.12366" y="0.54150" width="2.65222" height="0.77246"
        />
        <dataItem name="MEMBERSHIP_TYPE" datatype="vchar2" columnOrder="28"
         width="5" defaultWidth="50000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Membership Type">
          <dataDescriptor expression="MC.MEMBERSHIP_TYPE"
           descriptiveExpression="MEMBERSHIP_TYPE" order="15" width="5"/>
        </dataItem>
        <dataItem name="FBC_NAME" datatype="vchar2" columnOrder="36"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Name" breakOrder="none">
          <dataDescriptor expression="TI.FBC_NAME"
           descriptiveExpression="FBC_NAME" order="1" width="30"/>
        </dataItem>
        <summary name="CS_Membership_type_count" source="FTD_LONG_NUMBER"
         function="count" width="20" precision="10" reset="G_MEMBERSHIP_TYPE"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Membership Type Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_MEMBERSHIP_GRP">
        <displayInfo x="0.13403" y="1.64783" width="2.61926" height="3.67773"
        />
        <dataItem name="substr_OP_OWNER_PRESIDENT_NAME" datatype="vchar2"
         columnOrder="35" width="30" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Substr Op Owner President Name" breakOrder="none">
          <dataDescriptor
           expression="substr ( OP.OWNER_PRESIDENT_NAME , 1 , 30 )"
           descriptiveExpression="SUBSTR(OP.OWNER_PRESIDENT_NAME,1,30)"
           order="13" width="30"/>
        </dataItem>
        <dataItem name="FIELD_ACCT_REPRESENTATIVE_CODE" datatype="vchar2"
         columnOrder="16" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Field Acct Representative Code"
         breakOrder="none">
          <dataDescriptor expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE"
           descriptiveExpression="FIELD_ACCT_REPRESENTATIVE_CODE" order="2"
           width="3"/>
        </dataItem>
        <dataItem name="FTD_LONG_NUMBER" datatype="vchar2" columnOrder="17"
         width="9" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Ftd Long Number" breakOrder="none">
          <dataDescriptor expression="MC.FTD_LONG_NUMBER"
           descriptiveExpression="FTD_LONG_NUMBER" order="3" width="9"/>
        </dataItem>
        <dataItem name="SHOP_NAME" datatype="vchar2" columnOrder="18"
         width="40" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Shop Name" breakOrder="none">
          <dataDescriptor expression="MC.SHOP_NAME"
           descriptiveExpression="SHOP_NAME" order="4" width="40"/>
        </dataItem>
        <dataItem name="DIVISION_MANAGER_CODE" datatype="vchar2"
         columnOrder="19" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Division Manager Code"
         breakOrder="none">
          <dataDescriptor expression="AB.DIVISION_MANAGER_CODE"
           descriptiveExpression="DIVISION_MANAGER_CODE" order="5" width="3"/>
        </dataItem>
        <dataItem name="AMOUNT_DUE" oracleDatatype="number" columnOrder="20"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Amount Due" breakOrder="none">
          <dataDescriptor expression="AR.AMOUNT_DUE"
           descriptiveExpression="AMOUNT_DUE" order="6"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="ADDRESS_LINE_1" datatype="vchar2" columnOrder="21"
         width="40" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Address Line 1" breakOrder="none">
          <dataDescriptor expression="MC.ADDRESS_LINE_1"
           descriptiveExpression="ADDRESS_LINE_1" order="7" width="40"/>
        </dataItem>
        <dataItem name="INCOMING_RANK" oracleDatatype="number"
         columnOrder="22" width="22" defaultWidth="70000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Incoming Rank"
         breakOrder="none">
          <dataDescriptor expression="CI.INCOMING_RANK"
           descriptiveExpression="INCOMING_RANK" order="8"
           oracleDatatype="number" width="22" precision="5"/>
        </dataItem>
        <dataItem name="OUTGOING_RANK" oracleDatatype="number"
         columnOrder="23" width="22" defaultWidth="70000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Outgoing Rank"
         breakOrder="none">
          <dataDescriptor expression="CI.OUTGOING_RANK"
           descriptiveExpression="OUTGOING_RANK" order="9"
           oracleDatatype="number" width="22" precision="5"/>
        </dataItem>
        <dataItem name="CITY" datatype="vchar2" columnOrder="24" width="20"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="City" breakOrder="none">
          <dataDescriptor expression="MC.CITY" descriptiveExpression="CITY"
           order="10" width="20"/>
        </dataItem>
        <dataItem name="STATE" datatype="character"
         oracleDatatype="aFixedChar" columnOrder="25" width="2"
         defaultWidth="20000" defaultHeight="10000" columnFlags="0"
         defaultLabel="State" breakOrder="none">
          <dataDescriptor expression="MC.STATE" descriptiveExpression="STATE"
           order="11" oracleDatatype="aFixedChar" width="2"/>
        </dataItem>
        <dataItem name="ZIP_CODE" datatype="vchar2" columnOrder="26"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Zip Code" breakOrder="none">
          <dataDescriptor expression="MC.ZIP_CODE"
           descriptiveExpression="ZIP_CODE" order="12" width="10"/>
        </dataItem>
        <dataItem name="SHOP_PHONE" datatype="vchar2" columnOrder="27"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Shop Phone" breakOrder="none">
          <dataDescriptor expression="shop_phone_convert ( MC.SHOP_PHONE )"
           descriptiveExpression="SHOP_PHONE" order="14" width="4000"/>
        </dataItem>
        <dataItem name="REASON_FOR_CHANGE" datatype="vchar2" columnOrder="29"
         width="149" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Reason For Change" breakOrder="none">
          <dataDescriptor expression="MC.REASON_FOR_CHANGE"
           descriptiveExpression="REASON_FOR_CHANGE" order="16" width="149"/>
        </dataItem>
        <dataItem name="MEMBER_DATE" datatype="date" oracleDatatype="date"
         columnOrder="30" width="9" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Date" breakOrder="none">
          <dataDescriptor expression="MC.MEMBER_DATE"
           descriptiveExpression="MEMBER_DATE" order="17"
           oracleDatatype="date" width="9"/>
        </dataItem>
        <dataItem name="NONMEMBER_DATE" datatype="date" oracleDatatype="date"
         columnOrder="31" width="9" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Nonmember Date" breakOrder="none">
          <dataDescriptor expression="MC.NONMEMBER_DATE"
           descriptiveExpression="NONMEMBER_DATE" order="18"
           oracleDatatype="date" width="9"/>
        </dataItem>
        <dataItem name="BOOK_DATE" datatype="date" oracleDatatype="date"
         columnOrder="32" width="9" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Book Date" breakOrder="none">
          <dataDescriptor expression="MC.BOOK_DATE"
           descriptiveExpression="BOOK_DATE" order="19" oracleDatatype="date"
           width="9"/>
        </dataItem>
        <dataItem name="NEWSLETTER_DATE" datatype="date" oracleDatatype="date"
         columnOrder="33" width="9" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Newsletter Date" breakOrder="none">
          <dataDescriptor expression="MC.NEWSLETTER_DATE"
           descriptiveExpression="NEWSLETTER_DATE" order="20"
           oracleDatatype="date" width="9"/>
        </dataItem>
        <dataItem name="DATE_LAST_UPDATED" datatype="date"
         oracleDatatype="date" columnOrder="34" width="9" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Date Last Updated">
          <dataDescriptor expression="MC.DATE_LAST_UPDATED"
           descriptiveExpression="DATE_LAST_UPDATED" order="21"
           oracleDatatype="date" width="9"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_TITLE" source="cf_titleformula" datatype="character"
     width="50" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="3.41663" y="0.85413" width="0.79993" height="0.19995"/>
    </formula>
    <summary name="CS_RECORD_COUNT" source="FTD_LONG_NUMBER" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="3.79163" y="1.91663" width="1.55212" height="0.19995"/>
    </summary>
    <formula name="CF_NO_DATA_MESSAGE" source="cf_no_data_messageformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" valueIfNull="blank" breakOrder="none">
      <displayInfo x="3.83337" y="1.37500" width="1.60413" height="0.19995"/>
    </formula>
  </data>
  <layout>
  <section name="main">
    <body width="7.93750" height="9.43750">
      <location x="0.06250" y="0.62500"/>
      <frame name="M_MEMBERSHIP_GROUP">
        <geometryInfo x="0.00000" y="0.23621" width="7.81250" height="8.82629"
        />
        <generalLayout verticalElasticity="expand"/>
        <visualSettings fillPattern="transparent"/>
        <frame name="M_MEMBER_CHANGE_GRPFR">
          <geometryInfo x="0.00000" y="0.23621" width="7.81250"
           height="2.38879"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <repeatingFrame name="R_MEMBERSHIP_TYPE" source="G_MEMBERSHIP_TYPE"
           printDirection="down" maxRecordsPerPage="1" minWidowRecords="1"
           columnMode="no">
            <geometryInfo x="0.00000" y="0.23621" width="7.81250"
             height="2.13879"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_MEMBERSHIP_TYPE_GROUP" source="MEMBERSHIP_TYPE"
             spacing="single" alignment="start">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.37500" y="0.31274" width="0.50000"
               height="0.12500"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_MembershipTypeCount"
             source="CS_Membership_type_count" spacing="single"
             alignment="start">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.87500" y="0.31274" width="0.56250"
               height="0.12500"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <repeatingFrame name="R_MEMBERSHIP" source="G_MEMBERSHIP_GRP"
             printDirection="down" minWidowRecords="1" columnMode="no">
              <geometryInfo x="0.00000" y="0.73621" width="7.81250"
               height="1.63879"/>
              <generalLayout pageProtect="yes" verticalElasticity="variable"/>
              <advancedLayout printObjectOnPage="firstPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillPattern="transparent"/>
              <field name="F_SHOP_NAME" source="SHOP_NAME" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.75000" y="0.81274" width="3.25000"
                 height="0.12500"/>
                <visualSettings lineWidth="2" fillPattern="transparent"/>
              </field>
              <field name="F_FBC_Name" source="FBC_NAME" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="4.56250" y="0.81396" width="1.18750"
                 height="0.12354"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_4">
                <geometryInfo x="5.81250" y="0.81274" width="0.93750"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7"/>
                  <string>
                  <![CDATA[Past Due Amount:]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_AMOUNT_DUE" source="AMOUNT_DUE"
               formatMask="$NNN,NN0.00" spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="6.75000" y="0.81274" width="1.00000"
                 height="0.12500"/>
                <generalLayout>
                  <conditionalFormat>
                    <formatException label="(:AMOUNT_DUE = &apos;0&apos;)">
                      <font face="Arial" size="7"/>
                      <formatVisualSettings fillPattern="transparent"/>
                    <cond name="first" column="AMOUNT_DUE" exception="1"
                     lowValue="0" conjunction="1"/>
                    </formatException>
                  </conditionalFormat>
                </generalLayout>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"
                 formatTrigger="f_amount_dueformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_ADDRESS_LINE_1" source="ADDRESS_LINE_1"
               spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.01379" y="1.00000" width="3.06250"
                 height="0.18774"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_INCOMING_RANK" source="INCOMING_RANK"
               spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="5.12500" y="1.00024" width="0.50000"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_5">
                <geometryInfo x="4.62500" y="1.00024" width="0.50000"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7"/>
                  <string>
                  <![CDATA[Rank In:]]>
                  </string>
                </textSegment>
              </text>
              <text name="B_6">
                <geometryInfo x="6.18750" y="1.00024" width="0.50000"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7"/>
                  <string>
                  <![CDATA[Rank Out:]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_OUTGOING_RANK" source="OUTGOING_RANK"
               spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="6.75000" y="1.00024" width="0.43750"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_CITY" source="CITY" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.01379" y="1.18774" width="1.43750"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_STATE" source="STATE" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="1.51379" y="1.18774" width="0.50000"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_ZIP_CODE" source="ZIP_CODE" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="2.01379" y="1.18774" width="0.56250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_7">
                <geometryInfo x="4.25000" y="1.18774" width="0.87500"
                 height="0.12476"/>
                <textSegment>
                  <font face="Arial" size="7"/>
                  <string>
                  <![CDATA[Owner/President:]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_OWNER_PRESIDENT_NAME"
               source="substr_OP_OWNER_PRESIDENT_NAME" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="5.12500" y="1.18774" width="2.62500"
                 height="0.12476"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_REASON_FOR_CHANGE" source="REASON_FOR_CHANGE"
               spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.43750" y="1.50024" width="7.37500"
                 height="0.12500"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_8">
                <textSettings justify="center"/>
                <geometryInfo x="0.56250" y="1.81274" width="0.75000"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Member Date]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_MEMBER_DATE" source="MEMBER_DATE"
               spacing="single" alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.56250" y="2.00024" width="0.68750"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_9">
                <textSettings justify="center"/>
                <geometryInfo x="1.87500" y="1.81274" width="0.75000"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Book Date]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_BOOK_DATE" source="BOOK_DATE" spacing="single"
               alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="1.87500" y="2.00024" width="0.68750"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_11">
                <textSettings justify="center"/>
                <geometryInfo x="4.93750" y="1.81274" width="0.75000"
                 height="0.11462"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[NonMbr Date]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_NONMEMBER_DATE" source="NONMEMBER_DATE"
               spacing="single" alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="4.93750" y="2.00024" width="0.75000"
                 height="0.12500"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_12">
                <textSettings justify="center"/>
                <geometryInfo x="6.31250" y="1.81274" width="0.75000"
                 height="0.11462"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Last Updated]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_DATE_LAST_UPDATED" source="DATE_LAST_UPDATED"
               spacing="single" alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="6.31250" y="2.00024" width="0.75000"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_10">
                <textSettings justify="center"/>
                <geometryInfo x="3.25000" y="1.81274" width="0.93750"
                 height="0.12500"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Newsltr Date]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_NEWSLETTER_DATE" source="NEWSLETTER_DATE"
               spacing="single" alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="3.25000" y="2.00024" width="0.87500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_SHOP_PHONE" source="SHOP_PHONE" spacing="single"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.01379" y="1.31274" width="1.12500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_MEMBERSHIP_TYPE" source="MEMBERSHIP_TYPE"
               spacing="single" alignment="start">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="0.01379" y="1.50024" width="0.31250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <line name="B_16" arrow="none">
                <geometryInfo x="0.01379" y="0.75000" width="7.73621"
                 height="0.00000"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings lineWidth="2" fillPattern="transparent"
                 linePattern="solid"/>
                <points>
                  <point x="0.01379" y="0.75000"/>
                  <point x="7.75000" y="0.75000"/>
                </points>
              </line>
              <field name="F_FTD_LONG_NUMBER" source="FTD_LONG_NUMBER"
               spacing="single" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.01379" y="0.81274" width="0.62500"
                 height="0.12500"/>
                <visualSettings lineWidth="2" fillPattern="transparent"/>
              </field>
              <field name="F_FLD_ACCT_REPRESENTATIVE_CODE"
               source="FIELD_ACCT_REPRESENTATIVE_CODE" spacing="single"
               alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="4.18750" y="0.81250" width="0.31250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
            </repeatingFrame>
            <line name="B_1" arrow="none">
              <geometryInfo x="0.25000" y="0.25000" width="1.37500"
               height="0.00000"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings lineWidth="2" fillPattern="transparent"
               linePattern="solid"/>
              <points>
                <point x="0.25000" y="0.25000"/>
                <point x="1.62500" y="0.25000"/>
              </points>
            </line>
            <line name="B_2" arrow="none">
              <geometryInfo x="0.25000" y="0.50000" width="1.37500"
               height="0.00000"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings lineWidth="2" fillPattern="transparent"
               linePattern="solid"/>
              <points>
                <point x="0.25000" y="0.50000"/>
                <point x="1.62500" y="0.50000"/>
              </points>
            </line>
          </repeatingFrame>
        </frame>
        <text name="B_3">
          <textSettings justify="center"/>
          <geometryInfo x="2.26379" y="3.07629" width="4.00000"
           height="0.67371"/>
          <advancedLayout formatTrigger="b_3formattrigger"/>
          <visualSettings fillBackgroundColor="r50g88b75"/>
          <textSegment>
            <font face="Arial" size="9"/>
            <string>
            <![CDATA[
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="9"/>
            <string>
            <![CDATA[&<CF_NO_DATA_MESSAGE>
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="9"/>
            <string>
            <![CDATA[
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="9"/>
            <string>
            <![CDATA[                                                                                                                                                                                                                                                                  ]]>
            </string>
          </textSegment>
        </text>
      </frame>
    </body>
    <margin>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/RRRR"
       alignment="left">
        <font face="Arial" size="7"/>
        <geometryInfo x="0.37500" y="0.25000" width="0.83337" height="0.17004"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_13">
        <textSettings justify="center"/>
        <geometryInfo x="2.18750" y="0.25000" width="4.12500" height="0.17004"
        />
        <visualSettings fillBackgroundColor="r100g88b50"/>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[MEMBER CHANGE REPORT &<CF_TITLE>]]>
          </string>
        </textSegment>
      </text>
      <line name="B_14" arrow="none">
        <geometryInfo x="0.31250" y="0.50000" width="7.25000" height="0.00000"
        />
        <visualSettings lineWidth="2" fillPattern="transparent"
         fillBackgroundColor="r100g88b50" linePattern="solid"/>
        <points>
          <point x="0.31250" y="0.50000"/>
          <point x="7.56250" y="0.50000"/>
        </points>
      </line>
      <line name="B_15" arrow="none">
        <geometryInfo x="0.31250" y="0.12500" width="7.25000" height="0.00000"
        />
        <visualSettings lineWidth="2" fillPattern="transparent"
         fillBackgroundColor="r100g88b50" linePattern="solid"/>
        <points>
          <point x="0.31250" y="0.12500"/>
          <point x="7.56250" y="0.12500"/>
        </points>
      </line>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TITLEFormula return Char is
  lv_title varchar2(60);
begin
     if :P_MEMBER != 'blank' then
  	     lv_title := ('BY MEMBER #'||' '||:P_MEMBER);  	
     elsif :P_DIVISION != 'blank' and :P_DIVISION != 'All' then
         lv_title := ('BY DIVISION #'||' '||:P_DIVISION);
     elsif :P_TERRITORY != 'blank' and :P_TERRITORY != 'All' then
  	     lv_title := ('BY TERRITORY #'||' '||:P_TERRITORY); 
  	 elsif :P_TERRITORY = 'All' or :P_DIVISION = 'All' then
	          lv_title := ('FOR '||:P_DATE1 ||' - '|| :P_DATE2);
     end if;
          			
  return (lv_title);	
end;

]]>
      </textSource>
    </function>
    <function name="cf_no_data_messageformula" returnType="character">
      <textSource>
      <![CDATA[function CF_NO_DATA_MESSAGEFormula return Char is
 lv_Message varchar2(80);
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;
 return lv_Message; 
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CF_NO_DATA_MESSAGE LIKE 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_amount_dueformattrigger">
      <textSource>
      <![CDATA[function F_AMOUNT_DUEFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AMOUNT_DUE = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
