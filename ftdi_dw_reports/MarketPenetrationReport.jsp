<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="MarketPenetrationReport" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="MARKETPENETRATIONREPORT" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_Month" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Year" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_Sales_Rpt_Code" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_1">
      <select>
      <![CDATA[select
       decode(a.sales_reporting_code_1,
       'P', 'Warehouse Containers',
       'B', 'Branded Containers',
       'S', 'Support Containers',
       'N', 'Generic Containers',
       'H', 'Codified Containers',
       'M', 'Dropship',
       'J', 'Fresh Flowers',
       'G', 'Renaissance Cards',
       'I', 'Member Marketing') as Brand,
        count(distinct a.address_number) as members,
        (count(distinct a.address_number) / b.current_month_total_shops * 100 ) as penetration,
        (sum(a.quantity*a.unit_list_price)/count(distinct a.address_number)) as avg_spending,
        sdly.members_ly,
        sdly.penetration_ly,
        sdly.avg_spending_ly
from
   jde_sales_order_detail a, jde_member_services b,
 (select
     a.sales_reporting_code_1 as sales_reporting_code_1,
     count(distinct a.address_number)  as members_ly,
     (count(distinct a.address_number) / b.current_month_total_shops * 100 ) penetration_ly,
     (sum(a.quantity*a.unit_list_price)/count(distinct a.address_number)) as avg_spending_ly
  from
    jde_sales_order_detail a,jde_member_services b
  where
      a.sales_reporting_code_5 = 'M' and
      a.Quantity > 0 and
      ((upper(:P_Sales_Rpt_Code) = 'ALL' and 
      a.sales_reporting_code_1 IN ('P','B','S','N','H','M','J','I','G')) or 
      (upper(:P_Sales_Rpt_Code) = 'CONTAINERS' and a.sales_reporting_code_1 IN ('P','B','S','N','H')) or
      (upper(:P_Sales_Rpt_Code) = 'DROPSHIP' and a.sales_reporting_code_1 = 'M') or
      (upper(:P_Sales_Rpt_Code) = 'FRESHFLOWERS' and a.sales_reporting_code_1 = 'J') or
      (upper(:P_Sales_Rpt_Code) = 'MEMBERMARKET' and a.sales_reporting_code_1 = 'I') or
      (upper(:P_Sales_Rpt_Code) = 'CARDS' and a.sales_reporting_code_1 = 'G')) and
      a.order_type in ('SO','SR') and
      a.status_code_last = '600' and
      b.effective_month_date = add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-12) and
      (a.invoice_date >= add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-12) and
      a.invoice_date < add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-11)) and
     b.grocery_store_flag <> 'Y' and
      b.field_account_representative = 'CM'
  group by a.sales_reporting_code_1, b.current_month_total_shops) sdly
where
  a.sales_reporting_code_1 = sdly.sales_reporting_code_1 (+) and
  a.sales_reporting_code_5 = 'M' and
  a.Quantity > 0 and
  ((upper(:P_Sales_Rpt_Code) = 'ALL' and 
  a.sales_reporting_code_1 IN ('P','B','S','N','H','M','J','I','G')) or 
  (upper(:P_Sales_Rpt_Code) = 'CONTAINERS' and a.sales_reporting_code_1 IN ('P','B','S','N','H')) or
  (upper(:P_Sales_Rpt_Code) = 'DROPSHIP' and a.sales_reporting_code_1 = 'M') or
  (upper(:P_Sales_Rpt_Code) = 'FRESHFLOWERS' and a.sales_reporting_code_1 = 'J') or
  (upper(:P_Sales_Rpt_Code) = 'MEMBERMARKET' and a.sales_reporting_code_1 = 'I') or
  (upper(:P_Sales_Rpt_Code) = 'CARDS' and a.sales_reporting_code_1 = 'G')) and
  a.order_type in ('SO','SR') and
  a.status_code_last = '600' and
  b.effective_month_date = to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY') and
  (a.invoice_date >= to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY') and
  a.invoice_date < add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),1)) and
  b.grocery_store_flag <> 'Y' and
  b.field_account_representative = 'CM'
group by a.sales_reporting_code_1, b.current_month_total_shops, sdly.members_ly, sdly.penetration_ly, sdly.avg_spending_ly
/
]]>
      </select>
      <displayInfo x="1.65002" y="1.00000" width="0.69995" height="0.19995"/>
      <group name="G_Brand">
        <displayInfo x="1.45007" y="1.94995" width="1.09998" height="0.43066"
        />
        <dataItem name="Brand" datatype="vchar2" columnOrder="14" width="20"
         defaultWidth="100000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Brand">
          <dataDescriptor
           expression="decode ( a.sales_reporting_code_1 , &apos;P&apos; , &apos;Warehouse Containers&apos; , &apos;B&apos; , &apos;Branded Containers&apos; , &apos;S&apos; , &apos;Support Containers&apos; , &apos;N&apos; , &apos;Generic Containers&apos; , &apos;H&apos; , &apos;Codified Containers&apos; , &apos;M&apos; , &apos;Dropship&apos; , &apos;J&apos; , &apos;Fresh Flowers&apos; , &apos;G&apos; , &apos;Renaissance Cards&apos; , &apos;I&apos; , &apos;Member Marketing&apos; )"
           descriptiveExpression="BRAND" order="1" width="20"/>
        </dataItem>
      </group>
      <group name="G_address_number">
        <displayInfo x="1.15759" y="3.13965" width="1.68494" height="0.77246"
        />
        <dataItem name="members_ly" oracleDatatype="number" columnOrder="18"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Members Ly">
          <dataDescriptor expression="sdly.members_ly"
           descriptiveExpression="MEMBERS_LY" order="5"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="penetration_ly" oracleDatatype="number"
         columnOrder="19" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Penetration Ly"
         breakOrder="none">
          <dataDescriptor expression="sdly.penetration_ly"
           descriptiveExpression="PENETRATION_LY" order="6"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="avg_spending_ly" oracleDatatype="number"
         columnOrder="20" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Avg Spending Ly"
         breakOrder="none">
          <dataDescriptor expression="sdly.avg_spending_ly"
           descriptiveExpression="AVG_SPENDING_LY" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="penetration" oracleDatatype="number" columnOrder="16"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Penetration">
          <dataDescriptor
           expression="( count ( distinct a.address_number ) / b.current_month_total_shops * 100 )"
           descriptiveExpression="PENETRATION" order="3"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="avg_spending" oracleDatatype="number" columnOrder="17"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Avg Spending">
          <dataDescriptor
           expression="( sum ( a.quantity * a.unit_list_price ) / count ( distinct a.address_number ) )"
           descriptiveExpression="AVG_SPENDING" order="4"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="members" oracleDatatype="number" columnOrder="15"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Members">
          <xmlSettings xmlTag="ADDRESS_NUMBER"/>
          <dataDescriptor expression="count ( distinct a.address_number )"
           descriptiveExpression="MEMBERS" order="2" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <dataSource name="Q_2">
      <select>
      <![CDATA[SELECT ALL 
    b.CURRENT_MONTH_TOTAL_SHOPS,
    b.PR_YEAR_MONTH_TOTAL_SHOPS,
    count(distinct a.address_number) as tot_members,
                (count(distinct a.address_number) / b.current_month_total_shops * 100 ) as tot_penetration,
                (sum(a.quantity*a.unit_list_price)/count(distinct a.address_number)) as tot_avg_spending,
    sdly.pr_tot_members,
    sdly.pr_tot_penetration,
    sdly.pr_tot_avg_spending
FROM 
     jde_sales_order_detail a,  jde_member_services b,
                (select
                                b.CURRENT_MONTH_TOTAL_SHOPS,
                                c.CURRENT_MONTH_TOTAL_SHOPS as future_month_tot_shops,
        count(distinct a.address_number) as pr_tot_members,
                    (count(distinct a.address_number) / b.CURRENT_MONTH_TOTAL_SHOPS * 100) as pr_tot_penetration,
                    (sum(a.quantity*a.unit_list_price)/count(distinct a.address_number)) as pr_tot_avg_spending
                from    
            jde_sales_order_detail a,  jde_member_services b, jde_member_services c
                where
            a.sales_reporting_code_5 = 'M' and
            a.Quantity > 0 and
            ((upper(:P_Sales_Rpt_Code) = 'ALL' and 
                                                a.sales_reporting_code_1 IN ('P','B','S','N','H','M','J','I','G')) or 
                                                (upper(:P_Sales_Rpt_Code) = 'CONTAINERS' and a.sales_reporting_code_1 IN ('P','B','S','N','H')) or
                                                (upper(:P_Sales_Rpt_Code) = 'DROPSHIP' and a.sales_reporting_code_1 = 'M') or
                                                (upper(:P_Sales_Rpt_Code) = 'FRESHFLOWERS' and a.sales_reporting_code_1 = 'J') or
                                                (upper(:P_Sales_Rpt_Code) = 'MEMBERMARKET' and a.sales_reporting_code_1 = 'I') or
                                                (upper(:P_Sales_Rpt_Code) = 'CARDS' and a.sales_reporting_code_1 = 'G')) and
            a.order_type in ('SO','SR') and
            a.status_code_last = '600' and
            (a.invoice_date >= add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-12) and
            a.invoice_date < add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-11)) and
            b.effective_month_date = add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),-12) and
            b.grocery_store_flag <> 'Y' and
            b.field_account_representative = 'CM' and
            c.effective_month_date = to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY') and
            c.grocery_store_flag <> 'Y' and
            c.field_account_representative = 'CM'
                group by b.CURRENT_MONTH_TOTAL_SHOPS, c.CURRENT_MONTH_TOTAL_SHOPS
                ) sdly
WHERE 
    b.CURRENT_MONTH_TOTAL_SHOPS = sdly.future_month_tot_shops (+) and
    a.sales_reporting_code_5 = 'M' and
    a.Quantity > 0 and
                ((upper(:P_Sales_Rpt_Code) = 'ALL' and 
                a.sales_reporting_code_1 IN ('P','B','S','N','H','M','J','I','G')) or 
                (upper(:P_Sales_Rpt_Code) = 'CONTAINERS' and a.sales_reporting_code_1 IN ('P','B','S','N','H')) or
                (upper(:P_Sales_Rpt_Code) = 'DROPSHIP' and a.sales_reporting_code_1 = 'M') or
                (upper(:P_Sales_Rpt_Code) = 'FRESHFLOWERS' and a.sales_reporting_code_1 = 'J') or
                (upper(:P_Sales_Rpt_Code) = 'MEMBERMARKET' and a.sales_reporting_code_1 = 'I') or
                (upper(:P_Sales_Rpt_Code) = 'CARDS' and a.sales_reporting_code_1 = 'G')) and
    a.order_type in ('SO','SR') and
    a.status_code_last = '600' and
    b.effective_month_date = to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY') and
    (a.invoice_date >= to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY') and
    a.invoice_date < add_months(to_date(:P_Month || '/01/' || :P_Year, 'MM/DD/YYYY'),1)) and
    b.grocery_store_flag <> 'Y' and
    b.field_account_representative = 'CM' 
group by b.CURRENT_MONTH_TOTAL_SHOPS, b.PR_YEAR_MONTH_TOTAL_SHOPS, sdly.pr_tot_members,sdly.pr_tot_penetration,sdly.pr_tot_avg_spending
]]>
      </select>
      <displayInfo x="4.14990" y="0.95825" width="0.69995" height="0.19995"/>
      <group name="G_CURRENT_MONTH_TOTAL_SHOPS">
        <displayInfo x="3.29456" y="1.98120" width="2.43176" height="0.77246"
        />
        <dataItem name="pr_tot_members" oracleDatatype="number"
         columnOrder="28" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1" defaultLabel="Pr Tot Members">
          <dataDescriptor expression="sdly.pr_tot_members"
           descriptiveExpression="PR_TOT_MEMBERS" order="6"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="tot_members" oracleDatatype="number" columnOrder="23"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Tot Members" breakOrder="none">
          <xmlSettings xmlTag="MEMBERS1"/>
          <dataDescriptor expression="count ( distinct a.address_number )"
           descriptiveExpression="TOT_MEMBERS" order="3"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="tot_penetration" oracleDatatype="number"
         columnOrder="24" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Tot Penetration"
         breakOrder="none">
          <dataDescriptor
           expression="( count ( distinct a.address_number ) / b.current_month_total_shops * 100 )"
           descriptiveExpression="TOT_PENETRATION" order="4"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="tot_avg_spending" oracleDatatype="number"
         columnOrder="25" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1" defaultLabel="Tot Avg Spending">
          <dataDescriptor
           expression="( sum ( a.quantity * a.unit_list_price ) / count ( distinct a.address_number ) )"
           descriptiveExpression="TOT_AVG_SPENDING" order="5"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="pr_tot_penetration" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Pr Tot Penetration">
          <dataDescriptor expression="sdly.pr_tot_penetration"
           descriptiveExpression="PR_TOT_PENETRATION" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="pr_tot_avg_spending" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Pr Tot Avg Spending">
          <dataDescriptor expression="sdly.pr_tot_avg_spending"
           descriptiveExpression="PR_TOT_AVG_SPENDING" order="8"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="CURRENT_MONTH_TOTAL_SHOPS" oracleDatatype="number"
         columnOrder="21" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Current Month Total Shops">
          <dataDescriptor expression="b.CURRENT_MONTH_TOTAL_SHOPS"
           descriptiveExpression="CURRENT_MONTH_TOTAL_SHOPS" order="1"
           oracleDatatype="number" width="22" precision="15"/>
        </dataItem>
        <dataItem name="PR_YEAR_MONTH_TOTAL_SHOPS" oracleDatatype="number"
         columnOrder="22" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Pr Year Month Total Shops">
          <dataDescriptor expression="b.PR_YEAR_MONTH_TOTAL_SHOPS"
           descriptiveExpression="PR_YEAR_MONTH_TOTAL_SHOPS" order="2"
           oracleDatatype="number" width="22" precision="15"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_HeaderText" source="cf_1formula" datatype="character"
     width="55" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="3.71875" y="3.19800" width="1.86462" height="0.37500"/>
    </formula>
    <summary name="CS_Record_Count" source="Brand" function="count" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="7.05212" y="1.13538" width="1.16663" height="0.38550"/>
    </summary>
    <formula name="CF_No_Data_Msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="7.08337" y="1.68750" width="1.16663" height="0.37500"/>
    </formula>
  </data>
  <layout>
  <section name="main">
    <body height="6.81250">
      <location x="0.31250" y="0.81250"/>
      <frame name="M_G_CURRENT_MONTH_TOTAL_SHOPS">
        <geometryInfo x="0.12500" y="0.00000" width="7.37500" height="0.97913"
        />
        <generalLayout horizontalElasticity="variable"/>
      </frame>
      <repeatingFrame name="R_G_CURRENT_MONTH_TOTAL_SHOPS"
       source="G_CURRENT_MONTH_TOTAL_SHOPS" printDirection="across"
       minWidowRecords="1" columnMode="no">
        <geometryInfo x="3.42371" y="0.00000" width="4.07629" height="1.00000"
        />
        <generalLayout verticalElasticity="expand"/>
        <visualSettings fillPattern="transparent"/>
        <field name="F_CURRENT_MONTH_TOTAL_SHOPS"
         source="CURRENT_MONTH_TOTAL_SHOPS" formatMask="N,NNN,NNN"
         alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="3.43750" y="0.25000" width="0.75000"
           height="0.12500"/>
          <generalLayout verticalElasticity="expand"/>
        </field>
        <field name="F_tot_Purch_Members_curr" source="tot_members"
         formatMask="N,NNN,NNN" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="3.43750" y="0.43750" width="0.75000"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <field name="F_tot_Penetration_Curr" source="tot_penetration"
         formatMask="NNNNNNNNNNN0.00%" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="3.43750" y="0.62500" width="0.75000"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <field name="F_tot_Avg_Spending_Curr" source="tot_avg_spending"
         formatMask="LNNNGNN0D00" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="3.43750" y="0.81250" width="0.75000"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <text name="B_CURRENT_MONTH_TOTAL_SHOPS">
          <textSettings justify="center"/>
          <geometryInfo x="3.50000" y="0.00000" width="1.00000"
           height="0.18750"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[This Year]]>
            </string>
          </textSegment>
        </text>
        <field name="F_PR_YEAR_MONTH_TOTAL_SHOPS"
         source="PR_YEAR_MONTH_TOTAL_SHOPS" formatMask="N,NNN,NNN"
         alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.68750" y="0.25000" width="0.68750"
           height="0.12500"/>
          <generalLayout verticalElasticity="expand"/>
        </field>
        <field name="F_tot_Purch_members_Prior" source="pr_tot_members"
         formatMask="N,NNN,NNN" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.68750" y="0.43750" width="0.68750"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <field name="F_tot_Penetration_Prior" source="pr_tot_penetration"
         formatMask="NNNNNNNNNNN0.00%" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.68750" y="0.62500" width="0.68750"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <field name="F_tot_Avg_Spending_Prior" source="pr_tot_avg_spending"
         formatMask="LNNNGNN0D00" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.68750" y="0.81250" width="0.68750"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
        </field>
        <text name="B_PR_YEAR_MONTH_TOTAL_SHOPS">
          <textSettings justify="center"/>
          <geometryInfo x="5.68750" y="0.00000" width="0.93750"
           height="0.18750"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[Last Year]]>
            </string>
          </textSegment>
        </text>
        <line name="B_9" arrow="none">
          <geometryInfo x="3.43750" y="0.18750" width="3.81250"
           height="0.00000"/>
          <visualSettings lineWidth="2" linePattern="solid"/>
          <points>
            <point x="3.43750" y="0.18750"/>
            <point x="7.25000" y="0.18750"/>
          </points>
        </line>
      </repeatingFrame>
      <frame name="M_G_Brand_GRPFR">
        <geometryInfo x="0.00000" y="1.00000" width="7.50000" height="1.56250"
        />
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_Brand" source="G_Brand"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.12500" y="1.50000" width="6.62500"
           height="0.93750"/>
          <generalLayout pageProtect="yes" verticalElasticity="variable"/>
          <field name="F_Brand" source="Brand" alignment="start">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="0.12500" y="1.56250" width="3.31250"
             height="0.18750"/>
            <generalLayout horizontalElasticity="expand"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <repeatingFrame name="R_G_address_number" source="G_address_number"
           printDirection="down" minWidowRecords="1" columnMode="no">
            <geometryInfo x="3.43750" y="1.68750" width="3.31250"
             height="0.62500"/>
            <generalLayout verticalElasticity="expand"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Purch_Members_Curr" source="members"
             alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="3.56250" y="1.93750" width="0.75000"
               height="0.12500"/>
              <generalLayout verticalElasticity="expand"/>
            </field>
            <field name="F_Avg_Spending_Curr" source="avg_spending"
             formatMask="LNNNGNN0D00" alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="3.56250" y="2.12500" width="0.75000"
               height="0.12500"/>
            </field>
            <field name="F_Penetration_Curr" source="penetration"
             formatMask="NNNNNNNNNNN0.00%" alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="3.56250" y="1.75000" width="0.75000"
               height="0.12500"/>
            </field>
            <field name="F_Penetration_Prior" source="penetration_ly"
             formatMask="NNNNNNNNNNN0.00%" alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="5.56250" y="1.75000" width="0.75000"
               height="0.12500"/>
            </field>
            <field name="F_Purch_Members_Prior" source="members_ly"
             alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="5.56250" y="1.93750" width="0.75000"
               height="0.12500"/>
            </field>
            <field name="F_Avg_Spending_Prior" source="avg_spending_ly"
             formatMask="LNNNGNN0D00" alignment="right">
              <font face="Arial" size="8"/>
              <geometryInfo x="5.56250" y="2.12500" width="0.75000"
               height="0.12500"/>
            </field>
          </repeatingFrame>
          <text name="B_1">
            <geometryInfo x="0.37500" y="1.75000" width="2.50000"
             height="0.16663"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject" formatTrigger="b_1formattrigger"
            />
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Penetration]]>
              </string>
            </textSegment>
          </text>
          <text name="B_2">
            <geometryInfo x="0.37500" y="1.93750" width="2.50000"
             height="0.16663"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[# of Purchasing Members]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <geometryInfo x="0.37500" y="2.12500" width="2.41663"
             height="0.16663"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject" formatTrigger="b_3formattrigger"
            />
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Avg Spending per MP Purchaser]]>
              </string>
            </textSegment>
          </text>
        </repeatingFrame>
        <frame name="M_G_Brand_HDR">
          <geometryInfo x="0.06250" y="1.25000" width="1.93750"
           height="0.18750"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="anchoringObject"/>
          <text name="B_Brand">
            <geometryInfo x="0.06250" y="1.25000" width="1.93750"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:CS_Record_Count = &apos;0&apos;)">
                  <font face="Courier New" size="12"/>
                  <formatVisualSettings fillPattern="transparent"
                   borderPattern="solid" borderForegroundColor="white"/>
                <cond name="first" column="CS_Record_Count" exception="1"
                 lowValue="0" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_brandformattrigger"/>
            <visualSettings fillPattern="transparent" linePattern="solid"
             lineForegroundColor="white"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Brand Specific Metrics]]>
              </string>
            </textSegment>
          </text>
        </frame>
        <text name="B_10">
          <textSettings justify="center"/>
          <geometryInfo x="3.50000" y="1.25000" width="1.00000"
           height="0.20837"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_10formattrigger"
          />
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[This Year]]>
            </string>
          </textSegment>
        </text>
        <text name="B_11">
          <textSettings justify="center"/>
          <geometryInfo x="5.68750" y="1.25000" width="0.93750"
           height="0.20837"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_Count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_Count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_11formattrigger"
          />
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[Last Year]]>
            </string>
          </textSegment>
        </text>
        <line name="B_12" arrow="none">
          <geometryInfo x="3.43750" y="1.45129" width="3.81250"
           height="0.00000"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_Count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings borderPattern="solid"/>
              <cond name="first" column="CS_Record_Count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_12formattrigger"
          />
          <visualSettings lineWidth="2" linePattern="solid"/>
          <points>
            <point x="3.43750" y="1.45129"/>
            <point x="7.25000" y="1.45129"/>
          </points>
        </line>
      </frame>
      <text name="B_13">
        <textSettings justify="center"/>
        <geometryInfo x="2.25000" y="2.62500" width="3.18750" height="0.15625"
        />
        <advancedLayout formatTrigger="b_13formattrigger"/>
        <visualSettings lineForegroundColor="white"/>
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[&<CF_No_Data_Msg>]]>
          </string>
        </textSegment>
      </text>
      <frame name="M_G_CURRENT_MONTH_TOTAL_SHOPS1">
        <geometryInfo x="0.00000" y="0.00000" width="3.43750" height="1.00000"
        />
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_4">
          <geometryInfo x="0.12500" y="0.25000" width="2.50000"
           height="0.18750"/>
          <advancedLayout formatTrigger="b_4formattrigger"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[Average Members]]>
            </string>
          </textSegment>
        </text>
        <text name="B_5">
          <geometryInfo x="0.12500" y="0.43750" width="2.50000"
           height="0.18750"/>
          <advancedLayout formatTrigger="b_5formattrigger"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[# of Purchasing Members]]>
            </string>
          </textSegment>
        </text>
        <text name="B_6">
          <geometryInfo x="0.12500" y="0.62500" width="2.43750"
           height="0.18750"/>
          <advancedLayout formatTrigger="b_6formattrigger"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[Penetration]]>
            </string>
          </textSegment>
        </text>
        <text name="B_7">
          <geometryInfo x="0.12500" y="0.81250" width="2.56250"
           height="0.18750"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_Count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_Count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_7formattrigger"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[Avg Spending per MP Purchaser]]>
            </string>
          </textSegment>
        </text>
      </frame>
    </body>
    <margin>
      <text name="B_8">
        <textSettings justify="center"/>
        <geometryInfo x="2.06250" y="0.43750" width="4.68750" height="0.16663"
        />
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[&<CF_HeaderText>]]>
          </string>
        </textSegment>
      </text>
      <text name="B_PAGENUM1">
        <textSettings justify="center"/>
        <geometryInfo x="2.79163" y="7.70837" width="2.91663" height="0.29163"
        />
        <textSegment>
          <font face="Arial" size="10" bold="yes"/>
          <string>
          <![CDATA[Page &<PageNumber> of &<TotalPages>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/RRRR"
       alignment="left">
        <font face="Arial" size="9" bold="yes"/>
        <geometryInfo x="0.56250" y="0.43750" width="1.37500" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_1formula" returnType="character">
      <textSource>
      <![CDATA[function CF_1Formula return Char is
begin
  return('MARKETPLACE PENETRATION REPORT FOR ' || to_char(:P_Month) || '/' || to_char(:P_Year));
end;]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_MsgFormula return Char is
 lv_Message varchar2(80);
begin
  if :cs_record_count = 0 then
   lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;
  return lv_message;  
end;]]>
      </textSource>
    </function>
    <function name="b_13formattrigger">
      <textSource>
      <![CDATA[function B_13FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count != '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_5formattrigger">
      <textSource>
      <![CDATA[function B_5FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_6formattrigger">
      <textSource>
      <![CDATA[function B_6FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_7formattrigger">
      <textSource>
      <![CDATA[function B_7FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_10formattrigger">
      <textSource>
      <![CDATA[function B_10FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_11formattrigger">
      <textSource>
      <![CDATA[function B_11FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_brandformattrigger">
      <textSource>
      <![CDATA[function B_BrandFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_12formattrigger">
      <textSource>
      <![CDATA[function B_12FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_1formattrigger">
      <textSource>
      <![CDATA[function B_1FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate defaultReportType="tabular" templateName="rwbeige"
   sectionTitle="Totals Group"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>



</head>


<body>

<!-- Data Area Generated by Reports Developer -->
<rw:dataArea id="MGBrandGRPFR">
<table>
<caption>  </caption>
 <!-- Header -->
 <thead>
  <tr>
   <th <rw:id id="HBBrand" asArray="no"/>> Brand </th>
   <th <rw:id id="HBaddressnumber" asArray="no"/>> Address Number </th>
  </tr>
 </thead>
 <!-- Body -->
 <tbody>
  <rw:foreach id="R_G_Brand_1" src="G_Brand">
   <rw:foreach id="R_G_address_number_1" src="G_address_number">
    <tr>
     <td <rw:id id="HFBrand" breakLevel="R_G_Brand_1" asArray="no"/>><rw:field id="Brand" src="Brand" breakLevel="R_G_Brand_1" breakValue="&nbsp;"> F_Brand </rw:field></td>
     <td <rw:headers id="HFaddressnumber" src="HBaddressnumber, HBBrand, HFBrand"/>><rw:field id="F_address_number" src="address_number" nullValue="&nbsp;"> F_address_number </rw:field></td>
    </tr>
   </rw:foreach>
  </rw:foreach>
 </tbody>
</table>
</rw:dataArea> <!-- id="MGBrandGRPFR" -->
<!-- End of Data Area Generated by Reports Developer -->




</body>
</html>

<!--
</rw:report> 
-->