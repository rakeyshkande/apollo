<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="OrderGathererHistory" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="ORDERGATHERERHISTORYXXX" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_OUT_ORDER_TOTAL" datatype="number" width="4"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_COUNTRY_CODE" datatype="character" width="3"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_RATIO" datatype="number" precision="10"
     inputMask="NNNNN0.00" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_GROUPBY" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_REPORT" datatype="character" precision="10"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Order_Gatherer_History ">
      <select>
      <![CDATA[SELECT  
AB.FIELD_ACCT_REPRESENTATIVE_CODE, 
AB.DIVISION_MANAGER_CODE,
AB.SEND_ONLY_ORDER_GATHERER_CODE,
AB.ADDRESS_NUMBER as AB_ADDRESS_NUMBER, 
AB.LONG_ADDRESS_NUMBER,
substr(ltrim(AB.ALPHA_NAME,' '),1,30) as NAME, 
CI.OUTGOING_RANK,
to_char(CM.DATE_LAST_PAID,'MM/DD/YY') AS DATE_LAST_PAID, 
CM.LAST_PAID_AMOUNT,
AR.OPEN_AMOUNT, 
(AR.CURRENT_AMOUNT_DUE + AR.FUTURE_AMOUNT_DUE + AR.AGING_AMOUNT_1) AS AGING_AMOUNT_1,
AR.AGING_AMOUNT_2,  AR.AGING_AMOUNT_3,  AR.AGING_AMOUNT_4,
(AR.AGING_AMOUNT_5 + AR.AGING_AMOUNT_6 + AR.AGING_AMOUNT_7) AS AGING_AMOUNT_5,
CM.COLLECTION_MANAGER, 
WOS1.ADDRESS_NUMBER, 
decode (pcb1.bottom_most_flag, 'N','Y', ' ') LINK_FLAG,
WOS1.TOTAL_FTD_OUT_ORDERS, 
WOS1.TOTAL_FTD_IN_ORDERS,
( WOS1.TOTAL_FTD_OUT_ORDERS / decode(WOS1.TOTAL_FTD_IN_ORDERS,0,1,WOS1.TOTAL_FTD_IN_ORDERS))  AS PM_RATIO,
( WOS2.TOTAL_FTD_OUT_ORDERS / decode(WOS2.TOTAL_FTD_IN_ORDERS,0,1,WOS2.TOTAL_FTD_IN_ORDERS))  AS PM2_RATIO,
( WOS3.TOTAL_FTD_OUT_ORDERS / decode(WOS3.TOTAL_FTD_IN_ORDERS,0,1,WOS3.TOTAL_FTD_IN_ORDERS))  AS PM3_RATIO,
( WOS4.TOTAL_FTD_OUT_ORDERS / decode(WOS4.TOTAL_FTD_IN_ORDERS,0,1,WOS4.TOTAL_FTD_IN_ORDERS))  AS PM4_RATIO,
( WOS5.TOTAL_FTD_OUT_ORDERS / decode(WOS5.TOTAL_FTD_IN_ORDERS,0,1,WOS5.TOTAL_FTD_IN_ORDERS))  AS PM5_RATIO,
( WOS6.TOTAL_FTD_OUT_ORDERS / decode(WOS6.TOTAL_FTD_IN_ORDERS,0,1,WOS6.TOTAL_FTD_IN_ORDERS))  AS PM6_RATIO, 
Decode(:P_GROUPBY, 'CM',CM.Collection_Manager,AB.Division_Manager_Code) as GROUP_BY,
Decode(:P_GROUPBY, 'DM',CM.Collection_Manager,AB.Division_Manager_Code) as DISPLAY_MANAGER_CODE
FROM JDE_ADDRESS_BOOK AB, 
     JDE_CUSTOMER_MASTER CM, 
     JDE_AR_AGING_BALANCES AR,
     JDE_CUSTOMER_INFORMATION CI,
     JDE_PARENT_CHILD_BRIDGE PCB1,
     JDE_PARENT_CHILD_BRIDGE PCB2,
     JDE_WIRE_ORDER_SUMMARY WOS1, 
     JDE_WIRE_ORDER_SUMMARY WOS2, 
     JDE_WIRE_ORDER_SUMMARY WOS3, 
     JDE_WIRE_ORDER_SUMMARY WOS4, 
     JDE_WIRE_ORDER_SUMMARY WOS5, 
     JDE_WIRE_ORDER_SUMMARY WOS6   
WHERE  WOS1.COUNTRY_CODE = :P_COUNTRY_CODE
      AND ( WOS1.TOTAL_FTD_OUT_ORDERS / decode(WOS1.TOTAL_FTD_IN_ORDERS,0,1,WOS1.TOTAL_FTD_IN_ORDERS))  >= :P_RATIO
      AND WOS1.TOTAL_FTD_OUT_ORDERS >= :P_OUT_ORDER_TOTAL
      AND WOS1.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -1)
      AND WOS2.ADDRESS_NUMBER = WOS1.ADDRESS_NUMBER
      AND WOS2.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -2)
      AND WOS3.ADDRESS_NUMBER = WOS1.ADDRESS_NUMBER      
      AND WOS3.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -3)
      AND WOS4.ADDRESS_NUMBER = WOS1.ADDRESS_NUMBER
      AND WOS4.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -4)
      AND WOS5.ADDRESS_NUMBER = WOS1.ADDRESS_NUMBER
      AND WOS5.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -5)
      AND WOS6.ADDRESS_NUMBER = WOS1.ADDRESS_NUMBER 
      AND WOS6.EFFECTIVE_MONTH_DATE = ADD_MONTHS(TO_DATE((TO_CHAR(SYSDATE,'MM') || TO_CHAR(SYSDATE, 'YYYY')), 'MM/YYYY'), -6)
      AND WOS1.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND WOS1.ADDRESS_NUMBER = AR.ADDRESS_NUMBER (+)
      AND WOS1.ADDRESS_NUMBER = CM.ADDRESS_NUMBER 
      AND WOS1.ADDRESS_NUMBER = CI.ADDRESS_NUMBER 
      AND WOS1.ADDRESS_NUMBER = PCB2.CHILD_ADDRESS_NUMBER
      AND PCB2.PARENT_ADDRESS_NUMBER != '3999' 
      AND PCB2.TOP_MOST_FLAG = 'Y' 
      AND WOS1.ADDRESS_NUMBER = PCB1.PARENT_ADDRESS_NUMBER
      AND PCB1.LEVELS_BETWEEN = 0 
      AND AR.COMPANY(+) = '00000'
      AND AR.PARENT_CHILD_RELATIONSHIP (+) <> 'P'
ORDER BY GROUP_BY, NVL(AR.OPEN_AMOUNT, 0) DESC ]]>
      </select>
      <displayInfo x="0.59167" y="0.12842" width="1.89160" height="0.19995"/>
      <group name="G_MANAGER">
        <displayInfo x="0.51770" y="0.79443" width="2.04541" height="2.48145"
        />
        <dataItem name="GROUP_BY" datatype="vchar2" columnOrder="42"
         defaultWidth="100000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Group By">
          <dataDescriptor
           expression="Decode ( : P_GROUPBY , &apos;CM&apos; , CM.Collection_Manager , AB.Division_Manager_Code )"
           descriptiveExpression="GROUP_BY" order="27" width="10"/>
        </dataItem>
        <dataItem name="COLLECTION_MANAGER" datatype="vchar2" columnOrder="16"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Collection Manager" breakOrder="none">
          <dataDescriptor expression="CM.COLLECTION_MANAGER"
           descriptiveExpression="COLLECTION_MANAGER" order="16" width="10"/>
        </dataItem>
        <dataItem name="DISPLAY_MANAGER_CODE" datatype="vchar2"
         columnOrder="43" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Display Manager Code" breakOrder="none">
          <dataDescriptor
           expression="Decode ( : P_GROUPBY , &apos;DM&apos; , CM.Collection_Manager , AB.Division_Manager_Code )"
           descriptiveExpression="DISPLAY_MANAGER_CODE" order="28" width="10"
          />
        </dataItem>
        <summary name="CS_Total_Out_Orders" source="TOTAL_FTD_OUT_ORDERS"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Out Orders">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_In_Orders" source="TOTAL_FTD_IN_ORDERS"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total In Orders">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Last_Paid" source="LAST_PAID_AMOUNT"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Last Paid">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Open_Amount" source="OPEN_AMOUNT"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Open Amount">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Aging_1" source="AGING_AMOUNT_1"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Aging 1">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Aging_2" source="AGING_AMOUNT_2"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Aging 2" valueIfNull="0">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Aging_3" source="AGING_AMOUNT_3"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Aging 3">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Aging_4" source="AGING_AMOUNT_4"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Aging 4">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Total_Aging_5" source="AGING_AMOUNT_5"
         function="sum" width="20" precision="10" reset="G_MANAGER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Total Aging 5">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <formula name="CF_Total_Ratio" source="cf_total_ratioformula"
         datatype="number" width="20" precision="10" defaultWidth="0"
         defaultHeight="0" columnFlags="16" breakOrder="none">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </formula>
      </group>
      <group name="G_Order_Detail">
        <displayInfo x="0.26221" y="3.82458" width="2.54211" height="4.87402"
        />
        <dataItem name="OPEN_AMOUNT" oracleDatatype="number" columnOrder="22"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="3"
         defaultLabel="Open Amount" breakOrder="descending">
          <dataDescriptor expression="AR.OPEN_AMOUNT"
           descriptiveExpression="OPEN_AMOUNT" order="10"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="DIVISION_MANAGER_CODE" datatype="vchar2"
         columnOrder="30" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Division Manager Code"
         breakOrder="none">
          <dataDescriptor expression="AB.DIVISION_MANAGER_CODE"
           descriptiveExpression="DIVISION_MANAGER_CODE" order="2" width="3"/>
        </dataItem>
        <dataItem name="FIELD_ACCT_REPRESENTATIVE_CODE" datatype="vchar2"
         columnOrder="17" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Field Acct Representative Code"
         breakOrder="none">
          <dataDescriptor expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE"
           descriptiveExpression="FIELD_ACCT_REPRESENTATIVE_CODE" order="1"
           width="3"/>
        </dataItem>
        <dataItem name="SEND_ONLY_ORDER_GATHERER_CODE" datatype="vchar2"
         columnOrder="18" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Send Only Order Gatherer Code"
         breakOrder="none">
          <dataDescriptor expression="AB.SEND_ONLY_ORDER_GATHERER_CODE"
           descriptiveExpression="SEND_ONLY_ORDER_GATHERER_CODE" order="3"
           width="3"/>
        </dataItem>
        <dataItem name="LINK_FLAG" datatype="vchar2" columnOrder="41"
         width="1" defaultWidth="10000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Link Flag" breakOrder="none">
          <dataDescriptor
           expression="decode ( pcb1.bottom_most_flag , &apos;N&apos; , &apos;Y&apos; , &apos; &apos; )"
           descriptiveExpression="LINK_FLAG" order="18" width="1"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="28" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Long Address Number" breakOrder="none">
          <dataDescriptor expression="AB.LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="5" width="20"/>
        </dataItem>
        <dataItem name="NAME" datatype="vchar2" columnOrder="29" width="30"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Name" breakOrder="none">
          <dataDescriptor
           expression="substr ( ltrim ( AB.ALPHA_NAME , &apos; &apos; ) , 1 , 30 )"
           descriptiveExpression="NAME" order="6" width="30"/>
        </dataItem>
        <dataItem name="OUTGOING_RANK" oracleDatatype="number"
         columnOrder="34" width="22" defaultWidth="70000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Outgoing Rank"
         breakOrder="none">
          <dataDescriptor expression="CI.OUTGOING_RANK"
           descriptiveExpression="OUTGOING_RANK" order="7"
           oracleDatatype="number" width="22" precision="5"/>
        </dataItem>
        <dataItem name="TOTAL_FTD_OUT_ORDERS" oracleDatatype="number"
         columnOrder="32" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Total Ftd Out Orders" breakOrder="none">
          <dataDescriptor expression="WOS1.TOTAL_FTD_OUT_ORDERS"
           descriptiveExpression="TOTAL_FTD_OUT_ORDERS" order="19"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="TOTAL_FTD_IN_ORDERS" oracleDatatype="number"
         columnOrder="33" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Total Ftd In Orders" breakOrder="none">
          <dataDescriptor expression="WOS1.TOTAL_FTD_IN_ORDERS"
           descriptiveExpression="TOTAL_FTD_IN_ORDERS" order="20"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="PM_RATIO" oracleDatatype="number" columnOrder="35"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS1.TOTAL_FTD_OUT_ORDERS / decode ( WOS1.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS1.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM_RATIO" order="21" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="PM2_RATIO" oracleDatatype="number" columnOrder="36"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm2 Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS2.TOTAL_FTD_OUT_ORDERS / decode ( WOS2.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS2.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM2_RATIO" order="22"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PM3_RATIO" oracleDatatype="number" columnOrder="37"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm3 Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS3.TOTAL_FTD_OUT_ORDERS / decode ( WOS3.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS3.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM3_RATIO" order="23"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PM4_RATIO" oracleDatatype="number" columnOrder="38"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm4 Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS4.TOTAL_FTD_OUT_ORDERS / decode ( WOS4.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS4.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM4_RATIO" order="24"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PM5_RATIO" oracleDatatype="number" columnOrder="39"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm5 Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS5.TOTAL_FTD_OUT_ORDERS / decode ( WOS5.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS5.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM5_RATIO" order="25"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PM6_RATIO" oracleDatatype="number" columnOrder="40"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pm6 Ratio" breakOrder="none">
          <dataDescriptor
           expression="( WOS6.TOTAL_FTD_OUT_ORDERS / decode ( WOS6.TOTAL_FTD_IN_ORDERS , 0 , 1 , WOS6.TOTAL_FTD_IN_ORDERS ) )"
           descriptiveExpression="PM6_RATIO" order="26"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="DATE_LAST_PAID" datatype="vchar2" columnOrder="20"
         width="8" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Date Last Paid" breakOrder="none">
          <dataDescriptor
           expression="to_char ( CM.DATE_LAST_PAID , &apos;MM/DD/YY&apos; )"
           descriptiveExpression="DATE_LAST_PAID" order="8" width="8"/>
        </dataItem>
        <dataItem name="LAST_PAID_AMOUNT" oracleDatatype="number"
         columnOrder="21" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Last Paid Amount"
         breakOrder="none">
          <dataDescriptor expression="CM.LAST_PAID_AMOUNT"
           descriptiveExpression="LAST_PAID_AMOUNT" order="9"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AB_ADDRESS_NUMBER" oracleDatatype="number"
         columnOrder="31" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Ab Address Number" breakOrder="none">
          <xmlSettings xmlTag="ADDRESS_NUMBER1"/>
          <dataDescriptor expression="AB.ADDRESS_NUMBER"
           descriptiveExpression="AB_ADDRESS_NUMBER" order="4"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="ADDRESS_NUMBER" oracleDatatype="number"
         columnOrder="19" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Address Number"
         breakOrder="none">
          <dataDescriptor expression="WOS1.ADDRESS_NUMBER"
           descriptiveExpression="ADDRESS_NUMBER" order="17"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="AGING_AMOUNT_1" oracleDatatype="number"
         columnOrder="23" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging Amount 1"
         breakOrder="none">
          <dataDescriptor
           expression="( AR.CURRENT_AMOUNT_DUE + AR.FUTURE_AMOUNT_DUE + AR.AGING_AMOUNT_1 )"
           descriptiveExpression="AGING_AMOUNT_1" order="11"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="AGING_AMOUNT_2" oracleDatatype="number"
         columnOrder="24" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging Amount 2"
         breakOrder="none">
          <dataDescriptor expression="AR.AGING_AMOUNT_2"
           descriptiveExpression="AGING_AMOUNT_2" order="12"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_AMOUNT_3" oracleDatatype="number"
         columnOrder="25" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging Amount 3"
         breakOrder="none">
          <dataDescriptor expression="AR.AGING_AMOUNT_3"
           descriptiveExpression="AGING_AMOUNT_3" order="13"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_AMOUNT_4" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging Amount 4"
         breakOrder="none">
          <dataDescriptor expression="AR.AGING_AMOUNT_4"
           descriptiveExpression="AGING_AMOUNT_4" order="14"
           oracleDatatype="number" width="22" scale="2" precision="15"/>
        </dataItem>
        <dataItem name="AGING_AMOUNT_5" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Aging Amount 5"
         breakOrder="none">
          <dataDescriptor
           expression="( AR.AGING_AMOUNT_5 + AR.AGING_AMOUNT_6 + AR.AGING_AMOUNT_7 )"
           descriptiveExpression="AGING_AMOUNT_5" order="15"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_Manager_Heading" source="cf_manager_headingformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.04163" y="2.47913" width="1.29175" height="0.23962"/>
    </formula>
    <formula name="CF_Div_Coll_Hdg" source="cf_div_coll_hdgformula"
     datatype="character" width="4" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.20837" y="2.81250" width="1.11450" height="0.21875"/>
    </formula>
    <placeholder name="CP_Div_Coll_Hdg" datatype="character" width="4"
     precision="10" defaultWidth="0" defaultHeight="0" columnFlags="16">
      <displayInfo x="3.25000" y="3.22913" width="1.09375" height="0.19995"/>
    </placeholder>
    <formula name="CF_NO_DATA_MESSAGE" source="cf_no_data_messageformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.37500" y="1.11462" width="1.62500" height="0.19995"/>
    </formula>
    <formula name="CF_Title3" source="cf_title3formula" datatype="character"
     width="30" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="3.39587" y="2.02087" width="0.91663" height="0.20825"/>
    </formula>
    <summary name="CS_RECORD_COUNT" source="ADDRESS_NUMBER" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="3.40625" y="0.65625" width="1.42493" height="0.19995"/>
    </summary>
    <formula name="CF_Title1" source="cf_title_1formula" datatype="character"
     width="40" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="3.40637" y="1.41663" width="0.91663" height="0.19995"/>
    </formula>
    <formula name="CF_Title2" source="cf_title2formula" datatype="character"
     width="30" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="3.40637" y="1.70837" width="0.90613" height="0.19995"/>
    </formula>
    <formula name="CF_Month3_Title" source="cf_month3_titleformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.53125" y="2.02087" width="0.95837" height="0.19995"/>
    </formula>
    <formula name="CF_Month1_Title" source="cf_month1formula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.53137" y="1.44788" width="0.91663" height="0.20837"/>
    </formula>
    <formula name="CF_Month2_Title" source="cf_month2_titleformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.54163" y="1.72913" width="0.92712" height="0.19995"/>
    </formula>
    <formula name="CF_Month4_Title" source="cf_month4_titleformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.55212" y="2.30212" width="0.94788" height="0.19995"/>
    </formula>
    <formula name="CF_Month5_Title" source="cf_month5_titleformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.56250" y="2.56250" width="0.91675" height="0.19995"/>
    </formula>
    <formula name="CF_Month6_Title" source="cf_month6_titleformula"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.57300" y="2.85413" width="0.95825" height="0.19995"/>
    </formula>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.75000" height="6.56250">
      <location x="0.00000" y="0.81250"/>
      <frame name="M_G_ORDER_GATHERER_GRPFR">
        <geometryInfo x="0.00000" y="0.00000" width="13.75000"
         height="1.56250"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_MANAGER_GRPFR" source="G_MANAGER"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.68750" width="13.75000"
           height="0.62500"/>
          <generalLayout pageProtect="yes" verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <repeatingFrame name="R_G_ORDER_GATHERER_DETAIL"
           source="G_Order_Detail" printDirection="down" minWidowRecords="1"
           columnMode="no">
            <geometryInfo x="0.00000" y="0.68750" width="13.68750"
             height="0.31250"/>
            <generalLayout verticalElasticity="expand"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Fld_Acct_Rep_Code"
             source="FIELD_ACCT_REPRESENTATIVE_CODE" spacing="single"
             alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.00000" y="0.81250" width="0.18750"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Name" source="NAME" spacing="single"
             alignment="start">
              <font face="Arial" size="7"/>
              <geometryInfo x="1.75000" y="0.81250" width="2.18750"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Date_Last_Paid" source="DATE_LAST_PAID"
             spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="8.87500" y="0.81250" width="0.50000"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Last_Paid_Amount" source="LAST_PAID_AMOUNT"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="9.43750" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Open_Amount" source="OPEN_AMOUNT"
             formatMask="NNN,NNN,NN0" spacing="single" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="10.06250" y="0.81250" width="0.62500"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Aging_Amt_1" source="AGING_AMOUNT_1"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="10.81250" y="0.81250" width="0.56250"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:AGING_AMOUNT_1 = &apos;0&apos;)">
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="AGING_AMOUNT_1" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_aging_amt_1formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Aging_Amt_2" source="AGING_AMOUNT_2"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="11.43750" y="0.81250" width="0.56250"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:AGING_AMOUNT_2 = &apos;0&apos;)">
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="AGING_AMOUNT_2" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_aging_amt_2formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Aging_Amt_3" source="AGING_AMOUNT_3"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="12.06250" y="0.81250" width="0.56250"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:AGING_AMOUNT_3 = &apos;0&apos;)">
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="AGING_AMOUNT_3" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_aging_amt_3formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Aging_Amt_4" source="AGING_AMOUNT_4"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="12.68750" y="0.81250" width="0.50000"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:AGING_AMOUNT_4 = &apos;0&apos;)">
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="AGING_AMOUNT_4" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_aging_amt_4formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Aging_Amt_5" source="AGING_AMOUNT_5"
             formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="13.25000" y="0.81250" width="0.43750"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:AGING_AMOUNT_5 = &apos;0&apos;)">
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="AGING_AMOUNT_5" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_aging_amt_5formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Long_member_number" source="LONG_ADDRESS_NUMBER"
             alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="1.06250" y="0.81250" width="0.62500"
               height="0.12500"/>
              <visualSettings lineWidth="1" fillBackgroundColor="gray28"/>
            </field>
            <field name="F_Division_Collection_Manager"
             source="DISPLAY_MANAGER_CODE" alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.25000" y="0.81250" width="0.25000"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="r88g0b75"/>
            </field>
            <field name="F_SO_Order_Gatherer_Code"
             source="SEND_ONLY_ORDER_GATHERER_CODE" alignment="start">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.50000" y="0.81250" width="0.25000"
               height="0.12500"/>
              <visualSettings lineWidth="1" fillBackgroundColor="gray28"/>
            </field>
            <field name="F_Outgoing_Rank" source="OUTGOING_RANK"
             alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="4.00000" y="0.81250" width="0.23950"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Out_Orders" source="TOTAL_FTD_OUT_ORDERS"
             formatMask="(NNN,NNN)" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="4.31250" y="0.81250" width="0.50696"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="green"/>
            </field>
            <field name="F_In_orders" source="TOTAL_FTD_IN_ORDERS"
             formatMask="(NNN,NN0)" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="4.87500" y="0.81250" width="0.43750"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="green"/>
            </field>
            <field name="F_Month1_Ratio" source="PM_RATIO"
             formatMask="NNNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="5.37500" y="0.81250" width="0.56250"
               height="0.12500"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
            </field>
            <field name="F_Month2_Ratio" source="PM2_RATIO"
             formatMask="NNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="6.00000" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Month3_Ratio" source="PM3_RATIO"
             formatMask="NNNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="6.56250" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Month4_Ratio" source="PM4_RATIO"
             formatMask="NNNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="7.12500" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Month5_Ratio" source="PM5_RATIO"
             formatMask="NNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="7.68750" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Month6_Ratio" source="PM6_RATIO"
             formatMask="NNNN0.00" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="8.25000" y="0.81250" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_LINK_FLAG" source="LINK_FLAG" spacing="single"
             alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.81250" y="0.81250" width="0.18750"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </repeatingFrame>
          <frame name="M_G_TOTALS">
            <geometryInfo x="0.00000" y="1.00000" width="13.68750"
             height="0.31250"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
            <text name="B_33">
              <geometryInfo x="1.43750" y="1.12500" width="0.37500"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="r50g88b100"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Totals:]]>
                </string>
              </textSegment>
            </text>
            <field name="F_Total_Last_Paid" source="CS_Total_Last_Paid"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="9.25000" y="1.12500" width="0.75000"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Open_Amount" source="CS_Total_Open_Amount"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="10.06250" y="1.12500" width="0.62500"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Aging_1" source="CS_Total_Aging_1"
             formatMask="$NNN,NNN,NN0" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="10.75000" y="1.12500" width="0.62500"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Aging_2" source="CS_Total_Aging_2"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="11.43750" y="1.12500" width="0.56250"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:CS_Total_Aging_2 = &apos;0&apos;)"
                    >
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="CS_Total_Aging_2" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_total_aging_2formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Aging_3" source="CS_Total_Aging_3"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="12.06250" y="1.12500" width="0.56250"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:CS_Total_Aging_3 = &apos;0&apos;)"
                    >
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="CS_Total_Aging_3" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_total_aging_3formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Aging_4" source="CS_Total_Aging_4"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="12.68750" y="1.12500" width="0.50000"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:CS_Total_Aging_4 = &apos;0&apos;)"
                    >
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="CS_Total_Aging_4" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_total_aging_4formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Aging_5" source="CS_Total_Aging_5"
             formatMask="$NNN,NNN,NN0" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="13.18750" y="1.12500" width="0.50000"
               height="0.12500"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:CS_Total_Aging_5 = &apos;0&apos;)"
                    >
                    <font face="Arial" size="6"/>
                    <formatVisualSettings fillPattern="transparent"/>
                  <cond name="first" column="CS_Total_Aging_5" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_total_aging_5formattrigger"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Manager" source="GROUP_BY" alignment="center">
              <font face="Arial" size="7"/>
              <geometryInfo x="1.06250" y="1.12500" width="0.31250"
               height="0.12500"/>
              <visualSettings fillBackgroundColor="r50g88b100"/>
            </field>
            <line name="B_29" arrow="none">
              <geometryInfo x="9.50000" y="1.05212" width="4.18750"
               height="0.00000"/>
              <visualSettings lineWidth="1" fillPattern="transparent"
               linePattern="solid"/>
              <points>
                <point x="9.50000" y="1.05212"/>
                <point x="13.68750" y="1.05212"/>
              </points>
            </line>
            <field name="F_Total_In_Orders" source="CS_Total_In_Orders"
             formatMask="NNN,NN0" spacing="single" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="4.91675" y="1.12500" width="0.36462"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Total_Ratio" source="CF_Total_Ratio"
             formatMask="NNNN0.00" spacing="single" alignment="end">
              <font face="Arial" size="7"/>
              <geometryInfo x="5.37500" y="1.12500" width="0.56250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <line name="B_30" arrow="none">
              <geometryInfo x="4.43750" y="1.06250" width="1.50000"
               height="0.00000"/>
              <visualSettings lineWidth="1" fillPattern="transparent"
               linePattern="solid"/>
              <points>
                <point x="4.43750" y="1.06250"/>
                <point x="5.93750" y="1.06250"/>
              </points>
            </line>
            <field name="F_Total_Out_Orders" source="CS_Total_Out_Orders"
             formatMask="NNN,NN0" spacing="single" alignment="right">
              <font face="Arial" size="7"/>
              <geometryInfo x="4.28137" y="1.12500" width="0.50696"
               height="0.12500"/>
              <visualSettings lineWidth="1" fillPattern="transparent"/>
            </field>
            <field name="F_Manager_Heading" source="CF_Manager_Heading"
             spacing="single" alignment="start">
              <font face="Arial" size="7"/>
              <geometryInfo x="0.05212" y="1.12500" width="0.93750"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </frame>
        </repeatingFrame>
        <field name="F_NO_DATA_MESSAGE" source="CF_NO_DATA_MESSAGE"
         spacing="single" alignment="center">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.50000" y="1.37500" width="3.06250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <frame name="M_G_Order_Gatherer_Hdr">
          <geometryInfo x="0.00000" y="0.00000" width="13.69446"
           height="0.50000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"
           formatTrigger="m_g_order_gatherer_hdrformattr"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_1">
            <geometryInfo x="0.00000" y="0.12500" width="0.25000"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Terr
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[  #]]>
              </string>
            </textSegment>
          </text>
          <text name="B_2">
            <textSettings justify="center"/>
            <geometryInfo x="0.18750" y="0.26038" width="0.31250"
             height="0.17712"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Mgr]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <textSettings justify="center"/>
            <geometryInfo x="0.50000" y="0.12500" width="0.25000"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[OG Code]]>
              </string>
            </textSegment>
          </text>
          <text name="B_4">
            <geometryInfo x="0.81250" y="0.12500" width="0.25000"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Link
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Flag]]>
              </string>
            </textSegment>
          </text>
          <text name="B_5">
            <textSettings justify="center"/>
            <geometryInfo x="1.12500" y="0.12500" width="0.43750"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Member Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_6">
            <textSettings justify="center"/>
            <geometryInfo x="1.75000" y="0.25000" width="2.06250"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Shop Name]]>
              </string>
            </textSegment>
          </text>
          <text name="B_7">
            <textSettings justify="center"/>
            <geometryInfo x="4.00000" y="0.25000" width="0.30212"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Rank]]>
              </string>
            </textSegment>
          </text>
          <text name="B_8">
            <textSettings justify="center"/>
            <geometryInfo x="4.43750" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Out Orders]]>
              </string>
            </textSegment>
          </text>
          <text name="B_9">
            <textSettings justify="center"/>
            <geometryInfo x="5.00000" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[In Orders]]>
              </string>
            </textSegment>
          </text>
          <text name="B_10">
            <textSettings justify="center"/>
            <geometryInfo x="5.62500" y="0.25000" width="0.31250"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <text name="B_11">
            <textSettings justify="center"/>
            <geometryInfo x="6.12500" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <text name="B_12">
            <textSettings justify="center"/>
            <geometryInfo x="6.68750" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <text name="B_13">
            <textSettings justify="center"/>
            <geometryInfo x="7.25000" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <text name="B_14">
            <textSettings justify="center"/>
            <geometryInfo x="7.81250" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <text name="B_16">
            <textSettings justify="center"/>
            <geometryInfo x="8.37500" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Ratio]]>
              </string>
            </textSegment>
          </text>
          <line name="B_17" arrow="none">
            <geometryInfo x="0.00000" y="0.43750" width="13.68750"
             height="0.00000"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             fillBackgroundColor="black" linePattern="solid"/>
            <points>
              <point x="0.00000" y="0.43750"/>
              <point x="13.68750" y="0.43750"/>
            </points>
          </line>
          <text name="B_18">
            <textSettings justify="center"/>
            <geometryInfo x="8.87500" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Last Paid]]>
              </string>
            </textSegment>
          </text>
          <text name="B_19">
            <textSettings justify="center"/>
            <geometryInfo x="9.50000" y="0.12500" width="0.50000"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Last Paid Amount]]>
              </string>
            </textSegment>
          </text>
          <text name="B_20">
            <textSettings justify="center"/>
            <geometryInfo x="10.25000" y="0.12500" width="0.47913"
             height="0.31250"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[Open Amount]]>
              </string>
            </textSegment>
          </text>
          <text name="B_21">
            <textSettings justify="center"/>
            <geometryInfo x="11.00000" y="0.25000" width="0.43750"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[0 - 30]]>
              </string>
            </textSegment>
          </text>
          <text name="B_22">
            <textSettings justify="center"/>
            <geometryInfo x="11.56250" y="0.25000" width="0.43750"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[31 - 60]]>
              </string>
            </textSegment>
          </text>
          <text name="B_23">
            <textSettings justify="center"/>
            <geometryInfo x="12.18750" y="0.25000" width="0.43750"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[61 - 90]]>
              </string>
            </textSegment>
          </text>
          <text name="B_25">
            <textSettings justify="center"/>
            <geometryInfo x="12.75000" y="0.25000" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[91 - 120]]>
              </string>
            </textSegment>
          </text>
          <text name="B_26">
            <textSettings justify="center"/>
            <geometryInfo x="13.31250" y="0.25000" width="0.37500"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[> 120]]>
              </string>
            </textSegment>
          </text>
          <text name="B_27">
            <textSettings justify="center"/>
            <geometryInfo x="4.37500" y="0.03125" width="0.43750"
             height="0.15625"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[*********]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Prior_Month" source="CF_Month1_Title"
           alignment="center">
            <font face="Arial" size="7"/>
            <geometryInfo x="4.87500" y="0.00000" width="0.50000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <text name="B_28">
            <textSettings justify="center"/>
            <geometryInfo x="5.50000" y="0.04163" width="0.37500"
             height="0.14587"/>
            <visualSettings fillBackgroundColor="yellow"/>
            <textSegment>
              <font face="Arial" size="7"/>
              <string>
              <![CDATA[*********]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Month2_Title" source="CF_Month2_Title"
           alignment="center">
            <font face="Arial" size="7"/>
            <geometryInfo x="6.12500" y="0.12500" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
          </field>
          <field name="F_Month3_Title" source="CF_Month3_Title"
           alignment="center">
            <font face="Arial" size="7"/>
            <geometryInfo x="6.68750" y="0.12500" width="0.50000"
             height="0.12500"/>
            <visualSettings fillBackgroundColor="yellow"/>
          </field>
          <field name="F_Month4_Title" source="CF_Month4_Title"
           alignment="center">
            <font face="Arial" size="7"/>
            <geometryInfo x="7.25000" y="0.12500" width="0.50000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Month5_Title" source="CF_Month5_Title"
           alignment="right">
            <font face="Arial" size="7"/>
            <geometryInfo x="7.81250" y="0.12500" width="0.50000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Month6_Title" source="CF_Month6_Title"
           alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="8.37500" y="0.12500" width="0.50000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Div_Coll_Hdg" source="CP_Div_Coll_Hdg"
           alignment="center">
            <font face="Arial" size="7"/>
            <geometryInfo x="0.25000" y="0.12488" width="0.18750"
             height="0.16663"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </frame>
      </frame>
    </body>
    <margin>
      <text name="B_24">
        <textSettings justify="center"/>
        <geometryInfo x="5.50000" y="0.25000" width="2.93750" height="0.50000"
        />
        <visualSettings fillBackgroundColor="r88g50b75"/>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[&<CF_TITLE1>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[&<CF_TITLE2>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[&<CF_TITLE3>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[ 
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[ ]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Arial" size="9"/>
        <geometryInfo x="0.56250" y="0.18750" width="0.75000" height="0.15625"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_PAGENUM1">
        <textSettings justify="center"/>
        <geometryInfo x="5.33337" y="8.06250" width="3.07288" height="0.26038"
        />
        <textSegment>
          <font face="Arial" size="9"/>
          <string>
          <![CDATA[Page &<PageNumber> of &<TotalPages>]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_no_data_messageformula" returnType="character">
      <textSource>
      <![CDATA[function CF_NO_DATA_MESSAGEFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	
end;]]>
      </textSource>
    </function>
    <function name="cf_title_1formula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_1Formula return Char is
 lv_title1 varchar2(40);
 lv_country varchar2(3);
begin
	if :p_country_code = 'US' then
		lv_country := 'USD';
	else
		lv_country := 'CAD';
	end if;	
  lv_title1 := ('Order Gatherer History Report - '||lv_country);
return lv_title1;
end;]]>
      </textSource>
    </function>
    <function name="cf_title2formula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title2Formula return Char is
  lv_title2 varchar2(30);
begin
  lv_title2 := (:P_OUT_ORDER_TOTAL|| ' Orders Out or Greater');

return lv_title2; 

end;]]>
      </textSource>
    </function>
    <function name="cf_title3formula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title3Formula return Char is
  lv_title3 varchar2(30);
begin
  lv_title3 := (to_char(to_number(:P_Ratio),'99.00')||' : 1 or Greater Ratio');
  
return lv_title3;
end;]]>
      </textSource>
    </function>
    <function name="cf_month2_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month2_titleFormula return Char is
  lv_month2 varchar2(20);
begin
  lv_month2 := to_char(add_months(sysdate, - 2),'Month');
 return lv_month2;
end;]]>
      </textSource>
    </function>
    <function name="cf_month1formula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month1Formula return Char is
  lv_month1 varchar2(20);
begin
  lv_month1 := to_char(add_months(sysdate, - 1),'Month');
 return lv_month1;
end;]]>
      </textSource>
    </function>
    <function name="cf_month3_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month3_titleFormula return Char is
	lv_month3 varchar2(20);
begin
  lv_month3 := to_char(add_months(sysdate, - 3),'Month');
    
 return lv_month3; 
end;]]>
      </textSource>
    </function>
    <function name="cf_month4_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month4_TitleFormula return Char is
  lv_month4 varchar(20);
begin
   lv_month4 := to_char(add_months(sysdate, - 4),'Month');
   return lv_month4;   
end;]]>
      </textSource>
    </function>
    <function name="cf_month5_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month5_TitleFormula return Char is
 lv_month5 varchar2(20);
begin
   lv_month5 := to_char(add_months(sysdate, -5),'Month');
 
 return lv_month5;
end;]]>
      </textSource>
    </function>
    <function name="cf_month6_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Month6_TitleFormula return Char is
 lv_month6 varchar2(20);
begin
   lv_month6 := to_char(add_months(sysdate, - 6),'Month');
   
 return lv_month6;  
end;]]>
      </textSource>
    </function>
    <function name="cf_total_ratioformula" returnType="number">
      <textSource>
      <![CDATA[function CF_Total_RatioFormula return Number is
  lv_temp_in_orders number;
 begin
 	if :cs_total_in_orders = 0 then 
 		lv_temp_in_orders := 1;
 		return (:cs_total_out_orders / lv_temp_in_orders);
 	end if; 		
  return (:cs_total_out_orders / :cs_total_in_orders);
end;]]>
      </textSource>
    </function>
    <function name="f_total_aging_2formattrigger">
      <textSource>
      <![CDATA[function F_Total_Aging_2FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Total_Aging_2 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_aging_3formattrigger">
      <textSource>
      <![CDATA[function F_Total_Aging_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Total_Aging_3 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_aging_amt_2formattrigger">
      <textSource>
      <![CDATA[function F_Aging_Amt_2FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AGING_AMOUNT_2 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_aging_amt_3formattrigger">
      <textSource>
      <![CDATA[function F_Aging_Amt_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AGING_AMOUNT_3 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_aging_amt_4formattrigger">
      <textSource>
      <![CDATA[function F_Aging_Amt_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AGING_AMOUNT_4 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_aging_amt_5formattrigger">
      <textSource>
      <![CDATA[function F_Aging_Amt_5FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AGING_AMOUNT_5 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_aging_amt_1formattrigger">
      <textSource>
      <![CDATA[function F_Aging_Amt_1FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:AGING_AMOUNT_1 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_aging_4formattrigger">
      <textSource>
      <![CDATA[function F_Total_Aging_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Total_Aging_4 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_aging_5formattrigger">
      <textSource>
      <![CDATA[function F_Total_Aging_5FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Total_Aging_5 = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="m_g_order_gatherer_hdrformattr">
      <textSource>
      <![CDATA[function M_G_Order_Gatherer_HdrFormatTr return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_manager_headingformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Manager_HeadingFormula return Char is
 lv_mgr_hdg varchar2(20);
begin
  if :P_GroupBy = 'CM' then
  	lv_mgr_hdg := 'Collection Manager';
  else
  	lv_mgr_hdg := 'Division Manager';
  end if;
return lv_mgr_hdg;  
  	
end;]]>
      </textSource>
    </function>
    <function name="cf_div_coll_hdgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Div_Coll_HdgFormula return Char is
begin
  if :p_GroupBy = 'DM' then
  	:cp_div_coll_hdg := 'Coll';
  else
    :cp_div_coll_hdg := 'Div';
  end if;
  return(0);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
