<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ProductSalesInventoryReport" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="PRODUCTSALESINVENTORYREPORT" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_CODIFIED_SYMBOL" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE1" datatype="character" width="40"
     initialValue="blank" validationTrigger="p_date1validtrigger"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_STATUS" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE2" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_CODIFIED_YEAR" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_SalesInventoryQuery">
      <select>
      <![CDATA[select
    ci.codified_symbol,
    ci.codified_item_number,
    pq.po_quantity as goal,
    sum(sd.quantity) as cartons,
    sum(sd.quantity * sd.unit_list_price) as revenue,
    (sum(sd.quantity) / pq.po_quantity)*100 as pct_sold,
    (pq.po_quantity - sum(sd.quantity)) as left_to_goal,
    pysd.codified_item_number as item_ly,
    pysd.cases_sold as cases_ly,
    pysd.revenue as revenue_ly
from
    ( select ci.codified_item_number as item_number, pqi.po_quantity
      from purchase_order_item_quantities pqi, codification_info ci
      where pqi.item_number = ci.component_item_number
      and ci.codified_year = :P_Codified_Year
      UNION ALL
      select ci.codified_item_number as item_number, pqi.po_quantity
      from purchase_order_item_quantities pqi, codification_info ci
      where ci.component_item_number is null
      and pqi.item_number = ci.codified_item_number
      and ci.codified_year = :P_Codified_Year  )  pq,
    jde_sales_order_detail sd,
    codification_info ci,
    (select
        ci.codified_symbol,
                                cly.codified_item_number,
                                ci.codified_item_number as curr_yr_item_number,
                                sum(sdly.quantity) as cases_sold,
                                sum(sdly.quantity * sdly.unit_list_price) as revenue
    from
        codification_info ci,
        codification_info cly,
        jde_sales_order_detail sdly
    where
        cly.codified_symbol = ci.codified_symbol and
        cly.codified_item_number = sdly.item_number and
        (ci.codified_symbol = :P_Codified_Symbol or upper(:P_Codified_Symbol) = 'ALL') and
        ci.codified_year = :P_Codified_Year and
        (cly.codified_symbol = :P_Codified_Symbol or upper(:P_Codified_Symbol) = 'ALL') and
        cly.codified_year = to_char(add_months(to_date(:P_Codified_Year,'YYYY'),-12),'YYYY') and
        sdly.order_date >= add_months(to_date(:P_Date1,'MM/DD/YYYY'),-12) and
        sdly.order_date <= add_months(to_date(:P_Date2,'MM/DD/YYYY'),-12) and
        sdly.status_code_last <= '600' and
        cly.codified_item_number <> 'N/A' and
        sdly.order_type in ('SO', 'SR') and
        sdly.sales_reporting_code_5 = 'M' and
        sdly.quantity > 0
    group by ci.codified_symbol, cly.codified_item_number, ci.codified_item_number) pysd
where
    ci.codified_item_number = pysd.curr_yr_item_number (+) and
    ci.codified_item_number = sd.item_number and
    ci.codified_item_number <> 'N/A' and
    (ci.codified_symbol = :P_Codified_Symbol or upper(:P_Codified_Symbol) = 'ALL') and
    ci.codified_year = :P_Codified_Year and
    sd.order_date >= to_date(:P_Date1,'MM/DD/YYYY') and
    sd.order_date <= to_date(:P_Date2,'MM/DD/YYYY') and
    ((upper(:P_Status) = 'OPEN' and sd.status_code_last < '600') or
    (upper(:P_Status) = 'CLOSED' and sd.status_code_last = '600') or
    (upper(:P_Status) = 'ALL' and sd.status_code_last <= '600')) and
    pq.item_number = ci.codified_item_number and
    sd.order_type in ('SO', 'SR') and
    sd.sales_reporting_code_5 = 'M' and
    sd.quantity > 0 
group by ci.codified_symbol, ci.codified_item_number, pq.po_quantity, pysd.codified_item_number,
    pysd.cases_sold, pysd.revenue]]>
      </select>
      <displayInfo x="0.16663" y="0.68750" width="1.54175" height="0.39575"/>
      <group name="G_codified_symbol">
        <displayInfo x="0.07178" y="1.42688" width="1.77197" height="3.16504"
        />
        <dataItem name="codified_symbol" datatype="vchar2" columnOrder="16"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Codified Symbol" breakOrder="none">
          <dataDescriptor expression="ci.codified_symbol"
           descriptiveExpression="CODIFIED_SYMBOL" order="1" width="10"/>
        </dataItem>
        <dataItem name="codified_item_number" datatype="vchar2"
         columnOrder="17" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Codified Item Number">
          <dataDescriptor expression="ci.codified_item_number"
           descriptiveExpression="CODIFIED_ITEM_NUMBER" order="2" width="10"/>
        </dataItem>
        <dataItem name="goal" oracleDatatype="number" columnOrder="18"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Goal" breakOrder="none">
          <dataDescriptor expression="pq.po_quantity"
           descriptiveExpression="GOAL" order="3" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="cartons" oracleDatatype="number" columnOrder="19"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Cartons" breakOrder="none">
          <dataDescriptor expression="sum ( sd.quantity )"
           descriptiveExpression="CARTONS" order="4" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="revenue" oracleDatatype="number" columnOrder="20"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Revenue" breakOrder="none">
          <dataDescriptor
           expression="sum ( sd.quantity * sd.unit_list_price )"
           descriptiveExpression="REVENUE" order="5" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="pct_sold" oracleDatatype="number" columnOrder="21"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Pct Sold" breakOrder="none">
          <dataDescriptor
           expression="( sum ( sd.quantity ) / pq.po_quantity ) * 100"
           descriptiveExpression="PCT_SOLD" order="6" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="left_to_goal" oracleDatatype="number" columnOrder="22"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Left To Goal" breakOrder="none">
          <dataDescriptor
           expression="( pq.po_quantity - sum ( sd.quantity ) )"
           descriptiveExpression="LEFT_TO_GOAL" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="item_ly" datatype="vchar2" columnOrder="23"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Item Ly" breakOrder="none">
          <dataDescriptor expression="pysd.codified_item_number"
           descriptiveExpression="ITEM_LY" order="8" width="10"/>
        </dataItem>
        <dataItem name="cases_ly" oracleDatatype="number" columnOrder="24"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Cases Ly" breakOrder="none">
          <dataDescriptor expression="pysd.cases_sold"
           descriptiveExpression="CASES_LY" order="9" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="revenue_ly" oracleDatatype="number" columnOrder="25"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Revenue Ly" breakOrder="none">
          <dataDescriptor expression="pysd.revenue"
           descriptiveExpression="REVENUE_LY" order="10"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_Header_Text_Line_2" source="cf_headertext_line_2"
     datatype="character" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.23950" y="3.72913" width="2.40625" height="0.31250"/>
    </formula>
    <formula name="CF_HeaderText" source="cf_headertextformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.25000" y="3.31250" width="2.40625" height="0.34375"/>
    </formula>
    <summary name="CS_Goal" source="goal" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Goal">
      <displayInfo x="3.66663" y="0.69800" width="0.84155" height="0.19995"/>
    </summary>
    <summary name="CS_Revenue" source="revenue" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Revenue">
      <displayInfo x="3.66663" y="1.37500" width="0.82300" height="0.19995"/>
    </summary>
    <summary name="CS_Pct_Sold" source="pct_sold" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Pct Sold">
      <displayInfo x="3.66663" y="1.72913" width="0.84375" height="0.20837"/>
    </summary>
    <summary name="CS_Left_to_Goal" source="left_to_goal" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Left To Goal">
      <displayInfo x="3.66663" y="2.12500" width="0.85425" height="0.19995"/>
    </summary>
    <summary name="CS_Cartons" source="cartons" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Cartons">
      <displayInfo x="3.67712" y="1.02087" width="0.79993" height="0.19995"/>
    </summary>
    <summary name="CS_Revenue_ly" source="revenue_ly" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Revenue Ly">
      <displayInfo x="3.67712" y="2.88538" width="0.89575" height="0.19995"/>
    </summary>
    <summary name="CS_Cases_ly" source="cases_ly" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40" defaultLabel="Cs Cases Ly">
      <displayInfo x="3.68750" y="2.51038" width="0.86462" height="0.19995"/>
    </summary>
    <formula name="CF_Percent_of_Goal" source="cf_percent_of_goalformula"
     datatype="number" width="20" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="6.44800" y="1.19800" width="1.37500" height="0.22913"/>
    </formula>
    <summary name="CS_Record_Count" source="codified_item_number"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="6.46875" y="1.68750" width="1.46875" height="0.23962"/>
    </summary>
    <formula name="CF_No_Data_Msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="6.46875" y="2.18750" width="1.54163" height="0.26038"/>
    </formula>
  </data>
  <layout>
  <section name="header" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.00000" height="6.50000"/>
  </section>
  <section name="trailer" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.00000" height="6.93750"/>
  </section>
  <section name="main" width="14.00000" height="8.50000"
   orientation="landscape">
    <body width="13.00000" height="6.43750">
      <location y="0.87500"/>
      <frame name="M_G_codified_symbol_GRPFR">
        <geometryInfo x="0.00000" y="0.00000" width="9.31250" height="0.68750"
        />
        <generalLayout verticalElasticity="expand"/>
        <visualSettings fillPattern="transparent"/>
        <frame name="M_G_codified_symbol_HDR">
          <geometryInfo x="0.00000" y="0.00000" width="9.31250"
           height="0.56250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"
           formatTrigger="m_g_codified_symbol_hdrformatt"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_codified_symbol">
            <textSettings justify="left"/>
            <geometryInfo x="0.00000" y="0.18750" width="1.25000"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Codified Symbol]]>
              </string>
            </textSegment>
          </text>
          <text name="B_codified_item_number">
            <textSettings justify="right"/>
            <geometryInfo x="1.12500" y="0.18750" width="0.93750"
             height="0.18750"/>
            <generalLayout horizontalElasticity="expand"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Item Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_goal">
            <textSettings justify="center"/>
            <geometryInfo x="2.31250" y="0.18750" width="0.37500"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Goal]]>
              </string>
            </textSegment>
          </text>
          <text name="B_cartons">
            <textSettings justify="center"/>
            <geometryInfo x="2.93750" y="0.18750" width="0.43750"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Cases]]>
              </string>
            </textSegment>
          </text>
          <text name="B_revenue">
            <textSettings justify="center"/>
            <geometryInfo x="3.68750" y="0.18750" width="0.68750"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Revenue]]>
              </string>
            </textSegment>
          </text>
          <text name="B_pct_sold">
            <textSettings justify="center"/>
            <geometryInfo x="4.75000" y="0.18750" width="0.50000"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[% Sold]]>
              </string>
            </textSegment>
          </text>
          <text name="B_left_to_goal">
            <textSettings justify="center"/>
            <geometryInfo x="5.50000" y="0.18750" width="0.87500"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Left To Sell]]>
              </string>
            </textSegment>
          </text>
          <text name="B_item_ly">
            <textSettings justify="center"/>
            <geometryInfo x="6.50000" y="0.18750" width="0.68750"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Item LY]]>
              </string>
            </textSegment>
          </text>
          <text name="B_cases_ly">
            <textSettings justify="center"/>
            <geometryInfo x="7.31250" y="0.18750" width="0.75000"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Cases LY]]>
              </string>
            </textSegment>
          </text>
          <text name="B_revenue_ly">
            <textSettings justify="center"/>
            <geometryInfo x="8.31250" y="0.18750" width="0.87500"
             height="0.18750"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Revenue LY]]>
              </string>
            </textSegment>
          </text>
        </frame>
      </frame>
      <repeatingFrame name="R_G_codified_symbol" source="G_codified_symbol"
       printDirection="down" minWidowRecords="1" columnMode="no">
        <geometryInfo x="0.00000" y="0.81250" width="9.37500" height="0.31250"
        />
        <generalLayout verticalElasticity="variable"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings fillPattern="transparent"/>
        <field name="F_codified_symbol" source="codified_symbol"
         alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="0.00000" y="0.87500" width="0.62500"
           height="0.18750"/>
        </field>
        <field name="F_codified_item_number" source="codified_item_number"
         alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="1.18750" y="0.87500" width="0.56250"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_goal" source="goal" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="2.00000" y="0.87500" width="0.62500"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_cartons" source="cartons" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="2.68750" y="0.87500" width="0.56250"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_revenue" source="revenue" formatMask="LNNNGNNNGNN0D00"
         alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="3.37500" y="0.87500" width="1.00000"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_pct_sold" source="pct_sold"
         formatMask="NNNNNNNNNNN0.00%" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="4.56250" y="0.87500" width="0.68750"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_left_to_goal" source="left_to_goal" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="5.43750" y="0.87500" width="0.68750"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_item_ly" source="item_ly" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="6.50000" y="0.87500" width="0.50000"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_cases_ly" source="cases_ly" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.25000" y="0.87500" width="0.56250"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
        <field name="F_revenue_ly" source="revenue_ly"
         formatMask="LNNNGNNNGNN0D00" alignment="right">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.87500" y="0.87500" width="1.18750"
           height="0.18750"/>
          <generalLayout horizontalElasticity="expand"/>
        </field>
      </repeatingFrame>
      <field name="F_tot_Goal" source="CS_Goal" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="1.93750" y="1.25000" width="0.68750" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <field name="F_tot_Cartons" source="CS_Cartons" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="2.68750" y="1.25000" width="0.56250" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <text name="B_4">
        <textSettings justify="center"/>
        <geometryInfo x="1.87500" y="2.00000" width="4.62500" height="0.14587"
        />
        <advancedLayout formatTrigger="b_4formattrigger"/>
        <textSegment>
          <font face="Arial" size="8"/>
          <string>
          <![CDATA[&<CF_No_Data_Msg>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_tot_Revenue" source="CS_Revenue"
       formatMask="LNNNGNNNGNN0D00" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="3.37500" y="1.25000" width="1.00000" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <field name="F_tot_Left_to_Goal" source="CS_Left_to_Goal"
       alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="5.43750" y="1.25000" width="0.68750" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <field name="F_tot_Cases_ly" source="CS_Cases_ly" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="7.25000" y="1.25000" width="0.56250" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <field name="F_tot_Revenue_ly" source="CS_Revenue_ly"
       formatMask="LNNNGNNNGNN0D00" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="7.87500" y="1.25000" width="1.18750" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
      <text name="B_2">
        <textSettings justify="center"/>
        <geometryInfo x="0.00000" y="1.25000" width="1.00000" height="0.18750"
        />
        <advancedLayout formatTrigger="b_2formattrigger"/>
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[Total Codified]]>
          </string>
        </textSegment>
      </text>
      <field name="F_Percent_of_Goal" source="CF_Percent_of_Goal"
       formatMask="NNNNNNNNNNN0.00%" alignment="right">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="4.56250" y="1.25000" width="0.68750" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
      </field>
    </body>
    <margin>
      <text name="B_PAGENUM1">
        <textSettings justify="center"/>
        <geometryInfo x="4.29163" y="7.33337" width="2.91663" height="0.16663"
        />
        <textSegment>
          <font face="Arial" size="10"/>
          <string>
          <![CDATA[Page &<PageNumber> of &<TotalPages>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/RRRR"
       alignment="left">
        <font face="Arial" size="10" bold="yes"/>
        <geometryInfo x="0.56250" y="0.43750" width="0.87500" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_1">
        <textSettings justify="center"/>
        <geometryInfo x="2.56250" y="0.43750" width="5.93750" height="0.16663"
        />
        <generalLayout horizontalElasticity="expand"/>
        <textSegment>
          <font face="Arial" size="10" bold="yes"/>
          <string>
          <![CDATA[&<CF_HeaderText>]]>
          </string>
        </textSegment>
      </text>
      <text name="B_3">
        <textSettings justify="center"/>
        <geometryInfo x="4.62500" y="0.62500" width="1.68750" height="0.16663"
        />
        <textSegment>
          <font face="Arial" size="10" bold="yes"/>
          <string>
          <![CDATA[&<CF_Header_Text_Line_2>]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_headertextformula" returnType="character">
      <textSource>
      <![CDATA[function CF_HeaderTextFormula return Char is
begin
  return('Product Sales vs. Inventory for ' || :P_DATE1 || ' to ' || :P_DATE2);
end;]]>
      </textSource>
    </function>
    <function name="p_date1validtrigger">
      <textSource>
      <![CDATA[function P_DATE1ValidTrigger return boolean is
begin
  
  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_headertext_line_2" returnType="character">
      <textSource>
      <![CDATA[function CF_HeaderText_Line_2 return char is
begin
  return(:P_Status || ' Orders');
end;]]>
      </textSource>
    </function>
    <function name="cf_percent_of_goalformula" returnType="number">
      <textSource>
      <![CDATA[function CF_Percent_of_GoalFormula return Number is
begin
  return (:cs_cartons/:cs_goal*100);
end;]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_MsgFormula return Char is
 lv_Message varchar2(80);
begin
  if :CS_Record_Count = 0 then
   lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;
  return lv_message;
end;]]>
      </textSource>
    </function>
    <function name="b_2formattrigger">
      <textSource>
      <![CDATA[function B_2FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count != '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="m_g_codified_symbol_hdrformatt">
      <textSource>
      <![CDATA[function M_G_codified_symbol_HDRFormatT return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate defaultReportType="tabular" templateName="rwbeige"
   sectionTitle="Product Sales vs. Inventory"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->