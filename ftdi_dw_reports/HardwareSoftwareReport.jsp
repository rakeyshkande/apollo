<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="HardwareSoftwareReport" DTDVersion="9.0.2.0.10"
 afterParameterFormTrigger="afterpform">
  <xmlSettings xmlTag="HARDWARESOFTWAREREPORTADD1" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_ORDER_BY" datatype="character" width="250"
     precision="10" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Hardware_Software_Report">
      <select>
      <![CDATA[SELECT ALL
substr(WW.MAILING_NAME,1,30) as shop_name,
AB.LONG_ADDRESS_NUMBER,
AB.MEMBER_STATUS,
AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' || TI.FBC_NAME as FBC_Info,
AB.DIVISION_MANAGER_CODE || ' ' ||  RI.MANAGER_NAME as DIV_Info,
SUM(MR.CURR_MTH_MERC_TOTAL_ACCESS) as Curr_Total_Access,
SUM(MR.PYR_MTH_MERC_TOTAL_ACCESS) as Pyr_Total_Access,
AB.TECHNOLOGY_PLATFORM,
SUM(MR.CURR_MTH_MERC_EQUIP_TRAIN) as Curr_Total_Equip_Train,
SUM(MR.PYR_MTH_MERC_EQUIP_TRAIN) as Pyr_Total_Equip_Train,
SUM(MR.CURR_MTH_MERC_TOTAL_ACCESS + MR.CURR_MTH_MERC_EQUIP_TRAIN) as HWSW_Curr_Total,  
SUM(MR.PYR_MTH_MERC_TOTAL_ACCESS + MR.PYR_MTH_MERC_EQUIP_TRAIN) as HWSW_Pyr_Total
FROM  JDE_MEMBER_REVENUE_SUMMARY MR,
            JDE_ADDRESS_BOOK AB,
            JDE_WHOS_WHO WW ,
            TERRITORY_INFO TI,
            REGION_INFO RI  
WHERE MR.ADDRESS_NUMBER = AB.ADDRESS_NUMBER  
      AND AB.SEARCH_TYPE in ('C' , 'I')
      AND ((:P_TERRITORY = 'All' or :P_DIVISION = 'All')
        OR  ((:P_TERRITORY = 'blank' or  AB.FIELD_ACCT_REPRESENTATIVE_CODE = :P_TERRITORY )
      AND  (:P_DIVISION = 'blank'  or  AB.DIVISION_MANAGER_CODE = :P_DIVISION)))
      AND MR.EFFECTIVE_MONTH_DATE >=  TO_DATE(:P_MONTH1 || '/01/' || :P_YEAR1, 'MM/DD/YYYY') 
      AND MR.EFFECTIVE_MONTH_DATE <=  TO_DATE(:P_MONTH2 || '/01/' || :P_YEAR2, 'MM/DD/YYYY') 
      AND TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
      AND RI.REGION = AB.DIVISION_MANAGER_CODE
      AND WW.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND WW.LINE_ID = 0
GROUP BY
  substr(WW.MAILING_NAME,1,30),
  AB.LONG_ADDRESS_NUMBER, 
  AB.MEMBER_STATUS,
  AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' || TI.FBC_NAME,
  AB.DIVISION_MANAGER_CODE || ' ' ||  RI.MANAGER_NAME,
  TECHNOLOGY_PLATFORM
&p_order_by ]]>
      </select>
      <displayInfo x="1.11060" y="0.05518" width="1.84998" height="0.19995"/>
      <group name="G_Report_Detail">
        <displayInfo x="0.81738" y="0.67908" width="2.44836" height="2.31055"
        />
        <dataItem name="shop_name" datatype="vchar2" columnOrder="20"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Shop Name">
          <dataDescriptor expression="substr ( WW.MAILING_NAME , 1 , 30 )"
           descriptiveExpression="SHOP_NAME" order="1" width="30"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="19" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Long Address Number" breakOrder="none">
          <dataDescriptor expression="AB.LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="2" width="20"/>
        </dataItem>
        <dataItem name="MEMBER_STATUS" datatype="vchar2" columnOrder="30"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="AB.MEMBER_STATUS"
           descriptiveExpression="MEMBER_STATUS" order="3" width="30"/>
        </dataItem>
        <dataItem name="FBC_Info" datatype="vchar2" columnOrder="21"
         width="34" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor
           expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE || &apos; &apos; || TI.FBC_NAME"
           descriptiveExpression="FBC_INFO" order="4" width="34"/>
        </dataItem>
        <dataItem name="DIV_Info" datatype="vchar2" columnOrder="22"
         width="34" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor
           expression="AB.DIVISION_MANAGER_CODE || &apos; &apos; || RI.MANAGER_NAME"
           descriptiveExpression="DIV_INFO" order="5" width="34"/>
        </dataItem>
        <dataItem name="Curr_Total_Access" oracleDatatype="number"
         columnOrder="23" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Curr Total Access" breakOrder="none">
          <dataDescriptor expression="SUM ( MR.CURR_MTH_MERC_TOTAL_ACCESS )"
           descriptiveExpression="CURR_TOTAL_ACCESS" order="6"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="Pyr_Total_Access" oracleDatatype="number"
         columnOrder="24" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Pyr Total Access"
         breakOrder="none">
          <xmlSettings xmlTag="PRY_TOTAL_ACCESS"/>
          <dataDescriptor expression="SUM ( MR.PYR_MTH_MERC_TOTAL_ACCESS )"
           descriptiveExpression="PYR_TOTAL_ACCESS" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="TECHNOLOGY_PLATFORM" datatype="vchar2"
         columnOrder="25" width="30" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Technology Platform" breakOrder="none">
          <dataDescriptor expression="AB.TECHNOLOGY_PLATFORM"
           descriptiveExpression="TECHNOLOGY_PLATFORM" order="8" width="30"/>
        </dataItem>
        <dataItem name="Curr_Total_Equip_Train" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Curr Total Equip Train" breakOrder="none">
          <dataDescriptor expression="SUM ( MR.CURR_MTH_MERC_EQUIP_TRAIN )"
           descriptiveExpression="CURR_TOTAL_EQUIP_TRAIN" order="9"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="Pyr_Total_Equip_Train" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Pyr Total Equip Train" breakOrder="none">
          <xmlSettings xmlTag="PRY_TOTAL_EQUIP_TRAIN"/>
          <dataDescriptor expression="SUM ( MR.PYR_MTH_MERC_EQUIP_TRAIN )"
           descriptiveExpression="PYR_TOTAL_EQUIP_TRAIN" order="10"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="HWSW_Pyr_Total" oracleDatatype="number"
         columnOrder="29" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Hwsw Pyr Total"
         breakOrder="none">
          <dataDescriptor
           expression="SUM ( MR.PYR_MTH_MERC_TOTAL_ACCESS + MR.PYR_MTH_MERC_EQUIP_TRAIN )"
           descriptiveExpression="HWSW_PYR_TOTAL" order="12"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="HWSW_Curr_Total" oracleDatatype="number"
         columnOrder="28" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Hwsw Curr Total"
         breakOrder="none">
          <dataDescriptor
           expression="SUM ( MR.CURR_MTH_MERC_TOTAL_ACCESS + MR.CURR_MTH_MERC_EQUIP_TRAIN )"
           descriptiveExpression="HWSW_CURR_TOTAL" order="11"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="CS_Total_Curr_Equip_Train" source="Curr_Total_Equip_Train"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Curr Equip Train">
      <displayInfo x="3.47888" y="2.01062" width="1.56250" height="0.21875"/>
    </summary>
    <summary name="CS_All_Members_Count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs All Members Count">
      <displayInfo x="3.48022" y="0.94788" width="1.55212" height="0.19995"/>
    </summary>
    <summary name="CS_Total_Pyr_Equip_Train" source="Pyr_Total_Equip_Train"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Pyr Equip Train">
      <displayInfo x="3.48938" y="2.40625" width="1.59375" height="0.19995"/>
    </summary>
    <summary name="CS_Total_Pyr_Access" source="Pyr_Total_Access"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Pyr Access">
      <displayInfo x="3.48950" y="1.66663" width="1.60425" height="0.20837"/>
    </summary>
    <summary name="CS_Total_Curr_Access" source="Curr_Total_Access"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Curr Access">
      <displayInfo x="3.49060" y="1.32288" width="1.63538" height="0.19995"/>
    </summary>
    <summary name="CS_Record_Count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="3.54187" y="0.21875" width="1.32275" height="0.21875"/>
    </summary>
    <formula name="CF_Title_Dates" source="cf_title_datesformula"
     datatype="character" width="40" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.58313" y="0.53125" width="1.33325" height="0.19995"/>
    </formula>
    <formula name="CF_NO_DATA_MESSAGE" source="cf_no_data_messageformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.10449" y="0.22888" width="1.52087" height="0.22925"/>
    </formula>
    <formula name="CF_Title_Terr_Reg" source="cf_terr_div_titleformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.20837" y="0.56250" width="1.45825" height="0.19995"/>
    </formula>
    <summary name="CS_Total_HWSW_Curr" source="HWSW_Curr_Total" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Hwsw Curr">
      <displayInfo x="5.32288" y="1.29163" width="1.58337" height="0.20837"/>
    </summary>
    <summary name="CS_Total_HWSW_Pyr" source="HWSW_Pyr_Total" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Hwsw Pyr">
      <displayInfo x="5.35413" y="1.64587" width="1.53125" height="0.20825"/>
    </summary>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="14.00000" height="7.00000">
      <location x="0.00000" y="0.75000"/>
      <frame name="M_G_HardwareSoftware_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="14.00000"
         height="1.50000"/>
        <generalLayout verticalElasticity="variable"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_1">
          <textSettings justify="center"/>
          <geometryInfo x="0.00000" y="0.37500" width="0.62500"
           height="0.18750"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Shop Name]]>
            </string>
          </textSegment>
        </text>
        <text name="B_2">
          <geometryInfo x="1.87500" y="0.25000" width="0.50000"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Member Number]]>
            </string>
          </textSegment>
        </text>
        <text name="B_3">
          <geometryInfo x="3.81250" y="0.37500" width="1.56250"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="P_DIVISION" exception="1"
               lowValue="blank" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_3formattrigger"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Field Business Consultant]]>
            </string>
          </textSegment>
        </text>
        <text name="B_4">
          <geometryInfo x="5.75000" y="0.37500" width="1.25000"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:P_DIVISION != &apos;All&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="P_DIVISION" exception="2"
               lowValue="All" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject" formatTrigger="b_4formattrigger"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Division Manager]]>
            </string>
          </textSegment>
        </text>
        <text name="B_6">
          <textSettings justify="center"/>
          <geometryInfo x="7.68750" y="0.06250" width="1.43750"
           height="0.15625"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Access, Support & Rental]]>
            </string>
          </textSegment>
        </text>
        <text name="B_7">
          <textSettings justify="center"/>
          <geometryInfo x="7.75000" y="0.25000" width="0.56250"
           height="0.28125"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Current Period]]>
            </string>
          </textSegment>
        </text>
        <text name="B_8">
          <textSettings justify="center"/>
          <geometryInfo x="8.43750" y="0.25000" width="0.56250"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Prior Year Period]]>
            </string>
          </textSegment>
        </text>
        <line name="B_9" arrow="none">
          <geometryInfo x="0.00000" y="0.50000" width="14.00000"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings lineWidth="1" fillPattern="transparent"
           linePattern="solid"/>
          <points>
            <point x="0.00000" y="0.50000"/>
            <point x="14.00000" y="0.50000"/>
          </points>
        </line>
        <text name="B_10">
          <textSettings justify="center"/>
          <geometryInfo x="10.81250" y="0.06250" width="1.31250"
           height="0.18750"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Equipment & Training ]]>
            </string>
          </textSegment>
        </text>
        <text name="B_11">
          <textSettings justify="center"/>
          <geometryInfo x="10.87500" y="0.25000" width="0.56250"
           height="0.28125"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Current Period]]>
            </string>
          </textSegment>
        </text>
        <text name="B_12">
          <textSettings justify="center"/>
          <geometryInfo x="11.56250" y="0.25000" width="0.62500"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Prior Year Period]]>
            </string>
          </textSegment>
        </text>
        <repeatingFrame name="R_G_Detail" source="G_Report_Detail"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.56250" width="14.00000"
           height="0.25000"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_SHOP_NAME" source="shop_name" spacing="single"
           alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="0.00000" y="0.62500" width="1.87500"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_LONG_ADDRESS_NUMBER" source="LONG_ADDRESS_NUMBER"
           spacing="single" alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="1.87500" y="0.62500" width="0.56250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_FBC_Info" source="FBC_Info" spacing="single"
           alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="3.81250" y="0.62500" width="1.87500"
             height="0.12500"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                  <font face="Arial" size="7" bold="yes"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" column="P_DIVISION" exception="1"
                 lowValue="blank" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="f_fbc_infoformattrigger"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_DIV_Info" source="DIV_Info" spacing="single"
           alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="5.75000" y="0.62500" width="1.87500"
             height="0.12500"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION != &apos;All&apos;)">
                  <font face="Arial" size="7" bold="yes"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" column="P_DIVISION" exception="2"
                 lowValue="All" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="f_div_infoformattrigger"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Curr_Total_Access" source="Curr_Total_Access"
           formatMask="NN,NNN.00" spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="7.68750" y="0.62500" width="0.56250"
             height="0.12500"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:Curr_Total_Access = &apos;0&apos;)">
                  <font face="Arial" size="9"/>
                  <formatVisualSettings fillPattern="transparent"/>
                <cond name="first" column="Curr_Total_Access" exception="1"
                 lowValue="0" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="f_curr_total_accessformattrigg"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Pyr_Total_Access" source="Pyr_Total_Access"
           formatMask="NN,NNN.00" spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="8.31250" y="0.62500" width="0.62500"
             height="0.12500"/>
            <advancedLayout formatTrigger="f_pyr_total_accessformattrigge"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Curr_Total_Equip_Train"
           source="Curr_Total_Equip_Train" formatMask="NNN,NNN.00"
           spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="10.75000" y="0.62500" width="0.68750"
             height="0.12500"/>
            <advancedLayout formatTrigger="f_curr_total_equip_trainformat"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Pyr_Total_Equip_Train" source="Pyr_Total_Equip_Train"
           formatMask="NNN,NNN.00" spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="11.50000" y="0.62500" width="0.68750"
             height="0.12500"/>
            <advancedLayout formatTrigger="f_pyr_total_equip_trainformatt"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Technology_Platform" source="TECHNOLOGY_PLATFORM"
           alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="9.06250" y="0.62500" width="1.62500"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_HWSW_Curr_Total" source="HWSW_Curr_Total"
           formatMask="NNN,NNN.00" spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="12.31250" y="0.62500" width="0.75000"
             height="0.12500"/>
            <advancedLayout formatTrigger="f_hwsw_curr_totalformattrigger"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_HWSW_Pyr_Total" source="HWSW_Pyr_Total"
           formatMask="NNN,NNN.00" spacing="single" alignment="end">
            <font face="Arial" size="7"/>
            <geometryInfo x="13.18750" y="0.62500" width="0.75000"
             height="0.12500"/>
            <advancedLayout formatTrigger="f_hwsw_pyr_totalformattrigger"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Member_Status" source="MEMBER_STATUS"
           spacing="single" alignment="start">
            <font face="Arial" size="7"/>
            <geometryInfo x="2.43750" y="0.62500" width="1.37500"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <frame name="M_Report_Totals">
          <geometryInfo x="0.00000" y="0.81250" width="14.00000"
           height="0.43750"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_15">
            <geometryInfo x="0.12500" y="1.00000" width="0.93750"
             height="0.15625"/>
            <advancedLayout formatTrigger="b_15formattrigger"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[Grand Totals]]>
              </string>
            </textSegment>
          </text>
          <text name="B_16">
            <textSettings justify="end"/>
            <geometryInfo x="1.12500" y="1.00000" width="1.06250"
             height="0.15625"/>
            <advancedLayout formatTrigger="b_16formattrigger"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[ Members Count:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Total_Curr_Trans_Count" source="CS_Total_Curr_Access"
           formatMask="$NN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="7.43750" y="1.00000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_Pyr_Trans_Count" source="CS_Total_Pyr_Access"
           formatMask="$NN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="8.31250" y="1.00000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_Curr_Trans_Rev"
           source="CS_Total_Curr_Equip_Train" formatMask="$NN,NNN,NNN.00"
           spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="10.56250" y="1.00000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_Pry_Trans_Rev"
           source="CS_Total_Pyr_Equip_Train" formatMask="$NN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="11.43750" y="1.00000" width="0.75000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_All_Members_Count" source="CS_All_Members_Count"
           formatMask="NNN,NNN" spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="2.37500" y="1.00000" width="0.75000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_HWSW_Curr" source="CS_Total_HWSW_Curr"
           formatMask="$NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="12.25000" y="1.00000" width="0.87500"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_HWSW_Pyr" source="CS_Total_HWSW_Pyr"
           formatMask="$NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="13.18750" y="1.00000" width="0.81250"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <line name="B_23" arrow="none">
            <geometryInfo x="7.50000" y="0.93750" width="1.62500"
             height="0.00000"/>
            <advancedLayout formatTrigger="b_23formattrigger"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             linePattern="solid"/>
            <points>
              <point x="7.50000" y="0.93750"/>
              <point x="9.12500" y="0.93750"/>
            </points>
          </line>
          <line name="B_24" arrow="none">
            <geometryInfo x="10.50000" y="1.00000" width="3.50000"
             height="0.00000"/>
            <advancedLayout formatTrigger="b_24formattrigger"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             linePattern="solid"/>
            <points>
              <point x="10.50000" y="1.00000"/>
              <point x="14.00000" y="1.00000"/>
            </points>
          </line>
        </frame>
        <text name="B_22">
          <textSettings justify="center"/>
          <geometryInfo x="5.37500" y="1.31250" width="3.25000"
           height="0.15625"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[&<CF_NO_DATA_MESSAGE>]]>
            </string>
          </textSegment>
        </text>
        <text name="B_5">
          <geometryInfo x="9.06250" y="0.37500" width="1.37500"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Technology Platform]]>
            </string>
          </textSegment>
        </text>
        <text name="B_19">
          <textSettings justify="center"/>
          <geometryInfo x="12.37500" y="0.25000" width="0.75000"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Total Current Period]]>
            </string>
          </textSegment>
        </text>
        <text name="B_20">
          <textSettings justify="center"/>
          <geometryInfo x="13.25000" y="0.25000" width="0.68750"
           height="0.31250"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Total Prior Year Period]]>
            </string>
          </textSegment>
        </text>
        <line name="B_18" arrow="none">
          <geometryInfo x="10.81250" y="0.18750" width="1.25000"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings lineWidth="1" fillPattern="transparent"
           linePattern="solid"/>
          <points>
            <point x="10.81250" y="0.18750"/>
            <point x="12.06250" y="0.18750"/>
          </points>
        </line>
        <line name="B_21" arrow="none">
          <geometryInfo x="7.68750" y="0.18750" width="1.37500"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings lineWidth="1" fillPattern="transparent"
           linePattern="solid"/>
          <points>
            <point x="7.68750" y="0.18750"/>
            <point x="9.06250" y="0.18750"/>
          </points>
        </line>
        <text name="B_25">
          <geometryInfo x="2.43750" y="0.37500" width="0.75000"
           height="0.12500"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <textSegment>
            <font face="Arial" size="7"/>
            <string>
            <![CDATA[Member Status]]>
            </string>
          </textSegment>
        </text>
      </frame>
      <text name="B_17">
        <textSettings justify="center"/>
        <geometryInfo x="6.43750" y="1.56250" width="1.00000" height="0.31250"
        />
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[End of Report
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="8"/>
          <string>
          <![CDATA[V1.5]]>
          </string>
        </textSegment>
      </text>
    </body>
    <margin>
      <text name="B_13">
        <textSettings justify="center"/>
        <geometryInfo x="5.75000" y="0.12500" width="2.43750" height="0.15625"
        />
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[Hardware / Software Revenue]]>
          </string>
        </textSegment>
      </text>
      <text name="B_14">
        <textSettings justify="center"/>
        <geometryInfo x="5.31250" y="0.31250" width="3.25000" height="0.37500"
        />
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[&<CF_TITLE_DATES>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[&<CF_TITLE_TERR_REG>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[
]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="0.18750" y="0.12500" width="0.68750" height="0.11462"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_no_data_messageformula" returnType="character">
      <textSource>
      <![CDATA[function CF_NO_DATA_MESSAGEFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	
end;]]>
      </textSource>
    </function>
    <function name="cf_title_datesformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_DatesFormula return Char is
lv_title_dates varchar2(40);
begin
lv_title_dates :=('From:  '||:P_MONTH1 ||' / '||:P_YEAR1 ||'   To:  '||:P_MONTH2 ||' / '|| :P_YEAR2);
  return (lv_title_dates);
end;]]>
      </textSource>
    </function>
    <function name="afterpform">
      <textSource>
      <![CDATA[function AfterPForm return boolean is
begin
  IF :P_ORDER_BY = 'SN' THEN
  	:P_ORDER_BY := 'ORDER BY substr(WW.MAILING_NAME,1,30)';
  ELSIF
  	:P_ORDER_BY = 'TP' THEN
  	  :P_ORDER_BY := 'ORDER BY AB.TECHNOLOGY_PLATFORM'; 
  END IF;	  
  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION = 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_infoformattrigger">
      <textSource>
      <![CDATA[function F_FBC_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION = 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_div_infoformattrigger">
      <textSource>
      <![CDATA[function F_DIV_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_terr_div_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Terr_Div_TitleFormula return Char is
  lv_title3 varchar2 (80) null;
  temp_title varchar2(80);
begin
 temp_title := title_name (:p_member, :p_territory, :p_division); 
 If :p_territory != 'blank' and :p_territory != 'All' then
 	 lv_title3 := 'Territory # '||:p_territory ||' - '|| temp_title;
 elsif :p_division != 'blank' and :p_division != 'All' then
 	lv_title3 := 'Region # ' ||:p_division|| ' - ' ||temp_title;
 else
 	lv_title3 := 'All Members';
 end if;
  return lv_title3;
end;
]]>
      </textSource>
    </function>
    <function name="f_curr_total_accessformattrigg">
      <textSource>
      <![CDATA[function F_Curr_Total_AccessFormatTrigg return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Curr_Total_Access = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_pyr_total_accessformattrigge">
      <textSource>
      <![CDATA[function F_Pyr_Total_AccessFormatTrigge return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Pyr_Total_Access = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_curr_total_equip_trainformat">
      <textSource>
      <![CDATA[function F_Curr_Total_Equip_TrainFormat return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Curr_Total_Equip_Train = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_pyr_total_equip_trainformatt">
      <textSource>
      <![CDATA[function F_Pyr_Total_Equip_TrainFormatT return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:Pyr_Total_Equip_Train = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_hwsw_curr_totalformattrigger">
      <textSource>
      <![CDATA[function F_HWSW_Curr_TotalFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:HWSW_Curr_Total = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_hwsw_pyr_totalformattrigger">
      <textSource>
      <![CDATA[function F_HWSW_Pyr_TotalFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:HWSW_Pyr_Total = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_15formattrigger">
      <textSource>
      <![CDATA[function B_15FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_All_Members_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_16formattrigger">
      <textSource>
      <![CDATA[function B_16FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_All_Members_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_23formattrigger">
      <textSource>
      <![CDATA[function B_23FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_All_Members_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_24formattrigger">
      <textSource>
      <![CDATA[function B_24FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_All_Members_Count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
