<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ParentChild" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="PARENTCHILD" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_MEMBER_CODE" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Parent_Child">
      <select canParse="no">
      <![CDATA[ SELECT /*+ NO_FACT(AB) */ 
        COALESCE (SUBSTR (ABP.LONG_ADDRESS_NUMBER, 1, 9), TO_CHAR (PCP.PARENT_ADDRESS_NUMBER))
AS PARENT_LONG_ADDRESS_NUMBER,
        DECODE (PCP.PARENT_SEARCH_TYPE, 'I', 'Internal Link: ', 'C', 'Parent Address Number: ', null) AS GRP_TITLE,
        SUBSTR (AB.LONG_ADDRESS_NUMBER, 1, 9) AS CHILD_LONG_ADDRESS_NUMBER,
        AB.ADDRESS_NUMBER,
        AB.DIVISION_MANAGER_CODE || '  ' || RI.MANAGER_NAME as DIV_Info, 
        AB.FIELD_ACCT_REPRESENTATIVE_CODE || '  ' || TI.FBC_NAME as FBC_Info,
        AB.MEMBER_STATUS,
       DECODE (UPPER(AB.MEMBER_STATUS),'ACTIVE',1,0) ACTIVE_COUNT,
       DECODE (UPPER(AB.MEMBER_STATUS),'ACTIVE',0,1)NON_ACTIVE_COUNT,
       substr( WW.MAILING_NAME,1,30) as alpha_name,
        PCC.LEVELS_BETWEEN
FROM JDE_PARENT_CHILD_BRIDGE PCT
JOIN JDE_PARENT_CHILD_BRIDGE PCC
        ON PCT.PARENT_ADDRESS_NUMBER = PCC.PARENT_ADDRESS_NUMBER AND PCT.TOP_MOST_FLAG = 'Y'
LEFT OUTER JOIN JDE_PARENT_CHILD_BRIDGE PCP
        ON PCC.CHILD_ADDRESS_NUMBER = PCP.CHILD_ADDRESS_NUMBER AND PCP.LEVELS_BETWEEN = 1
JOIN JDE_ADDRESS_BOOK AB
        ON PCC.CHILD_ADDRESS_NUMBER = AB.ADDRESS_NUMBER
        AND ((PCC.PARENT_SEARCH_TYPE = 'I' AND PCC.LEVELS_BETWEEN <> 0) OR (PCC.PARENT_SEARCH_TYPE = 'C'))
JOIN REGION_INFO RI
        ON RI.REGION = AB.DIVISION_MANAGER_CODE
JOIN TERRITORY_INFO TI
        ON TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
JOIN JDE_WHOS_WHO WW
        ON AB.ADDRESS_NUMBER = WW.ADDRESS_NUMBER AND WW.LINE_ID = 0
LEFT OUTER JOIN JDE_ADDRESS_BOOK ABP
        ON PCP.PARENT_ADDRESS_NUMBER = ABP.ADDRESS_NUMBER
WHERE PCT.CHILD_ADDRESS_NUMBER = GET_ADDRESS_NUMBER (:P_MEMBER_CODE)
ORDER BY PCC.LEVELS_BETWEEN, PARENT_LONG_ADDRESS_NUMBER, CHILD_LONG_ADDRESS_NUMBER;

]]>
      </select>
      <displayInfo x="1.15198" y="0.10767" width="1.69177" height="0.19995"/>
      <group name="G_PARENT_ADDRESS_NUMBER">
        <displayInfo x="0.76648" y="0.66626" width="2.48450" height="1.11426"
        />
        <dataItem name="Grp_title" datatype="vchar2" columnOrder="13"
         width="23" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Grp Title" breakOrder="none">
          <dataDescriptor expression="GRP_TITLE"
           descriptiveExpression="GRP_TITLE" order="2" width="23"/>
        </dataItem>
        <dataItem name="LEVELS_BETWEEN" oracleDatatype="number"
         columnOrder="19" width="22" defaultWidth="50000"
         defaultHeight="10000" columnFlags="1" defaultLabel="Levels Between">
          <dataDescriptor expression="LEVELS_BETWEEN"
           descriptiveExpression="LEVELS_BETWEEN" order="11"
           oracleDatatype="number" width="22" precision="3"/>
        </dataItem>
        <dataItem name="PARENT_LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="14" width="40" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Parent Long Address Number">
          <dataDescriptor expression="PARENT_LONG_ADDRESS_NUMBER"
           descriptiveExpression="PARENT_LONG_ADDRESS_NUMBER" order="1"
           width="40"/>
        </dataItem>
        <summary name="CS_Active_Total" source="ACTIVE_COUNT" function="sum"
         width="20" precision="10" reset="G_PARENT_ADDRESS_NUMBER"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Active Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_NonActive_Total" source="NON_ACTIVE_COUNT"
         function="sum" width="20" precision="10"
         reset="G_PARENT_ADDRESS_NUMBER" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Nonactive Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_CHILD_ADDRESS_NUMBER">
        <displayInfo x="0.78601" y="2.21912" width="2.44836" height="1.62695"
        />
        <dataItem name="ALPHA_NAME" datatype="vchar2" columnOrder="22"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Alpha Name">
          <dataDescriptor expression="ALPHA_NAME"
           descriptiveExpression="ALPHA_NAME" order="10" width="30"/>
        </dataItem>
        <dataItem name="DIV_INFO" datatype="vchar2" columnOrder="20"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor expression="DIV_INFO"
           descriptiveExpression="DIV_INFO" order="5" width="35"/>
        </dataItem>
        <dataItem name="FBC_INFO" datatype="vchar2" columnOrder="21"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor expression="FBC_INFO"
           descriptiveExpression="FBC_INFO" order="6" width="35"/>
        </dataItem>
        <dataItem name="NON_ACTIVE_COUNT" oracleDatatype="number"
         columnOrder="18" width="2" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Non Active Count" breakOrder="none">
          <dataDescriptor expression="NON_ACTIVE_COUNT"
           descriptiveExpression="NON_ACTIVE_COUNT" order="9"
           oracleDatatype="number" width="2" precision="38"/>
        </dataItem>
        <dataItem name="ACTIVE_COUNT" oracleDatatype="number" columnOrder="17"
         width="2" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Active Count" breakOrder="none">
          <dataDescriptor expression="ACTIVE_COUNT"
           descriptiveExpression="ACTIVE_COUNT" order="8"
           oracleDatatype="number" width="2" precision="38"/>
        </dataItem>
        <dataItem name="CHILD_LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="15" width="9" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Child Long Address Number"
         breakOrder="none">
          <dataDescriptor expression="CHILD_LONG_ADDRESS_NUMBER"
           descriptiveExpression="CHILD_LONG_ADDRESS_NUMBER" order="3"
           width="9"/>
        </dataItem>
        <dataItem name="ADDRESS_NUMBER" oracleDatatype="number"
         columnOrder="16" width="22" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1" defaultLabel="Address Number">
          <dataDescriptor expression="ADDRESS_NUMBER"
           descriptiveExpression="ADDRESS_NUMBER" order="4"
           oracleDatatype="number" width="22" precision="8"/>
        </dataItem>
        <dataItem name="MEMBER_STATUS" datatype="vchar2" columnOrder="12"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="MEMBER_STATUS"
           descriptiveExpression="MEMBER_STATUS" order="7" width="30"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="CS_Grand_Total_Active" source="ACTIVE_COUNT" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Grand Total Active">
      <displayInfo x="3.94788" y="0.38538" width="1.38550" height="0.19995"/>
    </summary>
    <summary name="CS_Grand_Total_Terminated" source="NON_ACTIVE_COUNT"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Grand Total Terminated">
      <displayInfo x="4.01038" y="0.80212" width="1.68750" height="0.26038"/>
    </summary>
    <summary name="CS_Record_count" source="ADDRESS_NUMBER" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="4.01038" y="1.21875" width="1.12500" height="0.26038"/>
    </summary>
    <formula name="CF_No_Data_Msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.07288" y="1.62500" width="1.13550" height="0.28125"/>
    </formula>
  </data>
  <layout>
  <section name="main">
    <body width="8.00000" height="9.50000">
      <location x="0.00000" y="0.68750"/>
      <repeatingFrame name="R_G_PARENT_ADDRESS_NUMBER"
       source="G_PARENT_ADDRESS_NUMBER" printDirection="down"
       minWidowRecords="1" columnMode="no" vertSpaceBetweenFrames="0.1250">
        <geometryInfo x="0.00000" y="0.06250" width="7.93750" height="1.18750"
        />
        <generalLayout pageProtect="yes" verticalElasticity="expand"/>
        <advancedLayout printObjectOnPage="firstPage"
         basePrintingOn="enclosingObject"/>
        <visualSettings fillPattern="transparent"/>
        <frame name="M_G_Parent_Child_Detail_Enclos">
          <geometryInfo x="0.00000" y="0.56250" width="7.93750"
           height="0.18750"/>
          <generalLayout verticalElasticity="variable"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"
           fillBackgroundColor="r50g88b75"/>
          <repeatingFrame name="R_G_Parent_Child_Detail"
           source="G_CHILD_ADDRESS_NUMBER" printDirection="down"
           minWidowRecords="1" columnMode="no">
            <geometryInfo x="0.00000" y="0.56250" width="7.93750"
             height="0.18750"/>
            <generalLayout verticalElasticity="expand"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Address_Number" source="ADDRESS_NUMBER"
             spacing="single" alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="0.00000" y="0.56250" width="0.75000"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Long_Address_Number"
             source="CHILD_LONG_ADDRESS_NUMBER" spacing="single"
             alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="0.93750" y="0.56250" width="0.87500"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Alpha_Name" source="ALPHA_NAME" spacing="single"
             alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="2.06250" y="0.56250" width="2.00000"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Member_Status" source="MEMBER_STATUS"
             spacing="single" alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="6.87500" y="0.56250" width="1.06250"
               height="0.12500"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Division_Info" source="DIV_INFO" spacing="single"
             alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="4.18750" y="0.56250" width="1.18750"
               height="0.12500"/>
            </field>
            <field name="F_Territory_Info" source="FBC_INFO" spacing="single"
             alignment="start">
              <font face="Arial" size="8"/>
              <geometryInfo x="5.50000" y="0.56250" width="1.31250"
               height="0.12500"/>
            </field>
          </repeatingFrame>
        </frame>
        <text name="B_10">
          <geometryInfo x="6.75000" y="0.87500" width="0.43750"
           height="0.14587"/>
          <visualSettings fillBackgroundColor="r50g88b50"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[Active:]]>
            </string>
          </textSegment>
        </text>
        <text name="B_11">
          <geometryInfo x="6.50000" y="1.06250" width="0.58337"
           height="0.14587"/>
          <visualSettings fillBackgroundColor="r0g88b75"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[Terminated:]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Active_total" source="CS_Active_Total" alignment="end">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.12500" y="0.87500" width="0.56250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Terminated_total" source="CS_NonActive_Total"
         alignment="end">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.12500" y="1.06250" width="0.56250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <frame name="M_G_ParentChild_Headings">
          <geometryInfo x="0.00000" y="0.31250" width="7.93750"
           height="0.25000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_1">
            <geometryInfo x="0.00000" y="0.31250" width="0.87500"
             height="0.14587"/>
            <visualSettings fillBackgroundColor="r100g75b100"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Address Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_2">
            <geometryInfo x="0.93750" y="0.31250" width="0.87500"
             height="0.14587"/>
            <visualSettings fillBackgroundColor="r100g75b100"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Member Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <geometryInfo x="2.06250" y="0.31250" width="1.37500"
             height="0.14587"/>
            <visualSettings fillBackgroundColor="r100g75b100"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Alpha Name]]>
              </string>
            </textSegment>
          </text>
          <text name="B_8">
            <geometryInfo x="6.87500" y="0.31250" width="1.06250"
             height="0.14587"/>
            <visualSettings fillBackgroundColor="r100g75b100"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Member Status]]>
              </string>
            </textSegment>
          </text>
          <line name="B_13" arrow="none">
            <geometryInfo x="0.00000" y="0.50000" width="7.93750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="0.00000" y="0.50000"/>
              <point x="7.93750" y="0.50000"/>
            </points>
          </line>
          <text name="B_4">
            <geometryInfo x="4.18750" y="0.31250" width="1.18750"
             height="0.14587"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Division Information]]>
              </string>
            </textSegment>
          </text>
          <text name="B_5">
            <geometryInfo x="5.50000" y="0.31250" width="1.25000"
             height="0.14587"/>
            <textSegment>
              <font face="Arial" size="8"/>
              <string>
              <![CDATA[Territory  Information]]>
              </string>
            </textSegment>
          </text>
        </frame>
        <field name="F_Heading_Title" source="Grp_title" spacing="single"
         alignment="start">
          <font face="Arial" size="8" bold="yes"/>
          <geometryInfo x="0.00000" y="0.06250" width="1.43750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Parent_Long_address_number"
         source="PARENT_LONG_ADDRESS_NUMBER" spacing="single"
         alignment="start">
          <font face="Arial" size="8" bold="yes"/>
          <geometryInfo x="1.50000" y="0.06250" width="0.68750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
      </repeatingFrame>
      <frame name="M_G_Group_Totals">
        <geometryInfo x="0.00000" y="1.25000" width="7.93750" height="0.62500"
        />
        <visualSettings fillPattern="transparent"/>
        <text name="B_6">
          <textSettings justify="center"/>
          <geometryInfo x="6.06250" y="1.37500" width="0.88538"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g75b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_6formattrigger"/>
          <visualSettings fillBackgroundColor="r0g75b88"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[Status Totals  ]]>
            </string>
          </textSegment>
        </text>
        <text name="B_7">
          <textSettings justify="end"/>
          <geometryInfo x="6.50000" y="1.56250" width="0.47913"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g75b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_7formattrigger"/>
          <visualSettings fillBackgroundColor="r0g75b88"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[Active: ]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Grand_Total_Active" source="CS_Grand_Total_Active"
         alignment="end">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.00000" y="1.56250" width="0.62500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Grand_Total_Terminated"
         source="CS_Grand_Total_Terminated" alignment="end">
          <font face="Arial" size="8"/>
          <geometryInfo x="7.00000" y="1.68750" width="0.62500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_12">
          <textSettings justify="end"/>
          <geometryInfo x="6.18750" y="1.68750" width="0.76038"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g75b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_12formattrigger"/>
          <visualSettings fillBackgroundColor="r0g75b88"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[Terminated:]]>
            </string>
          </textSegment>
        </text>
        <field name="F_No_Data_Msg" source="CF_No_Data_Msg" alignment="center"
          >
          <font face="Arial" size="8"/>
          <geometryInfo x="2.81250" y="1.50000" width="3.06250"
           height="0.18750"/>
          <visualSettings fillPattern="transparent"/>
        </field>
      </frame>
    </body>
    <margin>
      <text name="B_9">
        <textSettings justify="center"/>
        <geometryInfo x="3.37500" y="0.31250" width="1.81250" height="0.15625"
        />
        <textSegment>
          <font face="Arial" size="10" bold="yes"/>
          <string>
          <![CDATA[Parent-Child Relationship]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="right">
        <font face="Arial" size="9"/>
        <geometryInfo x="7.22913" y="0.06250" width="0.83337" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_MsgFormula return Char is
  lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	
end;]]>
      </textSource>
    </function>
    <function name="b_6formattrigger">
      <textSource>
      <![CDATA[function B_6FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_7formattrigger">
      <textSource>
      <![CDATA[function B_7FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_12formattrigger">
      <textSource>
      <![CDATA[function B_12FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
