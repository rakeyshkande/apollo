<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="MercuryDirectInstalls" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="MERCURYDIRECTINSTALLSXXX" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_MEMBER" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE1" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE2" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_CALLLOG">
      <select>
      <![CDATA[SELECT /*+ ORDERED */ HEAT_CALLLOG.CALL_ID CL_CALL_ID,
JDE_ADDRESS_BOOK.DIVISION_MANAGER_CODE,
HEAT_CALLLOG.CUST_ID CL_CUST_ID,
to_char(TO_DATE(HEAT_CALLLOG.RECVD_DATE,'YYYY-MM-DD'),'MM-DD-YYYY') as RECVD_DATE,
HEAT_CALLLOG.CAUSE,
strip_control_chars(substr(to_char(HEAT_CALLLOG.CALL_DESC), 1, 4000)) AS CALL_DESC,
HEAT_CALLLOG.CALL_STATUS,
JDE_ADDRESS_BOOK.FIELD_ACCT_REPRESENTATIVE_CODE,
TERRITORY_INFO.FBC_NAME,
HEAT_SUBSET.SLA_CLASS,
HEAT_SUBSET.COMPANY,
HEAT_SUBSET.CITY,
HEAT_SUBSET.STATE,
to_char(TO_DATE(HEAT_CALLLOG.MOD_DATE,'YYYY-MM-DD'),'MM-DD-YYYY') as MOD_DATE,
HEAT_TRACKER.FULL_NAME HT_FULL_NAME,
HEAT_TRACKER.LOGIN_ID
FROM   HEAT_CALLLOG,
           HEAT_PROFILE,
    HEAT_SUBSET,
    HEAT_TRACKER,
    JDE_ADDRESS_BOOK,
    TERRITORY_INFO
WHERE HEAT_CALLLOG.CUST_ID = HEAT_PROFILE.CUST_ID
      AND  HEAT_CALLLOG.CALL_ID = HEAT_SUBSET.CALL_ID
      AND HEAT_CALLLOG.TRACKER = HEAT_TRACKER.LOGIN_ID        
      AND ((:P_TERRITORY = 'All' or  :P_DIVISION = 'All')
        OR ( :P_DIVISION != 'blank' and :P_DIVISION = JDE_ADDRESS_BOOK.DIVISION_MANAGER_CODE)
        OR (:P_TERRITORY != 'blank' and :P_TERRITORY = JDE_ADDRESS_BOOK.FIELD_ACCT_REPRESENTATIVE_CODE))
      AND (:P_MEMBER = 'blank' or :P_MEMBER = HEAT_CALLLOG.CUST_ID)
      AND ((:p_date1 = 'blank' or HEAT_CALLLOG.RECVD_DATE >= to_char(TO_DATE(:P_DATE1, 'MM/DD/YYYY'), 'YYYY-MM-DD'))
      AND (:p_date2 = 'blank' or HEAT_CALLLOG.RECVD_DATE <= to_char(TO_DATE(:P_DATE2, 'MM/DD/YYYY'),'YYYY-MM-DD')))
     AND HEAT_CALLLOG.CUST_ID = JDE_ADDRESS_BOOK.LONG_ADDRESS_NUMBER
     AND TERRITORY_INFO.TERRITORY = JDE_ADDRESS_BOOK.FIELD_ACCT_REPRESENTATIVE_CODE
     AND HEAT_CALLLOG.CALL_CATEGORY = 'MERCURY DIRECT'
     AND HEAT_CALLLOG.CAUSE = 'Setup'
     AND HEAT_CALLLOG.CALL_STATUS <> 'Closed'
ORDER BY HEAT_CALLLOG.RECVD_DATE ASC, HEAT_SUBSET.COMPANY 

]]>
      </select>
      <displayInfo x="0.69946" y="0.13806" width="1.55212" height="0.20825"/>
      <group name="G_CALL_COUNTS">
        <displayInfo x="0.37415" y="0.73328" width="2.20911" height="0.77246"
        />
        <dataItem name="CALL_STATUS" datatype="vchar2" columnOrder="21"
         defaultWidth="100000" defaultHeight="10000" columnFlags="3"
         defaultLabel="Call Status" breakOrder="descending">
          <dataDescriptor expression="HEAT_CALLLOG.CALL_STATUS"
           descriptiveExpression="CALL_STATUS" order="7" width="10"/>
        </dataItem>
        <summary name="CS_CALL_ID_COUNTER" source="CL_CALL_ID"
         function="count" width="20" precision="10" reset="G_CALL_COUNTS"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Call Id Counter">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_CUST_ID_COUNTER" source="CL_CUST_ID"
         function="count" width="20" precision="10" reset="G_CALL_COUNTS"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Cust Id Counter">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_CALL_CATEGORY">
        <displayInfo x="0.22913" y="2.00061" width="2.48962" height="2.82324"
        />
        <dataItem name="MOD_DATE" datatype="vchar2" columnOrder="31"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Mod Date" breakOrder="none">
          <dataDescriptor
           expression="to_char ( TO_DATE ( HEAT_CALLLOG.MOD_DATE , &apos;YYYY-MM-DD&apos; ) , &apos;MM-DD-YYYY&apos; )"
           descriptiveExpression="MOD_DATE" order="14" width="10"/>
        </dataItem>
        <dataItem name="FBC_NAME" datatype="vchar2" columnOrder="30"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Name" breakOrder="none">
          <dataDescriptor expression="TERRITORY_INFO.FBC_NAME"
           descriptiveExpression="FBC_NAME" order="9" width="30"/>
        </dataItem>
        <dataItem name="FIELD_ACCT_REPRESENTATIVE_CODE" datatype="vchar2"
         columnOrder="29" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Field Acct Representative Code"
         breakOrder="none">
          <dataDescriptor
           expression="JDE_ADDRESS_BOOK.FIELD_ACCT_REPRESENTATIVE_CODE"
           descriptiveExpression="FIELD_ACCT_REPRESENTATIVE_CODE" order="8"
           width="3"/>
        </dataItem>
        <dataItem name="COMPANY" datatype="vchar2" columnOrder="28" width="35"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Company" breakOrder="none">
          <dataDescriptor expression="HEAT_SUBSET.COMPANY"
           descriptiveExpression="COMPANY" order="11" width="35"/>
        </dataItem>
        <dataItem name="RECVD_DATE" datatype="vchar2" columnOrder="27"
         defaultWidth="100000" defaultHeight="10000" columnFlags="3"
         defaultLabel="Recvd Date" breakOrder="descending">
          <xmlSettings xmlTag="TO_CHAR_TO_DATE_HEAT_CALLLOG_R"/>
          <dataDescriptor
           expression="to_char ( TO_DATE ( HEAT_CALLLOG.RECVD_DATE , &apos;YYYY-MM-DD&apos; ) , &apos;MM-DD-YYYY&apos; )"
           descriptiveExpression="RECVD_DATE" order="4" width="10"/>
        </dataItem>
        <dataItem name="CL_CALL_ID" datatype="vchar2" columnOrder="16"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Cl Call Id" breakOrder="none">
          <dataDescriptor expression="HEAT_CALLLOG.CALL_ID"
           descriptiveExpression="CL_CALL_ID" order="1" width="8"/>
        </dataItem>
        <dataItem name="DIVISION_MANAGER_CODE" datatype="vchar2"
         columnOrder="17" width="3" defaultWidth="30000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Division Manager Code"
         breakOrder="none">
          <dataDescriptor expression="JDE_ADDRESS_BOOK.DIVISION_MANAGER_CODE"
           descriptiveExpression="DIVISION_MANAGER_CODE" order="2" width="3"/>
        </dataItem>
        <dataItem name="CL_CUST_ID" datatype="vchar2" columnOrder="18"
         width="25" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Cl Cust Id" breakOrder="none">
          <dataDescriptor expression="HEAT_CALLLOG.CUST_ID"
           descriptiveExpression="CL_CUST_ID" order="3" width="25"/>
        </dataItem>
        <dataItem name="CAUSE" datatype="vchar2" columnOrder="19" width="15"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Cause" breakOrder="none">
          <dataDescriptor expression="HEAT_CALLLOG.CAUSE"
           descriptiveExpression="CAUSE" order="5" width="15"/>
        </dataItem>
        <dataItem name="CALL_DESC" datatype="vchar2" columnOrder="20"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Call Desc" breakOrder="none">
          <dataDescriptor
           expression="strip_control_chars ( substr ( to_char ( HEAT_CALLLOG.CALL_DESC ) , 1 , 4000 ) )"
           descriptiveExpression="CALL_DESC" order="6" width="4000"/>
        </dataItem>
        <dataItem name="SLA_CLASS" datatype="vchar2" columnOrder="22"
         width="15" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Sla Class" breakOrder="none">
          <dataDescriptor expression="HEAT_SUBSET.SLA_CLASS"
           descriptiveExpression="SLA_CLASS" order="10" width="15"/>
        </dataItem>
        <dataItem name="CITY" datatype="vchar2" columnOrder="23" width="24"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="City" breakOrder="none">
          <dataDescriptor expression="HEAT_SUBSET.CITY"
           descriptiveExpression="CITY" order="12" width="24"/>
        </dataItem>
        <dataItem name="STATE" datatype="vchar2" columnOrder="24" width="2"
         defaultWidth="20000" defaultHeight="10000" columnFlags="0"
         defaultLabel="State" breakOrder="none">
          <dataDescriptor expression="HEAT_SUBSET.STATE"
           descriptiveExpression="STATE" order="13" width="2"/>
        </dataItem>
        <dataItem name="HT_FULL_NAME" datatype="vchar2" columnOrder="25"
         width="25" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Ht Full Name" breakOrder="none">
          <dataDescriptor expression="HEAT_TRACKER.FULL_NAME"
           descriptiveExpression="HT_FULL_NAME" order="15" width="25"/>
        </dataItem>
        <dataItem name="LOGIN_ID" datatype="vchar2" columnOrder="26" width="8"
         defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Login Id" breakOrder="none">
          <dataDescriptor expression="HEAT_TRACKER.LOGIN_ID"
           descriptiveExpression="LOGIN_ID" order="16" width="8"/>
        </dataItem>
      </group>
    </dataSource>
    <dataSource name="Q_JOURNAL">
      <select>
      <![CDATA[SELECT ALL
HEAT_JOURNAL.ENTRY_DATE,
strip_control_chars(substr(to_char(HEAT_JOURNAL.ENTRY_TEXT), 1, 4000) ) AS ENTRY_TEXT,
HEAT_JOURNAL.ENTRY_TIME,
HEAT_JOURNAL.CALL_ID HJ_CALL_ID,
HEAT_JOURNAL.HEAT_SEQ HJ_HEAT_SEQ, 
HEAT_TRACKER.FULL_NAME HT_TRACKER,
HEAT_JOURNAL.TRACKER HJ_TRACKER_ID,
HEAT_TRACKER.LOGIN_ID HT_LOGIN_ID
FROM HEAT_JOURNAL,
           HEAT_TRACKER
WHERE (HEAT_JOURNAL.TRACKER = HEAT_TRACKER.LOGIN_ID) 
ORDER BY HEAT_JOURNAL.HEAT_SEQ]]>
      </select>
      <displayInfo x="3.16602" y="0.20825" width="1.29236" height="0.32983"/>
      <group name="G_ENTRY_DATE">
        <displayInfo x="3.06104" y="1.00122" width="1.49426" height="1.62695"
        />
        <dataItem name="ENTRY_DATE" datatype="vchar2" columnOrder="34"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Entry Date" breakOrder="none">
          <dataDescriptor expression="HEAT_JOURNAL.ENTRY_DATE"
           descriptiveExpression="ENTRY_DATE" order="1" width="10"/>
        </dataItem>
        <dataItem name="ENTRY_TEXT" datatype="vchar2" columnOrder="35"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Entry Text" breakOrder="none">
          <dataDescriptor
           expression="strip_control_chars ( substr ( to_char ( HEAT_JOURNAL.ENTRY_TEXT ) , 1 , 4000 ) )"
           descriptiveExpression="ENTRY_TEXT" order="2" width="4000"/>
        </dataItem>
        <dataItem name="ENTRY_TIME" datatype="vchar2" columnOrder="36"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Entry Time" breakOrder="none">
          <dataDescriptor expression="HEAT_JOURNAL.ENTRY_TIME"
           descriptiveExpression="ENTRY_TIME" order="3" width="8"/>
        </dataItem>
        <dataItem name="HJ_CALL_ID" datatype="vchar2" columnOrder="37"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Hj Call Id">
          <dataDescriptor expression="HEAT_JOURNAL.CALL_ID"
           descriptiveExpression="HJ_CALL_ID" order="4" width="8"/>
        </dataItem>
        <dataItem name="HJ_HEAT_SEQ" oracleDatatype="number" columnOrder="38"
         width="22" defaultWidth="120000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Hj Heat Seq">
          <dataDescriptor expression="HEAT_JOURNAL.HEAT_SEQ"
           descriptiveExpression="HJ_HEAT_SEQ" order="5"
           oracleDatatype="number" width="22" precision="10"/>
        </dataItem>
        <dataItem name="HT_TRACKER" datatype="vchar2" columnOrder="39"
         width="25" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Ht Tracker" breakOrder="none">
          <dataDescriptor expression="HEAT_TRACKER.FULL_NAME"
           descriptiveExpression="HT_TRACKER" order="6" width="25"/>
        </dataItem>
        <dataItem name="HJ_TRACKER_ID" datatype="vchar2" columnOrder="40"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Hj Tracker Id" breakOrder="none">
          <dataDescriptor expression="HEAT_JOURNAL.TRACKER"
           descriptiveExpression="HJ_TRACKER_ID" order="7" width="8"/>
        </dataItem>
        <dataItem name="HT_LOGIN_ID" datatype="vchar2" columnOrder="41"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Ht Login Id" breakOrder="none">
          <dataDescriptor expression="HEAT_TRACKER.LOGIN_ID"
           descriptiveExpression="HT_LOGIN_ID" order="8" width="8"/>
        </dataItem>
      </group>
    </dataSource>
    <dataSource name="Q_ASSIGNMENT">
      <select>
      <![CDATA[SELECT ALL 
HEAT_ASSIGNMENT.ASSIGNED_BY, HEAT_ASSIGNMENT.DATE_ASSIGN, 
HEAT_ASSIGNMENT.ASSIGNEE,
strip_control_chars(substr(to_char(HEAT_ASSIGNMENT.COMMENTS), 1, 4000)) AS COMMENTS,
HEAT_ASSIGNMENT.DATE_RESOLV, 
HEAT_ASSIGNMENT.CALL_ID HA_CALL_ID, HEAT_ASSIGNMENT.HEAT_SEQ HA_HEAT_SEQ
FROM HEAT_ASSIGNMENT ]]>
      </select>
      <displayInfo x="5.08362" y="2.23962" width="1.19995" height="0.32983"/>
      <group name="G_ASSIGNED_BY">
        <displayInfo x="4.96716" y="3.05334" width="1.43176" height="1.45605"
        />
        <dataItem name="ASSIGNED_BY" datatype="vchar2" columnOrder="42"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Assigned By" breakOrder="none">
          <dataDescriptor expression="HEAT_ASSIGNMENT.ASSIGNED_BY"
           descriptiveExpression="ASSIGNED_BY" order="1" width="8"/>
        </dataItem>
        <dataItem name="DATE_ASSIGN" datatype="vchar2" columnOrder="43"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Date Assign" breakOrder="none">
          <dataDescriptor expression="HEAT_ASSIGNMENT.DATE_ASSIGN"
           descriptiveExpression="DATE_ASSIGN" order="2" width="10"/>
        </dataItem>
        <dataItem name="ASSIGNEE" datatype="vchar2" columnOrder="44"
         width="20" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Assignee" breakOrder="none">
          <dataDescriptor expression="HEAT_ASSIGNMENT.ASSIGNEE"
           descriptiveExpression="ASSIGNEE" order="3" width="20"/>
        </dataItem>
        <dataItem name="COMMENTS" datatype="vchar2" columnOrder="45"
         width="4000" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Comments" breakOrder="none">
          <dataDescriptor
           expression="strip_control_chars ( substr ( to_char ( HEAT_ASSIGNMENT.COMMENTS ) , 1 , 4000 ) )"
           descriptiveExpression="COMMENTS" order="4" width="4000"/>
        </dataItem>
        <dataItem name="DATE_RESOLV" datatype="vchar2" columnOrder="46"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Date Resolv" breakOrder="none">
          <dataDescriptor expression="HEAT_ASSIGNMENT.DATE_RESOLV"
           descriptiveExpression="DATE_RESOLV" order="5" width="10"/>
        </dataItem>
        <dataItem name="HA_CALL_ID" datatype="vchar2" columnOrder="47"
         width="8" defaultWidth="80000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Ha Call Id">
          <dataDescriptor expression="HEAT_ASSIGNMENT.CALL_ID"
           descriptiveExpression="HA_CALL_ID" order="6" width="8"/>
        </dataItem>
        <dataItem name="HA_HEAT_SEQ" oracleDatatype="number" columnOrder="48"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Ha Heat Seq">
          <dataDescriptor expression="HEAT_ASSIGNMENT.HEAT_SEQ"
           descriptiveExpression="HA_HEAT_SEQ" order="7"
           oracleDatatype="number" width="22" precision="10"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_NO_DATA_MESSAGE" source="cf_no_data_foundformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" valueIfNull="blank" breakOrder="none">
      <displayInfo x="4.73950" y="1.05212" width="1.62500" height="0.28125"/>
    </formula>
    <summary name="CS_RECORD_COUNT" source="CL_CALL_ID" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="4.75000" y="0.79163" width="1.53125" height="0.23962"/>
    </summary>
    <formula name="CF_TITLE" source="cf_titleformula" datatype="character"
     width="50" precision="10" defaultWidth="0" defaultHeight="0"
     columnFlags="16" breakOrder="none">
      <displayInfo x="4.78125" y="0.14587" width="1.16663" height="0.28125"/>
    </formula>
    <formula name="CF_TITLE_NAME" source="cf_title_nameformula"
     datatype="character" width="40" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="4.81250" y="0.54163" width="1.33337" height="0.21875"/>
    </formula>
    <link name="L_1" parentGroup="G_CALL_CATEGORY" parentColumn="CL_CALL_ID"
     childQuery="Q_JOURNAL" childColumn="HJ_CALL_ID" condition="eq"
     sqlClause="where"/>
    <link name="L_2" parentGroup="G_CALL_CATEGORY" parentColumn="CL_CALL_ID"
     childQuery="Q_ASSIGNMENT" childColumn="HA_CALL_ID" condition="eq"
     sqlClause="where"/>
  </data>
  <layout>
  <section name="main">
    <body width="7.87500" height="9.18750">
      <location x="0.12500" y="0.43750"/>
      <frame name="M_G_MERCURY_DIRECT_GRPFR">
        <geometryInfo x="0.00000" y="0.00000" width="7.87500" height="4.00000"
        />
        <generalLayout verticalElasticity="expand"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_G_CALLS" source="G_CALL_COUNTS"
         printDirection="down" maxRecordsPerPage="1" minWidowRecords="1"
         columnMode="no">
          <geometryInfo x="0.00000" y="0.00000" width="7.87500"
           height="2.93750"/>
          <generalLayout verticalElasticity="variable"/>
          <advancedLayout printObjectOnPage="firstPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <frame name="M_G_MEMBER_GRPFR">
            <geometryInfo x="0.00000" y="0.00000" width="7.87500"
             height="2.43750"/>
            <generalLayout verticalElasticity="expand"/>
            <advancedLayout printObjectOnPage="firstPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
            <repeatingFrame name="R_G_MEMBER" source="G_CALL_CATEGORY"
             printDirection="down" minWidowRecords="1" columnMode="no">
              <geometryInfo x="0.00000" y="0.00000" width="7.87500"
               height="2.37500"/>
              <generalLayout pageProtect="yes" verticalElasticity="variable"/>
              <visualSettings fillPattern="transparent"/>
              <field name="F_CALL_ID" source="CL_CALL_ID" spacing="single"
               alignment="center">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.00000" y="0.35144" width="0.62500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_RECVD_DATE" source="RECVD_DATE" spacing="single"
               alignment="center">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.68750" y="0.35144" width="0.75000"
                 height="0.14856"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_CAUSE" source="CAUSE" spacing="single"
               alignment="center">
                <font face="Courier New" size="8"/>
                <geometryInfo x="1.50000" y="0.35144" width="0.87500"
                 height="0.15417"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_CALL_DESC" source="CALL_DESC" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="2.87500" y="0.35144" width="2.43750"
                 height="0.12500"/>
                <generalLayout verticalElasticity="expand"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <frame name="M_Journal_Assignment_Groups">
                <geometryInfo x="0.00000" y="1.37500" width="7.81250"
                 height="0.87500"/>
                <generalLayout verticalElasticity="expand"/>
                <advancedLayout printObjectOnPage="firstPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"/>
                <repeatingFrame name="R_G_JOURNAL" source="G_ENTRY_DATE"
                 printDirection="down" minWidowRecords="1" columnMode="no">
                  <geometryInfo x="0.00000" y="1.81250" width="4.37500"
                   height="0.25000"/>
                  <generalLayout verticalElasticity="variable"/>
                  <advancedLayout printObjectOnPage="firstPage"
                   basePrintingOn="enclosingObject"/>
                  <visualSettings fillPattern="transparent"
                   lineForegroundColor="white"/>
                  <field name="F_ENTRY_DATE" source="ENTRY_DATE"
                   alignment="start">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="0.00000" y="1.85144" width="0.75000"
                     height="0.14856"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_ENTRY_TEXT" source="ENTRY_TEXT"
                   alignment="start">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="0.87500" y="1.85144" width="2.12500"
                     height="0.14856"/>
                    <generalLayout verticalElasticity="expand"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_J_TRACKER" source="HT_TRACKER"
                   alignment="start">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="3.06250" y="1.85144" width="0.68750"
                     height="0.14856"/>
                    <generalLayout verticalElasticity="expand"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_ENTRY_TIME" source="ENTRY_TIME"
                   alignment="center">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="3.75000" y="1.85144" width="0.62500"
                     height="0.14856"/>
                    <generalLayout verticalElasticity="expand"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                </repeatingFrame>
                <line name="B_21" arrow="none"
                 stretchWithFrame="M_Journal_Assignment_Groups">
                  <geometryInfo x="4.43750" y="1.64600" width="0.00000"
                   height="0.60400"/>
                  <visualSettings lineWidth="1" fillPattern="transparent"
                   linePattern="solid"/>
                  <points>
                    <point x="4.43750" y="1.64600"/>
                    <point x="4.43750" y="2.25000"/>
                  </points>
                </line>
                <repeatingFrame name="R_G_ASSIGNMENT" source="G_ASSIGNED_BY"
                 printDirection="down" minWidowRecords="1" columnMode="no">
                  <geometryInfo x="4.50000" y="1.81250" width="3.25696"
                   height="0.25000"/>
                  <generalLayout verticalElasticity="variable"/>
                  <advancedLayout printObjectOnPage="firstPage"
                   basePrintingOn="enclosingObject"/>
                  <visualSettings lineWidth="1" fillPattern="transparent"
                   lineForegroundColor="white"/>
                  <field name="F_ASSIGNED_BY" source="ASSIGNED_BY"
                   alignment="center">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="4.56250" y="1.85144" width="0.56946"
                     height="0.14856"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_DATE_ASSIGN" source="DATE_ASSIGN"
                   alignment="center">
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="5.18750" y="1.85144" width="0.81250"
                     height="0.14856"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_ASSIGNEE" source="ASSIGNEE" alignment="start"
                    >
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="6.06250" y="1.85144" width="0.68750"
                     height="0.14856"/>
                    <generalLayout verticalElasticity="expand"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                  <field name="F_COMMENTS" source="COMMENTS" alignment="start"
                    >
                    <font face="Courier New" size="8"/>
                    <geometryInfo x="6.87500" y="1.85144" width="0.87500"
                     height="0.14856"/>
                    <generalLayout verticalElasticity="expand"/>
                    <visualSettings fillPattern="transparent"/>
                  </field>
                </repeatingFrame>
              </frame>
              <frame name="M_G_Heading_2">
                <geometryInfo x="0.00000" y="1.37476" width="7.81250"
                 height="0.31274"/>
                <advancedLayout printObjectOnPage="firstPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"
                 lineForegroundColor="white"/>
                <text name="B_7">
                  <textSettings justify="center"/>
                  <geometryInfo x="0.00000" y="1.43750" width="0.75000"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[EntryDate]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_8">
                  <textSettings justify="center"/>
                  <geometryInfo x="0.93750" y="1.43750" width="0.75000"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[EntryText]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_9">
                  <textSettings justify="center"/>
                  <geometryInfo x="3.00000" y="1.43750" width="0.62500"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[Tracker]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_10">
                  <textSettings justify="center"/>
                  <geometryInfo x="3.68750" y="1.43750" width="0.68750"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[EntryTime]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_12">
                  <geometryInfo x="4.50000" y="1.43750" width="0.62500"
                   height="0.14600"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[AssignBy]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_13">
                  <textSettings justify="center"/>
                  <geometryInfo x="5.18750" y="1.43750" width="0.81250"
                   height="0.14600"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[DateAssign]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_14">
                  <textSettings justify="center"/>
                  <geometryInfo x="6.06250" y="1.43750" width="0.68750"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[Assignee]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_18">
                  <textSettings justify="center"/>
                  <geometryInfo x="6.87500" y="1.43750" width="0.68750"
                   height="0.14600"/>
                  <visualSettings fillBackgroundColor="r88g75b75"
                   lineForegroundColor="white"/>
                  <textSegment>
                    <font face="Courier New" size="8"/>
                    <string>
                    <![CDATA[Comments]]>
                    </string>
                  </textSegment>
                </text>
                <line name="B_20" arrow="none">
                  <geometryInfo x="4.50000" y="1.62500" width="3.31250"
                   height="0.00000"/>
                  <visualSettings lineWidth="1" fillPattern="transparent"
                   fillBackgroundColor="r25g75b50" linePattern="solid"/>
                  <points>
                    <point x="4.50000" y="1.62500"/>
                    <point x="7.81250" y="1.62500"/>
                  </points>
                </line>
                <line name="B_11" arrow="none">
                  <geometryInfo x="0.00000" y="1.62500" width="4.37500"
                   height="0.00000"/>
                  <visualSettings lineWidth="1" fillPattern="transparent"
                   linePattern="solid"/>
                  <points>
                    <point x="0.00000" y="1.62500"/>
                    <point x="4.37500" y="1.62500"/>
                  </points>
                </line>
              </frame>
              <field name="F_COMPANY" source="COMPANY" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.12500" y="0.81250" width="2.62500"
                 height="0.18750"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_CITY" source="CITY" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.12500" y="1.00000" width="1.50000"
                 height="0.18750"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_STATE" source="STATE" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.12500" y="1.18750" width="0.87500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_T_FULL_NAME" source="HT_FULL_NAME"
               spacing="single" alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="5.62500" y="0.35144" width="1.18750"
                 height="0.18591"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_CL_CUST_ID" source="CL_CUST_ID" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="0.12500" y="0.56250" width="0.81250"
                 height="0.18750"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_SLA_CLASS" source="SLA_CLASS" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="1.06250" y="0.56250" width="1.06250"
                 height="0.18750"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <frame name="M_G_Heading_1">
                <geometryInfo x="0.00000" y="0.00000" width="7.87500"
                 height="0.25000"/>
                <visualSettings fillPattern="transparent"
                 fillBackgroundColor="gray" linePattern="solid"/>
                <text name="B_1">
                  <geometryInfo x="0.06250" y="0.06250" width="0.50000"
                   height="0.12500"/>
                  <visualSettings fillBackgroundColor="gray"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Call Id]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_2">
                  <textSettings justify="center"/>
                  <geometryInfo x="0.68750" y="0.06250" width="0.75000"
                   height="0.12500"/>
                  <visualSettings fillBackgroundColor="gray"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Recvd Date]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_3">
                  <textSettings justify="center"/>
                  <geometryInfo x="1.56250" y="0.06250" width="0.68750"
                   height="0.12500"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Cause]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_4">
                  <textSettings justify="center"/>
                  <geometryInfo x="2.93750" y="0.06250" width="1.43750"
                   height="0.12500"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Call Description]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_5">
                  <textSettings justify="center"/>
                  <geometryInfo x="5.56250" y="0.06250" width="1.00000"
                   height="0.12500"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Tracker]]>
                    </string>
                  </textSegment>
                </text>
                <text name="B_6">
                  <textSettings justify="center"/>
                  <geometryInfo x="6.87500" y="0.06250" width="0.87500"
                   height="0.18750"/>
                  <visualSettings fillBackgroundColor="gray"/>
                  <textSegment>
                    <font face="Courier New" size="8" bold="yes"/>
                    <string>
                    <![CDATA[Call Status]]>
                    </string>
                  </textSegment>
                </text>
              </frame>
              <field name="F_FBC_Name" source="FBC_NAME" alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="5.81250" y="0.81250" width="1.81250"
                 height="0.18750"/>
                <generalLayout>
                  <conditionalFormat>
                    <formatException
                     label="((:P_TERRITORY != &apos;All&apos;) and (:P_TERRITORY != &apos;blank&apos;)) "
                      >
                      <font face="Courier New" size="8"/>
                      <formatVisualSettings fillPattern="transparent"/>
                    <cond name="first" column="P_TERRITORY" exception="2"
                     lowValue="All" conjunction="1"/>
                    </formatException>
                  </conditionalFormat>
                </generalLayout>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"
                 formatTrigger="f_fbc_nameformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Mod_Date" source="MOD_DATE" spacing="single"
               alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="5.62500" y="0.56250" width="0.81250"
                 height="0.18750"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"
                 lineForegroundColor="gray32"/>
              </field>
              <field name="F_FBC_Code" source="FIELD_ACCT_REPRESENTATIVE_CODE"
               spacing="single" alignment="start">
                <font face="Courier New" size="8"/>
                <geometryInfo x="5.62500" y="0.81250" width="0.18750"
                 height="0.18750"/>
                <generalLayout>
                  <conditionalFormat>
                    <formatException
                     label="((:P_TERRITORY != &apos;All&apos;) and (:P_TERRITORY != &apos;blank&apos;)) "
                      >
                      <font face="Courier New" size="8"/>
                      <formatVisualSettings fillPattern="transparent"
                       borderForegroundColor="gray32"/>
                    <cond name="first" column="P_TERRITORY" exception="2"
                     lowValue="All" conjunction="1"/>
                    </formatException>
                  </conditionalFormat>
                </generalLayout>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"
                 formatTrigger="f_fbc_codeformattrigger"/>
                <visualSettings fillPattern="transparent"
                 lineForegroundColor="gray32"/>
              </field>
              <field name="F_Call_Status" source="CALL_STATUS"
               alignment="center">
                <font face="Courier New" size="8"/>
                <geometryInfo x="6.87500" y="0.35144" width="0.75000"
                 height="0.18750"/>
                <visualSettings fillBackgroundColor="gray"/>
              </field>
            </repeatingFrame>
          </frame>
          <line name="B_25" arrow="none">
            <geometryInfo x="0.00000" y="2.50000" width="7.81250"
             height="0.00000"/>
            <visualSettings lineWidth="1" fillPattern="transparent"
             linePattern="solid"/>
            <points>
              <point x="0.00000" y="2.50000"/>
              <point x="7.81250" y="2.50000"/>
            </points>
          </line>
          <field name="F_DistinctCallCount" source="CS_CALL_ID_COUNTER"
           spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="2.43750" y="2.68750" width="0.50000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="lastPage"
             basePrintingOn="anchoringObject"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <text name="B_22">
            <geometryInfo x="0.06250" y="2.68750" width="0.87500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="green"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Totals:]]>
              </string>
            </textSegment>
          </text>
          <text name="B_23">
            <geometryInfo x="1.31250" y="2.68750" width="1.12500"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="green"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Distinct Calls:]]>
              </string>
            </textSegment>
          </text>
          <text name="B_24">
            <geometryInfo x="4.12500" y="2.68750" width="1.71875"
             height="0.18750"/>
            <visualSettings fillBackgroundColor="green"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Distinct Florist Calls:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Florist_calls" source="CS_CUST_ID_COUNTER"
           spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="6.00000" y="2.68750" width="0.50000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <text name="B_17">
          <textSettings justify="center"/>
          <geometryInfo x="2.25000" y="3.18750" width="3.75000"
           height="0.25000"/>
          <advancedLayout formatTrigger="b_17formattrigger"/>
          <textSegment>
            <font face="Courier New" size="8"/>
            <string>
            <![CDATA[&<CF_NO_DATA_MESSAGE>]]>
            </string>
          </textSegment>
        </text>
        <text name="B_26">
          <textSettings justify="center"/>
          <geometryInfo x="3.56250" y="3.62500" width="1.09375"
           height="0.37500"/>
          <textSegment>
            <font face="Courier New" size="8"/>
            <string>
            <![CDATA[End of Report
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Courier New" size="8"/>
            <string>
            <![CDATA[V 1.2]]>
            </string>
          </textSegment>
        </text>
      </frame>
    </body>
    <margin>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Courier New" size="9"/>
        <geometryInfo x="0.18750" y="0.12500" width="0.87500" height="0.12500"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_15">
        <textSettings justify="center"/>
        <geometryInfo x="1.43750" y="0.12500" width="6.06250" height="0.18750"
        />
        <generalLayout horizontalElasticity="expand"/>
        <visualSettings fillBackgroundColor="r100g100b50"/>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[PENDING MERCURY DIRECT INSTALLATIONS &<CF_TITLE> &<CF_TITLE_NAME> ]]>
          </string>
        </textSegment>
      </text>
      <line name="B_16" arrow="none">
        <geometryInfo x="0.12500" y="0.37500" width="7.87500" height="0.00000"
        />
        <visualSettings lineWidth="2" fillBackgroundColor="r100g100b50"
         linePattern="solid"/>
        <points>
          <point x="0.12500" y="0.37500"/>
          <point x="8.00000" y="0.37500"/>
        </points>
      </line>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TITLEFormula return Char is
  lv_title varchar2(60);
begin
     if :P_MEMBER != 'blank' then
  	     lv_title := ('BY MEMBER #'||' '||:P_MEMBER);  	
     elsif :P_DIVISION != 'blank' and :P_DIVISION != 'All' then
         lv_title := ('BY DIVISION #'||' '||:P_DIVISION);
     elsif :P_TERRITORY != 'blank' and :P_TERRITORY != 'All' then
  	     lv_title := ('BY TERRITORY #'||' '||:P_TERRITORY); 
  	 elsif :P_TERRITORY = 'All' or :P_DIVISION = 'All' then
	          lv_title := ('FOR '||:P_DATE1 ||' - '|| :P_DATE2);
  	 end if;
  	           			
  return (lv_title);	
end;
]]>
      </textSource>
    </function>
    <function name="cf_no_data_foundformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_data_foundFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	
end;]]>
      </textSource>
    </function>
    <function name="cf_title_nameformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TITLE_NAMEFormula return Char is 
begin
 return title_name (:p_member, :p_territory, :p_division); 
end;]]>
      </textSource>
    </function>
    <function name="b_17formattrigger">
      <textSource>
      <![CDATA[function B_17FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_RECORD_COUNT != '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_codeformattrigger">
      <textSource>
      <![CDATA[function F_FBC_CodeFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if ((:P_TERRITORY != 'All') and
      (:P_TERRITORY != 'blank')) 
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_nameformattrigger">
      <textSource>
      <![CDATA[function F_FBC_NameFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if ((:P_TERRITORY != 'All') and
      (:P_TERRITORY != 'blank')) 
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
