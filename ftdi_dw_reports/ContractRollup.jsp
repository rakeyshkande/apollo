<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ContractRollup" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="CONTRACTROLLUP" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DATE2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Contract_Rollup">
      <select>
      <![CDATA[SELECT ALL HC.CUST_ID, 
HC.CALL_TYPE, HC.CALL_STATUS, HC.CAUSE, HC.CLOSED_DATE, 
HC.RECVD_DATE, HC.CALL_CATEGORY, HP.CUST_ID  HP_CUST_ID, 
substr(HP.COMPANY,1,30) as COMPANY_NAME, HP.TERRITORY, HP.CONSULTANT, 
AB.LONG_ADDRESS_NUMBER,
AB.DIVISION_MANAGER_CODE || '  ' || Replace(RI.MANAGER_NAME, '.', ' ') as DIV_Info, 
AB.FIELD_ACCT_REPRESENTATIVE_CODE || '  ' || Replace(TI.FBC_NAME, '.',' ') as FBC_Info
FROM HEAT_CALLLOG HC,
          HEAT_PROFILE HP,
          JDE_ADDRESS_BOOK AB,
          REGION_INFO RI,
          TERRITORY_INFO TI
WHERE HC.CUST_ID = HP.CUST_ID
          and (:P_TERRITORY = 'All' OR :P_DIVISION = 'All' OR ( :P_DIVISION != 'blank'
          AND ( (( RTRIM(AB.DIVISION_MANAGER_CODE) IS NOT NULL )
		  AND (:P_DIVISION = AB.division_manager_code ))
            OR EXISTS (
              SELECT 1 FROM REGION_TO_TERRITORY RT
              WHERE RT.TERRITORY = HP.TERRITORY
              AND RT.REGION = :P_DIVISION ) ) )
        OR ( :P_TERRITORY != 'blank' AND :P_TERRITORY =
          NVL(RTRIM(AB.FIELD_ACCT_REPRESENTATIVE_CODE), HP.TERRITORY) ) )
      AND (:P_MEMBER = 'blank' or :P_MEMBER = HC.CUST_ID)
      AND HC.CUST_ID = AB.LONG_ADDRESS_NUMBER(+) 
      AND TI.Territory = AB.FIELD_ACCT_REPRESENTATIVE_CODE
      AND RI.Region = AB.DIVISION_MANAGER_CODE
      AND AB.SEARCH_TYPE in ('C', 'I')
      AND to_date(HC.recvd_date, 'YYYY-MM-DD') between to_date (:p_date1, 'MM/DD/YYYY')  AND to_date(:p_date2, 'MM/DD/YYYY')
      AND UPPER(HC.Cause) in
            (Select cause from CONTRACT_CAUSES)
     AND UPPER( HC.call_category) in('FTD MEMBERSHIP','FOL','FAH','CREDIT CARDS','TECH SALES','SALES OPERATIONS',
                                                              'TERMS CREDIT/COLLECTIONS','TERMS PARTNERSHIP','FAH CONTRACTS')
ORDER BY
   HC.Call_Category,
   HC.Call_Type,
   HC.Cause,
   to_date(HC.RECVD_DATE,'YYYY-MM-DD') desc,
   HC.CUST_ID]]>
      </select>
      <displayInfo x="0.66333" y="0.17712" width="1.77710" height="0.19995"/>
      <group name="G_CALL_CATEGORY">
        <displayInfo x="0.34436" y="0.79370" width="2.40649" height="0.60156"
        />
        <dataItem name="CALL_CATEGORY" datatype="vchar2" columnOrder="20"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Call Category">
          <dataDescriptor expression="HC.CALL_CATEGORY"
           descriptiveExpression="CALL_CATEGORY" order="7" width="30"/>
        </dataItem>
        <summary name="CS_Call_Category_Count" source="LONG_ADDRESS_NUMBER"
         function="count" width="20" precision="10" reset="G_CALL_CATEGORY"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Call Category Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_CALL_TYPE">
        <displayInfo x="0.57361" y="1.74084" width="1.93823" height="0.60156"
        />
        <dataItem name="CALL_TYPE" datatype="vchar2" columnOrder="16"
         width="15" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Call Type">
          <dataDescriptor expression="HC.CALL_TYPE"
           descriptiveExpression="CALL_TYPE" order="2" width="15"/>
        </dataItem>
        <summary name="CS_Call_Type_Count" source="LONG_ADDRESS_NUMBER"
         function="count" width="20" precision="10" reset="G_CALL_TYPE"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Call Type Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_CAUSE">
        <displayInfo x="0.46533" y="2.61536" width="2.17163" height="0.60156"
        />
        <dataItem name="CAUSE" datatype="vchar2" columnOrder="18" width="15"
         defaultWidth="100000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Cause">
          <dataDescriptor expression="HC.CAUSE" descriptiveExpression="CAUSE"
           order="4" width="15"/>
        </dataItem>
        <summary name="CS_Cause_Count" source="CUST_ID" function="count"
         width="20" precision="10" reset="G_CAUSE" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cause Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_CONTRACT_DETAIL">
        <displayInfo x="0.32764" y="3.55212" width="2.44836" height="2.65234"
        />
        <dataItem name="COMPANY_NAME" datatype="vchar2" columnOrder="29"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Company Name" breakOrder="none">
          <dataDescriptor expression="substr ( HP.COMPANY , 1 , 30 )"
           descriptiveExpression="COMPANY_NAME" order="9" width="30"/>
        </dataItem>
        <dataItem name="DIV_Info" datatype="vchar2" columnOrder="27"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor
           expression="AB.DIVISION_MANAGER_CODE || &apos;  &apos; || Replace ( RI.MANAGER_NAME , &apos;.&apos; , &apos; &apos; )"
           descriptiveExpression="DIV_INFO" order="13" width="35"/>
        </dataItem>
        <dataItem name="FBC_Info" datatype="vchar2" columnOrder="28"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor
           expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE || &apos;  &apos; || Replace ( TI.FBC_NAME , &apos;.&apos; , &apos; &apos; )"
           descriptiveExpression="FBC_INFO" order="14" width="35"/>
        </dataItem>
        <dataItem name="CUST_ID" datatype="vchar2" columnOrder="26" width="25"
         defaultWidth="100000" defaultHeight="10000" columnFlags="1"
         defaultLabel="Cust Id">
          <dataDescriptor expression="HC.CUST_ID"
           descriptiveExpression="CUST_ID" order="1" width="25"/>
        </dataItem>
        <dataItem name="CLOSED_DATE" datatype="vchar2" columnOrder="25"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Closed Date" breakOrder="none">
          <dataDescriptor expression="HC.CLOSED_DATE"
           descriptiveExpression="CLOSED_DATE" order="5" width="10"/>
        </dataItem>
        <dataItem name="CALL_STATUS" datatype="vchar2" columnOrder="17"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Call Status" breakOrder="none">
          <dataDescriptor expression="HC.CALL_STATUS"
           descriptiveExpression="CALL_STATUS" order="3" width="10"/>
        </dataItem>
        <dataItem name="RECVD_DATE" datatype="vchar2" columnOrder="19"
         defaultWidth="100000" defaultHeight="10000" columnFlags="3"
         defaultLabel="Recvd Date" breakOrder="descending">
          <dataDescriptor expression="HC.RECVD_DATE"
           descriptiveExpression="RECVD_DATE" order="6" width="10"/>
        </dataItem>
        <dataItem name="HP_CUST_ID" datatype="vchar2" columnOrder="21"
         width="25" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Hp Cust Id" breakOrder="none">
          <xmlSettings xmlTag="CUST_ID1"/>
          <dataDescriptor expression="HP.CUST_ID"
           descriptiveExpression="HP_CUST_ID" order="8" width="25"/>
        </dataItem>
        <dataItem name="TERRITORY" datatype="vchar2" columnOrder="22"
         width="2" defaultWidth="20000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Territory" breakOrder="none">
          <dataDescriptor expression="HP.TERRITORY"
           descriptiveExpression="TERRITORY" order="10" width="2"/>
        </dataItem>
        <dataItem name="CONSULTANT" datatype="vchar2" columnOrder="23"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Consultant" breakOrder="none">
          <dataDescriptor expression="HP.CONSULTANT"
           descriptiveExpression="CONSULTANT" order="11" width="30"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="24" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Long Address Number">
          <dataDescriptor expression="AB.LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="12" width="20"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_Title_Dates" source="cf_title_datesformula"
     datatype="character" width="40" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.15637" y="0.68750" width="1.22913" height="0.19995"/>
    </formula>
    <formula name="CF_Title_Name" source="cf_title_nameformula"
     datatype="character" width="50" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.23962" y="0.33337" width="0.97913" height="0.19995"/>
    </formula>
    <summary name="CS_Record_Count" source="CUST_ID" function="count"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Record Count">
      <displayInfo x="3.23962" y="1.08337" width="1.05212" height="0.19995"/>
    </summary>
    <formula name="CF_No_Data_Msg" source="cf_no_data_foundformula"
     datatype="character" width="75" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.23962" y="1.43738" width="1.14575" height="0.25000"/>
    </formula>
  </data>
  <layout>
  <section name="main">
    <body width="8.00000" height="9.31250">
      <location x="0.00000" y="0.68750"/>
      <frame name="M_Contract_Rollup_GRPFR">
        <geometryInfo x="0.00000" y="0.00000" width="7.81250" height="2.12500"
        />
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_Call_Category_RPFR" source="G_CALL_CATEGORY"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.00000" width="7.81250"
           height="1.06250"/>
          <generalLayout pageProtect="yes" verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"
           fillBackgroundColor="gray32"/>
          <field name="F_Call_Category" source="CALL_CATEGORY"
           spacing="single" alignment="start">
            <font face="Arial" size="9" bold="yes"/>
            <geometryInfo x="0.00000" y="0.00000" width="1.56250"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"
             fillBackgroundColor="gray32"/>
          </field>
          <field name="F_Call_Category_Count" source="CS_Call_Category_Count"
           spacing="single" alignment="start">
            <font face="Arial" size="9" bold="yes"/>
            <geometryInfo x="1.56250" y="0.00000" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"
             fillBackgroundColor="gray32"/>
          </field>
          <frame name="M_Call_Type_Enclosing_Frm">
            <geometryInfo x="0.00000" y="0.18750" width="7.81250"
             height="0.87500"/>
            <generalLayout verticalElasticity="variable"/>
            <visualSettings fillPattern="transparent"/>
            <repeatingFrame name="R_Call_Type_rpfrm" source="G_CALL_TYPE"
             printDirection="down" minWidowRecords="1" columnMode="no">
              <geometryInfo x="0.06250" y="0.18750" width="7.75000"
               height="0.87500"/>
              <generalLayout pageProtect="yes" verticalElasticity="expand"/>
              <visualSettings fillPattern="transparent"
               fillBackgroundColor="gray24"/>
              <frame name="M_Cause_Enclosing_Frm">
                <geometryInfo x="0.06250" y="0.37500" width="7.75000"
                 height="0.68750"/>
                <generalLayout verticalElasticity="variable"/>
                <visualSettings fillPattern="transparent"/>
                <repeatingFrame name="R_Cause_RP_GRPFR" source="G_CAUSE"
                 printDirection="down" minWidowRecords="1" columnMode="no">
                  <geometryInfo x="0.18750" y="0.37500" width="7.62500"
                   height="0.68750"/>
                  <generalLayout verticalElasticity="expand"/>
                  <visualSettings fillPattern="transparent"
                   fillBackgroundColor="gray16"/>
                  <field name="F_Cause" source="CAUSE" spacing="single"
                   alignment="start">
                    <font face="Arial" size="8" bold="yes" italic="yes"/>
                    <geometryInfo x="0.18750" y="0.37500" width="1.18750"
                     height="0.18750"/>
                    <advancedLayout printObjectOnPage="allPage"
                     basePrintingOn="enclosingObject"/>
                    <visualSettings fillPattern="transparent"
                     fillBackgroundColor="gray16"/>
                  </field>
                  <field name="F_Cause_Count" source="CS_Cause_Count"
                   spacing="single" alignment="start">
                    <font face="Arial" size="8" bold="yes" italic="yes"/>
                    <geometryInfo x="1.68750" y="0.37500" width="0.56250"
                     height="0.18750"/>
                    <advancedLayout printObjectOnPage="allPage"
                     basePrintingOn="enclosingObject"/>
                    <visualSettings fillPattern="transparent"
                     fillBackgroundColor="gray16"/>
                  </field>
                  <frame name="M_Contract_Detail_Enclosing">
                    <geometryInfo x="0.18750" y="0.56250" width="7.62500"
                     height="0.50000"/>
                    <generalLayout verticalElasticity="variable"/>
                    <visualSettings fillPattern="transparent"/>
                    <frame name="M_Contract_Detail_Hdgs_Frm">
                      <geometryInfo x="0.18750" y="0.56250" width="7.61462"
                       height="0.25000"/>
                      <advancedLayout printObjectOnPage="allPage"
                       basePrintingOn="enclosingObject"/>
                      <visualSettings fillPattern="transparent"/>
                      <text name="B_1">
                        <geometryInfo x="0.31250" y="0.62500" width="0.68750"
                         height="0.16663"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[Member # ]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_2">
                        <geometryInfo x="1.12500" y="0.62500" width="1.06250"
                         height="0.15625"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[Shop Name]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_3">
                        <geometryInfo x="3.06250" y="0.62500" width="1.00000"
                         height="0.15625"/>
                        <generalLayout>
                          <conditionalFormat>
                            <formatException
                             label="(:P_DIVISION = &apos;blank&apos;)">
                              <font face="Courier New" size="12"/>
                            <cond name="first" column="P_DIVISION"
                             exception="1" lowValue="blank" conjunction="1"/>
                            </formatException>
                          </conditionalFormat>
                        </generalLayout>
                        <advancedLayout formatTrigger="b_3formattrigger"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[FBC Information]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_4">
                        <geometryInfo x="4.43750" y="0.62500" width="1.00000"
                         height="0.15625"/>
                        <generalLayout>
                          <conditionalFormat>
                            <formatException
                             label="(:P_DIVISION != &apos;All&apos;)">
                              <font face="Courier New" size="12"/>
                            <cond name="first" column="P_DIVISION"
                             exception="2" lowValue="&apos;All&apos;"
                             conjunction="2"/>
                            </formatException>
                          </conditionalFormat>
                        </generalLayout>
                        <advancedLayout formatTrigger="b_4formattrigger"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[DIV Information]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_5">
                        <geometryInfo x="5.62500" y="0.62500" width="0.75000"
                         height="0.15625"/>
                        <visualSettings fillBackgroundColor="r50g50b88"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[Date Opened]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_6">
                        <textSettings justify="center"/>
                        <geometryInfo x="6.37500" y="0.62500" width="0.78125"
                         height="0.15625"/>
                        <visualSettings fillBackgroundColor="r50g50b88"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[Date Closed]]>
                          </string>
                        </textSegment>
                      </text>
                      <text name="B_7">
                        <geometryInfo x="7.18750" y="0.62500" width="0.48962"
                         height="0.15625"/>
                        <visualSettings fillBackgroundColor="r50g50b88"/>
                        <textSegment>
                          <font face="Arial" size="8" bold="yes"/>
                          <string>
                          <![CDATA[Status]]>
                          </string>
                        </textSegment>
                      </text>
                    </frame>
                    <repeatingFrame name="R_Contract_Detail_Rpfrm"
                     source="G_CONTRACT_DETAIL" printDirection="down"
                     minWidowRecords="1" columnMode="no">
                      <geometryInfo x="0.18750" y="0.81250" width="7.62500"
                       height="0.25000"/>
                      <generalLayout verticalElasticity="expand"/>
                      <visualSettings fillPattern="transparent"/>
                      <field name="F_Cust_Id" source="CUST_ID"
                       spacing="single" alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="0.25000" y="0.87500" width="0.62500"
                         height="0.12500"/>
                        <visualSettings fillPattern="transparent"/>
                      </field>
                      <field name="F_Company" source="COMPANY_NAME"
                       spacing="single" alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="0.87500" y="0.87500" width="2.12500"
                         height="0.12500"/>
                        <visualSettings fillPattern="transparent"/>
                      </field>
                      <field name="F_Recvd_Date" source="RECVD_DATE"
                       spacing="single" alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="5.68750" y="0.87500" width="0.68750"
                         height="0.12500"/>
                        <visualSettings fillPattern="transparent"/>
                      </field>
                      <field name="F_Closed_Date" source="CLOSED_DATE"
                       spacing="single" alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="6.43750" y="0.87500" width="0.68750"
                         height="0.12500"/>
                        <visualSettings fillPattern="transparent"/>
                      </field>
                      <field name="F_Call_Status" source="CALL_STATUS"
                       spacing="single" alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="7.18750" y="0.87500" width="0.62500"
                         height="0.12500"/>
                        <visualSettings fillPattern="transparent"/>
                      </field>
                      <field name="F_FBC_Information" source="FBC_Info"
                       alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="3.06250" y="0.87500" width="1.31250"
                         height="0.12500"/>
                        <generalLayout>
                          <conditionalFormat>
                            <formatException
                             label="((:P_DIVISION = &apos;blank&apos;) or (:FBC_Info = &apos;.&apos;)) "
                              >
                              <font face="Arial" size="8"/>
                            <cond name="first" column="P_DIVISION"
                             exception="1" lowValue="blank" conjunction="2"/>
                            </formatException>
                          </conditionalFormat>
                        </generalLayout>
                        <advancedLayout
                         formatTrigger="f_fbc_informationformattrigger"/>
                      </field>
                      <field name="F_DIV_Information" source="DIV_Info"
                       alignment="start">
                        <font face="Arial" size="8"/>
                        <geometryInfo x="4.43750" y="0.87500" width="1.18750"
                         height="0.12500"/>
                        <generalLayout>
                          <conditionalFormat>
                            <formatException
                             label="(:P_DIVISION != &apos;All&apos;)">
                              <font face="Arial" size="8"/>
                            <cond name="first" column="P_DIVISION"
                             exception="2" lowValue="&apos;All&apos;"
                             conjunction="2"/>
                            </formatException>
                          </conditionalFormat>
                        </generalLayout>
                        <advancedLayout
                         formatTrigger="f_div_informationformattrigger"/>
                      </field>
                    </repeatingFrame>
                    <line name="B_11" arrow="none">
                      <geometryInfo x="0.18750" y="0.75000" width="7.62500"
                       height="0.00000"/>
                      <advancedLayout printObjectOnPage="allPage"
                       basePrintingOn="enclosingObject"/>
                      <visualSettings fillPattern="transparent"
                       linePattern="solid"/>
                      <points>
                        <point x="0.18750" y="0.75000"/>
                        <point x="7.81250" y="0.75000"/>
                      </points>
                    </line>
                  </frame>
                </repeatingFrame>
              </frame>
              <field name="F_Call_Type" source="CALL_TYPE" spacing="single"
               alignment="start">
                <font face="Arial" size="8" bold="yes" italic="yes"/>
                <geometryInfo x="0.06250" y="0.18750" width="1.18750"
                 height="0.18750"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"
                 fillBackgroundColor="gray24"/>
              </field>
              <field name="F_Call_Type_Count" source="CS_Call_Type_Count"
               spacing="single" alignment="start">
                <font face="Arial" size="8" bold="yes" italic="yes"/>
                <geometryInfo x="1.62500" y="0.18750" width="0.68750"
                 height="0.18750"/>
                <advancedLayout printObjectOnPage="allPage"
                 basePrintingOn="enclosingObject"/>
                <visualSettings fillPattern="transparent"
                 fillBackgroundColor="gray24"/>
              </field>
            </repeatingFrame>
          </frame>
        </repeatingFrame>
        <text name="B_10">
          <textSettings justify="center"/>
          <geometryInfo x="2.56250" y="1.12500" width="3.18750"
           height="0.18750"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[&<CF_No_Data_Msg>]]>
            </string>
          </textSegment>
        </text>
        <text name="B_12">
          <textSettings justify="center"/>
          <geometryInfo x="3.68750" y="1.50000" width="1.18750"
           height="0.16663"/>
          <visualSettings fillBackgroundColor="green"/>
          <textSegment>
            <font face="Arial" size="9" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <text name="B_13">
          <textSettings justify="center"/>
          <geometryInfo x="4.00000" y="1.68750" width="0.50000"
           height="0.16663"/>
          <textSegment>
            <font face="Arial" size="8"/>
            <string>
            <![CDATA[V 1.16]]>
            </string>
          </textSegment>
        </text>
      </frame>
    </body>
    <margin>
      <text name="B_8">
        <textSettings justify="center"/>
        <geometryInfo x="2.81250" y="0.18750" width="2.93750" height="0.21875"
        />
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[Contract Rollup &<CF_TITLE_NAME>]]>
          </string>
        </textSegment>
      </text>
      <text name="B_9">
        <textSettings justify="center"/>
        <geometryInfo x="2.81250" y="0.43750" width="2.93750" height="0.16663"
        />
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[&<CF_TITLE_DATES>]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION = 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_title_nameformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_NameFormula return VARCHAR2 is
lv_tmp_title varchar2(50);
begin
	lv_tmp_title := title_name(:p_member, :p_territory, :p_division);
  If lv_tmp_title is Null then
    return lv_tmp_title;
  else
   	return ' for '|| lv_tmp_title;
  end if;

  exception when others then
    	return null;
  	
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_informationformattrigger">
      <textSource>
      <![CDATA[function F_FBC_InformationFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if ((:P_DIVISION = 'blank') or
      (:FBC_Info = '.')) 
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_div_informationformattrigger">
      <textSource>
      <![CDATA[function F_DIV_InformationFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_title_datesformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_DatesFormula return Char is
lv_title_dates varchar2(40);
begin
  lv_title_dates :=('From:  '||:P_DATE1 ||'    '||'To:  '||:P_DATE2);
  
  return (lv_title_dates);
end;]]>
      </textSource>
    </function>
    <function name="cf_no_data_foundformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_data_foundFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	

end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
