<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="TransmissionRevenueReport" DTDVersion="9.0.2.0.10"
 afterParameterFormTrigger="afterpform">
  <xmlSettings xmlTag="TRANSMISSIONREVENUEREPORT" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="p_order_by" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_REPORT_TYPE" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Transmission_Revenue ">
      <select canParse="no">
      <![CDATA[SELECT ALL
substr(WW.MAILING_NAME,1,30) as shop_name,
AB.LONG_ADDRESS_NUMBER,
AB.MEMBER_STATUS,
Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1,trim(' ' from to_char( MRT.TIER_2_REBATE_AMOUNT,'$990.00')) || '/' || trim( ' ' from to_char(MRT.TIER_3_REBATE_AMOUNT,'$990.00')),MR.CURR_MTH_SPECIAL_DEAL_ORDS) as SD_Rate,
AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' ||  TI.FBC_NAME as FBC_Info,
AB.DIVISION_MANAGER_CODE || ' ' || RI.MANAGER_NAME as DIV_Info,
SUM(MR.CURR_MTH_MERC_TRANSMISSIONS) as Curr_Trans_Count,
SUM(MR.PYR_MTH_MERC_TRANSMISSIONS) as Pry_Trans_Count,

SUM(MR.CURR_MTH_MERC_TRANSMISSIONS) - SUM(MR.PYR_MTH_MERC_TRANSMISSIONS)  as Trans_Count_Var,

SUM( MR.CURR_MTH_TRANS_DOLLARS) as Curr_Trans_Rev,
SUM( MR.PYR_MTH_TRANS_DOLLARS) as Pry_Trans_Rev,

SUM(MR.CURR_MTH_TRANS_DOLLARS)  - SUM( MR.PYR_MTH_TRANS_DOLLARS)  as Trans_Rev_Var,

-- following fields are for the All Members Report 
sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS) as SD_Count,
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.CURR_MTH_MERC_TRANSMISSIONS, 0)) as SPD_Curr_Trans_Count,
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.PYR_MTH_MERC_TRANSMISSIONS,0)) as SPD_Pry_Trans_Count,
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.CURR_MTH_MERC_TRANSMISSIONS, 0)) -
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.PYR_MTH_MERC_TRANSMISSIONS,0)) as  SPD_Count_Var,

SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.CURR_MTH_TRANS_DOLLARS,0)) as SPD_Curr_Trans_Rev,
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.PYR_MTH_TRANS_DOLLARS,0)) as SPD_Pry_Trans_Rev,

SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.CURR_MTH_TRANS_DOLLARS,0)) -
SUM(Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1, MR.PYR_MTH_TRANS_DOLLARS,0)) as  SPD_Rev_Var 
FROM  JDE_MEMBER_REVENUE_SUMMARY MR,
            JDE_ADDRESS_BOOK AB,
            TERRITORY_INFO TI,
            REGION_INFO RI,
            JDE_WHOS_WHO WW,
            JDE_MEMBER_REBATE_TIERS_VW MRT

WHERE  ((:P_TERRITORY = 'All' or :P_DIVISION = 'All')
        OR  ((:P_TERRITORY = 'blank' or  AB.FIELD_ACCT_REPRESENTATIVE_CODE = :P_TERRITORY )
      AND  (:P_DIVISION = 'blank'  or  AB.DIVISION_MANAGER_CODE = :P_DIVISION)))
      AND MR.EFFECTIVE_MONTH_DATE >=  TO_DATE(:P_MONTH1 || '/01/' || :P_YEAR1, 'MM/DD/YYYY') 
      AND MR.EFFECTIVE_MONTH_DATE <=  TO_DATE(:P_MONTH2 || '/01/' || :P_YEAR2, 'MM/DD/YYYY') 
      AND sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS) = 
            DECODE (:P_REPORT_TYPE, 'SDM', 1, 
         DECODE (:P_REPORT_TYPE, 'NSDM', 0, SIGN(MR.CURR_MTH_SPECIAL_DEAL_ORDS)))
-- SDM-SPECIAL DEAL MEMBERS- CURR MONTH SPECIAL DEAL ORDERS > 0
-- NSDM-NON SPECIAL DEAL MEMBERS- CURR MONTH SPECIAL DEAL ORDERS = 0
      AND MRT.EFFECTIVE_DATE =  TO_DATE(:P_MONTH2 ||'/01/'|| :P_YEAR2,'MM/DD/YYYY') 
      AND MRT.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND MR.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
      AND RI.REGION = AB.DIVISION_MANAGER_CODE
      AND WW.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND WW.LINE_ID = 0
      AND AB.SEARCH_TYPE in ('C' , 'I')
GROUP BY
  substr(WW.MAILING_NAME,1,30),
  AB.LONG_ADDRESS_NUMBER,
  AB.MEMBER_STATUS,
  Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1,trim(' ' from to_char( MRT.TIER_2_REBATE_AMOUNT,'$990.00')) || '/'  || trim( ' ' from to_char(MRT.TIER_3_REBATE_AMOUNT,'$990.00')),MR.CURR_MTH_SPECIAL_DEAL_ORDS),
  AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' ||  TI.FBC_NAME,
AB.DIVISION_MANAGER_CODE || ' ' || RI.MANAGER_NAME,
  sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS)
&p_order_by]]>
      </select>
      <displayInfo x="0.97852" y="0.15955" width="2.04358" height="0.19995"/>
      <group name="G_DETAIL_INFORMATION">
        <displayInfo x="0.76624" y="0.77979" width="2.44836" height="3.50684"
        />
        <dataItem name="SHOP_NAME" datatype="vchar2" columnOrder="23"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Shop Name">
          <dataDescriptor expression="SHOP_NAME"
           descriptiveExpression="SHOP_NAME" order="1" width="30"/>
        </dataItem>
        <dataItem name="MEMBER_STATUS" datatype="vchar2" columnOrder="25"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="MEMBER_STATUS"
           descriptiveExpression="MEMBER_STATUS" order="3" width="30"/>
        </dataItem>
        <dataItem name="FBC_Info" datatype="vchar2" columnOrder="21"
         width="34" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor expression="FBC_INFO"
           descriptiveExpression="FBC_INFO" order="5" width="34"/>
        </dataItem>
        <dataItem name="DIV_Info" datatype="vchar2" columnOrder="20"
         width="34" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor expression="DIV_INFO"
           descriptiveExpression="DIV_INFO" order="6" width="34"/>
        </dataItem>
        <dataItem name="SD_RATE" datatype="vchar2" columnOrder="24" width="40"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Sd Rate" breakOrder="none">
          <dataDescriptor expression="SD_RATE" descriptiveExpression="SD_RATE"
           order="4" width="40"/>
        </dataItem>
        <dataItem name="CURR_TRANS_COUNT" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Curr Trans Count"
         breakOrder="none">
          <dataDescriptor expression="CURR_TRANS_COUNT"
           descriptiveExpression="CURR_TRANS_COUNT" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PRY_TRANS_COUNT" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Pry Trans Count"
         breakOrder="none">
          <dataDescriptor expression="PRY_TRANS_COUNT"
           descriptiveExpression="PRY_TRANS_COUNT" order="8"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="CURR_TRANS_REV" oracleDatatype="number"
         columnOrder="28" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Curr Trans Rev"
         breakOrder="none">
          <dataDescriptor expression="CURR_TRANS_REV"
           descriptiveExpression="CURR_TRANS_REV" order="10"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PRY_TRANS_REV" oracleDatatype="number"
         columnOrder="29" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Pry Trans Rev"
         breakOrder="none">
          <dataDescriptor expression="PRY_TRANS_REV"
           descriptiveExpression="PRY_TRANS_REV" order="11"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="22" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Long Address Number" breakOrder="none">
          <dataDescriptor expression="LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="2" width="20"/>
        </dataItem>
        <dataItem name="SD_COUNT" oracleDatatype="number" columnOrder="30"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Sd Count" breakOrder="none">
          <dataDescriptor expression="SD_COUNT"
           descriptiveExpression="SD_COUNT" order="13" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_CURR_TRANS_COUNT" oracleDatatype="number"
         columnOrder="31" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Spd Curr Trans Count" breakOrder="none">
          <dataDescriptor expression="SPD_CURR_TRANS_COUNT"
           descriptiveExpression="SPD_CURR_TRANS_COUNT" order="14"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_PRY_TRANS_COUNT" oracleDatatype="number"
         columnOrder="32" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Spd Pry Trans Count" breakOrder="none">
          <dataDescriptor expression="SPD_PRY_TRANS_COUNT"
           descriptiveExpression="SPD_PRY_TRANS_COUNT" order="15"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_CURR_TRANS_REV" oracleDatatype="number"
         columnOrder="33" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Spd Curr Trans Rev" breakOrder="none">
          <dataDescriptor expression="SPD_CURR_TRANS_REV"
           descriptiveExpression="SPD_CURR_TRANS_REV" order="17"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_PRY_TRANS_REV" oracleDatatype="number"
         columnOrder="34" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Spd Pry Trans Rev" breakOrder="none">
          <dataDescriptor expression="SPD_PRY_TRANS_REV"
           descriptiveExpression="SPD_PRY_TRANS_REV" order="18"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="TRANS_COUNT_VAR" oracleDatatype="number"
         columnOrder="35" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Trans Count Var"
         breakOrder="none">
          <dataDescriptor expression="TRANS_COUNT_VAR"
           descriptiveExpression="TRANS_COUNT_VAR" order="9"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="TRANS_REV_VAR" oracleDatatype="number"
         columnOrder="36" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Trans Rev Var"
         breakOrder="none">
          <dataDescriptor expression="TRANS_REV_VAR"
           descriptiveExpression="TRANS_REV_VAR" order="12"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_COUNT_VAR" oracleDatatype="number"
         columnOrder="37" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Spd Count Var"
         breakOrder="none">
          <dataDescriptor expression="SPD_COUNT_VAR"
           descriptiveExpression="SPD_COUNT_VAR" order="16"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="SPD_REV_VAR" oracleDatatype="number" columnOrder="38"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Spd Rev Var" breakOrder="none">
          <dataDescriptor expression="SPD_REV_VAR"
           descriptiveExpression="SPD_REV_VAR" order="19"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="CS_Total_Curr_Trans_Count" source="CURR_TRANS_COUNT"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Curr Trans Count">
      <displayInfo x="3.36462" y="1.00000" width="1.67712" height="0.19995"/>
    </summary>
    <summary name="CS_Total_Pry_Trans_Count" source="PRY_TRANS_COUNT"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Pry Trans Count">
      <displayInfo x="3.36462" y="1.35413" width="1.64587" height="0.20837"/>
    </summary>
    <summary name="CS_Total_Curr_Trans_Rev" source="CURR_TRANS_REV"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Curr Trans Rev">
      <displayInfo x="3.39612" y="1.67712" width="1.58325" height="0.21875"/>
    </summary>
    <summary name="CS_SPD_Trans_Count_Var_Total" source="SPD_COUNT_VAR"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Spd Trans Count Var Total">
      <displayInfo x="3.42712" y="2.85425" width="1.86450" height="0.22913"/>
    </summary>
    <summary name="CS_Total_Pry_Trans_Rev" source="PRY_TRANS_REV"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Pry Trans Rev">
      <displayInfo x="3.43774" y="2.03149" width="1.59375" height="0.23962"/>
    </summary>
    <summary name="CS_Trans_Count_Var_Total" source="TRANS_COUNT_VAR"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Trans Count Var Total">
      <displayInfo x="3.50000" y="2.44788" width="1.54163" height="0.21875"/>
    </summary>
    <summary name="CS_All_Members_Count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs All Members Count">
      <displayInfo x="3.51050" y="0.65625" width="1.37500" height="0.19995"/>
    </summary>
    <formula name="CF_Title_Dates" source="cf_title_datesformula"
     datatype="character" width="45" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.55200" y="0.06262" width="1.31250" height="0.20825"/>
    </formula>
    <formula name="CF_TitleGroup" source="cf_titleggroupformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.56262" y="0.33337" width="1.32288" height="0.20825"/>
    </formula>
    <formula name="CF_Title_Terr_Reg" source="cf_title_terr_regformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.10413" y="0.08350" width="1.25000" height="0.19995"/>
    </formula>
    <formula name="CF_No_Data_msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.10413" y="0.35425" width="1.14587" height="0.19995"/>
    </formula>
    <summary name="CS_SD_Members_Count" source="SD_COUNT" function="sum"
     width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Sd Members Count" valueIfNull="0">
      <displayInfo x="5.10413" y="0.63550" width="1.37500" height="0.20837"/>
    </summary>
    <summary name="CS_Total_SPD_Curr_Trans_Count"
     source="SPD_CURR_TRANS_COUNT" function="sum" width="17" precision="15"
     reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Spd Curr Trans Count">
      <displayInfo x="5.22925" y="1.02087" width="1.87488" height="0.19995"/>
    </summary>
    <summary name="CS_Total_SPD_Pry_Trans_Count" source="SPD_PRY_TRANS_COUNT"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Spd Pry Trans Count">
      <displayInfo x="5.23962" y="1.35413" width="1.83325" height="0.19995"/>
    </summary>
    <summary name="CS_Total_SPD_Curr_Trans_Rev" source="SPD_CURR_TRANS_REV"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Spd Curr Trans Rev">
      <displayInfo x="5.29163" y="1.64587" width="1.78125" height="0.21875"/>
    </summary>
    <summary name="CS_Total_SPD_Pry_Trans_Rev" source="SPD_PRY_TRANS_REV"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Total Spd Pry Trans Rev">
      <displayInfo x="5.39587" y="2.00000" width="1.76038" height="0.25000"/>
    </summary>
    <summary name="CS_SPD_Trans_Rev_Var_Total" source="SPD_REV_VAR"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Spd Trans Rev Var Total">
      <displayInfo x="5.41663" y="2.89575" width="1.73950" height="0.20837"/>
    </summary>
    <summary name="CS_Trans_Revenue_Var_Total" source="TRANS_REV_VAR"
     function="sum" width="17" precision="15" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Trans Revenue Var Total">
      <displayInfo x="5.42712" y="2.45837" width="1.66663" height="0.20825"/>
    </summary>
    <summary name="CS_Record_count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="6.53125" y="0.35400" width="1.11462" height="0.19995"/>
    </summary>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.62500" height="7.31250">
      <location x="0.00000" y="0.81250"/>
      <frame name="M_G_Trans_Revenue_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="13.62500"
         height="1.50000"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <frame name="M_Report_Headings_Group">
          <geometryInfo x="0.00000" y="0.06250" width="13.62500"
           height="0.60449"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_1">
            <textSettings justify="center"/>
            <geometryInfo x="0.12500" y="0.31250" width="0.50000"
             height="0.29163"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Shop
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Name]]>
              </string>
            </textSegment>
          </text>
          <text name="B_2">
            <geometryInfo x="2.25000" y="0.31250" width="0.50000"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Member
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <geometryInfo x="4.00000" y="0.31250" width="1.12500"
             height="0.31250"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                  <font face="Courier New" size="12"/>
                <cond name="first" column="P_DIVISION" exception="1"
                 lowValue="blank" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_3formattrigger"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Field Business Consultant]]>
              </string>
            </textSegment>
          </text>
          <text name="B_4">
            <geometryInfo x="5.75000" y="0.31250" width="0.68750"
             height="0.31250"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION != &apos;All&apos;)">
                  <font face="Courier New" size="12"/>
                <cond name="first" column="P_DIVISION" exception="2"
                 lowValue="All" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_4formattrigger"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Division Manager]]>
              </string>
            </textSegment>
          </text>
          <text name="B_6">
            <textSettings justify="center"/>
            <geometryInfo x="8.12500" y="0.31250" width="0.62500"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Current
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period
]]>
              </string>
            </textSegment>
          </text>
          <text name="B_7">
            <textSettings justify="center"/>
            <geometryInfo x="8.81250" y="0.31250" width="0.75000"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Prior Year
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period
]]>
              </string>
            </textSegment>
          </text>
          <text name="B_8">
            <textSettings justify="center"/>
            <geometryInfo x="10.75000" y="0.31250" width="0.75000"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Current
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period ]]>
              </string>
            </textSegment>
          </text>
          <text name="B_9">
            <textSettings justify="center"/>
            <geometryInfo x="11.75000" y="0.31250" width="0.81250"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Prior Year
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period ]]>
              </string>
            </textSegment>
          </text>
          <text name="B_22">
            <geometryInfo x="2.93750" y="0.31250" width="0.56250"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Member
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Status]]>
              </string>
            </textSegment>
          </text>
          <text name="B_10">
            <textSettings justify="center"/>
            <geometryInfo x="8.18750" y="0.06250" width="2.12500"
             height="0.14587"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Transmission Counts]]>
              </string>
            </textSegment>
          </text>
          <text name="B_14">
            <textSettings justify="center"/>
            <geometryInfo x="10.81250" y="0.06250" width="2.56250"
             height="0.14587"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Transmission Revenue]]>
              </string>
            </textSegment>
          </text>
          <line name="B_24" arrow="none">
            <geometryInfo x="8.56250" y="0.31250" width="1.75000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent"/>
            <points>
              <point x="8.56250" y="0.31250"/>
              <point x="10.31250" y="0.31250"/>
            </points>
          </line>
          <line name="B_25" arrow="none">
            <geometryInfo x="8.18750" y="0.25000" width="2.12500"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="8.18750" y="0.25000"/>
              <point x="10.31250" y="0.25000"/>
            </points>
          </line>
          <line name="B_26" arrow="none">
            <geometryInfo x="10.81250" y="0.25000" width="2.68750"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="10.81250" y="0.25000"/>
              <point x="13.50000" y="0.25000"/>
            </points>
          </line>
          <text name="B_5">
            <textSettings justify="center"/>
            <geometryInfo x="7.25000" y="0.43750" width="0.81250"
             height="0.20837"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[SD Rate]]>
              </string>
            </textSegment>
          </text>
          <text name="B_27">
            <textSettings justify="center"/>
            <geometryInfo x="12.81250" y="0.31250" width="0.65625"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Variance]]>
              </string>
            </textSegment>
          </text>
          <text name="B_28">
            <textSettings justify="center"/>
            <geometryInfo x="9.68750" y="0.31250" width="0.63538"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Variance]]>
              </string>
            </textSegment>
          </text>
          <line name="B_31" arrow="none">
            <geometryInfo x="0.00000" y="0.62500" width="13.62500"
             height="0.00000"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings linePattern="solid"/>
            <points>
              <point x="0.00000" y="0.62500"/>
              <point x="13.62500" y="0.62500"/>
            </points>
          </line>
        </frame>
        <repeatingFrame name="R_G_Detail_Information"
         source="G_DETAIL_INFORMATION" printDirection="down"
         minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.68750" width="13.62500"
           height="0.29199"/>
          <generalLayout pageProtect="yes" verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_Shop_Name" source="SHOP_NAME" spacing="single"
           alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="0.00000" y="0.75000" width="2.12500"
             height="0.18750"/>
          </field>
          <field name="F_Long_Address_Number" source="LONG_ADDRESS_NUMBER"
           spacing="single" alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="2.18750" y="0.75000" width="0.68750"
             height="0.18750"/>
          </field>
          <field name="F_FBC_Info" source="FBC_Info" spacing="single"
           alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="4.00000" y="0.75000" width="1.81250"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_fbc_infoformattrigger"/>
          </field>
          <field name="F_Div_Info" source="DIV_Info" spacing="single"
           alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="5.68750" y="0.75000" width="1.50000"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION != &apos;All&apos;)">
                  <font face="Arial" size="7"/>
                  <formatVisualSettings borderPattern="solid"/>
                <cond name="first" column="P_DIVISION" exception="2"
                 lowValue="All" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="f_div_infoformattrigger"/>
          </field>
          <field name="F_SD_Rate" source="SD_RATE" spacing="single"
           alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="7.25000" y="0.75000" width="0.81250"
             height="0.18750"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:SD_RATE = &apos;0&apos;)">
                  <font face="Arial" size="7"/>
                <cond name="first" column="SD_RATE" exception="1" lowValue="0"
                 conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="f_sd_rateformattrigger"/>
          </field>
          <field name="F_Curr_Trans_Count" source="CURR_TRANS_COUNT"
           formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="8.18750" y="0.75000" width="0.43750"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_curr_trans_countformattrigge"/>
          </field>
          <field name="F_Pry_Trans_Count" source="PRY_TRANS_COUNT"
           formatMask="NN,NNN,NNN,NNN" spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="8.87500" y="0.75000" width="0.62500"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_pry_trans_countformattrigger"/>
          </field>
          <field name="F_Curr_Trans_Rev" source="CURR_TRANS_REV"
           formatMask="NNN,NNN,NNN.00 " spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="10.68750" y="0.75000" width="0.75000"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_curr_trans_revformattrigger"/>
          </field>
          <field name="F_Member_Status" source="MEMBER_STATUS"
           alignment="start">
            <font face="Courier New" size="8"/>
            <geometryInfo x="2.93750" y="0.75000" width="1.00000"
             height="0.18750"/>
            <generalLayout verticalElasticity="variable"/>
          </field>
          <field name="F_Pry_Trans_Rev" source="PRY_TRANS_REV"
           formatMask="NNN,NNN,NN0.00" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="11.68750" y="0.75000" width="0.75000"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_pry_trans_revformattrigger"/>
          </field>
          <field name="F_Trans_Count_Var" source="TRANS_COUNT_VAR"
           formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="9.62500" y="0.75000" width="0.62500"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Trans_Rev_Var" source="TRANS_REV_VAR"
           formatMask="NNN,NNN,NN0.00" alignment="end">
            <font face="Courier New" size="8"/>
            <geometryInfo x="12.68750" y="0.75000" width="0.75000"
             height="0.18750"/>
            <advancedLayout formatTrigger="f_trans_rev_varformattrigger"/>
          </field>
        </repeatingFrame>
        <text name="B_20">
          <textSettings justify="center"/>
          <geometryInfo x="5.87500" y="1.06250" width="3.06250"
           height="0.18750"/>
          <visualSettings fillBackgroundColor="r0g88b88"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[&<CF_No_Data_Msg>
]]>
            </string>
          </textSegment>
        </text>
      </frame>
      <frame name="M_Report_Total">
        <geometryInfo x="0.00000" y="1.54199" width="13.62500"
         height="1.33301"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_15">
          <geometryInfo x="0.00000" y="1.54199" width="1.00000"
           height="0.14551"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_15formattrigger"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[Grand Totals]]>
            </string>
          </textSegment>
        </text>
        <text name="B_16">
          <textSettings justify="end"/>
          <geometryInfo x="0.37500" y="1.75000" width="2.12500"
           height="0.25000"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g88b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_16formattrigger"/>
          <visualSettings fillBackgroundColor="r0g88b88"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[All Members Count:]]>
            </string>
          </textSegment>
        </text>
        <frame name="M_SDM_COUNT">
          <geometryInfo x="0.00000" y="2.00000" width="13.62500"
           height="0.31250"/>
          <advancedLayout formatTrigger="m_sdm_countformattrigger"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_SD_Members_Count" source="CS_SD_Members_Count"
           formatMask="N,NN0" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="2.93750" y="2.06250" width="1.00000"
             height="0.18750"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_SPD_Curr_Trans_count"
           source="CS_Total_SPD_Curr_Trans_Count" formatMask="NNN,NNN,NNN"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="8.00000" y="2.06250" width="0.68750"
             height="0.18213"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_SPD_Pry_Trans_Count"
           source="CS_Total_SPD_Pry_Trans_Count" formatMask="NNN,NNN,NNN"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="8.81250" y="2.06250" width="0.75000"
             height="0.18213"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_SPD_Curr_Trans_Rev"
           source="CS_Total_SPD_Curr_Trans_Rev" formatMask="$NNN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="10.31250" y="2.06250" width="1.12500"
             height="0.18213"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Total_SPD_Pry_Trans_Rev"
           source="CS_Total_SPD_Pry_Trans_Rev" formatMask="$NNN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="11.50000" y="2.06250" width="1.00000"
             height="0.18213"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_SPD_Trans_Count_Var_Total"
           source="CS_SPD_Trans_Count_Var_Total" formatMask="NNN,NNN,NNN"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="9.62500" y="2.06250" width="0.56250"
             height="0.18213"/>
            <advancedLayout formatTrigger="f_spd_trans_count_var_totalfor"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_SPD_Trans_Rev_Var_Total"
           source="CS_SPD_Trans_Rev_Var_Total" formatMask="$NNN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="12.62500" y="2.06250" width="0.93750"
             height="0.18213"/>
            <advancedLayout formatTrigger="f_spd_trans_rev_var_totalforma"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <text name="B_18">
            <textSettings justify="end"/>
            <geometryInfo x="0.37500" y="2.06250" width="2.12500"
             height="0.14587"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:CS_Record_count = &apos;0&apos;)">
                  <font face="Courier New" size="12"/>
                  <formatVisualSettings fillForegroundColor="r0g88b88"/>
                <cond name="first" column="CS_Record_count" exception="1"
                 lowValue="0" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_18formattrigger"/>
            <visualSettings fillBackgroundColor="r0g88b88"/>
            <textSegment>
              <font face="Courier New" size="8" bold="yes"/>
              <string>
              <![CDATA[Special Deal Members Count:]]>
              </string>
            </textSegment>
          </text>
        </frame>
        <field name="F_All_Members_Count" source="CS_All_Members_Count"
         formatMask="NNN,NNN,NNN" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="2.93750" y="1.75000" width="1.00000"
           height="0.18750"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Total_Curr_Trans_Count"
         source="CS_Total_Curr_Trans_Count" formatMask="NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.00000" y="1.81250" width="0.68750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Total_Pry_Trans_Count"
         source="CS_Total_Pry_Trans_Count" formatMask="NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.81250" y="1.81250" width="0.75000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Total_Curr_Trans_Rev" source="CS_Total_Curr_Trans_Rev"
         formatMask="$NNN,NNN,NN0.00" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="10.31250" y="1.81250" width="1.12500"
           height="0.12500"/>
        </field>
        <field name="F_Total_Pry_Trans_Rev" source="CS_Total_Pry_Trans_Rev"
         formatMask="$NNN,NNN,NNN,NN0.00" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="11.50000" y="1.81250" width="1.00000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_21">
          <textSettings justify="center"/>
          <geometryInfo x="6.31250" y="2.39612" width="1.00000"
           height="0.20837"/>
          <textSegment>
            <font face="Courier New" size="9" bold="yes"/>
            <string>
            <![CDATA[End of Report]]>
            </string>
          </textSegment>
        </text>
        <text name="B_23">
          <textSettings justify="center"/>
          <geometryInfo x="6.31250" y="2.62500" width="0.93750"
           height="0.16663"/>
          <textSegment>
            <font face="Courier New" size="8"/>
            <string>
            <![CDATA[V1.6]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Trans_Count_Var_Total"
         source="CS_Trans_Count_Var_Total" formatMask="NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="9.62500" y="1.81250" width="0.56250"
           height="0.12500"/>
          <advancedLayout formatTrigger="f_trans_count_var_totalformatt"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Trans_Revenue_Var_Total"
         source="CS_Trans_Revenue_Var_Total" formatMask="$NNN,NNN,NN0.00"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="12.62500" y="1.81250" width="0.93750"
           height="0.12500"/>
          <advancedLayout formatTrigger="f_trans_revenue_var_totalforma"/>
          <visualSettings fillPattern="transparent"/>
        </field>
      </frame>
    </body>
    <margin>
      <text name="B_12">
        <textSettings justify="center"/>
        <geometryInfo x="5.31250" y="0.06250" width="3.00000" height="0.68750"
        />
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[Transmission Revenue
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_TitleGroup>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Dates>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Terr_Reg>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Courier New" size="9" bold="yes"/>
        <geometryInfo x="0.25000" y="0.06250" width="0.83337" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_title_datesformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_DatesFormula return Char is
lv_title_dates varchar2(45);
begin
  lv_title_dates :=('From: '||:P_MONTH1||'/'||:P_YEAR1||' '||'To: '||:P_MONTH2||'/'||:P_YEAR2);
 
  return (lv_title_dates);
end; ]]>
      </textSource>
    </function>
    <function name="cf_title_terr_regformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_Terr_RegFormula return Char is
  lv_title3 varchar2 (80) null;
  temp_title varchar2(80);
begin
 temp_title := title_name (:p_member, :p_territory, :p_division); 
 If :p_territory != 'blank' and :p_territory != 'All' then
 	 lv_title3 := 'Territory # '||:p_territory ||' - '|| temp_title;
 elsif :p_division != 'blank' and :p_division != 'All' then
 	lv_title3 := 'Region # '||:p_division|| ' - ' ||temp_title;
 else
 	 lv_title3 := 'All Members';
 end if;
 return lv_title3;
end;
  	]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_msgFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	 
end;]]>
      </textSource>
    </function>
    <function name="b_15formattrigger">
      <textSource>
      <![CDATA[function B_15FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_16formattrigger">
      <textSource>
      <![CDATA[function B_16FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_18formattrigger">
      <textSource>
      <![CDATA[function B_18FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_infoformattrigger">
      <textSource>
      <![CDATA[function F_FBC_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if :P_TERRITORY != 'All' and :P_DIVISION = 'blank'
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if :P_TERRITORY != 'All' and :P_DIVISION = 'blank'
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_div_infoformattrigger">
      <textSource>
      <![CDATA[function F_Div_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_sd_rateformattrigger">
      <textSource>
      <![CDATA[function F_SD_RateFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:SD_RATE = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_titleggroupformula" returnType="character">
      <textSource>
      <![CDATA[function CF_TitleGgroupFormula return Char is
  lv_title varchar2 (80):= 'All Members';
begin
  If :P_REPORT_TYPE = 'NSDM' then
  	lv_title := 'Non Special Deal Members';
  elsif :P_REPORT_TYPE = 'SDM' then
  	lv_title := 'Special Deal Members';
  end if;
  return lv_title;
end;
    
 ]]>
      </textSource>
    </function>
    <function name="afterpform">
      <textSource>
      <![CDATA[function AfterPForm return boolean is
begin
  IF :P_ORDER_BY = 'SN' THEN
  	:P_ORDER_BY := 'ORDER BY substr(WW.MAILING_NAME,1,30)';
  ELSIF
  	:P_ORDER_BY = 'TCHL' THEN
  	  :P_ORDER_BY := 'ORDER BY Curr_Trans_Count DESC'; 
  ELSIF
  	:P_ORDER_BY = 'TCLH' THEN
  	  :P_ORDER_BY := 'ORDER BY Curr_Trans_Count';
  END IF;	  
  return (TRUE);
end;
 ]]>
      </textSource>
    </function>
    <function name="f_curr_trans_revformattrigger">
      <textSource>
      <![CDATA[function F_Curr_Trans_RevFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CURR_TRANS_REV = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_curr_trans_countformattrigge">
      <textSource>
      <![CDATA[function F_Curr_Trans_CountFormatTrigge return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CURR_TRANS_COUNT = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_pry_trans_countformattrigger">
      <textSource>
      <![CDATA[function F_Pry_Trans_CountFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:PRY_TRANS_COUNT = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_pry_trans_revformattrigger">
      <textSource>
      <![CDATA[function F_Pry_Trans_RevFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:PRY_TRANS_REV = '.00')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_trans_rev_varformattrigger">
      <textSource>
      <![CDATA[function F_Trans_Rev_VarFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:TRANS_REV_VAR = '.00')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_trans_revenue_var_totalforma">
      <textSource>
      <![CDATA[function F_Trans_Revenue_Var_TotalForma return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Trans_Revenue_Var_Total = '.00')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_spd_trans_rev_var_totalforma">
      <textSource>
      <![CDATA[function F_SPD_Trans_Rev_Var_TotalForma return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_SPD_Trans_Rev_Var_Total = '.00')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_spd_trans_count_var_totalfor">
      <textSource>
      <![CDATA[function F_SPD_Trans_Count_Var_TotalFor return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_SPD_Trans_Count_Var_Total = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_trans_count_var_totalformatt">
      <textSource>
      <![CDATA[function F_Trans_Count_Var_TotalFormatT return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Trans_Count_Var_Total = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="m_sdm_countformattrigger">
      <textSource>
      <![CDATA[function M_SDM_COUNTFormatTrigger return boolean is
begin
   if (:P_REPORT_TYPE != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
