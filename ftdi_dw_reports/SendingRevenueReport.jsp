<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="SendingRevenueReport" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="SENDINGREVENUEREPORTXXX" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Sending_Revenue_Report">
      <select canParse="no">
      <![CDATA[SELECT ALL
AB.LONG_ADDRESS_NUMBER,
substr(WW.MAILING_NAME,1,25) as shop_name,
AB.DIVISION_MANAGER_CODE || ' ' || (substr(RI.MANAGER_NAME,1,18)) as DIV_Info,
AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' || (substr(TI.FBC_NAME,1,18)) as FBC_Info,
AB.MEMBER_STATUS,
Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1,trim(' ' from to_char( MRT.TIER_2_REBATE_AMOUNT,'$990.00')) || '/' || trim( ' ' from to_char(MRT.TIER_3_REBATE_AMOUNT,'$990.00')),MR.CURR_MTH_SPECIAL_DEAL_ORDS) as SD_Rate,
SUM(MR.CURR_MTH_SPECIAL_DEAL_ORDS + MR.CURR_MTH_3_DOLLAR_ORDERS + MR.CURR_MTH_4_DOLLAR_ORDERS
 + MR.CURR_MTH_0_DOLLAR_ORDERS) AS CURR_PERIOD_ORDERS,
SUM(MR.CURR_MTH_OUT_ORD_DOLLARS) AS CURR_PERIOD_REVENUE,
SUM(MR.PYR_MTH_SPECIAL_DEAL_ORDS + MR.PYR_MTH_3_DOLLAR_ORDERS + MR.PYR_MTH_4_DOLLAR_ORDERS + 
MR.PYR_MTH_0_DOLLAR_ORDERS) AS PYR_PERIOD_ORDERS,
SUM(MR.PYR_MTH_OUT_ORD_DOLLARS) AS PYR_PERIOD_REVENUE,

SUM(MR.CURR_MTH_SPECIAL_DEAL_ORDS + MR.CURR_MTH_3_DOLLAR_ORDERS + MR.CURR_MTH_4_DOLLAR_ORDERS
 + MR.CURR_MTH_0_DOLLAR_ORDERS) - SUM(MR.PYR_MTH_SPECIAL_DEAL_ORDS + MR.PYR_MTH_3_DOLLAR_ORDERS + MR.PYR_MTH_4_DOLLAR_ORDERS + MR.PYR_MTH_0_DOLLAR_ORDERS) AS ORDERS_VAR,

SUM(MR.CURR_MTH_OUT_ORD_DOLLARS)  - SUM(MR.PYR_MTH_OUT_ORD_DOLLARS) AS REVENUE_VAR,

SUM( decode( sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS), 0, (MR.CURR_MTH_SPECIAL_DEAL_ORDS + MR.CURR_MTH_3_DOLLAR_ORDERS + MR.CURR_MTH_4_DOLLAR_ORDERS + MR.CURR_MTH_0_DOLLAR_ORDERS), 0)) AS NSD_Curr_Period_Orders,
Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, MR.CURR_MTH_OUT_ORD_DOL_NON_SD)) as NSD_Curr_Period_Rev,
Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, (MR.PYR_MTH_SPECIAL_DEAL_ORDS + MR.PYR_MTH_3_DOLLAR_ORDERS +
MR.PYR_MTH_4_DOLLAR_ORDERS + MR.PYR_MTH_0_DOLLAR_ORDERS),0)) AS NSD_Pyr_Period_Orders,
Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, MR.PYR_MTH_OUT_ORD_DOL_NON_SD)) as NSD_Pyr_Period_Rev,

SUM( decode( sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS), 0, (MR.CURR_MTH_SPECIAL_DEAL_ORDS + MR.CURR_MTH_3_DOLLAR_ORDERS + MR.CURR_MTH_4_DOLLAR_ORDERS + MR.CURR_MTH_0_DOLLAR_ORDERS), 0)) - Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, (MR.PYR_MTH_SPECIAL_DEAL_ORDS + MR.PYR_MTH_3_DOLLAR_ORDERS + MR.PYR_MTH_4_DOLLAR_ORDERS +MR.PYR_MTH_0_DOLLAR_ORDERS),0)) AS NSD_ORDERS_VAR,

Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, MR.CURR_MTH_OUT_ORD_DOL_NON_SD)) - Sum(decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),0, MR.PYR_MTH_OUT_ORD_DOL_NON_SD)) as NSD_REV_VAR,

decode(sign(mr.curr_mth_special_deal_ords), 0,1,0) as Non_Special_Count,
decode (sign(mr.curr_mth_special_deal_ords), 1,'Special Deals',
 decode (sign(mr.curr_mth_4_dollar_orders), 1, '$4 Rebate USD / $5.25 Rebate CAD',
  decode (sign(mr.curr_mth_3_dollar_orders), 1, '$3 Rebate USD / $4.25 Rebate CAD',
   decode (sign(mr.curr_mth_0_dollar_orders), 1, '$0 Rebate USD / $0 Rebate CAD',
  '$0 Rebate USD / $0 Rebate CAD')))) as group_code
FROM
   JDE_ADDRESS_BOOK AB,
   JDE_MEMBER_REVENUE_SUMMARY MR,
   JDE_WHOS_WHO WW,
   JDE_MEMBER_REBATE_TIERS_VW MRT,
   REGION_INFO RI,
   TERRITORY_INFO TI
WHERE ((:P_TERRITORY = 'All' or  :P_DIVISION = 'All')
        OR ( :P_DIVISION != 'blank' and :P_DIVISION = AB.DIVISION_MANAGER_CODE)
        OR (:P_TERRITORY != 'blank' and :P_TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE))
      AND MR.EFFECTIVE_MONTH_DATE >= TO_DATE(:P_MONTH1||'/01/'||:P_YEAR1,'MM/DD/YYYY') 
      AND MR.EFFECTIVE_MONTH_DATE <= TO_DATE(:P_MONTH2||'/01/'||:P_YEAR2,'MM/DD/YYYY')
      AND MRT.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND MRT.EFFECTIVE_DATE = TO_DATE(:P_MONTH2 ||'/01/'||:P_YEAR2,'MM/DD/YYYY')
      AND MR.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
      AND RI.REGION = AB.DIVISION_MANAGER_CODE
      AND WW.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND WW.LINE_ID = 0
      AND AB.SEARCH_TYPE in ('C','I')
Group by
AB.LONG_ADDRESS_NUMBER,
substr(WW.MAILING_NAME,1,25),
AB.DIVISION_MANAGER_CODE || ' ' || (substr(RI.MANAGER_NAME,1,18)),
AB.FIELD_ACCT_REPRESENTATIVE_CODE || ' ' || (substr(TI.FBC_NAME,1,18)),
AB.MEMBER_STATUS,
Decode(sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS),1,trim(' ' from to_char( MRT.TIER_2_REBATE_AMOUNT,'$990.00')) || '/' || trim( ' ' from to_char(MRT.TIER_3_REBATE_AMOUNT,'$990.00')),MR.CURR_MTH_SPECIAL_DEAL_ORDS),
sign(MR.CURR_MTH_SPECIAL_DEAL_ORDS), 
decode(sign(mr.curr_mth_special_deal_ords), 0,1,0),
decode (sign(mr.curr_mth_special_deal_ords), 1,'Special Deals',
  decode (sign(mr.curr_mth_4_dollar_orders), 1, '$4 Rebate USD / $5.25 Rebate CAD',
   decode (sign(mr.curr_mth_3_dollar_orders), 1, '$3 Rebate USD / $4.25 Rebate CAD',
    decode (sign(mr.curr_mth_0_dollar_orders), 1, '$0 Rebate USD / $0 Rebate CAD',
     '$0 Rebate USD / $0 Rebate CAD')))) 
order by shop_name   
]]>
      </select>
      <displayInfo x="1.15198" y="0.15955" width="1.69373" height="0.19995"/>
      <group name="G_SD_REBATE_GROUPS">
        <displayInfo x="0.83936" y="0.71851" width="2.31787" height="1.62695"
        />
        <dataItem name="GROUP_CODE" datatype="vchar2" columnOrder="27"
         width="32" defaultWidth="100000" defaultHeight="10000"
         columnFlags="3" defaultLabel="Group Code" breakOrder="descending">
          <dataDescriptor expression="GROUP_CODE"
           descriptiveExpression="GROUP_CODE" order="20" width="32"/>
        </dataItem>
        <summary name="CS_Curr_Period_Orders_Total"
         source="CURR_PERIOD_ORDERS" function="sum" width="20" precision="10"
         reset="G_SD_REBATE_GROUPS" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Curr Period Orders Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Pyr_Period_Orders_Total" source="PYR_PERIOD_ORDERS"
         function="sum" width="20" precision="10" reset="G_SD_REBATE_GROUPS"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Pyr Period Orders Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Curr_Period_Revenue_Total"
         source="CURR_PERIOD_REVENUE" function="sum" width="20" precision="10"
         reset="G_SD_REBATE_GROUPS" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Curr Period Revenue Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Pyr_Period_Revenue_Total"
         source="PYR_PERIOD_REVENUE" function="sum" width="20" precision="10"
         reset="G_SD_REBATE_GROUPS" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Pyr Period Revenue Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Group_Member_Count" source="LONG_ADDRESS_NUMBER"
         function="count" width="20" precision="10" reset="G_SD_REBATE_GROUPS"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Group Member Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Order_Var_Total" source="ORDERS_VAR" function="sum"
         width="20" precision="10" reset="G_SD_REBATE_GROUPS" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Order Var Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Revenue_Var_Total" source="REVENUE_VAR"
         function="sum" width="20" precision="10" reset="G_SD_REBATE_GROUPS"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Revenue Var Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_DETAIL_INFORMATION">
        <displayInfo x="0.76624" y="2.68604" width="2.44836" height="3.67773"
        />
        <dataItem name="SHOP_NAME" datatype="vchar2" columnOrder="21"
         width="25" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Shop Name">
          <dataDescriptor expression="SHOP_NAME"
           descriptiveExpression="SHOP_NAME" order="2" width="25"/>
        </dataItem>
        <dataItem name="MEMBER_STATUS" datatype="vchar2" columnOrder="33"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="MEMBER_STATUS"
           descriptiveExpression="MEMBER_STATUS" order="5" width="30"/>
        </dataItem>
        <dataItem name="SD_RATE" datatype="vchar2" columnOrder="22" width="40"
         defaultWidth="100000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Sd Rate" breakOrder="none">
          <dataDescriptor expression="SD_RATE" descriptiveExpression="SD_RATE"
           order="6" width="40"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="20" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Long Address Number" breakOrder="none">
          <dataDescriptor expression="LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="1" width="20"/>
        </dataItem>
        <dataItem name="FBC_Info" datatype="vchar2" columnOrder="19"
         width="22" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor expression="FBC_INFO"
           descriptiveExpression="FBC_INFO" order="4" width="22"/>
        </dataItem>
        <dataItem name="DIV_Info" datatype="vchar2" columnOrder="18"
         width="22" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor expression="DIV_INFO"
           descriptiveExpression="DIV_INFO" order="3" width="22"/>
        </dataItem>
        <dataItem name="CURR_PERIOD_ORDERS" oracleDatatype="number"
         columnOrder="23" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Curr Period Orders" breakOrder="none">
          <dataDescriptor expression="CURR_PERIOD_ORDERS"
           descriptiveExpression="CURR_PERIOD_ORDERS" order="7"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PYR_PERIOD_ORDERS" oracleDatatype="number"
         columnOrder="25" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Pyr Period Orders" breakOrder="none">
          <dataDescriptor expression="PYR_PERIOD_ORDERS"
           descriptiveExpression="PYR_PERIOD_ORDERS" order="9"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="ORDERS_VAR" oracleDatatype="number" columnOrder="34"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Orders Var" breakOrder="none">
          <dataDescriptor expression="ORDERS_VAR"
           descriptiveExpression="ORDERS_VAR" order="11"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="CURR_PERIOD_REVENUE" oracleDatatype="number"
         columnOrder="24" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Curr Period Revenue" breakOrder="none">
          <dataDescriptor expression="CURR_PERIOD_REVENUE"
           descriptiveExpression="CURR_PERIOD_REVENUE" order="8"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="PYR_PERIOD_REVENUE" oracleDatatype="number"
         columnOrder="26" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Pyr Period Revenue" breakOrder="none">
          <dataDescriptor expression="PYR_PERIOD_REVENUE"
           descriptiveExpression="PYR_PERIOD_REVENUE" order="10"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="REVENUE_VAR" oracleDatatype="number" columnOrder="35"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Revenue Var" breakOrder="none">
          <dataDescriptor expression="REVENUE_VAR"
           descriptiveExpression="REVENUE_VAR" order="12"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NON_SPECIAL_COUNT" oracleDatatype="number"
         columnOrder="28" width="2" defaultWidth="90000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Non Special Count" breakOrder="none">
          <dataDescriptor expression="NON_SPECIAL_COUNT"
           descriptiveExpression="NON_SPECIAL_COUNT" order="19"
           oracleDatatype="number" width="2" precision="38"/>
        </dataItem>
        <dataItem name="NSD_CURR_PERIOD_ORDERS" oracleDatatype="number"
         columnOrder="29" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Nsd Curr Period Orders" breakOrder="none">
          <dataDescriptor expression="NSD_CURR_PERIOD_ORDERS"
           descriptiveExpression="NSD_CURR_PERIOD_ORDERS" order="13"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NSD_PYR_PERIOD_ORDERS" oracleDatatype="number"
         columnOrder="31" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Nsd Pyr Period Orders" breakOrder="none">
          <dataDescriptor expression="NSD_PYR_PERIOD_ORDERS"
           descriptiveExpression="NSD_PYR_PERIOD_ORDERS" order="15"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NSD_ORDERS_VAR" oracleDatatype="number"
         columnOrder="36" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Nsd Orders Var"
         breakOrder="none">
          <dataDescriptor expression="NSD_ORDERS_VAR"
           descriptiveExpression="NSD_ORDERS_VAR" order="17"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NSD_CURR_PERIOD_REV" oracleDatatype="number"
         columnOrder="30" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Nsd Curr Period Rev" breakOrder="none">
          <dataDescriptor expression="NSD_CURR_PERIOD_REV"
           descriptiveExpression="NSD_CURR_PERIOD_REV" order="14"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NSD_PYR_PERIOD_REV" oracleDatatype="number"
         columnOrder="32" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Nsd Pyr Period Rev" breakOrder="none">
          <dataDescriptor expression="NSD_PYR_PERIOD_REV"
           descriptiveExpression="NSD_PYR_PERIOD_REV" order="16"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="NSD_REV_VAR" oracleDatatype="number" columnOrder="37"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Nsd Rev Var" breakOrder="none">
          <dataDescriptor expression="NSD_REV_VAR"
           descriptiveExpression="NSD_REV_VAR" order="18"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <formula name="CF_Title_Dates" source="cf_title_datesformula"
     datatype="character" width="45" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.57288" y="0.36462" width="1.31250" height="0.20825"/>
    </formula>
    <formula name="CF_Title_Terr_Reg" source="cf_title_terr_regformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.59375" y="0.67700" width="1.25000" height="0.19995"/>
    </formula>
    <summary name="CS_Cur_Period_Ords_Grand_Total"
     source="CS_Curr_Period_Orders_Total" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Cur Period Ords Grand Total">
      <displayInfo x="3.60413" y="1.45837" width="1.88550" height="0.19995"/>
    </summary>
    <summary name="CS_Member_Grand_Total_Count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Member Grand Total Count">
      <displayInfo x="3.60449" y="1.13525" width="1.83337" height="0.19995"/>
    </summary>
    <summary name="CS_Cur_Period_Rev_Grand_Total"
     source="CS_Curr_Period_Revenue_Total" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Cur Period Rev Grand Total">
      <displayInfo x="3.61487" y="1.77087" width="1.83337" height="0.19995"/>
    </summary>
    <summary name="CS_Pyr_Period_Ords_Grand_Total"
     source="CS_Pyr_Period_Orders_Total" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Pyr Period Ords Grand Total">
      <displayInfo x="3.62524" y="2.08337" width="1.88538" height="0.20825"/>
    </summary>
    <summary name="CS_Pry_Period_Rev_Grand_Total"
     source="CS_Pyr_Period_Revenue_Total" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Pry Period Rev Grand Total">
      <displayInfo x="3.64587" y="2.39587" width="1.84375" height="0.19995"/>
    </summary>
    <summary name="CS_Order_Var_Grand_Total" source="CS_Order_Var_Total"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Order Var Grand Total">
      <displayInfo x="3.65625" y="2.76038" width="1.83337" height="0.20837"/>
    </summary>
    <summary name="CS_Revenue_Var_Grand_Total" source="CS_Revenue_Var_Total"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Revenue Var Grand Total">
      <displayInfo x="3.66663" y="3.12512" width="1.83337" height="0.19995"/>
    </summary>
    <formula name="CF_No_Data_msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.10413" y="0.69800" width="1.14587" height="0.19995"/>
    </formula>
    <summary name="CS_Record_count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="5.11450" y="0.38538" width="1.11462" height="0.20837"/>
    </summary>
    <summary name="CS_NSD_Cur_Prd_Rev_Grand_Total"
     source="NSD_CURR_PERIOD_REV" function="sum" width="20" precision="10"
     reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Cur Prd Rev Grand Total">
      <displayInfo x="5.60400" y="1.78125" width="2.02087" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Member_Grand_Total_Cnt" source="NON_SPECIAL_COUNT"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Member Grand Total Cnt">
      <displayInfo x="5.60437" y="1.13538" width="1.99976" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Cur_Prd_Ords_GrandTotal"
     source="NSD_CURR_PERIOD_ORDERS" function="sum" width="20" precision="10"
     reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Cur Prd Ords Grandtotal">
      <displayInfo x="5.63538" y="1.44788" width="2.00000" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Order_Var_Grand_Total" source="NSD_ORDERS_VAR"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Order Var Grand Total">
      <displayInfo x="5.65625" y="2.80212" width="1.96875" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Pyr_Prd_Ords_GrandTotal"
     source="NSD_PYR_PERIOD_ORDERS" function="sum" width="20" precision="10"
     reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Pyr Prd Ords Grandtotal">
      <displayInfo x="5.66663" y="2.07288" width="1.95837" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Pyr_Prd_Rev_Grand_Total" source="NSD_PYR_PERIOD_REV"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Pyr Prd Rev Grand Total">
      <displayInfo x="5.66663" y="2.40625" width="1.94800" height="0.19995"/>
    </summary>
    <summary name="CS_NSD_Rev_Var_Grand_Total" source="NSD_REV_VAR"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Nsd Rev Var Grand Total">
      <displayInfo x="5.72913" y="3.12500" width="1.98962" height="0.20837"/>
    </summary>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="14.00000" height="7.31250">
      <location x="0.00000" y="0.68750"/>
      <frame name="M_G_SendingRevenue_Enclosing">
        <geometryInfo x="0.00000" y="0.00000" width="14.00000"
         height="2.12500"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <frame name="M_Report_Headings_Group">
          <geometryInfo x="0.01550" y="0.00000" width="13.98450"
           height="0.62500"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings fillPattern="transparent"/>
          <text name="B_1">
            <textSettings justify="center"/>
            <geometryInfo x="0.12500" y="0.25000" width="0.50000"
             height="0.29163"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Shop
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Name]]>
              </string>
            </textSegment>
          </text>
          <text name="B_2">
            <geometryInfo x="2.00000" y="0.25000" width="0.50000"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Member
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Number]]>
              </string>
            </textSegment>
          </text>
          <text name="B_3">
            <geometryInfo x="3.81250" y="0.25000" width="1.12500"
             height="0.31250"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                  <font face="Courier New" size="12"/>
                <cond name="first" column="P_DIVISION" exception="1"
                 lowValue="blank" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_3formattrigger"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Field Business Consultant]]>
              </string>
            </textSegment>
          </text>
          <text name="B_4">
            <geometryInfo x="5.37500" y="0.25000" width="0.68750"
             height="0.31250"/>
            <generalLayout>
              <conditionalFormat>
                <formatException label="(:P_DIVISION != &apos;All&apos;)">
                  <font face="Courier New" size="12"/>
                <cond name="first" column="P_DIVISION" exception="2"
                 lowValue="All" conjunction="1"/>
                </formatException>
              </conditionalFormat>
            </generalLayout>
            <advancedLayout formatTrigger="b_4formattrigger"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Division Manager]]>
              </string>
            </textSegment>
          </text>
          <text name="B_6">
            <textSettings justify="center"/>
            <geometryInfo x="7.87500" y="0.25000" width="0.62500"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Current
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period
]]>
              </string>
            </textSegment>
          </text>
          <text name="B_7">
            <textSettings justify="center"/>
            <geometryInfo x="8.62500" y="0.25000" width="0.75000"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Prior Year
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period
]]>
              </string>
            </textSegment>
          </text>
          <text name="B_8">
            <textSettings justify="center"/>
            <geometryInfo x="10.62500" y="0.25000" width="0.75000"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Current
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period ]]>
              </string>
            </textSegment>
          </text>
          <text name="B_9">
            <textSettings justify="center"/>
            <geometryInfo x="11.75000" y="0.25000" width="0.87500"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Prior Year
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Period ]]>
              </string>
            </textSegment>
          </text>
          <text name="B_22">
            <geometryInfo x="2.68750" y="0.25000" width="0.56250"
             height="0.31250"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Member
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Status]]>
              </string>
            </textSegment>
          </text>
          <text name="B_10">
            <textSettings justify="center"/>
            <geometryInfo x="7.93750" y="0.00000" width="2.12500"
             height="0.14587"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Sending Counts]]>
              </string>
            </textSegment>
          </text>
          <text name="B_14">
            <textSettings justify="center"/>
            <geometryInfo x="10.43750" y="0.00000" width="3.50000"
             height="0.14587"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Sending Revenue]]>
              </string>
            </textSegment>
          </text>
          <line name="B_24" arrow="none">
            <geometryInfo x="8.68750" y="0.25000" width="1.75000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent"/>
            <points>
              <point x="8.68750" y="0.25000"/>
              <point x="10.43750" y="0.25000"/>
            </points>
          </line>
          <line name="B_25" arrow="none">
            <geometryInfo x="7.93750" y="0.18750" width="2.12500"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="7.93750" y="0.18750"/>
              <point x="10.06250" y="0.18750"/>
            </points>
          </line>
          <line name="B_26" arrow="none">
            <geometryInfo x="10.43750" y="0.18750" width="3.50000"
             height="0.00000"/>
            <visualSettings fillPattern="transparent" linePattern="solid"/>
            <points>
              <point x="10.43750" y="0.18750"/>
              <point x="13.93750" y="0.18750"/>
            </points>
          </line>
          <text name="B_5">
            <textSettings justify="center"/>
            <geometryInfo x="7.12500" y="0.25000" width="0.37500"
             height="0.33337"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[SD Rate]]>
              </string>
            </textSegment>
          </text>
          <text name="B_27">
            <textSettings justify="center"/>
            <geometryInfo x="13.12500" y="0.25000" width="0.75000"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Variance]]>
              </string>
            </textSegment>
          </text>
          <text name="B_28">
            <textSettings justify="center"/>
            <geometryInfo x="9.43750" y="0.25000" width="0.62500"
             height="0.30212"/>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[
]]>
              </string>
            </textSegment>
            <textSegment>
              <font face="Courier New" size="8"/>
              <string>
              <![CDATA[Variance]]>
              </string>
            </textSegment>
          </text>
        </frame>
        <frame name="M_G_Group_Enlosing">
          <geometryInfo x="0.00000" y="0.68750" width="13.68750"
           height="0.93750"/>
          <generalLayout verticalElasticity="variable"/>
          <visualSettings fillPattern="transparent"/>
        </frame>
        <text name="B_20">
          <textSettings justify="center"/>
          <geometryInfo x="5.87500" y="1.75000" width="3.06250"
           height="0.18750"/>
          <visualSettings fillBackgroundColor="r0g88b88"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[&<CF_No_Data_Msg>
]]>
            </string>
          </textSegment>
        </text>
        <repeatingFrame name="R_G_SD_Rebate_Group" source="G_SD_REBATE_GROUPS"
         printDirection="down" minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.68750" width="14.00000"
           height="0.93750"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <field name="F_Group_Code_Heading" source="GROUP_CODE"
           spacing="single" alignment="start">
            <font face="Courier New" size="9" bold="yes" italic="yes"/>
            <geometryInfo x="0.00000" y="0.75000" width="2.50000"
             height="0.18750"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
          </field>
          <repeatingFrame name="R_G_Detail_Information"
           source="G_DETAIL_INFORMATION" printDirection="down"
           minWidowRecords="1" columnMode="no">
            <geometryInfo x="0.00000" y="0.93750" width="14.00000"
             height="0.31250"/>
            <generalLayout pageProtect="yes" verticalElasticity="expand"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Shop_Name" source="SHOP_NAME" spacing="single"
             alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="0.00000" y="1.00000" width="1.87500"
               height="0.18750"/>
            </field>
            <field name="F_Long_Address_Number" source="LONG_ADDRESS_NUMBER"
             spacing="single" alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="1.93750" y="1.00000" width="0.68750"
               height="0.18750"/>
            </field>
            <field name="F_FBC_Info" source="FBC_Info" spacing="single"
             alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="3.81250" y="1.00000" width="1.56250"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                    <font face="Arial" size="7"/>
                    <formatVisualSettings borderPattern="solid"/>
                  <cond name="first" column="P_DIVISION" exception="1"
                   lowValue="blank" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_fbc_infoformattrigger"/>
            </field>
            <field name="F_Div_Info" source="DIV_Info" spacing="single"
             alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="5.37500" y="1.00000" width="1.50000"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:P_DIVISION != &apos;All&apos;)">
                    <font face="Arial" size="7"/>
                    <formatVisualSettings borderPattern="solid"/>
                  <cond name="first" column="P_DIVISION" exception="2"
                   lowValue="All" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_div_infoformattrigger"/>
            </field>
            <field name="F_SD_Rate" source="SD_RATE" spacing="single"
             alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="6.87500" y="1.00000" width="0.81250"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:SD_RATE = &apos;0&apos;)">
                    <font face="Arial" size="7"/>
                  <cond name="first" column="SD_RATE" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_sd_rateformattrigger"/>
            </field>
            <field name="F_Curr_Period_Orders" source="CURR_PERIOD_ORDERS"
             formatMask="NNN,NNN,NN0" spacing="single" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="7.75000" y="1.00000" width="0.56250"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException
                   label="(:CURR_PERIOD_ORDERS = &apos;0&apos;)">
                    <font face="Arial" size="7"/>
                    <formatVisualSettings borderPattern="solid"/>
                  <cond name="first" column="CURR_PERIOD_ORDERS" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_1formattrigger0024"/>
            </field>
            <field name="F_Curr_Pd_Revenue" source="CURR_PERIOD_REVENUE"
             formatMask="NNN,NNN,NN0.00" spacing="single" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="10.12500" y="1.00000" width="1.06250"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException
                   label="(:PYR_PERIOD_ORDERS = &apos;0&apos;)">
                    <font face="Arial" size="7"/>
                    <formatVisualSettings borderPattern="solid"/>
                  <cond name="first" column="PYR_PERIOD_ORDERS" exception="1"
                   lowValue="0" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_curr_pd_revenueformattrigger"/>
            </field>
            <field name="F_Member_Status" source="MEMBER_STATUS"
             alignment="start">
              <font face="Courier New" size="8"/>
              <geometryInfo x="2.75000" y="1.00000" width="1.00000"
               height="0.18750"/>
              <generalLayout verticalElasticity="variable"/>
            </field>
            <field name="F_Pyr_Period_Revenue" source="PYR_PERIOD_REVENUE"
             formatMask="NNN,NNN,NNN.00" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="11.43750" y="1.00000" width="1.12500"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException
                   label="(:PYR_PERIOD_REVENUE = &apos;.00&apos;)">
                    <font face="Arial" size="7"/>
                  <cond name="first" column="PYR_PERIOD_REVENUE" exception="1"
                   lowValue=".00" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout formatTrigger="f_pyr_period_revenueformattrig"/>
            </field>
            <field name="F_Revenue_Var" source="REVENUE_VAR"
             formatMask="NNN,NNN,NNN.00" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="12.75000" y="1.00000" width="1.18750"
               height="0.18750"/>
              <advancedLayout formatTrigger="f_revenue_varformattrigger"/>
            </field>
            <field name="F_Orders_Var" source="ORDERS_VAR"
             formatMask="NNN,NNN" spacing="single" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="9.12500" y="1.00000" width="0.68750"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <field name="F_Pry_Period_Orders" source="PYR_PERIOD_ORDERS"
             formatMask="NNN,NNN" spacing="single" alignment="end">
              <font face="Courier New" size="8"/>
              <geometryInfo x="8.37500" y="1.00000" width="0.68750"
               height="0.18750"/>
              <visualSettings fillPattern="transparent"/>
            </field>
          </repeatingFrame>
          <text name="B_11">
            <geometryInfo x="0.25000" y="1.37500" width="1.00000"
             height="0.18750"/>
            <textSegment>
              <font face="Courier New" size="8" bold="yes"/>
              <string>
              <![CDATA[Group Totals]]>
              </string>
            </textSegment>
          </text>
          <text name="B_13">
            <geometryInfo x="2.75000" y="1.37500" width="1.00000"
             height="0.18750"/>
            <textSegment>
              <font face="Courier New" size="8" bold="yes"/>
              <string>
              <![CDATA[Member Count:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Group_Member_Count" source="CS_Group_Member_Count"
           formatMask="N,NNN,NNN" spacing="single" alignment="start">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="3.81250" y="1.37500" width="0.56250"
             height="0.18750"/>
          </field>
          <line name="B_30" arrow="none">
            <geometryInfo x="7.68750" y="1.31250" width="6.31250"
             height="0.00000"/>
            <visualSettings linePattern="solid"/>
            <points>
              <point x="7.68750" y="1.31250"/>
              <point x="14.00000" y="1.31250"/>
            </points>
          </line>
          <field name="F_Curr_Period_Orders_Total"
           source="CS_Curr_Period_Orders_Total" formatMask="N,NNN,NNN,NNN"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="7.43750" y="1.43750" width="0.75000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Pry_Period_Orders_Total"
           source="CS_Pyr_Period_Orders_Total" formatMask="NNN,NNN,NNN"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="8.25000" y="1.43750" width="0.75000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Order_Var_Total" source="CS_Order_Var_Total"
           formatMask="NNN,NNN,NNN" spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="9.06250" y="1.43750" width="0.75000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Cur_Pd_Revenue_Total"
           source="CS_Curr_Period_Revenue_Total" formatMask="$NNN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="9.87500" y="1.43750" width="1.31250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Pry_Period_Revenue_Total"
           source="CS_Pyr_Period_Revenue_Total" formatMask="$NNN,NNN,NN0.00"
           spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="11.31250" y="1.43750" width="1.25000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Revenue_Var_Total" source="CS_Revenue_Var_Total"
           formatMask="$NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Courier New" size="8" bold="yes"/>
            <geometryInfo x="12.68750" y="1.43750" width="1.25000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
        <line name="B_31" arrow="none">
          <geometryInfo x="0.00000" y="0.56250" width="13.93750"
           height="0.00000"/>
          <advancedLayout printObjectOnPage="allPage"
           basePrintingOn="enclosingObject"/>
          <visualSettings linePattern="solid"/>
          <points>
            <point x="0.00000" y="0.56250"/>
            <point x="13.93750" y="0.56250"/>
          </points>
        </line>
      </frame>
      <frame name="M_Group_Report_Total">
        <geometryInfo x="0.00000" y="2.16699" width="14.00000"
         height="0.77051"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_15">
          <geometryInfo x="0.00000" y="2.16699" width="1.12500"
           height="0.14551"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_15formattrigger"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[Report Totals]]>
            </string>
          </textSegment>
        </text>
        <text name="B_16">
          <geometryInfo x="0.37500" y="2.43750" width="0.87500"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g88b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_16formattrigger"/>
          <visualSettings fillBackgroundColor="r0g88b88"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[All Members]]>
            </string>
          </textSegment>
        </text>
        <text name="B_18">
          <geometryInfo x="0.37500" y="2.62500" width="2.06250"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r0g88b88"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_18formattrigger"/>
          <visualSettings fillBackgroundColor="r0g88b88"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[All Non-Special Deal Members]]>
            </string>
          </textSegment>
        </text>
        <text name="B_19">
          <textSettings justify="center"/>
          <geometryInfo x="2.81250" y="2.62500" width="1.05212"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_19formattrigger"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[Member Count: ]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Group_Member_Grand_Total"
         source="CS_Member_Grand_Total_Count" formatMask="NNN,NNN,NNN"
         alignment="start">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="3.93750" y="2.43750" width="0.75000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NonSD_Member_Count"
         source="CS_NSD_Member_Grand_Total_Cnt" formatMask="NNN,NNN,NNN"
         alignment="start">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="3.93750" y="2.62500" width="0.75000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Cur_Period_Ords_Grand_Total"
         source="CS_Cur_Period_Ords_Grand_Total" formatMask="NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="7.25000" y="2.43750" width="0.81250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Cur_Period_Ords_Tot"
         source="CS_NSD_Cur_Prd_Ords_GrandTotal" formatMask="NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="7.25000" y="2.62500" width="0.81250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Pry_Pd_Ords_Grand_Total"
         source="CS_Pyr_Period_Ords_Grand_Total" formatMask="NN,NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.12500" y="2.43750" width="0.75000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Cur_Pd_Rev_Grand_Total"
         source="CS_Cur_Period_Rev_Grand_Total" formatMask="$NNN,NNN,NNN.00"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="9.68750" y="2.43750" width="1.37500"
           height="0.12500"/>
        </field>
        <field name="F_Pry_Period_Rev_Grand_Total"
         source="CS_Pry_Period_Rev_Grand_Total" formatMask="$NNN,NNN,NN0.00"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="11.25000" y="2.43750" width="1.31250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Pry_Pd_Ord_Grand_Total"
         source="CS_NSD_Pyr_Prd_Ords_GrandTotal" formatMask="NN,NNN,NNN,NNN"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.12500" y="2.62500" width="0.75000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Cur_Pd_Rev_Grand_Total"
         source="CS_NSD_Cur_Prd_Rev_Grand_Total" formatMask="$NNN,NNN,NNN.00"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="9.68750" y="2.62500" width="1.37500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Pyr_Prd_Rev_Grand_Total"
         source="CS_NSD_Pyr_Prd_Rev_Grand_Total" formatMask="$NNN,NNN,NNN.00"
         spacing="single" alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="11.25000" y="2.62500" width="1.31250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_17">
          <textSettings justify="center"/>
          <geometryInfo x="2.81250" y="2.43750" width="1.05212"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_17formattrigger"/>
          <textSegment>
            <font face="Courier New" size="8" bold="yes"/>
            <string>
            <![CDATA[Member Count: ]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Order_Var_Grand_Total"
         source="CS_Order_Var_Grand_Total" formatMask="NNN,NNN,NNN"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.93750" y="2.43750" width="0.62500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Revenue_Var_Grand_Total"
         source="CS_Revenue_Var_Grand_Total" formatMask="$NNN,NNN,NNN.00"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="12.75000" y="2.43750" width="1.18750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Order_Var_Grand_Total"
         source="CS_NSD_Order_Var_Grand_Total" formatMask="NNN,NNN,NNN"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="8.93750" y="2.62500" width="0.62500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_NSD_Revenue_Var_Grand_Total"
         source="CS_NSD_Rev_Var_Grand_Total" formatMask="$NNN,NNN,NNN.00"
         alignment="end">
          <font face="Courier New" size="8" bold="yes"/>
          <geometryInfo x="12.75000" y="2.62500" width="1.18750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
      </frame>
      <text name="B_21">
        <textSettings justify="center"/>
        <geometryInfo x="6.25000" y="3.06250" width="1.00000" height="0.20837"
        />
        <textSegment>
          <font face="Courier New" size="8" bold="yes"/>
          <string>
          <![CDATA[End of Report]]>
          </string>
        </textSegment>
      </text>
      <text name="B_23">
        <textSettings justify="center"/>
        <geometryInfo x="6.25000" y="3.31250" width="1.00000" height="0.16663"
        />
        <textSegment>
          <font face="Courier New" size="8"/>
          <string>
          <![CDATA[V1.7]]>
          </string>
        </textSegment>
      </text>
    </body>
    <margin>
      <text name="B_12">
        <textSettings justify="center"/>
        <geometryInfo x="5.31250" y="0.06250" width="3.00000" height="0.56250"
        />
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[Sending Revenue
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Dates>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Courier New" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Terr_Reg>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Courier New" size="9" bold="yes"/>
        <geometryInfo x="0.25000" y="0.06250" width="0.83337" height="0.16663"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_title_datesformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_DatesFormula return Char is
lv_title_dates varchar2(45);

begin
  lv_title_dates :=('From: '||:P_MONTH1||'/'||:P_YEAR1||' '||'To: '||:P_MONTH2||'/'||:P_YEAR2);
 
  return (lv_title_dates);
end; 
]]>
      </textSource>
    </function>
    <function name="cf_title_terr_regformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_Terr_RegFormula return Char is
  lv_title3 varchar2 (80) null;
  temp_title varchar2(80);
begin
 temp_title := title_name (:p_member, :p_territory, :p_division); 
 If :p_territory != 'blank' and :p_territory != 'All' then
 	 lv_title3 := 'Territory # '||:p_territory ||' - '|| temp_title;
 elsif :p_division != 'blank' and :p_division != 'All' then
 	lv_title3 := 'Region # '||:p_division|| ' - ' ||temp_title;
 else
 	 lv_title3 := 'All Members';
 end if;
 return lv_title3;
end;
  	]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_msgFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR THE PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	 
end;]]>
      </textSource>
    </function>
    <function name="b_15formattrigger">
      <textSource>
      <![CDATA[function B_15FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_16formattrigger">
      <textSource>
      <![CDATA[function B_16FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_18formattrigger">
      <textSource>
      <![CDATA[function B_18FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_17formattrigger">
      <textSource>
      <![CDATA[function B_17FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_19formattrigger">
      <textSource>
      <![CDATA[function B_19FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_infoformattrigger">
      <textSource>
      <![CDATA[function F_FBC_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
   if :P_TERRITORY != 'All' and :P_DIVISION = 'blank'
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
   if :P_TERRITORY != 'All' and :P_DIVISION = 'blank'
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_div_infoformattrigger">
      <textSource>
      <![CDATA[function F_Div_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_1formattrigger0024">
      <textSource>
      <![CDATA[function F_1FORMATTRIGGER0024 return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CURR_PERIOD_ORDERS = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_sd_rateformattrigger">
      <textSource>
      <![CDATA[function F_SD_RateFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:SD_RATE = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_pyr_period_revenueformattrig">
      <textSource>
      <![CDATA[function F_Pyr_Period_RevenueFormatTrig return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:PYR_PERIOD_REVENUE = '.00')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_curr_pd_revenueformattrigger">
      <textSource>
      <![CDATA[function F_Curr_Pd_RevenueFormatTrigger return boolean is
begin
   if (:Curr_Period_Revenue = '0')
  then
    return (FALSE);
  end if;
  
  return (True);
end;  
 ]]>
      </textSource>
    </function>
    <function name="f_revenue_varformattrigger">
      <textSource>
      <![CDATA[function F_Revenue_VarFormatTrigger return boolean is
begin
   if (:Revenue_Var = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
