<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="MonthlyRevenueReport" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="MONTHLYREVENUEREPORT" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <userParameter name="P_TERRITORY" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_DIVISION" datatype="character" width="40"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR1" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MONTH2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_YEAR2" datatype="character" width="40"
     defaultWidth="0" defaultHeight="0"/>
    <userParameter name="P_MEMBER" datatype="character" precision="10"
     initialValue="blank" defaultWidth="0" defaultHeight="0"/>
    <dataSource name="Q_Monthly_Revenue_Report">
      <select>
      <![CDATA[SELECT ALL 
MR.CURRENCY_CODE, 
AB.LONG_ADDRESS_NUMBER,
substr(WW.MAILING_NAME,1,30) as shop_name, 
AB.DIVISION_MANAGER_CODE || '  ' || RI.MANAGER_NAME as DIV_Info, 
AB.FIELD_ACCT_REPRESENTATIVE_CODE || '  ' || TI.FBC_NAME as FBC_Info,
AB.MEMBER_STATUS,
to_char(CI.MEMBER_DATE,'MM/DD/YYYY') as member_date, 
to_char(CI.NONMEMBER_DATE,'MM/DD/YYYY') as nonmember_date, 
SUM(MR.CURR_MTH_MEMBER_FEE_US) AS MONTHLY_FEES,
SUM(MR.CURR_MTH_MF_ONETIME_CREDITS) AS MANUAL_CREDIT, 
SUM(MR.CURR_MTH_QUALITY_ASSURANCE) AS QA_FEES, 
SUM(MR.CURR_MTH_DELUXE_SEL_GUIDES) AS FSG, 
SUM(MR.CURR_MTH_MEMBER_FEE_US + MR.CURR_MTH_QUALITY_ASSURANCE + MR.CURR_MTH_MF_ONETIME_CREDITS +          MR.CURR_MTH_DELUXE_SEL_GUIDES) AS TOTAL_CURR_PERIOD,
SUM(MR.PYR_MTH_MEMBER_FEE_US + MR.PYR_MTH_QUALITY_ASSURANCE + MR.PYR_MTH_MF_ONETIME_CREDITS + 
         MR.PYR_MTH_DELUXE_SEL_GUIDES) AS TOTAL_PYR_PERIOD,
DECODE(mr.curr_mth_ppo_orders_count , 0,
decode (mr.currency_code, 'USD',
    decode (ab.wire_order_country_group, 'INT', '$149 Members',
       decode (sign((mr.curr_mth_member_fee_us/199.00) - 1), 1, '$219 & Up Members',
       decode (sign((mr.curr_mth_member_fee_us/71.01) - 1), 1, '$149 Members',
       decode (sign((mr.curr_mth_member_fee_us/26.00) - 1), 1, '$69 - $79 Members',
       decode (sign((mr.curr_mth_member_fee_us/.01) - 1), 1, '$1 - $25 Members',
       decode (sign(mr.curr_mth_member_fee_us), -1,'$1 - $25 Members',
       decode (mr.curr_mth_member_fee_us, 0, '$0 Members', '$0 Members')
 ) ) ) ) ) ),
       decode (sign((mr.curr_mth_member_fee/69.01 ) - 1), 1, '$89 & Up Members',
       decode (sign((mr.curr_mth_member_fee/41.00 ) - 1), 1, '$69 Members',
       decode (sign((mr.curr_mth_member_fee/.01 ) - 1), 1, '$60 Members',
       decode (mr.curr_mth_member_fee, 0, '$0 Members', '$0 Members')
   ) ) ) )  ,  ' PPO Members' ) range_code
FROM
   JDE_ADDRESS_BOOK AB, 
   JDE_CUSTOMER_INFORMATION CI,
   JDE_MEMBER_REVENUE_SUMMARY MR, 
   JDE_WHOS_WHO WW,
   TERRITORY_INFO TI,
   REGION_INFO RI
WHERE ((:P_TERRITORY = 'All'  or :P_DIVISION = 'All' )
        OR (:P_TERRITORY = 'blank' or :P_TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE)
      AND (:P_DIVISION = 'blank' or :P_DIVISION = AB.DIVISION_MANAGER_CODE))
      AND MR.EFFECTIVE_MONTH_DATE >= TO_DATE(:P_MONTH1||'/01/'||:P_YEAR1,'MM/DD/YYYY') 
      AND MR.EFFECTIVE_MONTH_DATE <= TO_DATE(:P_MONTH2||'/01/'||:P_YEAR2,'MM/DD/YYYY')
      AND MR.ADDRESS_NUMBER = AB.ADDRESS_NUMBER
      AND AB.ADDRESS_NUMBER = CI.ADDRESS_NUMBER 
      AND TI.TERRITORY = AB.FIELD_ACCT_REPRESENTATIVE_CODE
      AND RI.REGION =  AB.DIVISION_MANAGER_CODE
      AND AB.ADDRESS_NUMBER = WW.ADDRESS_NUMBER
      AND WW.LINE_ID = 0
group by
MR.CURRENCY_CODE, 
substr(WW.MAILING_NAME,1,30) , 
AB.LONG_ADDRESS_NUMBER,
AB.DIVISION_MANAGER_CODE || '  ' || RI.MANAGER_NAME, 
AB.FIELD_ACCT_REPRESENTATIVE_CODE || '  ' || TI.FBC_NAME,
AB.MEMBER_STATUS,
to_char(CI.MEMBER_DATE,'MM/DD/YYYY') , 
to_char(CI.NONMEMBER_DATE,'MM/DD/YYYY'), 
DECODE(mr.curr_mth_ppo_orders_count , 0,
decode (mr.currency_code, 'USD',
    decode (ab.wire_order_country_group, 'INT', '$149 Members',
       decode (sign((mr.curr_mth_member_fee_us/199.00) - 1), 1, '$219 & Up Members',
       decode (sign((mr.curr_mth_member_fee_us/71.01) - 1), 1, '$149 Members',
       decode (sign((mr.curr_mth_member_fee_us/26.00) - 1), 1, '$69 - $79 Members',
       decode (sign((mr.curr_mth_member_fee_us/.01) - 1), 1, '$1 - $25 Members',
       decode (sign(mr.curr_mth_member_fee_us), -1,'$1 - $25 Members',
       decode (mr.curr_mth_member_fee_us, 0, '$0 Members', '$0 Members')
 ) ) ) ) ) ),
       decode (sign((mr.curr_mth_member_fee/69.01 ) - 1), 1, '$89 & Up Members',
       decode (sign((mr.curr_mth_member_fee/41.00 ) - 1), 1, '$69 Members',
       decode (sign((mr.curr_mth_member_fee/.01 ) - 1), 1, '$60 Members',
       decode (mr.curr_mth_member_fee, 0, '$0 Members', '$0 Members')
   ) ) ) ) ,  ' PPO Members' )




]]>
      </select>
      <displayInfo x="1.07935" y="0.02405" width="1.69373" height="0.19995"/>
      <group name="G_CURRENCY_CODE_GROUP">
        <displayInfo x="0.73120" y="0.57922" width="2.38477" height="2.13965"
        />
        <dataItem name="CURRENCY_CODE" datatype="vchar2" columnOrder="24"
         width="3" defaultWidth="30000" defaultHeight="10000" columnFlags="3"
         defaultLabel="Currency Code" breakOrder="descending">
          <dataDescriptor expression="MR.CURRENCY_CODE"
           descriptiveExpression="CURRENCY_CODE" order="1" width="3"/>
        </dataItem>
        <summary name="CS_Curr_Code_Grp_Member_count"
         source="CS_Fee_Grp_Member_Count" function="sum" width="20"
         precision="10" reset="G_CURRENCY_CODE_GROUP" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Curr Code Grp Member Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Curr_Code_Grp_NonMember_Cnt"
         source="CS_Fee_Grp_NonMember_Count" function="sum" width="20"
         precision="10" reset="G_CURRENCY_CODE_GROUP" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Curr Code Grp Nonmember Cnt">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_Monthly_Fee_Tot" source="MONTHLY_FEES"
         function="sum" width="20" precision="10"
         reset="G_CURRENCY_CODE_GROUP" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cur Cd Grp Monthly Fee Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_FSG_Tot" source="FSG" function="sum"
         width="20" precision="10" reset="G_CURRENCY_CODE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Cur Cd Grp Fsg Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_QA_Fee_Tot" source="QA_FEES"
         function="sum" width="20" precision="10"
         reset="G_CURRENCY_CODE_GROUP" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cur Cd Grp Qa Fee Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_Manual_Cred_Tot" source="MANUAL_CREDIT"
         function="sum" width="20" precision="10"
         reset="G_CURRENCY_CODE_GROUP" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cur Cd Grp Manual Cred Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_Tot_Curr_Prd_Tot"
         source="TOTAL_CURR_PERIOD" function="sum" width="20" precision="10"
         reset="G_CURRENCY_CODE_GROUP" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cur Cd Grp Tot Curr Prd Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Cur_Cd_Grp_Total_Pyr_Pd_Tot"
         source="TOTAL_PYR_PERIOD" function="sum" width="20" precision="10"
         reset="G_CURRENCY_CODE_GROUP" compute="report" defaultWidth="120000"
         defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Cur Cd Grp Total Pyr Pd Tot">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <formula name="CF_Group_Total_Label"
         source="cf_group_total_labelformula" datatype="character" width="50"
         precision="10" defaultWidth="0" defaultHeight="0" columnFlags="16"
         breakOrder="none">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </formula>
        <formula name="CF_CC_Grp_Title" source="cf_cc_grp_titleformula"
         datatype="character" width="20" precision="10" defaultWidth="0"
         defaultHeight="0" columnFlags="16" breakOrder="none">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </formula>
      </group>
      <group name="G_MEMBER_FEE_GROUP">
        <displayInfo x="0.76563" y="2.95764" width="2.31787" height="1.96875"
        />
        <dataItem name="range_code" datatype="vchar2" columnOrder="25"
         width="17" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Range Code" breakOrder="none">
          <dataDescriptor
           expression="DECODE ( mr.curr_mth_ppo_orders_count , 0 , decode ( mr.currency_code , &apos;USD&apos; , decode ( ab.wire_order_country_group , &apos;INT&apos; , &apos;$149 Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee_us / 199.00 ) - 1 ) , 1 , &apos;$219 &amp; Up Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee_us / 71.01 ) - 1 ) , 1 , &apos;$149 Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee_us / 26.00 ) - 1 ) , 1 , &apos;$69 - $79 Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee_us / .01 ) - 1 ) , 1 , &apos;$1 - $25 Members&apos; , decode ( sign ( mr.curr_mth_member_fee_us ) , - 1 , &apos;$1 - $25 Members&apos; , decode ( mr.curr_mth_member_fee_us , 0 , &apos;$0 Members&apos; , &apos;$0 Members&apos; ) ) ) ) ) ) ) , decode ( sign ( ( mr.curr_mth_member_fee / 69.01 ) - 1 ) , 1 , &apos;$89 &amp; Up Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee / 41.00 ) - 1 ) , 1 , &apos;$69 Members&apos; , decode ( sign ( ( mr.curr_mth_member_fee / .01 ) - 1 ) , 1 , &apos;$60 Members&apos; , decode ( mr.curr_mth_member_fee , 0 , &apos;$0 Members&apos; , &apos;$0 Members&apos; ) ) ) ) ) , &apos; PPO Members&apos; )"
           descriptiveExpression="RANGE_CODE" order="15" width="17"/>
        </dataItem>
        <formula name="CF_Range_Code_Sort" source="cf_1formula"
         datatype="number" width="20" precision="10" defaultWidth="0"
         defaultHeight="0" columnFlags="17" breakOrder="ascending">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </formula>
        <summary name="CS_Fee_Grp_Member_Count" source="LONG_ADDRESS_NUMBER"
         function="count" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Member Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_NonMember_Count" source="NONMEMBER_DATE"
         function="count" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Nonmember Count">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_Monthly_Fee_Total" source="MONTHLY_FEES"
         function="sum" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Monthly Fee Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_Manual_Credit_Total" source="MANUAL_CREDIT"
         function="sum" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Manual Credit Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_FSG_Total" source="FSG" function="sum"
         width="20" precision="10" reset="G_MEMBER_FEE_GROUP" compute="report"
         defaultWidth="120000" defaultHeight="10000" columnFlags="40"
         defaultLabel="Cs Fee Grp Fsg Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_QA_Fee_Total" source="QA_FEES"
         function="sum" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Qa Fee Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_Cur_Period_Total" source="TOTAL_CURR_PERIOD"
         function="sum" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Cur Period Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
        <summary name="CS_Fee_Grp_Pyr_Period_Total" source="TOTAL_PYR_PERIOD"
         function="sum" width="20" precision="10" reset="G_MEMBER_FEE_GROUP"
         compute="report" defaultWidth="120000" defaultHeight="10000"
         columnFlags="40" defaultLabel="Cs Fee Grp Pyr Period Total">
          <displayInfo x="0.00000" y="0.00000" width="0.00000"
           height="0.00000"/>
        </summary>
      </group>
      <group name="G_DETAIL_INFORMATION">
        <displayInfo x="0.69336" y="5.20691" width="2.44836" height="2.48145"
        />
        <dataItem name="shop_name" datatype="vchar2" columnOrder="18"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="1" defaultLabel="Shop Name">
          <xmlSettings xmlTag="MAILING_NAME"/>
          <dataDescriptor expression="substr ( WW.MAILING_NAME , 1 , 30 )"
           descriptiveExpression="SHOP_NAME" order="3" width="30"/>
        </dataItem>
        <dataItem name="MEMBER_STATUS" datatype="vchar2" columnOrder="31"
         width="30" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Member Status" breakOrder="none">
          <dataDescriptor expression="AB.MEMBER_STATUS"
           descriptiveExpression="MEMBER_STATUS" order="6" width="30"/>
        </dataItem>
        <dataItem name="LONG_ADDRESS_NUMBER" datatype="vchar2"
         columnOrder="23" width="20" defaultWidth="100000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Long Address Number" breakOrder="none">
          <dataDescriptor expression="AB.LONG_ADDRESS_NUMBER"
           descriptiveExpression="LONG_ADDRESS_NUMBER" order="2" width="20"/>
        </dataItem>
        <dataItem name="FBC_Info" datatype="vchar2" columnOrder="22"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Fbc Info" breakOrder="none">
          <dataDescriptor
           expression="AB.FIELD_ACCT_REPRESENTATIVE_CODE || &apos;  &apos; || TI.FBC_NAME"
           descriptiveExpression="FBC_INFO" order="5" width="35"/>
        </dataItem>
        <dataItem name="DIV_Info" datatype="vchar2" columnOrder="21"
         width="35" defaultWidth="100000" defaultHeight="10000"
         columnFlags="0" defaultLabel="Div Info" breakOrder="none">
          <dataDescriptor
           expression="AB.DIVISION_MANAGER_CODE || &apos;  &apos; || RI.MANAGER_NAME"
           descriptiveExpression="DIV_INFO" order="4" width="35"/>
        </dataItem>
        <dataItem name="MEMBER_DATE" datatype="vchar2" columnOrder="19"
         defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Member Date" breakOrder="none">
          <dataDescriptor
           expression="to_char ( CI.MEMBER_DATE , &apos;MM/DD/YYYY&apos; )"
           descriptiveExpression="MEMBER_DATE" order="7" width="10"/>
        </dataItem>
        <dataItem name="NONMEMBER_DATE" datatype="vchar2" columnOrder="20"
         defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Nonmember Date" breakOrder="none">
          <dataDescriptor
           expression="to_char ( CI.NONMEMBER_DATE , &apos;MM/DD/YYYY&apos; )"
           descriptiveExpression="NONMEMBER_DATE" order="8" width="10"/>
        </dataItem>
        <dataItem name="MANUAL_CREDIT" oracleDatatype="number"
         columnOrder="27" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Manual Credit"
         breakOrder="none">
          <xmlSettings xmlTag="TOTAL_CREDIT"/>
          <dataDescriptor expression="SUM ( MR.CURR_MTH_MF_ONETIME_CREDITS )"
           descriptiveExpression="MANUAL_CREDIT" order="10"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="MONTHLY_FEES" oracleDatatype="number" columnOrder="26"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Monthly Fees" breakOrder="none">
          <xmlSettings xmlTag="TOTAL_MONTHLY_FEES"/>
          <dataDescriptor expression="SUM ( MR.CURR_MTH_MEMBER_FEE_US )"
           descriptiveExpression="MONTHLY_FEES" order="9"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="QA_FEES" oracleDatatype="number" columnOrder="28"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Qa Fees" breakOrder="none">
          <xmlSettings xmlTag="TOTAL_QA_FEES"/>
          <dataDescriptor expression="SUM ( MR.CURR_MTH_QUALITY_ASSURANCE )"
           descriptiveExpression="QA_FEES" order="11" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="FSG" oracleDatatype="number" columnOrder="32"
         width="22" defaultWidth="90000" defaultHeight="10000" columnFlags="0"
         defaultLabel="Fsg" breakOrder="none">
          <dataDescriptor expression="SUM ( MR.CURR_MTH_DELUXE_SEL_GUIDES )"
           descriptiveExpression="FSG" order="12" oracleDatatype="number"
           width="22" precision="38"/>
        </dataItem>
        <dataItem name="TOTAL_CURR_PERIOD" oracleDatatype="number"
         columnOrder="30" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0"
         defaultLabel="Total Curr Period" breakOrder="none">
          <dataDescriptor
           expression="SUM ( MR.CURR_MTH_MEMBER_FEE_US + MR.CURR_MTH_QUALITY_ASSURANCE + MR.CURR_MTH_MF_ONETIME_CREDITS + MR.CURR_MTH_DELUXE_SEL_GUIDES )"
           descriptiveExpression="TOTAL_CURR_PERIOD" order="13"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
        <dataItem name="TOTAL_PYR_PERIOD" oracleDatatype="number"
         columnOrder="29" width="22" defaultWidth="90000"
         defaultHeight="10000" columnFlags="0" defaultLabel="Total Pyr Period"
         breakOrder="none">
          <dataDescriptor
           expression="SUM ( MR.PYR_MTH_MEMBER_FEE_US + MR.PYR_MTH_QUALITY_ASSURANCE + MR.PYR_MTH_MF_ONETIME_CREDITS + MR.PYR_MTH_DELUXE_SEL_GUIDES )"
           descriptiveExpression="TOTAL_PYR_PERIOD" order="14"
           oracleDatatype="number" width="22" precision="38"/>
        </dataItem>
      </group>
    </dataSource>
    <summary name="CS_Report_Monthly_Fee_Total"
     source="CS_Cur_Cd_Grp_Monthly_Fee_Tot" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Monthly Fee Total">
      <displayInfo x="3.84375" y="1.43750" width="1.71875" height="0.20837"/>
    </summary>
    <summary name="CS_Report_FSG_Total" source="CS_Cur_Cd_Grp_FSG_Tot"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Fsg Total">
      <displayInfo x="3.84375" y="2.02087" width="1.68750" height="0.19995"/>
    </summary>
    <summary name="CS_Report_Manual_Credit_Total"
     source="CS_Cur_Cd_Grp_Manual_Cred_Tot" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Manual Credit Total">
      <displayInfo x="3.84375" y="2.37500" width="1.86462" height="0.21875"/>
    </summary>
    <summary name="CS_Report_QA_Fee_Total" source="CS_Cur_Cd_Grp_QA_Fee_Tot"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Qa Fee Total">
      <displayInfo x="3.85425" y="1.72913" width="1.64587" height="0.19995"/>
    </summary>
    <formula name="CF_Title_Terr_Reg" source="cf_title_terr_regformula"
     datatype="character" width="100" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.86462" y="0.71875" width="1.25000" height="0.21875"/>
    </formula>
    <formula name="CF_Title_Dates" source="cf_title_datesformula"
     datatype="character" width="45" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="3.87500" y="0.39587" width="1.31250" height="0.20825"/>
    </formula>
    <summary name="CS_Report_Member_Count" source="CS_Fee_Grp_Member_Count"
     function="sum" width="20" precision="10" reset="report" compute="report"
     defaultWidth="120000" defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Member Count">
      <displayInfo x="3.87500" y="1.13538" width="1.54163" height="0.19995"/>
    </summary>
    <formula name="CF_No_Data_msg" source="cf_no_data_msgformula"
     datatype="character" width="80" precision="10" defaultWidth="0"
     defaultHeight="0" columnFlags="16" breakOrder="none">
      <displayInfo x="5.36462" y="0.68738" width="1.16663" height="0.21875"/>
    </formula>
    <summary name="CS_Record_count" source="LONG_ADDRESS_NUMBER"
     function="count" width="20" precision="10" reset="report"
     compute="report" defaultWidth="120000" defaultHeight="10000"
     columnFlags="40" defaultLabel="Cs Record Count">
      <displayInfo x="5.37488" y="0.40625" width="1.11462" height="0.19995"/>
    </summary>
    <summary name="CS_Report_NonMember_Count"
     source="CS_Fee_Grp_NonMember_Count" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Nonmember Count">
      <displayInfo x="5.54150" y="1.11462" width="1.78125" height="0.19995"/>
    </summary>
    <summary name="CS_Report_ Pyr_Period_Total"
     source="CS_Cur_Cd_Grp_Total_Pyr_Pd_Tot" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Pyr Period Total">
      <displayInfo x="5.73962" y="1.78125" width="1.62500" height="0.19995"/>
    </summary>
    <summary name="CS_Report_Curr_Period_Total"
     source="CS_Cur_Cd_Grp_Tot_Curr_Prd_Tot" function="sum" width="20"
     precision="10" reset="report" compute="report" defaultWidth="120000"
     defaultHeight="10000" columnFlags="40"
     defaultLabel="Cs Report Curr Period Total">
      <displayInfo x="5.81250" y="1.46875" width="1.67712" height="0.19995"/>
    </summary>
  </data>
  <layout>
  <section name="main" width="14.00000" height="8.50000">
    <body width="13.75000" height="6.87500">
      <location x="0.00000" y="0.75000"/>
      <frame name="M_G_MONTHLY_FEE_RPT_ENCLOSING">
        <geometryInfo x="0.00000" y="0.00000" width="13.75000"
         height="2.25000"/>
        <generalLayout verticalElasticity="variable"/>
        <visualSettings fillPattern="transparent"/>
        <repeatingFrame name="R_Currency_Code_Group_Repeat"
         source="G_CURRENCY_CODE_GROUP" printDirection="down"
         minWidowRecords="1" columnMode="no">
          <geometryInfo x="0.00000" y="0.00000" width="13.75000"
           height="2.18750"/>
          <generalLayout verticalElasticity="expand"/>
          <visualSettings fillPattern="transparent"/>
          <frame name="M_Headings_Group">
            <geometryInfo x="0.00000" y="0.25000" width="13.75000"
             height="0.37500"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
            <text name="B_1">
              <geometryInfo x="0.06250" y="0.43750" width="0.56250"
               height="0.18750"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Shop Name]]>
                </string>
              </textSegment>
            </text>
            <text name="B_2">
              <textSettings justify="center"/>
              <geometryInfo x="2.00000" y="0.31250" width="0.50000"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Member
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Number]]>
                </string>
              </textSegment>
            </text>
            <text name="B_3">
              <geometryInfo x="3.37500" y="0.43750" width="1.25000"
               height="0.18750"/>
              <generalLayout>
                <conditionalFormat>
                  <formatException label="(:P_DIVISION = &apos;blank&apos;)">
                    <font face="Courier New" size="12"/>
                    <formatVisualSettings fillForegroundColor="r50g88b0"/>
                  <cond name="first" column="P_DIVISION" exception="1"
                   lowValue="blank" conjunction="1"/>
                  </formatException>
                </conditionalFormat>
              </generalLayout>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"
               formatTrigger="b_3formattrigger"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Field Business Consultant]]>
                </string>
              </textSegment>
            </text>
            <text name="B_4">
              <geometryInfo x="5.12500" y="0.43750" width="0.87500"
               height="0.18750"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"
               formatTrigger="b_4formattrigger"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Division Manager]]>
                </string>
              </textSegment>
            </text>
            <text name="B_5">
              <textSettings justify="center"/>
              <geometryInfo x="7.00000" y="0.31250" width="0.43750"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Member Date]]>
                </string>
              </textSegment>
            </text>
            <text name="B_6">
              <textSettings justify="center"/>
              <geometryInfo x="7.56250" y="0.31250" width="0.75000"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Non Member Date]]>
                </string>
              </textSegment>
            </text>
            <text name="B_7">
              <textSettings justify="center"/>
              <geometryInfo x="8.75000" y="0.31250" width="0.50000"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Monthly
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[ Fees]]>
                </string>
              </textSegment>
            </text>
            <text name="B_8">
              <textSettings justify="center"/>
              <geometryInfo x="9.68750" y="0.31250" width="0.37500"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Q/A
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Fees]]>
                </string>
              </textSegment>
            </text>
            <text name="B_9">
              <textSettings justify="center"/>
              <geometryInfo x="11.37500" y="0.31250" width="0.43750"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Manual
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Credit]]>
                </string>
              </textSegment>
            </text>
            <text name="B_10">
              <textSettings justify="center"/>
              <geometryInfo x="12.12500" y="0.31250" width="0.68750"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Total Current
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Period]]>
                </string>
              </textSegment>
            </text>
            <text name="B_11">
              <textSettings justify="center"/>
              <geometryInfo x="13.12500" y="0.31250" width="0.62500"
               height="0.31250"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Total Prior
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Year Period]]>
                </string>
              </textSegment>
            </text>
            <line name="B_14" arrow="none">
              <geometryInfo x="0.00000" y="0.56250" width="13.68750"
               height="0.00000"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings lineWidth="1" fillPattern="transparent"
               linePattern="solid"/>
              <points>
                <point x="0.00000" y="0.56250"/>
                <point x="13.68750" y="0.56250"/>
              </points>
            </line>
            <text name="B_22">
              <textSettings justify="center"/>
              <geometryInfo x="2.62500" y="0.31250" width="0.43750"
               height="0.31250"/>
              <visualSettings fillBackgroundColor="r50g88b0"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Member
]]>
                </string>
              </textSegment>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Status]]>
                </string>
              </textSegment>
            </text>
            <text name="B_26">
              <textSettings justify="center"/>
              <geometryInfo x="10.37500" y="0.31250" width="0.75000"
               height="0.31250"/>
              <textSegment>
                <font face="Arial" size="7"/>
                <string>
                <![CDATA[Floral Selection Guide]]>
                </string>
              </textSegment>
            </text>
          </frame>
          <repeatingFrame name="R_G_Fee_Groups" source="G_MEMBER_FEE_GROUP"
           printDirection="down" minWidowRecords="1" columnMode="no">
            <geometryInfo x="0.00000" y="0.62500" width="13.75000"
             height="0.87500"/>
            <generalLayout verticalElasticity="expand"/>
            <visualSettings fillPattern="transparent"/>
            <field name="F_Member_Fee_Group" source="range_code"
             spacing="single" alignment="start">
              <font face="Arial" size="8" bold="yes" italic="yes"/>
              <geometryInfo x="0.00000" y="0.68750" width="1.25000"
               height="0.12500"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillPattern="transparent"/>
            </field>
            <repeatingFrame name="R_G_Detail_Information"
             source="G_DETAIL_INFORMATION" printDirection="down"
             minWidowRecords="1" columnMode="no">
              <geometryInfo x="0.00000" y="0.87500" width="13.75000"
               height="0.25000"/>
              <generalLayout verticalElasticity="expand"/>
              <visualSettings fillPattern="transparent"/>
              <field name="F_Shop_Name" source="shop_name" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="0.00000" y="0.93750" width="2.00000"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Long_Address_Number" source="LONG_ADDRESS_NUMBER"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="2.00000" y="0.93750" width="0.56250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_FBC_Info" source="FBC_Info" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="3.37500" y="0.93750" width="1.68750"
                 height="0.12500"/>
                <generalLayout>
                  <conditionalFormat>
                    <formatException label="(:P_DIVISION = &apos;blank&apos;)"
                      >
                      <font face="Arial" size="7"/>
                      <formatVisualSettings fillPattern="transparent"
                       fillForegroundColor="r0g75b0" borderPattern="solid"/>
                    <cond name="first" column="P_DIVISION" exception="1"
                     lowValue="blank" conjunction="1"/>
                    </formatException>
                  </conditionalFormat>
                </generalLayout>
                <advancedLayout formatTrigger="f_fbc_infoformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_DIV_Info" source="DIV_Info" alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="5.12500" y="0.93750" width="1.75000"
                 height="0.12500"/>
                <generalLayout>
                  <conditionalFormat>
                    <formatException label="(:P_DIVISION != &apos;All&apos;)">
                      <font face="Arial" size="7"/>
                      <formatVisualSettings fillPattern="transparent"/>
                    <cond name="first" column="P_DIVISION" exception="2"
                     lowValue="All" conjunction="1"/>
                    </formatException>
                  </conditionalFormat>
                </generalLayout>
                <advancedLayout formatTrigger="f_div_infoformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Member_Date" source="MEMBER_DATE"
               alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="6.93750" y="0.93750" width="0.62500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_NonMember_Date" source="NONMEMBER_DATE"
               alignment="center">
                <font face="Arial" size="7"/>
                <geometryInfo x="7.62500" y="0.93750" width="0.62500"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_QA_Fees" source="QA_FEES" formatMask="NNN,NNN.00"
               alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="9.25000" y="0.93750" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_qa_feesformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Monthly_Fees" source="MONTHLY_FEES"
               formatMask="NNN,NNN.00" alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="8.37500" y="0.93750" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_monthly_feesformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Manual_Credit" source="MANUAL_CREDIT"
               formatMask="NN,NNN,NNN.00" alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="11.00000" y="0.93750" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_manual_creditformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Total_Current_Period" source="TOTAL_CURR_PERIOD"
               formatMask="N,NNN,NNN.00" alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="11.87500" y="0.93750" width="0.87500"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_total_current_periodformattr"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Total_Pyr_Period" source="TOTAL_PYR_PERIOD"
               formatMask="N,NNN,NNN.00" alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="12.81250" y="0.93750" width="0.87500"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_total_pyr_periodformattrigge"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Member_status" source="MEMBER_STATUS"
               alignment="start">
                <font face="Arial" size="7"/>
                <geometryInfo x="2.62500" y="0.93750" width="0.68750"
                 height="0.12500"/>
                <generalLayout verticalElasticity="variable"/>
                <visualSettings fillBackgroundColor="r50g88b0"/>
              </field>
              <field name="F_FSG" source="FSG" formatMask="NN,NNN.NN"
               spacing="single" alignment="end">
                <font face="Arial" size="7"/>
                <geometryInfo x="10.12500" y="0.93750" width="0.81250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
            </repeatingFrame>
            <frame name="M_Group_Fee_Totals">
              <geometryInfo x="0.00000" y="1.12500" width="13.75000"
               height="0.37500"/>
              <advancedLayout printObjectOnPage="allPage"
               basePrintingOn="enclosingObject"/>
              <visualSettings fillPattern="transparent"/>
              <text name="B_15">
                <geometryInfo x="0.68750" y="1.25000" width="0.81250"
                 height="0.12500"/>
                <visualSettings fillBackgroundColor="r100g0b100"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Group Totals]]>
                  </string>
                </textSegment>
              </text>
              <text name="B_16">
                <geometryInfo x="2.06250" y="1.25000" width="0.75000"
                 height="0.14587"/>
                <visualSettings fillBackgroundColor="r100g0b100"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Member Count:]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_Member_Count" source="CS_Fee_Grp_Member_Count"
               formatMask="NNN,NNN" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="2.81250" y="1.25000" width="0.50000"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <text name="B_17">
                <geometryInfo x="3.62500" y="1.25000" width="1.06250"
                 height="0.14587"/>
                <visualSettings fillBackgroundColor="r100g0b100"/>
                <textSegment>
                  <font face="Arial" size="7" bold="yes"/>
                  <string>
                  <![CDATA[Non Member Count:]]>
                  </string>
                </textSegment>
              </text>
              <field name="F_NonMember_Count"
               source="CS_Fee_Grp_NonMember_Count" formatMask="NNN,NN0"
               alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="4.68750" y="1.25000" width="0.43750"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Total_Monthly_Fees"
               source="CS_Fee_Grp_Monthly_Fee_Total"
               formatMask="$N,NNN,NN0.00" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="8.37500" y="1.25000" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_total_monthly_feesformattrig"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Total_QA_Fees" source="CS_Fee_Grp_QA_Fee_Total"
               formatMask="$N,NNN,NN0.00" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="9.25000" y="1.25000" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_total_qa_feesformattrigger"/>
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Total_Manual_Credit"
               source="CS_Fee_Grp_Manual_Credit_Total"
               formatMask="$NNN,NNN,NN0.00" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="11.00000" y="1.25000" width="0.81250"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_total_manual_creditformattri"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Grp_Total_Curr_Period"
               source="CS_Fee_Grp_Cur_Period_Total"
               formatMask="$NNN,NNN,NN0.00" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="11.87500" y="1.25000" width="0.87500"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_grp_total_curr_periodformatt"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <field name="F_Grp_Total_Pyr_Period"
               source="CS_Fee_Grp_Pyr_Period_Total"
               formatMask="$NNN,NNN,NNN.00" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="12.81250" y="1.25000" width="0.87500"
                 height="0.12500"/>
                <advancedLayout formatTrigger="f_grp_total_pyr_periodformattr"
                />
                <visualSettings fillPattern="transparent"/>
              </field>
              <line name="B_25" arrow="none">
                <geometryInfo x="8.25000" y="1.18750" width="5.43750"
                 height="0.00000"/>
                <visualSettings fillPattern="transparent" linePattern="solid"/>
                <points>
                  <point x="8.25000" y="1.18750"/>
                  <point x="13.68750" y="1.18750"/>
                </points>
              </line>
              <field name="F_Total_FSG" source="CS_Fee_Grp_FSG_Total"
               formatMask="$N,NNN,NN0.00" spacing="single" alignment="end">
                <font face="Arial" size="7" bold="yes"/>
                <geometryInfo x="10.12500" y="1.25000" width="0.81250"
                 height="0.12500"/>
                <visualSettings fillPattern="transparent"/>
              </field>
            </frame>
          </repeatingFrame>
          <text name="B_23">
            <geometryInfo x="2.06250" y="1.68750" width="0.75000"
             height="0.14587"/>
            <textSegment>
              <font face="Arial" size="7" bold="yes"/>
              <string>
              <![CDATA[Member Count:]]>
              </string>
            </textSegment>
          </text>
          <text name="B_24">
            <geometryInfo x="3.62500" y="1.68750" width="1.06250"
             height="0.14587"/>
            <textSegment>
              <font face="Arial" size="7" bold="yes"/>
              <string>
              <![CDATA[Non Member Count:]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Grp_Member_Count"
           source="CS_Curr_Code_Grp_Member_count" formatMask="NNN,NNN"
           alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="2.81250" y="1.68750" width="0.50000"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_NonMember_Count"
           source="CS_Curr_Code_Grp_NonMember_Cnt" formatMask="NNN,NNN"
           alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="4.68750" y="1.68750" width="0.43750"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <text name="B_18">
            <textSettings justify="center"/>
            <geometryInfo x="5.06250" y="1.93750" width="3.25000"
             height="0.14587"/>
            <textSegment>
              <font face="Arial" size="8" bold="yes"/>
              <string>
              <![CDATA[&<CF_NO_DATA_MSG>]]>
              </string>
            </textSegment>
          </text>
          <field name="F_Grp_Monthly_Fee_Total"
           source="CS_Cur_Cd_Grp_Monthly_Fee_Tot" formatMask="$NNN,NNN,NN0.00"
           alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="8.37500" y="1.68750" width="0.81250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_QA_Fee_Total" source="CS_Cur_Cd_Grp_QA_Fee_Tot"
           formatMask="$NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="9.25000" y="1.68750" width="0.81250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_Manual_Credit_Total"
           source="CS_Cur_Cd_Grp_Manual_Cred_Tot"
           formatMask="$NNN,NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="11.00000" y="1.68750" width="0.81250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_Total_Current_Period_Tot"
           source="CS_Cur_Cd_Grp_Tot_Curr_Prd_Tot"
           formatMask="$NNN,NNN,NNN,NN0.00" alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="11.87500" y="1.68750" width="0.87500"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_Total_Pyr_Period_Total"
           source="CS_Cur_Cd_Grp_Total_Pyr_Pd_Tot"
           formatMask="$NNN,NNN,NNN,NN0.00" alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="12.81250" y="1.68750" width="0.87500"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Group_Total_Label" source="CF_Group_Total_Label"
           spacing="single" alignment="start">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="0.06250" y="1.68750" width="1.43750"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Grp_Fsg_Total" source="CS_Cur_Cd_Grp_FSG_Tot"
           formatMask="$NNN,NNN,NN0.00" spacing="single" alignment="end">
            <font face="Arial" size="7" bold="yes"/>
            <geometryInfo x="10.12500" y="1.68750" width="0.81250"
             height="0.12500"/>
            <visualSettings fillPattern="transparent"/>
          </field>
          <field name="F_Currency_code" source="CF_CC_Grp_Title"
           spacing="single" alignment="start">
            <font face="Arial" size="8" bold="yes"/>
            <geometryInfo x="0.06250" y="0.06250" width="1.18750"
             height="0.12500"/>
            <advancedLayout printObjectOnPage="allPage"
             basePrintingOn="enclosingObject"/>
            <visualSettings fillPattern="transparent"/>
          </field>
        </repeatingFrame>
      </frame>
      <frame name="M_G_Report_Totals">
        <geometryInfo x="0.00000" y="2.31250" width="13.75000"
         height="0.62500"/>
        <advancedLayout printObjectOnPage="lastPage"
         basePrintingOn="anchoringObject"
         formatTrigger="m_g_report_totalsformattrigger"/>
        <visualSettings fillPattern="transparent"/>
        <text name="B_20">
          <geometryInfo x="2.06250" y="2.37500" width="0.75000"
           height="0.12500"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
                <formatVisualSettings fillForegroundColor="r50g88b0"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_20formattrigger"/>
          <visualSettings fillBackgroundColor="r50g88b0"/>
          <textSegment>
            <font face="Arial" size="7" bold="yes"/>
            <string>
            <![CDATA[Member Count:]]>
            </string>
          </textSegment>
        </text>
        <text name="B_21">
          <geometryInfo x="3.62500" y="2.37500" width="1.06250"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_21formattrigger"/>
          <textSegment>
            <font face="Arial" size="7" bold="yes"/>
            <string>
            <![CDATA[Non Member Count:]]>
            </string>
          </textSegment>
        </text>
        <text name="B_13">
          <textSettings justify="center"/>
          <geometryInfo x="6.25000" y="2.56250" width="0.93750"
           height="0.31250"/>
          <visualSettings fillBackgroundColor="r100g50b75"/>
          <textSegment>
            <font face="Arial" size="8" bold="yes"/>
            <string>
            <![CDATA[End of Report
]]>
            </string>
          </textSegment>
          <textSegment>
            <font face="Arial" size="7" bold="yes"/>
            <string>
            <![CDATA[V1.12]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Report_Member_Count" source="CS_Report_Member_Count"
         formatMask="NNN,NNN" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="2.87500" y="2.37500" width="0.43750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_NonMember_Count"
         source="CS_Report_NonMember_Count" formatMask="NNN,NNN"
         alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="4.68750" y="2.37500" width="0.43750"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_Monthly_Fee_Total"
         source="CS_Report_Monthly_Fee_Total" formatMask="$NNN,NNN,NNN,NN0.00"
         spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="8.18750" y="2.37500" width="1.00000"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_QA_Fee_Total" source="CS_Report_QA_Fee_Total"
         formatMask="$NNN,NNN,NNN,NN0.00" spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="9.25000" y="2.37500" width="0.81250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_Manual_Credit_Total"
         source="CS_Report_Manual_Credit_Total"
         formatMask="$NNN,NNN,NNN,NN0.00" spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="11.00000" y="2.37500" width="0.81250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_Curr_Period_Total"
         source="CS_Report_Curr_Period_Total" formatMask="$NNN,NNN,NNN,NN0.00"
         spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="11.87500" y="2.37500" width="0.87500"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <field name="F_Report_Pyr_Period_Total"
         source="CS_Report_ Pyr_Period_Total" formatMask="$NNN,NNN,NNN,NN0.00"
         spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="12.81250" y="2.37488" width="0.87500"
           height="0.12512"/>
          <visualSettings fillPattern="transparent"/>
        </field>
        <text name="B_19">
          <geometryInfo x="0.06250" y="2.37500" width="0.81250"
           height="0.14587"/>
          <generalLayout>
            <conditionalFormat>
              <formatException label="(:CS_Record_count = &apos;0&apos;)">
                <font face="Courier New" size="12"/>
              <cond name="first" column="CS_Record_count" exception="1"
               lowValue="0" conjunction="1"/>
              </formatException>
            </conditionalFormat>
          </generalLayout>
          <advancedLayout formatTrigger="b_19formattrigger"/>
          <textSegment>
            <font face="Arial" size="7" bold="yes"/>
            <string>
            <![CDATA[Report Totals]]>
            </string>
          </textSegment>
        </text>
        <field name="F_Report_FSG_Total" source="CS_Report_FSG_Total"
         formatMask="$NNN,NNN,NNN,NN0.00" spacing="single" alignment="end">
          <font face="Arial" size="7" bold="yes"/>
          <geometryInfo x="10.12500" y="2.37500" width="0.81250"
           height="0.12500"/>
          <visualSettings fillPattern="transparent"/>
        </field>
      </frame>
    </body>
    <margin>
      <text name="B_12">
        <textSettings justify="center"/>
        <geometryInfo x="5.43750" y="0.06250" width="2.68750" height="0.62500"
        />
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[Monthly Fee Revenue
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[in US Dollars
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Dates>
]]>
          </string>
        </textSegment>
        <textSegment>
          <font face="Arial" size="9" bold="yes"/>
          <string>
          <![CDATA[&<cf_Title_Terr_Reg>]]>
          </string>
        </textSegment>
      </text>
      <field name="F_DATE1" source="CurrentDate" formatMask="MM/DD/YYYY"
       alignment="left">
        <font face="Arial" size="8" bold="yes"/>
        <geometryInfo x="0.18750" y="0.06250" width="0.93750" height="0.15625"
        />
        <generalLayout horizontalElasticity="variable"/>
      </field>
      <text name="B_PAGENUM1">
        <textSettings justify="right"/>
        <geometryInfo x="11.06250" y="0.06250" width="2.50000"
         height="0.14587"/>
        <textSegment>
          <font face="Arial" size="8" bold="yes"/>
          <string>
          <![CDATA[Page &<PageNumber> of &<TotalPages>]]>
          </string>
        </textSegment>
      </text>
    </margin>
  </section>
  </layout>
  <programUnits>
    <function name="cf_title_datesformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_DatesFormula return Char is
lv_title_dates varchar2(45);
begin
  lv_title_dates :=('From:  '||:P_MONTH1||' / '||:P_YEAR1||'  '||'To:  '||:P_MONTH2||' / '||:P_YEAR2);
 
  return (lv_title_dates);
end; 
]]>
      </textSource>
    </function>
    <function name="cf_title_terr_regformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Title_Terr_RegFormula return Char is
  lv_title3 varchar2 (100) null;
  temp_title varchar2(100);
begin
 temp_title := title_name (:p_member, :p_territory, :p_division); 
 If :p_territory != 'blank' and :p_territory != 'All' then
 	 lv_title3 := 'Territory # '||:p_territory ||' - '|| temp_title;
 elsif :p_division != 'blank' and :p_division != 'All' then
 	lv_title3 := 'Region # ' ||:p_division|| ' - ' ||temp_title;
 else
 	 lv_title3 := 'All Members';
 end if;
 return lv_title3;

end;    	
  	]]>
      </textSource>
    </function>
    <function name="cf_no_data_msgformula" returnType="character">
      <textSource>
      <![CDATA[function CF_No_Data_msgFormula return Char is
 lv_Message varchar2(80) null;
begin
  if :cs_record_count = 0 then
  	lv_Message := 'NO DATA FOUND FOR PARAMETERS REQUESTED';
  end if;	
 return lv_Message; 	 
end;]]>
      </textSource>
    </function>
    <function name="f_fbc_infoformattrigger">
      <textSource>
      <![CDATA[function F_FBC_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION = 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_3formattrigger">
      <textSource>
      <![CDATA[function B_3FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION = 'blank')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_4formattrigger">
      <textSource>
      <![CDATA[function B_4FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_div_infoformattrigger">
      <textSource>
      <![CDATA[function F_DIV_InfoFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:P_DIVISION != 'All')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_manual_creditformattri">
      <textSource>
      <![CDATA[function F_Total_Manual_CreditFormatTri return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_monthly_feesformattrig">
      <textSource>
      <![CDATA[function F_Total_Monthly_FeesFormatTrig return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_qa_feesformattrigger">
      <textSource>
      <![CDATA[function F_Total_QA_FeesFormatTrigger return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_grp_total_curr_periodformatt">
      <textSource>
      <![CDATA[function F_Grp_Total_Curr_PeriodFormatT return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_grp_total_pyr_periodformattr">
      <textSource>
      <![CDATA[function F_Grp_Total_Pyr_PeriodFormatTr return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_monthly_feesformattrigger">
      <textSource>
      <![CDATA[function F_Monthly_FeesFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:MONTHLY_FEES = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_qa_feesformattrigger">
      <textSource>
      <![CDATA[function F_QA_FeesFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:QA_FEES = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_manual_creditformattrigger">
      <textSource>
      <![CDATA[function F_Manual_CreditFormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:MANUAL_CREDIT = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_current_periodformattr">
      <textSource>
      <![CDATA[function F_Total_Current_PeriodFormatTr return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:TOTAL_CURR_PERIOD = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="f_total_pyr_periodformattrigge">
      <textSource>
      <![CDATA[function F_Total_Pyr_PeriodFormatTrigge return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:TOTAL_PYR_PERIOD = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_19formattrigger">
      <textSource>
      <![CDATA[function B_19FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_20formattrigger">
      <textSource>
      <![CDATA[function B_20FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="b_21formattrigger">
      <textSource>
      <![CDATA[function B_21FormatTrigger return boolean is
begin

  -- Automatically Generated from Report Builder.
  if (:CS_Record_count = '0')
  then
    return (FALSE);
  end if;

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_1formula" returnType="number">
      <textSource>
      <![CDATA[function CF_1Formula return Number is
 range_code_sort numeric;
begin
	If :currency_code = 'USD' then
    If :range_code = '$219 & Up Members' then
    	range_code_sort := 1;
    elsif :range_code = '$149 Members' then
    	range_code_sort := 2;
    elsif :range_code ='$69 - $79 Members' then
    	range_code_sort := 3;  
    elsif :range_code = '$1 - $25 Members' then
    	range_code_sort := 4;
    elsif :range_code = '$0 Members' then
    	range_code_sort := 5;
    elsif :range_code = 'PPO Members' then
    	range_code_sort := 6;
    end if;
  else
    	If :range_code = '$89 & Up Members' then
    	  range_code_sort := 1;
      elsif :range_code = '$69 Members' then
      	range_code_sort := 2;
      elsif :range_code = '$60 Members' then
      	range_code_sort := 3;  
      elsif :range_code = '$0 Members' then
      	range_code_sort := 4;
      elsif :range_code = 'PPO Members' then
      	range_code_sort := 5;
      end if;
  end if;
  return range_code_sort;
end;
]]>
      </textSource>
    </function>
    <function name="m_g_report_totalsformattrigger">
      <textSource>
      <![CDATA[function M_G_Report_TotalsFormatTrigger return boolean is
begin

  return (TRUE);
end;]]>
      </textSource>
    </function>
    <function name="cf_group_total_labelformula" returnType="character">
      <textSource>
      <![CDATA[function CF_Group_Total_LabelFormula return Char is
  Group_Total_Label Varchar2 (50);  
begin
  If :currency_code = 'CAD' then
  	Group_Total_Label := 'Canadian (in USD) Totals';
  else
  	Group_Total_Label := 'US Totals';
  end if;
  return Group_Total_Label;
   	
end;]]>
      </textSource>
    </function>
    <function name="cf_cc_grp_titleformula" returnType="character">
      <textSource>
      <![CDATA[function CF_CC_GRP_TitleFormula return Char is
  CC_grp_title varchar2 (20) null;
begin
  If :currency_code = 'USD' then
 	 CC_grp_title := 'US Members';
  else
 	 CC_grp_title := 'Canadian Members';
 end if;
 return CC_grp_title;
end;    	
  	]]>
      </textSource>
    </function>
  </programUnits>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
