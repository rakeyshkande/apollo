<%@ taglib uri="/WEB-INF/lib/reports_tld.jar" prefix="rw" %> 
<%@ page language="java" import="java.io.*" errorPage="/rwerror.jsp" session="false" %>
<%@ page contentType="text/html;charset=ISO-8859-1" %>
<!--
<rw:report id="report"> 
<rw:objects id="objects">
<?xml version="1.0" encoding="WINDOWS-1252" ?>
<report name="ReportServerCheck" DTDVersion="9.0.2.0.10">
  <xmlSettings xmlTag="MODULE5" xmlPrologType="text">
  <![CDATA[<?xml version="1.0" encoding="&Encoding"?>]]>
  </xmlSettings>
  <data>
    <dataSource name="Q_1">
      <select>
      <![CDATA[SELECT USER, SYS_CONTEXT('USERENV','HOST'), TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS')
FROM DUAL;]]>
      </select>
      <displayInfo x="1.65002" y="1.00000" width="0.69995" height="0.19995"/>
      <group name="G_USER">
        <displayInfo x="0.57166" y="1.94995" width="2.85681" height="0.79956"
        />
        <dataItem name="USER" datatype="vchar2" columnOrder="11" width="30"
         defaultWidth="100000" defaultHeight="10000" columnFlags="1"
         defaultLabel="User">
          <dataDescriptor expression="USER" descriptiveExpression="USER"
           order="1" width="30"/>
        </dataItem>
        <dataItem name="SYS_CONTEXT_USERENV_HOST" datatype="vchar2"
         columnOrder="12" width="256" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="Sys Context Userenv Host">
          <dataDescriptor
           expression="SYS_CONTEXT ( &apos;USERENV&apos; , &apos;HOST&apos; )"
           descriptiveExpression="SYS_CONTEXT(&apos;USERENV&apos;,&apos;HOST&apos;)"
           order="2" width="256"/>
        </dataItem>
        <dataItem name="TO_CHAR_SYSDATE_MM_DD_YYYY_HH2" datatype="vchar2"
         columnOrder="13" width="19" defaultWidth="100000"
         defaultHeight="10000" columnFlags="1"
         defaultLabel="To Char Sysdate Mm Dd Yyyy Hh2">
          <dataDescriptor
           expression="TO_CHAR ( SYSDATE , &apos;MM/DD/YYYY HH24:MI:SS&apos; )"
           descriptiveExpression="TO_CHAR(SYSDATE,&apos;MM/DD/YYYYHH24:MI:SS&apos;)"
           order="3" width="19"/>
        </dataItem>
      </group>
    </dataSource>
  </data>
  <layout>
  <section name="main">
    <body>
      <repeatingFrame name="R_G_USER" source="G_USER" printDirection="down"
       maxRecordsPerPage="1" minWidowRecords="1" columnMode="no">
        <geometryInfo x="0.00000" y="0.00000" width="6.93750" height="0.18750"
        />
        <generalLayout verticalElasticity="variable"/>
        <text name="B_tbp">
          <geometryInfo x="0.00000" y="0.00000" width="6.93750"
           height="0.18750"/>
          <textSegment>
            <font face="Courier New" size="10"/>
            <string>
            <![CDATA[Report server on host &<SYS_CONTEXT_USERENV_HOST> is up as of &<TO_CHAR_SYSDATE_MM_DD_YYYY_HH2>.]]>
            </string>
          </textSegment>
        </text>
      </repeatingFrame>
    </body>
  </section>
  </layout>
  <reportPrivate templateName="rwbeige"/>
</report>
</rw:objects>
-->

<html>

<head>
<meta name="GENERATOR" content="Oracle 9i Reports Developer"/>
<title> Your Title </title>

<rw:style id="yourStyle">
   <!-- Report Wizard inserts style link clause here -->
</rw:style>

</head>


<body>

<rw:dataArea id="yourDataArea">
   <!-- Report Wizard inserts the default jsp here -->
</rw:dataArea>



</body>
</html>

<!--
</rw:report> 
-->
