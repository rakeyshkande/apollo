package com.ftd.qd.bo;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.VendorVO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cache.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.qd.common.ProcessedStatus;
import com.ftd.qd.constants.QueueDeleteConstants;
import com.ftd.qd.dao.QueueDeleteDAO;
import com.ftd.qd.util.MessagingServiceLocator;
import com.ftd.qd.util.QueueDeleteUtil;
import com.ftd.qd.vo.CommentsVO;
import com.ftd.qd.vo.MercuryVO;
import com.ftd.qd.vo.MessageOrderStatusVO;
import com.ftd.qd.vo.VenusVO;

import java.io.StringReader;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

//import oracle.xml.parser.v2.DOMParser;
//import oracle.xml.parser.v2.XMLDocument;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;


/**
 * This class is responsible for processing queue delete record.
 */
public class QueueDeleteBO 
{
  private Connection connection;
  private static Logger logger  = new Logger("com.ftd.qd.bo.QueueDeleteBO");
  private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
 
  
  /*
   * Default Constructor.
   */
  public QueueDeleteBO(Connection connection) {
      this.connection = connection;
  }


  public void processRequest(QueueDeleteHeaderVO queueDeleteHeaderVO)
      throws Exception, Throwable {
      assert(this.connection!=null) : "Database connection is null in QueueDeleteBO.processRequest";
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(this.connection);
      String systemMessage = "MQD";
           
      //retrieve QueueDeleteDetailVO
      QueueDeleteDetailVO queueDeleteDetailVO = null;
      List queueDeleteDetailVOList = queueDeleteHeaderVO.getQueueDeleteDetailVOList();
      Iterator it = queueDeleteDetailVOList.iterator();
      while (it.hasNext()) {
          queueDeleteDetailVO = (QueueDeleteDetailVO)it.next();
      }
      
      Long queueDeleteHeaderId = queueDeleteHeaderVO.getQueueDeleteHeaderId();
      Long queueDeleteDetailId = queueDeleteDetailVO.getQueueDeleteDetailId();    
      boolean isDuplicateMessage = false;
      
      //retrieve delete batch by type
      String batchType = queueDeleteHeaderVO.getDeleteBatchByType();
      boolean deleteByMessageId = false;
      boolean deleteByExternalOrderNumber = false;
      String idToDelete = null;
      
      if (batchType.equalsIgnoreCase(QueueDeleteConstants.MESSAGE_ID_BATCH_TYPE))
      {
          deleteByMessageId = true;
          idToDelete = queueDeleteDetailVO.getMessageId().toString();

          //check if dup record
          isDuplicateMessage = qdDAO.isDuplicateMessageId(queueDeleteDetailId, queueDeleteHeaderId, queueDeleteDetailVO.getMessageId());
      }
      else if (batchType.equalsIgnoreCase(QueueDeleteConstants.ORDER_NUMBER_BATCH_TYPE))
      {
          deleteByExternalOrderNumber = true;
          idToDelete = queueDeleteDetailVO.getExternalOrderNumber();

          //check if dup record
          isDuplicateMessage = qdDAO.isDuplicateOrderNumber(queueDeleteDetailId, queueDeleteHeaderId, queueDeleteDetailVO.getExternalOrderNumber());
      }
      
      if(isDuplicateMessage)
      {
         qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.DUPLICATE.toString(), null, QueueDeleteConstants.UPDATED_BY);
      }
      else
      {
        //begin MQD code
        boolean messageProcessed = false; 
         try
         {
              //Check for the partners that need to be excluded.
              List excludedPartners = new ArrayList();

              if (queueDeleteHeaderVO.getExcludePartnerName() != null && !queueDeleteHeaderVO.getExcludePartnerName().equalsIgnoreCase(""))
              {
                StringTokenizer qST = new StringTokenizer(queueDeleteHeaderVO.getExcludePartnerName(), ",");
                while (qST.hasMoreTokens())
                {
                  String excludedPartnerName = qST.nextToken().trim();
                  excludedPartners.add(excludedPartnerName);
                }
              }
         
             /**************************************************/
             /** DELETE ASSOCIATED QUEUE MESSAGES **/
             /**************************************************/
             boolean bDeleteOtherQueues = false;
             String cbDeleteOtherQueuesRaw = queueDeleteHeaderVO.getDeleteQueueMsgFlag();

             if (cbDeleteOtherQueuesRaw != null && cbDeleteOtherQueuesRaw.equalsIgnoreCase("Y"))
             {
               bDeleteOtherQueues = true;
             }
             String deleteOtherQueuesSelected = queueDeleteHeaderVO.getDeleteQueueMsgType();
                         
             /**************************************************/
             /** SEND MESSAGE **/
             /**************************************************/
             boolean bSendMessage = false;
             String cbSendMessageRaw = queueDeleteHeaderVO.getSendMessageFlag();

             if (cbSendMessageRaw != null && cbSendMessageRaw.equalsIgnoreCase("Y"))
             {
               bSendMessage = true;
             }
             
             String sSendMessageType = queueDeleteHeaderVO.getMessageType();
             
             /**************************************************/
             /** RESUBMIT ORDER FOR PROCESSING **/
             /**************************************************/
             boolean isOrderReprocessed = false;
             String isOrderReprocessedRaw = queueDeleteHeaderVO.getResubmitOrderFlag();

             if (isOrderReprocessedRaw != null && isOrderReprocessedRaw.equalsIgnoreCase("Y"))
             {
               isOrderReprocessed = true;
             }
           
             /**************************************************/
             /** ADD ORDER COMMENT **/
             /**************************************************/
             boolean isCommentAdded = false;
             String isCommentAddedRaw = queueDeleteHeaderVO.getIncludeCommentFlag();

             if (isCommentAddedRaw != null && isCommentAddedRaw.equalsIgnoreCase("Y"))
             {
               isCommentAdded = true;
             }
          
             /**************************************************/
             /** SEND CUSTOMER EMAIL **/
             /**************************************************/
             boolean isEmailAdded = false;
             String isEmailAddedRaw = queueDeleteHeaderVO.getSendEmailFlag();

             if (isEmailAddedRaw != null && isEmailAddedRaw.equalsIgnoreCase("Y"))
             {
               isEmailAdded = true;
             }
         
             /**************************************************/
             /** REFUND ORDER **/
             /**************************************************/
             boolean isOrderRefunded = false;
             String isOrderRefundedRaw = queueDeleteHeaderVO.getRefundOrderFlag();
             if (isOrderRefundedRaw != null && isOrderRefundedRaw.equalsIgnoreCase("Y"))
             {
               isOrderRefunded = true;
             }
             
             
             String refundDisposition = queueDeleteHeaderVO.getRefundDispCode();
             String responsibleParty = queueDeleteHeaderVO.getResponsibleParty();
         
             logger.debug("Flags set based on checkboxes checked are as follows:");
             logger.debug("deleteOtherQueueTypes: " + bDeleteOtherQueues);
             logger.debug("sendMercuryVenus: " + bSendMessage);
             logger.debug("isOrderReprocessed: " + isOrderReprocessed);
             logger.debug("isCommentAdded: " + isCommentAdded);
             logger.debug("isEmailAdded: " + isEmailAdded);
             logger.debug("isOrderRefunded: " + isOrderRefunded);
             logger.debug("-----------------------------------------------------------------------------------------------------------");

             /* Call lookupCurrentUserId to get the current user�s id */
             String userId = queueDeleteHeaderVO.getCreatedBy();

             if (userId == null)
             {
               throw new RuntimeException("execute: User ID can not be null.");
             }

             try
             {
        
               List otherQueuesList = new ArrayList();
            
               queueDeleteHeaderVO.setBatchComment(systemMessage);
                          
               //List out all the other queues that the user has selected to delete
               if (bDeleteOtherQueues)
               {
                 StringTokenizer qST = new StringTokenizer(deleteOtherQueuesSelected, ",");
                 while (qST.hasMoreTokens())
                 {
                   String otherQueueSelected = qST.nextToken().trim();
                   otherQueuesList.add(otherQueueSelected);
                 }
               }
                      
                 logger.info("MQD working on " + idToDelete);
                 
                 // Obtain the order detail ID, if any.  Since this query relies on the queue record's existence,
                 // this must be performed before any message deletion.
                 String messageId = null;
                 String orderDetailId = null;
                 String queueType = null;
                 String messageType = null;
                 String mercuryMessageNumber = null;
                 String mercuryOrderNumber = null;
                 String mercuryId = null;
                 String venusOrderNumber = null;
                 String venusId = null;
                 String externalOrderNumber = null;
                 String system = null;
                 String queueIndicator = null;
                 String pointOfContactId = null;
                 boolean recordFound = false;
                 boolean isOrderAlreadyProcessed = true;
                 String partnerName = null;
                 String sourceCode = null;
                 BigDecimal priceFTD = null;
                 BigDecimal priceASKP = null;

                 HashMap retrievedFields = new HashMap();

                 try //outer catch block
                 {
                   CachedResultSet crs;

                   //if the user entered the queue id, search by queue id
                   if (deleteByMessageId)
                   {
                     if (!StringUtils.isNumeric(idToDelete))
                       throw new Exception("Invalid Queue Id detected");
                 
                     crs = qdDAO.getQueueMessage(idToDelete);
                   }
                   //else, search by external order number
                   else
                     crs = this.getQueueMessageByExternalOrderNumber(idToDelete, otherQueuesList);

                   while (crs.next())
                   {
                     recordFound = true;

                     if (crs.getObject("message_id") != null)
                       messageId = crs.getString("message_id");
                       
                     if (crs.getObject("order_detail_id") != null)
                       orderDetailId = crs.getString("order_detail_id");
                     if (crs.getObject("queue_type") != null)
                       queueType = crs.getString("queue_type");
                     if (crs.getObject("message_type") != null)
                       messageType = crs.getString("message_type");
                     if (crs.getObject("mercury_number") != null)
                       mercuryMessageNumber = crs.getString("mercury_number");
                     if (crs.getObject("mercury_id") != null)
                     {
                       mercuryId = crs.getString("mercury_id");
                       venusId = crs.getString("mercury_id");
                     }
                     if (crs.getObject("mercury_venus_order_number") != null)
                     {
                       mercuryOrderNumber = crs.getString("mercury_venus_order_number");
                       venusOrderNumber = crs.getString("mercury_venus_order_number");
                     }
                     if (crs.getObject("external_order_number") != null)
                       externalOrderNumber = crs.getString("external_order_number");
                     if (crs.getObject("system") != null)
                       system = crs.getString("system");
                     if (crs.getObject("queue_indicator") != null)
                       queueIndicator = crs.getString("queue_indicator");
                     if (crs.getObject("point_of_contact_id") != null)
                       pointOfContactId = crs.getString("point_of_contact_id");
                       
                     if (crs.getObject("partner_name") != null)
                       partnerName = crs.getString("partner_name");
                       
                     if (crs.getObject("price_ftd") != null)
                       priceFTD = crs.getBigDecimal("price_ftd");

                     if (crs.getObject("price_askp") != null)
                       priceASKP = crs.getBigDecimal("price_askp");
                       

                     if (orderDetailId != null && !orderDetailId.equalsIgnoreCase(""))
                       break;
                       
                  } //end-while

                   if (recordFound)
                   {
                     retrievedFields.put("idToDelete", idToDelete);
                     retrievedFields.put("messageId", messageId);
                     retrievedFields.put("orderDetailId", orderDetailId);
                     retrievedFields.put("queueType", queueType);
                     retrievedFields.put("messageType", messageType);
                     retrievedFields.put("externalOrderNumber", externalOrderNumber);
                     retrievedFields.put("system", system);
                     retrievedFields.put("queueIndicator", queueIndicator);
                     retrievedFields.put("mercuryMessageNumber", mercuryMessageNumber);
                     retrievedFields.put("mercuryOrderNumber", mercuryOrderNumber);
                     retrievedFields.put("mercuryId", mercuryId);
                     retrievedFields.put("venusOrderNumber", venusOrderNumber);
                     retrievedFields.put("venusId", venusId);
                     retrievedFields.put("pointOfContactId", pointOfContactId);
                     retrievedFields.put("priceFTD", priceFTD);
                     retrievedFields.put("priceASKP", priceASKP);

                     logger.info("Record found for ID = " + idToDelete + ". The orderDetailId = " + orderDetailId);
                 
                     //partner is excluded
                     if(isPartnerExcluded(partnerName, excludedPartners)) {
                    
                         throw new Exception(partnerName + " orders were excluded and will not be affected by the selected procedure.");
                     }

                     // Perform the message deletion
                     if (deleteByMessageId)
                     {
                       //delete based off of the queue id entered
                       qdDAO.deleteQueue(idToDelete, userId);

                       systemMessage += " queue deleted id: " + idToDelete;
                      

                       //if the DELETE ASSOCIATED QUEUE MESSAGES is checked, delete all other queue messages based off of Order Detail Id
                       if (bDeleteOtherQueues && orderDetailId != null && !orderDetailId.equalsIgnoreCase(""))
                       {
                         this.deleteAllQsByOrdDetId(orderDetailId, otherQueuesList, userId);
                         systemMessage += ", queues deleted: " + deleteOtherQueuesSelected;
                       }
                     }
                     else
                     {
                       //DELETE ASSOCIATED QUEUE MESSAGES must be checked.  Delete all queue types selected based off of the external order number
                       this.deleteAllQsByExtOrdNum(idToDelete, otherQueuesList, userId);
                       systemMessage += " queue deleted id: " + idToDelete;
                       systemMessage += ", queues deleted: " + deleteOtherQueuesSelected;
                     }

                     //At this point, recordFound is true, implying that the record is found in the CLEAN.Queue table.
                     //
                     //If a record exists in CLEAN.Queue but order detail is NOT found, we should check to see if the user is trying to 
                     //q-delete ONLY, or if s/he is trying to perform other tasks along with. 
                     //  1) If the user is q-deleting ONLY, we will allow. 
                     //  2) If the user is q-deleting but has selected other options that require order detail id, we will NOT q-delete. The
                     //     user will get an error message.
                     if (orderDetailId == null || orderDetailId.equalsIgnoreCase(""))
                     {
                       if (isCommentAdded || isOrderRefunded || bSendMessage || isOrderReprocessed || isEmailAdded)
                         throw new Exception("Expected Order Detail ID does not exist");

                       else
                       {
                         //add system-generated SUCCESS order comment that will show what MQD did
                         processComment(systemMessage, retrievedFields, this.connection, orderDetailId, "SYS");
                       }
                     }
                     else
                     {
                         //get the customer and the order information and put them in the retrievedFields hashmap
                         getCustomerOrderInfo(orderDetailId, retrievedFields, this.connection);
                         //get the recipient and the order information and put them in the retrievedFields hashmap
                         getRecipientOrderInfo(orderDetailId, retrievedFields, this.connection);
                         //get the mercury / venus info and save it in retrievedFields hashmap
                         getMercuryVenusInfo(retrievedFields, this.connection);

                         // If pertinent, add an order comment
                         if (isCommentAdded)
                         {
                           //add user-generated order comment
                           processComment(queueDeleteHeaderVO.getCommentText(), retrievedFields, this.connection, orderDetailId, userId);

                           //append useful info to the success message, to be added to the order comments
                           systemMessage += ", comment added";
                         }

                         // If pertinent, refund the order
                         if (isOrderRefunded)
                         {
                           String sOrderDeliveryDate = (String) retrievedFields.get("orderDeliveryDate");
                           Date orderDeliveryDate = sdf.parse(sOrderDeliveryDate);
                           Calendar today = Calendar.getInstance();
                           today.set(Calendar.HOUR_OF_DAY, 0);
                           today.set(Calendar.MINUTE, 0);
                           today.set(Calendar.SECOND, 0);
                           today.set(Calendar.MILLISECOND, 0);

                           if (orderDeliveryDate != null && 
                               today.getTime().after(orderDeliveryDate) && 
                               (new Character(refundDisposition.charAt(0)).toString().equalsIgnoreCase("A")))
                             throw new Exception("You cannot choose an A-type refund for orders that are past the delivery date");
                           else
                           {
                              try {
                                 logger.debug("API call to postFullRemainingRefund...disp" + refundDisposition + " || responsible party" + responsibleParty);
                                 QueueDeleteUtil.postFullRemainingRefund(orderDetailId, queueDeleteHeaderVO);
                               } catch (Exception e) {
                                 logger.error("postFullRemainingRefund call failed: " + e);
                               }

                             //append useful info to the success message, to be added to the order comments
                             systemMessage += ", order refunded";
                           }
                         }

                         // If pertinent, add a mercury/venus message
                         if (deleteByMessageId && bSendMessage)
                         {
                           //If the user is trying to send a message for anything but Mercury, Venus, Argo, Order type, throw an exception
                           if (queueIndicator.equalsIgnoreCase("Email"))
                             throw new Exception("Send message functionality cannot be used for Email queue.  No Message sent.");

                           processMessage(queueDeleteHeaderVO, retrievedFields, sSendMessageType, this.connection, userId, systemMessage);

                           //append useful info to the success message, to be added to the order comments
                           systemMessage += ", " + sSendMessageType + " message sent";
                         }

                         // If pertinent, reprocess the order
                         if (deleteByMessageId && isOrderReprocessed)
                         {
                           //If the user is trying to send a message for anything but Mercury, Venus, Argo, Order type, throw an exception
                           if (queueIndicator.equalsIgnoreCase("Email"))
                             throw new Exception("Resubmit order functionality cannot be used for Email queue.  Order was not reprocessed.");

                           systemMessage  = processMessage(queueDeleteHeaderVO, retrievedFields, "FTD", this.connection, userId, systemMessage);

                           //append useful info to the success message, to be added to the order comments
                           systemMessage += ", order reprocessed";
                         }

                         // If pertinent, send an email
                         if (isEmailAdded)
                         {
                           sendCustomerEmail(queueDeleteHeaderVO, retrievedFields, this.connection, orderDetailId, userId);

                           //append useful info to the success message, to be added to the order comments
                           systemMessage += ", email sent";
                         }

                         //add system-generated SUCCESS order comment that will show what MQD did
                         processComment(systemMessage, retrievedFields, this.connection, orderDetailId, "SYS");

                     }
                   }
                   //if queue record does not exist, maybe the user just wants to send another email or something.  Check if the user initiated an 
                   //order vs. queue search, and if former, allow some actions. 
                   else
                   {
                     if (deleteByMessageId)
                     {
                                            
                       if (!messageProcessed)
                       {
                        qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.FAILURE.toString(), "Id could not be found", QueueDeleteConstants.UPDATED_BY);
                        messageProcessed = true;
                       }
                     }
                     else
                     {
                       //if user want to update the comment, or apply refund, or send an email, allow it - Issue 5464
                       if (isCommentAdded || isOrderRefunded || isEmailAdded)
                       {
                         //see if its a valid order number.  If so, allow some actions.
                         orderDetailId = getOrderDetailInfo(idToDelete, this.connection); 
                         
                         //if order number cannot be found, i.e., invalid external order number was entered, throw an exception
                         if (orderDetailId == null || orderDetailId.equalsIgnoreCase(""))
                           throw new Exception("Id for the queue types selected could not be found");
                         else
                         {                     
                             //get the customer and the order information and put them in the retrievedFields hashmap
                             getCustomerOrderInfo(orderDetailId, retrievedFields, this.connection);
         
                             //get the recipient and the order information and put them in the retrievedFields hashmap
                             getRecipientOrderInfo(orderDetailId, retrievedFields, this.connection);
                             
                             //partner is excluded
                             if(isPartnerExcluded(partnerName, excludedPartners)) {
                                 throw new Exception(partnerName + " orders were excluded and will not be affected by the selected procedure.");
                             }

                             // If pertinent, add an order comment
                             if (isCommentAdded)
                             {
                               //add user-generated order comment
                               processComment(queueDeleteHeaderVO.getCommentText(), retrievedFields, this.connection, orderDetailId, userId);
         
                               //append useful info to the success message, to be added to the order comments
                               systemMessage += ", comment added";
                             }
         
                             // If pertinent, refund the order
                             if (isOrderRefunded)
                             {
                               String sOrderDeliveryDate = (String) retrievedFields.get("orderDeliveryDate");
                               Date orderDeliveryDate = sdf.parse(sOrderDeliveryDate);
                               Calendar today = Calendar.getInstance();
                               today.set(Calendar.HOUR_OF_DAY, 0);
                               today.set(Calendar.MINUTE, 0);
                               today.set(Calendar.SECOND, 0);
                               today.set(Calendar.MILLISECOND, 0);
         
                               if (orderDeliveryDate != null && 
                                   today.getTime().after(orderDeliveryDate) && 
                                   (new Character(refundDisposition.charAt(0)).toString().equalsIgnoreCase("A")))
                                 throw new Exception("You cannot choose an A-type refund for orders that are past the delivery date");
                               else
                               {
                                 try {
                                     logger.debug("EJB call to postFullRemainingRefund...disp" + refundDisposition + " || responsible party" + responsibleParty);
                                     QueueDeleteUtil.postFullRemainingRefund(orderDetailId, queueDeleteHeaderVO);
                                 } catch (Exception e) {
                                     logger.error("postFullRemainingRefund call failed: " + e);
                                 }
         
                                 //append useful info to the success message, to be added to the order comments
                                 systemMessage += ", order refunded";
                               }
                             }

                             // If pertinent, send an email
                             if (isEmailAdded)
                             {
                               sendCustomerEmail(queueDeleteHeaderVO, retrievedFields, this.connection, orderDetailId, userId);
         
                               //append useful info to the success message, to be added to the order comments
                               systemMessage += ", email sent";
                             }

       
                             //add system-generated SUCCESS order comment that will show what MQD did
                             processComment(systemMessage, retrievedFields, this.connection, orderDetailId, "SYS");
                  
                           
                         }
                       }
                       //else, user is trying to perform actions that are not allowed if the message is not in queue.  
                       else
                         throw new Exception("Cannot Send Message by an input of external order number");
                         
                     }

                   }

                   if (!messageProcessed)
                   {
                      qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.SUCCESS.toString(), null, QueueDeleteConstants.UPDATED_BY);
                      messageProcessed = true;
                   }
                   logger.debug("execute: Finished processing messageId= " + idToDelete + " orderDetailId=" + orderDetailId);
                 } //end - outer catch block

                 catch (Exception e)
                 {
                   // record the error and proceeed to the next record.
                   logger.error("MQD execute: Failed To Delete Id = " + idToDelete, e);
                  
                   if (!messageProcessed)
                   {
                     qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.FAILURE.toString(), e.getMessage() + ". " + partnerName, QueueDeleteConstants.UPDATED_BY);
                     messageProcessed = true;
                   }  
                   
                   
                                     
                   if(partnerName == null) {
                     partnerName = "";
                   }
                   
                   
                   throw new Exception(e.getMessage() + ". " + partnerName);
                   
                 }
           
             }
             catch (Exception e)
             {
                logger.error(e);

                if (!messageProcessed)
                {
                  qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.FAILURE.toString(), e.getMessage(), QueueDeleteConstants.UPDATED_BY);
                  messageProcessed = true;
                }
                throw new Exception(e.getMessage());

              }
         }
         catch (Exception e)
         {
           logger.error(e);

           if (!messageProcessed)
           {
             qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.FAILURE.toString(), e.getMessage(), QueueDeleteConstants.UPDATED_BY);
             messageProcessed = true;
           } 
           throw new Exception(e.getMessage());
         }
         finally
         {
           try
           {
           }
           catch (Exception e)
           {
             logger.error(e);

             
               
             qdDAO.updateQueueDeleteDetailRecord(queueDeleteDetailId.toString(), ProcessedStatus.FAILURE.toString(), e.getMessage(), QueueDeleteConstants.UPDATED_BY);
             throw new Exception(e.getMessage());
           }
         }
        //end MQD code
       }//end else
     
  }  
  
 
        /**
         * Process an ANS message.
         *
         * @param queueDeleteHeaderVO - QueueDeleteHeaderVO
         * @param retrievedFields - HashMap that contains values retrieved from DB
         * @param messagesVO - An Array consisting of all messages from Mercury table
         * @param userId - String
         * @param conn - Database Connection
         *
         * @return n/a
         *
         * @throws Exception
         */
        private String processANSMessage(QueueDeleteHeaderVO queueDeleteHeaderVO, HashMap retrievedFields, ArrayList messagesVO, String userId, Connection conn, String systemMessage)
          throws Exception
        {
          String queueType = (String) retrievedFields.get("queueType");
          String messageType = (String) retrievedFields.get("messageType");
          String system = (String) retrievedFields.get("system");
          String newMessage = queueDeleteHeaderVO.getTextOrReason();
          String sAnspAmount = null;
          if (queueDeleteHeaderVO.getAnspAmount() != null)
            sAnspAmount = queueDeleteHeaderVO.getAnspAmount().toString();
          String sAnspOverride = queueDeleteHeaderVO.getGiveFloristAskingPrice();
          BigDecimal priceFTD = (BigDecimal) retrievedFields.get("priceFTD");
          BigDecimal priceASKP = (BigDecimal) retrievedFields.get("priceASKP");
          
          
          MercuryVO mVO = null;
          MercuryVO mFtdVO = null;

          //check to see if the queue id that we are working on points to an ASK.  Since an ANS can only be sent on an ASK, if the queue id entered points to any 
          //other queue, throw an exception. 
          if (!queueType.equalsIgnoreCase("ASK"))
            throw new Exception("ASK Message not in queue.  No ANS sent");

          if (system.equalsIgnoreCase("Venus") || system.equalsIgnoreCase("Argo"))
            throw new Exception("ANS message cannot be sent for a Vendor Order.");

          //get the FTD record
          for (int x = 0; x < messagesVO.size(); x++)
          {
            mVO = (MercuryVO) messagesVO.get(x);
            if (mVO.getMsgType().equalsIgnoreCase("FTD"))
            {
              mFtdVO = (MercuryVO) messagesVO.get(x);
              break;
            }
          }

          ResultTO result = null;
          ANSMessageTO ansTO = new ANSMessageTO();

          ansTO.setMercuryId(mFtdVO.getMercuryId());
          ansTO.setComments(newMessage);
          ansTO.setOperator(userId);
          ansTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
          ansTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
          ansTO.setOldPrice(mFtdVO.getOldPrice());
          ansTO.setPrice(mFtdVO.getPrice());
          ansTO.setFillingFlorist(mFtdVO.getFillingFlorist());
          ansTO.setSendingFlorist(mFtdVO.getSendingFlorist());
          ansTO.setAskAnswerCode(null);

          //issues 8410 and 8411 - start
          //if message type = ASKP or ASKPI, and neither sAnspOverride != Y nor sAnspAmount is valid
          if  ( ( messageType.equalsIgnoreCase("ASKP") || 
                  messageType.equalsIgnoreCase("ASKPI")
                ) &&
                ( StringUtils.equalsIgnoreCase(sAnspOverride, "N") && 
                  ( StringUtils.isBlank(sAnspAmount) || 
                    !NumberUtils.isNumber(sAnspAmount)
                  )
                )
              )
          {
            throw new Exception("ANSP cannot be sent without a dollar value");
          }
 
          //if sAnspOverride = Y, it can only be sent on an ASKP or ASKPI
          if (StringUtils.equalsIgnoreCase(sAnspOverride, "Y"))
          {
            if (messageType.equalsIgnoreCase("ASKP") || messageType.equalsIgnoreCase("ASKPI"))
            {
              ansTO.setAskAnswerCode("P");
              if (priceASKP != null)
              {
                ansTO.setPrice(priceASKP.doubleValue());//   = BigDecimal.valueOf(ansTO.getPrice());
                systemMessage += ", ASKP price is:"+ priceASKP.doubleValue();
              }
            }
            else
            {
              throw new Exception("MQD allows ANSP to be sent for an ASKP only");
            }
          }

          //if valid sAnspAmount, it can only be sent on an ASKP or ASKPI
          if (!StringUtils.isBlank(sAnspAmount) && NumberUtils.isNumber(sAnspAmount))
          {
            if (messageType.equalsIgnoreCase("ASKP") || messageType.equalsIgnoreCase("ASKPI"))
            {
              BigDecimal incrementPrice = BigDecimal.valueOf(Double.valueOf(sAnspAmount));   
              ansTO.setAskAnswerCode("P");
              if (priceFTD != null)
              {
                ansTO.setPrice((priceFTD.add(incrementPrice)).doubleValue());
              }
            }
            else
            {
              throw new Exception("MQD allows ANSP to be sent for an ASKP only");
            }
          }//issues 8410 and 8411 - end

          result = MessagingServiceLocator.getInstance().getMercuryAPI().sendANSMessage(ansTO, conn);

          //check for errors
          if (!result.isSuccess())
          {
            logger.error("Cannot create Mercury ANS message: " + result.getErrorString());
            throw new Exception("Cannot create Mercury ANS message");
          }
          return systemMessage ;
        }


        /**
         * Process a CAN message.
         *
         * @param queueDeleteHeaderVO - QueueDeleteHeaderVO
         * @param retrievedFields - HashMap that contains values retrieved from DB
         * @param messagesVO - An Array consisting of all messages from Mercury or Venus table
         * @param mosVO - MessageOrderStatusVO - contains values that shows if a cancel/reject has been sent,
         *                if an order is live, etc.
         * @param conn - Connection
         * @param userId - String
         *
         * @return n/a
         *
         * @throws Exception
         */
        private void processCANMessage(QueueDeleteHeaderVO queueDeleteHeaderVO, HashMap retrievedFields, ArrayList messagesVO, MessageOrderStatusVO mosVO, 
                                       Connection conn, String userId)
          throws Exception
        {
          String system = (String) retrievedFields.get("system");
          String newMessage = queueDeleteHeaderVO.getTextOrReason();
          String sCancelReasonCode = queueDeleteHeaderVO.getCancelReasonCode();
          ResultTO result = null;

          //Create a Calendar Object for todays date - 90 days
          Calendar cTodayMinus90 = Calendar.getInstance();
          cTodayMinus90.add(cTodayMinus90.DATE, -90);

          MercuryVO mVO = null;
          MercuryVO mFtdVO = null;
          VenusVO vVO = null;
          VenusVO vFtdVO = null;
          VendorVO vndrVO = null;

          /**
           * ERRORS that are common to both Mercury and Venus records
           **/
          //if the florist/vendor cancelled/rejected, or there was an EFOS reject
          if ((!mosVO.isHasLiveFTD() && !mosVO.isFloristRejectOrder() && !mosVO.isCancelSent()) 
              || 
              mosVO.isCancelSent() 
              || 
              mosVO.isFloristRejectOrder()
             )
            throw new Exception("Associated FTD is not Live.  Cannot send CAN");

          if (mosVO.isFTDM())
            throw new Exception("Cannot cancel an FTDM via this screen");


          /**
           * Check for errors petaining to MERCURY orders, and if everything looks ok, create a CanTO
           **/
          if (system.equalsIgnoreCase("Merc") || system.equalsIgnoreCase("Mercury"))
          {
            //get the FTD record
            for (int x = 0; x < messagesVO.size(); x++)
            {
              mVO = (MercuryVO) messagesVO.get(x);
              if (mVO.getMsgType().equalsIgnoreCase("FTD"))
              {
                mFtdVO = (MercuryVO) messagesVO.get(x);
                break;
              }
            }

            //if FTD is not found, throw an exception
            if (mFtdVO == null)
              throw new Exception("Mercury record not found.");

            //if the FTD does not have a delivery date, throw an exception
            if (mFtdVO.getDeliveryDate() == null)
              throw new Exception("Delivery date is not found on the FTD.");

            //if the FTD is older than 90 days, throw an exception
            if (mFtdVO.getDeliveryDate().before(cTodayMinus90.getTime()))
              throw new Exception("Cannot send message on FTD more 90 days after delivery date.");

            //if the delivery date is in a different month than today, it requires an ADJ.  Thus, throw an exception
            if (checkDeliveryDate(mFtdVO.getDeliveryDate()) == true)
              throw new Exception("Cannot do CAN that requires an ADJ");

            //Validation passed.  Create a Cancel Transfer Object
            CANMessageTO canTO = new CANMessageTO();
            canTO.setMercuryId(mFtdVO.getMercuryId());
            canTO.setComments(newMessage);
            canTO.setOperator(userId);
            canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            canTO.setFillingFlorist(mFtdVO.getFillingFlorist());
            canTO.setSendingFlorist(mFtdVO.getSendingFlorist());

            //send mercury cancel
            result = MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(canTO, conn);

            //check for errors
            if (!result.isSuccess())
            {
              logger.error("Cannot create Mercury CAN message: " + result.getErrorString());
              throw new Exception("Cannot create Mercury CAN message");
            }
          }

          /**
           * Check for errors petaining to VENUS orders, and if everything looks ok, create a CanTO
           **/
          else
          {
            for (int x = 0; x < messagesVO.size(); x++)
            {
              vVO = (VenusVO) messagesVO.get(x);
              if (vVO.getMsgType().equalsIgnoreCase("FTD"))
              {
                vFtdVO = (VenusVO) messagesVO.get(x);
                break;
              }
            }

            //if FTD is not found, throw an exception
            if (vFtdVO == null)
              throw new Exception("Venus record not found.");

            //if the FTD does not have a delivery date, throw an exception
            if (vFtdVO.getDeliveryDate() == null)
              throw new Exception("Delivery date is not found on the FTD.");

            //if the FTD is older than 90 days, throw an exception
            if (vFtdVO.getDeliveryDate().before(cTodayMinus90.getTime()))
              throw new Exception("Cannot send message on FTD more 90 days after delivery date.");

            //if the FTD is in SHIPPED status, it requires an ADJ.  Thus, throw an exception
            if (vFtdVO.getSdsStatus().equalsIgnoreCase(QueueDeleteConstants.SDS_STATUS_SHIPPED))
              throw new Exception("Cannot do CAN that requires an ADJ");

            //get the vendor info 
            if (vFtdVO.getVendorId() != null && !vFtdVO.getVendorId().equalsIgnoreCase(""))
            {
              QueueDeleteDAO queueDeleteDAO = new QueueDeleteDAO(conn);
              CachedResultSet crs = queueDeleteDAO.getVendorInfo(vFtdVO.getVendorId());

              while (crs.next())
              {
                vndrVO = new VendorVO();
                vndrVO.setVendorMemberNumber(crs.getString("MEMBER_NUMBER"));
                vndrVO.setVendorId(vFtdVO.getVendorId());
                vndrVO.setVendorName(crs.getString("VENDOR_NAME"));
                vndrVO.setVendorType(crs.getString("VENDOR_TYPE"));

                break;
              }
            }

            //ModifyOrderUTIL moUtil = new ModifyOrderUTIL(conn);
            Date today = new Date();

            //if FTP Vendor, throw an exception
            if (vndrVO.getVendorType() != null && !vndrVO.getVendorType().equalsIgnoreCase("") && 
                vndrVO.getVendorType().equalsIgnoreCase("FTP"))
              throw new Exception("Cannot cancel a product from an FTP type vendor");

            //if status is PRINTED and order is in Transit, throw an exception  
            if (vFtdVO.getSdsStatus().equalsIgnoreCase(QueueDeleteConstants.SDS_STATUS_PRINTED) && 
                ((vFtdVO.getZoneJumpFlag() != null && 
                  today.after(vFtdVO.getZoneJumpLabelDate())) || 
                 this.isOrderInTransit(vFtdVO.getShipDate(), mVO.getDeliveryDate())))
              throw new Exception("Order is in transit and cannot be cancelled");

            //Validation passed.  Create a Cancel Transfer Object
            CancelOrderTO canTO = new CancelOrderTO();
            canTO.setVenusId(vFtdVO.getVenusId());
            canTO.setComments(newMessage);
            canTO.setCsr(userId);
            canTO.setCancelReasonCode(sCancelReasonCode);

            //send venus cancel
            result = MessagingServiceLocator.getInstance().getVenusAPI().sendCancel(canTO, conn);

            //check for errors
            if (!result.isSuccess())
            {
              logger.error("Cannot create Venus CAN message: " + result.getErrorString());
              throw new Exception("Cannot create Venus CAN message");
            }
          }

        }


        /**
         * Process an FTD message.
         *
         * @param retrievedFields - HashMap that contains values retrieved from DB
         * @param messagesVO - An Array consisting of all messages from Mercury or Venus table
         * @param mosVO - MessageOrderStatusVO - contains values that shows if a cancel/reject has been sent,
         *                if an order is live, etc.
         * @param userId - String
         * @param conn - Database Connection
         *
         * @return n/a
         *
         * @throws Exception
         */
        private void processFTDMessage(HashMap retrievedFields, ArrayList messagesVO, MessageOrderStatusVO mosVO, String userId, Connection conn, QueueDeleteDAO queueDeleteDAO)
          throws Exception
        {
          String system = (String) retrievedFields.get("system");
          ResultTO result = null;

          //get the message status. this will show if the FTD for this order number has been Cancelled or Rejected, and if the Order has any Live FTD
          MercuryVO mVO = null;
          MercuryVO mFtdVO = null;
          VenusVO vVO = null;
          VenusVO vFtdVO = null;

          //If there is a live FTD, do not send a new FTD
          if (mosVO.isHasLiveFTD())
            throw new Exception("This order has a live FTD.  Cannot resubmit for processing.");

          /**
           * Check for errors petaining to MERCURY orders, and if everything looks ok, create a FTDTO
           **/
          if (system.equalsIgnoreCase("Merc") || system.equalsIgnoreCase("Mercury"))
          {
            //get the FTD record
            for (int x = 0; x < messagesVO.size(); x++)
            {
              mVO = (MercuryVO) messagesVO.get(x);
              if (mVO.getMsgType().equalsIgnoreCase("FTD"))
              {
                mFtdVO = (MercuryVO) messagesVO.get(x);
                break;
              }
            }

            //if FTD is not found, throw an exception
            if (mFtdVO == null)
              throw new Exception("Mercury record not found.");


            //Find the florist that can fulfill this request
            RWDFloristTO rwdFloristTO = autoSelectFlorist(retrievedFields, mFtdVO, conn);
            if (rwdFloristTO == null || rwdFloristTO.getFloristId() == null || rwdFloristTO.getFloristId().trim().length() == 0)
            {
              throw new Exception("There are no florists available to fulfill this order.");
            }

          //update the florist on the order
            String orderDetailId = (String) retrievedFields.get("orderDetailId");
            this.updateOrder(userId, orderDetailId, rwdFloristTO, queueDeleteDAO);
            
            FTDMessageTO ftdTO = new FTDMessageTO();

            if (mFtdVO.getDeliveryDate() != null)
            {
              java.sql.Date sqlDeliveryDate = new java.sql.Date(mFtdVO.getDeliveryDate().getTime());
              ftdTO.setDeliveryDate(sqlDeliveryDate);
            }
            if (mFtdVO.getOrderDate() != null)
            {
              java.sql.Date sqlOrderDate = new java.sql.Date(mFtdVO.getOrderDate().getTime());
              ftdTO.setOrderDate(sqlOrderDate);
            }
            ftdTO.setDeliveryDateText(mFtdVO.getDeliveryDateText());
            ftdTO.setAddress(mFtdVO.getAddress());
            ftdTO.setCardMessage(mFtdVO.getCardMessage());
            ftdTO.setCityStateZip(mFtdVO.getCityStateZip());
            ftdTO.setPhoneNumber(mFtdVO.getPhoneNumber());
            ftdTO.setDirection(mFtdVO.getMessageDirection());
            ftdTO.setFirstChoice(mFtdVO.getFirstChoice());
            ftdTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            ftdTO.setOldPrice(null);
            ftdTO.setRequestConfirmation(mFtdVO.getRequireConfirmation());
            ftdTO.setFillingFlorist(rwdFloristTO.getFloristId());
            ftdTO.setOccasion(mFtdVO.getOccasion());
            ftdTO.setOperator(userId);
            ftdTO.setOrderDetailId(mFtdVO.getReferenceNumber());
            ftdTO.setPhoneNumber(mFtdVO.getPhoneNumber());
            ftdTO.setPrice(mFtdVO.getPrice());
            ftdTO.setProductId(mFtdVO.getProductId());
            ftdTO.setRecipient(mFtdVO.getRecipient());
            ftdTO.setSecondChoice(mFtdVO.getSecondChoice());
            ftdTO.setSendingFlorist(mFtdVO.getSendingFlorist());
            ftdTO.setSpecialInstructions(mFtdVO.getSpecialInstructions());
            ftdTO.setZipCode(mFtdVO.getZipCode());


            //create Mercury FTD message TO.
            result = MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(ftdTO, conn);

            //check for errors
            if (!result.isSuccess())
            {
              logger.error("Cannot create Mercury FTD message: " + result.getErrorString());
              throw new Exception("Cannot create Mercury FTD message");
            }
            
            /* add floristSelectionLogId to the mercury table */
            String floristSelectionLogId = rwdFloristTO.getFloristSelectionLogId();
            logger.info("floristSelectionLogId: " + floristSelectionLogId);
            logger.info("key: " + result.getKey());
            if(floristSelectionLogId != null && result.getKey() != null)
            {
            	QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
            	qdDAO.updateFloristSelectionLogId(result.getKey(), floristSelectionLogId);
            }
          }

          /**
           * Check for errors petaining to VENUS orders, and if everything looks ok, create a CanTO
           **/
          else
          {
            for (int x = 0; x < messagesVO.size(); x++)
            {
              vVO = (VenusVO) messagesVO.get(x);
              if (vVO.getMsgType().equalsIgnoreCase("FTD"))
              {
                vFtdVO = (VenusVO) messagesVO.get(x);
                break;
              }
            }
            OrderTO ftdTO = new OrderTO();

            ftdTO.setReferenceNumber(vFtdVO.getReferenceNumber());
            ftdTO.setAddress1(vFtdVO.getAddress1());
            ftdTO.setAddress2(vFtdVO.getAddress2());
            ftdTO.setBusinessName(vFtdVO.getBusinessName());
            ftdTO.setCardMessage(vFtdVO.getCardMessage());
            ftdTO.setCity(vFtdVO.getCity());
            ftdTO.setDeliveryDate(vFtdVO.getDeliveryDate());
            ftdTO.setFillingVendor(vFtdVO.getFillingVendor());
            ftdTO.setMessageType("FTD");
            ftdTO.setOperator(userId);
            ftdTO.setOrderDate(vFtdVO.getOrderDate());
            ftdTO.setOverUnderCharge(new Double(0.0));
            ftdTO.setPhoneNumber(vFtdVO.getPhoneNumber());
            ftdTO.setPrice(vFtdVO.getPrice());
            ftdTO.setProductId(vFtdVO.getProductId());
            ftdTO.setRecipient(vFtdVO.getRecipient());
            ftdTO.setState(vFtdVO.getState());
            ftdTO.setShipMethod(vFtdVO.getShipMethod());
            ftdTO.setShipDate(vFtdVO.getShipDate());
            ftdTO.setVenusOrderNumber(vFtdVO.getVenusOrderNumber());
            ftdTO.setZipCode(vFtdVO.getZip());
            ftdTO.setCountry(vFtdVO.getCountry());
            ftdTO.setFromCommunicationScreen(false);
            ftdTO.setCompOrder(null);

            result = MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(ftdTO, conn);

            //check for errors
            if (!result.isSuccess())
            {
              logger.error("Cannot create Venus FTD message: " + result.getErrorString());
              throw new Exception("Cannot create Venus FTD message");
            }
          }

        }


        /**
         * Process an FTD message.  This method is called if there is no FTD record.
         *
         * @param retrievedFields - HashMap that contains values retrieved from DB
         * @param conn - Database Connection
         *
         * @return n/a
         *
         * @throws Exception
         */
        private void processFTDMessageByOrdDetInfo(HashMap retrievedFields, QueueDeleteDAO queueDeleteDAO, String userId, Connection conn)
          throws Exception
        {
          String system = (String) retrievedFields.get("system");
          String orderDetailId = (String) retrievedFields.get("orderDetailId");
          ResultTO result = null;
          RWDFloristTO rwdTO = null;


          if (system.equalsIgnoreCase("Merc") || system.equalsIgnoreCase("Mercury"))
          {
            //auto select florist by order detail id
            rwdTO = this.autoSelectFloristByOrderDetailId(orderDetailId, conn); 

            //check for errors
            if (rwdTO != null &&
                rwdTO.getFloristId() != null    &&
                rwdTO.getFloristId().trim().length()>0
               )
            {
              //update the florist on the order
              this.updateOrder(userId, orderDetailId, rwdTO, queueDeleteDAO);

              //create OrderDetailKeyTO 
              com.ftd.op.mercury.to.OrderDetailKeyTO mercuryOrderDetailTo = new com.ftd.op.mercury.to.OrderDetailKeyTO();
        
              //set the order detail id
              mercuryOrderDetailTo.setOrderDetailId(orderDetailId);
        
              //create Mercury FTD message by passing the OrderDetailKeyTO.
              result = MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(mercuryOrderDetailTo, rwdTO.getFloristId(), conn);

              //check for errors
              if (!result.isSuccess())
              {
                logger.error("Cannot create Mercury FTD message: " + result.getErrorString());
                throw new Exception("Cannot create Mercury FTD message");
              }
            }
            else
            {
              logger.error("No florist found - florists may be blocked, not available, or the product may be codified");
              throw new Exception("No florist found - florists may be blocked, not available, or the product may be codified");
            }

          }

          /**
           * Check for errors petaining to VENUS orders, and if everything looks ok, create an FTD
           **/
          else
          {
            com.ftd.op.venus.to.OrderDetailKeyTO venusOrderDetailTo = new com.ftd.op.venus.to.OrderDetailKeyTO();

            //set the order detail id
            venusOrderDetailTo.setOrderDetailId(orderDetailId);

            result = MessagingServiceLocator.getInstance().getVenusAPI().sendOrder(venusOrderDetailTo, conn);

            //check for errors
            if (!result.isSuccess())
            {
              logger.error("Cannot create Venus FTD message: " + result.getErrorString());
              throw new Exception("Cannot create Venus FTD message");
            }
          }

        }


        /**
         * Call Random Weighted Distribution to retrieve the florist id, and pass the RWD Transfer Object back
         *
         * @param retrievedFields - HashMap that contains all the data to be passed
         * @param mFtdVO - MercuryVO
         * @param conn - Database Connection
         * @return RWDFloristTO
         */
        private RWDFloristTO autoSelectFlorist(HashMap retrievedFields, MercuryVO mFtdVO, Connection conn)
        {
          //Find the florist that can fulfill this request
          RWDTO rwdTO = new RWDTO();
          RWDFloristTO rwdFloristTO = new RWDFloristTO();

          try
          {
            String orderDetailId = (String) retrievedFields.get("orderDetailId");
            String recipientCity = (String) retrievedFields.get("recipientCity");
            String recipientState = (String) retrievedFields.get("recipientState");
            String recipientZipCode = (String) retrievedFields.get("recipientZipCode");
            String productId = (String) retrievedFields.get("productId");
            String sourceCode = (String) retrievedFields.get("sourceCode");

            rwdTO.setOrderDetailId(orderDetailId);
            rwdTO.setDeliveryCity(recipientCity);
            rwdTO.setDeliveryState(recipientState);
            rwdTO.setZipCode(recipientZipCode.toUpperCase());
            rwdTO.setReturnAll(false);
            rwdTO.setOrderValue(mFtdVO.getPrice().doubleValue());
            rwdTO.setProduct(productId);
            rwdTO.setDeliveryDate(mFtdVO.getDeliveryDate());
            rwdTO.setSourceCode(sourceCode);
            rwdTO.setDeliveryDateEnd(null);
            rwdTO.setRWDFlagOverride(Boolean.TRUE);
            rwdFloristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFlorist(rwdTO, conn);
            logger.info("rwdFloristTO : zipCityFlag: " + rwdFloristTO.getZipCityFlag());
          
          }
          catch (Exception e)
          {
            logger.error(e);
          }

          return rwdFloristTO;

        }
  
    /**
     * Returns true if order is in printed or shipped status and is on or after 
     * the ship date but before the delivery date
     * @param shipDate
     * @param deliveryDate
     * @return boolean
     * @throws Exception
     */
     public boolean isOrderInTransit(Date shipDate, Date deliveryDate)
        throws Exception 
    {
        // if ship date is null, assume it's a florist order and return false
        if (shipDate == null){
            return false;
        }
        
        Date today = new Date();

        if(today.after(shipDate) &&  today.before(deliveryDate))
        {
            return true;
        }
      return false;
    }
 
    /**
     * Check if the current date is greater than the last day of the delivery date month
     *
     * @param deliveryDate - Date
     *
     * @return boolean - true if the current date is after the adjustment date (calculated based off of Delivery Date)
     *
     * @throws Exception
     */
    private boolean checkDeliveryDate(Date deliveryDate)
      throws Exception
    {
      if (logger.isDebugEnabled())
      {
        logger.debug("Entering checkDeliveryDate");
        logger.debug("deliveryDate: " + deliveryDate);
      }

      boolean dateComparision = false;

      try
      {
        Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
        Date currentDate = calCurrent.getTime();

        if (currentDate.after(deliveryDate))
        {
          Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
          calDelivery.setTime(deliveryDate);

          int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

          Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
          calAdjustment.setTime(deliveryDate);
          calAdjustment.set(calDelivery.get(Calendar.YEAR), calDelivery.get(Calendar.MONTH), day);

          Date adjustmentDate = calAdjustment.getTime();

          if (currentDate.after(adjustmentDate))
          {
            dateComparision = true;
          }
        }
      }
      finally
      {
        if (logger.isDebugEnabled())
        {
          logger.debug("Exiting checkDeliveryDate");
        }
      }

      return dateComparision;
    }
    
    /**
     * Call Random Weighted Distribution to retrieve the florist id, and pass the RWD Transfer Object back
     *
     * @param orderDetailId - order detail id
     * @param conn - Database Connection
     * @return RWDFloristTO
     */
    private RWDFloristTO autoSelectFloristByOrderDetailId(String orderDetailId, Connection conn)
      throws Exception
    {
      //create OrderTO 
      com.ftd.op.order.to.OrderTO orderTO = new com.ftd.op.order.to.OrderTO();

      //set the order detail id
      orderTO.setOrderDetailId(orderDetailId);
      orderTO.setRWDFlagOverride(Boolean.TRUE);
      //Call florist selection API to get the list of florists 
      RWDFloristTO rwdTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFlorist(orderTO, conn);
      logger.info("zipCityflag: " + rwdTO.getZipCityFlag());
      return rwdTO; 
    }
    
    /**
     * Update the florist on the order detail record
     *
     * @param csrId - user id
     * @param orderDetailId - order detail id
     * @param fillingFlorist - filling florist
     * @param msgDAO - message dao
     * 
     * @return none
     */
    private void updateOrder(String csrId, String orderDetailId, RWDFloristTO rwdTO, 
                             QueueDeleteDAO queueDeleteDAO)
      throws Exception
    {
        String orderDispostion = "Processed";
        queueDeleteDAO.modifyOrder(orderDetailId, rwdTO.getFloristId().toUpperCase(), csrId, orderDispostion);
        queueDeleteDAO.orderFloristUsed(orderDetailId, rwdTO.getFloristId(), rwdTO.getZipCityFlag());
    }
    

    /**
     * Create a comment
     *
     * @param commentText - String containing the comment text
     * @param retrievedFields - HashMap that contains values retrieved from DB
     * @param conn - Connection
     * @param orderDetailId - String containing order detail id
     * @param userId - String
     *
     * @return n/a
     *
     * @throws Exception
     */
    private void processComment(String commentText, HashMap retrievedFields, Connection conn, String orderDetailId, 
                                String userId)
      throws Exception
    {
      // Submit (save) comment sub-action
      //get all the comment parameters in the request object and build CommentVO object
      CommentsVO commentObj = new CommentsVO();
      String orderGuid = (String) retrievedFields.get("orderGuid");
      String customerId = (String) retrievedFields.get("customerId");

      commentObj.setComment(commentText);
      commentObj.setCommentType(QueueDeleteConstants.ORDER_COMMENT_TYPE);
      commentObj.setOrderDetailId(orderDetailId);

      if (userId != null)
      {
        commentObj.setCreatedBy(userId);
        commentObj.setUpdatedBy(userId);
      }

      commentObj.setCommentOrigin(QueueDeleteConstants.START_ORIGIN_GEN);
      commentObj.setReason("queue message delete");
      commentObj.setOrderGuid(orderGuid);
      commentObj.setCustomerId(customerId);

      // Note that there is intentionally no lock check for this operation.
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
      qdDAO.insertComment(commentObj);

    }
    
    /**
     * Send a Mercury/Venus message - This is a generic method which will do common checks, and will then call specific methods to
     * send an ANS or a CAN or an FTD.
     *
     * NOTE: A user can ONLY send a message if they have entered the queue id; the GUI will stop the user from choosing this option
     * if they have entered an external order number.
     *
     * @param queueDeleteHeaderVO - QueueDeleteHeaderVO
     * @param retrievedFields - HashMap that contains values retrieved from DB
     * @param sSendMessageType - Message type, like ANS, CAN, FTD etc
     * @param conn - Connection
     * @param userId - String
     *
     * @return n/a
     *
     * @throws Exception
     */
    private String processMessage(QueueDeleteHeaderVO queueDeleteHeaderVO, HashMap retrievedFields, String sSendMessageType, Connection conn, String userId, String systemMessage )
      throws Exception
    {
      String system = (String) retrievedFields.get("system");
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
      
      //get the message status. this will show if the FTD for this order number has been Cancelled or Rejected, and if the Order has any Live FTD
      MessageOrderStatusVO mosVO = (MessageOrderStatusVO) retrievedFields.get("mosVO");
      ArrayList messagesVO = (ArrayList) retrievedFields.get("messagesVO");

      //If the user is trying to send an ANS message where the queue-id entered is not an ASK, throw an exception
      if (sSendMessageType.equalsIgnoreCase("ANS"))
      {
        if (mosVO == null || messagesVO == null)
          throw new Exception("Mercury/Venus order number not found.  Answer message cannot be sent");
        systemMessage = processANSMessage(queueDeleteHeaderVO, retrievedFields, messagesVO, userId, conn, systemMessage);
      }
      //If the user is trying to send an ANS message where the queue-id entered is not an ASK, throw an exception
      else if (sSendMessageType.equalsIgnoreCase("CAN"))
      {
        if (mosVO == null || messagesVO == null)
          throw new Exception("Mercury/Venus order number not found.  Cancel message cannot be sent");
        processCANMessage(queueDeleteHeaderVO, retrievedFields, messagesVO, mosVO, conn, userId);
        //since a CAN has been successfully sent, switch the flags 
        mosVO.setCancelSent(true);
        mosVO.setHasLiveFTD(false);
      }
      //If the user is trying to send an ANS message where the queue-id entered is not an ASK, throw an exception
      else if (sSendMessageType.equalsIgnoreCase("FTD"))
      {
        //for some queues (example ZIP), we don't have the mercury/venus id or order number in Clean.Queue.  Hence, we never retrieve the mercury/venus info. 
        //However, these orders may still have FTDs out there such that the info on the last FTD is different than the order detail
        //In cases like these, try retrieving the last FTD before using info from order detail to create a new FTD
        if (mosVO == null || messagesVO == null)
        {
          ArrayList ftdsVO = null;
          Date createdOnOfLastFtd = null;
          Date tempCreatedOn = null;

          //Before taking the info from the order, as a last resort, we should see if there ever existed an FTD on the order.  If so, retrieve the last FTD
          if (system.equalsIgnoreCase("Merc") || system.equalsIgnoreCase("Mercury"))
          {
            MercuryVO mFtdVO = null;

            //retrieve all the FTDs
            ftdsVO = qdDAO.getAllMercuryOrVenusMessages(((String) retrievedFields.get("orderDetailId")), null, null, null, "'FTD'", "Mercury", "ORD");

            if (ftdsVO.size() > 0)
            {
              messagesVO = new ArrayList();

              //go thru the FTD records, retrieve the last FTD and add it to the messagesVO
              for (int x = 0; x < ftdsVO.size(); x++)
              {
                mFtdVO = (MercuryVO) ftdsVO.get(x);
                tempCreatedOn = mFtdVO.getCreatedOn();
                if (createdOnOfLastFtd == null || tempCreatedOn.after(createdOnOfLastFtd))
                {
                  messagesVO.clear();
                  messagesVO.add(mFtdVO);
                  createdOnOfLastFtd = mFtdVO.getCreatedOn();
                }
              }

              mosVO = qdDAO.getMessageOrderStatus(((MercuryVO) messagesVO.get(0)).getMercuryOrderNumber(), (String) retrievedFields.get("orderDetailId"), "Mercury");
            }
          }
          else
          {
            VenusVO vFtdVO = null;

            //retrieve all the FTDs
            ftdsVO = qdDAO.getAllMercuryOrVenusMessages(((String) retrievedFields.get("orderDetailId")), null, null, null, "'FTD'", "Venus", "ORD");

            if (ftdsVO.size() > 0)
            {
              messagesVO = new ArrayList();

              //go thru the FTD records, retrieve the last FTD and add it to the messagesVO
              for (int x = 0; x < ftdsVO.size(); x++)
              {
                vFtdVO = (VenusVO) ftdsVO.get(x);
                tempCreatedOn = vFtdVO.getCreatedOn();
                if (createdOnOfLastFtd == null || tempCreatedOn.after(createdOnOfLastFtd))
                {
                  messagesVO.clear();
                  messagesVO.add(vFtdVO);
                  createdOnOfLastFtd = vFtdVO.getCreatedOn();
                }
              }

              mosVO = qdDAO.getMessageOrderStatus(((VenusVO) messagesVO.get(0)).getVenusOrderNumber(), (String) retrievedFields.get("orderDetailId"), "Venus");
            }
          }

          //if at least one FTD is found, send a new message using the info from the last FTD. 
          if (messagesVO != null && messagesVO.size() > 0)
            processFTDMessage(retrievedFields, messagesVO, mosVO, userId, conn, qdDAO);
          //else resubmit an FTD using info from the original order
          else
            processFTDMessageByOrdDetInfo(retrievedFields, qdDAO, userId, conn);
        }
        else
          processFTDMessage(retrievedFields, messagesVO, mosVO, userId, conn, qdDAO);
      }
      return systemMessage ;
    }
    
    private void sendCustomerEmail(QueueDeleteHeaderVO queueDeleteHeaderVO, HashMap retrievedFields, Connection conn, String orderDetailId, String userId)
      throws Exception
    {
      String OUTGOING_MESSAGE = "O";

      // Obtain the customer email address to send the message.
      String orderEmailAddress = null;
      String customerEmailAddress = (String) retrievedFields.get("customerEmailAddress");
      String customerAltEmailAddress = (String) retrievedFields.get("customerAltEmailAddress");

      if (customerEmailAddress != null && !customerEmailAddress.equalsIgnoreCase(""))
        orderEmailAddress = customerEmailAddress;
      else
        orderEmailAddress = customerAltEmailAddress;


      if (orderEmailAddress != null)
      {

        String customerId = (String) retrievedFields.get("customerId");
        String orderGuid = (String) retrievedFields.get("orderGuid");
        String extOrderNum = (String) retrievedFields.get("externalOrderNumber");
        String masterOrderNum = (String) retrievedFields.get("masterOrderNumber");
        String customerFirstname = (String) retrievedFields.get("customerFirstName");
        String customerLastname = (String) retrievedFields.get("customerLastName");
        String companyId = (String) retrievedFields.get("companyId");
        String originId = (String) retrievedFields.get("originId");
        String customerPhone = (String) retrievedFields.get("customerPhoneNumber");
        
        // scrub the origin id against config file origins list
        if (originId != null)
        {
          String originList = 
            ConfigurationUtil.getInstance().getProperty(QueueDeleteConstants.CONFIG_FILE, QueueDeleteConstants.EMAIL_ORIGINS);
          if (originList.indexOf(originId) < 0)
          {
            originId = null;
          }
        }

        // With Customer info complete, compose the Order info.
        // Retrieve order data using OrderDAO to be used for the token replacement
        Document dataDocXML = this.getOrderDataDocumentXML(orderDetailId, "Y", conn);

        // get csr name and append to dataDocXML
        QueueDeleteDAO queueDeleteDAO = new QueueDeleteDAO(conn);
        CachedResultSet rs = queueDeleteDAO.getQueueDeleteUserInfo(userId);
        String firstName = "";
        String lastName = "";
        if (rs.next())
        {
            firstName = rs.getString("first_name");
            lastName =  rs.getString("last_name");
        }
        Document csrXML = 
          this.toXML("<CSR><first_name>" + firstName + "</first_name><last_name>" + lastName + 
                     "</last_name></CSR>");
        DOMUtil.addSection(dataDocXML, csrXML.getChildNodes());
       

        // Create a PointOfContactVO using pocType, customerId, guid, orderDetailId, companyId and body from request object
        PointOfContactVO pocObj = new PointOfContactVO();
        pocObj.setPointOfContactType(QueueDeleteConstants.EMAIL_MESSAGE_TYPE);

        pocObj.setDataDocument(dataDocXML);

        long customerIdNum = 0;
        if (customerId != null)
          customerIdNum = Long.parseLong(customerId);

        long orderDetailIdNum = 0;
        if (orderDetailId != null && !orderDetailId.equals(""))
          orderDetailIdNum = Long.parseLong(orderDetailId);

        pocObj.setCustomerId(customerIdNum);
        pocObj.setOrderGuid(orderGuid);
        pocObj.setOrderDetailId(orderDetailIdNum);
        pocObj.setCompanyId(companyId);
        pocObj.setMasterOrderNumber(masterOrderNum);
        pocObj.setExternalOrderNumber(extOrderNum);
        pocObj.setFirstName(customerFirstname);
        pocObj.setLastName(customerLastname);
        pocObj.setDaytimePhoneNumber(customerPhone);
        pocObj.setSentReceivedIndicator(OUTGOING_MESSAGE);
        pocObj.setBody(queueDeleteHeaderVO.getEmailBody());
        pocObj.setCommentType(QueueDeleteConstants.ORDER_COMMENT_TYPE);
        pocObj.setOriginId(originId);

        // These props are intentionally set to NULL so that the StockMessageGenerator (used later) will replace tokens within the custom-message getBody() value.
        pocObj.setTemplateId(null);
        pocObj.setLetterTitle(null);

        if (userId != null)
        {
          pocObj.setCreatedBy(userId);
          pocObj.setUpdatedBy(userId);
        }

        // use company_id to pull SenderEmailAddress from db
        MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(conn);
        String companyEmailAddress = msgGenDAO.loadSenderEmailAddress(null, companyId,orderDetailIdNum);

        pocObj.setSenderEmailAddress(companyEmailAddress);
        pocObj.setRecipientEmailAddress(orderEmailAddress);
        pocObj.setEmailSubject(queueDeleteHeaderVO.getEmailSubject());
        
        //        }

        // 1. Compose the final message be replacing tokens within the message with actual customer and order values.
        StockMessageGenerator msgGen = new StockMessageGenerator(conn);
        
        // Changes for ET      
        ScrubMapperDAO scrubMapperDao = new ScrubMapperDAO(connection);
		OrderVO orderVo = null;
		try {
			// fetching orderVo from database for populating record attribute xml
			orderVo = scrubMapperDao.mapOrderFromDB(orderGuid);
		} catch (Exception e) {
			logger.error("Exception occured while fetching orderVo for orderGuid: "	+ orderGuid, e);
		}
		if (orderVo != null) {
			String recordAttributesXML = msgGen.generateRecordAttributeXML(orderVo);
			// populating record attribute xml
			pocObj.setRecordAttributesXML(recordAttributesXML);
		} else {
			logger.error("Null OrderVO returned for order guid: " + orderGuid + " Not populating record attribute xml");
		}
		
		// populating source code and email type
		pocObj.setSourceCode(orderVo.getSourceCode());
        pocObj.setEmailType(MessageConstants.COM_EMAIL_TYPE);     
        
        pocObj = msgGen.buildMessage(pocObj);

        // 2. Insert the email into the db table by passing the newly created POC object
        pocObj = msgGen.insertMessage(pocObj);

        // 3. Send the email by calling sendMessage() of StockMessageGenerator
        pocObj = msgGen.sendMessage(pocObj);

        logger.debug("sendCustomerEmail: Order Detail Id " + orderDetailId + " email sent to " + 
                     pocObj.getRecipientEmailAddress());
              
        //if the order detail id exists
        if(orderDetailId != null && !orderDetailId.equals("")){

          QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
          
          //grab the message id
          String messageId = (String) retrievedFields.get(QueueDeleteConstants.MESSAGE_ID);

          //if it's a DCON email
          if(qdDAO.isDCONEmail(messageId))
          {   //update the order details record to confirmed
              qdDAO.updateDCONStatus(orderDetailId, GeneralConstants.DCON_CONFIRMED, userId);
          }
        }

        // Call insertComment() on OrderDAO passing the specified comment attached to the current order and customer
        CommentsVO commentObj = new CommentsVO();
        String messageTitle = queueDeleteHeaderVO.getEmailTitle();
        String commentText = 
          messageTitle == null? "Custom email sent to customer as part of queue message delete": "Email Title: " + messageTitle;
        commentObj.setComment(commentText);
        commentObj.setCommentType(QueueDeleteConstants.ORDER_COMMENT_TYPE);
        commentObj.setOrderDetailId(orderDetailId);
        commentObj.setOrderGuid(orderGuid);
        commentObj.setCustomerId(customerId);

        if (userId != null)
        {
          commentObj.setCreatedBy(userId);
          commentObj.setUpdatedBy(userId);
        }

        commentObj.setCommentOrigin(QueueDeleteConstants.START_ORIGIN_GEN);
        commentObj.setReason("email");

        // Append a comment to document the order email just sent.  Note that there is intentionally no lock check for this operation.
        QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
        qdDAO.insertComment(commentObj);
      }
      else
      {
        throw new Exception("sendCustomerEmail: Customer Info missing from DB for Order Detail Id " + orderDetailId);
      }
    }
    
    /**
     * Send a Mercury/Venus message - This is a generic method which will do common checks, and will then call specific methods to
     * send an ANS or a CAN or an FTD.
     *
     * NOTE: A user can ONLY send a message if they have entered the queue id; the GUI will stop the user from choosing this option
     * if they have entered an external order number.
     *
     * @param retrievedFields - HashMap that contains values retrieved from DB
     * @param conn - Connection
     *
     * @return n/a
     *
     * @throws Exception
     */
    private void getMercuryVenusInfo(HashMap retrievedFields, Connection conn)
      throws Exception
    {
      String system = (String) retrievedFields.get("system");
      String mercuryOrderNumber = (String) retrievedFields.get("mercuryOrderNumber");
      String venusOrderNumber = (String) retrievedFields.get("venusOrderNumber");
      String orderDetailId = (String) retrievedFields.get("orderDetailId");
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);

      //get the message status. this will show if the FTD for this order number has been Cancelled or Rejected, and if the Order has any Live FTD
      MessageOrderStatusVO mosVO = null;
      ArrayList messagesVO = null;

      logger.info("MQD - processMessage - mercuryOrderNumber = " + mercuryOrderNumber + " and venusOrderNumber = " + venusOrderNumber);
      if ((mercuryOrderNumber != null && !mercuryOrderNumber.equalsIgnoreCase("")) || 
          (venusOrderNumber != null && !venusOrderNumber.equalsIgnoreCase(""))
         )
      {
        if (system.equalsIgnoreCase("Merc") || system.equalsIgnoreCase("Mercury"))
        {
          mosVO = qdDAO.getMessageOrderStatus(mercuryOrderNumber, orderDetailId, "Mercury");
          messagesVO = qdDAO.getAllMercuryOrVenusMessages(null, null, null, mercuryOrderNumber, null, "Mercury", "MSG");
        }
        else
        {
          mosVO = qdDAO.getMessageOrderStatus(venusOrderNumber, orderDetailId, "Venus");
          messagesVO = qdDAO.getAllMercuryOrVenusMessages(null, null, null, venusOrderNumber, null, "Venus", "MSG");
        }
      }

      retrievedFields.put("mosVO", mosVO);
      retrievedFields.put("messagesVO", messagesVO);

    }
    
    /**
     * Retrieve the customer info based on the order detail id
     *
     * @param orderDetailId - order detail id for which the customer info will be retrieved
     * @param retrievedFields - HashMap that contains all the data to be passed
     * @param conn - Connection
     *
     * @return n/a
     *
     * @throws Exception
     */
    private void getCustomerOrderInfo(String orderDetailId, HashMap retrievedFields, Connection conn)
      throws Exception
    {
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
      CachedResultSet rs = qdDAO.getOrderCustomerInfo(orderDetailId);

      if (rs.next())
      {
        retrievedFields.put("orderGuid", (String) rs.getString("order_guid"));
        retrievedFields.put("masterOrderNumber", (String) rs.getString("master_order_number"));
        if (rs.getDate("delivery_date") != null)
        {
          retrievedFields.put("orderDeliveryDate", sdf.format(rs.getDate("delivery_date")));
        }
        retrievedFields.put("customerId", (String) rs.getString("customer_id"));
        retrievedFields.put("customerEmailAddress", (String) rs.getString("email_address"));
        retrievedFields.put("customerFirstName", (String) rs.getString("first_name"));
        retrievedFields.put("customerLastName", (String) rs.getString("last_name"));
        retrievedFields.put("companyId", (String) rs.getString("company_id"));
        retrievedFields.put("orderDispCode", (String) rs.getString("order_disp_code"));
        retrievedFields.put("originId", (String) rs.getString("origin_id"));
        retrievedFields.put("customerPhoneNumber", (String) rs.getString("customer_phone_number"));
        retrievedFields.put("customerExtension", (String) rs.getString("customer_extension"));
        retrievedFields.put("customerAltEmailAddress", (String) rs.getString("alt_email_address"));

      }

    }
    
    /**
     * Retrieve the recipient info based on the order detail id
     *
     * @param orderDetailId - order detail id for which the customer info will be retrieved
     * @param retrievedFields - HashMap that contains all the data to be passed
     * @param conn - Connection
     *
     * @return n/a
     *
     * @throws Exception
     */
    private void getRecipientOrderInfo(String orderDetailId, HashMap retrievedFields, Connection conn)
      throws Exception
    {
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
      CachedResultSet rs = qdDAO.getOrderRecipientInfo(orderDetailId);

      if (rs.next())
      {
        retrievedFields.put("recipientFirstName", (String) rs.getString("first_name"));
        retrievedFields.put("recipientLastName", (String) rs.getString("last_name"));
        retrievedFields.put("recipientAddress1", (String) rs.getString("address_1"));
        retrievedFields.put("recipientAddress2", (String) rs.getString("address_2"));
        retrievedFields.put("recipientCity", (String) rs.getString("city"));
        retrievedFields.put("recipientState", (String) rs.getString("state"));
        retrievedFields.put("recipientZipCode", (String) rs.getString("zip_code"));
        retrievedFields.put("recipientCountry", (String) rs.getString("country"));
        retrievedFields.put("recipientBusinessName", (String) rs.getString("business_name"));
        retrievedFields.put("productId", (String) rs.getString("product_id"));
        retrievedFields.put("productName", (String) rs.getString("product_name"));
        retrievedFields.put("cardMessage", (String) rs.getString("card_message"));
        retrievedFields.put("internationalFlag", (String) rs.getString("international_flag"));
        retrievedFields.put("recipientCountryName", (String) rs.getString("country_name"));
        retrievedFields.put("sourceCode", (String) rs.getString("source_code"));

      }

    }
    
    /**
     * Retrieve the order info based on the external order number
     *
     * @param externalOrderNumber - external order number for which, we have to find the order detail id
     * @param conn - Connection
     *
     * @return n/a
     *
     * @throws Exception
     */
    private String getOrderDetailInfo(String externalOrderNumber, Connection conn)
      throws Exception
    {
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);
      HashMap orderMap = qdDAO.findOrderNumber(externalOrderNumber);

      String orderDetailId = null; 
    
      if (orderMap != null && orderMap.get("OUT_ORDER_DETAIL_ID") != null )
        orderDetailId = orderMap.get("OUT_ORDER_DETAIL_ID").toString();
      
      return orderDetailId; 
    }

    private Document toXML(String xmlString)
      throws Exception
    {
      return DOMUtil.getDocument(xmlString);
    }
    
    protected Document getOrderDataDocumentXML(String orderDetailId, String includeComments, Connection conn)
      throws Exception
    {
      //Instantiate OrderDAO
      QueueDeleteDAO qdDAO = new QueueDeleteDAO(conn);

      // pull base document
      Document primaryOrderXML = (Document) DOMUtil.getDocument();

      if (orderDetailId != null && !orderDetailId.equals(""))
      {
        //hashmap that will contain the output from the stored proc
        HashMap printOrderInfo = new HashMap();

        //Call getCartInfo method in the DAO
        printOrderInfo = 
            qdDAO.getOrderInfoForPrint(orderDetailId, includeComments, QueueDeleteConstants.CONS_PROCESSING_ID_CUSTOMER);

        //Create an XMLDocument and call a method that will transform the CachedResultSet from the cursor:
        //OUT_ORDERS_CUR into an master XMLDocument. 
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDERS_CUR", "ORDERS", "ORDER").getChildNodes());

        //OUT_ORDER_ADDON_CUR into an XMLDocument.  Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "ORDER_ADDONS", "ORDER_ADDON").getChildNodes());

        //OUT_ORDER_BILLS_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "ORDER_BILLS", "ORDER_BILL").getChildNodes());

        //OUT_ORDER_REFUNDS_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "ORDER_REFUNDS", "ORDER_REFUND").getChildNodes());

        //OUT_ORDER_TOTAL_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "ORDER_TOTALS", "ORDER_TOTAL").getChildNodes());

        //OUT_ORDER_PAYMENT_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "ORDER_PAYMENTS", "ORDER_PAYMENT").getChildNodes());

        //CUST_CART_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "CUST_CART_CUR", "CUSTOMERS", "CUSTOMER").getChildNodes());

        //OUT_CUSTOMER_PHONE_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "CUSTOMER_PHONES", "CUSTOMER_PHONE").getChildNodes());

        //OUT_MEMBERSHIP_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "MEMBERSHIPS", "MEMBERSHIP").getChildNodes());

        //OUT_TAX_REFUND_CUR into an XMLDocument. Append this a master XML document
        DOMUtil.addSection(primaryOrderXML, 
                           buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "TAX_REFUNDS", "TAX_REFUND").getChildNodes());

        // append the current date to the document
        Date rightNow = new Date();
        String dateNow = DateFormat.getDateInstance(DateFormat.SHORT).format(rightNow);
        Document dateXML = this.toXML("<cur_date>" + dateNow + "</cur_date>");
        DOMUtil.addSection(primaryOrderXML, dateXML.getChildNodes());
        //primaryOrderXML.print(System.out);
      }

      return primaryOrderXML;
    }
    
    /*******************************************************************************************
     * buildXML()
     *******************************************************************************************/
    private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
      throws Exception
    {

      //Create a CachedResultSet object using the cursor name that was passed. 
      CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);
      Document doc = null;

      try
      {
        //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
        //can be/is called by various other methods, the top and botton names MUST be passed to it. 
        XMLFormat xmlFormat = new XMLFormat();
        xmlFormat.setAttribute("type", "element");
        xmlFormat.setAttribute("top", topName);
        xmlFormat.setAttribute("bottom", bottomName);

        //call the DOMUtil's converToXMLDOM method
        doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);
      }
      finally
      {
      }

      //return the xml document. 
      return doc;
    }
    
    /**
       * Delete queue records based on order detail id and queue types
       * @param orderDetailId - String
       * @param deleteOtherQueuesList - String - all the queue types selected
       * @return CachedResultSet
       * @throws Exception
       */
    public void deleteAllQsByOrdDetId(String orderDetailId, List deleteOtherQueuesList, String userId)
      throws Exception
    {
      QueueDeleteDAO queueDAO = new QueueDeleteDAO(this.connection);

      String queueTypesSelected = "";
      for (int i = 0; i < deleteOtherQueuesList.size(); i++)
      {
        queueTypesSelected += "'" + (String) deleteOtherQueuesList.get(i) + "',";
      }

      //if queue types were selected, remove the last ,
      if (queueTypesSelected.length() > 0)
        queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
      //else initialize to null 
      else
        queueTypesSelected = null;

      queueDAO.deleteAllQsByOrdDetId(orderDetailId, queueTypesSelected, userId);
    }
    
    /**
       * Delete queue records based on external order number and queue types
       * @param externalOrderNumber - String
       * @param otherQueuesList - String - all the queue types selected
       * @return CachedResultSet
       * @throws Exception
       */
    public void deleteAllQsByExtOrdNum(String externalOrderNumber, List otherQueuesList, String userId)
      throws Exception
    {
      QueueDeleteDAO queueDeleteDAO = new QueueDeleteDAO(this.connection);

      String queueTypesSelected = "";
      for (int i = 0; i < otherQueuesList.size(); i++)
      {
          queueTypesSelected += "'" + (String) otherQueuesList.get(i) + "',";
      }

      //if queue types were selected, remove the last ,
      if (queueTypesSelected.length() > 0)
        queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
      //else initialize to null 
      else
        queueTypesSelected = null;

      queueDeleteDAO.deleteAllQsByExtOrdNum(externalOrderNumber, queueTypesSelected, userId);
    }

    
    private boolean isPartnerExcluded(String partnerName, List<String> excludedPartners) {
          if(excludedPartners != null && partnerName != null) {
              return excludedPartners.contains(partnerName);
          }
          return false;
    }
    
    /**
       * Get queue info based on the external order number
       * @param externalOrderNumber - String
       * @param otherQueuesList - String - all the queue types selected
       * @return CachedResultSet
       * @throws Exception
       */
    public CachedResultSet getQueueMessageByExternalOrderNumber(String externalOrderNumber, List otherQueuesList)
      throws Exception
    {
      QueueDeleteDAO queueDeleteDAO = new QueueDeleteDAO(this.connection);

        String queueTypesSelected = "";
        for (int i = 0; i < otherQueuesList.size(); i++)
        {
            queueTypesSelected += "'" + (String) otherQueuesList.get(i) + "',";
        }

        //if queue types were selected, remove the last ,
        if (queueTypesSelected.length() > 0)
          queueTypesSelected = queueTypesSelected.substring(0, queueTypesSelected.length() - 1);
        //else initialize to null 
        else
          queueTypesSelected = null;
        return queueDeleteDAO.getQueueMsgByExtOrdNum(externalOrderNumber, queueTypesSelected);
    //  return queueDeleteDAO.getQueueMessageByExternalOrderNumber(externalOrderNumber, queueTypesSelected);
    }


    
}   