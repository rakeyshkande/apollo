package com.ftd.qd.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;

import com.ftd.qd.vo.*;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.QueueDeleteDetailVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;

import com.ftd.qd.constants.QueueDeleteConstants;

import java.math.BigDecimal;

import java.sql.Clob;

import java.util.ArrayList;

import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * This Data Access Object.
 */
public class QueueDeleteDAO 
{

    private Logger logger = new Logger("com.ftd.qd.dao.QueueDeleteDAO");
    private Connection connection = null;
    
    /**
   * 
   * @param conn
   */
    public QueueDeleteDAO(Connection conn)
    {
        super();
        connection = conn;
    }
    
   /**
   * 
   * @param queueDeleteHeaderId
   * @param batchProcessedFlag
   * @param updatedBy
   * @throws SAXException
   * @throws IOException
   * @throws SQLException
   * @throws ParserConfigurationException
   * @throws Exception
   */
    public void updateBatchProcessedFlag(String queueDeleteHeaderId, String batchProcessedFlag, String updatedBy)  throws SAXException, IOException, SQLException, 
        ParserConfigurationException, Exception
    {
      DataRequest request = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_QUEUE_DELETE_HEADER_ID", queueDeleteHeaderId);
      inputParams.put("IN_BATCH_PROCESSED_FLAG", batchProcessedFlag);
      inputParams.put("IN_UPDATED_BY", updatedBy);
  
      // build DataRequest object
      request.setConnection(connection);
      request.reset();
      request.setInputParams(inputParams);
  
      // Note that this DB proc will delete the queue message regardless
      // if the message is tagged by a CSR.
      request.setStatementID("UPDATE_BATCH_PROCESSED_FLAG");
  
      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
    }

  /**
   * 
   * @param queueDeleteDetailId
   * @param processedStatus
   * @param errorText
   * @param updatedBy
   * @throws SAXException
   * @throws IOException
   * @throws SQLException
   * @throws ParserConfigurationException
   * @throws Exception
   */
    public void updateQueueDeleteDetailRecord(String queueDeleteDetailId, String processedStatus, String errorText, String updatedBy)  throws SAXException, IOException, SQLException, 
        ParserConfigurationException, Exception
    {
      DataRequest request = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_QUEUE_DELETE_DETAIL_ID", queueDeleteDetailId);
      inputParams.put("IN_PROCESSED_STATUS", processedStatus);
      inputParams.put("IN_ERROR_TEXT", errorText);
      inputParams.put("IN_UPDATED_BY", updatedBy);
  
      // build DataRequest object
      request.setConnection(connection);
      request.reset();
      request.setInputParams(inputParams);
  
      // Note that this DB proc will delete the queue message regardless
      // if the message is tagged by a CSR.
      request.setStatementID("UPDATE_QUEUE_DELETE_DETAIL_RECORD");
  
      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        
        throw new Exception(message);
      }
    }
    
    /**
     * method to retrieve associated queue delete record based on the input data
     *
     * @param String - queueDeleteDetailId
     * @return QueueDeleteHeaderVO - queueDeleteHeaderVO
     *
     * @throws IOException
     * @throws ParserConfiguraitonException
     * @throws SAXException
     * @throws SQLException
     */

     public QueueDeleteHeaderVO getQueueDeleteRecord(String queueDeleteDetailId) throws Exception
     {
       QueueDeleteHeaderVO qdHeaderVO = new QueueDeleteHeaderVO();
       Map qdMap = null;

     
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("GET_QUEUE_DELETE_RECORD");
       dataRequest.addInputParam("IN_QUEUE_DELETE_DETAIL_ID",queueDeleteDetailId);

       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

       qdMap = (Map) dataAccessUtil.execute(dataRequest);
       if(qdMap != null)
       {
         // Get Queue Delete Header Info
         qdHeaderVO = getQueueDeleteHeaderInfo(qdMap,qdHeaderVO);
         // Get Queue Delete Detail Info
         qdHeaderVO = getQueueDeleteDetailInfo(qdMap,qdHeaderVO);
       }
    
       return qdHeaderVO;
     }
    
     /**
      * Retrieve the queue delete header information.
      * 
      * @param queueDeleteMap Map
      * @param queueDeleteHeaderVO QueueDeleteHeaderVO 
      * @return  QueueDeleteHeaderVO
      */
     private QueueDeleteHeaderVO getQueueDeleteHeaderInfo(Map queueDeleteMap, QueueDeleteHeaderVO queueDeleteHeaderVO) throws Exception
     {
         // Get the queue delete header information
         CachedResultSet rs = new CachedResultSet();
         rs = (CachedResultSet) queueDeleteMap.get("OUT_QUEUE_DELETE_HEADER_CUR");
               
         while(rs != null && rs.next())
         {
             //populate value object
             queueDeleteHeaderVO.setQueueDeleteHeaderId(Long.parseLong(rs.getObject("queue_delete_header_id").toString()));
             queueDeleteHeaderVO.setBatchDescription((String)rs.getObject("batch_description"));
             queueDeleteHeaderVO.setBatchCount(Integer.parseInt(rs.getObject("batch_count").toString()));
             queueDeleteHeaderVO.setBatchProcessedFlag((String)rs.getObject("batch_processed_flag"));
             queueDeleteHeaderVO.setIncludePartnerName((String)rs.getObject("include_partner_name"));
             queueDeleteHeaderVO.setExcludePartnerName((String)rs.getObject("exclude_partner_name"));
             queueDeleteHeaderVO.setDeleteQueueMsgFlag((String)rs.getObject("delete_queue_msg_flag"));
             queueDeleteHeaderVO.setDeleteQueueMsgType((String)rs.getObject("delete_queue_msg_type"));
             queueDeleteHeaderVO.setSendMessageFlag((String)rs.getObject("send_message_flag"));
             queueDeleteHeaderVO.setMessageType((String)rs.getObject("message_type"));
             queueDeleteHeaderVO.setAnspAmount((BigDecimal)rs.getObject("ansp_amount"));
             queueDeleteHeaderVO.setGiveFloristAskingPrice((String)rs.getObject("give_florist_asking_price"));
             queueDeleteHeaderVO.setCancelReasonCode((String)rs.getObject("cancel_reason_code"));
             queueDeleteHeaderVO.setTextOrReason((String)rs.getObject("text_or_reason"));
             queueDeleteHeaderVO.setResubmitOrderFlag((String)rs.getObject("resubmit_order_flag"));
             queueDeleteHeaderVO.setIncludeCommentFlag((String)rs.getObject("include_comment_flag"));
             queueDeleteHeaderVO.setCommentText((String)rs.getObject("comment_text"));
             queueDeleteHeaderVO.setSendEmailFlag((String)rs.getObject("send_email_flag"));
             queueDeleteHeaderVO.setEmailTitle((String)rs.getObject("email_title"));
             queueDeleteHeaderVO.setRefundOrderFlag((String)rs.getObject("refund_order_flag"));
             queueDeleteHeaderVO.setRefundDispCode((String)rs.getObject("refund_disp_code"));
             queueDeleteHeaderVO.setResponsibleParty((String)rs.getObject("responsible_party"));
             queueDeleteHeaderVO.setDeleteBatchByType((String)rs.getObject("delete_batch_by_type"));
             queueDeleteHeaderVO.setCreatedBy((String)rs.getObject("created_by"));
             Clob pocBody = rs.getClob("email_body");
             if(pocBody!=null)
                 queueDeleteHeaderVO.setEmailBody(pocBody.getSubString((long)1, (int) pocBody.length()));
             queueDeleteHeaderVO.setEmailSubject((String)rs.getObject("email_subject"));
         }

         return queueDeleteHeaderVO;
       }   

     /**
      * Retrieve the queue delete detail information.
      * 
      * @param queueDeleteMap Map
      * @param queueDeleteHeaderVO QueueDeleteHeaderVO 
      * @return  QueueDeleteHeaderVO
      */
     private QueueDeleteHeaderVO getQueueDeleteDetailInfo(Map queueDeleteMap, QueueDeleteHeaderVO queueDeleteHeaderVO) throws Exception
     {
         ArrayList queueDeleteDetailList = new ArrayList();
         
         // Get the queue delete detail information
         CachedResultSet rs = new CachedResultSet();
         rs = (CachedResultSet) queueDeleteMap.get("OUT_QUEUE_DELETE_DETAIL_CUR");
               
         while(rs != null && rs.next())
         {
             //populate value object
             QueueDeleteDetailVO queueDeleteDetailVO = new QueueDeleteDetailVO();
             queueDeleteDetailVO.setQueueDeleteDetailId(Long.parseLong(rs.getObject("queue_delete_detail_id").toString()));
             queueDeleteDetailVO.setQueueDeleteHeaderId(Long.parseLong(rs.getObject("queue_delete_header_id").toString()));
             if (rs.getObject("message_id") != null)
                queueDeleteDetailVO.setMessageId(Long.parseLong(rs.getObject("message_id").toString()));
             queueDeleteDetailVO.setExternalOrderNumber((String)rs.getObject("external_order_number"));
             queueDeleteDetailVO.setProcessedStatus((String)rs.getObject("processed_status"));
             queueDeleteDetailVO.setErrorText((String)rs.getObject("error_text"));
             queueDeleteDetailList.add(queueDeleteDetailVO);
         }
         queueDeleteHeaderVO.setQueueDeleteDetailVOList(queueDeleteDetailList);
         return queueDeleteHeaderVO;
       }   

     /**
      * isDuplicateMessageId
      *
      * This method checks to see if message id passed in is a duplicate request.  If it is, return true,
      * else return false.
      *
      * @param queueDeleteDetailId long 
      * @param queueDeleteHeaderId long
      * @param messageId long
      * @return boolean
      * @throws java.lang.Exception
      */
     public boolean isDuplicateMessageId(long queueDeleteDetailId, long queueDeleteHeaderId, long messageId) throws Exception
     {
       boolean isDuplicate = false;

       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("IS_DUPLICATE_MESSAGE_ID");
       dataRequest.addInputParam("IN_QUEUE_DELETE_DETAIL_ID", queueDeleteDetailId);
       dataRequest.addInputParam("IN_QUEUE_DELETE_HEADER_ID", queueDeleteHeaderId);
       dataRequest.addInputParam("IN_MESSAGE_ID", messageId);

       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

       String duplicateMsgFlag = (String) dataAccessUtil.execute(dataRequest);
       
       isDuplicate = StringUtils.equals(duplicateMsgFlag, "Y");

       return isDuplicate;
     }
     
     
     /**
      * isDuplicateOrderNumber
      *
      * This method checks to see if the external order number passed in is a duplicate request.  If it is, return true,
      * else return false.
      *
      * @param queueDeleteDetailId long 
      * @param queueDeleteHeaderId long
      * @param externalOrderNumber String
      * @return boolean
      * @throws java.lang.Exception
      */
     public boolean isDuplicateOrderNumber(long queueDeleteDetailId, long queueDeleteHeaderId, String externalOrderNumber) throws Exception
     {
       boolean isDuplicate = false;

       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("IS_DUPLICATE_ORDER_NUMBER");
       dataRequest.addInputParam("IN_QUEUE_DELETE_DETAIL_ID", queueDeleteDetailId);
       dataRequest.addInputParam("IN_QUEUE_DELETE_HEADER_ID", queueDeleteHeaderId);
       dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);

       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

       String duplicateMsgFlag = (String) dataAccessUtil.execute(dataRequest);
         
       isDuplicate = StringUtils.equals(duplicateMsgFlag, "Y");

       return isDuplicate;
     }
     
     /**
      * This method returns the name, type and member number for the passed in vendor id.
      * 
      * @param String - vendorId
      * @return CachedResultSet
      */
      public CachedResultSet getVendorInfo(String vendorId) throws Exception
     {        
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(this.connection);
         dataRequest.setStatementID("OP_GET_VENDOR");
         dataRequest.addInputParam("VENDOR_ID", vendorId);
     
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         
         CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
     
         return rs;

     }
     
     /**
      * updates the ORDER_FLORIST_USED table to indicate that a florist has
      * been used for an order
      * @param orderDetailID
      * @param floristID
      * @return n/a
      * @throws SAXException
      * @throws ParserConfigurationException
      * @throws IOException
      * @throws SQLException
      * @throws Exception
      */ 
     public void orderFloristUsed(String orderDetailID, String floristID, String selectionData)
            throws IOException, SAXException, SQLException, 
         ParserConfigurationException, Exception
         {
         if(logger.isDebugEnabled()){
             logger.debug("Entering orderFloristUsed");
             logger.debug("orderDetailID : " + orderDetailID);
             logger.debug("floristID : " + floristID);
             logger.debug("selectionData : " + selectionData);
         }
          DataRequest request = new DataRequest();
          logger.info("selectionData : " + selectionData);
         try {
             /* setup store procedure input parameters */
             HashMap inputParams = new HashMap();

             inputParams.put(QueueDeleteConstants.INSERT_ORDER_FLORIST_USED_IN_ORDER_DETAIL_ID, 
                 new Long(orderDetailID));
             inputParams.put(QueueDeleteConstants.INSERT_ORDER_FLORIST_USED_IN_FLORIST_ID,
                 floristID);
             inputParams.put("SELECTION_DATA", selectionData);
         
             /* build DataRequest object */
             request.setConnection(this.connection);
             request.reset();
             request.setInputParams(inputParams);
             request.setStatementID(QueueDeleteConstants.INSERT_ORDER_FLORIST_USED);

            /* execute the store prodcedure and retrieve output*/
             DataAccessUtil dau = DataAccessUtil.getInstance();
             Map outputs = (Map) dau.execute(request);
             /* read store prodcedure output parameters to determine 
              * if the procedure executed successfully */
                 String status = (String) outputs.get(
                     QueueDeleteConstants.INSERT_ORDER_FLORIST_USED_OUT_STATUS);
                 if(status.equals("N"))
                 {
                     String message = (String) outputs.get(
                         QueueDeleteConstants.INSERT_ORDER_FLORIST_USED_OUT_ERROR_MESSAGE);

                     throw new Exception(message);
                 }
         } finally {
             if(logger.isDebugEnabled()){
                logger.debug("Exiting orderFloristUsed");
             } 
         }
     }

     
     /**
      * Update the order detail for an order
      * @param orderDetailID - String
      * @param floristID - String
      * @param csr_id - String
      * @return n/a
      * @throws IOException, SAXException, ParserConfigurationException, 
         XSLException, SQLException, Exception
      */
     public void modifyOrder(String orderDetailID, String floristID, String csr_id, String orderDispostion) 
         throws IOException, SAXException, ParserConfigurationException, 
         SQLException, Exception
     {
         if(logger.isDebugEnabled()){
             logger.debug("Entering modifyOrder");
             logger.debug("orderDetailID: " + orderDetailID);
             logger.debug("floristID: [" + floristID+"]");

         }
          DataRequest request = new DataRequest();

         try {
             /* setup store procedure input parameters */
             HashMap inputParams = new HashMap();


             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID, 
                 new Long(orderDetailID));
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_RECIPIENT_ID, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_PRODUCT_ID, 
                 null);                
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_QUANTITY, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_COLOR_1, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_COLOR_2, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SUBSTITUTION_INDICATOR, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SAME_DAY_GIFT, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_OCCASION, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_CARD_MESSAGE, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_CARD_SIGNATURE, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SPECIAL_INSTRUCTIONS, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_RELEASE_INFO_INDICATOR, 
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_FLORIST_ID, 
                 floristID.trim());            
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SHIP_METHOD, 
                 null);            
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SHIP_DATE, 
                 null);   
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ORDER_DISP_CODE, 
                 orderDispostion);           
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SECOND_CHOICE_PRODUCT, 
                 null);   
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ZIP_QUEUE_COUNT, 
                 null);         
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_UPDATED_BY,
                 csr_id);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_ON,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SCRUBBED_BY,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_UNSPSC_CODE,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_PO_NUMBER,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_AMS_PROJECT_CODE,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_ARIBA_COST_CENTER,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SIZE_INDICATOR,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_MILES_POINTS,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SUBCODE,
                 null);
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_SOURCE_CODE,
                 null);       
             inputParams.put(QueueDeleteConstants.UPDATE_ORDER_DETAILS_IN_REJECT_RETRY_COUNT,
                 null);       

             /* build DataRequest object */
             request.setConnection(this.connection);
             request.reset();
             request.setInputParams(inputParams);
             request.setStatementID(QueueDeleteConstants.UPDATE_ORDER_DETAILS);

             // get data
             DataAccessUtil dau = DataAccessUtil.getInstance();
             Map outputs = (Map) dau.execute(request);
             /* read store prodcedure output parameters to determine 
              * if the procedure executed successfully */
                 String status = (String) outputs.get(
                     QueueDeleteConstants.OUT_STATUS);
                 if(status.equals("N"))
                 {
                     String message = (String) outputs.get(
                     QueueDeleteConstants.OUT_MESSAGE);

                     throw new Exception(message);
                 }
       
         } finally {
             if(logger.isDebugEnabled()){
                logger.debug("Exiting modifyOrder");
             } 
         }
     }

     
     /**
        * @param connection database connection
        * @param venusId to get transaction for
        * @return
        * @throws Exception
        */
     public ArrayList getAllMercuryOrVenusMessages(String orderDetailId, String mercuryVenusId, String mercuryMessageNumber, 
                                                 String mercuryVenusOrderNumber, String msgType, String messageType, 
                                                 String levelOfDetails)
       throws Exception
     {
       ArrayList returnList = new ArrayList(); 
       CachedResultSet output = null;

       DataRequest dataRequest = new DataRequest();
       dataRequest.reset();
       dataRequest.setConnection(this.connection);
       dataRequest.setStatementID("QD_GET_MESSAGE_DETAILS_GENERIC");
       dataRequest.addInputParam("IN_REFERENCE_NUMBER", orderDetailId);
       dataRequest.addInputParam("IN_MERCURY_VENUS_ID", mercuryVenusId);
       dataRequest.addInputParam("IN_MERCURY_MESSAGE_NUMBER", mercuryMessageNumber);
       dataRequest.addInputParam("IN_MERCURY_VENUS_ORDER_NUMBER", mercuryVenusOrderNumber);
       dataRequest.addInputParam("IN_MSG_TYPE", msgType);
       dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
       dataRequest.addInputParam("IN_ORD_OR_MSG_OR_ID_LEVEL", levelOfDetails);

       DataAccessUtil dau = DataAccessUtil.getInstance();

       output = (CachedResultSet) dau.execute(dataRequest);
       MercuryVO mVO = null; 
       VenusVO vVO = null; 

       while (output.next())
       {
         if (messageType.equalsIgnoreCase("Mercury"))
         {
           mVO = new MercuryVO(); 
           
           mVO.setMercuryId(output.getString("MERCURY_ID"));
           mVO.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
           mVO.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
           mVO.setMercuryStatus(output.getString("MERCURY_STATUS"));
           mVO.setMsgType(output.getString("MSG_TYPE"));
           mVO.setOutboundId(output.getString("OUTBOUND_ID"));
           mVO.setSendingFlorist(output.getString("SENDING_FLORIST"));
           mVO.setFillingFlorist(output.getString("FILLING_FLORIST"));
           mVO.setOrderDate(getUtilDate(output.getDate("ORDER_DATE")));
           mVO.setRecipient(output.getString("RECIPIENT"));
           mVO.setAddress(output.getString("ADDRESS"));
           mVO.setCityStateZip(output.getString("CITY_STATE_ZIP"));
           mVO.setPhoneNumber(output.getString("PHONE_NUMBER"));
           mVO.setDeliveryDate(getUtilDate(output.getDate("DELIVERY_DATE")));
           mVO.setDeliveryDateText(output.getString("DELIVERY_DATE_TEXT"));
           mVO.setFirstChoice(output.getString("FIRST_CHOICE"));
           mVO.setSecondChoice(output.getString("SECOND_CHOICE"));
           mVO.setPrice(new Double(output.getDouble("PRICE")));
           mVO.setCardMessage(output.getString("CARD_MESSAGE"));
           mVO.setOccasion(output.getString("OCCASION"));
           mVO.setSpecialInstructions(output.getString("SPECIAL_INSTRUCTIONS"));
           mVO.setPriority(output.getString("PRIORITY"));
           mVO.setOperator(output.getString("OPERATOR"));
           mVO.setComments(output.getString("COMMENTS"));
           mVO.setSakText(output.getString("SAK_TEXT"));
           mVO.setCtseq(output.getInt("CTSEQ"));
           mVO.setCrseq(output.getInt("CRSEQ"));
           mVO.setTransmissionDate(getUtilDate(output.getDate("TRANSMISSION_TIME")));
           mVO.setReferenceNumber(output.getString("REFERENCE_NUMBER"));
           mVO.setProductId(output.getString("PRODUCT_ID"));
           mVO.setZipCode(output.getString("ZIP_CODE"));
           mVO.setAskAnswerCode(output.getString("ASK_ANSWER_CODE"));
           mVO.setSortValue(output.getString("SORT_VALUE"));
           mVO.setRetrievalFlag(output.getString("RETRIEVAL_FLAG"));
           mVO.setCombinedReportNumber(output.getString("COMBINED_REPORT_NUMBER"));
           mVO.setRofNumber(output.getString("ROF_NUMBER"));
           mVO.setAdjReasonCode(output.getString("ADJ_REASON_CODE"));
           mVO.setOverUnderCharge(new Double(output.getDouble("OVER_UNDER_CHARGE")));
           mVO.setFromMessageNumber(output.getString("FROM_MESSAGE_NUMBER"));
           mVO.setFromMessageDate(getUtilDate(output.getDate("FROM_MESSAGE_DATE")));
           mVO.setToMessageNumber(output.getString("TO_MESSAGE_NUMBER"));
           mVO.setToMessageDate(getUtilDate(output.getDate("TO_MESSAGE_DATE")));
           mVO.setCreatedOn(getUtilDate(output.getDate("CREATED_ON")));
           mVO.setViewQueue(output.getString("VIEW_QUEUE"));
           mVO.setResponsibleFlorist(output.getString("RESPONSIBLE_FLORIST"));
           mVO.setLockedOn(getUtilDate(output.getDate("LOCKED_ON")));
           mVO.setLockedBy(output.getString("LOCKED_BY"));
           mVO.setCompOrder(output.getString("COMP_ORDER"));
           mVO.setRequireConfirmation(output.getString("REQUIRE_CONFIRMATION"));
           mVO.setSuffix(output.getString("SUFFIX"));
           mVO.setOrderSequence(output.getString("ORDER_SEQ"));
           mVO.setAdminSequence(output.getString("ADMIN_SEQ"));
           mVO.setOldPrice(new Double(output.getDouble("OLD_PRICE")));
           mVO.setMessageDirection(output.getString("MESSAGE_DIRECTION"));
           mVO.setManualReconcileDate(getUtilDate(output.getDate("MANUAL_RECONCILE_DATE")));
           mVO.setRequireConfirmation(output.getString("REQUIRE_CONFIRMATION"));

           returnList.add(mVO); 
         }
         else if (messageType.equalsIgnoreCase("Venus"))
         {
           vVO = new VenusVO(); 
           
           vVO.setVenusId((String)output.getObject("VENUS_ID"));
           vVO.setVenusOrderNumber((String)output.getObject("VENUS_ORDER_NUMBER"));
           vVO.setVenusStatus((String)output.getObject("VENUS_STATUS"));
           vVO.setMsgType((String)output.getObject("MSG_TYPE"));
           vVO.setSendingVendor((String)output.getObject("SENDING_VENDOR"));
           vVO.setFillingVendor((String)output.getObject("FILLING_VENDOR"));
           vVO.setOrderDate(getUtilDate(output.getObject("ORDER_DATE")));
           vVO.setRecipient((String)output.getObject("RECIPIENT"));
           vVO.setBusinessName((String)output.getObject("BUSINESS_NAME"));
           vVO.setAddress1((String)output.getObject("ADDRESS_1"));
           vVO.setAddress2((String)output.getObject("ADDRESS_2"));
           vVO.setCity((String)output.getObject("CITY"));
           vVO.setState((String)output.getObject("STATE"));
           vVO.setZip((String)output.getObject("ZIP"));
           vVO.setPhoneNumber((String)output.getObject("PHONE_NUMBER"));
           vVO.setDeliveryDate(getUtilDate(output.getObject("DELIVERY_DATE")));
           vVO.setFirstChoice((String)output.getObject("FIRST_CHOICE"));
           vVO.setPrice(new Double(output.getDouble("PRICE")));
           vVO.setCardMessage((String)output.getObject("CARD_MESSAGE"));
           vVO.setShipDate(getUtilDate(output.getObject("SHIP_DATE")));
           vVO.setOperator((String)output.getObject("OPERATOR"));
           vVO.setComments((String)output.getObject("COMMENTS"));
           vVO.setTransmissionTime(getUtilDate(output.getObject("TRANSMISSION_TIME")));
           vVO.setReferenceNumber((String)output.getObject("REFERENCE_NUMBER"));
           vVO.setProductID((String)output.getObject("PRODUCT_ID"));
           vVO.setCombinedReportNumber((String)output.getObject("COMBINED_REPORT_NUMBER"));
           vVO.setRofNumber((String)output.getObject("ROF_NUMBER"));
           vVO.setAdjReasonCode((String)output.getObject("ADJ_REASON_CODE"));
           vVO.setOverUnderCharge(new Double(output.getDouble("OVER_UNDER_CHARGE")));
           vVO.setProcessedIndicator((String)output.getObject("PROCESSED_INDICATOR"));
           vVO.setMessageText((String)output.getObject("MESSAGE_TEXT"));
           vVO.setSendToVenus((String)output.getObject("SEND_TO_VENUS"));
           vVO.setOrderPrinted((String)output.getObject("ORDER_PRINTED"));
           vVO.setSubType((String)output.getObject("SUB_TYPE"));
           vVO.setCreatedOn(getUtilDate(output.getObject("CREATED_ON")));
           vVO.setUpdatedOn(getUtilDate(output.getObject("UPDATED_ON")));
           vVO.setErrorDescription((String)output.getObject("ERROR_DESCRIPTION"));
           vVO.setCountry((String)output.getObject("COUNTRY"));
           vVO.setCancelReasonCode((String)output.getObject("CANCEL_REASON_CODE"));
           vVO.setCompOrder((String)output.getObject("COMP_ORDER"));
           vVO.setOldPrice(new Double(output.getDouble("OLD_PRICE")));
           vVO.setShipMethod(output.getString("SHIP_METHOD"));
           vVO.setVendorSKU(output.getString("VENDOR_SKU"));
           vVO.setProductDescription(output.getString("PRODUCT_DESCRIPTION"));
           vVO.setProductWeight(output.getString("PRODUCT_WEIGHT"));
           vVO.setMessageDirection(output.getString("MESSAGE_DIRECTION"));
           vVO.setExternalSystemStatus(output.getString("EXTERNAL_SYSTEM_STATUS"));
           vVO.setVendorId(output.getString("VENDOR_ID"));
           vVO.setShippingSystem(output.getString("SHIPPING_SYSTEM"));
           vVO.setTrackingNumber(output.getString("TRACKING_NUMBER"));
           vVO.setPrintedStatusDate(getUtilDate(output.getObject("PRINTED")));
           vVO.setShippedStatusDate(getUtilDate(output.getObject("SHIPPED")));
           vVO.setCancelledStatusDate(getUtilDate(output.getObject("CANCELLED")));
           vVO.setRejectedStatusDate(getUtilDate(output.getObject("REJECTED")));
           vVO.setFinalCarrier(output.getString("FINAL_CARRIER"));
           vVO.setFinalShipMethod(output.getString("FINAL_SHIP_METHOD"));
           vVO.setSdsStatus(output.getString("SDS_STATUS"));
           vVO.setForcedShipment(output.getString("FORCED_SHIPMENT"));
           vVO.setRatedShipment(output.getString("RATED_SHIPMENT"));
           vVO.setBatchNumber(output.getString("BATCH_NUMBER"));
           vVO.setBatchSequence(output.getString("BATCH_SEQUENCE"));
           vVO.setLastScan(getUtilDate(output.getObject("LAST_SCAN")));
           vVO.setDeliveryScan(getUtilDate(output.getObject("DELIVERY_SCAN")));
           vVO.setGetSdsResponse(output.getString("GET_SDS_RESPONSE"));
           vVO.setZoneJumpFlag(output.getString("ZONE_JUMP_FLAG"));
           vVO.setZoneJumpLabelDate(getUtilDate(output.getObject("ZONE_JUMP_LABEL_DATE")));
           vVO.setZoneJumpTrailerNumber(output.getString("ZONE_JUMP_TRAILER_NUMBER"));

           returnList.add(vVO); 
         }


       }

       return returnList;
     }
     
     /*
      * Converts a database timestamp to a java.util.Date.
      */
     private java.util.Date getUtilDate(Object time)
     {
         java.util.Date theDate = null;
         boolean test  = false;
         if(time != null)
         {
             if(test || time instanceof Timestamp)
             {
                 Timestamp timestamp = (Timestamp) time;
                 theDate = new java.util.Date(timestamp.getTime());
             }
             else
             {
                 theDate = (java.util.Date) time;            
             }
         }
         
         return theDate;
     }
     
    public CachedResultSet getOrderRecipientInfo(String orderDetailId) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("QD_GET_OD_CUST_DETAILS");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
          return rs;
    }
    
    /**
     * findOrderNumber
     *
     * @param a_orderNum
     * @throws java.lang.Exception
     */
    public HashMap findOrderNumber(String a_orderNum) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("OP_FIND_ORDER_NUMBER");
      dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    
      HashMap searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

      return searchResults;
    }
    
    public void insertComment(CommentsVO comment) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("OP_INSERT_COMMENTS");
          dataRequest.addInputParam("IN_CUSTOMER_ID", comment.getCustomerId());
          dataRequest.addInputParam("IN_ORDER_GUID", comment.getOrderGuid());
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", comment.getOrderDetailId());
          dataRequest.addInputParam("IN_COMMENT_ORIGIN", comment.getCommentOrigin());
          dataRequest.addInputParam("IN_REASON", comment.getReason());
          dataRequest.addInputParam("IN_DNIS_ID", comment.getDnisId());
          dataRequest.addInputParam("IN_COMMENT_TEXT", comment.getComment());
          dataRequest.addInputParam("IN_COMMENT_TYPE", comment.getCommentType());
          dataRequest.addInputParam("IN_CSR_ID", comment.getUpdatedBy());
          
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);      
          String status = (String) outputs.get(QueueDeleteConstants.OUT_STATUS);
          if(status != null && status.equals("N"))
          {
              String message = (String) outputs.get(QueueDeleteConstants.OUT_MESSAGE);
              throw new Exception(message);
          }
          logger.debug("Insert Comments Successful. Status="+status);

    }
    
    /**
     * This method will update the delivery confirmation status on the order detail record
     * @param orderDetailId
     * @param status
     * @param csrId
     * @throws Exception
     */
    public void updateDCONStatus(String orderDetailId, String status, String csrId) throws Exception
    {         
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
      inputParams.put("IN_STATUS", status);
      inputParams.put("IN_CSR_ID", csrId);
      
      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("OP_UPDATE_DCON_STATUS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
      String rsStatus = (String) outputs.get("OUT_STATUS");
      if(rsStatus != null && rsStatus.equalsIgnoreCase("N"))
      {
        String message = (String) outputs.get("OUT_ERROR_MESSAGE");
        throw new SQLException(message);
      }
    }

    /**
     * This method will check if stock email type id is DCON.  If it is, it will return a 'Y'
     * 
     * @throws java.lang.Exception
     * @param String stockEmailId
     * @return boolean
     */

    public boolean isDCONEmail(String stockEmailId) throws Exception
    {
        boolean isDCONEmail = false;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("QD_IS_DCON_EMAIL");
        dataRequest.addInputParam("IN_STOCK_EMAIL_ID", stockEmailId);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String stockEmailTypeId = (String) dataAccessUtil.execute(dataRequest);
        
        if(stockEmailTypeId != null && stockEmailTypeId.equalsIgnoreCase("Y"))
        {        
            isDCONEmail = true;    
        }
        return isDCONEmail;
    }
    
    /**
     * Call stored procedure GET_MESSAGE_ORDER_STATUS and return 
     * MessageOrderStatus Value Object 
     * based on message order number and order detail id.
     * @param messageOrderNumber - String
     * @param messageType - String
     * @param orderDetailId 
     * @return MessageOrderStatusVO
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SQLException
     */
    public MessageOrderStatusVO getMessageOrderStatus(
        String messageOrderNumber, String orderDetailId, String messageType) 
        throws SAXException, IOException, SQLException, ParserConfigurationException{
        
        MessageOrderStatusVO statusVO=new MessageOrderStatusVO();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.connection);
        dataRequest.setStatementID("QD_GET_MESSAGE_ORDER_STATUS");
        dataRequest.addInputParam("IN_MESSAGE_ORDER_NUMBER", messageOrderNumber);
        logger.info("messageOrderNumber: " + messageOrderNumber);
        logger.info("messageType: " + messageType);
        logger.info("orderDetailId: " + orderDetailId);
        dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet result=(CachedResultSet)dataAccessUtil.execute(dataRequest);
        statusVO.setMessageOrderNumber(messageOrderNumber);
        
      if(result.next()){
        statusVO.setOrderDetailId(result.getString("order_detail_id"));
        statusVO.setCancelSent(this.flagToBoolean(result.getString("cancel_sent")));
        statusVO.setDeliveryDate(result.getTimestamp("delivery_date"));
        statusVO.setFillingFloristCode(result.getString("filling_florist"));
        statusVO.setFloristRejectOrder(this.flagToBoolean(result.getString("florist_rej_order")));
        logger.debug("HAS_LIVE_FTD "+result.getString("has_live_ftd"));
        statusVO.setHasLiveFTD(this.flagToBoolean(result.getString("has_live_ftd")));
        
        
        statusVO.setAttemptedFTD(this.flagToBoolean(result.getString("ATTEMPTED_FTD")));
        statusVO.setOrderStatus(result.getString("order_status"));
        logger.info("orderStatus: " + result.getString("order_status"));
        statusVO.setOrderType(result.getString("order_type"));
        logger.debug("OUT_ORDER_TYPE="+statusVO.getOrderType());
        statusVO.setHasInboundASKP(this.flagToBoolean(result.getString("inbound_askp")));
        statusVO.setATypeRefund(this.flagToBoolean(result.getString("a_type_refund")));
        statusVO.setCancelDenied(this.flagToBoolean(result.getString("cancel_denied")));
        statusVO.setRecipientCountry(result.getString("recipient_country"));
        logger.debug("ATTEMPTED_FTD "+result.getString("ATTEMPTED_FTD"));
        String compOrderFlag=result.getString("comp_order");
        statusVO.setCompOrder(StringUtils.isNotBlank(compOrderFlag)&&compOrderFlag.equalsIgnoreCase("C"));
        logger.debug("Comp Order "+statusVO.isCompOrder());
      }
      
      if(logger.isDebugEnabled())
      {
        
        logger.debug("status: attempted_ftd: "+statusVO.isAttemptedFTD());
        logger.debug("status: cancelled: "+statusVO.isCancelSent());
        logger.debug("status: cancel denied: "+statusVO.isCancelDenied());
        logger.debug("status: recipient_country: "+statusVO.getRecipientCountry());
      }
        return statusVO;
    }
    
    private boolean flagToBoolean(String flag){
        logger.debug("flag: ["+flag+"]");
        if(flag!=null&&flag.trim().equalsIgnoreCase("Y")){
                return true;
        }else{
                return false;
        }
    }
    
    public CachedResultSet getOrderCustomerInfo(String orderDetailId) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("QD_GET_ORDER_CUSTOMER_INFO");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
          return rs;
    }
    
    /**
     * method to search for a shopping cart based on the input data
     *
     * @param session id
     * @param customer service representative id
     * @param master order number
     * @param order guid
     * @param start position
     * @param maxRecords
     *
     * @return HashMap containing cursors and output parameters
     *
     * @throws java.lang.Exception
     */

    public HashMap getOrderInfoForPrint(String orderDetailId, String includeComments, String processingId)
            throws Exception
    {
      HashMap searchResults = new HashMap();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.connection);
      dataRequest.setStatementID("QD_GET_ORDER_INFO_FOR_PRINT");

      dataRequest.addInputParam("IN_ORDER_DETAIL_ID",       orderDetailId);
      dataRequest.addInputParam("IN_INCLUDE_COMMENTS",        includeComments);
      dataRequest.addInputParam("IN_PROCESSING_ID",           processingId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
      searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

      return searchResults;

    }
    
    public CachedResultSet getQueueMessage(String messageId)
        throws SAXException, 
        ParserConfigurationException, IOException, SQLException, Exception {   
        // Build db stored procedure input parms

        Map inputParams = new HashMap(1);
        inputParams.put("IN_MESSAGE_ID", messageId);
        DataRequest request = new DataRequest();
        request.setConnection(this.connection);
        request.reset();
        request.setInputParams(inputParams);
        request.setStatementID("QD_GET_QUEUE_MESSAGE_BY_ID");

        // Submit the request to the database.
        try {
            // Execute the stored procedure
            DataAccessUtil dau = DataAccessUtil.getInstance();
            return (CachedResultSet) dau.execute(request);
        } catch (SQLException e) {
            String errorMsg = "getQueueMessage: Failed.";
            logger.error(errorMsg, e);
            throw new Exception(errorMsg, e);
        }
    }
    
    
       /**
        * Call stored procedure to delete queue from queue table.
        * @param queueMessageId - String
        * @return n/a
        * @throws SAXException
        * @throws IOException
        * @throws SQLException
        * @throws ParserConfigurationException
        * @throws Exception
        */
       public void deleteQueue(String queueMessageId, String csrID)
       throws 
           IOException, SAXException, SQLException, ParserConfigurationException, 
           Exception
           {
           if(logger.isDebugEnabled()){
               logger.debug("Entering removeQueue");
               logger.debug("MessageID : " + queueMessageId);
           }
            DataRequest request = new DataRequest();
            
           try {
               /* setup store procedure input parameters */
               HashMap inputParams = new HashMap();
               inputParams.put(
                   QueueDeleteConstants.DELETE_QUEUE_RECORD_NO_AUTH_IN_MESSAGE_ID, queueMessageId);
               inputParams.put(
                   QueueDeleteConstants.DELETE_QUEUE_RECORD_NO_AUTH_IN_CSR_ID, csrID);
                   
               // build DataRequest object
               request.setConnection(this.connection);
               request.reset();
               request.setInputParams(inputParams);
               
               // Note that this DB proc will delete the queue message regardless
               // if the message is tagged by a CSR.
               request.setStatementID(QueueDeleteConstants.DELETE_QUEUE_RECORD_NO_AUTH);

               // get data
               DataAccessUtil dau = DataAccessUtil.getInstance();
               Map outputs = (Map) dau.execute(request);
                   String status = (String) outputs.get(
                       QueueDeleteConstants.DELETE_QUEUE_RECORD_NO_AUTH_OUT_STATUS);
                   if(status.equals("N"))
                   {
                       String message = (String) outputs.get(
                           QueueDeleteConstants.DELETE_QUEUE_RECORD_NO_AUTH_OUT_MESSAGE);

                       throw new Exception(message);
                   }
           } finally {
               if(logger.isDebugEnabled()){
                  logger.debug("Exiting removeQueue");
               } 
           }       
           
       }
    
    /**
       * Call stored procedure to get the queue record based off of the external order number.
       * @param externalOrderNumber - String
       * @return n/a
       * @throws Exception
       */
    public CachedResultSet getQueueMessageByExternalOrderNumber(String externalOrderNumber, String queueTypesSelected)
      throws Exception
    {
      // Build db stored procedure input parms
      Map inputParams = new HashMap(1);
      inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
      inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
      DataRequest request = new DataRequest();
      request.setConnection(this.connection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID("QD_GET_QUEUE_MESSAGE_BY_EXTERNAL_ORDER_NUMBER");

      // Submit the request to the database.
      try
      {
        // Execute the stored procedure
        DataAccessUtil dau = DataAccessUtil.getInstance();
        return (CachedResultSet) dau.execute(request);
      }
      catch (Exception e)
      {
        String errorMsg = "getQueueMessageByExternalOrderNumber: Failed for " + externalOrderNumber;
        logger.error(errorMsg, e);
        throw new Exception(errorMsg, e);
      }
    }
    
     /**
       * Delete queue records based on order detail id and queue types
       * @param orderDetailId - String
       * @param queueTypesSelected - String - all the queue types selected
       * @return n/a
       * @throws Exception
       */
    public void deleteAllQsByOrdDetId(String orderDetailId, String queueTypesSelected, String csrID)
      throws Exception
    {
      logger.debug("Entering deleteAllQsByOrdDetId - Order Detail Id = " + orderDetailId + " and queueTypesSelected = " + 
                   queueTypesSelected);

      DataRequest request = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
      inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
      inputParams.put("IN_CSR_ID", csrID);

      // build DataRequest object
      request.setConnection(this.connection);
      request.reset();
      request.setInputParams(inputParams);

      // Note that this DB proc will delete the queue message regardless
      // if the message is tagged by a CSR.
      request.setStatementID("Q_DELETE_ALL_QUEUE_RECORDS_BY_ORD_DET_NO_AUTH");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }

    }
    
    /**
       * Call stored procedure to delete selected queue types from queue table based off of the external order number and queue type
       * @param externalOrderNumber - String
       * @return n/a
       * @throws Exception
       */
    public void deleteAllQsByExtOrdNum(String externalOrderNumber, String queueTypesSelected, String csrID)
      throws Exception
    {
      logger.debug("Entering deleteQueueByExtOrdNum - External Order Number = " + externalOrderNumber + 
                   " and queueTypesSelected = " + queueTypesSelected);

      DataRequest request = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
      inputParams.put("IN_QUEUE_TYPES", queueTypesSelected);
      inputParams.put("IN_CSR_ID", csrID);

      // build DataRequest object
      request.setConnection(this.connection);
      request.reset();
      request.setInputParams(inputParams);

      // Note that this DB proc will delete the queue message regardless
      // if the message is tagged by a CSR.
      request.setStatementID("Q_DELETE_ALL_QUEUE_RECORDS_BY_EXT_ORD_NO_AUTH");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      String status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        String message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }

    }
    
    public CachedResultSet getQueueMsgByExtOrdNum(String externalOrderNumber, String queueTypesSelected) throws Exception
    {
    
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("QD_GET_QUEUE_MESSAGE_BY_EXTERNAL_ORDER_NUMBER");
          dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
          dataRequest.addInputParam("IN_QUEUE_TYPES", queueTypesSelected);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
          return rs;
    }
    
    public CachedResultSet getQueueDeleteUserInfo(String csrId) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID("QD_GET_USER_INFO");
          dataRequest.addInputParam("IN_CSR_ID",csrId);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
          return rs;
    }
    
    /**
      * Get a list of queue delete detail ids for given queue delete header id
      *
      * @param queueDelteHeaderId
      * @return List
      * @throws Exception
      */
    public List getQueueDeleteDetailIds(String queueuDeleteHeaderId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();

      List queueDeleteDetailIdList = new ArrayList();

      /* setup stored procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_QUEUE_DELETE_HEADER_ID", Long.parseLong(queueuDeleteHeaderId));

      /* build DataRequest object */
      dataRequest.setConnection(connection);
      dataRequest.setStatementID("GET_QUEUE_DELETE_DETAIL_IDS");
      dataRequest.setInputParams(inputParams);

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      while (outputs.next())
      {
        queueDeleteDetailIdList.add(outputs.getString("queue_delete_detail_id"));
      }

      return queueDeleteDetailIdList;
    }
    
    /**
     * This method updates the florist selection log id on the mercury.mercury table
     * @param mercuryID
     * @param floristSelectionLogId
     * @throws Exception
     */
      public void updateFloristSelectionLogId(String mercuryId, String floristSelectionLogId) throws Exception
      {         
    	 DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(connection);
        
         dataRequest.setStatementID("UPDATE_FLORIST_SELECTION_ID");
         Map paramMap = new HashMap();   
         
         paramMap.put("IN_MERCURY_ID",mercuryId);
         paramMap.put("IN_FLORIST_SELECTION_LOG_ID", floristSelectionLogId);
         
         dataRequest.setInputParams(paramMap);
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
         String status = (String) outputs.get("OUT_STATUS");
         if(status.equals("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
      }  
    
}
