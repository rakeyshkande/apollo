package com.ftd.qd.mdb;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;
import com.ftd.qd.bo.QueueDeleteBO;
import com.ftd.qd.dao.QueueDeleteDAO;
import com.ftd.qd.util.QueueDeleteUtil;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.StringTokenizer;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.naming.Context;
import javax.naming.InitialContext;


public class QueueDeleteMDB implements MessageDrivenBean, MessageListener 
{
  private static Logger logger  = 
          new Logger("com.ftd.qd.mdb.QueueDeleteMDB");
  protected MessageDrivenContext messageDrivenContext = null;
  
  public void ejbCreate()
  {

  }

  public void onMessage(Message msg){
    logger.info("Entering onMessage");   
    
    String queueDeleteHeaderId = null;
    QueueDeleteHeaderVO queueDeleteHeaderVO = null;
    boolean processBatch = false;
    boolean processDetail = false;
    String queueDeleteDetailId = null;
    Connection connection = null;
    
   try {
      try{
          connection = QueueDeleteUtil.getConnection();
          QueueDeleteDAO qdDAO = new QueueDeleteDAO(connection);
          
          //get message and extract queue delete detail id
          TextMessage textMessage = (TextMessage)msg;
          String textMsg = textMessage.getText();
                    
          String request = textMsg.substring(0,1);
          if(request.equalsIgnoreCase("B"))
          {
            processBatch = true;
            queueDeleteHeaderId = textMsg.substring(1);
            logger.info("processing QUEUE DELETE JMS MESSAGE for queue delete header id: " + queueDeleteHeaderId);

            //retrieve all queue delete detail ids associated to queue delete header id
            List queueDeleteDetailIdList = new ArrayList();
            queueDeleteDetailIdList = qdDAO.getQueueDeleteDetailIds(queueDeleteHeaderId);
            //loop through list and process the batch
            Iterator iter = queueDeleteDetailIdList.iterator();
            while(iter.hasNext())                        
            {
                queueDeleteDetailId = (String)iter.next();
                //dispatch batch to queue delete for processing 
                this.sendQueueDeleteMessage(queueDeleteDetailId);
            }
          }
          else 
          {
            processDetail = true;
            queueDeleteDetailId = textMsg;
            logger.info("processing QUEUE DELETE JMS MESSAGE for queue delete detail id: " + queueDeleteDetailId);
            queueDeleteHeaderVO = qdDAO.getQueueDeleteRecord(queueDeleteDetailId);
            QueueDeleteBO qdBO = new QueueDeleteBO(connection);
            qdBO.processRequest(queueDeleteHeaderVO);
          }          
          
          logger.info("Leaving onMessage"); 
          
      }catch (Throwable t) {
          String errMsg = "";
          if(processBatch)
            errMsg = "Queue Delete MDB failed with Throwable. queue delete header id = " + queueDeleteHeaderId;
          else
            errMsg = "Queue Delete MDB failed with Throwable. queue delete detail id = " + queueDeleteDetailId;
          logger.error(errMsg);
          logger.error(t);
          throw new Exception(errMsg);
         
      } 
    } catch (Exception e) { 
      logger.info(e.toString());
      // Rollback the transaction mananged by container and do not acknowledge message. 
      messageDrivenContext.setRollbackOnly();
    } finally 
    {
      try { 
        if (connection != null) {
            connection.close();
        }
      } catch (Exception e) 
      {
        logger.error(e);
      }
      if (messageDrivenContext.getRollbackOnly())
        	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
    }
  }

  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.messageDrivenContext = ctx;
  }
  
  private void sendQueueDeleteMessage(String queueDeleteDetailId) throws Exception
  {
    logger.debug("Sending queue delete detail id: " + queueDeleteDetailId + " to queue delete...");

    MessageToken token = new MessageToken();
    Context context = new InitialContext();
    token.setMessage(queueDeleteDetailId);
    token.setStatus("QUEUEDELETE");
    token.setJMSCorrelationID(queueDeleteDetailId);
    //Per Jason Weiss on 9/26/2006, put in a 5 second delay
    token.setProperty("JMS_OracleDelay", String.valueOf(5),"int");
    Dispatcher dispatcher = Dispatcher.getInstance();
    dispatcher.dispatchTextMessage(context, token);
  }
    
 
}