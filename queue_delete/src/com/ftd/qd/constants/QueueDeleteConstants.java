package com.ftd.qd.constants;


import com.ftd.osp.utilities.plugins.Logger;

/**
 * Static information 
 */

public class QueueDeleteConstants
{
    private Logger logger = new Logger("com.ftd.qd.QueueDeleteConstants");

    public static final String EMPTY_STRING                                 = "";

    /*********************************************************************************************
    //config file locations
    *********************************************************************************************/
    public static final String CONFIG_FILE                                  = "queue_delete_config.xml";
    public static final String SECURITY_FILE                                = "security-config.xml";
    public static final String DATASOURCE_NAME                              = "DATASOURCE_NAME";
    public static final String QD_CONTEXT                                   = "QUEUE_DELETE";
    public static final String JNDI_USERTRANSACTION_ENTRY                   = "jndi_usertransaction_entry";
    public static final String SERVICE_LOCATOR_CONFIG_FILE                  = "service-locator-config.xml";
    public static final String ERP_CONFIG_CONTEXT                           = "EMAIL_REQUEST_PROCESSING_CONFIG";
    
   

    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT                                      = "context";
    public static final String SEC_TOKEN                                    = "securitytoken";
    public static final String SECURITY_IS_ON                               = "SECURITY_IS_ON";


    /*********************************************************************************************
    //comments
    *********************************************************************************************/
    public static final String ORDER_COMMENT_TYPE                           = "Order";
    public static final String COMMENT_ID                                   = "comment_id";
    public static final String COMMENT_ORIGIN                               = "comment_origin";
    public static final String COMMENT_REASON                               = "comment_reason";
    public static final String COMMENT_TEXT                                 = "comment_text";
    public static final String COMMENT_TYPE                                 = "comment_type";
    public static final String START_ORIGIN_GEN                             = "GEN";
    public static final String OPERATOR_QD                                  = "QueueDelete";



     /*********************************************************************************************
     //constants to pass in for processing id
     *********************************************************************************************/
     public static final String CONS_PROCESSING_ID_CUSTOMER                  = "CUSTOMER";
     
    /*********************************************************************************************
    //queue delete constants
    *********************************************************************************************/
    public static final String MESSAGE_ID_BATCH_TYPE                        = "MESSAGE_ID";
    public static final String ORDER_NUMBER_BATCH_TYPE                      = "EXTERNAL_ORDER_NUMBER";
    
    public static final String UPDATED_BY                                   = "QUEUE_DELETE";
    
    public static final String BATCH_PROCESSED_FLAG                         = "Y";
    
    public static final String SDS_STATUS_PRINTED                           = "Printed";
    public static final String SDS_STATUS_SHIPPED                           = "Shipped";
    
    public static final String EMAIL_MESSAGE_TYPE                           = "Email";
    public static final String EMAIL_ORIGINS                                = "EMAIL_ORIGINS";
    public static final String MESSAGE_CONTENT                              = "message_content";
    public static final String MESSAGE_ID                                   = "message_id";
    public static final String MESSAGE_SUBJECT                              = "message_subject";
    public static final String MESSAGE_TITLE                                = "message_title";
    public static final String MESSAGE_TYPE                                 = "message_type";
    
    public static final String OUT_STATUS                                   = "OUT_STATUS";
    public static final String OUT_MESSAGE                                  = "OUT_MESSAGE";
    
    
    /* UPDATE_ORDER_DETAILS stored procedure and parameters */
    public static final String UPDATE_ORDER_DETAILS                                    
    = "QD_UPDATE_ORDER_DETAILS";
    public static final String UPDATE_ORDER_DETAILS_IN_ORDER_DETAIL_ID                 
    = "IN_ORDER_DETAIL_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE                   
    = "IN_DELIVERY_DATE";
    public static final String UPDATE_ORDER_DETAILS_IN_RECIPIENT_ID                    
    = "IN_RECIPIENT_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_PRODUCT_ID                      
    = "IN_PRODUCT_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_QUANTITY                        
    = "IN_QUANTITY";
    public static final String UPDATE_ORDER_DETAILS_IN_COLOR_1                         
    = "IN_COLOR_1";
    public static final String UPDATE_ORDER_DETAILS_IN_COLOR_2                         
    = "IN_COLOR_2";
    public static final String UPDATE_ORDER_DETAILS_IN_SUBSTITUTION_INDICATOR          
    = "IN_SUBSTITUTION_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_SAME_DAY_GIFT                   
    = "IN_SAME_DAY_GIFT";
    public static final String UPDATE_ORDER_DETAILS_IN_OCCASION                        
    = "IN_OCCASION";
    public static final String UPDATE_ORDER_DETAILS_IN_CARD_MESSAGE                    
    = "IN_CARD_MESSAGE";
    public static final String UPDATE_ORDER_DETAILS_IN_CARD_SIGNATURE                  
    = "IN_CARD_SIGNATURE";
    public static final String UPDATE_ORDER_DETAILS_IN_SPECIAL_INSTRUCTIONS            
    = "IN_SPECIAL_INSTRUCTIONS";
    public static final String UPDATE_ORDER_DETAILS_IN_RELEASE_INFO_INDICATOR          
    = "IN_RELEASE_INFO_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_FLORIST_ID                      
    = "IN_FLORIST_ID";
    public static final String UPDATE_ORDER_DETAILS_IN_SHIP_METHOD                     
    = "IN_SHIP_METHOD";
    public static final String UPDATE_ORDER_DETAILS_IN_SHIP_DATE                       
    = "IN_SHIP_DATE";
    public static final String UPDATE_ORDER_DETAILS_IN_ORDER_DISP_CODE                 
    = "IN_ORDER_DISP_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_SECOND_CHOICE_PRODUCT           
    = "IN_SECOND_CHOICE_PRODUCT";
    public static final String UPDATE_ORDER_DETAILS_IN_ZIP_QUEUE_COUNT                 
    = "IN_ZIP_QUEUE_COUNT";
    public static final String UPDATE_ORDER_DETAILS_IN_UPDATED_BY                      
    = "IN_UPDATED_BY";
    public static final String UPDATE_ORDER_DETAILS_IN_DELIVERY_DATE_RANGE_END         
    = "IN_DELIVERY_DATE_RANGE_END";
    public static final String UPDATE_ORDER_DETAILS_IN_SCRUBBED_ON                     
    = "IN_SCRUBBED_ON";
    public static final String UPDATE_ORDER_DETAILS_IN_SCRUBBED_BY                     
    = "IN_SCRUBBED_BY";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_UNSPSC_CODE               
    = "IN_ARIBA_UNSPSC_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_PO_NUMBER                 
    = "IN_ARIBA_PO_NUMBER";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_AMS_PROJECT_CODE          
    = "IN_ARIBA_AMS_PROJECT_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_ARIBA_COST_CENTER               
    = "IN_ARIBA_COST_CENTER";
    public static final String UPDATE_ORDER_DETAILS_IN_SIZE_INDICATOR                  
    = "IN_SIZE_INDICATOR";
    public static final String UPDATE_ORDER_DETAILS_IN_MILES_POINTS                    
    = "IN_MILES_POINTS";
    public static final String UPDATE_ORDER_DETAILS_IN_SUBCODE                         
    = "IN_SUBCODE";
    public static final String UPDATE_ORDER_DETAILS_IN_SOURCE_CODE                     
    = "IN_SOURCE_CODE";
    public static final String UPDATE_ORDER_DETAILS_IN_REJECT_RETRY_COUNT              
    = "IN_REJECT_RETRY_COUNT";
    public static final String UPDATE_ORDER_DETAILS_OUT_STATUS                      
    = "OUT_STATUS";
    public static final String UPDATE_ORDER_DETAILS_OUT_MESSAGE                     
    = "OUT_MESSAGE";
    
    /* INSERT_ORDER_FLORIST_USED stored procedure and parameters */
    public static final String INSERT_ORDER_FLORIST_USED =
    "OP_INSERT_ORDER_FLORIST_USED";
    public static final String INSERT_ORDER_FLORIST_USED_IN_ORDER_DETAIL_ID
    = "ORDER_DETAIL_ID";
    public static final String INSERT_ORDER_FLORIST_USED_IN_FLORIST_ID
    = "FLORIST_ID";
    public static final String INSERT_ORDER_FLORIST_USED_OUT_STATUS
    = "OUT_STATUS";
    public static final String INSERT_ORDER_FLORIST_USED_OUT_ERROR_MESSAGE
    = "OUT_MESSAGE";
    
    /* remove queue entries stored procedure and parameters */
    public static final String DELETE_QUEUE_RECORD =
    "DELETE_QUEUE_RECORD";
    public static final String DELETE_QUEUE_RECORD_IN_MESSAGE_ID
    = "IN_MESSAGE_ID";
    public static final String DELETE_QUEUE_RECORD_OUT_STATUS
    = "OUT_STATUS";
    public static final String DELETE_QUEUE_RECORD_OUT_MESSAGE
    = "OUT_ERROR_MESSAGE";
    
    /* DELETE_QUEUE_RECORD_NO_AUTH stored procedure and parameters */
    public static final String DELETE_QUEUE_RECORD_NO_AUTH =
    "OP_DELETE_QUEUE_RECORD";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_IN_MESSAGE_ID
    = "IN_MESSAGE_ID";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_IN_CSR_ID
    = "IN_CSR_ID";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_OUT_STATUS
    = "OUT_STATUS";
    public static final String DELETE_QUEUE_RECORD_NO_AUTH_OUT_MESSAGE
    = "OUT_ERROR_MESSAGE";
    
    /* User order cancel/refund constants */        
    public static final String USER_REFUND_DISP_CODE = "A10";
    public static final String USER_REFUND_STATUS    = "Unbilled";
    
}