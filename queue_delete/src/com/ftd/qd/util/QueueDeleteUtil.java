package com.ftd.qd.util;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ftd.customerordermanagement.bo.RefundAPIBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.QueueDeleteHeaderVO;
import com.ftd.qd.constants.QueueDeleteConstants;


/**
 * Utility class for queue delete application
 */

public class QueueDeleteUtil 
{
    public static final int LEVEL_DEBUG = SystemMessengerVO.LEVEL_DEBUG;
    public static final int LEVEL_PRODUCTION = SystemMessengerVO.LEVEL_PRODUCTION;
    private static Logger logger  = new Logger("com.ftd.qd.util.QueueDeleteUtil");
    private static String initialContextStr = null;
    private static String ejbProviderUrl = null;
    
    
    /* 
     * Get database connection
     */
    static public Connection getConnection() throws Exception
    {
      return getConnection("DATABASE_CONNECTION");
    }  
    
    static public Connection getConnection(String connectionName) throws Exception
    {
      
      ConfigurationUtil config = ConfigurationUtil.getInstance();            
      String dbConnection = config.getProperty(QueueDeleteConstants.CONFIG_FILE,connectionName);         
     
      //get DB connection
      DataSource dataSource = (DataSource)lookupResource(dbConnection);
      return dataSource.getConnection();  
    }    
    
    
    static public DataSource getDataSource() throws Exception 
    {
      ConfigurationUtil config = ConfigurationUtil.getInstance();            
      String dbConnection = config.getProperty(QueueDeleteConstants.CONFIG_FILE,"DATABASE_CONNECTION");         
     
      //get DB connection
      DataSource dataSource = (DataSource)lookupResource(dbConnection);
      return dataSource;
      
    }
    
     /*
     * This method sends a message to the System Messenger.
     * 
     * @param String message
     * @param String error
     * @param Connection conn
     * @returns String message id
     */
     public static String sendSystemMessage(String message, String error, Connection conn) throws Exception
    {
        String messageID = "";
    
        try{
          logger.info("Sending System Message:" + message);         
                
          String messageSource = "QUEUE DELETE";
    
          //build system vo
          SystemMessengerVO systemMessage = new SystemMessengerVO();
          systemMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          systemMessage.setSource(messageSource);
          systemMessage.setType("ERROR");
          systemMessage.setMessage(message);
       
          SystemMessenger systemMessenger = SystemMessenger.getInstance();
          messageID = systemMessenger.send(systemMessage,conn);
    
          if(messageID == null)
          {
            String msg = "Unrecoverable error processing queue delete record: " + message + " Caused exception: " + error;
            logger.info(msg);
          }
        }
        catch (Exception ex) {
                    logger.error(ex);
        }
        return messageID;  
    } 
    
    /**
     * Private method to use COM EJB to post a full refund or (remaining) order amount
     */
     public static void postFullRemainingRefund(String a_orderDetailId, QueueDeleteHeaderVO queueDeleteHeaderVO) throws Exception
     {    	 
		RefundAPIBO refundApi = new RefundAPIBO();
		Connection conn = null;
		try {
			conn =getConnection();
			refundApi.postFullRemainingRefund(a_orderDetailId,
					queueDeleteHeaderVO.getRefundDispCode(),
					queueDeleteHeaderVO.getResponsibleParty(),
					QueueDeleteConstants.USER_REFUND_STATUS,
					queueDeleteHeaderVO.getCreatedBy(),
					conn);

			logger.debug("postFullRemainingRefund succeeded");
		} catch (Exception e) {
			logger.debug("postFullRemainingRefund returned with unsuccessful status: "
					+ e.getMessage());
		} finally {
			conn.close();
		}
     }
     
    /**
     * Returns a transactional resource from the EJB container.
     * 
     * @param jndiName
     * @return 
     * @throws javax.naming.NamingException
     */
    public static Object lookupResource(String jndiName)
                throws NamingException
    {
      InitialContext initContext = null;
      try
      {
        initContext = new InitialContext();
           
        return initContext.lookup(jndiName);      
      }finally  {
        try  {
          initContext.close();
        } catch (Exception ex)  {

        } finally  {
        }
      }
    }  

}