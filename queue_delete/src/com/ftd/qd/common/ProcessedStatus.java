package com.ftd.qd.common;


/**
 * Enumerates the possible values for clean.queue_delete.processed_status
 */
public enum ProcessedStatus {
    SUCCESS,
    FAILURE,
    DUPLICATE;
}
