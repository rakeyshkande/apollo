package com.ftd.partnerreward.client;

import com.ftd.partnerreward.core.bo.TransportServiceBO;
import com.ftd.partnerreward.core.controller.ControllerFactory;
import com.ftd.partnerreward.core.controller.IController;
import com.ftd.partnerreward.core.dao.FtpAdapter;
import com.ftd.partnerreward.core.ejb.RewardServiceEJB;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBHome;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;


public class RewardServiceClient {
    public static void main(String[] args) {
        RewardServiceClient rewardServiceEJBClient1 = new RewardServiceClient();

        try {
        
//        BigDecimal factor = new BigDecimal(100);
//        BigDecimal dollar = new BigDecimal(39.99);
//        System.out.println("Total: " + dollar.multiply(factor).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
        
            Context context = getInitialContext();
            RewardServiceEJBHome rewardServiceEJBHome = (RewardServiceEJBHome) PortableRemoteObject.narrow(context.lookup(
                        "RewardServiceEJB"), RewardServiceEJBHome.class);
            RewardServiceEJB rewardServiceEJB;

            // Use one of the create() methods below to create a new instance
            rewardServiceEJB = rewardServiceEJBHome.create();
            
            // Monday
//            rewardServiceEJB.rePostRewardFile("ALASKA AIRLINES", new Date(), "/tmp/alaska_ftd.txt-20051024-183103");
//            rewardServiceEJB.rePostRewardFile("GOLD POINTS", new Date(), "/tmp/ftd_batchpts.dat-20051024-151552");
//            rewardServiceEJB.rePostRewardFile("UPROMISE", new Date(), "/tmp/FTD_UPI_Activity_Request-20051019-142305");
//            

// Tues
//            rewardServiceEJB.rePostRewardFile("AMERICAN AUTO ASSOC.", new Date(), "/tmp/UMCFTD-20051026-161440");
//            rewardServiceEJB.rePostRewardFile("AIR CANADA", new Date(), "/tmp/aircanada_points-20051017-085318");
//            rewardServiceEJB.rePostRewardFile("AMERICA WEST", new Date(), "none");
//            rewardServiceEJB.rePostRewardFile("HILTON HOTELS", new Date(), "/tmp/HILTON092005.TXT-20051026-155119");
//     
// Wed
//            rewardServiceEJB.rePostRewardFile("GREENPOINTS", new Date(), "/tmp/ftd_-20051007-173934");
//            rewardServiceEJB.rePostRewardFile("MIDWEST AIRLINES", new Date(), "/tmp/midwest.txt-20051010-080543");
//            rewardServiceEJB.rePostRewardFile("QUIXTAR/FLORAGIFT", new Date(), "/tmp/FTD_quixtar_10272005_185524");
//            rewardServiceEJB.rePostRewardFile("UNITED AIRLINES", new Date(), "/tmp/FTD_ACTIVITY-20051027-223325");
      
      // Thurs      
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -1);
//            rewardServiceEJB.rePostRewardFile("AMERICAN EXPRESS", cal.getTime(), "/u02/apollo/partner-reward/american-express/outbound/amexrewards.5842-20051103-132119");
//            rewardServiceEJB.rePostRewardFile("CENDANT HOTELS", new Date(), "/tmp/ftd-20051024-180757");

// Fri
//            rewardServiceEJB.rePostRewardFile("AMERICAN AIRLINES", new Date(), "/tmp/FTDPOST.DAT-20051018-180231");
//            rewardServiceEJB.rePostRewardFile("NORTHWEST AIRLINES", new Date(), "/tmp/activ20.txt-20051019-134018");
//            rewardServiceEJB.rePostRewardFile("US AIRWAYS", new Date(), "/tmp/FTD_POST-20051011-091156");

// Others
//            rewardServiceEJB.rePostRewardFile("VIRGIN AIRWAYS", new Date(), "/tmp/virgin.txt-20051006-082640");
            rewardServiceEJB.rePostRewardFile("DISCOVER", new Date(), "/tmp/discover-20051021-112236");
            
//            rewardServiceEJB.rePostRewardFile("BASS HOTELS", new Date(), "/tmp/BASS_FTD_BONUS.TXT-20051025-154149");
//            rewardServiceEJB.rePostRewardFile("BEST WESTERN", new Date(), "/tmp/FTD-20051025-154040");
//            rewardServiceEJB.rePostRewardFile("DELTA AIRLINES", new Date(), "/tmp/DELTA.txt-20051017-082648");            
//            rewardServiceEJB.rePostRewardFile("HAWAIIAN AIRLINES", new Date(), "/tmp/HAWA-20051012-121955");
//            

//rewardServiceEJB.reTransmitRewardFile("ALASKA AIRLINES", "/tmp/midwest.txt", "midwesttest.txt");
//rewardServiceEJB.reTransmitRewardFile("AMERICA WEST", "/tmp/FTDPOST-20050929-110448", "FTDTest.txt");
            // Call any of the Remote methods below to access the EJB
//             rewardServiceEJB.getReward( java.lang.String orderDetailId, java.util.Date activeDate );
//            System.out.println("Reward: " + rewardServiceEJB.getReward("1492502"));
//            System.out.println("Reward: " + rewardServiceEJB.getReward("102146"));
//            System.out.println("Reward: " + rewardServiceEJB.getReward("102810"));
//            System.out.println("Reward: " + rewardServiceEJB.getReward("102382"));
//            System.out.println("Reward: " + rewardServiceEJB.getReward("102492"));
//            Calendar futureDate = Calendar.getInstance();
//            futureDate.add(Calendar.DATE, 50);
// UNITED AIRLINES
// AMERICAN AIRLINES
// ALASKA AIRLINES
// AMERICA WEST
// BEST WESTERN
// BASS HOTELS
// CENDANT HOTELS
// DELTA AIRLINES
// HAWAIIAN AIRLINES
// UPROMISE
//            rewardServiceEJB.postRewardFile("QUIXTAR/FLORAGIFT", new Date());
//            rewardServiceEJB.postRewardFile("UNITED AIRLINES", new Date());
//if (TransportServiceBO.getFtpAdapter().prDownload(
//"whatever.txt", "FTDBILL.DAT", "/u02/reports/partner-posting/american-airlines", "stheno-dev", "reports", "reports", false)) 
//{
//  System.out.println("file obtained!!!");
  
//  IController controller = ControllerFactory.getInstance("AMERICAN AIRLINES", (String) null);
//
//        // Start the workflow.
//        controller.execute();
//} else 
//{
//  System.err.println("file not obtained.");
//}
//            rewardServiceEJB.getResponseFile("NORTHWEST AIRLINES", "nodeResponseExtract");
//            rewardServiceEJB.getResponseFile("AIR CANADA", "nodeResponseExtractBilling");
//            rewardServiceEJB.getResponseFile("AIR CANADA", "nodeResponseExtractError");
//            rewardServiceEJB.getResponseFile("AIR CANADA", "nodeResponseExtractDecode");
//            rewardServiceEJB.getResponseFile("AIR CANADA", "nodeResponseExtractReject");
//            rewardServiceEJB.getResponseFile("BASS HOTELS", "nodeResponseExtractConfirm");
//            rewardServiceEJB.getResponseFile("BASS HOTELS", "nodeResponseExtractError");
//            rewardServiceEJB.getResponseFile("AMERICAN AIRLINES", "nodeResponseExtractConfirm");
//            rewardServiceEJB.getResponseFile("AMERICAN AIRLINES", "nodeResponseExtractBilling");
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private static Context getInitialContext() throws NamingException {
        Hashtable env = new Hashtable();

        // Standalone OC4J connection details
        env.put(Context.INITIAL_CONTEXT_FACTORY,
            "com.evermind.server.rmi.RMIInitialContextFactory");
        env.put(Context.SECURITY_PRINCIPAL, "oc4jadmin");
        env.put(Context.SECURITY_CREDENTIALS, "lli7tst");
//        env.put(Context.PROVIDER_URL, "opmn:ormi://hercules-dev:6003:OC4J_PARTNER_REWARD/partner-reward");
//        env.put(Context.PROVIDER_URL, "ormi://hercules-dev:3201/partner-reward");
//        env.put(Context.PROVIDER_URL, "ormi://illiad-prod:3214/partner-reward");
        env.put(Context.PROVIDER_URL, "opmn:ormi://medusa:6003:OC4J_PARTNER_REWARD/partner-reward");

        return new InitialContext(env);
//return new InitialContext();
    }
}
