package com.ftd.partnerreward.client;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBLocal;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBLocalHome;

import java.util.StringTokenizer;
import javax.naming.Context;
import javax.naming.InitialContext;


public class BatchResponseClient extends EventHandler {
    protected Logger logger = new Logger(BatchResponseClient.class.getName());

    public BatchResponseClient() {
        super();
    }

    public void invoke(Object payload) throws Throwable {
        try {
            // The CDATA of the payload is implemented as a MessageToken.
            MessageToken event = (MessageToken) payload;
            String message = (String) event.getMessage();
            String partnerName = null;
            StringTokenizer st = new StringTokenizer(message, "|");

            if (st.hasMoreTokens()) {
                partnerName = st.nextToken();
            } else {
                throw new RuntimeException(
                    "invoke: A partner name value is required.");
            }
            
            String workflowNodeName = null;
            if (st.hasMoreTokens()) {
                workflowNodeName = st.nextToken();
            }

            // Obtain the EJB service interface.
            Context jndiContext = new InitialContext();

            try {
                RewardServiceEJBLocalHome home = (RewardServiceEJBLocalHome) jndiContext.lookup("java:comp/env/ejb/local/RewardServiceEJB");

                // create an EJB instance 
                RewardServiceEJBLocal rsEjb = home.create();

                // Post a reward file.
                rsEjb.getResponseFile(partnerName, workflowNodeName);
            } finally {
                jndiContext.close();
            }
        } catch (Throwable t) {
            logger.error("invoke: PARTNER-REWARD CONFIRMATION-FILE event failed.",
                t);
            AlertService.processError(t);
        }
    }
}
