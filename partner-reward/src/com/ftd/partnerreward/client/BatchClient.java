package com.ftd.partnerreward.client;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.ejb.RewardServiceEJB;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBHome;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBLocal;
import com.ftd.partnerreward.core.ejb.RewardServiceEJBLocalHome;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;


public class BatchClient extends EventHandler {
    protected Logger logger = new Logger(BatchClient.class.getName());

    public BatchClient() {
        super();
    }

    public void invoke(Object payload) throws Throwable {
        try {
            // The CDATA of the payload is implemented as a MessageToken.
            MessageToken event = (MessageToken) payload;
            String message = (String) event.getMessage();

            // The message is pipe "|" delimited:
            // 1. partnerName (required)
            // 2. endDate in format MMddyyyy
            String partnerName = null;
            Date endDate = null;
            StringTokenizer st = new StringTokenizer(message, "|");

            if (st.hasMoreTokens()) {
                partnerName = st.nextToken();
            } else {
                throw new RuntimeException(
                    "invoke: A partner name value is required.");
            }

            if (st.hasMoreTokens()) {
                SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
                endDate = sdf.parse(st.nextToken());
            } else {
                // Note that since we deal in zero-time, the current date
                // is decremented by one.
                Calendar yesterday = Calendar.getInstance();
                yesterday.add(Calendar.DATE, -1);
                endDate = yesterday.getTime();
            }

            // Obtain the EJB service interface.
            Context jndiContext = new InitialContext();

            try {
                RewardServiceEJBLocalHome home = (RewardServiceEJBLocalHome) jndiContext.lookup("java:comp/env/ejb/local/RewardServiceEJB");

                // create an EJB instance 
                RewardServiceEJBLocal rsEjb = home.create();

                // Post a reward file.
                rsEjb.postRewardFile(partnerName, endDate);
            } finally {
                jndiContext.close();
            }
        } catch (Throwable t) {
            logger.error("invoke: PARTNER-REWARD POST-FILE-CREATION event failed.",
                t);
            AlertService.processError(t);
        }
    }
}
