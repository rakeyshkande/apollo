package com.ftd.partnerreward.core.controller;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.text.TextUtil;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;


abstract class BaseController implements IController {
    private static final Map _PROCESS_DEFINITION_CACHE = new HashMap();
    private ProcessInstance processInstance = null;

    protected BaseController(String partnerName,
        boolean useProcessDefinitionCache) {
        // Clean-up the partner name string.
        String cleanPartnerName = TextUtil.cleanPartnerName(partnerName);
        String pdFileName = "pd-" + cleanPartnerName + ".xml";

        // This cache is provided for performance reasons for any implementation
        // that wishes to use it (i.e. the AdHocController implementation).
        ProcessDefinition processDefinition = null;

        if (useProcessDefinitionCache) {
            // Check the cache to see if the ProcessDefinition has already been parsed.
            processDefinition = (ProcessDefinition) _PROCESS_DEFINITION_CACHE.get(cleanPartnerName);

            if (processDefinition == null) {
                // Create and add to cache.
                processDefinition = ProcessDefinition.parseXmlResource(pdFileName);
                _PROCESS_DEFINITION_CACHE.put(cleanPartnerName,
                    processDefinition);
            }
        } else {
            processDefinition = ProcessDefinition.parseXmlResource(pdFileName);
        }

        processInstance = new ProcessInstance(processDefinition);
        
        // Bind the partner name to the context.
        processInstance.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME,
                    partnerName); 
    }

    protected ProcessInstance getProcessInstance() {
        return processInstance;
    }
}
