package com.ftd.partnerreward.core.controller;

import java.util.Date;


public class ControllerFactory {
    public static IController getInstance(String partnerName,
        String orderDetailId, Date activeDate) {
        return new AdHocController(partnerName, orderDetailId, activeDate);
    }

    public static IController getInstance(String partnerName, Date endDate,
        Date startDate) {
        return new BatchController(partnerName, endDate, startDate);
    }

    public static IController getInstance(String partnerName, String workflowNodeName) {
        return new BatchResponseController(partnerName, workflowNodeName);
    }

    public static IController getInstance(String partnerName, Date endDate) {
        return getInstance(partnerName, endDate, null);
    }

    public static IController getInstance(String partnerName,
        String fullLocalFileName, String remoteFileName) {
        if ((partnerName == null) || (fullLocalFileName == null) ||
                (remoteFileName == null)) {
            throw new RuntimeException(
                "getInstance: Input parameter can not be null.");
        }

        return new BatchTransportController(partnerName, fullLocalFileName,
            remoteFileName);
    }
}
