package com.ftd.partnerreward.core.controller;

import com.ftd.partnerreward.core.IWorkflowConstants;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ExecutionContext;
import org.jbpm.graph.exe.ProcessInstance;

import java.util.Date;
import org.jbpm.graph.exe.Token;


class BatchTransportController extends BaseController {

    private String fullLocalFileName;
    private String remoteFileName;
    
    protected BatchTransportController(String partnerName, String fullLocalFileName, String remoteFileName) {
        super(partnerName, false);
        this.fullLocalFileName = fullLocalFileName;
        this.remoteFileName = remoteFileName;
    }

    public Object execute() {
        // Bind the start and end dates to the context.
        ContextInstance context = getProcessInstance().getContextInstance();

        // Bind the "end" indicator to the context.
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_END_IND,
            new Boolean(true));
        
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, fullLocalFileName);

        // Start the workflow from the response file check node.
        Token token = getProcessInstance().getRootToken();
        token.setNode(getProcessInstance().getProcessDefinition().getNode("nodeTransport"));
        token.getNode().enter(new ExecutionContext(token));

        return null;
    }
}
