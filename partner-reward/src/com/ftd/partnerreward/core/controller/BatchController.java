package com.ftd.partnerreward.core.controller;

import com.ftd.partnerreward.core.IWorkflowConstants;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;

import java.util.Date;


class BatchController extends BaseController {
    private Date startDate;
    private Date endDate;

    protected BatchController(String partnerName, Date endDate, Date startDate) {
        super(partnerName, false);
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Object execute() {
        // Bind the start and end dates to the context.
        ContextInstance context = getProcessInstance().getContextInstance();
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE, endDate);

        if (startDate != null) {
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE,
                startDate);
        }

        // Start the workflow from the beginning.
        getProcessInstance().signal();

        return null;
    }
}
