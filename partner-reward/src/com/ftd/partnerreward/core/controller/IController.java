package com.ftd.partnerreward.core.controller;

public interface IController {
    public Object execute();
}
