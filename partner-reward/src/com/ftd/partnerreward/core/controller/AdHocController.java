package com.ftd.partnerreward.core.controller;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import org.jbpm.context.exe.ContextInstance;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jbpm.graph.exe.ExecutionContext;
import org.jbpm.graph.exe.Token;


class AdHocController extends BaseController {
    private String orderDetailId;
    private Date activeDate;

    protected AdHocController(String partnerName, String orderDetailId,
        Date activeDate) {
        super(partnerName, true);
        this.orderDetailId = orderDetailId;
        this.activeDate = activeDate;
    }

    public Object execute() {
        ContextInstance context = getProcessInstance().getContextInstance();

        // Bind the "end" indicator to the context.
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_END_IND,
            new Boolean(true));

        // Bind the active date to the context.
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE,
            activeDate);

        // Bind the single requested order detail id to the context.
        List orderDetailIdList = new ArrayList();
        orderDetailIdList.add(orderDetailId);
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT,
            orderDetailIdList);

        // Only execute the calculation step.
//        getProcessInstance().getRootToken().setNode(getProcessInstance()
//                                                        .getProcessDefinition()
//                                                        .getNode("nodeExtract"));
//
//        // Advance the process token.
//        getProcessInstance().getRootToken().signal();
        Token token = getProcessInstance().getRootToken();
        token.setNode(getProcessInstance().getProcessDefinition().getNode("nodeCalculate"));
        token.getNode().enter(new ExecutionContext(token));

        // Obtain the calculation result from the context.
        Float result = new Float(0);
        List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

        if ((rewardList != null) && (rewardList.size() > 0)) {
            OrderDetailVO odVO = (OrderDetailVO) rewardList.get(0);
            result = new Float(odVO.getRewardTotal());
        }

        return result;
    }
}
