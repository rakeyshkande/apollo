package com.ftd.partnerreward.core.controller;

import com.ftd.partnerreward.core.IWorkflowConstants;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ExecutionContext;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;

import java.util.Date;


class BatchResponseController extends BaseController {
    private String workflowNodeName;

    protected BatchResponseController(String partnerName,
        String workflowNodeName) {
        super(partnerName, false);
        this.workflowNodeName = workflowNodeName;
    }

    public Object execute() {
        // Bind the start and end dates to the context.
        ContextInstance context = getProcessInstance().getContextInstance();

        // Start the workflow from the response file check node.
        Token token = getProcessInstance().getRootToken();

        if (workflowNodeName == null) {
            // This is the default starting node for responses.
            workflowNodeName = "nodeResponseExtract";
        }

        //        token.setNode(getProcessInstance().getProcessDefinition().getNode("nodeResponseExtract"));
        token.setNode(getProcessInstance().getProcessDefinition().getNode(workflowNodeName));
        token.getNode().enter(new ExecutionContext(token));

        return null;
    }

    public String getWorkflowNodeName() {
        return workflowNodeName;
    }
}
