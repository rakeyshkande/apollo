package com.ftd.partnerreward.core;

public interface IWorkflowConstants {
//    public static final String CONTEXT_KEY_PROCESS_TYPE = "keyProcessType";
//    public static final String CONTEXT_VALUE_PROCESS_TYPE_BATCH = "batch";
//    public static final String CONTEXT_VALUE_PROCESS_TYPE_AD_HOC = "adHoc";
    /*****************************************************/
    public static final String CONTEXT_TRANSITION_END = "end";
    public static final String CONTEXT_TRANSITION_ERROR = "error";
    public static final String CONTEXT_TRANSITION_RELOOP = "reloop";
    public static final String CONTEXT_TRANSITION_EXTRACT_COMPLETE = "complete";
    /*****************************************************/
    public static final String CONTEXT_KEY_PARTNER_NAME = "keyPartnerName";
    public static final String CONTEXT_KEY_POSTED_FILE_NAME = "keyPostedFileName";
//    public static final String CONTEXT_KEY_POSTED_FILE_NAME_LOCAL = "keyPostedFileNameLocal";
    public static final String CONTEXT_KEY_POSTED_FILE_PATH_LOCAL = "keyPostedFilePathLocal";
    public static final String CONTEXT_KEY_POSTED_FILE_DATE = "keyPostedFileDate";
    public static final String CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL = "keyResponseFilePathLocal";
    public static final String CONTEXT_KEY_EXTRACT_COMPLETE_IND = "keyExtractCompleteInd";
    public static final String CONTEXT_KEY_END_IND = "keyEndInd";
    public static final String CONTEXT_KEY_START_DATE = "keyStartDate";
    public static final String CONTEXT_KEY_END_DATE = "keyEndDate";
    public static final String CONTEXT_KEY_ACTIVE_DATE = "keyActiveDate";
//    public static final String CONTEXT_KEY_CURRENT_RECORD_COUNT = "keyCurrentRecordCount";
//    public static final String CONTEXT_KEY_TOTAL_DAILY_RECORD_COUNT = "keyTotalDailyRecordCount";
    public static final String CONTEXT_KEY_TOTAL_ROLLING_RECORD_COUNT = "keyTotalRollingRecordCount";
    public static final String CONTEXT_KEY_EMPTY_RESULT_SET = "keyEmptyResultSet";
    /*****************************************************/
    public static final String CONTEXT_KEY_EXTRACTION_RESULT = "keyOrderDetailIdList";
    public static final String CONTEXT_KEY_CALCULATION_RESULT = "keyRewardResult";
    public static final String CONTEXT_KEY_TRANSFORMATION_RESULT = "keyTransformationResult";
    /*****************************************************/
    public static final String GLOBAL_PARMS_CONTEXT = "PARTNER_REWARD";
    public static final String GLOBAL_PARMS_NAME = "DAYS_PAST_DELIVERY_FOR_POSTING";
    /*****************************************************/
    public static final String SEPARATE_BONUS_DETAIL_LINE_VALUE = "D";
    /*****************************************************/
    
}
