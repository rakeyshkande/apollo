package com.ftd.partnerreward.core.alert;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import com.ftd.partnerreward.core.dao.ReportingDAO;
import com.ftd.partnerreward.core.dao.UtilDAO;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;


public class AlertService {
    protected static final String _ALERT_TYPE_UNRECOVERABLE_FAILURE = "CRITICAL WARNING";
    protected static final String _ALERT_TYPE_DATA_WARNING = "INFO";
    protected static Logger logger = new Logger(AlertService.class.getName());

    public static void processError(Throwable t) {
        // Notify operations of the error.        
        notifyOperations("PARTNER REWARD", _ALERT_TYPE_UNRECOVERABLE_FAILURE,
            getStackTrace(t), null);
    }

    public static void processAlert(String projectName, String subject,
        String msg) {
        // Notify operations of the event.        
        notifyOperations(projectName, _ALERT_TYPE_DATA_WARNING, msg, subject);
    }

    //    public static void processError(String type, Throwable t) {
    //        processError(type, t, null);
    //    }
    protected static void notifyOperations(String originName, String type,
        String message, String subject) {
        SystemMessenger sm = SystemMessenger.getInstance();
        SystemMessengerVO vo = new SystemMessengerVO();

        vo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        vo.setSource(originName);
        vo.setType(type);

        if (subject != null) {
            vo.setSubject(subject);
        }

        vo.setMessage(message);

        Connection dbConn = UtilDAO.getDbConnection();

        try {
            logger.debug("notifyOperations: alertSubject=" + vo.getSubject() +
                " alertBody=" + vo.getMessage());

            String systemMsgId = null;

            if ((systemMsgId = sm.send(vo, dbConn, false)) == null) {
                throw new RuntimeException(
                    "notifyOperations: Null system message ID for " +
                    vo.getSubject());
            }

            //logger.debug("notifyOperations: alert Result=" + ReportingDAO.insertMessage(vo));
            logger.info("notifyOperations: alertSubject=" + vo.getSubject() + 
            " alertBody=" + vo.getMessage() +
            " systemMessageID=" + systemMsgId);
        } catch (Exception e) {
            throw new RuntimeException(
                "notifyOperations: Could not notify operations of alert: " +
                message, e);
        } finally {
            try {
                dbConn.close();
            } catch (Exception e) {
                logger.error("notifyOperations: JDBC clean-up failed.", e);
            }
        }
    }

    //    private static Connection getDbConnection() {
    //        Connection conn = null;
    //
    //        try {
    //            // Note: This will obtain the InitialContext of the container
    //            // in which this class is executed.
    //            Context jndiContext = new InitialContext();
    //
    //            try {
    //                DataSource ds = (DataSource) jndiContext.lookup(_ALIAS_DATA_SOURCE);
    //                conn = ds.getConnection();
    //            } finally {
    //                jndiContext.close();
    //            }
    //        } catch (Exception e) {
    //            throw new RuntimeException(
    //                "getDbConnection: Could not obtain connection for " +
    //                _ALIAS_DATA_SOURCE + " datasource.", e);
    //        }
    //
    //        return conn;
    //    }
    //    public static boolean isOracleRACException(Exception e, Session session) {
    //        if (e instanceof JMSException && session instanceof AQjmsSession) {
    //            SQLException sqlExc = (SQLException) ((JMSException) e).getLinkedException();
    //
    //            try {
    //                Connection dbConn = ((AQjmsSession) session).getDBConnection();
    //
    //                return DbUtil.oracleFatalError(sqlExc, dbConn);
    //            } catch (JMSException je) {
    //                logger.error(
    //                    "isOracleRACException: Could not determine type of JMS error.");
    //            }
    //        }
    //
    //        return false;
    //    }
    public static String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();

        return sw.toString();
    }
}
