package com.ftd.partnerreward.core.dao;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Manages the state of and access to an FTP connection
 *
 */
public class FtpAdapter implements IFtpAdapter {
    private static Logger logger = new Logger(FtpAdapter.class.getName());

    public FtpAdapter() {
        super();
    }

    public void prUpload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary, boolean isOverwriteProtected,
        String overwriteProtectedFileName) {
        try {
            
  
            logger.debug("Attempting connection to ftpServer " + ftpServer + " remote file is " + remoteFileName );
            

            
            FTPClient client = createSession(ftpServer, remoteFilePath,
                    username, password, isBinary);
            

            try {
                // Check if a file already exists.
                String testFile = remoteFileName;

                if (overwriteProtectedFileName != null) {
                    testFile = overwriteProtectedFileName;
                }

                boolean exists = false;
                
                try {
                  exists = FtpAdapter.fileExists(client.dir(), testFile);
                } catch (Throwable e) {
                  // Some destination servers, such as those having a VMS OS,
                  // will return a "file not found" error if the directory
                  // is empty and client.dir()
                  // issues an "ls" rather than "dir" command.
                  // Ignore this error.
                }

                if (exists && !isOverwriteProtected) {
                    // As a general rule, delete any existing file, since
                    // we do not want to rely on overwrite privileges.
                    try {
                        client.delete(testFile);
                        logger.warn("prUpload: Existing file deleted: " +
                            testFile);
                    } catch (Throwable t) {
                        // do nothing.
                    }
                } else if (exists && isOverwriteProtected) {
                    logger.warn(
                        "prUpload: Existing file can not be overwritten: " +
                        testFile);

                    return;
                }

                client.put(localFullFileName, remoteFileName);
            } finally {
                client.quit();
            }
        } catch (Exception e) {
            
            logger.error(e.getMessage());
            throw new RuntimeException("prUpload: Failed.", e);
            
        }
    }

    private static boolean fileExists(String[] fileNameArray,
        String remoteFileName) {
        boolean exists = false;

        if (fileNameArray != null) {
            for (int index = 0; index < fileNameArray.length; index++) {
                String currentFileName = fileNameArray[index];

                if (currentFileName.equals(remoteFileName)) {
                    exists = true;

                    break;
                }
            }
        }

        return exists;
    }

    public boolean prDownload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary) {
        boolean exists = false;

        try {
            FTPClient client = createSession(ftpServer, remoteFilePath,
                    username, password, isBinary);

            try {
                exists = FtpAdapter.fileExists(client.dir(), remoteFileName);

                if (exists) {
                    // Get the file.
                    client.get(localFullFileName, remoteFileName);
                }
            } finally {
                client.quit();
            }
        } catch (Exception e) {
            throw new RuntimeException("prDownload: Failed.", e);
        }

        return exists;
    }

    private FTPClient createSession(String ftpServer, String ftpLocation,
        String username, String password, boolean isBinary)
        throws IOException, FTPException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering login");
            logger.debug("ftpLocation : " + ftpLocation);
            logger.debug("Opening FTP connection to " + ftpServer +
                " as user " + username);
        }

        /* connect to FTP server */
        FTPClient ftpClient = new FTPClient(ftpServer);
        ftpClient.login(username, password);
        ftpClient.chdir(ftpLocation);
        ftpClient.setConnectMode(FTPConnectMode.PASV);

        /* set the transfer type */
        if (isBinary) {
            ftpClient.setType(FTPTransferType.BINARY);
        } else {
            ftpClient.setType(FTPTransferType.ASCII);
        }

        return ftpClient;
    }

  public List prDownloadFiles(String localFilePath, String remoteFileNamePattern, String remoteFilePath, 
        String ftpServer, String userName, String password, boolean isBinary) {
        
        List resultList = new ArrayList();
        try {
            FTPClient client = createSession(ftpServer, remoteFilePath,
                    userName, password, isBinary);

            try {
                List remoteFiles = getFilesList(client.dir(), remoteFileNamePattern);

                for (Iterator it = remoteFiles.iterator(); it.hasNext();) {
                    // Get the file.
                    String remoteFileName = (String) it.next();
                    String localFullFileName = localFilePath + File.separator + remoteFileName;
                    client.get(localFullFileName, remoteFileName);
                    resultList.add(localFullFileName);
                }
            } finally {
                client.quit();
            }
        } catch (Exception e) {
            throw new RuntimeException("prDownload: Failed.", e);
        }
        
        return resultList;
  }
  
  private List getFilesList(String[] fileNameArray,
        String remoteFileNamePattern) {
        List resultList = new ArrayList();

        if (fileNameArray != null) {
            for (int index = 0; index < fileNameArray.length; index++) {
                String currentFileName = fileNameArray[index];

                if (currentFileName.startsWith(remoteFileNamePattern)) {
                    resultList.add(currentFileName);
                }
            }
        }

        return resultList;
    }
}
