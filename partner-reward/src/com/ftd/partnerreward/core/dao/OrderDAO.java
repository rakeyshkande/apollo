package com.ftd.partnerreward.core.dao;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.vo.GlobalParameterVO;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import org.xml.sax.SAXException;

import java.io.IOException;

import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;


public class OrderDAO {
    protected static Logger logger = new Logger(OrderDAO.class.getName());
    protected final static String DB_SELECT_ORDER_DETAIL_ID = "PARTNERREWARD.SELECT_ORDER_DETAIL_ID";
    protected final static String DB_SELECT_ORDER_DETAIL = "PARTNERREWARD.SELECT_ORDER_DETAIL";
    protected final static String DB_SELECT_ORDER_TOTALS = "PARTNERREWARD.SELECT_ORDER_TOTALS";
    protected final static String DB_GET_GLOBAL_PARM_VALUE = "GET_GLOBAL_PARM_VALUE";

    // Note: The column lookups below are a SUBSET of the physical table.
    // There are several physical "legacy" columns which are not used or referenced
    // in this app.
    protected final static String DB_IN_ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
    protected final static String DB_IN_ID_STR = "IN_ID_STR";
    protected final static String DB_IN_DATE = "IN_DATE";
    protected final static String DB_IN_ORDER_DATE = "IN_ORDER_DATE";
    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
    protected final static String DB_IN_START_POSITION = "IN_START_POSITION";
    protected final static String DB_IN_MAX_NUMBER_RETURNED = "IN_MAX_NUMBER_RETURNED";
    protected final static String DB_IN_DAYS_PAST_DELIVERY = "IN_DAYS_PAST_DELIVERY";
    protected final static String DB_OUT_CURSOR = "OUT_CURSOR";
    protected final static String DB_OUT_CUR = "OUT_CUR";
    protected final static String DB_OUT_MAX_ROWS = "OUT_MAX_ROWS";
    protected final static String DB_GLOBAL_PARMS_CONTEXT = "CONTEXT";
    protected final static String DB_GLOBAL_PARMS_NAME = "NAME";
//    protected final static String DB_IN_CREATED_BY = "IN_CREATED_BY";
//    protected final static String DB_IN_UPDATED_BY = "IN_UPDATED_BY";
//    protected final static String DB_IN_UPDATED_ON_START = "IN_UPDATED_ON_START";
//    protected final static String DB_IN_UPDATED_ON_END = "IN_UPDATED_ON_END";
//    protected final static String DB_IN_SOURCE_CODE = "IN_SOURCE_CODE";
//    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
//    protected final static String DB_IN_DESCRIPTION = "IN_DESCRIPTION";
//    protected final static String DB_IN_SNH_ID = "IN_SNH_ID";
//    protected final static String DB_IN_PRICE_HEADER_ID = "IN_PRICE_HEADER_ID";
//    protected final static String DB_IN_START_DATE = "IN_START_DATE";
//    protected final static String DB_IN_END_DATE = "IN_END_DATE";
//    protected final static String DB_IN_PAYMENT_METHOD_ID = "IN_PAYMENT_METHOD_ID";
//    protected final static String DB_IN_DEFAULT_SOURCE_CODE_FLAG = "IN_DEFAULT_SOURCE_CODE_FLAG";
//    protected final static String DB_IN_BILLING_INFO_PROMPT = "IN_BILLING_INFO_PROMPT";
//    protected final static String DB_IN_BILLING_INFO_LOGIC = "IN_BILLING_INFO_LOGIC";
//    protected final static String DB_IN_EXTERNAL_CALL_CENTER = "IN_EXTERNAL_CALL_CENTER";
//    protected final static String DB_IN_HIGHLIGHT_DESCRIPTION_FLAG = "IN_HIGHLIGHT_DESCRIPTION_FLAG";
//    protected final static String DB_IN_DISCOUNT_ALLOWED_FLAG = "IN_DISCOUNT_ALLOWED_FLAG";
//    protected final static String DB_IN_BIN_NUMBER_CHECK_FLAG = "IN_BIN_NUMBER_CHECK_FLAG";
//    protected final static String DB_IN_ORDER_SOURCE = "IN_ORDER_SOURCE";
//    protected final static String DB_IN_JCPENNEY_FLAG = "IN_JCPENNEY_FLAG";
//    protected final static String DB_IN_COMMENT_TEXT = "IN_COMMENT_TEXT";
//    protected final static String DB_IN_SOURCE_PROGRAM_REF_ID = "IN_SOURCE_PROGRAM_REF_ID";

    //    protected final static String DB_IN_OE_DEFAULT_FLAG = "IN_OE_DEFAULT_FLAG";
//    protected final static String DB_IN_COMPANY_ID = "IN_COMPANY_ID";
//    protected final static String DB_IN_SEND_TO_SCRUB = "IN_SEND_TO_SCRUB";
//    protected final static String DB_IN_FRAUD_FLAG = "IN_FRAUD_FLAG";
//    protected final static String DB_IN_ENABLE_LP_PROCESSING = "IN_ENABLE_LP_PROCESSING";
//    protected final static String DB_IN_EMERGENCY_TEXT_FLAG = "IN_EMERGENCY_TEXT_FLAG";
//    protected final static String DB_IN_REQUIRES_DELIVERY_CONFIRMATION = "IN_REQUIRES_DELIVERY_CONFIRMATION";
//    protected final static String DB_IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
//    protected final static String DB_IN_PROMOTION_CODE = "IN_PROMOTION_CODE";
//    protected final static String DB_IN_BONUS_PROMOTION_CODE = "IN_BONUS_PROMOTION_CODE";
//    protected final static String DB_IN_REQUESTED_BY = "IN_REQUESTED_BY";
//    protected final static String DB_IN_OVER_21_FLAG = "IN_OVER_21_FLAG";
//    protected final static String DB_IN_RELATED_SOURCE_CODE = "IN_RELATED_SOURCE_CODE";
//    protected final static String DB_IN_SAME_DAY_GIFT_FLAG = "IN_SAME_DAY_GIFT_FLAG";
//    protected final static String DB_IN_PROGRAM_WEBSITE_URL = "IN_PROGRAM_WEBSITE_URL";
//    protected final static String DB_IN_SORT_BY = "IN_SORT_BY";
//    protected final static String DB_IN_SOURCE_CODE_BEGIN = "IN_SOURCE_CODE_BEGIN";
//    protected final static String DB_IN_SOURCE_CODE_END = "IN_SOURCE_CODE_END";
//    protected final static String DB_IN_STATUS = "IN_STATUS";
//    protected final static String DB_IN_SOURCE_CODE_ID = "IN_SOURCE_CODE_ID";
//    protected final static String DB_IN_CREDIT_CARD_TYPE = "IN_CREDIT_CARD_TYPE";
//    protected final static String DB_IN_CREDIT_CARD_NUMBER = "IN_CREDIT_CARD_NUMBER";
//    protected final static String DB_IN_CREDIT_CARD_EXPIRATION_DATE = "IN_CREDIT_CARD_EXPIRATION_DATE";

    /**
     * constructor to prevent clients from instantiating.
     */
    private OrderDAO() {
        super();
    }
    
    public static CachedResultSet queryOrderTotals(String orderDetailId) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(1);
        inputParams.put(DB_IN_ID_STR, orderDetailId);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ORDER_TOTALS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryOrderTotals: Could not obtain order totals from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }

    public static CachedResultSet queryOrderDetail(String orderDetailId) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(1);
        inputParams.put(DB_IN_ORDER_DETAIL_ID, orderDetailId);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ORDER_DETAIL,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryOrderDetails: Could not obtain order details from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }

    public static CachedResultSet queryOrderDetailIds(String partnerName, Date activeDate, 
        Date orderDateThreshold, int recordStart, int groupSize, String daysPastDelivery, List resultRecordCount) {
        CachedResultSet result = null;
        // Build db stored procedure input parms
        Map inputParams = new HashMap(4);
        inputParams.put(DB_IN_PARTNER_NAME, partnerName);
        inputParams.put(DB_IN_DATE, new java.sql.Date(activeDate.getTime()));
        inputParams.put(DB_IN_ORDER_DATE, new java.sql.Date(orderDateThreshold.getTime()));
        inputParams.put(DB_IN_START_POSITION, new Integer(recordStart));
        inputParams.put(DB_IN_MAX_NUMBER_RETURNED, new Integer(groupSize));
        inputParams.put(DB_IN_DAYS_PAST_DELIVERY, new Integer(daysPastDelivery));

        // Submit the request to the database.
        try {
            Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_ORDER_DETAIL_ID,
                inputParams, false);
//for(Iterator i = compositeResult.keySet().iterator(); i.hasNext();) 
//{
//  Object key = i.next();
//  logger.debug("resultKey=" + key);
//  logger.debug("valueClass=" + compositeResult.get(key).getClass().getName());
//  
//}
                result = (CachedResultSet) compositeResult.get(DB_OUT_CURSOR);

                // Record the total records returned.
                // Verify that the total record count variable
                // was populated.
                resultRecordCount.add(0, new Integer(((BigDecimal) compositeResult.get(DB_OUT_MAX_ROWS)).intValue()));
                
        } catch (Exception e) {
            String errorMsg = "queryOrderDetailIds: Could not obtain order detail IDs from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
        
        return result;
    }

  /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.  
   * It populates a global parameter VO based on the returned record set.
   * 
   * @param String - context
   * @param String - name
   * @return GlobalParameterVO 
   */
  public static GlobalParameterVO getGlobalParameter(String context, String name) throws Exception
  {    
    GlobalParameterVO param = new GlobalParameterVO();
    boolean isEmpty = true;
    String outputs = null;
    
    try
    {
      /* setup store procedure input parameters */
      Map inputParams = new HashMap();
      inputParams.put(DB_GLOBAL_PARMS_CONTEXT, context);
      inputParams.put(DB_GLOBAL_PARMS_NAME, name);
      
      outputs = (String) UtilDAO.submitDbRequest(DB_GET_GLOBAL_PARM_VALUE,
                  inputParams, false);
      if (outputs != null)
          isEmpty = false;
          param.setName(name);     
          param.setValue(outputs);      
    }
    catch (Exception e) 
    {            
        logger.error(e);
        throw e;
    } 
    if (isEmpty)
        return null;
    else
        return param;
  }
  
}
