package com.ftd.partnerreward.core.dao;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class PartnerProgramDAO {
    protected static Logger logger = new Logger(PartnerProgramDAO.class.getName());
    protected final static String DB_SELECT_PARTNER_PROGRAM_TERMS = "PARTNERREWARD.SELECT_PARTNER_PROGRAM_TERMS";
    protected final static String DB_SELECT_PROGRAM_REWARD_RANGE = "PARTNERREWARD.SELECT_PROGRAM_REWARD_RANGE";
    protected final static String DB_DECREMENT_PARTNER_BANK_BALANCE = "PARTNERREWARD.DECREMENT_PARTNER_BANK_BALANCE";
    protected final static String DB_INSERT_POSTED_REWARD = "PARTNERREWARD.INSERT_POSTED_REWARD";
    protected final static String DB_SELECT_PARTNER_SEQUENCE = "PARTNERREWARD.SELECT_PARTNER_SEQUENCE";
    protected final static String DB_SELECT_PARTNER_PARTIAL_REFUNDS = "PARTNERREWARD.SELECT_PARTNER_PARTIAL_REFUNDS";
    protected final static String DB_DELETE_POSTED_REWARD_ORDER_DETAIL_ID = "PARTNERREWARD.DELETE_POSTED_REWARD_ORDER_DETAIL_ID";
    protected final static String DB_DELETE_POSTED_REWARD_FILE_NAME = "PARTNERREWARD.DELETE_POSTED_REWARD_FILE_NAME";
    protected final static String DB_IN_SOURCE_CODE = "IN_SOURCE_CODE";
    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
    protected final static String DB_IN_SEQUENCE_TYPE = "IN_SEQUENCE_TYPE";
    protected final static String DB_IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
    protected final static String DB_IN_PROGRAM_TYPE = "IN_PROGRAM_TYPE";
    protected final static String DB_IN_AMOUNT = "IN_AMOUNT";
    protected final static String DB_IN_PARTNER_BANK_ID = "IN_PARTNER_BANK_ID";
    protected final static String DB_IN_DATE = "IN_DATE";
    protected final static String DB_IN_REFUND_DATE = "IN_REFUND_DATE";
    protected final static String DB_IN_NO_PARTNER_ID = "IN_NO_PARTNER_ID";
    protected final static String DB_IN_REWARD_BY_CART = "IN_REWARD_BY_CART";

    // Insertion parameters
    protected final static String DB_IN_ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
    protected final static String DB_IN_FILE_POST_DATE = "IN_FILE_POST_DATE";
    protected final static String DB_IN_FILE_RECORD_ID = "IN_FILE_RECORD_ID";
    protected final static String DB_IN_FILE_NAME = "IN_FILE_NAME";
    protected final static String DB_IN_REWARD = "IN_REWARD";
    protected final static String DB_IN_REWARD_DATE = "IN_REWARD_DATE";

    // Output parameters
    protected final static String DB_OUT_STATUS = "OUT_STATUS";
    protected final static String DB_OUT_MESSAGE = "OUT_MESSAGE";

    /**
     * constructor to prevent clients from instantiating.
     */
    private PartnerProgramDAO() {
        super();
    }

    public static CachedResultSet queryPartnerProgramTerms(String sourceCode,
        Date activeDate) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_SOURCE_CODE, sourceCode);
        inputParams.put(DB_IN_DATE, new java.sql.Date(activeDate.getTime()));

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_PARTNER_PROGRAM_TERMS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryPartnerProgramTerms: Could not obtain partner program terms from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }

    public static CachedResultSet queryProgramRewardRange(String programName) {
        CachedResultSet result = null;

        if (programName != null) {
            Map inputParams = new HashMap(1);

            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_NAME, programName);

            // Submit the request to the database.
            try {
                result = (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_PROGRAM_REWARD_RANGE,
                        inputParams, false);
            } catch (Exception e) {
                String errorMsg = "queryProgramRewardRange: Could not obtain program reward range from database.";
                logger.error(errorMsg, e);
                throw new RuntimeException(errorMsg, e);
            }
        }

        return result;
    }

    public static void decrementPartnerBankBalance(String partnerBankId,
        float decrement) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_PARTNER_BANK_ID, partnerBankId);
        inputParams.put(DB_IN_AMOUNT, new Float(decrement));

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_DECREMENT_PARTNER_BANK_BALANCE,
                    inputParams, false);
        } catch (Exception e) {
            String errorMsg = "decrementPartnerBankBalance: Could not decrement partner bank balance in database.";
            logger.warn(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(DB_OUT_STATUS);

        if ((status != null) && status.equals("N")) {
            throw new RuntimeException((String) result.get(DB_OUT_MESSAGE));
        }
    }

    public static void insertPostedRewards(String partnerName,
        String postFileName, Date postFileDate, List orderDetailVOList, boolean noPartnerId, boolean rewardByCart) {
        Map result = new HashMap(0);

        Connection dbConn = UtilDAO.getDbConnection();

        // Iterate through the List, persisting each record.
        try {
            for (Iterator i = orderDetailVOList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();

                // Build db stored procedure input parms
                Map inputParams = new HashMap(7);
                inputParams.put(DB_IN_ORDER_DETAIL_ID, odVO.getOrderDetailId());
                inputParams.put(DB_IN_FILE_POST_DATE,
                    new java.sql.Date(postFileDate.getTime()));
                inputParams.put(DB_IN_FILE_NAME, postFileName);

                inputParams.put(DB_IN_SOURCE_CODE, odVO.getSourceCode());
                inputParams.put(DB_IN_PROGRAM_NAME, odVO.getProgramName());
                inputParams.put(DB_IN_REWARD, new Float(odVO.getRewardTotal()));
                inputParams.put(DB_IN_REWARD_DATE,
                    new java.sql.Date(odVO.getDeliveryDate().getTime()));

                if (odVO.getPostFileRecordId() != null) {
                    inputParams.put(DB_IN_FILE_RECORD_ID,
                        odVO.getPostFileRecordId());
                }
                
                inputParams.put(DB_IN_NO_PARTNER_ID, noPartnerId? "Y" : "N");
                inputParams.put(DB_IN_REWARD_BY_CART, rewardByCart? "Y" : "N");

                // Submit the request to the database.
                try {
                    result = (Map) UtilDAO.submitDbRequest(DB_INSERT_POSTED_REWARD,
                            inputParams, dbConn);
                } catch (Exception e) {
                    String errorMsg = "insertPostedRewards: Could not insert posted rewards into database.";
                    logger.error(errorMsg, e);
                    throw new RuntimeException(errorMsg, e);

                }

                String status = (String) result.get(DB_OUT_STATUS);
                String message = (String) result.get(DB_OUT_MESSAGE);
                String m = "WARNING: No partner_program updated for program_name " + partnerName + ".";

                if ((status != null) && status.equals("N")) {
                    if(noPartnerId && message.equals(m)) {
                        // No partner id. Ignore failure resulted from updating program last posting date.
                        logger.warn(m);
                    } else {
                        throw new RuntimeException(message);
                    }
                }
            } // end of FOR loop
        } finally {
            try {
                dbConn.close();
            } catch (Throwable t) {
                logger.error("insertPostedRewards: DBConnection close failed.",
                    t);
            }
        }
    }

    public static void deletePostedRewardsByFileName(String postFileName) {
        Map result = new HashMap(0);

        Connection dbConn = UtilDAO.getDbConnection();

        // Iterate through the List, persisting each record.
        try {
            // Build db stored procedure input parms
            Map inputParams = new HashMap(1);
            inputParams.put(DB_IN_FILE_NAME, postFileName);

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_DELETE_POSTED_REWARD_FILE_NAME,
                        inputParams, dbConn);
            } catch (Exception e) {
                String errorMsg =
                    "deletePostedRewardsByFileName: Could not delete posted rewards from database for file name: " +
                    postFileName;
                logger.error(errorMsg, e);
                throw new RuntimeException(errorMsg, e);
            }

            String status = (String) result.get(DB_OUT_STATUS);

            if ((status != null) && status.equals("N")) {
                throw new RuntimeException((String) result.get(DB_OUT_MESSAGE));
            }
        } finally {
            try {
                dbConn.close();
            } catch (Throwable t) {
                logger.error("deletePostedRewardsByFileName: DBConnection close failed.",
                    t);
            }
        }
    }
    
    public static void deletePostedRewardsByOrderDetailId(String orderDetailId) {
        Map result = new HashMap(0);

        Connection dbConn = UtilDAO.getDbConnection();

        // Iterate through the List, persisting each record.
        try {
            // Build db stored procedure input parms
            Map inputParams = new HashMap(1);
            inputParams.put(DB_IN_ORDER_DETAIL_ID, orderDetailId);

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_DELETE_POSTED_REWARD_ORDER_DETAIL_ID,
                        inputParams, dbConn);
            } catch (Exception e) {
                String errorMsg =
                    "deletePostedRewardsByOrderDetailId: Could not delete posted rewards from database for order detail id: " +
                    orderDetailId;
                logger.error(errorMsg, e);
                throw new RuntimeException(errorMsg, e);
            }

            String status = (String) result.get(DB_OUT_STATUS);

            if ((status != null) && status.equals("N")) {
                throw new RuntimeException((String) result.get(DB_OUT_MESSAGE));
            }
        } finally {
            try {
                dbConn.close();
            } catch (Throwable t) {
                logger.error("deletePostedRewardsByOrderDetailId: DBConnection close failed.",
                    t);
            }
        }
    }

    public static Long querySequenceNextVal(String partnerName,
        PartnerSequenceType sequenceType) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_PARTNER_NAME, partnerName);
        inputParams.put(DB_IN_SEQUENCE_TYPE, sequenceType.toString());
        
        logger.debug("***partnerName: " + partnerName);
        logger.debug("***sequenceType: " + sequenceType.toString());
        
        // Submit the request to the database.
        BigDecimal result = null;

        try {
            result = (BigDecimal) UtilDAO.submitDbRequest(DB_SELECT_PARTNER_SEQUENCE,
                    inputParams, false);
        } catch (Exception e) {
            String errorMsg =
                "querySequenceNextVal: Could not obtain the partner sequence next val from database for Partner=" +
                partnerName + " SequenceType=" + sequenceType;
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }

        if (result == null) {
            String errorMsg =
                "querySequenceNextVal: The sequence does not exist: Partner=" +
                partnerName + " SequenceType=" + sequenceType;
            throw new RuntimeException(errorMsg);
        }

        //        BigDecimal nextVal = (BigDecimal) result.get(
        //                "RegisterOutParameterNextVal");
        //        if ((nextVal == null) || (nextVal.longValue() == 0)) {
        //            throw new RuntimeException("querySequenceNextVal: NextVal=" +
        //                nextVal + " for Partner=" + partnerName + " SequenceType=" +
        //                sequenceType);
        //        }
        //
        return new Long(result.longValue());
    }

    public static CachedResultSet queryPartnerPartialRefunds(
        String partnerName, Date activeDate) {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_PARTNER_NAME, partnerName);
        inputParams.put(DB_IN_REFUND_DATE,
            new java.sql.Date(activeDate.getTime()));

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_PARTNER_PARTIAL_REFUNDS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryPartnerPartialRefunds: Could not obtain partner partial refunds from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
}
