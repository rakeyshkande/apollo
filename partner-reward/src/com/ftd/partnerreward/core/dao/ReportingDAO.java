package com.ftd.partnerreward.core.dao;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

import com.ftd.partnerreward.core.text.TextUtil;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;

import javax.naming.InitialContext;
import javax.naming.NamingException;


public class ReportingDAO {
    protected static Logger logger = new Logger(ReportingDAO.class.getName());
    protected final static String DB_INSERT_REPORT_SUBMISSION_LOG = "PARTNERREWARD.INSERT_REPORT_SUBMISSION_LOG";
    protected final static String DB_IN_REPORT_ID = "IN_REPORT_ID";

    //    protected final static String DB_IN_REPORT_FILE_NAME = "IN_REPORT_FILE_NAME";
    protected final static String DB_IN_SUBMISSION_TYPE = "IN_SUBMISSION_TYPE";
    protected final static String DB_IN_SUBMITTED_BY = "IN_SUBMITTED_BY";
    protected final static String DB_IN_REPORT_STATUS = "IN_REPORT_STATUS";
    protected final static String DB_IN_REPORT_FILE_NAME = "IN_REPORT_FILE_NAME";
    protected final static String DB_IN_ORACLE_REPORT_URL = "IN_ORACLE_REPORT_URL";

    // Output parameters
    protected final static String DB_OUT_STATUS = "OUT_STATUS";
    protected final static String DB_OUT_REPORT_SUBMISSION_ID = "OUT_REPORT_SUBMISSION_ID";
    protected final static String DB_OUT_MESSAGE = "OUT_MESSAGE";

    /**
     * constructor to prevent clients from instantiating.
     */
    private ReportingDAO() {
        super();
    }

    public static String insertReportLog(String reportId, String rdfFileName, String reportUrl) {
        String submissionId = "";
        Map result = new HashMap(0);

        // Build db stored procedure input parms
        Map inputParams = new HashMap(6);
        inputParams.put(DB_IN_REPORT_ID, reportId);

        // The file name inserted into the db must be unique, so add
        // a unique timestamp suffix.
        String uniqueFileName = rdfFileName + "_" + TextUtil.formatDate(new Date(), "yyyyMMddHHmmss");
        inputParams.put(DB_IN_REPORT_FILE_NAME, uniqueFileName);
        // Submission type is a "scheduled" report.
        inputParams.put(DB_IN_SUBMISSION_TYPE, "S");

        inputParams.put(DB_IN_SUBMITTED_BY, "partner-reward");
        inputParams.put(DB_IN_REPORT_STATUS, "O");
        inputParams.put(DB_IN_ORACLE_REPORT_URL, reportUrl);

        // Submit the request to the database.
        try {
            result = (Map) UtilDAO.submitDbRequest(DB_INSERT_REPORT_SUBMISSION_LOG,
                    inputParams, false);
        } catch (Exception e) {
            String errorMsg = "insertReportLog: Could not insert report submission into database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }

        String status = (String) result.get(DB_OUT_STATUS);

        if ((status != null) && status.equals("N")) {
            throw new RuntimeException((String) result.get(DB_OUT_MESSAGE));
        }

        submissionId = (String) result.get(DB_OUT_REPORT_SUBMISSION_ID);

        return submissionId;
    }

    public static void dispatchReportRequest(String submissionId) {
        MessageToken token = new MessageToken();
        token.setMessage(submissionId);

        token.setStatus("Reporting");
        try {
            Dispatcher.getInstance().dispatchTextMessage(new InitialContext(), token);
        } catch (Exception e) {
            throw new RuntimeException("dispatchReportRequest: Failed.", e);
        }
    }
    
    public static String insertMessage(SystemMessengerVO message)
    {
        String ret = null;
        
        Map result = new HashMap(0);

        // Build db stored procedure input parms
        Map inputParams = new HashMap(5);
        inputParams.put("IN_SOURCE", message.getSource());
        inputParams.put("IN_TYPE", message.getType());
        inputParams.put("IN_MESSAGE", message.getMessage());
        // JP Puzon 08-29-2005
        // Added the following subject parameter to support added
        // functionality of specifying the email subject line
        // of the alert.
        inputParams.put("IN_EMAIL_SUBJECT", message.getSubject());
        //dataRequest.addInputParam("IN_READ", "N");
        inputParams.put("IN_COMPUTER", "test");

        // Submit the request to the database.
        try {
            result = (Map) UtilDAO.submitDbRequest("INSERT_MESSAGE",
                    inputParams, false);
        } catch (Exception e) {
            String errorMsg = "insertReportLog: Could not insert report submission into database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }

        String status = (String) result.get("RegisterOutParameterStatus");
        if(status.equals("N"))
        {
            ret = (String) result.get("RegisterOutParameterMessage");
        }
        else
        {
            ret = (String) result.get("RegisterOutParameterMessageId");            
        }

        return ret;
    }
}
