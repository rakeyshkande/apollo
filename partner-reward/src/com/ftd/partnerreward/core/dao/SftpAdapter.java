package com.ftd.partnerreward.core.dao;

import com.enterprisedt.net.ftp.FTPClient;

import com.ftd.osp.utilities.plugins.Logger;

import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.sftp.SftpException;
import com.jscape.inet.sftp.SftpFile;
import com.jscape.inet.sftp.events.SftpConnectedEvent;
import com.jscape.inet.sftp.events.SftpDisconnectedEvent;
import com.jscape.inet.sftp.events.SftpDownloadEvent;
import com.jscape.inet.sftp.events.SftpUploadEvent;
import com.jscape.inet.ssh.util.SshParameters;

import java.io.File;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;


/**
 * Manages the state of and access to an SFTP connection
 *
 */
public class SftpAdapter extends com.jscape.inet.sftp.events.SftpAdapter
    implements IFtpAdapter {
    private static Logger logger = new Logger(FtpAdapter.class.getName());

    public SftpAdapter() {
        super();
    }

    // captures upload event
    public void upload(SftpUploadEvent evt) {
        System.out.println("Uploaded file: " + evt.getFilename());
    }

    // captures download event
    public void download(SftpDownloadEvent evt) {
        System.out.println("Downloaded file: " + evt.getFilename());
    }

    // captures connect event
    public void connected(SftpConnectedEvent evt) {
        System.out.println("Connected to server: " + evt.getHostname());
    }

    // captures disconnect event
    public void disconnected(SftpDisconnectedEvent evt) {
        System.out.println("Disconnected from server: " + evt.getHostname());
    }

    private Sftp createSession(String ftpServer, String ftpLocation,
        String username, String password, boolean isBinary)
        throws SftpException {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering login");
            logger.debug("ftpLocation : " + ftpLocation);
            logger.debug("Opening FTP connection to " + ftpServer +
                " as user " + username);
        }

        /* connect to FTP server */

        // create new Sftp instance
        Sftp ftpClient = new Sftp(new SshParameters(ftpServer, username,
                    password));

        // register to capture SFTP related events
        ftpClient.addSftpListener(this);

        // establish secure connection
        ftpClient.connect();

        if (isBinary) {
            ftpClient.setBinary();
        } else {
            ftpClient.setAscii();
        }

        ftpClient.setDir(ftpLocation);

        return ftpClient;
    }

    // Common Partner-Reward interface methods.
    public void prUpload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary, boolean isOverwriteProtected, String overwriteProtectedFileName) {
        // create new Sftp instance
        try {
            Sftp ftpClient = null;

            try {
                ftpClient = createSession(ftpServer, remoteFilePath, username,
                        password, isBinary);

                // As a general rule, delete any existing file, since
                // we do not want to rely on overwrite privileges.
                try {
                    ftpClient.deleteFile(remoteFileName);
                    logger.warn("prUpload: Existing file deleted: " +
                        remoteFileName);
                } catch (Throwable t) {
                    // do nothing
                }

                // upload all files in local directory matching filter
                ftpClient.upload(localFullFileName, remoteFileName);
            } finally {
                // disconnect
                ftpClient.disconnect();
            }
        } catch (Exception e) {
            throw new RuntimeException("prUpload: Failed.", e);
        }
    }

    public boolean prDownload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary) {
        boolean exists = false;

        try {
            Sftp ftpClient = null;

            try {
                ftpClient = createSession(ftpServer, remoteFilePath, username,
                        password, isBinary);

                for (Enumeration enum1 = ftpClient.getDirListing(remoteFileName);
                        enum1.hasMoreElements();) {
                    // If this loop is entered, it means that an exact file
                    // match was found.
                    exists = true;
                    ftpClient.download(localFullFileName, remoteFileName);
                }
            } finally {
                // disconnect
                ftpClient.disconnect();
            }
        } catch (Exception e) {
            throw new RuntimeException("prDownload: Failed.", e);
        }

        return exists;
    }
    
    public List prDownloadFiles(String localFilePath, String remoteFileNamePattern, String remoteFilePath, 
        String ftpServer, String userName, String password, boolean isBinary) {
        
        List resultList = new ArrayList();
        try {
            Sftp client = createSession(ftpServer, remoteFilePath,
                    userName, password, isBinary);

            try {
                List remoteFiles = getFilesList(client.getDirListing(), remoteFileNamePattern);

                for (Iterator it = remoteFiles.iterator(); it.hasNext();) {
                    // Get the file.
                    String remoteFileName = (String) it.next();
                    String localFullFileName = localFilePath + File.separator + remoteFileName;
                    client.download(localFullFileName, remoteFileName);
                    resultList.add(localFullFileName);
                }
            } finally {
                client.disconnect();
            }
        } catch (Exception e) {
            throw new RuntimeException("prDownload: Failed.", e);
        }
        
        return resultList;
    }
  
    private List getFilesList(Enumeration files,
        String remoteFileNamePattern) {
        List resultList = new ArrayList();
        while (files.hasMoreElements()) {
            SftpFile file = (SftpFile)files.nextElement();
            if (file.getFilename().startsWith(remoteFileNamePattern)) {
                resultList.add(file.getFilename());
            }
        }
        return resultList;
    }
}
