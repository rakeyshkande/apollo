package com.ftd.partnerreward.core.dao;

import java.util.List;

public interface IFtpAdapter {
    public void prUpload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary, boolean isOverwriteProtected, String overwriteProtectedFileName);

    public boolean prDownload(String localFullFileName, String remoteFileName,
        String remoteFilePath, String ftpServer, String username,
        String password, boolean isBinary);
        
    public List prDownloadFiles(String localFilePath, String remoteFileNamePattern, 
        String remoteFilePath, String ftpServer, String userName, 
        String password, boolean isBinary);
}
