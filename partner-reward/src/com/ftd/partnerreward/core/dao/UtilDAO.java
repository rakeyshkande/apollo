package com.ftd.partnerreward.core.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

//import oracle.xml.parser.v2.XMLDocument;
//import oracle.xml.parser.v2.XMLElement;
//import oracle.xml.parser.v2.XMLText;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


/**
 * Common utility services used by the Data Access Object classes.
 * @author JP Puzon
 */
public class UtilDAO {
    protected static Logger logger = new Logger(UtilDAO.class.getName());
//    protected final static String _RESULT_MAP_KEY_OUT_PARAMETERS = "out-parameters";
    protected static String _PARTNERREWARD_DATASOURCE_NAME = "PARTNERREWARD";
    
//    public final static String DB_OUT_STATUS = "OUT_STATUS";
//    public final static String DB_OUT_MESSAGE = "OUT_MESSAGE";
//    public final static String DB_OUT_CURSOR = "OUT_CURSOR";

    /**
    * Generic utility for creating a DataRequest for submission to
     * a database stored procedure.
     * This convenience method should be used by clients who do not
     * need control of transactional boundaries.
    * @throws java.lang.Exception
    * @throws java.sql.SQLException
    * @throws org.xml.sax.SAXException
    * @throws javax.xml.parsers.ParserConfigurationException
    * @throws java.io.IOException
    * @return
    * @param issueCommit
    * @param inputParams
    * @param statementID
    */
    public static Object submitDbRequest(String statementID, Map inputParams,
        boolean issueCommit)
        throws IOException, ParserConfigurationException, SAXException, 
            SQLException, Exception {
        Connection conn = getDbConnection();

        Object result = null;

        try {
            result = UtilDAO.submitDbRequest(statementID, inputParams, conn);

            if (issueCommit) {
                conn.commit();
            }
        } catch (Exception e) {
//            conn.rollback();
            throw e;
        } finally {
            conn.close();
        }

        return result;
    }

    /**
    * This convenience method is used in situations where the client
    * needs control of the transactional boundaries.
    * @throws java.lang.Exception
    * @throws java.sql.SQLException
    * @throws org.xml.sax.SAXException
    * @throws javax.xml.parsers.ParserConfigurationException
    * @throws java.io.IOException
    * @return
    * @param conn
    * @param inputParams
    * @param statementID
    */
    public static Object submitDbRequest(String statementID, Map inputParams,
        Connection conn)
        throws IOException, ParserConfigurationException, SAXException, 
            SQLException, Exception {
        // Build DataRequest object
        DataRequest request = new DataRequest();
        request.reset();
        request.setConnection(conn);
        request.setInputParams(inputParams);
        request.setStatementID(statementID);

        // Eexecute the stored procedure
        DataAccessUtil dau = DataAccessUtil.getInstance();

        return dau.execute(request);
    }

    //    private static Connection getDbConnection(String propertyFileName,
    //        String datasourceName)
    //        throws IOException, ParserConfigurationException, SAXException, 
    //            TransformerException, XSLException, Exception {
    //        Connection conn = DataSourceUtil.getInstance().getConnection(ConfigurationUtil.getInstance()
    //                                                                                      .getProperty(propertyFileName,
    //                    datasourceName));
    //
    //        return conn;
    //    }
    public static Connection getDbConnection() {
        Connection conn = null;

        try {
            conn = DataSourceUtil.getInstance().getConnection(_PARTNERREWARD_DATASOURCE_NAME);
        } catch (Exception e) {
            throw new RuntimeException(
                "getDbConnection: Could not obtain connection for '" +
                _PARTNERREWARD_DATASOURCE_NAME + "' datasource: " + e);
        }

        return conn;
    }

    public static void printXml(Document xmlDocument)
        throws IOException {
        StringWriter sw = new StringWriter(); //string representation of xml document
        try {
			DOMUtil.print(xmlDocument,new PrintWriter(sw));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        logger.debug("printXml(): Data DOM = \n" + sw.toString());
    }

    /**
    * Convenience method for null-checking prior to Map record addition.
    * @param value
    * @param key
    * @param inputParms
    */
    public static void addSearchFilter(Map inputParams, String key, Object value) {
        if (value != null) {
            if (value instanceof java.util.Date) {
                java.sql.Date sqlDate = new java.sql.Date(((java.util.Date) value).getTime());
                inputParams.put(key, sqlDate);
            } else {
                inputParams.put(key, value);
            }
        }
    }

//    public static Integer extractTotalRecordCountXml(Map compositeResult,
//        String targetNodeName) {
//        Integer result = null;
//        XMLDocument document = (XMLDocument) compositeResult.get(_RESULT_MAP_KEY_OUT_PARAMETERS);
//        String nodeValue = null;
//        NodeList nl = ((XMLElement) document.getFirstChild()).getChildrenByTagName(targetNodeName);
//
//        if (nl.getLength() > 0) {
//            nodeValue = ((XMLText) nl.item(0).getFirstChild()).getText();
//        }
//
//        //        Node node = (nl.item(0).getFirstChild());
//        //        String nodeValue = ((XMLText)node).getText();
//        if (nodeValue != null) {
//            result = new Integer(nodeValue);
//        }
//
//        return result;
//    }
    
//    public static Integer extractTotalRecordCount(Map compositeResult,
//        String targetNodeName) {
//        CachedResultSet crs = (CachedResultSet) compositeResult.get(DB_OUT_CURSOR);
//        return new Integer(crs.getRowCount());
//    }
}
