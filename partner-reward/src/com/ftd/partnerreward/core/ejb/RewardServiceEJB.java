package com.ftd.partnerreward.core.ejb;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

public interface RewardServiceEJB extends EJBObject 
{
  Float getReward(String orderDetailId, Date activeDate) throws RemoteException;

  Float getReward(String orderDetailId) throws RemoteException;

  void postRewardFile(String partnerName, Date endDate, Date startDate) throws RemoteException;

  void postRewardFile(String partnerName, Date endDate) throws RemoteException;


  void deletePostHistory(List orderDetailIdList) throws RemoteException;

  void rePostRewardFile(String partnerName, Date endDate, String existingLocalFileName) throws RemoteException;

  void reTransmitRewardFile(String partnerName, String fullLocalFileName, String remoteFileName) throws RemoteException;

  void getResponseFile(String partnerName, String workflowNodeName) throws RemoteException;
}