package com.ftd.partnerreward.core.ejb;

import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.OrderServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.controller.ControllerFactory;
import com.ftd.partnerreward.core.controller.IController;

import java.util.Date;
import java.util.List;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;


public class RewardServiceEJBean implements SessionBean {
    protected SessionContext ctx = null;

    public void ejbCreate() {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() {
    }

    public void setSessionContext(SessionContext ctx) {
        this.ctx = ctx;
    }

    public void postRewardFile(String partnerName, Date endDate) {
        IController controller = ControllerFactory.getInstance(partnerName,
                endDate);

        // Start the workflow.
        controller.execute();
    }

    public void postRewardFile(String partnerName, Date endDate, Date startDate) {
        IController controller = ControllerFactory.getInstance(partnerName,
                endDate, startDate);

        // Start the workflow.
        controller.execute();
    }

    public void getResponseFile(String partnerName, String workflowNodeName) {
        IController controller = ControllerFactory.getInstance(partnerName, workflowNodeName);

        // Start the workflow.
        controller.execute();
    }

    public Float getReward(String orderDetailId, Date activeDate) {
        if (activeDate == null) {
            // Default to the current date, zero-hour.
            activeDate = new Date();
            activeDate = ActionUtil.setToZeroHour(activeDate);
        }

        String partnerName = OrderServiceBO.getOrderPartnerName(orderDetailId,
                activeDate);

        // If no partner program is associated with the order, there are no rewards.
        if (partnerName == null) {
            return new Float(0);
        }

        IController controller = ControllerFactory.getInstance(partnerName,
                orderDetailId, activeDate);

        return (Float) controller.execute();
    }

    public Float getReward(String orderDetailId) {
        return getReward(orderDetailId, null);
    }

    public void reTransmitRewardFile(String partnerName, String fullLocalFileName, String remoteFileName) {
        IController controller = ControllerFactory.getInstance(partnerName,
                fullLocalFileName, remoteFileName);

        // Start the workflow.
        controller.execute();
    }

    public void rePostRewardFile(String partnerName, Date endDate, String existingLocalFileName) {
        // Obtain the workflow controller for the given partner.
        IController controller = ControllerFactory.getInstance(partnerName,
                endDate);

        // Next step is to delete any existing post history records in the database for the given
        // time-stamped local file name.
        RewardServiceBO.deletePostedRewardsHistory(existingLocalFileName);

        // Now that existing history has been cleared, proceed with the file re-post.
        controller.execute();
    }

    public void deletePostHistory(List orderDetailIdList) {
        RewardServiceBO.deletePostedRewardsHistory(orderDetailIdList);
    }
}
