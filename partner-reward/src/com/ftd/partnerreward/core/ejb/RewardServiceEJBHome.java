package com.ftd.partnerreward.core.ejb;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface RewardServiceEJBHome extends EJBHome 
{
  RewardServiceEJB create() throws RemoteException, CreateException;
}