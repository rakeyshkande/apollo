package com.ftd.partnerreward.core.ejb;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface RewardServiceEJBLocalHome extends EJBLocalHome 
{
  RewardServiceEJBLocal create() throws CreateException;
}