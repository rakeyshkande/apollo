package com.ftd.partnerreward.core.ejb;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import com.ftd.eventhandling.eventmanager.EventHandlingUtility;
import com.ftd.eventhandling.eventmanager.ResourceNameRegister;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.eventhandling.events.EventHandlerFactory;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 *
 * @author Anshu Gaind
 */
public class BatchActivatorMDBean implements MessageDrivenBean, MessageListener {
    private static Logger logger;
    private MessageDrivenContext messageDrivenContext;
    private String eventsLogDataSourceName;
    private DataSource eventsLogDataSource;
    private String eventsSchemaName;

    /**
     * A container invokes this method before it ends the life of the message-driven object.
     * This happens when a container decides to terminate the message-driven object.
     * This method is called with no transaction context.
     *
     * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
     */
    public void ejbCreate() {
    }

    /**
     * A container invokes this method before it ends the life of the
     * message-driven object. This happens when a container decides to terminate
     * the message-driven object.
     * This method is called with no transaction context.
     *
     * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
     */
    public void ejbRemove() {
    }

    /**
     * Set the associated message-driven context. The container calls this method after the instance creation.
     * The enterprise Bean instance should store the reference to the context object in an instance variable.
     * This method is called with no transaction context.
     *
     * @param ctx A MessageDrivenContext interface for the instance.
     * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
     */
    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.messageDrivenContext = ctx;

        InitialContext initContext = null;

        try {
            logger = new Logger(
                    "com.ftd.partnerreward.core.ejb.BatchActivatorMDBean");
            initContext = new InitialContext();

            Context myenv = new InitialContext();

            try {
                logger.debug("Begin Registering Component");
                eventsLogDataSourceName = (String) myenv.lookup(
                        "java:comp/env/Events Log Data Source Name");
                ResourceNameRegister.register("Events Log Data Source Name",
                    eventsLogDataSourceName);
                logger.debug("Events Log Data Source Name :: " +
                    eventsLogDataSourceName);
                eventsLogDataSource = (DataSource) myenv.lookup(eventsLogDataSourceName);

                this.eventsSchemaName = (String) myenv.lookup(
                        "java:comp/env/Events Schema Name");
                ResourceNameRegister.register("Events Schema Name",
                    this.eventsSchemaName);

                logger.debug("Created events schema :: " + this.eventsSchemaName);
                logger.debug("End Registering Component");
            } finally {
                myenv.close();
            }
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            try {
                initContext.close();
            } catch (Exception ex) {
                logger.error(ex);
            }

            logger.info("Message Driven Context Set");
        }
    }

    /**
     * Passes a message to the listener.
     *
     * @param message the message passed to the listener
     */
    public void onMessage(Message msg) {
        String eventName = null;
        String context = null;
        String payload = null;

        // timestamp the start of the event
        Date started = new Date();
        Date ended = null;
        String status = "SUCCESS";
        String log_message = null;

        //        Connection con = null;
        try {
            logger.debug(
                "***********************************************************");
            logger.debug("* BEGIN OPERATION *");
            logger.debug(
                "***********************************************************");

            /**
             * acquire database connection to check whether or not the event is active
             * and to log the event
             */
//            con = eventsLogDataSource.getConnection();

            String textMessage = ((TextMessage) msg).getText();

            StringReader sr = new StringReader(textMessage);
            InputSource inputSource = new InputSource(sr);
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(ResourceUtil.getInstance().getResource(this.eventsSchemaName));
            
                  DefaultHandler errorHandler = new DefaultHandler();
                  Document doc = EventHandlingUtility.validateSchema(schema, inputSource, errorHandler);
            //
            //      if (errorHandler.isValid())
            //      {
            //        logger.debug("The 'event' document is valid");
            //DOMParser domParser = new DOMParser();

            //            domParser.setValidationMode(XMLParser.SCHEMA_VALIDATION);
            //    domParser.setErrorHandler(handler);
            //            domParser.setXMLSchema(schema);
            
            Document messageDoc = DOMUtil.getDocument(textMessage);
//            domParser.showWarnings(true);
//            domParser.parse(inputSource);
            logger.debug("The 'event' document was parsed and validated");

//            Document messageDoc = domParser.getDocument();
            EventHandlerFactory factory = EventHandlerFactory.getInstance();

            // obtain the appropriate handler, while setting the message driven context
            Node eventNameNode = DOMUtil.selectSingleNode(messageDoc,"/event/event-name");
            Node contextNode = DOMUtil.selectSingleNode(messageDoc,"/event/context");
            Node payloadNode = DOMUtil.selectSingleNode(messageDoc,"/event/payload");

            eventName = (eventNameNode.getFirstChild() != null)
                ? eventNameNode.getFirstChild().getNodeValue() : null;
            context = (contextNode.getFirstChild() != null)
                ? contextNode.getFirstChild().getNodeValue() : null;
            payload = ((payloadNode != null) &&
                (payloadNode.getFirstChild() != null))
                ? payloadNode.getFirstChild().getNodeValue() : "";

            logger.debug("*** eventName: " + eventName);
            logger.debug("*** context: " + context);
            logger.debug("*** payload: " + payload);

            if (eventName == null) {
                throw new UnsupportedOperationException(
                    "The event name could not be found on the message");
            }

            if (context == null) {
                throw new UnsupportedOperationException(
                    "The context could not be found on the message");
            }

            Connection con = eventsLogDataSource.getConnection();

            try {
                // CHECK WHETHER OR NOT THE EVENT IS ACTIVE
                if (!EventHandlingUtility.isEventActive(eventName, context, con)) {
                    throw new Exception("The event " + eventName +
                        " is not active");
                }
            } finally {
                con.close();
            }

            EventHandler eventHandler = factory.getEventHandler(eventName,
                    this.messageDrivenContext);
            logger.debug("* BEGIN INVOKING EVENT HANDLER " +
                eventHandler.getClass().getName() + " FOR EVENT " + eventName +
                " *");

            MessageToken messageToken = new MessageToken();

            messageToken.setMessage(payload.trim());

            eventHandler.invoke(messageToken);

            logger.debug("* END INVOKING EVENT HANDLER " +
                eventHandler.getClass().getName() + " FOR EVENT " + eventName +
                " *");

        } catch (Throwable t) {
            status = "FAILURE";

            try {
                // write the stack trace to the log_message variable
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw, true);
                t.printStackTrace(pw);
                log_message = sw.toString();
                pw.close();
            } catch (Exception ex) {
                logger.error(ex);
            }

            logger.error(t);

        } finally {
            // timestamp the end of the event
            ended = new Date();

            try {
                Connection con = eventsLogDataSource.getConnection();

                try {
                    EventHandlingUtility.logEvent(eventName, context, started,
                        ended, status, log_message, con);
                } finally {
                    con.close();
                    logger.debug(this.eventsLogDataSourceName +
                        " Connection Released");
                }
            } catch (Throwable t2) {
                logger.error(t2);
            }

            logger.debug(
                "***********************************************************");
            logger.debug("* END OPERATION *");
            logger.debug(
                "***********************************************************");
        }
    }
}
