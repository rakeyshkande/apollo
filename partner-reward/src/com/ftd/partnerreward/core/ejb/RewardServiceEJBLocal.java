package com.ftd.partnerreward.core.ejb;
import javax.ejb.EJBLocalObject;
import java.util.Date;
import java.util.List;

public interface RewardServiceEJBLocal extends EJBLocalObject 
{
  Float getReward(String orderDetailId, Date activeDate);

  Float getReward(String orderDetailId);

  void postRewardFile(String partnerName, Date endDate, Date startDate);

  void postRewardFile(String partnerName, Date endDate);


  void deletePostHistory(List orderDetailIdList);

  void rePostRewardFile(String partnerName, Date endDate, String existingLocalFileName);

  void reTransmitRewardFile(String partnerName, String fullLocalFileName, String remoteFileName);

  void getResponseFile(String partnerName, String workflowNodeName);
}