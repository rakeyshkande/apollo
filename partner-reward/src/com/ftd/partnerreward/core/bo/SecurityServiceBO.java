package com.ftd.partnerreward.core.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;

import java.util.Iterator;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;


public class SecurityServiceBO {
    private SecurityServiceBO() {
        super();
    }

    /***
    * A generic java program to create a sha1 hash and be loaded into the database.
    *
    * Uses code from Metalink
    * Uses code from http://64.233.167.104/search?q=cache:g-BFQA4fHZAJ:uncc.servehttp.com/php/programming/java.php+sha1+hash+tohex&hl=en
    * */
    public static synchronized String encryptSHA1(String plaintext)
        throws Exception {
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance("SHA1"); //step 1
        } catch (NoSuchAlgorithmException e) {
            throw new NoSuchAlgorithmException(e.getMessage());
        }

        md.update(plaintext.getBytes()); //step 2

        byte[] raw = md.digest(); //step 3,4,5

        return toHex(raw); //step 6   
    }

    private static String toHex(byte[] hash) {
        StringBuffer buf = new StringBuffer(hash.length * 2);

        for (int i = 0; i < hash.length; i++) {
            int intVal = hash[i] & 0xff;

            if (intVal < 0x10) {
                // append a zero before a one digit hex
                // number to make it two digits.
                buf.append("0");
            }

            buf.append(Integer.toHexString(intVal));
        }

        return buf.toString();
    }
    
    
    /**
     * A simple routine that opens a key ring file and loads the first available key suitable for
     * encryption.
     *
     * @param in
     * @return
     * @throws IOException
     * @throws PGPException
     */
    private static PGPPublicKey readPublicKey(InputStream in)
        throws IOException, PGPException {
        in = PGPUtil.getDecoderStream(in);

        PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(in);

        //
        // we just loop through the collection till we find a key suitable for encryption, in the real
        // world you would probably want to be a bit smarter about this.
        //
        PGPPublicKey key = null;

        //
        // iterate through the key rings.
        //
        Iterator rIt = pgpPub.getKeyRings();

        while ((key == null) && rIt.hasNext()) {
            PGPPublicKeyRing kRing = (PGPPublicKeyRing) rIt.next();
            Iterator kIt = kRing.getPublicKeys();

            while ((key == null) && kIt.hasNext()) {
                PGPPublicKey k = (PGPPublicKey) kIt.next();

                if (k.isEncryptionKey()) {
                    key = k;
                }
            }
        }

        if (key == null) {
            throw new IllegalArgumentException(
                "Can't find encryption key in key ring.");
        }

        return key;
    }    

     public static File encryptPGP(String targetFile, String publicKeyPath,
         boolean isAsciiArmored, boolean isIntegrityChecked)
         throws Exception {
         FileInputStream keyIn = new FileInputStream(publicKeyPath);

         return encryptFile(targetFile, readPublicKey(keyIn), isAsciiArmored,
             isIntegrityChecked);
     }    
    
    private static File encryptFile(String targetFile, PGPPublicKey encKey,
        boolean isAsciiArmored, boolean withIntegrityCheck)
        throws IOException, NoSuchProviderException, Exception {
        OutputStream out = null;
        PGPEncryptedDataGenerator cPk = null;
        PGPCompressedDataGenerator comData = null;
        String outFileName = null;

        try {
            //Add BC as provider
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            
            if (isAsciiArmored) {
                outFileName = targetFile + ".asc";
                out = new ArmoredOutputStream(new FileOutputStream(outFileName));
            } else {
                outFileName = targetFile + ".pgp";
                out = new FileOutputStream(outFileName);
            }

            cPk = new PGPEncryptedDataGenerator(PGPEncryptedData.TRIPLE_DES,
                    withIntegrityCheck, new SecureRandom(), "BC");

            cPk.addMethod(encKey);

            OutputStream cOut = cPk.open(out, new byte[1 << 16]);

            comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);

            PGPUtil.writeFileToLiteralData(comData.open(cOut),
                PGPLiteralData.BINARY, new File(targetFile), new byte[1 << 16]);

            return new File(outFileName);
        } catch (PGPException e) {
            if (e.getUnderlyingException() != null) {
                throw e.getUnderlyingException();
            } else {
                throw e;
            }
        } finally {
            if (comData != null) {
                comData.close();
            }

            if (cPk != null) {
                cPk.close();
            }

            if (out != null) {
                out.close();
            }
        }
    }

}
