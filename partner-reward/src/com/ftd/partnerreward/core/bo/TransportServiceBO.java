package com.ftd.partnerreward.core.bo;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.dao.FtpAdapter;
import com.ftd.partnerreward.core.dao.IFtpAdapter;
import com.ftd.partnerreward.core.dao.SftpAdapter;


public class TransportServiceBO {
    protected static Logger logger = new Logger(TransportServiceBO.class.getName());

    private TransportServiceBO() {
    }

//    public static IFtpAdapter getFtpAdapter() {
//        return TransportServiceBO.getFtpAdapter(false);
//    }
    
    public static IFtpAdapter getFtpAdapter() {
        return new FtpAdapter();
    }
    
    public static IFtpAdapter getSftpAdapter() {
        return new SftpAdapter();
    }
}
