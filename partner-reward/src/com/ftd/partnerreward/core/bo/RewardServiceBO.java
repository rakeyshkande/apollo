package com.ftd.partnerreward.core.bo;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.dao.OrderDAO;
import com.ftd.partnerreward.core.dao.PartnerProgramDAO;
import com.ftd.partnerreward.core.dao.ReportingDAO;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CalculationBasis;
import com.ftd.partnerreward.core.vo.MembershipVO;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.OrderVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;
import com.ftd.partnerreward.core.vo.RewardCalcParms;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class RewardServiceBO {
    protected static Logger logger = new Logger(RewardServiceBO.class.getName());

    // The following metrics are used for reward calculations.
    //    private static final int _CALC_ROUNDING_MODE = BigDecimal.ROUND_HALF_UP;
    //    private static final int _CALC_SCALE = 2;

    /**
     * Constructor to prevent instantiation of this class.
     */
    private RewardServiceBO() {
        super();
    }

    //    public static OrderDetailVO getReward(String orderDetailId,
    //        Date activeDate, int dollarScale, int dollarRoundMethod,
    //        int rewardScale, int rewardRoundMethod) {
    //        return RewardServiceBO.getReward(orderDetailId, activeDate,
    //            dollarScale, dollarRoundMethod, rewardScale, rewardRoundMethod,
    //            false, false);
    //    }
    public static OrderDetailVO getReward(String orderDetailId,
        Date activeDate, int dollarScale, int dollarRoundMethod,
        int rewardScale, int rewardRoundMethod, RewardCalcParms rewardCalcParms) {
        // Obtain the order detail data.
        CachedResultSet order = OrderDAO.queryOrderDetail(orderDetailId);

        // Only one record should return.
        if (order.getRowCount() != 1) {
            logger.warn("getReward: Database returned " + order.getRowCount() +
                " records for order detail id: " + orderDetailId +
                ((order.getRowCount() == 0)
                ? ". Order may have been cancelled." : "."));

            return null;
        }

        order.next();

        // Get the source code and delivery date.
        String sourceCode = (String) order.getObject(4);
        Date deliveryDate = (Date) order.getObject(5);

        if (deliveryDate == null) {
            // If the order has not been delivered, use the input activeDate.
            if (activeDate == null) {
                throw new RuntimeException(
                    "getReward: Both delivery date and active date can not be null for order detail id: " +
                    orderDetailId);
            }

            deliveryDate = activeDate;
        }

        // These metrics represent the *original* purchase amounts before any rounding.
        BigDecimal purchaseTotal = new BigDecimal(0);
        BigDecimal mdseTotal = new BigDecimal(0);

        //        BigDecimal addOnTotal = new BigDecimal(0);
        // Obtain the dollar amounts of the order.
        CachedResultSet orderBilling = OrderDAO.queryOrderTotals(orderDetailId);

        // The result set is multi-line. We need to sum the columns from each line.
        while (orderBilling.next()) {
            purchaseTotal = purchaseTotal.add((BigDecimal) orderBilling.getObject(
                        8));
            mdseTotal = mdseTotal.add((BigDecimal) orderBilling.getObject(3));

            //                addOnTotal = addOnTotal.add((BigDecimal) orderBilling.getObject(4));
        }

        // The majority of programs are NOT credit card based.
        if (rewardCalcParms.isCreditCardBased()) {
            // Based the purchase on the actual amount debited from the credit card.
            BigDecimal creditPurchaseAmount = (BigDecimal) order.getObject(14);

            if (creditPurchaseAmount == null) {
                logger.warn(
                    "getReward: The credit card purchase amount is null for" +
                    " order detail id: " + orderDetailId);
            } else {
                // Compare the order totals with the credit card charge amount.
                // Override the total order value if the charge amount is less.
                if (creditPurchaseAmount.floatValue() < purchaseTotal.floatValue()) {
                    purchaseTotal = creditPurchaseAmount;
                }

                // Override the merchandise order value if the charge amount is less.
                if (creditPurchaseAmount.floatValue() < mdseTotal.floatValue()) {
                    mdseTotal = creditPurchaseAmount;
                }
            }
        }
        
        BigDecimal dollarOrderTotal = purchaseTotal.setScale(dollarScale,
                dollarRoundMethod);
        BigDecimal dollarMerchandise = mdseTotal.setScale(dollarScale,
                dollarRoundMethod);

        Date orderDate = order.getDate("order_date");
        if (orderDate == null) {
            orderDate = deliveryDate;
        }
        // Obtain the partner program terms and conditions.
        CachedResultSet partnerProgram = PartnerProgramDAO.queryPartnerProgramTerms(sourceCode,
                orderDate);

        OrderDetailVO orderDetailVO = new OrderDetailVO();

        // Calculate the rewards based on the DB result sets.
        // Depending upon the partner program terms and conditions,
        // there are 3 possible calculations:
        // (1) base points
        // (2) bonus points
        // (3) minimum points
        partnerProgram.next();
        
        BigDecimal baseRewardPoints = (BigDecimal) partnerProgram.getObject(6);
        String programType = (String) partnerProgram.getObject(16);

        if (programType == null) {
            logger.warn("getReward: NULL Program Type returned for: sourceCode=" + sourceCode + " orderDetailId=" + orderDetailId);
            return null;
        }
        if (!programType.equalsIgnoreCase("Default")) {
            logger.warn(
                "getReward: This order may pertain to a CostCenter partner. No effective partner *reward* program for: sourceCode=" +
                sourceCode + " orderDetailId=" + orderDetailId +
                " deliveryDate=" +
                TextUtil.formatDate(deliveryDate, "MM-dd-yyyy HHmmss") +
                " programType=" + programType);
        } else {
            if (baseRewardPoints != null) {
            	orderDetailVO.setRewardPoints(baseRewardPoints.floatValue());
            }            
            float basePoints = calculateReward((String) partnerProgram.getObject(
                        5), baseRewardPoints,
                    dollarOrderTotal, dollarMerchandise, dollarScale,
                    dollarRoundMethod, rewardScale, rewardRoundMethod);

            // Next, calculate the bonus points (if any).
            float bonusPoints = calculateReward((String) partnerProgram.getObject(
                        9), (BigDecimal) partnerProgram.getObject(10),
                    dollarOrderTotal, dollarMerchandise, dollarScale,
                    dollarRoundMethod, rewardScale, rewardRoundMethod);

            // Next, calculate the minimum points (if any).
            String programName = (String) partnerProgram.getObject(4);
            logger.debug("getReward: DB data: sourceCode=" + sourceCode +
                " programName=" + programName + " deliveryDate=" +
                TextUtil.formatDate(deliveryDate, "MM-dd-yyyy HHmmss"));

            CachedResultSet rewardRanges = PartnerProgramDAO.queryProgramRewardRange(programName);

            // Cycle through the resultset to determine the appropriate minimum points
            // to apply.
            float minPoints = 0;

            while (rewardRanges.next()) {
                BigDecimal rangeMinDollar = (rewardRanges.getObject(2) == null)
                    ? new BigDecimal(0)
                    : ((BigDecimal) rewardRanges.getObject(2)).setScale(dollarScale,
                        dollarRoundMethod);
                BigDecimal rangeMaxDollar = (rewardRanges.getObject(3) == null)
                    ? new BigDecimal(0)
                    : ((BigDecimal) rewardRanges.getObject(3)).setScale(dollarScale,
                        dollarRoundMethod);
                String rangeCalculationBasis = (String) rewardRanges.getObject(4);
                BigDecimal rangePointsFactor = (rewardRanges.getObject(5) == null)
                    ? new BigDecimal(0) : (BigDecimal) rewardRanges.getObject(5);

                BigDecimal dollarAmount = new BigDecimal(0);

                if (rangeCalculationBasis.equals(
                            CalculationBasis.FIXED_AMOUNT_TYPE.toString())) {
                    dollarAmount = new BigDecimal(1);
                } else if (rangeCalculationBasis.equals(
                            CalculationBasis.LINE_ITEM_COUNT_TYPE.toString()) ||
                        rangeCalculationBasis.equals(
                            CalculationBasis.MERCHANDISE_TYPE.toString())) {
                    dollarAmount = dollarMerchandise;
                } else if (rangeCalculationBasis.equals(
                            CalculationBasis.ORDER_TOTAL_TYPE.toString())) {
                    dollarAmount = dollarOrderTotal;
                } else {
                    throw new RuntimeException(
                        "getReward: Reward Range calculation basis value is unrecognized: " +
                        rangeCalculationBasis);
                }

                if ((rangeMinDollar.floatValue() <= dollarAmount.floatValue()) &&
                        (dollarAmount.floatValue() <= rangeMaxDollar.floatValue())) {
                    // This is the minimum points value.
                    minPoints = rangePointsFactor.multiply(dollarAmount)
                                                 .setScale(rewardScale,
                            rewardRoundMethod).floatValue();
                    
                    // If, however, the designated dollar range minimum value
                    // is NOT zero, this minium reward becomes a "kicker" to be added to
                    // the base points.
                    if (rangeMinDollar.floatValue() != 0) {
                      basePoints = basePoints + minPoints;
                    }

                    break;
                }
            }

            // Partner program metrics
            orderDetailVO.setPstCode((String) partnerProgram.getObject(1));
            orderDetailVO.setPstCodeBonus((String) partnerProgram.getObject(2));

            BigDecimal partnerBankId = (BigDecimal) partnerProgram.getObject(3);

            if (partnerBankId != null) {
                logger.warn("Found a record with a partnerBank: " +
                    partnerBankId);

                orderDetailVO.setPartnerBankId(partnerBankId.setScale(0,
                        BigDecimal.ROUND_HALF_UP).toString());
                orderDetailVO.setPartnerBankPstCode((String) partnerProgram.getObject(
                        13));
                orderDetailVO.setPartnerBankBalance((partnerProgram.getObject(
                        14) == null) ? 0
                                     : ((BigDecimal) partnerProgram.getObject(
                        14)).setScale(rewardScale, rewardRoundMethod)
                                        .floatValue());

                logger.warn("Found a record with a partnerBank: bankPstCode=" +
                    orderDetailVO.getPartnerBankPstCode() + " balance=" +
                    orderDetailVO.getPartnerBankBalance());
            }

            orderDetailVO.setRewardType((String) partnerProgram.getObject(7));
            orderDetailVO.setRewardName((String) partnerProgram.getObject(8));
            orderDetailVO.setBonusSeparateData((String) partnerProgram.getObject(
                    11));

            float maximumPoints = (partnerProgram.getObject(12) == null) ? 0
                                                                         : ((BigDecimal) partnerProgram
                                                                            .getObject(12)).setScale(rewardScale,
                    rewardRoundMethod).floatValue();

            orderDetailVO.setRewardTotalLimit(maximumPoints);

            orderDetailVO.setRewardBase(basePoints);
            orderDetailVO.setRewardBonus(bonusPoints);
            orderDetailVO.setRewardMinimum(minPoints);
        }

        // Now that all values have been calculated, compose the value object.
        orderDetailVO.setProgramName((String) partnerProgram.getObject(4));

        OrderVO orderHeaderVO = new OrderVO();
        orderHeaderVO.setCompanyId((String) order.getObject(1));
        orderHeaderVO.setEmailAddress((String) order.getObject(11));
        orderHeaderVO.setMasterOrderNumber((String) order.getObject(2));
        orderHeaderVO.setCustomerFirstName((String) order.getObject(16));
        orderHeaderVO.setCustomerLastName((String) order.getObject(17));

        MembershipVO membershipVO = new MembershipVO();
        membershipVO.setFirstName((String) order.getObject(7));
        membershipVO.setLastName((String) order.getObject(8));
        membershipVO.setMembershipType((String) order.getObject(9));

        String membershipNumber = (String) order.getObject(10);

        if (rewardCalcParms.isCreditCardBased()) {
            // Use the credit card number as the membership ID.
            membershipNumber = (String) order.getObject(15);
        }

        membershipVO.setMembershipNumber(membershipNumber);
        membershipVO.setAddress1((String) order.getObject(13));
        membershipVO.setAddress2((String) order.getObject(18));
        membershipVO.setCity((String) order.getObject(19));
        membershipVO.setState((String) order.getObject(20));
        membershipVO.setZipCode((String) order.getObject(21));
        orderDetailVO.setOrderDate((Date) order.getObject(22));
        membershipVO.setCountry((String) order.getObject(23));
        //        OrderDetailVO orderDetailVO = new OrderDetailVO();
        // Partner program metrics
        //        orderDetailVO.setPstCode((String) partnerProgram.getObject(1));
        //        orderDetailVO.setPstCodeBonus((String) partnerProgram.getObject(2));
        //        orderDetailVO.setPartnerBankId((String) partnerProgram.getObject(3));
        //        orderDetailVO.setProgramName((String) partnerProgram.getObject(4));
        //        orderDetailVO.setRewardType((String) partnerProgram.getObject(7));
        //        orderDetailVO.setRewardName((String) partnerProgram.getObject(8));
        //        orderDetailVO.setBonusSeparateData((String) partnerProgram.getObject(11));
        //        orderDetailVO.setPartnerBankPstCode((String) partnerProgram.getObject(
        //                13));
        //        orderDetailVO.setPartnerBankBalance((partnerProgram.getObject(14) == null)
        //            ? 0
        //            : ((BigDecimal) partnerProgram.getObject(14)).setScale(
        //                rewardScale, rewardRoundMethod).floatValue());
        //
        //        float maximumPoints = (partnerProgram.getObject(12) == null) ? 0
        //                                                                     : ((BigDecimal) partnerProgram
        //                                                                        .getObject(12)).setScale(rewardScale,
        //                rewardRoundMethod).floatValue();
        //
        //        orderDetailVO.setRewardTotalLimit(maximumPoints);
        // Record the order metrics that are common for both reward and
        // cost center partners.
        orderDetailVO.setExternalOrderNumber((String) order.getObject(3));
        orderDetailVO.setSourceCode((String) order.getObject(4));

        if (order.getObject(6) != null) {
            orderDetailVO.setOrderDetailId(((BigDecimal) order.getObject(6)).toString());
        }

        orderDetailVO.setBillingTotal(dollarOrderTotal.floatValue());
        orderDetailVO.setBillingMerchandise(dollarMerchandise.floatValue());
        orderDetailVO.setProductName((String) order.getObject(12));
        orderDetailVO.setDeliveryDate(deliveryDate);

        // Set the original dollar amounts.
        orderDetailVO.setBillingTotalOriginal(purchaseTotal.setScale(2,
                BigDecimal.ROUND_HALF_UP).floatValue());
        orderDetailVO.setBillingMerchandiseOriginal(mdseTotal.setScale(2,
                BigDecimal.ROUND_HALF_UP).floatValue());

        // Compose the final object.
        orderDetailVO.setMembershipVO(membershipVO);
        orderDetailVO.setOrderHeaderVO(orderHeaderVO);

        return orderDetailVO;
    }

    public static void updateBankBalance(String partnerName,
        List orderDetailsList, Float bankBalanceThreshold, String alertSubject,
        String alertBody) {
        logger.warn("updateBankBalance: Order detail list size is: " +
            orderDetailsList.size());

        // Grab the first record in the collection as a represenative sample to obtain
        // the current bank balance.
        if (orderDetailsList.size() > 0) {

            // Initalize the bank balance to a known value.
            float bankBalance = Float.MIN_VALUE;

            // Iterate through the collection, decrementing the credited rewards.
            for (Iterator i = orderDetailsList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();

                String bankPstCode = odVO.getPartnerBankPstCode();

                if (bankPstCode == null) {
                    // This indicates that no bank promo code exists (i.e. there is no bank account)
                    continue;
                }

                // Now, obtain the bank balance.
                if (bankBalance == Float.MIN_VALUE) {
                    bankBalance = odVO.getPartnerBankBalance();
                }

                logger.warn("updateBankBalance: Reward Bank " + bankPstCode +
                    " balance is: " + bankBalance);

                if (bankBalance <= 0) {
                    // No funds exist to decrement.
                    // Clear the bank promo.
                    clearBankMetadata(odVO);

                    continue;
                }

                float rewardBonus = odVO.getRewardBonus();
                float newBankBalance = bankBalance - rewardBonus;

                if (newBankBalance >= 0) {
                    // Decrement the database balance.
                    PartnerProgramDAO.decrementPartnerBankBalance(odVO.getPartnerBankId(),
                        rewardBonus);

                    logger.warn(
                        "updateBankBalance: After decrement, reward Bank " +
                        bankPstCode + " balance is now: " + newBankBalance);

                    if ((bankBalanceThreshold.floatValue() <= bankBalance) &&
                            (newBankBalance < bankBalanceThreshold.floatValue())) {
                        logger.warn("updateBankBalance: Reward Bank " +
                            bankPstCode + " balance is below " +
                            bankBalanceThreshold + ":" + newBankBalance);
                        
                        logger.debug("updateBankBalance: alertSubject=" + alertSubject + " alertBody=" + alertBody);
                        AlertService.processAlert(
                            "PARTNER-REWARD-BANK-BALANCE-" + partnerName,
                            alertSubject, alertBody);
                    }
                    
                    bankBalance = newBankBalance;
                } else {
                    // Clear the Bank PST code from the record to indicate that funds
                    // could not be decremented from the bank.
                    clearBankMetadata(odVO);
                }
            }
        }
    }

    protected static void clearBankMetadata(OrderDetailVO odVO) {
        odVO.setPartnerBankPstCode(null);
        odVO.setPartnerBankId(null);
        odVO.setPartnerBankBalance(0);
    }

    public static String generateReport(String partnerName, String reportId, String reportUrl,
        String rdfFilename, String emailAddresses, String postFileName, String emailSubject) {
        try {
            /// Compose the complete Url.
            StringBuffer fullReportUrl = new StringBuffer(reportUrl);
            fullReportUrl.append("&report=");
            fullReportUrl.append(rdfFilename);
            fullReportUrl.append("&desname=");
            fullReportUrl.append(emailAddresses);
            fullReportUrl.append("&p_file_name=");
            fullReportUrl.append(postFileName);
            fullReportUrl.append("&p_partner_name=");
            fullReportUrl.append(partnerName);
            
            if (emailSubject != null && emailSubject.length() > 0) {
            fullReportUrl.append("&subject=");
            fullReportUrl.append(emailSubject);
            }

            //            fullReportUrl.append("&");
            //            fullReportUrl.append("p_start_date");
            //            fullReportUrl.append("=");
            //            fullReportUrl.append(TextUtil.formatDate(startDate, "MM/dd/yyyy"));
            //            fullReportUrl.append("&");
            //            fullReportUrl.append("p_end_date");
            //            fullReportUrl.append("=");
            //            fullReportUrl.append(TextUtil.formatDate(endDate, "MM/dd/yyyy"));
            // This is a 2-step process.
            // Step 1: insert a submission record into the database.
            // Obtain the submission id.
            String submissionId = ReportingDAO.insertReportLog(reportId,
                    rdfFilename, fullReportUrl.toString());

            // Use the submission id to dispatch a message to the reporting app.
            ReportingDAO.dispatchReportRequest(submissionId);

            return submissionId;
        } catch (Exception e) {
            throw new RuntimeException("generateReport: Failed.", e);
        }
    }

    public static Long getPartnerSequenceNextVal(String partnerName,
        PartnerSequenceType sequenceType) {
        return PartnerProgramDAO.querySequenceNextVal(partnerName, sequenceType);
    }

    private static float calculateReward(String calculationBasis,
        BigDecimal pointsFactor, BigDecimal dollarOrderTotal,
        BigDecimal dollarMerchandise, int dollarScale, int dollarRoundMethod,
        int rewardScale, int rewardRoundMethod) {
        // Clean the inputs.
        pointsFactor = (pointsFactor == null) ? new BigDecimal(0) : pointsFactor;

        dollarOrderTotal = (dollarOrderTotal == null) ? new BigDecimal(0)
                                                      : dollarOrderTotal.setScale(dollarScale,
                dollarRoundMethod);
        dollarMerchandise = (dollarMerchandise == null) ? new BigDecimal(0)
                                                        : dollarMerchandise.setScale(dollarScale,
                dollarRoundMethod);

        float points = 0;

        if (pointsFactor.floatValue() > 0) {
            // Let's calculate the base points first.
            BigDecimal baseDollarAmount = new BigDecimal(0);

            if (calculationBasis.equals(
                        CalculationBasis.FIXED_AMOUNT_TYPE.toString())) {
                baseDollarAmount = new BigDecimal(1);
            } else if (calculationBasis.equals(
                        CalculationBasis.LINE_ITEM_COUNT_TYPE.toString()) ||
                    calculationBasis.equals(
                        CalculationBasis.MERCHANDISE_TYPE.toString())) {
                baseDollarAmount = dollarMerchandise;
            } else if (calculationBasis.equals(
                        CalculationBasis.ORDER_TOTAL_TYPE.toString())) {
                baseDollarAmount = dollarOrderTotal;
            } else {
                throw new RuntimeException(
                    "getReward: Calculation basis value is unrecognized: " +
                    calculationBasis);
            }

            // This is the base points value.
            points = pointsFactor.multiply(baseDollarAmount)
                                 .setScale(rewardScale, rewardRoundMethod)
                                 .floatValue();
        }

        return points;
    }

    /**
    * IMPORTANT: The method below only processes BASE refunds!!!
    * @return
    * @param rewardRoundMethod
    * @param rewardScale
    * @param dollarRoundMethod
    * @param dollarScale
    * @param activeDate
    * @param partnerName
    */
    public static List getRewardRefund(String partnerName, Date activeDate,
        int dollarScale, int dollarRoundMethod, int rewardScale,
        int rewardRoundMethod) {
        List odVOList = new ArrayList();

        // Obtain the order data and associated terms.
        CachedResultSet order = PartnerProgramDAO.queryPartnerPartialRefunds(partnerName,
                activeDate);

        while (order.next()) {
            // Calculate the refund reward amount.
            float creditReward = calculateReward((String) order.getObject(21),
                    (BigDecimal) order.getObject(22),
                    (BigDecimal) order.getObject(8),
                    (BigDecimal) order.getObject(8), dollarScale,
                    dollarRoundMethod, rewardScale, rewardRoundMethod);

            // Now that all values have been calculated, compose the value object.
            OrderVO orderHeaderVO = new OrderVO();
            orderHeaderVO.setCompanyId((String) order.getObject(1));
            orderHeaderVO.setEmailAddress((String) order.getObject(14));
            orderHeaderVO.setMasterOrderNumber((String) order.getObject(2));

            MembershipVO membershipVO = new MembershipVO();
            membershipVO.setFirstName((String) order.getObject(9));
            membershipVO.setLastName((String) order.getObject(10));
            membershipVO.setMembershipType((String) order.getObject(11));
            membershipVO.setMembershipNumber((String) order.getObject(12));
            membershipVO.setAddress1((String) order.getObject(13));

            OrderDetailVO orderDetailVO = new OrderDetailVO();

            // Partner program metrics
            orderDetailVO.setPstCode((String) order.getObject(16));
            orderDetailVO.setPstCodeBonus((String) order.getObject(17));

            orderDetailVO.setProgramName((String) order.getObject(18));
            orderDetailVO.setRewardType((String) order.getObject(23));
            orderDetailVO.setRewardName((String) order.getObject(24));
            orderDetailVO.setBonusSeparateData((String) order.getObject(27));

            float maximumPoints = (order.getObject(28) == null) ? 0
                                                                : ((BigDecimal) order
                                                                   .getObject(28)).setScale(rewardScale,
                    rewardRoundMethod).floatValue();

            orderDetailVO.setRewardTotalLimit(maximumPoints);

            // Order metrics
            orderDetailVO.setExternalOrderNumber((String) order.getObject(3));
            orderDetailVO.setSourceCode((String) order.getObject(4));

            if (order.getObject(6) != null) {
                orderDetailVO.setOrderDetailId(((BigDecimal) order.getObject(6)).toString());
            }

            orderDetailVO.setBillingTotal((order.getObject(8) == null) ? 0
                                                                       : ((BigDecimal) order
                                                                          .getObject(
                    8)).setScale(dollarScale, dollarRoundMethod).floatValue());
            orderDetailVO.setProductName((String) order.getObject(15));
            orderDetailVO.setRewardBase(creditReward);
            orderDetailVO.setIsRefundReward(true);

            //        orderDetailVO.setRewardBonus(bonusPoints);
            //        orderDetailVO.setRewardMinimum(minPoints);
            orderDetailVO.setDeliveryDate(activeDate);

            // Compose the final object.
            orderDetailVO.setMembershipVO(membershipVO);
            orderDetailVO.setOrderHeaderVO(orderHeaderVO);

            odVOList.add(orderDetailVO);
        }

        return odVOList;
    }

    public static void deletePostedRewardsHistory(String existingLocalFileName) {
        PartnerProgramDAO.deletePostedRewardsByFileName(existingLocalFileName);
    }

    public static void deletePostedRewardsHistory(List orderDetailIdList) {
        for (Iterator i = orderDetailIdList.iterator(); i.hasNext();) {
            PartnerProgramDAO.deletePostedRewardsByOrderDetailId((String) i.next());
        }
    }
}
