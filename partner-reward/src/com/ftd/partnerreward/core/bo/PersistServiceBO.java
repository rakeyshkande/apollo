package com.ftd.partnerreward.core.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.dao.PartnerProgramDAO;
import com.ftd.partnerreward.core.text.IRecordCollection;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


public class PersistServiceBO {
    protected static Logger logger = new Logger(PersistServiceBO.class.getName());
    public static final String FILE_COMPRESSION_TYPE_ZIP = "zip";
    public static final String FILE_COMPRESSION_TYPE_GZIP = "gzip";

    /**
     * Constructor to prevent instantiation of this class.
     */
    private PersistServiceBO() {
        super();
    }

    public static void writeFileBody(String filename,
        IRecordCollection recordCollection) {
        writeFileBody(filename, recordCollection, false);
    }

    public static void writeFileBody(String filename,
        IRecordCollection recordCollection, boolean useLineTerminator) {
        try {
            File file = new File(filename);

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file, true)), true);

            try {
                // Leave empty lines for the header, if any.
                //                for (short count = 0; count < headerLineCount; count++) {
                //                    out.println(TextUtil.padString("", headerLineLength, 'x', false));
                //                }
                // Iterate through the transformation collection, writing each record to the file.
                for (Iterator i = recordCollection.iterator(); i.hasNext();) {
                    if (useLineTerminator) {
                        // Note: A carriage return (and a carriage return only, no new line)
                        // must be used so that we are backward compatible with
                        // partners who expect the file to be generated from
                        // an HP system.
                        out.println(i.next().toString());
                    } else {
                        out.print(i.next().toString());
                    }
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeFileHeader(String filename,
        IRecordCollection recordCollection, boolean useLineTerminator) {
        PersistServiceBO.writeFileHeader(filename, recordCollection, null, useLineTerminator);
    }

    public static void writeFileHeader(String filename,
        IRecordCollection recordCollection) {
        PersistServiceBO.writeFileHeader(filename, recordCollection, null, true);
    }

    public static void writeFileHeader(String filename,
        IRecordCollection recordCollection, String endOfLineTerminator, boolean useLineTerminator) {
        try {
            File file = new File(filename);
            logger.debug(
                "writeFileHeader: writeFileHeader called with filename: " +
                filename);
            

            if (!file.exists()) {
                file.createNewFile();
            }

            // Read the contents of the existing file.
            LineNumberReader in = new LineNumberReader(new FileReader(file));
            List bodyRecordList = new ArrayList();

            try {
                String record = null;

                while ((record = in.readLine()) != null) {
                    bodyRecordList.add(record);

                    logger.debug(
                        "writeFileHeader: This is a READ body record: " +
                        record);
                }
            } finally {
                in.close();
            }

            // Open the file again for writing.
            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file)), true);

            // Write the header.
            for (Iterator i = recordCollection.iterator(); i.hasNext();) {
                String headerLine = i.next().toString();
                if (useLineTerminator) {
                    out.println(headerLine);
                } else {
                    out.print(headerLine);
                }

                logger.debug("writeFileHeader: This is a WRITE header record. " + headerLine);
            }

            // Rewrite the body records.
            for (Iterator i = bodyRecordList.iterator(); i.hasNext();) {
                String detailLine = i.next().toString();

                // Preserve any explicit end-of-line terminators.
                if (endOfLineTerminator != null) {
                    detailLine += endOfLineTerminator;
                }

                if (useLineTerminator) {
                    out.println(detailLine);
                } else {
                    out.print(detailLine);
                }

                logger.debug("writeFileHeader: This is a WRITE body record. " + detailLine);
            }
            
            out.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeFileTrailer(String filename,
        IRecordCollection recordCollection) {
        try {
            File file = new File(filename);

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file, true)), true);

            try {
                for (Iterator i = recordCollection.iterator(); i.hasNext();) {
                    out.println(i.next().toString());
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //    public static void writeToFile(String filename,
    //        String recordCollection, boolean append) {
    //        
    //        try {
    //            PrintWriter out = new PrintWriter(new BufferedWriter(
    //                        new FileWriter(filename, append)));
    //
    //            try {
    //                out.write(recordCollection);
    //            } finally {
    //                out.close();
    //            }
    //        } catch (IOException e) {
    //            throw new RuntimeException(e);
    //        }
    //    }
    public static void recordPostedRewards(String partnerName,
        String postFileName, Date postFileDate, List orderDetailVOList, boolean noPartnerId, boolean rewardByCart) {
        PartnerProgramDAO.insertPostedRewards(partnerName, postFileName,
            postFileDate, orderDetailVOList, noPartnerId, rewardByCart);
    }

    public static String compressFile(String compressionType, String inputFile)
        throws IOException, Exception {
        return PersistServiceBO.compressFile(compressionType, inputFile, null);
    }

    public static String compressFile(String compressionType, String inputFile,
        String zipMemberFileName) throws IOException, Exception {
        String compressedFileName = null;

        if (compressionType.equalsIgnoreCase(FILE_COMPRESSION_TYPE_ZIP)) {
            compressedFileName = PersistServiceBO.createZipFile(inputFile,
                    zipMemberFileName);
        } else if (compressionType.equalsIgnoreCase(FILE_COMPRESSION_TYPE_GZIP)) {
            compressedFileName = PersistServiceBO.createGzipFile(inputFile);
        } else {
            throw new RuntimeException(
                "compressFile: compressionType value is not recognized: " +
                compressionType);
        }

        return compressedFileName;
    }

    private static String createZipFile(String inputFile, String memberFileName)
        throws IOException, Exception {
        /* These are the files to include in the ZIP file */

        //        String[] filenames = new String[] { inputFile };

        /* Create a buffer for reading the files */
        byte[] buf = new byte[1024];

        // Compose the full path output file name.
        String outFilename = inputFile + ".zip";

        /* Create the ZIP file */
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
                    outFilename));

        try {
            /* Compress the files */

            //            for (int i = 0; i < filenames.length; i++) {
            FileInputStream in = new FileInputStream(inputFile);

            try {
                if (memberFileName == null) {
                    // Remove any path from the input file name when naming the entry.
                    memberFileName = inputFile.substring(inputFile.lastIndexOf(
                                File.separator) + 1, inputFile.length());
                }

                /* Add ZIP entry to output stream. */
                out.putNextEntry(new ZipEntry(memberFileName));

                /* Transfer bytes from the file to the ZIP file */
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                /* Complete the entry */
                out.closeEntry();
            } finally {
                in.close();
            }

            //            }
        } finally {
            // Complete the ZIP file */
            out.close();
        }

        return outFilename;
    }

    private static String createGzipFile(String inputFile)
        throws IOException, Exception {
        /* Create a buffer for reading the files */
        byte[] buf = new byte[1024];

        // Compose the full path output file name.
        String outFilename = inputFile + ".gz";

        /* Create the ZIP file */
        GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(
                    outFilename));

        try {
            FileInputStream in = new FileInputStream(inputFile);

            try {
                /* Transfer bytes from the file to the ZIP file */
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                in.close();
            }
        } finally {
            // Complete the ZIP file */
            out.close();
        }

        return outFilename;
    }

    /**
    * Overwrites contents of a file at the specified line column position.
    * Note: Header data is preserved, but trailer data may be affected.
    * @param value
    * @param column
    * @param headerLineCount
    * @param inputFile
    */
    public static void overwriteFileContents(String inputFile,
        int headerLineCount, int trailerLineCount, int column, String value) {
        if (column == 0) {
            throw new RuntimeException(
                "overwriteFileContents: Column position input value can not be zero.");
        }

        try {
            // Open the file for writing.
            RandomAccessFile pointer = new RandomAccessFile(inputFile, "rw");

            logger.debug("overwriteFileContents: At file position: " +
                pointer.getFilePointer());

            for (int count = 0; count < headerLineCount; count++) {
                pointer.readLine();
            }

            logger.debug(
                "overwriteFileContents: After headers, at file position: " +
                pointer.getFilePointer());

            while (pointer.skipBytes(column) == column) {
                logger.debug(
                    "overwriteFileContents: After bytes skip, at file position: " +
                    pointer.getFilePointer());
                pointer.write(value.getBytes());
                logger.debug(
                    "overwriteFileContents: After write, at file position: " +
                    pointer.getFilePointer());

                // Read to end-of-current-line
                String endOfLineString = pointer.readLine();
                logger.debug("overwriteFileContents: Reading line: " +
                    endOfLineString);

                // Record the length.
                long offset = pointer.getFilePointer();
                logger.debug("overwriteFileContents: At file position: " +
                    offset);

                // The folowing logic preserves trailer lines.
                // Read forward to check for any trailers and EOF.
                if (trailerLineCount > 0) {
                    String forwardLine = null;

                    // Read one more line than the trailer count.
                    for (int count = 0; count <= trailerLineCount; count++) {
                        forwardLine = pointer.readLine();
                    }

                    if (forwardLine == null) {
                        // EOF is reached.
                        break;
                    } else {
                        // Restore the pointer to the previous location.
                        pointer.seek(offset);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
   *  Currently, only ZIP compression type is supported. 
   * @param inputFile
   * @param compressionType
   */
    public static void uncompressFile(String compressionType, String inputFile) {
        try {
            logger.debug("inputFile: " + inputFile);
            logger.debug("CompressionType: " + compressionType);

            BufferedOutputStream destination = null;
            FileInputStream finps = new FileInputStream(inputFile);
            ZipInputStream zipinp = new ZipInputStream(new BufferedInputStream(
                        finps));
            ZipEntry entry;

            while ((entry = zipinp.getNextEntry()) != null) {

                int count;
                byte[] databuff = new byte[1024];

                // write the files 
                FileOutputStream fos = new FileOutputStream(entry.getName());
                destination = new BufferedOutputStream(fos, 1024);

                while ((count = zipinp.read(databuff, 0, 1024)) != -1) {
                    destination.write(databuff, 0, count);
                }

                destination.flush();
                destination.close();
            }

            zipinp.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
