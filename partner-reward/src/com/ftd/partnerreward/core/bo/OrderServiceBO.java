package com.ftd.partnerreward.core.bo;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.dao.OrderDAO;
import com.ftd.partnerreward.core.dao.PartnerProgramDAO;
import com.ftd.partnerreward.core.vo.GlobalParameterVO;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class OrderServiceBO {
    protected static Logger logger = new Logger(OrderServiceBO.class.getName());

    /**
     * Constructor to prevent instantiation of this class.
     */
    private OrderServiceBO() {
        super();
    }

    public static List getOrderDetailIds(String partnerName, Date activeDate,
        Date orderDateThreshold, int startRecord, int groupSize,
        List resultMetadata) throws Exception {
        String globalContext = IWorkflowConstants.GLOBAL_PARMS_CONTEXT;
        String globalName = IWorkflowConstants.GLOBAL_PARMS_NAME;
        String daysPastDelivery = "0";

        GlobalParameterVO globalParameterVO = OrderDAO.getGlobalParameter(globalContext,
                globalName);

        if (globalParameterVO != null) {
            daysPastDelivery = new String(globalParameterVO.getValue());
        }

        CachedResultSet crs = OrderDAO.queryOrderDetailIds(partnerName,
                activeDate, orderDateThreshold, startRecord, groupSize,
                daysPastDelivery, resultMetadata);

        List resultList = new ArrayList(crs.getRowCount());
        
        // Defect 937: The following logic accomodates any merged partner.
        // AMERICA WEST was merged into US AIRWAYS.
        if (partnerName.equalsIgnoreCase("US AIRWAYS")) {
            CachedResultSet crsMerger = OrderDAO.queryOrderDetailIds("AMERICA WEST",
                    activeDate, orderDateThreshold, startRecord, groupSize,
                    daysPastDelivery, resultMetadata);

            // Combine the two result sets into one.
            resultList = new ArrayList(crs.getRowCount() +
                    crsMerger.getRowCount());

            // This transfer pattern is "wasteful" but necessary, since the jBPM
            // framework can context-bind only serializable objects, which
            // CachedResultSet is not.
            while (crsMerger.next()) {
                resultList.add(String.valueOf(
                        ((BigDecimal) crsMerger.getObject(1)).intValue()));
            }
        }

        // This transfer pattern is "wasteful" but necessary, since the jBPM
        // framework can context-bind only serializable objects, which
        // CachedResultSet is not.
        while (crs.next()) {
            resultList.add(String.valueOf(
                    ((BigDecimal) crs.getObject(1)).intValue()));
        }

        return resultList;
    }

    public static String getOrderPartnerName(String orderDetailId,
        Date activeDate) {
        // First, obtain the source code for the order.
        CachedResultSet crsOrder = OrderDAO.queryOrderDetail(orderDetailId);

        if (crsOrder.getRowCount() == 0) {
            return null;
        }

        crsOrder.next();

        String sourceCode = (String) crsOrder.getObject(4);

        if (sourceCode == null) {
            throw new RuntimeException(
                "getOrderPartnerName: Source code can not be null for order: " +
                orderDetailId);
        }

        // Next, obtain the active partner name (and program) for the source.
        CachedResultSet crsPartner = PartnerProgramDAO.queryPartnerProgramTerms(sourceCode,
                activeDate);

        // Only a single record should return.
        if (crsPartner.getRowCount() != 1) {
            throw new RuntimeException(
                "getOrderPartnerName: One and only one partner can match order detail id " +
                orderDetailId);
        }

        crsPartner.next();

        return (String) crsPartner.getObject(15);
    }

    //    public static CachedResultSet getOrderTotals(String orderDetailId) 
    //    {
    //      return OrderDAO.queryOrderDetailTotals(orderDetailId);
    //    }
}
