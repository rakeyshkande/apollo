package com.ftd.partnerreward.core.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class DelimitedRecordCollection extends BaseRecordCollection {
    private char delimitingChar;

    DelimitedRecordCollection(boolean isUpperCase, char delimitingChar) {
        super(isUpperCase);
        this.delimitingChar = delimitingChar;
    }

    public IRecordLine createLineInstance() {
        return new DelimitedRecordLine(isUpperCase(), delimitingChar);
    }
}
