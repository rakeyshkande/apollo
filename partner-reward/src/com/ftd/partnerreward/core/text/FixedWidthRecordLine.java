package com.ftd.partnerreward.core.text;

class FixedWidthRecordLine implements IRecordLine {
    private StringBuffer sb = new StringBuffer();
    private boolean isUpperCase = false;

    FixedWidthRecordLine(boolean isUpperCase) {
        this.isUpperCase = isUpperCase;
    }

    public void addStringValue(String value, int fieldLength) {
        if (isUpperCase) {
            value = value == null? value : value.toUpperCase();
        }

        sb.append(TextUtil.padString(value, fieldLength, ' ', false));
    }

    public void addStringValue(String value) {
        if (isUpperCase) {
            value = value == null? value : value.toUpperCase();
        }

        addStringValue(value, value.length());
    }

    public void addNumberValue(String value, int fieldLength) {
        sb.append(TextUtil.padString(value, fieldLength, '0', true));
    }
    
    public void addNumberValue(String value, int fieldLength, char padChar)  {
        sb.append(TextUtil.padString(value, fieldLength, padChar, true));
    }

//    public void markEndOfLine() {
////        sb.append("\r\n");
//    }

    public String toString() {
        return sb.toString();
    }
}
