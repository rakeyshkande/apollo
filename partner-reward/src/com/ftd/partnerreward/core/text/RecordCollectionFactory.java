package com.ftd.partnerreward.core.text;

public class RecordCollectionFactory {

    private RecordCollectionFactory() {
    }

    public static IRecordCollection getDelimitedRecordCollection(
        boolean isUpperCase, char delimitingChar) {
        return new DelimitedRecordCollection(isUpperCase, delimitingChar);
    }

    public static IRecordCollection getFixedWidthRecordCollection(boolean isUpperCase) {
        return new FixedWidthRecordCollection(isUpperCase);
    }
}
