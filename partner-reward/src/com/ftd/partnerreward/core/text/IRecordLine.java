package com.ftd.partnerreward.core.text;
import java.io.Serializable;

public interface IRecordLine extends Serializable {
    public void addStringValue(String value, int fieldLength);
    public void addStringValue(String value);
    public void addNumberValue(String value, int fieldLength);
    public void addNumberValue(String value, int fieldLength, char padChar);
//    public void markEndOfLine();
    public String toString();
}
