package com.ftd.partnerreward.core.text;
import java.io.Serializable;
import java.util.Iterator;

public interface IRecordCollection extends Serializable {
    public IRecordLine createLineInstance();

    public void addRecordLine(IRecordLine recordLine);
    public int size();
    public Iterator iterator();
    public String toString();
}
