package com.ftd.partnerreward.core.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class FixedWidthRecordCollection extends BaseRecordCollection {
    FixedWidthRecordCollection(boolean isUpperCase) {
        super(isUpperCase);
    }

    public IRecordLine createLineInstance() {
        return new FixedWidthRecordLine(isUpperCase());
    }
}
