package com.ftd.partnerreward.core.text;

class DelimitedRecordLine implements IRecordLine {
    private char delimitingChar;
    private StringBuffer sb = new StringBuffer();
    private boolean isUpperCase = false;

    DelimitedRecordLine(boolean isUpperCase, char delimitingChar) {
        this.isUpperCase = isUpperCase;
        this.delimitingChar = delimitingChar;
    }

    public void addStringValue(String value, int fieldLength) {
        throw new UnsupportedOperationException(
            "addStringValue: This method is not implemented.");
    }

    public void addStringValue(String value) {
        if (sb.length() > 0) {
            sb.append(delimitingChar);
        }

        if (value != null) {
            if (isUpperCase) {
                value = value.toUpperCase();
            }

            sb.append(value);
        }

        //        sb.append(delimitingChar);
    }

    public void addNumberValue(String value, int fieldLength) {
        throw new UnsupportedOperationException(
            "addNumberValue: This method is not implemented.");
    }

    public void addNumberValue(String value, int fieldLength, char padChar) {
        throw new UnsupportedOperationException(
            "addNumberValue: This method is not implemented.");
    }

    //    public void markEndOfLine() {
    //        // Strip the last delimiter.
    //        if (sb.charAt(sb.length() - 1) == delimitingChar) {
    //            sb.deleteCharAt(sb.length() - 1);
    //        }
    //    }
    public String toString() {
        return sb.toString();
    }
}
