package com.ftd.partnerreward.core.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


abstract class BaseRecordCollection implements IRecordCollection {
    private List recordList = new ArrayList();
    private boolean isUpperCase = false;

    BaseRecordCollection(boolean isUpperCase) {
        this.isUpperCase = isUpperCase;
    }

    public void addRecordLine(IRecordLine recordLine) {
//        recordLine.markEndOfLine();
        recordList.add(recordLine);
    }

    public String toString() {
        // Iterate through the collection.
        StringBuffer sb = new StringBuffer();

        for (Iterator i = recordList.iterator(); i.hasNext();) {
            IRecordLine recordLine = (IRecordLine) i.next();
            sb.append(recordLine.toString());
        }

        return sb.toString();
    }

    public int size() {
        return recordList.size();
    }
    
    public Iterator iterator() {
        return recordList.iterator();
    }

    protected boolean isUpperCase() {
        return isUpperCase;
    }
}
