package com.ftd.partnerreward.core.text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;


public class TextUtil {
    protected static final DateFormat _DF = DateFormat.getDateInstance(DateFormat.SHORT);

        
    public static String padString(String s, int n, char c, boolean paddingLeft) {
        StringBuffer str = new StringBuffer((s == null) ? "" : s);
        int strLength = str.length();

        if (n > 0) {
            if (n > strLength) {
                for (int i = 0; i <= n; i++) {
                    if (paddingLeft) {
                        if (i < (n - strLength)) {
                            str.insert(0, c);
                        }
                    } else {
                        if (i > strLength) {
                            str.append(c);
                        }
                    }
                }
            } else if (n < strLength) {
                // Return a truncated string.
                str.setLength(n);
            }
        }

        return str.toString();
    }
    
    
    /**
     * Will pad and trunc the String, including specifying the direction of pad and trunc.
     * Example: If you want to pad right, paddingLeft=false.
     * @return
     * @param s
     */
    public static String smartPadAndTruncString(String s, int n, char c, boolean paddingLeft, boolean truncLeft) {
        StringBuffer str = new StringBuffer((s == null) ? "" : s);
        int strLength = str.length();

        if (n > 0) {
            if (n > strLength) {
                for (int i = 0; i <= n; i++) {
                    if (paddingLeft) {
                        if (i < (n - strLength)) {
                            str.insert(0, c);
                        }
                    } else {
                        if (i > strLength) {
                            str.append(c);
                        }
                    }
                }
            } else if (n < strLength) {
            	if (truncLeft){
            		str.delete(0, strLength-n);
            	}
            	else{
                    str.setLength(n);
            	}
            }
        }

        return str.toString();
    }
    

    /**
    * Will strip all special characters from a string, including spaces.
    * @return
    * @param s
    */
    public static String stripSpecialChars(String s) {
        if (s == null) {
            return s;
        }

        String goodChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuffer result = new StringBuffer(s.length());

        for (int i = 0; i < s.length(); i++) {
            if (goodChars.indexOf(s.charAt(i)) >= 0) {
                result.append(s.charAt(i));
            }
        }

        return result.toString();
    }

    /**
    * Will strip all non-numeric characters from a string, including spaces.
    * @return
    * @param s
    */
    public static String stripNonNumericChars(String s) {
        if (s == null) {
            return s;
        }

        String goodChars = "0123456789";
        StringBuffer result = new StringBuffer(s.length());

        for (int i = 0; i < s.length(); i++) {
            if (goodChars.indexOf(s.charAt(i)) >= 0) {
                result.append(s.charAt(i));
            }
        }

        return result.toString();
    }
    
    /**
    * Will strip X number of characters from a String.
    * @return
    * @param s
    * @param numberOfChartsToStrip
    */
    public static String stripBeginningChars(String s, int numberOfCharsToStrip) {
        if (s == null) {
            return s;
        }

        return s.substring(numberOfCharsToStrip);
    }
    
    /**
    * Will strip all tab characters and carriage returns from a string.
    * @return
    * @param s
    */
    public static String stripTabs(String inString) {

        if (inString == null) {
            return inString;
        }

        String tab = "\t";
        StringBuffer result = new StringBuffer(inString.length());
        String s = null;

        inString = inString.replaceAll("\\n","<br>");
        inString = inString.replaceAll("\\r","<br>");

        for (int i=0; i < inString.length(); i++) {
            s = inString.substring(i,i+1);
            if (!s.equals(tab)) {
                result.append(inString.charAt(i));
            }
        }

        return result.toString();
    }

    /**
     * Will strip all pipe characters and carriage returns from a string.
     * @return
     * @param s
     */
    public static String stripPipe(String inString) {

        if (inString == null) {
            return inString;
        }

        String pipe = "|";
        StringBuffer result = new StringBuffer(inString.length());
        String s = null;

        inString = inString.replaceAll("\\|","");

        for (int i=0; i < inString.length(); i++) {
            s = inString.substring(i,i+1);
            if (!s.equals(pipe)) {
                result.append(inString.charAt(i));
            }
        }

        return result.toString();
    }

    public static String cleanPartnerName(String partnerName) {
        return stripSpecialChars(partnerName).toLowerCase();
    }

    //    public static String formatDate(Date thisDate) {
    //        return _DF.format(thisDate);
    //    }
    public static String formatDate(Date thisDate, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        return sdf.format(thisDate);
    }
    
    public static Date parseString(String dateString, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        return sdf.parse(dateString);
    }

    public static String trim(String str) {
        return (str != null) ? str.trim() : str;
    }
}
