package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.RewardServiceBO;

import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.RewardCalcParms;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class StandardCalculator implements ActionHandler {
    protected Logger logger = new Logger(StandardCalculator.class.getName());

    // These fields will be initialized by the process archive.
    //    protected Float bankBalanceThreshold;
    protected int dollarScale;
    protected String dollarRoundMethod;
    protected int rewardScale;
    protected String rewardRoundMethod;
    
    // OPTIONAL indicator that designates the reward calculation should be
    // based on the actual credit card charge amount rather than the order billing.
    // Default is false (i.e. reward calculation uses the order billing)
    protected boolean isCreditCardBased;
    
    // OPTIONAL indicator that designates if the partner promotion has a "merchandise"
    // calculation basis, the reward calculation should NOT consider add-ons
    // merchandise.
    // Default is false (i.e. reward calculation will include add-ons as merchandise)
//    protected boolean isMdseAddOnExcluded;

    public StandardCalculator() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List orderDetailIdList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT);

            // Obtain the active date from the context.
            Date activeDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE);

            // Translate the desingated round methods.
            int dollarRoundMethodCode = translateRoundMethod(dollarRoundMethod);
            int rewardRoundMethodCode = translateRoundMethod(rewardRoundMethod);

            // Iterate through each order, calculating the reward.
            // Compose a list of reward objects.
            List rewardList = new ArrayList(orderDetailIdList.size());

            for (Iterator i = orderDetailIdList.iterator(); i.hasNext();) {
                String orderDetailId = (String) i.next();
                RewardCalcParms rewardCalcParms = new RewardCalcParms();
                rewardCalcParms.setCreditCardBased(isCreditCardBased);
//                rewardCalcParms.setMdseAddOnExcluded(isMdseAddOnExcluded);
                OrderDetailVO odVO = RewardServiceBO.getReward(orderDetailId,
                        activeDate, dollarScale, dollarRoundMethodCode,
                        rewardScale, rewardRoundMethodCode, rewardCalcParms);

                if (odVO != null) {
                    rewardList.add(odVO);
                }
            }

            // Bind the reward list to the process context.
            executionContext.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);

            // Advance the process token.
            // Verify if the workflow must now end.
            Boolean endIndicator = (Boolean) executionContext.getContextInstance()
                                                             .getVariable(IWorkflowConstants.CONTEXT_KEY_END_IND);

            if ((endIndicator != null) && endIndicator.booleanValue()) {
                // End the workflow.
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_END;
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    public static int translateRoundMethod(String roundMethod) {
        if (roundMethod.equalsIgnoreCase("ROUND_CEILING")) {
            return BigDecimal.ROUND_CEILING;
        } else if (roundMethod.equalsIgnoreCase("ROUND_DOWN")) {
            return BigDecimal.ROUND_DOWN;
        } else if (roundMethod.equalsIgnoreCase("ROUND_FLOOR")) {
            return BigDecimal.ROUND_FLOOR;
        } else if (roundMethod.equalsIgnoreCase("ROUND_HALF_DOWN")) {
            return BigDecimal.ROUND_HALF_DOWN;
        } else if (roundMethod.equalsIgnoreCase("ROUND_HALF_EVEN")) {
            return BigDecimal.ROUND_HALF_EVEN;
        } else if (roundMethod.equalsIgnoreCase("ROUND_HALF_UP")) {
            return BigDecimal.ROUND_HALF_UP;
        } else if (roundMethod.equalsIgnoreCase("ROUND_UP")) {
            return BigDecimal.ROUND_UP;
        } else {
            throw new RuntimeException(
                "translateRoundMethod: Designated round method is unrecognized: " +
                roundMethod);
        }
    }
}
