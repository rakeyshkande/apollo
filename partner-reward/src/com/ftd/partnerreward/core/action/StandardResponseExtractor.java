package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Date;


public class StandardResponseExtractor extends BaseResponseExtractor {
    protected Logger logger = new Logger(StandardResponseExtractor.class.getName());
    
    public StandardResponseExtractor() {
        super();
    }

    // The default behavior is to return a standard composition of the
    // file name.
    protected String composeResponseFilePathLocal(String filePath,
        String fileName, Date retrievalDate) {
        return ActionUtil.composePostFilePathLocal(filePath, fileName,
            retrievalDate);
    }

    // The default behavior is to return the file name verbatim as specified in the
    // process definition file.
    protected String composeResponseFileNameRemote(String fileName,
        Date retrievalDate) {
        return fileName;
    }

    // Unfortunately, because of the jBPM framework, the accessors below must exist in
    // the implementing class (can not only be inherited from base class).
    protected void setAlertBody(String alertBody) {
        super.setAlertBody(alertBody);
    }

    protected void setAlertSubject(String alertSubject) {
        super.setAlertSubject(alertSubject);
    }

    protected void setFtpDir(String ftpDir) {
        super.setFtpDir(ftpDir);
    }

    protected void setFtpPassword(String ftpPassword) {
        super.setFtpPassword(ftpPassword);
    }

    protected void setFtpPort(int ftpPort) {
        super.setFtpPort(ftpPort);
    }

    protected void setFtpServer(String ftpServer) {
        super.setFtpServer(ftpServer);
    }

    protected void setFtpUsername(String ftpUsername) {
        super.setFtpUsername(ftpUsername);
    }

    protected void setLocalFileName(String localFileName) {
        super.setLocalFileName(localFileName);
    }

    protected void setLocalFilePath(String localFilePath) {
        super.setLocalFilePath(localFilePath);
    }

    protected void setRemoteFileName(String remoteFileName) {
        super.setRemoteFileName(remoteFileName);
    }
    
    public void setRemoteFileNameIsPattern(boolean remoteFileNameIsPattern) {
        super.remoteFileNameIsPattern = remoteFileNameIsPattern;
    }

    public boolean isRemoteFileNameIsPattern() {
        return super.remoteFileNameIsPattern;
    }        
}
