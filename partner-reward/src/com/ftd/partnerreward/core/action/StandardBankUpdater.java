package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.RewardServiceBO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.List;


public class StandardBankUpdater implements ActionHandler {
    protected Logger logger = new Logger(StandardBankUpdater.class.getName());

    // These fields will be initialized by the process archive.
    protected Float bankBalanceThreshold;
    protected String alertSubject;
    protected String alertBody;

    public StandardBankUpdater() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

            logger.debug("execute: alertSubject=" + alertSubject + " alertBody=" + alertBody);
            RewardServiceBO.updateBankBalance(partnerName, rewardList,
                bankBalanceThreshold, alertSubject, alertBody);

            // Re-bind the revised reward list to the process context.
            executionContext.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
