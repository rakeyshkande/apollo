package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.bo.TransportServiceBO;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.File;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public abstract class BaseResponseExtractor implements ActionHandler {
    private static Map partnerNameToProject = new HashMap();

    // Used to obtain Secure Configuration Properties
    private static final String SECURE_CONFIG_CONTEXT = "partner_reward";
    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String INBOUND_USERNAME = "_InboundFtpUser";
    private static final String INBOUND_PASSWORD = "_InboundFtpPswd";
    private static final String INBOUND_SERVER = "_INBOUND_SERVER";

    static {
        // Create a lookup table of "project" name to partner name.
        // @todo In a later release, make this db data-driven rather than hardcode.
        partnerNameToProject.put("ALASKA AIRLINES", "1");
        partnerNameToProject.put("AMERICAN AIRLINES", "1");
        partnerNameToProject.put("AMERICA WEST", "1");
        partnerNameToProject.put("NORTHWEST AIRLINES", "1");
        partnerNameToProject.put("QUIXTAR/FLORAGIFT", "1");

        partnerNameToProject.put("AIR CANADA", "2");
        partnerNameToProject.put("BEST WESTERN", "2");
        partnerNameToProject.put("DELTA AIRLINES", "2");
        partnerNameToProject.put("UNITED AIRLINES", "2");
        partnerNameToProject.put("US AIRWAYS", "2");
    }

    protected Logger logger = new Logger(BaseResponseExtractor.class.getName());

    // These fields will be initialized by the process archive.
    private String ftpServer;
    private String ftpDir;
    private String ftpUsername;
    private String ftpPassword;
    private int ftpPort;
    private String remoteFileName;
    private String localFilePath;
    private String alertSubject;
    private String alertBody;
    protected boolean remoteFileNameIsPattern;

    // OPTIONAL Specifies the locally mandated file name
    private String localFileName;

    // OPTIONAL transmission type indicator.  True means a binary transfer.
    // Default is false (i.e. ASCII transfer)
    protected boolean isBinary;

    // OPTIONAL indicator to prevent overwriting any existing file on the remote server.
    // Default is false (i.e. no protection; file CAN be overwritten)
    protected boolean isOverwriteProtected;

    public BaseResponseExtractor() {
    }

    protected abstract String composeResponseFilePathLocal(String filePath,
        String fileName, Date retrievalDate);

    protected abstract String composeResponseFileNameRemote(String fileName,
        Date retrievalDate);

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            
            // Obtain Secure username and password
            if(partnerName != null) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String consPartnerName = TextUtil.stripSpecialChars(partnerName);
                ftpUsername = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_USERNAME) );
                ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_PASSWORD) );
                ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_SERVER) );
                ActionUtil.logInfo(logger,executionContext,"FTP Server is " + ftpServer);                    
            } else {
                throw new Exception("partnerName is null.  partnerName is required to obtain the Secure Configuration Properties.");  
            }

            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            if (responseFilePathLocal == null) {
                responseFilePathLocal = new ArrayList();
            }

            // Compose the retrieval date.  This is the current date.
            Date retrievalDate = new Date();

            // Compose the remote file name.
            String cleanRemoteFileName = composeResponseFileNameRemote(remoteFileName,
                    retrievalDate);

            // Compose the local file name.
            String fullFileNameLocal = null;

            if (localFileName == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(localFilePath,
                        cleanRemoteFileName, retrievalDate);
            } else {
                fullFileNameLocal = composeResponseFilePathLocal(localFilePath,
                        localFileName, retrievalDate);
            }

            // Obtain the response file from the remote partner server.
            boolean isSuccess = false;
            logger.debug("remoteFileNameIsPattern:" + remoteFileNameIsPattern);
            if(remoteFileNameIsPattern) {
                // Retrieve all file that match
                List fileList = TransportServiceBO.getFtpAdapter().prDownloadFiles 
                (localFilePath, remoteFileName, ftpDir, ftpServer, ftpUsername, ftpPassword, isBinary);
                if(fileList != null && fileList.size() > 0) {
                    isSuccess = true;
                    fullFileNameLocal = (String)fileList.get(0); // for alert
                    cleanRemoteFileName = (String)fileList.get(0); // for alert
                } else {
                    cleanRemoteFileName = cleanRemoteFileName + "*"; // for alert
                }
            } else {
                isSuccess = TransportServiceBO.getFtpAdapter().prDownload(fullFileNameLocal,
                    cleanRemoteFileName, ftpDir, ftpServer, ftpUsername,
                    ftpPassword, isBinary);
            }

            if (isSuccess) {
                ActionUtil.logInfo(logger, executionContext,
                    fullFileNameLocal + " successfully obtained " +
                    partnerName + " response file from " + ftpServer + ":" +
                    ftpDir + File.separator + cleanRemoteFileName);

                // Bind the local file to the context list.
                responseFilePathLocal.add(fullFileNameLocal);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL,
                    responseFilePathLocal);

                // Provide an alert to interested parties.
                String groupId = (String) partnerNameToProject.get(partnerName);

                if (groupId == null) {
                    groupId = "3";
                }

                AlertService.processAlert("RESPONSE GROUP " + groupId,
                    alertSubject + " " + partnerName,
                    alertBody + " " + fullFileNameLocal);
            } else {
                ActionUtil.logInfo(logger, executionContext,
                    "Could not obtain " + partnerName + " response file from " +
                    ftpServer + ":" + ftpDir + File.separator +
                    cleanRemoteFileName + " because it does not exist.");

                // End the workflow.
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_END;
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    protected String getAlertBody() {
        return alertBody;
    }

    protected void setAlertBody(String alertBody) {
        this.alertBody = alertBody;
    }

    protected String getAlertSubject() {
        return alertSubject;
    }

    protected void setAlertSubject(String alertSubject) {
        this.alertSubject = alertSubject;
    }

    protected String getFtpDir() {
        return ftpDir;
    }

    protected void setFtpDir(String ftpDir) {
        this.ftpDir = ftpDir;
    }

    protected String getFtpPassword() {
        return ftpPassword;
    }

    protected void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    protected int getFtpPort() {
        return ftpPort;
    }

    protected void setFtpPort(int ftpPort) {
        this.ftpPort = ftpPort;
    }

    protected String getFtpServer() {
        return ftpServer;
    }

    protected void setFtpServer(String ftpServer) {
        this.ftpServer = ftpServer;
    }

    protected String getFtpUsername() {
        return ftpUsername;
    }

    protected void setFtpUsername(String ftpUsername) {
        this.ftpUsername = ftpUsername;
    }

    protected String getLocalFileName() {
        return localFileName;
    }

    protected void setLocalFileName(String localFileName) {
        this.localFileName = localFileName;
    }

    protected String getLocalFilePath() {
        return localFilePath;
    }

    protected void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    protected String getRemoteFileName() {
        return remoteFileName;
    }

    protected void setRemoteFileName(String remoteFileName) {
        this.remoteFileName = remoteFileName;
    }


    public void setRemoteFileNameIsPattern(boolean remoteFileNameIsPattern) {
        this.remoteFileNameIsPattern = remoteFileNameIsPattern;
    }

    public boolean isRemoteFileNameIsPattern() {
        return remoteFileNameIsPattern;
    }
}
