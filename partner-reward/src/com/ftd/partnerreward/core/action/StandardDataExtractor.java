package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.OrderServiceBO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class StandardDataExtractor implements ActionHandler {
    protected static final String _CONTEXT_KEY_TOTAL_DAILY_RECORD_COUNT = "_keyTotalDailyRecordCount";

    //    protected static final String _CONTEXT_KEY_CURRENT_RECORD_COUNT = "_keyCurrentRecordCount";
//    protected static final String _CONTEXT_KEY_TARGET_DATE = "_keyTargetDate";
    protected Logger logger = new Logger(StandardDataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected int historicalTimeSpanValue;
    protected String historicalTimeSpanUnit;
    protected int recordGroupSize;
    protected int orderDateThresholdDays;

    public StandardDataExtractor() {
    }

    protected static Date incrementDate(Date thisDate) {
        Calendar activeCalendar = Calendar.getInstance();
        activeCalendar.setTimeInMillis(thisDate.getTime());
        activeCalendar.add(Calendar.DATE, 1);

        return activeCalendar.getTime();
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the candidate order details Ids for the needed time span.
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            
            // Since partner name is used to obtain the process definition XML
            // that activated this workflow, this is the only metric that
            // is not obtained from the definition XML, and must be known.
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            // Initialize a List that will contain all order detail IDs obtained
            // in this pass.
            List aggregateOrderDetailIdList = new ArrayList();

            // First, get the date span.
            // Clean the end date.
            if (endDate == null) {
                throw new RuntimeException("End Date value can not be null.");
            } else {
                endDate = ActionUtil.setToZeroHour(endDate);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE,
                    endDate);
            }

            // Initalize the order date threshold based on the end date (i.e. the batch run date).
            Calendar orderDateThreshold = Calendar.getInstance();
            orderDateThreshold.setTimeInMillis(endDate.getTime());
            orderDateThreshold.add(Calendar.DATE, orderDateThresholdDays * -1);

            // Initalize the start date since it can either be specified (when
            // placed in the context), or derived based on the process archive file.
            if (startDate == null) {
                Calendar startCal = Calendar.getInstance();
                startCal.setTimeInMillis(endDate.getTime());

                int unit = 0;

                if (historicalTimeSpanUnit.equalsIgnoreCase("DAY")) {
                    unit = Calendar.DATE;
                } else if (historicalTimeSpanUnit.equalsIgnoreCase("MONTH")) {
                    unit = Calendar.MONTH;
                } else if (historicalTimeSpanUnit.equalsIgnoreCase("YEAR")) {
                    unit = Calendar.YEAR;
                }

                // In the past.
                startCal.add(unit, historicalTimeSpanValue * -1);

                startDate = startCal.getTime();
            } else {
                // Clean the start date.
                startDate = ActionUtil.setToZeroHour(startDate);
            }

            context.setVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE,
                startDate);

            // Cycle for each day, since the partner program terms and conditions
            // must be established for each order on a specific day.
            Date targetDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE); 
//            Date targetDate = (Date) context.getVariable(_CONTEXT_KEY_TARGET_DATE);

            if (postedFileDate == null) {
              // Bind the post date timestamp to the context.
              context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE,
                    new Date());
            }
            
            if (targetDate == null) {
                // default to the start date.
                targetDate = new Date();
                targetDate.setTime(startDate.getTime());
            } else {
                // Increment the active date.
                if (targetDate.before(endDate)) {
                    targetDate = incrementDate(targetDate);
                } else {
                    // This indicates that all days have been processed, so
                    // skip forward to the appropriate workflow node.
                    ActionUtil.logInfo(logger, executionContext,
                        "All days have been processed. EXTRACT COMPLETE.");
                    tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_EXTRACT_COMPLETE;
                }
            }

            if (tokenSignal == null) {
                do {
                    // Perform the data extraction using the metrics established above.
                    //                    List resultMetadata = new ArrayList();
                    // Note: Because of intra-day aggregations which may be performed
                    // at the later transformation step, we must extract a full
                    // day of activity.  (Also note that the requested result set
                    // size is obscenely large, meaning that all records for
                    // the target date are being requested.)
                    List orderDetailIdList = OrderServiceBO.getOrderDetailIds(partnerName,
                            targetDate, orderDateThreshold.getTime(), 1,
                            1000000000, new ArrayList());

                    // Bind these latest metrics to the context.                    
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE, targetDate);
//                    context.setVariable(_CONTEXT_KEY_TARGET_DATE, targetDate);

                    // Append the latest results to the aggregate list.
                    if (orderDetailIdList.size() > 0) {
//                        for (Iterator i = orderDetailIdList.iterator();
//                                i.hasNext();) {
//                            aggregateOrderDetailIdList.add((String) i.next());
//                        }
                        aggregateOrderDetailIdList.addAll(orderDetailIdList);
                    }
                    
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT,
                            aggregateOrderDetailIdList);

                    // Record a ROLLING total record count across all days.
                    Integer totalRollingRecordNumber = (Integer) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TOTAL_ROLLING_RECORD_COUNT);

                    if (totalRollingRecordNumber == null) {
                        totalRollingRecordNumber = new Integer(orderDetailIdList.size());
                    } else {
                        totalRollingRecordNumber = new Integer(totalRollingRecordNumber.intValue() +
                                orderDetailIdList.size());
                    }

                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_TOTAL_ROLLING_RECORD_COUNT,
                        totalRollingRecordNumber);

                    ActionUtil.logInfo(logger, executionContext,
                        "Extracted rows from DB for calculation: " +
                        orderDetailIdList.size(), targetDate);

                    // With the latest data extraction, set a context indicator if more
                    // rows need to be extracted.
                    // This indicator will be referenced by other actors in the workflow.
                    if (!targetDate.before(endDate)) {
                        context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND,
                            new Boolean(true));
                    }

                    if (aggregateOrderDetailIdList.size() < recordGroupSize) {
                        // Increment to the next day in the range.
                        targetDate = incrementDate(targetDate);
                    } else {
                        // Process the latest batch of records.
                        break;
                    }
                } while (!targetDate.after(endDate));
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
}
