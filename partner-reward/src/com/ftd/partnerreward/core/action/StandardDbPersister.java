package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.PersistServiceBO;

import java.util.Date;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class StandardDbPersister implements ActionHandler {
    protected Logger logger = new Logger(StandardDbPersister.class.getName());

    protected boolean noPartnerId;
    protected boolean rewardByCart;
    
    public StandardDbPersister() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            Object fullFileNameLocal = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            String local = "Not Applicable";

            if (fullFileNameLocal instanceof java.util.List) {
                List fullFileNameLocalList = (List) fullFileNameLocal;

                if (fullFileNameLocalList.size() > 0) {
                // Simply use the first file name record.
                local = (String) fullFileNameLocalList.get(0);
                }
            } else if (fullFileNameLocal instanceof String) {
                local = (String) fullFileNameLocal;
            } else {
                throw new RuntimeException("execute: Local posting file name class is unrecognized: " + fullFileNameLocal.getClass().getName());
            }

            // Persist the reward records to the database.
            PersistServiceBO.recordPostedRewards(partnerName, local,
                postedFileDate, rewardList, noPartnerId, rewardByCart);

            ActionUtil.logInfo(logger, executionContext,
                rewardList.size() +
                " records have been successfully persisted to database.");

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                ActionUtil.logInfo(logger, executionContext,
                    "All records across all days have been successfully persisted to database.");
            } else {
                // Reloop to the start of the workflow to obtain more records.
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_RELOOP;
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
