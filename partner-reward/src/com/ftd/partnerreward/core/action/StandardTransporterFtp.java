package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.TransportServiceBO;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.File;

import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class StandardTransporterFtp implements ActionHandler {
    protected Logger logger = new Logger(StandardTransporterFtp.class.getName());

    // Used to obtain Secure Configuration Properties
    private static final String SECURE_CONFIG_CONTEXT = "partner_reward";
    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String OUTBOUND_USERNAME = "_OutboundFtpUser";
    private static final String OUTBOUND_PASSWORD = "_OutboundFtpPswd";
    private static final String OUTBOUND_SERVER = "_OUTBOUND_SERVER";

    
    // This constant also appears in com.ftd.reports.core.IWorkflowConstants.java.
    // Conceptual knowledge of the reports package added below as per JP's 
    // advice; in that case, perhaps the IWorkflowConstants files should be merged
    // as well, rather than keeping reports constants separate from the 
    // partnerrewards core constants in com.ftd.partnerreward.core.IWorkflowConstants.java.
    private static final String CONTEXT_KEY_REPORT_NAME = "keyReportName";
    
    // The constants below are specific to florist reports; the fact that florist-report-
    // specific knowledge is in the StandardTransporterFtp class, perhaps calls for 
    // a florist-report-specific TransporterFtp module to replace this one for florist
    // report processing, while Allant reports can still use this standard code because
    // the username follows the same pattern as that of the partners. 
    private static final String FLORIST_PARTNER_NAME = "florist";
    private static final String FLORIST_USERNAME_EXTENSION = "ZIPCODECOUNT";
    


    // These fields will be initialized by the process archive.
    protected String ftpServer;
    protected String ftpDir;
    protected String ftpUsername;
    protected String ftpPassword;
    protected int ftpPort;

    // OPTIONAL transmission type indicator.  True means a binary transfer.
    // Default is false (i.e. ASCII transfer)
    protected boolean isBinary;

    // OPTIONAL indicator to prevent overwriting any existing file on the remote server.
    // Default is false (i.e. no protection; file CAN be overwritten)
    protected boolean isOverwriteProtected;
    
    // OPTIONAL The file name that is overwrite protected, if different from the "put" file.
    // This is needed for platforms such as AS/400 where the file listing name is different
    // from the actual "put" name.
    protected String overwriteProtectedFileName;

    // OPTIONAL Compression type that will be applied to the file prior to sending.
    protected String compressionType;

    // OPTIONAL Zip file name that overrides the standard naming convention
    // of the specified compression type.
    protected String zipFileName;

    public StandardTransporterFtp() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            
            // Obtain Secure username and password
            if(partnerName != null) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String consPartnerName = TextUtil.stripSpecialChars(partnerName);
                ftpUsername = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + OUTBOUND_USERNAME) );
                ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + OUTBOUND_PASSWORD) );  
                ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                    (consPartnerName + OUTBOUND_SERVER) );
            } else {
                String reportName = (String) context.getVariable(CONTEXT_KEY_REPORT_NAME);
                if (reportName != null) {
                    ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    String consPartnerName = TextUtil.stripSpecialChars(partnerName);
                    if (reportName.equalsIgnoreCase(FLORIST_PARTNER_NAME)) {
                        ftpUsername = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                            (consPartnerName + FLORIST_USERNAME_EXTENSION + OUTBOUND_USERNAME) );
                        ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                            (consPartnerName + FLORIST_USERNAME_EXTENSION + OUTBOUND_PASSWORD) );     
                        ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                            (consPartnerName + FLORIST_USERNAME_EXTENSION + OUTBOUND_SERVER) );
                    } else {
                       ftpUsername = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                           (consPartnerName + OUTBOUND_USERNAME) );
                       ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                           (consPartnerName + OUTBOUND_PASSWORD) );     
                        ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                            (consPartnerName + OUTBOUND_SERVER) );
                    }
                }
                else {
                   throw new Exception("partnerName is null.  partnerName is required to obtain the Secure Configuration Properties."); 
                }
            }
            
            Object remoteFileName = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            Object fullFileNameLocal = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            // Transmit the file(s).
            if (fullFileNameLocal instanceof java.util.List) {
                List fullFileNameLocalList = (List) fullFileNameLocal;

                // The remoteFileName should also be a list of the corresponding
                // remote file names.
                List remoteFileNameList = (List) remoteFileName;

                for (int index = 0; index < fullFileNameLocalList.size();
                        index++) {
                    String localPath = (String) fullFileNameLocalList.get(index);
                    String remoteName = (String) remoteFileNameList.get(index);

                    TransportServiceBO.getFtpAdapter().prUpload(localPath,
                        remoteName, ftpDir, ftpServer, ftpUsername,
                        ftpPassword, isBinary, isOverwriteProtected, overwriteProtectedFileName);

                    ActionUtil.logInfo(logger, executionContext,
                        localPath + " successfully transmitted to " +
                        ftpServer + ":" + ftpDir);
                }
            } else if (fullFileNameLocal instanceof String) {
                String local = (String) fullFileNameLocal;
                String remote = (String) remoteFileName;

                if (compressionType != null) {
                    // Perform any specified compression prior to transmission.
                    String fullFileNameLocalZip = PersistServiceBO.compressFile(compressionType,
                            local, remote);

                    // Compose the remote file name.
                    if (zipFileName == null) {
                        zipFileName = fullFileNameLocalZip.substring(fullFileNameLocalZip.lastIndexOf(
                                    File.separator) + 1,
                                fullFileNameLocalZip.length());
                    }

                    // Bind the new remote file name to the context.
                    remote = zipFileName;
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                        remote);
                    
                    local = fullFileNameLocalZip;
                }

                TransportServiceBO.getFtpAdapter().prUpload(local, remote,
                    ftpDir, ftpServer, ftpUsername, ftpPassword, isBinary,
                    isOverwriteProtected, overwriteProtectedFileName);

                ActionUtil.logInfo(logger, executionContext,
                    fullFileNameLocal + " successfully transmitted to " +
                    ftpServer + ": " + ftpDir + File.separator + remote);
            } else {
                throw new RuntimeException("execute: Local posting file name class is unrecognized: " + fullFileNameLocal.getClass().getName());
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
