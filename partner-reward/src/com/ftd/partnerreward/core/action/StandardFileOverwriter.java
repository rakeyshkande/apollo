package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.PersistServiceBO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.Iterator;
import java.util.List;


public class StandardFileOverwriter implements ActionHandler {
    protected Logger logger = new Logger(StandardFileOverwriter.class.getName());
    protected String overwriteValue;
    protected int columnPosition;
    
    // OPTIONAL Count of header lines to preserve from overwrite.
    protected int headerLineCount;
    
    // OPTIONAL Count of trailer lines to preserve from overwrite.
    protected int trailerLineCount;

    public StandardFileOverwriter() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            Object fullFileNameLocal = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            if (fullFileNameLocal instanceof java.util.List) {
                List fullFileNameLocalList = (List) fullFileNameLocal;

                // Iterate through each file, overwriting the contents.
                for (Iterator i = fullFileNameLocalList.iterator();
                        i.hasNext();) {
                    String local = (String) i.next();
                    PersistServiceBO.overwriteFileContents(local,
                        headerLineCount, trailerLineCount, columnPosition, overwriteValue);
                }
            } else {
                String local = (String) fullFileNameLocal;
                PersistServiceBO.overwriteFileContents(local, headerLineCount,
                    trailerLineCount, columnPosition, overwriteValue);
            }

            ActionUtil.logInfo(logger, executionContext,
                "Local file contents have been overwritten with value " +
                overwriteValue);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
