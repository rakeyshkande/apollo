package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.text.TextUtil;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;


public class ActionUtil {
    private ActionUtil() {
    }

    //    public static void advanceToken(ExecutionContext executionContext) {
    //        // Verify if the workflow must now end.
    //        String endIndicator = (String) executionContext.getContextInstance()
    //                                                       .getVariable(IWorkflowConstants.CONTEXT_KEY_END_IND);
    //
    //        if (endIndicator != null) {
    //            // End the workflow.
    //            executionContext.getToken().signal(IWorkflowConstants.CONTEXT_TRANSITION_END);
    //        } else {
    //            // Continue to the next step in the workflow.
    //            executionContext.getToken().signal();
    //        }
    //    }
    private static void logError(Logger logger, ExecutionContext ec, Throwable t) {
        logger.error(composeFullMessage(ec, AlertService.getStackTrace(t)));
    }

    public static void processError(Logger logger, ExecutionContext ec,
        Throwable t) {
        try {
          ActionUtil.logError(logger, ec, t);
          AlertService.processError(t);
        }
        //Many callers to processErrror set the executionContext signal after this method is called
        //therefore, we must make sure that any exceptions are caught beforehand.
        catch(Exception e) {
          logger.error("An exception was caught during the processError() function. " +
              "Proper error reporting did not occur. " + e);        
        }
    }

    public static void logInfo(Logger logger, ExecutionContext ec, String msg,
        Date targetDate) {
        logger.info(composeFullMessage(ec, msg));
    }

    public static void logInfo(Logger logger, ExecutionContext ec, String msg) {
        logger.info(composeFullMessage(ec, msg));
    }
    
    public static void logDebug(Logger logger, ExecutionContext ec, String msg) {
        logger.debug(composeFullMessage(ec, msg));
    }

    protected static String composeFullMessage(ExecutionContext ec, String msg) {
        ContextInstance context = ec.getContextInstance();
        StringBuffer sb = new StringBuffer("ProcessId=");
        sb.append(ec.getProcessInstance().getId());

        sb.append(" : ParterName=");
        sb.append((String) context.getVariable(
                IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME));
        sb.append(" : Node=");
        sb.append(ec.getNode().getName());

        sb.append(" : StartDate=");

        Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
        sb.append((startDate == null) ? "null"
                                      : TextUtil.formatDate(startDate,
                "MM/dd/yyyy HH:mm:ss"));

        //        if (targetDate != null) {
        //            sb.append(" : TargetDate=");
        //            sb.append(TextUtil.formatDate(targetDate, "MM/dd/yyyy HH:mm:ss"));
        //        } else {
        sb.append(" : ActiveDate=");

        Date activeDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE);
        sb.append((activeDate == null) ? "null"
                                       : TextUtil.formatDate(activeDate,
                "MM/dd/yyyy HH:mm:ss"));

        //        }
        sb.append(" : EndDate=");

        Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
        sb.append((endDate == null) ? "null"
                                    : TextUtil.formatDate(endDate,
                "MM/dd/yyyy HH:mm:ss"));

        sb.append(" : TotalRollingRecordCount=");
        sb.append((Integer) context.getVariable(
                IWorkflowConstants.CONTEXT_KEY_TOTAL_ROLLING_RECORD_COUNT));
        sb.append(" : Message=");
        sb.append(msg);

        return sb.toString();
    }

    public static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName + "-" +
        TextUtil.formatDate(postDate, "yyyyMMdd-HHmmss");
    }
    
    /**
     * Returns the last day of the last month in yyMMdd format
     * @return                String with dat in yyMMdd format
     */

    public static String getLastDayOfPreviousMonth() {
        Calendar calendar = Calendar.getInstance();
       // define date format
       SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");     
       // set the month back one month
       calendar.add(Calendar.MONTH, -1);
       // set the day to the last day of the month
       calendar.set( calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
       return sdf.format(calendar.getTime());              

    }
    
    

    public static Date setToZeroHour(Date thisDate) {
        Calendar thisCalendar = Calendar.getInstance();
        thisCalendar.setTimeInMillis(thisDate.getTime());
        thisCalendar.clear(Calendar.MILLISECOND);
        thisCalendar.clear(Calendar.SECOND);
        thisCalendar.clear(Calendar.MINUTE);
        thisCalendar.set(Calendar.HOUR_OF_DAY, 0);

        return thisCalendar.getTime();
    }

    /**
    * Verifies if the membership id is zero-filled.
    * @return
    * @param membershipId
    */
    public static boolean isDummyMembershipId(String membershipId) {
        boolean result = false;

        if (membershipId == null) {
            result = true;
        } else {
            try {
                long number = Long.parseLong(membershipId);

                if (number == 0) {
                    result = true;
                }
            } catch (Throwable e) {
                // do nothing
            }
        }

        return result;
    }
}
