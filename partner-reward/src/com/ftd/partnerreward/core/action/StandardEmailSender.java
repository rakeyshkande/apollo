package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.SecurityServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.reports.dao.OrderDAO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.util.StringTokenizer;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


/**
 * This class is responsible for sending an email notification.
 */
public class StandardEmailSender implements ActionHandler {
    protected Logger logger = new Logger(StandardEmailSender.class.getName());
    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String MESSAGE_GENERATOR_CONFIG = "MESSAGE_GENERATOR_CONFIG";
    private static final String SMTP_HOST_NAME = "SMTP_HOST_NAME";
    
    // These fields will be initialized by the process archive.
    protected String emailGroupKey;
    protected String subject;
    protected String fromAddress;
    protected String content;
    protected String contentEmptyFile;
    protected boolean attachmentFlag;
    protected boolean deleteFileFlag;
    

    public StandardEmailSender() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String toEmailAddressList = null;
        String fullFileNameLocal = null;
        String emptyResultSet = null;
        File sendFile = null;
        
        try {
            logger.info("execute: Beginning sending email to " + emailGroupKey);
            
            ContextInstance context = executionContext.getContextInstance();
            emptyResultSet = (String)context.getVariable(IWorkflowConstants.CONTEXT_KEY_EMPTY_RESULT_SET);

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            toEmailAddressList = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, emailGroupKey);
            
            NotificationVO notifVO = new NotificationVO();
            NotificationUtil mail = NotificationUtil.getInstance();

            notifVO.setMessageTOAddress(toEmailAddressList);
            notifVO.setAddressSeparator(",");
            notifVO.setMessageFromAddress(fromAddress);
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(MESSAGE_GENERATOR_CONFIG, SMTP_HOST_NAME));
            notifVO.setMessageSubject(subject);
            notifVO.setMessageContent(contentEmptyFile);
            
            if(attachmentFlag && !"Y".equals(emptyResultSet)) {
                fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
                sendFile = new File(fullFileNameLocal);
                notifVO.setFileAttachment(new File(fullFileNameLocal));
                notifVO.setMessageContent(content);
                notifVO.setMessageContentHTML(content);
                mail.notifyMultipart(notifVO, deleteFileFlag);
            } else {
                mail.notify(notifVO);
            }

            logger.info("execute: Completed processing of sending email.");           
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
}
