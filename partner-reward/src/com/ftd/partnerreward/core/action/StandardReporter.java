package com.ftd.partnerreward.core.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.bo.RewardServiceBO;

import java.util.List;
import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class StandardReporter implements ActionHandler {
    protected Logger logger = new Logger(StandardReporter.class.getName());
    protected String reportId;
    protected String reportUrl;
    protected String reportFile;
    protected String emailAddresses;

    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String REPORT_SERVER_URL = "REPORT_SERVER_URL";
    private static final String EMAIL_GROUP1 = "EMAIL_GROUP1";
    private static final String EMAIL_GROUP2 = "EMAIL_GROUP2";
    private static final String DEFAULT_EMAIL_GROUP = "weboperations@ftdi.com";

    public StandardReporter() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            Object fullFileNameLocal = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            String local = "Not Applicable";

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            reportUrl = cu.getFrpGlobalParm(CONFIG_CONTEXT, REPORT_SERVER_URL);

            if (emailAddresses != null && emailAddresses.equalsIgnoreCase("emailGroup1")) {
                emailAddresses = cu.getFrpGlobalParm(CONFIG_CONTEXT, EMAIL_GROUP1);
            } else {
                emailAddresses = cu.getFrpGlobalParm(CONFIG_CONTEXT, EMAIL_GROUP2);
            }
            if (emailAddresses == null) {
                emailAddresses = DEFAULT_EMAIL_GROUP;
            }

            if (fullFileNameLocal instanceof java.util.List) {
                List fullFileNameLocalList = (List) fullFileNameLocal;

                // Simply use the first file name record.
                if (fullFileNameLocalList.size() > 0) {
                local = (String) fullFileNameLocalList.get(0);
                }
            } else if (fullFileNameLocal instanceof String) {
                local = (String) fullFileNameLocal;
            } else {
                throw new RuntimeException("execute: Local posting file name class is unrecognized: " + fullFileNameLocal.getClass().getName());
            }

            // Generate the report.
            String submissionId = RewardServiceBO.generateReport(partnerName, reportId,
                    reportUrl, reportFile, emailAddresses, local, 
                    "Apollo Reward File Generation - " + partnerName);

            ActionUtil.logInfo(logger, executionContext,
                "Summary report successfully submitted for submissionId=" +
                submissionId + " reportId=" + reportId + " reportUrl=" +
                reportUrl + " rdfFileName=" + reportFile + " emailAddresses=" +
                emailAddresses + " fullFileNameLocal=" + local);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
