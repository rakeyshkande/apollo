package com.ftd.partnerreward.core.vo;

public final class CompanyCode implements IEnum {
    public static final CompanyCode FTD = new CompanyCode("FTD");
    public static final CompanyCode FLOWERS_USA = new CompanyCode("FUSA");
    public static final CompanyCode BUTTERFIELD_BLOOMS = new CompanyCode("HIGH");
    public static final CompanyCode GIFT_SENSE = new CompanyCode("GIFT");
    public static final CompanyCode SF_MUSIC_BOX = new CompanyCode("SFMB");
    public static final CompanyCode FLORIST_COM = new CompanyCode("FLORIST");
    public static final CompanyCode FLOWERS_DIRECT = new CompanyCode("FDIRECT");
    
    private String type;

    private CompanyCode(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof CompanyCode) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}