package com.ftd.partnerreward.core.vo;

import com.ftd.partnerreward.core.text.IRecordCollection;

import java.io.Serializable;


public class TransformMetadata implements Serializable {
    private IRecordCollection recordCollection;
    private long pointsTotal;

    public TransformMetadata(IRecordCollection recordCollection) {
        this.recordCollection = recordCollection;
    }

    public IRecordCollection getRecordCollection() {
        return recordCollection;
    }

    public long getPointsTotal() {
        return pointsTotal;
    }

    public void setPointsTotal(long pointsTotal) {
        this.pointsTotal = pointsTotal;
    }
}
