package com.ftd.partnerreward.core.vo;

public class RewardCalcParms {
//    private boolean mdseAddOnExcluded;
    private boolean creditCardBased;

    public RewardCalcParms() {
    }

//    public boolean isMdseAddOnExcluded() {
//        return mdseAddOnExcluded;
//    }
//
//    public void setMdseAddOnExcluded(boolean mdseAddOnExcluded) {
//        this.mdseAddOnExcluded = mdseAddOnExcluded;
//    }

    public boolean isCreditCardBased() {
        return creditCardBased;
    }

    public void setCreditCardBased(boolean creditCardBased) {
        this.creditCardBased = creditCardBased;
    }
}
