package com.ftd.partnerreward.core.vo;

/**
 * Typesafe enum.
 */
public final class CalculationBasis implements IEnum {
    public static final CalculationBasis MERCHANDISE_TYPE = new CalculationBasis("M");
    public static final CalculationBasis ORDER_TOTAL_TYPE = new CalculationBasis("T");
    public static final CalculationBasis LINE_ITEM_COUNT_TYPE = new CalculationBasis("L");
    public static final CalculationBasis FIXED_AMOUNT_TYPE = new CalculationBasis("F");
    private String type;

    private CalculationBasis(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof CalculationBasis) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}