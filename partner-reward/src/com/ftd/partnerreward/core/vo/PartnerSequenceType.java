package com.ftd.partnerreward.core.vo;
 
/**
 * Typesafe enum.
 */
public final class PartnerSequenceType implements IEnum {
    public static final PartnerSequenceType FILE = new PartnerSequenceType("file");
    public static final PartnerSequenceType RECORD = new PartnerSequenceType("record");
    private String type;

    private PartnerSequenceType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof PartnerSequenceType) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}