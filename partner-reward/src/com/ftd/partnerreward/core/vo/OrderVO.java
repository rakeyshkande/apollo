package com.ftd.partnerreward.core.vo;

import com.ftd.partnerreward.core.text.TextUtil;

import java.io.Serializable;


public class OrderVO implements Serializable {
    private String masterOrderNumber;
    private String companyId;
    private String emailAddress;
    private String customerFirstName;
    private String customerLastName;

    public OrderVO() {
    }

    public void setCompanyId(String companyId) {
        this.companyId = TextUtil.trim(companyId);
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setMasterOrderNumber(String masterOrderNumber) {
        this.masterOrderNumber = TextUtil.trim(masterOrderNumber);
    }

    public String getMasterOrderNumber() {
        return masterOrderNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = TextUtil.trim(emailAddress);
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }
}
