package com.ftd.partnerreward.core.vo;

import com.ftd.partnerreward.core.text.TextUtil;

import java.io.Serializable;

import java.util.Date;


public class OrderDetailVO implements Serializable {
    private OrderVO orderHeaderVO;
    private String externalOrderNumber;
    private MembershipVO membershipVO;
    private float rewardBase;
    private float rewardMinimum;
    private float rewardBonus;
    private String pstCode;
    private String pstCodeBonus;
    private String partnerBankPstCode;
    private String rewardType;
    private String rewardName;
    private String bonusSeparateData;
    private float partnerBankBalance;
    private String partnerBankId;
    private Date deliveryDate;
    private float rewardTotalLimit;
    private String orderDetailId;
    private String sourceCode;
    private String programName;
    private float billingTotal;
    private String productName;
    private String postFileRecordId;
    private float rewardBaseCalc;
    private float rewardBonusCalc;
    private float rewardMinimumCalc;
    private boolean isRefundReward;
    private float billingMerchandise;
    private float billingMerchandiseOriginal;
    private float billingTotalOriginal;
    private Date orderDate;
    private float rewardPoints;

	public OrderDetailVO() {
    }

    public void setOrderHeaderVO(OrderVO orderHeaderVO) {
        this.orderHeaderVO = orderHeaderVO;
    }

    public OrderVO getOrderHeaderVO() {
        return orderHeaderVO;
    }

    public void setExternalOrderNumber(String externalOrderNumber) {
        this.externalOrderNumber = TextUtil.trim(externalOrderNumber);
    }

    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }

    public float getRewardBase() {
        if ((getRewardMinimum() > 0) && (rewardBase < getRewardMinimum())) {
            return getRewardMinimum();
        }

        return rewardBase;
    }

    public void setRewardBase(float rewardBase) {
        this.rewardBase = rewardBase;
    }

    public float getRewardMinimum() {
        return rewardMinimum;
    }

    public void setRewardMinimum(float rewardMinimum) {
        this.rewardMinimum = rewardMinimum;
    }

    public float getRewardBonus() {
        return rewardBonus;
    }

    public void setRewardBonus(float rewardBonus) {
        this.rewardBonus = rewardBonus;
    }

    public String getPstCode() {
        return pstCode;
    }

    public void setPstCode(String pstCode) {
        this.pstCode = TextUtil.trim(pstCode);
    }

    public String getPstCodeBonus() {
        return pstCodeBonus;
    }

    public void setPstCodeBonus(String pstCodeBonus) {
        this.pstCodeBonus = TextUtil.trim(pstCodeBonus);
    }

    public String getPartnerBankPstCode() {
        return partnerBankPstCode;
    }

    public void setPartnerBankPstCode(String partnerBankPstCode) {
        this.partnerBankPstCode = TextUtil.trim(partnerBankPstCode);
    }

    public MembershipVO getMembershipVO() {
        return membershipVO;
    }

    public void setMembershipVO(MembershipVO membershipVO) {
        this.membershipVO = membershipVO;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = TextUtil.trim(rewardType);
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = TextUtil.trim(rewardName);
    }

    public String getBonusSeparateData() {
        return bonusSeparateData;
    }

    public void setBonusSeparateData(String bonusSeparateData) {
        this.bonusSeparateData = TextUtil.trim(bonusSeparateData);
    }

    public float getPartnerBankBalance() {
        return partnerBankBalance;
    }

    public void setPartnerBankBalance(float partnerBankBalance) {
        this.partnerBankBalance = partnerBankBalance;
    }

    public String getPartnerBankId() {
        return partnerBankId;
    }

    public void setPartnerBankId(String partnerBankId) {
        this.partnerBankId = TextUtil.trim(partnerBankId);
    }

    public float getRewardTotal() {
        float rawTotal = getRewardBase() + getRewardBonus();
        float limit = getRewardTotalLimit();

        if ((limit != 0) && (limit < rawTotal)) {
            rawTotal = limit;
        }

        return rawTotal;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public float getRewardTotalLimit() {
        return rewardTotalLimit;
    }

    public void setRewardTotalLimit(float rewardTotalLimit) {
        this.rewardTotalLimit = rewardTotalLimit;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = TextUtil.trim(orderDetailId);
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = TextUtil.trim(sourceCode);
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = TextUtil.trim(programName);
    }

    public float getBillingTotal() {
        return billingTotal;
    }

    public void setBillingTotal(float billingTotal) {
        this.billingTotal = billingTotal;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPostFileRecordId() {
        return postFileRecordId;
    }

    public void setPostFileRecordId(String postFileRecordId) {
        this.postFileRecordId = postFileRecordId;
    }

    public float getRewardBaseCalc() {
        return rewardBaseCalc;
    }

    public void setRewardBaseCalc(float rewardBaseCalc) {
        this.rewardBaseCalc = rewardBaseCalc;
    }

    public float getRewardBonusCalc() {
        return rewardBonusCalc;
    }

    public void setRewardBonusCalc(float rewardBonusCalc) {
        this.rewardBonusCalc = rewardBonusCalc;
    }

    public float getRewardMinimumCalc() {
        return rewardMinimumCalc;
    }

    public void setRewardMinimumCalc(float rewardMinimumCalc) {
        this.rewardMinimumCalc = rewardMinimumCalc;
    }

    public float getRewardTotalCalc() {
        float rawTotal = getRewardBaseCalc() + getRewardBonusCalc();
        float limit = getRewardTotalLimit();

        if ((limit != 0) && (limit < rawTotal)) {
            rawTotal = limit;
        }

        return rawTotal;
    }

    public boolean isIsRefundReward() {
        return isRefundReward;
    }

    public void setIsRefundReward(boolean isRefundReward) {
        this.isRefundReward = isRefundReward;
    }

    public float getBillingMerchandise() {
        return billingMerchandise;
    }

    public void setBillingMerchandise(float billingMerchandise) {
        this.billingMerchandise = billingMerchandise;
    }

    public float getBillingMerchandiseOriginal() {
        return billingMerchandiseOriginal;
    }

    public void setBillingMerchandiseOriginal(float billingMerchandiseOriginal) {
        this.billingMerchandiseOriginal = billingMerchandiseOriginal;
    }

    public float getBillingTotalOriginal() {
        return billingTotalOriginal;
    }

    public void setBillingTotalOriginal(float billingTotalOriginal) {
        this.billingTotalOriginal = billingTotalOriginal;
    }

  public void setOrderDate(Date orderDate) {
    this.orderDate = orderDate;
  }

  public Date getOrderDate() {
    return orderDate;
  }

  public float getRewardPoints() {
		return rewardPoints;
	}

  public void setRewardPoints(float rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

}
