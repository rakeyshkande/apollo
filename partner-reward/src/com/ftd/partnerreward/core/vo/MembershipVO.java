package com.ftd.partnerreward.core.vo;

import com.ftd.partnerreward.core.text.TextUtil;

import java.io.Serializable;


public class MembershipVO implements Serializable {
    private String membershipNumber;
    private String membershipType;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private String country;

    public MembershipVO() {
    }

    public void setMembershipNumber(String membershipNumber) {
        this.membershipNumber = TextUtil.trim(membershipNumber);
    }

    public String getMembershipNumber() {
        return membershipNumber;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = TextUtil.trim(membershipType);
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setFirstName(String firstName) {
        this.firstName = TextUtil.trim(firstName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = TextUtil.trim(lastName);
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}
