package com.ftd.partnerreward.core.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class GlobalParameterVO extends BaseVO
{
  private String context;
  private String name;
  private String value;
  
  public GlobalParameterVO()
  {
  }
  
  public void setName(String name)
  {
    if(valueChanged(this.name, name))
    {
      setChanged(true);
    }
    this.name = trim(name);
  }

  public String getName()
  {
    return name;
  }
  
  public void setContext(String context)
  {
    if(valueChanged(this.context, context))
    {
      setChanged(true);
    }
    this.context = trim(context);
  }

  public String getContext()
  {
    return context;
  }
  
  public void setValue(String value)
  {
    if(valueChanged(this.value, value))
    {
      setChanged(true);
    }
    this.value = trim(value);
  }

  public String getValue()
  {
    return value;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
 
}