package com.ftd.partnerreward.partner.quixtar.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected static final String _CONTEXT_KEY_FILE_MDSE_SUM = "_keyFileMdseSum";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List<OrderDetailVO> rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Float fileMerchandiseSum = (Float) context.getVariable(_CONTEXT_KEY_FILE_MDSE_SUM);

            float rollingMerchandiseSum = 0;

            if (fileMerchandiseSum != null) {
                rollingMerchandiseSum = fileMerchandiseSum.floatValue();
            }

            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,' ');

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("############0.00");

            Iterator<OrderDetailVO> i = rewardList.iterator();

            while(i.hasNext()){
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO().getMembershipNumber();

                if (membershipId != null && !ActionUtil.isDummyMembershipId(membershipId)) {
                	if(membershipId.startsWith("IQ") || membershipId.startsWith("CQ")){
                		membershipId = TextUtil.stripBeginningChars(membershipId, 2);
                	}
                	
                	// Pads or truncates the membershipId to be exactly 11 characters
	        		membershipId = TextUtil.smartPadAndTruncString(membershipId, 11, '0', true, true);
                	
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<Sale");
                    recordLine.addStringValue("product_transfer_type=\"DS\"");

                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName, PartnerSequenceType.RECORD);
                    recordLine.addStringValue("invoice_number=\"" + partnerRecordId.toString() + "\"");
                    odVO.setPostFileRecordId(partnerRecordId.toString());

                    recordLine.addStringValue("invoice_date=\"" + TextUtil.formatDate(odVO.getDeliveryDate(), "yyyy-MM-dd") + "\"");

                    recordLine.addStringValue("promotion_code=\"FLOR\"");
                    recordLine.addStringValue("ship_to_name=\"" + odVO.getMembershipVO().getFirstName() + " " + odVO.getMembershipVO().getLastName() + "\"");

                    recordLine.addStringValue("distrib_id_number=\"" + membershipId + "\"");

                    float merchandiseBilling = odVO.getBillingMerchandise();
                    recordLine.addStringValue("list_price=\"" + df.format(new Float(merchandiseBilling).doubleValue()) + "\"");

                    // Since Quixtar is not "rewards"-based per se, manually
                    // populate this metric for the later reporting step.
                    odVO.setRewardBase(merchandiseBilling);
                    
                    // Maintain a rolling merchandise sum across all orders.
                    rollingMerchandiseSum += merchandiseBilling;

                    recordLine.addStringValue("/>");

                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,"Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT, recordCollection);

            // Record the sum of merchandise cost.
            context.setVariable(_CONTEXT_KEY_FILE_MDSE_SUM, new Float(rollingMerchandiseSum));

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
