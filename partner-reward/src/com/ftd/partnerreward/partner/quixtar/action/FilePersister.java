package com.ftd.partnerreward.partner.quixtar.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.dao.OrderDAO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import com.ftd.partnerreward.core.vo.GlobalParameterVO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class FilePersister implements ActionHandler {
    private static final String _CONTEXT_KEY_FILE_RECORD_COUNT = "_keyFileRecordCount";
    private static final String _CONTEXT_RECORD_COUNT_LIST = "_recordCountList";
    private static final String _CONTEXT_FILE_TOTAL_LIST = "_fileTotalList";
    protected Logger logger = new Logger(FilePersister.class.getName());
    private static final String _DB_QUIXTAR_MAX_FILE_SIZE = "QUIXTAR_MAX_FILE_SIZE";
    private static final int ARRAY_SIZE = 999;

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    private static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName +
        TextUtil.formatDate(postDate, "MMddyyyy_HHmmss");
    }

    private static String composePostFileNameRemote(String fileName,
        Date postDate) {
        return fileName + TextUtil.formatDate(postDate, "MMddyyyy_HHmmss");
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            int MAX_FILE_SIZE = 1000;
            GlobalParameterVO param = OrderDAO.getGlobalParameter(IWorkflowConstants.GLOBAL_PARMS_CONTEXT, 
                    _DB_QUIXTAR_MAX_FILE_SIZE);
            if (param != null && param.getValue() != null) {
                MAX_FILE_SIZE = Integer.parseInt(param.getValue());
            }
            logger.debug("MAX_FILE_SIZE: " + MAX_FILE_SIZE);

            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            List fullFileNameLocalList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            int recordCountList[] = new int[ARRAY_SIZE];
            BigDecimal fileTotalList[] = new BigDecimal[ARRAY_SIZE];
            BigDecimal fileTotalTemp[] = (BigDecimal[]) context.getVariable(_CONTEXT_FILE_TOTAL_LIST);
            int recordCountTemp[] = (int[]) context.getVariable(_CONTEXT_RECORD_COUNT_LIST);
            if (recordCountTemp != null) {
                for (int index=0; index < ARRAY_SIZE; index++) {
                    recordCountList[index] = recordCountTemp[index];
                    fileTotalList[index] = fileTotalTemp[index];
                }
            }

            Integer fileRecordCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_RECORD_COUNT);

            // Initialize the file record count.
            int rollingFileRecordCount = 0;

            if (fileRecordCount != null) {
                rollingFileRecordCount = fileRecordCount.intValue();
            }

            // Initialize the local destination file name.
            if (fullFileNameLocalList == null) {
                fullFileNameLocalList = new ArrayList();

                Date fileDate = new Date();
                fullFileNameLocalList.add(FilePersister.composePostFilePathLocal(
                        filePath, fileName, fileDate));
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocalList);
                recordCountList[0] = 0;
                fileTotalList[0] = new BigDecimal("0");
                context.setVariable(_CONTEXT_RECORD_COUNT_LIST, recordCountList);
                context.setVariable(_CONTEXT_FILE_TOTAL_LIST, fileTotalList);
            }

            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {
                // Keep track of the number of records being processed.
                // For every thousand, we need to roll to another
                // physical file.
                String fullFileNameLocal = (String) fullFileNameLocalList.get(fullFileNameLocalList.size() - 1);

                for (Iterator i = transformResult.iterator(); i.hasNext();) {
                    IRecordLine recordLine = (IRecordLine) i.next();
                    IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                            ' ');
                    recordCollection.addRecordLine(recordLine);

                    PersistServiceBO.writeFileBody(fullFileNameLocal,
                        recordCollection, true);

                    rollingFileRecordCount++;

                    Document doc = DOMUtil.getDocument(recordLine.toString());
                    NodeList nl = DOMUtil.selectNodes(doc,"Sale");
                    if (nl.getLength() > 0) {
                        Element temp = (Element) nl.item(0);
                        int listIndex = fullFileNameLocalList.indexOf(fullFileNameLocal);
                        recordCountList[listIndex] += 1;
                        BigDecimal price = new BigDecimal(temp.getAttribute("list_price"));
                        fileTotalList[listIndex] = fileTotalList[listIndex].add(price).setScale(2, BigDecimal.ROUND_HALF_UP);
                        context.setVariable(_CONTEXT_RECORD_COUNT_LIST, recordCountList);
                        context.setVariable(_CONTEXT_FILE_TOTAL_LIST, fileTotalList);
                    }

                    if ((rollingFileRecordCount >= MAX_FILE_SIZE) &&
                            ((rollingFileRecordCount % MAX_FILE_SIZE) == 0)) {
                        // Wait some seconds to increment the file timestamp suffix,
                        // so that two files are not named the same.
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                            logger.error("Thread sleep error");
                        }

                        Date fileDate = new Date();

                        // Roll the record to a new file name.
                        fullFileNameLocal = FilePersister.composePostFilePathLocal(filePath,
                                fileName, fileDate);
                        fullFileNameLocalList.add(fullFileNameLocal);
                        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                            fullFileNameLocalList);

                        int listIndex = fullFileNameLocalList.indexOf(fullFileNameLocal);
                        recordCountList[listIndex] = 0;
                        fileTotalList[listIndex] = new BigDecimal("0");
                        context.setVariable(_CONTEXT_RECORD_COUNT_LIST, recordCountList);
                        context.setVariable(_CONTEXT_FILE_TOTAL_LIST, fileTotalList);
                    }
                }

                // Record the number of records that have been placed in
                // the file.
                context.setVariable(_CONTEXT_KEY_FILE_RECORD_COUNT,
                    new Integer(rollingFileRecordCount));

                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {

                List fileNameRemoteList = new ArrayList(fullFileNameLocalList.size());

                // Compose the header and trailer for all files.
                for (int index = 0; index < fullFileNameLocalList.size(); index++) {
                    String fullFileNameLocal = (String) fullFileNameLocalList.get(index);

                    int listIndex = fullFileNameLocalList.indexOf(fullFileNameLocal);
                    int thisFileRecordCount = recordCountList[listIndex];
                    Float fileMerchandiseSum = fileTotalList[listIndex].floatValue();
                    logger.debug("listIndex[" + listIndex + "] retrieved: " + 
                        thisFileRecordCount + " " + fileMerchandiseSum);

                    PersistServiceBO.writeFileHeader(fullFileNameLocal,
                        FilePersister.composeHeader(fullFileNameLocal, 
                            thisFileRecordCount, fileMerchandiseSum.floatValue()));

                    PersistServiceBO.writeFileTrailer(fullFileNameLocal,
                        FilePersister.composeTrailer());

                    // Record the file input list as the remote file name list.
                    // Iterate through the local list to remove the path for the remote names.
                    fileNameRemoteList.add(fullFileNameLocal.substring(fullFileNameLocal.lastIndexOf(
                                File.separator) + 1, fullFileNameLocal.length()));
                }

                // Record the remote file name list.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    fileNameRemoteList);

                ActionUtil.logInfo(logger, executionContext, extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted.");
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    private static IRecordCollection composeTrailer() {
        IRecordCollection trailerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                ' ');
        IRecordLine trailerLine = trailerCollection.createLineInstance();
        trailerLine.addStringValue("</Sale_Data>");
        trailerCollection.addRecordLine(trailerLine);

        return trailerCollection;
    }

    private static Date parseDateFromFileName(String fullFileNameLocal) {
        // Each Quixtar posting file has a datetime suffix.
        String fileNameLocal = fullFileNameLocal.substring(fullFileNameLocal.lastIndexOf(
                    File.separator) + 1, fullFileNameLocal.length());

        try {
            return TextUtil.parseString(fileNameLocal.substring(12,
                    fileNameLocal.length()), "MMddyyyy_HHmmss");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static IRecordCollection composeHeader(String fullFileNameLocal,
        int rollingFileRecordCount, float rollingFileMdseSum) {

        IRecordCollection headerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                ' ');
        IRecordLine preHeaderLine = headerCollection.createLineInstance();
        preHeaderLine.addStringValue("<?xml version=\"1.0\"?>");
        headerCollection.addRecordLine(preHeaderLine);

        IRecordLine headerLine = headerCollection.createLineInstance();
        headerLine.addStringValue("<Sale_Data");
        headerLine.addStringValue("merchant_id=\"GFT100000001240\"");
        headerLine.addStringValue("receiver_id=\"00602679313\"");

        Date fileTimestamp = parseDateFromFileName(fullFileNameLocal);
        headerLine.addStringValue("xg_batch_id=\"" +
            TextUtil.formatDate(fileTimestamp, "MMddyyyyHHmmss") + "\"");

        headerLine.addStringValue("number_qty_elements=\"" +
            rollingFileRecordCount + "\"");

        headerLine.addStringValue("file_creation_date=\"" +
            TextUtil.formatDate(fileTimestamp, "yyyy-MM-dd") + "\"");
        headerLine.addStringValue("currency=\"USD\"");

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("#######0.00");
        headerLine.addStringValue("sum_list_price=\"" +
            df.format(new Float(rollingFileMdseSum).doubleValue()) + "\"");

        headerLine.addStringValue("transtype=\"SSD\"");
        headerLine.addStringValue(">");
        headerCollection.addRecordLine(headerLine);

        return headerCollection;
    }

}
