package com.ftd.partnerreward.partner.alaskaairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;

import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import com.ftd.partnerreward.core.text.TextUtil;

import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {
    protected Logger logger = new Logger(FilePersister.class.getName());
    private static final String _CONTEXT_KEY_FILE_RECORD_COUNT = "_keyFileRecordCount";
    private static final String _CONTEXT_KEY_FILE_REWARD_SUM = "_keyFileRewardSum";

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            Integer fileRecordCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_RECORD_COUNT);
            // Initialize the file record count.
            int rollingFileRecordCount = 0;

            if (fileRecordCount != null) {
                rollingFileRecordCount = fileRecordCount.intValue();
            }

            Integer fileRewardCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_REWARD_SUM);
            // Initialize the file record count.
            int rollingFileRewardCount = 0;
            if (fileRewardCount != null) {
                rollingFileRewardCount = fileRewardCount.intValue();
            }

            // Initialize the remote file name.
            if (remoteFileName == null) {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    fileName);
            }

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                        fileName, postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            // Record the number of records that have been placed in
            // the file.
            rollingFileRecordCount += transformResult.size();
            context.setVariable(_CONTEXT_KEY_FILE_RECORD_COUNT,
                new Integer(rollingFileRecordCount));

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) && extractCompleteInd.booleanValue()) {
                // Compose the trailer.
                IRecordLine trailerLine = transformResult.createLineInstance();
                trailerLine.addStringValue("9", 1);
                trailerLine.addNumberValue(String.valueOf(rollingFileRecordCount), 8);
                trailerLine.addNumberValue(String.valueOf(rollingFileRewardCount), 15);
                trailerLine.addStringValue(" ", 141);

                transformResult.addRecordLine(trailerLine);
            }

            // Write the reward records to the destination file.
            // Note: If no records are in transformResult, an empty file is produced
            PersistServiceBO.writeFileBody(fullFileNameLocal,
                transformResult, false);

            ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records have been successfully persisted to file " +
                fullFileNameLocal);

            if ((extractCompleteInd != null) && extractCompleteInd.booleanValue()) {

                String fileDateString = TextUtil.formatDate(postedFileDate,
                        "MM/dd/yyyy");

                // Compose the header.
                IRecordCollection headerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine headerLine = headerCollection.createLineInstance();
                headerLine.addStringValue("1", 1);
                headerLine.addStringValue(fileDateString, 10);
                headerLine.addStringValue("FT", 2);
                headerLine.addStringValue("AS", 2);
                headerLine.addNumberValue(RewardServiceBO.getPartnerSequenceNextVal(
                        partnerName, PartnerSequenceType.FILE).toString(), 13);
                headerLine.addStringValue(" ", 122);
                headerCollection.addRecordLine(headerLine);
                PersistServiceBO.writeFileHeader(fullFileNameLocal,
                    headerCollection, false);

                ActionUtil.logInfo(logger, executionContext,
                    extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to file " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
