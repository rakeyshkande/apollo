package com.ftd.partnerreward.partner.alaskaairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());
    private static final String _CONTEXT_KEY_FILE_REWARD_SUM = "_keyFileRewardSum";

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

            Integer fileRewardSum = (Integer)(context.getVariable(_CONTEXT_KEY_FILE_REWARD_SUM));
            int rollingRewardSum = 0;
            if (fileRewardSum != null) {
                rollingRewardSum = fileRewardSum;
            }

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
                    df.applyPattern("####0");
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();
                
                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {

                IRecordLine recordLine = recordCollection.createLineInstance();
                recordLine.addStringValue("5", 1);
                recordLine.addNumberValue(membershipId, 12);
                recordLine.addStringValue(odVO.getMembershipVO().getLastName(), 20);
                recordLine.addStringValue(odVO.getMembershipVO().getFirstName(), 20);
                recordLine.addStringValue("A", 10);
                recordLine.addStringValue("FT", 2);

                recordLine.addStringValue(TextUtil.formatDate(
                        odVO.getDeliveryDate(), "MM/dd/yyyy"), 10);
                recordLine.addStringValue("FTFTP", 6);

                // Filler
                recordLine.addStringValue(" ", 6);

                // Filler
                recordLine.addStringValue(" ", 25);

                // Round the total value to an integer.
                int rewardTotal = new Float(odVO.getRewardTotal()).intValue();
                recordLine.addNumberValue(df.format(rewardTotal), 9);
                rollingRewardSum += rewardTotal;

                // Filler
                recordLine.addStringValue(" ", 29);
                
                recordCollection.addRecordLine(recordLine);
                
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
            
            // Record the total rewards
            context.setVariable(_CONTEXT_KEY_FILE_REWARD_SUM, rollingRewardSum);
            
            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
