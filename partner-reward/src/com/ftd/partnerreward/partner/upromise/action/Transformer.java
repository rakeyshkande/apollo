package com.ftd.partnerreward.partner.upromise.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.bo.SecurityServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                    '|');

            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();

            int rejectedRecordCount = 0;

            // For each calculation result, transform into the recordCollection.
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();

                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the candidate from the list.
                    i.remove();
                    rejectedRecordCount++;

                    // re-loop
                    continue;
                }

                IRecordLine recordLine = recordCollection.createLineInstance();
                recordLine.addStringValue("AB");

                // encrypt the credit card number.
                recordLine.addStringValue(SecurityServiceBO.encryptSHA1(
                        membershipId));
                recordLine.addStringValue(TextUtil.formatDate(
                        odVO.getDeliveryDate(), "yyyyMMddHHmmss"));

                // Format the reward amount.
                df.applyPattern("####0.00");
                recordLine.addStringValue(df.format(
                        new Float(odVO.getRewardTotal()).doubleValue()));

                // Empty field.
                recordLine.addStringValue("");

                // Unique shared partner order identifier.
                Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                        PartnerSequenceType.RECORD);
                recordLine.addStringValue(partnerRecordId.toString());
                odVO.setPostFileRecordId(partnerRecordId.toString());

                // Empty field.
                recordLine.addStringValue("");

                recordLine.addStringValue(odVO.getSourceCode());
                recordLine.addStringValue("cdHash");


                // Company codes.
                String company = odVO.getOrderHeaderVO().getCompanyId();

                // Translate only certain codes.
                if (company.equals("HIGH")) {
                    company = "BB";
                } else if (company.equals("GIFT")) {
                    company = "GS";
                } else if (company.equals("FUSA")) {
                    // do nothing
                } else if (company.equals("SFMB")) {
                    // do nothing
                } else {
                    company = "FTD";
                }

                recordLine.addStringValue(company);

                df.applyPattern("######0.00");
                recordLine.addStringValue(df.format(
                        new Float(odVO.getBillingTotal()).doubleValue()));

                recordLine.addStringValue("1");


                recordLine.addStringValue(odVO.getProductName());

                recordCollection.addRecordLine(recordLine);
            }

            // Bind the transformed data sets to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);

            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}

