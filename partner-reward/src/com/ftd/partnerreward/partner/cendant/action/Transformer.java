package com.ftd.partnerreward.partner.cendant.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected static final String _CONTEXT_KEY_FILE_DATE_RANGE_MIN = 
        "_keyFileDateRangeMin";
    protected static final String _CONTEXT_KEY_FILE_DATE_RANGE_MAX = 
        "_keyFileDateRangeMax";
    protected Logger logger = new Logger(Transformer.class.getName());
    protected static final String _RECORD_LINE_REWARD_TYPE_BASE = "0";
    protected static final String _RECORD_LINE_REWARD_TYPE_BONUS = "1";
    private static final DecimalFormat _DF = 
        (DecimalFormat)DecimalFormat.getInstance();
    private static final DecimalFormat _IF = 
        (DecimalFormat)DecimalFormat.getInstance();
    private static final String _PARTNER_ML_CODE_BASE = "ML250";
    private static final String _PARTNER_ML_CODE_BONUS = "ML251";

    static {
        _DF.applyPattern("#######0.00");
        _IF.applyPattern("#####0");
    }

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = 
                (List)context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = 
                (String)context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Date fileDateRangeMin = 
                (Date)context.getVariable(_CONTEXT_KEY_FILE_DATE_RANGE_MIN);
            Date fileDateRangeMax = 
                (Date)context.getVariable(_CONTEXT_KEY_FILE_DATE_RANGE_MAX);

            if (fileDateRangeMin == null) {
                fileDateRangeMin = new Date();
            }

            if (fileDateRangeMax == null) {
                fileDateRangeMax = new Date(0);
            }

            IRecordCollection recordCollection = 
                RecordCollectionFactory.getDelimitedRecordCollection(true, 
                                                                     '~');

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;

            for (Iterator i = rewardList.iterator(); i.hasNext(); ) {
                OrderDetailVO odVO = (OrderDetailVO)i.next();
                String membershipId = 
                    odVO.getMembershipVO().getMembershipNumber();

                if ((membershipId != null) && 
                    !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = 
                        recordCollection.createLineInstance();

                    // Defect 2241: Partner requested that base and
                    // bonus be separate records.
                    // Process the base record.
                    composeRecordLine(recordLine, odVO, partnerName, 
                                      _PARTNER_ML_CODE_BASE, 
                                      odVO.getBillingMerchandiseOriginal());

                    recordCollection.addRecordLine(recordLine);

                    // Process the bonus record.
                    if (odVO.getRewardBonus() > 0) {

                        IRecordLine recordLineBonus = 
                            recordCollection.createLineInstance();
                        composeRecordLine(recordLineBonus, odVO, partnerName, 
                                          _PARTNER_ML_CODE_BONUS, 
                                          odVO.getRewardBonus());

                        recordCollection.addRecordLine(recordLineBonus);
                    }

                    Date deliveryDate = odVO.getDeliveryDate();
                    // Compare the delivery dates to determine the minimum date
                    // within the posting file.
                    if (deliveryDate.before(fileDateRangeMin)) {
                        fileDateRangeMin.setTime(deliveryDate.getTime());
                    }

                    // Compare the delivery dates to determine the maximum date
                    // within the posting file.
                    if (deliveryDate.after(fileDateRangeMax)) {
                        fileDateRangeMax.setTime(deliveryDate.getTime());
                    }

                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext, 
                                       "Following order detail id was rejected in transformation due to invalid membership id: " + 
                                       odVO.getOrderDetailId(), 
                                       odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT, 
                                recordCollection);

            context.setVariable(_CONTEXT_KEY_FILE_DATE_RANGE_MIN, 
                                fileDateRangeMin);

            context.setVariable(_CONTEXT_KEY_FILE_DATE_RANGE_MAX, 
                                fileDateRangeMax);

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, 
                                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    private static void composeRecordLine(IRecordLine recordLine, 
                                          OrderDetailVO odVO, 
                                          String partnerName, 
                                          String partnerMlValue, 
                                          float reward) {
        recordLine.addStringValue("2");
        recordLine.addStringValue(odVO.getMembershipVO().getMembershipNumber());
        recordLine.addStringValue(odVO.getMembershipVO().getFirstName() + " " + 
                                  odVO.getMembershipVO().getLastName());

        String companyId = odVO.getOrderHeaderVO().getCompanyId();
        String pstCode = odVO.getPstCode();

        if (pstCode == null) {
            // Each company has a different default.
            if (companyId.equalsIgnoreCase(CompanyCode.FLORIST_COM.toString())) {
                pstCode = "FLORS";
            } else if (companyId.equalsIgnoreCase(CompanyCode.BUTTERFIELD_BLOOMS.toString())) {
                pstCode = "BB";
            } else if (companyId.equalsIgnoreCase(CompanyCode.GIFT_SENSE.toString())) {
                pstCode = "GIFT";
            } else if (companyId.equalsIgnoreCase(CompanyCode.FLOWERS_DIRECT.toString())) {
                pstCode = "FLDIR";
            } else {
                pstCode = "FTD";
            }
        }

        recordLine.addStringValue(pstCode);
        recordLine.addStringValue("");

        Date deliveryDate = odVO.getDeliveryDate();
        recordLine.addStringValue(TextUtil.formatDate(deliveryDate, 
                                                      "yyyyMMdd"));
        recordLine.addStringValue(TextUtil.formatDate(deliveryDate, 
                                                      "yyyyMMdd"));

        recordLine.addStringValue("");

        // Unique shared partner order identifier.
        Long partnerRecordId = 
            RewardServiceBO.getPartnerSequenceNextVal(partnerName, 
                                                      PartnerSequenceType.RECORD);
        recordLine.addStringValue(partnerRecordId.toString());
        odVO.setPostFileRecordId(partnerRecordId.toString());

        recordLine.addStringValue("");
        recordLine.addStringValue("");

        // Bonus points.
//        if (partnerMlValue.equals(_PARTNER_ML_CODE_BASE)) {
            recordLine.addStringValue("");
//        } else {
//            recordLine.addStringValue(_IF.format(new Float(reward).doubleValue()));
//        }

        recordLine.addStringValue("FTD");

        recordLine.addStringValue(odVO.getMembershipVO().getAddress1());
        recordLine.addStringValue(partnerMlValue);

//        if (partnerMlValue.equals(_PARTNER_ML_CODE_BASE)) {
            recordLine.addStringValue(_DF.format(new Float(reward).doubleValue()));
//        } else {
//            recordLine.addStringValue("");
//        }
        recordLine.addStringValue("FTD.COM Rewards Points");
    }
}
