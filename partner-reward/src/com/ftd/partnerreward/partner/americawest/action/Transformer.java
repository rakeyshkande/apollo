package com.ftd.partnerreward.partner.americawest.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("######0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("2", 1);
                    recordLine.addStringValue("FT", 2);

                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                            PartnerSequenceType.RECORD);
                    recordLine.addStringValue(partnerRecordId.toString(), 20);
                    odVO.setPostFileRecordId(partnerRecordId.toString());

                    recordLine.addNumberValue(membershipId, 11);
                    recordLine.addStringValue(" ", 8);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "MMddyyyy"), 8);

                    recordLine.addStringValue("0000", 4);

                    // Translate the company code.
                    String baseLevel = "01";
                    String companyId = odVO.getOrderHeaderVO().getCompanyId();

                    if (companyId.equalsIgnoreCase(
                                CompanyCode.BUTTERFIELD_BLOOMS.toString())) {
                        baseLevel = "02";
                    } else if (companyId.equalsIgnoreCase(
                                CompanyCode.GIFT_SENSE.toString())) {
                        baseLevel = "03";
                    } else if (companyId.equalsIgnoreCase(
                                CompanyCode.SF_MUSIC_BOX.toString())) {
                        baseLevel = "04";
                    }

                    recordLine.addStringValue(baseLevel, 2);

                    // Compose member name.
                    String lastName = odVO.getMembershipVO().getLastName();
                    String firstName = odVO.getMembershipVO().getFirstName();

                    if ((lastName == null) || (lastName.length() < 1)) {
                        // Get the customer name.
                        lastName = odVO.getOrderHeaderVO().getCustomerLastName();
                        firstName = odVO.getOrderHeaderVO()
                                        .getCustomerFirstName();
                    }

                    recordLine.addStringValue(lastName, 18);
                    recordLine.addStringValue(firstName, 14);

                    // Show the total value as an integer.
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getRewardTotal()).doubleValue()), 7);
                    recordLine.addStringValue(" ", 1);
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue(" ", 2);

                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
