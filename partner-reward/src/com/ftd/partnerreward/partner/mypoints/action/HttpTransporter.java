package com.ftd.partnerreward.partner.mypoints.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.BufferedReader;
import java.io.File;

import java.io.InputStream;

import java.io.InputStreamReader;

import java.util.List;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class HttpTransporter implements ActionHandler {
    protected Logger logger = new Logger(this.getClass().getName());

    // Used to obtain Secure Configuration Properties
    private static final String SECURE_CONFIG_CONTEXT = "partner_reward";
    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    
    //From Secure Config
    private static final String OUTBOUND_USERNAME = "_OutboundHttpUser";
    private static final String OUTBOUND_PASSWORD = "_OutboundHttpPswd";
    
    //From Global Parms
    private static final String OUTBOUND_SERVER = "_OUTBOUND_SERVER";
    private static final String OUTBOUND_SERVER_LOGIN_URL = "_OUTBOUND_SERVER_LOGIN_URL";
    private static final String OUTBOUND_SERVER_LOGIN_VALIDATION_URL = "_OUTBOUND_SERVER_LOGIN_VALIDATION_URL";    
    private static final String OUTBOUND_SERVER_UPLOAD_URL = "_OUTBOUND_SERVER_UPLOAD_URL";
  
    HttpClient httpclient = new HttpClient();

    protected String loginUsernameParameter; 
    protected String loginPasswordParameter;
    protected String loginActionParameter;
    protected String loginActionValue;
    
    protected String uploadFileParameter;
    protected String uploadActionParameter;
    protected String uploadActionValue;
    protected String uploadHeaderRedirectUrlParameter;

    public HttpTransporter() {

    }

    public void execute(ExecutionContext executionContext) 
    {
        
        String tokenSignal = null;
        
        String httpUserName;
        String httpUserPassword;
        
        String httpOutboundServer;
        String httpOutboundServerLoginUrl;
        String httpOutboundServerLoginValidationUrl;
        String httpOutboundServeruploadUrl;

        String emptyResultSet = (String) executionContext.getVariable(IWorkflowConstants.CONTEXT_KEY_EMPTY_RESULT_SET);
        
        if (emptyResultSet == null)
        {
            try
            {      
                
                ContextInstance context = executionContext.getContextInstance();
                String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
                
                if(partnerName != null) 
                {
                    ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    
                    
                    String consPartnerName = TextUtil.stripSpecialChars(partnerName);
                    httpUserName = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_USERNAME) );
                    httpUserPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_PASSWORD) );  
                    
                    
                    httpOutboundServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_SERVER) );
                    httpOutboundServerLoginUrl = httpOutboundServer + cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_SERVER_LOGIN_URL) );
                    httpOutboundServerLoginValidationUrl = httpOutboundServer + cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_SERVER_LOGIN_VALIDATION_URL) );        
                    httpOutboundServeruploadUrl = httpOutboundServer + cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                        (consPartnerName + OUTBOUND_SERVER_UPLOAD_URL) );
                    
                    ActionUtil.logInfo(logger,executionContext,"HTTP Server is " + httpOutboundServer);
                    ActionUtil.logInfo(logger,executionContext,"HTTP Login URL is " + httpOutboundServerLoginUrl);  
                    ActionUtil.logInfo(logger,executionContext,"HTTP Login verification URL is " + httpOutboundServerLoginValidationUrl);                    
                    ActionUtil.logInfo(logger,executionContext,"HTTP upload URL is " + httpOutboundServeruploadUrl);                    

                }
                
                else 
                {
                   throw new Exception("partnerName is null.  partnerName is required to obtain the Secure Configuration Properties."); 
                }
                
                
                this.performHTTPLogin(executionContext, httpOutboundServerLoginUrl, httpOutboundServerLoginValidationUrl, httpUserName, httpUserPassword);
    
                
    
                Object fullFileNameLocal = context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
                
                // Transmit the file(s).
                if (fullFileNameLocal instanceof java.util.List) 
                {
                    List fullFileNameLocalList = (List) fullFileNameLocal;
    
                    for (int index = 0; index < fullFileNameLocalList.size();
                            index++) 
                    {
                        String localPath = (String) fullFileNameLocalList.get(index);
                        this.performHTTPUpload(executionContext, httpOutboundServer, httpOutboundServeruploadUrl, localPath);
    
                        ActionUtil.logInfo(logger, executionContext,
                            localPath + " successfully transmitted to " +
                            httpOutboundServer);
                    }
                } 
                else if (fullFileNameLocal instanceof String) 
                {
                    String local = (String) fullFileNameLocal;
    
                    this.performHTTPUpload(executionContext, httpOutboundServer, httpOutboundServeruploadUrl, local);
    
    
                    ActionUtil.logInfo(logger, executionContext,
                        fullFileNameLocal + " successfully transmitted to " +
                        httpOutboundServer);
                } 
                else 
                {
                    throw new RuntimeException("execute: Local posting file name class is unrecognized: " + fullFileNameLocal.getClass().getName());
                }
    
                // Advance the process token.
            }
            catch (Throwable t) 
            {
                ActionUtil.processError(logger, executionContext, t);
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
            }
        }
        else
        {
            ActionUtil.logInfo(logger,executionContext,"No data recieved to upload.");         
        }
        if (tokenSignal != null) 
        {
            executionContext.getToken().signal(tokenSignal);
        } 
        else 
        {
            executionContext.getToken().signal();
        }
    }
    
    private void performHTTPLogin(ExecutionContext executionContext, String httpOutboundServerLoginUrl, 
                                 String httpOutboundServerLoginValidationUrl, String loginUsername, String loginPasswordValue) throws Exception
    {       
        
        NameValuePair userName = new NameValuePair(loginUsernameParameter, loginUsername);
        NameValuePair password = new NameValuePair(loginPasswordParameter, loginPasswordValue);
        NameValuePair action = new NameValuePair(loginActionParameter, loginActionValue);
        
        PostMethod authpost = new PostMethod(httpOutboundServerLoginUrl);
                
        authpost.setRequestBody(new NameValuePair[] {userName,password,action});
        
        try 
        { 
            httpclient.executeMethod(authpost);
            ActionUtil.logInfo(logger, executionContext, "Login form post status: " + authpost.getStatusLine().toString());
            if (logger.isDebugEnabled())
            {
                Cookie[] cookies = httpclient.getState().getCookies();
                for (Cookie cookie : cookies) 
                {
                    ActionUtil.logDebug(logger, executionContext,
                     "Cookie: " + cookie.getName() +
                     ", Value: " + cookie.getValue() +
                     ", IsPersistent?: " + cookie.isPersistent() +
                     ", Expiry Date: " + cookie.getExpiryDate() +
                     ", Comment: " + cookie.getComment());
                }
            }
        } 
        catch (Exception e)
        {
            throw (e);
        }
        
        finally 
        {
            authpost.releaseConnection();
        }
         
        
        GetMethod loginCheck = new GetMethod(httpOutboundServerLoginValidationUrl); 
        try 
        { 
            httpclient.executeMethod(loginCheck);
            logger.debug("Login check connection status: " + loginCheck.getStatusLine());
            String response = loginCheck.getResponseBodyAsString();
            if (!response.contains(loginUsername))
            {
                throw (new Exception ("Unable to validate successful login"));
            }               
        } 
        catch (Exception e)
        {
            throw (e);
        }
        finally 
        {
          loginCheck.releaseConnection();
        }   
    }
    
    private void performHTTPUpload(ExecutionContext executionContext, String  httpOutboundServer, 
                                  String httpOutboundServeruploadUrl, String uploadFileName) throws Exception
    {
        
        File uploadFile = new File(uploadFileName);
        NameValuePair redirectUrl;
        
        ActionUtil.logInfo(logger, executionContext,"MyPoints Upload File Name: " + uploadFile.getName());
        ActionUtil.logInfo(logger, executionContext,"MyPoints Upload File Path: " + uploadFile.getAbsolutePath());
        
        
        PostMethod uploadpost = new PostMethod(httpOutboundServeruploadUrl);
        
        Part uploadFilepart = new FilePart(uploadFileParameter, uploadFile.getName(), uploadFile);         
        Part uploadActionPart = new StringPart(uploadActionParameter, uploadActionValue);
        uploadpost.setRequestEntity(new MultipartRequestEntity(new Part [] {uploadFilepart, uploadActionPart}, uploadpost.getParams()));
        
        try 
        { 
        
            httpclient.executeMethod(uploadpost);
            ActionUtil.logInfo(logger, executionContext, "Upload form post status: " + uploadpost.getStatusLine().toString());
        
            redirectUrl = uploadpost.getResponseHeader(uploadHeaderRedirectUrlParameter);
            //If the url is wrong or the value doesn't exist, the next line will throw a Null Pointer Exception
            ActionUtil.logDebug(logger, executionContext,"Upload redirectUrl :" + httpOutboundServer + redirectUrl.getValue());
        
        } 
        catch (Exception e)
        {
            throw (e);
        }
        finally
        {
            uploadpost.releaseConnection();
        }
        
        GetMethod uploadCheck = new GetMethod(httpOutboundServer + redirectUrl.getValue()); 
        
        try 
        { 
            httpclient.executeMethod(uploadCheck);
            ActionUtil.logInfo(logger, executionContext,"Upload Connection status :" + uploadCheck.getStatusLine());
        
            InputStream response = uploadCheck.getResponseBodyAsStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(response));
            StringBuilder responseStringBuilder = new StringBuilder();
             
            String responseLine = null;
            while ((responseLine = responseReader.readLine()) != null)
            {
                responseStringBuilder.append(responseLine + "\n");
            }
            response.close();
    
            String responseString = responseStringBuilder.toString();
            
            if (responseString.contains("Duplicate file detected"))
            {
                throw (new Exception ("A file with the same name has already been uploaded.  If this file has new data" +
                    "try to upload it again in 30 minutes."));
            }

            if (!responseString.contains("File Uploaded"))
            {
                throw (new Exception ("Unable to validate successful file upload"));                
            }                
        } 
        catch (Exception e)
        {
            throw (e);
        }
        finally 
        {
            uploadCheck.releaseConnection();
        } 
    }
}
