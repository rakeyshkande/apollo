package com.ftd.partnerreward.partner.midwestairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    private static final String _DEFAULT_PST_CODE_BASE = "FC01";
    private static final String _DEFAULT_PST_CODE_BONUS = "FC02";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            // Note this List is a shallow "copy" of the bound reference.
            List aggrRewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            for (int masterIndex = 0; masterIndex < aggrRewardList.size();
                    masterIndex++) {
                OrderDetailVO masterOdVO = (OrderDetailVO) aggrRewardList.get(masterIndex);
                String masterMembershipId = masterOdVO.getMembershipVO()
                                                      .getMembershipNumber();
                Date masterDeliveryDate = masterOdVO.getDeliveryDate();
                String masterPstCode = masterOdVO.getPstCode();

                if (masterPstCode == null) {
                    masterPstCode = _DEFAULT_PST_CODE_BASE;
                    masterOdVO.setPstCode(masterPstCode);
                }

                String masterBonusPstCode = masterOdVO.getPstCodeBonus();

                if (masterBonusPstCode == null) {
                    masterBonusPstCode = _DEFAULT_PST_CODE_BONUS;
                    masterOdVO.setPstCodeBonus(masterBonusPstCode);
                }

                if ((masterMembershipId != null) &&
                        (!ActionUtil.isDummyMembershipId(masterMembershipId))) {
                    // Cycle through the list comparing the current membership
                    // value for matches, for the same day.
                    for (int index = masterIndex + 1;
                            index < aggrRewardList.size(); index++) {
                        OrderDetailVO odVO = (OrderDetailVO) aggrRewardList.get(index);
                        String membershipId = odVO.getMembershipVO()
                                                  .getMembershipNumber();

                        if ((membershipId != null) &&
                                (!ActionUtil.isDummyMembershipId(membershipId))) {
                            // If the membership ID matches, aggregate the reward values
                            // to the master (first) occurrence record.
                            if (masterMembershipId.equals(membershipId) &&
                                    (ActionUtil.setToZeroHour(
                                        masterDeliveryDate).getTime() == ActionUtil.setToZeroHour(
                                        odVO.getDeliveryDate()).getTime())) {
                                // Default the current record's PST codes.
                                String pstCode = odVO.getPstCode();

                                if (pstCode == null) {
                                    pstCode = _DEFAULT_PST_CODE_BASE;
                                    odVO.setPstCode(pstCode);
                                }

                                // We have to aggregate BOTH the base and bonus PST codes.
                                if (pstCode.equals(masterPstCode)) {
                                    masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                        odVO.getRewardBase());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBase(0);
                                }

                                // Evaluate the bonus PST code.
                                String pstCodeBonus = odVO.getPstCodeBonus();

                                if (pstCodeBonus == null) {
                                    pstCodeBonus = _DEFAULT_PST_CODE_BONUS;
                                    odVO.setPstCodeBonus(pstCodeBonus);
                                }

                                if (pstCodeBonus.equals(masterBonusPstCode)) {
                                    masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                        odVO.getRewardBonus());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBonus(0);
                                }
                            }
                        }
                    } // end of FOR index LOOP 

                    // Create a transformation record.
                    // We must do two passes for the base and bonus points.
                    boolean evalBonus = false;
                    boolean evalBase = true;

                    do {
                        if (!evalBase) {
                            // set an indicator for this last second loop
                            // to evaluate the bonus metrics.
                            evalBonus = true;
                        }

                        if ((evalBase &&
                                ((masterOdVO.getRewardBase() > 0) ||
                                (masterOdVO.getRewardBaseCalc() > 0))) ||
                                (evalBonus &&
                                ((masterOdVO.getRewardBonus() > 0) ||
                                (masterOdVO.getRewardBonusCalc() > 0)))) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            
                            if(masterMembershipId.length() == 8)
                              masterMembershipId = "000" + masterMembershipId;
                            else if(masterMembershipId.length() == 9)
                              masterMembershipId = "00" + masterMembershipId;
                            
                            recordLine.addStringValue(TextUtil.formatDate(
                                    masterOdVO.getDeliveryDate(), "MM/dd/yy"), 8);
                            recordLine.addStringValue(" ", 2);
                            recordLine.addStringValue(masterMembershipId, 11);
                            recordLine.addStringValue(" ", 1);
                            
                            String fullName = masterOdVO.getMembershipVO().getLastName() +
                                    " " + masterOdVO.getMembershipVO().getFirstName();
                            int fullNameLength = fullName.length() > 25 ? 25 : fullName.length();
                            recordLine.addStringValue(fullName.substring(0, fullNameLength),
                                25);
                            
                            recordLine.addStringValue(" ", 1);

                            float rewardTotal = 0;

                            if (evalBase) {
                                rewardTotal = masterOdVO.getRewardBaseCalc() +
                                    masterOdVO.getRewardBase();
                                if (masterPstCode.equals(masterBonusPstCode)) {
                                  // Since the base and bonus PST codes match
                                  // for the same record, combine all reward values.
                                  rewardTotal += masterOdVO.getRewardBonusCalc() +
                                    masterOdVO.getRewardBonus();
                                  
                                  // Zero the bonus values so that they are ignored
                                  // for the next loop.
                                  masterOdVO.setRewardBonusCalc(0);
                                  masterOdVO.setRewardBonus(0);
                                }
                            } else {
                                rewardTotal = masterOdVO.getRewardBonusCalc() +
                                    masterOdVO.getRewardBonus();
                            }

                            recordLine.addNumberValue(df.format(
                                    new Float(rewardTotal).doubleValue()), 5, ' ');

                            // Filler
                            recordLine.addStringValue(" ", 1);
                            
                            String pstCode = masterPstCode;

                            if (evalBonus) {
                                pstCode = masterBonusPstCode;
                            }

                            recordLine.addStringValue(pstCode, 4);

                            recordCollection.addRecordLine(recordLine);
                        }

                        // Since the first pass of prcessing of base point is
                        // complete, set the indicator for the second loop.
                        evalBase = false;
                    } while (!evalBonus);
                }
            } // end of FOR masterIndex LOOP

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Obtain a fresh copy from the context.
            // The resulting list of this processing will be used by StandardDbPersister.
            int rejectedRecordCount = 0;
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        (ActionUtil.isDummyMembershipId(membershipId))) {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
