package com.ftd.partnerreward.partner.americanautoassoc.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("########0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue(" ", 4);
                    recordLine.addStringValue("FTD.COM", 25);
                    recordLine.addStringValue(" ", 13);
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue(" ", 5);
                    recordLine.addStringValue(" ", 16);
                    recordLine.addStringValue(" ", 4);            
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "MMddyyyy"), 8);
                    recordLine.addStringValue(" ", 6);
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getBillingTotal()).doubleValue() * 100), 9);
                            
                    // Since AAA is not "rewards"-based per se, manually
                    // populate this metric for the later reporting step.
                    odVO.setRewardBase(odVO.getBillingTotal());
                    
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue("S", 1);
                    
                    // Unique shared partner order identifier.
                Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(
                        partnerName, PartnerSequenceType.RECORD);
                recordLine.addStringValue(partnerRecordId.toString(), 12);
                odVO.setPostFileRecordId(partnerRecordId.toString());
                
                recordLine.addStringValue(" ", 1);
                recordLine.addStringValue(" ", 36);
                recordLine.addStringValue(" ", 8);
                recordLine.addStringValue("000000000", 9);
                recordLine.addStringValue(" ", 13);
                recordLine.addStringValue(" ", 9);
                recordLine.addStringValue(" ", 9);
                recordLine.addStringValue(" ", 13);
                recordLine.addStringValue(" ", 2);
                recordLine.addStringValue(" ", 4);
                
                // This is the club code.
                recordLine.addStringValue(membershipId.substring(3, 6), 3);
                
                String customerFirstName = odVO.getMembershipVO().getFirstName();
                if (customerFirstName == null) 
                {
                  customerFirstName = odVO.getOrderHeaderVO().getCustomerFirstName();
                }
                recordLine.addStringValue(customerFirstName, 50);
                
                String customerLastName = odVO.getMembershipVO().getLastName();
                if (customerLastName == null) 
                {
                  customerLastName = odVO.getOrderHeaderVO().getCustomerLastName();
                }
                recordLine.addStringValue(customerLastName, 50);
                recordLine.addStringValue(odVO.getMembershipVO().getAddress1(), 50);
                recordLine.addStringValue(odVO.getMembershipVO().getAddress2(), 50);
                recordLine.addStringValue(odVO.getMembershipVO().getCity(), 50);
                recordLine.addStringValue(odVO.getMembershipVO().getState(), 2);
                recordLine.addStringValue(odVO.getMembershipVO().getZipCode(), 10);
                recordLine.addStringValue(" ", 10);
                
                    
//                    // The reward date has to be in greenwich mean time.
//                    recordLine.addStringValue(" ", 8);
//                    // Filler.
//                    recordLine.addStringValue(" ", 2);
//                    
//                    // Delta does not allow ANY punctuation in strings.
//                    recordLine.addStringValue(TextUtil.stripSpecialChars(odVO.getMembershipVO().getFirstName()),
//                    15);
//                    recordLine.addStringValue(TextUtil.stripSpecialChars(odVO.getMembershipVO().getLastName()),
//                    22);
//                    recordLine.addStringValue("FTD", 3);
//                    
//                    // Unique shared partner order identifier.
//                Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(
//                        partnerName, PartnerSequenceType.RECORD);
//                recordLine.addStringValue(partnerRecordId.toString(), 15);
//                odVO.setPostFileRecordId(partnerRecordId.toString());
//                
//                // Filler.
//                    recordLine.addStringValue(" ", 15);
//                    
//                    // Show the total value as an integer.
//                    recordLine.addNumberValue(df.format(
//                            new Float(odVO.getRewardTotal()).doubleValue()), 7);
//                    
//                    recordLine.addStringValue(TextUtil.formatDate(
//                            odVO.getDeliveryDate(), "ddMMMyy"), 7);
//                            
//                    // Filler.
//                    recordLine.addStringValue(" ", 11);
//                    
//                    String isBonusIndicator = "4"; // no bonus 
//                    if (odVO.getRewardBonus() > 0) 
//                    {
//                      isBonusIndicator = "6";
//                    }
//                    recordLine.addStringValue(isBonusIndicator, 8);
//                    recordLine.addNumberValue(df.format(
//                            new Float(odVO.getRewardBonus()).doubleValue()), 7);
//                    // Filler.
//                    recordLine.addStringValue(" ", 1);
//                    recordLine.addStringValue("E", 1);
//                    // Filler.
//                    recordLine.addStringValue(" ", 492);
                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
                    
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
