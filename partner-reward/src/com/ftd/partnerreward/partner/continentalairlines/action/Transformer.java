package com.ftd.partnerreward.partner.continentalairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("#####0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("2", 1);
                    recordLine.addStringValue(membershipId, 8);

                    // Compose the batch ID.
                    Calendar calPostDate = Calendar.getInstance();
                    calPostDate.setTime(postedFileDate);
                    recordLine.addStringValue(Integer.toString(calPostDate.get(
                                Calendar.YEAR)).substring(1, 4) +
                        Integer.toString(calPostDate.get(Calendar.DAY_OF_YEAR)),
                        6);

                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                            PartnerSequenceType.RECORD);
                    recordLine.addStringValue(partnerRecordId.toString(), 14);
                    odVO.setPostFileRecordId(partnerRecordId.toString());

                    // Property Code
                    // The bonus PST code takes precedence.
                    String pstCode = odVO.getPstCode();

                    if (odVO.getRewardBonus() > 0) {
                        pstCode = odVO.getPstCodeBonus();
                    }

                    recordLine.addStringValue(pstCode, 6);

                    // Partner Code
                    recordLine.addStringValue("F2", 2);
                    recordLine.addStringValue("", 2);
                    recordLine.addNumberValue("0000", 4, '0');

                    // Award date
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyMMdd"), 6);

                    // Member last name
                    String lastName = odVO.getMembershipVO().getLastName();

                    if ((lastName == null) || (lastName.length() < 1)) {
                        lastName = odVO.getOrderHeaderVO().getCustomerLastName();
                    }

                    recordLine.addStringValue(lastName.toUpperCase(), 20);

                    // Internal tracking number.
                    recordLine.addStringValue(partnerRecordId.toString(), 19);

                    recordLine.addStringValue("", 47);
                    recordLine.addStringValue("P", 1);
                    recordLine.addStringValue("", 31);
                    recordLine.addNumberValue("", 6, '0');

                    // Reward
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getRewardTotal()).doubleValue()), 6,
                        '0');

                    recordLine.addStringValue("", 71);
                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
