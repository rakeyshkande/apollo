package com.ftd.partnerreward.partner.unitedairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.StandardResponseExtractor;
import com.ftd.partnerreward.core.text.TextUtil;

import java.util.Date;


public class ResponseExtractor extends StandardResponseExtractor {
   protected Logger logger = new Logger(ResponseExtractor.class.getName());
    
    public ResponseExtractor() {
        super();
        // We want to get files starting with pattern set via composeResponseFileNameRemote
        setRemoteFileNameIsPattern(true);
    }

    protected String composeResponseFileNameRemote(String fileName,
        Date retrievalDate) {
    	// File should end with yyyyMMddhhmmss, but let's get everything for current day
        return fileName + TextUtil.formatDate(retrievalDate, "yyyyMMdd");
    }
    
    // Unfortunately, because of the jBPM framework, the accessors below must exist in
    // the implementing class (can not only be inherited from base class).
    protected void setAlertBody(String alertBody) {
        super.setAlertBody(alertBody);
    }

    protected void setAlertSubject(String alertSubject) {
        super.setAlertSubject(alertSubject);
    }

    protected void setFtpDir(String ftpDir) {
        super.setFtpDir(ftpDir);
    }

    protected void setFtpPassword(String ftpPassword) {
        super.setFtpPassword(ftpPassword);
    }

    protected void setFtpPort(int ftpPort) {
        super.setFtpPort(ftpPort);
    }

    protected void setFtpServer(String ftpServer) {
        super.setFtpServer(ftpServer);
    }

    protected void setFtpUsername(String ftpUsername) {
        super.setFtpUsername(ftpUsername);
    }

    protected void setLocalFileName(String localFileName) {
        super.setLocalFileName(localFileName);
    }

    protected void setLocalFilePath(String localFilePath) {
        super.setLocalFilePath(localFilePath);
    }

    protected void setRemoteFileName(String remoteFileName) {
        super.setRemoteFileName(remoteFileName);
    }
}
