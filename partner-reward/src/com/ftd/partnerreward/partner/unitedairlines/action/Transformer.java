package com.ftd.partnerreward.partner.unitedairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    private static final String DEFAULT_PST_CODE         = "X9E";  
    private static final String DEFAULT_PURCHASE_TYPE    = "KY ";

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String pstCode = null;
        String pstCodeBonus = null;
        String reward = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            int count = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("######0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {

                    // Get PST code
                    pstCode = (odVO.getPstCode() != null) ? odVO.getPstCode() : DEFAULT_PST_CODE;
                    pstCodeBonus = (odVO.getPstCodeBonus() != null) ? odVO.getPstCodeBonus() : pstCode;
                    // Bank activity overrides any source-assigned Bonus PST code.
                    if (odVO.getPartnerBankPstCode() != null) {
                        pstCodeBonus = odVO.getPartnerBankPstCode();
                    }
                    
                    // If the base and bonus PST codes are the same,
                    // represent the award as a single record.
                    if (pstCode.equalsIgnoreCase(pstCodeBonus)) {
					  IRecordLine recordLineSingle = recordCollection.createLineInstance();
					  reward = df.format(new Float(odVO.getRewardTotal()).doubleValue());
					  composeRecordLine(recordLineSingle, odVO, pstCode, DEFAULT_PURCHASE_TYPE, reward, count); 
					  count++;
					  recordCollection.addRecordLine(recordLineSingle);

                    // Otherwise base and bonus points need to be treated as separate records.
                    } else {
	                    // Base points
	                    if (odVO.getRewardBase() > 0) {
	                      IRecordLine recordLineBase = recordCollection.createLineInstance();
	                      reward = df.format(new Float(odVO.getRewardBase()).doubleValue());
	                      composeRecordLine(recordLineBase, odVO, pstCode, DEFAULT_PURCHASE_TYPE, reward, count); 
	                      count++;
	                      recordCollection.addRecordLine(recordLineBase);
	                    }
	                    // Bonus points
	                    if (odVO.getRewardBonus() > 0) {
	                      IRecordLine recordLineBonus = recordCollection.createLineInstance();
	                      reward = df.format(new Float(odVO.getRewardBonus()).doubleValue());
	                      composeRecordLine(recordLineBonus, odVO, pstCodeBonus, DEFAULT_PURCHASE_TYPE, reward, count);
	                      count++;
	                      recordCollection.addRecordLine(recordLineBonus);
	                    }
                    }
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
            
            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }


    private void composeRecordLine(IRecordLine recordLine, OrderDetailVO odVO, 
        String promoCode, String promoCodeBonus, String reward, int count) {

        recordLine.addStringValue("2", 1);
        recordLine.addStringValue(odVO.getMembershipVO().getMembershipNumber(), 11);
        recordLine.addStringValue(TextUtil.formatDate(
                odVO.getDeliveryDate(), "yyMMdd"));     // Transaction Create Date
        recordLine.addStringValue(odVO.getExternalOrderNumber() + count, 20);           // Transaction ID (just an arbitrary unique value)
        recordLine.addStringValue("FTD" + TextUtil.formatDate(odVO.getDeliveryDate(), "DDD"), 6);  // Batch ID
        recordLine.addStringValue(promoCode, 3);        // Purchase Code
        recordLine.addStringValue(promoCodeBonus, 3);   // Purchase Type
    	recordLine.addStringValue("000000", 6);         // Base Miles are always all zero
        recordLine.addNumberValue(reward, 6, '0');      // Bonus Miles field actually used for both base and bonus 
        recordLine.addStringValue("F2", 2);             // Partner Code
        recordLine.addStringValue("P", 1);              // Source Indicator
        recordLine.addStringValue(odVO.getMembershipVO().getLastName(), 20);  // Member Last Name
        recordLine.addStringValue(odVO.getExternalOrderNumber(), 34);         // External Order Number
    	recordLine.addStringValue("", 131);             // Filler
    }

}
