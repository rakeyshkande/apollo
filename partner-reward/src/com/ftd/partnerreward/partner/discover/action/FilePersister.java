package com.ftd.partnerreward.partner.discover.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {

    // 2006 October -- constants for revised Discover file format specification 
    // A more extensive reworking might be useful here -- placing some of these
    // constants in a Discover-specific constants file. Right now, two of the 
    // CONTEXT constants below are duplicated in the Transformer.java file. 
        
    // constants for actual Discover file fields in header and trailer records 
    private static final String headerRecordCodeFromDiscover = "1";
    private static final String trailerRecordCodeFromDiscover = "9";
    private static final String vendorCodeFromDiscover = "IERFTDFL";
    private static final String toBePaddedWithSpaces = "";
    
    // constants for process context; lookup keys for values shared between process states
    private static final String _CONTEXT_KEY_FILE_RECORD_COUNT = "_keyFileRecordCount";
    private static final String _CONTEXT_DISCOVER_CHECKSUM_VALUE = "_keyDiscoverChecksumValue";
    private static final String _CONTEXT_DISCOVER_PROMOTION_VALUE = "_keyDiscoverPromotionValue";
    // end of 2006 October added constants
    
    
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

//    protected String composePostFileNameRemote(String fileName, Date postDate) {
//        return fileName + TextUtil.formatDate(postDate, "yyyyMMdd") +
//        ".txt.asc";
//    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            // Initialize the remote file name.
            if (remoteFileName == null) {
//                remoteFileName = composePostFileNameRemote(fileName,
//                        postedFileDate);
                remoteFileName = fileName;

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    remoteFileName);
            }

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                        fileName, postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }


            
            // 2006 October -- revision of Discover file format requires that the
            // total number of records be written into the trailer record; therefore,
            // we need to keep track of the total number of records. 
            Integer fileRecordCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_RECORD_COUNT);
            int rollingFileRecordCount;
            if (fileRecordCount != null) {                             // some records were written to file earlier
                rollingFileRecordCount = fileRecordCount.intValue();
            } else {                                                   // these are first records being written to file
               rollingFileRecordCount = 0;
            }
            
            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {                                       
                // Write the reward records to the destination file.
                PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                    true);
                    
                // Record the number of records that have been placed in           
                // the file.                                                       
                rollingFileRecordCount += transformResult.size();                  
                context.setVariable(_CONTEXT_KEY_FILE_RECORD_COUNT,                
                    new Integer(rollingFileRecordCount));                          

                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }
            // end of 2006 October revision of record-writing procedure
            
            

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                 
                // 2006 October -- revised Discover file format specification requires
                // a header and trailer record

                // The header and trailer records need the current date and time 
                //  and a file sequence number from the database
                String promotionNumberFromDiscover = (String) context.getVariable(_CONTEXT_DISCOVER_PROMOTION_VALUE);
                Long contextChecksum = (Long) context.getVariable(_CONTEXT_DISCOVER_CHECKSUM_VALUE);               
                String eightCharDate = TextUtil.formatDate(postedFileDate, "yyyyMMdd");
                String sixCharTime = TextUtil.formatDate(postedFileDate, "HHmmss");
                Long fileSequenceNumber = RewardServiceBO.getPartnerSequenceNextVal(partnerName, 
                                                                             PartnerSequenceType.FILE);
                    
                // start of header record
                IRecordCollection headerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine headerRecordLine = headerCollection.createLineInstance();
                
                headerRecordLine.addStringValue(headerRecordCodeFromDiscover, 1);
                headerRecordLine.addStringValue(vendorCodeFromDiscover, 8);
                headerRecordLine.addNumberValue(Long.toString(fileSequenceNumber), 9);      
                headerRecordLine.addStringValue(eightCharDate, 8);
                headerRecordLine.addStringValue(sixCharTime, 6);
                headerRecordLine.addStringValue(promotionNumberFromDiscover, 6);  
                
                
                // Nov 2006 -- Discover received our file in the October revised format
                // and asked that one space be trimmed from the end of each record
                // to account for the end-of-line character
                // headerRecordLine.addStringValue(toBePaddedWithSpaces, 26);
                headerRecordLine.addStringValue(toBePaddedWithSpaces, 25);
                
                headerCollection.addRecordLine(headerRecordLine);
                PersistServiceBO.writeFileHeader(fullFileNameLocal, headerCollection);
                // end of header record
                
                
                
                // start of trailer record
                IRecordCollection trailerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine trailerRecordLine = trailerCollection.createLineInstance();
                
                trailerRecordLine.addStringValue(trailerRecordCodeFromDiscover, 1);
                trailerRecordLine.addStringValue(vendorCodeFromDiscover, 8);
                trailerRecordLine.addNumberValue(Long.toString(fileSequenceNumber), 9);      
                trailerRecordLine.addStringValue(eightCharDate, 8);
                trailerRecordLine.addStringValue(sixCharTime, 6);
                trailerRecordLine.addStringValue(promotionNumberFromDiscover, 6);  
                trailerRecordLine.addNumberValue(Integer.toString(rollingFileRecordCount), 9);
                trailerRecordLine.addNumberValue(contextChecksum.toString(), 15);  
                
                
                // Nov 2006 -- Discover received our file in the October revised format
                // and asked that one space be trimmed from the end of each record
                // to account for the end-of-line character
                // trailerRecordLine.addStringValue(toBePaddedWithSpaces, 2);
                trailerRecordLine.addStringValue(toBePaddedWithSpaces, 1);
                
                
                trailerCollection.addRecordLine(trailerRecordLine);
                PersistServiceBO.writeFileTrailer(fullFileNameLocal, trailerCollection);
                // end of trailer record

                // end of 2006 October header and trailer record write


                ActionUtil.logInfo(logger, executionContext,
                    extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to file " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
