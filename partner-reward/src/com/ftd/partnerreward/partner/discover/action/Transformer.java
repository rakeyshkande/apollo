package com.ftd.partnerreward.partner.discover.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.vo.GlobalParameterVO;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import com.ftd.partnerreward.core.dao.OrderDAO;           // JZ

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {

    // 2006 October -- constants for revised Discover file format specification 
    // A more extensive reworking might be useful here -- placing some of these
    // constants in a Discover-specific constants file. Right now, the 
    // two CONTEXT constants below are duplicated in the FilePersister.java file. 
    
    // constant name of global parm that we want to place in each of our records
    private static final String _DB_DISCOVER_PROMOTION_VALUE = "DISC_PROMOTION_VALUE";
     
    // constants for detail record fields
    private static final String detailRecordCodeFromDiscover = "5";
    private static final String offerID = "000000000000001";
    private static final String toBePaddedWithSpaces = "";
    
    // constants for process context; lookup keys for values shared between process states
    private static final String _CONTEXT_DISCOVER_CHECKSUM_VALUE = "_keyDiscoverChecksumValue";
    private static final String _CONTEXT_DISCOVER_PROMOTION_VALUE = "_keyDiscoverPromotionValue";
    
    // end of 2006 October added constants
    
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);


            // 2006 October -- revised Discover file format requires a stored promotion value in each 
            // detail record, and a checksum to be calculated as we go for writing to the file later.
            // If they are not already in the context from an earlier trip to this class, then we 
            // must create them and add them to the context. 
            
            // Promotion number
            String promotionNumberFromDiscover = (String) context.getVariable(_CONTEXT_DISCOVER_PROMOTION_VALUE);
            if (promotionNumberFromDiscover == null) {
               promotionNumberFromDiscover = OrderDAO.getGlobalParameter(
                                                   IWorkflowConstants.GLOBAL_PARMS_CONTEXT, 
                                                   _DB_DISCOVER_PROMOTION_VALUE).getValue();
               context.setVariable(_CONTEXT_DISCOVER_PROMOTION_VALUE, promotionNumberFromDiscover);                     
            }                            
            
            // Checksum value
            Long contextChecksum = (Long) context.getVariable(_CONTEXT_DISCOVER_CHECKSUM_VALUE);
            if (contextChecksum == null) {
                contextChecksum = new Long(0);
            }
            long checksumValue = contextChecksum.longValue();
            // end of 2006 October context manipulation
             
             
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("##########0");
            
            int rejectedRecordCount = 0;
            // For each calculation result, transform into the recordCollection.
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());
                    
                    // Remove the candidate from the list.
                    i.remove();
                    rejectedRecordCount++;

                    // re-loop
                    continue;
                }
                
 
                // revised Discover file format specifies a detail record of 64 bytes
                IRecordLine detailRecordLine = recordCollection.createLineInstance(); 
                detailRecordLine.addStringValue(detailRecordCodeFromDiscover, 1);
                detailRecordLine.addStringValue(promotionNumberFromDiscover, 6);  
                detailRecordLine.addStringValue(membershipId, 16);  
                detailRecordLine.addNumberValue(df.format(
                                 new Float(odVO.getRewardTotal()).doubleValue() * 100), 7);  
                detailRecordLine.addStringValue(toBePaddedWithSpaces, 3);
                detailRecordLine.addStringValue(offerID, 15);
                
                
                // Nov 2006 -- Discover received our file in the October revised format
                // and asked that one space be trimmed from the end of each record
                // to account for the end-of-line character
                // detailRecordLine.addStringValue(toBePaddedWithSpaces, 16);
                detailRecordLine.addStringValue(toBePaddedWithSpaces, 15);
                 
                 
                recordCollection.addRecordLine(detailRecordLine);
                // end of 2006 October revision of detail record
                
                
                // October 2006 - checksum manipulation
                //
                // add last six digits of membership ID to checksum
                checksumValue += Long.parseLong(membershipId.substring(10, 16));
            }
            
            // Store our running total in the context for use by a later state (or a later
            //  return to this state)
            context.setVariable(_CONTEXT_DISCOVER_CHECKSUM_VALUE, new Long(checksumValue));
            // end of October 2006 checksum manipulation
            

            // Bind the transformed data sets to the context.
            if (rejectedRecordCount > 0) {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
