package com.ftd.partnerreward.partner.koreanair.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.StandardResponseExtractor;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.File;

import java.util.Date;


public class ResponseExtractor extends StandardResponseExtractor {
    protected Logger logger = new Logger(ResponseExtractor.class.getName());
    
    public ResponseExtractor() {
        super();
    }

    protected String composeResponseFilePathLocal(String filePath,
        String fileName, Date retrievalDate) {
        
        return filePath + File.separator + replaceFileNameDate(fileName, retrievalDate);
    }
    
    protected String composeResponseFileNameRemote(String fileName,
        Date retrievalDate) {

        return replaceFileNameDate(fileName, retrievalDate);
    }
    
    /**
     * This method replaces the instance of YYYYMMDD in the file name parameter
     * with the correct values from the date parameter.
     * @param fileName The file name to replace YYYYMMDD in.
     * @param replaceDate The date to replace the YYYYMMDD with.
     * @return The updated file name as a string.
     * @author Andy Liakas 08/29/2006
     */
    private String replaceFileNameDate(String fileName, Date replaceDate) {
        String fileNameDate;
        
        /* If date is valid, replace YYYYMMDD with the date. */
        if(replaceDate != null) {
            fileNameDate = TextUtil.formatDate(replaceDate, "yyyyMMdd");
        }
        /* Else, replace YYYYMMDD with the current date. */
        else {
            fileNameDate = TextUtil.formatDate(new Date(), "yyyyMMdd");
        }
        
        return fileName.replace("YYYYMMDD", fileNameDate);
    }
    
    // Unfortunately, because of the jBPM framework, the accessors below must exist in
    // the implementing class (can not only be inherited from base class).
    protected void setAlertBody(String alertBody) {
        super.setAlertBody(alertBody);
    }

    protected void setAlertSubject(String alertSubject) {
        super.setAlertSubject(alertSubject);
    }

    protected void setFtpDir(String ftpDir) {
        super.setFtpDir(ftpDir);
    }

    protected void setFtpPassword(String ftpPassword) {
        super.setFtpPassword(ftpPassword);
    }

    protected void setFtpPort(int ftpPort) {
        super.setFtpPort(ftpPort);
    }

    protected void setFtpServer(String ftpServer) {
        super.setFtpServer(ftpServer);
    }

    protected void setFtpUsername(String ftpUsername) {
        super.setFtpUsername(ftpUsername);
    }

    protected void setLocalFileName(String localFileName) {
        super.setLocalFileName(localFileName);
    }

    protected void setLocalFilePath(String localFilePath) {
        super.setLocalFilePath(localFilePath);
    }

    protected void setRemoteFileName(String remoteFileName) {
        super.setRemoteFileName(remoteFileName);
    }
}
