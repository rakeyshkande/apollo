package com.ftd.partnerreward.partner.koreanair.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List aggrRewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
            
            // For each calculation result, transform into the recordCollection.
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("######0");

            for (int masterIndex = 0; masterIndex < aggrRewardList.size(); masterIndex++) {
                OrderDetailVO masterOdVO = (OrderDetailVO) aggrRewardList.get(masterIndex);
                String masterMembershipId = masterOdVO.getMembershipVO().getMembershipNumber();
                masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBase());
                masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonus());
                
                if ((masterMembershipId != null) &&
                        !ActionUtil.isDummyMembershipId(masterMembershipId)) {
                    // Cycle through the list comparing the current membership
                    // value for matches.
                    for (int index = masterIndex + 1; index < aggrRewardList.size(); index++) {
                        OrderDetailVO odVO = (OrderDetailVO) aggrRewardList.get(index);
                        String membershipId = odVO.getMembershipVO().getMembershipNumber();
                        
                        if ((membershipId != null) && !ActionUtil.isDummyMembershipId(membershipId)) {
                            // If the membership ID matches, aggregate the reward values
                            // to the master (first) occurrence record.
                            if (masterMembershipId.equals(membershipId)) {
                                masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                    odVO.getRewardBase());
                                masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                    odVO.getRewardBonus());
                                masterOdVO.setRewardTotalLimit(masterOdVO.getRewardTotalLimit() 
                                                                + odVO.getRewardTotalLimit());
                                

                                aggrRewardList.remove(index);
                                index--;
                                
                            }  
                        }
                    } // end of FOR index LOOP 

                     // Create a transformation record.
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addNumberValue("20", 2);
                    
                    /* SKYPASS number */
                    recordLine.addStringValue(masterMembershipId, 10);

                    // Surname (not used)
                    recordLine.addStringValue(" ", 28);
                    
                    // Date of birth (not used)
                    recordLine.addNumberValue("0", 6);
                    
                    /* Check in/out dates */
                    recordLine.addStringValue(TextUtil.formatDate(
                            masterOdVO.getDeliveryDate(), "yyMMdd"), 6); 
                    recordLine.addStringValue(TextUtil.formatDate(
                            masterOdVO.getDeliveryDate(), "yyMMdd"), 6);
                    
                    /* Mileage type */
                    recordLine.addStringValue("P", 1);
                    
                    // Amount (not used)
                    recordLine.addNumberValue("0", 9);
                    
                    // Reward total
                    recordLine.addNumberValue(df.format(
                            new Float(masterOdVO.getRewardTotalCalc()).doubleValue()), 7);
                    
                    // Filler.
                    recordLine.addStringValue(" ", 15);
                    
                    // Data sequence number (not used)
                    recordLine.addNumberValue(" ", 8);
                    
                    // Return code (not used)
                    recordLine.addNumberValue(" ", 2);

                    recordCollection.addRecordLine(recordLine);
                } 
            } // end of FOR masterIndex LOOP
            
            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                    recordCollection);

            // Obtain a fresh copy from the context.
            // The resulting list of this processing will be used by StandardDbPersister.
            int rejectedRecordCount = 0;
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) || ActionUtil.isDummyMembershipId(membershipId)) {
                    
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }
            
            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }
    
        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
