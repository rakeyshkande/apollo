package com.ftd.partnerreward.partner.hawaiianairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected static final String _CONTEXT_KEY_ROLLING_MILES_TOTAL = "_keyRollingMilesTotal";
    private static final DecimalFormat _DF = (DecimalFormat) DecimalFormat.getInstance();
    private static final String GeneralPartnerCode = "FTD";

    static {
        _DF.applyPattern("#####0");
    }

    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Long rollingMilesTotal = (Long) context.getVariable(_CONTEXT_KEY_ROLLING_MILES_TOTAL);

            if (rollingMilesTotal == null) {
                rollingMilesTotal = new Long(0);
            }
           
           IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(true,',');
 
            // For each calculation result, transform into the recordCollection.
            long latestRollingMilesTotal = rollingMilesTotal.longValue();

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                            PartnerSequenceType.RECORD);
                    odVO.setPostFileRecordId(partnerRecordId.toString());

                    // If the base and bonus PST codes are the same, compose
                    // a single record.
                    // If different, compose two records.
                    // Default any blank PST codes to "R".
                    String pstCode = (odVO.getPstCode() == null) ? "R"
                                                                 : odVO.getPstCode();
                    String pstCodeBonus = (odVO.getPstCodeBonus() == null)
                        ? "R" : odVO.getPstCodeBonus();

                    IRecordLine recordLine = recordCollection.createLineInstance();
          

                    if (pstCode.trim().equalsIgnoreCase(pstCodeBonus.trim())) {
                    
                        // Compose a single record having a reward total.                      
                        composeRecordLine(recordLine, odVO, 
                            odVO.getRewardTotal());
                        
                    } else {
                 
                        composeRecordLine(recordLine, odVO,
                            odVO.getRewardBase() + odVO.getRewardBonus());
                        
                        
                    }
                    
                    recordCollection.addRecordLine(recordLine);

                    // Increment a miles counter.
                    latestRollingMilesTotal += odVO.getRewardTotal();
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Record the rolling total miles for use by subsequent action classes,
            // i.e. the FilePersister
            context.setVariable(_CONTEXT_KEY_ROLLING_MILES_TOTAL,
                new Long(latestRollingMilesTotal));

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
        
        private static void composeRecordLine(IRecordLine recordLine,
            OrderDetailVO odVO, float reward) {
        recordLine.addStringValue("1");
        recordLine.addStringValue("HA");
        recordLine.addStringValue(odVO.getMembershipVO().getMembershipNumber());
       // recordLine.addStringValue(GeneralPartnerCode);
        recordLine.addStringValue(odVO.getOrderHeaderVO().getCustomerLastName());
        recordLine.addStringValue(odVO.getOrderHeaderVO().getCustomerFirstName());    
        recordLine.addStringValue(TextUtil.formatDate(odVO.getDeliveryDate(),"yyyyMMdd"));
        recordLine.addStringValue(_DF.format(new Float(reward).doubleValue()));
        recordLine.addStringValue(odVO.getExternalOrderNumber());
        recordLine.addStringValue(GeneralPartnerCode);
    }
}
