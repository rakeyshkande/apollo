package com.ftd.partnerreward.partner.virginamerica.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;


import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("#####");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO().getMembershipNumber();

                if ((membershipId != null) && !ActionUtil.isDummyMembershipId(membershipId)) {
                    try {
                        String customerFirstName = odVO.getMembershipVO().getFirstName();
                        String customerLastName = odVO.getMembershipVO().getLastName();
                        if (customerFirstName == null || customerFirstName.length() == 0) {
                            customerFirstName = odVO.getOrderHeaderVO().getCustomerFirstName();
                        }                    
                        if (customerLastName == null || customerLastName.length() == 0) {
                            customerLastName = odVO.getOrderHeaderVO().getCustomerLastName();
                        }
                        String addressLine1 = odVO.getMembershipVO().getAddress1();
                        String addressLine2 = odVO.getMembershipVO().getAddress2();
                        String city = odVO.getMembershipVO().getCity();
                        String country = odVO.getMembershipVO().getCountry();
                        String state = odVO.getMembershipVO().getState();
                        String pstCode = odVO.getPstCode();
                        String pstCodeBonus = odVO.getPstCodeBonus();
                        String externalOrderNumber = odVO.getExternalOrderNumber();   
                        float rewardBase = odVO.getRewardBase();
                        float rewardBonus = odVO.getRewardBonus();
                        String orderDateStr = TextUtil.formatDate(odVO.getOrderDate(), "yyyyMMdd");
                        
                        // Base points
                        if (rewardBase > 0) {
                            IRecordLine recordLineBase = recordCollection.createLineInstance();
                            composeRecordLine(recordLineBase, 
                                externalOrderNumber, 
                                customerFirstName,
                                customerLastName,
                                addressLine1,
                                addressLine2,
                                city,
                                country,
                                state,
                                membershipId,
                                pstCode,
                                pstCodeBonus,
                                rewardBase,
                                rewardBonus,
                                orderDateStr,
                                df, 
                                false
                            ); 
                            recordCollection.addRecordLine(recordLineBase); 
                        }
                        // Add bonus as seperate line.
                        if (rewardBonus > 0) {
                            IRecordLine recordLineBonus = recordCollection.createLineInstance();
                            composeRecordLine(recordLineBonus, 
                                externalOrderNumber, 
                                customerFirstName,
                                customerLastName,
                                addressLine1,
                                addressLine2,
                                city,
                                country,
                                state,
                                membershipId,
                                pstCode,
                                pstCodeBonus,
                                rewardBase,
                                rewardBonus,
                                orderDateStr,
                                df, 
                                true
                            ); 
                            recordCollection.addRecordLine(recordLineBonus); 
                        }
                    } catch (Exception e) {
                        logger.error("Failed to process order detail " + odVO.getOrderDetailId() + ". Process will continue");
                        ActionUtil.processError(logger, executionContext, e);
                    }

                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
    
    private void composeRecordLine(IRecordLine recordLine, 
                                   String externalOrderNumber, 
                                   String customerFirstName,
                                   String customerLastName,
                                   String addressLine1,
                                   String addressLine2,
                                   String city,
                                   String country,
                                   String state,
                                   String membershipId,
                                   String pstCode,
                                   String pstCodeBonus,
                                   float rewardBase,
                                   float rewardBonus,
                                   String orderDateStr,
                                   DecimalFormat df, 
                                   boolean isBonus) throws Exception {

        String eventCode = "";
        String itemIdentifier = String.valueOf(externalOrderNumber.charAt(externalOrderNumber.length()-1));
        String promoCode = pstCode;
        if(isBonus) {
            promoCode = pstCodeBonus;
        }
        // event code is promo code plus lat digit of external order number.
        eventCode = promoCode == null? itemIdentifier : (promoCode + itemIdentifier);
        
        // Record Type Indicator
        recordLine.addStringValue("2", 1);
        // Partner Code
        recordLine.addStringValue("FTD", 4);
        // Partner Member ID (external order number)
        recordLine.addStringValue(externalOrderNumber, 12);
        // First Name
        recordLine.addStringValue(customerFirstName, 20);
        // Middle Initital
        recordLine.addStringValue(" ", 1);
        // Last Name
        recordLine.addStringValue(customerLastName, 20);
        // Address Line 1
        recordLine.addStringValue(addressLine1, 30);
        // Address Line 2
        recordLine.addStringValue(addressLine2, 30);     
        // City
        recordLine.addStringValue(city, 18);
        // Country
        recordLine.addStringValue(country, 2);
        // State
        recordLine.addStringValue(state, 2);
        // Member Number
        recordLine.addStringValue(membershipId, 12);
        // Event Code
        recordLine.addStringValue(eventCode, 5); 
        // Base Points
        if(!isBonus) {
            // base
            recordLine.addNumberValue(df.format(new Float(rewardBase).doubleValue()), 10);
            // bonus
            recordLine.addNumberValue("0", 10);
        } else {
            // base
            recordLine.addNumberValue("0", 10);
            // Bonus Points
            recordLine.addNumberValue(df.format(new Float(rewardBonus).doubleValue()), 10);        
        }
        // Order Date
        recordLine.addStringValue(orderDateStr, 8);
        // Location Code
        recordLine.addStringValue(" ", 66);
    }
}
