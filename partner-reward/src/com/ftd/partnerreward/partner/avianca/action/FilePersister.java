package com.ftd.partnerreward.partner.avianca.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;
import com.ftd.partnerreward.partner.avianca.action.Transformer;

import java.io.File;
import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {
	private static final String _PARTNER_CODE = "FTDUS";
    private static final String _CONTEXT_KEY_FILE_RECORD_COUNT = "_keyFileRecordCount";
    protected static final String _CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID = "_keyPostedFileSequenceId";
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    protected String composePostFileNameRemote(String fileName, Date postDate) {
    	return fileName + TextUtil.formatDate(postDate, "yyyyMMdd") + ".TXT";
    }
        
    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            Long partnerFileId = (Long) context.getVariable(_CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID); 
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);            
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            Integer fileRecordCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_RECORD_COUNT);

            // Initialize the file record count.
            int rollingFileRecordCount = 0;

            if (fileRecordCount != null) {
                rollingFileRecordCount = fileRecordCount.intValue();
            }

            // Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = composePostFileNameRemote(fileName,
                        postedFileDate);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    remoteFileName);

                partnerFileId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                        PartnerSequenceType.FILE);
                context.setVariable(_CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID,
                    partnerFileId);
            }

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                        fileName, postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {
                PersistServiceBO.writeFileBody(fullFileNameLocal,
                    transformResult, true);

                // Record the number of records that have been placed in
                // the file.
                rollingFileRecordCount += transformResult.size();
                context.setVariable(_CONTEXT_KEY_FILE_RECORD_COUNT,
                    new Integer(rollingFileRecordCount));

                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {

            	// Compose the headers.
                IRecordCollection headerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine headerLine = headerCollection.createLineInstance();
                headerLine.addStringValue("H", 1);
                headerLine.addStringValue(TextUtil.formatDate(postedFileDate, "yyyyMMdd"), 8);
                headerLine.addStringValue(TextUtil.formatDate(postedFileDate, "HHmmss"), 6);
                headerLine.addStringValue(_PARTNER_CODE + "RQST", 10);
                headerLine.addStringValue(TextUtil.formatDate(postedFileDate, "yyyyMMddHH"), 10);  // Our unique identifier
                headerLine.addStringValue(_PARTNER_CODE, 5);  // Sender
                headerLine.addStringValue("LM", 5);           // Receiver
                headerLine.addNumberValue(partnerFileId.toString(), 9, '0');
                headerLine.addStringValue("", 946);
                headerCollection.addRecordLine(headerLine);
                PersistServiceBO.writeFileHeader(fullFileNameLocal, headerCollection);
            	
                // Compose the trailer.
                IRecordCollection trailerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine trailerLine = trailerCollection.createLineInstance();
                trailerLine.addStringValue("T", 1);
                trailerLine.addNumberValue(String.valueOf(rollingFileRecordCount), 9, '0');  // Number of records (excluding header/trailer)
                trailerLine.addNumberValue(String.valueOf(rollingFileRecordCount), 6, '0');  // Number of successful records (optional) 
                trailerLine.addStringValue("000000", 6);
                Long rollingMilesTotal = (Long) context.getVariable(Transformer._CONTEXT_KEY_ROLLING_MILES_TOTAL);
                trailerLine.addNumberValue(rollingMilesTotal.toString(), 12);  // Total miles posted
                trailerLine.addStringValue("", 966);                
                trailerCollection.addRecordLine(trailerLine);
                PersistServiceBO.writeFileTrailer(fullFileNameLocal, trailerCollection);

                ActionUtil.logInfo(logger, executionContext,
                    extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to file " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
