package com.ftd.partnerreward.partner.avianca.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected static final String _CONTEXT_KEY_ROLLING_MILES_TOTAL = "_keyRollingMilesTotal";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Long rollingMilesTotal = (Long) context.getVariable(_CONTEXT_KEY_ROLLING_MILES_TOTAL);

            if (rollingMilesTotal == null) {
                rollingMilesTotal = new Long(0);
            }

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");
            long latestRollingMilesTotal = rollingMilesTotal.longValue();

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO().getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("X", 1);
                    recordLine.addStringValue("", 3);  // Country code (optional)
                    recordLine.addStringValue(membershipId, 25);

                    String customerFirstName = odVO.getMembershipVO().getFirstName();
                    if (customerFirstName == null) {
                        customerFirstName = odVO.getOrderHeaderVO().getCustomerFirstName();
                    }
                    recordLine.addStringValue(customerFirstName, 20);

                    String customerLastName = odVO.getMembershipVO().getLastName();
                    if (customerLastName == null) {
                        customerLastName = odVO.getOrderHeaderVO().getCustomerLastName();
                    }
                    recordLine.addStringValue(customerLastName, 20);

                    recordLine.addStringValue("", 12);  // Member account (optional)
                    // Show the total value as an integer.
                    recordLine.addNumberValue(df.format(new Float(odVO.getRewardTotal()).doubleValue()), 7, '0');
                    recordLine.addStringValue(odVO.getPstCode(), 10);     // Avianca "Transaction Code" (i.e., promotion_code in source_master)
                    recordLine.addStringValue(odVO.getProgramName(), 6);  // Avianca "Service Type" (i.e., program_name in program_reward)
                    recordLine.addStringValue(TextUtil.formatDate(odVO.getDeliveryDate(), "yyyy-MM-dd-HH.mm.ss.000000"), 26);  // Date of transaction
                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName, PartnerSequenceType.RECORD);
                    recordLine.addNumberValue(partnerRecordId.toString(), 20, '0');  // Unique order ID
                    odVO.setPostFileRecordId(partnerRecordId.toString());
                    recordLine.addStringValue("", 10);   // Result Code (optional)
                    recordLine.addStringValue("", 14);   // Response Date (optional)
                    recordLine.addStringValue("", 100);  // Result Message (optional)
                    recordLine.addStringValue("", 25);   // Loyalty Number (optional)
                    recordLine.addStringValue("", 701);  // Filler
                    
                    recordCollection.addRecordLine(recordLine);

                    // Increment a miles counter.
                    latestRollingMilesTotal += odVO.getRewardTotal();
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Record the rolling total miles for use by subsequent action classes,
            // i.e. the FilePersister
            context.setVariable(_CONTEXT_KEY_ROLLING_MILES_TOTAL,
                new Long(latestRollingMilesTotal));

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
