package com.ftd.partnerreward.partner.ata.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    private static final String DEFAULT_PST_CODE         = "FTD";  //???
    private static final String PST_BRAND_FTD            = "FTD";
    private static final String PST_BRAND_FLORIST        = "FLO";
    private static final String PROMO_CODE_BASE_FTD      = "FTD001";
    private static final String PROMO_CODE_BONUS_FTD     = "FTD002";
    private static final String PROMO_CODE_BASE_FLORIST  = "FLO001";
    private static final String PROMO_CODE_BONUS_FLORIST = "FLO002";

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String promoCode = null;
        String reward = null;
        Long partnerRecordId = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, '^');

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("######0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {

                    // Get PST code
                    String pstCode = (odVO.getPstCode() != null) ? odVO.getPstCode() : DEFAULT_PST_CODE;
                    String pstCodeBonus = (odVO.getPstCodeBonus() != null) ? odVO.getPstCodeBonus() : pstCode;

                    // Base and bonus points need to be treated as separate records
                    
                    // Base points
                    if (odVO.getRewardBase() > 0) {
                      IRecordLine recordLineBase = recordCollection.createLineInstance();
                      if (pstCode.equalsIgnoreCase(PST_BRAND_FTD)) {
                        promoCode = PROMO_CODE_BASE_FTD;
                      } else if (pstCode.equalsIgnoreCase(PST_BRAND_FLORIST)) {
                        promoCode = PROMO_CODE_BASE_FLORIST;
                      }
                      partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName, PartnerSequenceType.RECORD);
                      odVO.setPostFileRecordId(partnerRecordId.toString()); //???
                      reward = df.format(new Float(odVO.getRewardBase()).doubleValue());
                      composeRecordLine(recordLineBase, odVO, partnerRecordId, promoCode, reward); 
                      recordCollection.addRecordLine(recordLineBase);
                    }
                    // Bonus points
                    if (odVO.getRewardBonus() > 0) {
                      IRecordLine recordLineBonus = recordCollection.createLineInstance();
                      if (pstCodeBonus.equalsIgnoreCase(PST_BRAND_FTD)) {
                        promoCode = PROMO_CODE_BONUS_FTD;
                      } else if (pstCodeBonus.equalsIgnoreCase(PST_BRAND_FLORIST)) {
                        promoCode = PROMO_CODE_BONUS_FLORIST;
                      }
                      partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName, PartnerSequenceType.RECORD);
                      odVO.setPostFileRecordId(partnerRecordId.toString()); //???
                      reward = df.format(new Float(odVO.getRewardBonus()).doubleValue());
                      composeRecordLine(recordLineBonus, odVO, partnerRecordId, promoCode, reward); 
                      recordCollection.addRecordLine(recordLineBonus);
                    }
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }


    private void composeRecordLine(IRecordLine recordLine, OrderDetailVO odVO, 
        Long partnerRecordId, String promoCode, String reward) {

        recordLine.addStringValue("D");   // Record Type
        recordLine.addStringValue("FTD"); // Partner Code
    
        // Unique shared partner order identifier.
        recordLine.addStringValue(partnerRecordId.toString());  // Transaction Number
    
        recordLine.addStringValue(TextUtil.formatDate(
                odVO.getDeliveryDate(), "MMddyyyy"));     // Transaction Create Date
    
        recordLine.addStringValue(odVO.getMembershipVO().getMembershipNumber());  // Travel Award Member Number
        recordLine.addStringValue("");                    // Location Code - not used
        recordLine.addStringValue(TextUtil.formatDate(
                odVO.getDeliveryDate(), "MMddyyyy"));     // Activity Date
        recordLine.addStringValue("");                    // Duration - not used
        recordLine.addStringValue("Floral");              // Activity Description
    
        recordLine.addStringValue(promoCode);             // Promotion ID
    
        recordLine.addStringValue(odVO.getMembershipVO().getLastName());  // Member Last Name
        recordLine.addStringValue(odVO.getMembershipVO().getFirstName()); // Member First Name
    
        recordLine.addStringValue(reward); // Point Amount 
    
        recordLine.addStringValue("");  // Filler

    }

}
