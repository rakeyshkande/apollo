package com.ftd.partnerreward.partner.virginairways.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            // Note this List is a shallow "copy" of the bound reference.
            List aggrRewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(true,
                    '~');

            for (int masterIndex = 0; masterIndex < aggrRewardList.size();
                    masterIndex++) {
                OrderDetailVO masterOdVO = (OrderDetailVO) aggrRewardList.get(masterIndex);
                String masterMembershipId = masterOdVO.getMembershipVO()
                                                      .getMembershipNumber();
                Date masterDeliveryDate = masterOdVO.getDeliveryDate();

                if ((masterMembershipId != null) && !ActionUtil.isDummyMembershipId(masterMembershipId)) {
                    
                    String pstCode = masterOdVO.getPstCode();
                    if (pstCode == null || pstCode.equals("")) pstCode = "0001";
                    String bonusPstCode = masterOdVO.getPstCodeBonus();
                    if (bonusPstCode == null || bonusPstCode.equals("")) bonusPstCode = "0001";

                    // Cycle through the list comparing the current membership
                    // value for matches, for the same day.
                    for (int index = masterIndex + 1; index < aggrRewardList.size(); index++) {
                        OrderDetailVO odVO = (OrderDetailVO) aggrRewardList.get(index);
                        String membershipId = odVO.getMembershipVO().getMembershipNumber();

                        if ((membershipId != null) && !ActionUtil.isDummyMembershipId(membershipId)) {
                            // If the membership ID matches, aggregate the reward values
                            // to the master (first) occurrence record.
                            if (masterMembershipId.equals(membershipId) &&
                                    (ActionUtil.setToZeroHour(masterDeliveryDate).getTime() == 
                                     ActionUtil.setToZeroHour(odVO.getDeliveryDate()).getTime())) {
                                // Set the derived calculations fields of the value object.
                                masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                    odVO.getRewardBase());
                                masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                    odVO.getRewardBonus());

                                // Remove the matching record from the shallow copy.
                                aggrRewardList.remove(index);
                                index = index - 1;
                            }
                        }
                    } // end of FOR index LOOP 
                    
                    // Create a transformation record.
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue(masterOdVO.getMembershipVO().getMembershipNumber());
                    recordLine.addStringValue("FTD");
                    recordLine.addStringValue(pstCode);
                    recordLine.addStringValue("A");
                    recordLine.addStringValue(df.format(
                            new Float(masterOdVO.getRewardBase() + masterOdVO.getRewardBaseCalc())));
                    recordLine.addStringValue(TextUtil.formatDate(
                            masterOdVO.getDeliveryDate(), "yyyyMMdd"));

                    recordCollection.addRecordLine(recordLine);
                    
                    if ( (masterOdVO.getRewardBonus() + masterOdVO.getRewardBonusCalc() ) > 0) {
                        IRecordLine bonusLine = recordCollection.createLineInstance();
                        bonusLine.addStringValue(masterOdVO.getMembershipVO().getMembershipNumber());
                        bonusLine.addStringValue("FTD");
                        bonusLine.addStringValue(bonusPstCode);
                        bonusLine.addStringValue("B");
                        bonusLine.addStringValue(df.format(
                                new Float(masterOdVO.getRewardBonus() + masterOdVO.getRewardBonusCalc())));
                        bonusLine.addStringValue(TextUtil.formatDate(
                                masterOdVO.getDeliveryDate(), "yyyyMMdd"));

                        recordCollection.addRecordLine(bonusLine);
                    }
                }
            } // end of FOR masterIndex LOOP

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Obtain a fresh copy from the context.
            // The resulting list of this processing will be used by StandardDbPersister.
            int rejectedRecordCount = 0;
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) || ActionUtil.isDummyMembershipId(membershipId)) {
                    
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }
            
            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
