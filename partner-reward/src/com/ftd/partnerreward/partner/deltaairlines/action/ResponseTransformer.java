package com.ftd.partnerreward.partner.deltaairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class ResponseTransformer implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());

    public ResponseTransformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                String currentDirectory = filePath.substring(0,
                        filePath.lastIndexOf(File.separator) + 1);

                // Unzip the file.
                PersistServiceBO.uncompressFile(PersistServiceBO.FILE_COMPRESSION_TYPE_ZIP,
                    filePath);

                // The unzipped file name is known.                
                // Open the file for writing.
                String unzippedFilePath = currentDirectory + "recon70";
                RandomAccessFile pointer = new RandomAccessFile(unzippedFilePath,
                        "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');
                IRecordCollection summaryCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + unzippedFilePath);

                    byte[] buffer = new byte[54];

                    if (pointer.read(buffer) != -1) {
                        // The first line in the source file is a header line.
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Layout Version ", buffer, 2, 2);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Processing Partner Code ", buffer, 4, 3);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Partner Code ", buffer, 7, 3);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "File Type Code ", buffer, 10, 2);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "File Date ", buffer, 12, 8);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Partner Batch Number ", buffer, 20, 20);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "NW Batch Number ", buffer, 40, 14);

                        // Go to the end of line.
                        pointer.readLine();

                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Record Type Code");
                        header.addStringValue("Account#");
                        header.addStringValue("Last Name");
                        header.addStringValue("First Name");
                        header.addStringValue("Partner Code");
                        header.addStringValue("Start Date");
                        header.addStringValue("End Date");
                        header.addStringValue("Order #");
                        header.addStringValue("Seq #");
                        header.addStringValue("Miles");
                        header.addStringValue("Amount Spent");
                        header.addStringValue("Customer Request Mileage");
                        header.addStringValue("Approved?");
                        header.addStringValue("Error 1");
                        header.addStringValue("Error 2");
                        header.addStringValue("Error 3");
                        header.addStringValue("Error 4");
                        header.addStringValue("Error 5");
                        header.addStringValue("Error 6");
                        header.addStringValue("Override 1");
                        header.addStringValue("Override 2");
                        header.addStringValue("Override 3");
                        header.addStringValue("Override 4");
                        header.addStringValue("Override 5");
                        header.addStringValue("Override 6");
                        header.addStringValue("WorldPerks #");

                        recordCollection.addRecordLine(header);
                        
                        buffer = new byte[190];

                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 2));
                            recordLine.addStringValue(new String(buffer, 2, 12));
                            recordLine.addStringValue(new String(buffer, 14, 40));
                            recordLine.addStringValue(new String(buffer, 54, 25));
                            recordLine.addStringValue(new String(buffer, 79, 3));
                            recordLine.addStringValue(new String(buffer, 82, 8));
                            recordLine.addStringValue(new String(buffer, 90, 8));
                            recordLine.addStringValue(new String(buffer, 98, 19));
                            recordLine.addStringValue(new String(buffer, 117, 3));
                            recordLine.addStringValue(new String(buffer, 120, 9));
                            recordLine.addStringValue(new String(buffer, 129, 9));
                            recordLine.addStringValue(new String(buffer, 138, 9));
                            recordLine.addStringValue(new String(buffer, 147, 1));
                            recordLine.addStringValue(new String(buffer, 148, 4));
                            recordLine.addStringValue(new String(buffer, 152, 4));
                            recordLine.addStringValue(new String(buffer, 156, 4));
                            recordLine.addStringValue(new String(buffer, 160, 4));
                            recordLine.addStringValue(new String(buffer, 164, 4));
                            recordLine.addStringValue(new String(buffer, 168, 4));
                            recordLine.addStringValue(new String(buffer, 172, 1));
                            recordLine.addStringValue(new String(buffer, 173, 1));
                            recordLine.addStringValue(new String(buffer, 174, 1));
                            recordLine.addStringValue(new String(buffer, 175, 1));
                            recordLine.addStringValue(new String(buffer, 176, 1));
                            recordLine.addStringValue(new String(buffer, 177, 1));
                            recordLine.addStringValue(new String(buffer, 178, 12));

                            recordCollection.addRecordLine(recordLine);

                            // Go to the end of line.
                            pointer.readLine();

                            // Skip the 1-line footer.
                            // Read two lines ahead.
                            long offset = pointer.getFilePointer();
                            pointer.readLine();

                            if (pointer.readLine() == null) {
                                // EOF is reached.
                                break;
                            } else {
                                // Restore the pointer to the previous location.
                                pointer.seek(offset);
                            }
                        }
                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = currentDirectory +
                    "Delta_Air_recon_" +
                    TextUtil.formatDate(new Date(), "MM-dd-yy") + ".txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
                PersistServiceBO.writeFileTrailer(transformedFilePath,
                    summaryCollection);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    private static void appendSummaryLine(IRecordCollection recordCollection,
        String fieldLabel, byte[] buffer, int fieldStartIndex, int fieldLength) {
        ResponseTransformer.appendSummaryLine(recordCollection, fieldLabel,
            buffer, fieldStartIndex, fieldLength);
    }
}
