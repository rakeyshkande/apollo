package com.ftd.partnerreward.partner.aircanada.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Iterator;
import java.util.List;


public class ResponseTransformerDecode implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformerDecode.class.getName());

    public ResponseTransformerDecode() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                String currentDirectory = filePath.substring(0,
                        filePath.lastIndexOf(File.separator) + 1);

                RandomAccessFile pointer = new RandomAccessFile(filePath, "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + filePath);

                    byte[] buffer = new byte[111];

                    IRecordLine header = recordCollection.createLineInstance();
                    header.addStringValue("Partner ID");
                    header.addStringValue("Business Name");
                    header.addStringValue("Marketing Program Code");
                    header.addStringValue("Marketing Program Name - English");
                    header.addStringValue("Marketing Program Name - French");
                    header.addStringValue("Start Date");
                    header.addStringValue("End Date");
                    header.addStringValue("Last Posting Date");

                    recordCollection.addRecordLine(header);

                    while (pointer.read(buffer) != -1) {
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        recordLine.addStringValue(new String(buffer, 0, 3));
                        recordLine.addStringValue(new String(buffer, 3, 20));
                        recordLine.addStringValue(new String(buffer, 23, 4));
                        recordLine.addStringValue(new String(buffer, 27, 30));
                        recordLine.addStringValue(new String(buffer, 57, 30));
                        recordLine.addStringValue(new String(buffer, 87, 8));
                        recordLine.addStringValue(new String(buffer, 95, 8));
                        recordLine.addStringValue(new String(buffer, 103, 8));

                        recordCollection.addRecordLine(recordLine);

                        // Go to the end of line.
                        pointer.readLine();
                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = currentDirectory +
                    "Air_Canada_Program_Decodes.txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
