package com.ftd.partnerreward.partner.aircanada.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Iterator;
import java.util.List;


public class ResponseTransformerReject implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformerReject.class.getName());

    public ResponseTransformerReject() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                String currentDirectory = filePath.substring(0,
                        filePath.lastIndexOf(File.separator) + 1);

                RandomAccessFile pointer = new RandomAccessFile(filePath, "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');
                IRecordCollection summaryCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + filePath);

                    byte[] buffer = new byte[18];

                    if (pointer.read(buffer) != -1) {
                        // The first line in the source file is a header line.
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Record Type ", buffer, 0, 2);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Partner ID Code ", buffer, 2, 3);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Creation Date ", buffer, 5, 8);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Data Record Count ", buffer, 13, 5);

                        // Go to the end of line.
                        pointer.readLine();

                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Record Type");
                        header.addStringValue("Partner ID");
                        header.addStringValue("Personal ID");
                        header.addStringValue("Alliance Location");
                        header.addStringValue("Order Number");
                        header.addStringValue("Earn Date");
                        header.addStringValue("Promotional Item Code");
                        header.addStringValue("Marketing Program Code");
                        header.addStringValue("Miles");
                        header.addStringValue("Receipt Date");
                        header.addStringValue("Error Code");

                        recordCollection.addRecordLine(header);
                        
                        buffer = new byte[97];

                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 2));
                            recordLine.addStringValue(new String(buffer, 2, 3));
                            recordLine.addStringValue(new String(buffer, 5, 10));
                            recordLine.addStringValue(new String(buffer, 15, 10));
                            recordLine.addStringValue(new String(buffer, 25, 10));
                            recordLine.addStringValue(new String(buffer, 35, 8));
                            recordLine.addStringValue(new String(buffer, 43, 10));
                            recordLine.addStringValue(new String(buffer, 53, 4));
                            recordLine.addStringValue(new String(buffer, 57, 7));
                            recordLine.addStringValue(new String(buffer, 86, 8));
                            recordLine.addStringValue(new String(buffer, 94, 3));

                            recordCollection.addRecordLine(recordLine);

                            // Go to the end of line.
                            pointer.readLine();

                            // Skip the 1-line footer.
                            // Read two lines ahead.
                            long offset = pointer.getFilePointer();
                            pointer.readLine();

                            if (pointer.readLine() == null) {
                                // EOF is reached.
                                // Restore the pointer to the start of the trailer.
                                buffer = new byte[59];
                                pointer.seek(offset);
                                logger.debug("At the start of the footer: " + pointer.getFilePointer());
                                pointer.read(buffer);
                                logger.debug("Contents of footer: " + (new String(buffer)));

                                // Compose additional lines in the summary section.
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "", "");

                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Record Type ", buffer, 0, 2);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Partner ID Code ", buffer, 2, 3);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "New Pre-Edit Records ", buffer, 5, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Accepted Pre-Edit Records ", buffer, 11, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Rejected Pre-Edit Records ", buffer, 17, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "New Records Ready ", buffer, 23, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Recycled Input Records ", buffer, 29, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Suspense Records N/A ", buffer, 35, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Accepted Records ", buffer, 41, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Rejected Records ", buffer, 47, 6);
                                ResponseTransformer.appendSummaryLine(summaryCollection,
                                    "Unprocessed Records ", buffer, 53, 6);

                                break;
                            } else {
                                // Restore the pointer to the previous location.
                                pointer.seek(offset);
                            }
                        }
                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = currentDirectory +
                    "Air_Canada_Rejected_Activities.txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
                PersistServiceBO.writeFileTrailer(transformedFilePath,
                    summaryCollection);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
