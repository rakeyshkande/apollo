package com.ftd.partnerreward.partner.aircanada.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Iterator;
import java.util.List;


public class ResponseTransformer {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());

    private ResponseTransformer() {
    }

    protected static void appendSummaryLine(IRecordCollection recordCollection,
        String fieldLabel, byte[] buffer, int fieldStartIndex, int fieldLength) {
        ResponseTransformer.appendSummaryLine(recordCollection, fieldLabel,
            new String(buffer, fieldStartIndex, fieldLength));
    }

    protected static void appendSummaryLine(IRecordCollection recordCollection,
        String fieldLabel, String fieldValue) {
        IRecordLine sLine = recordCollection.createLineInstance();
        sLine.addStringValue(fieldLabel + fieldValue);
        recordCollection.addRecordLine(sLine);
    }
}
