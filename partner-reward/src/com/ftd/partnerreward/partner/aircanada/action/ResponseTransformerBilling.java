package com.ftd.partnerreward.partner.aircanada.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Iterator;
import java.util.List;


public class ResponseTransformerBilling implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformerBilling.class.getName());

    public ResponseTransformerBilling() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                String currentDirectory = filePath.substring(0,
                        filePath.lastIndexOf(File.separator) + 1);

                RandomAccessFile pointer = new RandomAccessFile(filePath, "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');
                IRecordCollection summaryCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + filePath);

                    byte[] buffer = new byte[40];

                    if (pointer.read(buffer) != -1) {
                        // The first line in the source file is a header line.
                        // Note that the first character is ignored.
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Club Code ", buffer, 1, 2);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Partner ID Code ", buffer, 3, 3);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Currency ISO Code ", buffer, 6, 3);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Billing Cycle End Date ", buffer, 9, 8);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Record Count ", buffer, 17, 9);
                        ResponseTransformer.appendSummaryLine(summaryCollection,
                            "Billing Amount ", buffer, 26, 14);

                        // Go to the end of line.
                        pointer.readLine();

                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Record Type");
                        header.addStringValue("Club Code");
                        header.addStringValue("Partner ID Code");
                        header.addStringValue("Currency ISO Code");
                        header.addStringValue("Personal ID Number");
                        header.addStringValue("Marketing Program");
                        header.addStringValue("Crediting Source Code");
                        header.addStringValue("Billing Rate");
                        header.addStringValue("Unit of Measure");
                        header.addStringValue("Activity Date");
                        header.addStringValue("Posting Date");
                        header.addStringValue("Alliance Location Name");
                        header.addStringValue("Order Number");
                        header.addStringValue("Promotional Item Code");
                        header.addStringValue("Partner Reference ID");
                        header.addStringValue("Mileage");
//                        header.addStringValue("Reject Message Code");

                        recordCollection.addRecordLine(header);

                        buffer = new byte[105];
                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 1));
                            recordLine.addStringValue(new String(buffer, 1, 2));
                            recordLine.addStringValue(new String(buffer, 3, 3));
                            recordLine.addStringValue(new String(buffer, 6, 3));
                            recordLine.addStringValue(new String(buffer, 9, 10));
                            recordLine.addStringValue(new String(buffer, 19, 4));
                            recordLine.addStringValue(new String(buffer, 23, 2));
                            recordLine.addStringValue(new String(buffer, 25, 6));
                            recordLine.addStringValue(new String(buffer, 31, 4));
                            recordLine.addStringValue(new String(buffer, 35, 8));
                            recordLine.addStringValue(new String(buffer, 43, 8));
                            recordLine.addStringValue(new String(buffer, 51, 10));
                            recordLine.addStringValue(new String(buffer, 61, 10));
                            recordLine.addStringValue(new String(buffer, 71, 10));
                            recordLine.addStringValue(new String(buffer, 81, 16));
                            recordLine.addStringValue(new String(buffer, 97, 8));
//                            recordLine.addStringValue(new String(buffer, 106, 3));

                            recordCollection.addRecordLine(recordLine);

                            // Go to the end of line.
                            pointer.readLine();
                        }
                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = currentDirectory +
                    "Air_Canada_Billing_Recon.txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
                PersistServiceBO.writeFileTrailer(transformedFilePath,
                    summaryCollection);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
