package com.ftd.partnerreward.partner.jetblueairlines.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;


import com.ftd.partnerreward.core.dao.UtilDAO;

import java.io.BufferedWriter;
import java.io.File;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import org.xml.sax.SAXException;


public class EmailSender implements ActionHandler {

    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String MESSAGE_GENERATOR_CONFIG = "MESSAGE_GENERATOR_CONFIG";
    private static final String SMTP_HOST_NAME = "SMTP_HOST_NAME";
    
    // These fields will be initialized by the process archive.
    protected String emailGroupKey;
    protected String emailSubject;
    protected String fromAddress;
    protected String emailContent;
    protected boolean deleteFileFlag;
    private static Logger logger  = new Logger("com.ftd.partnerreward.partner.jetblueairlines.action.EmailSender");
    
    
    public EmailSender()
    {
    }
  
     public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String toEmailAddressList = null;
        List fullFileNamesLocal = null;
        File sendFile = null;
        
        try {
            logger.info("execute: Beginning sending email to " + emailGroupKey);
            ContextInstance context = executionContext.getContextInstance();
            fullFileNamesLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            toEmailAddressList = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, emailGroupKey);
            //send one email per file received
            for (Iterator it = fullFileNamesLocal.iterator(); it.hasNext();) {
                String fullFileNameLocal = (String) it.next();
                
                NotificationVO notifVO = new NotificationVO();
                NotificationUtil mail = NotificationUtil.getInstance();
    
                notifVO.setMessageTOAddress(toEmailAddressList);
                notifVO.setAddressSeparator(",");
                notifVO.setMessageFromAddress(fromAddress);
                notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm(MESSAGE_GENERATOR_CONFIG, SMTP_HOST_NAME));
                notifVO.setMessageSubject(emailSubject);

                sendFile = new File(fullFileNameLocal);
                
                
                notifVO.setFileAttachment(new File(fullFileNameLocal));
                notifVO.setMessageContent(emailContent);
                notifVO.setMessageContentHTML(emailContent);
                mail.notifyMultipart(notifVO, deleteFileFlag);

            }
            
            String errorFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT);
            if (errorFileName!=null) {
                File inFile = new File(errorFileName);
                logger.debug(
                    "execute: start errors processing: " + errorFileName);
                if (!inFile.exists()) {
                  inFile.createNewFile();
                }    
                LineNumberReader in = new LineNumberReader(new FileReader(inFile));
                
                StringBuffer sb = new StringBuffer(2048);
                sb.append("Full list of errors in a file: ").append(errorFileName).append("\n");
                try {
                  String record = null;
                  while ((record = in.readLine()) != null) {
                      sb.append(record).append("\n");
                  }
                } finally {
                  in.close();
                }
                logger.debug("execute: error email body: " + sb.toString());
                sendError(sb.toString());
            }
            logger.info("execute: Completed processing of sending email.");           
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private void sendError(String message) 
      throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException, Exception
    {
        Connection dbConn = UtilDAO.getDbConnection();

        try {
            // create the System Messenger VO and send out the system message 
            SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
            systemMessengerVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            systemMessengerVO.setSource("PARTNER REWARD");
            systemMessengerVO.setType("INFO");
            systemMessengerVO.setSubject("PARTNER REWARD: JetBlue - some transactions were rejected");
            systemMessengerVO.setMessage(message);
            
            logger.debug("sendError: alertSubject=" + systemMessengerVO.getSubject() +
                " alertBody=" + systemMessengerVO.getMessage());

            String systemMsgId = null;

            if ((systemMsgId = SystemMessenger.getInstance().send(systemMessengerVO, dbConn, false)) == null) {
                throw new RuntimeException(
                    "sendError: Null system message ID for " +
                    systemMessengerVO.getSubject());
            }

            //logger.debug("notifyOperations: alert Result=" + ReportingDAO.insertMessage(vo));
            logger.info("sendError: alertSubject=" + systemMessengerVO.getSubject() + 
            " alertBody=" + systemMessengerVO.getMessage() +
            " systemMessageID=" + systemMsgId);
        } finally {
            try {
                dbConn.close();
            } catch (Exception e) {
                logger.error("sendError: JDBC clean-up failed.", e);
            }
        }
    }
         
}
