package com.ftd.partnerreward.partner.jetblueairlines.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.action.BaseResponseExtractor;

import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.bo.TransportServiceBO;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.File;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class ResponseExtractor implements ActionHandler {
    private static Map partnerNameToProject = new HashMap();

    // Used to obtain Secure Configuration Properties
    private static final String SECURE_CONFIG_CONTEXT = "partner_reward";
    private static final String CONFIG_CONTEXT = "PARTNER_REWARD_CONFIG";
    private static final String INBOUND_USERNAME = "_InboundFtpUser";
    private static final String INBOUND_PASSWORD = "_InboundFtpPswd";
    private static final String INBOUND_SERVER = "_INBOUND_SERVER";

    static {
        // Create a lookup table of "project" name to partner name.
        // @todo In a later release, make this db data-driven rather than hardcode.
        partnerNameToProject.put("JETBLUE", "1");

    }

    protected Logger logger = new Logger(BaseResponseExtractor.class.getName());

    // These fields will be initialized by the process archive.
    private String ftpServer;
    private String ftpDir;
    private String ftpUsername;
    private String ftpPassword;
    private int ftpPort;
    private String remoteFileName;
    private String localFilePath;
    private String alertSubject;
    private String alertBody;

    // OPTIONAL Specifies the locally mandated file name
    private String localFileName;

    // OPTIONAL transmission type indicator.  True means a binary transfer.
    // Default is false (i.e. ASCII transfer)
    protected boolean isBinary;

    // OPTIONAL indicator to prevent overwriting any existing file on the remote server.
    // Default is false (i.e. no protection; file CAN be overwritten)
    protected boolean isOverwriteProtected;

    public ResponseExtractor() {
    }

    protected String composeResponseFilePathLocal(String filePath,
        String fileName, Date retrievalDate) {
        return filePath;
    }

    protected String composeResponseFileNameRemote(String fileName,
        Date retrievalDate){
        return fileName + TextUtil.formatDate(retrievalDate, "yyyyMMdd");
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            
            // Obtain Secure username and password
            if(partnerName != null) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String consPartnerName = TextUtil.stripSpecialChars(partnerName);
                ftpUsername = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_USERNAME) );
                ftpPassword = cu.getSecureProperty(SECURE_CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_PASSWORD) );
                ftpServer = cu.getFrpGlobalParm(CONFIG_CONTEXT, 
                    (consPartnerName + INBOUND_SERVER) );
                ActionUtil.logInfo(logger,executionContext,"FTP Server is " + ftpServer);                    
            } else {
                throw new Exception("partnerName is null.  partnerName is required to obtain the Secure Configuration Properties.");  
            }

            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            if (responseFilePathLocal == null) {
                responseFilePathLocal = new ArrayList();
            }

            // Compose the retrieval date.  This is the current date.
            Date retrievalDate = new Date();

            // Compose the remote file name.
            String cleanRemoteFilePattern = composeResponseFileNameRemote(remoteFileName,
                    retrievalDate);

            // Compose the local file name.
            String fullFileNameLocal = composeResponseFilePathLocal(localFilePath,
                        localFileName, retrievalDate);


            // Obtain the response file from the remote partner server.
            List localFiles = TransportServiceBO.getFtpAdapter().prDownloadFiles(fullFileNameLocal,
                    cleanRemoteFilePattern, ftpDir, ftpServer, ftpUsername,
                    ftpPassword, isBinary);

            if (localFiles!=null && localFiles.size()>0) {
                ActionUtil.logInfo(logger, executionContext,
                    localFiles.size() + " files successfully obtained " +
                    partnerName + " response files from " + ftpServer + ":" +
                    ftpDir + File.separator + cleanRemoteFilePattern + "*");

                // Bind the local file to the context list.
                responseFilePathLocal.addAll(localFiles);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL,
                    responseFilePathLocal);

                // Provide an alert to interested parties.
                String groupId = (String) partnerNameToProject.get(partnerName);

                if (groupId == null) {
                    groupId = "1";
                }

                StringBuffer sb = new StringBuffer();  
                for (Iterator<String> it = localFiles.iterator() ; it.hasNext() ; ){  
                    sb.append(it.next());  
                    if (it.hasNext()) {  
                      sb.append(", ");   
                    }  
                }

                AlertService.processAlert("RESPONSE GROUP " + groupId,
                    alertSubject + " " + partnerName,
                    alertBody + " " + sb.toString());
            } else {
                ActionUtil.logInfo(logger, executionContext,
                    "Could not obtain " + partnerName + " response files from " +
                    ftpServer + ":" + ftpDir + File.separator +
                    cleanRemoteFilePattern + "* because files with such name pattern do not exist.");

                // End the workflow.
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_END;
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
    
    

    protected String getAlertBody() {
        return alertBody;
    }

    protected void setAlertBody(String alertBody) {
        this.alertBody = alertBody;
    }

    protected String getAlertSubject() {
        return alertSubject;
    }

    protected void setAlertSubject(String alertSubject) {
        this.alertSubject = alertSubject;
    }

    protected String getFtpDir() {
        return ftpDir;
    }

    protected void setFtpDir(String ftpDir) {
        this.ftpDir = ftpDir;
    }

    protected String getFtpPassword() {
        return ftpPassword;
    }

    protected void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    protected int getFtpPort() {
        return ftpPort;
    }

    protected void setFtpPort(int ftpPort) {
        this.ftpPort = ftpPort;
    }

    protected String getFtpServer() {
        return ftpServer;
    }

    protected void setFtpServer(String ftpServer) {
        this.ftpServer = ftpServer;
    }

    protected String getFtpUsername() {
        return ftpUsername;
    }

    protected void setFtpUsername(String ftpUsername) {
        this.ftpUsername = ftpUsername;
    }

    protected String getLocalFileName() {
        return localFileName;
    }

    protected void setLocalFileName(String localFileName) {
        this.localFileName = localFileName;
    }

    protected String getLocalFilePath() {
        return localFilePath;
    }

    protected void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    protected String getRemoteFileName() {
        return remoteFileName;
    }

    protected void setRemoteFileName(String remoteFileName) {
        this.remoteFileName = remoteFileName;
    }
}
