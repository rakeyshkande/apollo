package com.ftd.partnerreward.partner.jetblueairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    
    private static final DecimalFormat _DF = (DecimalFormat) DecimalFormat.getInstance();


    protected Logger logger = new Logger(Transformer.class.getName());
    
    static {
        _DF.applyPattern("#####0");
    }


    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

           
           IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,'|');
 


            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) { 
                  
                    if (odVO.getRewardBase()>0) {
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        composeRecordLine(recordLine, odVO, true);
                        recordCollection.addRecordLine(recordLine);
                        
                    }
                    if (odVO.getRewardBonus()>0) {
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        composeRecordLine(recordLine, odVO, false);
                        recordCollection.addRecordLine(recordLine);
                    }
                    
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            
            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
        
        private static void composeRecordLine(IRecordLine recordLine, OrderDetailVO odVO, boolean isBaseOffer) {
          recordLine.addStringValue("D"); //record type
          recordLine.addStringValue(odVO.getMembershipVO().getMembershipNumber()); //membership #
          String firstName = odVO.getMembershipVO().getFirstName();
          if (firstName != null) {
              firstName = pipeReplacer(firstName.toUpperCase());
          }
          recordLine.addStringValue(firstName); //first name
          String lastName = odVO.getMembershipVO().getLastName();
          if (lastName != null) {
              lastName = pipeReplacer(lastName.toUpperCase());
          }
          recordLine.addStringValue(lastName); //last name
          recordLine.addStringValue(isBaseOffer ? "B" : "P"); //base or bonus offer
          recordLine.addStringValue(TextUtil.formatDate(odVO.getOrderDate(), "MMddyyyy")); // partner transacrion date
          recordLine.addStringValue(TextUtil.formatDate(odVO.getOrderDate(), "HHmm")); //partner transaction time
          recordLine.addStringValue("US/CENTRAL"); //time zone
          recordLine.addStringValue(_DF.format(isBaseOffer ? odVO.getRewardBase() : odVO.getRewardBonus())); //points
          recordLine.addStringValue(""); //reward
          recordLine.addStringValue("FTD.com Purchase"); //description
          recordLine.addStringValue(odVO.getExternalOrderNumber() + (isBaseOffer ? "B" : "P")); //base or bonus offer with order number
          recordLine.addStringValue(""); //Reversal transaction identifier
    }
        public static String pipeReplacer(String temp){  
            return temp.replaceAll("\\|", "&#124;");          
        }
}
