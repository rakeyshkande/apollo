package com.ftd.partnerreward.partner.jetblueairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Date;

import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {
    protected Logger logger = new Logger(FilePersister.class.getName());
    
    protected static final String _CONTEXT_KEY_POST_FILE_NAME = "_keyPostFileName";
    protected static final String _CONTEXT_KEY_POST_FILES_RECORD_COUNT = "_keyPostFilesRecordCount";
    protected static final String _CONTEXT_KEY_POST_FILE_SIZE = "_keyPostFileSize";

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    protected String fileSizeLimit;

    public FilePersister() {
    }

    protected String composePostFileNameRemote(String fileName, Date postDate) {
        return fileName + TextUtil.formatDate(postDate, "yyyyMMdd_HH_mm_ss_")+"FT.trn";
    }

    protected String composePostFileNameLocal(String fileName, Date postDate) {
        return filePath + File.separator + fileName + TextUtil.formatDate(postDate, "yyyyMMdd_HH_mm_ss_")+"FT.trn";
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            logger.debug("***context: " + context);
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            logger.debug("***transformResult: " + transformResult);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            logger.debug("***postedFileDate: " + postedFileDate);
            logger.debug("***transformResult.size(): " + transformResult.size());
            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {
                writeFilesBody(transformResult, context);
                logger.debug("***after writeFilesBody()***");
                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file(s) ");
            }
            
            //ArrayList remoteFileNames = (ArrayList) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            ArrayList fullFileNamesLocal = (ArrayList) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            Map recordsCount = (Map) context.getVariable(_CONTEXT_KEY_POST_FILES_RECORD_COUNT);
            
            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);


            if (fullFileNamesLocal==null) {
                logger.debug("***fullFileNamesLocal was null, assigning new ArrayList");
                fullFileNamesLocal = new ArrayList();
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, fullFileNamesLocal);
            }

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                // Compose the trailer.
                for (int i = 0; i < fullFileNamesLocal.size(); i++) {
                    String fullFileNameLocal = (String) fullFileNamesLocal.get(i);
                    writeHeader(fullFileNameLocal, postedFileDate);
                    writeTrailer(fullFileNameLocal, (Long)recordsCount.get(fullFileNameLocal));
                    ActionUtil.logInfo(logger, executionContext,
                        extractCompleteInd.toString() +
                        " records representing all records across all days have been successfully persisted to file " +
                        fullFileNameLocal);
                }      
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
    
    private void writeHeader(String fileNameLocal, Date date) {
        logger.debug("***inside writeHeader()***");
        IRecordCollection headerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, '|');
        IRecordLine headerLine = headerCollection.createLineInstance();
        headerLine.addStringValue("H");
        headerLine.addStringValue(TextUtil.formatDate(date,"yyyyMMdd"));
        headerLine.addStringValue("FT\r\n");
        headerCollection.addRecordLine(headerLine);
        PersistServiceBO.writeFileHeader(fileNameLocal, headerCollection,"\r\n",false);
    }
    
    private void writeTrailer(String fileNameLocal, Long recordsCount) {
        logger.debug("***inside writeTrailer()***");
        IRecordCollection trailerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, '|');
        IRecordLine trailerLine = trailerCollection.createLineInstance();
        trailerLine.addStringValue("T");
        trailerLine.addStringValue(recordsCount.toString());
        trailerCollection.addRecordLine(trailerLine);
        writeFileTrailer(fileNameLocal, trailerCollection);
    }
    
    private void writeFilesBody(IRecordCollection recordCollection, ContextInstance context) {

        try {
            logger.debug("***inside writeFilesBody()***");
            String fullFileNameLocal = (String) context.getVariable(_CONTEXT_KEY_POST_FILE_NAME);
            logger.debug("***fullFileNameLocal: " + fullFileNameLocal);
            ArrayList remoteFileNames = (ArrayList) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            ArrayList fullFileNamesLocal = (ArrayList) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            Map recordsCount = (Map) context.getVariable(_CONTEXT_KEY_POST_FILES_RECORD_COUNT);
            Long fileSizeL = (Long) context.getVariable(_CONTEXT_KEY_POST_FILE_SIZE);
            logger.debug("***fileSizeL: " + fileSizeL);
            
            if(recordsCount==null){
                logger.debug("***recordsCount was null, assigning new HashMap");
                recordsCount = new HashMap();
            }
            
            if (fullFileNamesLocal==null) {
                logger.debug("***fullFileNamesLocal was null, assigning new ArrayList");
                fullFileNamesLocal = new ArrayList();
            }
            
            if (remoteFileNames==null) {
                logger.debug("***remoteFileNames was null, assigning new ArrayList");
                remoteFileNames = new ArrayList();
            }
            
            long fileSize = 0;
            long fileRecordsCount = 0;
            if (fileSizeL!=null){
                logger.debug("***fileSizeL was NOT null, getting long value");
                fileSize = fileSizeL.longValue();
            }    
            if (fullFileNameLocal==null) {
                logger.debug("***fullFileNameLocal was null...");
                Date date = new Date();
                fullFileNameLocal = composePostFileNameLocal(fileName, date);
                logger.debug("***fullFileNameLocal after composePostFileNameLocal(): " + fullFileNameLocal);
                fullFileNamesLocal.add(fullFileNameLocal);
                remoteFileNames.add(composePostFileNameRemote(fileName, date));
                logger.debug("***remoteFileNames after composePostFileNameRemote(): " + remoteFileNames);
                context.setVariable(_CONTEXT_KEY_POST_FILE_NAME, fullFileNameLocal);
            }
            
            if (recordsCount.get(fullFileNameLocal) != null)
                fileRecordsCount = ((Long)recordsCount.get(fullFileNameLocal)).longValue();
                
            logger.debug("***fileRecordsCount: " + fileRecordsCount);
            File file = new File(fullFileNameLocal);

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file, true)), true);
                        
            long fileSizeLimit = 0;
            try {
                fileSizeLimit = Long.parseLong(this.fileSizeLimit);
            } catch (Exception e) {
                logger.error("Can not parse File Size Limit (" + this.fileSizeLimit + ")", e);
            }
            logger.debug("***fileSizeLimit: " + fileSizeLimit);
            logger.debug("***Going to iterate through collection...");
            try {
                // Iterate through the transformation collection, writing each record to the file.
                for (Iterator i = recordCollection.iterator(); i.hasNext();) {
                    logger.debug("***recordCollection.size(): " + recordCollection.size());
                    String str = i.next().toString();
                    logger.debug("***str: " + str);
                    logger.debug("***fileSize: " + fileSize);
                    logger.debug("***str.length(): " + str.length());
                    logger.debug("***fileSizeLimit - 32: " + (fileSizeLimit - 32));
                    logger.debug("testing if filesize > 50mb...");
                    if (!((fileSizeLimit>0) && ((fileSize + str.length() + 2) < (fileSizeLimit - 32)))) {
                    //close current file and create new one if size greter then fileSizeLimit
                        logger.debug(".... file size is > 50mb");
                        out.close();
                        recordsCount.put(fullFileNameLocal, new Long(fileRecordsCount));
                        fileRecordsCount = 0;
                        fileSize = 0;
                        Date date = new Date();
                        fullFileNameLocal = composePostFileNameLocal(fileName, date);
                        fullFileNamesLocal.add(fullFileNameLocal);
                        remoteFileNames.add(composePostFileNameRemote(fileName, date));
                        context.setVariable(_CONTEXT_KEY_POST_FILE_NAME, fullFileNameLocal);
                        file = new File(fullFileNameLocal);

                        if (!file.exists()) {
                            file.createNewFile();
                        }
            
                        out = new PrintWriter(new BufferedWriter(
                                    new FileWriter(file, true)), true);
                    }
                    logger.debug("...file NOT > 50mb");
                    out.print(str+"\r\n");
                    fileSize += str.length() + 2;
                    fileRecordsCount++;
                    logger.debug("***fileRecordsCount: " + fileRecordsCount);
                }
            } finally {  
                out.close();
            }
            recordsCount.put(fullFileNameLocal, fileRecordsCount);
            logger.debug("***fullFileNameLocal: " + fileRecordsCount);
            context.setVariable(_CONTEXT_KEY_POST_FILES_RECORD_COUNT, recordsCount);
            logger.debug("***_CONTEXT_KEY_POST_FILES_RECORD_COUNT: " + recordsCount);
            context.setVariable(_CONTEXT_KEY_POST_FILE_SIZE, fileSize);
            logger.debug("***_CONTEXT_KEY_POST_FILE_SIZE: " + fileSize);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, fullFileNamesLocal);
            logger.debug("***CONTEXT_KEY_POSTED_FILE_PATH_LOCAL: " + fullFileNamesLocal);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileNames);
            logger.debug("***CONTEXT_KEY_POSTED_FILE_NAME: " + remoteFileNames);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void writeFileTrailer(String filename,
        IRecordCollection recordCollection) {
        try {
            File file = new File(filename);

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(file, true)), true);

            try {
                for (Iterator i = recordCollection.iterator(); i.hasNext();) {
                    out.print(i.next().toString()+"\r\n");
                }
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
