package com.ftd.partnerreward.partner.jetblueairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.TextUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class ResponseTransformer implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());
    
    private String localErrorFilePath;
    
    public ResponseTransformer()
    {
    }
    
    private String getErrorFileName () {
        return localErrorFilePath + File.separator + "JETBLUE_ERROR_" + TextUtil.formatDate(new Date(), "yyyyMMdd_HH_mm_ss_")+".txt";
    }

    private static List errorCodes = new ArrayList();
    static {
        errorCodes.add("0001");
        errorCodes.add("0003");
        errorCodes.add("0004");
        errorCodes.add("0005");
        errorCodes.add("0007");
        errorCodes.add("0008");
        errorCodes.add("0009");
    }
    
  
    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);
            List responseFileTxtPathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            //CONTEXT_KEY_EXTRACTION_RESULT
            if (responseFileTxtPathLocal==null)
                responseFileTxtPathLocal = new LinkedList();
            
            // Rename the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                
                String filePath = (String) i.next();
                String processedFile = processFile(filePath, context);
                if (processedFile!=null)
                    responseFileTxtPathLocal.add(processedFile);
            }
          
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, responseFileTxtPathLocal);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
    
    private String processFile (String fileName, ContextInstance context) {
        String outFileName = null;
        try {
            File inFile = new File(fileName);
            logger.debug(
                "processFile: processFile called with filename: " +
                fileName);
            

            if (!inFile.exists()) {
                inFile.createNewFile();
            }
            
            outFileName = fileName + ".txt";
            File outFile = new File(outFileName);
            if (!outFile.exists()) {
                outFile.createNewFile();
            }
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFile)), true);

            // Read the contents of the existing file.
            LineNumberReader in = new LineNumberReader(new FileReader(inFile));
            List outRecordList = new ArrayList();

            try {
                String record = null;
                //read line and store all error records into the List
                while ((record = in.readLine()) != null) {
                    String rec = record.replaceAll("\n","").replaceAll("\r","");
                    if (rec.length()>0)
                        out.println(rec);
                    
                    String[] recs = rec.split("\\|");
                    //if record has corect format and error code in the list or record is not empy store it in error list
                    if ((recs.length==5 && errorCodes.contains(recs[3])) || (recs.length!=5 && rec.trim().length()>0))
                        outRecordList.add(rec.trim());

                    logger.debug(
                        "processFile: This is a READ body record: " +
                        record);
                }
            } finally {
                in.close();
                out.close();
            }
            //write error output into the file
            if (outRecordList.size()>0) {
                String errorFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT);
                if (errorFileName==null) {
                    errorFileName = getErrorFileName();
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACTION_RESULT, errorFileName);
                }
                PrintWriter errOut = null;
                try {
                  File file = new File(errorFileName);
                  if (!file.exists()) {
                      file.createNewFile();
                  }
                  errOut = new PrintWriter(new BufferedWriter(
                              new FileWriter(file)), true);
      
                  for (Iterator i = outRecordList.iterator(); i.hasNext();) {
                      String recLine = i.next().toString();
                      errOut.println(recLine);
                  }
                } finally {
                    if (errOut!=null)
                      errOut.close();
                }
                
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return outFileName;
    }
    
}
