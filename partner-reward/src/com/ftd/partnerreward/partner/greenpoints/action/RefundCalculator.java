package com.ftd.partnerreward.partner.greenpoints.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.action.StandardCalculator;
import com.ftd.partnerreward.core.bo.RewardServiceBO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class RefundCalculator implements ActionHandler {
    protected static final String _CONTEXT_KEY_REFUND_ACTIVE_DATE = "_keyRefundActiveDate";
    protected Logger logger = new Logger(RefundCalculator.class.getName());

    // These fields will be initialized by the process archive.
    //    protected Float bankBalanceThreshold;
    protected int dollarScale;
    protected String dollarRoundMethod;
    protected int rewardScale;
    protected String rewardRoundMethod;
    protected int historicalTimeSpanValue;
    protected String historicalTimeSpanUnit;

    public RefundCalculator() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            Date activeDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_ACTIVE_DATE);

            Calendar refundActiveCalendar = (Calendar) context.getVariable(_CONTEXT_KEY_REFUND_ACTIVE_DATE);

            // Calculate the start date for refund processing.
            if (refundActiveCalendar == null) {
                Calendar startCal = Calendar.getInstance();
                startCal.setTimeInMillis(endDate.getTime());

                int unit = 0;

                if (historicalTimeSpanUnit.equalsIgnoreCase("DAY")) {
                    unit = Calendar.DATE;
                } else if (historicalTimeSpanUnit.equalsIgnoreCase("MONTH")) {
                    unit = Calendar.MONTH;
                } else if (historicalTimeSpanUnit.equalsIgnoreCase("YEAR")) {
                    unit = Calendar.YEAR;
                }

                // In the past.
                startCal.add(unit, historicalTimeSpanValue * -1);

                refundActiveCalendar = startCal;

                // Bind the refund start date to the process context.
                executionContext.getContextInstance().setVariable(_CONTEXT_KEY_REFUND_ACTIVE_DATE,
                    refundActiveCalendar);
            }

            // Since the reward and refund processing may have different
            // date spans to process (IMPORTANT: refunds MUST have a shorter date span
            // in past history), determine if the processing active date is valid
            // for refund processing.
            while (!refundActiveCalendar.getTime().after(activeDate)) {
                // Translate the designated round methods.
                int dollarRoundMethodCode = StandardCalculator.translateRoundMethod(dollarRoundMethod);
                int rewardRoundMethodCode = StandardCalculator.translateRoundMethod(rewardRoundMethod);

                // Compose a list of refund VOs for the active date.
                List refundList = RewardServiceBO.getRewardRefund(partnerName,
                        activeDate, dollarScale, dollarRoundMethodCode,
                        rewardScale, rewardRoundMethodCode);

                if (refundList.size() > 0) {
                    rewardList.addAll(refundList);
                }

                // Increment to the next refund active date.
                refundActiveCalendar.add(Calendar.DATE, 1);
            }

            // Bind the current refund active date to the process context.
            executionContext.getContextInstance().setVariable(_CONTEXT_KEY_REFUND_ACTIVE_DATE,
                refundActiveCalendar);

            // Bind the reward list to the process context.
            executionContext.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);

            // Advance the process token.
            // Verify if the workflow must now end.
            Boolean endIndicator = (Boolean) executionContext.getContextInstance()
                                                             .getVariable(IWorkflowConstants.CONTEXT_KEY_END_IND);

            if ((endIndicator != null) && endIndicator.booleanValue()) {
                // End the workflow.
                tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_END;
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
