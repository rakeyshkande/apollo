package com.ftd.partnerreward.partner.americanairlines.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


public class Transformer implements ActionHandler {
    //    private static final String _DEFAULT_PST_CODE = "";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    private static String getPstCodeDefault(String companyId) {
        String pstCode = "FTD10";

        // Each company has a different default.
        if (companyId.equalsIgnoreCase(CompanyCode.FLOWERS_USA.toString())) {
            pstCode = "FU010";
        } else if (companyId.equalsIgnoreCase(
                    CompanyCode.BUTTERFIELD_BLOOMS.toString())) {
            pstCode = "BB010";
        } else if (companyId.equalsIgnoreCase(CompanyCode.GIFT_SENSE.toString())) {
            pstCode = "GS010";
        }

        return pstCode;
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            // Note this List is a shallow "copy" of the bound reference.
            List aggrRewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("######0");

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            for (int masterIndex = 0; masterIndex < aggrRewardList.size();
                    masterIndex++) {
                OrderDetailVO masterOdVO = (OrderDetailVO) aggrRewardList.get(masterIndex);
                String masterMembershipId = masterOdVO.getMembershipVO()
                                                      .getMembershipNumber();
                Date masterDeliveryDate = masterOdVO.getDeliveryDate();
                String masterPstCode = masterOdVO.getPstCode();
                String masterCompanyId = masterOdVO.getOrderHeaderVO()
                                                   .getCompanyId();

                if (masterPstCode == null) {
                    // Each company has a different default.
                    masterPstCode = Transformer.getPstCodeDefault(masterCompanyId);
                    masterOdVO.setPstCode(masterPstCode);
                }

                String masterBonusPstCode = masterOdVO.getPstCodeBonus();

                if (masterBonusPstCode == null) {
                    masterBonusPstCode = Transformer.getPstCodeDefault(masterCompanyId);
                    masterOdVO.setPstCodeBonus(masterBonusPstCode);
                }

                if ((masterMembershipId != null) &&
                        !ActionUtil.isDummyMembershipId(masterMembershipId)) {
                    // Cycle through the list comparing the current membership
                    // value for matches, for the same day.
                    for (int index = masterIndex + 1;
                            index < aggrRewardList.size(); index++) {
                        OrderDetailVO odVO = (OrderDetailVO) aggrRewardList.get(index);
                        String membershipId = odVO.getMembershipVO()
                                                  .getMembershipNumber();

                        if ((membershipId != null) &&
                                !ActionUtil.isDummyMembershipId(membershipId)) {
                            // If the membership ID matches, aggregate the reward values
                            // to the master (first) occurrence record.
                            if (masterMembershipId.equals(membershipId) &&
                                    (ActionUtil.setToZeroHour(
                                        masterDeliveryDate).getTime() == ActionUtil.setToZeroHour(
                                        odVO.getDeliveryDate()).getTime())) {
                                // Default the current record's PST codes.
                                String companyId = odVO.getOrderHeaderVO()
                                                       .getCompanyId();
                                String pstCode = odVO.getPstCode();

                                if (pstCode == null) {
                                    pstCode = Transformer.getPstCodeDefault(companyId);
                                    odVO.setPstCode(pstCode);
                                }

                                String pstCodeBonus = odVO.getPstCodeBonus();

                                if (pstCodeBonus == null) {
                                    pstCodeBonus = Transformer.getPstCodeDefault(companyId);
                                    odVO.setPstCodeBonus(pstCodeBonus);
                                }

                                // We have to aggregate BOTH the base and bonus PST codes
                                // into a single master record for all matches.
                                if (masterPstCode.equals(pstCode)) {
                                    masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                        odVO.getRewardBase());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBase(0);
                                }

                                if (masterPstCode.equals(pstCodeBonus)) {
                                    masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                        odVO.getRewardBonus());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBonus(0);
                                }

                                // Evaluate the bonus PST code.
                                if (masterBonusPstCode.equals(pstCode)) {
                                    masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                        odVO.getRewardBase());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBase(0);
                                }

                                if (masterBonusPstCode.equals(pstCodeBonus)) {
                                    masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                        odVO.getRewardBonus());

                                    // Zero the current record's value to prevent
                                    // double-counting.
                                    odVO.setRewardBonus(0);
                                }
                            }
                        }
                    } // end of FOR index LOOP 

                    // Create a transformation record.
                    // We must do two passes for the base and bonus points.
                    boolean evalBonus = false;
                    boolean evalBase = true;

                    do {
                        if (!evalBase) {
                            // set an indicator for this last second loop
                            // to evaluate the bonus metrics.
                            evalBonus = true;
                        }

                        if ((evalBase &&
                                ((masterOdVO.getRewardBase() > 0) ||
                                (masterOdVO.getRewardBaseCalc() > 0))) ||
                                (evalBonus &&
                                ((masterOdVO.getRewardBonus() > 0) ||
                                (masterOdVO.getRewardBonusCalc() > 0)))) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            String transactionType = "M";

                            String pstCode = masterPstCode;

                            if (evalBonus) {
                                pstCode = masterBonusPstCode;
                            }

                            logger.debug("execute: current PST code value is: " + pstCode);
                            if (pstCode.indexOf("*") != -1) {
                                // If the pst code is delimited with an asterisk character,
                                // split and obtain the transaction code.
                                StringTokenizer tokenizer = new StringTokenizer(pstCode,
                                        "*");

                                if (tokenizer.hasMoreTokens()) {
                                    pstCode = tokenizer.nextToken();
                                }

                                if (tokenizer.hasMoreTokens()) {
                                    transactionType = tokenizer.nextToken();
                                }
                                
                                logger.debug("execute: REVISED PST code value is: " + pstCode);
                            }

                            recordLine.addStringValue(transactionType, 1);
                            recordLine.addStringValue("FTD", 3);
                            recordLine.addStringValue(pstCode, 5);
                            recordLine.addStringValue(masterMembershipId, 7);
                            recordLine.addStringValue(masterOdVO.getMembershipVO()
                                                                .getLastName(),
                                20);
                            recordLine.addStringValue((masterOdVO.getMembershipVO()
                                                                 .getFirstName() == null)
                                ? " "
                                : masterOdVO.getMembershipVO().getFirstName()
                                            .substring(0, 1), 1);

                            recordLine.addStringValue(TextUtil.formatDate(
                                    masterOdVO.getDeliveryDate(), "yyyyMMdd"), 8);

                            float rewardTotal = 0;

                            if (evalBase) {
                                rewardTotal = masterOdVO.getRewardBaseCalc() +
                                    masterOdVO.getRewardBase();

                                if (masterPstCode.equals(masterBonusPstCode)) {
                                    // Since the base and bonus PST codes match
                                    // for the same record, combine all reward values.
                                    rewardTotal += (masterOdVO.getRewardBonusCalc() +
                                    masterOdVO.getRewardBonus());

                                    // Zero the bonus values so that they are ignored
                                    // for the next loop.
                                    masterOdVO.setRewardBonusCalc(0);
                                    masterOdVO.setRewardBonus(0);
                                }
                            } else {
                                rewardTotal = masterOdVO.getRewardBonusCalc() +
                                    masterOdVO.getRewardBonus();
                            }

                            recordLine.addNumberValue(df.format(
                                    new Float(rewardTotal).doubleValue()), 7);

                            // Filler
                            recordLine.addStringValue(" ", 16);
                            recordLine.addStringValue(" ", 5);
                            recordLine.addStringValue(" ", 12);

                            // Unique shared partner order identifier.
                            Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                                    PartnerSequenceType.RECORD);
                            recordLine.addStringValue(partnerRecordId.toString(),
                                10);
                            masterOdVO.setPostFileRecordId(partnerRecordId.toString());

                            // Filler
                            recordLine.addStringValue(" ", 15);

                            recordCollection.addRecordLine(recordLine);
                        }

                        // Since the first pass of prcessing of base point is
                        // complete, set the indicator for the second loop.
                        evalBase = false;
                    } while (!evalBonus);
                }
            } // end of FOR masterIndex LOOP

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Obtain a fresh copy from the context.
            // The resulting list of this processing will be used by StandardDbPersister.
            int rejectedRecordCount = 0;
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
