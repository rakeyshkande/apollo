package com.ftd.partnerreward.partner.americanairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;

import java.io.RandomAccessFile;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class ResponseTransformer implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());

    public ResponseTransformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                // Open the file for writing.
                RandomAccessFile pointer = new RandomAccessFile(filePath, "r");
                
                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                                '\t');

                try {
                    if (filePath.toUpperCase().indexOf("BILL") != -1) {
                        
                        ActionUtil.logInfo(logger, executionContext, "execute: Transforming " + filePath);
                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Partner ID");
                        header.addStringValue("Partner Service Term Code");
                        header.addStringValue("Member Number");
                        header.addStringValue("Member Last Name");
                        header.addStringValue("First Initial");
                        header.addStringValue("Activity Date");
                        header.addStringValue("Entry Source Code");
                        header.addStringValue("Transaction Type");
                        header.addStringValue(
                            "Partner Service Term Description");
                        header.addStringValue("Billing Type");
                        header.addStringValue("Total Miles");
                        
                        recordCollection.addRecordLine(header);

                        byte[] buffer = new byte[93];

                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 3));
                            recordLine.addStringValue(new String(buffer, 3, 5));
                            recordLine.addStringValue(new String(buffer, 8, 7));
                            recordLine.addStringValue(new String(buffer, 15, 20));
                            recordLine.addStringValue(new String(buffer, 35, 1));
                            // Activity Date
                            recordLine.addStringValue(new String(buffer, 36, 8));
                            // Entry Source Code
                            recordLine.addStringValue(new String(buffer, 53, 1));
                            recordLine.addStringValue(new String(buffer, 54, 1));
                            recordLine.addStringValue(new String(buffer, 55, 30));
                            recordLine.addStringValue(new String(buffer, 85, 1));
                            recordLine.addStringValue(new String(buffer, 86, 7));

                            recordCollection.addRecordLine(recordLine);

                            pointer.readLine();
                        }
                        
                    } else if (filePath.toUpperCase().indexOf("CONFIRMS") != -1) {

                        ActionUtil.logInfo(logger, executionContext, "execute: Transforming " + filePath);
                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Member ID");
                        header.addStringValue("Member Last Name");
                        header.addStringValue("First Initial");
                        header.addStringValue("Entry Source Code");
                        header.addStringValue("Transaction Type Code");
                        header.addStringValue("Activity Date");
                        header.addStringValue("Program ID");
                        header.addStringValue("Partner ID");
                        header.addStringValue("Partner Service Term Code");
                        header.addStringValue("Order Number");
                        header.addStringValue("Mileage Supplied");
                        header.addStringValue("Base Miles Posted");
                        header.addStringValue("Bonus Miles Posted");
                        header.addStringValue("Total Miles Posted");
                        header.addStringValue("Date of Posting");
                        header.addStringValue("Time of Posting");
                        header.addStringValue("Error Status");
                        header.addStringValue("Error Code");
                        
                        recordCollection.addRecordLine(header);

                        byte[] buffer = new byte[191];

                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 1));
                            recordLine.addStringValue(new String(buffer, 2, 7));
                            recordLine.addStringValue(new String(buffer, 9, 20));
                            recordLine.addStringValue(new String(buffer, 29, 1));
                            recordLine.addStringValue(new String(buffer, 30, 1));
                            recordLine.addStringValue(new String(buffer, 31, 1));
                            recordLine.addStringValue(new String(buffer, 32, 8));
                            recordLine.addStringValue(new String(buffer, 40, 3));
                            recordLine.addStringValue(new String(buffer, 43, 3));
                            recordLine.addStringValue(new String(buffer, 46, 5));
                            recordLine.addStringValue(new String(buffer, 104, 10));
                            recordLine.addStringValue(new String(buffer, 114, 9));
                            recordLine.addStringValue(new String(buffer, 123, 9));
                            recordLine.addStringValue(new String(buffer, 132, 9));
                            recordLine.addStringValue(new String(buffer, 141, 9));
                            recordLine.addStringValue(new String(buffer, 166, 8));
                            recordLine.addStringValue(new String(buffer, 174, 6));
                            recordLine.addStringValue(new String(buffer, 187, 1));
                            recordLine.addStringValue(new String(buffer, 188, 3));

                            recordCollection.addRecordLine(recordLine);

                            pointer.readLine();
                        }
                    }
                } finally {
                    pointer.close();
                }
                
                // Write to the final file.
                PersistServiceBO.writeFileBody(filePath + ".TXT", recordCollection, true);
                
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
