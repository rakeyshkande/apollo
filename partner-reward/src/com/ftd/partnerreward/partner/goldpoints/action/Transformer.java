package com.ftd.partnerreward.partner.goldpoints.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    private static final String _CONTEXT_KEY_BATCH_INDEX_COUNTER = "_keyBatchIndexCounter";
    private static final String _CONTEXT_KEY_RECORD_INDEX_COUNTER = "_keyRecordIndexCounter";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            Long batchIndexCounter = (Long) context.getVariable(_CONTEXT_KEY_BATCH_INDEX_COUNTER);
            Long recordIndexCounter = (Long) context.getVariable(_CONTEXT_KEY_RECORD_INDEX_COUNTER);

            if (batchIndexCounter == null) {
                batchIndexCounter = new Long(1);
            }

            if (recordIndexCounter == null) {
                recordIndexCounter = new Long(0);
            }

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            long dailyRewardTotal = 0;
            float dailyProductPriceTotal = 0;
            long dailyRecordCount = 0;
            Date previousEarnDate = null;
            String previousSourceCode = null;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    Date earnDate = odVO.getDeliveryDate();
                    String sourceCode = odVO.getSourceCode();
                    float reward = odVO.getRewardTotal();

                    // Eliminate the decimal from the product price.
                    float productPrice = odVO.getBillingMerchandiseOriginal() * 100;

                    // Write the summary record for the previous day-group 
                    // if this is a new day.
                    if ((previousEarnDate != null) &&
                            (earnDate.getTime() != previousEarnDate.getTime())) {
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        recordLine.addStringValue("2102", 4);
                        recordLine.addStringValue(TextUtil.formatDate(
                                previousEarnDate, "yyyyMMdd"), 8);
                        recordLine.addStringValue(TextUtil.formatDate(
                                postedFileDate, "yyyyMMddHHmmss"), 14);
                        recordLine.addStringValue("3047", 4);

                        recordLine.addStringValue(getStoreNumber(
                                previousSourceCode), 4);

                        recordLine.addNumberValue(String.valueOf(
                                dailyRecordCount), 6);
                        recordLine.addNumberValue(df.format(
                                new Float(dailyProductPriceTotal).doubleValue()),
                            12);
                        recordLine.addNumberValue(df.format(
                                new Float(dailyRewardTotal).doubleValue()), 14);
                        recordLine.addNumberValue("0", 14);
                        recordLine.addNumberValue("0", 14);
                        recordLine.addNumberValue("0", 14);

                        recordCollection.addRecordLine(recordLine);

                        // Reset the daily summations.
                        dailyRewardTotal = 0;
                        dailyProductPriceTotal = 0;
                        dailyRecordCount = 0;
                    }

                    // Detail line item.
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("3455", 4);

                    // Filler.
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue(" ", 20);
                    recordLine.addStringValue(" ", 2);

                    recordLine.addStringValue("+", 1);

                    // Show the total value as an integer.
                    recordLine.addNumberValue(df.format(
                            new Float(reward).doubleValue()), 8);
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue(TextUtil.formatDate(earnDate,
                            "yyyyMMdd"), 8);
                    recordLine.addStringValue("+", 1);
                    recordLine.addNumberValue(df.format(
                            new Float(productPrice).doubleValue()), 16);
                    recordLine.addStringValue(TextUtil.formatDate(
                            postedFileDate, "yyyyMMddHHmmss"), 14);
                    recordLine.addStringValue("3047", 4);
                    recordLine.addStringValue(getStoreNumber(sourceCode), 4);
                    recordLine.addStringValue("000", 3);
                    recordLine.addStringValue("000000000", 9);
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue(membershipId, 16);
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("0000000000", 10);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("00000000", 8);
                    recordLine.addStringValue("000000", 6);
                    recordLine.addStringValue("F", 1);
                    recordIndexCounter = new Long(recordIndexCounter.longValue() +
                            1);
                    recordLine.addNumberValue(recordIndexCounter.toString(), 6);

                    // The batch index is incremented only every 1000 records.
                    if ((recordIndexCounter.longValue() >= 1000) &&
                            ((recordIndexCounter.longValue() % 1000) == 0)) {
                        batchIndexCounter = new Long(batchIndexCounter.longValue() +
                                1);
                    }

                    recordLine.addNumberValue(batchIndexCounter.toString(), 12);

                    recordCollection.addRecordLine(recordLine);
                    dailyRecordCount++;
                    dailyRewardTotal += reward;
                    dailyProductPriceTotal += productPrice;

                    // Write the summary record if this is the last record in the group.
                    // The data extractions are performed on a complete per-day basis,
                    // so the last record denotes the last record for a day.
                    if (!i.hasNext()) {
                        IRecordLine summaryRecordLine = recordCollection.createLineInstance();
                        summaryRecordLine.addStringValue("2102", 4);
                        summaryRecordLine.addStringValue(TextUtil.formatDate(
                                earnDate, "yyyyMMdd"), 8);
                        summaryRecordLine.addStringValue(TextUtil.formatDate(
                                postedFileDate, "yyyyMMddHHmmss"), 14);
                        summaryRecordLine.addStringValue("3047", 4);

                        summaryRecordLine.addStringValue(getStoreNumber(
                                sourceCode), 4);

                        summaryRecordLine.addNumberValue(String.valueOf(
                                dailyRecordCount), 6);
                        summaryRecordLine.addNumberValue(df.format(
                                new Float(dailyProductPriceTotal).doubleValue()),
                            12);
                        summaryRecordLine.addNumberValue(df.format(
                                new Float(dailyRewardTotal).doubleValue()), 14);
                        summaryRecordLine.addNumberValue("0", 14);
                        summaryRecordLine.addNumberValue("0", 14);
                        summaryRecordLine.addNumberValue("0", 14);

                        recordCollection.addRecordLine(summaryRecordLine);

                        // Reset the daily summations.
                        dailyRewardTotal = 0;
                        dailyProductPriceTotal = 0;
                        dailyRecordCount = 0;
                    } else {
                        // Record current metrics for the next loop.
                        previousEarnDate = new Date(earnDate.getTime());
                        previousSourceCode = sourceCode;
                    }
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Bind the counters.
            context.setVariable(_CONTEXT_KEY_BATCH_INDEX_COUNTER,
                batchIndexCounter);
            context.setVariable(_CONTEXT_KEY_RECORD_INDEX_COUNTER,
                recordIndexCounter);

            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    protected static String getStoreNumber(String sourceCode) {
        // Determine the "store" number based on source code.
        String storeNumber = "0001";

        if (sourceCode.equals("4919")) {
            storeNumber = "0003";
        } else if (sourceCode.equals("4925")) {
            storeNumber = "0002";
        } else if (sourceCode.equals("4926")) {
            storeNumber = "0002";
        }

        return storeNumber;
    }
}
