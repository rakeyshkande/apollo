package com.ftd.partnerreward.partner.basshotels.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class ResponseTransformer implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());

    public ResponseTransformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                RandomAccessFile pointer = new RandomAccessFile(filePath,
                        "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');
                IRecordCollection summaryCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + filePath);

                    byte[] buffer = new byte[95];

                    if (pointer.read(buffer) != -1) {                        

                        // The first line in the source file is a header line.
                        IRecordLine sLine1 = summaryCollection.createLineInstance();
                        sLine1.addStringValue("Tape Date");
                        sLine1.addStringValue("Status ID");
                        sLine1.addStringValue("Transaction ID");
                        sLine1.addStringValue("Description");
                        sLine1.addStringValue("Program ID");
                        sLine1.addStringValue("Agent ID");
                        sLine1.addStringValue("File Sequence Number");
                        summaryCollection.addRecordLine(sLine1);
                        
                        IRecordLine sLine2 = summaryCollection.createLineInstance();
                        sLine2.addStringValue(new String(buffer, 9, 8));
                        sLine2.addStringValue(new String(buffer, 17, 3));
                        sLine2.addStringValue(new String(buffer, 20, 2));
                        sLine2.addStringValue(new String(buffer, 22, 30));
                        sLine2.addStringValue(new String(buffer, 52, 5));
                        sLine2.addStringValue(new String(buffer, 57, 20));
                        sLine2.addStringValue(new String(buffer, 77, 9));
                        summaryCollection.addRecordLine(sLine2);

                        // Go to the end of line.
                        pointer.readLine();
                        
                        IRecordLine header = recordCollection.createLineInstance();
                        header.addStringValue("Record Type");
                        header.addStringValue("Member Number");
                        header.addStringValue("Alliance Code");
                        header.addStringValue("First Name");
                        header.addStringValue("Last Name");
                        header.addStringValue("Start Date");
                        header.addStringValue("End Date");
                        header.addStringValue("Promotion ID");
                        header.addStringValue("Points Earned");
                        header.addStringValue("Source Agent");
                        header.addStringValue("Status");

                        recordCollection.addRecordLine(header);

                        while (pointer.read(buffer) != -1) {
                            IRecordLine recordLine = recordCollection.createLineInstance();
                            recordLine.addStringValue(new String(buffer, 0, 2));
                            recordLine.addStringValue(new String(buffer, 2, 9));
                            recordLine.addStringValue(new String(buffer, 11, 5));
                            recordLine.addStringValue(new String(buffer, 25, 1));
                            recordLine.addStringValue(new String(buffer, 26, 35));
                            recordLine.addStringValue(new String(buffer, 61, 8));
                            recordLine.addStringValue(new String(buffer, 69, 8));
                            recordLine.addStringValue(new String(buffer, 77, 6));
                            recordLine.addStringValue(new String(buffer, 83, 6));
                            recordLine.addStringValue(new String(buffer, 89, 5));
                            recordLine.addStringValue(new String(buffer, 94, 1));

                            recordCollection.addRecordLine(recordLine);

                            // Go to the end of line.
                            pointer.readLine();

                            // Skip the 1-line footer.
                            // Read two lines ahead.
                            long offset = pointer.getFilePointer();
                            pointer.readLine();

                            if (pointer.readLine() == null) {
                                // EOF is reached.
                                break;
                            } else {
                                // Restore the pointer to the previous location.
                                pointer.seek(offset);
                            }
                        }
                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = filePath + ".txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
                PersistServiceBO.writeFileTrailer(transformedFilePath,
                    summaryCollection);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
