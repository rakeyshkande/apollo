package com.ftd.partnerreward.partner.basshotels.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    String memberFirstName = odVO.getMembershipVO()
                                                 .getFirstName();
                    String memberLastName = odVO.getMembershipVO().getLastName();

                    if ((memberFirstName == null) || (memberLastName == null)) {
                        // Defect 368
                        // Exclude the record from the posting.
                        i.remove();
                        rejectedRecordCount++;

                        // Go to the next record.
                        continue;
                    }

                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("20", 2);
                    recordLine.addNumberValue(membershipId, 9);
                    recordLine.addStringValue("HPC", 5);
                    recordLine.addStringValue("000000000", 9);

                    recordLine.addStringValue(memberFirstName.substring(0, 1), 1);
                    recordLine.addStringValue(memberLastName, 35);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyyyMMdd"), 8);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyyyMMdd"), 8);

                    String pstCode = odVO.getPstCode();

                    if (pstCode == null) {
                        pstCode = "FTDBA";
                    }

                    recordLine.addStringValue(pstCode, 6);
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getRewardTotal()).doubleValue()), 6);
                    recordLine.addStringValue("0000000", 7);
                    recordLine.addStringValue("000", 3);
                    recordLine.addStringValue("000000000", 9);
                    recordLine.addStringValue("FTD", 5);
                    recordLine.addStringValue(" ", 16);
                    recordLine.addStringValue("000000", 6);
                    recordLine.addStringValue(" ", 5);
                    recordLine.addStringValue(" ", 10);
                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            if (rejectedRecordCount > 0) {
                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
