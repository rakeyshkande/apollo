package com.ftd.partnerreward.partner.marriott.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.TransformMetadata;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String pstCode = null;
        IRecordLine recordLine = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
            TransformMetadata transformMetadata = new TransformMetadata(recordCollection);

            // Format the reward amount.
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("#########0");

            int rejectedRecordCount = 0;

            // For each calculation result, transform into the recordCollection.
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();

                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the candidate from the list.
                    i.remove();
                    rejectedRecordCount++;

                    // re-loop
                    continue;
                }

                // Base and bonus points need to be treated as separate records so loop twice if necessary
                boolean doBasePoints  = false; 
                boolean doBonusPoints = false; 
                if (odVO.getRewardBase() > 0) {
                  doBasePoints = true;
                }
                if (odVO.getRewardBonus() > 0) {
                  doBonusPoints = true;
                }
                while (doBasePoints || doBonusPoints) {
                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("DET", 3);
                    recordLine.addStringValue(" ", 6);
                    recordLine.addNumberValue(membershipId, 9);
    
                    String lastName = odVO.getMembershipVO().getLastName();
    
                    if (lastName == null) {
                        lastName = odVO.getOrderHeaderVO().getCustomerLastName();
                    }
    
                    String firstName = odVO.getMembershipVO().getFirstName();
    
                    if (firstName == null) {
                        firstName = odVO.getOrderHeaderVO().getCustomerFirstName();
                    }
    
                    recordLine.addStringValue(lastName, 25);
                    recordLine.addStringValue(firstName, 25);
                    recordLine.addStringValue(" ", 15); // @todo Partner Account ID???
    
                    String reward;
                    if (doBasePoints) {
                      reward = df.format(new Float(odVO.getRewardBase()).doubleValue());
                      pstCode = odVO.getPstCode();
                      doBasePoints = false;   // base points done
                    } else {
                      reward = df.format(new Float(odVO.getRewardBonus()).doubleValue());
                      pstCode = odVO.getPstCodeBonus();
                      doBonusPoints = false;  // bonus points done
                    }
                    recordLine.addNumberValue(reward, 7);
    
                    // Record the reward for a running total.
                    transformMetadata.setPointsTotal(transformMetadata.getPointsTotal() +
                        Long.parseLong(reward));
    
                    recordLine.addStringValue("+", 1);
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyyyMMdd"), 8);
    
                    if (pstCode == null) {
                        // Company codes.
                        String company = odVO.getOrderHeaderVO().getCompanyId();
    
                        // Translate only certain codes.
                        if (company.equals("HIGH")) {
                            pstCode = "FTD3";
                        } else if (company.equals("GIFT")) {
                            pstCode = "FTD2";
                        } else if (company.equals("FUSA")) {
                            pstCode = "FTD4";
                        } else if (company.equals("FDIRECT")) {
                            pstCode = "FTD5";
                        } else if (company.equals("FLORIST")) {
                            pstCode = "FTD6";
                        } else {
                            pstCode = "FTD1";
                        }
                    }

                    recordLine.addStringValue(pstCode, 4);
                    recordLine.addStringValue(" ", 5);

                    recordCollection.addRecordLine(recordLine);
                }
            }

            // Bind the transformed data sets to the context.
            if (rejectedRecordCount > 0) {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }

            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                transformMetadata);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
