package com.ftd.partnerreward.partner.marriott.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;
import com.ftd.partnerreward.core.vo.TransformMetadata;

import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {
    private static final String _CONTEXT_KEY_FILE_RECORD_COUNT = "_keyFileRecordCount";
    private static final String _CONTEXT_KEY_FILE_POINT_COUNT = "_keyFilePointCount";
    protected static final String _CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID = "_keyPostedFileSequenceId";
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    protected String composePostFileNameRemote(String fileName, Date postDate) {
        return fileName + TextUtil.formatDate(postDate, "MMddyy");
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            TransformMetadata transformMetadata = (TransformMetadata) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            Long partnerFileId = (Long) context.getVariable(_CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            Integer fileRecordCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_RECORD_COUNT);
            Integer filePointCount = (Integer) context.getVariable(_CONTEXT_KEY_FILE_POINT_COUNT);

            // Initialize the file record count.
            int rollingFileRecordCount = 0;

            if (fileRecordCount != null) {
                rollingFileRecordCount = fileRecordCount.intValue();
            }

            // Record the rolling point count.
            int rollingFilePointCount = 0;

            if (filePointCount != null) {
                rollingFilePointCount = filePointCount.intValue();
            }

            // Initialize the remote file name.
            if (remoteFileName == null) {
                partnerFileId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                        PartnerSequenceType.FILE);

                context.setVariable(_CONTEXT_KEY_POSTED_FILE_SEQUENCE_ID,
                    partnerFileId);

                remoteFileName = composePostFileNameRemote(fileName,
                        postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    remoteFileName);
            }

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                        fileName, postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            // Write the reward records to the destination file.
            IRecordCollection transformResult = transformMetadata.getRecordCollection();

            if (transformResult.size() > 0) {
                PersistServiceBO.writeFileBody(fullFileNameLocal,
                    transformResult, true);

                // Record the number of records that have been placed in
                // the file.
                rollingFileRecordCount += transformResult.size();
                context.setVariable(_CONTEXT_KEY_FILE_RECORD_COUNT,
                    new Integer(rollingFileRecordCount));

                // Record the total points thus far.
                rollingFilePointCount += transformMetadata.getPointsTotal();
                context.setVariable(_CONTEXT_KEY_FILE_POINT_COUNT,
                    new Integer(rollingFilePointCount));

                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                // Compose the headers.
                IRecordCollection headerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

                IRecordLine headerLine = headerCollection.createLineInstance();
                headerLine.addStringValue("MCM", 3);
                headerLine.addNumberValue(partnerFileId.toString(), 5);
                headerLine.addStringValue(TextUtil.formatDate(postedFileDate,
                        "yyyyMMdd"));
                headerLine.addStringValue(" ", 94);

                headerCollection.addRecordLine(headerLine);
                PersistServiceBO.writeFileHeader(fullFileNameLocal,
                    headerCollection);

                // Compose the trailer.
                IRecordCollection trailerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                IRecordLine trailerLine = trailerCollection.createLineInstance();
                trailerLine.addStringValue("TRL", 3);
                trailerLine.addNumberValue(partnerFileId.toString(), 5);
                trailerLine.addNumberValue(String.valueOf(rollingFileRecordCount),
                    8);
                trailerLine.addStringValue(" ", 6);
                trailerLine.addNumberValue(String.valueOf(rollingFilePointCount),
                    10);
                trailerLine.addStringValue(" ", 78);

                trailerCollection.addRecordLine(trailerLine);
                PersistServiceBO.writeFileTrailer(fullFileNameLocal,
                    trailerCollection);

                ActionUtil.logInfo(logger, executionContext,
                    extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to file " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
