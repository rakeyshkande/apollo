package com.ftd.partnerreward.partner.northwestairlines.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);

            // For each calculation result, transform into the recordCollection.
            int rejectedRecordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("20", 2);
                    recordLine.addNumberValue(membershipId, 12, ' ');

                    String customerLastName = odVO.getMembershipVO()
                                                  .getLastName();

                    if (customerLastName == null) {
                        customerLastName = odVO.getOrderHeaderVO()
                                               .getCustomerLastName();
                    }

                    recordLine.addStringValue(customerLastName, 40);

                    String customerFirstName = odVO.getMembershipVO()
                                                   .getFirstName();

                    if (customerFirstName == null) {
                        customerFirstName = odVO.getOrderHeaderVO()
                                                .getCustomerFirstName();
                    }

                    recordLine.addStringValue(customerFirstName, 25);
                    recordLine.addStringValue("FTD", 3);
                    recordLine.addStringValue("BSE", 3);
                    recordLine.addStringValue("FTDBSE", 8);
                    recordLine.addStringValue("BASE", 5);
                    recordLine.addStringValue(" ", 5);
                    recordLine.addStringValue(" ", 5);
                    recordLine.addStringValue(" ", 10);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyyyMMdd"), 8);
                    recordLine.addStringValue(TextUtil.formatDate(
                            odVO.getDeliveryDate(), "yyyyMMdd"), 8);

                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                            PartnerSequenceType.RECORD);
                    recordLine.addStringValue(partnerRecordId.toString(), 19);
                    odVO.setPostFileRecordId(partnerRecordId.toString());

                    recordLine.addStringValue("001", 3);
                    recordLine.addStringValue(" ", 2);
                    recordLine.addStringValue("000000", 6);

                    // Show the total value as an integer.
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getRewardTotal()).doubleValue()), 9);
                    recordLine.addNumberValue(df.format(
                            new Float(odVO.getBillingMerchandise()).doubleValue()),
                        9);

                    recordLine.addStringValue("000000000", 9);
                    recordLine.addStringValue(" ", 159);

                    recordCollection.addRecordLine(recordLine);
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
