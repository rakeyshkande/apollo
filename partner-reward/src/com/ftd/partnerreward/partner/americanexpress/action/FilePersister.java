package com.ftd.partnerreward.partner.americanexpress.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;
import com.ftd.partnerreward.partner.americanexpress.action.PstFileMetadata;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.ftd.partnerreward.core.vo.TransformMetadata;


public class FilePersister implements ActionHandler {
    protected static final String _CONTEXT_KEY_PST_FILE_METADATA_MAP = "_keyPstFileMetadataMap";
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    protected static String composePostFileName(String fileName, String pstCode) {
        return fileName + pstCode;
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            Map pstCodeToTransformResultMap = (Map) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Map pstCodeToMetadataMap = (Map) context.getVariable(_CONTEXT_KEY_PST_FILE_METADATA_MAP);
            List fullFileNameLocalList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            List fileNameRemoteList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);

            if (pstCodeToMetadataMap == null) {
                pstCodeToMetadataMap = new HashMap();
            }

            if (fullFileNameLocalList == null) {
                fullFileNameLocalList = new ArrayList();
            }

            if (fileNameRemoteList == null) {
                fileNameRemoteList = new ArrayList();
            }

            // Write the reward records to the destination file.
            if (pstCodeToTransformResultMap.size() > 0) {
                // Iterate through the PstCode keys.
                for (Iterator i = pstCodeToTransformResultMap.keySet().iterator();
                        i.hasNext();) {
                    String pstCodeKey = (String) i.next();
                    TransformMetadata transformMetadata = (TransformMetadata)pstCodeToTransformResultMap.get(pstCodeKey);

                    // Obtain the existing collection of metadata.
                    PstFileMetadata pstFileMetadata = (PstFileMetadata) pstCodeToMetadataMap.get(pstCodeKey);

                    if (pstFileMetadata == null) {
                        String fullLocalFileName = ActionUtil.composePostFilePathLocal(filePath,
                                composePostFileName(fileName, pstCodeKey),
                                postedFileDate);

                        // Initialize a new metadata object.
                        pstFileMetadata = new PstFileMetadata(composePostFileName(
                                    fileName, pstCodeKey),
                                RewardServiceBO.getPartnerSequenceNextVal(
                                    partnerName, PartnerSequenceType.FILE),
                                fullLocalFileName);

                        pstCodeToMetadataMap.put(pstCodeKey, pstFileMetadata);

                        // Also maintain a list of the local file names.
                        fullFileNameLocalList.add(pstFileMetadata.getFullLocalFileName());

                        // Also, create a corresponding list of remote file names.
                        fileNameRemoteList.add(pstFileMetadata.getRemoteFileName());
                    }

                    // Record some stats.
                    pstFileMetadata.setRollingRecordCount(pstFileMetadata.getRollingRecordCount() +
                        transformMetadata.getRecordCollection().size());
                    pstFileMetadata.setRollingPointsCount(pstFileMetadata.getRollingPointsCount() +
                        transformMetadata.getPointsTotal());

                    PersistServiceBO.writeFileBody(pstFileMetadata.getFullLocalFileName(),
                        transformMetadata.getRecordCollection(), true);

                    ActionUtil.logInfo(logger, executionContext,
                        transformMetadata.getRecordCollection().size() +
                        " records have been successfully persisted to file " +
                        pstFileMetadata.getFullLocalFileName());
                } // end of FOR loop
            }

            // Lastly, bind the latest stats to the context.
            context.setVariable(_CONTEXT_KEY_PST_FILE_METADATA_MAP,
                pstCodeToMetadataMap);
            
            // List of local file names.
            // This will be used by any Standard Actions.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                fullFileNameLocalList);
                
            // List of remote file names.
            // This will be used by any Standard Actions.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                 fileNameRemoteList);

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                // Cycle through each PST file to create the headers and trailers.
                for (Iterator i = pstCodeToMetadataMap.keySet().iterator();
                        i.hasNext();) {
                    String pstCodeKey = (String) i.next();
                    PstFileMetadata pstFileMetadata = (PstFileMetadata) pstCodeToMetadataMap.get(pstCodeKey);

                    String fullLocalFileName = pstFileMetadata.getFullLocalFileName();
                    PersistServiceBO.writeFileHeader(fullLocalFileName,
                        composeHeader(pstCodeKey,
                            pstFileMetadata.getRemoteFileSequence(),
                            postedFileDate));

                    PersistServiceBO.writeFileTrailer(fullLocalFileName,
                        composeTrailer(
                            pstFileMetadata.getRollingPointsCount(),
                            pstFileMetadata.getRollingRecordCount()));

                    ActionUtil.logInfo(logger, executionContext,
                        extractCompleteInd.toString() +
                        " records representing all records across all days have been successfully persisted to file " +
                        fullLocalFileName);
                }
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    protected static IRecordCollection composeHeader(String pstCode,
        Long fileSequence, Date postedFileDate) {
        // Compose the header.
        IRecordCollection headerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
        IRecordLine headerLine = headerCollection.createLineInstance();
        headerLine.addStringValue("1", 1);
        headerLine.addNumberValue(fileSequence.toString(), 9);
        headerLine.addStringValue("7994100022", 10);
        headerLine.addStringValue(pstCode, 10);
        headerLine.addStringValue("840", 3);
        headerLine.addStringValue("USMR", 6);
        headerLine.addStringValue(TextUtil.formatDate(postedFileDate, "yyyyMMdd"),
            8);
        headerLine.addStringValue(" ", 53);

        headerCollection.addRecordLine(headerLine);

        return headerCollection;
    }

    protected static IRecordCollection composeTrailer(long totalPoints,
        long totalRecordCount) {
        // Compose the trailer.
        IRecordCollection trailerCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
        IRecordLine trailerLine = trailerCollection.createLineInstance();
        trailerLine.addStringValue("9", 1);
        trailerLine.addStringValue("+", 1);
        trailerLine.addNumberValue(String.valueOf(totalPoints), 12);
        trailerLine.addNumberValue(String.valueOf(totalRecordCount), 13);

        trailerCollection.addRecordLine(trailerLine);

        return trailerCollection;
    }
}
