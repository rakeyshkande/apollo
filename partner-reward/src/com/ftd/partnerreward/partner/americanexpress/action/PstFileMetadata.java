package com.ftd.partnerreward.partner.americanexpress.action;

import java.io.Serializable;


public class PstFileMetadata implements Serializable {
    private String remoteFileName;
    private Long remoteFileSequence;
    private long rollingRecordCount;
    private long rollingPointsCount;
    private String fullLocalFileName;

    public PstFileMetadata(String remoteFileName, Long remoteFileSequence,
        String fullLocalFileName) {
        this.remoteFileName = remoteFileName;
        this.remoteFileSequence = remoteFileSequence;
        this.fullLocalFileName = fullLocalFileName;
    }

    public String getRemoteFileName() {
        return remoteFileName;
    }

    public Long getRemoteFileSequence() {
        return remoteFileSequence;
    }

    public long getRollingRecordCount() {
        return rollingRecordCount;
    }

    public long getRollingPointsCount() {
        return rollingPointsCount;
    }

    public void setRollingPointsCount(long rollingPointsCount) {
        this.rollingPointsCount = rollingPointsCount;
    }

    public void setRollingRecordCount(long rollingRecordCount) {
        this.rollingRecordCount = rollingRecordCount;
    }

    public String getFullLocalFileName() {
        return fullLocalFileName;
    }
}
