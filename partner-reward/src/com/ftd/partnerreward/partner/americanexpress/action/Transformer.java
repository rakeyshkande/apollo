package com.ftd.partnerreward.partner.americanexpress.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.CompanyCode;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.TransformMetadata;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());
    protected String defaultPstCodeFtd;
    protected String defaultPstCodeButterfieldBlooms;
    protected String defaultPstCodeGiftSense;
    protected String defaultPstCodeFloristCom;

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);

            Map pstCodeToRecordCollection = new HashMap(rewardList.size());

            // Format the reward amount.
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            int rejectedRecordCount = 0;

            // For each calculation result, transform into the recordCollection.
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();

                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the candidate from the list.
                    i.remove();
                    rejectedRecordCount++;

                    // re-loop
                    continue;
                }

                // Based on the PST code, insert the record in a Map
                // that will be referenced during file persistence.
                String pstCode = odVO.getPstCode();
               
                if (pstCode == null) {
                    String companyId = odVO.getOrderHeaderVO().getCompanyId();

                    if (companyId.equalsIgnoreCase(
                                CompanyCode.BUTTERFIELD_BLOOMS.toString())) {
                        pstCode = defaultPstCodeButterfieldBlooms;
                    } else if (companyId.equalsIgnoreCase(
                                CompanyCode.GIFT_SENSE.toString())) {
                        pstCode = defaultPstCodeGiftSense;
                    } else if (companyId.equalsIgnoreCase(
                                CompanyCode.FLORIST_COM.toString())) {
                        pstCode = defaultPstCodeFloristCom;
                    } else {
                        pstCode = defaultPstCodeFtd;
                    }
                }
                
                TransformMetadata transformMetadata = (TransformMetadata) pstCodeToRecordCollection.get(pstCode);
                IRecordCollection recordCollection = null;

                if (transformMetadata == null) {
                    recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                    transformMetadata = new TransformMetadata(recordCollection);
                    pstCodeToRecordCollection.put(pstCode, transformMetadata);
                } else {
                    recordCollection = transformMetadata.getRecordCollection();
                }

                IRecordLine recordLine = recordCollection.createLineInstance();
                recordLine.addStringValue("2", 1);
                recordLine.addStringValue("7994100022", 10);

                recordLine.addStringValue(odVO.getMembershipVO()
                                              .getMembershipNumber(), 20);
                recordLine.addStringValue("+", 1);

                /*
                 * This was modified to pull just the base reward (up to limit),
                 * not including bonus reward (as commented line does). 
                 */
                String reward = df.format(new Float(odVO.getRewardBase()).doubleValue());
//              String reward = df.format(new Float(odVO.getRewardTotal()).doubleValue());
                recordLine.addNumberValue(reward, 10);

                // Record the reward for a running total.
                transformMetadata.setPointsTotal(transformMetadata.getPointsTotal() +
                    Long.parseLong(reward));

                recordLine.addStringValue(TextUtil.formatDate(
                        odVO.getDeliveryDate(), "yyyyMMdd"), 8);

                recordLine.addStringValue(" ", 50);

                recordCollection.addRecordLine(recordLine);
                
                /* 
                 * The following section was added to create a new output file
                 * for each bonus PST code.  It is the same logic as above, but
                 * is only executed if the bonus PST code exists.
                 */
                String pstCodeBonus = odVO.getPstCodeBonus();
                
                if(pstCodeBonus != null) {
                    TransformMetadata transformMetadataBonus = (TransformMetadata) pstCodeToRecordCollection.get(pstCodeBonus);
                    IRecordCollection recordCollectionBonus = null;
    
                    if (transformMetadataBonus == null) {
                        recordCollectionBonus = RecordCollectionFactory.getFixedWidthRecordCollection(true);
                        transformMetadataBonus = new TransformMetadata(recordCollectionBonus);
                        pstCodeToRecordCollection.put(pstCodeBonus, transformMetadataBonus);
                    } else {
                        recordCollectionBonus = transformMetadataBonus.getRecordCollection();
                    }
    
                    IRecordLine recordLineBonus = recordCollectionBonus.createLineInstance();
                    recordLineBonus.addStringValue("2", 1);
                    recordLineBonus.addStringValue("7994100022", 10);
                    
                    recordLineBonus.addStringValue(odVO.getMembershipVO()
                                               .getMembershipNumber(), 20);
                    recordLineBonus.addStringValue("+", 1);
    
                    String rewardBonus = df.format(new Float(odVO.getRewardBonus()).doubleValue());
                    recordLineBonus.addNumberValue(rewardBonus, 10);
                    
                    // Record the reward for a running total.
                    transformMetadataBonus.setPointsTotal(transformMetadataBonus.getPointsTotal() +
                     Long.parseLong(rewardBonus));
                    
                    recordLineBonus.addStringValue(TextUtil.formatDate(
                         odVO.getDeliveryDate(), "yyyyMMdd"), 8);
                    
                    recordLineBonus.addStringValue(" ", 50);
                    
                    recordCollectionBonus.addRecordLine(recordLineBonus);
                }
                /* End added section */
            }

            // Bind the transformed data sets to the context.
            if (rejectedRecordCount > 0) {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);
            }

            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                pstCodeToRecordCollection);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
