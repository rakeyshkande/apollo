package com.ftd.partnerreward.partner.bestwestern.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();

            // Note this List is a shallow "copy" of the bound reference.
            List aggrRewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(true,
                    '\t');

            for (int masterIndex = 0; masterIndex < aggrRewardList.size();
                    masterIndex++) {
                OrderDetailVO masterOdVO = (OrderDetailVO) aggrRewardList.get(masterIndex);
                String masterMembershipId = masterOdVO.getMembershipVO()
                                                      .getMembershipNumber();
                Date masterDeliveryDate = masterOdVO.getDeliveryDate();
                String masterPstCode = masterOdVO.getPstCode();

                if ((masterMembershipId != null) &&
                        !ActionUtil.isDummyMembershipId(masterMembershipId)) {
                    // Cycle through the list comparing the current membership
                    // value for matches, for the same day.
                    for (int index = masterIndex + 1;
                            index < aggrRewardList.size(); index++) {
                        OrderDetailVO odVO = (OrderDetailVO) aggrRewardList.get(index);
                        String membershipId = odVO.getMembershipVO()
                                                  .getMembershipNumber();

                        if ((membershipId != null) &&
                                !ActionUtil.isDummyMembershipId(membershipId)) {
                            // If the membership ID matches, aggregate the reward values
                            // to the master (first) occurrence record.
                            if (masterMembershipId.equals(membershipId) &&
                                    (ActionUtil.setToZeroHour(
                                        masterDeliveryDate).getTime() == ActionUtil.setToZeroHour(
                                        odVO.getDeliveryDate()).getTime())) {
                                // Set the derived calculations fields of the value object.
                                masterOdVO.setRewardBaseCalc(masterOdVO.getRewardBaseCalc() +
                                    odVO.getRewardBase());
                                masterOdVO.setRewardBonusCalc(masterOdVO.getRewardBonusCalc() +
                                    odVO.getRewardBonus());
//                                masterOdVO.setRewardMinimumCalc(masterOdVO.getRewardMinimumCalc() +
//                                    odVO.getRewardMinimum());

                                // Remove the matching record from the shallow copy.
                                aggrRewardList.remove(index);
                            }
                        }
                    } // end of FOR index LOOP 
                    
                    // Create a transformation record.
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("PNT");
                    recordLine.addStringValue("FTD");
                    recordLine.addStringValue(masterOdVO.getMembershipVO().getMembershipNumber());
                    recordLine.addStringValue(TextUtil.formatDate(
                            masterOdVO.getDeliveryDate(), "MM/dd/yy"));
                    recordLine.addStringValue(df.format(
                            new Float(masterOdVO.getRewardBaseCalc() > 0 ?
                            masterOdVO.getRewardBaseCalc() + masterOdVO.getRewardBase() : masterOdVO.getRewardBase()).doubleValue()));
                    recordLine.addStringValue(df.format(
                            new Float(masterOdVO.getRewardBonusCalc()  > 0 ?
                            masterOdVO.getRewardBonusCalc() + masterOdVO.getRewardBonus() : masterOdVO.getRewardBonus()).doubleValue()));
                    recordLine.addStringValue(masterOdVO.getMembershipVO().getLastName());
                    recordLine.addStringValue(masterOdVO.getMembershipVO()
                                                  .getFirstName());
                                                  
                    // Unique shared partner order identifier.
                    Long partnerRecordId = RewardServiceBO.getPartnerSequenceNextVal(partnerName,
                            PartnerSequenceType.RECORD);
                    recordLine.addStringValue(partnerRecordId.toString());
                    masterOdVO.setPostFileRecordId(partnerRecordId.toString());
                    recordLine.addStringValue("REQUESTED");

                    recordCollection.addRecordLine(recordLine);
                }
            } // end of FOR masterIndex LOOP

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Obtain a fresh copy from the context.
            // The resulting list of this processing will be used by StandardDbPersister.
            int rejectedRecordCount = 0;
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId == null) ||
                        ActionUtil.isDummyMembershipId(membershipId)) {
                    
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                    rejectedRecordCount++;
                }
            }

                // Based on behavior, it appears that the jBPM context-bound
                // object is retrieved "by value",
                // so we have to re-bind the latest rewardList collection.
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                    rewardList);

        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
