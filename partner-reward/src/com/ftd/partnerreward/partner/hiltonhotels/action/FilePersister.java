package com.ftd.partnerreward.partner.hiltonhotels.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class FilePersister implements ActionHandler {
    private static final String _CONTEXT_KEY_VALID_POST_FILE_NAME = "_keyValidPostFile";
    private static final String _CONTEXT_KEY_REJECT_POST_FILE_NAME = "_keyRejectPostFile";
    private static final String _CONTEXT_KEY_MEMBERSHIP_ID_TO_RECORD_MAP = "_keyMembershipIdToRecordMap";
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    protected String composePostFileNameRemote(String fileName, Date postDate) {
        return fileName + TextUtil.formatDate(postDate, "MMddyyyy") + ".TXT";
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            Long pointsRunningTotal = (Long) context.getVariable(Transformer._CONTEXT_KEY_POINTS_RUNNING_TOTAL);
            String tempString = null;
            
            // Initialize the remote file name
            if (remoteFileName == null ) {
                remoteFileName = composePostFileNameRemote(fileName, postedFileDate);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
            }

            // Initialize the local destination file name
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                    fileName, postedFileDate);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            ActionUtil.logInfo(logger, executionContext, "remoteFileName=" + remoteFileName);
            ActionUtil.logInfo(logger, executionContext, "fullFileNameLocal=" + fullFileNameLocal);

            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {
                PersistServiceBO.writeFileBody(fullFileNameLocal,
                    transformResult, true);

                ActionUtil.logInfo(logger, executionContext, transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            // The files are only composed when ALL records for all days
            // have been collected.
            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                
                // Compose the header for each file.
                IRecordCollection headerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, ' ');
                IRecordLine header = headerCollection.createLineInstance();
                String partnerCode = "1234567";
                String headerDate = TextUtil.formatDate(postedFileDate, "yyyyDDD");
                String headerTime = TextUtil.formatDate(postedFileDate, "HHmmss");
                tempString = "0" + TextUtil.padString(partnerCode, 7, ' ', false) + "HC" + headerDate + headerTime;
                header.addStringValue(TextUtil.padString(tempString, 325, ' ', false));
                headerCollection.addRecordLine(header);
                
                tempString = "1" + "T" + TextUtil.padString(partnerCode, 8, ' ', false) + headerDate;
                header = headerCollection.createLineInstance();
                header.addStringValue(TextUtil.padString(tempString, 325, ' ', false));
                headerCollection.addRecordLine(header);
                
                PersistServiceBO.writeFileHeader(fullFileNameLocal,
                    headerCollection);
                
                // Next persist the trailer records.
                PersistServiceBO.writeFileTrailer(fullFileNameLocal,
                    composeTrailer(pointsRunningTotal.longValue(),
                        transformResult.size()));

                ActionUtil.logInfo(logger, executionContext, extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }

    protected static IRecordCollection composeTrailer(long totalPoints,
        int totalRecordCount) {
        // Compose the trailer.
        IRecordCollection trailerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                ' ');
        IRecordLine trailerLine = trailerCollection.createLineInstance();
        String tempString = "9" + TextUtil.padString(String.valueOf(totalRecordCount), 9, '0', true) +
            TextUtil.padString(String.valueOf(totalPoints), 11, '0', true);
        trailerLine.addStringValue(TextUtil.padString(tempString, 325, ' ', false));
        trailerCollection.addRecordLine(trailerLine);

        IRecordLine trailerLine2 = trailerCollection.createLineInstance();
        tempString = "9" + TextUtil.padString(String.valueOf(totalRecordCount + 2), 9, '0', true);
        trailerLine2.addStringValue(TextUtil.padString(tempString, 325, ' ', false));
        trailerCollection.addRecordLine(trailerLine2);

        return trailerCollection;
    }
}
