package com.ftd.partnerreward.partner.hiltonhotels.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;
import java.io.RandomAccessFile;

import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class ResponseTransformer implements ActionHandler {
    protected Logger logger = new Logger(ResponseTransformer.class.getName());

    public ResponseTransformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            List responseFilePathLocal = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_RESPONSE_FILE_PATH_LOCAL);

            // Transform the given file(s)
            for (Iterator i = responseFilePathLocal.iterator(); i.hasNext();) {
                String filePath = (String) i.next();

                RandomAccessFile pointer = new RandomAccessFile(filePath,
                        "r");

                IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        '\t');

                try {
                    ActionUtil.logInfo(logger, executionContext,
                        "execute: Transforming " + filePath);

                    byte[] buffer = new byte[325];

                    while (pointer.read(buffer) != -1) {
                    
                        int recordType = Integer.parseInt( new String(buffer, 0, 1) );
                        
                        switch (recordType) {
                        
                            case 0:
                                break;
                                
                            case 2:
                                IRecordLine recordLine = recordCollection.createLineInstance();
                                recordLine.addStringValue(new String(buffer, 17, 9));
                                recordLine.addStringValue(new String(buffer, 26, 20));
                                recordLine.addStringValue(new String(buffer, 65, 7));
                                recordLine.addStringValue(new String(buffer, 72, 6));
                                recordLine.addStringValue(new String(buffer, 78, 5));
                                recordLine.addStringValue(new String(buffer, 83, 2));
                                recordLine.addStringValue(new String(buffer, 85, 9));
                                recordLine.addStringValue(new String(buffer, 94, 20));

                                recordCollection.addRecordLine(recordLine);
                                break;
                                
                            case 9:
                                String totalPoints = TextUtil.stripNonNumericChars(new String(buffer, 10, 11));
                                if (totalPoints.length() > 0) {
                                    IRecordLine recordLine2 = recordCollection.createLineInstance();
                                    int outPoints = Integer.parseInt(totalPoints);
                                    recordLine2.addStringValue("Total Points: " + String.valueOf(outPoints));
                                    recordCollection.addRecordLine(recordLine2);
                                    
                                    String recordCount = TextUtil.stripNonNumericChars(new String(buffer, 1, 9));
                                    if (recordCount.length() > 0) {
                                        IRecordLine recordLine3 = recordCollection.createLineInstance();
                                        int outRecordCount = Integer.parseInt(recordCount);
                                        recordLine3.addStringValue("Total Records: " + String.valueOf(outRecordCount));
                                        recordCollection.addRecordLine(recordLine3);
                                    }
                                }
                                
                                break;
                            
                            default:
                                break;

                        }

                        // Go to the end of line.
                        pointer.readLine();

                    }
                } finally {
                    pointer.close();
                }

                // Write to the final file.
                String transformedFilePath = filePath + ".txt";
                PersistServiceBO.writeFileBody(transformedFilePath,
                    recordCollection, true);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
