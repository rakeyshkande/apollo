package com.ftd.partnerreward.partner.hiltonhotels.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.text.DecimalFormat;

import java.util.Iterator;
import java.util.List;

public class Transformer implements ActionHandler {
    protected static final String _CONTEXT_KEY_POINTS_RUNNING_TOTAL = "_keyPointsRunningTotal";
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);

            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, ' ');

            // For each calculation result, transform into the recordCollection
            long totalPointsRaw = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("0000000");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO()
                                          .getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {
                    
                    String lastName = odVO.getMembershipVO().getLastName();
                    if (lastName == null) {
                        lastName = odVO.getOrderHeaderVO().getCustomerLastName();
                    }
                    String sourceCode = odVO.getSourceCode();
                    String pstCode = odVO.getPstCode();
                    String bonusPstCode = odVO.getPstCodeBonus();
                    float rewardBase = odVO.getRewardBase();
                    float rewardBonus = odVO.getRewardBonus();
                    String separateBonus = odVO.getBonusSeparateData();
                    
                    if (rewardBonus > 0 && separateBonus.equals(
                        IWorkflowConstants.SEPARATE_BONUS_DETAIL_LINE_VALUE)) {
                        
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        String temp = buildRecord(membershipId, lastName,
                            df.format(new Float(rewardBase).doubleValue()),
                            pstCode, sourceCode);
                        recordLine.addStringValue(TextUtil.padString(temp, 325, ' ', false));
                        recordCollection.addRecordLine(recordLine);
                        
                        if (bonusPstCode == null) bonusPstCode = pstCode;
                        
                        recordLine = recordCollection.createLineInstance();
                        temp = buildRecord(membershipId, lastName,
                            df.format(new Float(rewardBonus).doubleValue()),
                            bonusPstCode, sourceCode);
                        recordLine.addStringValue(TextUtil.padString(temp, 325, ' ', false));
                        recordCollection.addRecordLine(recordLine);

                    } else {
                    
                        float rewardTotal = rewardBase + rewardBonus;

                        IRecordLine recordLine = recordCollection.createLineInstance();
                        String temp = buildRecord(membershipId, lastName,
                            df.format(new Float(rewardTotal).doubleValue()),
                            pstCode, sourceCode);
                        recordLine.addStringValue(TextUtil.padString(temp, 325, ' ', false));
                        recordCollection.addRecordLine(recordLine);

                    }

                    totalPointsRaw += rewardBase + rewardBonus;

                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                }
            }

            // Bind the transformed data sets to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
            context.setVariable(_CONTEXT_KEY_POINTS_RUNNING_TOTAL,
                new Long(totalPointsRaw));

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
    
    private static String buildRecord(String membershipID, String lastName, String points,
                                      String pstCode, String sourceCode) {
        
        String result = "2";
        result += TextUtil.padString("", 16, ' ', false);
        result += TextUtil.padString(membershipID, 9, '0', true);
        result += TextUtil.padString(lastName, 20, ' ', false);
        result += TextUtil.padString("", 19, ' ', false);
        result += TextUtil.padString(points, 7, '0', true);
        result += TextUtil.padString(pstCode, 6, ' ', false);
        result += TextUtil.padString(sourceCode, 5, ' ', false);
        
        return result;
    }

}
