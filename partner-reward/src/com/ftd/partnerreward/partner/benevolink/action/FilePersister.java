package com.ftd.partnerreward.partner.benevolink.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;

import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class FilePersister implements ActionHandler {
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    protected String composePostFileNameRemote(String fileName, Date postDate) {
        return fileName + TextUtil.formatDate(postDate, "MMddyyyyHHmm");
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);
            Date postedFileDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            
            // Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = composePostFileNameRemote(fileName,
                        postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
                    remoteFileName);
            }

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = ActionUtil.composePostFilePathLocal(filePath,
                        fileName, postedFileDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }
            
            ActionUtil.logInfo(logger, executionContext,
                "remoteFileName=" + remoteFileName);
            ActionUtil.logInfo(logger, executionContext,
                "fullFileNameLocal=" + fullFileNameLocal);

            // Write the reward records to the destination file.
            if (transformResult.size() > 0) {
                PersistServiceBO.writeFileBody(fullFileNameLocal,
                    transformResult, true);

                ActionUtil.logInfo(logger, executionContext,
                    transformResult.size() +
                    " records have been successfully persisted to file " +
                    fullFileNameLocal);
            }

            // Determine if more data rows need to be extracted.
            Boolean extractCompleteInd = (Boolean) context.getVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND);

            if ((extractCompleteInd != null) &&
                    extractCompleteInd.booleanValue()) {
                // Compose the trailer.
                IRecordCollection headerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        ' ');

                IRecordLine headerLine = headerCollection.createLineInstance();
                headerLine.addStringValue("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                headerCollection.addRecordLine(headerLine);
                
                headerLine = headerCollection.createLineInstance();
                headerLine.addStringValue("<TransactionListing>");
                headerCollection.addRecordLine(headerLine);

                PersistServiceBO.writeFileHeader(fullFileNameLocal,
                    headerCollection);

                IRecordCollection trailerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                        ' ');

                IRecordLine trailerLine = trailerCollection.createLineInstance();
                trailerLine.addStringValue("</TransactionListing>");
                trailerCollection.addRecordLine(trailerLine);
                
                PersistServiceBO.writeFileTrailer(fullFileNameLocal,
                    trailerCollection);

                ActionUtil.logInfo(logger, executionContext,
                    extractCompleteInd.toString() +
                    " records representing all records across all days have been successfully persisted to file " +
                    fullFileNameLocal);
            }
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
