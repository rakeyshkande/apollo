package com.ftd.partnerreward.partner.benevolink.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.RewardServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.partnerreward.core.vo.PartnerSequenceType;

import java.text.DecimalFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class Transformer implements ActionHandler {
    protected Logger logger = new Logger(Transformer.class.getName());

    public Transformer() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            // Obtain the list of candidate order detail IDs from the context.
            ContextInstance context = executionContext.getContextInstance();
            List rewardList = (List) context.getVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT);
            String partnerName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            Date dateCreated = new Date();
            String partnerId = "384";

            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                    ' ');

            // For each calculation result, transform into the recordCollection.
            int recordCount = 0;
            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("############0.00");

            for (Iterator i = rewardList.iterator(); i.hasNext();) {
                OrderDetailVO odVO = (OrderDetailVO) i.next();
                String membershipId = odVO.getMembershipVO().getMembershipNumber();

                if ((membershipId != null) &&
                        !ActionUtil.isDummyMembershipId(membershipId)) {

                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<Transaction>");
                    recordCollection.addRecordLine(recordLine);

                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<PartnerID>" + partnerId + "</PartnerID>");
                    recordCollection.addRecordLine(recordLine);
                    
                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<DateCreated>" +
                        TextUtil.formatDate(dateCreated, "yyyy-MM-dd hh:mm:ss") +
                        "</DateCreated>");
                    recordCollection.addRecordLine(recordLine);

                    String externalOrderNumber = odVO.getExternalOrderNumber();
                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<TransactionID>" +
                        externalOrderNumber + "</TransactionID>");
                    recordCollection.addRecordLine(recordLine);

                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<ConsumerID>" +
                        membershipId + "</ConsumerID>");
                    recordCollection.addRecordLine(recordLine);

                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<TransactionDate>" +
                        TextUtil.formatDate(odVO.getDeliveryDate(), "yyyy-MM-dd hh:mm:ss") +
                        "</TransactionDate>");
                    recordCollection.addRecordLine(recordLine);

                    float merchandiseBilling = odVO.getBillingMerchandiseOriginal();
                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("<SubTotal>" +
                        df.format(new Float(merchandiseBilling).doubleValue()) +
                        "</SubTotal>");
                    recordCollection.addRecordLine(recordLine);
                    
                    recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue("</Transaction>");
                    recordCollection.addRecordLine(recordLine);
                    
                    recordCount++;
                    
                } else {
                    // Decrement from the total overall count.
                    ActionUtil.logInfo(logger, executionContext,
                        "Following order detail id was rejected in transformation due to invalid membership id: " +
                        odVO.getOrderDetailId(), odVO.getDeliveryDate());

                    // Remove the current calculation from the list of results.
                    i.remove();
                }
            }

            // Add record with empty values if none were posted
            if (recordCount == 0) 
            {

                IRecordLine recordLine = recordCollection.createLineInstance();
                recordLine.addStringValue("<Transaction>No transaction</Transaction>");
                recordCollection.addRecordLine(recordLine);

            }

            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);

            // Based on behavior, it appears that the jBPM context-bound
            // object is retrieved "by value",
            // so we have to re-bind the latest rewardList collection.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT,
                rewardList);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
