package com.ftd.reports.dao;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.dao.UtilDAO;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class OrderDAO {
    protected static Logger logger = new Logger(OrderDAO.class.getName());
    protected final static String DB_SELECT_ALLANT_ORDERS = "REPORTS.SELECT_ALLANT_ORDERS";
    protected final static String DB_SELECT_EMAIL_UNSUBSCRIBES = "REPORTS.SELECT_EMAIL_UNSUBSCRIBES";
    protected final static String DB_SELECT_PRODUCT_DATA_FEED = "REPORTS.SELECT_PRODUCT_DATA_FEED";
    protected final static String DB_SELECT_REFUND_BY_SOURCE_CODE = "REPORTS.SELECT_REFUND_BY_SOURCE_CODE";
    protected final static String DB_SELECT_ORDER_BY_SOURCE_TYPE = "REPORTS.SELECT_ORDER_BY_SOURCE_TYPE";
    protected final static String DB_SELECT_ORDER_BY_EXTENSION = "REPORTS.SELECT_ORDER_BY_EXTENSION";
    
    protected final static String DB_GET_PROD_FEED_CI_PRODUCTS = "REPORTS.GET_PROD_FEED_CI_PRODUCTS";
    protected final static String DB_GET_PROD_FEED_CI_UPSELLS = "REPORTS.GET_PROD_FEED_CI_UPSELLS";
    protected final static String DB_GET_DOMESTIC_SF_BY_SOURCE_CODE = "REPORTS.GET_DOMESTIC_SF_BY_SOURCE_CODE";
    protected final static String DB_GET_IOTWS_BY_SOURCE_CODE_AND_PRODUCT_ID = "REPORTS.GET_IOTWS_BY_SC_AND_PROD_ID";
    protected final static String DB_GET_DISCOUNTS_BY_SC_AND_PRODUCT_ID = "REPORTS.GET_DISCOUNTS_BY_SC_AND_PROD_ID";
    
    protected final static String DB_IN_START_DATE = "IN_START_DATE";
    protected final static String DB_IN_END_DATE = "IN_END_DATE";
    protected final static String DB_IN_SOURCE_CODE = "IN_SOURCE_CODE";
    protected final static String DB_IN_SOURCE_TYPE = "IN_SOURCE_TYPE";
    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
    protected final static String DB_IN_EXTENSION_NAME = "IN_EXTENSION_NAME";

    protected final static String DB_OUT_CUR = "OUT_CURSOR";
    protected final static String DB_GET_SUBCODE_DETAILS = "REPORTS.GET_SUBCODE_DETAILS";
    protected final static String DB_GET_ADDITIONAL_PRODUCT_INFO = "REPORTS.GET_ADDITIONAL_PRODUCT_INFO";

    /**
     * constructor to prevent clients from instantiating.
     */
    private OrderDAO() {
        super();
    }

    public static CachedResultSet queryAllantOrders(Date startDate, Date endDate) {
        // Submit the request to the database.
        Map inputParams = new HashMap(0);
        inputParams.put(DB_IN_START_DATE, new java.sql.Timestamp(startDate.getTime()));
        inputParams.put(DB_IN_END_DATE, new java.sql.Timestamp(endDate.getTime()));

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ALLANT_ORDERS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryAllantOrders: Could not obtain Allant orders from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
        
    public static CachedResultSet queryEmailUnsubscribes(Date startDate, Date endDate) {
        // Submit the request to the database.
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_START_DATE, new java.sql.Timestamp(startDate.getTime()));
        inputParams.put(DB_IN_END_DATE, new java.sql.Timestamp(endDate.getTime()));

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_EMAIL_UNSUBSCRIBES,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryEmailUnsubscribes: Could not obtain email unsubscribes from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet queryProductDataFeed() {

        CachedResultSet result = null;
        
        Map inputParams = new HashMap(0);
        inputParams.put("IN_PRODUCT_ID", null);
        inputParams.put("IN_NOVATOR_ID", null);
        inputParams.put("IN_SORT_BY", "product_id");
        inputParams.put("IN_START_POSITION", new Long(0));
        inputParams.put("IN_END_POSITION", new Long(Integer.MAX_VALUE));

        try {
            Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_PRODUCT_DATA_FEED,
                inputParams, false);
            result = (CachedResultSet) compositeResult.get(DB_OUT_CUR);
            
        } catch (Exception e) {
            String errorMsg = "queryProductDataFeed: Could not obtain product data from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
        
        return result;
    }
        
    
    public static CachedResultSet getSubcodeDetails(String productId) {

        Map inputParams = new HashMap(0);
        inputParams.put("IN_PRODUCT_ID", productId);

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_SUBCODE_DETAILS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "getSubcodeDetails: Could not retrieve from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet getAdditionalProductInfo(String productId) {
        Map inputParams = new HashMap(0);
        inputParams.put("IN_PRODUCT_ID", productId);

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_ADDITIONAL_PRODUCT_INFO,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "getAdditionalProductInfo: Could not retrieve from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
        
    public static CachedResultSet queryRefundBySourceCode(String sourceCode, Date startDate, Date endDate) {
        Map inputParams = new HashMap(3);
        inputParams.put(DB_IN_SOURCE_CODE, sourceCode);
        inputParams.put(DB_IN_START_DATE, new java.sql.Timestamp(startDate.getTime()));
        inputParams.put(DB_IN_END_DATE, new java.sql.Timestamp(endDate.getTime()));

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_REFUND_BY_SOURCE_CODE,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryRefundBySourceCode: Could not obtain refund from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }    
    
    public static CachedResultSet queryOrderBySourceType(String sourceType, Date startDate, Date endDate) {
        Map inputParams = new HashMap(3);
        inputParams.put(DB_IN_SOURCE_TYPE, sourceType);
        inputParams.put(DB_IN_START_DATE, new java.sql.Timestamp(startDate.getTime()));
        inputParams.put(DB_IN_END_DATE, new java.sql.Timestamp(endDate.getTime()));

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ORDER_BY_SOURCE_TYPE,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryOrderBySourceType: Could not obtain order from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }    
    
    public static CachedResultSet queryOrderByExtension(String extensionName, String partnerName, Date startDate, Date endDate) {
        Map inputParams = new HashMap(4);
        inputParams.put(DB_IN_EXTENSION_NAME, extensionName);
        inputParams.put(DB_IN_PARTNER_NAME, partnerName);
        inputParams.put(DB_IN_START_DATE, new java.sql.Timestamp(startDate.getTime()));
        inputParams.put(DB_IN_END_DATE, new java.sql.Timestamp(endDate.getTime()));

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ORDER_BY_EXTENSION, inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryOrderByExtension: Could not obtain order from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    } 
    
    public static CachedResultSet queryGetProdFeedCIProducts(String productId, String upsellID) {
        CachedResultSet result = null;
        
        Map inputParams = new HashMap(0);
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_UPSELL_ID", upsellID);
        
        try {
        	return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_PROD_FEED_CI_PRODUCTS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryGetProdFeedCIProducts: Could not obtain product data from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet queryGetProdFeedCIUpsells() {        
        Map inputParams = new HashMap(0);

        try {
        	return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_PROD_FEED_CI_UPSELLS,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryGetProdFeedCIUpsells: Could not obtain product data from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet getDomesticServiceFeeChargeBySourceCode(String sourceCode) {
        Map inputParams = new HashMap(0);
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_DOMESTIC_SF_BY_SOURCE_CODE,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "getDomesticServiceFeeChargeBySourceCode: Could not retrieve domestic service fee for source code: " + sourceCode;
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet getIOTWsBySourceCodeAndProductId(String sourceCode, String productId) {
        Map inputParams = new HashMap(0);
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        inputParams.put("IN_PRODUCT_ID", productId);

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_IOTWS_BY_SOURCE_CODE_AND_PRODUCT_ID,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "getIOTWsBySourceCodeAndProductId: Could not obtain IOTWs for source code " + sourceCode + " and product ID " + productId;
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }
    
    public static CachedResultSet getDiscountsBySCAndProductId(String sourceCode, String productId) {
        Map inputParams = new HashMap(0);
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        inputParams.put("IN_PRODUCT_ID", productId);

        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_DISCOUNTS_BY_SC_AND_PRODUCT_ID,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "getDiscountsBySCAndProductId: Could not obtain IOTWs for source code " + sourceCode + " and product ID " + productId;
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }

}
