package com.ftd.reports.dao;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.dao.UtilDAO;

import java.util.HashMap;
import java.util.Map;


public class FloristDAO {
    protected static Logger logger = new Logger(FloristDAO.class.getName());
    protected final static String DB_SELECT_FLORIST_ZIP_CODE_COUNT = "REPORTS.SELECT_FLORIST_ZIP_CODE_COUNT";

    protected final static String DB_OUT_CUR = "OUT_CUR";

    /**
     * constructor to prevent clients from instantiating.
     */
    private FloristDAO() {
        super();
    }

    public static CachedResultSet queryZipCodeCount() {
        // Submit the request to the database.
        Map inputParams = new HashMap(0);
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_FLORIST_ZIP_CODE_COUNT,
                inputParams, false);
        } catch (Exception e) {
            String errorMsg = "queryZipCodeCount: Could not obtain florist zip code count from database.";
            logger.error(errorMsg, e);
            throw new RuntimeException(errorMsg, e);
        }
    }

//    public static CachedResultSet queryOrderDetailIds(String partnerName,
//        Date activeDate, Date orderDateThreshold, int recordStart, int groupSize, List resultRecordCount) {
//        CachedResultSet result = null;
//        // Build db stored procedure input parms
//        Map inputParams = new HashMap(4);
//        inputParams.put(DB_IN_PARTNER_NAME, partnerName);
//        inputParams.put(DB_IN_DATE, new java.sql.Date(activeDate.getTime()));
//        inputParams.put(DB_IN_ORDER_DATE, new java.sql.Date(orderDateThreshold.getTime()));
//        inputParams.put(DB_IN_START_POSITION, new Integer(recordStart));
//        inputParams.put(DB_IN_MAX_NUMBER_RETURNED, new Integer(groupSize));
//
//        // Submit the request to the database.
//        try {
//            Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_ORDER_DETAIL_ID,
//                inputParams, false);
//
//                result = (CachedResultSet) compositeResult.get(DB_OUT_CURSOR);
//
//                // Record the total records returned.
//                // Verify that the total record count variable
//                // was populated.
//                resultRecordCount.add(0, new Integer(((BigDecimal) compositeResult.get(DB_OUT_MAX_ROWS)).intValue()));
//                
//        } catch (Exception e) {
//            String errorMsg = "queryOrderDetailIds: Could not obtain order detail IDs from database.";
//            logger.error(errorMsg, e);
//            throw new RuntimeException(errorMsg, e);
//        }
//        
//        return result;
//    }
}
