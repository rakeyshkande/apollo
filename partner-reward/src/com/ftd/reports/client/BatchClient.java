package com.ftd.reports.client;

import com.ftd.eventhandling.events.EventHandler;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import com.ftd.partnerreward.core.alert.AlertService;
import com.ftd.partnerreward.core.text.TextUtil;

import com.ftd.reports.ejb.ReportServiceEJBLocal;
import com.ftd.reports.ejb.ReportServiceEJBLocalHome;

import java.util.Date;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;


public class BatchClient extends EventHandler {
    protected Logger logger = new Logger(com.ftd.reports.client.BatchClient.class.getName());

    public BatchClient() {
        super();
    }

    public void invoke(Object payload) throws Throwable {
        try {
            // The CDATA of the payload is implemented as a MessageToken.
            MessageToken event = (MessageToken) payload;
            String message = (String) event.getMessage();

            // The message is pipe "|" delimited:
            // 1. partnerName (required)
            // 2. endDate in format MMddyyyy
            String reportName = null;
            Date startDate = null;
            Date endDate = null;
            StringTokenizer st = new StringTokenizer(message, "|");

            if (st.hasMoreTokens()) {
                reportName = st.nextToken();
            } else {
                throw new RuntimeException(
                    "invoke: A report name value is required.");
            }

            if (st.hasMoreTokens()) {
                startDate = TextUtil.parseString(st.nextToken(), "MMddyyyy");

                if (st.hasMoreTokens()) {
                    endDate = TextUtil.parseString(st.nextToken(), "MMddyyyy");
                }
            }

            // Obtain the EJB service interface.
            Context jndiContext = new InitialContext();

            try {
                ReportServiceEJBLocalHome home = (ReportServiceEJBLocalHome) jndiContext.lookup(
                        "java:comp/env/ejb/local/ReportServiceEJB");

                // create an EJB instance 
                ReportServiceEJBLocal rsEjb = home.create();

                // Execute the report.
                if (startDate == null) {
                    rsEjb.executeReport(reportName);
                } else {
                    rsEjb.executeReport(reportName, startDate, endDate);
                }
            } finally {
                jndiContext.close();
            }
        } catch (Throwable t) {
            logger.error("invoke: REPORT execution failed.", t);
            AlertService.processError(t);
        }
    }
}
