package com.ftd.reports.ejb;
import javax.ejb.EJBLocalObject;
import java.util.Date;

public interface ReportServiceEJBLocal extends EJBLocalObject 
{
  void executeReport(String reportName);

  void reTransmitReport(String reportName, String fullLocalFileName, String remoteFileName);

  void executeReport(String reportName, Date startDate, Date endDate);
}