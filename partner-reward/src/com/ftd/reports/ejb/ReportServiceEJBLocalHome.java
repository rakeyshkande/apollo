package com.ftd.reports.ejb;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface ReportServiceEJBLocalHome extends EJBLocalHome 
{
  ReportServiceEJBLocal create() throws CreateException;
}