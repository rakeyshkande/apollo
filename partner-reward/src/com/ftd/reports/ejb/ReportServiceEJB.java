package com.ftd.reports.ejb;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import java.util.Date;

public interface ReportServiceEJB extends EJBObject 
{
  void executeReport(String reportName) throws RemoteException;

  void reTransmitReport(String reportName, String fullLocalFileName, String remoteFileName) throws RemoteException;

  void executeReport(String reportName, Date startDate, Date endDate) throws RemoteException;
}