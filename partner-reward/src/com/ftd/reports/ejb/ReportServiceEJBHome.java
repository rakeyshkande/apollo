package com.ftd.reports.ejb;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface ReportServiceEJBHome extends EJBHome 
{
  ReportServiceEJB create() throws RemoteException, CreateException;
}