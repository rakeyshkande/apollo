package com.ftd.reports.ejb;
import com.ftd.reports.bo.ReportServiceBO;

import java.util.Date;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;


public class ReportServiceEJBean implements SessionBean {
    protected SessionContext ctx = null;

    public void ejbCreate() {
    }

    public void ejbActivate() {
    }

    public void ejbPassivate() {
    }

    public void ejbRemove() {
    }

    public void setSessionContext(SessionContext ctx) {
        this.ctx = ctx;
    }

    public void executeReport(String reportName) {
        ReportServiceBO.executeReport(reportName);
    }
    
    public void executeReport(String reportName, Date startDate, Date endDate) {
        ReportServiceBO.executeReport(reportName, startDate, endDate);
    }

    public void reTransmitReport(String reportName, String fullLocalFileName, String remoteFileName) {
        ReportServiceBO.reTransmitReport(reportName, fullLocalFileName, remoteFileName);
    }
}