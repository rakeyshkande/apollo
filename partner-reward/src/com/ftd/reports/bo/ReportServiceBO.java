package com.ftd.reports.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;

import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ExecutionContext;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;


public class ReportServiceBO {
    protected static Logger logger = new Logger(ReportServiceBO.class.getName());

    /**
     * Constructor to prevent instantiation of this class.
     */
    private ReportServiceBO() {
        super();
    }

    private static ProcessInstance initReport(String reportName) {
        String pdFileName = ReportServiceBO.getPdFileName(reportName);

        ProcessDefinition processDefinition = ProcessDefinition.parseXmlResource(pdFileName);

        ProcessInstance processInstance = new ProcessInstance(processDefinition);

        // Bind the report name to the context.
        processInstance.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_REPORT_NAME,
            reportName);

        // Bind the partner name to the context.
        processInstance.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME,
                    reportName); 

        return processInstance;
    }

    public static void executeReport(String reportName) {
        ProcessInstance processInstance = initReport(reportName);

        // Start the workflow from the beginning.
        processInstance.signal();
    }

    public static void executeReport(String reportName, Date startDate,
        Date endDate) {
        ProcessInstance processInstance = initReport(reportName);
        processInstance.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE,
            startDate);
        processInstance.getContextInstance().setVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE,
            endDate);

        // Start the workflow from the beginning.
        processInstance.signal();
    }

    public static void reTransmitReport(String reportName,
        String fullLocalFileName, String remoteFileName) {
        // Clean-up the partner name string.
        String pdFileName = ReportServiceBO.getPdFileName(reportName);
        ProcessDefinition processDefinition = ProcessDefinition.parseXmlResource(pdFileName);
        ProcessInstance processInstance = new ProcessInstance(processDefinition);
        ContextInstance context = processInstance.getContextInstance();

        // Bind the "end" indicator to the context.
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_END_IND,
            new Boolean(true));

        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
            remoteFileName);
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
            fullLocalFileName);

        // Start the workflow from the response file check node.
        Token token = processInstance.getRootToken();
        token.setNode(processInstance.getProcessDefinition().getNode("nodeTransport"));
        token.getNode().enter(new ExecutionContext(token));
    }

    private static String getPdFileName(String reportName) {
        String cleanReportName = TextUtil.cleanPartnerName(reportName);

        return "pd-rpt-" + cleanReportName + ".xml";
    }
}
