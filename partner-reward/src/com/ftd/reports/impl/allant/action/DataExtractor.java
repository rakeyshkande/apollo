package com.ftd.reports.impl.allant.action;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;

import java.io.File;
import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    
    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);

            DateFormat dateFormat = new SimpleDateFormat(
                    "MM-dd-yyyy HH:mm:ss:SSS");
            logger.info(
                "execute: Beginning Allant order extraction for dates from " +
                dateFormat.format(startDate) + " to " +
                dateFormat.format(endDate));

            // Transform into the needed format.
            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);

            Calendar activeDateSOD = Calendar.getInstance();
            activeDateSOD.setTimeInMillis(startDate.getTime());
            activeDateSOD.set(Calendar.HOUR_OF_DAY, 0);
            activeDateSOD.set(Calendar.MINUTE, 0);
            activeDateSOD.set(Calendar.SECOND, 0);
            activeDateSOD.set(Calendar.MILLISECOND, 0);

            // Since the order date within the result set may at times contain a timestamp,
            // we need to range from the zero-hour to the last second of the day.
            Calendar activeDateEOD = Calendar.getInstance();
            activeDateEOD.setTimeInMillis(activeDateSOD.getTime().getTime());
            activeDateEOD.add(Calendar.DATE, 1);
            activeDateEOD.add(Calendar.MILLISECOND, -1);

            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            while (activeDateSOD.getTimeInMillis() <= endDate.getTime()) {
                logger.info("execute: Querying Allant orders from " +
                    dateFormat.format(activeDateSOD.getTime()) + " to " +
                    dateFormat.format(activeDateEOD.getTime()));

                CachedResultSet report = OrderDAO.queryAllantOrders(activeDateSOD.getTime(),
                        activeDateEOD.getTime());

                logger.info("execute: Obtained result set rows: " + report.getRowCount());

                // Iterate through the result set, transferring the results
                // into a sorted Map because the business requires the final
                // results to be sorted by external order number.
                while (report.next()) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    String orderNumber = (String) report.getObject(1);
                    recordLine.addStringValue(orderNumber, 10);
                    recordLine.addStringValue(((BigDecimal) report.getObject(2)).setScale(
                            0, BigDecimal.ROUND_HALF_UP).toString(), 8);
                    recordLine.addStringValue(((BigDecimal) report.getObject(3)).setScale(
                            0, BigDecimal.ROUND_HALF_UP).toString(), 8);
                    recordLine.addStringValue((String) report.getObject(4), 15);
                    recordLine.addStringValue((String) report.getObject(5), 20);
                    recordLine.addStringValue((String) report.getObject(6), 30);
                    recordLine.addStringValue((String) report.getObject(7), 30);
                    recordLine.addStringValue((String) report.getObject(8), 30);
                    recordLine.addStringValue((String) report.getObject(9), 2);
                    recordLine.addStringValue((String) report.getObject(10), 10);
                    recordLine.addStringValue((String) report.getObject(11), 10);

                    Date orderDate = new Date(((Date) report.getObject(12)).getTime());
                    recordLine.addStringValue(TextUtil.formatDate(orderDate,
                            "MMddyyyy"), 8);
                    recordLine.addStringValue((String) report.getObject(13), 5);
                    recordLine.addStringValue((String) report.getObject(14), 20);
                    recordLine.addStringValue((String) report.getObject(15), 5);

                    // Remove any decimals.
                    BigDecimal retailPrice = (BigDecimal) report.getObject(16);

                    if (retailPrice == null) {
                        retailPrice = new BigDecimal(0);
                    }

                    recordLine.addNumberValue(df.format(
                            retailPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
                                       .doubleValue() * 100), 5, ' ');

                    // Remove any decimals.
                    BigDecimal orderTotal = (BigDecimal) report.getObject(17);

                    if (orderTotal == null) {
                        orderTotal = new BigDecimal(0);
                    }

                    recordLine.addNumberValue(df.format(
                            orderTotal.setScale(2, BigDecimal.ROUND_HALF_UP)
                                      .doubleValue() * 100), 5, ' ');

                    recordCollection.addRecordLine(recordLine);
                }

                logger.info(
                    "execute: Completed processing of current result set.");

                // Process the next day's orders.
                activeDateSOD.add(Calendar.DATE, 1);
                activeDateEOD.add(Calendar.DATE, 1);
            }

            
            // This step was eliminated since the recordCollection is so large
            // that the JVM struggles with the memory management.
            // Bind the transformed data set to the context.
//            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
//                recordCollection);
            
            // Start the file persistence logic.
            // This breaks with established workflow standards, but the
            // large data volume mandates this since we can not
            // bind to the context.
            persistToFile(executionContext, recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName + TextUtil.formatDate(postDate, "MMyy");
    }

    private void persistToFile(ExecutionContext executionContext, IRecordCollection transformResult) {

//        try {
            ContextInstance context = executionContext.getContextInstance();
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
//            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);

            //            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            // Initialize the remote file name.
            //            if (remoteFileName == null) {
            //                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
            //                    fileName);
            //            }
            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = DataExtractor.composePostFilePathLocal(filePath,
                        fileName, endDate == null ? new Date() : endDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            // Write the reward records to the destination file.
            PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                true);

            ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
//        } catch (Throwable t) {
//            ActionUtil.processError(logger, executionContext, t);
//            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
//        }
//
//        // Advance the process token.
//        if (tokenSignal != null) {
//            executionContext.getToken().signal(tokenSignal);
//        } else {
//            executionContext.getToken().signal();
//        }
    }
}
