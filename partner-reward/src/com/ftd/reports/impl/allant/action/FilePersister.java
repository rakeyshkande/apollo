package com.ftd.reports.impl.allant.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.TextUtil;

import com.ftd.reports.core.IWorkflowConstants;

import org.jbpm.context.exe.ContextInstance;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.File;

import java.util.Date;


public class FilePersister implements ActionHandler {
    protected Logger logger = new Logger(FilePersister.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    public FilePersister() {
    }

    private static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName + TextUtil.formatDate(postDate, "MMyy");
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            IRecordCollection transformResult = (IRecordCollection) context.getVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT);

            //            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            // Initialize the remote file name.
            //            if (remoteFileName == null) {
            //                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME,
            //                    fileName);
            //            }
            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = FilePersister.composePostFilePathLocal(filePath,
                        fileName, endDate == null ? new Date() : endDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }

            // Write the reward records to the destination file.
            PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                true);

            ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal != null) {
            executionContext.getToken().signal(tokenSignal);
        } else {
            executionContext.getToken().signal();
        }
    }
}
