package com.ftd.reports.impl.productfeed.vo;

import java.math.BigDecimal;
import java.util.Date;

public class ProductFeedVO
{
  protected String productId;
  protected String novatorId;
  protected String productName;
  protected String novatorName;
  protected String productStatus;
  protected String deliveryType;
  protected String category;
  protected String productType;
  protected String productSubType;
  protected String colorSizeFlag;
  protected BigDecimal standardPrice;
  protected BigDecimal deluxePrice;
  protected BigDecimal premiumPrice;
  protected BigDecimal variablePriceMax;
  protected String shortDescription;
  protected String longDescription;
  protected String floristReferenceNumber;
  protected String mercuryDescription;
  protected String itemComments;
  protected String addonBalloonsFlag;
  protected String addonBearsFlag;
  protected String addonCardsFlag;
  protected String addonFuneralFlag;
  protected String addonChocolateFlag;
  protected String codifiedFlag;
  protected String exceptionCode;
  protected Date exceptionStartDate;
  protected Date exceptionEndDate;
  protected String exceptionMessage;
  protected String secondChoiceCode;
  protected String discountAllowedFlag;
  protected String eGiftFlag;
  protected String countryId;
  protected String arrangementSize;
  protected String dominantFlowers;
  protected String searchPriority;
  protected String recipe;
  protected String subcodeFlag;
  protected String dimWeight;
  protected String nextDayUpgradeFlag;
  protected String corporateSite;
  protected String unspscCode;
  protected String priceRank1;
  protected String priceRank2;
  protected String priceRank3;
  protected String shipMethodCarrier;
  protected String shipMethodFlorist;
  protected String shippingKey;
  protected Date lastUpdate;
  protected String variablePriceFlag;
  protected String noTaxFlag;
  protected String generalComments;
  protected String holdUntilAvailable;
  protected String mondayDeliveryFreshcut;
  protected String webOEBlocked;
  protected String expressShippingOnly;

  protected String productColors;
  protected String excludedStates;
  protected String keywords;
  protected String excludedShipDays;
  protected String shipMethods;
  protected BigDecimal shippingCost;
  protected String recipientSearch;

  protected String buyURL;
  protected String imageURL;
  protected String importantProduct;
  protected String promotionalText;
  protected String shippingPromotionalText;

  public ProductFeedVO() {
  }
  
  public String getProductId() {
    return productId;
  }
  
  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getNovatorId() {
    return novatorId;
  }
  
  public void setNovatorId(String novatorId) {
    this.novatorId = novatorId;
  }

  public String getProductName() {
    return productName;
  }
  
  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getNovatorName() {
    return novatorName;
  }
  
  public void setNovatorName(String novatorName) {
    this.novatorName = novatorName;
  }

  public String getProductStatus() {
    return productStatus;
  }

  public void setProductStatus(String productStatus) {
    this.productStatus = productStatus;
  }

  public String getDeliveryType() {
    return deliveryType;
  }

  public void setDeliveryType(String deliveryType) {
    this.deliveryType = deliveryType;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public String getProductSubType() {
    return productSubType;
  }

  public void setProductSubType(String productSubType) {
    this.productSubType = productSubType;
  }

  public String getColorSizeFlag() {
    return colorSizeFlag;
  }

  public void setColorSizeFlag(String colorSizeFlag) {
    this.colorSizeFlag = colorSizeFlag;
  }

  public BigDecimal getStandardPrice() {
    return standardPrice;
  }

  public void setStandardPrice(BigDecimal standardPrice) {
    this.standardPrice = standardPrice;
  }

  public BigDecimal getDeluxePrice() {
    return deluxePrice;
  }

  public void setDeluxePrice(BigDecimal deluxePrice) {
    this.deluxePrice = deluxePrice;
  }

  public BigDecimal getPremiumPrice() {
    return premiumPrice;
  }

  public void setPremiumPrice(BigDecimal premiumPrice) {
    this.premiumPrice = premiumPrice;
  }

  public BigDecimal getVariablePriceMax() {
    return variablePriceMax;
  }

  public void setVariablePriceMax(BigDecimal variablePriceMax) {
    this.variablePriceMax = variablePriceMax;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public String getFloristReferenceNumber() {
    return floristReferenceNumber;
  }

  public void setFloristReferenceNumber(String floristReferenceNumber) {
    this.floristReferenceNumber = floristReferenceNumber;
  }

  public String getMercuryDescription() {
    return mercuryDescription;
  }

  public void setMercuryDescription(String mercuryDescription) {
    this.mercuryDescription = mercuryDescription;
  }

  public String getItemComments() {
    return itemComments;
  }

  public void setItemComments(String itemComments) {
    this.itemComments = itemComments;
  }

  public String getAddonBalloonsFlag() {
    return addonBalloonsFlag;
  }

  public void setAddonBalloonsFlag(String addonBalloonsFlag) {
    this.addonBalloonsFlag = addonBalloonsFlag;
  }

  public String getAddonBearsFlag() {
    return addonBearsFlag;
  }

  public void setAddonBearsFlag(String addonBearsFlag) {
    this.addonBearsFlag = addonBearsFlag;
  }

  public String getAddonCardsFlag() {
    return addonCardsFlag;
  }

  public void setAddonCardsFlag(String addonCardsFlag) {
    this.addonCardsFlag = addonCardsFlag;
  }

  public String getAddonFuneralFlag() {
    return addonFuneralFlag;
  }

  public void setAddonFuneralFlag(String addonFuneralFlag) {
    this.addonFuneralFlag = addonFuneralFlag;
  }

  public String getAddonChocolateFlag() {
    return addonChocolateFlag;
  }

  public void setAddonChocolateFlag(String addonChocolateFlag) {
    this.addonChocolateFlag = addonChocolateFlag;
  }

  public String getCodifiedFlag() {
    return codifiedFlag;
  }

  public void setCodifiedFlag(String codifiedFlag) {
    this.codifiedFlag = codifiedFlag;
  }

  public String getExceptionCode() {
    return exceptionCode;
  }

  public void setExceptionCode(String exceptionCode) {
    this.exceptionCode = exceptionCode;
  }

  public Date getExceptionStartDate() {
    return exceptionStartDate;
  }

  public void setExceptionStartDate(Date exceptionStartDate) {
    this.exceptionStartDate = exceptionStartDate;
  }

  public Date getExceptionEndDate() {
    return exceptionEndDate;
  }

  public void setExceptionEndDate(Date exceptionEndDate) {
    this.exceptionEndDate = exceptionEndDate;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public String getSecondChoiceCode() {
    return secondChoiceCode;
  }

  public void setSecondChoiceCode(String secondChoiceCode) {
    this.secondChoiceCode = secondChoiceCode;
  }

  public String getDiscountAllowedFlag() {
    return discountAllowedFlag;
  }

  public void setDiscountAllowedFlag(String discountAllowedFlag) {
    this.discountAllowedFlag = discountAllowedFlag;
  }

  public String getEGiftFlag() {
    return eGiftFlag;
  }

  public void setEGiftFlag(String eGiftFlag) {
    this.eGiftFlag = eGiftFlag;
  }

  public String getCountryId() {
    return countryId;
  }

  public void setCountryId(String countryId) {
    this.countryId = countryId;
  }

  public String getArrangementSize() {
    return arrangementSize;
  }

  public void setArrangementSize(String arrangementSize) {
    this.arrangementSize = arrangementSize;
  }

  public String getDominantFlowers() {
    return dominantFlowers;
  }

  public void setDominantFlowers(String dominantFlowers) {
    this.dominantFlowers = dominantFlowers;
  }

  public String getSearchPriority() {
    return searchPriority;
  }

  public void setSearchPriority(String searchPriority) {
    this.searchPriority = searchPriority;
  }

  public String getRecipe() {
    return recipe;
  }

  public void setRecipe(String recipe) {
    this.recipe = recipe;
  }

  public String getSubcodeFlag() {
    return subcodeFlag;
  }

  public void setSubcodeFlag(String subcodeFlag) {
    this.subcodeFlag = subcodeFlag;
  }

  public String getDimWeight() {
    return dimWeight;
  }

  public void setDimWeight(String dimWeight) {
    this.dimWeight = dimWeight;
  }

  public String getNextDayUpgradeFlag() {
    return nextDayUpgradeFlag;
  }

  public void setNextDayUpgradeFlag(String nextDayUpgradeFlag) {
    this.nextDayUpgradeFlag = nextDayUpgradeFlag;
  }

  public String getCorporateSite() {
    return corporateSite;
  }

  public void setCorporateSite(String corporateSite) {
    this.corporateSite = corporateSite;
  }

  public String getUnspscCode() {
    return unspscCode;
  }

  public void setUnspscCode(String unspscCode) {
    this.unspscCode = unspscCode;
  }

  public String getPriceRank1() {
    return priceRank1;
  }

  public void setPriceRank1(String priceRank1) {
    this.priceRank1 = priceRank1;
  }

  public String getPriceRank2() {
    return priceRank2;
  }

  public void setPriceRank2(String priceRank2) {
    this.priceRank2 = priceRank2;
  }

  public String getPriceRank3() {
    return priceRank3;
  }

  public void setPriceRank3(String priceRank3) {
    this.priceRank3 = priceRank3;
  }

  public String getShipMethodCarrier() {
    return shipMethodCarrier;
  }

  public void setShipMethodCarrier(String shipMethodCarrier) {
    this.shipMethodCarrier = shipMethodCarrier;
  }

  public String getShipMethodFlorist() {
    return shipMethodFlorist;
  }

  public void setShipMethodFlorist(String shipMethodFlorist) {
    this.shipMethodFlorist = shipMethodFlorist;
  }

  public String getShippingKey() {
    return shippingKey;
  }

  public void setShippingKey(String shippingKey) {
    this.shippingKey = shippingKey;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public String getVariablePriceFlag() {
    return variablePriceFlag;
  }

  public void setVariablePriceFlag(String variablePriceFlag) {
    this.variablePriceFlag = variablePriceFlag;
  }

  public String getNoTaxFlag() {
    return noTaxFlag;
  }

  public void setNoTaxFlag(String noTaxFlag) {
    this.noTaxFlag = noTaxFlag;
  }

  public String getGeneralComments() {
    return generalComments;
  }

  public void setGeneralComments(String generalComments) {
    this.generalComments = generalComments;
  }

  public String getHoldUntilAvailable() {
    return holdUntilAvailable;
  }

  public void setHoldUntilAvailable(String holdUntilAvailable) {
    this.holdUntilAvailable = holdUntilAvailable;
  }

  public String getMondayDeliveryFreshcut() {
    return mondayDeliveryFreshcut;
  }

  public void setMondayDeliveryFreshcut(String mondayDeliveryFreshcut) {
    this.mondayDeliveryFreshcut = mondayDeliveryFreshcut;
  }

  public String getWebOEBlocked() {
    return webOEBlocked;
  }

  public void setWebOEBlocked(String webOEBlocked) {
    this.webOEBlocked = webOEBlocked;
  }

  public String getExpressShippingOnly() {
    return expressShippingOnly;
  }

  public void setExpressShippingOnly(String expressShippingOnly) {
    this.expressShippingOnly = expressShippingOnly;
  }

  public String getProductColors() {
    return productColors;
  }

  public void setProductColors(String productColors) {
    this.productColors = productColors;
  }

  public String getExcludedStates() {
    return excludedStates;
  }

  public void setExcludedStates(String excludedStates) {
    this.excludedStates = excludedStates;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }

  public String getExcludedShipDays() {
    return excludedShipDays;
  }

  public void setExcludedShipDays(String excludedShipDays) {
    this.excludedShipDays = excludedShipDays;
  }

  public String getShipMethods() {
    return shipMethods;
  }

  public void setShipMethods(String shipMethods) {
    this.shipMethods = shipMethods;
  }

  public BigDecimal getShippingCost() {
    return shippingCost;
  }

  public void setShippingCost(BigDecimal shippingCost) {
    this.shippingCost = shippingCost;
  }

  public String getRecipientSearch() {
    return recipientSearch;
  }

  public void setRecipientSearch(String recipientSearch) {
    this.recipientSearch = recipientSearch;
  }

  public String getBuyURL() {
    return buyURL;
  }

  public void setBuyURL(String buyURL) {
    this.buyURL = buyURL;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  public String getImportantProductFlag() {
    return importantProduct;
  }

  public void setImportantProductFlag(String importantProduct) {
    this.importantProduct = importantProduct;
  }

  public String getPromotionalText() {
    return promotionalText;
  }

  public void setPromotionalText(String promotionalText) {
    this.promotionalText = promotionalText;
  }

  public String getShippingPromotionalText() {
    return shippingPromotionalText;
  }

  public void setShippingPromotionalText(String shippingPromotionalText) {
    this.shippingPromotionalText = shippingPromotionalText;
  }

}