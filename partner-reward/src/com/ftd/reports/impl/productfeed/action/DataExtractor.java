package com.ftd.reports.impl.productfeed.action;

import com.ftd.reports.impl.productfeed.vo.ProductFeedVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    
    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        PrintWriter out = null;

        try {
            ContextInstance context = executionContext.getContextInstance();

            logger.info("execute: Beginning Product feed extraction");

            // Transform into the needed format.
            //IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                    '\t');

            logger.info("execute: Querying Product Data Feed");

            CachedResultSet report = OrderDAO.queryProductDataFeed();

            logger.info("execute: Obtained result set rows: " + report.getRowCount());

            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = composePostFilePathLocal(filePath, fileName, new Date());
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
                logger.debug("fullFileNameLocal: " + fullFileNameLocal);
            }

            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            // Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = fileName;
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
                logger.debug("remoteFileName: " + remoteFileName);
            }

            File file = new File(fullFileNameLocal);
            if (!file.exists()) {
                file.createNewFile();
            }

            out = new PrintWriter(new BufferedWriter(
                new FileWriter(file, true)), true);

            buildHeader(recordCollection, out);

            // Iterate through the result set
            while (report.next()) {

                ProductFeedVO pfVO = new ProductFeedVO();

                String productId = report.getString("product_id");
                pfVO.setProductId(productId);
                String novatorId = report.getString("novator_id");
                if (novatorId == null) {
                  novatorId = productId;
                }
                pfVO.setNovatorId(novatorId);
                pfVO.setProductName(report.getString("product_name"));
                pfVO.setNovatorName(report.getString("novator_name"));
                String productStatus = report.getString("status");
                if (productStatus.equals("A")) {
                    pfVO.setProductStatus("Y");
                } else {
                    pfVO.setProductStatus("N");
                }
                pfVO.setDeliveryType(report.getString("delivery_type"));
                pfVO.setCategory(report.getString("category_description"));
                pfVO.setProductType(report.getString("product_type"));
                pfVO.setProductSubType(report.getString("product_sub_type"));
                pfVO.setColorSizeFlag(report.getString("color_size_flag"));
                pfVO.setStandardPrice( (BigDecimal) report.getObject("standard_price"));
                pfVO.setDeluxePrice( (BigDecimal) report.getObject("deluxe_price"));
                pfVO.setPremiumPrice( (BigDecimal) report.getObject("premium_price"));
                pfVO.setVariablePriceMax( (BigDecimal) report.getObject("variable_price_max"));
                pfVO.setShortDescription(report.getString("short_description"));
                pfVO.setLongDescription(report.getString("long_description"));
                pfVO.setFloristReferenceNumber(report.getString("florist_reference_number"));
                pfVO.setMercuryDescription(report.getString("mercury_description"));
                pfVO.setItemComments(report.getString("item_comments"));
                pfVO.setAddonBalloonsFlag(report.getString("add_on_balloons_flag"));
                pfVO.setAddonBearsFlag(report.getString("add_on_bears_flag"));
                pfVO.setAddonCardsFlag(report.getString("add_on_cards_flag"));
                pfVO.setAddonChocolateFlag(report.getString("add_on_chocolate_flag"));
                pfVO.setAddonFuneralFlag(report.getString("add_on_funeral_flag"));
                pfVO.setCodifiedFlag(report.getString("codification_id"));
                pfVO.setExceptionCode(report.getString("exception_code"));
                pfVO.setExceptionStartDate( (Date) report.getDate("exception_start_date"));
                pfVO.setExceptionEndDate( (Date) report.getDate("exception_end_date"));
                pfVO.setExceptionMessage(report.getString("exception_message"));
                pfVO.setSecondChoiceCode(report.getString("second_choice_code"));
                pfVO.setDiscountAllowedFlag(report.getString("discount_allowed_flag"));
                pfVO.setEGiftFlag(report.getString("egift_flag"));
                pfVO.setCountryId(report.getString("country_id"));
                pfVO.setArrangementSize(report.getString("arrangement_size"));
                pfVO.setDominantFlowers(report.getString("dominant_flowers"));
                pfVO.setSearchPriority(report.getString("search_priority"));
                pfVO.setRecipe(report.getString("recipe"));
                pfVO.setDimWeight(report.getString("dim_weight"));
                pfVO.setNextDayUpgradeFlag(report.getString("next_day_upgrade_flag"));
                pfVO.setCorporateSite(report.getString("corporate_site"));
                pfVO.setUnspscCode(report.getString("unspsc_code"));
                pfVO.setPriceRank1(report.getString("price_rank_1"));
                pfVO.setPriceRank2(report.getString("price_rank_2"));
                pfVO.setPriceRank3(report.getString("price_rank_3"));
                pfVO.setShipMethodCarrier(report.getString("ship_method_carrier"));
                pfVO.setShipMethodFlorist(report.getString("ship_method_florist"));
                pfVO.setShippingKey(report.getString("shipping_key"));
                pfVO.setLastUpdate( (Date) report.getDate("last_update"));
                pfVO.setVariablePriceFlag(report.getString("variable_price_flag"));
                pfVO.setNoTaxFlag(report.getString("no_tax_flag"));
                pfVO.setGeneralComments(report.getString("general_comments"));
                pfVO.setHoldUntilAvailable(report.getString("hold_until_available"));
                pfVO.setMondayDeliveryFreshcut(report.getString("monday_delivery_freshcut"));
                pfVO.setWebOEBlocked(report.getString("weboe_blocked"));
                pfVO.setExpressShippingOnly(report.getString("express_shipping_only"));
                
                pfVO.setBuyURL(report.getString("buy_url"));
                pfVO.setImageURL(report.getString("image_url"));
                pfVO.setImportantProductFlag(report.getString("important_product_flag"));
                pfVO.setPromotionalText(report.getString("promotional_text"));
                pfVO.setShippingPromotionalText(report.getString("shipping_promotional_text"));
                
                CachedResultSet additionalData = OrderDAO.getAdditionalProductInfo(productId);
                while (additionalData.next()) {
                    pfVO.setProductColors(additionalData.getString("product_colors"));
                    pfVO.setExcludedStates(additionalData.getString("excluded_states"));
                    pfVO.setKeywords(additionalData.getString("product_keywords"));
                    pfVO.setExcludedShipDays(additionalData.getString("excluded_ship_days"));
                    pfVO.setShipMethods(additionalData.getString("ship_methods"));
                    String shippingCost = additionalData.getString("ground_cost");
                    if (shippingCost == null || shippingCost.equals("")) {
                        shippingCost = additionalData.getString("two_day_cost");
                        if (shippingCost == null || shippingCost .equals("")) {
                            shippingCost = additionalData.getString("next_day_cost");
                        }
                    }
                    if (shippingCost == null || shippingCost.equals("")) {
                        pfVO.setShippingCost(null);
                    } else {
                        pfVO.setShippingCost( new BigDecimal(shippingCost));
                    }
                    pfVO.setRecipientSearch(additionalData.getString("recipient_search"));
                }


                String subcodeFlag = report.getString("subcode_flag");
                pfVO.setSubcodeFlag(subcodeFlag);
                if (subcodeFlag != null && subcodeFlag.equals("Y")) {
                    CachedResultSet subcodeData = OrderDAO.getSubcodeDetails(productId);
                    while (subcodeData.next()) {
                        pfVO.setProductId(subcodeData.getString("product_subcode_id"));
                        pfVO.setProductName(subcodeData.getString("subcode_description"));
                        pfVO.setStandardPrice( (BigDecimal) subcodeData.getObject("subcode_price"));
                        pfVO.setProductStatus(subcodeData.getString("active_flag"));
                        if (subcodeData.getString("subcode_reference_number") != null) {
                            pfVO.setFloristReferenceNumber(subcodeData.getString("subcode_reference_number"));
                        }
                        if (subcodeData.getString("dim_weight") != null) {
                            pfVO.setDimWeight(subcodeData.getString("dim_weight"));
                        }
                        
                        buildLine(pfVO, recordCollection, out);
                    }
                } else {
                    buildLine(pfVO, recordCollection, out);
                }

            }

            logger.info("execute: Completed processing of current result set.");

            // Bind the transformed data set to the context.
            //context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
            //    recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        } finally {
            out.close();
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }

    private static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName + "." + TextUtil.formatDate(postDate, "MMddyyHHmm");
    }

    private void buildLine(ProductFeedVO pfVO,
        IRecordCollection recordCollection,
        PrintWriter out) {

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("####0.00");

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue("FTD.COM");
        recordLine.addStringValue(pfVO.getProductId());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getNovatorId()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getProductName()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getShortDescription()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getLongDescription()));

        BigDecimal standardPrice = pfVO.getStandardPrice();
        if (standardPrice == null) {
            recordLine.addStringValue("0.00");
        } else {
            recordLine.addStringValue(df.format(standardPrice
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }
        if (standardPrice == null) {
            recordLine.addStringValue("0.00");
        } else {
            recordLine.addStringValue(df.format(standardPrice
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }

        recordLine.addStringValue(pfVO.getProductStatus());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getBuyURL()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getImageURL()));
        recordLine.addStringValue(pfVO.getDimWeight());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getCategory()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getKeywords()));
        recordLine.addStringValue("New");
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getImportantProductFlag()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getPromotionalText()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getShippingPromotionalText()));

        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getNovatorName()));
        recordLine.addStringValue(pfVO.getDeliveryType());
        recordLine.addStringValue(pfVO.getProductType());
        recordLine.addStringValue(pfVO.getProductSubType());
        recordLine.addStringValue(pfVO.getColorSizeFlag());

        BigDecimal deluxePrice = pfVO.getDeluxePrice();
        if (deluxePrice == null) {
            recordLine.addStringValue("");
        } else {
            recordLine.addStringValue(df.format(deluxePrice
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }
        BigDecimal premiumPrice = pfVO.getPremiumPrice();
        if (premiumPrice == null) {
            recordLine.addStringValue("");
        } else {
            recordLine.addStringValue(df.format(premiumPrice
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }
        recordLine.addStringValue(pfVO.getVariablePriceFlag());
        BigDecimal variablePriceMax = pfVO.getVariablePriceMax();
        if (variablePriceMax == null) {
            recordLine.addStringValue("");
        } else {
            recordLine.addStringValue(df.format(variablePriceMax
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getFloristReferenceNumber()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getMercuryDescription()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getItemComments()));
        recordLine.addStringValue(pfVO.getAddonBalloonsFlag());
        recordLine.addStringValue(pfVO.getAddonBearsFlag());
        recordLine.addStringValue(pfVO.getAddonCardsFlag());
        recordLine.addStringValue(pfVO.getAddonChocolateFlag());
        recordLine.addStringValue(pfVO.getAddonFuneralFlag());
        recordLine.addStringValue(pfVO.getCodifiedFlag());
        
        String exceptionCode = pfVO.getExceptionCode();
        if (exceptionCode != null &&
          (exceptionCode.equals("A") || exceptionCode.equals("U"))) {
            recordLine.addStringValue(exceptionCode);
            Date exceptionStartDate = pfVO.getExceptionStartDate();
            if (exceptionStartDate == null) {
                recordLine.addStringValue("");
            } else {
                recordLine.addStringValue(TextUtil.formatDate(exceptionStartDate, "MM/dd/yyyy"));
            }
            Date exceptionEndDate = pfVO.getExceptionEndDate();
            if (exceptionEndDate == null) {
                recordLine.addStringValue("");
            } else {
                recordLine.addStringValue(TextUtil.formatDate(exceptionEndDate, "MM/dd/yyyy"));
            }
            recordLine.addStringValue(TextUtil.stripTabs(pfVO.getExceptionMessage()));
        } else {
            recordLine.addStringValue("");
            recordLine.addStringValue("");
            recordLine.addStringValue("");
            recordLine.addStringValue("");
        }
        recordLine.addStringValue(pfVO.getSecondChoiceCode());
        recordLine.addStringValue(pfVO.getDiscountAllowedFlag());
        recordLine.addStringValue(pfVO.getEGiftFlag());
        recordLine.addStringValue(pfVO.getCountryId());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getArrangementSize()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getDominantFlowers()));
        recordLine.addStringValue(pfVO.getRecipientSearch());
        recordLine.addStringValue(pfVO.getSearchPriority());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getRecipe()));
        recordLine.addStringValue(pfVO.getSubcodeFlag());
        recordLine.addStringValue(pfVO.getNextDayUpgradeFlag());
        recordLine.addStringValue(pfVO.getCorporateSite());
        recordLine.addStringValue(pfVO.getUnspscCode());
        recordLine.addStringValue(pfVO.getPriceRank1());
        recordLine.addStringValue(pfVO.getPriceRank2());
        recordLine.addStringValue(pfVO.getPriceRank3());
        recordLine.addStringValue(pfVO.getShipMethodCarrier());
        recordLine.addStringValue(pfVO.getShipMethodFlorist());
        Date lastUpdate = pfVO.getLastUpdate();
        if (lastUpdate == null) {
            recordLine.addStringValue("");
        } else {
            recordLine.addStringValue(TextUtil.formatDate(lastUpdate, "MM/dd/yyyy"));
        }
        recordLine.addStringValue(pfVO.getNoTaxFlag());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getGeneralComments()));
        recordLine.addStringValue(pfVO.getHoldUntilAvailable());
        recordLine.addStringValue(pfVO.getMondayDeliveryFreshcut());
        recordLine.addStringValue(pfVO.getWebOEBlocked());
        recordLine.addStringValue(pfVO.getExpressShippingOnly());
        
        recordLine.addStringValue(pfVO.getProductColors());
        recordLine.addStringValue(pfVO.getExcludedStates());
        recordLine.addStringValue(pfVO.getExcludedShipDays());
        recordLine.addStringValue(pfVO.getShippingKey());
        recordLine.addStringValue(pfVO.getShipMethods());
        BigDecimal shippingCost = pfVO.getShippingCost();
        if (shippingCost == null) {
            recordLine.addStringValue("");
        } else {
            recordLine.addStringValue(df.format(shippingCost
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
        }

        //recordCollection.addRecordLine(recordLine);
        out.println(recordLine.toString());
    }

    private void buildHeader(IRecordCollection recordCollection, PrintWriter out) {

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue("Retailer Name");
        recordLine.addStringValue("Retailer SKU");
        recordLine.addStringValue("Manufacturer Model Number");
        recordLine.addStringValue("Product Name");
        recordLine.addStringValue("Product Short Description");
        recordLine.addStringValue("Product Long Description");
        recordLine.addStringValue("MSRP");
        recordLine.addStringValue("MAP Price");
        recordLine.addStringValue("Availability");
        recordLine.addStringValue("Buy URL");
        recordLine.addStringValue("Image URL");
        recordLine.addStringValue("Shipping Weight");
        recordLine.addStringValue("Retailer Category");
        recordLine.addStringValue("Keywords");
        recordLine.addStringValue("Condition");
        recordLine.addStringValue("Important Product");
        recordLine.addStringValue("Promotional Text");
        recordLine.addStringValue("Shipping Promotional Text");
        
        recordLine.addStringValue("Novator Name");
        recordLine.addStringValue("Domestic Flag");
        recordLine.addStringValue("Product Type");
        recordLine.addStringValue("Product Sub-Type");
        recordLine.addStringValue("Color/Size Flag");
        recordLine.addStringValue("Deluxe Price");
        recordLine.addStringValue("Premium Price");
        recordLine.addStringValue("Variable Price Flag");
        recordLine.addStringValue("Variable Price Max");
        recordLine.addStringValue("Florist Reference Number");
        recordLine.addStringValue("Mercury Description");
        recordLine.addStringValue("Item Comments");
        recordLine.addStringValue("Add-on Balloons Flag");
        recordLine.addStringValue("Add-on Bears Flag");
        recordLine.addStringValue("Add-on Greeting Cards Flag");
        recordLine.addStringValue("Add-on Chocolate Flag");
        recordLine.addStringValue("Add-on Funeral Flag");
        recordLine.addStringValue("Codified Flag");
        recordLine.addStringValue("Exception Code");
        recordLine.addStringValue("Exception Start Date");
        recordLine.addStringValue("Exception End Date");
        recordLine.addStringValue("Exception Message");
        recordLine.addStringValue("Second Choice Code");
        recordLine.addStringValue("Discount Allowed Flag");
        recordLine.addStringValue("E-Gift Flag");
        recordLine.addStringValue("Country Id");
        recordLine.addStringValue("Arrangement Size");
        recordLine.addStringValue("Dominant Flowers");
        recordLine.addStringValue("Recipient Search");
        recordLine.addStringValue("Search Priority");
        recordLine.addStringValue("Recipe");
        recordLine.addStringValue("Subcode Flag");
        recordLine.addStringValue("Next Day Upgrade Flag");
        recordLine.addStringValue("Corporate Site");
        recordLine.addStringValue("UNSPSC Code");
        recordLine.addStringValue("Price Rank 1");
        recordLine.addStringValue("Price Rank 2");
        recordLine.addStringValue("Price Rank 3");
        recordLine.addStringValue("Carrier Delivered Flag");
        recordLine.addStringValue("Florist Delivered Flag");
        recordLine.addStringValue("Last Update");
        recordLine.addStringValue("No Tax Flag");
        recordLine.addStringValue("General Comments");
        recordLine.addStringValue("Hold Until Available Flag");
        recordLine.addStringValue("Monday Delivered Freshcut Flag");
        recordLine.addStringValue("WebOE Blocked");
        recordLine.addStringValue("Express Shipping Only");

        recordLine.addStringValue("Product Colors");
        recordLine.addStringValue("Excluded States");
        recordLine.addStringValue("Excluded Shipping Days");
        recordLine.addStringValue("Shipping Key");
        recordLine.addStringValue("Ship Methods");
        recordLine.addStringValue("Shipping Cost");

        //recordCollection.addRecordLine(recordLine);
        out.println(recordLine.toString());
    }

}
