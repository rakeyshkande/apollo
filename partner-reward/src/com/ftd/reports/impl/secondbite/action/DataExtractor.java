package com.ftd.reports.impl.secondbite.action;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;

import java.io.File;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    
    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);

            DateFormat dateFormat = new SimpleDateFormat(
                    "MM-dd-yyyy HH:mm:ss");
            logger.info(
                "execute: Beginning Second Bite email unsubscribe extraction for dates from " +
                dateFormat.format(startDate) + " to " +
                dateFormat.format(endDate));

            // Transform into the needed format.
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, ',');

            Calendar activeDateSOD = Calendar.getInstance();
            activeDateSOD.setTimeInMillis(startDate.getTime());
            activeDateSOD.set(Calendar.HOUR_OF_DAY, 0);
            activeDateSOD.set(Calendar.MINUTE, 0);
            activeDateSOD.set(Calendar.SECOND, 0);
            activeDateSOD.set(Calendar.MILLISECOND, 0);

            // Since the order date within the result set may at times contain a timestamp,
            // we need to range from the zero-hour to the last second of the day.
            Calendar activeDateEOD = Calendar.getInstance();
            activeDateEOD.setTimeInMillis(activeDateSOD.getTime().getTime());
            activeDateEOD.add(Calendar.DATE, 1);
            activeDateEOD.add(Calendar.MILLISECOND, -1);

            DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
            df.applyPattern("####0");

            while (activeDateSOD.getTimeInMillis() <= endDate.getTime()) {
                logger.info("execute: Querying Second Bite orders from " +
                    dateFormat.format(activeDateSOD.getTime()) + " to " +
                    dateFormat.format(activeDateEOD.getTime()));

                CachedResultSet report = OrderDAO.queryEmailUnsubscribes(activeDateSOD.getTime(),
                        activeDateEOD.getTime());

                logger.info("execute: Obtained result set rows: " + report.getRowCount());

                // Iterate through the result set, transferring the results
                // into a sorted Map because the business requires the final
                // results to be sorted by external order number.
                while (report.next()) {
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    recordLine.addStringValue((String) report.getObject(1));
                    recordLine.addStringValue("Unsubscribe");
                    recordLine.addStringValue((String) report.getObject(2));

//                    Date unsubDate = new Date(((Date) report.getObject(2)).getTime());
//                    recordLine.addStringValue(TextUtil.formatDate(unsubDate,
//                            "MM/dd/yyyy HH:mm"));

                    recordCollection.addRecordLine(recordLine);
                }

                logger.info(
                    "execute: Completed processing of current result set.");

                // Process the next day's orders.
                activeDateSOD.add(Calendar.DATE, 1);
                activeDateEOD.add(Calendar.DATE, 1);
            }

            
            // This step was eliminated since the recordCollection is so large
            // that the JVM struggles with the memory management.
            // Bind the transformed data set to the context.
//            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
//                recordCollection);
            
            // Start the file persistence logic.
            // This breaks with established workflow standards, but the
            // large data volume mandates this since we can not
            // bind to the context.
            persistToFile(executionContext, recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private static String composePostFilePathLocal(String filePath,
        String fileName, Date postDate) {
        return filePath + File.separator + fileName + TextUtil.formatDate(postDate, "yyyyMMdd") + TextUtil.formatDate(new Date(), "_yyyyMMdd_HHmm");
    }

    private void persistToFile(ExecutionContext executionContext, IRecordCollection transformResult) {

            ContextInstance context = executionContext.getContextInstance();
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);

            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);

            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = DataExtractor.composePostFilePathLocal(filePath,
                        fileName, endDate == null ? new Date() : endDate);

                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
            }
            
        String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
        // Initialize the remote file name.
        if (remoteFileName == null) {
            remoteFileName = fileName + TextUtil.formatDate(endDate, "yyyyMMdd") + ".csv";
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
            logger.debug("remoteFileName: " + remoteFileName);
        }

            // Write the reward records to the destination file.
            PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                true);

            ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
    }
}
