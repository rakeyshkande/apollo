package com.ftd.reports.impl.mypoints.action;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.reports.dao.OrderDAO;

import java.io.File;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


/**
 * This class is responsible for creating a refund file for orders with the My Points source code.
 */
public class DataExtractor implements ActionHandler 
{

    protected Logger logger = new Logger(DataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String extensionName;
    
    private static final Integer RECORDS_PER_FILE = 1000000;

    public DataExtractor() 
    {
    }

    public void execute(ExecutionContext executionContext) 
    {
        String partnerName = null;
        String tokenSignal = null;
        ArrayList <String> fileNames = new ArrayList <String> ();
        
        try 
        {
            
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            List<OrderDetailVO> rewardList = new ArrayList <OrderDetailVO> ();    
            
            partnerName = (String)context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME);
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            logger.info(
                "execute: Beginning My Points rewards extraction for dates from " +
                dateFormat.format(startDate) + " to " +
                dateFormat.format(endDate) + " partner " + partnerName + " extension " + extensionName);
            

            CachedResultSet report = OrderDAO.queryOrderByExtension(extensionName, partnerName, startDate, endDate);
            logger.info("execute: Obtained result set rows: " + report.getRowCount());

            // Iterate through the result set, transferring the results
            // into a sorted Map because the business requires the final
            // results.
            if (report.getRowCount() != 0)
            {
                Integer numberOfRecordsWritten = 0;
                Integer numberOfRecordsToWriteInFile = 0;
                Integer currentFileNumber = 1;
                
                while (numberOfRecordsWritten < report.getRowCount())
                { 
                    IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,',');

                    if (report.getRowCount() - numberOfRecordsWritten > RECORDS_PER_FILE)
                    {
                        //There are more than the the allowed records per file left.  The count of records in the file
                        //will be the max number of records per file.
                        numberOfRecordsToWriteInFile = RECORDS_PER_FILE;
                    }
                    else
                    {
                        numberOfRecordsToWriteInFile = report.getRowCount() - numberOfRecordsWritten;
                    }
                    
                    ActionUtil.logInfo(logger, executionContext, "Writing: " + numberOfRecordsToWriteInFile + " to file number " + currentFileNumber);

                    
                    IRecordCollection headerCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,'\n');
                    IRecordLine headerLine = headerCollection.createLineInstance();
                    headerLine.addStringValue("TRANSACTIONS");
                    
                    headerLine.addStringValue("records=" + numberOfRecordsToWriteInFile);
                    headerLine.addStringValue("name=FTD");
                    headerLine.addStringValue("end-header");                    

                    headerCollection.addRecordLine(headerLine);
                 
                    for (int i = 0; i < numberOfRecordsToWriteInFile; i++)
                    {
                        report.next();
                                            
                        //Record format                        
                        //<VisitID>, <Date>, <Amount>,, <Tag>, <OrderID>,
                        
                        OrderDetailVO odVO = new OrderDetailVO();
                        odVO.setOrderDetailId(report.getString("ORDER_DETAIL_ID"));
                        // MYPPOINTS is not a real partner program. Save it nontheless.
                        odVO.setProgramName(partnerName);

                        // Delivery date is used to set as reward date on order_reward_posting for purchases.
                        // Use end date as the reward date for refunds.
                        odVO.setDeliveryDate(endDate);
                        
                        //The persister will error if this is not included
                        odVO.setSourceCode(report.getString("SOURCE_CODE"));


                        rewardList.add(odVO);
                        
                        IRecordLine recordLine = recordCollection.createLineInstance();
                        // Position 1: Visit id
                        recordLine.addStringValue(report.getString("INFO_VALUE"));
                        
                        // Position 2: Order Date
                        recordLine.addStringValue(dateFormat.format(report.getTimestamp("ORDER_DATE")));
                                 
                        // Position 3: Product Plus Addon Amount.
                        BigDecimal rewardAmount = null;
                        BigDecimal productAmount = report.getBigDecimal("PRODUCT_AMOUNT");
                        BigDecimal addonAmount = report.getBigDecimal("ADD_ON_AMOUNT");
                        
                        rewardAmount = productAmount.add(addonAmount);

                        recordLine.addStringValue(rewardAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        
                        // Position 4: Vuser ID - leave blank
                        recordLine.addStringValue("");
                         
                        // Position 5: Tag - this is always 'A'
                        recordLine.addStringValue("A");

                        // Position 6: Order Id - the external order number
                        recordLine.addStringValue(report.getString("EXTERNAL_ORDER_NUMBER"));

                        // Position 7: unique id - this is always blank
                        recordLine.addStringValue("");

                        recordCollection.addRecordLine(recordLine);
                    }
                    ActionUtil.logInfo(logger, executionContext, "execute: Completed processing of current result set.");
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, rewardList);
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE, endDate);
                    context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND, Boolean.valueOf(true));
                    String writtenFile = persistToFile(executionContext, headerCollection, recordCollection, currentFileNumber);
                    ActionUtil.logInfo(logger, executionContext, "data written to file " + writtenFile);
                    fileNames.add(writtenFile);
                    
                    numberOfRecordsWritten += numberOfRecordsToWriteInFile;
                    currentFileNumber++;
                }     
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, fileNames);
            }
            else
            {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, rewardList);
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND, Boolean.valueOf(true));
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL, "NoRecordsFound");
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_EMPTY_RESULT_SET, "Y");
                logger.info("No data recieved from query");
            }
            ActionUtil.logInfo(logger, executionContext, "Records representing all records across all days have been successfully persisted");        
        } 
        
        catch (Throwable t) 
        {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) 
        {
            executionContext.getToken().signal();
        }
        else 
        {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private String persistToFile(ExecutionContext executionContext, IRecordCollection headerCollection, IRecordCollection transformResult, 
                               Integer currentFileNumber)
    {

        ContextInstance context = executionContext.getContextInstance();
    
        // Initialize the local destination file name.
        String fullFileNameLocal = this.composePostFilePathLocal(filePath, currentFileNumber);
            
        // Write the reward records to the destination file.
        if (headerCollection != null)
        {
            PersistServiceBO.writeFileHeader(fullFileNameLocal, headerCollection);
        }
        if (transformResult != null)
        {
            PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult, true);
        }

        ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
        return fullFileNameLocal;
    }    
    
    private static String composePostFilePathLocal(String filePath, Integer fileNumber) 
    {
        //File format
        //ftdmypoints_mmddyyyy_nn.txt
        //When testing, this can be changed to
        //TESTFILE-ftdmypoints_mmddyyyy_nn.txt
        //So it will not be processed by mypoints

        return filePath + File.separator + "ftdmypoints" + TextUtil.formatDate(new Date(), "_MMddyyyy_") + fileNumber.toString() + ".txt";
    }    
}
