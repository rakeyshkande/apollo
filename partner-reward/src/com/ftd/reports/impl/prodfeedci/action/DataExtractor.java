package com.ftd.reports.impl.prodfeedci.action;

import com.ftd.reports.impl.prodfeedci.vo.ProductFeedVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());
    
    protected ArrayList<String> upsellChildrenArrayList;
    protected DecimalFormat df;
    
    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    
    protected static BigDecimal percentageMultipier;
    protected static BigDecimal domesticServiceFeeCharge;
    
    protected static final String FLORAL_PRODUCT_TYPE = "FLORAL";
    protected static final String SAME_DAY_GIFT_PRODUCT_TYPE = "SDG";
    protected static final String SAME_DAY_FRESH_CUTS_PRODUCT_TYPE = "SDFC";
    protected static final String FRESH_CUT_PRODUCT_TYPE = "FRECUT";
    protected static final String SERVICES_PRODUCT_TYPE = "SERVICES";
    protected static final String UPSELL_TYPE = "UPSELL";
    protected static final String PRODUCT_TYPE = "PRODUCT";
    //User Story#1401 - New global parms 
    protected static final String CONFIG_CONTEXT = "PARTNER_REWARD";
    protected static final String IMAGE_URL = "IMAGE_URL";
    protected static final String IMAGE_SIZE = "IMAGE_SIZE";
    protected static final String SITEWIDE_DISCOUNT_ALLOWED = "SITEWIDE_DISCOUNT_ALLOWED";
    protected static final String SITEWIDE_DISCOUNT_SOURCE_CODE = "SITEWIDE_DISCOUNT_SOURCE_CODE";
    
    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        PrintWriter out = null;

        upsellChildrenArrayList = new ArrayList<String>();
        
        percentageMultipier = new BigDecimal(0.01);

        df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("####0.00");      
       
        try {
            ContextInstance context = executionContext.getContextInstance();
         
            //User Story#1401 - config to read the global parm
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            logger.info("execute: Beginning Prod CI feed for extraction");
            
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
            '\t');

            logger.info("execute: Querying Prod CI Data Feed - Products");
            CachedResultSet prodFeedCIProducts = OrderDAO.queryGetProdFeedCIProducts(null, null);
            logger.info("execute: Obtained result set rows (prodFeedCIProducts): " + prodFeedCIProducts.getRowCount());
            
            logger.info("execute: Querying Prod CI Data Feed - Upsells");
            CachedResultSet prodFeedCIUpsells = OrderDAO.queryGetProdFeedCIUpsells();
            logger.info("execute: Obtained result set rows (prodFeedCIUpsells): " + prodFeedCIUpsells.getRowCount());
                        
            //Get Domestic Service Fee Charge for 350
            CachedResultSet defaultDomesticService = OrderDAO.getDomesticServiceFeeChargeBySourceCode("350");
            while (defaultDomesticService.next()) {
            	domesticServiceFeeCharge = (BigDecimal) defaultDomesticService.getObject("first_order_domestic");
            }
      
            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            //Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = composePostFilePathLocal(filePath, fileName, new Date());
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
                logger.debug("fullFileNameLocal: " + fullFileNameLocal);
            }
            
            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            //Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = fileName;
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
                logger.debug("remoteFileName: " + remoteFileName);
            }
            
            File file = new File(fullFileNameLocal);
            if (!file.exists()) {
                file.createNewFile();
            }
            else{
            	//Don't want to simply append new content to existing file, want a whole new one!
            	file.delete();
            }
            out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)), true);

            //Build the header
            buildHeader(recordCollection, out);
            
			// Iterate through the upsells
			while(prodFeedCIUpsells.next()){
				ProductFeedVO pfVO1 = new ProductFeedVO();
				
				//#1 SKU (Upsell Master ID)       
				String upsellMasterId = prodFeedCIUpsells.getString("upsell_master_id"); //SKU
				pfVO1.setSKU(upsellMasterId); //SKU
								
				//#2 PARENT_SKU (Upsell Master ID)
				pfVO1.setParentSKU(""); //PARENT_SKU
				
				//#6 NAME (Upsell Name)
				pfVO1.setName(prodFeedCIUpsells.getString("upsell_name")); //NAME 
				
	            //#7 DESCRIPTION (Description)
				pfVO1.setDescription(prodFeedCIUpsells.getString("upsell_description"));
				
				//#13 BUY_URL (URL that links to product)
				if(pfVO1.getName() != null && pfVO1.getSKU() != null){
					  String buyURL = createBuyURLforProductFeed(pfVO1.getName(), pfVO1.getSKU());
					  pfVO1.setBuyURL(buyURL);
				}

	            //#15 AVAILABILITY (Available or not)
	            String productStatus = prodFeedCIUpsells.getString("upsell_status");
	            pfVO1.setAvailability(productStatus);

	            //#17 LARGE_IMAGE_URL (URL that links to large product image)
	          //User Story#1401 - Get the image url and size from the global parm
	            String imageURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, 
                        IMAGE_URL);
	            String imageSize = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, 
                        IMAGE_SIZE);
	            //pfVO1.setLargeImgURL("http://www.ftdimg.com/pics/products/" + pfVO1.getSKU() + "_330x370.jpg");
	            pfVO1.setLargeImgURL(imageURL + pfVO1.getSKU() + imageSize);
				
	            //#26 KEYWORDS (Keyword Search)
	            CachedResultSet additionalData = OrderDAO.getAdditionalProductInfo(pfVO1.getSKU());
    			while(additionalData.next()){
		            String keywords = additionalData.getString("product_keywords");
	            	if(keywords != null && !keywords.isEmpty() && keywords.trim().length() != 0){
		            	 pfVO1.setKeywords(keywords.replaceAll(",", "|"));
		             }
	            	else{
	            		pfVO1.setKeywords("");
	            	}
    			}
	            	
    			
				ArrayList<BigDecimal> upsellStandardPricesList = new ArrayList<BigDecimal>();
				ArrayList<BigDecimal> upsellIOTWPricesList = new ArrayList<BigDecimal>();
				ArrayList<BigDecimal> upsellShippingPrices = new ArrayList<BigDecimal>();
				
				ArrayList<String> variationThemesList = new ArrayList<String>();
    			variationThemesList.add(upsellMasterId);   
				
    			String upsellTypeId = "";
    			String productId = "";
    			String upsellChildProductId = "";
    			//NOW it's that time of year that we get the upsell child products, fingers crossed
    			CachedResultSet upsellChildProducts = OrderDAO.queryGetProdFeedCIProducts(null, upsellMasterId);
    			while (upsellChildProducts != null && upsellChildProducts.next()) {	
    				int upsellChildSequence = upsellChildProducts.getInt("upsell_detail_sequence");
    				upsellTypeId = upsellChildProducts.getString("type_id");
    				productId = upsellChildProducts.getString("product_id");
    				upsellChildProductId = upsellChildProducts.getString("novator_id");
    				
		            //#30 VARIATION_THEMES (Master Sku | Child Sku | Child Sku | Child Sku or blank for regular products or upsell children)
    				if(upsellChildProductId != null){
    					variationThemesList.add(upsellChildProductId);
    				}
    				
    				//This represents the "base" product under the upsell, we'll use him to get all the base values
    				if(upsellChildSequence == 1){
    					//#43 SAMEDAY_FLAG (Y or N)
    		             upsellTypeId = upsellChildProducts.getString("type_id");
    		             if(upsellTypeId != null){
    		            	 if(upsellTypeId.equals(FLORAL_PRODUCT_TYPE) || upsellTypeId.equals(SAME_DAY_FRESH_CUTS_PRODUCT_TYPE) || upsellTypeId.equals(SAME_DAY_GIFT_PRODUCT_TYPE)){
    		            		 pfVO1.setSameDayFlag('Y');
    		            	 }
    		            	 else{
    		            		 pfVO1.setSameDayFlag('N');
    		            	 }
    		             }
    		             else{
    		            	 pfVO1.setSameDayFlag('N');
    		             }
    		             
    		             //#14 CATEGORY (Category|Type|Sub-type)
    		             String productCategoryDescription = upsellChildProducts.getString("description");
    		             String productTypeDescription = upsellChildProducts.getString("type_description");
    		             String productSubTypeDescription = upsellChildProducts.getString("sub_type_description");
    		             
    		             ArrayList<String> categoryArrayList = new ArrayList<String>();
    		             
    		             if(productCategoryDescription != null && !productCategoryDescription.isEmpty() && productCategoryDescription.trim().length() != 0){
    		            	 categoryArrayList.add(productCategoryDescription);
    		             }
    		             if(productTypeDescription != null && !productTypeDescription.isEmpty() && productTypeDescription.trim().length() != 0){
    		            	 categoryArrayList.add(productTypeDescription);
    		             }
    		             if(productSubTypeDescription != null && !productSubTypeDescription.isEmpty() && productSubTypeDescription.trim().length() != 0){
    		            	 categoryArrayList.add(productSubTypeDescription);
    		             }
    		             
    		             Iterator<String> categoryIter = categoryArrayList.iterator();
    		             StringBuilder categories = new StringBuilder();  //using builder cause its faster and this is only one thread of execution

    		             while (categoryIter.hasNext()) {
    		             	categories.append(categoryIter.next());
    		                 if (categoryIter.hasNext()) {
    		                 	categories.append('|');
    		                 }
    		             }
    		             pfVO1.setCategory(categories.toString());
    		             
    		             //#44 AVAILABILITY_DATES (MM/DD/YY - MM/DD/YY or blank)
    		             Date exceptionStartDate = (Date) upsellChildProducts.getDate("exception_start_date");
    		             Date exceptionEndDate = (Date) upsellChildProducts.getDate("exception_end_date");
    		             if(exceptionStartDate != null && exceptionEndDate != null){
    		            	 pfVO1.setAvailabilityDates(TextUtil.formatDate(exceptionStartDate, "MM/dd/yyyy") + " - " + TextUtil.formatDate(exceptionEndDate, "MM/dd/yyyy"));
    		             }
    		             else{
    		            	 pfVO1.setAvailabilityDates("");
    		             }
    		             
    		             //#31 ATTRIBUTE_NAMES (Included Colors | Size | Dominant Flowers) and #32 ATTRIBUTE_VALUES (example: Red|18H x 12W|roses;yellow)        
    		             ArrayList<String> attributeNameArrayList = new ArrayList<String>();
    		             ArrayList<String> attributeValuesArrayList = new ArrayList<String>();
    		             
    		             String arrangementSize = upsellChildProducts.getString("arrangement_size");
    		             String dominantColors = upsellChildProducts.getString("dominant_flowers");
    		             
    		             if(pfVO1.getProductColors() != null && !pfVO1.getProductColors().isEmpty() && pfVO1.getProductColors().trim().length() != 0){
    		            	 attributeNameArrayList.add("Included Colors");
    		            	 attributeValuesArrayList.add(pfVO1.getProductColors());
    		             }
    		             
    		             if(arrangementSize != null && !arrangementSize.isEmpty() && arrangementSize.trim().length() != 0){
    		            	 attributeNameArrayList.add("Size");
    		            	 attributeValuesArrayList.add(arrangementSize);
    		             }
    		             
    		             if(dominantColors != null && !dominantColors.isEmpty() && dominantColors.trim().length() != 0){
    		            	 attributeNameArrayList.add("Dominant Flowers");
    		            	 attributeValuesArrayList.add(dominantColors);
    		             }

    		             Iterator<String> attributeNameIter = attributeNameArrayList.iterator();
    		             Iterator<String> attributeValueIter = attributeValuesArrayList.iterator();
    		             
    		             StringBuilder attributeNames = new StringBuilder();  //using builder cause its faster and this is only one thread of execution
    		             StringBuilder attributeValues = new StringBuilder(); //using builder cause its faster and this is only one thread of execution
    		             
    		             while (attributeNameIter.hasNext()) {
    		            	 attributeNames.append(attributeNameIter.next());
    		                 if (attributeNameIter.hasNext()) {
    		                	 attributeNames.append('|');
    		                 }
    		             }
    		             
    		             while (attributeValueIter.hasNext()) {
    		            	 attributeValues.append(attributeValueIter.next());
    		                 if (attributeValueIter.hasNext()) {
    		                	 attributeValues.append('|');
    		                 }
    		             }
    		             pfVO1.setAttributeNames(attributeNames.toString());
    		             pfVO1.setAttributeValues(attributeValues.toString());
    				}
    				Iterator<String> variationThemesIter = variationThemesList.iterator();
    				StringBuilder variationThemes = new StringBuilder(); //using builder cause its faster and this is only one thread of execution
		            
    				while(variationThemesIter.hasNext()){
    					variationThemes.append(variationThemesIter.next());
		                 if (variationThemesIter.hasNext()) {
		                	 variationThemes.append('|');
		                 }
    				}
    				pfVO1.setVariationThemes(variationThemes.toString());
    				
    				//#9 REGULAR_PRICE (Lowest price) & #10 SALE_PRICE (Lowest discounted price)
    				BigDecimal upsellChildStandardPrice = (BigDecimal) upsellChildProducts.getObject("standard_price");
            		if(upsellChildStandardPrice != null){
            			upsellStandardPricesList.add(upsellChildStandardPrice);
            		}
            		
            		            		
            		//#10 SALE_PRICE (Lowest discounted price)
            		//User Story 1401:Enhancements to Channel Advisor Feed
            		 
            		String discountFlag = upsellChildProducts.getString("DISCOUNT_ALLOWED_FLAG");
            		String siteWideDiscountAllowed = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, SITEWIDE_DISCOUNT_ALLOWED);
               	    String siteWideDiscountSourceCode = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, SITEWIDE_DISCOUNT_SOURCE_CODE);
               	    
            		if("Y".equalsIgnoreCase(discountFlag))
            		{
   	             	String iotwSourceCode = upsellChildProducts.getString("iotw_source_code");
   	             	if(iotwSourceCode != null){
						
						BigDecimal discountAmt = null;
						String discountType = null;
	   	            	 //Have to call this method because IOTWs can have variable pricing.  This call returns the correct one based on product price.
						//User Story#1401 - change the default 350 code to global parm 
	   	                 CachedResultSet iotwSourceCodeData = OrderDAO.getIOTWsBySourceCodeAndProductId(siteWideDiscountSourceCode, productId); 
	   	                 if(iotwSourceCodeData != null){
	   		                 while (iotwSourceCodeData.next()) {
	   		                	 discountAmt = (BigDecimal) iotwSourceCodeData.getObject("discount_amt");
	   		                	 discountType = iotwSourceCodeData.getString("discount_type");
	   		                 }
	   	                 }
	   	            	 //For IOTW source codes that are 'D' for dollar discounts
	               		 if(discountType != null && discountType.equals("D")){
	               			 if(discountAmt != null){
	   	            			upsellIOTWPricesList.add(upsellChildStandardPrice.subtract(discountAmt)); //SALE_PRICE
	               			 }
	               		 }
	               		 //For IOTW source codes that are 'P' for percentage discounts
	               		 else if (discountType != null && discountType.equals("P")){
	               			 if(discountAmt != null){
	               				 discountAmt = discountAmt.multiply(percentageMultipier);
	               				 upsellIOTWPricesList.add(upsellChildStandardPrice.subtract(discountAmt.multiply(upsellChildStandardPrice))); 
	               			 } 
	               		 }
   	             	}
   	             	else {
	   	             	if("Y".equalsIgnoreCase(siteWideDiscountAllowed))
	               	    {
							BigDecimal discountAmt = null;
							String discountType = null;
	   	            	 
		   	            	 //Have to call this method because to get site wide source codes can have variable pricing.  This call returns the correct one based on product price.
							//User Story#1401 - change the default 350 code to global parm 
		   	                 CachedResultSet siteWideSourceCodeData = OrderDAO.getDiscountsBySCAndProductId(siteWideDiscountSourceCode, productId); 
		   	                 if(siteWideSourceCodeData != null){
		   		                 while (siteWideSourceCodeData.next()) {
		   		                	 discountAmt = (BigDecimal) siteWideSourceCodeData.getObject("discount_amt");
		   		                	 discountType = siteWideSourceCodeData.getString("discount_type");
		   		                 }
		   	                 }
		   	            	 //For site wide  source codes that are 'D' for dollar discounts
		               		 if(discountType != null && discountType.equals("D")){
		               			 if(discountAmt != null){
		   	            			upsellIOTWPricesList.add(upsellChildStandardPrice.subtract(discountAmt)); //SALE_PRICE
		               			 }
		               		 }
		               		 //For site wide source codes that are 'P' for percentage discounts
		               		 else if (discountType != null && discountType.equals("P")){
		               			 if(discountAmt != null){
		               				 discountAmt = discountAmt.multiply(percentageMultipier);
		               				 upsellIOTWPricesList.add(upsellChildStandardPrice.subtract(discountAmt.multiply(upsellChildStandardPrice))); 
		               			 } 
		               		 }
	               	    }
	               	    else
	               	    {
	               	    	//no sale price 
	               	    	upsellIOTWPricesList.add(upsellChildStandardPrice);
	               	    }
	               	    
            		}
            	}

					//#11 SHIPPING_COST (Use lowest cost)
					CachedResultSet additionalData2 = OrderDAO.getAdditionalProductInfo(productId);
					while (additionalData2.next()) {
						String groundCostString = additionalData2.getString("ground_cost");
					String twoDayCostString = additionalData2.getString("two_day_cost");
					String nextDayCostString = additionalData2.getString("next_day_cost");
					
					ArrayList<BigDecimal> shippingOptions = new ArrayList<BigDecimal>();
					
					BigDecimal minShippingCost = null;
					
					if(upsellTypeId != null && !upsellTypeId.equals(FLORAL_PRODUCT_TYPE) && !upsellTypeId.equals(SAME_DAY_FRESH_CUTS_PRODUCT_TYPE) && !upsellTypeId.equals(FRESH_CUT_PRODUCT_TYPE)){
						if (groundCostString != null && !groundCostString.equals("")) {
							shippingOptions.add(new BigDecimal(groundCostString));
						}
						if (twoDayCostString != null && !twoDayCostString.equals("")){
							shippingOptions.add(new BigDecimal(twoDayCostString));
						}
						if (nextDayCostString != null && !nextDayCostString.equals("")){
							shippingOptions.add(new BigDecimal(nextDayCostString));
						}
						if(shippingOptions.size() > 0){
							minShippingCost = Collections.min(shippingOptions);
							upsellShippingPrices.add(minShippingCost);
						}
					}
				    else{
				    	upsellShippingPrices.add(domesticServiceFeeCharge);
				    }
				}  
    			} //after while loop
    			
    			//Find the min child standard price, use this for regular price
    			if(upsellStandardPricesList != null && !upsellStandardPricesList.isEmpty()){
					pfVO1.setRegularPrice(Collections.min(upsellStandardPricesList));
				}
    			//Find the min child iotw price, use this for sale price
    			if(upsellIOTWPricesList != null && !upsellIOTWPricesList.isEmpty()){
					pfVO1.setSalePrice(Collections.min(upsellIOTWPricesList));
				}
    			
    			//#12 MAP (Lowest retail price, exactly the same as regular price)
	         	pfVO1.setMapPrice(pfVO1.getRegularPrice());
	         	
	         	//Find the min shipping cost, use this for shipping cost
	         	if(upsellShippingPrices != null && !upsellShippingPrices.isEmpty()){
	         		pfVO1.setShippingCost(Collections.min(upsellShippingPrices));
	         	}
	         		
				buildProductLine(pfVO1, recordCollection, out);
			} //End of product upsell feed
			
			
           
	       //NOW Iterate through the products
	       while (prodFeedCIProducts.next()) {
	        	 ProductFeedVO pfVO2 = new ProductFeedVO();

	        	 //#1 SKU (Novator ID)
	        	 String novatorId = prodFeedCIProducts.getString("novator_id"); //SKU
	             pfVO2.setSKU(novatorId); //SKU
	             
	        	 //Safety check to make sure we aren't adding products that belong to multiple upsells
	        	 String upsellMasterId = prodFeedCIProducts.getString("upsell_master_id");
	             if(upsellMasterId != null){
	            	 //If you already have this upsell child in this feed, don't add it again (this is to handle child upsells belonging to multiple upsell masters)
	            	 if(upsellChildrenArrayList.contains(novatorId)){
	            		 continue;
	            	 } 
	            	 else{
	            		 //Per requirements, add the Novator ID for upsell children products
	            		 upsellChildrenArrayList.add(novatorId);
	            	 }
	             }
	             
	             //#2 PARENT_SKU (Upsell Master ID)
	             pfVO2.setParentSKU(novatorId); //PARENT_SKU

	             //#6 NAME (Novator name)
	             pfVO2.setName(prodFeedCIProducts.getString("novator_name")); //NAME 
	
	             //#7 DESCRIPTION (Description)
	             pfVO2.setDescription(prodFeedCIProducts.getString("long_description"));
	             
	             //#9 REGULAR_PRICE (Lowest price)
	             BigDecimal standardPrice = (BigDecimal) prodFeedCIProducts.getObject("standard_price");
	             
	             //All products will use the "Good Price" as the price we send out
	             pfVO2.setRegularPrice(standardPrice);  //REGULAR_PRICE
	             
	             //#10 SALE_PRICE (Lowest discounted price)
	             
	             //User Story 1401:Enhancements to Channel Advisor Feed
	             String discountFlag = prodFeedCIProducts.getString("DISCOUNT_ALLOWED_FLAG");
	             String siteWideDiscountAllowed = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, SITEWIDE_DISCOUNT_ALLOWED);
               	 String siteWideDiscountSourceCode = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, SITEWIDE_DISCOUNT_SOURCE_CODE);
               	 
	             if("Y".equalsIgnoreCase(discountFlag)){
	             String iotwSourceCode = prodFeedCIProducts.getString("iotw_source_code");
	             if(iotwSourceCode != null){
	            	 String productId = prodFeedCIProducts.getString("product_id");
	            	 BigDecimal discountAmt = null;
	            	 String discountType = null;
	            	 
	            	 //Have to call this method because IOTWs can have variable pricing.  This call returns the correct one based on product price.
	                 CachedResultSet iotwSourceCodeData = OrderDAO.getIOTWsBySourceCodeAndProductId(siteWideDiscountSourceCode, productId); 
	                 if(iotwSourceCodeData != null){
		                 while (iotwSourceCodeData.next()) {
		                	 discountAmt = (BigDecimal) iotwSourceCodeData.getObject("discount_amt");
		                	 discountType = iotwSourceCodeData.getString("discount_type");
		                 }
	                 }
	            	 //For IOTW source codes that are 'D' for dollar discounts
            		 if(discountType != null && discountType.equals("D")){
            			 if(discountAmt != null){			 
	            			 pfVO2.setSalePrice(pfVO2.getRegularPrice().subtract(discountAmt)); //SALE_PRICE
            			 }
            		 }
            		 //For IOTW source codes that are 'P' for percentage discounts
            		 else if (discountType != null && discountType.equals("P")){
            			 if(discountAmt != null){
            				 discountAmt = discountAmt.multiply(percentageMultipier);
            				 pfVO2.setSalePrice(standardPrice.subtract(discountAmt.multiply(standardPrice)));
            			 } 
            		 }
	             }
	             else{
	               	   if("Y".equalsIgnoreCase(siteWideDiscountAllowed))
	               	    {
		            	 String productId = prodFeedCIProducts.getString("product_id");
		            	 BigDecimal discountAmt = null;
		            	 String discountType = null;
		            	 
		            	 //Have to call this method because site wide source code can have variable pricing.  This call returns the correct one based on product price.
		                 CachedResultSet siteWideSourceCodeData = OrderDAO.getDiscountsBySCAndProductId(siteWideDiscountSourceCode, productId); 
		                 if(siteWideSourceCodeData != null){
			                 while (siteWideSourceCodeData.next()) {
			                	 discountAmt = (BigDecimal) siteWideSourceCodeData.getObject("discount_amt");
			                	 discountType = siteWideSourceCodeData.getString("discount_type");
			                 }
		                 }
		            	 //For Site wide source codes that are 'D' for dollar discounts
	            		 if(discountType != null && discountType.equals("D")){
	            			 if(discountAmt != null){			 
		            			 pfVO2.setSalePrice(pfVO2.getRegularPrice().subtract(discountAmt)); //SALE_PRICE
	            			 }
	            		 }
	            		 //For Site wide source codes that are 'P' for percentage discounts
	            		  else if (discountType != null && discountType.equals("P")){
	            			 if(discountAmt != null){
	            				 discountAmt = discountAmt.multiply(percentageMultipier);
	            				 pfVO2.setSalePrice(standardPrice.subtract(discountAmt.multiply(standardPrice)));
	            			 } 
	            		  }
	               	    }
	               	    else
	               	    {
	               	    	//no sale price 
	               	    	pfVO2.setSalePrice(null);
	               	    }
	             }
	             }

	             //#12 MAP (Lowest retail price, exactly the same as regular price)
	             pfVO2.setMapPrice(pfVO2.getRegularPrice());
	             
	             //#13 BUY_URL (URL that links to product)
	             if(pfVO2.getName() != null){
	            	 String buyURL = pfVO2.getName();
	            	 buyURL = createBuyURLforProductFeed(buyURL, novatorId);
	            	 pfVO2.setBuyURL(buyURL);
	             }

	             //#14 CATEGORY (Category|Type|Sub-type)
	             String productCategoryDescription = prodFeedCIProducts.getString("description");
	             String productTypeDescription = prodFeedCIProducts.getString("type_description");
	             String productSubTypeDescription = prodFeedCIProducts.getString("sub_type_description");
	             
	             ArrayList<String> categoryArrayList = new ArrayList<String>();
	             
	             if(productCategoryDescription != null && !productCategoryDescription.isEmpty() && productCategoryDescription.trim().length() != 0){
	            	 categoryArrayList.add(productCategoryDescription);
	             }
	             if(productTypeDescription != null && !productTypeDescription.isEmpty() && productTypeDescription.trim().length() != 0){
	            	 categoryArrayList.add(productTypeDescription);
	             }
	             if(productSubTypeDescription != null && !productSubTypeDescription.isEmpty() && productSubTypeDescription.trim().length() != 0){
	            	 categoryArrayList.add(productSubTypeDescription);
	             }
	             
	             Iterator<String> categoryIter = categoryArrayList.iterator();
	             StringBuilder categories = new StringBuilder();  //using builder cause its faster and this is only one thread of execution

	             while (categoryIter.hasNext()) {
	             	categories.append(categoryIter.next());
	                 if (categoryIter.hasNext()) {
	                 	categories.append('|');
	                 }
	             }
	             pfVO2.setCategory(categories.toString());
	             
	             //#15 AVAILABILITY (Available or not)
	             String productStatus = prodFeedCIProducts.getString("status");
	             if(productStatus.equals("A")){
	            	 pfVO2.setAvailability("Y");
	             }
	             else if (productStatus.equals("U")){
	            	 pfVO2.setAvailability("N");
	             }

	             //#17 LARGE_IMAGE_URL (URL that links to large product image)
	             // User Story
	                String imageURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, IMAGE_URL);

		            String imageSize = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, IMAGE_SIZE);
		         
		            //pfVO2.setLargeImgURL("http://www.ftdimg.com/pics/products/" + novatorId + "_330x370.jpg");
		              pfVO2.setLargeImgURL(imageURL + novatorId + imageSize);
	             
	             //#43 SAMEDAY_FLAG (Y or N)
	             String typeId = prodFeedCIProducts.getString("type_id");
	             if(typeId != null){
	            	 //Don't add services product types to product feed
	            	 if(typeId.equals(SERVICES_PRODUCT_TYPE)){
	            		 continue;
	            	 }
	            	 if(typeId.equals(FLORAL_PRODUCT_TYPE) || typeId.equals(SAME_DAY_FRESH_CUTS_PRODUCT_TYPE) || typeId.equals(SAME_DAY_GIFT_PRODUCT_TYPE)){
	            		 pfVO2.setSameDayFlag('Y');
	            	 }
	            	 else{
	            		 pfVO2.setSameDayFlag('N');
	            	 }
	             }
	             else{
	            	 pfVO2.setSameDayFlag('N');
	             }
	             
	             //#11 SHIPPING_COST (Use lowest cost) and #26 KEYWORDS (Keyword Search) and #31 ATTRIBUTE_NAMES (Included Colors | Size | Dominant Flowers) and #32 ATTRIBUTE_VALUES (example: Red|18H x 12W|roses;yellow)        
	             String productId = prodFeedCIProducts.getString("product_id");
	             pfVO2.setProductId(productId);
	             CachedResultSet additionalData = OrderDAO.getAdditionalProductInfo(productId);

	             while (additionalData.next()) {
	            	String productColors = (additionalData.getString("product_colors"));
	            	if(productColors != null && !productColors.isEmpty() && productColors.trim().length() != 0){
	            		pfVO2.setProductColors(productColors);
	            	}
	            	String keywords = additionalData.getString("product_keywords");
	            	if(keywords != null && !keywords.isEmpty() && keywords.trim().length() != 0){
		            	 pfVO2.setKeywords(keywords.replaceAll(",", "|"));
		             }
	            	else{
	            		pfVO2.setKeywords("");
	            	}
	            	String groundCostString = additionalData.getString("ground_cost");
	            	String twoDayCostString = additionalData.getString("two_day_cost");
	            	String nextDayCostString = additionalData.getString("next_day_cost");

	                ArrayList<BigDecimal> shippingOptions = new ArrayList<BigDecimal>();
	            	
		            BigDecimal minShippingCost = null;
		            
		            if(typeId != null && !typeId.equals(FLORAL_PRODUCT_TYPE) && !typeId.equals(SAME_DAY_FRESH_CUTS_PRODUCT_TYPE) && !typeId.equals(FRESH_CUT_PRODUCT_TYPE)){
						if (groundCostString != null && !groundCostString.equals("")) {
							shippingOptions.add(new BigDecimal(groundCostString));
						}
						if (twoDayCostString != null && !twoDayCostString.equals("")){
							shippingOptions.add(new BigDecimal(twoDayCostString));
						}
						if (nextDayCostString != null && !nextDayCostString.equals("")){
							shippingOptions.add(new BigDecimal(nextDayCostString));
						}
						if(shippingOptions.size() > 0){
							minShippingCost = Collections.min(shippingOptions);
							pfVO2.setShippingCost(minShippingCost);
						}
		            }
		            else{
		            	pfVO2.setShippingCost(domesticServiceFeeCharge);
		            }
	             }
	             ArrayList<String> attributeNameArrayList = new ArrayList<String>();
	             ArrayList<String> attributeValuesArrayList = new ArrayList<String>();
	             
	             String arrangementSize = prodFeedCIProducts.getString("arrangement_size");
	             String dominantColors = prodFeedCIProducts.getString("dominant_flowers");
	             
	             if(pfVO2.getProductColors() != null && !pfVO2.getProductColors().isEmpty() && pfVO2.getProductColors().trim().length() != 0){
	            	 attributeNameArrayList.add("Included Colors");
	            	 attributeValuesArrayList.add(pfVO2.getProductColors());
	             }
	             
	             if(arrangementSize != null && !arrangementSize.isEmpty() && arrangementSize.trim().length() != 0){
	            	 attributeNameArrayList.add("Size");
	            	 attributeValuesArrayList.add(arrangementSize);
	             }
	             
	             if(dominantColors != null && !dominantColors.isEmpty() && dominantColors.trim().length() != 0){
	            	 attributeNameArrayList.add("Dominant Flowers");
	            	 attributeValuesArrayList.add(dominantColors);
	             }

	             Iterator<String> attributeNameIter = attributeNameArrayList.iterator();
	             Iterator<String> attributeValueIter = attributeValuesArrayList.iterator();
	             
	             StringBuilder attributeNames = new StringBuilder();  //using builder cause its faster and this is only one thread of execution
	             StringBuilder attributeValues = new StringBuilder(); //using builder cause its faster and this is only one thread of execution
	             
	             while (attributeNameIter.hasNext()) {
	            	 attributeNames.append(attributeNameIter.next());
	                 if (attributeNameIter.hasNext()) {
	                	 attributeNames.append('|');
	                 }
	             }
	             
	             while (attributeValueIter.hasNext()) {
	            	 attributeValues.append(attributeValueIter.next());
	                 if (attributeValueIter.hasNext()) {
	                	 attributeValues.append('|');
	                 }
	             }
	             pfVO2.setAttributeNames(attributeNames.toString());
	             pfVO2.setAttributeValues(attributeValues.toString());

	             //#40 COGS (this field will remain blank)
	             
	             //#41 MARGIN (this field will remain blank)
	             
	             //#42 MARGIN_PRESENT (this field will remain blank)

	             //#44 AVAILABILITY_DATES (MM/DD/YY - MM/DD/YY or blank)
	             Date exceptionStartDate = (Date) prodFeedCIProducts.getDate("exception_start_date");
	             Date exceptionEndDate = (Date) prodFeedCIProducts.getDate("exception_end_date");
	             if(exceptionStartDate != null && exceptionEndDate != null){
	            	 pfVO2.setAvailabilityDates(TextUtil.formatDate(exceptionStartDate, "MM/dd/yyyy") + " - " + TextUtil.formatDate(exceptionEndDate, "MM/dd/yyyy"));
	             }
	             else{
	            	 pfVO2.setAvailabilityDates("");
	             }
	             
	             buildProductLine(pfVO2, recordCollection, out);
	         } //after while loop
        } catch (Throwable t) {
        	logger.error("The following error occured during processing of Prod Feed CI: " + t.getStackTrace() );
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        } finally {
            out.close();
        } 
        
        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private static String composePostFilePathLocal(String filePath,String fileName, Date postDate) {
        return filePath + File.separator + fileName;
    }
        
    public static String createBuyURLforProductFeed(String name, String productId) {
        if (name == null) {
            return name;
        }
        name = name.trim();
        name = name.replace("& ", " "); //this is required for regex on next line to function properly
        name = name.replaceAll("&.*?;", ""); //removes all CSS special characters between & and ; 
        name = name.replaceAll("\\<.*?>",""); //removes html tags
        name = name.replace("-", " ");
        name = name.replaceAll("\\s+", " ");
        name = name.replace(" ", "-");
        
        String goodChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";
        StringBuffer result = new StringBuffer(name.length());

        for (int i = 0; i < name.length(); i++) {
            if (goodChars.indexOf(name.charAt(i)) >= 0) {
                result.append(name.charAt(i));
            }
        }

        return "http://www.ftd.com/" + result.toString().toLowerCase() + "-prd/" + productId + "/";
    }
    
    public static BigDecimal round(BigDecimal d, int scale, boolean roundUp) {
	  int mode = (roundUp) ? BigDecimal.ROUND_UP : BigDecimal.ROUND_DOWN;
	  return d.setScale(scale, mode);
    }
   
    private void buildProductLine(ProductFeedVO pfVO, IRecordCollection recordCollection, PrintWriter out) {
    	IRecordLine recordLine = recordCollection.createLineInstance();
    	recordLine.addStringValue(TextUtil.stripTabs(pfVO.getSKU())); //SKU
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getParentSKU())); //PARENT_SKU
    	recordLine.addStringValue(""); //MANUFACTURER_NAME
    	recordLine.addStringValue(TextUtil.stripTabs(pfVO.getName())); //NAME
    	recordLine.addStringValue(TextUtil.stripTabs(pfVO.getDescription())); //DESCRIPTION
    	recordLine.addStringValue(""); //SHORT_DESCRIPTION
        if(pfVO.getRegularPrice() != null){
        	//Now to fix that silly rounding issue
        	pfVO.setRegularPrice(round(pfVO.getRegularPrice(), 2, true));
        	recordLine.addStringValue(df.format(pfVO.getRegularPrice().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())); //REGULAR_PRICE
        }
        else{
        	recordLine.addStringValue(""); //REGULAR_PRICE
        }
    	if(pfVO.getSalePrice() != null){
    		//Now to fix that silly rounding issue
        	pfVO.setSalePrice(round(pfVO.getSalePrice(), 2, true));
    		recordLine.addStringValue(df.format(pfVO.getSalePrice().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())); //SALE_PRICE 
    	}
	    else{
	    	recordLine.addStringValue(""); //SALE_PRICE 
	    }
        if(pfVO.getShippingCost() != null){
        	//Now to fix that silly rounding issue.  Note: With shipping you want to round down.
        	pfVO.setShippingCost(round(pfVO.getShippingCost(), 2, false));
        	recordLine.addStringValue(df.format(pfVO.getShippingCost().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())); //SHIPPING_COST 
        }	  
        else{
        	recordLine.addStringValue(""); //SHIPPING_COST  
        }
        if(pfVO.getMapPrice() != null){
        	pfVO.setMapPrice(round(pfVO.getMapPrice(), 2, true));
        	recordLine.addStringValue(df.format(pfVO.getMapPrice().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue())); //MAP_PRICE	
        }
        else{
        	recordLine.addStringValue(""); //MAP_PRICE
        }
        recordLine.addStringValue(pfVO.getBuyURL()); //BUY_URL
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getCategory())); //CATEGORY
        recordLine.addStringValue(pfVO.getAvailability()); //AVAILABILITY
        recordLine.addStringValue(""); //QUANTITY_IN_STOCK
        recordLine.addStringValue(pfVO.getLargeImgURL()); //LARGE_IMAGE_URL
        recordLine.addStringValue(""); //SMALL_IMAGE_URL
        recordLine.addStringValue("New"); //CONDITION
        recordLine.addStringValue("USD"); //CURRENCY
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getKeywords())); //KEYWORDS
        recordLine.addStringValue(pfVO.getVariationThemes()); //VARIATION_THEMES
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getAttributeNames())); //ATTRIBUTE_NAMES
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getAttributeValues())); //ATTRIBUTE_VALUES
        recordLine.addStringValue(""); //COGS
        recordLine.addStringValue(""); //MARGIN
        recordLine.addStringValue(""); //MARGIN_PRECENT
        recordLine.addStringValue(Character.toString(pfVO.getSameDayFlag())); //SAMEDAY_FLAG
        recordLine.addStringValue(pfVO.getAvailabilityDates()); //AVAILABILITY_DATES
                
        out.println(recordLine.toString());
    }
    
    private void buildHeader(IRecordCollection recordCollection, PrintWriter out) {
        IRecordLine recordLine = recordCollection.createLineInstance();
        recordLine.addStringValue("sku");
        recordLine.addStringValue("parent_sku");
        recordLine.addStringValue("manufacturer_name");
        recordLine.addStringValue("name");
        recordLine.addStringValue("description");
        recordLine.addStringValue("short_description");
        recordLine.addStringValue("regular_price");
        recordLine.addStringValue("sale_price");
        recordLine.addStringValue("shipping_cost");
        recordLine.addStringValue("map");
        recordLine.addStringValue("buy_url");
        recordLine.addStringValue("category");
        recordLine.addStringValue("availability");
        recordLine.addStringValue("quantity_in_stock");
        recordLine.addStringValue("large_image_url");
        recordLine.addStringValue("small_image_url");
        recordLine.addStringValue("condition");
        recordLine.addStringValue("currency");
        recordLine.addStringValue("keywords");
        recordLine.addStringValue("variation_themes");
        recordLine.addStringValue("attribute_names");
        recordLine.addStringValue("attribute_values");
        recordLine.addStringValue("cogs");
        recordLine.addStringValue("margin");
        recordLine.addStringValue("margin_percent");
        recordLine.addStringValue("sameday_flag");
        recordLine.addStringValue("availability_dates");
        out.println(recordLine.toString());
    }     
}

