package com.ftd.reports.impl.prodfeedci.vo;

import java.math.BigDecimal;

public class ProductFeedVO
{
	protected String SKU;
	protected String parentSKU;
	protected String name;
	protected String description;
	protected char sameDayFlag;
	protected BigDecimal mapPrice;
	protected BigDecimal salePrice;
	protected BigDecimal regularPrice;
	protected BigDecimal shippingCost;
	protected String buyURL;
	protected String category;
	protected String availability;
	protected String largeImgURL;
	protected String productId;
	protected String productColors;
	protected String keywords;
	protected String attributeNames;
	protected String attributeValues;
	protected String availabilityDates;
	protected String variationThemes;
	
	public ProductFeedVO() {
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public String getParentSKU() {
		return parentSKU;
	}

	public void setParentSKU(String parentSKU) {
		this.parentSKU = parentSKU;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public char getSameDayFlag() {
		return sameDayFlag;
	}

	public void setSameDayFlag(char sameDayFlag) {
		this.sameDayFlag = sameDayFlag;
	}

	public BigDecimal getMapPrice() {
		return mapPrice;
	}

	public void setMapPrice(BigDecimal mapPrice) {
		this.mapPrice = mapPrice;
	}

	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public BigDecimal getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(BigDecimal regularPrice) {
		this.regularPrice = regularPrice;
	}

	public BigDecimal getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(BigDecimal shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getBuyURL() {
		return buyURL;
	}

	public void setBuyURL(String buyURL) {
		this.buyURL = buyURL;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getLargeImgURL() {
		return largeImgURL;
	}

	public void setLargeImgURL(String largeImgURL) {
		this.largeImgURL = largeImgURL;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductColors() {
		return productColors;
	}

	public void setProductColors(String productColors) {
		this.productColors = productColors;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getAttributeNames() {
		return attributeNames;
	}

	public void setAttributeNames(String attributeNames) {
		this.attributeNames = attributeNames;
	}

	public String getAttributeValues() {
		return attributeValues;
	}

	public void setAttributeValues(String attributeValues) {
		this.attributeValues = attributeValues;
	}

	public String getAvailabilityDates() {
		return availabilityDates;
	}

	public void setAvailabilityDates(String availabilityDates) {
		this.availabilityDates = availabilityDates;
	}

	public String getVariationThemes() {
		return variationThemes;
	}

	public void setVariationThemes(String variationThemes) {
		this.variationThemes = variationThemes;
	}
	
}