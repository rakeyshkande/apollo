package com.ftd.reports.impl.jellyfish.action;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.reports.dao.OrderDAO;

import java.io.File;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


/**
 * This class is responsible for creating a refund file for orders with the Jellyfish source code.
 */
public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());

    // These fields will be initialized by the process archive.
    protected String sourceCode;
    protected String filePath;

    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String delimiter = "\t";
        
        try {
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
            List<OrderDetailVO> rewardList = new ArrayList();            

            DateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            logger.info(
                "execute: Beginning Jellyfish refund extraction for dates from " +
                dateFormat.format(startDate) + " to " +
                dateFormat.format(endDate));

            // Transform into the needed format.
            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);

            CachedResultSet report = OrderDAO.queryRefundBySourceCode(sourceCode, startDate, endDate);

            logger.info("execute: Obtained result set rows: " + report.getRowCount());

            // Iterate through the result set, transferring the results
            // into a sorted Map because the business requires the final
            // results.
            while (report.next()) {
                    OrderDetailVO odVO = new OrderDetailVO();
                    odVO.setOrderDetailId(report.getString(5));
                    odVO.setSourceCode(report.getString(6));
                    // Delivery date is used to set as reward date on order_reward_posting for purchases.
                    // Use end date as the reward date for refunds.
                    odVO.setDeliveryDate(endDate);
                    // JELLYFISH is not a real partner program. Save it nontheless.
                    odVO.setProgramName((String)context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME));
                    // Reward total is not calculated.
                    rewardList.add(odVO);
                    
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    // Position 1: Record Type. 'R' for returned.
                    recordLine.addStringValue("R");
                    
                    // tab delimited
                    recordLine.addStringValue(delimiter);
                    
                    // Position 2: MPI (Merchant Product Identifier.
                    recordLine.addStringValue(report.getString(1));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 3: Product Amount.
                    recordLine.addStringValue((report.getBigDecimal(2)).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 4: Transaction ID - leave blank for refunds
                    recordLine.addStringValue(delimiter);
                     
                    // Position 5: Event Date - format YYYY-MM-DD HH:MI:SS. Formated in procedure since Date loses time component.
                    recordLine.addStringValue(report.getString(3));
                    recordLine.addStringValue(delimiter);
                    
                    // Postion 6: Order Number
                    recordLine.addStringValue(report.getString(4));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 7: Quantity. This is always 1
                    recordLine.addStringValue("1");

                    recordCollection.addRecordLine(recordLine);
            }
            logger.info("execute: Completed processing of current result set.");
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, rewardList);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE, endDate);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND, Boolean.valueOf(true));
            persistToFile(executionContext, recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private void persistToFile(ExecutionContext executionContext, IRecordCollection transformResult) {

        ContextInstance context = executionContext.getContextInstance();
        //Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
        Date today = Calendar.getInstance().getTime();

        String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            
        String fileName = TextUtil.formatDate(today, "yyyyMMdd");

        // Initialize the local destination file name.
        if (fullFileNameLocal == null) {
            fullFileNameLocal = DataExtractor.composePostFilePathLocal(filePath, fileName);

            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                    fullFileNameLocal);
        }
            
        String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
        // Initialize the remote file name.
        if (remoteFileName == null) {
            remoteFileName = fileName + ".txt";
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
            logger.debug("remoteFileName: " + remoteFileName);
        }

        // Write the reward records to the destination file.
        PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                true);

        ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
    }    
    
    private static String composePostFilePathLocal(String filePath,
        String fileName) {
        return filePath + File.separator + fileName + TextUtil.formatDate(new Date(), "_yyyyMMdd_HHmm");
    }    
}
