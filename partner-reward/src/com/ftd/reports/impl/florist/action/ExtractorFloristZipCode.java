package com.ftd.reports.impl.florist.action;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.FloristDAO;

import java.math.BigDecimal;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

public class ExtractorFloristZipCode implements ActionHandler {
    protected Logger logger = new Logger(com.ftd.reports.impl.florist.action.ExtractorFloristZipCode.class.getName());

    public ExtractorFloristZipCode() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;

        try {
            ContextInstance context = executionContext.getContextInstance();
            
            // Obtain the result set of florist and their zip code counts.
            CachedResultSet report = FloristDAO.queryZipCodeCount();
            
            // Transform into the needed format.
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false, ',');
            while (report.next()) 
            {
              // Result: florist id, zip code count, GoTo indicator
              IRecordLine recordLine = recordCollection.createLineInstance();
              recordLine.addStringValue((String) report.getObject(1));
              recordLine.addStringValue(String.valueOf(((BigDecimal) report.getObject(2)).intValue()));
              recordLine.addStringValue((String) report.getObject(3));
              recordCollection.addRecordLine(recordLine);
            }
            
            // Bind the transformed data set to the context.
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_TRANSFORMATION_RESULT,
                recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
}
