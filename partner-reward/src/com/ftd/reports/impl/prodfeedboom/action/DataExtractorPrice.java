package com.ftd.reports.impl.prodfeedboom.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;
import com.ftd.reports.impl.prodfeedboom.vo.ProductFeedVO;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DataExtractorPrice implements ActionHandler {

    protected final static char DELIMETER = '|';
    protected final static String BLANK = "";
    protected static final String CONFIG_CONTEXT = "PARTNER_REWARD";

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;
    protected Logger logger = new Logger(DataExtractorPrice.class.getName());

    protected Map<String,String> productMap;

    public DataExtractorPrice() {
        productMap = new HashMap<String, String>();
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        PrintWriter out = null;

        try {
            ContextInstance context = executionContext.getContextInstance();

            logger.info("execute: Beginning Boomerang Product feed extraction");

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            // Transform into the needed format.
            //IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                    DELIMETER);

            logger.info("execute: Querying Product Data Feed");

            CachedResultSet report = OrderDAO.queryGetProdFeedCIProducts(null,null);

            logger.info("execute: Obtained result set rows: " + report.getRowCount());

            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = composePostFilePathLocal(filePath, fileName, new Date());
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                        fullFileNameLocal);
                logger.debug("fullFileNameLocal: " + fullFileNameLocal);
            }

            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            // Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = fileName;
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
                logger.debug("remoteFileName: " + remoteFileName);
            }

            File file = new File(fullFileNameLocal);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            out = new PrintWriter(new BufferedWriter(
                    new FileWriter(file, true)), true);

            buildHeader(recordCollection, out);

            while (report.next()) {
                ProductFeedVO pfVO2 = new ProductFeedVO();

                // Price
                BigDecimal standardPrice = (BigDecimal) report.getObject("standard_price");
                pfVO2.setRegularPrice(standardPrice);  //REGULAR_PRICE

                // Product ID
                String productId = report.getString("product_id");
                pfVO2.setProductId(productId);

                if (notDuplicate(pfVO2))
                {
                    buildProductLine(pfVO2, recordCollection, out);
                }
            } //after while loop
        } catch (Throwable t) {
            logger.error("The following error occured during processing of Prod Feed Price Boomerang: " + t.getStackTrace() );
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        } finally {
            out.close();
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }

    protected static String composePostFilePathLocal(String filePath,
                                                     String fileName, Date postDate) {
        return filePath + File.separator + fileName + "_" + TextUtil.formatDate(postDate, "yyyyMMdd");
    }

    protected void buildProductLine(ProductFeedVO pfVO,
        IRecordCollection recordCollection,
        PrintWriter out) {

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("####0.00");

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue(pfVO.getProductId());
        recordLine.addStringValue("1"); // channel id
        recordLine.addStringValue(TextUtil.formatDate(new Date(), "MM/dd/yyyy"));

        String standardPriceString = "0.00";
        BigDecimal standardPrice = pfVO.getRegularPrice();
        if (standardPrice != null) {
            standardPriceString = df.format(standardPrice
                    .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
        }
        recordLine.addStringValue(standardPriceString); // list_price
        recordLine.addStringValue(standardPriceString); // reg_price
        recordLine.addStringValue(standardPriceString); // offer_price

        //recordCollection.addRecordLine(recordLine);
        out.println(recordLine.toString());
    }

    protected void buildHeader(IRecordCollection recordCollection, PrintWriter out) {

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue("sku");
        recordLine.addStringValue("channel_id");
        recordLine.addStringValue("effective_date");
        recordLine.addStringValue("list_price");
        recordLine.addStringValue("reg_price");
        recordLine.addStringValue("offer_price");

        //recordCollection.addRecordLine(recordLine);
        out.println(recordLine.toString());
    }

    /**
     * Return true if the product is not a duplicate.  The data set has products and upsell data and can have duplicates if
     * the products is in more than one upsell.
     * @param pfvo
     * @return
     */
    protected boolean notDuplicate(ProductFeedVO pfvo)
    {
        String pfvoKey = productMap.get(pfvo.getProductId());
        if (pfvoKey == null)
        {
            productMap.put(pfvo.getProductId(),pfvo.getProductId());
            return true;
        }
        else
        {
            return false;
        }

    }
}
