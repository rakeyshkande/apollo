package com.ftd.reports.impl.prodfeedboom.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.reports.core.IWorkflowConstants;
import com.ftd.reports.dao.OrderDAO;
import com.ftd.reports.impl.prodfeedboom.vo.ProductFeedVO;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DataExtractorItem implements ActionHandler {

    protected final static char DELIMETER = '|';
    protected final static String BLANK = "";
    protected static final String IMAGE_URL = "IMAGE_URL";
    protected static final String IMAGE_SIZE = "IMAGE_SIZE";
    protected static final String CONFIG_CONTEXT = "PARTNER_REWARD";

    protected Logger logger = new Logger(DataExtractorItem.class.getName());

    // These fields will be initialized by the process archive.
    protected String filePath;
    protected String fileName;

    protected Map<String,String> productMap;

    public DataExtractorItem() {
        productMap = new HashMap<String, String>();
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        PrintWriter out = null;

        try {
            ContextInstance context = executionContext.getContextInstance();

            logger.info("execute: Beginning Boomerang Product feed extraction");

            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

            // Transform into the needed format.
            //IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);
            IRecordCollection recordCollection = RecordCollectionFactory.getDelimitedRecordCollection(false,
                    DELIMETER);

            logger.info("execute: Querying Product Data Feed");

            CachedResultSet report = OrderDAO.queryGetProdFeedCIProducts(null,null);

            logger.info("execute: Obtained result set rows: " + report.getRowCount());

            String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
            // Initialize the local destination file name.
            if (fullFileNameLocal == null) {
                fullFileNameLocal = composePostFilePathLocal(filePath, fileName, new Date());
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,
                        fullFileNameLocal);
                logger.debug("fullFileNameLocal: " + fullFileNameLocal);
            }

            String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
            // Initialize the remote file name.
            if (remoteFileName == null) {
                remoteFileName = fileName;
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
                logger.debug("remoteFileName: " + remoteFileName);
            }

            File file = new File(fullFileNameLocal);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            out = new PrintWriter(new BufferedWriter(
                    new FileWriter(file, true)), true);

            buildHeader(recordCollection, out);

            while (report.next()) {
                ProductFeedVO pfVO2 = new ProductFeedVO();

                //#1 SKU (Novator ID)
                String novatorId = report.getString("novator_id"); //SKU
                pfVO2.setSKU(novatorId); //SKU

                //#2 PARENT_SKU (Upsell Master ID)
                pfVO2.setParentSKU(novatorId); //PARENT_SKU

                //#6 NAME (Novator name)
                pfVO2.setName(report.getString("novator_name")); //NAME

                //#7 DESCRIPTION (Description)
                pfVO2.setDescription(report.getString("long_description"));

                //#9 REGULAR_PRICE (Lowest price)
                BigDecimal standardPrice = (BigDecimal) report.getObject("standard_price");

                //All products will use the "Good Price" as the price we send out
                pfVO2.setRegularPrice(standardPrice);  //REGULAR_PRICE

                //#13 BUY_URL (URL that links to product)
                if(pfVO2.getName() != null){
                    String buyURL = pfVO2.getName();
                    buyURL = createBuyURLforProductFeed(buyURL, novatorId);
                    pfVO2.setBuyURL(buyURL);
                }

                //#14 CATEGORY (Category|Type|Sub-type)
                String productCategoryDescription = report.getString("description");
                String productTypeDescription = report.getString("type_description");
                String productSubTypeDescription = report.getString("sub_type_description");

                pfVO2.setTypeDescription(report.getString("type_description"));
                pfVO2.setSubTypeDescription(report.getString("sub_type_description"));

                //#15 AVAILABILITY (Available or not)
                String productStatus = report.getString("status");
                if(productStatus.equals("A")){
                    pfVO2.setAvailability("Y");
                }
                else if (productStatus.equals("U")){
                    pfVO2.setAvailability("N");
                }

                //#17 LARGE_IMAGE_URL (URL that links to large product image)
                // User Story
                String imageURL = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, IMAGE_URL);

                String imageSize = configUtil.getFrpGlobalParm(CONFIG_CONTEXT, IMAGE_SIZE);

                //pfVO2.setLargeImgURL("http://www.ftdimg.com/pics/products/" + novatorId + "_330x370.jpg");
                pfVO2.setLargeImgURL(imageURL + novatorId + imageSize);


                //#11 SHIPPING_COST (Use lowest cost) and #26 KEYWORDS (Keyword Search) and #31 ATTRIBUTE_NAMES (Included Colors | Size | Dominant Flowers) and #32 ATTRIBUTE_VALUES (example: Red|18H x 12W|roses;yellow)
                String productId = report.getString("product_id");
                pfVO2.setProductId(productId);

                String pquadID = report.getString("pquad_product_id");
                pfVO2.setPquadProductID(pquadID);


                if (notDuplicate(pfVO2))
                {
                    buildProductLine(pfVO2, recordCollection, out);
                }
            } //after while loop
        } catch (Throwable t) {
            logger.error("The following error occured during processing of Prod Feed Item Boomerang: " + t.getStackTrace() );
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        } finally {
            out.close();
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }

    protected static String composePostFilePathLocal(String filePath,
                                                     String fileName, Date postDate) {
        return filePath + File.separator + fileName + "_" + TextUtil.formatDate(postDate, "yyyyMMdd");
    }

    protected void buildProductLine(ProductFeedVO pfVO,
        IRecordCollection recordCollection,
        PrintWriter out) {

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern("####0.00");

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue(pfVO.getProductId());
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getName()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getBuyURL()));
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getLargeImgURL()));
        recordLine.addStringValue(TextUtil.stripPipe(TextUtil.stripTabs(pfVO.getDescription())));
        recordLine.addStringValue("FTD"); // Brand
        recordLine.addStringValue(pfVO.getProductId()); // UPC
        recordLine.addStringValue("FTD"); // manufacturer
        recordLine.addStringValue(pfVO.getProductId()); // mpno

        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getTypeDescription())); // merch_l1_id
        recordLine.addStringValue(BLANK); // merch_l1_name
        recordLine.addStringValue(TextUtil.stripTabs(pfVO.getSubTypeDescription())); // merch_l2_id
        recordLine.addStringValue(BLANK); // merch_l2_name
        recordLine.addStringValue(BLANK); // merch_l3_id
        recordLine.addStringValue(BLANK); // merch_l3_name
        recordLine.addStringValue(BLANK); // merch_l4_id
        recordLine.addStringValue(BLANK); // merch_l4_name
        recordLine.addStringValue(BLANK); // merch_l5_id
        recordLine.addStringValue(BLANK); // merch_l5_name

        recordLine.addStringValue(BLANK); // color
        recordLine.addStringValue(BLANK); // planning_type
        recordLine.addStringValue(BLANK); // size
        recordLine.addStringValue(BLANK); // product_sub_type
        recordLine.addStringValue(BLANK); // time_in_transit
        recordLine.addStringValue(BLANK); // start_date
        recordLine.addStringValue(BLANK); // end_date
        recordLine.addStringValue(BLANK); // ship_origin
        recordLine.addStringValue(BLANK); // source_origin
        recordLine.addStringValue("1"); // channel_id
        recordLine.addStringValue(pfVO.getAvailability()); // active_flag
        recordLine.addStringValue(pfVO.getPquadProductID()); // ftd_pf_sku_mapping

        out.println(recordLine.toString());
    }

    protected void buildHeader(IRecordCollection recordCollection, PrintWriter out) {

        IRecordLine recordLine = recordCollection.createLineInstance();

        recordLine.addStringValue("sku");
        recordLine.addStringValue("title");
        recordLine.addStringValue("product_url");
        recordLine.addStringValue("image_url");
        recordLine.addStringValue("description");
        recordLine.addStringValue("brand");
        recordLine.addStringValue("upc");
        recordLine.addStringValue("manufacturer");
        recordLine.addStringValue("mpno/model no");
        recordLine.addStringValue("merch_l1_id");
        recordLine.addStringValue("merch_l1_name");
        recordLine.addStringValue("merch_l2_id");
        recordLine.addStringValue("merch_l2_name");
        recordLine.addStringValue("merch_l3_id");
        recordLine.addStringValue("merch_l3_name");
        recordLine.addStringValue("merch_l4_id");
        recordLine.addStringValue("merch_l4_name");
        recordLine.addStringValue("merch_l5_id");
        recordLine.addStringValue("merch_l5_name");
        recordLine.addStringValue("color");
        recordLine.addStringValue("planning_type");
        recordLine.addStringValue("size");
        recordLine.addStringValue("product_sub_type");
        recordLine.addStringValue("time_in_transit");
        recordLine.addStringValue("start_date");
        recordLine.addStringValue("end_date");
        recordLine.addStringValue("ship_origin");
        recordLine.addStringValue("source_origin");
        recordLine.addStringValue("channel_id");
        recordLine.addStringValue("active_flag");
        recordLine.addStringValue("ftd_pf_sku_mapping");

        //recordCollection.addRecordLine(recordLine);
        out.println(recordLine.toString());
    }

    protected static String createBuyURLforProductFeed(String name, String productId) {
        if (name == null) {
            return name;
        }
        name = name.trim();
        name = name.replace("& ", " "); //this is required for regex on next line to function properly
        name = name.replaceAll("&.*?;", ""); //removes all CSS special characters between & and ;
        name = name.replaceAll("\\<.*?>",""); //removes html tags
        name = name.replace("-", " ");
        name = name.replaceAll("\\s+", " ");
        name = name.replace(" ", "-");

        String goodChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";
        StringBuffer result = new StringBuffer(name.length());

        for (int i = 0; i < name.length(); i++) {
            if (goodChars.indexOf(name.charAt(i)) >= 0) {
                result.append(name.charAt(i));
            }
        }

        return "http://www.ftd.com/" + result.toString().toLowerCase() + "-prd/" + productId + "/";
    }

    /**
     * Return true if the product is not a duplicate.  The data set has products and upsell data and can have duplicates if
     * the products is in more than one upsell.
     * @param pfvo
     * @return
     */
    protected boolean notDuplicate(ProductFeedVO pfvo)
    {
        String pfvoKey = productMap.get(pfvo.getProductId());
        if (pfvoKey == null)
        {
            productMap.put(pfvo.getProductId(),pfvo.getProductId());
            return true;
        }
        else
        {
            return false;
        }

    }

}
