package com.ftd.reports.impl.bankofamerica.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.partnerreward.core.IWorkflowConstants;
import com.ftd.partnerreward.core.action.ActionUtil;
import com.ftd.partnerreward.core.bo.PersistServiceBO;
import com.ftd.partnerreward.core.bo.SecurityServiceBO;
import com.ftd.partnerreward.core.text.IRecordCollection;
import com.ftd.partnerreward.core.text.IRecordLine;
import com.ftd.partnerreward.core.text.RecordCollectionFactory;
import com.ftd.partnerreward.core.text.TextUtil;
import com.ftd.partnerreward.core.vo.OrderDetailVO;
import com.ftd.reports.dao.OrderDAO;

import java.io.File;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;


/**
 * This class is responsible for creating a refund file for orders with the Bank of America source type.
 */
public class DataExtractor implements ActionHandler {
    protected Logger logger = new Logger(DataExtractor.class.getName());
    private static final String SECURE_CONFIG_CONTEXT = "partner_reward";
    
    // These fields will be initialized by the process archive.
    protected String sourceType;
    protected String filePath;
    protected String fileName;

    public DataExtractor() {
    }

    public void execute(ExecutionContext executionContext) {
        String tokenSignal = null;
        String delimiter = ",";
        
        try {
            ContextInstance context = executionContext.getContextInstance();
            Date startDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_START_DATE);
            Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);      

            List<OrderDetailVO> rewardList = new ArrayList();            

            DateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            logger.info(
                "execute: Beginning Bank of America extraction for dates from " +
                dateFormat.format(startDate) + " to " +
                dateFormat.format(endDate));

            // Transform into the needed format.
            IRecordCollection recordCollection = RecordCollectionFactory.getFixedWidthRecordCollection(false);

            CachedResultSet report = OrderDAO.queryOrderBySourceType(sourceType, startDate, endDate);

            logger.info("execute: Obtained result set rows: " + report.getRowCount());
            
            if(report.getRowCount() == 0) {
                context.setVariable(IWorkflowConstants.CONTEXT_KEY_EMPTY_RESULT_SET, "Y");
            }

            // Iterate through the result set, transferring the results
            // into a sorted Map because the business requires the final
            // results.
            while (report.next()) {
                    OrderDetailVO odVO = new OrderDetailVO();
                    odVO.setOrderDetailId(report.getString("ORDER_DETAIL_ID"));
                    odVO.setSourceCode(report.getString("SOURCE_CODE"));
                    // Delivery date is used to set as reward date on order_reward_posting for purchases.
                    // Use end date as the reward date for refunds.
                    odVO.setDeliveryDate(endDate);
                    odVO.setProgramName((String)context.getVariable(IWorkflowConstants.CONTEXT_KEY_PARTNER_NAME));
                    rewardList.add(odVO);
                    
                    IRecordLine recordLine = recordCollection.createLineInstance();
                    // Position 1: source_type.
                    recordLine.addStringValue(report.getString("SOURCE_TYPE"));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 2: source_code.
                    recordLine.addStringValue(report.getString("SOURCE_CODE"));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 3: company_id.
                     recordLine.addStringValue(report.getString("COMPANY_ID"));
                     recordLine.addStringValue(delimiter);      
                     
                    // Position 4: origin_type.
                     recordLine.addStringValue(report.getString("ORIGIN_TYPE"));
                     recordLine.addStringValue(delimiter);
                     
                    // Position 5: master_order_number.
                     recordLine.addStringValue(report.getString("MASTER_ORDER_NUMBER"));
                     recordLine.addStringValue(delimiter);
                     
                    // Position 6: item_in_cart.
                    recordLine.addStringValue(report.getString("ITEM_IN_CART"));
                    recordLine.addStringValue(delimiter);
                     
                    // Position 7: product_amount.
                    recordLine.addStringValue((report.getBigDecimal("PRODUCT_AMOUNT")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                     
                    // Position 8: add_on_amount.
                    recordLine.addStringValue((report.getBigDecimal("ADD_ON_AMOUNT")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);            
                    
                    // Position 9: shipping_fee.
                    recordLine.addStringValue((report.getBigDecimal("SHIPPING_FEE")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 10: service_fee.
                    recordLine.addStringValue((report.getBigDecimal("SERVICE_FEE")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 11: discount_amount.
                    recordLine.addStringValue((report.getBigDecimal("DISCOUNT_AMOUNT")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 12: tax
                    recordLine.addStringValue((report.getBigDecimal("TAX")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);                     
                     
                    // Position 13: total_cart_value
                    recordLine.addStringValue((report.getBigDecimal("TOTAL_CART_VALUE")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 14: aov
                    recordLine.addStringValue((report.getBigDecimal("AOV")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 15: first_name
                    recordLine.addStringValue(report.getString("FIRST_NAME"));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 16: last_name
                    recordLine.addStringValue(report.getString("LAST_NAME"));
                    recordLine.addStringValue(delimiter);
                    
                    // Position 17: order_date
                    if(report.getString("ORDER_DATE") != null) { 
                        recordLine.addStringValue(dateFormat.format(report.getDate("ORDER_DATE")));
                    }
                    recordLine.addStringValue(delimiter);
                    
                    // Position 18: cc_number_masked
                    recordLine.addStringValue(report.getString("CC_NUMBER_MASKED"));
                    recordLine.addStringValue(delimiter);   
                    
                    // Position 19: transaction_date
                    if(report.getString("TRANSACTION_DATE") != null) { 
                        recordLine.addStringValue(dateFormat.format(report.getDate("TRANSACTION_DATE")));
                    }
                    recordLine.addStringValue(delimiter);
                     
                    // Position 20: cc_auth_code
                    recordLine.addStringValue(report.getString("CC_AUTH_CODE"));
                    recordLine.addStringValue(delimiter);   
                    
                    // Position 21: transaction_amt
                    recordLine.addStringValue((report.getBigDecimal("TRANSACTION_AMT")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    recordLine.addStringValue(delimiter);
                    
                    // Position 22: merchant_city
                    recordLine.addStringValue(report.getString("MERCHANT_CITY"));
                    recordLine.addStringValue(delimiter);   

                    recordCollection.addRecordLine(recordLine);
            }
            logger.info("execute: Completed processing of current result set.");
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_CALCULATION_RESULT, rewardList);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_DATE, endDate);
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_EXTRACT_COMPLETE_IND, Boolean.valueOf(true));
            persistToFile(executionContext, recordCollection);
            
        } catch (Throwable t) {
            ActionUtil.processError(logger, executionContext, t);
            tokenSignal = IWorkflowConstants.CONTEXT_TRANSITION_ERROR;
        }

        // Advance the process token.
        if (tokenSignal == null) {
            executionContext.getToken().signal();
        } else {
            executionContext.getToken().signal(tokenSignal);
        }
    }
    
    private void persistToFile(ExecutionContext executionContext, IRecordCollection transformResult) throws Exception {

        ContextInstance context = executionContext.getContextInstance();
        //Date endDate = (Date) context.getVariable(IWorkflowConstants.CONTEXT_KEY_END_DATE);
        Date today = Calendar.getInstance().getTime();
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String securityPublicKey = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT, "BOFA_publickeyfile");
        
        logger.debug("public key path:" + securityPublicKey);        
        fileName = fileName + TextUtil.formatDate(today, "MMddyyyy") + ".csv";

        // Initialize the local destination file name.
        String fullFileNameLocal = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL);
        if (fullFileNameLocal == null) {
            fullFileNameLocal = filePath + File.separator + fileName;
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,fullFileNameLocal);
        }
            
        String remoteFileName = (String) context.getVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME);
        // Initialize the remote file name.
        if (remoteFileName == null) {
            remoteFileName = fileName + ".pgp";
            context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_NAME, remoteFileName);
            logger.debug("remoteFileName: " + remoteFileName);
        }  
        
        File localFile = new File(fullFileNameLocal);
        localFile.delete();
        
        // Write the reward records to the destination file.
        PersistServiceBO.writeFileBody(fullFileNameLocal, transformResult,
                true);
                
        SecurityServiceBO.encryptPGP(fullFileNameLocal, securityPublicKey, false, false);

        // Include the pgp extension on the filename so the encrypted file gets FTP'd
        fullFileNameLocal += ".pgp";
        context.setVariable(IWorkflowConstants.CONTEXT_KEY_POSTED_FILE_PATH_LOCAL,fullFileNameLocal);

        ActionUtil.logInfo(logger, executionContext,
                transformResult.size() +
                " records records representing all records have been successfully persisted to file " +
                fullFileNameLocal);
    }    
    
  
}
