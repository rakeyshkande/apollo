package com.ftd.reports.core;

public interface IWorkflowConstants {
    /*****************************************************/
    public static final String CONTEXT_TRANSITION_END = "end";
    public static final String CONTEXT_TRANSITION_ERROR = "error";
    public static final String CONTEXT_TRANSITION_RELOOP = "reloop";
    public static final String CONTEXT_TRANSITION_EXTRACT_COMPLETE = "complete";
    /*****************************************************/
    public static final String CONTEXT_KEY_REPORT_NAME = "keyReportName";
    public static final String CONTEXT_KEY_POSTED_FILE_NAME = "keyPostedFileName";
    public static final String CONTEXT_KEY_PARTNER_NAME = "keyPartnerName";

    public static final String CONTEXT_KEY_POSTED_FILE_PATH_LOCAL = "keyPostedFilePathLocal";
    public static final String CONTEXT_KEY_POSTED_FILE_DATE = "keyPostedFileDate";
    public static final String CONTEXT_KEY_EXTRACT_COMPLETE_IND = "keyExtractCompleteInd";
    public static final String CONTEXT_KEY_END_IND = "keyEndInd";
    public static final String CONTEXT_KEY_START_DATE = "keyStartDate";
    public static final String CONTEXT_KEY_END_DATE = "keyEndDate";

    /*****************************************************/
    public static final String CONTEXT_KEY_EXTRACTION_RESULT = "keyOrderDetailIdList";
    public static final String CONTEXT_KEY_TRANSFORMATION_RESULT = "keyTransformationResult";
    /*****************************************************/
    
}