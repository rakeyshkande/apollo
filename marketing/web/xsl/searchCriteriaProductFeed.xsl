<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  	<xsl:output method="html" indent="yes"/>
	<xsl:template name="searchCriteriaProductFeed">
  
	<xsl:param name="scActionType"/>
	<xsl:param name="scRecordSortBy"/>	
	<xsl:param name="scPageMaxRecordCount"/>	
	<xsl:param name="scRecordNumber"/>
	<xsl:param name="scProductId"/>
        <xsl:param name="scNovatorId"/>
  <!--<xsl:param name="scActiveFlag"/>-->

    <!--search metadata-->
    <input type="hidden" name="scActionType"    id="scActionType"  value="{$scActionType}"/>
    <input type="hidden" name="scRecordSortBy"          id="scRecordSortBy"        value="{$scRecordSortBy}"/>
    <input type="hidden" name="scPageMaxRecordCount"      id="scPageMaxRecordCount"    value="{$scPageMaxRecordCount}"/>
    <input type="hidden" name="scRecordNumber" 	    id="scRecordNumber"	    value="{$scRecordNumber}"/>
        
    <!-- search criteria -->
    <input type="hidden" name="scProductId" 			      id="scProductId" 		        value="{$scProductId}" />
    <input type="hidden" name="scNovatorId" 			      id="scNovatorId" 		        value="{$scNovatorId}" />
    <!--<input type="hidden" name="scActiveFlag" 			      id="scActiveFlag" 		        value="{$scActiveFlag}" />-->
	</xsl:template>	
</xsl:stylesheet>