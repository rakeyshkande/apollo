<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="productFeedSearchOptions" >
<xsl:param name="startTabIndex"/>
<xsl:param name="recordCountTotal"/>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="left"><button class="blueButton" onClick="javascript:doAdHocSearch('1', 'productId')" accesskey="s">
          <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 1"/></xsl:attribute>
              (S)earch
          </button>&nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doClear()" accesskey="c">
          <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 2"/></xsl:attribute>
              (C)lear All Fields
            </button>&nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:postProductFeed()" accesskey="p">
          <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 3"/></xsl:attribute>
              (P)ost File
            </button>
          </td>
          <td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e">
          <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 4"/></xsl:attribute>
              (E)xit
            </button></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>

</xsl:template>
</xsl:stylesheet>