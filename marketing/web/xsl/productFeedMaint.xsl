<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:import href="searchCriteriaProductFeed.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />

   <!-- The product record metrics are placed here for easy maintenance -->
   <xsl:variable name="novator_id" select="root/productList/product/@novator_id"/>
   <xsl:variable name="product_name" select="root/productList/product/@product_name"/>
   <xsl:variable name="short_description" select="root/productList/product/@short_description"/>
   <xsl:variable name="long_description" select="root/productList/product/@long_description"/>
   <xsl:variable name="status" select="root/productList/product/@status"/>
   <xsl:variable name="standard_price" select="root/productList/product/@standard_price"/>
   <!--<xsl:variable name="active_flag" select="root/productList/product/@active_flag"/>-->
   <xsl:variable name="buy_url" select="root/productList/product/@buy_url"/>
   <xsl:variable name="image_url" select="root/productList/product/@image_url"/>
   <xsl:variable name="important_product_flag" select="root/productList/product/@important_product_flag"/>
   <xsl:variable name="promotional_text" select="root/productList/product/@promotional_text"/>
   <xsl:variable name="shipping_promotional_text" select="root/productList/product/@shipping_promotional_text"/>
   <xsl:variable name="updated_on" select="root/productList/product/@updated_on"/>
   <xsl:variable name="updated_by" select="root/productList/product/@updated_by"/>

   <xsl:variable name="today" select="root/pageData/data[@name='today']/@value" />

   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Product Feed Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" language="JavaScript">
var isNewRecord = false;
promoCount = 0;
startArray = new Array();
<![CDATA[
function doBackAction() {
   if (changedFlag == 'Y' && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doSave("Y");
   } else {
   document.theform.action_type.value = "reload";
   submitAction("productFeedSearch.do");
   }
}

function doExit() {
   if (changedFlag == 'Y' && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doSave("Y");
   } else {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }
}

function init() {
   if (document.theform.buyUrl && document.theform.buyUrl.value == "") {
   isNewRecord = true; 
   }

   // set change flag to N
   changedFlag = 'N';

  focusToTop();
}

function setDefaultFieldValues() {
  // do nothing for now.
}

function focusToTop() {
  scroll(0,0);
  if (document.theform.buyUrl) {
  document.theform.buyUrl.focus();
  }
}

function trimString(inString) {
  inString = inString.replace( /^\s+/g, "" );// strip leading
  return inString.replace( /\s+$/g, "" );// strip trailing
}

function doReset() {
   document.theform.action_type.value = "load";
   submitAction("productFeedMaint.do?requestProductId=" + document.theform.requestProductId.value);
}

function doClear() {
   clearAllFields(theform);
   initInputFieldStyle();
   setDefaultFieldValues();
   focusToTop();
}

function initInputFieldStyle() {
   document.theform.buyUrl.className="input";
   document.theform.imageUrl.className="input";
   document.theform.importantProductFlag.className="input";
   document.theform.promotionalText.className="input";
   document.theform.shippingPromotionalText.className="input";
}

function FormValidator() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   this.errorMsg = msg;
}

function doSave() {
   var formValidator = new FormValidator();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
     document.theform.action_type.value = "update";
     submitAction("productFeedMaint.do");
   }
}

function submitAction(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function setChangedFlag(fieldName) {
  changedFlag = 'Y';
  //alert(fieldName);
  if (fieldName != '') {
    fieldName.value = fieldName.value.toUpperCase();
  }
}
]]>
</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Product Feed Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<input type="hidden" name="requestProductId" value="{key('pagedata','requestProductId')/@value}"/>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="result" value="{key('pagedata','result')/@value}"/>
<input type="hidden" name="invokingPage" value="ProductFeedMaint"/>
<!--<input type="hidden" name="activeFlag" value="$active_flag"/>-->
<xsl:call-template name="securityanddata"></xsl:call-template>
<xsl:call-template name="searchCriteriaProductFeed">
   <xsl:with-param name="scActionType" select="key('pagedata','scActionType')/@value"/>
   <xsl:with-param name="scRecordSortBy" select="key('pagedata','scRecordSortBy')/@value"/>
   <xsl:with-param name="scPageMaxRecordCount" select="key('pagedata','scPageMaxRecordCount')/@value"/>
   <xsl:with-param name="scRecordNumber" select="key('pagedata','scRecordNumber')/@value"/>
   <xsl:with-param name="scProductId" select="key('pagedata','scProductId')/@value"/>
   <xsl:with-param name="scNovatorId" select="key('pagedata','scNovatorId')/@value"/>
   <!--<xsl:with-param name="scActiveFlag" select="key('pagedata','scActiveFlag')/@value"/>-->
</xsl:call-template>
<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="key('pagedata','result')/@value &gt; '0'">
				   <td class="OperationSuccess">
						PRODUCT&nbsp;<xsl:value-of select="key('pagedata','requestProductId')/@value"/>&nbsp;SUCCESSFULLY SAVED
				   </td>
         </xsl:when>
         <!--<xsl:when test="key('pagedata','result')/@value &gt; '0' and $active_flag = 'N'">
           <td class="OperationSuccess">
						PRODUCT&nbsp;<xsl:value-of select="key('pagedata','requestProductId')/@value"/>&nbsp;SUCCESSFULLY DELETED
				   </td>
         </xsl:when>-->
         <xsl:when test="key('pagedata','result')/@value = '-1'">
				   <td class="errorMessage">
						PRODUCT&nbsp;<xsl:value-of select="key('pagedata','requestProductId')/@value"/>&nbsp;FAILED. RECORD ALREADY EXISTS.
				   </td>
         </xsl:when>
         </xsl:choose>
			   </tr>
</table>		   
       
<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td width="5%" align="left">
        <button class="blueButton" onClick="javascript:doSave()" tabindex="200" accesskey="u">(U)pdate</button>&nbsp;&nbsp;
        <button class="blueButton" onClick="javascript:doReset()" tabindex="201" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
      </td>
      <td class="TopHeading" align="left">Product ID: <xsl:value-of select="key('pagedata','requestProductId')/@value"/></td>
      <td align="right">
        <button class="blueButton" onClick="javascript:doExit()" tabindex="202">(E)xit</button>
      </td>
   </tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
   <tr>
      <td width="25%" class="label" align="right" valign="top">Novator ID:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="$novator_id"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Name:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="$product_name"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Short Description:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="$short_description"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Long Description:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="$long_description"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Price:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="format-number($standard_price, '###,##0.00')"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Availability:</td>
      <td width="25%" align="left" valign="top">
      <xsl:value-of select="$status"/>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Buy URL:</td>
      <td width="25%" align="left" valign="top">
      <textarea cols="50" rows="4" name="buyUrl" tabindex="1" onChange="javascript:setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$buy_url" disable-output-escaping="yes"/>
         </textarea>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Fully qualified URL to purchase the product at the merchant site. This is not the URL to the Cart, it is the URL to the product page.
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Image URL:</td>
      <td width="25%" align="left" valign="top">
      <textarea cols="50" rows="4" name="imageUrl" tabindex="2" onChange="javascript:setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$image_url" disable-output-escaping="yes"/>
         </textarea>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Fully qualified URL to an image for the product. This should link to an image file, not a web page.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Important Product:</td>
      <td align="left" valign="top">
      <input type="radio" name="importantProductFlag" value="1" onChange="setChangedFlag('')" tabindex="3">
      <xsl:if test="$important_product_flag = '1'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="importantProductFlag" value="0" onChange="setChangedFlag('')" tabindex="4">
      <xsl:if test="$important_product_flag = '0' or $important_product_flag = ''">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </td>
      <td></td>
      <td class="Instruction" align="left">
      Set this column to 1 for the top products you are selling.  This flag should be set for no more than 10% to 20% of your products.  This helps the feed provider check the products that are important to you.
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Promotional Text:</td>
      <td width="25%" align="left" valign="top">
         <textarea cols="50" rows="4" name="promotionalText" tabindex="5" onChange="javascript:setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$promotional_text" disable-output-escaping="yes"/>
         </textarea>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Product-specific promotional text.
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Shipping Promotional Text:</td>
      <td width="25%" align="left" valign="top">
         <textarea cols="50" rows="4" name="shippingPromotionalText" tabindex="6" onChange="javascript:setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$shipping_promotional_text" disable-output-escaping="yes"/>
         </textarea>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Product-specific shipping promotional text.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Last Updated On:</td>
      <td align="left" valign="top"><xsl:value-of select="$updated_on"/></td>
      <td></td>
      <td class="Instruction" align="left">
      This is the last time that a change was made to this record.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Last Updated By:</td>
      <td align="left" valign="top"><xsl:value-of select="$updated_by"/></td>
      <td></td>
      <td class="Instruction" align="left">
      This field will contain the name of the person who last edited this record.
      </td>
   </tr>
</table>

</td></tr>
</table>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td width="5%" align="left">
        <button class="blueButton" onClick="javascript:doSave('Y')" tabindex="100" accesskey="u">(U)pdate</button>&nbsp;&nbsp;
        <button class="blueButton" onClick="javascript:doReset()" tabindex="101" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
      </td>
      <td></td>
      <td align="right">
        <button class="blueButton" onClick="javascript:doExit()" tabindex="102">(E)xit</button>
      </td>
   </tr>
   <tr>
   <td></td>
   <td></td>
   <td align="right">
     <button class="blueButton" onclick="javascript:scroll(0,0)" tabindex="103" accesskey="t">(T)op of page</button>
   </td></tr>
</table>
</form>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>