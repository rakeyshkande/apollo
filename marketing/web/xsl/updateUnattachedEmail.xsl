<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Update Unattached Email Address</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/copyright.js"></script>
<script type="text/javascript" language="JavaScript">
<![CDATA[
var changedFlag = false;

function setChangedFlag() {
  changedFlag = true;
}

function retrieveData(){
  var emailAddress = document.getElementById("email_address").value;
  if (emailAddress == null || emailAddress == "")
  {
    document.theform.email_address.className="ErrorField";
    alert("Email Address is required.");
  }
  else
  {
    var url = "updateUnattachedEmail.do" +
      "?action_type=retrieve_data" + "&email_address=" + emailAddress + getSecurityParams(false);
  
    performAction(url);
  }
}

function performAction(url)
{
  document.forms[0].action = url;
  document.forms[0].target = window.name;
  document.forms[0].submit();
}

function doExit() {
   if (changedFlag && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
      doSave();
    }
    else
    {
      document.theform.action = "mainMenu.do";
      document.theform.target = "_top";
      document.theform.submit();
    }
}

function doSave() {
  
  var emailAddress = document.getElementById("email_address_hidden").value;
	var url = "updateUnattachedEmail.do" +"?action_type=update" + "&email_address=" + emailAddress;
  var div = document.getElementById("companyListDiv");
  var list = div.getElementsByTagName("INPUT");
  var numUpdates = 0;
  for (var i = 0; i < list.length; i ++)
  {
    if (list[i].type.toUpperCase() == "CHECKBOX")
    {
      if ((list[i].checked == true)&&(list[i].checked.toString() != list[i].initiallyChecked))
      {
        url = url + "&company_id_"+numUpdates+"="+list[i].companyId + "&subscribe_status_"+numUpdates+"="+list[i].value;
        numUpdates = numUpdates + 1;
     }
    }
  }
  url = url + "&num_updates="+numUpdates;
  url = url + getSecurityParams(false);
  performAction(url);
}

function onCheckboxChange(checkbox)
{
  var list = document.getElementsByName(checkbox.id);
  for (var i = 0; i < list.length; i ++)
  {
    if (list[i] != checkbox)
    {
      list[i].checked = !checkbox.checked;
    }
  }
}

]]>
</script>
</head>

<body>

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Update Unattached Email Address'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="false()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="email_address_hidden" value="{//data[@name='email_address']/@value}" />
<div id="errorMessageDiv">
  <xsl:choose>
  <xsl:when test="key('pagedata','has_customer_record')/@value = 'true'">
    <font class="ErrorMessage">
    &nbsp;&nbsp;EMAIL ADDRESS IS ASSIGNED TO A CUSTOMER, PLEASE UPDATE THE ADDRESS ON THE CUSTOMER ACCOUNT SCREEN.  
    </font>
  </xsl:when>
  <xsl:when test="key('pagedata','no_records')/@value = 'true'">
    <font class="ErrorMessage">
      &nbsp;&nbsp;EMAIL ADDRESS DOES NOT EXIST IN THE SYSTEM. 
    </font>
  </xsl:when>
  <xsl:when test="key('pagedata','validation_success')/@value = 'true'">
    <font class="OperationSuccess">
      &nbsp;&nbsp;SUBSCRIPTIONS SAVED.
    </font>
  </xsl:when>
  </xsl:choose>
</div>
<table border="0" width="98%"><tr><td>
<table width="98%" border="3" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>
<table width="98%" border="0" cellpadding="2" cellspacing="0">
  <tr>
  	<td colspan="3">&nbsp;</td>
  </tr>
  
  <tr>
    <td class="label" width="20%" align="right">Email Address:&nbsp;</td>
    <td align="left" width="80%">
     <xsl:choose>
       <xsl:when test="//company">
          <xsl:value-of select="//data[@name='email_address']/@value"/>
      </xsl:when>
      <xsl:otherwise>
        <input type="text" name="email_address" size="55" maxlength="55" tabindex="1" value="{//data[@name='email_address']/@value}"/>&nbsp;&nbsp;
        <button class="blueButton" onClick="javascript:retrieveData()" accesskey="R" tabindex="2">(R)etrieve Data</button>
      </xsl:otherwise>
      </xsl:choose>
    </td>
  </tr>
  <tr>
  	<td colspan="3">&nbsp;</td>
  </tr>
  </table>
  <xsl:if test="//company">
  <div id="companyListDiv">
  <table width="98%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td width="10%">&nbsp;</td>
    <!--<td class="label" width="20%">Brands</td>-->
    <td class="label" width="20%">Current Status</td>
    <td class="label" width="20%">Subscribe</td>
    <td class="label" width="20%">Unsubscribe</td>
  </tr>
  <!--<xsl:for-each select="//company">-->
    <xsl:variable name="companyId" select="//company/@companyid"/>
    <xsl:variable name="subscribeStatus" select="//email[@company_id = $companyId]/@subscribe_status"/>
    <tr>
      <td width="150">&nbsp;</td>
      <!--<td><xsl:value-of select="//company/@companyname"/></td>-->
      <td>
        <xsl:choose>
          <xsl:when test="$subscribeStatus"><xsl:value-of select="$subscribeStatus"/></xsl:when>
          <xsl:otherwise>Unsubscribe</xsl:otherwise>
        </xsl:choose>
      </td>
      <td>
        <input type="checkbox" value="Subscribe" onclick = "javascript: onCheckboxChange(this)" companyId = "{//company/@companyid}" onChange="setChangedFlag()">
        <xsl:attribute name="tabIndex"><xsl:value-of select="((//company/@num* 2)-1) +100"/></xsl:attribute>
        <xsl:attribute name="name">company_<xsl:value-of select="//company/@companyid"/></xsl:attribute>
        <xsl:attribute name="id">subscribeStatus_<xsl:value-of select="//company/@companyid"/></xsl:attribute>
        <xsl:choose>
        <xsl:when test="$subscribeStatus = 'Subscribe'">
          <xsl:attribute name="checked">true</xsl:attribute>
          <xsl:attribute name="initiallyChecked">true</xsl:attribute>
        </xsl:when>
        <xsl:otherwise><xsl:attribute name="initiallyChecked">false</xsl:attribute></xsl:otherwise>
        </xsl:choose>
        </input>
      </td>
      <td>
        <input type="checkbox" value="Unsubscribe" onclick = "javascript: onCheckboxChange(this)" companyId = "{//company/@companyid}" onChange="setChangedFlag()">
        <xsl:attribute name="tabIndex"><xsl:value-of select="((//company/@num* 2)) +100"/></xsl:attribute>
          <xsl:attribute name="name">company_<xsl:value-of select="//company/@companyid"/></xsl:attribute>
          <xsl:attribute name="id">subscribeStatus_<xsl:value-of select="//company/@companyid"/></xsl:attribute>
          <xsl:if test="not($subscribeStatus) or $subscribeStatus = 'Unsubscribe'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          <xsl:choose>
          <xsl:when test="not($subscribeStatus) or $subscribeStatus = 'Unsubscribe'">
            <xsl:attribute name="initiallyChecked">true</xsl:attribute>
            <xsl:attribute name="checked">true</xsl:attribute>
          </xsl:when>
          <xsl:otherwise><xsl:attribute name="initiallyChecked">false</xsl:attribute></xsl:otherwise>
          </xsl:choose>
        </input>
      </td>
    </tr>
  <!--</xsl:for-each>-->
  </table>  
  </div>
  </xsl:if>
 </td></tr>
</table>
</td></tr><tr><td>
 <table width="98%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td align="left">
      <xsl:if test="//company">
        <button class="blueButton" onClick="javascript:doSave()" accesskey="s" tabindex="300">(S)ave</button>
      </xsl:if>
    </td>
    <td align="right">
      <button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="301">(E)xit</button>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  </table>  
  </td></tr></table>
</form>
<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>

   </xsl:template>
</xsl:stylesheet>
