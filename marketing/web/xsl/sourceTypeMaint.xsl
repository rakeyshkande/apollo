<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<title>
        Source Type Maintenance
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<style type="text/css">
#sourceTypeListAllDiv {display: block;}
#sourceTypeListTempDiv {display: none;}
select.fixedWidthSelect { width:300px } 
</style>

<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">
var changedFlag = false;
function doBackAction() {
   history.back();
}

function setChangedFlag() {
  changedFlag = true;
}
<xsl:choose>
<xsl:when test="key('pagedata','SourceTypeData')/@value = 'Y'">
<![CDATA[
function doExit() {
   if (changedFlag && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doAdd();
   } else {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }
}
]]>
</xsl:when>
<xsl:otherwise>
function doExit() {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
}
</xsl:otherwise>
</xsl:choose>
function initInputFieldStyle() {
    document.theform.sourceType.className="input";
}

function validateForm() {
   initInputFieldStyle();
   var msg = "";  
   if (document.theform.sourceType.value == "") {
       msg = msg + "Source Type is required.\n";
       document.theform.sourceType.className="ErrorField";
       document.theform.sourceType.focus();
   }
   
   return msg;
}

function doClear() {
   initInputFieldStyle();
   document.theform.sourceType.value = "";
   obj1.bldUpdate();
   if (document.all.resultMsgDiv) {
       document.all.resultMsgDiv.style.display = "none";
   }
   changedFlag = false;
   document.theform.sourceType.focus();
}

<xsl:if test="key('pagedata','SourceTypeData')/@value = 'Y'">
function doAdd() {
   var errorMsg = validateForm();
   if (errorMsg != "") {
     alert(errorMsg);
   } else {
     document.theform.sourceType.value = document.theform.sourceType.value.toUpperCase();
     document.theform.action_type.value = "insert";
     submitAction();
   }
}
</xsl:if>

function viewAll() {
   document.theform.action_type.value = "search";
   submitActionNewWindow("sourceTypeMaint.do");
}

function submitAction() {
   document.theform.action = "sourceTypeMaint.do";
   document.theform.target = "";
   document.theform.submit();
}

function submitActionNewWindow(url) {
   var winName = "win" + Math.round(1000*Math.random());
   window.open('',winName,'scrollbars=0,menubar=1,toolbar=0,location=0,status=0,resizable=1'); 
   document.theform.action = url;
   document.theform.target = winName;
   document.theform.submit();
}

function SelObj(formname,selname,textname,str) {
   this.formname = formname;
   this.selname = selname;
   this.textname = textname;
   this.select_str = str || '';
   this.selectArr = new Array();
   this.initialize = initialize;
   this.bldInitial = bldInitial;
   this.bldUpdate = bldUpdate;
}

function initialize() {
   
   var isRefresh = false;
   if (this.select_str == '') {
   
      // Read ALL values from the "master" source type list sourceTypeListAll.
      for(var i=0;i &lt; document.theform.sourceTypeListAll.options.length;i++) {
         this.selectArr[i] = document.theform.sourceTypeListAll.options[i];
         this.select_str += document.theform.sourceTypeListAll.options[i].value+":"+
            document.theform.sourceTypeListAll.options[i].text+",";
      }
      
      isRefresh = true;
   }
   else {
      var tempArr = this.select_str.split(',');
      for(var i=0;i &lt; tempArr.length;i++) {
         var prop = tempArr[i].split(':');
         this.selectArr[i] = new Option(prop[1],prop[0]);
      }
   }
   return isRefresh;
}

function bldInitial() {
     if (this.initialize() == false) {
       for(var i=0;i &lt; this.selectArr.length;i++) {
          document.forms[this.formname][this.selname].options[i] = this.selectArr[i];
          document.forms[this.formname][this.selname].options.length = this.selectArr.length;
       }
      }
   return;
}

function bldUpdate() {
   var str = document.forms[this.formname][this.textname].value.replace('^\\s*','');
   if(str == '') {
      
      // To eliminate the need to render the full list to screen, simply
      // manipulate the visibility.
      document.all.sourceTypeListAllDiv.style.display = "block";
      document.all.sourceTypeListTempDiv.style.display = "none";
    
      return;
   }
   this.initialize();
   document.all.sourceTypeListAllDiv.style.display = "none";
   document.all.sourceTypeListTempDiv.style.display = "block";
        
   var j = 0;
   pattern1 = new RegExp("^"+str,"i");
   for(var i=0;i &lt; this.selectArr.length;i++) {
      if(pattern1.test(this.selectArr[i].text)) {
         document.forms[this.formname][this.selname].options[j++] = this.selectArr[i];
      }
   }
   document.forms[this.formname][this.selname].options.length = j;
   // JP Puzon -- Removed any highlight of list members.
   //if(j==1){
      //document.forms[this.formname][this.selname].options[0].selected = true;
   //}
}

function setUp() {
   obj1 = new SelObj('theform','sourceTypeListTemp','sourceType');
   // menuform is the name of the form you use
   // itemlist is the name of the select pulldown menu you use
   // entry is the name of text box you use for typing in
   obj1.bldInitial(); 
   document.theform.sourceType.focus();
}

</script>
</head>
<body OnLoad="javascript:setUp()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Source Type Maintenance'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
<form name="theform" method="post" action="">
        <input type="hidden" name="action_type" value="{key('pagedata','action_type')/value}"/>
        <input type="hidden" name="result" value="{key('pagedata','result')/value}"/>
        <xsl:call-template name="securityanddata"></xsl:call-template>
        <div id="content" style="display:block">
		   <div id="resultMsgDiv">
       <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="key('pagedata','result')/@value &gt; '0'">
				   <td class="OperationSuccess">
						SOURCE TYPE&nbsp;<xsl:value-of select="key('pagedata','sourceType')/@value"/>&nbsp;SUCCESSFULLY SAVED
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-1'">
				   <td class="errorMessage">
						SOURCE TYPE&nbsp;<xsl:value-of select="key('pagedata','sourceType')/@value"/>&nbsp;FAILED. RECORD ALREADY EXISTS.
				   </td>
         </xsl:when>
         </xsl:choose>
			   </tr>
		   </table>		   
       </div>
        <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
          <tr>
            <td><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td class="label" align="right">Source Type:</td>
                  <td align="left"><input type="text" name="sourceType" tabindex="1" size="40" maxlength="40" value="{key('pagedata','sourceType')/@value}" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" onKeyUp="javascript:obj1.bldUpdate();"/></td>
                </tr>
                <tr>
                  <td class="label" align="right" valign="top">Matching Source Types:</td>
                  <td><div id="sourceTypeListAllDiv">
                      <select name="sourceTypeListAll" size="20" tabindex="-1" class="fixedWidthSelect">
                        <xsl:for-each select="root/sourceTypeList/sourceType">
                          <option>
                            <xsl:value-of select="@source_type"/>
                          </option>
                        </xsl:for-each>
                      </select>
                    </div>
                    <div id="sourceTypeListTempDiv">
                      <select name="sourceTypeListTemp" size="20" tabindex="-1" class="fixedWidthSelect">
                      </select>
                    </div></td>
                <td width="5%"></td>
      <td class="Instruction" align="left" valign="top">
      As a Source Type string is entered, the list box will dynamically display any existing Source Types that match the entered value. If no Source Types match the user input, the list box will be empty. Source Type values are saved in UPPERCASE. 
      </td>
                </tr>
                <tr>
                  <td colspan="4">&#160;</td>
                </tr>
                <tr>
                  <td align="right"></td>
                  <td align="left"></td>
                  <td align="left"></td>
                </tr>
                <tr>
                  <td colspan="4">&#160;</td>
                </tr>
              </table></td>
          </tr>
        </table>
        </div>
      </form>
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="left">
          <xsl:if test="key('pagedata','SourceTypeData')/@value = 'Y'">
          <button class="blueButton" onClick="javascript:doAdd()" accesskey="s" tabindex="50">
              (S)ave
            </button>
          </xsl:if>  
          &nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:viewAll()" accesskey="v" tabindex="51">
              (V)iew All
            </button>
          &nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doClear()" accesskey="c" tabindex="52">
              (C)lear All Fields
            </button></td>
          <td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="53">
              (E)xit
            </button></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
<xsl:call-template name="footer"></xsl:call-template>
</body></html>  </xsl:template>
  </xsl:stylesheet>
  