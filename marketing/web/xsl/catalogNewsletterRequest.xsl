<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<!--  <xsl:import href="pagination.xsl"/> --> <!--  This is unused -->
<xsl:import href="sourceSearchOptions.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="result" select="number(key('pagedata','result')/@value)"/>
<xsl:variable name="daytimephone" select="key('pagedata','daytimePhone')/@value"/>
<xsl:variable name="eveningphone" select="key('pagedata','eveningPhone')/@value"/>
<xsl:variable name="hid_daytimephone" select="key('pagedata','hid_daytimePhone')/@value"/>
<xsl:variable name="hid_eveningphone" select="key('pagedata','hid_eveningPhone')/@value"/>
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<title>
        Catalog/Newsletter Request
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<!--<link rel="stylesheet" type="text/css" href="css/calendar.css"/>-->
<style type="text/css">
		    /* Scrolling table overrides for the search results table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" language="JavaScript">
<![CDATA[
var changedFlag = false;
function initInputFieldStyle() {
    document.theform.companyCode.className="input";
    document.theform.firstName.className="input";
    document.theform.lastName.className="input";
    document.theform.address1.className="input";
    document.theform.city.className="input";
    document.theform.state.className="input";
    document.theform.zipCode.className="input";
    document.theform.country.className="input";
    document.theform.daytimePhone.className="input";
    document.theform.daytimeExt.className="input";
    document.theform.eveningPhone.className="input";
    document.theform.eveningExt.className="input";
    document.theform.emailAddress.className="input";
}

// This is an object that validates the form fields.
function FormValidator() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.companyCode.value == "") {
       msg = msg + "Company Code is required.\n";
       if (document.theform.companyCode.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.companyCode.tabIndex;
        this.focusOnField = document.theform.companyCode.name;
       }
       document.theform.companyCode.className="ErrorField";
   }
   if (document.theform.firstName.value == "") {
       msg = msg + "First Name is required.\n";
       if (document.theform.firstName.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.firstName.tabIndex;
        this.focusOnField = document.theform.firstName.name;
       }
       document.theform.firstName.className="ErrorField";
   }
   if (document.theform.lastName.value == "") {
       msg = msg + "Last Name is required.\n";
       if (document.theform.lastName.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.lastName.tabIndex;
        this.focusOnField = document.theform.lastName.name;
       }
       document.theform.lastName.className="ErrorField";
   }
   if (document.theform.address1.value == "") {
       msg = msg + "Address is required.\n";
       if (document.theform.address1.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.address1.tabIndex;
        this.focusOnField = document.theform.address1.name;
       }
       document.theform.address1.className="ErrorField";
   }
   if (document.theform.city.value == "") {
       msg = msg + "City is required.\n";
       if (document.theform.city.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.city.tabIndex;
        this.focusOnField = document.theform.city.name;
       }
       document.theform.city.className="ErrorField";
   }
   if (document.theform.state.value == "") {
       msg = msg + "State is required.\n";
       if (document.theform.state.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.state.tabIndex;
        this.focusOnField = document.theform.state.name;
       }
       document.theform.state.className="ErrorField";
   }   
   if (document.theform.zipCode.value == "") {
       msg = msg + "Zip Code is required.\n";
       if (document.theform.zipCode.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.zipCode.tabIndex;
        this.focusOnField = document.theform.zipCode.name;
       }
       document.theform.zipCode.className="ErrorField";
   }
   if (document.theform.country.value == "") {
       msg = msg + "Country is required.\n";
       if (document.theform.country.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.country.tabIndex;
        this.focusOnField = document.theform.country.name;
       }
       document.theform.country.className="ErrorField";
   }
   if (document.theform.daytimePhone.value == "") {
       msg = msg + "Phone 1 is required.\n";
       if (document.theform.daytimePhone.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.daytimePhone.tabIndex;
        this.focusOnField = document.theform.daytimePhone.name;
       }
       document.theform.daytimePhone.className="ErrorField";
   }
   if (document.theform.daytimePhone.value != "" && 
   (isNaN(document.theform.daytimePhone.value.replace(/[\s()-]+/g, "")) ||
   document.theform.daytimePhone.value.replace(/[\s()-]+/g, "").length != 10)) {
       msg = msg + "Phone 1 must be a 10-digit numeric.\n";
       if (document.theform.daytimePhone.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.daytimePhone.tabIndex;
        this.focusOnField = document.theform.daytimePhone.name;
       }
       document.theform.daytimePhone.className="ErrorField";
   }
   if (document.theform.daytimeExt.value != null && isNaN(document.theform.daytimeExt.value)) {
       msg = msg + "Phone 1 Extension must be numeric.\n";
       if (document.theform.daytimeExt.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.daytimeExt.tabIndex;
        this.focusOnField = document.theform.daytimeExt.name;
       }
       document.theform.daytimeExt.className="ErrorField";
   }
   if (document.theform.eveningPhone.value != "" && 
   (isNaN(document.theform.eveningPhone.value.replace(/[\s()-]+/g, "")) ||
   document.theform.eveningPhone.value.replace(/[\s()-]+/g, "").length != 10)) {
       msg = msg + "Phone 2 must be a 10-digit numeric.\n";
       if (document.theform.eveningPhone.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.eveningPhone.tabIndex;
        this.focusOnField = document.theform.eveningPhone.name;
       }
       document.theform.eveningPhone.className="ErrorField";
   }
   if (document.theform.eveningExt.value != null && isNaN(document.theform.eveningExt.value)) {
       msg = msg + "Phone 2 Ext must be numeric.\n";
       if (document.theform.eveningExt.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.eveningExt.tabIndex;
        this.focusOnField = document.theform.eveningExt.name;
       }
       document.theform.eveningExt.className="ErrorField";
   }
   if (document.theform.emailAddress.value != "" && 
   document.theform.emailAddress.value.indexOf("@") == -1) {
       msg = msg + "Email must have the correct @ format.\n";
       if (document.theform.emailAddress.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.emailAddress.tabIndex;
        this.focusOnField = document.theform.emailAddress.name;
       }
       document.theform.emailAddress.className="ErrorField";
   }
   
   this.errorMsg = msg;
}

function doAdd() {
   var formValidator = new FormValidator();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
     document.theform.action_type.value = "insert";
     // Clean-up the phone number fields.
     document.theform.daytimePhone.value = document.theform.daytimePhone.value.replace(/[\s()-]+/g, "");
     document.theform.eveningPhone.value = document.theform.eveningPhone.value.replace(/[\s()-]+/g, "");
     submitAction();
   }
}

function doUpdate() {
    document.theform.action_type.value = "update";
    submitAction();
}

function submitAction() {
   document.theform.action = "catNewsRequest.do";
   document.theform.target = "";
   document.theform.submit();
}

function getDate() {
   dte = new Date();
   today = dte.getMonth()+1 + "/" + dte.getDate() + "/" + dte.getYear();
   return today;
}

function doClear() {
   changedFlag = false;
   initInputFieldStyle();
   clearAllFields(theform);
   init();
}

function init() {
  if (document.theform.firstName) {
  document.theform.firstName.focus();
  }
}

function setChangedFlag() {
  changedFlag = true;
}

function doExit() {
   if (changedFlag && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doAdd();
   } else {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }
}
]]>
</script>
</head>
<body onLoad="init()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Catalog/Newsletter Request'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
<form name="theform" method="post" action="">
        <input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
        <input type="hidden" name="customer_id" value="{key('pagedata','customer_id')/@value}"/>
        <xsl:choose>
        <xsl:when test="$result = -2">
            <input type="hidden" name="hid_eveningPhone" value="{key('pagedata','hid_eveningPhone')/@value}"/>
            <input type="hidden" name="hid_daytimePhone" value="{key('pagedata','hid_daytimePhone')/@value}"/>
            <input type="hidden" name="hid_address1" value="{key('pagedata','hid_address1')/@value}"/>
            <input type="hidden" name="hid_firstName" value="{key('pagedata','hid_firstName')/@value}"/>
            <input type="hidden" name="hid_lastName" value="{key('pagedata','hid_lastName')/@value}"/>
            <input type="hidden" name="hid_address2" value="{key('pagedata','hid_address2')/@value}"/>
            <input type="hidden" name="hid_zipCode" value="{key('pagedata','hid_zipCode')/@value}"/>
            <input type="hidden" name="hid_eveningExt" value="{key('pagedata','hid_eveningExt')/@value}"/>
            <input type="hidden" name="hid_country" value="{key('pagedata','hid_country')/@value}"/>
            <input type="hidden" name="hid_emailAddress" value="{key('pagedata','hid_emailAddress')/@value}"/>
            <input type="hidden" name="hid_companyCode" value="{key('pagedata','hid_companyCode')/@value}"/>
            <input type="hidden" name="hid_state" value="{key('pagedata','hid_state')/@value}"/>
            <input type="hidden" name="hid_daytimeExt" value="{key('pagedata','hid_daytimeExt')/@value}"/>
            <input type="hidden" name="hid_city" value="{key('pagedata','hid_city')/@value}"/>
        </xsl:when>
        <xsl:otherwise>
            <input type="hidden" name="hid_eveningPhone" value="{key('pagedata','eveningPhone')/@value}"/>
            <input type="hidden" name="hid_daytimePhone" value="{key('pagedata','daytimePhone')/@value}"/>
            <input type="hidden" name="hid_address1" value="{key('pagedata','address1')/@value}"/>
            <input type="hidden" name="hid_firstName" value="{key('pagedata','firstName')/@value}"/>
            <input type="hidden" name="hid_lastName" value="{key('pagedata','lastName')/@value}"/>
            <input type="hidden" name="hid_address2" value="{key('pagedata','address2')/@value}"/>
            <input type="hidden" name="hid_zipCode" value="{key('pagedata','zipCode')/@value}"/>
            <input type="hidden" name="hid_eveningExt" value="{key('pagedata','eveningExt')/@value}"/>
            <input type="hidden" name="hid_country" value="{key('pagedata','country')/@value}"/>
            <input type="hidden" name="hid_emailAddress" value="{key('pagedata','emailAddress')/@value}"/>
            <input type="hidden" name="hid_companyCode" value="{key('pagedata','companyCode')/@value}"/>
            <input type="hidden" name="hid_state" value="{key('pagedata','state')/@value}"/>
            <input type="hidden" name="hid_daytimeExt" value="{key('pagedata','daytimeExt')/@value}"/>
            <input type="hidden" name="hid_city" value="{key('pagedata','city')/@value}"/>
        </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="securityanddata"></xsl:call-template>
<div id="content" style="display:block">
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="$result &gt; 0">
				   <td class="OperationSuccess">
						CUSTOMER SUCCESSFULLY SAVED.
				   </td>
           </xsl:when>
           <xsl:when test="$result = -1">
				   <td class="errorMessage">
						CUSTOMER ALREADY EXISTS. UPDATE CUSTOMER THROUGH CUSTOMER ACCOUNT SCREEN. 
				   </td>
           </xsl:when>
           <xsl:when test="$result = -2">
				   <td class="errorMessage">
						<xsl:value-of select="key('pagedata','lock_error')/@value"/> 
				   </td>
           </xsl:when>
           </xsl:choose>
			   </tr>
		   </table>	
        <table width="100%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<!--<tr>
   <td width="200">&space;</td>
   <td width="300" align="left" class="Instruction">
      <xsl:if test="key('pagedata','result')/value !=''">
         <script type="text/javascript">document.write(theform.result.value);</script>
      </xsl:if>
      &space;
   </td>
   <td width="250" align="left">&space;</td>
</tr>-->
<tr>
   <td class="label" align="right" style="display:none;">Company/Brand<font color="red">*&space;</font>:</td>
   <td align="left">
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','companyCode')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_companyCode')/@value"/>
         </xsl:when>
         <xsl:otherwise>
     <select name="companyCode" onChange="setChangedFlag()" tabindex="1" style="display:none;">
        <!--<option value=""></option>-->
        <!--<xsl:for-each select="root/companyList/company">-->
        <option value="{root/companyList/company/@companyid}"><xsl:value-of select="root/companyList/company/@companyname"/></option>
        <!--</xsl:for-each>-->
     </select>
     </xsl:otherwise>
     </xsl:choose>
   </td>
   <td align="left" class="label">
   Request Date:
   <script type="text/javascript">document.write(getDate());</script>
   </td>
</tr>
<tr>
   <td colspan="4" class="colHeader">&space;&space;Recipient Information</td>
</tr>
<tr>

   <td class="label" align="right">First Name<font color="red">*&space;</font>:</td>
   <td>

   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','firstName')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_firstName')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="firstName" size="20" maxlength="20" tabindex="2" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
   <!--<td class="Instruction">
   <xsl:if test="key('pagedata','result')/value !=''">
       <script type="text/javascript">document.write(theform.result.value);</script>
   </xsl:if>
   </td>-->
</tr>
<tr>
   <td class="label" align="right">Last Name<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','lastName')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_lastName')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="lastName" size="20" maxlength="20" tabindex="3" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Address 1<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','address1')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_address1')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="address1" size="30" maxlength="30" tabindex="4" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Address 2 :</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','address2')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_address2')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="address2" size="30" maxlength="30" tabindex="5" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">City<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','city')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_city')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="city" size="30" maxlength="30" tabindex="6" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">State<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','state')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_state')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <select name="state" tabindex="7" onChange="setChangedFlag()" >
      <option value=""></option>
      <xsl:for-each select="root/stateList/state">
        <option value="{@statemasterid}"><xsl:value-of select="@statename"/></option>
      </xsl:for-each>
    </select>
    </xsl:otherwise>
    </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Zip Code<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','zipCode')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_zipCode')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <input type="text" name="zipCode" size="12" maxlength="12" tabindex="8" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
   </xsl:otherwise>
   </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Country<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','country')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_country')/@value"/>
         </xsl:when>
         <xsl:otherwise>
   <select name="country" tabindex="9" onChange="setChangedFlag()" >
      <option value=""></option>
      <xsl:for-each select="root/countryList/country">
        <xsl:if test="@country_id = 'US' or @country_id = 'CA'">
        <option value="{@country_id}"><xsl:value-of select="@name"/></option>
        </xsl:if>
      </xsl:for-each>
    </select>
    </xsl:otherwise>
    </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Phone 1<font color="red">*&space;</font>:</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:if test="string-length($daytimephone) &gt; '0'">
         (<xsl:value-of select="substring($daytimephone,1,3)"/>)&nbsp;<xsl:value-of select="substring($daytimephone,4,3)"/>-<xsl:value-of select="substring($daytimephone,7,4)"/>
         </xsl:if>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:if test="string-length($hid_daytimephone) &gt; '0'">
         (<xsl:value-of select="substring($hid_daytimephone,1,3)"/>)&nbsp;<xsl:value-of select="substring($hid_daytimephone,4,3)"/>-<xsl:value-of select="substring($hid_daytimephone,7,4)"/>
         </xsl:if>
         </xsl:when>
         <xsl:otherwise>
       <input type="text" name="daytimePhone" size="20" maxlength="20" tabindex="10" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
       </xsl:otherwise>
       </xsl:choose>
       &space;<font class="label">Ext :</font>&space;
       <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','daytimeExt')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_daytimeExt')/@value"/>
         </xsl:when>
         <xsl:otherwise>
       <input type="text" name="daytimeExt" size="10" maxlength="10" tabindex="11" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
       </xsl:otherwise>
       </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Phone 2 :</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:if test="string-length($eveningphone) &gt; '0'">
         (<xsl:value-of select="substring($eveningphone,1,3)"/>)&nbsp;<xsl:value-of select="substring($eveningphone,4,3)"/>-<xsl:value-of select="substring($eveningphone,7,4)"/>
         </xsl:if>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:if test="string-length($hid_eveningphone) &gt; '0'">
         (<xsl:value-of select="substring($hid_eveningphone,1,3)"/>)&nbsp;<xsl:value-of select="substring($hid_eveningphone,4,3)"/>-<xsl:value-of select="substring($hid_eveningphone,7,4)"/>
         </xsl:if>
         </xsl:when>
         <xsl:otherwise>
       <input type="text" name="eveningPhone" size="20" maxlength="20" tabindex="12" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
       </xsl:otherwise>
       </xsl:choose>
       &space;<font class="label">Ext :</font>&space;
       <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','eveningExt')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_eveningExt')/@value"/>
         </xsl:when>
         <xsl:otherwise>
       <input type="text" name="eveningExt" size="10" maxlength="10" tabindex="13" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
       </xsl:otherwise>
       </xsl:choose>
   </td>
</tr>
<tr>
   <td class="label" align="right">Email Address :</td>
   <td>
   <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1">
         <xsl:value-of select="key('pagedata','emailAddress')/@value"/>
         </xsl:when>
         <xsl:when test="$result = -2">
         <xsl:value-of select="key('pagedata','hid_emailAddress')/@value"/>
         </xsl:when>
         <xsl:otherwise>
    <input type="text" name="emailAddress" size="55" maxlength="55" tabindex="14" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"></input>
    </xsl:otherwise>
    </xsl:choose>
    </td>
</tr>
<!--<tr><td colspan="3">&space;</td></tr>
<tr>
   <td align="right"></td>
   <td align="left">
       <button class="blueButton" onClick="javascript:doAdd()" tabindex="15">(A)dd</button>
       &space;&space;&space;
       <button class="blueButton" onClick="javascript:doClear()" tabindex="16">(C)lear All Fields</button>
   </td>
   <td align="left"><font color="red">* = Required Field</font></td>
</tr>
<tr><td colspan="3">&space;</td></tr>-->

</table>
</td></tr>
</table>
        </div>
      </form>
<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>
       <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        <xsl:choose>
         <xsl:when test="$result &gt; 0 or $result = -1 or $result = -2">
				   <td class="OperationSuccess">
						If you would like to edit this Customer Record:&nbsp;<button class="blueButton" onClick="javascript:doUpdate()" accesskey="u" tabindex="50">(U)pdate Customer Account</button>
				   </td>
           </xsl:when>
          <xsl:otherwise>
          <td align="left"><button class="blueButton" onClick="javascript:doAdd()" accesskey="s" tabindex="50">
              (S)ave
            </button>
          <!--&nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doSearch()" accesskey="v" tabindex="51">
              (V)iew All
            </button>-->
          &nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doClear()" accesskey="c" tabindex="52">
              (C)lear All Fields
            </button></td>
          </xsl:otherwise>
          </xsl:choose>
          <td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="53">
              (E)xit
            </button></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>      
<xsl:call-template name="footer"></xsl:call-template>
</body></html>
  </xsl:template>
  </xsl:stylesheet>
  