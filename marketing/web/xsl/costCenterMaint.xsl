<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   
   <!-- Retrieve Promotion values -->
   <xsl:variable name="program" select="root/pageData/data[@name='program']/@value" />
   <xsl:variable name="entity" select="root/pageData/data[@name='entity']/@value" />
   <xsl:variable name="location" select="root/pageData/data[@name='location']/@value" />
   <xsl:variable name="expenseCenter" select="root/pageData/data[@name='expenseCenter']/@value" />
   <xsl:variable name="account" select="root/pageData/data[@name='account']/@value" />
   <xsl:variable name="costCenter" select="root/pageData/data[@name='costCenter']/@value" />
   <xsl:variable name="glNumber" select="root/pageData/data[@name='glNumber']/@value" />
   <xsl:variable name="sourceCode" select="root/pageData/data[@name='sourceCode']/@value" />
      
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Cost Center Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">
<![CDATA[

function doBackAction() {
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function doExit() {
  if (madeChanges) {
    if (!confirm("Are you sure you want to exit without saving?")) {
      return;
    }
  }
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function init() {
  programChange();
  document.theform.program.focus();
  madeChanges = false;
}

function submitAction(url) {
  document.theform.action = url;
  document.theform.target = "";
  document.theform.submit();
}

function doUpdate() {
  document.getElementById('SourceCodeNOF').style.display = 'none';
  document.theform.program.className="input";
  if (document.theform.program.value == '') {
    document.theform.program.className="ErrorField";
    alert("Program Name is required");
    document.theform.program.focus();
    return;
  }

  this.minTabIndex = 99999;
  var msg = "";

  if (document.theform.program.value == 'ADVO') {
    document.theform.entity.className="input";
    document.theform.location.className="input";
    document.theform.costCenter.className="input";
    document.theform.glNumber.className="input";
    temp=document.theform.entity.value;
    if (temp.length != 2) {
      document.theform.entity.className="ErrorField";
      msg = msg + "Entity# must be 2 digits\n";
      if (document.theform.entity.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.entity.tabIndex;
        document.theform.entity.focus();
      }
    }
    if (!isNumeric(temp)) {
      document.theform.entity.className="ErrorField";
      msg = msg + "Entity# must be numeric\n";
      if (document.theform.entity.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.entity.tabIndex;
        document.theform.entity.focus();
      }
    }
    temp=document.theform.location.value;
    if (temp.length != 3) {
      document.theform.location.className="ErrorField";
      msg = msg + "Location# must be 3 digits\n";
      if (document.theform.location.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.location.tabIndex;
        document.theform.location.focus();
      }
    }
    temp=document.theform.costCenter.value;
    if (temp.length != 4) {
      document.theform.costCenter.className="ErrorField";
      msg = msg + "Cost Center# must be 4 digits\n";
      if (document.theform.costCenter.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.costCenter.tabIndex;
        document.theform.costCenter.focus();
      }
    }
    temp=document.theform.glNumber.value;
    if (temp.length != 6) {
      document.theform.glNumber.className="ErrorField";
      msg = msg + "G/L# must be 6 digits\n";
      if (document.theform.glNumber.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.glNumber.tabIndex;
        document.theform.glNumber.focus();
      }
    }
    document.theform.entity.value = document.theform.entity.value.toUpperCase();
    document.theform.location.value = document.theform.location.value.toUpperCase();
    document.theform.costCenter.value = document.theform.costCenter.value.toUpperCase();
    document.theform.glNumber.value = document.theform.glNumber.value.toUpperCase();
    document.theform.concatId.value = document.theform.entity.value +
                                      document.theform.location.value +
                                      document.theform.costCenter.value +
                                      document.theform.glNumber.value +
                                      "0000";
  }
  if (document.theform.program.value == 'TARGET') {
    document.theform.location.className="input";
    document.theform.expenseCenter.className="input";
    document.theform.account.className="input";
    document.theform.sourceCode.className="input";
    temp=document.theform.location.value;
    if (temp.length != 4) {
      document.theform.location.className="ErrorField";
      msg = msg + "Location# must be 4 digits\n";
      if (document.theform.location.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.location.tabIndex;
        document.theform.location.focus();
      }
    }
    temp=document.theform.expenseCenter.value;
    if (temp.length != 4) {
      document.theform.expenseCenter.className="ErrorField";
      msg = msg + "Expense Center must be 4 digits\n";
      if (document.theform.expenseCenter.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.expenseCenter.tabIndex;
        document.theform.expenseCenter.focus();
      }
    }
    temp=document.theform.account.value;
    if (temp.length != 6) {
      document.theform.account.className="ErrorField";
      msg = msg + "Account# must be 6 digits\n";
      if (document.theform.account.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.account.tabIndex;
        document.theform.account.focus();
      }
    }
    if (document.theform.sourceCode.value == "") {
      document.theform.sourceCode.className="ErrorField";
      msg = msg + "Source Code is required\n";
      if (document.theform.sourceCode.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceCode.tabIndex;
        document.theform.sourceCode.focus();
      }
    }
    document.theform.location.value = document.theform.location.value.toUpperCase();
    document.theform.expenseCenter.value = document.theform.expenseCenter.value.toUpperCase();
    document.theform.account.value = document.theform.account.value.toUpperCase();
    document.theform.sourceCode.value = document.theform.sourceCode.value.toUpperCase();
    document.theform.concatId.value = document.theform.location.value +
                                      document.theform.expenseCenter.value +
                                      document.theform.account.value;
  }
  if (msg != "") {
    alert(msg);
    return;
  }
  document.theform.action_type.value = "update";
  //alert(document.theform.concatId.value);
  submitAction("costCenterMaint.do");
}


function programChange() {
  disableFields();
  if (document.theform.program.value == 'ADVO') {
    document.theform.entity.disabled = false;
    document.theform.entity.className="input";
    document.theform.location.disabled = false;
    document.theform.location.className="input";
    document.theform.costCenter.disabled = false;
    document.theform.costCenter.className="input";
    document.theform.glNumber.disabled = false;
    document.theform.glNumber.className="input";
  }
  if (document.theform.program.value == 'TARGET') {
    document.theform.location.disabled = false;
    document.theform.location.className="input";
    document.theform.expenseCenter.disabled = false;
    document.theform.expenseCenter.className="input";
    document.theform.account.disabled = false;
    document.theform.account.className="input";
    document.theform.sourceCode.disabled = false;
    document.theform.sourceCode.className="input";
  }
}

function disableFields() {
  document.theform.entity.disabled = true;
  document.theform.entity.className = 'disabledField';
  document.theform.location.disabled = true;
  document.theform.location.className = 'disabledField';
  document.theform.expenseCenter.disabled = true;
  document.theform.expenseCenter.className = 'disabledField';
  document.theform.account.disabled = true;
  document.theform.account.className = 'disabledField';
  document.theform.costCenter.disabled = true;
  document.theform.costCenter.className = 'disabledField';
  document.theform.glNumber.disabled = true;
  document.theform.glNumber.className = 'disabledField';
  document.theform.sourceCode.disabled = true;
  document.theform.sourceCode.className = 'disabledField';
}

function markChanged() {
  madeChanges = true;
}

]]>
</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Cost Center Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="concatId" value=""/>

<xsl:if test="key('pagedata','result')/@value = '1'">
  <font class="OperationSuccess">
    Cost Center saved
  </font>
  <br/>
</xsl:if>
<xsl:if test="key('pagedata','result')/@value = '2'">
  <font class="ErrorMessage">
    <xsl:value-of select="key('pagedata','program')/@value" />
    &nbsp;Cost Center&nbsp;
    <xsl:value-of select="key('pagedata','concatId')/@value" />
    &nbsp;already exists
  </font>
  <br/>
</xsl:if>
<div id="SourceCodeNOF">
  <xsl:if test="key('pagedata','result')/@value = '3'">
    <font class="ErrorMessage">
      <xsl:value-of select="key('pagedata','sourceCode')/@value" />
      &nbsp;-&nbsp;Source Code not on file
    </font>
  </xsl:if>
</div>

<table width="800" border="3" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="800" border="0" cellpadding="2" cellspacing="0">
  <tr>
  	<td width="150">&nbsp;</td>
	  <td width="650">&nbsp;</td>
  </tr>
  <tr>
    <td class="label" align="right">Program:&nbsp;</td>
    <td align="left" colspan="4">
      <select name="program" onChange="programChange()" tabindex="1">
        <option value=""></option>
        <option value="ADVO">
          <xsl:if test="$program = 'ADVO'">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          ADVO
        </option>
        <option value="TARGET">
          <xsl:if test="$program = 'TARGET'">
            <xsl:attribute name="selected">true</xsl:attribute>
          </xsl:if>
          Target
        </option>
      </select>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td colspan="4" class="colHeader">&nbsp;Cost Center Information</td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td class="label" align="right">Entity#:&nbsp;</td>
    <td>
      <input type="text" name="entity" value="{$entity}" size="10" maxlength="2" tabindex="2" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">Location#:&nbsp;</td>
    <td>
      <input type="text" name="location" value="{$location}" size="10" maxlength="4" tabindex="3" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">Expense Center:&nbsp;</td>
    <td>
      <input type="text" name="expenseCenter" value="{$expenseCenter}" size="10" maxlength="4" tabindex="4" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">Account#:&nbsp;</td>
    <td>
      <input type="text" name="account" value="{$account}" size="10" maxlength="6" tabindex="5" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">Cost Center#:&nbsp;</td>
    <td>
      <input type="text" name="costCenter" value="{$costCenter}" size="10" maxlength="4" tabindex="6" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">G/L#:&nbsp;</td>
    <td>
      <input type="text" name="glNumber" value="{$glNumber}" size="10" maxlength="6" tabindex="7" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr>
    <td class="label" align="right">Source Code:&nbsp;</td>
    <td>
      <input type="text" name="sourceCode" value="{$sourceCode}" size="10" maxlength="6" tabindex="8" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>

  <tr>
    <td>&nbsp;</td>
    <td align="left">
      <button class="blueButton" onClick="javascript:doUpdate()" accesskey="s" tabindex="100">(S)ave</button>&nbsp;&nbsp;
    </td>
    <td align="right">
      <button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="111">(E)xit</button>&nbsp;&nbsp;
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  </table>  
  
</td></tr>
</table>

</form>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>
