<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="header.xsl"/>
  <xsl:import href="footer.xsl"/>
  <xsl:import href="securityanddata.xsl"/>
  <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   
  <!-- Retrieve Promotion values -->
  <xsl:variable name="startDate" select="root/sourceProgramList/sourceProgram/@start_date" />
  <xsl:variable name="programName" select="root/sourceProgramList/sourceProgram/@program_name" />
  <xsl:variable name="updatedOn" select="root/sourceProgramList/sourceProgram/@updated_on" />
  <xsl:variable name="updatedBy" select="root/sourceProgramList/sourceProgram/@updated_by" />

  <!-- Root template -->
  <xsl:template match="/">
   
<html>
  <head>
    <base target="_self" />
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252"></meta>
    <title>Source Code Partner Program Maintenance</title>

<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" language="JavaScript">

function doBackAction() {
  alert("doExit()");
}

function init() {
  var isValid = "<xsl:value-of select="key('pagedata', 'VALIDATION_SUCCESS')/@value"/>";
  var isAfterSubmit = <xsl:value-of select="boolean(key('pagedata', 'SUBMIT_ACTION')/@value)" />;
<![CDATA[
  //alert("isvalid="+isValid+"  isaftersubmit="+isAfterSubmit);
  if(isValid=="Y" && isAfterSubmit)
  {
    //alert("Operation successful!");
    window.returnValue = "RELOAD";
    window.close();
  }
  else // edit or add action failed
  if(isAfterSubmit)
  {
    //display error message
    var errMsg = "";
]]>
    <xsl:for-each select="/root/validation/error" >
    <xsl:choose>
    <xsl:when test="name != 'VALIDATION_SUCCESS'" >
    errMsg = errMsg + "<xsl:value-of select='name' />: <xsl:value-of select='value' />\n";
    </xsl:when>
    </xsl:choose>
    </xsl:for-each>
    <![CDATA[
    replaceText("lockError", errMsg+"Edit or Add program not successful. Please correct the errors and re-submit.");
  }
  if (document.theform.promoStartDate.value == "") {
    var currDate = new Date();
    document.theform.promoStartDate.value = (currDate.getMonth() + 1) + "/" + currDate.getDate() + "/" + currDate.getFullYear();      
  }
  document.theform.hideSubmit.style.display = 'none';
  document.theform.promoStartDate.focus();
}

function doClear() {
  document.theform.reset();
}

function doUpdate() {
  document.theform.promoStartDate.className="input";
  document.theform.programName.className="input";

  this.minTabIndex = 99999;
  var msg = "";

  if (document.theform.promoStartDate.value == '')
  {
    document.theform.promoStartDate.className="ErrorField";
    msg = msg + "Promotion Start Date is required\n";
    if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.promoStartDate.tabIndex;
      document.theform.promoStartDate.focus();
    }
  }
  if (!validateDate(document.theform.promoStartDate.value, "U", "A")) {
    document.theform.promoStartDate.className="ErrorField";
    msg = msg + "Please enter a valid Start date in MM/DD/YYYY format.\n";
    if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.promoStartDate.tabIndex;
      document.theform.promoStartDate.focus();
    }
  }
  if (document.theform.action_type.value == 'add_program') {
    now = new Date();
    today = now.getTime();
    dateToCheck = new Date();
    temp = new Array();
    temp = document.theform.promoStartDate.value.split('/');
    dateToCheck.setMonth(temp[0]-1);
    dateToCheck.setDate(temp[1]);
    dateToCheck.setYear(temp[2]);
    checkDate = dateToCheck.getTime();
    pastDate = (today > checkDate);
    if (pastDate)
    {
      document.theform.promoStartDate.className="ErrorField";
      msg = msg + "Promotion Start Date cannot be earlier than today\n";
      if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.promoStartDate.tabIndex;
        document.theform.promoStartDate.focus();
      }
   }
  } else {
    if (document.theform.promoStartDate.value != document.theform.originalStartDate.value) {
      now = new Date();
      today = now.getTime();
      dateToCheck = new Date();
      temp = new Array();
      temp = document.theform.originalStartDate.value.split('/');
      dateToCheck.setMonth(temp[0]-1);
      dateToCheck.setDate(temp[1]);
      dateToCheck.setYear(temp[2]);
      checkDate = dateToCheck.getTime();
      pastDate = (today >= checkDate);
      if (pastDate) {
        document.theform.promoStartDate.className="ErrorField";
        msg = msg + "Promotion Start Date cannot be changed\n";
        if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
          this.minTabIndex = document.theform.promoStartDate.tabIndex;
          document.theform.promoStartDate.focus();
        }
      }
      temp = document.theform.promoStartDate.value.split('/');
      dateToCheck.setMonth(temp[0]-1);
      dateToCheck.setDate(temp[1]);
      dateToCheck.setYear(temp[2]);
      checkDate = dateToCheck.getTime();
      pastDate = (today > checkDate);
      if (pastDate) {
        document.theform.promoStartDate.className="ErrorField";
        msg = msg + "Promotion Start Date cannot be earlier than today\n";
        if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
          this.minTabIndex = document.theform.promoStartDate.tabIndex;
          document.theform.promoStartDate.focus();
        }
      }
      prevTemp = document.theform.previousStartDate.value.split('/');
      if ((temp[2] + temp[0] + temp[1]) <= (prevTemp[2] + prevTemp[0] + prevTemp[1])) {
        document.theform.promoStartDate.className="ErrorField";
        msg = msg + "Promotion Start Date must be after previous promotion\n";
        if (document.theform.promoStartDate.tabIndex < this.minTabIndex) {
          this.minTabIndex = document.theform.promoStartDate.tabIndex;
          document.theform.promoStartDate.focus();
        }
      }
    }
  }
  if (document.theform.programName.value == '')
  {
    document.theform.programName.className="ErrorField";
    msg = msg + "Partner Promotion Code is required\n";
    if (document.theform.programName.tabIndex < this.minTabIndex) {
      this.minTabIndex = document.theform.programName.tabIndex;
      document.theform.programName.focus();
    }
  }
  if (msg != "") {
    alert(msg);
    return;
  }
  document.theform.action_type.value = "save_program";
  document.theform.action = "sourceCodeMaint.do";
  document.theform.target = "";
  document.theform.submit();
}

function doExit() {
  window.returnValue = "NO_RELOAD";
  window.close();
}

]]>
</script>
</head>

<body onLoad="init()">

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="requestSourceCode" value="{key('pagedata','requestSourceCode')/@value}"/>
<input type="hidden" name="promoId" value="{key('pagedata','promoId')/@value}"/>
<input type="hidden" name="previousStartDate" value="{key('pagedata','previousStartDate')/@value}"/>
<input type="hidden" name="originalStartDate" value="{$startDate}"/>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td width="35%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="5%">&nbsp;</td>
      <td width="35%">&nbsp;</td>
   </tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
   <tr>
      <td width="30%">&nbsp;</td>
      <td width="20%">&nbsp;</td>
      <td width="50%"><input type="submit" name="hideSubmit" value="hidden"/></td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Promotion Start Date<font color="red">*&nbsp;</font>:</td>
      <td align="left" valign="top">
         <input type="text" value="{$startDate}" size="10" maxlength="10" name="promoStartDate" tabindex="10" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
         &nbsp;&nbsp;<input type="image" tabindex="11" name="tim" id="calendar1" SRC="images/calendar.gif" style="width: 20px; height: 20px;" align="ABSMIDDLE"/>
      </td>
      <td class="Instruction" align="left">
      This is the date that the promotion will begin. The format should be MM/DD/YYYY
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Partner Promotion Code<font color="red">*&nbsp;</font>:</td>
      <td align="left" valign="top">
         <select name="programName" tabindex="40">
            <option value=""></option>
            <xsl:for-each select="root/partnerProgramList/partnerProgram">
               <xsl:choose>
                  <xsl:when test="@program_name = $programName">
                     <option value="{@program_name}" selected="true"><xsl:value-of select="@program_name"/></option>
                  </xsl:when>
                  <xsl:otherwise>
                     <option value="{@program_name}"><xsl:value-of select="@program_name"/></option>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </select></td>
      <td class="Instruction" align="left">
      The name of the Partner Program that will be used to calculate miles/points.
      </td>
   </tr>

<script type="text/javascript">
    Calendar.setup(
    {
      inputField  : "promoStartDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar1"  // ID of the button
    }
    );
</script>

   <tr>
      <td align="right" class="label" valign="top">Last Updated On:</td>
      <td align="left" valign="top">
         <xsl:value-of select="$updatedOn" />
      </td>
      <td class="Instruction" align="left">
      This is the last time that a change was made to this promotion.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Last Updated By:</td>
      <td align="left" valign="top">
         <xsl:value-of select="$updatedBy" />
      </td>
      <td class="Instruction" align="left">
      This is the person who last made changes to this promotion.
      </td>
   </tr>
</table>

</td></tr>
</table>

<table width="98%" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="50%" align="center">
        <button class="blueButton" onClick="return doUpdate()" tabindex="90" accesskey="s">(S)ave</button>&nbsp;&nbsp;
        <button class="blueButton" onClick="doClear()" tabindex="91" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
    </td>
    <td width="50%" align="right">
        <button class="blueButton" onClick="doExit()" tabindex="92" accesskey="e">(E)xit</button>
    </td>
  </tr>
</table>

</form>

</body>
</html>

  </xsl:template>
</xsl:stylesheet>
