<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  	<xsl:output method="html" indent="yes"/>
	<xsl:template name="searchCriteria">
  
	<xsl:param name="scActionType"/>
	<xsl:param name="scRecordSortBy"/>	
	<xsl:param name="scPageMaxRecordCount"/>	
	<xsl:param name="scRecordNumber"/>
	<xsl:param name="scSourceCode"/>
	<xsl:param name="scUpdatedBy"/>
	<xsl:param name="scRequestedBy"/>
	<xsl:param name="scCompanyId"/>
	<xsl:param name="scSourceType"/>
	<xsl:param name="scPriceHeaderId"/>
	<xsl:param name="scSnhId"/>
	<xsl:param name="scProgramName"/>
	<xsl:param name="scProgramWebsiteUrl"/>
	<xsl:param name="scPromotionCode"/>
	<xsl:param name="scDefaultSourceCodeFlag"/>
	<xsl:param name="scJcpenneyFlag"/>
	<xsl:param name="scOver21Flag"/>
	<xsl:param name="scEndDate"/>
	<xsl:param name="scUpdatedOnStart"/>
	<xsl:param name="scUpdatedOnEnd"/>
        <xsl:param name="scIotwFlag"/>

    <!--search metadata-->
    <input type="hidden" name="scActionType"    id="scActionType"  value="{$scActionType}"/>
    <input type="hidden" name="scRecordSortBy"          id="scRecordSortBy"        value="{$scRecordSortBy}"/>
    <input type="hidden" name="scPageMaxRecordCount"      id="scPageMaxRecordCount"    value="{$scPageMaxRecordCount}"/>
    <input type="hidden" name="scRecordNumber" 	    id="scRecordNumber"	    value="{$scRecordNumber}"/>
        
    <!-- search criteria -->
    <input type="hidden" name="scSourceCode" 			      id="scSourceCode" 		        value="{$scSourceCode}" />
    <input type="hidden" name="scUpdatedBy"      id="scUpdatedBy" 	  value="{$scUpdatedBy}" />
    <input type="hidden" name="scRequestedBy" 			    id="scRequestedBy" 		    value="{$scRequestedBy}" />
    <input type="hidden" name="scSourceType" 		    id="scSourceType" 		    value="{$scSourceType}" />
    <input type="hidden" name="scCompanyId" 		    id="scCompanyId" 		    value="{$scCompanyId}" />
    <input type="hidden" name="scPriceHeaderId" 	    id="scPriceHeaderId" 		  value="{$scPriceHeaderId}" />
    <input type="hidden" name="scSnhId" 			      id="scSnhId" 		      value="{$scSnhId}" />
    <input type="hidden" name="scProgramName" 			      id="scProgramName" 		      value="{$scProgramName}" />
    <input type="hidden" name="scProgramWebsiteUrl" 		    id="scProgramWebsiteUrl" 		    value="{$scProgramWebsiteUrl}" />
    <input type="hidden" name="scPromotionCode" 		    id="scPromotionCode" 		  value="{$scPromotionCode}" />
    <input type="hidden" name="scDefaultSourceCodeFlag" 			      id="scDefaultSourceCodeFlag" 		        value="{$scDefaultSourceCodeFlag}" />
    <input type="hidden" name="scJcpenneyFlag" 			      id="scJcpenneyFlag" 		      value="{$scJcpenneyFlag}" />
    <input type="hidden" name="scOver21Flag" 			    id="scOver21Flag" 		    value="{$scOver21Flag}"/>
    <input type="hidden" name="scEndDate" 			    id="scEndDate" 		    value="{$scEndDate}"/>
    <input type="hidden" name="scUpdatedOnStart" 		    id="scUpdatedOnStart" 		    value="{$scUpdatedOnStart}"/>
    <input type="hidden" name="scUpdatedOnEnd" 				      id="scUpdatedOnEnd" 			        value="{$scUpdatedOnEnd}"/>
    <input type="hidden" name="scIotwFlag" 			    id="scIotwFlag" 		    value="{$scIotwFlag}"/>
                

	</xsl:template>	
</xsl:stylesheet>