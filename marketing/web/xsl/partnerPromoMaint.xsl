<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   
   <!-- Retrieve Promotion values -->
   <xsl:variable name="programType" select="root/programRewardList/programReward/@program_type" />
   <xsl:variable name="pointsPerDollar" select="root/programRewardList/programReward/@points" />
   <xsl:variable name="maximumPoints" select="root/programRewardList/programReward/@maximum_points" />
   <xsl:variable name="calculationBasis" select="root/programRewardList/programReward/@calculation_basis" />
   <xsl:variable name="emailExcludeFlag" select="root/programRewardList/programReward/@email_exclude_flag" />
   <xsl:variable name="rewardType" select="root/programRewardList/programReward/@reward_type" />
   <xsl:variable name="rewardName" select="root/programRewardList/programReward/@reward_name" />
   <xsl:variable name="programLongName" select="root/programRewardList/programReward/@program_long_name" />
   <xsl:variable name="customerInfo" select="root/programRewardList/programReward/@customer_info" />
   <xsl:variable name="participantIdLength" select="root/programRewardList/programReward/@participant_id_length_check" />
   <xsl:variable name="participantEmail" select="root/programRewardList/programReward/@participant_email_check" />
   <xsl:variable name="updatedBy" select="root/programRewardList/programReward/@updated_by" />
   <xsl:variable name="updatedOn" select="root/programRewardList/programReward/@updated_on" />
   <xsl:variable name="lastPostDate" select="root/programRewardList/programReward/@last_post_date" />
   <xsl:variable name="updateAllowed" select="root/pageData/data[@name='PartnerPromotionData']/@value" />
   <xsl:variable name="bonusAmount" select="root/programRewardList/programReward/@bonus_points" />
   <xsl:variable name="bonusCalculationBasis" select="root/programRewardList/programReward/@bonus_calculation_basis" />
   <xsl:variable name="bonusStructure" select="root/programRewardList/programReward/@bonus_separate_data" />
   <xsl:variable name="partnerId" select="root/pageData/data[@name='partnerId']/@value" />
   <xsl:variable name="membershipDataRequired" select="root/programRewardList/programReward/@membership_data_required" />
   
   <xsl:variable name="basisLoop" select="root/calculationBasisList"/>
   
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Partner Promotion Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" language="JavaScript">
function doBackAction() {
  <xsl:if test="key('pagedata','action_type')/@value != 'search'">
    document.theform.action_type.value = "search";
    document.theform.partnerId.disabled = false;
    submitAction("partnerPromoMaint.do");
    return;
  </xsl:if>
  document.theform.action = "mainMenu.do";
  document.theform.submit();
  //history.go("http://192.168.110.172:8988/marketing/menu.html");
}

function doExit() {
  if (madeChanges) {
    if (!confirm("Are you sure you want to exit without saving?")) {
      return;
    }
  }
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function init() {
  madeChanges = false;
  <xsl:choose>
    <xsl:when test="key('pagedata','action_type')/@value = 'search'">
      myArray = new Array();
      partnerId="";
      for(var i=0;i &lt; document.theform.partnerPromoCode.options.length;i++)
      {
        temp = new Array();
        work = document.theform.partnerPromoCode.options[i].text;
        temp = work.split('~');
        if (partnerId == '')
        {
          partnerId = temp[0];
          partnerCnt = 0;
          promoCnt = 0;
          myArray[partnerCnt] = new Array();
        } else {
          if (partnerId != temp[0])
          {
            partnerId = temp[0];
            partnerCnt = partnerCnt + 1;
            promoCnt = 0;
            myArray[partnerCnt] = new Array();
          }
        }
        myArray[partnerCnt][promoCnt] = new Array();
        myArray[partnerCnt][promoCnt][0] = trimString(temp[0]);
        myArray[partnerCnt][promoCnt][1] = work;
        //alert(partnerCnt + '/' + promoCnt);
        promoCnt = promoCnt + 1;
      }
      //alert(myArray);
      document.theform.partnerPromoCode.options.length = 0;
      if (document.theform.partnerId.value) {
        partnerChange();
      } else {
        document.theform.partnerId.selectedIndex = 0;
      }
      document.theform.partnerId.focus();
    </xsl:when>
    <xsl:when test="not($updateAllowed = 'Y')">
      disableRewardFields();
      document.theform.partnerId.disabled = true;
      document.theform.programType.disabled = true;
    </xsl:when>
    <xsl:when test="not($lastPostDate = '') and ($lastPostDate)">
      confirmMsg="Rewards have already been posted. Changing the Partner\n";
      confirmMsg=confirmMsg + "Promotion Code may cause pending miles to be posted\n";
      confirmMsg=confirmMsg + "incorrectly. Would you like to continue with your update?";
      if (!confirm(confirmMsg)) {
        doBackAction();
      }
      document.theform.partnerId.disabled = true;
      document.theform.programType.disabled = true;
      document.theform.pointsPerDollar.disabled = true;
      document.theform.mileageCalcSource.disabled = true;
      document.theform.maximumPoints.disabled = true;
      document.theform.bonusAmount.disabled = true;
      document.theform.bonusMileageCalcSource.disabled = true;
      document.theform.bonusStructure.focus();
    </xsl:when>
    <xsl:when test="key('pagedata','action_type')/@value != 'search'">
      if (document.theform.programType.value == 'Default')
      {
        enableRewardFields();
      } else {
        disableRewardFields();
      }
      document.theform.programType.focus();
      document.theform.partnerId.disabled = true;
    </xsl:when>
  </xsl:choose>
}

function markChanges(fieldName) {
  madeChanges = true;
  if (fieldName == 'programType')
  {
    if (document.theform.programType.value == 'Default')
    {
      enableRewardFields();
    } else {
      disableRewardFields();
    }
  }
  //alert(madeChanges);
}

function disableRewardFields() {
  document.theform.pointsPerDollar.disabled = true;
  document.theform.mileageCalcSource.disabled = true;
  document.theform.rewardType.disabled = true;
  document.theform.emailExcludeFlag[0].disabled = true;
  document.theform.emailExcludeFlag[1].disabled = true;
  document.theform.rewardName.disabled = true;
  document.theform.maximumPoints.disabled = true;
  document.theform.bonusAmount.disabled = true;
  document.theform.bonusMileageCalcSource.disabled = true;
  document.theform.bonusStructure.disabled = true;
  document.theform.programLongName.disabled = true;
}

function enableRewardFields() {
  document.theform.pointsPerDollar.disabled = false;
  document.theform.mileageCalcSource.disabled = false;
  document.theform.rewardType.disabled = false;
  document.theform.emailExcludeFlag[0].disabled = false;
  document.theform.emailExcludeFlag[1].disabled = false;
  document.theform.rewardName.disabled = false;
  document.theform.maximumPoints.disabled = false;
  document.theform.bonusAmount.disabled = false;
  document.theform.bonusMileageCalcSource.disabled = false;
  document.theform.bonusStructure.disabled = false;
  document.theform.programLongName.disabled = false;
}

function doClear() {
  document.theform.reset();
  document.theform.pointsPerDollar.className="input";
  document.theform.rewardName.className="input";
  document.theform.maximumPoints.className="input";
  document.theform.bonusAmount.className="input";
  init();
}

function doGet() {
  if (document.theform.partnerId.value == '')
  {
    document.theform.partnerId.className="ErrorField";
    alert("Partner Id is required");
    document.theform.partnerId.focus();
    return;
  }
  document.theform.action_type.value = "edit";
  submitAction("partnerPromoMaint.do");
}

function doAdd() {
  if (document.theform.partnerId.value == '')
  {
    document.theform.partnerId.className="ErrorField";
    alert("Partner Id is required");
    document.theform.partnerId.focus();
    return;
  }
  newPromo = prompt('Enter new Partner Promotion Code: ','');
  if (newPromo == '')
  {
    alert("Invalid Partner Promotion Code");
  } else if (newPromo != null)
  {
    document.theform.action_type.value = "insert";
    document.theform.partnerPromoCode.options.length = 1;
    document.theform.partnerPromoCode.options[0].value = newPromo.toUpperCase();
    document.theform.partnerPromoCode.options[0].selected = true;
    submitAction("partnerPromoMaint.do");
  }
}

function doUpdate() {
  document.theform.pointsPerDollar.className="input";
  document.theform.rewardName.className="input";
  document.theform.maximumPoints.className="input";
  document.theform.bonusAmount.className="input";
  document.theform.programLongName.className="input";
  msg="";
  if (document.theform.programType.value == 'Default') {
    if (document.theform.pointsPerDollar.value == '') {
      document.theform.pointsPerDollar.className="ErrorField";
      if (msg == "") document.theform.pointsPerDollar.focus();
      msg = msg + "Points Per Dollar is required\n";
    } else {
      if (!isNumericDecimal(document.theform.pointsPerDollar.value)) {
        document.theform.pointsPerDollar.className="ErrorField";
        if (msg == "") document.theform.pointsPerDollar.focus();
        msg = msg + "Points Per Dollar must be numeric\n";
      }
    }
    if (!isNumericDecimal(document.theform.maximumPoints.value)) {
      document.theform.maximumPoints.className="ErrorField";
      if (msg == "") document.theform.maximumPoints.focus();
      msg = msg + "Maximum Points must be numeric\n";
    }
    if (!isNumericDecimal(document.theform.bonusAmount.value)) {
      document.theform.bonusAmount.className="ErrorField";
      if (msg == "") document.theform.bonusAmount.focus();
      msg = msg + "Bonus Amount must be numeric\n";
    }
    if (document.theform.emailExcludeFlag[0].checked == true) {
      if (document.theform.rewardName.value == '') {
        document.theform.rewardName.className="ErrorField";
        if (msg == "") document.theform.rewardName.focus();
        msg = msg + "Reward Name is required\n";
      }
      if (document.theform.programLongName.value == '') {
        document.theform.programLongName.className="ErrorField";
        if (msg == "") document.theform.programLongName.focus();
        msg = msg + "Email Confirmation Name is required\n";
      }
    }
    if (msg != "" ) {
      alert(msg);
      return;
    }
    document.theform.programType.disabled = false;
    document.theform.pointsPerDollar.disabled = false;
    document.theform.mileageCalcSource.disabled = false;
    document.theform.maximumPoints.disabled = false;
    document.theform.bonusAmount.disabled = false;
    document.theform.bonusMileageCalcSource.disabled = false;
    document.theform.programLongName.disabled = false;
  }
  document.theform.partnerPromoCode.disabled = false;
  document.theform.partnerId.disabled = false;
  document.theform.action_type.value = "update";
  submitAction("partnerPromoMaint.do");
}

function submitAction(url) {
  document.theform.action = url;
  document.theform.target = "";
  document.theform.submit();
}

function trimString(inString) {
  inString = inString.replace( /^\s+/g, "" );// strip leading
  return inString.replace( /\s+$/g, "" );// strip trailing
}

function partnerChange() {
  newPartner = document.theform.partnerId.value;
  //alert(newPartner);
  addCnt = 0;
  document.theform.partnerPromoCode.options.length = 0;
  for(var i=0;i &lt; myArray.length;i++)
  {
    for(var j=0;j &lt; myArray[i].length;j++)
    {
      if (myArray[i][j][0] == newPartner)
      {
        //alert(myArray[i][j][1]);
        temp = new Array();
        temp = myArray[i][j][1].split('~')
        document.theform.partnerPromoCode.options.length = addCnt + 1;
        document.theform.partnerPromoCode.options[addCnt].value = trimString(temp[1])
        document.theform.partnerPromoCode.options[addCnt].text = trimString(temp[1]) + " - " + trimString(temp[2]);
        addCnt = addCnt + 1;
      }
    }
  }
}

</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Partner Promotion Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="customerInfo" value="{$customerInfo}"/>
<input type="hidden" name="participantIdLength" value="{$participantIdLength}"/>
<input type="hidden" name="participantEmail" value="{$participantEmail}"/>
<input type="hidden" name="bonusDate" value=""/>
<input type="hidden" name="lastPostDate" value="{$lastPostDate}"/>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td width="25%">
        <xsl:if test="key('pagedata','result')/@value = '1'">
          <font class="OperationSuccess">PROGRAM&nbsp;<xsl:value-of select="key('pagedata','partnerPromoCode')/@value" />&nbsp;SAVED</font>
        </xsl:if>
        &nbsp;
      </td>
      <td width="25%">&nbsp;</td>
      <td width="50%" align="right">
        <xsl:if test="key('pagedata','action_type')/@value != 'search'">
          <xsl:if test="$updateAllowed = 'Y'">
            <button class="blueButton" onClick="javascript:doUpdate()" tabindex="-1" accesskey="s">(S)ave</button>&nbsp;&nbsp;
            <button class="blueButton" onClick="javascript:doClear()" tabindex="-1" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
          </xsl:if>
          <button class="blueButton" onClick="javascript:doExit()" tabindex="-1" accesskey="e">(E)xit</button>&nbsp;&nbsp;
        </xsl:if>
      </td>
   </tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
   <tr>
      <td width="20%" align="right" class="label" valign="top">Partner Id<font color="red">*&nbsp;</font>:</td>
      <td width="25%" align="left" valign="top">
        <xsl:choose>
          <xsl:when test="key('pagedata','action_type')/@value = 'search'">
            <select name="partnerId" tabindex="1" onchange="partnerChange()" style="width: 200px">
              <option value=""></option>
              <xsl:for-each select="root/partnerList/partner">
                <option value="{@partner_name}">
                  <xsl:if test="@partner_name = $partnerId">
                    <xsl:attribute name="selected">
                    true
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="@partner_name"/>
                </option>
              </xsl:for-each>
            </select>
          </xsl:when>
          <xsl:otherwise>
            <input type="text" name="partnerId" value="{key('pagedata','partnerId')/@value}"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td width="55%" class="Instruction" align="left">
      This field is a list of all available partners.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Partner Promotion Code:</td>
      <td align="left" valign="top">
        <xsl:choose>
          <xsl:when test="key('pagedata','action_type')/@value = 'search'">
            <select name="partnerPromoCode" tabindex="2" style="width: 200px">
              <xsl:for-each select="root/partnerProgramList/partnerProgram">
                <option value="{program_name}">
                  <xsl:value-of select="@partner_name"/>~
                  <xsl:value-of select="@program_name"/>~
                  <xsl:value-of select="@program_type"/>
                </option>
              </xsl:for-each>
            </select>
          </xsl:when>
          <xsl:otherwise>
            <input type="text" name="partnerPromoCode" value="{key('pagedata','partnerPromoCode')/@value}" disabled = "true"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td class="Instruction" align="left">
      This field is assigned to source codes to control the appropriate offer.
      </td>
   </tr>
   <xsl:choose>
     <xsl:when test="key('pagedata','action_type')/@value = 'search'">
       <tr>
         <td></td>
         <td align="left">
           <button class="blueButton" onClick="javascript:doGet()" tabindex="3" accesskey="r">(R)etrieve Data</button>
           &nbsp;&nbsp;
           <xsl:if test="$updateAllowed = 'Y'">
             <button class="blueButton" onClick="javascript:doAdd()" tabindex="4" accesskey="a">(A)dd New</button>
           </xsl:if>
         </td>
       </tr>
     </xsl:when>
     <xsl:otherwise>
   <tr>
      <td align="right" class="label" valign="top">Program Type:</td>
      <td align="left" valign="top">
         <select name="programType" onChange="markChanges('programType')" tabindex="10">
            <xsl:for-each select="root/programTypeList/programType">
               <xsl:choose>
                  <xsl:when test="@program_type = $programType">
                     <option value="{@program_type}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:when test="@program_type = 'Default' and not($programType)">
                     <option value="{@program_type}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:otherwise>
                     <option value="{@program_type}"><xsl:value-of select="@description"/></option>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </select>
      </td>
      <td class="Instruction" align="left">
      This field is used to define alternate program type such as cost centers (CC) or purchasing card partners (PC).
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Points Per Dollar<font color="red">*&nbsp;</font>:</td>
      <td align="left" valign="top"><input type="text" value="{$pointsPerDollar}" size="10" maxlength="10" name="pointsPerDollar" onChange="markChanges('pointsPerDollar')" tabindex="11" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
      <td class="Instruction" align="left">
      This field controls the number of miles or points assigned based on the calculation source.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Calculation Source:</td>
      <td align="left" valign="top">
         <select name="mileageCalcSource" onChange="markChanges('mileageCalcSource')" tabindex="12">
            <xsl:for-each select="root/calculationBasisList/calculationBasis">
               <xsl:choose>
                  <xsl:when test="@calculation_basis = $calculationBasis">
                     <option value="{@calculation_basis}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:when test="@calculation_basis = 'M' and (not($calculationBasis) or $calculationBasis = '')">
                     <option value="{@calculation_basis}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:otherwise>
                     <option value="{@calculation_basis}"><xsl:value-of select="@description"/></option>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </select></td>
      <td class="Instruction" align="left">
      This field dictates if the miles or points are calculated from the total or the merchandise amount only.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Maximum Points:</td>
      <td align="left" valign="top"><input type="text" value="{$maximumPoints}" size="10" maxlength="10" name="maximumPoints" onChange="markChanges('maxPoints')" tabindex="13" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
      <td class="Instruction" align="left">
      This is the maximum number of points or miles that will be earned on each purchase using this promotion.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Bonus Amount:</td>
      <td align="left" valign="top"><input type="text" value="{$bonusAmount}" size="10" maxlength="10" name="bonusAmount" tabindex="14" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
      <td class="Instruction" align="left">
      This is the amount of bonus miles or points that are tied to the promotion.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Bonus Calculation Source:</td>
      <td align="left" valign="top">
         <select name="bonusMileageCalcSource" onChange="markChanges('mileageCalcSource')" tabindex="15">
            <xsl:for-each select="root/calculationBasisList/calculationBasis">
               <xsl:choose>
                  <xsl:when test="@calculation_basis = $bonusCalculationBasis">
                     <option value="{@calculation_basis}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:when test="@calculation_basis = 'M' and (not($calculationBasis) or $calculationBasis = '')">
                     <option value="{@calculation_basis}" selected="true"><xsl:value-of select="@description"/></option>
                  </xsl:when>
                  <xsl:otherwise>
                     <option value="{@calculation_basis}"><xsl:value-of select="@description"/></option>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </select></td>
      <td class="Instruction" align="left">
      Does the consumer receive a fixed bonus amount or are they calculated on a per dollar or line item basis?
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Bonus Structure:</td>
      <td align="left" valign="top">
         <select name="bonusStructure" tabindex="16">
            <option value="S">
            <xsl:if test="$bonusStructure = 'S'">
              <xsl:attribute name="selected">true</xsl:attribute>
            </xsl:if>
            Summary
            </option>
            <option value="D">
            <xsl:if test="$bonusStructure = 'D' or not($bonusStructure)">
              <xsl:attribute name="selected">true</xsl:attribute>
            </xsl:if>
            Detail
            </option>
         </select></td>
      <td class="Instruction" align="left">
      The Summary option means bonus points are combined with regular points to form one line item on the posting file while the Detail option puts bonus points on a separate line item.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Reward Type:</td>
      <td align="left" valign="top">
         <select name="rewardType" onChange="markChanges('rewardType')" tabindex="17">
            <xsl:for-each select="root/rewardTypeList/rewardType">
               <xsl:choose>
                  <xsl:when test="@reward_type = $rewardType">
                     <option value="{@reward_type}" selected="true"><xsl:value-of select="@reward_type"/></option>
                  </xsl:when>
                  <xsl:when test="@reward_type = 'Points' and (not($rewardType) or $rewardType = '')">
                     <option value="{@reward_type}" selected="true"><xsl:value-of select="@reward_type"/></option>
                  </xsl:when>
                  <xsl:otherwise>
                     <option value="{@reward_type}"><xsl:value-of select="@reward_type"/></option>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
         </select></td>
      <td class="Instruction" align="left">
      What type of rewards program is being applied?
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Exclude Email Flag:</td>
      <td align="left" valign="top">
        <input type="radio" size="10" maxlength="4" name="emailExcludeFlag" value="N" onChange="markChanges('emailExcludeFlag')" tabindex="18">
        <xsl:if test="$emailExcludeFlag = 'N' or not($emailExcludeFlag)">
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:if>
        </input>No
        <input type="radio" size="10" maxlength="4" name="emailExcludeFlag" value="Y" onChange="markChanges('emailExcludeFlag')" tabindex="18">
        <xsl:if test="$emailExcludeFlag = 'Y'">
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:if>
        </input>Yes
      </td>
      <td class="Instruction" align="left">
      If this field is set to 'Yes', the miles/points will not be displayed on the confirmation email.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Reward Name:</td>
      <td align="left" valign="top">
        <input type="text" size="30" name="rewardName" value="{$rewardName}" maxlength="30" onChange="markChanges('rewardName')" tabindex="19" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </td>
      <td class="Instruction" align="left">
      The program reward name that will be used on the confirmation email. This field is required if the Email Exclude Flag is 'No'.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Email Confirmation Name:</td>
      <td align="left" valign="top">
        <input type="text" size="30" name="programLongName" value="{$programLongName}" maxlength="50" onChange="markChanges('programLongName')" tabindex="20" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </td>
      <td class="Instruction" align="left">
      The partner name that will be used on the confirmation email. This field is required if the Email Exclude Flag is 'No'.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Membership Data Required:</td>
      <td align="left" valign="top">
        <input type="radio" size="10" maxlength="4" name="membershipDataRequired" value="N" onChange="markChanges('membershipDataRequired')" tabindex="21">
        <xsl:if test="$membershipDataRequired = 'N'">
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:if>
        </input>No
        <input type="radio" size="10" maxlength="4" name="membershipDataRequired" value="Y" onChange="markChanges('membershipDataRequired')" tabindex="21">
        <xsl:if test="$membershipDataRequired = 'Y' or not($membershipDataRequired)">
          <xsl:attribute name="checked">true</xsl:attribute>
        </xsl:if>
        </input>Yes
      </td>
      <td class="Instruction" align="left">
      If this field is set to 'No', Web O/E will not prompt for Membership Id and Name information.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Last Update:</td>
      <td align="left" valign="top">
        <xsl:value-of select="$updatedBy"/>
        &nbsp;&nbsp;&nbsp;
        <xsl:value-of select="$updatedOn"/>
      </td>
      <td class="Instruction" align="left">
      The User Id and timestamp of the last update to this promotion program.
      </td>
   </tr>
   <tr>
      <td colspan="3">&nbsp;</td>
   </tr>
   <xsl:if test="$updateAllowed = 'Y'">
     <tr>
        <td>&nbsp;</td>
        <td colspan="2">
          <button class="blueButton" onClick="javascript:doUpdate()" tabindex="90">(S)ave</button>&nbsp;&nbsp;
          <button class="blueButton" onClick="javascript:doClear()" tabindex="91">(R)eset All Fields</button>&nbsp;&nbsp;
        </td>
     </tr>
   </xsl:if>
   <tr>
      <td colspan="3">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="3" class="colHeader">&nbsp;</td>
   </tr>
   </xsl:otherwise>
   </xsl:choose>
</table>

</td></tr>
</table>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td align="right">
        <button class="blueButton" onClick="javascript:doExit()" tabindex="99" accesskey="e">(E)xit</button>&nbsp;&nbsp;
      </td>
   </tr>
</table>

</form>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>