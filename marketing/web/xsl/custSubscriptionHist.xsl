<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="pagination.xsl"/>
<xsl:import href="sourceSearchOptions.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="pageNumber" select="key('pagedata','pageNumber')/@value"/>
<xsl:variable name="pageCountTotal" select="key('pagedata','pageCountTotal')/@value"/>
<xsl:variable name="recordCountTotal" select="key('pagedata','recordCountTotal')/@value"/>
<xsl:variable name="scRecordNumber" select="key('pagedata','scRecordNumber')/@value"/>
<!-- Hardcoded value: only 50 records per page can be viewed -->
<xsl:variable name="scPageMaxRecordCount" select="'50'"/>
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<base target="_self" />
<title>
        Customer Subscription History
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<style type="text/css">
		    /* Scrolling table overrides for the search results table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" language="JavaScript">

function doExit() {
  window.returnValue = "NO_RELOAD";
  window.close();
}

function init() {
   scroll(0,0);
}

function doSortedSearch(recordNumber, recordSortBy) {
   // Set a hidden field.
   document.theform.scRecordSortBy.value = recordSortBy;
   doSearch(recordNumber);
}

function doSearch(recordNumber) {
   document.theform.scRecordNumber.value = recordNumber;
   document.theform.scActionType.value = "search";
   submitInfoAction("custSubscriptionHist.do", "Searching");
}

function doSearchPageNext() {
  // Note that the multiplication with 1 forces the vars to numbers, rather than strings.
  doSearch(parseInt(document.theform.scRecordNumber.value)  + parseInt(document.theform.scPageMaxRecordCount.value));
}

function doSearchPagePrevious() {
  doSearch(document.theform.scRecordNumber.value - document.theform.scPageMaxRecordCount.value);
}

function doSearchPageLast() {
  doSearch((document.theform.pageCountTotal.value - 1) * document.theform.scPageMaxRecordCount.value + 1);
}

function submitInfoAction(url, visualMessage) {
   scroll(0,0);
   showWaitMessage("content", "wait", visualMessage);
   submitAction(url);
}

function submitAction(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function printIt(){
	window.print(); 
}

</script>
</head>
<body onLoad="init()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Customer Subscription History'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="true()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
<form name="theform" method="post" action="">
        <xsl:call-template name="securityanddata"></xsl:call-template>
        <input type="hidden" name="scActionType" value="{key('pagedata','scActionType')/@value}"/>
        <input type="hidden" name="scCustomerId" value="{key('pagedata','scCustomerId')/@value}"/>
        <input type="hidden" name="scOriginUrl" value="{key('pagedata','scOriginUrl')/@value}"/>
       <!-- <input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>-->
       <input type="hidden" name="action_type" value=""/>
        <input type="hidden" name="pageNumber" value="{$pageNumber}"/>
        <input type="hidden" name="pageCountTotal" value="{$pageCountTotal}"/>
        <input type="hidden" name="recordCountTotal" value="{$recordCountTotal}"/>
        <input type="hidden" name="scRecordNumber" value="{$scRecordNumber}"/>
        <input type="hidden" name="scPageMaxRecordCount" value="{$scPageMaxRecordCount}"/>
        <input type="hidden" name="scRecordSortBy" value="{key('pagedata','scRecordSortBy')/@value}"/>
<div id="content" style="display:block">	
      <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:if test="$recordCountTotal = '0'">
				   <td class="errorMessage">
						NO RECORDS FOUND
				   </td>
          </xsl:if>
			   </tr>
		   </table>
        <!-- Only show the results pane if results exist -->       
          <div id="searchContent" style="display:block">
            <!--  Main Table thick blue border -->
            <table width="99%" border="0" cellpadding="0" cellspacing="0">
          <tr><td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="53">
              (E)xit
            </button></td></tr>
        </table>
        <xsl:if test="$recordCountTotal &gt; '0'">
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="70" /></xsl:call-template>
            <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
              <tr>
                <td><table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                      <td colspan="2"><div class="tableContainer" id="messageContainer" >
                          <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
                            <thead class="fixedHeader">
                              <tr style="text-align:left">
                                <td  class="ColHeaderLink" width="3%"  ></td>
                                <td  class="ColHeaderLink" width="18%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'updatedOn');" class="textlink" tabindex="80">
                                    DATE
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="15%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'updatedBy');" class="textlink" tabindex="81">
                                    OPERATOR
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="26%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'target');" class="textlink" tabindex="82">
                                    DESCRIPTION
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="20%"></td>
                                <td  class="ColHeaderLink" width="28%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'companyId');" class="textlink" tabindex="83">
                                    CO-BRAND
                                  </a>
                                </td>
                              </tr>
                            </thead>
                            <tbody class="scrollContent" >
                              <xsl:for-each select="root/subscriptionHistoryList/subscriptionHistory">
                                <tr id="row">
                                <!-- Alternate background line shading -->
                              	<xsl:choose>
		                             <xsl:when test="(position() mod 2) = 0">
                                    <xsl:attribute name="style">vertical-align:text-top;background:#ffffff</xsl:attribute>
                                 </xsl:when>
		                             <xsl:otherwise>
                                   <xsl:attribute name="style">vertical-align:text-top;background:#f4f4f4</xsl:attribute>
                                 </xsl:otherwise>
                                </xsl:choose>

                                  <td width="4%" style="text-align:left;">
                                  <xsl:value-of select="@rnum"/>
                                  </td>
                                  <td id="td_{@num}" style="text-align:left;"><xsl:value-of select="substring(@updated_on, 1, 16)"/>
                                  </td>
                                  <td style="text-align:left;"><xsl:value-of select="@updated_by"/></td>
                                  <td style="text-align:left;"><span><xsl:attribute name="title"><xsl:value-of select="@target"/></xsl:attribute><xsl:value-of select="@target"/></span></td>
                                  <td style="text-align:left;"><xsl:value-of select="@subscribe_status"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@company_id"/></td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div></td>
                    </tr>
                    <tr>
                      <td class="Label" width="50%" >
                      </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="400" /></xsl:call-template>
        <table width="99%" border="0" cellpadding="0" cellspacing="0">
          <tr><td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="53">
              (E)xit
            </button></td></tr>
        </table>
        <!-- end of the entire search results div -->
        </xsl:if>
        <!-- end of the entire content div -->
        </div>
        </div>
      </form>
<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>

<xsl:call-template name="footer"></xsl:call-template>

</body></html>
  </xsl:template>
  </xsl:stylesheet>
  