<?xml version='1.0' encoding='windows-1252'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   <!-- Root template -->
   <xsl:template match="/">
   
   <html>
   <head>
   <title>Error Page</title>
   </head>
   
   <body width="500">
   
   <center>
   <h2>An Unexpected System Exception has occurred.</h2>

   <form name="errorForm">
   <input type="hidden" name="error_message" value="{key('pagedata','error_message')/@value}"/>
   <script type="text/javascript">document.write(errorForm.error_message.value);</script>
   </form>
   
   </center>
   
   </body>
   </html>
   
   </xsl:template>
</xsl:stylesheet>
