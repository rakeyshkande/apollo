<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   
   <!-- Retrieve Promotion values -->
   <xsl:variable name="updateAllowed" select="root/pageData/data[@name='updatePricingAllowed']/@value" />
   <xsl:variable name="clubCode" select="root/pageData/data[@name='clubCode']/@value" />
   <xsl:variable name="memberName" select="root/aaaClubList/aaaClub/@membername" />
      
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>AAA Club Code Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">

function doBackAction() {
  <xsl:if test="key('pagedata','action_type')/@value != 'search'">
    document.theform.action_type.value = "search";
    document.theform.clubCode.disabled = false;
    submitAction("clubCodeMaint.do");
    return;
  </xsl:if>
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function doExit() {
  if (madeChanges) {
    if (!confirm("Are you sure you want to exit without saving?")) {
      return;
    }
  }
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function init() {
  <xsl:choose>
    <xsl:when test="key('pagedata','action_type')/@value = 'search'">
      document.theform.clubCode.focus();
    </xsl:when>
    <xsl:otherwise>
      document.theform.memberName.focus();
    </xsl:otherwise>
  </xsl:choose>
<![CDATA[
  madeChanges = false;
}

function doGet() {
  document.theform.action_type.value = "edit";
  submitAction("clubCodeMaint.do");
}

function doAdd() {
  newCode = prompt('Enter new AAA Club Code: ','');
  if (newCode == null) {
    return;
  }
  if (newCode == '')
  {
    alert("Entry is required");
    return;
  }
  if (newCode.length > 6)
  {
    alert("Club Code cannot be more than 6 digits");
    return;
  }
  if (!isNumeric(newCode)) {
    alert("Club Code must be numeric");
    return;
  }
  document.theform.action_type.value = "insert";
  document.theform.clubCode.options.length = 1;
  document.theform.clubCode.options[0].value = newCode.toUpperCase();
  document.theform.clubCode.options[0].selected = true;
  submitAction("clubCodeMaint.do");
}

function submitAction(url) {
  document.theform.action = url;
  document.theform.target = "";
  document.theform.submit();
}

function doUpdate() {
  document.theform.memberName.className="input";
  if (document.theform.memberName.value == '') {
    document.theform.memberName.className="ErrorField";
    alert("Member Name is required");
    document.theform.memberName.focus();
    return;
  }
  if (!madeChanges) {
    alert("No changes have been made. Save not allowed.")
    return;
  }
  document.theform.clubCode.disabled = false;
  document.theform.action_type.value = "update";
  submitAction("clubCodeMaint.do");
}

function doClear() {
  document.theform.reset();
  madeChanges = false;
  document.theform.memberName.className="input";
}

function markChanged() {
  madeChanges = true;
}

]]>
</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'AAA Club Code Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="rowCount" value=""/>

<xsl:if test="key('pagedata','result')/@value = '1'">
  <font class="OperationSuccess">Club Code <xsl:value-of select="key('pagedata','clubCode')/@value" /> saved</font>
  <br/>
</xsl:if>

<table width="800" border="3" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="800" border="0" cellpadding="2" cellspacing="0">
  <tr>
  	<td width="150">&nbsp;</td>
	  <td width="650">&nbsp;</td>
  </tr>
  <tr>
    <td class="label" align="right">Club Code:&nbsp;</td>
    <td align="left" colspan="4">
      <xsl:choose>
        <xsl:when test="key('pagedata','action_type')/@value = 'search'">
          <select name="clubCode" tabindex="1" style="width: 400px">
            <xsl:for-each select="root/aaaClubList/aaaClub">
              <option value="{@aaa_member_id}">
                <xsl:value-of select="@aaa_member_id"/>
                &nbsp;-&nbsp;
                <xsl:value-of select="@member_name"/>
              </option>
            </xsl:for-each>
          </select>
        </xsl:when>
        <xsl:otherwise>
          <input type="text" name="clubCode" value="{$clubCode}" disabled="true"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </tr>
  <xsl:choose>
    <xsl:when test="key('pagedata','action_type')/@value = 'search'">
      <tr>
        <td>&nbsp;</td>
        <td>
          <button class="blueButton" onClick="javascript:doGet()" accesskey="r" tabindex="3">(R)etrieve Data</button>
          &nbsp;&nbsp;
          <button class="blueButton" onClick="javascript:doAdd()" accesskey="a" tabindex="4">(A)dd New</button>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
    </xsl:when>
    <xsl:otherwise>
      <tr>
        <td class="label" align="right">Member Name:&nbsp;</td>
        <td>
          <input type="text" name="memberName" value="{$memberName}" size="50" maxlength="40" tabindex="2" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:if test="key('pagedata','action_type')/@value != 'search'">
    <tr>
      <td>&nbsp;</td>
      <td align="left">
        <button class="blueButton" onClick="javascript:doUpdate()" accesskey="s" tabindex="100">(S)ave</button>&nbsp;&nbsp;
      </td>
      <td align="left">
        <button class="blueButton" onClick="javascript:doClear()" accesskey="r" tabindex="110">(R)eset</button>&nbsp;&nbsp;
      </td>
      <td align="right">
        <button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="111">(E)xit</button>&nbsp;&nbsp;
      </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
  </xsl:if>
  
  </table>  
  
</td></tr>
</table>

</form>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>
