<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="header" >

<xsl:param name="showBackButton"/>
<xsl:param name="showSearchBox"/>
<xsl:param name="searchLabel"/>
<xsl:param name="showCSRIDs"/>
<xsl:param name="cservNumber"/>
<xsl:param name="showPrinter"/>
<xsl:param name="dnisNumber"/>
<xsl:param name="headerName"/>
<xsl:param name="brandName"/>
<xsl:param name="indicator"/>
<xsl:param name="showTime"/>

	<!--
	  If you plan to use the search box on your page you will need
	  to define a doEntryAction() function on your page in order to
	  catch and user actions
	-->
	   <script type="text/javascript" src="js/clock.js"/>
	   <table width="98%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="30%">
				<div class="floatleft">
					<img border="0" src="images/wwwftdcom_131x32.gif" width="131" height="32"/>
				</div>
				<xsl:if test="$showSearchBox">
				  <div class="floatright" id="searchBlock">
					     &nbsp;
             <label for="numberEntry" id="numberLabel"><xsl:value-of select="$searchLabel"/><br/>
               <input type="text" size="7" maxlength="20" name="numberEntry" id="numberEntry" onkeypress="doEntryAction();"/>&nbsp;&nbsp;
             </label>
				  </div>
				</xsl:if>
			</td>
			 <td  align="center" class="Header" id="pageHeader">
            <xsl:value-of select="$headerName"/>
       </td>
        <td  id="time" align="center" class="Label" width="20%">
            <xsl:if test="$showTime"><script type="text/javascript">startClock();</script></xsl:if>
        </td>
          <xsl:if test="$showBackButton">
            <td  align="right" width="5%">
            <input type="button" class="BlueButton" name="backButtonHeader" id="backButtonHeader" value="(B)ack" onclick="javascript:doBackAction();" accesskey="b"/>
           </td>
        </xsl:if>
		</tr>
		<tr>
			<td colspan="3">
				<div class="floatleft">
				  <xsl:if test="$dnisNumber != '0000'">
					<xsl:choose>
						<xsl:when test="$dnisNumber != '' and $brandName != ''">
							<span class="PopupHeader"> <xsl:value-of select="$dnisNumber"/> - <xsl:value-of select="$brandName" /></span>
						</xsl:when>
						<xsl:when test="$dnisNumber != ''">
							<span class="PopupHeader"><xsl:value-of select="$dnisNumber"/></span>
						</xsl:when>
						<xsl:when test="$brandName != ''">
							<span class="PopupHeader"><xsl:value-of select="$brandName"/></span>
						</xsl:when>
					</xsl:choose>
					</xsl:if>
				</div>
				<div class="floatright" >
					<span class="PopupHeader" ><xsl:value-of select="$cservNumber"/>&nbsp;<xsl:value-of select="$indicator"/></span>
				</div>
			</td>
			<td style="text-align:right;">
				<xsl:if test="$showPrinter">
            <a href="#" onkeypress="javascript:isPrintButtonClicked();"><img src="images/printer.jpg" width="25" height="25" name="printer" id="printer" border="0" onclick="javascript:printIt();" /></a>
        </xsl:if>
			</td>
		</tr>
				<xsl:if test="$showCSRIDs and CSR_VIEWINGS/CSR_VIEWING != ''">
    		<tr>
    			<td colspan="4"  id="selectBoxTd">
				 <span class="ErrorMessage">Currently being viewed by:&nbsp;</span>
				  <xsl:choose>
					<xsl:when test="count(//CSR_VIEWINGS/CSR_VIEWING) > 1">
						 <select id="currentCSRs" name="currentUsers" >
                 <xsl:for-each select="CSR_VIEWINGS/CSR_VIEWING">
                     <option>
                      <xsl:value-of select="csr_id"/>
                     </option>
                 </xsl:for-each>
             </select>
					</xsl:when>
				    <xsl:otherwise>
						<span id="csrLabel"><xsl:value-of select="//CSR_VIEWINGS/CSR_VIEWING/csr_id"/></span>
				    </xsl:otherwise>
				  </xsl:choose>				 
    			</td>
    		</tr>
				</xsl:if>
		<tr>
			<td colspan="4">
				<hr/>
			</td>
		</tr>
	   </table>
  <!--div id="iframe_div">
	   <iframe id="CSRFrame" name="CSRFrame" width="0px" height="0px"  border="0"></iframe>
  </div-->
  </xsl:template>
</xsl:stylesheet>