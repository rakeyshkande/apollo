<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="pagination.xsl"/>
<xsl:import href="sourceSearchOptions.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="pageNumber" select="key('pagedata','pageNumber')/@value"/>
<xsl:variable name="pageCountTotal" select="key('pagedata','pageCountTotal')/@value"/>
<xsl:variable name="recordCountTotal" select="key('pagedata','recordCountTotal')/@value"/>
<xsl:variable name="scRecordNumber" select="key('pagedata','scRecordNumber')/@value"/>
<!-- Hardcoded value: only 50 records per page can be viewed -->
<xsl:variable name="scPageMaxRecordCount" select="'50'"/>
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<title>
        Source Code Maintenance - Search
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<style type="text/css">
		    /* Scrolling table overrides for the search results table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" language="JavaScript">

var rowNumber = "<xsl:value-of select="$scRecordNumber"/>";
var sslSiteName = "<xsl:value-of select="$ssl_site_name"/>";

function initInputFieldStyle() {
    document.theform.scSourceCode.className="input";
    document.theform.scUpdatedOnStart.className="input";
    document.theform.scUpdatedOnEnd.className="input";
    document.theform.scEndDate.className="input";
}

// This is an object that validates the form fields when the Add button is pressed.
function FormValidatorAdd() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.scSourceCode.value == "") {
       msg = msg + "Please enter a Source Code.\n";
       if (document.theform.scSourceCode.tabIndex &lt; this.minTabIndex) {
        this.minTabIndex = document.theform.scSourceCode.tabIndex;
        this.focusOnField = document.theform.scSourceCode.name;
       }
       document.theform.scSourceCode.className="ErrorField";
   }
   
   this.errorMsg = msg;
}

// This is an object that validates the form fields when the Line Number GO button is pressed.
function FormValidatorJump() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   document.theform.recordJumpIndex.className="input";
   
   var msg = "";
   if (isNaN(document.theform.recordJumpIndex.value) || !document.theform.recordJumpIndex.value) {
       msg = msg + "Please enter a Line value that is numeric.\n";
        this.minTabIndex = document.theform.recordJumpIndex.tabIndex;
        this.focusOnField = document.theform.recordJumpIndex.name;
       document.theform.recordJumpIndex.className="ErrorField";
   } else if (!document.theform.elements['scSourceCode' + document.theform.recordJumpIndex.value]) {
   msg = msg + "Please enter a valid LINE number that is shown on-screen.\n";
   this.minTabIndex = document.theform.recordJumpIndex.tabIndex;
        this.focusOnField = document.theform.recordJumpIndex.name;
       document.theform.recordJumpIndex.className="ErrorField";
   }
   
   this.errorMsg = msg;
}

<![CDATA[
// This is an object that validates the form fields when the Search button is pressed.
function FormValidatorSearch() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.scUpdatedOnStart.value != "" && !validateDate(document.theform.scUpdatedOnStart.value, "U", "A")) {
       msg = msg + "Please enter a valid Last Updated On Start date in MM/DD/YYYY format.\n";
       if (document.theform.scUpdatedOnStart.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scUpdatedOnStart.tabIndex;
        this.focusOnField = document.theform.scUpdatedOnStart.name;
       }
       document.theform.scUpdatedOnStart.className="ErrorField";
   }
   if (document.theform.scUpdatedOnEnd.value != "" && !validateDate(document.theform.scUpdatedOnEnd.value, "U", "A")) {
       msg = msg + "Please enter a valid Last Updated On End date in MM/DD/YYYY format.\n";
       if (document.theform.scUpdatedOnEnd.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scUpdatedOnEnd.tabIndex;
        this.focusOnField = document.theform.scUpdatedOnEnd.name;
       }
       document.theform.scUpdatedOnEnd.className="ErrorField";
   }
   if (validateDate(document.theform.scUpdatedOnStart.value, "U", "A") && validateDate(document.theform.scUpdatedOnEnd.value, "U", "A")) {
   if ((document.theform.scUpdatedOnStart.value != "") && (document.theform.scUpdatedOnEnd.value != "")  && (Date.parse(document.theform.scUpdatedOnStart.value) > Date.parse(document.theform.scUpdatedOnEnd.value))) {
       msg = msg + "Last Updated On Start date can not be after Last Updated On End date.\n";
       if (document.theform.scUpdatedOnStart.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scUpdatedOnStart.tabIndex;
        this.focusOnField = document.theform.scUpdatedOnStart.name;
       }
       document.theform.scUpdatedOnStart.className="ErrorField";
   }
   }
   if (document.theform.scEndDate.value != "" && !validateDate(document.theform.scEndDate.value, "U", "A")) {
       msg = msg + "Please enter a valid Expiration Date in MM/DD/YYYY format.\n";
       if (document.theform.scEndDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scEndDate.tabIndex;
        this.focusOnField = document.theform.scEndDate.name;
       }
       document.theform.scEndDate.className="ErrorField";
   }
   
   this.errorMsg = msg;
}

function doBackAction() {
   history.back();
}

function doExit() {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
}

function setUp() {
    init();
    // If only a single record is shown on-screen, immediately jump to it.
    if (document.theform.recordCountTotal.value == "1" && document.theform.referrerPage.value != "SourceCodeMaint") {
     document.theform.recordJumpIndex.value = "1";
     doRecordJump("1");
    }
}
function init() {
   scroll(0,0);
   document.theform.hideSubmit.style.display = 'none';
   document.theform.scSourceCode.focus();
   if (document.theform.recordJumpIndex && document.all.searchContent.style.display != "none") {
   document.theform.recordJumpIndex.focus();
   }
}
]]>
function doClear() {
  clearAllFields(theform);
  initInputFieldStyle();
  document.theform.scRecordNumber.value = "1";
  document.theform.scRecordSortBy.value = "";
  document.theform.scActionType.value = "";
  if (document.all.searchContent) {
  document.all.searchContent.style.display = "none";
  }
  init();
}

function doAdd(targetSourceCode) {
   var formValidator = new FormValidatorAdd();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
    // doEdit(targetSourceCode);
    document.theform.scSourceCode.value = "";
    document.theform.requestSourceCode.value = targetSourceCode;
     document.theform.action_type.value = "load";     
     document.theform.recordCountTotal.value = "-1";
     // Submit to SSL site.
     submitInfoActionSSL("sourceCodeSearch.do", "Retrieving Record");
   }
}

function doEdit(targetSourceCode) {
   // Set a hidden field.
   document.theform.requestSourceCode.value = targetSourceCode;
   document.theform.action_type.value = "load";
   submitInfoActionSSL("sourceCodeMaint.do", "Retrieving Record");
}

function isPrintButtonClicked() {
   // Verify if the Enter key was pressed.
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
	  return false;
    
   printIt();
}

function isGoClicked() {
   // Verify if the Enter key was pressed.
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
	  return false;
    
   doRecordJump(document.theform.recordJumpIndex.value);
}
// Given a search results line number, edits the corresponding source code.
function doRecordJump(recordLineNumber) {
     
   // Validate the input.
   var formValidator = new FormValidatorJump();
   if (formValidator.errorMsg != "") {
    alert(formValidator.errorMsg);
    document.theform[formValidator.focusOnField].focus();
    return;
   }
   var cleanLine = stripZeros(recordLineNumber);
   var targetSourceCode = document.getElementById("scSourceCode" + cleanLine).value;
   doEdit(targetSourceCode);
}

function doSearch24Hours() {
   doClear();
   document.theform.scActionType.value = "search24Hours";
   submitInfoAction("sourceCodeSearch.do", "Searching");
}

function doSearch10Records() {
   doClear();
   document.theform.scActionType.value = "search10Records";
   submitInfoAction("sourceCodeSearch.do", "Searching");
}

function doSortedSearch(recordNumber, recordSortBy) {
   // Set a hidden field.
   document.theform.scRecordSortBy.value = recordSortBy;
   doSearch(recordNumber);
}

function doAdHocSearch(recordNumber, recordSortBy) {
   // Set a hidden field.
   document.theform.scActionType.value = "search";
   document.theform.scRecordSortBy.value = recordSortBy;
   // Submit to SSL site.
   //doSearch(recordNumber);
   doSearchSSL(recordNumber);
}

function doSearch(recordNumber) {
   var formValidator = new FormValidatorSearch();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
   document.theform.scRecordNumber.value = recordNumber;
   submitInfoAction("sourceCodeSearch.do", "Searching");
   }
}

function doSearchSSL(recordNumber) {
   var formValidator = new FormValidatorSearch();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
   document.theform.scRecordNumber.value = recordNumber;
   submitInfoActionSSL("sourceCodeSearch.do", "Searching");
   }
}

function doSearchPageNext() {
  // Note that the multiplication with 1 forces the vars to numbers, rather than strings.
  doSearch(parseInt(document.theform.scRecordNumber.value)  + parseInt(document.theform.scPageMaxRecordCount.value));
}

function doSearchPagePrevious() {
  doSearch(document.theform.scRecordNumber.value - document.theform.scPageMaxRecordCount.value);
}

function doSearchPageLast() {
  doSearch((document.theform.pageCountTotal.value - 1) * document.theform.scPageMaxRecordCount.value + 1);
}

function submitInfoAction(url, visualMessage) {
   scroll(0,0);
   showWaitMessage("content", "wait", visualMessage);
   submitAction(url);
}

function submitInfoActionSSL(url, visualMessage) {
   scroll(0,0);
   showWaitMessage("content", "wait", visualMessage);
   submitAction("https://" + sslSiteName + url);
}

function submitAction(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function printIt(){
	window.print(); 
}

</script>
</head>
<body onLoad="setUp()">
<xsl:choose>
<xsl:when test="$recordCountTotal &gt; '0'">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Source Code Maintenance - Search'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="true()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>
</xsl:when>
<xsl:otherwise>
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Source Code Maintenance - Search'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>
</xsl:otherwise>
</xsl:choose>
<form name="theform" method="post" action="">
        <xsl:call-template name="securityanddata"></xsl:call-template>
        <input type="hidden" name="scActionType" value="{key('pagedata','scActionType')/@value}"/>
        <input type="hidden" name="referrerPage" value="{key('pagedata','invokingPage')/@value}"/>
       <!-- <input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>-->
       <input type="hidden" name="action_type" value=""/>
        <input type="hidden" name="pageNumber" value="{$pageNumber}"/>
        <input type="hidden" name="pageCountTotal" value="{$pageCountTotal}"/>
        <input type="hidden" name="recordCountTotal" value="{$recordCountTotal}"/>
        <input type="hidden" name="scRecordNumber" value="{$scRecordNumber}"/>
        <input type="hidden" name="scPageMaxRecordCount" value="{$scPageMaxRecordCount}"/>
        <input type="hidden" name="scRecordSortBy" value="{key('pagedata','scRecordSortBy')/@value}"/>
        <input type="hidden" name="requestSourceCode" value=""/>
        <input type="submit" name="hideSubmit" value="Rendering...."/>
<div id="content" style="display:block">		 
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="$recordCountTotal = '0'">
				   <td class="errorMessage">
						NO RECORDS FOUND
				   </td>
          </xsl:when>
          <xsl:when test="key('pagedata','result')/@value = '-11'">
				   <td class="errorMessage">
						YOU DO NOT HAVE SECURITY PERMISSIONS TO CREATE SOURCE RECORDS.
				   </td>
          </xsl:when>
          	<!-- 13046 - Sympathy - customer will not be allowed to create SC that falls in the range reserved for sympathy -->
			<xsl:when test="key('pagedata','result')/@value = '-3'">
				<td class="errorMessage">
					REQUESTED SOURCE CODE IS RESERVED FOR AUTO GENERATED SYMPATHY SITES.
				</td>
			</xsl:when>
          
          </xsl:choose>
			   </tr>
		   </table>
        <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
          <tr>
            <td><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td width="20%" class="label" align="right">Source Code:</td>
                  <td width="35%" align="left"><input type="text" size="10" maxlength="10" name="scSourceCode" tabindex="1" selected="true" value="{key('pagedata','scSourceCode')/@value}" onChange="javascript:setValueToUpperCase('theform','scSourceCode')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" />
&nbsp;
&nbsp;
&nbsp;
&nbsp;
                  <xsl:if test="key('pagedata','SourceDefinitionData')/@value = 'Y' or key('pagedata','SourcePaymentMethodData')/@value = 'Y' or key('pagedata','SourcePriceHeaderData')/@value = 'Y' or key('pagedata','SourcePromotionCodeData')/@value = 'Y' or key('pagedata','SourcePCardData')/@value = 'Y' or key('pagedata','SourceSnhData')/@value = 'Y'">
                  <button class="blueButton" onClick="javascript:doAdd(document.theform.scSourceCode.value)" tabindex="2" accesskey="a">
              (A)dd New/Update
            </button>
            </xsl:if>
            </td>                  
                  <td width="20%" class="label" align="right">Expiration Date:</td>
                  <td width="25%" colspan="3"><input type="text" size="10" maxlength="10" name="scEndDate" tabindex="20" value="{key('pagedata','scEndDate')/@value}" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
&nbsp;
&nbsp;
                    <input type="image" id="calendarEndDate" tabindex="20" BORDER="0" SRC="images/calendar.gif" width="16" height="16"/>
                  </td>
                </tr>
                <tr>
                  <td align="right" class="label">Last Updated On:</td>
                  <td align="left"><input type="text" size="10" maxlength="10" name="scUpdatedOnStart" tabindex="3" value="{key('pagedata','scUpdatedOnStart')/@value}" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
&nbsp;
&nbsp;
                    <input type="image" id="calendarUpdatedOnStart" tabindex="3" BORDER="0" SRC="images/calendar.gif" width="16" height="16"/>
&nbsp;
&nbsp;
                    <font class="label">
                      to
                    </font>
&nbsp;
&nbsp;
                    <input type="text" size="10" maxlength="10" name="scUpdatedOnEnd" tabindex="4" value="{key('pagedata','scUpdatedOnEnd')/@value}" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
&nbsp;
&nbsp;
                    <input type="image" id="calendarUpdatedOnEnd" tabindex="4" BORDER="0" SRC="images/calendar.gif" width="16" height="16"/>
                  </td>
                  <td align="right" class="label">Partner Promotion Code:</td>
                  <td align="left" colspan="3"><input type="text" size="25" maxlength="25" name="scProgramName" tabindex="21" value="{key('pagedata','scProgramName')/@value}" onChange="javascript:setValueToUpperCase('theform','scProgramName')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
                </tr>
                <tr>
                  <td align="right" class="label">Last Updated By:</td>
                  <td align="left"><select name="scUpdatedBy" tabindex="5">
                      <option value=""></option>
                      <xsl:for-each select="root/deptUserList/deptUser">
                        <xsl:if test="@department_id = 'OPS'">
                          <option value="{@user_id}">
                          <xsl:if test="@user_id = key('pagedata','scUpdatedBy')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                           </xsl:if>
                            <xsl:value-of select="@last_name"/>,
                            <xsl:value-of select="@first_name"/>
                          </option>
                        </xsl:if>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="right" class="label">Website URL:</td>
                  <td align="left" colspan="3"><input type="text" size="50" maxlength="50" name="scProgramWebsiteUrl" tabindex="22" value="{key('pagedata','scProgramWebsiteUrl')/@value}" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
                </tr>
                <tr>
                  <td align="right" class="label">Requested By:</td>
                  <td align="left"><select name="scRequestedBy" tabindex="6">
                      <option value=""></option>
                      <xsl:for-each select="root/roleUserList/roleUser">
                          <option value="{@user_id}">
                          <xsl:if test="@user_id = key('pagedata','scRequestedBy')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                           </xsl:if>
                            <xsl:value-of select="@last_name"/>,
                            <xsl:value-of select="@first_name"/>
                          </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="right" class="label">PST Code:</td>
                  <td align="left" colspan="3"><input type="text" size="15" maxlength="15" name="scPromotionCode" tabindex="23" value="{key('pagedata','scPromotionCode')/@value}" onChange="javascript:setValueToUpperCase('theform','scPromotionCode')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
                </tr>
                <tr>
                  <td align="right" class="label">Company/Brand Code:</td>
                  <td align="left"><select name="scCompanyId" tabindex="7">
                      <option value=""></option>
                      <xsl:for-each select="root/companyList/company">
                        <option value="{@companyid}">
                        <xsl:if test="@companyid = key('pagedata','scCompanyId')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                           </xsl:if>
                          <xsl:value-of select="@companyname"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="right" class="label">Default Source Code:</td>
                  <td align="left" width="5%"><input type="checkbox" name="scDefaultSourceCodeFlag" tabindex="24" value="Y">
                  <xsl:if test="key('pagedata','scDefaultSourceCodeFlag')/@value != ''">
                            <xsl:attribute name="checked">
                          true
                           </xsl:attribute>
                           </xsl:if>
                  </input>
                  </td>
                  <td align="right" width="10%" class="label">IOTW Flag:</td>
                  <td align="left" width="5%"><input type="checkbox" name="scIotwFlag" tabindex="24" value="Y">
                  <xsl:if test="key('pagedata','scIotwFlag')/@value != ''">
                            <xsl:attribute name="checked">
                          true
                           </xsl:attribute>
                           </xsl:if>
                  </input>
                  </td>
                </tr>
                <tr>
                  <td align="right" class="label">Source Type:</td>
                  <td align="left"><input type="text" size="50" maxlength= "50" name="scSourceType" value="{key('pagedata','scSourceType')/@value}" tabindex="8" onChange="javascript:setValueToUpperCase('theform','scSourceType')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/></td>
                  <td align="right" class="label">JCPenney Flag:</td>
                  <td align="left" colspan="3"><input type="checkbox" name="scJcpenneyFlag" tabindex="25" value="Y">
                  <xsl:if test="key('pagedata','scJcpenneyFlag')/@value != ''">
                            <xsl:attribute name="checked">
                          true
                           </xsl:attribute>
                           </xsl:if>
                  </input>
                  </td>
                </tr>
                <tr>
                  <td align="right" class="label">Pricing Code:</td>
                  <td align="left"><select name="scPriceHeaderId" tabindex="9" >
                      <option value=""></option>
                      <xsl:for-each select="/root/pricingCodeList/pricingCode">
                        <option value="{@price_header_id}">
                        <xsl:if test="@price_header_id = key('pagedata','scPriceHeaderId')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                           </xsl:if>
                          <xsl:value-of select="@price_header_id"/> - <xsl:value-of select="@description"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="right" class="label">Over 21 Flag:</td>
                  <td align="left" colspan="3"><input type="checkbox" name="scOver21Flag" tabindex="26" value="Y">
                  <xsl:if test="key('pagedata','scOver21Flag')/@value != ''">
                            <xsl:attribute name="checked">
                          true
                           </xsl:attribute>
                           </xsl:if>
                  </input>
                  </td>
                </tr>
                <tr>
                  <td align="right" valign="top" class="label">Service Charge Code:</td>
                  <td align="left" valign="top"><select name="scSnhId" tabindex="10" >
                      <option value=""></option>
                      <xsl:for-each select="root/serviceFeeCodeList/serviceFeeCode">
                        <option value="{@snh_id}">
                        <xsl:if test="@snh_id = key('pagedata','scSnhId')/@value">
                            <xsl:attribute name="selected">
                          true
                           </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="@snh_id"/>&nbsp;-&nbsp;
                            <xsl:value-of select="@description"/>&nbsp;-&nbsp;DF&nbsp;=&nbsp;
                            <xsl:value-of select="@first_order_domestic"/>&nbsp;,&nbsp;I&nbsp;=&nbsp;
                            <xsl:value-of select="@first_order_international"/>&nbsp;,&nbsp;V&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_charge"/>&nbsp;,&nbsp;VS&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_sat_upcharge"/>&nbsp;,&nbsp;SUN&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_sun_upcharge"/>&nbsp;,&nbsp;M&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_mon_upcharge"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td align="left" colspan="1" class="label">&nbsp;</td>
                  <td  class="label" align="center">
									QUICK SEARCH <br/>                  
								  <button class="blueButton" tabindex="27" onClick="javascript:doSearch24Hours()" accesskey="2">
              Last (2)4 Hours
            </button><br/>
								  OR <br/>
								  <button class="blueButton" tabindex="28" onClick="javascript:doSearch10Records()" accesskey="1">
              Last (1)0 Records
            </button>
			</td>
                </tr>
              </table></td>
          </tr>
        </table>
        <xsl:call-template name="sourceSearchOptions"><xsl:with-param name="startTabIndex" select="50" /></xsl:call-template>
        <!-- Only show the results pane if results exist -->
        <xsl:if test="$recordCountTotal &gt; '0'">
          <div id="searchContent" style="display:block">
           <div class="header">
           Search Results
           </div>
           <div align="center">
           <label class="label" for="recordJumpIndex" id="recordJumpIndexLabel">Enter Line#:
           <input type="text" size="5" name="recordJumpIndex" tabindex="60" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" onKeyPress="javascript:isGoClicked()"/>
           </label>
           <button id="goButton" class="blueButton" onClick="javascript:doRecordJump(document.theform.recordJumpIndex.value)" tabindex="61" accesskey="g">
              (G)o
            </button>
           </div>
            <!--  Main Table thick blue border -->
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="70" /></xsl:call-template>
            <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
              <tr>
                <td><table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                      <td colspan="2"><div class="tableContainer" id="messageContainer" >
                          <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
                            <thead class="fixedHeader">
                              <tr style="text-align:left">
                                <td  class="ColHeaderLink" width="3%"  ></td>
                                <td  class="ColHeaderLink" width="13%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'sourceCode');" class="textlink" tabindex="80">
                                    SOURCE CODE
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="30%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'description');" class="textlink" tabindex="81">
                                    DESCRIPTION
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="30%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'sourceType');" class="textlink" tabindex="82">
                                    SOURCE TYPE
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="24%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'updatedOn');" class="textlink" tabindex="83">
                                    LAST UPDATED
                                  </a>
                                </td>
                              </tr>
                            </thead>
                            <tbody class="scrollContent" >
                              <xsl:for-each select="root/sourceList/source">
                                <tr id="row">
                                <!-- Alternate background line shading -->
                              	<xsl:choose>
		                             <xsl:when test="(position() mod 2) = 0">
                                    <xsl:attribute name="style">vertical-align:text-top;background:#ffffff</xsl:attribute>
                                 </xsl:when>
		                             <xsl:otherwise>
                                   <xsl:attribute name="style">vertical-align:text-top;background:#f4f4f4</xsl:attribute>
                                 </xsl:otherwise>
                                </xsl:choose>

                                  <td style="text-align:left;"><script type="text/javascript">
													 	<![CDATA[
													 	  document.write(rowNumber++ + ".");

											 			]]>
</script>
                                  </td>
                                  <td id="td_{@num}" style="text-align:left;"><a href="javascript:doEdit('{@source_code}');" class="textlink">
                                      <xsl:attribute name="tabindex"><xsl:value-of select="@num + 100"/></xsl:attribute>
                                      &nbsp;<xsl:value-of select="@source_code"/>
                                    </a>
                                    <input type="hidden" id="scSourceCode{@rnum}" value="{@source_code}"/>
                                  </td>
                                  <td style="text-align:left;"><xsl:value-of select="@description"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@source_type"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@updated_on"/> - <xsl:value-of select="@updated_by"/>
                                    </td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div></td>
                    </tr>
                    <tr>
                      <td class="Label" width="50%" >
                      </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="400" /></xsl:call-template>
        <xsl:call-template name="sourceSearchOptions"><xsl:with-param name="startTabIndex" select="450" /></xsl:call-template>
        <table width="99%" border="0" cellpadding="0" cellspacing="0">
          <tr><td align="right">
            <button class="blueButton" onclick="javascript:scroll(0,0)" tabindex="500" accesskey="t">(T)op of page</button>
          </td></tr>
        </table>
        <!-- end of the entire search results div -->
        </div>
        </xsl:if>
        <!-- end of the entire content div -->
        </div>
      </form>
<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>

<xsl:call-template name="footer"></xsl:call-template>

</body></html>  <script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "scUpdatedOnStart",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendarUpdatedOnStart"  // ID of the button
    }
  );
  Calendar.setup(
    {
      inputField  : "scUpdatedOnEnd",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendarUpdatedOnEnd"  // ID of the button
    }
  );
  Calendar.setup(
    {
      inputField  : "scEndDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendarEndDate"  // ID of the button
    }
  );
</script>
  </xsl:template>
  </xsl:stylesheet>
  