<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="pagination" >
<xsl:param name="startTabIndex"/>

<table border="0" width="98%" align="center" cellpadding="0" cellspacing="1" >
              <tr valign="top">
                <td width="1%"><xsl:choose>
                    <xsl:when test="number($pageNumber) &gt; 1">
                      <button name="firstItem" class="arrowButton"  id="firstItem" onclick="javascript:doSearch(1)">
                      <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex"/></xsl:attribute>
                      7</button>
                    </xsl:when>
                    <xsl:otherwise>
                      <button name="firstItem2" class="arrowButton"  id="firstItem2" onclick="" tabindex="-1">
                        <xsl:attribute name="disabled">
                          true
                        </xsl:attribute>
                      7</button>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td width="1%"><xsl:choose>
                    <xsl:when test="number($pageNumber) &gt; 1">
                      <button name="backItem" class="arrowButton" id="backItem" onclick="javascript:doSearchPagePrevious()">
                      <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 1"/></xsl:attribute>
                      3 </button>
                    </xsl:when>
                    <xsl:otherwise>
                      <button name="backItem2" class="arrowButton" id="backItem2" onclick="" tabindex="-1">
                        <xsl:attribute name="disabled">
                          true
                        </xsl:attribute>
                        3 </button>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td width="1%"><xsl:choose>
                    <xsl:when test="number($pageNumber) &lt; number($pageCountTotal)">
                      <button name="nextItem" class="arrowButton" id="nextItem" onclick="javascript:doSearchPageNext()">
                      <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 2"/></xsl:attribute>
                      4</button>
                    </xsl:when>
                    <xsl:otherwise>
                      <button name="nextItem2" class="arrowButton" id="nextItem2" onclick="" tabindex="-1">
                        <xsl:attribute name="disabled">
                          true
                        </xsl:attribute>
                      4</button>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td width="1%"><xsl:choose>
                    <xsl:when test="number($pageNumber) &lt; number($pageCountTotal)">
                      <button name="lastItem" class="arrowButton" id="lastItem" onclick="javascript:doSearchPageLast()">
                      <xsl:attribute name="tabindex"><xsl:value-of select="$startTabIndex + 3"/></xsl:attribute>
                      8</button>
                    </xsl:when>
                    <xsl:otherwise>
                      <button name="lastItem2" class="arrowButton" id="lastItem2" onclick="" tabindex="-1">
                        <xsl:attribute name="disabled">
                          true
                        </xsl:attribute>
                      8</button>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td class="Label" width="50%" >&nbsp;
                  Page
&nbsp;
                  <xsl:value-of select="$pageNumber"/>
&nbsp;
                  of
&nbsp;
                  <xsl:value-of select="$pageCountTotal"/>
                </td>
                <td class="Label" align="right" width="50%"><xsl:value-of select="$recordCountTotal"/>
&nbsp;Record<xsl:if test="$recordCountTotal &gt; '1'">s</xsl:if>&nbsp;Found
&nbsp;
                </td>
                <td align="right"><!--<button class="BlueButton" style="width:55px" tabindex="5" accesskey="b" onclick="javascript:doBackAction();">(B)ack</button>-->
                </td>
              </tr>
              <tr>
                <td colspan="5" align="right"><!--<button class="BigBlueButton" tabindex="6" onclick="javascript:doMainMenuAction();" style="width:55px;" accesskey="m" >(M)ain<br/>Menu</button>-->
                </td>
              </tr>
            </table>

</xsl:template>
</xsl:stylesheet>