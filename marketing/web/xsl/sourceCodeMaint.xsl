<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY space "&#160;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:import href="searchCriteria.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   <xsl:key name="sourceBinCheckPartnerList" match="root/sourceList/sourceBinCheckPartnerList/partner" use="@partner_name" />
   <xsl:key name="productAttributeExclusionList" match="/root/sourceList/product_attribute_exclusions/product_attribute_exclusion" use="@product_attr_restr_id" />
   

   <!-- The source record metrics are placed here for easy maintenance -->
   <xsl:variable name="description" select="root/sourceList/source/@description"/>
   <xsl:variable name="start_date" select="root/sourceList/source/@start_date"/>
   <xsl:variable name="end_date" select="root/sourceList/source/@end_date"/>
   <xsl:variable name="company_id" select="root/sourceList/source/@company_id"/>
   <xsl:variable name="source_type" select="root/sourceList/source/@source_type"/>
   <xsl:variable name="price_header_id" select="root/sourceList/source/@price_header_id"/>
   <xsl:variable name="snh_id" select="root/sourceList/source/@snh_id"/>
   <xsl:variable name="mp_redemption_rate_id" select="root/sourceList/source/@mp_redemption_rate_id"/>
   <xsl:variable name="payment_method_id" select="root/sourceList/source/@payment_method_id"/>
   <xsl:variable name="order_source" select="root/sourceList/source/@order_source"/>
   <xsl:variable name="requested_by" select="root/sourceList/source/@requested_by"/>
   <xsl:variable name="promotion_code" select="root/sourceList/source/@promotion_code"/>
   <xsl:variable name="bonus_promotion_code" select="root/sourceList/source/@bonus_promotion_code"/>
   <xsl:variable name="partner_bank_id" select="root/sourceList/source/@partner_bank_id"/>
   <xsl:variable name="related_source_code" select="root/sourceList/source/@related_source_code"/>
   <xsl:variable name="program_website_url" select="root/sourceList/source/@program_website_url"/>
   <xsl:variable name="default_source_code_flag" select="root/sourceList/source/@default_source_code_flag"/>
   <xsl:variable name="highlight_description_flag" select="root/sourceList/source/@highlight_description_flag"/>
   <xsl:variable name="send_to_scrub" select="root/sourceList/source/@send_to_scrub"/>
   <xsl:variable name="primary_backup_rwd_flag" select="root/sourceList/source/@primary_backup_rwd_flag"/>
   <xsl:variable name="oscar_selection_enabled_flag" select="root/sourceList/source/@oscar_selection_enabled_flag"/>
   <xsl:variable name="oscar_scenario_group_id" select="root/sourceList/source/@oscar_scenario_group_id"/>  
   <xsl:variable name="enable_lp_processing" select="root/sourceList/source/@enable_lp_processing"/>
   <xsl:variable name="discount_allowed_flag" select="root/sourceList/source/@discount_allowed_flag"/>
   <xsl:variable name="requires_delivery_confirmation" select="root/sourceList/source/@requires_delivery_confirmation"/>
   <xsl:variable name="emergency_text_flag" select="root/sourceList/source/@emergency_text_flag"/>
   <xsl:variable name="comment_text" select="root/sourceList/source/@comment_text"/>
   <xsl:variable name="updated_on" select="root/sourceList/source/@updated_on"/>
   <xsl:variable name="updated_by" select="root/sourceList/source/@updated_by"/>
   <xsl:variable name="invoice_password" select="root/sourceList/source/@invoice_password"/>
   <xsl:variable name="iotw_flag" select="root/sourceList/source/@iotw_flag"/>
   <xsl:variable name="gift_certificate_flag" select="root/sourceList/source/@gift_certificate_flag"/>
   <xsl:variable name="billing_info_flag" select="root/sourceList/source/@billing_info_flag"/>
   <xsl:variable name="display_service_fee_code" select="root/sourceList/source/@display_service_fee_code"/>
   <xsl:variable name="display_shipping_fee_code" select="root/sourceList/source/@display_shipping_fee_code"/>
   <xsl:variable name="display_same_day_upcharge" select="root/sourceList/source/@display_same_day_upcharge"/>
   <xsl:variable name="same_day_upcharge" select="root/sourceList/source/@same_day_upcharge"/>
   <xsl:variable name="morning_delivery_flag" select="root/sourceList/source/@morning_delivery_flag"/>
   <xsl:variable name="calculate_tax_flag" select="root/sourceList/source/@calculate_tax_flag"/>
   <xsl:variable name="custom_shipping_carrier" select="root/sourceList/source/@custom_shipping_carrier"/>
   <xsl:variable name="morning_delivery_charged_to_FS_members" select="root/sourceList/source/@morning_delivery_free_shipping"/>
   <xsl:variable name="delivery_fee_id" select="root/sourceList/source/@delivery_fee_id"/>
 	<xsl:variable name="merch_amt_full_refund_flag" select="root/sourceList/source/@merch_amt_full_refund_flag"/>
 
 	<xsl:variable name="automated_promotion_engine_flag" select="root/sourceList/source/@auto_promotion_engine"/>
 	<xsl:variable name="ape_product_catalog" select="root/sourceList/source/@ape_product_catalog"/>
 	<xsl:variable name="ape_base_source_codes" select="root/apeSourceCodes/base_source_code/@value"/>
    <!--Fuel Surcharge variables -->
    <xsl:variable name="apply_surcharge_code" select="root/sourceList/source/@apply_surcharge_code"/>
    <xsl:variable name="surcharge_amount" select="root/sourceList/source/@surcharge_amount"/>
    <xsl:variable name="surcharge_description" select="root/sourceList/source/@surcharge_description"/>
    <xsl:variable name="display_surcharge_flag" select="root/sourceList/source/@display_surcharge_flag"/>

   <xsl:variable name="allow_free_shipping_flag" select="root/sourceList/source/@allow_free_shipping_flag"/>

   <xsl:variable name="credit_card_type" select="root/cpcList/cpc/@credit_card_type"/>
   <!-- defect 2655. Mask credit card number -->
   <xsl:variable name="credit_card_number" select="root/cpcList/cpc/@credit_card_number"/>
   <xsl:variable name="credit_card_expiration_date" select="root/cpcList/cpc/@credit_card_expiration_date"/>

   <xsl:variable name="program_name" select="root/sourceProgramList/sourceProgram/@program_name"/>
   <xsl:variable name="partner_name" select="root/sourceProgramList/sourceProgram/@partner_name"/>
   <xsl:variable name="source_primary_florist_flag" select="root/sourceProgramList/sourceProgram/@source_primary_florist_flag"/>

   <!-- defect 3113 - Webloyalty Flag -->
   <xsl:variable name="webloyalty_flag" select="root/sourceList/source/@webloyalty_flag"/>
   
   <xsl:variable name="add_on_free_id" select="root/sourceList/source/@add_on_free_id"/>
   <xsl:variable name="today" select="root/pageData/data[@name='today']/@value" />

   <!--  Sympathy Controls -->
   <xsl:variable name="funeral_cemetery_loc_chk" select="root/sourceList/source/@funeral_cemetery_loc_chk"/>
   <xsl:variable name="hospital_loc_chck" select="root/sourceList/source/@hospital_loc_chck"/>
   <xsl:variable name="funeral_cemetery_lead_time_chk" select="root/sourceList/source/@funeral_cemetery_lead_time_chk"/>
   <xsl:variable name="funeral_cemetery_lead_time" select="root/sourceList/source/@funeral_cemetery_lead_time"/>
   <xsl:variable name="bo_hrs_mon_fri_start" select="root/sourceList/source/@bo_hrs_mon_fri_start"/>
   <xsl:variable name="bo_hrs_mon_fri_end" select="root/sourceList/source/@bo_hrs_mon_fri_end"/>
   <xsl:variable name="bo_hrs_sat_start" select="root/sourceList/source/@bo_hrs_sat_start"/>
   <xsl:variable name="bo_hrs_sat_end" select="root/sourceList/source/@bo_hrs_sat_end"/>
   <xsl:variable name="bo_hrs_sun_start" select="root/sourceList/source/@bo_hrs_sun_start"/>
   <xsl:variable name="bo_hrs_sun_end" select="root/sourceList/source/@bo_hrs_sun_end"/>
  
  <!-- Legacy ID  -->
  <xsl:variable name="legacy_id" select="root/sourceList/source/@legacy_id"/>
  <!-- SGC-2 Same Day Upcharge FS Member Controls -->
  <xsl:variable name="same_day_upcharge_fs" select="root/sourceList/source/@same_day_upcharge_fs"/>
  
  
   <!-- Root template -->
   <xsl:template match="/">

<html>
<head>
<title>Source Code Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/FormChek.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" language="JavaScript">
var isNewRecord = false;
promoCount = 0;
memberLvlCount=0;
memberLvlArray = new Array();
startArray = new Array();
var siteName = "<xsl:value-of select="$site_name"/>";
var sslSiteName = "<xsl:value-of select="$ssl_site_name"/>";

<![CDATA[

function doBackAction() {
   if (changedFlag == 'Y' && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doSave();
   } else {
   document.theform.action_type.value = "reload";
   if (document.theform.creditCardNumber) {
      document.theform.creditCardNumber.value = "";
   }
   submitAction("sourceCodeSearch.do");
   }
}

function doExit() {
   if (changedFlag == 'Y' && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doSave();
   } else {
   // revert unsecured site.
   if (document.theform.creditCardNumber) {
      document.theform.creditCardNumber.value = "";
   }
   document.theform.action = "http://" + siteName + "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }
}

function init() {
   if (document.theform.startDate && document.theform.startDate.value == "") {
   isNewRecord = true;
   var currDate = new Date();
   document.theform.startDate.value = (currDate.getMonth() + 1) + "/" + currDate.getDate() + "/" + currDate.getFullYear();
   }
   myArray = new Array();
   partnerName="";

   // only set up partner program data if inserting new record (assume description is null)
   if ((!document.theform.partner_name.value) && (document.theform.allowPartnerProgram.value == 'Y'))
   {
     for(var i=0;i < document.theform.programName.options.length;i++)
     {
       temp = new Array();
       work = document.theform.programName.options[i].text;
       temp = work.split('|');
       if (partnerName == '')
       {
         partnerName = temp[0];
         partnerCnt = 0;
         promoCnt = 0;
         myArray[partnerCnt] = new Array();
       } else {
         if (partnerName != temp[0])
         {
           partnerName = temp[0];
           partnerCnt = partnerCnt + 1;
           promoCnt = 0;
           myArray[partnerCnt] = new Array();
         }
       }
       myArray[partnerCnt][promoCnt] = new Array();
       myArray[partnerCnt][promoCnt][0] = trimString(temp[0]);
       myArray[partnerCnt][promoCnt][1] = work;
       promoCnt = promoCnt + 1;
     }

     // Initialize the related field groups on the page.
     // This is the Partner to Program field relationship.
     partnerChange();
   }

   // This is the Payment Method Id to Credit Card relationship.
   modifyCreditCardFields();

   // set change flag to N
   changedFlag = 'N';

]]>
  <xsl:for-each select="root/sourceProgramList/sourceProgram">
    startArray[promoCount] = '<xsl:value-of select="@start_date"/>'
    promoCount = promoCount+1;

    startDate = '<xsl:value-of select="@start_date"/>'
    endDate = '<xsl:value-of select="@end_date"/>'
    lastPostDate = '<xsl:value-of select="@last_post_date"/>'
    buttonName = 'edit<xsl:value-of select="@num"/>'
    createdOn = '<xsl:value-of select="@created_on"/>'


<![CDATA[
    now = new Date();
    thisMonth = now.getMonth() + 1;
    if (thisMonth < 10) thisMonth = '0' + thisMonth;
    thisDay = now.getDate();
    if (thisDay < 10) thisDay = '0' + thisDay;
    thisYear = now.getFullYear();
    today = thisYear + thisMonth + thisDay;

    temp = new Array;
    temp = startDate.split('/');
    sDate = temp[2] + temp[0] + temp[1];
    if (lastPostDate) {
      lastPostDate = lastPostDate.substring(0,10);
      temp = lastPostDate.split('-');
      lpDate = temp[0] + temp[1] + temp[2];
    } else {
      lpDate = '';
    }
    // It's OK to edit if created_on is after last_post_date
    if ((lpDate >= sDate) && (createdOn < lastPostDate)) {
      document.getElementById(buttonName).style.display = 'none';
    } else {
      if (endDate) {
        temp = endDate.split('/');
        eDate = temp[2] + temp[0] + temp[1];
        if (eDate <= today) {
          document.getElementById(buttonName).style.display = 'none';
        }
      }
    }
]]>
  </xsl:for-each>
  <xsl:for-each select="root/mp_member_levels/mp_member_level">	
    temp = '<xsl:value-of select="@mp_member_level_id"/>';
	<![CDATA[
	memberLvlArray[memberLvlCount] = temp;
	]]>
	memberLvlCount = memberLvlCount + 1;	    
    </xsl:for-each>
<![CDATA[

  focusToTop();
}

function setCCNumberUpdated () {
    document.theform.cc_number_updated_flag.value = "Y";
}

function setDefaultFieldValues() {

  // If the values are undefined, set them.
  if (!document.theform.defaultSourceCodeFlag.value)
  document.theform.defaultSourceCodeFlag[1].checked = true;
  if (!document.theform.highlightDescriptionFlag.value)
  document.theform.highlightDescriptionFlag[1].checked = true;
  if (!document.theform.sameDayGiftFlag.value)
  document.theform.sameDayGiftFlag[0].checked = true;
  if (!document.theform.sendToScrub.value)
  document.theform.sendToScrub[1].checked = true;
  if (!document.theform.over21Flag.value)
  document.theform.over21Flag[1].checked = true;
  if (!document.theform.enableLpProcessing.value)
  document.theform.enableLpProcessing[0].checked = true;
  if (!document.theform.discountAllowedFlag.value)
  document.theform.discountAllowedFlag[1].checked = true;
  if (!document.theform.requiresDeliveryConfirmation.value)
  document.theform.requiresDeliveryConfirmation[1].checked = true;
  if (!document.theform.webloyaltyFlag.value)
  document.theform.webloyaltyFlag[1].checked = true;
  if (!document.theform.emergencyTextFlag.value)
  document.theform.emergencyTextFlag[0].checked = true;
  if (!document.theform.displayServiceFeeCode.value)
  document.theform.displayServiceFeeCode[2].checked = true;
  if (!document.theform.displayShippingFeeCode.value)
  document.theform.displayShippingFeeCode[2].checked = true;
  if (!document.theform.randomWeightedFlag.value)
  document.theform.randomWeightedFlag[1].checked = true;
  if (!document.theform.allowFreeShippingFlag.value)
  document.theform.allowFreeShippingFlag[1].checked = true;
  if (!document.theform.applySurchargeCode.value)
  document.theform.applySurchargeCode[2].checked = true;
   if (!document.theform.displaySurcharge.value)
  document.theform.displaySurcharge[2].checked = true;
}

function focusToTop() {
  scroll(0,0);
  if (document.theform.description) {
  document.theform.description.focus();
  }
}

function trimToString(inString) {
  if (inString) {
     return trimString(inString);
  } else {
     return "";
  }
}

function trimString(inString) {
  inString = inString.replace( /^\s+/g, "" ); // Strip leading
  return inString.replace( /\s+$/g, "" );     // Strip trailing
}

function partnerChange() {
   setChangedFlag('');
   newPartner = document.theform.partnerName.value;
   //alert(newPartner);
   if (newPartner == "") {
     disableProgramFields();
   } else {
     enableProgramFields();
     addCnt = 0;
     document.theform.programName.options.length = 0;
     for(var i=0;i < myArray.length;i++)
     {
       for(var j=0;j < myArray[i].length;j++)
       {
         if (myArray[i][j][0] == newPartner)
         {
           //alert(myArray[i][j][1]);
           temp = new Array();
           temp = myArray[i][j][1].split('|')
           document.theform.programName.options.length = addCnt + 1;
           document.theform.programName.options[addCnt].value = trimString(temp[1])
           document.theform.programName.options[addCnt].text = trimString(temp[1]) + " - " + trimString(temp[2]);
           addCnt = addCnt + 1;
         }
       }
     }
   }
}

function doReset() {
   document.theform.action_type.value = "load";
   submitActionSSL("sourceCodeMaint.do?requestSourceCode=" + document.theform.requestSourceCode.value);
}


// Function to check Preferred Florist (i.e., Primary/Backup florists) Ajax function
//
function validatePreferredFloristAjax() { 
    var url = 'sourceCodeMaint.do';
    var parameters = "action_type=preferredFloristValidation" + 
                     "&securitytoken=" + document.theform.securitytoken.value + 
                     "&context=" + document.theform.context.value + 
                     "&adminAction=" + document.theform.adminAction.value +
                     "&applicationcontext=" + document.theform.applicationcontext.value +
                     "&primaryFloristId=" + encodeURI(trimToString(document.theform.primaryFloristId.value)) + 
                     "&backupFloristIds=" + encodeURI(trimToString(document.theform.backupFloristIds.value));
    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    xmlHttp.onreadystatechange = evalValidatePreferredFloristAjax; 
    xmlHttp.open("POST", url , true); 
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", parameters.length);
    xmlHttp.send(parameters);
    scroll(0,0);
    showWaitMessage("content", "wait", "Processing");
}

// Function called on return from validatePreferredFloristAjax's Ajax call
//
function evalValidatePreferredFloristAjax() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.theform.primaryFloristId.className="";  // Clear out any prior error fields
        document.theform.backupFloristIds.className="";
        hideWaitMessage("content", "wait");
        var xml = getXMLobjXML(xmlHttp.responseText);
        
        // If there was an error display the error alert
        var isSuccess = xml.selectSingleNode("root/success").text;
        if(isSuccess != "Y") {
            var errorMsg   = xml.selectSingleNode("root/error_msg").text;
            var errorField = xml.selectSingleNode("root/error_field").text;
            if (errorField == "primary") {
                document.theform.primaryFloristId.className="ErrorField";
                document.theform.primaryFloristId.focus();
                errorMsg = "Please enter a valid Primary Florist ID or leave the field blank to unassign Primary Florist.";
            } else {
                document.theform.backupFloristIds.className="ErrorField";
                document.theform.backupFloristIds.focus();
                errorMsg = "The following Backup Florist IDs are invalid: " + errorMsg + 
                           ".  Please enter one or more valid Florist IDs to assign Backup Florists, or leave the field blank to unassign all Backup Florists.";
            }
            alert(errorMsg);
        } else {
            doSaveAjax();
        }
    }
}

// Loads text into an XML Document
//
function getXMLobjXML(xmlToLoad) {
    var xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
    xml.async = false;
    xml.resolveExternals = false;
    try {
        xml.loadXML(xmlToLoad);
        return(xml)
    } catch(e) {
        throw(xml.parseError.reason)
    }
}


function doClear() {
   clearAllFields(theform);
   initInputFieldStyle();
   partnerChange();
   modifyCreditCardFields();
   setDefaultFieldValues();
   focusToTop();
}

function initInputFieldStyle() {
   document.theform.description.className="input";
   document.theform.startDate.className="input";
   document.theform.endDate.className="input";
   document.theform.sourceType.className="input";
   document.theform.orderSource.className="input";
   document.theform.requestedBy.className="input";
   document.theform.companyId.className="input";
   document.theform.surchargeAmount.className="input";
   document.theform.surchargeDescription.className="input";

   if (document.theform.priceHeaderId) {
   document.theform.priceHeaderId.className="input";
   }
   if (document.theform.snhId) {
   document.theform.snhId.className="input";
   }
   if (document.theform.mpRedemptionRateId) {
   document.theform.mpRedemptionRateId.className="input";
   }   
   if (document.theform.paymentMethodId) {
   document.theform.paymentMethodId.className="input";
   }
   if (document.theform.creditCardType) {
   document.theform.creditCardType.className="input";
   }
   if (document.theform.creditCardNumber) {
   document.theform.creditCardNumber.className="input";
   }
   if (document.theform.creditCardExpirationMonth) {
   document.theform.creditCardExpirationMonth.className="input";
   }
   if (document.theform.creditCardExpirationYear) {
   document.theform.creditCardExpirationYear.className="input";
   }
}

function isNumeric(thisValue){
	var result = false;
	result = /^[0-9]+$/.test(thisValue);
	return result;
		
}

function FormValidator() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.confirmBackupMsg = "";
   this.minTabIndex = 99999;

   initInputFieldStyle();

   var msg = "";
     
   if (document.theform.description.value == "") {
       msg = msg + "Description is required.\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.description.name;
       }
       document.theform.description.className="ErrorField";
   }
   if (document.theform.startDate.value == "") {
       msg = msg + "Start Date is required.\n";
       if (document.theform.startDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.startDate.tabIndex;
        this.focusOnField = document.theform.startDate.name;
       }
       document.theform.startDate.className="ErrorField";
   } else if (!validateDate(document.theform.startDate.value, "U", "A")) {
       msg = msg + "Please enter a valid Start Date in MM/DD/YYYY format.\n";
       if (document.theform.startDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.startDate.tabIndex;
        this.focusOnField = document.theform.startDate.name;
       }
       document.theform.startDate.className="ErrorField";
   } else if (isNewRecord && !validateDate(document.theform.startDate.value, "U", "F")) {
       msg = msg + "Please enter a Start Date in MM/DD/YYYY format that is today or in the future.\n";
       if (document.theform.startDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.startDate.tabIndex;
        this.focusOnField = document.theform.startDate.name;
       }
       document.theform.startDate.className="ErrorField";
   }
   if (document.theform.endDate.value != "" && !validateDate(document.theform.endDate.value, "U", "A")) {
       msg = msg + "Please enter a valid Expiration Date in MM/DD/YYYY format.\n";
       if (document.theform.endDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.endDate.tabIndex;
        this.focusOnField = document.theform.endDate.name;
       }
       document.theform.endDate.className="ErrorField";
   }
   if (document.theform.endDate.value != "") {
     now = new Date();
     today = now.getTime();
     dateToCheck = new Date();
     temp = new Array();
     temp = document.theform.endDate.value.split('/');
     dateToCheck.setMonth(temp[0]-1);
     dateToCheck.setDate(temp[1]);
     dateToCheck.setYear(temp[2]);
     checkEndDate = dateToCheck.getTime();
     pastDate = (today > checkEndDate);
     if (pastDate)
     {
       msg = msg + "Expiration Date cannot be before current date.\n";
       if (document.theform.endDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.endDate.tabIndex;
        this.focusOnField = document.theform.endDate.name;
       }
       document.theform.endDate.className="ErrorField";
     }
     temp = document.theform.startDate.value.split('/');
     dateToCheck.setMonth(temp[0]-1);
     dateToCheck.setDate(temp[1]);
     dateToCheck.setYear(temp[2]);
     checkStartDate = dateToCheck.getTime();
     pastDate = (checkStartDate > checkEndDate);
     if (pastDate)
     {
       msg = msg + "Expiration Date cannot be before Start Date.\n";
       if (document.theform.endDate.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.endDate.tabIndex;
        this.focusOnField = document.theform.endDate.name;
       }
       document.theform.endDate.className="ErrorField";
     }
   }
   if (document.theform.companyId.value == "") {
       msg = msg + "Company Code is required.\n";
       if (document.theform.companyId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.companyId.tabIndex;
        this.focusOnField = document.theform.companyId.name;
       }
       document.theform.companyId.className="ErrorField";
   }
   if (document.theform.sourceType.value == "") {
       msg = msg + "Source Type is required.\n";
       if (document.theform.sourceType.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceType.tabIndex;
        this.focusOnField = document.theform.sourceType.name;
       }
       document.theform.sourceType.className="ErrorField";
   }
   if (document.theform.priceHeaderId && document.theform.priceHeaderId.value == "") {
       msg = msg + "Pricing Code is required.\n";
       if (document.theform.priceHeaderId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.priceHeaderId.tabIndex;
        this.focusOnField = document.theform.priceHeaderId.name;
       }
       document.theform.priceHeaderId.className="ErrorField";
   }
   if (document.theform.snhId && document.theform.snhId.value == "") {
       msg = msg + "Service Fee Code is required.\n";
       if (document.theform.snhId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.snhId.tabIndex;
        this.focusOnField = document.theform.snhId.name;
       }
       document.theform.snhId.className="ErrorField";
   }
   if (document.theform.orderSource.value == "") {
       msg = msg + "Order Source is required.\n";
       if (document.theform.orderSource.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.orderSource.tabIndex;
        this.focusOnField = document.theform.orderSource.name;
       }
       document.theform.orderSource.className="ErrorField";
   }
   if (document.theform.requestedBy.value == "") {
       msg = msg + "Requested By is required.\n";
       if (document.theform.requestedBy.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.requestedBy.tabIndex;
        this.focusOnField = document.theform.requestedBy.name;
       }
       document.theform.requestedBy.className="ErrorField";
   }
	   	var apeFlag = "";
	    for (var i = 0; i <document.theform.automatedPromotionEngineFlag.length; i++) {
			if (document.theform.automatedPromotionEngineFlag[i].checked) {
			apeFlag = document.theform.automatedPromotionEngineFlag[i].value
			}
		}
   if (document.theform.apeProductCatalog.value == "" && apeFlag == 'Y') {
       msg = msg + "APE Product Catalog is required if APE is enabled.\n";
       if (document.theform.apeProductCatalog.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.apeProductCatalog.tabIndex;
        this.focusOnField = document.theform.apeProductCatalog.name;
       }
       document.theform.apeProductCatalog.className="ErrorField";
   }
   if (document.theform.apeBaseSourceCodes.value == "" && apeFlag == 'Y') {
       msg = msg + "APE Base Source Codes are required if APE is enabled.\n";
       if (document.theform.apeBaseSourceCodes.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.apeBaseSourceCodes.tabIndex;
        this.focusOnField = document.theform.apeBaseSourceCodes.name;
       }
       document.theform.apeBaseSourceCodes.className="ErrorField";
   }
   if (document.theform.apeBaseSourceCodes.value != "" && apeFlag == 'Y') {
   		var sourceCodes = document.theform.apeBaseSourceCodes.value;
   		var sourceTokens = sourceCodes.split( " " );
		if(sourceTokens.length > 50){
	       msg = msg + "APE Base Source Code(s) cannot exceed a maximum of 50.\n";
	       if (document.theform.apeBaseSourceCodes.tabIndex < this.minTabIndex) {
	        this.minTabIndex = document.theform.apeBaseSourceCodes.tabIndex;
	        this.focusOnField = document.theform.apeBaseSourceCodes.name;
	       }
	       document.theform.apeBaseSourceCodes.className="ErrorField";
	    }
   }
   if (document.theform.paymentMethodId && document.theform.paymentMethodId.value == "PC") {

      if (document.theform.creditCardType && document.theform.creditCardType.value == "") {
         msg = msg + "Purchasing Card Type is required.\n";
         if (document.theform.creditCardType.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.creditCardType.tabIndex;
            this.focusOnField = document.theform.creditCardType.name;
         }
         document.theform.creditCardType.className="ErrorField";
      }
      pattern = /^[0-9]*$/;
      // only check credit card number if credit card number has been updated
      if (document.theform.cc_number_updated_flag.value == "Y") {
          if (document.theform.creditCardNumber && document.theform.creditCardNumber.value == "") {
             msg = msg + "Purchasing Card Number is required.\n";
             if (document.theform.creditCardNumber.tabIndex < this.minTabIndex) {
                this.minTabIndex = document.theform.creditCardNumber.tabIndex;
                this.focusOnField = document.theform.creditCardNumber.name;
             }
             document.theform.creditCardNumber.className="ErrorField";
          } else if (pattern.test(document.theform.creditCardNumber.value)==false) {
             msg = msg + "Purchasing Card Number must be numeric.\n";
             if (document.theform.creditCardNumber.tabIndex < this.minTabIndex) {
                this.minTabIndex = document.theform.creditCardNumber.tabIndex;
                this.focusOnField = document.theform.creditCardNumber.name;
             }
             document.theform.creditCardNumber.className="ErrorField";
          }
      }
      if (document.theform.creditCardExpirationMonth && (document.theform.creditCardExpirationMonth.value == "" || isNaN(document.theform.creditCardExpirationMonth.value))) {
         msg = msg + "Purchasing Card Expiration Month is required and must be numeric.\n";
         if (document.theform.creditCardExpirationMonth.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.creditCardExpirationMonth.tabIndex;
            this.focusOnField = document.theform.creditCardExpirationMonth.name;
         }
         document.theform.creditCardExpirationMonth.className="ErrorField";
      } else if (document.theform.creditCardExpirationMonth) {
        var month = parseInt(document.theform.creditCardExpirationMonth.value);
           if (month > 12 || month < 0) {
           msg = msg + "Purchasing Card Expiration Month must be between 1 and 12.\n";
         if (document.theform.creditCardExpirationMonth.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.creditCardExpirationMonth.tabIndex;
            this.focusOnField = document.theform.creditCardExpirationMonth.name;
         }
         document.theform.creditCardExpirationMonth.className="ErrorField";
           }

        if (document.theform.creditCardExpirationYear && (document.theform.creditCardExpirationYear.value == "" || isNaN(document.theform.creditCardExpirationYear.value))) {
         msg = msg + "Purchasing Card Expiration Year is required and must be numeric.\n";
         if (document.theform.creditCardExpirationYear.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.creditCardExpirationYear.tabIndex;
            this.focusOnField = document.theform.creditCardExpirationYear.name;
         }
         document.theform.creditCardExpirationYear.className="ErrorField";
       } else if (document.theform.creditCardExpirationYear) {
         var inputDate = new Date(parseInt(20 + document.theform.creditCardExpirationYear.value),
         document.theform.creditCardExpirationMonth.value - 1, 1);
         var currentDate = new Date();
         if (inputDate.getTime() <= currentDate.getTime()) {
         msg = msg + "Purchasing Card Expiration Date must be later than the current date.\n";
       if (document.theform.creditCardExpirationYear.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.creditCardExpirationYear.tabIndex;
        this.focusOnField = document.theform.creditCardExpirationYear.name;
       }
       document.theform.creditCardExpirationYear.className="ErrorField";
       }
       } // end of date checks
      }
   }
   
   var primaryFlorist = trimToString(document.theform.primaryFloristId.value);
   if ((primaryFlorist != "") && (primaryFlorist.length != 9 || primaryFlorist.charAt(2) != '-')) {
      msg = msg + "Invalid Primary Florist ID format - must be in form nn-nnnnaa (where n = numeral and a = alphabetic).\n";
      if (document.theform.primaryFloristId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.primaryFloristId.tabIndex;
        this.focusOnField = document.theform.primaryFloristId.name;
      }
      document.theform.primaryFloristId.className="ErrorField";
   }
   if (primaryFlorist != "" && trimToString(document.theform.backupFloristIds.value) == "") {
       this.confirmBackupMsg = "Are you sure you want to save this source code without any Backup florists?  Click Ok to continue or Cancel to return to screen.\n";
       if (document.theform.backupFloristIds.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.backupFloristIds.tabIndex;
        this.focusOnField = document.theform.backupFloristIds.name;
       }
       document.theform.backupFloristIds.className="ErrorField";
   }
   if (primaryFlorist == "" && document.theform.source_primary_florist_flag.value == "Y") {
       msg = msg + "You must enter a valid Primary Florist ID for this source code.\n";
       if (document.theform.primaryFloristId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.primaryFloristId.tabIndex;
        this.focusOnField = document.theform.primaryFloristId.name;
       }
       document.theform.primaryFloristId.className="ErrorField";
   }
   if (trimToString(document.theform.backupFloristIds.value) != "") {
       var bkup = trimToString(document.theform.backupFloristIds.value);
       bkup = bkup.replace(/\s+/g, "+");
       var bkupArray = bkup.split("+");
       if (bkupArray.length > 10) {
          msg = msg + "You cannot assign more than 10 Backup florists.  Please enter 1 to 10 Backup Florist IDs or leave the Backup field blank.\n";
          if (document.theform.backupFloristIds.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.backupFloristIds.tabIndex;
            this.focusOnField = document.theform.backupFloristIds.name;
          } 
          document.theform.backupFloristIds.className="ErrorField";
       }
       for (var i=0; i < bkupArray.length; i++) {
         if ((bkupArray[i].length != 9) || (bkupArray[i].charAt(2) != '-')) {
           msg = msg + "Invalid Backup Florist ID format - must be in form nn-nnnnaa (where n = numeral and a = alphabetic).\n";
           if (document.theform.backupFloristIds.tabIndex < this.minTabIndex) {
            this.minTabIndex = document.theform.backupFloristIds.tabIndex;
            this.focusOnField = document.theform.backupFloristIds.name;
           }
           document.theform.backupFloristIds.className="ErrorField";
           break;
         }
       }
       if (primaryFlorist != "") {
          var uf = primaryFlorist.toUpperCase();
          var ub = trimToString(document.theform.backupFloristIds.value).toUpperCase();
          uf = uf.replace(/-/g, "");
          ub = ub.replace(/-/g, "");
          if (ub.indexOf(uf) >= 0) {
             msg = msg + "Primary Florist is a duplicate of a Backup florist.  Please enter a different Primary Florist or remove this florist from Backup Florist ID(s) field.\n";
             if (document.theform.backupFloristIds.tabIndex < this.minTabIndex) {
               this.minTabIndex = document.theform.backupFloristIds.tabIndex;
               this.focusOnField = document.theform.backupFloristIds.name;
             } 
             document.theform.backupFloristIds.className="ErrorField";
          }
       }
   }
   
  document.theform.surchargeAmount.value = trimToString(document.theform.surchargeAmount.value) ;
  document.theform.surchargeDescription.value = trimToString(document.theform.surchargeDescription.value);

  if(document.theform.surchargeAmount.value != "" && (!isFloat(document.theform.surchargeAmount.value)))
  {
    msg = msg + "Fuel Surcharge must be numeric.\n";
    if (document.theform.surchargeAmount.tabIndex < this.minTabIndex) 
    {
      this.minTabIndex = document.theform.surchargeAmount.tabIndex;
      this.focusOnField = document.theform.surchargeAmount.name;
    }
    document.theform.surchargeAmount.className="ErrorField";
  }


  if(document.theform.applySurchargeCode[0].checked == true)
  {
    if(document.theform.surchargeAmount.value == "")
    {
      msg = msg + "Fuel Surcharge is required.\n";
      if (document.theform.surchargeAmount.tabIndex < this.minTabIndex) 
      {
        this.minTabIndex = document.theform.surchargeAmount.tabIndex;
        this.focusOnField = document.theform.surchargeAmount.name;
      }
      document.theform.surchargeAmount.className="ErrorField";
    }

    if(document.theform.surchargeDescription.value == "")
    {
      msg = msg + "Surcharge Description is required.\n";
      if (document.theform.surchargeDescription.tabIndex < this.minTabIndex) 
      {
        this.focusOnField = document.theform.surchargeDescription.name;
        this.minTabIndex = document.theform.surchargeDescription.tabIndex;
      }
      document.theform.surchargeDescription.className="ErrorField";
    }
    
    if (!isNumeric(document.theform.funeralLeadTime.value)) {
       msg = msg + "The Funeral/Cemetery Lead Time value should be a numeric value.\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.funeralLeadTime.name;
       }
       document.theform.funeralLeadTime.className="ErrorField";
   }
   
   var hours = [];
   hours = document.theform.busOpHoursMonFriStart.value.split(":");
   var monFriStart = parseInt(hours[0]);
   hours = document.theform.busOpHoursMonFriEnd.value.split(":");
   var monFriEnd = parseInt(hours[0]);
   
   if(monFriStart > monFriEnd){
   	 msg = msg + "The Start Time should not be more than the End Time for Mon_Fri Business Hours.\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.busOpHoursMonFriStart.name;
       }
       document.theform.busOpHoursMonFriStart.className="ErrorField";
   }
   
   hours = document.theform.busOpHoursSatStart.value.split(":");
   var satStart = parseInt(hours[0]);
   hours = document.theform.busOpHoursSatEnd.value.split(":");
   var satEnd = parseInt(hours[0]);
   if(satStart > satEnd){
   	 msg = msg + "The Start Time should not be more than the End Time for Saturday Business Hours\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.busOpHoursSatStart.name;
       }
       document.theform.busOpHoursSatStart.className="ErrorField";
   }
   
   
   hours = document.theform.busOpHoursSunStart.value.split(":");
   var sunStart = parseInt(hours[0]);
   hours = document.theform.busOpHoursSunEnd.value.split(":");
   var sunEnd = parseInt(hours[0]);
   if(sunStart > sunEnd){
   	 msg = msg + "The Start Time should not be more than the End Time for Sunday Business Hours\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.busOpHoursSunStart.name;
       }
       document.theform.busOpHoursSunStart.className="ErrorField";
   }
    
  }
  var result = false;
  for(var i=0; i< memberLvlArray.length; i++) {
    var name = 'mp_member_level_id_' + memberLvlArray[i];    
   	var redemptionRate = document.getElementsByName('mpRedemptionRateId').item(0).value;
   	var elemdis = document.getElementsByName(name).item(0).disabled;  
   	var elemCheck = document.getElementsByName(name).item(0).checked;   	
   	<!-- alert('elemCheck:' + elemCheck + '-' + 'elemdis:' + elemdis+ '-' + 'redemptionRate:' + redemptionRate); -->    	
   	if(elemCheck == true && (redemptionRate == null || redemptionRate == ""))    		
   	  result = true;
  } 
  if(result == true) {   	 
    msg = msg + "Miles points member levels are only valid for source codes with a miles point redemption rate assigned.\n";     
    this.focusOnField = document.theform.mpRedemptionRateId.name;   
    document.theform.mpRedemptionRateId.className="ErrorField";
  }
  
   var shipCarrier;
  if (document.theform.shippingCarrierFlag.value != '') {
    document.theform.shippingCarrierFlag.value = document.theform.shippingCarrierFlag.value.toUpperCase();
    var shipCarrier = document.theform.shippingCarrierFlag.value ;
  }
  else{
    var shipCarrier = document.theform.shippingCarrierFlag.value ;
  }
  var pattern = /^[A-Z]*$/;
  var spacePattern = /^\s*$/;
  if(spacePattern.test(shipCarrier)){
     //allow the page to submit
  }
  else if(!pattern.test(trimString(shipCarrier)) || trimString(shipCarrier).length < 3 ){
    msg = msg + "User must enter exactly 3 letters in custom shipping carrier field in order to submit the page.\n";
    if (document.theform.shippingCarrierFlag.tabIndex < this.minTabIndex){
     this.minTabIndex = document.theform.shippingCarrierFlag.tabIndex;
     this.focusOnField = document.theform.shippingCarrierFlag.name;
    }
    document.theform.shippingCarrierFlag.className="ErrorField";
  }
  this.errorMsg = msg;
}

function doSave() {
   var formValidator = new FormValidator();
   var submitit = false;
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else if (formValidator.confirmBackupMsg != "") {
     document.theform[formValidator.focusOnField].focus();
     if (confirm(formValidator.confirmBackupMsg)) {
        submitit = true;
     }
   } else {
     submitit = true;
   }
   if (submitit == true) {
       if ( (trimToString(document.theform.primaryFloristId.value) != "")  || (trimToString(document.theform.backupFloristIds.value) != "") )
       {
          validatePreferredFloristAjax();
       } else {
         scroll(0,0);
         showWaitMessage("content", "wait", "Processing");
         document.theform.action_type.value = "update";
         submitActionSSL("sourceCodeMaint.do");
       }
   }
}

function doSaveAjax() {
    scroll(0,0);
    showWaitMessage("content", "wait", "Processing");
    document.theform.action_type.value = "update";
    submitActionSSL("sourceCodeMaint.do");
}

function submitAction(url) {
   // revert back to unsecured site.
   document.theform.action = "http://" + siteName + url;
   document.theform.target = "";
   document.theform.submit();
}

function submitActionSSL(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function disableProgramFields() {
  document.theform.programName.options.length = 0;
  document.theform.programName.disabled=true;
  document.theform.programName.className="disabledField";
  if (document.theform.allowPartnerProgram.value == 'Y') {
    //document.theform.promotionCode.value="";
    document.theform.promotionCode.disabled=true;
    document.theform.promotionCode.className="disabledField";
    //document.theform.bonusPromotionCode.value="";
    document.theform.bonusPromotionCode.disabled=true;
    document.theform.bonusPromotionCode.className="disabledField";
    document.theform.partnerBankFlag[0].disabled=true;
    document.theform.partnerBankFlag[1].disabled=true;
  }
}

function enableProgramFields() {
  document.theform.programName.disabled=false;
  document.theform.programName.className="input";
  if (document.theform.allowPartnerProgram.value == 'Y') {
    document.theform.promotionCode.disabled=false;
    document.theform.promotionCode.className="input";
    document.theform.bonusPromotionCode.disabled=false;
    document.theform.bonusPromotionCode.className="input";
    document.theform.partnerBankFlag[0].disabled=false;
    document.theform.partnerBankFlag[1].disabled=false;
  }
}

function disableCreditCardFields() {
  if (document.theform.creditCardType) {
  document.theform.creditCardType.selectedIndex = 0;
  document.theform.creditCardType.disabled=true;
  document.theform.creditCardType.className="disabledField";
  }
  if (document.theform.creditCardNumber) {
  document.theform.creditCardNumber.value = "";
  document.theform.creditCardNumber.disabled=true;
  document.theform.creditCardNumber.className="disabledField";
  }
  if (document.theform.creditCardExpirationMonth) {
  document.theform.creditCardExpirationMonth.value = "";
  document.theform.creditCardExpirationMonth.disabled=true;
  document.theform.creditCardExpirationMonth.className="disabledField";
  }
  if (document.theform.creditCardExpirationYear) {
  document.theform.creditCardExpirationYear.value = "";
  document.theform.creditCardExpirationYear.disabled=true;
  document.theform.creditCardExpirationYear.className="disabledField";
  }
}

function enableCreditCardFields() {
   if (document.theform.creditCardType) {
   document.theform.creditCardType.disabled=false;
   document.theform.creditCardType.className="input";
   }
   if (document.theform.creditCardNumber) {
  document.theform.creditCardNumber.disabled=false;
  document.theform.creditCardNumber.className="input";
  }
  if (document.theform.creditCardExpirationMonth) {
  document.theform.creditCardExpirationMonth.disabled=false;
  document.theform.creditCardExpirationMonth.className="input";
  }
  if (document.theform.creditCardExpirationYear) {
  document.theform.creditCardExpirationYear.disabled=false;
  document.theform.creditCardExpirationYear.className="input";
  }
}

function disableInvoicePassword() {
  document.theform.invoicePassword.value = "";
  document.theform.invoicePassword.disabled=true;
  document.theform.invoicePassword.className="disabledField";
}

function enableInvoicePassword() {
  document.theform.invoicePassword.disabled=false;
  document.theform.invoicePassword.className="input";
}

function modifyCreditCardFields() {
  if (document.theform.paymentMethodId && document.theform.paymentMethodId.value != 'PC') {
    // This indicates that the Source is not based on a Purchase Card program.
    disableCreditCardFields();
    if (document.theform.paymentMethodId.value == 'IN') {
      enableInvoicePassword();
    } else {
      disableInvoicePassword();
    }
  } else {
    enableCreditCardFields();
    disableInvoicePassword();
  }
  setChangedFlag('');
}

function programEdit(id, startDate, endDate, lastPostDate, num) {
  if (changedFlag == 'Y') {
    alert("Changes must be saved before editing a promotion");
    return;
  }
  if (num > 1) {
    document.theform.previousStartDate.value = startArray[num-2];
  }
  document.theform.action_type.value = "edit_program";
  document.theform.promoId.value = id;
  openPopup();
}

function programAdd() {
  if (changedFlag == 'Y') {
    alert("Changes must be saved before adding a promotion");
    return;
  }
  document.theform.action_type.value = "add_program";
  document.theform.promoId.value = '';
  openPopup();
}

function openPopup()
{
  var modal_dim = "dialogWidth:650px; dialogHeight:450px; center:yes; status:0; help:no; scroll:no";

  var urlEditPromo = "sourceCodeMaint.do?action_type=" + document.theform.action_type.value +
                     "&requestSourceCode=" + document.theform.requestSourceCode.value +
                     "&promoId=" + document.theform.promoId.value +
                     "&partner_name=" + document.theform.partner_name.value +
                     "&previousStartDate=" + document.theform.previousStartDate.value +
                     "&securitytoken=" + document.theform.securitytoken.value+
                     "&context=" + document.theform.context.value;

  var ret = window.showModalDialog(urlEditPromo, document.theform.action_type.value, modal_dim);
  //alert("ret=" + ret);

  if (ret != null)
  {
    if(ret == "RELOAD")
    {
      //alert("reload");
      document.theform.action_type.value = "edit";
      submitActionSSL("sourceCodeMaint.do");
    }
    else
    {
      // just return
      return;
    }
  }
  else
  {
    // some error may have occurred
    return;
  }
}

function setChangedFlag(fieldName) {
  changedFlag = 'Y';
  //alert(fieldName);
  if (fieldName != '') {
    fieldName.value = fieldName.value.toUpperCase();
  }
}

function showPrepopulateDeliveryAndBillingInfoPopup() {
  var sourceCode = document.theform.requestSourceCode.value;
  
  if(isNewRecord || changedFlag == 'Y') {
    alert("You must save the source code before adding/updating any\nPrepopulate Delivery and Billing Information");
    return;
  } 
  
  var modal_dim = "dialogWidth:800px; dialogHeight:450px; center:yes; status:0; help:no; scroll:no; resizable: yes";

  var urlEditPrepopulateInfo = "sourceCodeDefaultOrderInfoMaint.do?" +
                     "&requestSourceCode=" + document.theform.requestSourceCode.value+
                     "&securitytoken=" + document.theform.securitytoken.value+
                     "&context=" + document.theform.context.value;

  var ret = window.showModalDialog(urlEditPrepopulateInfo, "sourceCodeDefaultOrderInfoMaint", modal_dim);

  if (ret != null)
  {
    if(ret == "RELOAD")
    {
      //alert("reload");
      document.theform.action_type.value = "edit";
      submitActionSSL("sourceCodeMaint.do");
    }
    else
    {
      // just return
      return;
    }
  }
  else
  {
    // some error may have occurred
    return;
  }

}

]]>
</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Source Code Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<input type="hidden" name="requestSourceCode" value="{key('pagedata','requestSourceCode')/@value}"/>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="result" value="{key('pagedata','result')/@value}"/>
<input type="hidden" name="partner_name" value="{$partner_name}"/>
<input type="hidden" name="original_partner_name" value="{$partner_name}"/>
<input type="hidden" name="billingInfoFlag" value="{$billing_info_flag}"/>
<input type="hidden" name="promoId" value=""/>
<input type="hidden" name="previousStartDate" value=""/>
<input type="hidden" name="allowPartnerProgram" value="{key('pagedata','SourcePromotionCodeData')/@value}"/>
<input type="hidden" name="invokingPage" value="SourceCodeMaint"/>
<input type="hidden" name="cc_number_updated_flag" value="N"/>
<input type="hidden" name="binCheckPartnerValues" value="{count(root/binCheckPartnerList/partner)}"/>
<input type="hidden" name="source_primary_florist_flag" value="{$source_primary_florist_flag}"/> 
<xsl:call-template name="securityanddata"></xsl:call-template>
<xsl:call-template name="searchCriteria">
   <xsl:with-param name="scActionType" select="key('pagedata','scActionType')/@value"/>
   <xsl:with-param name="scRecordSortBy" select="key('pagedata','scRecordSortBy')/@value"/>
   <xsl:with-param name="scPageMaxRecordCount" select="key('pagedata','scPageMaxRecordCount')/@value"/>
   <xsl:with-param name="scRecordNumber" select="key('pagedata','scRecordNumber')/@value"/>
   <xsl:with-param name="scSourceCode" select="key('pagedata','scSourceCode')/@value"/>
   <xsl:with-param name="scUpdatedBy" select="key('pagedata','scUpdatedBy')/@value"/>
   <xsl:with-param name="scRequestedBy" select="key('pagedata','scRequestedBy')/@value"/>
   <xsl:with-param name="scCompanyId" select="key('pagedata','scCompanyId')/@value"/>
   <xsl:with-param name="scSourceType" select="key('pagedata','scSourceType')/@value"/>
   <xsl:with-param name="scPriceHeaderId" select="key('pagedata','scPriceHeaderId')/@value"/>
   <xsl:with-param name="scSnhId" select="key('pagedata','scSnhId')/@value"/>
   <xsl:with-param name="scProgramName" select="key('pagedata','scProgramName')/@value"/>
   <xsl:with-param name="scProgramWebsiteUrl" select="key('pagedata','scProgramWebsiteUrl')/@value"/>
   <xsl:with-param name="scPromotionCode" select="key('pagedata','scPromotionCode')/@value"/>
   <xsl:with-param name="scDefaultSourceCodeFlag" select="key('pagedata','scDefaultSourceCodeFlag')/@value"/>
   <xsl:with-param name="scJcpenneyFlag" select="key('pagedata','scJcpenneyFlag')/@value"/>
   <xsl:with-param name="scOver21Flag" select="key('pagedata','scOver21Flag')/@value"/>
   <xsl:with-param name="scEndDate" select="key('pagedata','scEndDate')/@value"/>
   <xsl:with-param name="scUpdatedOnStart" select="key('pagedata','scUpdatedOnStart')/@value"/>
   <xsl:with-param name="scUpdatedOnEnd" select="key('pagedata','scUpdatedOnEnd')/@value"/>
   <xsl:with-param name="scIotwFlag" select="key('pagedata','scIotwFlag')/@value"/>
</xsl:call-template>
<div id="content" style="display:block">
<table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="key('pagedata','result')/@value &gt; '0'">
				   <td class="OperationSuccess">
						SOURCE&nbsp;<xsl:value-of select="key('pagedata','requestSourceCode')/@value"/>&nbsp;SUCCESSFULLY SAVED
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-1'">
				   <td class="errorMessage">
						SOURCE&nbsp;<xsl:value-of select="key('pagedata','requestSourceCode')/@value"/>&nbsp;FAILED. RECORD ALREADY EXISTS.
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-10'">
				   <td class="errorMessage">
						APE Base Source Codes are not valid: <xsl:value-of select="key('pagedata','listInvalid')/@value"/>
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-11'">
				   <td class="errorMessage">
						APE Base Source Codes cannot have APE enabled: <xsl:value-of select="key('pagedata','listApeEnabled')/@value"/>
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-13'">
				   <td class="errorMessage">
						APE Base Source Codes cannot have miles/points promotions: <xsl:value-of select="key('pagedata','listMilesPoints')/@value"/>
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-12'">
				   <td class="errorMessage">
						APE Base Source Codes cannot be IOTW source codes: <xsl:value-of select="key('pagedata','listIOTW')/@value"/>
				   </td>
         </xsl:when>
         <xsl:when test="key('pagedata','result')/@value = '-14'">
				   <td class="errorMessage">
						APE cannot be enabled on this source code because it is already a base source code on: <xsl:value-of select="key('pagedata','listMasterSourceCodes')/@value"/>
				   </td>
         </xsl:when>
         </xsl:choose>
			   </tr>
</table>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr>
      <td width="5%" align="left">
      <xsl:if test="key('pagedata','SourceDefinitionData')/@value = 'Y' or key('pagedata','SourcePaymentMethodData')/@value = 'Y' or key('pagedata','SourcePriceHeaderData')/@value = 'Y' or key('pagedata','SourcePromotionCodeData')/@value = 'Y' or key('pagedata','SourcePCardData')/@value = 'Y' or key('pagedata','SourceSnhData')/@value = 'Y'">
        <button class="blueButton" onClick="javascript:doSave()" tabindex="1" accesskey="s">(S)ave</button>&nbsp;&nbsp;
        <button class="blueButton" onClick="javascript:doReset()" tabindex="2" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
      </xsl:if>
      </td>
      <td class="TopHeading" align="left">Source Code: <xsl:value-of select="key('pagedata','requestSourceCode')/@value"/></td>
      <td align="right">
        <button class="blueButton" onClick="javascript:doExit()" tabindex="3">(E)xit</button>
      </td>
   </tr>
</table>

<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
   <tr>
      <td width="25%" class="label" align="right" valign="top">Description<font color="red">*&space;</font>:</td>
      <td width="25%" align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <input type="text" size="40" maxlength="80" name="description" tabindex="10" value="{$description}" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$description"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      This text describes the offer for the source code.  This text will be visible on the order and in order entry.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Start Date<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <input type="text" size="10" maxlength="10" name="startDate" tabindex="11" value="{$start_date}" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
         &nbsp;&nbsp;<input type="image" id="calendar1" tabindex="11" BORDER="0" SRC="images/calendar.gif" width="16" height="16"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$start_date"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the date that the promotion will begin.  For new records, always enter in today's date or later.  The format should be MM/DD/YYYY
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Expiration Date:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <input type="text" size="10" maxlength="10" name="endDate" tabindex="12" value="{$end_date}" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
         &nbsp;&nbsp;<input type="image" id="calendar2" tabindex="12" BORDER="0" SRC="images/calendar.gif" width="16" height="16"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$end_date"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the date that the promotion will expire or end.  The format should be MM/DD/YYYY
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Company/Brand Code<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="companyId" tabindex="13" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/companyList/company">
            <option value="{@companyid}">
            <xsl:if test="@companyid = $company_id">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
            <xsl:value-of select="@companyname"/>
            </option>
            </xsl:for-each>
         </select>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$company_id"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the company that the source code is tied to.  This controls the company name that appears in emails, what company appears on the customer's credit card statement, etc
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Source Type<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="sourceType" tabindex="14" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/sourceTypeList/sourceType">
               <option value="{@source_type}">
               <xsl:if test="@source_type = $source_type">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@source_type"/></option>
            </xsl:for-each>
         </select>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$source_type"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      The source type is used to group source codes together into a higher-level list.  Examples are United, American Airlines, etc.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Pricing Code<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePriceHeaderData')/@value = 'Y'">
         <select name="priceHeaderId" tabindex="15" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/pricingCodeList/pricingCode">
               <option value="{@price_header_id}">
               <xsl:if test="@price_header_id = $price_header_id">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
                  <xsl:value-of select="@price_header_id"/>
                  &nbsp;-&nbsp;
                  <xsl:value-of select="@description"/>
               </option>
            </xsl:for-each>
         </select>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$price_header_id"/>
                  &nbsp;-&nbsp;
      <xsl:value-of select="root/pricingCodeList/pricingCode[@price_header_id=$price_header_id]/@description"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field drives the discount / offer for the source code.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Service Charge Code<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
         <select name="snhId" tabindex="16" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/serviceFeeCodeList/serviceFeeCode">
                     <option value="{@snh_id}">
                         <xsl:if test="@snh_id = $snh_id">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         <xsl:value-of select="@snh_id"/>&nbsp;-&nbsp;
                            <xsl:value-of select="@description"/>&nbsp;-&nbsp;DF&nbsp;=&nbsp;
                            <xsl:value-of select="@first_order_domestic"/>&nbsp;,&nbsp;SDU&nbsp;=&nbsp;
                            <xsl:value-of select="@same_day_upcharge"/>&nbsp;,&nbsp;SDUFSM&nbsp;=&nbsp;
                            <xsl:value-of select="@same_day_upcharge_fs"/>&nbsp;,&nbsp;I&nbsp;=&nbsp;
                            <xsl:value-of select="@first_order_international"/>&nbsp;,&nbsp;V&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_charge"/>&nbsp;,&nbsp;VS&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_sat_upcharge"/>&nbsp;,&nbsp;SUN&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_sun_upcharge"/>&nbsp;,&nbsp;M&nbsp;=&nbsp;
                            <xsl:value-of select="@vendor_mon_upcharge"/>
                     </option>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$snh_id"/>
                  &nbsp;-&nbsp;
      <xsl:value-of select="root/serviceFeeCodeList/serviceFeeCode[@snh_id=$snh_id]/@description"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field drives the service charge for the source code.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Miles Points Redemption Rate:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','RdmptnRateData')/@value = 'Y'">
         <select name="mpRedemptionRateId" tabindex="17" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/mpRedemptionRateList/mpRedemptionRate">
                     <option value="{@mp_redemption_rate_id}">
                     <xsl:if test="@mp_redemption_rate_id = $mp_redemption_rate_id">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
                        <xsl:value-of select="@mp_redemption_rate_id"/>
                        &space;-&space;
                        <xsl:value-of select="@description"/>
                     </option>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$mp_redemption_rate_id"/>
                  &nbsp;-&nbsp;
      <xsl:value-of select="root/mpRedemptionRateList/mpRedemptionRate[@mp_redemption_rate_id=$mp_redemption_rate_id]/@description"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      Defines the redemption rate that applies to purchases paid with miles/points under this source code.
      </td>
   </tr>   
   <tr>
      <td align="right" class="label" valign="top">Miles Points Member Level:</td>
        <td align="left" valign="top">
           <div style="height:70px; overflow:auto; border: 1px solid gray;">
              <table>              
                  <xsl:for-each select="root/mp_member_levels/mp_member_level">
                  <xsl:sort select="@mp_member_level_id" data-type="number" />
                  <tr> 
                  	  <td><xsl:value-of select="@mp_member_level_id"/></td>
                  	  <td>&space;-&space;</td>
                      <td><xsl:value-of select="@description"/></td>
                      <td>&space;</td>
                      <td>                       
	                       <xsl:choose>
	                       	<xsl:when test="key('pagedata','requestSourceCode')/@value = @source_code">  
	                          <input type="radio" id="mp_member_level_id_yes" name="mp_member_level_id_{@mp_member_level_id}" value="Y" tabindex="58" disabled="true" checked="checked"/>Yes       
	                          <input type="radio" id="mp_member_level_id_no" name="mp_member_level_id_{@mp_member_level_id}" value="N" tabindex="58" disabled="true"/>No
	                        </xsl:when>
	                        <xsl:otherwise>
	                          <input type="radio" id="mp_member_level_id_yes" name="mp_member_level_id_{@mp_member_level_id}" value="Y" tabindex="58"/>Yes       
	                          <input type="radio" id="mp_member_level_id_no" name="mp_member_level_id_{@mp_member_level_id}" value="N" tabindex="58" checked="checked"/>No
	                        </xsl:otherwise>     
	                       </xsl:choose>                     	                       	                       	
                     </td>                                         
                   </tr>
                   </xsl:for-each>                  
              </table>
          </div> 
        </td>        
      <td></td>
      <td class="Instruction" align="left">
         Defines the member levels that are eligible to make purchases on this source code (Assigning a member level to this source code will unassign it from other source code associated)
      </td>
   </tr> 
   <tr>
      <td align="right" class="label" valign="top">Order Source<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="orderSource" tabindex="18" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/orderSourceList/orderSource">
               <option value="{@order_source}">
               <xsl:if test="@order_source = $order_source">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@description"/></option>
            </xsl:for-each>
         </select>
      </xsl:when>
      <xsl:otherwise>
      <xsl:for-each select="root/orderSourceList/orderSource">
               <xsl:if test="@order_source = $order_source">
               <xsl:value-of select="@description"/>
               </xsl:if>
      </xsl:for-each>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field drives whether the source code is a phone source code or Internet source code. If the source code is an Internet source code it can still be used on the phones.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Requested By<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="requestedBy" tabindex="19" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/roleUserList/roleUser">
               <option value="{@user_id}">
               <xsl:if test="@user_id = $requested_by">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@last_name"/>,
               <xsl:value-of select="@first_name"/>
               </option>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:for-each select="root/roleUserList/roleUser">
               <xsl:if test="@user_id = $requested_by">
               <xsl:value-of select="@last_name"/>,
               <xsl:value-of select="@first_name"/>
               </xsl:if>
      </xsl:for-each>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      The person who requested the source code or modification to the source code will be entered here.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Payment Method<font color="red">*&space;</font>:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePaymentMethodData')/@value = 'Y'">
         <select name="paymentMethodId" tabindex="20" onChange="javascript:modifyCreditCardFields()">
            <option value="">All</option>
            <xsl:for-each select="root/payMethodList/payMethod">
               <option value="{@payment_method_id}">
               <xsl:if test="@payment_method_id = $payment_method_id">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@description"/></option>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$payment_method_id = ''">
      All
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$payment_method_id"/>
                  &nbsp;-&nbsp;
      <xsl:value-of select="root/payMethodList/payMethod[@payment_method_id=$payment_method_id]/@description"/>
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the valid pay method for the source code.  Please note that only one pay method can be selected per source code.
      </td>
   </tr>

   <tr>
      <td align="right" class="label" valign="top">Invoice Password:</td>
      <td align="left" valign="top">
          <xsl:choose>
              <xsl:when test="key('pagedata','SourcePaymentMethodData')/@value = 'Y'">
                  <input type="text" size="40" maxlength="60" name="invoicePassword" tabindex="41" value="{$invoice_password}" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="$invoice_password"/>
                  <input type="hidden" name="invoicePassword" value=""/>
              </xsl:otherwise>
          </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      All passwords are case sensitive.
      </td>
   </tr>

   <tr>
      <td align="right" class="label" valign="top">P-Card Type:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
             <select name="creditCardType" tabindex="19" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/payMethodList/payMethod">
               <xsl:if test="@payment_type = 'C'">
               <option value="{@payment_method_id}">
               <xsl:if test="@payment_method_id = $credit_card_type">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@description"/></option>
               </xsl:if>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$credit_card_type"/>
                  &nbsp;-&nbsp;
      <xsl:value-of select="root/payMethodList/payMethod[@payment_method_id=$credit_card_type]/@description"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If this source code is associated with a Purchasing Card, enter the card type here.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">P-Card Number:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
      <input type="text" size="20" maxlength="20" name="creditCardNumber" value="{$credit_card_number}" tabindex="19" onChange="setChangedFlag('');setCCNumberUpdated()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$credit_card_number"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If this source code is associated with a Purchasing Card, enter the card number here.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">P-Card Expiion Date:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
      <input type="text" size="2" maxlength="2" name="creditCardExpirationMonth" value="{substring-before($credit_card_expiration_date, '.')}" tabindex="19" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      &nbsp;-&nbsp;
      <input type="text" size="2" maxlength="2" name="creditCardExpirationYear" value="{substring-after($credit_card_expiration_date, '.')}" tabindex="19" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="substring-before($credit_card_expiration_date, '.')"/>&nbsp;-&nbsp;<xsl:value-of select="substring-after($credit_card_expiration_date, '.')"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If this source code is associated with a Purchasing Card, enter the expiration date here. Format = MM - YY
      </td>
   </tr>
   <tr>
      <tr>
      <td align="right" class="label" valign="top">Free Gift:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="addOnFreeId" tabindex="30" onChange="onChange=setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/complimentaryAddonList/addon">
               <option value="{@addonid}">
               <xsl:if test="@addonid = $add_on_free_id">
                  <xsl:attribute name="selected">
                  true
                 </xsl:attribute>
               </xsl:if>
               <xsl:value-of select="@description"/></option>
            </xsl:for-each>
         </select>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
        <xsl:when test="$add_on_free_id = ''">
          &nbsp;-&nbsp;
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="root/complimentaryAddonList/addon[@addonId=$add_on_free_id]/@description"/>
        </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
       If this source code includes products eligible for a Free Gift, the Free Gift selected for this source code will be offered to the consumer with their order(s).
      </td>
   </tr>

   
   
   
   
      <td colspan="4" class="colHeader">&nbsp;</td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Related Phone/Internet Code:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="text" size="10" maxlength="5" name="relatedSourceCode" tabindex="40" value="{$related_source_code}" onChange="javascript:setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$related_source_code"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will store the corresponding phone or Internet code that is associated with this source code.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Website URL (http://):</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="text" size="40" maxlength="40" name="programWebsiteUrl" tabindex="41" value="{$program_website_url}" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$program_website_url"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If the source code is an Internet source code this is the co-branded URL for the site.  The format should be www.ftd.com/xxxx
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Gift Certificate:</td>
      <td align="left" valign="top">
      <xsl:choose>
          <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
              <xsl:choose>
                  <xsl:when test="$billing_info_flag = 'Y'">
                      N/A
                  </xsl:when>
                  <xsl:otherwise>
                      <input type="radio" name="giftCertificateFlag" value="Y" onChange="setChangedFlag('')" tabindex="50">
                          <xsl:if test="$gift_certificate_flag = 'Y'">
                              <xsl:attribute name="checked">
                              true
                              </xsl:attribute>
                          </xsl:if>
                      </input>Yes
                      <input type="radio" name="giftCertificateFlag" value="N" onChange="setChangedFlag('')" tabindex="50">
                          <xsl:if test="$gift_certificate_flag = 'N' or $gift_certificate_flag = '' or not($gift_certificate_flag)">
                              <xsl:attribute name="checked">
                              true
                              </xsl:attribute>
                          </xsl:if>
                      </input>No
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
              <xsl:choose>
                  <xsl:when test="$billing_info_flag = 'Y'">
                      N/A
                  </xsl:when>
                  <xsl:when test="$gift_certificate_flag = 'Y'">
                      Y
                  </xsl:when>
                  <xsl:otherwise>
                      N
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If 'Yes', Order Entry will prompt for Gift Certificate Number
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Default Source Flag:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="defaultSourceCodeFlag" value="Y" onChange="setChangedFlag('')" tabindex="50">
      <xsl:if test="$default_source_code_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="defaultSourceCodeFlag" value="N" onChange="setChangedFlag('')" tabindex="50">
      <xsl:if test="$default_source_code_flag = 'N' or not($default_source_code_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$default_source_code_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If the source code is a default source code enter a Y in the field.  If it isn't enter an N.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Highlight Description:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="highlightDescriptionFlag" value="Y" onChange="setChangedFlag('')" tabindex="51">
      <xsl:if test="$highlight_description_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="highlightDescriptionFlag" value="N" onChange="setChangedFlag('')" tabindex="51">
      <xsl:if test="$highlight_description_flag = 'N' or not($highlight_description_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$highlight_description_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If the source code is to be highlighted on the Recipient / Order Screen then enter a Y in the field.  If it isn't then enter an N.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Send To Scrub Flag:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
        <input type="radio" name="sendToScrub" value="Y" onChange="setChangedFlag('')" tabindex="53">
        <xsl:if test="$send_to_scrub = 'Y'">
                          <xsl:attribute name="checked">
                          true
                          </xsl:attribute>
                    </xsl:if>
        </input>Yes
        <input type="radio" name="sendToScrub" value="N" onChange="setChangedFlag('')" tabindex="53">
        <xsl:if test="$send_to_scrub = 'N' or not($send_to_scrub)">
                          <xsl:attribute name="checked">
                          true
                          </xsl:attribute>
                    </xsl:if>
        </input>No
        </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$send_to_scrub = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If a Y is entered in this field then all Internet and B2B orders for this source code will be sent to Scrub.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">LPI Check Flag:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="enableLpProcessing" value="Y" onChange="setChangedFlag('')" tabindex="55">
                  <xsl:if test="$enable_lp_processing = 'Y' or not($enable_lp_processing)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="enableLpProcessing" value="N" onChange="setChangedFlag('')" tabindex="55">
      <xsl:if test="$enable_lp_processing = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$enable_lp_processing = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
This field will be used to indicate if the system is to perform the LPI check for orders received on this source code. If selected, orders received on this source code will be checked to determine if the LPI threshold has been met.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Discount Approved:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="discountAllowedFlag" value="Y" onChange="setChangedFlag('')" tabindex="56">
      <xsl:if test="$discount_allowed_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="discountAllowedFlag" value="N" onChange="setChangedFlag('')" tabindex="56">
      <xsl:if test="$discount_allowed_flag = 'N' or not($discount_allowed_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$discount_allowed_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will be used to indicate if discounts can be applied to non-discounted products. If selected, this source will be flagged to accept discounts on non-discounted products.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Delivery Confirmation:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="requiresDeliveryConfirmation" value="Y" onChange="setChangedFlag('')" tabindex="57">
      <xsl:if test="$requires_delivery_confirmation = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="requiresDeliveryConfirmation" value="N" onChange="setChangedFlag('')" tabindex="57">
      <xsl:if test="$requires_delivery_confirmation = 'N' or not($requires_delivery_confirmation)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$requires_delivery_confirmation = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will be used to indicate if the orders require an automatic delivery confirmation. If selected, this source will be flagged so that the system performs automatic messaging to the florist requesting proof of order delivery.
      </td>
   </tr>
   
   <tr>
      <td align="right" class="label" valign="top">Allow Free Shipping Membership:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="allowFreeShippingFlag" value="Y" onChange="setChangedFlag('')" tabindex="57">
      <xsl:if test="$allow_free_shipping_flag = 'Y' or not($allow_free_shipping_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="allowFreeShippingFlag" value="N" onChange="setChangedFlag('')" tabindex="57">
      <xsl:if test="$allow_free_shipping_flag = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$allow_free_shipping_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If "Yes" is selected, Free Shipping benefits will be applied to orders for this source code.
      </td>
   </tr>
   
   <tr>
      <td align="right" class="label" valign="top">Product Type Availability:</td>
        <td align="left" valign="top">
           <div style="height:70px; overflow:auto; border: 1px solid gray;">
              <table>              
                  <xsl:for-each select="root/product_attributes/product_attribute">
                  <tr> 
                      <td><xsl:value-of select="@product_attr_restr_desc"/></td>
                      <td>
                           <xsl:choose>
                              <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">         
                                  <input type="radio" name="product_attr_restr_id_{@product_attr_restr_id}" value="Y" tabindex="58">
                                        <xsl:if test="not(key('productAttributeExclusionList',@product_attr_restr_id)/@num !='')">
                                            <xsl:attribute name="checked">true</xsl:attribute>
                                        </xsl:if>
                                  </input>Yes
                                  <input type="radio" name="product_attr_restr_id_{@product_attr_restr_id}" value="N" tabindex="58">
                                    <xsl:if test="key('productAttributeExclusionList',@product_attr_restr_id)/@num != ''">
                                        <xsl:attribute name="checked">true</xsl:attribute>
                                    </xsl:if>
                                  </input>No
                              </xsl:when>
                              <xsl:otherwise>
                                  <xsl:choose>
                                      <xsl:when  test="not(key('productAttributeExclusionList',@product_attr_restr_id)/@num != '')">
                                      Yes
                                      </xsl:when>
                                      <xsl:otherwise>
                                      No
                                      </xsl:otherwise>
                                  </xsl:choose>
                              </xsl:otherwise>     
                           </xsl:choose>
                      </td>                   
                   </tr>
                   </xsl:for-each>                  
              </table>
          </div> 
        </td>        
      <td></td>
      <td class="Instruction" align="left">
         If "No" is selected, products with that attribute are not allowed to be purchased on this source code.
      </td>
   </tr>   
   
   <tr>
      <td align="right" class="label" valign="top">Emergency Text:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="emergencyTextFlag" value="Y" onChange="setChangedFlag('')" tabindex="58">
                  <xsl:if test="$emergency_text_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="emergencyTextFlag" value="N" onChange="setChangedFlag('')" tabindex="58">
      <xsl:if test="$emergency_text_flag = 'N' or not($emergency_text_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$emergency_text_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will be used to indicate if this source code is to be included if FTD needs to send out Emergency text to the florist via Mercury. If selected, this source code will be flagged as one where the florist will receive FTD Emergency text in the special instructions field of the mercury message.
</td>
   </tr>
   
    <!-- Custom Shipping Carrier -->
   
   <tr>
	<td align="right" class="label" valign="top">Custom Shipping Carrier:</td>
      <td align="left" valign="top">
       <xsl:choose>
        <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
          <input type="text" size="10" maxlength="3" name="shippingCarrierFlag" tabindex="41" value="{$custom_shipping_carrier}" style="text-transform: uppercase" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
        </xsl:when>
       <xsl:otherwise>
          <xsl:value-of select="$custom_shipping_carrier"/>
       </xsl:otherwise>
       </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field controls the affiliate code sent to west systems in availability calls. It is also used in order submission. Code must be set up on west systems before making source code live. Affiliate Code must be entered correctly as stated by development teams. The code must be 3 letters. 
</td>
   </tr>
   <!-- Custom Shipping Carrier -->
   
   
   <tr>
	<td align="right" class="label" valign="top">Morning Delivery:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
      <input type="radio" name="morningDeliveryFlag" value="Y" onChange="setChangedFlag('')" tabindex="58">
                  <xsl:if test="$morning_delivery_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="morningDeliveryFlag" value="N" onChange="setChangedFlag('')" tabindex="58">
      <xsl:if test="$morning_delivery_flag = 'N' or not($morning_delivery_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      <input type="radio" name="morningDeliveryFlag" value="G" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$morning_delivery_flag = 'G'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Global
      </xsl:when>
      <xsl:otherwise>
       <xsl:if test="$morning_delivery_flag = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$morning_delivery_flag = 'N'">
             No
          </xsl:if>
          <xsl:if test="$morning_delivery_flag = 'G'">
             Global
          </xsl:if> 
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will determine if the Morning Delivery should be charged for this source code. �Global� means the source code will follow the setup on the System Configuration page. 
</td>
   </tr>
   
   <!-- CalculateTax -->
   
   <tr>
	<td align="right" class="label" valign="top">Calculate Tax:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
	      <input type="radio" name="calculateTax" value="Y"  tabindex="58">
	        <xsl:if test="$calculate_tax_flag = 'Y' or not($calculate_tax_flag)">
	           <xsl:attribute name="checked">true</xsl:attribute>
	        </xsl:if>
	      </input>Yes
	      <input type="radio" name="calculateTax" value="N"  tabindex="58">
	      <xsl:if test="$calculate_tax_flag = 'N' ">
	        <xsl:attribute name="checked">true</xsl:attribute>
	      </xsl:if>
	      </input>No
      </xsl:when>
      <xsl:otherwise>
	       <xsl:if test="$calculate_tax_flag = 'Y'">Yes</xsl:if>
	       <xsl:if test="$calculate_tax_flag = 'N'">No</xsl:if>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will determine if tax should be charged. Yes = charge tax No = do not charge tax. 
</td>
   </tr>
   <!-- CalculateTax -->
   
   <tr>
      <td align="right" class="label" valign="top">Morning Delivery Charge Code:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
         <select name="deliveryFeeId" tabindex="58" onChange="setChangedFlag('')">
            <xsl:for-each select="root/morningFeeCodeList/morningFeeCode">
                     <option value="{@delivery_fee_id}">
                         <xsl:if test="@delivery_fee_id = $delivery_fee_id">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         <xsl:value-of select="@delivery_fee_id"/>&nbsp;-&nbsp;
                            <xsl:value-of select="@description"/>&nbsp;=&nbsp;
                            <xsl:value-of select="@delivery_fee"/>
                     </option>
            </xsl:for-each>
         </select>
         </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$delivery_fee_id"/>&nbsp;-&nbsp;
      <xsl:value-of select="root/morningFeeCodeList/morningFeeCode[@delivery_fee_id=$delivery_fee_id]/@description"/>&nbsp;=&nbsp;
      <xsl:value-of select="root/morningFeeCodeList/morningFeeCode[@delivery_fee_id=$delivery_fee_id]/@delivery_fee"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field drives the Morning Delivery charge for the source code.
      </td>
   </tr>
   <tr>
	<td align="right" class="label" valign="top">Morning Delivery Charged to FS Members?:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
      <input type="radio" name="morningDeliveryChargedToFSMembers" value="Y" onChange="setChangedFlag('')" tabindex="58">
                  <xsl:if test="$morning_delivery_charged_to_FS_members = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="morningDeliveryChargedToFSMembers" value="N" onChange="setChangedFlag('')" tabindex="58">
      <xsl:if test="$morning_delivery_charged_to_FS_members = 'N' or not($morning_delivery_charged_to_FS_members)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      <input type="radio" name="morningDeliveryChargedToFSMembers" value="G" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$morning_delivery_charged_to_FS_members = 'G'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Global
      </xsl:when>
      <xsl:otherwise>
       <xsl:if test="$morning_delivery_charged_to_FS_members = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$morning_delivery_charged_to_FS_members = 'N'">
             No
          </xsl:if>
          <xsl:if test="$morning_delivery_charged_to_FS_members = 'G'">
             Global
          </xsl:if> 
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This option determines to either charge or not charge Free Shipping Members for the Morning Delivery fee. 'Global' means the source code will follow the option on the System Configuration Page.
</td>
   </tr>
   <tr>
	  <td align="right" class="label" valign="top">Display Same Day Upcharge:</td>
	  <td align="left" valign="top">
	  <xsl:choose>
	  <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
	   <input type="radio" name="displaySameDayUpcharge" value="Y" onChange="setChangedFlag('')" tabindex="58">
	   			  <xsl:if test="$display_same_day_upcharge = 'Y'">
	                        <xsl:attribute name="checked">
                        true
                        	</xsl:attribute>
                  </xsl:if> 
       </input>Yes 
       <input type="radio" name="displaySameDayUpcharge" value="N" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_same_day_upcharge = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>No
      <input type="radio" name="displaySameDayUpcharge" value="D" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_same_day_upcharge = 'D' or not($display_same_day_upcharge)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Global
       </xsl:when>
      <xsl:otherwise>
         <xsl:if test="$display_same_day_upcharge = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$display_same_day_upcharge = 'N'">
             No
          </xsl:if>
          <xsl:if test="$display_same_day_upcharge = 'D'">
             Global
          </xsl:if>      
      </xsl:otherwise>
      </xsl:choose> 
      </td>
      <td></td>
      <td class="Instruction" align="left">
          This field will indicate when the Same Day Upcharge displays on a separate line on the order confirmation email.  �Global� means the source code will follow the configuration set on the System Configuration page.
       </td>        			  		
   </tr>
    <tr>
	  <td align="right" class="label" valign="top">Same Day Upcharge:</td>
	  <td align="left" valign="top">
	  <xsl:choose>
	  <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
	   <input type="radio" name="sameDayUpcharge" value="Y" onChange="setChangedFlag('')" tabindex="58">
	   			  <xsl:if test="$same_day_upcharge  = 'Y'">
	                        <xsl:attribute name="checked">
                        true
                        	</xsl:attribute>
                  </xsl:if> 
       </input>Yes 
       <input type="radio" name="sameDayUpcharge" value="N" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$same_day_upcharge = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>No
      <input type="radio" name="sameDayUpcharge" value="D" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$same_day_upcharge = 'D' or not($same_day_upcharge)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Global
       </xsl:when>
      <xsl:otherwise>
         <xsl:if test="$same_day_upcharge = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$same_day_upcharge = 'N'">
             No
          </xsl:if>
          <xsl:if test="$same_day_upcharge = 'D'">
             Global
          </xsl:if>      
      </xsl:otherwise>
      </xsl:choose> 
      </td>
      <td></td>
      <td class="Instruction" align="left">
         This field will determine if the Same Day Upcharge should be charged for this source code.  �Global� means the source code will follow the configuration set on the System Configuration page.
       </td>        			  		
   </tr>
   <tr>
	  <td align="right" class="label" valign="top">Same Day Upcharge-FS Members:</td>
	  <td align="left" valign="top">
	  <xsl:choose>
	  <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
	   <input type="radio" name="sameDayUpchargeFS" value="Y" onChange="setChangedFlag('')" tabindex="58">
	   			  <xsl:if test="$same_day_upcharge_fs  = 'Y'">
	                        <xsl:attribute name="checked">
                        true
                        	</xsl:attribute>
                  </xsl:if> 
       </input>Yes 
       <input type="radio" name="sameDayUpchargeFS" value="N" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$same_day_upcharge_fs = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>No
      <input type="radio" name="sameDayUpchargeFS" value="D" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$same_day_upcharge_fs = 'D' or not($same_day_upcharge_fs)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Global
       </xsl:when>
      <xsl:otherwise>
         <xsl:if test="$same_day_upcharge_fs = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$same_day_upcharge_fs = 'N'">
             No
          </xsl:if>
          <xsl:if test="$same_day_upcharge_fs = 'D'">
             Global
          </xsl:if>      
      </xsl:otherwise>
      </xsl:choose> 
      </td>
      <td></td>
      <td class="Instruction" align="left">
         This field will determine if the Same Day Upcharge should be charged to Free Shipping members for this source code. �Global� means the source code will follow the configuration set on the System Configuration page.
       </td>        			  		
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Display Service Fee:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="displayServiceFeeCode" value="Y" onChange="setChangedFlag('')" tabindex="58">
                  <xsl:if test="$display_service_fee_code = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="displayServiceFeeCode" value="N" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_service_fee_code = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>No
      <input type="radio" name="displayServiceFeeCode" value="D" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_service_fee_code = 'D' or not($display_service_fee_code)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Default
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="$display_service_fee_code = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$display_service_fee_code = 'N'">
             No
          </xsl:if>
          <xsl:if test="$display_service_fee_code = 'D'">
             Default
          </xsl:if>      
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
          This field will indicate if the service fee should be sent with the delivery date within the web service.  This allows the third party partner to display the delivery date and associated fee.
       </td>
   </tr>
   
   
      <tr>
      <td align="right" class="label" valign="top">Display Shipping Fee:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="displayShippingFeeCode" value="Y" onChange="setChangedFlag('')" tabindex="58">
                  <xsl:if test="$display_shipping_fee_code = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="displayShippingFeeCode" value="N" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_shipping_fee_code = 'N'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>No
      <input type="radio" name="displayShippingFeeCode" value="D" onChange="setChangedFlag('')" tabindex="58">
                    <xsl:if test="$display_shipping_fee_code = 'D' or not($display_shipping_fee_code)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                   </xsl:if>
      </input>Default
      </xsl:when>
      <xsl:otherwise>
          <xsl:if test="$display_shipping_fee_code = 'Y'">
             Yes
          </xsl:if>
          <xsl:if test="$display_shipping_fee_code = 'N'">
             No
          </xsl:if>
          <xsl:if test="$display_shipping_fee_code = 'D'">
             Default
          </xsl:if>      
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
            This field will indicate if the shipping fee should be sent with the delivery date within the web service.  This allows the third party partner to display the delivery date and associated fee.      
      </td>
   </tr>   

<!--**WEBLOYALTY - START*********************************************************************************-->
   <tr>
      <td align="right" class="label" valign="top">Webloyalty Flag:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="webloyaltyFlag" value="Y" onChange="setChangedFlag('')" tabindex="60">
      <xsl:if test="$webloyalty_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="webloyaltyFlag" value="N" onChange="setChangedFlag('')" tabindex="60">
      <xsl:if test="$webloyalty_flag = 'N' or not($webloyalty_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$webloyalty_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will be used to indicate if the source code is associated with Webloyalty Orders.
      </td>
   </tr>
<!--**WEBLOYALTY - END***********************************************************************************-->

   <tr>
      <td align="right" class="label" valign="top">IOTW Flag:</td>
      <td align="left" valign="top">
          <xsl:choose>
              <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
                  <input type="radio" name="iotwFlag" value="Y" onChange="setChangedFlag('')" tabindex="61">
                      <xsl:if test="$iotw_flag = 'Y'">
                          <xsl:attribute name="checked">
                          true
                          </xsl:attribute>
                      </xsl:if>
                  </input>Yes
                  <input type="radio" name="iotwFlag" value="N" onChange="setChangedFlag('')" tabindex="62">
                      <xsl:if test="$iotw_flag = 'N' or not($iotw_flag)">
                          <xsl:attribute name="checked">
                          true
                          </xsl:attribute>
                      </xsl:if>
                  </input>No
              </xsl:when>
              <xsl:otherwise>
                  <xsl:choose>
                      <xsl:when test="$iotw_flag = 'Y'">
                      Yes
                      </xsl:when>
                      <xsl:otherwise>
                      No
                      </xsl:otherwise>
                  </xsl:choose>
              </xsl:otherwise>
          </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will be used to indicate if the source code is associated with an IOTW promotion.
      </td>
   </tr>
   
   <tr>
      <td align="right" class="label" valign="top">Bin Check:</td>
        <td align="left" valign="top">
           <div style="height:70px; overflow:auto; border: 1px solid gray;">
              <table>              
                  <xsl:for-each select="root/binCheckPartnerList/partner">
                  <input type="hidden" name="binCheckPartnerName{@num}" value="{@partner_name}"/>
                  <tr> 
                      <td align="right">
                        <xsl:value-of select="@partner_name"/>:
                      </td>
                      <td>
                           <xsl:choose>
                              <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">         
                                  <input type="radio" name="binCheckPartnerActive{@num}" value="Y" onChange="setChangedFlag('')" tabindex="63">
                                      <xsl:if test="key('sourceBinCheckPartnerList',@partner_name)/@num != ''">
                                          <xsl:attribute name="checked">
                                          true
                                          </xsl:attribute>
                                      </xsl:if>
                                  </input>Yes
                                  <input type="radio" name="binCheckPartnerActive{@num}" value="N" onChange="setChangedFlag('')" tabindex="63">
                                      <xsl:if test="not(key('sourceBinCheckPartnerList',@partner_name)/@num != '')">
                                          <xsl:attribute name="checked">
                                          true
                                          </xsl:attribute>
                                      </xsl:if>
                                  </input>No
                              </xsl:when>
                              <xsl:otherwise>
                                  <xsl:choose>
                                      <xsl:when  test="key('sourceBinCheckPartnerList',@partner_name)/@num != ''">
                                      Yes
                                      </xsl:when>
                                      <xsl:otherwise>
                                      No
                                      </xsl:otherwise>
                                  </xsl:choose>
                              </xsl:otherwise>     
                           </xsl:choose>
                      </td>                   
                   </tr>
                   </xsl:for-each>                  
              </table>
          </div> 
        </td>        
      <td></td>
      <td class="Instruction" align="left">
         This field will be used to recognize the bin number of a credit card and apply a partner discount.
      </td>
   </tr>   
<!-- Fuel Surcharge Variables -->

    <tr>
      <td align="right" class="label" valign="top">Fuel Surcharge Flag:</td>
      <td align="left" valign="top">
          <input type="radio" name="applySurchargeCode" value="ON" onChange="setChangedFlag('')" tabindex="64">
                      <xsl:if test="$apply_surcharge_code = 'ON'">
                            <xsl:attribute name="checked">
                            true
                            </xsl:attribute>
                      </xsl:if>
          </input>Yes
          <input type="radio" name="applySurchargeCode" value="OFF" onChange="setChangedFlag('')" tabindex="64">
                      <xsl:if test="$apply_surcharge_code = 'OFF'">
                            <xsl:attribute name="checked">
                            true
                            </xsl:attribute>
                      </xsl:if>
          </input>No
          <input type="radio" name="applySurchargeCode" value="DEFAULT" onChange="setChangedFlag('')"  tabindex="64">
                      <xsl:if test="$apply_surcharge_code = 'DEFAULT' or not($apply_surcharge_code)">
                            <xsl:attribute name="checked">
                            true
                            </xsl:attribute>
                      </xsl:if>
          </input>Default
      </td>
      <td></td>
      <td class="Instruction" align="left">
          This field will indicate if the fuel surcharge is on or off at a source code level. "Default" indicates the source code will follow the configuration set on the Fuel Surcharge Maintenance page.
       </td>
   </tr>

                <tr>
                    <td align="right" class="label" valign="top">
                         Fuel Surcharge Amount:
                    </td>

                    <td align="left" valign="top">
                         <input type="text" name="surchargeAmount"  size="4" maxlength="4"  onChange="setChangedFlag('')" tabindex="65" value="{$surcharge_amount}" />
                    </td>
                    <td></td>
                    <td class="Instruction" align="left">
                        This field will contain the dollar value of the fuel surcharge if the fuel surcharge is on.
                     </td>
                </tr>
                 <tr>
                    <td class="Label" align="right">
                        Surcharge Description: 
                    </td>
                    <td>
                        <input type="text" name="surchargeDescription" onChange="setChangedFlag('')" tabindex="66"  maxlength="50" size = "65" value="{$surcharge_description}"/>
                    </td>
                    <td></td>
                    <td class="Instruction" align="left">
                        This field will contain the fuel surcharge description to be displayed on the Order Entry pages, Customer Service pages and Order Confirmation email if the fuel surcharge in on.
                     </td>                    
                </tr>                       
                <tr>
                    <td class="Label" align="right">
                        Display Surcharge Separately for Order Confirmation: 
                    </td>
                   
                          <td align="left" valign="top">
                              <input type="radio" name="displaySurcharge" value="Y" tabindex="67">
                                          <xsl:if test="$display_surcharge_flag = 'Y'">
                                                <xsl:attribute name="checked">
                                                true
                                                </xsl:attribute>
                                          </xsl:if>
                              </input>Yes
                              <input type="radio" name="displaySurcharge" value="N"  tabindex="67">
                                          <xsl:if test="$display_surcharge_flag = 'N'">
                                                <xsl:attribute name="checked">
                                                true
                                                </xsl:attribute>
                                          </xsl:if>
                              </input>No
                              <input type="radio" name="displaySurcharge" value="D" tabindex="67">
                                          <xsl:if test="$display_surcharge_flag = 'D' or not($display_surcharge_flag)">
                                                <xsl:attribute name="checked">
                                                true
                                                </xsl:attribute>
                                          </xsl:if>
                              </input>Default
                          </td>
                     <td></td>
                    <td class="Instruction" align="left">
                        This field will indicate that the fuel surcharge will be displayed as a separate line. "Default" indicates the source code will follow the configuration set on the Fuel Surcharge Maintenance page.
                     </td>                    
                </tr>


	  <!-- merch_amt_full_refund_flag -->
   
   <tr>
	<td align="right" class="label" valign="top">Require Fully Refund Merchandising Amount:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceSnhData')/@value = 'Y'">
      	  <input type="radio" name="merchAmtFullRefundFlag" value="Y"  tabindex="59">
	        <xsl:if test="$merch_amt_full_refund_flag ='Y' or not($merch_amt_full_refund_flag)">
	           <xsl:attribute name="checked">true</xsl:attribute>
	        </xsl:if>
	      </input>Yes
	      <input type="radio" name="merchAmtFullRefundFlag" value="N"  tabindex="59">
	      <xsl:if test="$merch_amt_full_refund_flag ='N'">
	        <xsl:attribute name="checked">true</xsl:attribute>
	      </xsl:if>
	      </input>No
      </xsl:when>
      <xsl:otherwise>
	       <xsl:if test="$merch_amt_full_refund_flag = 'Y'">Yes</xsl:if>
	       <xsl:if test="$merch_amt_full_refund_flag = 'N'">No</xsl:if>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      Yes = Allow users to post refunds that result in $0 remaining of the Merch + Addon buckets (together).
      No  = Do NOT allow users to post a result in $0 remaining of the Merch + Addon buckets (together).
       
</td>
   </tr>
   <!-- mech_amt_full_refund_flag -->


	<tr>
      <td align="right" class="label" valign="top">Automated Promotion Engine (APE):</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="automatedPromotionEngineFlag" value="Y" onChange="setChangedFlag('')" tabindex="156">
      <xsl:if test="$automated_promotion_engine_flag = 'Y'">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="automatedPromotionEngineFlag" value="N" onChange="setChangedFlag('')" tabindex="156">
      <xsl:if test="$automated_promotion_engine_flag = 'N' or not($automated_promotion_engine_flag)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$automated_promotion_engine_flag = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      Choose Yes if the Automated Promotion Engine will turned on for this source code to automatically created promotions (IOTWs).
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">APE Product Catalog:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <select name="apeProductCatalog" tabindex="157" onChange="onChange=setChangedFlag('')">
            <option value=""></option>
               <option value="Amazon">
               <xsl:if test="$ape_product_catalog = 'Amazon'">
                  <xsl:attribute name="selected">
                  true
                 </xsl:attribute>
               </xsl:if>
               Amazon</option>
         </select>
      </xsl:when>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
       Select a Product Catalog for the Automated Promotion Engine. Promotions will only be created for these products.
      </td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">APE Base Source Codes:</td>
      <td width="25%" align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <textarea cols="50" rows="4" name="apeBaseSourceCodes" tabindex="158" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$ape_base_source_codes" disable-output-escaping="yes"/>
         </textarea>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$ape_base_source_codes" disable-output-escaping="yes"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Enter one or more source codes. If assigning more than one source code, insert a space or carriage return after each source code. Automated Promotion Engine will create the "best" promotion for each product based on the promotions that exist on the Base Source Codes.
      </td>
   </tr>

   <tr>
      <td width="25%" class="label" align="right" valign="top">Special Comments:</td>
      <td width="25%" align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
         <textarea cols="50" rows="4" name="commentText" tabindex="69" onChange="setChangedFlag('')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:value-of select="$comment_text" disable-output-escaping="yes"/>
         </textarea>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$comment_text" disable-output-escaping="yes"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      This text contains any additional special comments for the handling of this Source.
      </td>
   </tr>
   <tr>
      <td colspan="4" class="colHeader">&nbsp;</td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Partner Id:</td>
      <td align="left" valign="top">
         <xsl:choose>
           <xsl:when test="not(key('pagedata','SourcePromotionCodeData')/@value = 'Y')">
             <xsl:value-of select="$partner_name"/>
           </xsl:when>
           <xsl:when test="$partner_name = '' or not($partner_name)">
             <select name="partnerName" onChange="javascript:partnerChange()" tabindex="70">
               <option value=""></option>
               <xsl:for-each select="root/partnerList/partner">
                  <option value="{@partner_name}">
                     <xsl:value-of select="@partner_name"/>
                  </option>
               </xsl:for-each>
             </select>
           </xsl:when>
           <xsl:otherwise>
             <xsl:value-of select="$partner_name"/>
           </xsl:otherwise>
         </xsl:choose>
      </td>
   </tr>
   <xsl:if test="key('pagedata','SourcePromotionCodeData')/@value = 'Y' and not($partner_name)">
   <tr>
      <td align="right" class="label" valign="top">Partner Promotion Code:</td>
      <td align="left" valign="top">
         <select name="programName" tabindex="71" onChange="setChangedFlag('')">
            <option value=""></option>
            <xsl:for-each select="root/partnerProgramList/partnerProgram">
               <option value="{@program_name}">
               <xsl:if test="@program_name = $program_name">
                <xsl:attribute name="selected">
                true
               </xsl:attribute>
               </xsl:if>
                  <xsl:value-of select="@partner_name"/>|
                  <xsl:value-of select="@program_name"/>|
                  <xsl:value-of select="@program_type"/>
               </option>
               </xsl:for-each>
         </select>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If the source code is one that gives miles or points the corresponding partnership code number will be entered in this field.  The partnership code will drive the offer on the source code
      </td>
   </tr>
   </xsl:if>
   <tr>
      <td align="right" class="label" valign="top">PST Code:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePromotionCodeData')/@value = 'Y'">
      <input type="text" size="15" maxlength="15" name="promotionCode" tabindex="72" value="{$promotion_code}" onChange="setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$promotion_code"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the PST code for the base miles or points.  This is sent to the partner to specify the amount of miles or points and for some partners to specify where the miles are to come from.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Bonus PST Code:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePromotionCodeData')/@value = 'Y'">
      <input type="text" size="15" maxlength="15" name="bonusPromotionCode" tabindex="73" value="{$bonus_promotion_code}" onChange="setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$bonus_promotion_code"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This is the PST code for the bonus miles or points if Bonus miles use a different PST code. This is sent to the partner to specify the amount of miles or points and for some partners to specify where the miles are to come from.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Bonus Bank:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="radio" name="partnerBankFlag" value="Y" onChange="setChangedFlag('')" tabindex="74">
      <xsl:if test="$partner_bank_id != ''">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>Yes
      <input type="radio" name="partnerBankFlag" value="N" onChange="setChangedFlag('')" tabindex="74">
      <xsl:if test="$partner_bank_id = '' or not($partner_bank_id)">
                        <xsl:attribute name="checked">
                        true
                        </xsl:attribute>
                  </xsl:if>
      </input>No
      </xsl:when>
      <xsl:otherwise>
      <xsl:choose>
      <xsl:when test="$partner_bank_id = 'Y'">
      Yes
      </xsl:when>
      <xsl:otherwise>
      No
      </xsl:otherwise>
      </xsl:choose>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      If a Y is entered in this field, bonus rewards for this source code will be obtained from a partner bonus bank balance.  If N is entered it will not.
      </td>
   </tr>
   
   <tr>
      <td colspan="4" class="colHeader">Sympathy Controls</td>
   </tr>
   <tr>
      <td width="25%" class="label" align="right" valign="top">Funeral Cemetery Location Check</td>
      <td width="25%" align="left" valign="top">
        <input type="radio" name="fcLocCheck" value="Y" onChange="setChangedFlag('')" tabindex="96">
            <xsl:if test="$funeral_cemetery_loc_chk = 'Y'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>On
        <input type="radio" name="fcLocCheck" value="N" onChange="setChangedFlag('')" tabindex="97">
            <xsl:if test="$funeral_cemetery_loc_chk = 'N'  or not($funeral_cemetery_loc_chk)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Off
        <input type="radio" name="fcLocCheck" value="D" onChange="setChangedFlag('')" tabindex="98">
            <xsl:if test="$funeral_cemetery_loc_chk = 'D'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Default
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
          This field will determine if dropship orders should be delivered to a Funeral/Cemetery.
          'Default' means the source code will follow the configuration set on System Config page. 
      </td>
    </tr>
    <tr>
      <td width="25%" class="label" align="right" valign="top">Hospital Location Check</td>
      <td width="25%" align="left" valign="top">
        <input type="radio" name="hospLocCheck" value="Y" onChange="setChangedFlag('')" tabindex="99">
            <xsl:if test="$hospital_loc_chck = 'Y'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>On
        <input type="radio" name="hospLocCheck" value="N" onChange="setChangedFlag('')" tabindex="100">
            <xsl:if test="$hospital_loc_chck = 'N'  or not($hospital_loc_chck)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Off
         <input type="radio" name="hospLocCheck" value="D" onChange="setChangedFlag('')" tabindex="101">
            <xsl:if test="$hospital_loc_chck = 'D'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Default
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
          This field will determine if dropship orders should be delivered to a Hospital.
          'Default' means the source code will follow the configuration set on System Config page. 
      </td>
    </tr>
    
    <tr>
      <td width="25%" class="label" align="right" valign="top">Funeral/Cemetery Lead Time Check</td>
      <td width="25%" align="left" valign="top">
        <input type="radio" name="funeralLeadTimeCheck" value="Y" onChange="setChangedFlag('')" tabindex="102">
            <xsl:if test="$funeral_cemetery_lead_time_chk = 'Y'  or not($funeral_cemetery_lead_time_chk)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>On
        <input type="radio" name="funeralLeadTimeCheck" value="N" onChange="setChangedFlag('')" tabindex="103">
            <xsl:if test="$funeral_cemetery_lead_time_chk = 'N'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Off
        <input type="radio" name="funeralLeadTimeCheck" value="D" onChange="setChangedFlag('')" tabindex="104">
            <xsl:if test="$funeral_cemetery_lead_time_chk = 'D'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Default
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
          This field will determine if lead time should be checked before accepting an order for delivery to a funeral/cemetery service.
          �Default� means the source code will follow the configuration set on the System Configuration page.
      </td>
    </tr>
     <tr>
      <td align="right" class="label" valign="top">Funeral/ Cemetery Lead time</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
      <input type="text" size="9" maxlength="2" name="funeralLeadTime" tabindex="105" onChange="javascript:setChangedFlag(this);" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
       <xsl:choose>
          <xsl:when test="$funeral_cemetery_lead_time = '' or not($funeral_cemetery_lead_time)">
         	<xsl:attribute name="value">
    			<xsl:value-of select="4" />
  			</xsl:attribute>
  		  </xsl:when>
  		  <xsl:otherwise>
  		  	<xsl:attribute name="value">
    			<xsl:value-of select="$funeral_cemetery_lead_time" />
  			</xsl:attribute>
  		  </xsl:otherwise>
  		</xsl:choose>
  	  </input>
         	
      </xsl:when>
      <xsl:otherwise>
      <xsl:value-of select="$funeral_cemetery_lead_time"/>
      </xsl:otherwise>
      </xsl:choose>
      
            
         
            
     
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will determine the number of hours (lead time)to use for processing an order if order is being delivered for a Funeral/Cemetery service.
      </td>
   </tr>
    <tr>
      <td align="right" class="label" valign="top">Business Operating Hours Mon_Fri</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
       <select name="busOpHoursMonFriStart" tabindex="106" onChange="setChangedFlag('')">
           
               <option value="9:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '9:00' or $bo_hrs_mon_fri_start = '' or not($bo_hrs_mon_fri_start)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '17:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '18:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_mon_fri_start = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
      &nbsp;&nbsp;to&nbsp;&nbsp;
      <select name="busOpHoursMonFriEnd" tabindex="107" onChange="setChangedFlag('')">
           
              <option value="9:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '9:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '17:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '18:00' or $bo_hrs_mon_fri_end = '' or not($bo_hrs_mon_fri_end)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_mon_fri_end = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
      
      </xsl:when>
      <xsl:otherwise>
     		<xsl:if test="$bo_hrs_mon_fri_start = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_start = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      &nbsp;-&nbsp;to&nbsp;-&nbsp;
      <xsl:if test="$bo_hrs_mon_fri_end = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_mon_fri_end = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will allow user to define start and end time for General Operating Business hours , Monday to Friday based on the recipients time zone. 
      </td>
   </tr>
    <tr>
      <td align="right" class="label" valign="top">Business Operating Hours Saturday</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
      <select name="busOpHoursSatStart" tabindex="108" onChange="setChangedFlag('')">
           
              <option value="9:00">
                         <xsl:if test="$bo_hrs_sat_start = '9:00' or $bo_hrs_sat_start = '' or not($bo_hrs_sat_start)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_sat_start = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_sat_start = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_sat_start = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_sat_start = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_sat_start = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_sat_start = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_sat_start = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_sat_start = '17:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_sat_start = '18:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_sat_start = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_sat_start = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
      &nbsp;&nbsp;to&nbsp;&nbsp;
      
      <select name="busOpHoursSatEnd" tabindex="109" onChange="setChangedFlag('')">
           
               <option value="9:00">
                         <xsl:if test="$bo_hrs_sat_end = '9:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_sat_end = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_sat_end = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_sat_end = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_sat_end = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_sat_end = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_sat_end = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_sat_end = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_sat_end = '17:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_sat_end = '18:00' or $bo_hrs_sat_end = '' or not($bo_hrs_sat_end)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_sat_end = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_sat_end = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
      	
      
      </xsl:when>
      <xsl:otherwise>
      	 <xsl:if test="$bo_hrs_sat_start = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_start = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      &nbsp;-&nbsp;to&nbsp;-&nbsp;
      <xsl:if test="$bo_hrs_sat_end = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sat_end = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will allow user to define start and end time for General Operating Business hours for Saturday based on the recipients time zone.
      </td>
   </tr>
    <tr>
      <td align="right" class="label" valign="top">Business Operating Hours Sunday</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="key('pagedata','SourcePCardData')/@value = 'Y'">
      <select name="busOpHoursSunStart" tabindex="110" onChange="setChangedFlag('')">
           
              
              <option value="9:00">
                         <xsl:if test="$bo_hrs_sun_start = '9:00' or $bo_hrs_sun_start = '' or not($bo_hrs_sun_start)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_sun_start = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_sun_start = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_sun_start = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_sun_start = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_sun_start = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_sun_start = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_sun_start = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_sun_start = '17:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_sun_start = '18:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_sun_start = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_sun_start = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
     &nbsp;&nbsp;to&nbsp;&nbsp;
     
      <select name="busOpHoursSunEnd" tabindex="111" onChange="setChangedFlag('')">
           
              <option value="9:00">
                         <xsl:if test="$bo_hrs_sun_end = '9:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         9:00 AM
              </option>
              <option value="10:00">
                         <xsl:if test="$bo_hrs_sun_end = '10:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         10:00 AM
              </option>
              <option value="11:00">
                         <xsl:if test="$bo_hrs_sun_end = '11:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         11:00 AM
              </option>
              <option value="12:00">
                         <xsl:if test="$bo_hrs_sun_end = '12:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         Noon
              </option>
              <option value="13:00">
                         <xsl:if test="$bo_hrs_sun_end = '13:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         1:00 PM
              </option>
              <option value="14:00">
                         <xsl:if test="$bo_hrs_sun_end = '14:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         2:00 PM
              </option>
              <option value="15:00">
                         <xsl:if test="$bo_hrs_sun_end = '15:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         3:00 PM
              </option>
              <option value="16:00">
                         <xsl:if test="$bo_hrs_sun_end = '16:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         4:00 PM
              </option>
              <option value="17:00">
                         <xsl:if test="$bo_hrs_sun_end = '17:00' or $bo_hrs_sun_end = '' or not($bo_hrs_sun_end)">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         5:00 PM
              </option>
              <option value="18:00">
                         <xsl:if test="$bo_hrs_sun_end = '18:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         6:00 PM
              </option>
              <option value="19:00">
                         <xsl:if test="$bo_hrs_sun_end = '19:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         7:00 PM
              </option>
              <option value="20:00">
                         <xsl:if test="$bo_hrs_sun_end = '20:00'">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         8:00 PM
              </option>
           
        </select>
      	
      
      </xsl:when>
      <xsl:otherwise>
      	 <xsl:if test="$bo_hrs_sun_start = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_start = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      &nbsp;-&nbsp;to&nbsp;-&nbsp;
      <xsl:if test="$bo_hrs_sun_end = '9:00'">
               <xsl:value-of select="'9:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '10:00'">
               <xsl:value-of select="'10:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '11:00'">
               <xsl:value-of select="'11:00 AM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '12:00'">
               <xsl:value-of select="'Noon'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '13:00'">
               <xsl:value-of select="'1:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '14:00'">
               <xsl:value-of select="'2:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '15:00'">
               <xsl:value-of select="'3:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '16:00'">
               <xsl:value-of select="'4:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '17:00'">
               <xsl:value-of select="'5:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '18:00'">
               <xsl:value-of select="'6:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '19:00'">
               <xsl:value-of select="'7:00 PM'"/>
         </xsl:if>
         <xsl:if test="$bo_hrs_sun_end = '20:00'">
               <xsl:value-of select="'8:00 PM'"/>
         </xsl:if>
      
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      This field will allow user to define start and end time for General Operating Business hours for Sunday based on the recipients time zone.
      </td>
   </tr>
   

   <tr>
      <td colspan="4" class="colHeader">Florist Selection for Source Code</td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Legacy ID:</td>
      <td align="left" valign="top"><xsl:value-of select="$legacy_id"/></td>
      <td></td>
      <td class="Instruction" align="left">
      If this Source Code is mapped to a Legacy ID the Legacy ID will appear here. This field is read only.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Primary Florist ID:</td>
      <td align="left" valign="top">
      <xsl:choose>
      <xsl:when test="/root/sourceList/sourceFloristPriorityList/sourceFloristPriority/@priority=1">
        <xsl:for-each select="/root/sourceList/sourceFloristPriorityList/sourceFloristPriority">
          <xsl:if test="@priority=1">
            <input type="text" size="9" maxlength="9" name="primaryFloristId" tabindex="80" value="{@florist_id}" onChange="javascript:setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
          </xsl:if>
         </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
            <input type="text" size="9" maxlength="9" name="primaryFloristId" tabindex="80" value="" onChange="javascript:setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
      </xsl:otherwise>
      </xsl:choose>
      </td>
      <td></td>
      <td class="Instruction" align="left">
      Enter a Florist ID to assign a Primary florist to the source code.  Example: nn-nnnnaa
      </td>
   </tr>

   <tr>
      <td width="25%" class="label" align="right" valign="top">Backup Florist ID(s):</td>
      <td width="25%" align="left" valign="top">
         <textarea cols="51" rows="2" name="backupFloristIds" tabindex="81" onChange="setChangedFlag(this)" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
         <xsl:if test="key('pagedata','SourceDefinitionData')/@value != 'Y'"><xsl:attribute name="readonly"/></xsl:if>
         <xsl:for-each select="/root/sourceList/sourceFloristPriorityList/sourceFloristPriority">
         <xsl:if test="@priority > 1"><xsl:value-of select="@florist_id"/><xsl:value-of select="' '"/></xsl:if><xsl:if test="(position() mod 4) = 1">&#13;</xsl:if>
         </xsl:for-each>
         </textarea>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
      Enter one or more Florist IDs to assign Backup florists to the source code.  If assigning more than one florist, insert a space 
      or carriage return after each Florist ID.
      </td>
   </tr>

    <tr>
      <td width="25%" class="label" align="right" valign="top">Allow Random Weighted Flag:</td>
      <td width="25%" align="left" valign="top">
        <input type="radio" name="randomWeightedFlag" value="Y" onChange="setChangedFlag('')" tabindex="82">
            <xsl:if test="$primary_backup_rwd_flag = 'Y' or not($primary_backup_rwd_flag)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>Yes
        <input type="radio" name="randomWeightedFlag" value="N" onChange="setChangedFlag('')" tabindex="83">
            <xsl:if test="$primary_backup_rwd_flag = 'N'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>
        </input>No
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
          Choose Yes if orders from this source code should follow the normal random weighted process 
          when the Primary and Backup florists are not available. 
          Choose No if orders should go to queues in this situation.
      </td>
    </tr>
    
    <tr>
      <td width="25%" class="label" align="right" valign="top">OSCAR Florist Selection: </td>
      <td width="25%" align="left" valign="top">
          <input type="radio" name="oscarSelectionEnabledFlag" value="Y" tabindex="84" onChange="javascript:setChangedFlag('')">            
            <xsl:if test="$oscar_selection_enabled_flag = 'Y'">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>              
          </input>Yes
          <input type="radio" name="oscarSelectionEnabledFlag" value="N" tabindex="85" onChange="javascript:setChangedFlag('')">           
            <xsl:if test="$oscar_selection_enabled_flag = 'N' or not($oscar_selection_enabled_flag)">
              <xsl:attribute name="checked">
                true
              </xsl:attribute>
            </xsl:if>             
          </input>No
       </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left">
          If Yes, Apollo will evaluate source code for OSCAR 
          florist selection when configured to check source 
          code. Group must be selected if control is set to Yes. 
          If control is set to No, Apollo will ignore Group.
      </td>
    </tr>
    
    <tr>
      <td width="25%" class="label" align="right" valign="top">OSCAR Group: </td>
      <td width="25%" align="left" valign="top">
        <select name="oscarScenarioGroupId" tabindex="86" onChange="setChangedFlag('')">
           <xsl:for-each select="root/oscarScenarioGroupList/oscarScenarioGroup">
              <option value="{@oscar_scenario_group_id}">
                         <xsl:if test="@oscar_scenario_group_id = $oscar_scenario_group_id">
                             <xsl:attribute name="selected">true</xsl:attribute>
                         </xsl:if>
                         <xsl:value-of select="@oscar_scenario_group_name"/>
              </option>
           </xsl:for-each>
        </select>
      </td>
      <td width="5%"></td>
      <td width="45%" class="Instruction" align="left"></td>
    </tr>
    
   <tr>
      <td colspan="4" class="colHeader">&nbsp;</td>
   </tr>
   
   <tr>
      <td></td>   
      <td align="left"><a href="javascript:showPrepopulateDeliveryAndBillingInfoPopup();">Prepopulate Delivery and Billing Information</a></td>
      <td></td>
      <td></td>
   </tr>

   <tr>
      <td align="right" class="label" valign="top">Last Updated On:</td>
      <td align="left" valign="top"><xsl:value-of select="$updated_on"/></td>
      <td></td>
      <td class="Instruction" align="left">
      This is the last time that a change was made to this source code.
      </td>
   </tr>
   <tr>
      <td align="right" class="label" valign="top">Last Updated By:</td>
      <td align="left" valign="top"><xsl:value-of select="$updated_by"/></td>
      <td></td>
      <td class="Instruction" align="left">
      This field will contain the name of the person who is adding a new source code or editing an existing one.
      </td>
   </tr>
   <tr>
      <td>&nbsp;</td>
      <td align="left">
      <xsl:if test="key('pagedata','SourceDefinitionData')/@value = 'Y' or key('pagedata','SourcePaymentMethodData')/@value = 'Y' or key('pagedata','SourcePriceHeaderData')/@value = 'Y' or key('pagedata','SourcePromotionCodeData')/@value = 'Y' or key('pagedata','SourcePCardData')/@value = 'Y' or key('pagedata','SourceSnhData')/@value = 'Y'">
        <button class="blueButton" onClick="javascript:doSave()" tabindex="90" accesskey="s">(S)ave</button>&nbsp;&nbsp;
        <button class="blueButton" onClick="javascript:doReset()" tabindex="91" accesskey="r">(R)eset All Fields</button>&nbsp;&nbsp;
      </xsl:if>
      </td>
   </tr>

   <xsl:if test="$partner_name != ''">

   <tr><td colspan="4">&nbsp;</td></tr>
   <tr>
      <td colspan="4" class="colHeader">&nbsp;Partner Promotion Information</td>
   </tr>
   <tr>
      <td colspan="4">
      <table width="850" border="0" cellpadding="3" cellspacing="0">
        <tr>
          <td width="50">&nbsp;</td>
          <td width="100" align="left" class="label">Start Date</td>
          <td width="100" align="left" class="label">End Date</td>
          <td width="150" align="left" class="label">Promotion Code</td>
          <td width="150" align="left" class="label">Last Updated By</td>
          <td width="150" align="left" class="label">Last Updated On</td>
          <td width="150" align="left" class="label">Last Post Date</td>
        </tr>
        <xsl:for-each select="root/sourceProgramList/sourceProgram">
          <tr>
            <td align="left">
              <div id="edit{@num}">
                <xsl:if test="key('pagedata','SourcePromotionCodeData')/@value = 'Y'">
                  <button class="blueButton" onClick="programEdit('{@source_program_ref_id}','{@start_date}','{@end_date}','{@last_post_date}','{@num}')" tabindex="93">
                  Edit
                  </button>
                </xsl:if>
              </div>
            </td>
            <td align="left"><xsl:value-of select="@start_date"/></td>
            <td align="left"><xsl:value-of select="@end_date"/></td>
            <td align="left"><xsl:value-of select="@program_name"/></td>
            <td align="left"><xsl:value-of select="@updated_by"/></td>
            <td align="left"><xsl:value-of select="@updated_on"/></td>
            <td align="left"><xsl:value-of select="@last_post_date"/></td>
          </tr>
        </xsl:for-each>
        <xsl:if test="key('pagedata','SourcePromotionCodeData')/@value = 'Y'">
          <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <button class="blueButton" onClick="programAdd()" accesskey="a" tabindex="95">(A)dd Promotion</button>
            </td>
          </tr>
        </xsl:if>
      </table>
      </td>
   </tr>

   </xsl:if>

</table>

</td></tr>
</table>

<table width="98%" border="0" align="center" cellpadding="2" cellspacing="0">
   <tr><td align="right">
     <button class="blueButton" onClick="javascript:doExit()" tabindex="998">(E)xit</button>
   </td></tr>
   <tr><td align="right">
     <button class="blueButton" onclick="javascript:scroll(0,0)" tabindex="999" accesskey="t">(T)op of page</button>
   </td></tr>
</table>

<!-- end of the entire content div -->
</div>

</form>

<div id="waitDiv" style="display:none">
<table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
      <tr>
        <td width="100%">
<table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
<tr>
  <td>
     <table width="100%" cellspacing="0" cellpadding="0" border="0">
         <tr>
           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
           <td id="waitTD"  width="50%" class="WaitMessage"></td>
         </tr>
     </table>
   </td>
</tr>
</table>
        </td>
      </tr>
</table>
</div>


</body>
</html>

<xsl:if test="key('pagedata','SourceDefinitionData')/@value = 'Y'">
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "startDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar1"  // ID of the button
    }
  );
  Calendar.setup(
    {
      inputField  : "endDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar2"  // ID of the button
    }
  );
</script>
</xsl:if>

   </xsl:template>

</xsl:stylesheet>