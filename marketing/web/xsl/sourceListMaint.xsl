<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<!--  <xsl:import href="pagination.xsl"/> --> <!--  This is unused -->
<xsl:import href="sourceSearchOptions.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="result" select="key('pagedata','result')/@value"/>
<xsl:variable name="resultAction" select="key('pagedata','radioAction')/@value"/>
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<title>
        Master Source Code List Maintenance
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<!--<link rel="stylesheet" type="text/css" href="css/calendar.css"/>-->
<style type="text/css">
		    /* Scrolling table overrides for the search results table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" language="JavaScript">
<![CDATA[
var changedFlag = false;

function initInputFieldStyle() {
    document.theform.sourceCodeStart.className="input";
    if (!document.theform.sourceCodeEnd.disabled) {
    document.theform.sourceCodeEnd.className="input";
    }
    if (!document.theform.description.disabled) {
    document.theform.description.className="input";
    }
    if (!document.theform.sourceType.disabled) {
    document.theform.sourceType.className="input";
    }
}

// This is an object that validates the form fields when the Save button is pressed.
function FormValidatorAdd() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.sourceCodeStart.value == "") {
       msg = msg + "Source Code Start is required.\n";
       if (document.theform.sourceCodeStart.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceCodeStart.tabIndex;
        this.focusOnField = document.theform.sourceCodeStart.name;
       }
       document.theform.sourceCodeStart.className="ErrorField";
   } 
   
   if ((document.theform.sourceCodeEnd.value != "") && isNaN(document.theform.sourceCodeStart.value)) {
       msg = msg + "For ranges, Source Code Start must be a numeric value.\n";
       if (document.theform.sourceCodeStart.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceCodeStart.tabIndex;
        this.focusOnField = document.theform.sourceCodeStart.name;
       }
       document.theform.sourceCodeStart.className="ErrorField";
   } else if ((document.theform.sourceCodeEnd.value != "") && isNaN(document.theform.sourceCodeEnd.value)) {
       msg = msg + "Source Code End must be a numeric value.\n";
       if (document.theform.sourceCodeEnd.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceCodeEnd.tabIndex;
        this.focusOnField = document.theform.sourceCodeEnd.name;
       }
       document.theform.sourceCodeEnd.className="ErrorField";
   } else if ((document.theform.sourceCodeStart.value != "") && (document.theform.sourceCodeEnd.value != "")  && (parseInt(document.theform.sourceCodeStart.value) > parseInt(document.theform.sourceCodeEnd.value))) {
       msg = msg + "Source Code Start can not be greater than Source Code End.\n";
       if (document.theform.sourceCodeStart.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceCodeStart.tabIndex;
        this.focusOnField = document.theform.sourceCodeStart.name;
       }
       document.theform.sourceCodeStart.className="ErrorField";
   }
   
   // Additional checks for "reserve" logic.
   if (document.theform.radioAction[0].checked) {
   if (document.theform.description.value == "") {
       msg = msg + "Description is required.\n";
       if (document.theform.description.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.description.tabIndex;
        this.focusOnField = document.theform.description.name;
       }
       document.theform.description.className="ErrorField";
   }
   if (document.theform.sourceType.value == "") {
       msg = msg + "Source Type is required.\n";
       if (document.theform.sourceType.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.sourceType.tabIndex;
        this.focusOnField = document.theform.sourceType.name;
       }
       document.theform.sourceType.className="ErrorField";
   }
   }
   
   this.errorMsg = msg;
}

function doBackAction() {
   history.back();
}

function setChangedFlag() {
  changedFlag = true;
}

function doExit() {
   if (changedFlag && confirm("Changes have been made. Would you like to return to the screen to save your changes?\n" +
    "Select OK to return to the screen to save your changes.\n" +
    "Select Cancel to discard changes and exit.")) {
    doAdd();
   } else {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
   }
}

function init() {
   modifyDataFields();
   scroll(0,0);
   document.theform.sourceCodeStart.focus();
   changedFlag = false;
}

function doClear() {
   initInputFieldStyle();
   clearAllFields(theform);
   setDefaultFieldValues();
   init();
}

function setDefaultFieldValues() {

  // If the values are undefined, set them.
  if (!document.theform.radioAction.value)
  document.theform.radioAction[0].checked = true;
}

function doAdd() {
   var formValidator = new FormValidatorAdd();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
     document.theform.action_type.value = "insert";
     submitAction("sourceListMaint.do");
   }
}

function doSearch() {
   // Set a hidden field.
   document.theform.action_type.value = "search";
   submitActionNewWindow("sourceListMaint.do");
}

function modifyDataFields() {
  changedFlag = true;
  
  // Parse the Source Code Start value.
  // Only numeric values with non-leading zeros can specify an End value.
  var codeStart = document.theform.sourceCodeStart.value;
  if (codeStart != "" && codeStart.substring(0,1) != "0" && !isNaN(codeStart)) {
    document.theform.sourceCodeEnd.disabled=false;
    document.theform.sourceCodeEnd.className="input";
    document.theform.sourceCodeEnd.focus();
  } else {
    document.theform.sourceCodeEnd.value="";
    document.theform.sourceCodeEnd.disabled=true;
    document.theform.sourceCodeEnd.className="DisabledField";
  }
  if (document.theform.radioAction[1].checked) {
  // Release is checked.
  document.theform.description.disabled=true;
  document.theform.sourceType.disabled=true;
  document.theform.description.className="DisabledField";
    document.theform.sourceType.className="DisabledField";
  } else {
  // Reserve is checked.
  document.theform.description.disabled=false;
  document.theform.sourceType.disabled=false;
 document.theform.description.className="input";
    document.theform.sourceType.className="input";
  }
}

function submitInfoAction(url, visualMessage) {
   scroll(0,0);
   showWaitMessage("content", "wait", visualMessage);
   submitAction(url);
}

function submitAction(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function submitActionNewWindow(url) {
   var winName = "win" + Math.round(1000*Math.random());
   window.open('',winName,'scrollbars=0,menubar=1,toolbar=0,location=0,status=0,resizable=1'); 
   document.theform.action = url;
   document.theform.target = winName;
   document.theform.submit();
}
]]>
</script>
</head>
<body onLoad="init()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Master Source Code List Maintenance'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
<form name="theform" method="post" action="">
        <input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
        <xsl:call-template name="securityanddata"></xsl:call-template>
<div id="content" style="display:block">
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="$result &gt; '0'">
				   <td class="OperationSuccess">
						<xsl:value-of select="$result"/>&nbsp;SOURCE CODE<xsl:if test="$result &gt; '1'">S</xsl:if>&nbsp;SUCCESSFULLY&nbsp;<xsl:if test="$resultAction = 'reserve'">RESERVED</xsl:if><xsl:if test="$resultAction = 'release'">RELEASED</xsl:if>
				   </td>
           </xsl:when>
           <xsl:when test="$result = '-2'">
				   <td class="errorMessage">
						OPERATION FAILED. THE REQUESTED MASTER SOURCE CODE(S) ARE NOT ON FILE. 
				   </td>
           </xsl:when>
           <xsl:when test="$result = '-1' and $resultAction = 'reserve'">
				   <td class="errorMessage">
						OPERATION FAILED. THE FOLLOWING SOURCE CODES HAVE ALREADY BEEN RESERVED:&nbsp;&nbsp; 
            <xsl:for-each select="root/sourceList/source">
            <xsl:if test="@num != 1">,&nbsp;
            </xsl:if>
                        <xsl:value-of select="@source_code"/>
                      </xsl:for-each>
				   </td>
           </xsl:when>
           <xsl:when test="$result = '-1' and $resultAction = 'release'">
				   <td class="errorMessage">
						OPERATION FAILED. THE FOLLOWING SOURCE CODES HAVE ALREADY BEEN CREATED:&nbsp;&nbsp; 
            <xsl:for-each select="root/sourceList/source">
            <xsl:if test="@num != 1">,&nbsp;
            </xsl:if>
                        <xsl:value-of select="@source_code"/>
                      </xsl:for-each>
				   </td>
           </xsl:when>
           </xsl:choose>
			   </tr>
		   </table>	
        <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
          <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td width="20%" class="label" align="right">Source Code Start<font color="red">*&space;</font>:</td>
                  <td width="10%" align="left"><input type="text" size="5" maxlength="5" name="sourceCodeStart" tabindex="1" selected="true" value="{key('pagedata','sourceCodeStart')/@value}" onChange="javascript:modifyDataFields()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" />
            </td>     
            <td width="15%"></td>
      <td class="Instruction" align="left" valign="top">
      Enter a Source Code to Reserve or Release. If the entered Source Code is numeric and does not have leading zeros, a Source Code End value can be specified to perform the operation over a range of numeric values.
      </td>
                  </tr>
                  <tr>
                  <td width="20%" class="label" align="right">Source Code End:</td>
                  <td width="10%"><input type="text" size="5" maxlength="5" name="sourceCodeEnd" tabindex="2" value="{key('pagedata','sourceCodeEnd')/@value}" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
                  </td>
                  <td width="5%"></td>
      <td class="Instruction" align="left" valign="top">
      When enabled, enter a Source Code End to define a range of values to Reserve or Release, inclusive.
      </td>
                  </tr>
                  <tr>
                  <td/>
                  <td align="left" valign="top">
      <input type="radio" name="radioAction" value="reserve" tabindex="3" onChange="setChangedFlag()" onClick="javascript:modifyDataFields()">
      <xsl:if test="key('pagedata','radioAction')/@value = 'reserve'">
      <xsl:attribute name="checked">
                          true
                           </xsl:attribute>                        
                        </xsl:if>
      </input>Reserve
      <input type="radio" name="radioAction" value="release" tabindex="4" onChange="setChangedFlag()" onClick="javascript:modifyDataFields()">
      <xsl:if test="key('pagedata','radioAction')/@value = 'release'">
      <xsl:attribute name="checked">
                          true
                           </xsl:attribute>                        
                        </xsl:if>
      </input>Release
      </td>
      <td width="5%"></td>
      <td class="Instruction" align="left" valign="top">
      Reserve will reserve the specified Source Code(s). If an entered Source Code is already reserved, the record must be released first.  Selecting Release will release the Source Code(s), provided that a corresponding Source Code record has not already been created on the system.
      </td>
                </tr>
                <tr>
                  <td align="right" class="label">Description:</td>
                  <td align="left"><input type="text" size="40" maxlength="40" name="description" tabindex="5" value="{key('pagedata','description')/@value}" onChange="setChangedFlag()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>
                </td>
                <td width="5%"></td>
      <td class="Instruction" align="left" valign="top">
      This text describes the offer for the source code.  This text will be visible on the order and in order entry.
      </td>
                </tr>
                <tr>
                  <td align="right" class="label">Source Type:</td>                  
                  <td align="left"><select name="sourceType" tabindex="6" onChange="setChangedFlag()">
                       <option value=""></option>
                      <xsl:for-each select="root/sourceTypeList/sourceType">
                          <option value="{@source_type}">
                          <xsl:if test="@source_type = key('pagedata','sourceType')/@value">
                           <xsl:attribute name="selected">
                          true
                           </xsl:attribute>                        
                        </xsl:if>
                        <xsl:value-of select="@source_type"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                  <td width="5%"></td>
      <td class="Instruction" align="left" valign="top">
      The source type is used to group source codes together into a higher-level list.  Examples are United, American Airlines, etc.
      </td>
                </tr>
              </table></td>
          </tr>
        </table>
        <!-- end of the entire content div -->
        </div>
      </form>
<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>
       <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="left"><button class="blueButton" onClick="javascript:doAdd()" accesskey="s" tabindex="50">
              (S)ave
            </button>
          &nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doSearch()" accesskey="v" tabindex="51">
              (V)iew All
            </button>
          &nbsp;&nbsp;&nbsp;<button class="blueButton" onClick="javascript:doClear()" accesskey="c" tabindex="52">
              (C)lear All Fields
            </button></td>
          <td align="right"><button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="53">
              (E)xit
            </button></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>      
      
      
<xsl:call-template name="footer"></xsl:call-template>
</body></html>
  </xsl:template>
  </xsl:stylesheet>
  