<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="name" />
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Source Type Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">

function doBackAction() {
   document.theform.action_type.value = "load";
   submitAction();
}

function submitAction() {
   document.theform.action = "sourceTypeMaint.do";
   document.theform.target = "";
   document.theform.submit();
}

</script>
</head>

<body>

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Source Type List'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<input type="hidden" name="action_type" value="{key('pagedata','action_type')/value}"/>

<table width="400" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="100" class="Label">Source Code</td>
        <td width="300" class="Label">Source Type</td>
    </tr>

<xsl:for-each select="sourceTypeList/sourceType">
    <tr>
        <td><xsl:value-of select="@num"/></td>
        <td><xsl:value-of select="marketing_group"/></td>
    </tr>
</xsl:for-each>

</table>

</form>

<xsl:call-template name="footer"></xsl:call-template>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>
