<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="pagination.xsl"/>
<xsl:import href="productFeedSearchOptions.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="pageNumber" select="key('pagedata','pageNumber')/@value"/>
<xsl:variable name="pageCountTotal" select="key('pagedata','pageCountTotal')/@value"/>
<xsl:variable name="recordCountTotal" select="key('pagedata','recordCountTotal')/@value"/>
<xsl:variable name="scRecordNumber" select="key('pagedata','scRecordNumber')/@value"/>
<!-- Hardcoded value: only 50 records per page can be viewed -->
<xsl:variable name="scPageMaxRecordCount" select="'50'"/>
<!-- Root template -->
<xsl:template match="/">
<html>
<head>
<title>
        Product Feed Maintenance - Search
      </title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<link rel="stylesheet" type="text/css" href="css/calendar.css"/>
<style type="text/css">
		    /* Scrolling table overrides for the search results table */
			div.tableContainer table {
				width: 97%;
			}
			div#messageContainer {
				height: 130px;
				width: 100%;
			}
			</style>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" language="JavaScript">

var rowNumber = "<xsl:value-of select="$scRecordNumber"/>";

<![CDATA[
function initInputFieldStyle() {
    document.theform.scProductId.className="input";
    document.theform.scNovatorId.className="input";
}

// This is an object that validates the form fields when the Search button is pressed.
function FormValidatorSearch() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   // Do nothing for now.  Functionality may be needed later.
}

// This is an object that validates the form fields when the Edit button is pressed.
function FormValidatorLoadProduct() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   initInputFieldStyle();
   
   var msg = "";
   if (document.theform.scProductId.value == "" && document.theform.scNovatorId.value == "") {
       msg = msg + "Please enter a Product ID or Novator ID.\n";
       if (document.theform.scProductId.tabIndex < this.minTabIndex) {
        this.minTabIndex = document.theform.scProductId.tabIndex;
        this.focusOnField = document.theform.scProductId.name;
       }
       document.theform.scProductId.className="ErrorField";
   }
   
   this.errorMsg = msg;
}

// This is an object that validates the form fields when the Line Number GO button is pressed.
function FormValidatorJump() {
   this.focusOnField = "";
   this.errorMsg = "";
   this.minTabIndex = 99999;
   
   document.theform.recordJumpIndex.className="input";
   
   var msg = "";
   if (isNaN(document.theform.recordJumpIndex.value) || !document.theform.recordJumpIndex.value) {
       msg = msg + "Please enter a Line value that is numeric.\n";
        this.minTabIndex = document.theform.recordJumpIndex.tabIndex;
        this.focusOnField = document.theform.recordJumpIndex.name;
       document.theform.recordJumpIndex.className="ErrorField";
   } else if (!document.theform.elements['scProductId' + document.theform.recordJumpIndex.value]) {
   msg = msg + "Please enter a valid LINE number that is shown on-screen.\n";
   this.minTabIndex = document.theform.recordJumpIndex.tabIndex;
        this.focusOnField = document.theform.recordJumpIndex.name;
       document.theform.recordJumpIndex.className="ErrorField";
   }
   
   this.errorMsg = msg;
}



function doBackAction() {
   history.back();
}

function doExit() {
   document.theform.action = "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
}

function setUp() {
    init();
    // If only a single record is shown on-screen, immediately jump to it.
    if (document.theform.recordCountTotal.value == "1" && document.theform.referrerPage.value != "ProductFeedMaint") {
     
     // Clear the search fields for this page, so on re-entry the search criteria
     // is not shown.
     document.theform.requestProductId.value = document.theform.scProductId.value; 
     document.theform.recordCountTotal.value = "-1";
     
     document.theform.recordJumpIndex.value = "1";
     doRecordJump("1");
    }
}
function init() {
   scroll(0,0);
   document.theform.hideSubmit.style.display = 'none';
   document.theform.scProductId.focus();
   if (document.theform.recordJumpIndex && document.all.searchContent.style.display != "none") {
   document.theform.recordJumpIndex.focus();
   }
}

function doClear() {
  clearAllFields(theform);
  initInputFieldStyle();
  document.theform.scRecordNumber.value = "1";
  document.theform.scRecordSortBy.value = "";
  document.theform.scActionType.value = "";
  if (document.all.searchContent) {
  document.all.searchContent.style.display = "none";
  }
  init();
}

function postProductFeed() {
   if (confirm("Are you sure you want to generate the product feed file?")) {
      document.theform.action_type.value = "";
      document.theform.scActionType.value = "generateFile";
     document.theform.recordCountTotal.value = "-1";
     submitInfoAction("productFeedSearch.do", "Generating product feed file");
   }
}

function doEdit(targetProductCode) {
   // Set a hidden field.
   document.theform.requestProductId.value = targetProductCode;
   document.theform.action_type.value = "load";
   submitInfoAction("productFeedMaint.do", "Retrieving Record");
}

function isPrintButtonClicked() {
   // Verify if the Enter key was pressed.
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
	  return false;
    
   printIt();
}

function isSearchButtonClicked() {
   // Verify if the Enter key was pressed.
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
	  return false;
    
   // doLoadProduct();
   doAdHocSearch('1', 'productId');
}

function isGoClicked() {
   // Verify if the Enter key was pressed.
   if(window.event)
     if(window.event.keyCode)
       if(window.event.keyCode != 13)
	  return false;
    
   doRecordJump(document.theform.recordJumpIndex.value);
}
// Given a search results line number, edits the corresponding product ID.
function doRecordJump(recordLineNumber) {
     
   // Validate the input.
   var formValidator = new FormValidatorJump();
   if (formValidator.errorMsg != "") {
    alert(formValidator.errorMsg);
    document.theform[formValidator.focusOnField].focus();
    return;
   }
   var cleanLine = stripZeros(recordLineNumber);
   var targetProductCode = document.getElementById("scProductId" + cleanLine).value;
   doEdit(targetProductCode);
}

function doSortedSearch(recordNumber, recordSortBy) {
   // Set a hidden field.
   document.theform.scRecordSortBy.value = recordSortBy;
   doSearch(recordNumber);
}

function doAdHocSearch(recordNumber, recordSortBy) {
   if (document.theform.scProductId.value == "" && document.theform.scNovatorId.value == "" && !confirm("This operation will return a large result set of data.\nAre you sure you want to view all products?")) {
      return;
   }
   // document.theform.scProductId.value = "";
      // document.theform.scNovatorId.value = "";
      // Set a hidden field.
   // document.theform.scActiveFlag.value = "Y";
   document.theform.scActionType.value = "search";
   document.theform.action_type.value = "";
   document.theform.scRecordSortBy.value = recordSortBy;
   doSearch(recordNumber);
}

function doSearch(recordNumber) {
   var formValidator = new FormValidatorSearch();
   if (formValidator.errorMsg != "") {
     alert(formValidator.errorMsg);
     document.theform[formValidator.focusOnField].focus();
   } else {
   document.theform.scRecordNumber.value = recordNumber;
   submitInfoAction("productFeedSearch.do", "Searching");
   }
}

function doSearchPageNext() {
  // Note that the multiplication with 1 forces the vars to numbers, rather than strings.
  doSearch(parseInt(document.theform.scRecordNumber.value)  + parseInt(document.theform.scPageMaxRecordCount.value));
}

function doSearchPagePrevious() {
  doSearch(document.theform.scRecordNumber.value - document.theform.scPageMaxRecordCount.value);
}

function doSearchPageLast() {
  doSearch((document.theform.pageCountTotal.value - 1) * document.theform.scPageMaxRecordCount.value + 1);
}

function submitInfoAction(url, visualMessage) {
   scroll(0,0);
   showWaitMessage("content", "wait", visualMessage);
   submitAction(url);
}

function submitAction(url) {
   document.theform.action = url;
   document.theform.target = "";
   document.theform.submit();
}

function printIt(){
	window.print(); 
}
]]>
</script>
</head>
<body onLoad="setUp()">
<xsl:choose>
<xsl:when test="$recordCountTotal &gt; '0'">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Product Feed Maintenance - Search'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="true()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>
</xsl:when>
<xsl:otherwise>
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Product Feed Maintenance - Search'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>
</xsl:otherwise>
</xsl:choose>
<form name="theform" method="post" action="">
        <xsl:call-template name="securityanddata"></xsl:call-template>
        <input type="hidden" name="scActionType" value="{key('pagedata','scActionType')/@value}"/>
        <input type="hidden" name="referrerPage" value="{key('pagedata','invokingPage')/@value}"/>
       <!-- <input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>-->
       <input type="hidden" name="action_type" value=""/>
        <input type="hidden" name="pageNumber" value="{$pageNumber}"/>
        <input type="hidden" name="pageCountTotal" value="{$pageCountTotal}"/>
        <input type="hidden" name="recordCountTotal" value="{$recordCountTotal}"/>
        <input type="hidden" name="scRecordNumber" value="{$scRecordNumber}"/>
        <input type="hidden" name="scPageMaxRecordCount" value="{$scPageMaxRecordCount}"/>
        <input type="hidden" name="scRecordSortBy" value="{key('pagedata','scRecordSortBy')/@value}"/>
        <input type="hidden" name="requestProductId" value=""/>
        <!-- Currently, any search will return only active feed products -->
        <!--<input type="hidden" name="scActiveFlag" value="{key('pagedata','scActiveFlag')/@value}"/>-->
        <input type="submit" name="hideSubmit" value="Rendering...."/>
<div id="content" style="display:block">		 
		   <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center">
			   <tr>
         <xsl:choose>
         <xsl:when test="$recordCountTotal = '0'">
				   <td class="errorMessage">
						NO RECORDS FOUND. PLEASE ENSURE THAT YOU ENTER THE PRODUCT ID, NOT SUBCODE ID.
				   </td>
          </xsl:when>
          <xsl:when test="$recordCountTotal = '-100'">
				   <td class="OperationSuccess">
						PRODUCT FEED FILE SUCCESSFULLY GENERATED.
				   </td>
          </xsl:when>
          <xsl:when test="$recordCountTotal = '-20'">
				   <td class="errorMessage">
						PRODUCT FEED FILE GENERATION FAILED.  PLEASE CONTACT THE APPLICATION ADMINISTRATOR FOR MANUAL INTERVENTION.
				   </td>
          </xsl:when>
          </xsl:choose>
			   </tr>
		   </table>
        <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
          <tr>
            <td><table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td width="16%"></td>
                  <td width="16%" class="label" align="right">Product ID:</td>
                  <td width="16%" align="left"><input type="text" size="15" maxlength="10" name="scProductId" tabindex="1" selected="true" value="{key('pagedata','scProductId')/@value}" onChange="javascript:setValueToUpperCase('theform','scProductId')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" onKeyPress="javascript:isSearchButtonClicked()"/></td>
                  <td width="16%" class="label" align="right">Novator ID:</td>
                  <td width="20%" align="left"><input type="text" size="15" maxlength="10" name="scNovatorId" tabindex="1" selected="true" value="{key('pagedata','scNovatorId')/@value}" onChange="javascript:setValueToUpperCase('theform','scNovatorId')" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" onKeyPress="javascript:isSearchButtonClicked()"/>
<!--&nbsp;
&nbsp;
&nbsp;
&nbsp;
                  <button class="blueButton" onClick="javascript:doLoadProduct()" tabindex="2" accesskey="e">
              (E)dit
            </button>-->
            </td>
            <td width="16%"></td>
                </tr>
              </table></td>
          </tr>
        </table>
        <xsl:call-template name="productFeedSearchOptions"><xsl:with-param name="startTabIndex" select="50" /><xsl:with-param name="recordCountTotal" select="$recordCountTotal" /></xsl:call-template>
        <!-- Only show the results pane if results exist -->
        <xsl:if test="$recordCountTotal &gt; '0'">
          <div id="searchContent" style="display:block">
           <div class="header">
           Search Results
           </div>
           <div align="center">
           <label class="label" for="recordJumpIndex" id="recordJumpIndexLabel">Enter Line#:
           <input type="text" size="5" name="recordJumpIndex" tabindex="60" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()" onKeyPress="javascript:isGoClicked()"/>
           </label>
           <button id="goButton" class="blueButton" onClick="javascript:doRecordJump(document.theform.recordJumpIndex.value)" tabindex="61" accesskey="g">
              (G)o
            </button>
           </div>
            <!--  Main Table thick blue border -->
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="70" /></xsl:call-template>
            <table class="mainTable" align="center" width="98%" cellpadding="0" cellspacing="1">
              <tr>
                <td><table class="innerTable" cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                      <td colspan="2"><div class="tableContainer" id="messageContainer" >
                          <table width="100%" class="scrollTable" border="0" cellpadding="0"  align="center" cellspacing="2">
                            <thead class="fixedHeader">
                              <tr style="text-align:left">
                                <td  class="ColHeaderLink" width="3%"/>
                                <td  class="ColHeaderLink" width="13%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'productId');" class="textlink" tabindex="80">
                                    PRODUCT ID
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="13%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'novatorId');" class="textlink" tabindex="80">
                                    NOVATOR ID
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="36%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'productName');" class="textlink" tabindex="81">
                                    NAME
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="12%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'price');" class="textlink" tabindex="82">
                                    PRICE
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="12%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'status');" class="textlink" tabindex="83">
                                    AVAILABLE
                                  </a>
                                </td>
                                <td  class="ColHeaderLink" width="11%">&nbsp;
                                  <a href="javascript:doSortedSearch('1', 'importantProductFlag');" class="textlink" tabindex="83">
                                    IMPORTANT
                                  </a>
                                </td>
                              </tr>
                            </thead>
                            <tbody class="scrollContent" >
                              <xsl:for-each select="root/productList/product">
                                <tr id="row">
                                <!-- Alternate background line shading -->
                              	<xsl:choose>
		                             <xsl:when test="(position() mod 2) = 0">
                                    <xsl:attribute name="style">vertical-align:text-top;background:#ffffff</xsl:attribute>
                                 </xsl:when>
		                             <xsl:otherwise>
                                   <xsl:attribute name="style">vertical-align:text-top;background:#f4f4f4</xsl:attribute>
                                 </xsl:otherwise>
                                </xsl:choose>

                                  <td style="text-align:left;"><script type="text/javascript">
													 	<![CDATA[
													 	  document.write(rowNumber++ + ".");

											 			]]>
</script>
                                  </td>
                                  <td id="td_{@num}" style="text-align:left;"><a href="javascript:doEdit('{@product_id}');" class="textlink">
                                      <xsl:attribute name="tabindex"><xsl:value-of select="@num + 101"/></xsl:attribute>
                                      &nbsp;<xsl:value-of select="@product_id"/>
                                    </a>
                                    <input type="hidden" id="scProductId{@rnum}" value="{@product_id}"/>
                                  </td>
                                  <td style="text-align:left;"><xsl:value-of select="@novator_id"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@product_name"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@standard_price"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@status"/></td>
                                  <td style="text-align:left;"><xsl:value-of select="@important_product_flag"/></td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div></td>
                    </tr>
                    <tr>
                      <td class="Label" width="50%" >
                      </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
            <xsl:call-template name="pagination"><xsl:with-param name="startTabIndex" select="400" /></xsl:call-template>
        <xsl:call-template name="productFeedSearchOptions"><xsl:with-param name="startTabIndex" select="450" /><xsl:with-param name="recordCountTotal" select="$recordCountTotal" /></xsl:call-template>
        <table width="99%" border="0" cellpadding="0" cellspacing="0">
          <tr><td align="right">
            <button class="blueButton" onclick="javascript:scroll(0,0)" tabindex="500" accesskey="t">(T)op of page</button>
          </td></tr>
        </table>
        <!-- end of the entire search results div -->
        </div>
        </xsl:if>
        <!-- end of the entire content div -->
        </div>
      </form>
<div id="waitDiv" style="display:none">
		   <table id="waitTable" class="mainTable" width="98%" height="300px" align="center" cellpadding="0" cellspacing="2" >
		      <tr>
		        <td width="100%">
              <table class="innerTable" width="100%" height="300px" cellpadding="0" cellspacing="2">
               <tr>
                  <td>
                     <table width="100%" cellspacing="0" cellpadding="0" border="0">
                         <tr>
                           <td id="waitMessage" align="right" width="50%" class="WaitMessage"></td>
                           <td id="waitTD"  width="50%" class="WaitMessage"></td>
                         </tr>
                     </table>
                   </td>
                </tr>
              </table>
		        </td>
		      </tr>
	      </table>
		   </div>

<xsl:call-template name="footer"></xsl:call-template>

</body></html>
  </xsl:template>
  </xsl:stylesheet>
  