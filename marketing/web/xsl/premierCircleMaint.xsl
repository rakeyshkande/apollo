<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY space "&#160;">
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="securityanddata.xsl"/>
<xsl:key name="pagedata" match="root/pageData/data" use="@name" />
<xsl:variable name="campURL" select="root/pageData/data[@name='campURL']/@value"/>
<xsl:template match="/">
<html>
<head>
<title>
        Premier Circle Membership Maintenance
</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<!-- <style type="text/css">
#sourceTypeListAllDiv {display: block;}
#sourceTypeListTempDiv {display: none;}
select.fixedWidthSelect { width:300px } 
</style> -->

<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">
var siteName = "<xsl:value-of select="$site_name"/>";
function init() {
   setNavigationHandlers();
}
	
function doExit() {
   document.theform.action = "http://" + siteName + "mainMenu.do";
   document.theform.target = "_top";
   document.theform.submit();
}

</script>
</head>
<body OnLoad="javascript:init()">
<xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Premier Circle Membership - Maintenance'" />
        <xsl:with-param name="showTime" select="true()"/>
        <xsl:with-param name="showBackButton" select="false()"/>
        <xsl:with-param name="showPrinter" select="false()"/>
        <xsl:with-param name="showSearchBox" select="false()"/>
      </xsl:call-template>
<form name="theform" method="post" action="">
        <input type="hidden" name="action_type" value="{key('pagedata','action_type')/value}"/>
        <input type="hidden" name="result" value="{key('pagedata','result')/value}"/>
        <input type="hidden" name="camp_URL" id="campURL" value="{key('pagedata','campURL')/@value}"/>
        <xsl:call-template name="securityanddata"></xsl:call-template>
        <!-- <div id="content" style="display:block">
		   <div id="resultMsgDiv">	   
       		</div>
        </div> -->
        <table width="98%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
        	<td>
        	<table width="100%">
				<xsl:if test="key('pagedata','campURL')/@value"> 
					<iframe src="{key('pagedata','campURL')/@value}" width="100%" height="600" scrolling="auto"></iframe>					
				 </xsl:if>		        	
                </table>
        	</td>
        </tr>
    </table>
        
      </form>
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="right"><button class="blueButton" onClick="javascript:doExit()" tabindex="53">
              Exit
            </button></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
<xsl:call-template name="footer"></xsl:call-template>
</body>
</html>  
</xsl:template>
  </xsl:stylesheet>