<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY space "&#160;">
    <!ENTITY newline "&#38;#x0D;&#38;#x0A;" >
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />

   <!-- The source record metrics are placed here for easy maintenance -->
   <xsl:variable name="sourceDefaultOrderInfoVO" select="root/sourceDefaultOrderInfoVO" />

   <!-- Root template -->
   <xsl:template match="/">

<html>
<head>
<base target="_self" />
<title>Prepopulate Delivery and Billing Information</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/FormChek.js"></script>
<script type="text/javascript" src="js/sourceCodeDefaultOrderInfoMaint.js"></script>

<!-- Convert XSL data to javascript -->
<xsl:call-template name="xmlToJavascriptObjectArray">
  <xsl:with-param name="varName">v_states</xsl:with-param>
  <xsl:with-param name="itemPath" select="root/stateList/state" />
  <xsl:with-param name="format">attribute</xsl:with-param>
</xsl:call-template>

<xsl:call-template name="xmlToJavascriptObjectArray">
  <xsl:with-param name="varName">v_countries</xsl:with-param>
  <xsl:with-param name="itemPath" select="root/countryList/country" />
  <xsl:with-param name="format">attribute</xsl:with-param>
</xsl:call-template>

<xsl:call-template name="xmlToJavascriptObjectArray">
  <xsl:with-param name="varName">v_addressTypes</xsl:with-param>
  <xsl:with-param name="itemPath" select="root/addressTypeList/addressType" />
  <xsl:with-param name="format">element</xsl:with-param>
</xsl:call-template>

<script type="text/javascript" language="JavaScript">
  v_isAfterSubmit = <xsl:value-of select="boolean(key('pagedata', 'SUBMIT_ACTION')/@value)" />;
</script>

</head>

<body onload="javascript:init();">
  <form name="theform" method="post" action="sourceCodeDefaultOrderInfoMaint.do">
  
  <div width='100%' class='Header' align='center'>Prepopulate Delivery and Billing Information</div>

  <input type="hidden" name="requestSourceCode" value="{$sourceDefaultOrderInfoVO/sourceCode}"/>
  <input type="hidden" name="action_type" value=""/>
  
  <xsl:call-template name="securityanddata"></xsl:call-template>

	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
      <td width="50%" valign='top'>
        <!-- Recipient Info Table -->
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td></td>
            <td class='HeaderLeft'>Recipient</td>
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientLocationTypeLabel" for="recipientLocationType">Location Type</label></td>
            <td>
              <select size="1" id="recipientLocationType" name="recipientLocationType">
                <option value=""></option>
                <xsl:for-each select="root/addressTypeList/addressType">
                  <option value="{address_type}"><xsl:if test="address_type = $sourceDefaultOrderInfoVO/recptLocationType"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="description"/></option> -->
                </xsl:for-each>
              </select>
            </td>
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientBusinessNameLabel" for="recipientBusinessName">Location Name</label></td>
            <td>
               <input type="text" id="recipientBusinessName" name="recipientBusinessName" size="45" maxlength="50" value="{$sourceDefaultOrderInfoVO/recptBusinessName}" />
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientLocationDetailLabel" for="recipientLocationDetail">Room</label></td>
            <td>
               <input type="text" id="recipientLocationDetail" name="recipientLocationDetail" size="20" maxlength="20" value="{$sourceDefaultOrderInfoVO/recptLocationDetail}" /> 
            </td>
          </tr>   
          <tr>
            <td class="labelRight">
              <label class="labelRight" id="recipientAddressLabel" for="recipientAddress">Address</label>
            </td>
            <td >
              <textarea cols="45" rows="2" id="recipientAddress" name="recipientAddress"><xsl:value-of select="$sourceDefaultOrderInfoVO/recptAddress"/></textarea>
            </td>          
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientZipCodeLabel" for="recipientZipCode">Zip Code</label></td>
            <td>
               <input type="text" id="recipientZipCode" name="recipientZipCode" size="6" maxlength="6" value="{$sourceDefaultOrderInfoVO/recptZipCode}"/> 
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientCityLabel" for="recipientCity">City</label></td>
            <td>
               <input type="text" id="recipientCity" name="recipientCity" size="30" maxlength="30" value="{$sourceDefaultOrderInfoVO/recptCity}"/>
            </td>
          </tr>            
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientStateLabel" for="recipientState">State</label></td>
            <td>
              <select size="1" id="recipientState" name="recipientState" >
                <option value=""></option>
                <!-- Populate with the entire list, javascript handler will trim -->
                <xsl:for-each select="root/stateList/state">
                  <option value="{@state_master_id}"><xsl:if test="@state_master_id = $sourceDefaultOrderInfoVO/recptStateId"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="@state_name"/></option>
                </xsl:for-each>              
              </select>
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientCountryLabel" for="recipientCountry">Country</label></td>
            <td>
              <select size="1" id="recipientCountry" name="recipientCountry" >
                <option value=""></option>
                <xsl:for-each select="root/countryList/country">
                  <option value="{@country_id}"><xsl:if test="@country_id = $sourceDefaultOrderInfoVO/recptCountryId"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="@name"/></option>
                </xsl:for-each>              
              </select>
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientPhoneLabel" for="recipientPhone">Phone</label></td>
            <td>
               <input type="text" id="recipientPhone" name="recipientPhone" size="20"  maxlength="20" value="{$sourceDefaultOrderInfoVO/recptPhone}"/>
            </td>
          </tr>            
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="recipientPhoneExtLabel" for="recipientPhoneExt">Ext</label></td>
            <td>
               <input type="text" id="recipientPhoneExt" name="recipientPhoneExt" size="10" maxlength="10" value="{$sourceDefaultOrderInfoVO/recptPhoneExt}"/> 
            </td>
          </tr>                
    

        </table>
        <!-- End Recipient Info Table -->
      </td>
      <td width="50%" valign='top'>
        <!-- Customer Info Table -->
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td></td>
            <td class='HeaderLeft'>Customer</td>
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerFirstNameLabel" for="customerFirstName">First Name</label></td>
            <td>
               <input type="text" id="customerFirstName" name="customerFirstName" size="25" maxlength="25" value="{$sourceDefaultOrderInfoVO/custFirstName}"/>
            </td>
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerLastNameLabel" for="customerLastName">Last Name</label></td>
            <td>
               <input type="text" id="customerLastName" name="customerLastName" size="25" maxlength="25" value="{$sourceDefaultOrderInfoVO/custLastName}"/>
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerDaytimePhoneLabel" for="customerDaytimePhone">Phone 1</label></td>
            <td>
               <input type="text" id="customerDaytimePhone" name="customerDaytimePhone" size="20"  maxlength="20" value="{$sourceDefaultOrderInfoVO/custDaytimePhone}"/>
            </td>
          </tr>            
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerDaytimePhoneExtLabel" for="customerDaytimePhoneExt">Phone 1 Ext</label></td>
            <td>
               <input type="text" id="customerDaytimePhoneExt" name="customerDaytimePhoneExt" size="10" maxlength="10" value="{$sourceDefaultOrderInfoVO/custDaytimePhoneExt}"/>
            </td>
          </tr>        
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerEveningPhoneLabel" for="customerEveningPhone">Phone 2</label></td>
            <td>
               <input type="text" id="customerEveningPhone" name="customerEveningPhone" size="20"  maxlength="20" value="{$sourceDefaultOrderInfoVO/custEveningPhone}"/>
            </td>
          </tr>            
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerEveningPhoneExtLabel" for="customerEveningPhoneExt">Phone 2 Ext</label></td>
            <td>
               <input type="text" id="customerEveningPhoneExt" name="customerEveningPhoneExt" size="10" maxlength="10" value="{$sourceDefaultOrderInfoVO/custEveningPhoneExt}"/>
            </td>
          </tr>             
          <tr>
            <td class="labelRight">
              <label class="labelRight" id="customerAddressLabel" for="customerAddress">Address</label>
            </td>
            <td >
              <textarea cols="45" rows="2" id="customerAddress" name="customerAddress"><xsl:value-of select="$sourceDefaultOrderInfoVO/custAddress"/></textarea>
            </td>          
          </tr>
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerZipCodeLabel" for="customerZipCode">Zip Code</label></td>
            <td>
               <input type="text" id="customerZipCode" name="customerZipCode" size="6" maxlength="6" value="{$sourceDefaultOrderInfoVO/custZipCode}"/> 
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerCityLabel" for="customerCity">City</label></td>
            <td>
               <input type="text" id="customerCity" name="customerCity" size="30" maxlength="30" value="{$sourceDefaultOrderInfoVO/custCity}"/>
            </td>
          </tr>            
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerStateLabel" for="customerState">State</label></td>
            <td>
              <select size="1" id="customerState" name="customerState" >
                <option value=""></option>
                <!-- Populate with the entire list, javascript handler will trim -->
                <xsl:for-each select="root/stateList/state">
                  <option value="{@state_master_id}"><xsl:if test="@state_master_id = $sourceDefaultOrderInfoVO/custStateId"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="@state_name"/></option>
                </xsl:for-each>              
              </select>              
            </td>
          </tr>          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerCountryLabel" for="customerCountry">Country</label></td>
            <td>
              <select size="1" id="customerCountry" name="customerCountry">
                <option value=""></option>
                <xsl:for-each select="root/countryList/country">
                  <option value="{@country_id}"><xsl:if test="@country_id = $sourceDefaultOrderInfoVO/custCountryId"><xsl:attribute name="selected"/></xsl:if><xsl:value-of select="@name"/></option>
                </xsl:for-each>
              </select>
            </td>
          </tr>       
          
          <tr> 
            <td class="labelRight">
              <label class="labelRight" id="customerEmailAddressLabel" for="customerEmailAddress">Email</label></td>
            <td>
               <input type="text" id="customerEmailAddress" name="customerEmailAddress" size="40" maxlength="40"  value="{$sourceDefaultOrderInfoVO/custEmailAddress}"/>
            </td>
          </tr>           

        </table>
         <!-- End Customer Info Table -->
      </td>
		</tr>
	</table>
  
  <!-- Control/Buttons Table -->
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<xsl:choose>
				<xsl:when test="$sourceDefaultOrderInfoVO/requestedBy = 'WEBSITE PORTAL'">
					<td align='left'><button class="blueButton" disabled="true" onClick="javascript:doSave()">Save</button></td>
				</xsl:when>
				<xsl:otherwise>
					<td align='left'><button class="blueButton" onClick="javascript:doSave()">Save</button></td>
				</xsl:otherwise>
			</xsl:choose>
			<td align='center'><button class="blueButton" onClick="javascript:doClearAllFields()">Clear All Fields</button></td>
			<td align='right'><button class="blueButton" onClick="javascript:doClose()">Close</button></td>
		</tr>	
	</table>
  
  
  </form>
</body>
</html>

   </xsl:template>

<!-- Helper template converts a  xml node to a javascript array of objects 
The field of objects can be obtained from the Node attributes or the Node children of the object -->    
<xsl:template name="xmlToJavascriptObjectArray">
    <xsl:param name="varName" />
    <xsl:param name="itemPath" />
    <xsl:param name="format">attribute</xsl:param>
    
    <xsl:text>&newline;</xsl:text>
    <script type="text/javascript" language="javascript">
    <xsl:text>&newline;</xsl:text>
    <xsl:value-of select="$varName" /><xsl:text> = new Array(); &newline;</xsl:text>       
    
    <xsl:choose >
      <xsl:when test="$format = 'attribute'" >
        <xsl:for-each select="$itemPath">
          <xsl:value-of select="$varName" /><xsl:text>.push({</xsl:text>       
          <xsl:for-each select="@*">
            <xsl:value-of select="name()"/><xsl:text>:'</xsl:text><xsl:value-of select="."/><xsl:text>'</xsl:text>
            <xsl:if test="position()!=last()">
              <xsl:text>, </xsl:text>
            </xsl:if>
          </xsl:for-each>
          <xsl:text>}); &newline;</xsl:text>        
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="$itemPath">
          <xsl:value-of select="$varName" /><xsl:text>.push({</xsl:text>       
          <xsl:for-each select="./*">
            <xsl:value-of select="name()"/><xsl:text>:'</xsl:text><xsl:value-of select="."/><xsl:text>'</xsl:text>
            <xsl:if test="position()!=last()">
              <xsl:text>, </xsl:text>
            </xsl:if>
          </xsl:for-each>
          <xsl:text>}); &newline;</xsl:text>        
        </xsl:for-each>      
      </xsl:otherwise>
    </xsl:choose>

    </script>
    
</xsl:template>

</xsl:stylesheet>