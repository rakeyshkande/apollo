<?xml version='1.0' encoding='windows-1252'?>
<!DOCTYPE FTD[
    <!ENTITY nbsp "&#160;">
    <!ENTITY space "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="header.xsl"/>
   <xsl:import href="footer.xsl"/>
   <xsl:import href="securityanddata.xsl"/>
   <xsl:key name="pagedata" match="root/pageData/data" use="@name" />
   
   <!-- Retrieve Promotion values -->
   <xsl:variable name="updateAllowed" select="root/pageData/data[@name='PricingCodeData']/@value" />
   <xsl:variable name="pricingCode" select="root/pageData/data[@name='pricingCode']/@value" />
   <xsl:variable name="description" select="root/priceHeaderDetailList/priceHeaderDetail/@description" />
   <xsl:variable name="discountType" select="root/priceHeaderDetailList/priceHeaderDetail/@discount_type" />
      
   <!-- Root template -->
   <xsl:template match="/">
   
<html>
<head>
<title>Price Code Maintenance</title>
<META http-equiv="Content-Type" content="text/html"/>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" language="JavaScript">

<![CDATA[

var rowNumber = 0;
var maxAmount = 9999.99
var thisField = 0;
var maxFields = 10;
var lastValue = new Array(maxFields);

var toArray = new Array(maxFields);
for (x=0; x<maxFields; x++) {
	toArray[x] = 'to' + (x+1);
}

var fromArray = new Array(maxFields);
for (x=0; x<maxFields; x++) {
  fromArray[x] = 'from' + (x+1);
}

var discountArray = new Array(maxFields);
for (x=0; x<maxFields; x++) {
  discountArray[x] = 'discount' + (x+1);
}
]]>

function doBackAction() {
  <xsl:if test="key('pagedata','action_type')/@value != 'search'">
    document.theform.action_type.value = "search";
    document.theform.pricingCode.disabled = false;
    submitAction("priceHeaderMaint.do");
    return;
  </xsl:if>
  document.theform.action = "mainMenu.do";
  document.theform.submit();
}

function doExit() {
  if (madeChanges) {
    if (!confirm("Are you sure you want to exit without saving?")) {
      return;
    }
  }
   document.theform.action = "mainMenu.do";
   document.theform.submit();
}

function init() {
  <xsl:choose>
    <xsl:when test="key('pagedata','action_type')/@value = 'search'">
      document.theform.pricingCode.focus();
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="$updateAllowed = 'Y'">
        document.theform.description.focus();
      </xsl:if>
      if (document.theform.from1.value == '') {
        document.theform.from1.value = '0';
      }
      setFields();
    </xsl:otherwise>
  </xsl:choose>
<![CDATA[
  madeChanges = false;
}

function setFields() {
  rowNumber = 0;
	for (y=0; y<document.theform.elements.length; y++) {
    for (x=0; x<maxFields; x++) {
      if (document.theform.elements[y].name == toArray[x]) {
        lastValue[x] = document.theform.elements[y].value;
        if (document.theform.elements[y].value == maxAmount) {
          rowNumber = x+1;
          clearFields(x+1);
          return;
        }
      }
    }
  }
}

function decide(obj) {
  fieldBlur();
	for (y=0; y<maxFields; y++) {
		if (toArray[y] == obj.name) {
			thisField = y;
		}
	}
	amount = parseFloat(obj.value);
	if (amount == lastValue[thisField]) {
		return;
	}
	if (amount > maxAmount) {
		alert('Amount cannot be greater than ' + addCommas(maxAmount));
		setFocus(thisField);
		return;
	}
	if (obj.value == '') {
		//alert('Value required');
		//setFocus(thisField);
		//return;
	} else {
	if (amount == maxAmount) {
		clearFields(thisField+1);
    rowNumber = thisField + 1;
	} else {
		newAmt = amount + .01;
		setNextField(newAmt);
    if (thisField+2 > rowNumber)
      rowNumber = thisField + 2;
	}
	lastValue[thisField] = amount;
  }
}

function clearFields(startVal) {
	for (x=startVal; x<maxFields; x++) {
		for (y=0; y<document.theform.elements.length; y++) {
			if (document.theform.elements[y].name == fromArray[x]) {
				document.theform.elements[y].value = '';
			}
			if (document.theform.elements[y].name == toArray[x] ||
			    document.theform.elements[y].name == discountArray[x] ) {
				document.theform.elements[y].value = '';
				document.theform.elements[y].disabled = true;
			}
		}
	}
}

function setNextField(newAmt) {
	for (y=0; y<document.theform.elements.length; y++) {
		if (document.theform.elements[y].name == fromArray[thisField+1]) {
			document.theform.elements[y].value = newAmt.toFixed(2);
		}
		if (document.theform.elements[y].name == toArray[thisField+1] ||
			    document.theform.elements[y].name == discountArray[thisField+1] ) {
			document.theform.elements[y].disabled = false;
			//document.theform.elements[y].focus();
		}
	}
}

function setFocus(fieldVal) {
	for (y=0; y<document.theform.elements.length; y++) {
		if (document.theform.elements[y].name == toArray[fieldVal]) {
			document.theform.elements[y].focus();
		}
	}
}

function doGet() {
  document.theform.action_type.value = "edit";
  submitAction("priceHeaderMaint.do");
}

function doAdd() {
  newCode = prompt('Enter new Pricing Code: ','');
  if (newCode == null) {
    return;
  }
  if (newCode == '')
  {
    alert("Entry is required");
    return;
  }
  if (newCode.length > 2)
  {
    alert("Pricing Code cannot be more than 2 characters");
    return;
  }
  document.theform.action_type.value = "insert";
  document.theform.pricingCode.options.length = 1;
  document.theform.pricingCode.options[0].value = newCode.toUpperCase();
  document.theform.pricingCode.options[0].selected = true;
  submitAction("priceHeaderMaint.do");
}

function submitAction(url) {
  document.theform.action = url;
  document.theform.target = "";
  document.theform.submit();
}

function doUpdate() {
  document.theform.description.className="input";
  if (document.theform.description.value == '') {
    document.theform.description.className="ErrorField";
    alert("Description is required");
    document.theform.description.focus();
    return;
  }
  lastTo = "";
  lastToRow = 0;
  msg = "";
	for (y=0; y<document.theform.elements.length; y++) {
		if (document.theform.elements[y].name == toArray[rowNumber-1]) {
			lastTo = document.theform.elements[y].value;
      lastToRow = y;
		}
    if (document.theform.elements[y].name.substr(0,2) == "to") {
      document.theform.elements[y].className = "input";
    }
    if (document.theform.elements[y].name.substr(0,8) == "discount") {
      document.theform.elements[y].className = "input";
    }
    for (z=0; z<rowNumber; z++) {
      if (document.theform.elements[y].name == toArray[z]) {
        if (document.theform.elements[y].value == "") {
          if (msg == "") document.theform.elements[y].focus();
          msg = msg + document.theform.elements[y].name + " required.\n";
          document.theform.elements[y].className = "ErrorField";
        }
        //msg = msg + z + " " + lastValue[z] + "\n";
//        if (z > 0 && (document.theform.elements[y].value <= lastValue[z-1])) {
//          if (msg == "") document.theform.elements[y].focus();
//          msg = msg + document.theform.elements[y].name + " must be greater than " + lastValue[z-1] + "\n";
//          document.theform.elements[y].className = "ErrorField";
//        }
        if ((z < rowNumber-1) && (document.theform.elements[y].value > lastValue[z+1])) {
          if (msg == "") document.theform.elements[y].focus();
          msg = msg + document.theform.elements[y].name + " must be less than " + lastValue[z+1] + "\n";
          document.theform.elements[y].className = "ErrorField";
        }
      }
      if (document.theform.elements[y].name == discountArray[z]) {
        if (document.theform.elements[y].value == "") {
          if (msg == "") document.theform.elements[y].focus();
          msg = msg + document.theform.elements[y].name + " required.\n";
          document.theform.elements[y].className = "ErrorField";
        }
        if (document.theform.discountType.value == 'P' && document.theform.elements[y].value > 100) {
          if (msg == "") document.theform.elements[y].focus();
          msg = msg + document.theform.elements[y].name + " cannot be greater than 100%.\n";
          document.theform.elements[y].className = "ErrorField";
        }
        if (document.theform.discountType.value == 'D' && document.theform.elements[y].value > maxAmount) {
          if (msg == "") document.theform.elements[y].focus();
          msg = msg + document.theform.elements[y].name + " cannot be greater than " + addCommas(maxAmount) + ".\n";
          document.theform.elements[y].className = "ErrorField";
        }
      }
    }
	}
  if (msg != "" ) {
    alert(msg);
    return;
  }
  if (lastTo != maxAmount) {
    alert("Final To Amount must be " + addCommas(maxAmount));
    document.theform.elements[lastToRow].className="ErrorField";
    document.theform.elements[lastToRow].focus();
    return;
  }
	for (y=0; y<document.theform.elements.length; y++) {
		if (document.theform.elements[y].name.substr(0,4) == "from") {
      document.theform.elements[y].disabled = false;
    }
  }
  document.theform.pricingCode.disabled = false;
  document.theform.rowCount.disabled = false;
  document.theform.rowCount.value = rowNumber;
  document.theform.action_type.value = "update";
  submitAction("priceHeaderMaint.do");
}

function doClear() {
  document.theform.reset();
  if (document.theform.from1.value == '') {
    document.theform.from1.value = '0';
  }
  setFields();
	for (y=0; y<document.theform.elements.length; y++) {
    for (x=0; x<maxFields; x++) {
      if (document.theform.elements[y].name == toArray[x]) {
        document.theform.elements[y].className = "input";
      }
      if (document.theform.elements[y].name == discountArray[x]) {
        document.theform.elements[y].className = "input";
      }
    }
  }
}

function markChanged() {
  madeChanges = true;
}

/*
 * Adds comma formatting to a number
 */
function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

]]>
</script>
</head>

<body onLoad="init()">

<xsl:call-template name="header">
	<xsl:with-param name="headerName" select="'Price Code Maintenance'" />
	<xsl:with-param name="showTime" select="true()"/>
	<xsl:with-param name="showBackButton" select="true()"/>
	<xsl:with-param name="showPrinter" select="false()"/>
	<xsl:with-param name="showSearchBox" select="false()"/>
</xsl:call-template>

<form name="theform" method="post" action="">

<xsl:call-template name="securityanddata"></xsl:call-template>
<input type="hidden" name="action_type" value="{key('pagedata','action_type')/@value}"/>
<input type="hidden" name="rowCount" value=""/>

<xsl:if test="key('pagedata','result')/@value = '1'">
  <font class="OperationSuccess">Pricing Code <xsl:value-of select="key('pagedata','pricingCode')/@value" />&nbsp;saved</font>
  <br/>
</xsl:if>

<table width="800" border="3" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
<tr><td>

<table width="800" border="0" cellpadding="2" cellspacing="0">
  <tr>
  	<td width="150">&nbsp;</td>
	  <td width="650">&nbsp;</td>
  </tr>
  <tr>
    <td class="label" align="right">Pricing Code:&nbsp;</td>
    <td align="left" colspan="4">
      <xsl:choose>
        <xsl:when test="key('pagedata','action_type')/@value = 'search'">
          <select name="pricingCode" tabindex="1" style="width: 200px">
            <xsl:for-each select="root/pricingCodeList/pricingCode">
              <option value="{@price_header_id}">
                <xsl:value-of select="@price_header_id"/>
                &nbsp;-&nbsp;
                <xsl:value-of select="@description"/>
              </option>
            </xsl:for-each>
          </select>
        </xsl:when>
        <xsl:otherwise>
          <input type="text" name="pricingCode" value="{$pricingCode}" disabled="true"/>
        </xsl:otherwise>
      </xsl:choose>
    </td>
  </tr>
  <xsl:choose>
    <xsl:when test="key('pagedata','action_type')/@value = 'search'">
      <tr>
        <td>&nbsp;</td>
        <td>
          <button class="blueButton" onClick="javascript:doGet()" accesskey="r" tabindex="3">(R)etrieve Data</button>
          &nbsp;&nbsp;
          <xsl:if test="key('pagedata','PricingCodeData')/@value = 'Y'">
            <button class="blueButton" onClick="javascript:doAdd()" accesskey="a" tabindex="4">(A)dd New</button>
          </xsl:if>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
    </xsl:when>
    <xsl:otherwise>
      <tr>
        <td class="label" align="right">Description:&nbsp;</td>
        <td>
          <input type="text" name="description" value="{$description}" size="50" maxlength="40" tabindex="2" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
            <xsl:if test="not($updateAllowed = 'Y')">
              <xsl:attribute name="disabled">
              true
              </xsl:attribute>
            </xsl:if>
          </input>
        </td>
      </tr>
      <tr>
        <td class="label" align="right">Discount Type:&nbsp;</td>
        <td>
          <select name="discountType" tabindex="3" onChange="markChanged()">
            <xsl:if test="not($updateAllowed = 'Y')">
              <xsl:attribute name="disabled">
              true
              </xsl:attribute>
            </xsl:if>
            <xsl:for-each select="root/discountTypeList/discountType">
              <option value="{@price_discount_type}">
              <xsl:if test="$discountType = @price_discount_type or (not($discountType) and @price_discount_type = 'D')">
                <xsl:attribute name="selected">true</xsl:attribute>
              </xsl:if>
              <xsl:value-of select="@description"/>
              </option>
            </xsl:for-each>
          </select>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
    </xsl:otherwise>
  </xsl:choose>
</table>

<xsl:if test="key('pagedata','action_type')/@value != 'search'">
  <table width="800" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <th width="50" class="colHeader">&nbsp;</th>
    <th width="150" align="left" class="colHeader">&nbsp;From</th>
    <th width="150" align="left" class="colHeader">&nbsp;To</th>
	  <th width="450" align="left" class="colHeader">Discount</th>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <xsl:for-each select="root/priceHeaderDetailList/priceHeaderDetail">
    <tr>
      <td>&nbsp;</td>
      <td>
        <input type="text" name="from{@num}" value="{@min_dollar_amt}" size="10" maxlength="10" disabled="true"/>
      </td>
      <td>
        <input type="text" name="to{@num}" value="{@max_dollar_amt}" size="10" maxlength="10" onChange="markChanged()" onBlur="decide(this)" onFocus="javascript:fieldFocus()">
        <xsl:attribute name="tabindex"><xsl:value-of select="@num * 3 + 1"/></xsl:attribute>
        <xsl:if test="not($updateAllowed = 'Y')">
          <xsl:attribute name="disabled">
          true
          </xsl:attribute>
        </xsl:if>
        </input>
      </td>
      <td>
        <input type="text" name="discount{@num}" value="{@discount_amt}" size="10" maxlength="10" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()">
        <xsl:attribute name="tabindex"><xsl:value-of select="@num * 3 + 2"/></xsl:attribute>
        <xsl:if test="not($updateAllowed = 'Y')">
          <xsl:attribute name="disabled">
          true
          </xsl:attribute>
        </xsl:if>
        </input>
      </td>
<script type="text/javascript">
<![CDATA[
  rowNumber++;
]]></script>
    </tr>
  </xsl:for-each>

<script type="text/javascript">
  for (tempRow = rowNumber+1; tempRow &lt; maxFields+1; tempRow++)
  {
    document.write("<tr>");
    document.write("<td></td>");
    document.write("<td>");
    document.write('<input type="text" name="from' + tempRow + '" size="10" maxlength="10" disabled="true"/>');
    document.write("</td>");
    document.write("<td>");
    document.write('<input type="text" name="to' + tempRow + '" size="10" maxlength="10" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="decide(this)" tabindex="' + tempRow * 3 + '"/>');
    document.write("</td>");
    document.write("<td>");
    document.write('<input type="text" name="discount' + tempRow + '" size="10" maxlength="10" tabindex="' + tempRow * 3 + '" onChange="markChanged()" onFocus="javascript:fieldFocus()" onBlur="javascript:fieldBlur()"/>');
    document.write("</td>");
    document.write("</tr>");
  }  
</script>

  <tr><td>&nbsp;</td></tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left">
      <xsl:if test="key('pagedata','PricingCodeData')/@value = 'Y'">
        <button class="blueButton" onClick="javascript:doUpdate()" accesskey="s" tabindex="100">(S)ave</button>&nbsp;&nbsp;
      </xsl:if>
    </td>
    <td align="left">
      <xsl:if test="key('pagedata','PricingCodeData')/@value = 'Y'">
        <button class="blueButton" onClick="javascript:doClear()" accesskey="r" tabindex="110">(R)eset</button>&nbsp;&nbsp;
      </xsl:if>
    </td>
    <td align="right">
      <button class="blueButton" onClick="javascript:doExit()" accesskey="e" tabindex="111">(E)xit</button>&nbsp;&nbsp;
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  </table>  
  
</xsl:if>

</td></tr>
</table>

</form>

</body>
</html>

   </xsl:template>
</xsl:stylesheet>
