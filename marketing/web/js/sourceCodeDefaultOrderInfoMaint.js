/*
  Implements javascript methods for the sourceCodeDefaultOrderInfoMaint.xsl page.
*/

// These are initialized from the XSL file
var v_states = new Array(); 
var v_addressTypes = new Array(); 
var v_countries = new Array(); 
var v_isAfterSubmit = 'false'; 
var v_unsavedChanges = false;

// Regular Expressions for validators
var REGEX_US_ZIP = /^\d{5}$/;
var REGEX_CA_POSTAL_CODE = /^([A-Za-z]\d[A-Za-z][-]?\d[A-Za-z]\d)/;
var REGEX_CA_POSTAL_CODE_SHORT = /^([A-Za-z]\d[A-Za-z])/;

/* Performs Page initialization */
function init() {

  // if this is after submit action, cloes the dialog
  if(v_isAfterSubmit) {
     window.returnValue = "RELOAD"; // Parent should reload
     doClose();
  }
  
  // Register default handlers for input fields
  for(var i = 0; i < theform.elements.length; i++){
    var theElement = theform.elements[i];
    
    if((theElement.nodeName.toLowerCase() == 'input' && theElement.type.toLowerCase() == 'text') ||
       (theElement.nodeName.toLowerCase() == 'select') ||
       (theElement.nodeName.toLowerCase() == 'textarea') ) {
        theElement.onfocus = fieldFocus;
        theElement.onblur = fieldBlur;
        theElement.onchange = setChangedFlag;
    }
  }
  
  // Associate the Labels to their Input elements so we can get them later for the validation
  var labels = document.getElementsByTagName('label'); 
  for (var i = 0; i < labels.length; i++) { 
    if (labels[i].htmlFor != '') { 
      var elem = document.getElementById(labels[i].htmlFor); 
      if (elem) {
        elem.label = labels[i];
      }
    } 
  } 

  clearAllErrors();
  
  // Register custom handlers
  $("recipientAddress").onkeyup = function() {limitText($("recipientAddress"), 90);}
  $("recipientAddress").onkeydown = function() {limitText($("recipientAddress"), 90);}
  $("customerAddress").onkeyup = function() {limitText($("customerAddress"), 90);}
  $("customerAddress").onkeydown = function() {limitText($("customerAddress"), 90);}   
  $("recipientLocationType").onchange = handleLocationTypeChanged;
  $("recipientCountry").onchange = function() { handleCountryChanged($("recipientCountry"), $("recipientState"), $("recipientZipCode")); }
  $("recipientPhone").onchange = function() { limitToDigits($("recipientPhone")); setChangedFlag();}
  $("recipientPhoneExt").onchange = function() { limitToDigits($("recipientPhoneExt")); setChangedFlag();}
  
  $("customerDaytimePhone").onchange = function() { limitToDigits($("customerDaytimePhone")); setChangedFlag();}
  $("customerDaytimePhoneExt").onchange = function() { limitToDigits($("customerDaytimePhoneExt")); setChangedFlag();}  
  $("customerEveningPhone").onchange = function() { limitToDigits($("customerEveningPhone")); setChangedFlag();}
  $("customerEveningPhoneExt").onchange = function() { limitToDigits($("customerEveningPhoneExt")); setChangedFlag();}  
  $("customerCountry").onchange = function() { handleCountryChanged($("customerCountry"), $("customerState"), $("customerZipCode")); }
  
  // Invoke the select handlers for first time/initial load
  handleLocationTypeChanged();
  $("recipientCountry").onchange();
  $("customerCountry").onchange();
  
  v_unsavedChanges = false;
}

/** Handles saving/submitting of the Form **/
function doSave() {
  if(!validateForm()) {
    return;
  }

  document.theform.action_type.value="update";
  document.theform.target = "";
  document.theform.submit();
}

/** Clears all the fields on the Form **/
function doClearAllFields() {
  clearAllFields(document.theform);
  init();
  v_unsavedChanges = true;
}

/** Handles the Close Button **/
function doClose() {
  if(v_unsavedChanges) {
    var answer = confirm("You have not saved your changes.\nClick OK to discard these changes and close the screen,or\nClick Cancel to return to the screen so you can save your changes.");
    
    if(!answer) {
      return;
    }
  }

  window.close();
}

/** Handle when the Location Selection has Changed **/
function handleLocationTypeChanged() {
  var location = getSelectionValue("recipientLocationType");
  var recipientBusinessNameLabel = $("recipientBusinessNameLabel");
  var recipientBusinessName = $("recipientBusinessName");
  var recipientLocationDetailLabel = $("recipientLocationDetailLabel");
  var recipientLocationDetail = $("recipientLocationDetail");

  var addressType = null;
  for(var i=0; i< v_addressTypes.length; i++) {
    if(v_addressTypes[i].address_type == location) {
      addressType = v_addressTypes[i];
      break;
    }
  }
  
  if(addressType != null && addressType.prompt_for_business_flag == 'Y' && addressType.business_label_txt != '') {
    recipientBusinessNameLabel.innerText = addressType.business_label_txt;
    setFieldEnabled(recipientBusinessName, true);
  } else {
    recipientBusinessNameLabel.innerText = "Location Name";
    recipientBusinessName.innerText="";
    setFieldEnabled(recipientBusinessName, false);
  }

  
  if(addressType != null && addressType.prompt_for_room_flag == 'Y' && addressType.room_label_txt != '') {
    recipientLocationDetailLabel.innerText = addressType.room_label_txt;
    setFieldEnabled(recipientLocationDetail, true);
  } else {
    recipientLocationDetailLabel.innerText = "Room";
    recipientLocationDetail.innerText = "";
    setFieldEnabled(recipientLocationDetail, false);
  }
  
  setChangedFlag();
}

/* Enables/Disables the Field and adds/removes the class to gray it out */
function setFieldEnabled(field, enabled) {
  field.disabled = !enabled;
  
  var currentClass = "" + field.className;
  currentClass = currentClass.replace("disabledField", "");
  
  if(!enabled) {
    currentClass = currentClass + " disabledField";
  }
  
  field.className = currentClass;
  
  if(!enabled) {
    setFieldError(field, false); // disabled fields cannot be in error
  }
}


/* Sets the Error Class for the Field */
function setFieldError(field, error) {  
  var currentClass = "" + field.className;
  currentClass = currentClass.replace("errorField", "");
  
  if(error) {
    currentClass = currentClass + " errorField";
  }
  
  field.className = currentClass;
}

/** Handles the changes in a country for the passed in fields**/
function handleCountryChanged(countryField, stateField, zipCodeField) {
  var countryId = getSelectionValue(countryField.id);
  
  if(countryId=='US' || countryId=='CA') {    
    // Update the options in the state Field (Save and restore the selection to make sure it is not lost)
    var selectedState = getSelectionValue(stateField.id);
    var selectedIndex = -1;
    clearOptions(stateField);
    addToOptionList(stateField, "", "");
    for(var i=0; i< v_states.length; i++) {
      if(v_states[i].country_id == countryId) {
        addToOptionList(stateField, v_states[i].state_master_id, v_states[i].state_name);
        
        if(selectedState == v_states[i].state_master_id) {
          selectedIndex = stateField.length -1;
        }
      }
    }
    
    if(selectedIndex >= 0) {
      stateField.selectedIndex = selectedIndex;
    }

    setFieldEnabled(zipCodeField, true);
    setFieldEnabled(stateField, true);  
  }
  else if (countryId == ""){
    setFieldEnabled(zipCodeField, true);
    setFieldEnabled(stateField, true);
  }
  else {
    zipCodeField.innerText = "";
    clearOptions(stateField);
    setFieldEnabled(zipCodeField, false);
    setFieldEnabled(stateField, false);
  }
 
  setChangedFlag(); 
}

/** Convenience method to retrieve an Element by Id **/
function $(elementId) {
  return document.getElementById(elementId);
}

/* Returns the current value of a selection */
function getSelectionValue(elementId) {
  var selectElement = $(elementId);
  var idx = selectElement.selectedIndex;
  
  if(idx < 0)
  {
    return "";
  }

  return selectElement.options[idx].value;
}


/** Clears an options list **/
function clearOptions(optionList) {
   // Always clear an option list from the last entry to the first
   for (x = optionList.length; x >= 0; x--) {
      optionList[x] = null;
   }
}

/** Adds an item to an options list **/
function addToOptionList(optionList, optionValue, optionText) {
   // Add option to the bottom of the list
   optionList[optionList.length] = new Option(optionText, optionValue);
}

/** Sets the changed flag for the form **/
function setChangedFlag() {
  v_unsavedChanges = true;
}

/* Clears all the fields's error status on the form */
function clearAllErrors() {
  for(var i = 0; i < theform.elements.length; i++){
    setFieldError(theform.elements[i], false);
  }
}

/** Validates the Form Submission **/
function validateForm() {
  var errors = new Array();
  
  // Clear all Errors initially
  clearAllErrors();
  
  // Invoke Validators in display order
  validateZipCode("Recipient ", $("recipientZipCode"), $("recipientCountry"), errors);  
  validatePhoneNumber("Recipient ", $("recipientPhone"), $("recipientCountry"), errors);  
  validateAddressFields("Recipient ",$("recipientCity"), $("recipientState"), $("recipientZipCode"), $("recipientCountry"), errors);

  validatePhoneNumber("Customer ",$("customerDaytimePhone"), $("customerCountry"), errors);
  validatePhoneNumber("Customer ",$("customerEveningPhone"), $("customerCountry"), errors);  
  validateZipCode("Customer ", $("customerZipCode"), $("customerCountry"), errors);  
  validateEmailField("Customer ", $("customerEmailAddress"), errors);  
  validateAddressFields("Customer ",$("customerCity"), $("customerState"), $("customerZipCode"), $("customerCountry"), errors);

  // if errors.length > 0 popup error message, and return false.
  if(errors.length > 0) {
    var errorMessage = errors.join("\n");
    alert(errorMessage);
    return false;
  }


  return true;
}

/** Removes non digits from a string **/
function stripNonDigits(inString) {
    var s = String(inString);
    var re = /\D/g;
    return s.replace(re,"");
}

/** Trims the passed in string **/
function trimString(inString) {
  if(inString) {
    inString = inString.replace( /^\s+/g, "" );// strip leading
    return inString.replace( /\s+$/g, "" );// strip trailing
  }
  
  return "";
}

/** Returns true if the passed in country is domestic **/
function isCountryDomestic(country_id) {
    for( var idx=0; idx<v_countries.length; idx++) {
        if( v_countries[idx].country_id==country_id ) {
          if(v_countries[idx].oe_country_type=="D" ) {
            return true;
          }          
          return false;
        }
    }        
    return false;
}  


/** Validates the Field and appends an error if it is in error **/
function validateEmailField(errorMessageHeader, emailField, errors) {
  emailField.value = trimString(emailField.value);

  if(isEmpty(emailField.value) || isEmail(emailField.value)) {
    return;
  }

  setFieldError(emailField, true);
  errors.push(errorMessageHeader + emailField.label.innerHTML + " is invalid.");
}

/** Validates the Zip Code for a Country and appends an error if it is in error **/
function validateZipCode(errorMessageHeader, zipCodeField, countryField, errors) {
  zipCodeField.value = trimString(zipCodeField.value);
  
  if(isEmpty(zipCodeField.value)) {
    return;
  }

  var countryId = getSelectionValue(countryField.id);
  
  var testZip = zipCodeField.value;
  var isValid = false;
  
  if( isCountryDomestic(countryId)==false ) {
    isValid = true; // no validation
  } else if( countryId=='US' ) {
    isValid = REGEX_US_ZIP.test(testZip) ;
  } else if( countryId=='CA' ) {
      if( testZip.length==3 ) {
          isValid = REGEX_CA_POSTAL_CODE_SHORT.test(testZip);
      } else if( testZip.length==6 ) {
          isValid = REGEX_CA_POSTAL_CODE.test(testZip);
      } else {
          isValid = false;
      }
  } else {
    var re = /^[\w]{0,6}$/;
    isValid = re.test(testZip);
  }
  
  if(!isValid) {
    setFieldError(zipCodeField, true); 
    errors.push(errorMessageHeader + zipCodeField.label.innerHTML + " is invalid.");    
  }
}

/** Validates the Phone Number for a Country **/
function validatePhoneNumber(errorMessageHeader, phoneNumberField, countryField, errors) {
  phoneNumberField.value = stripNonDigits(phoneNumberField.value);
  
  if(isEmpty(phoneNumberField.value)) {
    return;
  }

  var countryId = getSelectionValue(countryField.id);
  
  if( isCountryDomestic(countryId) && phoneNumberField.value.length != 10 ) {
    setFieldError(phoneNumberField, true); 
    errors.push(errorMessageHeader + phoneNumberField.label.innerHTML + " is invalid.");    
  } 
  
  
}

/** Validates the set of Address Fields **/
function validateAddressFields(errorMessageHeader,cityField, stateField, zipCodeField, countryField, errors) {
  cityField.value = trimString(cityField.value);
  zipCodeField.value = trimString(zipCodeField.value);
  
  var city = cityField.value;
  var zipCode = zipCodeField.value;
  var countryId = getSelectionValue(countryField.id);
  var stateId = getSelectionValue(stateField.id);
  
  if( countryId == 'US' || countryId == 'CA' ) {
    // Must enter all 4 if any is selected. By definition, country is selected, so other 3 must be specified.
    if(city.length == 0 || zipCode.length == 0 || stateId.length == 0) {
      setFieldError(cityField, true); 
      setFieldError(stateField, true); 
      setFieldError(zipCodeField, true); 
      setFieldError(countryField, true); 
      
      errors.push("You must enter " 
                    + errorMessageHeader + zipCodeField.label.innerHTML + ", " 
                    + errorMessageHeader + cityField.label.innerHTML + ", " 
                    + errorMessageHeader + stateField.label.innerHTML + ", and " 
                    + errorMessageHeader + countryField.label.innerHTML + ", "
                    + "or leave all four fields blank.");
    }
  } else {  
    // Must enter both if either is entered. 
    if( (city.length == 0 && countryId.length > 0) ||
        (city.length > 0 && countryId.length == 0) ) {
           setFieldError(cityField, true); 
           setFieldError(countryField, true); 

      errors.push("You must enter " 
                    + errorMessageHeader + cityField.label.innerHTML + " and " 
                    + errorMessageHeader + countryField.label.innerHTML + ", "
                    + "or leave both fields blank.");        
    }  
  }

}

/** Limits a Field length **/
function limitText(field, maxLength) {
  if(field.value.length > maxLength) {
    field.value = field.value.substr(0, maxLength);
  }
}

/** Limits a Field to only digits **/
function limitToDigits(field) {
  var currentVal = field.value;
  var newVal = stripNonDigits(currentVal);

  if(currentVal != newVal) {
    field.value = newVal;
  }
}