package com.ftd.marketing.sourcecode.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;

import com.ftd.enterprise.vo.SourceFloristPriorityVO;
import com.ftd.enterprise.vo.SourceProgramRefVO;
import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;
import com.ftd.marketing.sourcecode.constants.SourceCodeErrorConstants;
import com.ftd.marketing.sourcecode.model.CreateSourceCodeRequest;
import com.ftd.marketing.sourcecode.model.CreateSourceCodeResponse;
import com.ftd.marketing.sourcecode.model.DeleteSCFloristsRequest;
import com.ftd.marketing.sourcecode.model.Florist;
import com.ftd.marketing.sourcecode.model.GetSCFloristsResponse;
import com.ftd.marketing.sourcecode.model.OrderSource;
import com.ftd.marketing.sourcecode.model.SourceCodeRequest;
import com.ftd.marketing.sourcecode.model.SourceCodeResponse;
import com.ftd.marketing.sourcecode.model.UpdateSCAttributesResponse;
import com.ftd.marketing.sourcecode.model.UpdateSCFloristsRequest;
import com.ftd.marketing.sourcecode.model.UpdateSCTemplateRequest;
import com.ftd.marketing.sourcecode.model.UpdateSourceCodeAttributesRequest;
import com.ftd.marketing.vo.SourceProgramRequestVO;
import com.ftd.marketing.vo.SourceRequestVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;


@WebService(endpointInterface = "com.ftd.marketing.sourcecode.service.SourceCodeActions", 
						targetNamespace = "http://service.sourcecode.marketing.ftd.com/", 
						serviceName = "SourceCodeActionsImplService", 
						portName = "SourceCodeActionsImplPort")	
						
@InInterceptors(interceptors="com.ftd.marketing.sourcecode.service.handlers.AuthenticationInterceptor")						
public class SourceCodeActionsImpl implements SourceCodeActions {
	
	private Logger logger = new Logger("com.ftd.marketing.sourcecode.service.SourceCodeActionsImpl");
	
	private static final String RESOURCE_PROPERTITY_FILE = "SourceCodeService";
	
	private static ResourceBundle resrcBundle = ResourceBundle.getBundle(RESOURCE_PROPERTITY_FILE);
	
	/** Creates a New Source Code upon portal request.
	 * Mandatory request parameters are order source, template, location and requested URL.
	 * @param createSCRequest
	 * @return
	 */
	public CreateSourceCodeResponse createSCwithTemplate(CreateSourceCodeRequest createSCRequest) {	
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Create Source code portal request - recieved on - " + getCurrentTimeStamp() + 
				" for location: " + createSCRequest.getLocation() + 
				" for template: " + createSCRequest.getTemplateName() + "\n");
		
		return processCreateSCRequest(createSCRequest, logMessage);
	}

	/** Updates Source Code template upon portal request. 
	 * Mandatory request parameters are SC to be updated,modified template, location and requested URL.
	 * Modifies the SC as per the modified template attributes.
	 * @param updateSCRequest
	 * @return
	 */
	public SourceCodeResponse updateSourceCodeTemplate(UpdateSCTemplateRequest updateSCRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Update Source code request - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + updateSCRequest.getSourceCode() + 
				" for location: " + updateSCRequest.getLocation() +
				" for template: " + updateSCRequest.getTemplateName() + "\n");		
		
		return processUpdateSCRequest(updateSCRequest, logMessage);
	}

	/** updates the primary and secondary Florists choice into Source_master table.
	 * Mandatory request parameters are order source, template, location and requested URL.
	 * @param createSCRequest 
	 * @return
	 */
	@Override
	public SourceCodeResponse updateSourceCodeFlorists(UpdateSCFloristsRequest updateSCFloristsRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Update Florists request - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + updateSCFloristsRequest.getSourceCode());		
		
		return processUpdateSCFlorists(updateSCFloristsRequest, logMessage);
	}
	
	/** Gets the preferred florists associated with a given source code.
	 * @see com.ftd.marketing.sourcecode.service.SourceCodeActions#getSourceCodeFlorists(com.ftd.marketing.sourcecode.model.GetSCFloristsRequest)
	 */
	@Override
	public GetSCFloristsResponse getSourceCodeFlorists(SourceCodeRequest getSourceCodeFloristsRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Get Source Code Florists request - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + getSourceCodeFloristsRequest.getSourceCode());
		
		return processGetSCFlorists(getSourceCodeFloristsRequest, logMessage);		
	}
	

	/** Adds a new florist to the source code florist priority list.
	 * @see com.ftd.marketing.sourcecode.service.SourceCodeActions#addSourceCodeFlorists(com.ftd.marketing.sourcecode.model.UpdateSCFloristsRequest)
	 */
	@Override
	public SourceCodeResponse addSourceCodeFlorists(UpdateSCFloristsRequest addSCFloristRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Add Source code Florists request - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + addSCFloristRequest.getSourceCode());		
		
		return processAddSCFlorists(addSCFloristRequest, logMessage);
	}

	/** Deletes a florist from the source code florist priority list.
	 * @see com.ftd.marketing.sourcecode.service.SourceCodeActions#deleteSourceCodeFlorists(com.ftd.marketing.sourcecode.model.DeleteSCFloristsRequest)
	 */
	@Override
	public SourceCodeResponse deleteSourceCodeFlorists(DeleteSCFloristsRequest deleteSCFloristRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Delete Source code Florists request - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + deleteSCFloristRequest.getSourceCode());		
		
		return processDeleteSCFlorists(deleteSCFloristRequest, logMessage);
	}	
	
	/** Deletes Dependencies for the given source code.
	 * @see com.ftd.marketing.sourcecode.service.SourceCodeActions#deleteSCdependencies(com.ftd.marketing.sourcecode.model.DeleteSCFloristsRequest)
	 */
	@Override
	public SourceCodeResponse deleteSCdependencies(SourceCodeRequest deleteSCdependenyRequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Delete Source code dependencies - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + deleteSCdependenyRequest.getSourceCode());		
		
		return processDeleteSCdependencies(deleteSCdependenyRequest, logMessage);
	}	
	
	/** Updates attributes for the given source code.
	 * @see com.ftd.marketing.sourcecode.service.SourceCodeActions#updateSCAttributes(com.ftd.marketing.sourcecode.model.UpdateSourceCodeAttributesRequest)
	 */
	@Override
	public SourceCodeResponse updateSCAttributes(UpdateSourceCodeAttributesRequest updateSourceCodeAttributesrequest) {
		
		StringBuffer logMessage = new StringBuffer("\n-----------------------------\n");
		logMessage.append("Update Source Code Attributes - recieved on - " + getCurrentTimeStamp() + 
				" for source code: " + updateSourceCodeAttributesrequest.getSourceCode());		
		
		return processUpdateSCAttributes(updateSourceCodeAttributesrequest, logMessage);
	}	
	
		
	

	/** Process the create source code request.
	 * @param request
	 * @param logMessage
	 * @return
	 */
	private CreateSourceCodeResponse processCreateSCRequest(CreateSourceCodeRequest request, StringBuffer logMessage) {
		logger.info(":: processCreateSourceCodeRequest ::");	

		CreateSourceCodeResponse response = new CreateSourceCodeResponse();
		response.setLocation(request.getLocation());
		
		List<String> validationErrors = validateCreateSCRequest(request);
		if (validationErrors == null || validationErrors.size() == 0) {
			try {
				
				int internetSourceCode = 0;
				SourceVO existingSC = null;					
									
				String defaultSourceCode = getDefaultSourceCode(request.getTemplateName());				
				if (StringUtils.isEmpty(defaultSourceCode)) {
					try {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.TEMPLATE_NAME_INVALID));
					} catch (Exception e) {
						logger.error("Error caught obtaning resource value"	+ e.getMessage());
						validationErrors.add("Template Name does not exist");
					}
					throw new Exception("Default Source code for the default template : " + request.getTemplateName() + " cannot be determined");
				}
				
				boolean isLegacyTemplate = false;
				if(StringUtils.upperCase(request.getTemplateName()).indexOf(SourceCodeConstants.LEGACY_PARTNER) >= 0) {
					isLegacyTemplate = true;					
					if(request.getOrderSource().contains(SourceCodeConstants.PHONE_ORDER_SOURCE)) {
						try {
							validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.INVALID_ORDER_SOURCE_FOR_LEGACY));
						} catch (Exception e) {
							logger.error("Error caught obtaning resource value"	+ e.getMessage());
							validationErrors.add("Request for Phone Source code is invalid for Legacy");
						}
						
						throw new Exception("Phone Source Code requested for the legacy tempalte : " + request.getTemplateName());
					}
				}				
				
				while(true) {
					internetSourceCode = generateInternetSourceCode(request.getTemplateName());			
					logMessage.append("Generated Source Code : " + internetSourceCode + "\n");
					existingSC = SourceServiceBO.getSourceCodeDetail(""+internetSourceCode, SourceCodeConstants.INTERNET_ORDER_SOURCE);
					if(existingSC == null) {
						break;
					} else {
						logMessage.append("Generated Source Code already exists : Regenerating SC.\n");	
					}
				}
				
				logMessage.append("Default Source code for the template " + defaultSourceCode + "\n");				

				for (String orderSrc : request.getOrderSource()) {
					SourceVO sourceCodeDetail = SourceServiceBO.getSourceCodeDetail(defaultSourceCode, orderSrc);
					
					sourceCodeDetail.setDescription(request.getLocation());
					sourceCodeDetail.setId(""+internetSourceCode);
					//should be removed.
					sourceCodeDetail.setAutomatedPromotionEngineFlag("N");
					if (SourceCodeConstants.PHONE_ORDER_SOURCE.equalsIgnoreCase(sourceCodeDetail.getOrderSource())) {
						sourceCodeDetail.setRelatedSourceCode(sourceCodeDetail.getId());
						sourceCodeDetail.setId(sourceCodeDetail.getId().concat(SourceCodeConstants.PHONE_ORDER_SOURCE));						
					} else {						
						if(!isLegacyTemplate) {
							
							sourceCodeDetail.setRelatedSourceCode(sourceCodeDetail.getId().concat(SourceCodeConstants.PHONE_ORDER_SOURCE));						
						}	
					}					
					sourceCodeDetail.setProgramWebsiteUrl(request.getWebsiteURL());
					sourceCodeDetail.setStartDate(new Date());
					sourceCodeDetail.setLegacyId(request.getLegacyId());
					
					sourceCodeDetail.setRecptAddress(request.getAddress());
					sourceCodeDetail.setRecptCity(request.getCity());
					sourceCodeDetail.setRecptState(request.getState());
					sourceCodeDetail.setRecptZipcode(request.getZipcode());
					sourceCodeDetail.setRecptCountry(request.getCountry());
					sourceCodeDetail.setRecptPhoneNumber(request.getPhoneNumber());
					
					String recordSrcMsg = recordSource(sourceCodeDetail, true);
					if(recordSrcMsg != null) {
						logMessage.append("\n" + recordSrcMsg);		
					}					
					
					if (orderSrc.equalsIgnoreCase(SourceCodeConstants.PHONE_ORDER_SOURCE)) {
						response.setPhoneSourceCode(sourceCodeDetail.getId());
					} else if (orderSrc.equalsIgnoreCase(SourceCodeConstants.INTERNET_ORDER_SOURCE)) {
						response.setInternetSourceCode(sourceCodeDetail.getId());
					}					
				}

				// Set success response
				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;

			} catch (Exception e) {
				logger.error("Error caught processing Source code creation request: " + e.getMessage());
				logMessage.append(e.getMessage() + "\n");
			}
		} 
		
		// Set failure response
		return (CreateSourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	/** Process the Update Source code Request.
	 * @param request
	 * @param logMessage
	 * @return
	 */
	private SourceCodeResponse processUpdateSCRequest(UpdateSCTemplateRequest request, StringBuffer logMessage) {
		SourceCodeResponse response = new SourceCodeResponse();
		
		List<String> validationErrors = validateUpdateSCTemplateReq(request);
		SourceVO originalInternetSourceDetail = null;
		SourceVO originalPhoneSourceDetail = null;		
		
		String defaultSourceCode = null;
		try {
			
			if (validationErrors == null || validationErrors.size() == 0) {		
				
				defaultSourceCode = getDefaultSourceCode(request.getTemplateName());				
				if (StringUtils.isEmpty(defaultSourceCode)) {
					try {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.TEMPLATE_NAME_INVALID));
					} catch (Exception e) {
						logger.error("Error caught obtaning resource value"	+ e.getMessage());
						validationErrors.add("Template Name does not exist");
					}
					throw new Exception("Default Source code for the default template : " + request.getTemplateName() + " cannot be determined");
				}
				
				logMessage.append("Default Source code for the template " + defaultSourceCode + "\n");
				
				originalInternetSourceDetail = SourceServiceBO.getSourceCodeDetail(request.getSourceCode(), 
						SourceCodeConstants.INTERNET_ORDER_SOURCE);				
				
				if(originalInternetSourceDetail == null) {
					try{
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_INVALID));
					} catch(Exception e) {
						logger.error("Error caught obtaning resource value"	+ e.getMessage());
						validationErrors.add("Source Code does not exist");						
					}
				} else {					
					String originalSourceType = originalInternetSourceDetail.getSourceType();										
					if(isSameTemplate(request.getTemplateName(), originalSourceType, originalInternetSourceDetail.getCompanyId())) {
						try{
							validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.TEMPLATE_NAME_UNMODIFIED));
						} catch(Exception e) {
							logger.error("Error caught obtaning resource value"	+ e.getMessage());
							validationErrors.add("Default template is same as the modified template");						
						}
					} 
					
					if(originalInternetSourceDetail.getSourceType().indexOf(SourceCodeConstants.LEGACY_PARTNER) >= 0) {
						try {
							validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.INVALID_LEGACY_UPDATE_REQUEST));
						} catch (Exception e) {
							logger.error("Error caught obtaning resource value"	+ e.getMessage());
							validationErrors.add("Legacy Source Code cannot be updated in Apollo");
						}
					}
					
					if(!isSameGroup(request.getTemplateName(), originalSourceType, originalInternetSourceDetail.getCompanyId())) {
						try {							
							validationErrors.add(MessageFormat.format(resrcBundle.getString(SourceCodeErrorConstants.INVALID_SOURCE_TYPE_UPDATE), request.getTemplateName()));
						} catch (Exception e) {
							logger.error("Error caught obtaning resource value"	+ e.getMessage());
							validationErrors.add("Cannot be updated to incompatible type "+ request.getTemplateName());
						}
					}
				}				
			}
			
			if (validationErrors == null || validationErrors.size() == 0) {
				
				if(!StringUtils.isEmpty(originalInternetSourceDetail.getRelatedSourceCode()) && originalInternetSourceDetail != null) {
					// Get the phone source code detail from the Original Internet Source code
					originalPhoneSourceDetail = SourceServiceBO.getSourceCodeDetail(originalInternetSourceDetail.getRelatedSourceCode(), null);
				}
				
				OrderSource[] orderSources  = OrderSource.getValues();
				for (OrderSource orderSrc : orderSources) {
					
					if (originalPhoneSourceDetail == null
							&& orderSrc.getValue().equalsIgnoreCase(SourceCodeConstants.PHONE_ORDER_SOURCE)) {
						logMessage.append("Phone Source code doesnt exist in apollo for the location : " + request.getLocation() + "\n");
						continue;
					} 
					
					SourceVO sourceCodeDetail = SourceServiceBO.getSourceCodeDetail(defaultSourceCode, orderSrc.getValue());
					
					sourceCodeDetail.setDescription(request.getLocation());
					sourceCodeDetail.setProgramWebsiteUrl(request.getWebsiteURL());
					
					//sourceCodeDetail.setLegacyId(request.getLegacyId());
									
					if (SourceCodeConstants.PHONE_ORDER_SOURCE.equalsIgnoreCase(sourceCodeDetail.getOrderSource())) {
						populateSourceCode(sourceCodeDetail, originalPhoneSourceDetail, request, SourceCodeConstants.PHONE_ORDER_SOURCE);						
					} else {
						populateSourceCode(sourceCodeDetail, originalInternetSourceDetail, request, SourceCodeConstants.INTERNET_ORDER_SOURCE);						
					}									
					
					String recordSrcMsg = recordSource(sourceCodeDetail, false);
					if(recordSrcMsg != null) {
						logMessage.append("\n" + recordSrcMsg);		
					}
				}

				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;
			}			
			
		} catch(Exception e) {			
			logger.error("Error caught processing update Source code template request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");
		}
		
		// Set failure response
		return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);		
	}
	
	/**
	 * @param sourceCodeDetail
	 * @param originalPhoneSourceDetail
	 */
	private void populateSourceCode(SourceVO sourceCodeDetail, SourceVO originalSC, UpdateSCTemplateRequest request, String orderSource) {
		if(SourceCodeConstants.INTERNET_ORDER_SOURCE.equalsIgnoreCase(orderSource)) {
			sourceCodeDetail.setId(request.getSourceCode());
		} else {
			sourceCodeDetail.setId(originalSC.getId());
		}
		sourceCodeDetail.setRelatedSourceCode(originalSC.getRelatedSourceCode());
		sourceCodeDetail.setStartDate(originalSC.getStartDate());
		sourceCodeDetail.setEndDate(originalSC.getEndDate());
		if(StringUtils.isEmpty(request.getLocation())) {
			sourceCodeDetail.setDescription(originalSC.getDescription());
		}
		if(StringUtils.isEmpty(request.getWebsiteURL())) {
			sourceCodeDetail.setProgramWebsiteUrl(originalSC.getProgramWebsiteUrl());
		}
	}
	
	/** Processes the Update source code florists request.
	 * @param request
	 * @return
	 */
	private SourceCodeResponse processUpdateSCFlorists(UpdateSCFloristsRequest request, StringBuffer logMessage) {		
		SourceCodeResponse response = new SourceCodeResponse();					
		List<String> validationErrors = null;
		try {	
			
			SourceVO sourceCodeDetail = null; 
			if(!StringUtils.isEmpty(request.getSourceCode())) {
				sourceCodeDetail = SourceServiceBO.getSourceCodeDetail(request.getSourceCode(), SourceCodeConstants.INTERNET_ORDER_SOURCE);
			}
			validationErrors = validateUpdateSCFloristReq(request, sourceCodeDetail);
			
			if (validationErrors == null || validationErrors.size() == 0) {
							
				SourceVO sourceFloristVo = populateSCFlorists(request.getSecondaryFlorists(), request.getPrimaryFlorist(), sourceCodeDetail);	
								
				for (SourceFloristPriorityVO sourceFloristPriorityVO : sourceFloristVo.getSourceFloristPriorityList()) {
					CachedResultSet result = SourceServiceBO.getFloristDetails(sourceFloristPriorityVO.getFloristId());
					if(result.next()) {
						continue;
					} else {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.INVALID_FLORISTS));
						throw new Exception("Invalid florists included in request");
					}
				}
				
				SourceServiceBO.updateSCFlorists(sourceFloristVo);
				
				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;
			}
		} catch(Exception e) {
			logger.error("Error caught processing update Source code florists request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");			
		}
		// Set failure response
		return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	/** Processes the getSourceCodeFlorists request
	 * @param request
	 * @param logMessage
	 * @return
	 */
	private GetSCFloristsResponse processGetSCFlorists(SourceCodeRequest request, StringBuffer logMessage) {		
		GetSCFloristsResponse response = new GetSCFloristsResponse();		
		
		List<String> validationErrors = validateGetSCFloristReq(request);
		try {
			if(SourceServiceBO.getSourceCodeDetail(request.getSourceCode(), null) == null) {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_INVALID));
				throw new Exception("Source code does not exist. Update cannot be performed");
			}
			
			if (validationErrors == null || validationErrors.size() == 0) {
				List<Florist> florists = SourceServiceBO.getSCFlorists(request.getSourceCode());							
				
				return getSCfloristsSuccessResp(florists, logMessage, request.getSourceCode());				
			}
		} catch(Exception e) {
			logger.error("Error caught processing get Source code florists request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");
		}
		
		// Set failure response
		setFailureResponse(response, validationErrors, logMessage);	
		response.setSourceCode(request.getSourceCode());
		return response;
	}
	
	/** Processes the add source code florists request.
	 * Appends the valid florist detail for a given source code florist priority list.
	 * @param request
	 * @return
	 */
	private SourceCodeResponse processAddSCFlorists(UpdateSCFloristsRequest request, StringBuffer logMessage) {		
		SourceCodeResponse response = new SourceCodeResponse();	
		List<String> validationErrors = null;
		try {	
			SourceVO sourceCodeDetail = null; 
			if(!StringUtils.isEmpty(request.getSourceCode())) {
				sourceCodeDetail = SourceServiceBO.getSourceCodeDetail(request.getSourceCode(), SourceCodeConstants.INTERNET_ORDER_SOURCE);
			}
			validationErrors = validateUpdateSCFloristReq(request, sourceCodeDetail);
			if (validationErrors == null || validationErrors.size() == 0) {				
				
				SourceVO sourceFloristVo = populateSCFlorists(request.getSecondaryFlorists(), request.getPrimaryFlorist(), sourceCodeDetail);	
				
				logger.info("The same florist mapping will be inserted for related phone source code : " + sourceFloristVo.getRelatedSourceCode());
				
				// Check if the florists included in the request are valid.
				for (SourceFloristPriorityVO sourceFloristPriorityVO : sourceFloristVo.getSourceFloristPriorityList()) {
					CachedResultSet result = SourceServiceBO.getFloristDetails(sourceFloristPriorityVO.getFloristId());
					if(result.next()) {
						continue;
					} else {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.INVALID_FLORISTS));
						throw new Exception("Invalid florists included in request");
					}
				}
				List<Florist> actualFlorists = SourceServiceBO.getSCFlorists(request.getSourceCode());
				
				if(actualFlorists != null && actualFlorists.size() > 0) {
					Florist primaryFlorist = null;					
					int secondaryFlorists = actualFlorists.size();
					
					for (Florist florist : actualFlorists) {
						if(florist.isPrimary()) {
							primaryFlorist = florist;	
							secondaryFlorists -= 1;							
						} 
						
						// If requested secondary florist is already existing, throw an error.
						if(request.getSecondaryFlorists().contains(florist.getFloristMemeberId())) {
							validationErrors.add(MessageFormat.format(resrcBundle.getString(SourceCodeErrorConstants.FLORIST_ALREADY_EXISTS), florist.getFloristMemeberId()));							
							throw new Exception("Requested florist already exists for the source code");
						}
						
						//If requested primary florist is already existing, throw an error.
						if(request.getPrimaryFlorist() != null && request.getPrimaryFlorist().equals(florist.getFloristMemeberId())) {
							validationErrors.add(MessageFormat.format(resrcBundle.getString(SourceCodeErrorConstants.FLORIST_ALREADY_EXISTS), florist.getFloristMemeberId()));							
							throw new Exception("Requested florist already exists for the source code");
						}
						
					}
					
					// Check if it is duplicate request to add primary
					if(request.getPrimaryFlorist() != null && primaryFlorist != null) {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.PRIMARY_ALREADY_EXISTS));
						throw new Exception("Cannot add primary florist. Primary choice already exists.");
					}					
					
					// check if the sum of existing secondary florists and requested secondary florists is not more than 10
					if(request.getSecondaryFlorists() != null) {
						if(request.getSecondaryFlorists().size() + secondaryFlorists > 10) {
							validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.FLORISTS_RANGE_EXCEEDED));
							throw new Exception("Sum of existing secondary florists and requested secondary florists range exceeded.");
						}
					}					
				}
				
				SourceServiceBO.addSCFlorists(sourceFloristVo);
				
				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;
			}
		} catch(Exception e) {
			logger.error("Error caught processing add Source code florists request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");			
		}
		// Set failure response
		return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	/** Processes the add source code florists request.
	 * Appends the valid florist detail for a given source code florist priority list.
	 * @param request
	 * @return
	 */
	private SourceCodeResponse processDeleteSCFlorists(DeleteSCFloristsRequest request, StringBuffer logMessage) {		
		SourceCodeResponse response = new SourceCodeResponse();	
		List<String> validationErrors = null;
		try {	
			SourceVO sourceCodeDetail = null; 
			if(!StringUtils.isEmpty(request.getSourceCode())) {
				sourceCodeDetail = SourceServiceBO.getSourceCodeDetail(request.getSourceCode(), SourceCodeConstants.INTERNET_ORDER_SOURCE);
			}
			validationErrors = validateDeleteSCFloristReq(request, sourceCodeDetail);
			if (validationErrors == null || validationErrors.size() == 0) {			
			
				SourceVO sourceFloristVo = populateSCFlorists(request.getFlorists(), null, sourceCodeDetail);	
				sourceFloristVo.setRelatedSourceCode(sourceCodeDetail.getRelatedSourceCode());
				SourceServiceBO.deleteSCFlorists(sourceFloristVo);
				
				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;
			}
		} catch(Exception e) {
			logger.error("Error caught processing delete Source code florists request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");			
		}
		// Set failure response
		return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	
	private SourceCodeResponse processDeleteSCdependencies(
			SourceCodeRequest request, StringBuffer logMessage) {
		SourceCodeResponse response = new SourceCodeResponse();	
		List<String> validationErrors = new ArrayList<String>();
		try{
			
			if (request.getSourceCode() != null && request.getSourceCode().length()>0) {			
				SourceServiceBO.deleteSCdependencies(request);
				response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
				response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
				logDebugMessage(logMessage.toString());
				return response;
			}else{
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
				return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
			}
		}catch(Exception e){
			logger.error("Error caught processing delete Source code dependencies request: " + e.getMessage());	
			logMessage.append(e.getMessage() + "\n");	
		}
		
		return (SourceCodeResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	
	private UpdateSCAttributesResponse processUpdateSCAttributes(
			UpdateSourceCodeAttributesRequest request, StringBuffer logMessage) {
		UpdateSCAttributesResponse response = new UpdateSCAttributesResponse();	
		List<String> validationErrors = validateUpdateSCAttributesRequest(request);
		if (validationErrors == null || validationErrors.size() == 0) {
		
			try{
				
				if (request.getSourceCode() != null && request.getSourceCode().length()>0) {			
					SourceServiceBO.updateSCAttributes(request);
					response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
					response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
					logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
					logDebugMessage(logMessage.toString());
					return response;
				}else{
					validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
					return (UpdateSCAttributesResponse) setFailureResponse(response, validationErrors, logMessage);
				}
			}catch(Exception e){
				logger.error("Error caught processing update Source code attributes request: " + e.getMessage());	
				logMessage.append(e.getMessage() + "\n");	
			}
			
			return (UpdateSCAttributesResponse) setFailureResponse(response, validationErrors, logMessage);
		}
		
		// Set failure response
				return (UpdateSCAttributesResponse) setFailureResponse(response, validationErrors, logMessage);
	}
	
	
	private List<String> validateUpdateSCAttributesRequest(
			UpdateSourceCodeAttributesRequest request) {
		
		List<String> validationErrors = new ArrayList<String>();		

		if(StringUtils.isEmpty(request.getSourceCode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field Source Code is empty");
			}
		}
		
		if (StringUtils.isEmpty(request.getLocation())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.LOCATION_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field location name is empty");
			}
		} 		
		
		if (StringUtils.isEmpty(request.getAddress())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ADDRESS_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field address is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getCity())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.CITY_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field city is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getState())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.STATE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field state is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getZipcode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ZIPCODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field zipcode is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getCountry())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.COUNTRY_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field country is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getPhoneNumber())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.PHONE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field phone number is empty");
			}
		} 	
		

		return validationErrors;
		
	}

	/** Convenient method to validate the Create Source Code from portal. The
	 * mandatory non null parameters in the request are Template name, Location
	 * Requested URL, Order Source 
	 * @param request
	 * @return
	 */
	private List<String> validateCreateSCRequest(CreateSourceCodeRequest request) {

		List<String> validationErrors = new ArrayList<String>();		

		if (StringUtils.isEmpty(request.getTemplateName())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.TEMPLATE_NAME_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field template name is empty");
			}
		}

		if (StringUtils.isEmpty(request.getLocation())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.LOCATION_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field location name is empty");
			}
		}

		if (request.getOrderSource() == null || request.getOrderSource().size() == 0) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ORDER_SOURCE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field order source is empty");
			}
		} else {
			
			for (String orderSrc : request.getOrderSource()) {		
				if (OrderSource.getByValue(orderSrc) == null) {		
					try {
						validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ORDER_SOURCE_INVALID));
					} catch (Exception e) {
						logger.error("Error caught obtaning resource value"	+ e.getMessage());
						validationErrors.add("Mandatory field order source is empty");
					}
				}
			}
		}

		if (StringUtils.isEmpty(request.getWebsiteURL())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.REQUESTED_URL_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field requested URL is empty");
			}
		}
		
		if (StringUtils.isEmpty(request.getAddress())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ADDRESS_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field address is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getCity())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.CITY_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field city is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getState())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.STATE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field state is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getZipcode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ZIPCODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field zipcode is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getCountry())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.COUNTRY_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field country is empty");
			}
		} 	
		
		if (StringUtils.isEmpty(request.getPhoneNumber())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.PHONE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field phone number is empty");
			}
		} 	

		return validationErrors;
	}
	
	/** Convenient method to validate Update SC request.
	 * Mandatory parameters are template name, source code.	 
	 * @param request
	 * @return
	 */
	private List<String> validateUpdateSCTemplateReq(UpdateSCTemplateRequest request) {

		List<String> validationErrors = new ArrayList<String>();		

		if (StringUtils.isEmpty(request.getTemplateName())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.TEMPLATE_NAME_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field template name is empty");
			}
		} 		
		if(StringUtils.isEmpty(request.getSourceCode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field Source Code is empty");
			}
		}

		return validationErrors;
	}
	
	/** Validates the update source code florists request for valid input.
	 * @param request
	 * @param sourceCodeDetail
	 * @return
	 * @throws Exception 
	 * @throws FtdDataException 
	 */
	private List<String> validateUpdateSCFloristReq (UpdateSCFloristsRequest request, SourceVO sourceCodeDetail) throws FtdDataException, Exception {
		List<String> validationErrors = new ArrayList<String>();
		if(StringUtils.isEmpty(request.getSourceCode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field source code is empty");
			}
		} else if (sourceCodeDetail == null) {
			try {
					validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_INVALID));				
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Source code does not exist. Request cannot be processed");
			}
		}
		
		if(request.getPrimaryFlorist() == null && request.getSecondaryFlorists().size() == 0) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ATLEAST_ONE_FLORIST_REQURIED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field source code is empty");
			}
		} else  if (request.getSecondaryFlorists().size() > 10) {		
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.FLORISTS_RANGE_EXCEEDED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Florists choice cannot exceed 10");
			}
		}
		
		if(!StringUtils.isEmpty(request.getPrimaryFlorist())) {
			if(request.getSecondaryFlorists().contains(request.getPrimaryFlorist())) {
				try {
					validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.PRIMARY_CANNOT_BE_SECONDARY));
				} catch (Exception e) {
					logger.error("Error caught obtaning resource value"	+ e.getMessage());
					validationErrors.add("Florists choice cannot exceed 10");
				}
			}
		}
		
		return validationErrors;
	}
	
	/** Validates the service request
	 * @param request
	 * @return
	 */
	private List<String> validateGetSCFloristReq (SourceCodeRequest request) {
		List<String> validationErrors = new ArrayList<String>();
		if(StringUtils.isEmpty(request.getSourceCode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field source code is empty");
			}
		}
		return validationErrors;
	}	
	
	/** Validates the delete source code florists request for valid input.
	 * @param request
	 * @param sourceCodeDetail
	 * @return
	 * @throws Exception 
	 * @throws FtdDataException 
	 */
	private List<String> validateDeleteSCFloristReq (DeleteSCFloristsRequest request, SourceVO sourceCodeDetail) throws FtdDataException, Exception {
		List<String> validationErrors = new ArrayList<String>();
		if(StringUtils.isEmpty(request.getSourceCode())) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_REQUIRED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field source code is empty");
			}
		} else if (sourceCodeDetail == null) {
			try {
					validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.SOURCE_CODE_INVALID));				
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Source code does not exist. Request cannot be processed");
			}
		}
		
		if(request.getFlorists().size() == 0) {
			try {
				validationErrors.add(resrcBundle.getString(SourceCodeErrorConstants.ATLEAST_ONE_FLORIST_REQURIED));
			} catch (Exception e) {
				logger.error("Error caught obtaning resource value"	+ e.getMessage());
				validationErrors.add("Mandatory field source code is empty");
			}
		} 		
		
		return validationErrors;
	}

	/** returns the default source code for a given template name mapped in the global params.
	 * @param templateName
	 * @return
	 */
	private String getDefaultSourceCode(String templateName) {
		logger.info(":: getDefaultSourceCode ::");
		
		GlobalParmHandler paramHandler = (GlobalParmHandler) CacheManager
				.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");

		return paramHandler.getGlobalParm(SourceCodeConstants.MARKETING_CONFIG_CONTEXT,
				SourceCodeConstants.SYMPATHY_PARAM_NAME	+ StringUtils.upperCase(templateName));
	}

	/** Generate the SC from the reserved range for sympathy SCs.
	 * @return
	 * @throws Exception
	 */
	private int generateInternetSourceCode(String templateName) throws Exception {
		logger.info(":: generateInternetSourceCode ::");
		String partner = null;
		if(StringUtils.upperCase(templateName).indexOf(SourceCodeConstants.SCI_PARTNER) == 0) {
			partner = SourceCodeConstants.SCI_PARTNER;
		} else if (StringUtils.upperCase(templateName).indexOf(SourceCodeConstants.BATESVILLE_PARTNER) == 0) {
			partner = SourceCodeConstants.BATESVILLE_PARTNER;
		} 
		else if (StringUtils.upperCase(templateName).indexOf(SourceCodeConstants.LEGACY_PARTNER) == 0) {
			partner = SourceCodeConstants.LEGACY_PARTNER;
		}
		return SourceServiceBO.generateInternetSourceCode(partner);
	}

	/** Records the source code details into various tables.
	 * @param sourceVO
	 * @param isNewSCReq
	 * @return string - logger message, any error updating default email pgm Id will not stop SC creation, it is logged.
	 * @throws Exception
	 */
	private String recordSource(SourceVO sourceVO, boolean isNewSCReq) throws Exception {
		logger.info(":: recordSource ::");

		SourceRequestVO sourceRequestVO = new SourceRequestVO(sourceVO,
				SourceCodeConstants.WEBSITE_PORTAL_USER);
		
		SourceProgramRequestVO sourceProgramRequestVO = null;
		
		// there will be only one program ref record for each default template.
		if (sourceVO.getSrcProgramList() != null && sourceVO.getSrcProgramList().size() > 0 && isNewSCReq) {
			for (SourceProgramRefVO sprVO : sourceVO.getSrcProgramList()) {
				
				String programName = sprVO.getProgramName();
				SourceProgramRefVO sprVONew = null;
				
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
				Date now = new Date();
				String today = df.format(now);
				sprVONew = new SourceProgramRefVO(sourceVO.getId(),	programName, df.parse(today));				

				sourceProgramRequestVO = new SourceProgramRequestVO(sprVONew, SourceCodeConstants.WEBSITE_PORTAL_USER);
			}
		}
		
		return SourceServiceBO.recordSource(sourceRequestVO, sourceProgramRequestVO, sourceVO.getEmailProgramId());
	}
	
	/**
	 * @param requestedTemplate
	 * @param originalTemplate
	 * @param originalCompany
	 * @return
	 */
	private boolean isSameTemplate(String requestedTemplate, String originalTemplate, String originalCompany) {
		if(originalTemplate.indexOf("&") >= 0) {
			originalTemplate = originalTemplate.replace("&", "AND");			
		}		
		if(StringUtils.upperCase(requestedTemplate).indexOf(originalTemplate) >= 0) {
			if (StringUtils.upperCase(requestedTemplate).indexOf("COM") >= 0
					&& originalCompany.equalsIgnoreCase("FTD")) {
				return true;
			} else if (StringUtils.upperCase(requestedTemplate).indexOf("CA") >= 0
					&& originalCompany.equalsIgnoreCase("FTDCA")) {
				return true;
			}
		}		
		return false;
	}	
	
	/** Batesville cannot be updated to SCi and SCI cannot be updated to BV. Validate for the same.
	 * @param requestedTemplate
	 * @param originalTemplate
	 * @return
	 */
	private boolean isSameGroup(String requestedTemplate, String originalTemplate, String originalCompany) {
		
		if(requestedTemplate.indexOf(SourceCodeConstants.BATESVILLE_PARTNER) == 0 
				&& originalTemplate.indexOf(SourceCodeConstants.BATESVILLE_PARTNER) != 0) {
			return false;
		} 
		
		if(requestedTemplate.indexOf(SourceCodeConstants.SCI_PARTNER) == 0 
				&& originalTemplate.indexOf(SourceCodeConstants.SCI_PARTNER) != 0) {
			return false;
		}
		
		if (StringUtils.upperCase(requestedTemplate).indexOf("COM") >= 0 && !originalCompany.equalsIgnoreCase("FTD")) {
			return false;
		} 
		
		if(StringUtils.upperCase(requestedTemplate).indexOf("CA") >= 0 && !originalCompany.equalsIgnoreCase("FTDCA")) {
			return false;
		}
		
		return true;
	}
	
	/**Logs the debug message if the debug mode is enabled.	 * 
	 * @param message
	 */
	private void logDebugMessage(String message) {
		if (logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}
	
	private Timestamp getCurrentTimeStamp() {
		Date date= new Date();
		return new Timestamp(date.getTime());
	}	

	/**Populates the required data into sourceVO
	 * @param florists
	 * @param primaryFlorist
	 * @param sourceVO
	 * @return
	 */
	private SourceVO populateSCFlorists(List<String> florists, String primaryFlorist, SourceVO sourceVO) {
		if(!StringUtils.isEmpty(primaryFlorist)) {
			sourceVO.getSourceFloristPriorityList().add(new SourceFloristPriorityVO(primaryFlorist, 1));
		}		
		for (String floristId : florists) {
			if(!StringUtils.isEmpty(floristId)) {
				sourceVO.getSourceFloristPriorityList().add(new SourceFloristPriorityVO(floristId, 2));
			}
		}	
		
		sourceVO.setUpdatedBy(SourceCodeConstants.WEBSITE_PORTAL_USER);
		return sourceVO;		
	}	
	
	/** Creates the getSourceCodeFlorists success response.
	 * @param florists
	 * @param logMessage
	 * @param sourceCode
	 * @return
	 */
	private GetSCFloristsResponse getSCfloristsSuccessResp(List<Florist> florists, StringBuffer logMessage, String sourceCode) {
		GetSCFloristsResponse response = new GetSCFloristsResponse();
		
		if(florists == null || florists.size() ==0) {
			response.getErrorMessage().add(resrcBundle.getString(SourceCodeErrorConstants.NO_FLORIST_SAVED));
			logMessage.append("Error Message : " + resrcBundle.getString(SourceCodeErrorConstants.NO_FLORIST_SAVED) + "\n");			
		} else {
			for (Florist florist : florists) {
				if(florist.isPrimary()) {
					response.setPrimaryFlorist(florist);
				} else {
					response.getSecondaryFlorists().add(florist);
				}
			}
		}
		response.setSourceCode(sourceCode);
		response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_CODE));
		response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
		logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.SUCCESS_RESPONSE_MESSAGE));
		logDebugMessage(logMessage.toString());
		
		return response;
	}
	
	/** Sets the common failure response for all the service calls.
	 * @param response
	 * @param validationErrors
	 * @param logMessage
	 * @return
	 */
	private Object setFailureResponse(SourceCodeResponse response, List<String> validationErrors, StringBuffer logMessage) {
		response.setResponseCode(resrcBundle.getString(SourceCodeErrorConstants.FAILURE_RESPONSE_CODE));
		response.setResponseStatus(resrcBundle.getString(SourceCodeErrorConstants.FAILURE_RESPONSE_MESSAGE));
		if (validationErrors == null || validationErrors.size() == 0) {
			response.getErrorMessage().add(resrcBundle.getString(SourceCodeErrorConstants.INTERNAL_ERROR));
			logMessage.append("Error Message : " + resrcBundle.getString(SourceCodeErrorConstants.INTERNAL_ERROR) + "\n");			
		}
		
		for (String error : validationErrors) {
			response.getErrorMessage().add(error);
			logMessage.append("Error Message : " + error + "\n");
		}
		logMessage.append("Response status : " + resrcBundle.getString(SourceCodeErrorConstants.FAILURE_RESPONSE_MESSAGE));	
		logDebugMessage(logMessage.toString());
		return response;
	}
}