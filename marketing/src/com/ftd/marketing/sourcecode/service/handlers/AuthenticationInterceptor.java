package com.ftd.marketing.sourcecode.service.handlers;

import java.util.List;
import java.util.Map;

import org.apache.cxf.binding.soap.interceptor.SoapHeaderInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;

import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.service.util.AuthenticationUtil;
import com.ftd.service.util.Client;
import com.ftd.service.util.Credentials;

/**
 * @author smeka
 *
 */
public class AuthenticationInterceptor extends SoapHeaderInterceptor {
	
	private Logger logger = new Logger("com.ftd.marketing.sourcecode.service.handlers.AuthenticationInterceptor");

	@SuppressWarnings("unchecked")
	@Override
	public void handleMessage(Message message) throws Fault {		
		logger.info("########## AuthenticationInterceptor.handleMessage() #############");
	
		Map<String, List<String>> http_headers =  
				(Map<String, List<String>>)message.get(Message.PROTOCOL_HEADERS);
		
		List<String> userList = (List<String>) http_headers.get(SourceCodeConstants.CLIENT_APP_LOGIN_ID);
        List<String> passList = (List<String>) http_headers.get(SourceCodeConstants.CLIENT_APP_LOGIN_PWD);
 
        String username = null;
        String password = null;
 
        if(userList != null){
        	username = userList.get(0).toString();
        }
 
        if(passList != null){
        	password = passList.get(0).toString();
        }
        
        Client client = new Client();
        Credentials credentials = new Credentials(username, password);
        client.setCredentials(credentials);
        try {
	        if(!(AuthenticationUtil.authenticateClient(client))) {
	        	throw new Fault(new Exception("Authentication failed"));
	        }
        } catch(Exception e){
        	throw new Fault(e);
        }
	}
}
