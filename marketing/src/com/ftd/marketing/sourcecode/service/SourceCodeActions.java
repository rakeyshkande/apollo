package com.ftd.marketing.sourcecode.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.ftd.marketing.sourcecode.model.CreateSourceCodeRequest;
import com.ftd.marketing.sourcecode.model.CreateSourceCodeResponse;
import com.ftd.marketing.sourcecode.model.GetSCFloristsResponse;
import com.ftd.marketing.sourcecode.model.DeleteSCFloristsRequest;
import com.ftd.marketing.sourcecode.model.SourceCodeRequest;
import com.ftd.marketing.sourcecode.model.SourceCodeResponse;
import com.ftd.marketing.sourcecode.model.UpdateSCFloristsRequest;
import com.ftd.marketing.sourcecode.model.UpdateSCTemplateRequest;
import com.ftd.marketing.sourcecode.model.UpdateSourceCodeAttributesRequest;


@WebService(name = "SourceCodeActions", targetNamespace = "http://service.sourcecode.marketing.ftd.com/")
public interface SourceCodeActions {

	/**
	 * 
	 * @param createSCRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.CreateSourceCodeResponse
	 */
	@WebMethod
	public CreateSourceCodeResponse createSCwithTemplate(
			@WebParam(name = "createSCRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") CreateSourceCodeRequest createSCRequest);
	
	/**
	 * 
	 * @param updateSCRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.SourceCodeResponse
	 */
	@WebMethod
	public SourceCodeResponse updateSourceCodeTemplate(
			@WebParam(name = "updateSCRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") UpdateSCTemplateRequest updateSCRequest);
	
	/**
	 * 
	 * @param updateSCFloristsRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.SourceCodeResponse
	 */
	@WebMethod
	public SourceCodeResponse updateSourceCodeFlorists(
			@WebParam(name = "updateSCFloristsRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") UpdateSCFloristsRequest updateSCFloristsRequest);
	
	/**
	 * @param getSourceCodeFloristsRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.GetSCFloristsResponse
	 */
	@WebMethod
	public GetSCFloristsResponse getSourceCodeFlorists(
			@WebParam(name = "getSourceCodeFloristsRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") SourceCodeRequest getSourceCodeFloristsRequest);

	/**
	 * @param addSCFloristRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.SourceCodeResponse
	 */
	@WebMethod
	public SourceCodeResponse addSourceCodeFlorists(
			@WebParam(name = "addSCFloristRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") UpdateSCFloristsRequest addSCFloristRequest);
	
	/**
	 * @param deleteSCFloristRequest
	 * @return returns com.ftd.marketing.sourcecode.service.model.SourceCodeResponse
	 */
	@WebMethod
	public SourceCodeResponse deleteSourceCodeFlorists(
			@WebParam(name = "deleteSCFloristRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") DeleteSCFloristsRequest deleteSCFloristRequest);
	
	/**
	 * @param deleteSCdependencies
	 * @return returns com.ftd.marketing.sourcecode.service.model.SourceCodeResponse
	 */
	@WebMethod
	public SourceCodeResponse deleteSCdependencies(
			@WebParam(name = "deleteSCdependenyRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") SourceCodeRequest deleteSCdependenyRequest);
	
	@WebMethod
	public SourceCodeResponse updateSCAttributes(
			@WebParam(name = "updateSCAttributesRequest", targetNamespace = "http://service.sourcecode.marketing.ftd.com/") UpdateSourceCodeAttributesRequest updateSCAttributesRequest);


}
