/**
 * 
 */
package com.ftd.marketing.sourcecode.constants;


/**
 * @author smeka
 * 
 */
public class SourceCodeConstants {

	public static final String PHONE_ORDER_SOURCE = "P";
	
	public static final String INTERNET_ORDER_SOURCE = "I";	
	
	public static final String MARKETING_CONFIG_CONTEXT = "MARKETING_CONFIG";
	
	public static final String SYMPATHY_PARAM_NAME = "SYMPATHY.";
	
	public static final String SYMPATHY_SC_MIN_VAL = "SYMPATHY_SC_MIN_VALUE";
	
	public static final String SYMPATHY_SC_MAX_VAL = "SYMPATHY_SC_MAX_VALUE";
	
	public static final String WEBSITE_PORTAL_USER = "WEBSITE PORTAL";
	
	public static final String BATESVILLE_PARTNER = "BV";
	
	public static final String SCI_PARTNER = "SCI";
	
	public static final String LEGACY_PARTNER = "LEGACY";
	
	public static final String SERVCIE_CONTEXT = "SERVICE";
	
	public static final String CLIENT_APP_LOGIN_ID = "CLIENT_LOGIN_ID";
	
	public static final String CLIENT_APP_LOGIN_PWD = "CLIENT_PWD";
	
}
