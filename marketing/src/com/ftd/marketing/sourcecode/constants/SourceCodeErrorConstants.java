/**
 * 
 */
package com.ftd.marketing.sourcecode.constants;

/**
 * @author smeka
 * 
 */
public class SourceCodeErrorConstants {

	public static final String TEMPLATE_NAME_REQUIRED = "sourcecode.template.required";

	public static final String TEMPLATE_NAME_INVALID = "sourcecode.template.invalid";
	
	public static final String TEMPLATE_NAME_UNMODIFIED = "sourcecode.template.unchanged";

	public static final String LOCATION_REQUIRED = "sourcecode.location.required";
	
	public static final String ADDRESS_REQUIRED = "sourcecode.address.required";

	public static final String CITY_REQUIRED = "sourcecode.city.required";
	
	public static final String STATE_REQUIRED = "sourcecode.state.required";
	
	public static final String ZIPCODE_REQUIRED = "sourcecode.zipcode.required";
	
	public static final String COUNTRY_REQUIRED = "sourcecode.country.required";
	
	public static final String PHONE_REQUIRED = "sourcecode.phonenumber.required";

	public static final String ORDER_SOURCE_INVALID = "sourcecode.order_source.invalid";

	public static final String ORDER_SOURCE_REQUIRED = "sourcecode.order_source.required";
	
	public static final String SOURCE_CODE_REQUIRED = "sourcecode.required";

	public static final String SOURCE_CODE_INVALID = "sourcecode.invalid";

	public static final String REQUESTED_URL_REQUIRED = "sourcecode.requested.url.required";

	public static final String INTERNAL_ERROR = "apollo.internal.error";

	public static final String SUCCESS_RESPONSE_CODE = "response.code.success";

	public static final String SUCCESS_RESPONSE_MESSAGE = "response.message.success";

	public static final String FAILURE_RESPONSE_CODE = "response.code.failure";

	public static final String FAILURE_RESPONSE_MESSAGE = "response.message.failure";
	
	public static final String INVALID_ORDER_SOURCE_FOR_LEGACY = "sourcecode.order_source.invalid.legacy";
	
	public static final String INVALID_LEGACY_UPDATE_REQUEST = "sourcecode.legacy.update";
	
	public static final String INVALID_SOURCE_TYPE_UPDATE = "sourcecode.template.invalid.update";
	
	public static final String ATLEAST_ONE_FLORIST_REQURIED = "sourcecode.florist.required";
	
	public static final String ONE_PRIMARY_FLORIST_ALLOWED = "sourcecode.primary.florist.invalid";
	
	public static final String FLORISTS_RANGE_EXCEEDED = "sourcecode.florists.number.exceeded";
	
	public static final String PRIMARY_CANNOT_BE_SECONDARY = "sourcecode.florists.primary.sameas.secondary";
	
	public static final String NO_FLORIST_SAVED = "sourcecode.florists.doesnt.exist";
	
	public static final String AUTHENTICATION_FAILED = "Sympathy Service Authentication failed.";
	
	public static final String CANNOT_AUTHENTICATE = "Could not Authenticate the client.";
	
	public static final String INVALID_FLORISTS = "sourcecode.florists.invalid";
	
	public static final String PRIMARY_ALREADY_EXISTS = "sourcecode.primary.florist.exists";
	
	public static final String FLORIST_ALREADY_EXISTS = "sourcecode.florist.already.exists";

}
