package com.ftd.marketing.sourcecode.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Florist {
	
	@XmlElement(required = true)
	private String floristMemeberId;

	@XmlElement(required = true)
	private String floristName;
	
	@XmlElement(required = true)
	private String addressLine;
	
	@XmlElement(required = true)
	private String city;
	
	@XmlElement(required = true)
	private String state;
	
	@XmlElement(required = true)
	private String zipCode;
	
	@XmlElement(required = true)
	private String phoneNumber;
	
	@XmlElement(required = true)
	private String latitude;
	
	@XmlElement(required = true)
	private String longitude;
	
	private boolean isPrimary = false;
	
	private Date lastUpdatedOn = null;
	
	public String getFloristMemeberId() {
		return floristMemeberId;
	}

	public void setFloristMemeberId(String floristMemeberId) {
		this.floristMemeberId = floristMemeberId;
	}

	public String getFloristName() {
		return floristName;
	}

	public void setFloristName(String floristName) {
		this.floristName = floristName;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Date getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}		

}
