package com.ftd.marketing.sourcecode.model;

import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;

/**
 * @author smeka
 *
 */
public enum OrderSource {
	
	PHONE_ORDER_SOURCE(SourceCodeConstants.PHONE_ORDER_SOURCE),
	INTERNET_ORDER_SOURCE(SourceCodeConstants.INTERNET_ORDER_SOURCE);
	
	private String value;

	/**
	 * @param value
	 */
	private OrderSource(String value) {
		this.value = value;
	}
	
	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}
	
	/** Returns the Order Source for a given value. If no match found, returns null.
	 * @param val
	 * @return
	 */
	public static OrderSource getByValue(String val) {
		OrderSource[] orderSource = OrderSource.values();
		for (OrderSource orderSrc : orderSource) {
			if(orderSrc.value.equalsIgnoreCase(val)){
				return orderSrc;
			}
		}
		return null;
	}
	
	/** Returns all the order source values.
	 * @return
	 */
	public static OrderSource[] getValues() {
		return OrderSource.values();
	}
	
	
}
