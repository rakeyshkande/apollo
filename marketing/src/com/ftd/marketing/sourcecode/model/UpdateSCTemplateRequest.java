package com.ftd.marketing.sourcecode.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateSCTemplateRequest extends SourceCodeRequest {
	
	@XmlElement(required = true)
	protected String location;
	
	@XmlElement(required = true)
	protected String templateName;
	
	@XmlElement(required = true)
	protected String websiteURL;
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}


}
