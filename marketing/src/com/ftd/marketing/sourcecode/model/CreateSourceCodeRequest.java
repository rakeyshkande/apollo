/**
 * 
 */
package com.ftd.marketing.sourcecode.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author smeka
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateSourceCodeRequest extends SourceCodeRequest {
	
protected String bonusBank;
	
	protected String bonusPSTCode;
	
	protected String defaultSourceCode;
	
	protected String freeGift;
	
	protected String fuelSurchargeAmt;
	
	protected String invoicePassword;
	
	@XmlElement(required = true)
	protected String location;	

	protected String mpRedemptionRate;	
	
	@XmlElement(name = "PCardExpDate")
	protected String pCardExpDate;	
	
	@XmlElement(name = "PCardNumber")
	protected String pCardNumber;	
	
	@XmlElement(name = "PCardType")
	protected String pCardType;
	
	protected String pstCode;	
	
	protected String relatedPhoneSourceCode;
	
	protected Date scEndDate;
	
	protected String sourceType;
	
	protected String splComments;
	
	protected String surchargeDesc;	
	
	@XmlElement(required = true)
	protected String templateName;

	@XmlElement(required = true)
	protected List<String> orderSource;
	
	@XmlElement(required = true)
	protected String websiteURL;
	
	protected String legacyId;
	
	
	@XmlElement(required = true)
	protected String address;
	
	@XmlElement(required = true)
	protected String city;
	
	@XmlElement(required = true)
	protected String state;
	
	@XmlElement(required = true)
	protected String zipcode;
	
	@XmlElement(required = true)
	protected String country;
	
	@XmlElement(required = true)
	protected String phoneNumber;
	
	
	public String getBonusBank() {
		return bonusBank;
	}

	public void setBonusBank(String bonusBank) {
		this.bonusBank = bonusBank;
	}

	public String getBonusPSTCode() {
		return bonusPSTCode;
	}

	public void setBonusPSTCode(String bonusPSTCode) {
		this.bonusPSTCode = bonusPSTCode;
	}

	public String getDefaultSourceCode() {
		return defaultSourceCode;
	}

	public void setDefaultSourceCode(String defaultSourceCode) {
		this.defaultSourceCode = defaultSourceCode;
	}

	public String getFreeGift() {
		return freeGift;
	}

	public void setFreeGift(String freeGift) {
		this.freeGift = freeGift;
	}

	public String getFuelSurchargeAmt() {
		return fuelSurchargeAmt;
	}

	public void setFuelSurchargeAmt(String fuelSurchargeAmt) {
		this.fuelSurchargeAmt = fuelSurchargeAmt;
	}

	public String getInvoicePassword() {
		return invoicePassword;
	}

	public void setInvoicePassword(String invoicePassword) {
		this.invoicePassword = invoicePassword;
	}	
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMpRedemptionRate() {
		return mpRedemptionRate;
	}

	public void setMpRedemptionRate(String mpRedemptionRate) {
		this.mpRedemptionRate = mpRedemptionRate;
	}
	
	public String getpCardExpDate() {
		return pCardExpDate;
	}
	
	public void setpCardExpDate(String pCardExpDate) {
		this.pCardExpDate = pCardExpDate;
	}	
	
	public String getpCardNumber() {
		return pCardNumber;
	}

	public void setpCardNumber(String pCardNumber) {
		this.pCardNumber = pCardNumber;
	}
	
	public String getpCardType() {
		return pCardType;
	}

	public void setpCardType(String pCardType) {
		this.pCardType = pCardType;
	}

	public String getPstCode() {
		return pstCode;
	}

	public void setPstCode(String pstCode) {
		this.pstCode = pstCode;
	}
	
	public String getRelatedPhoneSourceCode() {
		return relatedPhoneSourceCode;
	}

	public void setRelatedPhoneSourceCode(String relatedPhoneSourceCode) {
		this.relatedPhoneSourceCode = relatedPhoneSourceCode;
	}

	public Date getScEndDate() {
		return scEndDate;
	}

	public void setScEndDate(Date scEndDate) {
		this.scEndDate = scEndDate;
	}	

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSplComments() {
		return splComments;
	}

	public void setSplComments(String splComments) {
		this.splComments = splComments;
	}

	public String getSurchargeDesc() {
		return surchargeDesc;
	}

	public void setSurchargeDesc(String surchargeDesc) {
		this.surchargeDesc = surchargeDesc;
	}
	
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public List<String> getOrderSource() {
		if(orderSource == null) {
			orderSource = new ArrayList<String>();
		}
		return orderSource;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getLegacyId() {
		// TODO Auto-generated method stub
		return legacyId;
	}
	
	public void setLegacyId(String legacyId){
		this.legacyId = legacyId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}
