/**
 * 
 */
package com.ftd.marketing.sourcecode.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author smeka
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateSourceCodeResponse extends SourceCodeResponse {	
	
	protected String internetSourceCode;
	
	protected String phoneSourceCode;
	

	public String getInternetSourceCode() {
		return internetSourceCode;
	}

	public void setInternetSourceCode(String internetSourceCode) {
		this.internetSourceCode = internetSourceCode;
	}
	
	public void setPhoneSourceCode(String phoneSourceCode) {
		this.phoneSourceCode = phoneSourceCode;
	}

	public String getPhoneSourceCode() {
		return phoneSourceCode;
	}

	
}
