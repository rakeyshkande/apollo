/**
 * 
 */
package com.ftd.marketing.sourcecode.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SourceCodeResponse {
	
	@XmlElement(nillable = true)
	protected List<String> errorMessage;
	
	@XmlElement(nillable = true)
	protected String location;
	
	@XmlElement(required = true)
	protected String responseCode;
	
	@XmlElement(required = true)
	protected String responseStatus;
	
	@XmlElement(nillable = true)
	protected String sourceCode;

	public List<String> getErrorMessage() {
		if(errorMessage == null) {
			errorMessage = new ArrayList<String>();
		}
		return errorMessage;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}
}
