package com.ftd.marketing.sourcecode.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateSCFloristsRequest extends SourceCodeRequest {
	
	private String primaryFlorist;	
	
	private List<String> secondaryFlorists;		

	public List<String> getSecondaryFlorists() {
		if(secondaryFlorists == null) {
			secondaryFlorists = new ArrayList<String>();
		}
		return secondaryFlorists;
	}

	public String getPrimaryFlorist() {
		return primaryFlorist;
	}

	public void setPrimaryFlorist(String primaryFlorist) {
		this.primaryFlorist = primaryFlorist;
	}
}
