package com.ftd.marketing.sourcecode.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GetSCFloristsResponse extends SourceCodeResponse {
	
	private Florist primaryFlorist;
	
	private ArrayList<Florist> secondaryFlorists;

	public Florist getPrimaryFlorist() {
		return primaryFlorist;
	}

	public void setPrimaryFlorist(Florist primaryFlorist) {
		this.primaryFlorist = primaryFlorist;
	}

	public ArrayList<Florist> getSecondaryFlorists() {
		if(secondaryFlorists == null) {
			secondaryFlorists = new ArrayList<Florist>();
		}
		return secondaryFlorists;
	}	

}
