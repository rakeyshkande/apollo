package com.ftd.marketing.sourcecode.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author smeka
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class SourceCodeRequest {	
	private String sourceCode;

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}	
}
