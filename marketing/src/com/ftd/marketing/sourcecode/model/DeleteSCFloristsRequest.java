package com.ftd.marketing.sourcecode.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author smeka
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DeleteSCFloristsRequest extends SourceCodeRequest {
	
	private List<String> florists;		

	public List<String> getFlorists() {
		if(florists == null) {
			florists = new ArrayList<String>();
		}
		return florists;
	}
}
