package com.ftd.marketing.sourcecode.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateSourceCodeAttributesRequest extends SourceCodeRequest {
	
	@XmlElement(required = true)
	protected String location;
	
	@XmlElement(required = true)
	protected String address;
	
	@XmlElement(required = true)
	protected String city;
	
	@XmlElement(required = true)
	protected String state;
	
	@XmlElement(required = true)
	protected String zipcode;
	
	@XmlElement(required = true)
	protected String country;
	
	@XmlElement(required = true)
	protected String phoneNumber;
	
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


}
