package com.ftd.marketing.filter;

import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.util.ServletHelper;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class DataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.marketing.filter.DataFilter");

    private static final String PARM_MAP = "parameters";
    
    // parm to sent to front end
    private static final String SITE_NAME = "site_name";
    private static final String SSL_SITE_NAME = "ssl_site_name";
    
    // parm in DB
    private static final String GLOBAL_SITENAME = "sitename";
    private static final String GLOBAL_SITESSLPORT = "sitesslport";
    private static final String MARKETING_APPCONTEXT = "appcontext";
    private static final String GLOBAL_CONFIG_CONTEXT = "GLOBAL_CONFIG";

    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        logger.debug("Entering DataFilter...");
        String siteName = null;
        String sslSiteName = null;

        try {
            HashMap parameters  = (HashMap)request.getAttribute(PARM_MAP);
                
            if(parameters == null)
            {
              parameters = new HashMap();
            }        
          
            siteName = (String)request.getParameter(SITE_NAME);
            sslSiteName = (String)request.getParameter(SSL_SITE_NAME);
            
            if(siteName == null || siteName.equals("") || sslSiteName == null || sslSiteName.equals("")) {
                String site = ConfigurationUtil.getInstance().getFrpGlobalParm(GLOBAL_CONFIG_CONTEXT, GLOBAL_SITENAME);
                String appContext = ConfigurationUtil.getInstance().getFrpGlobalParm(MarketingConstants.MARKETING_CONFIG_CONTEXT, MARKETING_APPCONTEXT);
                String sslPort = ConfigurationUtil.getInstance().getFrpGlobalParm(GLOBAL_CONFIG_CONTEXT, GLOBAL_SITESSLPORT);
                
                siteName = site + "/" + appContext + "/";
                String sslSite = ServletHelper.switchServerPort(site, sslPort);
                sslSiteName = sslSite + "/" + appContext + "/";
            }
            
            parameters.put(SITE_NAME, siteName != null ? siteName.trim() : "");
            parameters.put(SSL_SITE_NAME, sslSiteName != null ? sslSiteName.trim() : "");
             
            request.setAttribute(PARM_MAP, parameters);
            chain.doFilter(request, response);
        } catch (Exception e) {
            throw new ServletException (e.getMessage());
        }
    }


}
