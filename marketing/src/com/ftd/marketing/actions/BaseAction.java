package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.GuiPaginationVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * Common Marketing characteristics of the Struts Action class.
 *
 * @author JP Puzon
 */
public abstract class BaseAction extends Action {
    protected static Log logger = LogFactory.getLog(BaseAction.class.getName());
    protected static final String REQUEST_KEY_ACTION_TYPE = "action_type";
    protected static final String REQUEST_KEY_RESULT = "result";
    protected static final String REQUEST_KEY_ERROR_MESSAGE = "error_message";
    protected static final String REQUEST_VALUE_ACTION_TYPE_LOAD = "load";
    protected static final String REQUEST_VALUE_ACTION_TYPE_RELOAD = "reload";
    protected static final String REQUEST_VALUE_ACTION_TYPE_INSERT = "insert";
    protected static final String REQUEST_VALUE_ACTION_TYPE_DELETE = "delete";
    protected static final String REQUEST_VALUE_ACTION_TYPE_UPDATE = "update";
    protected static final String REQUEST_VALUE_ACTION_TYPE_SEARCH = "search";
    protected static final String REQUEST_VALUE_ACTION_TYPE_PREFERRED_FLORIST_VALIDATION = "preferredFloristValidation";    
    protected static final String REQUEST_VALUE_ACTION_TYPE_NEW = "new";
    protected static final String ERROR_PAGE = "errorPage";
    protected static final String BASE_CONFIG = "BASE_CONFIG";
    protected static final String BASE_URL = "BASE_URL";
    private static final String REQUEST_KEY_SECURITY_TOKEN = "securitytoken";
    private static final String REQUEST_KEY_CONTEXT = "context";
    private static final String REQUEST_KEY_APP_CONTEXT = "applicationcontext";

    // FTD customer service call header data.
    private static final String REQUEST_KEY_CALL_CS_NUMBER = "call_cs_number";
    private static final String REQUEST_KEY_CALL_TYPE_FLAG = "call_type_flag";
    private static final String REQUEST_KEY_CALL_BRAND_NAME = "call_brand_name";
    private static final String REQUEST_KEY_CALL_DNIS = "call_dnis";

    // FTD call log data.
    private static final String REQUEST_KEY_T_CALL_LOG_ID = "t_call_log_id";

    // FTD timer data.
    private static final String REQUEST_KEY_T_ENTITY_HISTORY_ID = "t_entity_history_id";
    private static final String REQUEST_KEY_T_COMMENT_ORIGIN_TYPE = "t_comment_origin_type";

    // FTD start origin.
    private static final String REQUEST_KEY_START_ORIGIN = "start_origin";
    
    

    // These are field members that store FTD context metadata for each
    // servlet instance.
    // These members are initialized by initFtdContext().
    // FTD app security data.
    protected String context = null;
    protected String applicationContext = null;

    // FTD customer service call header data.
    protected String callCsNumber = null;
    protected String callTypeFlag = null;
    protected String callBrandName = null;
    protected String callDnis = null;

    // FTD call log data.
    protected String tCallLogId = null;

    // FTD timer data.
    protected String tEntityHistoryId = null;
    protected String tCommentOriginType = null;

    // FTD start origin.
    protected String startOrigin = null;

    /**
    * Convenience method that initializes FTD-related context metadata
    * that is common to all servlets.
    * @param cleanFtdParams
    */
    protected void initFtdContext(Map cleanFtdParams) {
        // Initialize security context.
        context = (String) cleanFtdParams.get(REQUEST_KEY_CONTEXT);
        applicationContext = (String) cleanFtdParams.get(REQUEST_KEY_APP_CONTEXT);

        // FTD customer service call header data.
        callCsNumber = (String) cleanFtdParams.get(REQUEST_KEY_CALL_CS_NUMBER);
        callTypeFlag = (String) cleanFtdParams.get(REQUEST_KEY_CALL_TYPE_FLAG);
        callBrandName = (String) cleanFtdParams.get(REQUEST_KEY_CALL_BRAND_NAME);
        callDnis = (String) cleanFtdParams.get(REQUEST_KEY_CALL_DNIS);

        // FTD call log data.
        tCallLogId = (String) cleanFtdParams.get(REQUEST_KEY_T_CALL_LOG_ID);

        // FTD timer data.
        tEntityHistoryId = (String) cleanFtdParams.get(REQUEST_KEY_T_ENTITY_HISTORY_ID);
        tCommentOriginType = (String) cleanFtdParams.get(REQUEST_KEY_T_COMMENT_ORIGIN_TYPE);

        // FTD start origin.
        startOrigin = (String) cleanFtdParams.get(REQUEST_KEY_START_ORIGIN);
    }

    public abstract ActionForward execute(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException;

    /**
           * Retrieve name and location of XSL file
       *     1. get file lookup name
       *     2. get real file name
       *     3. get xsl file location
           *
           * @param ActionMapping      - Stuts mapping used for actionforward lookup
       * @param HttpServletRequest - user request
           * @param String             - xsl file name
       *
           * @return File              - XSL File name with real path
       *
           * @throws none
           */
    protected File getXSL(ActionMapping mapping, HttpServletRequest request,
        String xslFileLookUpName) {
        // get real file name
        ActionForward forward = mapping.findForward(xslFileLookUpName);

        // get xsl file location
        return new File(request.getSession().getServletContext().getRealPath(forward.getPath()));
    }

    protected void goToPage(ActionMapping mapping, HttpServletRequest request,
        HttpServletResponse response, Document responseDoc,
        String xslFileName) throws IOException, ServletException {
        try {
            File xslFile = getXSL(mapping, request, xslFileName);
            TraxUtil.getInstance().transform(request, response, responseDoc,
                xslFile,
                (HashMap) request.getAttribute(
                    MarketingConstants.CONS_APP_PARAMETERS));
        } catch (Throwable t) {
            logger.error("goToPage: Error: ", t);
        }
    }

    /*    protected boolean lockCustomerRecord(Connection conn, String customerID,
            Map cleanFtdParams, String securityToken)
            throws EntityLockedException, Exception {
            boolean successfulLock = false;

            try {
                LockUtil lockUtil = new LockUtil(conn);

                UserInfo user = SecurityServiceBO.getUserInfo(securityToken);

                if (user != null) {
                    if (lockUtil.obtainLock(
                                MarketingConstants.CUSTOMER_LOCK_ENTITY_TYPE,
                                customerID, user.getUserID(),
                                extractSessionId(cleanFtdParams))) {
                        successfulLock = true;
                    }
                }
            } finally {
            }

            return successfulLock;
        }
    */
    public String lookupCurrentUserId(String securityToken)
        throws Exception {
        String userId = "";

        if (SecurityManager.getInstance().getUserInfo(securityToken) != null) {
            userId = SecurityManager.getInstance().getUserInfo(securityToken)
                                    .getUserID();
        }

        return userId;
    }

    protected HashMap cleanHttpParameters(HttpServletRequest request) {
        HashMap result = new HashMap();

        for (Enumeration i = request.getParameterNames(); i.hasMoreElements();) {
            String key = (String) i.nextElement();
            String value = request.getParameter(key);

            if ((value != null) && (value.trim().length() > 0)) {
                result.put(key, value.trim());
            }
        }

        return result;
    }

    protected void appendPageDataToXml(Document responseDoc,
        HashMap metadataMap) {
        DOMUtil.addSection(responseDoc, "pageData", "data", metadataMap, false);
    }

    protected GuiPaginationVO extractGuiPagination(Map cleanFtdParams) {
        return new GuiPaginationVO(new Integer(
                (String) ((cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_PAGE_NUMBER) == null) ? "0"
                                                                  : (cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_PAGE_NUMBER)))),
            new Integer((String) ((cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_PAGE_MAX_RECORD_COUNT) == null)
                ? "0"
                : (cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_PAGE_MAX_RECORD_COUNT)))),
            new Integer((String) ((cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_RECORD_NUMBER) == null) ? "0"
                                                                    : (cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_RECORD_NUMBER)))),
            new Integer((String) ((cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_RECORD_COUNT_TOTAL) == null) ? "0"
                                                                         : (cleanFtdParams.get(
                    GuiPaginationVO.MAP_KEY_RECORD_COUNT_TOTAL)))));
    }

    protected void printXml(Document responseDoc) throws Exception {
        StringWriter sw = new StringWriter(); //string representation of xml document
        DOMUtil.print(responseDoc, new PrintWriter(sw));
        logger.debug("printXml: request data DOM = \n" + sw.toString());
    }

    /*
        protected UserInfo extractUserInfo(Map cleanFtdParams)
            throws FtdSecurityException {
            // get security token
            String sessionId = BaseAction.extractSessionId(cleanFtdParams);

            if (sessionId == null) {
                throw new FtdSecurityException(
                    "extractUserInfo: User session ID is null.");
            }

            UserInfo userInfo = null;

            try {
                // get user ID
                // use security manager to get user info object. extract csr_id from it.
                // check for null pointers
                userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            } catch (Exception e) {
                throw new FtdSecurityException(
                    "extractUserInfo: User session ID verification failed: " +
                    sessionId, e);
            }

            if (userInfo == null) {
                throw new FtdSecurityException(
                    "extractUserInfo: User session ID record could not be found: " +
                    sessionId);
            }

            // Add any pertinent user parameters which need to be returned
            // to the client.
            cleanFtdParams.put("operatorId", userInfo.getUserID());
            cleanFtdParams.put("operatorFirstName", userInfo.getFirstName());
            cleanFtdParams.put("operatorLastName", userInfo.getLastName());

            return userInfo;
        }
    */
    protected void appendSecurityPermission(Map cleanFtdParams,
        SecurityResource resource, SecurityPermission permission)
        throws FtdSecurityException {
        if (SecurityServiceBO.assertPermission(extractSessionId(cleanFtdParams),
                    resource, permission)) {
            // Add the permission to the params.
            cleanFtdParams.put(resource.toString(), "Y");
        }
    }

    protected String extractSessionId(Map cleanFtdParams) {
        return (String) cleanFtdParams.get(REQUEST_KEY_SECURITY_TOKEN);
        //        return "FTD_GUID_10323358010155648063308421399370-12548460550-7407807470-2046635676013981173030205226370113624679920-348377806016605573041-1885770612415046532782061472589767186-1012330199244803411327212";
    }
    
    /**
     * This method is responsible for retrieving the base url needed when redirecting to an
     * application outside of the marketing module.
     * @return
     */
    protected String retrieveBaseURL() throws Exception {
        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String baseUrl = cu.getFrpGlobalParm(BASE_CONFIG,BASE_URL);
        
        return baseUrl;
    }    
    
    /** #13046 - The method is used set all the permissions OFF when the source code detail is to be shown read only, irrespective of user roles/permissions.
     * @param cleanFtdParams
     */
    protected void setReadonlyPermissions(Map cleanFtdParams) {
    	 cleanFtdParams.put(SecurityResource.SOURCE_DEFINITION_DATA.toString(),  "N");
    	 //cleanFtdParams.put(SecurityResource.SOURCE_BILLING_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.SOURCE_PAYMENT_METHOD_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.SOURCE_PCARD_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.SOURCE_PRICE_HEADER_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.SOURCE_PROMOTION_CODE_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.SOURCE_SNH_DATA.toString(), "N");
    	 cleanFtdParams.put(SecurityResource.RDMPTN_RATE_DATA.toString(), "N");    	 
    }

}
