package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.ProductFeedVO;

import com.ftd.marketing.bo.ProductServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.ProductRequestVO;

import com.ftd.osp.utilities.xml.DOMUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author JP Puzon
 */
public class ProductFeedMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(ProductFeedMaintAction.class.getName());
    private static final String _REQUEST_KEY_PRODUCT_ID = "requestProductId";

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = "ProductFeedMaint";
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // Provide default parameter values.
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            logger.debug("action=" + actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }

            // This is the product ID value passed from the originating page.
            String requestProductId = (request.getParameter(_REQUEST_KEY_PRODUCT_ID) == null)
                ? null
                : request.getParameter(_REQUEST_KEY_PRODUCT_ID).toUpperCase();

            if ((requestProductId == null) || (requestProductId.length() == 0)) {
                throw new FtdDataException(
                    "execute: The requested product ID can not be null.");
            }

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_UPDATE)) {
                // Extract the metrics from the input parameters to build
                // a template ProductFeedVO.
                ProductFeedVO pfVO = new ProductFeedVO(requestProductId);
                pfVO.setBuyUrl(((String) cleanFtdParams.get("buyUrl")));
                pfVO.setImageUrl(((String) cleanFtdParams.get("imageUrl")));
                pfVO.setImportantProductFlag(((String) cleanFtdParams.get("importantProductFlag")));
                pfVO.setPromotionalText(((String) cleanFtdParams.get("promotionalText")));
                pfVO.setShippingPromotionalText(((String) cleanFtdParams.get("shippingPromotionalText")));

                ProductRequestVO productRequestVO = new ProductRequestVO(pfVO,
                        extractSessionId(cleanFtdParams));

                try {
                    // Execute the insert/update.
                    ProductServiceBO.recordFeedProduct(productRequestVO);

                    // Record the operation result; i.e. number of rows updated/inserted. 
                    cleanFtdParams.put("result", "1");
                } catch (FtdDataException fde) {
                    // Special handling for user-presentable errors.
                    if (fde.isUniquenessViolation()) {
                        // Set the appropriate response disposition
                        // for a primary key violation.
                        cleanFtdParams.put(REQUEST_KEY_RESULT,
                            FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                    } else {
                        throw fde;
                    }
                }
            }

            // Compose common page elements.
            //            buildPage(responseDoc);
            // The page needs to load db record data.
            Document xmlResultSource = ProductServiceBO.getFeedProduct(requestProductId);
            DOMUtil.addSection(responseDoc, xmlResultSource.getChildNodes());

            // Include any original and new input parameters to the next page.
            appendPageDataToXml(responseDoc, cleanFtdParams);

        } catch (Throwable t) {
            logger.error("execute: Failed.", t);
            cleanFtdParams.put(REQUEST_KEY_ERROR_MESSAGE, t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);
        
        if (logger.isDebugEnabled()) {
            try {
                printXml(responseDoc);
            }
            catch (Exception e) {
                logger.error("execute: Could not debug-output result XML.", e);
            }
        }

        goToPage(mapping, request, response, responseDoc, callPage);

        return null;
    }
}
