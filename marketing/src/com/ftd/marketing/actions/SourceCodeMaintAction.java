package com.ftd.marketing.actions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.enterprise.dao.GlobalDAO;
import com.ftd.enterprise.vo.BinCheckPartnerVO;
import com.ftd.enterprise.vo.CpcVO;
import com.ftd.enterprise.vo.MpMemberLevelVO;
import com.ftd.enterprise.vo.ProductAttrExclusionVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.enterprise.vo.SourceFloristPriorityVO;
import com.ftd.enterprise.vo.SourceProgramRefVO;
import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.bo.PartnerProgramServiceBO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;
import com.ftd.marketing.vo.SourceProgramRequestVO;
import com.ftd.marketing.vo.SourceRequestVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author Tim Schmig
 */
public class SourceCodeMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(SourceCodeMaintAction.class.getName());
    private static final String _REQUEST_KEY_SOURCE_CODE = "requestSourceCode";
    // Assuming credit card numbers are not longer than 30 digits.
    private static final String _CREDIT_CARD_MASK_STRING = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    private static final String FLORIST_RECORD_TYPE_R = "R";
    private static int PREFERRED_FLORIST_PRIMARY = 1;
    private static int PREFERRED_FLORIST_BACKUP = 2;

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // Provide default parameter values.
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            logger.debug("action=" + actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }

            // Handle Ajax call to validate Preferred Florists (i.e., primary/backup florists)
            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_PREFERRED_FLORIST_VALIDATION)) {
            	Document retDoc = checkPreferredFlorists((String) cleanFtdParams.get("primaryFloristId"), (String) cleanFtdParams.get("backupFloristIds"));
                DOMUtil.print(retDoc, response.getWriter());
                return null;
            }

            // This is the source code value passed from the originating page.
            String requestSourceCode = (request.getParameter(_REQUEST_KEY_SOURCE_CODE) == null)
                ? null
                : request.getParameter(_REQUEST_KEY_SOURCE_CODE).toUpperCase();

            if ((requestSourceCode == null) ||
                    (requestSourceCode.length() == 0)) {
                throw new FtdDataException(
                    "execute: The requested source code can not be null.");
            }

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_UPDATE)) {
                // Extract the metrics from the input parameters to build
                // a template SourceVO.
                SourceVO sVO = new SourceVO(requestSourceCode);
                sVO.setUpdatedBy(lookupCurrentUserId(extractSessionId(cleanFtdParams)));
                sVO.setSourceType(((String) cleanFtdParams.get("sourceType")));

                sVO.setBonusPromotionCode((String) cleanFtdParams.get(
                        "bonusPromotionCode"));

                sVO.setPromotionCode((String) cleanFtdParams.get(
                        "promotionCode"));

                sVO.setRequiresDeliveryConfirmation((String) cleanFtdParams.get(
                        "requiresDeliveryConfirmation"));
                sVO.setWebloyaltyFlag((String) cleanFtdParams.get(
                        "webloyaltyFlag"));
                sVO.setEmergencyTextFlag((String) cleanFtdParams.get(
                        "emergencyTextFlag"));
                sVO.setEnableLpProcessing((String) cleanFtdParams.get(
                        "enableLpProcessing"));
                sVO.setFraudFlag((String) cleanFtdParams.get(
                        "enableLpProcessing"));
                sVO.setSendToScrub((String) cleanFtdParams.get("sendToScrub"));
                sVO.setCompanyId((String) cleanFtdParams.get("companyId"));
                sVO.setJcpenneyFlag((String) cleanFtdParams.get("jcpenneyFlag"));
                sVO.setOrderSource((String) cleanFtdParams.get("orderSource"));
                sVO.setBinNumberCheckFlag((String) cleanFtdParams.get(
                        "binNumberCheckFlag"));
                sVO.setDiscountAllowedFlag((String) cleanFtdParams.get(
                        "discountAllowedFlag"));
                sVO.setHighlightDescriptionFlag((String) cleanFtdParams.get(
                        "highlightDescriptionFlag"));
                sVO.setExternalCallCenterFlag((String) cleanFtdParams.get(
                        "externalCallCenter"));

//                sVO.setBillingInfoLogic((String) cleanFtdParams.get(
//                        "billingInfoLogic"));
//                sVO.setBillingInfoPrompt((String) cleanFtdParams.get(
//                        "billingInfoPrompt"));

                sVO.setDefaultSourceCodeFlag((String) cleanFtdParams.get(
                        "defaultSourceCodeFlag"));

                sVO.setPaymentMethodId((String) cleanFtdParams.get(
                        "paymentMethodId"));

                sVO.setPriceHeaderId((String) cleanFtdParams.get(
                        "priceHeaderId"));

                sVO.setSnhId((String) cleanFtdParams.get("snhId"));
                sVO.setMpRedemptionRateId((String) cleanFtdParams.get("mpRedemptionRateId"));
                sVO.setDescription((String) cleanFtdParams.get("description"));
                sVO.setRequestedBy((String) cleanFtdParams.get("requestedBy"));
                sVO.setOver21Flag((String) cleanFtdParams.get("over21Flag"));
                sVO.setRelatedSourceCode((String) cleanFtdParams.get(
                        "relatedSourceCode"));
                sVO.setProgramWebsiteUrl((String) cleanFtdParams.get(
                        "programWebsiteUrl"));
                sVO.setAutomatedPromotionEngineFlag((String) cleanFtdParams.get("automatedPromotionEngineFlag"));
                sVO.setApeProductCatalog((String) cleanFtdParams.get("apeProductCatalog"));
                sVO.setApeBaseSourceCodes((String) cleanFtdParams.get("apeBaseSourceCodes"));
                sVO.setCommentText((String) cleanFtdParams.get("commentText"));
                
                sVO.setIotwFlag((String) cleanFtdParams.get("iotwFlag"));
                sVO.setInvoicePassword((String) cleanFtdParams.get("invoicePassword"));
                sVO.setBillingInfoFlag((String) cleanFtdParams.get("billingInfoFlag"));
                if (sVO.getBillingInfoFlag() == null) {
                    sVO.setBillingInfoFlag("N");
                }
                sVO.setGiftCertificateFlag((String) cleanFtdParams.get("giftCertificateFlag"));
                sVO.setAddOnFreeId((String) cleanFtdParams.get("addOnFreeId"));
                sVO.setDisplayServiceFeeCode((String) cleanFtdParams.get("displayServiceFeeCode"));
                sVO.setDisplayShippingFeeCode((String) cleanFtdParams.get("displayShippingFeeCode"));
                sVO.setApplySurchargeCode((String) cleanFtdParams.get("applySurchargeCode"));
                
                // Defect - 8276 - Same Day Upcharge - Iteration 1 changes
                sVO.setSameDayUpcharge((String)cleanFtdParams.get("sameDayUpcharge"));
                sVO.setDisplaySameDayUpcharge((String)cleanFtdParams.get("displaySameDayUpcharge"));
                
                sVO.setMorningDeliveryFlag((String)cleanFtdParams.get("morningDeliveryFlag"));
                sVO.setCalculateTaxFlag((String)cleanFtdParams.get("calculateTax"));
                sVO.setShippingCarrierFlag((String)cleanFtdParams.get("shippingCarrierFlag"));
                sVO.setMorningDeliveryToFSMembers((String)cleanFtdParams.get("morningDeliveryChargedToFSMembers"));
                sVO.setDeliveryFeeId((String)cleanFtdParams.get("deliveryFeeId"));
                
                sVO.setMerchAmtFullRefundFlag((String)cleanFtdParams.get("merchAmtFullRefundFlag"));
                
                //SGC-2 Same Day Upcharge FS Member Controls
                sVO.setSameDayUpchargeFS((String)cleanFtdParams.get("sameDayUpchargeFS"));

               // logger.info("mechAmtFullRefundFlag ->  in Source Code Maint Action "+(String)cleanFtdParams.get("mechAmtFullRefundFlag"));
                
                
                if (!(StringUtils.isEmpty((String)cleanFtdParams.get("surchargeAmount"))))
                {
                   sVO.setSurchargeAmount(Double.valueOf((String)cleanFtdParams.get("surchargeAmount")));                
                }
                else
                {
                  sVO.setSurchargeAmount(0);                
                }
                
                sVO.setSurchargeDescription((String) cleanFtdParams.get("surchargeDescription"));
                sVO.setDisplaySurcharge((String) cleanFtdParams.get("displaySurcharge"));
                sVO.setAllowFreeShippingFlag((String) cleanFtdParams.get("allowFreeShippingFlag"));
                
                //Sympathy Controls
                sVO.setFuneralCemeteryLocChk((String)cleanFtdParams.get("fcLocCheck"));
                sVO.setHospitalLocChck((String)cleanFtdParams.get("hospLocCheck"));
                sVO.setFuneralLeadTimeChck((String)cleanFtdParams.get("funeralLeadTimeCheck"));
                sVO.setFuneralLeadTime((String)cleanFtdParams.get("funeralLeadTime"));
                sVO.setBoHrsMonFriStart((String)cleanFtdParams.get("busOpHoursMonFriStart"));
                sVO.setBoHrsMonFriEnd((String)cleanFtdParams.get("busOpHoursMonFriEnd"));
                sVO.setBoHrsSatStart((String)cleanFtdParams.get("busOpHoursSatStart"));
                sVO.setBoHrsSatEnd((String)cleanFtdParams.get("busOpHoursSatEnd"));
                sVO.setBoHrsSunStart((String)cleanFtdParams.get("busOpHoursSunStart"));
                sVO.setBoHrsSunEnd((String)cleanFtdParams.get("busOpHoursSunEnd"));
                

                //Retrieve and populate the bin check partner values
                if (cleanFtdParams.get("binCheckPartnerValues") != null) {
                  ArrayList <BinCheckPartnerVO> binCheckPartnerList = new ArrayList <BinCheckPartnerVO> ();
                  Integer binCheckPartnerValues = Integer.parseInt((String)cleanFtdParams.get("binCheckPartnerValues"));
                  for (int i=1; i<=binCheckPartnerValues; i++) {
                      String binCheckPartnername = (String) cleanFtdParams.get("binCheckPartnerName" +i);
                      String binCheckPartnerActive = (String) cleanFtdParams.get("binCheckPartnerActive" +i);
                      if (binCheckPartnername != null && binCheckPartnerActive != null){
                          BinCheckPartnerVO binCheckPartnerVO = new BinCheckPartnerVO();
                          binCheckPartnerVO.setPartnerName(binCheckPartnername);
                          binCheckPartnerVO.setActive(binCheckPartnerActive.equalsIgnoreCase("Y") ? true : false);
                          binCheckPartnerList.add(binCheckPartnerVO);
                      }
                  }
                  sVO.setBinCheckPartnerList(binCheckPartnerList);
                }
                
                // Retrieve the list of product attributes.
                Document prodAttrDoc = GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PROD_ATTRIBUTES);
                ArrayList <ProductAttrExclusionVO> prodAttrExclusionList = new ArrayList <ProductAttrExclusionVO> ();
                if(prodAttrDoc != null) {
                    NodeList attrList = prodAttrDoc.getElementsByTagName("product_attribute");
                    if(attrList != null) {
                        for(int i = 0; i < attrList.getLength(); i++) {
                            Element attrElem = (Element)attrList.item(i);
                            String attrId = attrElem.getAttribute("product_attr_restr_id");
                            String attrSelectedValue = (String)cleanFtdParams.get("product_attr_restr_id_" + attrId);
                            ProductAttrExclusionVO paeVO = new ProductAttrExclusionVO();
                            paeVO.setProductAttributeId(attrId);
                            if (attrSelectedValue != null && attrSelectedValue.equals("N")) {
                                // No radio button is selected. Product attribute should be excluded for sourc code.
                                paeVO.setExcludedFlag("Y");
                            } else {
                                paeVO.setExcludedFlag("N");
                            }
                            prodAttrExclusionList.add(paeVO);
                        }
                    }
                }
                sVO.setProductAttrExclList(prodAttrExclusionList);
                
                //Retrieve the member level data for the source code
                Document srcCodeMemLvlDoc = GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SRC_CODES_AND_MEMBER_LEVEL);
                ArrayList<MpMemberLevelVO> mpMemberLevelList = new ArrayList<MpMemberLevelVO>();
                if(srcCodeMemLvlDoc != null) {
                	NodeList srcAttrList = srcCodeMemLvlDoc.getElementsByTagName("mp_member_level");
                    if(srcAttrList != null) {
                        for(int i = 0; i < srcAttrList.getLength(); i++) {
                            Element srcAttrElem = (Element)srcAttrList.item(i);
                            String srcAttrId = srcAttrElem.getAttribute("mp_member_level_id");
                            String selectedValue = (String)cleanFtdParams.get("mp_member_level_id_" + srcAttrId);
                            MpMemberLevelVO mpMemberLevelVO = new MpMemberLevelVO();
                            mpMemberLevelVO.setMpMemberLevelId(srcAttrId);
                            if (selectedValue != null && selectedValue.equals("Y")) {
                            	mpMemberLevelList.add(mpMemberLevelVO);
                            }                            
                        }
                    }
                }
                
                sVO.setMpMemberLevelList(mpMemberLevelList);
                // Date handling
                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

                String startDate = (String) cleanFtdParams.get("startDate");

                if ((startDate != null) && (startDate.length() > 0)) {
                    sVO.setStartDate(df.parse(startDate));
                }

                String endDate = (String) cleanFtdParams.get("endDate");

                if ((endDate != null) && (endDate.length() > 0)) {
                    sVO.setEndDate(df.parse(endDate));
                }

                // Include the cpc data.
                String creditCardNumber = (String) cleanFtdParams.get(
                        "creditCardNumber");

                if ((creditCardNumber != null) &&
                        (creditCardNumber.length() > 0)) {
                    // Note that the card's expiration date is put in MM.YY format.
                    sVO.addCpc(new CpcVO(
                            (String) cleanFtdParams.get("creditCardType"),
                            (String) cleanFtdParams.get("creditCardNumber"),
                            (String) cleanFtdParams.get(
                                "creditCardExpirationMonth"),
                            (String) cleanFtdParams.get(
                                "creditCardExpirationYear")));
                }

                SourceRequestVO sourceRequestVO = new SourceRequestVO(sVO,
                        extractSessionId(cleanFtdParams));

                sourceRequestVO.setCcNumberUpdated(
                    MarketingConstants.COMMON_VALUE_YES.equals((String)cleanFtdParams.get("cc_number_updated_flag")) ?
                    true :
                    false);
                    
                // Preferred Florist (i.e., Primary/Backup florists)
                ArrayList<SourceFloristPriorityVO> sfpvoArray = new ArrayList<SourceFloristPriorityVO>();
                String pfid = (String) cleanFtdParams.get("primaryFloristId");
                if (pfid != null) { 
                    pfid = pfid.trim();
                    logger.debug("Including Primary florist: '" + pfid + "'");
                    SourceFloristPriorityVO sfpvo = new SourceFloristPriorityVO();
                    sfpvo.setFloristId(pfid);
                    sfpvo.setPriority(PREFERRED_FLORIST_PRIMARY);
                    sfpvoArray.add(sfpvo);
                }
                String bfids = (String) cleanFtdParams.get("backupFloristIds");
                if (bfids != null) {
                    bfids = bfids.trim();
                    String[] btokens = bfids.split("\\s+"); 
                    for (String bf : btokens) {
                        logger.debug("Including Backup florist: '" + bf + "'");
                        SourceFloristPriorityVO sfpvo = new SourceFloristPriorityVO();
                        sfpvo.setFloristId(bf);
                        sfpvo.setPriority(PREFERRED_FLORIST_BACKUP);
                        sfpvoArray.add(sfpvo);
                    }
                }
                if (sfpvoArray.size() > 0) {
                    sVO.setSourceFloristPriorityList(sfpvoArray);
                }
                
                sVO.setRandomWeightedFlag((String) cleanFtdParams.get("randomWeightedFlag"));
                
                sVO.setOscarSelectionEnabledFlag((String) cleanFtdParams.get("oscarSelectionEnabledFlag"));
                sVO.setOscarScenarioGroupId((String) cleanFtdParams.get("oscarScenarioGroupId"));

                try {
                    // Execute the insert/update.
                    SourceServiceBO.recordSource(sourceRequestVO);

                    String partnerName = (String) cleanFtdParams.get(
                            "original_partner_name");

                    if ((partnerName == null) || (partnerName.length() == 0)) {
                        String programName = (String) cleanFtdParams.get(
                                "programName");

                        if ((programName != null) &&
                                (programName.length() > 0)) {
                            Date now = new Date();
                            String today = df.format(now);

                            SourceProgramRefVO sprVO = new SourceProgramRefVO(sVO.getId(),
                                    programName, df.parse(today));

                            SourceProgramRequestVO sourceProgramRequestVO = new SourceProgramRequestVO(sprVO,
                                    extractSessionId(cleanFtdParams));

                            SourceServiceBO.recordSourceProgram(sourceProgramRequestVO);
                        }
                    }

                    // After the insert/update, we now need to persist the
                    // partner bank value.
                    String partnerBankFlag = (String) cleanFtdParams.get("partnerBankFlag");
                    if (partnerBankFlag != null && partnerBankFlag.equalsIgnoreCase(
                                "Y")) {
                        // Obtain the single partner bank that pertains to the given
                        // assigned partner.
                        String partnerBankId = SourceServiceBO.getSourcePartnerBank(requestSourceCode);

                        if (partnerBankId != null) {
                            sVO.setPartnerBankId(partnerBankId);

                            // Persist a second time.
                            // Yes, this is inefficient, and really needs to be fixed at a later date.
                            SourceServiceBO.recordSource(sourceRequestVO);
                        }
                    }

                    boolean isBaseSourceCode = false;
                    //check if request source code is base source code
                    HashMap masterMap = new HashMap();
                    if(sVO.getAutomatedPromotionEngineFlag().equalsIgnoreCase("Y")){
                    	masterMap = SourceServiceBO.checkBaseSourceCode(requestSourceCode);
                    }
                    if(masterMap.size() > 0){
                    	isBaseSourceCode = true;
                    }
                    
                    if(isBaseSourceCode){
                    	String sourceList = "";
                    	ArrayList<String> arrList = new ArrayList<String>();
                    	if(masterMap.containsKey("BASE")){
	                    	cleanFtdParams.put("result", "-14");
	                    	arrList = (ArrayList<String>) masterMap.get("BASE");
	                    	for (String s : arrList) {     
	                    		sourceList += s + " "; 
	            			}
	                    	cleanFtdParams.put("listMasterSourceCodes", sourceList);
	                    	sourceList = "";
	                    }
                    }
                    else if(!isBaseSourceCode){
	                    //persist Automated promotion engine base source codes
	                    HashMap resultMap = SourceServiceBO.updateApeBaseSourceCodes(requestSourceCode, sVO.getApeBaseSourceCodes(), sVO.getUpdatedBy());
	                    if(resultMap.size() > 0 ){
	                    	String sourceList = "";
	                    	ArrayList<String> arrList = new ArrayList<String>();
	                    	if(resultMap.containsKey("INVALID")){
		                    	cleanFtdParams.put("result", "-10");
		                    	arrList = (ArrayList<String>) resultMap.get("INVALID");
		                    	for (String s : arrList) {     
		                    		sourceList += s + " "; 
		            			}
		                    	cleanFtdParams.put("listInvalid", sourceList);
		                    	sourceList = "";
		                    }
	                    	if(resultMap.containsKey("APEN")){
		                    	cleanFtdParams.put("result", "-11");
		                    	arrList = (ArrayList<String>) resultMap.get("APEN");
		                    	for (String s : arrList) {     
		                    		sourceList += s + " "; 
		            			}
		                    	cleanFtdParams.put("listApeEnabled", sourceList);
		                    	sourceList = "";
		                    }
	                    	if(resultMap.containsKey("IOTW")){
		                    	cleanFtdParams.put("result", "-12");
		                    	arrList = (ArrayList<String>) resultMap.get("IOTW");
		                    	for (String s : arrList) {     
		                    		sourceList += s + " "; 
		            			}
		                    	cleanFtdParams.put("listIOTW", sourceList);
		                    	sourceList = "";
		                    }
	                    	if(resultMap.containsKey("MILES")){
		                    	cleanFtdParams.put("result", "-13");
		                    	arrList = (ArrayList<String>) resultMap.get("MILES");
		                    	for (String s : arrList) {     
		                    		sourceList += s + " "; 
		            			}
		                    	cleanFtdParams.put("listMilesPoints", sourceList);
		                    	sourceList = "";
		                    }
	                    }
	                    // Record the operation result; i.e. number of rows updated/inserted.
	                    else{
	                    	cleanFtdParams.put("result", "1");
	                    }
                    }
                } catch (FtdDataException fde) {
                    // Special handling for user-presentable errors.
                    logger.error(fde);
                    if (fde.isUniquenessViolation()) {
                        // Set the appropriate response disposition
                        // for a primary key violation.
                        cleanFtdParams.put(REQUEST_KEY_RESULT,
                            FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                    } else {
                        throw fde;
                    }
                }
            }

            if (actionType.equals("edit_program") ||
                    actionType.equals("add_program")) {
                logger.debug("bonus");

                cleanFtdParams.put("action_type", actionType);
                appendPageDataToXml(responseDoc, cleanFtdParams);

                String partnerName = (String) cleanFtdParams.get("partner_name");
                Document tempDoc = PartnerProgramServiceBO.getPartnerPrograms(partnerName);
                DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

                String promoId = (String) cleanFtdParams.get("promoId");

                if ((promoId != null) && (promoId.length() > 0)) {
                    tempDoc = SourceServiceBO.getSourceProgram(promoId);
                    DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
                }

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceProgramRefMaint");

                return null;
            }

            if (actionType.equals("save_program")) {
                logger.debug("save_program");

                String sourceCode = (String) cleanFtdParams.get(
                        "requestSourceCode");
                String programName = (String) cleanFtdParams.get("programName");
                String startDate = (String) cleanFtdParams.get("promoStartDate");
                String promoId = (String) cleanFtdParams.get("promoId");

                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

                SourceProgramRefVO sprVO = null;

                if ((promoId != null) && (promoId.length() > 0)) {
                    sprVO = new SourceProgramRefVO(sourceCode, programName,
                            df.parse(startDate), promoId);
                } else {
                    sprVO = new SourceProgramRefVO(sourceCode, programName,
                            df.parse(startDate));
                }

                SourceProgramRequestVO sourceProgramRequestVO = new SourceProgramRequestVO(sprVO,
                        extractSessionId(cleanFtdParams));

                SourceServiceBO.recordSourceProgram(sourceProgramRequestVO);

                cleanFtdParams.put("VALIDATION_SUCCESS", "Y");
                cleanFtdParams.put("SUBMIT_ACTION", "true");
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceProgramRefMaint");

                return null;
            }

            // Compose common page elements.
            buildPage(responseDoc);

            // Include the user's field permissions in the response page.
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_DEFINITION_DATA,
                SecurityPermission.UPDATE);
//            appendSecurityPermission(cleanFtdParams,
//                SecurityResource.SOURCE_BILLING_DATA, SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PAYMENT_METHOD_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PCARD_DATA, SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PRICE_HEADER_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PROMOTION_CODE_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_SNH_DATA, SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.RDMPTN_RATE_DATA, SecurityPermission.UPDATE);

            // The page needs to load db record data.
            Document xmlResultSource = SourceServiceBO.getSource(requestSourceCode,
                    extractSessionId(cleanFtdParams));
            DOMUtil.addSection(responseDoc, xmlResultSource.getChildNodes());
            
            //get ape base source codes section
            Document xmlBaseSourceCodes = SourceServiceBO.getApeBaseSourceCodes(requestSourceCode);
            DOMUtil.addSection(responseDoc, xmlBaseSourceCodes.getChildNodes());
            // #13046 - If update, check if the requested source code is only readable.
            try{
	            if(xmlResultSource != null && xmlResultSource.getFirstChild().getChildNodes().getLength() >= 1) {
	            	NodeList sourceList = xmlResultSource.getFirstChild().getChildNodes();
	            	Node requestedByNode = sourceList.item(0).getAttributes().getNamedItem("requested_by");
	            	String requestedBy = requestedByNode.getNodeValue();
	            	if(SourceCodeConstants.WEBSITE_PORTAL_USER.equalsIgnoreCase(requestedBy)) {
	            		setReadonlyPermissions(cleanFtdParams);
	            	}
	            }
            } catch(Exception e){
            	logger.error("Error caught and unable to set sympathy SC read only permissions to the map : " + e.getMessage());
            }

            xmlResultSource = SourceServiceBO.getSourcePrograms(requestSourceCode);
            DOMUtil.addSection(responseDoc, xmlResultSource.getChildNodes());

            Document xmlResultCpc = SourceServiceBO.getCpc(requestSourceCode);

            // Defect 2655. Mask credit card number and only show the last 4 digits.
            // 12/21/2006 by chu.
            if(xmlResultCpc != null) {
                Element cpcElem = (Element)DOMUtil.selectSingleNode(xmlResultCpc, "/cpcList/cpc");

                if(cpcElem != null) {
                    NamedNodeMap attributeMap2 = cpcElem.getAttributes();
                    String creditCardNumber = attributeMap2.getNamedItem(
                            "credit_card_number").getNodeValue();

                    String maskedCreditCardNumber = creditCardNumber;
                    if (creditCardNumber.length() > 4) {
                        maskedCreditCardNumber = _CREDIT_CARD_MASK_STRING.substring(0, creditCardNumber.length() - 4)
                                                    + creditCardNumber.substring(creditCardNumber.length() - 4);
                    }

                    cpcElem.setAttribute("credit_card_number", maskedCreditCardNumber);
                }
            }
            DOMUtil.addSection(responseDoc, xmlResultCpc.getChildNodes());

            //            XMLDocument xmlResultPartnerBank = SourceServiceBO.getSourcePartnerBanks(requestSourceCode);
            //            DOMUtil.addSection(responseDoc, xmlResultPartnerBank.getChildNodes());
            Date now = new Date();

            //DateFormat df =  DateFormat.getDateInstance(DateFormat.SHORT);
            //cleanFtdParams.put("today",df.format(now));
            Format formatter = new SimpleDateFormat("MM/dd/yyyy");
            cleanFtdParams.put("today", formatter.format(now));

            // Include any original and new input parameters to the next page.
            appendPageDataToXml(responseDoc, cleanFtdParams);

            if (logger.isDebugEnabled()) {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc, "SourceCodeMaint");

            return null;
        } catch (Throwable t) {
            logger.error("", t);
            cleanFtdParams.put(REQUEST_KEY_ERROR_MESSAGE, t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }


    private Document checkPreferredFlorists(String primaryFloristId, String backupFloristIds) throws Exception {
    	Document returnDocument = DOMUtil.getDefaultDocument();
        String failureType = null;
        String badBackups = "";
        String[] btokens = null;
        
        logger.debug("Checking Primary FloristID: " + primaryFloristId + " and Backup FloristIDs: " + backupFloristIds);
        if (backupFloristIds != null) {
            btokens = backupFloristIds.split("\\s+"); // Separated by "whitespace"
        }

        CachedResultSet floristDetails = null; 
        if (primaryFloristId != null && !primaryFloristId.equalsIgnoreCase(""))
        {
          floristDetails = SourceServiceBO.getFloristDetails(primaryFloristId);
          if (!floristDetails.next() || !floristDetails.getObject("record_type").equals(FLORIST_RECORD_TYPE_R) || floristDetails.getObject("vendor_flag").equals("Y") ) {
              failureType = "primary";
          }
        }

        if ((failureType == null) && (btokens != null)) {        
            for (String bfid : btokens) {
                floristDetails = SourceServiceBO.getFloristDetails(bfid);
                if (!floristDetails.next() || !floristDetails.getObject("record_type").equals(FLORIST_RECORD_TYPE_R) || floristDetails.getObject("vendor_flag").equals("Y") ) {
                    badBackups += bfid + " ";
                    failureType = "backup";
                }
            }
        }

        Element root = (Element) returnDocument.createElement("root");
        returnDocument.appendChild(root);
        Element node;
        if (failureType == null) {
            node = (Element)returnDocument.createElement("success");
            node.appendChild(returnDocument.createTextNode("Y"));
            root.appendChild(node);
        } else {
            node = (Element)returnDocument.createElement("success");
            node.appendChild(returnDocument.createTextNode("N"));
            root.appendChild(node);
            node = (Element)returnDocument.createElement("error_msg");
            if (failureType.equals("primary")) {
                node.appendChild(returnDocument.createTextNode("N/A"));
            } else {
                node.appendChild(returnDocument.createTextNode(badBackups));   
            }
            root.appendChild(node);
            
            node = (Element)returnDocument.createElement("error_field");
            node.appendChild(returnDocument.createTextNode(failureType));
            root.appendChild(node);
        }
        
        return returnDocument;
    }


    private void buildPage(Document responseDoc) {
        try {
        	Document tempDoc = ReferenceServiceBO.getAllCompanies();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = SourceServiceBO.getAllSourceTypes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllPricingCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllServiceFeeCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = ReferenceServiceBO.getAllRedemptionRates();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllOrderSources();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = ReferenceServiceBO.getAllPaymentMethods();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
          
            tempDoc = ReferenceServiceBO.getAllComplimentaryAddons();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());  
        
            tempDoc = ReferenceServiceBO.getAllBinCheckPartners();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = SecurityServiceBO.getUsersByRole(MarketingConstants.AAS_USERS_MKT_ROLE_NAME);
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = PartnerProgramServiceBO.getAllPartners();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = PartnerProgramServiceBO.getAllPartnerPrograms();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = ReferenceServiceBO.getAllProdAttributes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllSourceCodesAndMemberlevels();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = ReferenceServiceBO.getAllDeliveryFeeCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = ReferenceServiceBO.getAllOSCARScenarioGroups();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
