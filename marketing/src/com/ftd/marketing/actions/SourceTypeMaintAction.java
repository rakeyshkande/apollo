package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.enterprise.vo.SourceTypeVO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.SourceTypeRequestVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;

import java.io.BufferedWriter;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * This handles requests for the Source Type Maintenance screen.
 *
 * @author JP Puzon
 */
public class SourceTypeMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(SourceTypeMaintAction.class.getName());

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // get input parameter
            String actionType = (String) cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }

            String sourceType = (String) cleanFtdParams.get("sourceType");

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_SEARCH)) {
                CachedResultSet tuples = SourceServiceBO.getAllGroupedSourceTypes();

                // Compose the collection of records for presentation,
                // a delimited list of values.
                StringBuffer sb = new StringBuffer("SOURCE TYPE\tSOURCE CODE\n");

                // tab-delimited so Excel will provide automatic formatting.
                while (tuples.next()) {
                    sb.append((String) tuples.getObject(1));
                    sb.append("\t");
                    sb.append((String) tuples.getObject(2));
                    sb.append("\n");
                }

                // Set the output data's mime type
                response.setContentType("application/vnd.ms-excel"); // MIME type for excel sheet

                BufferedWriter writer = new BufferedWriter(response.getWriter());

                try {
                    writer.write(sb.toString());
                } finally {
                    if (writer != null) {
                        writer.flush();
                        writer.close();
                    }
                }

                return null;
            } else {
      
                if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_INSERT)) {
                    // Since an input source type has been supplied, proceed to
                    // insert the new record.
                    SourceTypeVO sourceTypeVO = new SourceTypeVO(sourceType);                    
                    UserInfo user = new UserInfo();
                    user.setUserID(lookupCurrentUserId(extractSessionId(cleanFtdParams)));
                    try {
                        SourceServiceBO.recordSourceType(new SourceTypeRequestVO(
                                sourceTypeVO, user));

                        cleanFtdParams.put(REQUEST_KEY_RESULT, "1");
                    } catch (FtdDataException fde) {
                        // Special handling for user-presentable errors.
                        if (fde.isUniquenessViolation()) {
                            // Set the appropriate response disposition
                            // for a primary key violation.
                            cleanFtdParams.put(REQUEST_KEY_RESULT,
                                FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                        } else {
                            throw fde;
                        }
                    }
                }
                
                // Compose common page elements.
                buildPage(responseDoc);
                
                // Include the user's field permissions in the response page.
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_TYPE_DATA,
                SecurityPermission.ADD);

                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceTypeMaint");

                return null;
            }
        } catch (Throwable t) {
            logger.error(t);
            cleanFtdParams.put(REQUEST_KEY_ERROR_MESSAGE, t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    private void buildPage(Document responseDoc) {
        try {
        	Document tempDoc = SourceServiceBO.getAllSourceTypes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
