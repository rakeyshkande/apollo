package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.SourceTypeVO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.SourceTypeRequestVO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;

import java.io.BufferedWriter;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This handles requests to return to the main menu
 *
 * @author JP Puzon
 */
public class MainMenuAction extends BaseAction {
    private static Log logger = LogFactory.getLog(MainMenuAction.class.getName());

    /**
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
     *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        try {
            String  mainMenuUrl = ConfigurationUtil.getInstance().getFrpGlobalParm(
               MarketingConstants.MARKETING_CONFIG_CONTEXT, MarketingConstants.SECURITY_MAIN_MENU_REDIRECT);

            if(mainMenuUrl != null) {
    
              //append adminAction
              mainMenuUrl += "?adminAction=" + request.getParameter("adminAction");
      
              //append security to the main menu url
              mainMenuUrl += "&context=" + request.getParameter("context");
              mainMenuUrl += "&securitytoken=" + request.getParameter("securitytoken");
                
              response.sendRedirect(mainMenuUrl);
            }
        } catch (Exception e) {
            logger.error("Could not obtain " + MarketingConstants.SECURITY_MAIN_MENU_REDIRECT +
                         " from " + MarketingConstants.MARKETING_CONFIG_CONTEXT + " due to error: " + e);
        }
        return null;
    }
}
