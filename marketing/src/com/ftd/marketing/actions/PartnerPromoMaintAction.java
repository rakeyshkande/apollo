package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.ProgramRewardVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.marketing.bo.PartnerProgramServiceBO;
import com.ftd.marketing.vo.ProgramRewardRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author Tim Schmig
 */
public class PartnerPromoMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(
            "com.ftd.marketing.actions.PartnerPromoMaintAction");

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response
            responseDoc = DOMUtil.getDocument();
            
            // Build list of request parameters
            cleanFtdParams = cleanHttpParameters(request);
            logger.debug(cleanFtdParams);
            
            // Include the user's field permissions in the response page.
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.PARTNER_PROMOTION_DATA,
                SecurityPermission.UPDATE);

            // get input parameter
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            //logger.debug(actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }
                
            if (actionType.equals("update")) {
                logger.debug("update");
                cleanFtdParams.put("action_type","update");
                
                ProgramRewardVO prVO = new ProgramRewardVO( (String) cleanFtdParams.get("partnerPromoCode"));

                prVO.setProgramType( (String) cleanFtdParams.get("programType"));
                prVO.setPartnerName( (String) cleanFtdParams.get("partnerId"));
                prVO.setEmailExcludeFlag("Y");
                prVO.setUpdatedBy(extractSessionId(cleanFtdParams));
                prVO.setMembershipDataRequired( (String) cleanFtdParams.get("membershipDataRequired"));
                
                if (prVO.getProgramType().equals("Default"))
                {
                  prVO.setEmailExcludeFlag( (String) cleanFtdParams.get("emailExcludeFlag"));
                  prVO.setProgramLongName( (String) cleanFtdParams.get("programLongName"));
                  prVO.setCalculationBasis( (String) cleanFtdParams.get("mileageCalcSource"));
                  Float myFloat = Float.valueOf((String) cleanFtdParams.get("pointsPerDollar"));
                  prVO.setPoints(myFloat);
                  prVO.setRewardType( (String) cleanFtdParams.get("rewardType"));
                  prVO.setRewardName( (String) cleanFtdParams.get("rewardName"));
                  String maxPoints = (String) cleanFtdParams.get("maximumPoints");
                  if (maxPoints != null) 
                  {
                    myFloat = Float.valueOf(maxPoints);
                    prVO.setMaximumPoints(myFloat);
                  }
                  String bonusPoints = (String) cleanFtdParams.get("bonusAmount");
                  if (bonusPoints != null) {
                    myFloat = Float.valueOf(bonusPoints);
                    prVO.setBonusPoints(myFloat);
                  }
                  prVO.setBonusCalculationBasis( (String) cleanFtdParams.get("bonusMileageCalcSource"));
                  prVO.setBonusSeparateData( (String) cleanFtdParams.get("bonusStructure"));
                  prVO.setCustomerInfo( (String) cleanFtdParams.get("customerInfo"));
                  prVO.setParticipantIdLengthCheck( (String) cleanFtdParams.get("participantIdLength"));
                  prVO.setParticipantEmailCheck( (String) cleanFtdParams.get("participantEmail"));
                }
                
                // Execute the insert/update
                ProgramRewardRequestVO programRequestVO = new ProgramRewardRequestVO(prVO,
                        extractSessionId(cleanFtdParams));
                PartnerProgramServiceBO.recordRewardProgram(programRequestVO);
                
                cleanFtdParams.put("result","1");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                  printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "PartnerPromoMaint");

                return null;
            }
            if (actionType.equals("edit") || actionType.equals("insert")) {
                logger.debug("edit");
                logger.debug(cleanFtdParams.get("partnerPromoCode"));
                cleanFtdParams.put("action_type","edit");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "PartnerPromoMaint");

                return null;
            }

            logger.debug("No action");

            cleanFtdParams.put("action_type","search");
            appendPageDataToXml(responseDoc, cleanFtdParams);

            Document tempDoc = PartnerProgramServiceBO.getAllPartners();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = PartnerProgramServiceBO.getAllPartnerPrograms();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
                    
            if (logger.isDebugEnabled()) 
            {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc,
                "PartnerPromoMaint");

            return null;
                        
        } catch (Exception de) { // should be FtdDataException
            logger.error("de: " + de);

            if (com.ftd.marketing.actions.MarketingUtilities.searchForString(
                        de.toString(), "ORA-00001")) {
                try {
                    cleanFtdParams.put("action_type","add");
                    cleanFtdParams.put("result", "already exists");
                    appendPageDataToXml(responseDoc, cleanFtdParams);

                    goToPage(mapping, request, response, responseDoc,
                        "SourceCodeMaint");

                    return null;
                } catch (Exception e) {
                    logger.error("e: " + e);
                    cleanFtdParams.put("error_message", e.getMessage());
                    appendPageDataToXml(responseDoc, cleanFtdParams);
                    callPage = ERROR_PAGE;
                }
            } else {
                cleanFtdParams.put("error_message", de.toString());
                appendPageDataToXml(responseDoc, cleanFtdParams);
                callPage = ERROR_PAGE;
            }
        } catch (Throwable t) {
            logger.error("t: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
            throw new RuntimeException(t);
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    private void buildPage(Document responseDoc, HashMap cleanFtdParams) {
        try {
            String programName = (String) cleanFtdParams.get("partnerPromoCode");
            ProgramRewardVO prVO = new ProgramRewardVO(programName);
            ProgramRewardRequestVO programRequestVO = new ProgramRewardRequestVO(prVO,
                    extractSessionId(cleanFtdParams));
                    
            Document xmlResult = PartnerProgramServiceBO.getRewardProgram(programRequestVO);
            DOMUtil.addSection(responseDoc, xmlResult.getChildNodes());
                    
            Document tempDoc = PartnerProgramServiceBO.getAllProgramTypes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = PartnerProgramServiceBO.getAllRewardTypes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
                    
            tempDoc = PartnerProgramServiceBO.getAllCalculationBases();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
                    
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
