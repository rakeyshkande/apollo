package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.EmailVO;
import com.ftd.enterprise.vo.SubscribeStatus;
import com.ftd.marketing.bo.CustomerServiceBO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This handles requests for the Cost Center Maintenance screen.
 *
 * @author Tim Schmig
 */
public class UpdateUnattachedEmailAction extends BaseAction {
    private static Log logger = LogFactory.getLog(
            "com.ftd.marketing.actions.UpdateUnattachedEmail");

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response
            responseDoc = DOMUtil.getDocument();
            
            // Build list of request parameters
            cleanFtdParams = cleanHttpParameters(request);
            logger.debug(cleanFtdParams);
            
            // get input parameter
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            //logger.debug(actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }
            
            // Obtain the user session Id.
            String sessionId = extractSessionId(cleanFtdParams);

            if (actionType.equals("retrieve_data")) {
                logger.debug("retrieve_data");
                cleanFtdParams.put("action_type","retrieve_data");
                
                String emailAddress = (String) cleanFtdParams.get("email_address");
                Document customerEmailDoc = CustomerServiceBO.getCustomerEmail(emailAddress);
                // the customerEmailDoc will always have an upper node
                //NodeList nodeList = customerEmailDoc.getChildNodes();
                Node firstCustomerNode = (Node) customerEmailDoc.getChildNodes().item(0);
                if (firstCustomerNode.hasChildNodes())
                {
                  cleanFtdParams.put("has_customer_record","true");
                }
                else
                {
                	Document unattachedEmailDoc = CustomerServiceBO.getEmail(emailAddress);
                  Node firstEmailNode = (Node) unattachedEmailDoc.getChildNodes().item(0);  
                  if (!firstEmailNode.hasChildNodes())
                  {
                    cleanFtdParams.put("no_records","true");
                  }
                  else
                  {
                    DOMUtil.addSection(responseDoc,unattachedEmailDoc.getChildNodes());
                    Document companiesDoc = ReferenceServiceBO.getAllCompanies();
                    DOMUtil.addSection(responseDoc,companiesDoc.getChildNodes());
                  }
                }
                if (logger.isDebugEnabled()) 
                {
                    printXml(responseDoc);
                }

                appendPageDataToXml(responseDoc, cleanFtdParams);
                goToPage(mapping, request, response, responseDoc,
                        "UpdateUnattachedEmail");
                return null;
           }
           else if (actionType.equals("update")) {
                logger.debug("update");
                cleanFtdParams.put("action_type","update");
                
                String emailAddress = (String) cleanFtdParams.get("email_address");
                int numUpdates = (new Integer((String) cleanFtdParams.get("num_updates"))).intValue();
              
                for (int i = 0; i< numUpdates; i++)
                {
                  EmailVO emailVO = new EmailVO();
                  emailVO.setEmailAddress(emailAddress);
                  emailVO.setSubscribeDate(Calendar.getInstance());
                  emailVO.setUpdatedBy(SecurityServiceBO.getUserInfo(sessionId).getUserID());
                  String subscribeStatus = (String)cleanFtdParams.get("subscribe_status_"+i);
                  if (subscribeStatus.equalsIgnoreCase("Subscribe"))
                  {
                    emailVO.setSubscribeStatus(SubscribeStatus.SUBSCRIBE);
                  }
                  else
                  if (subscribeStatus.equalsIgnoreCase("Unsubscribe"))
                  {
                    emailVO.setSubscribeStatus(SubscribeStatus.UNSUBSCRIBE);
                    
                  }
                  emailVO.setCompanyId((String)cleanFtdParams.get("company_id_"+i));
                  CustomerServiceBO.updateUnattachedEmail(emailVO);
                }
                cleanFtdParams.put("validation_success","true");
                // This section rebuilds the check box page data to add to the request //
                Document doc = DOMUtil.getDocumentBuilder().newDocument();
                Element emailList = (Element) doc.createElement("emailList");
                          
                for (Enumeration i = request.getParameterNames(); i.hasMoreElements();) 
                {
                    String key = (String) i.nextElement();
                    String value = request.getParameter(key);
        
                    if ((key.startsWith("company_")&&(value != null) && (value.trim().length() > 0))) 
                    {
                        Element email = (Element) doc.createElement("email");
                        email.setAttribute("company_id", key.substring(key.indexOf("_")+1));
                        email.setAttribute("subscribe_status", value);
                        emailList.appendChild(email);
                    }
                  }
                doc.appendChild(emailList);
                DOMUtil.addSection(responseDoc, doc.getChildNodes());
                Document companiesDoc = ReferenceServiceBO.getAllCompanies();
                DOMUtil.addSection(responseDoc,companiesDoc.getChildNodes());
           }
            logger.debug("No action");

            cleanFtdParams.put("action_type","search");
            appendPageDataToXml(responseDoc, cleanFtdParams);

            if (logger.isDebugEnabled()) 
            {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc,
                "UpdateUnattachedEmail");

            return null;
                        
        } catch (FtdDataException fde) {
            logger.error("fde: " + fde);
            if (fde.isUniquenessViolation()) 
            {
              cleanFtdParams.put("result","2");
              appendPageDataToXml(responseDoc, cleanFtdParams);
                
              goToPage(mapping, request, response, responseDoc,
                  "UpdateUnattachedEmail");

              return null;
            }

            cleanFtdParams.put("error_message", fde.toString());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        } catch (Exception de) {
            logger.error("de: " + de);

            cleanFtdParams.put("error_message", de.toString());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        } catch (Throwable t) {
            logger.error("t: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
            throw new RuntimeException(t);
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }
    
}

