package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.DiscountTierVO;
import com.ftd.enterprise.vo.PricingCodeVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.marketing.bo.ProductServiceBO;
import com.ftd.marketing.bo.ReferenceServiceBO;
//import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.osp.utilities.xml.DOMUtil;
//import com.ftd.security.cache.vo.UserInfo;

//import java.text.DateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This handles requests for the Price Header Maintenance screen.
 *
 * @author Tim Schmig
 */
public class PriceHeaderMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(
            "com.ftd.marketing.actions.PriceHeaderMaintAction");

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response
            responseDoc = DOMUtil.getDocument();
            
            // Build list of request parameters
            cleanFtdParams = cleanHttpParameters(request);
            logger.debug(cleanFtdParams);
            
            // get input parameter
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            //logger.debug(actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }
            
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.PRICING_CODE_DATA,
                SecurityPermission.UPDATE);
                
            if (actionType.equals("update")) {
                logger.debug("update");
                cleanFtdParams.put("action_type","update");
                
                String pricingCode = (String) cleanFtdParams.get("pricingCode");
                String description = (String) cleanFtdParams.get("description");
                String discountType = (String) cleanFtdParams.get("discountType");
                int rowCount = Integer.parseInt((String) cleanFtdParams.get("rowCount"));
                Float minDollar, maxDollar, discountAmt;
                
                PricingCodeVO pcVO = new PricingCodeVO(pricingCode, description, discountType);
                
                for (int x=1; x < rowCount+1; x++)
                {
                  minDollar = Float.valueOf((String) cleanFtdParams.get("from" + x));
                  maxDollar = Float.valueOf((String) cleanFtdParams.get("to" + x));
                  discountAmt = Float.valueOf((String) cleanFtdParams.get("discount" + x));
                  DiscountTierVO dtVO = new DiscountTierVO(minDollar, maxDollar, discountAmt);
                  pcVO.addDiscountTier(dtVO);
                }
                String user = lookupCurrentUserId(extractSessionId(cleanFtdParams));
                pcVO.setUpdatedBy(user);
                ProductServiceBO.recordPricingCode(pcVO);
                
                cleanFtdParams.put("result","1");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                  printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "PriceHeaderMaint");

                return null;
            }
            if (actionType.equals("edit") || actionType.equals("insert")) {
                logger.debug("edit");
                logger.debug(cleanFtdParams.get("pricingCode"));
                cleanFtdParams.put("action_type","edit");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "PriceHeaderMaint");

                return null;
            }

            logger.debug("No action");

            cleanFtdParams.put("action_type","search");
            appendPageDataToXml(responseDoc, cleanFtdParams);

            Document tempDoc = ReferenceServiceBO.getAllPricingCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            if (logger.isDebugEnabled()) 
            {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc,
                "PriceHeaderMaint");

            return null;
                        
        } catch (Exception de) { // should be FtdDataException
            logger.error("de: " + de);

                cleanFtdParams.put("error_message", de.toString());
                appendPageDataToXml(responseDoc, cleanFtdParams);
                callPage = ERROR_PAGE;
        } catch (Throwable t) {
            logger.error("t: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
            throw new RuntimeException(t);
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
        }
 
    private void buildPage(Document responseDoc, HashMap cleanFtdParams) {
        try {
            String pricingCode = (String) cleanFtdParams.get("pricingCode");
            PricingCodeVO prVO = new PricingCodeVO();
            
            Document xmlResult = ProductServiceBO.getPricingCodeDetails(pricingCode);
            DOMUtil.addSection(responseDoc, xmlResult.getChildNodes());
            
            xmlResult = ReferenceServiceBO.getAllDiscountTypes();
            DOMUtil.addSection(responseDoc, xmlResult.getChildNodes());
                    
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}