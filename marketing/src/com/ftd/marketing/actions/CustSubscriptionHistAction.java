package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.*;

import com.ftd.marketing.bo.CustomerServiceBO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.marketing.vo.SourceRequestVO;

import com.ftd.osp.utilities.xml.DOMUtil;

import com.ftd.security.cache.vo.UserInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import java.io.IOException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author Tim Schmig & JP Puzon
 */
public class CustSubscriptionHistAction extends BaseAction {
    private static Log logger = LogFactory.getLog(CustSubscriptionHistAction.class.getName());
    private static final String _REQUEST_KEY_ACTION_TYPE = "scActionType";
    private static final String _REQUEST_KEY_RECORD_SORT_BY = "scRecordSortBy";
    private static final String _REQUEST_KEY_CUSTOMER_ID = "scCustomerId";

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Compose common page elements.
            //            buildPage(responseDoc);
            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // Provide default parameter values.
            String actionType = (String) cleanFtdParams.get(_REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_SEARCH;
            }

            String resultsSortedBy = (String) cleanFtdParams.get(_REQUEST_KEY_RECORD_SORT_BY);

            if (resultsSortedBy == null) {
                resultsSortedBy = "updatedOn";
            }

            // Special condition on first page load to set the max number of records per page.
            if (cleanFtdParams.get(
                        GuiPaginationVO.MAP_KEY_PAGE_MAX_RECORD_COUNT) == null) {
                cleanFtdParams.put(GuiPaginationVO.MAP_KEY_RECORD_NUMBER,
                    "1");
                cleanFtdParams.put(GuiPaginationVO.MAP_KEY_PAGE_MAX_RECORD_COUNT,
                    "50");
            }

            // Initialize.
            // Note that the -1 denotes on the XSL that the screen
            // is initializing.
            //            GuiPaginationVO gpVO = new GuiPaginationVO(new Integer(0),
            //                    new Integer(0), new Integer(1), new Integer(-1));
            // Obtain the search metadata being requested.
            GuiPaginationVO gpVO = extractGuiPagination(cleanFtdParams);

            // Obtain the user session Id.
            String sessionId = extractSessionId(cleanFtdParams);

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_SEARCH)) {
                // Submit the template to the business object.
                // IMPORTANT: NOTE how the query parameters are driven
                // by the GUI parameters of record number and max records
                // per page.
                List resultRecordCount = new ArrayList(1);
                Document searchResults = CustomerServiceBO.getSubscriptionHistory((String) cleanFtdParams.get(
                            _REQUEST_KEY_CUSTOMER_ID),
                        gpVO.getRecordNumber().intValue(),
                        gpVO.getPageMaxRecordCount().intValue(),
                        resultRecordCount, resultsSortedBy);

                // Add the results to the response.
                DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                // Record the new db stats.
                gpVO.setRecordCountTotal((Integer) resultRecordCount.get(0));
                gpVO.setPageNumber(new Integer(
                        new Double(Math.ceil(
                                gpVO.getRecordNumber().doubleValue() / gpVO.getPageMaxRecordCount()
                                                                           .doubleValue())).intValue()));

                // Add the pagination data to the parameter map.
                cleanFtdParams.putAll(gpVO.getDataMap());

                // Include any original and new input parameters to the next page.
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "CustSubscriptionHist");

                return null;
            }

            // Include any new and existing data parameters in the next page.
            //            cleanFtdParams.putAll(gpVO.getDataMap());
            //            appendPageDataToXml(responseDoc, cleanFtdParams);
            //
            //            if (logger.isDebugEnabled()) {
            //                printXml(responseDoc);
            //            }
            //
            //            goToPage(mapping, request, response, responseDoc, "CustSubscriptionHist");
            //
            //            return null;
        } catch (Throwable t) {
            logger.error("execute: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    /*private void buildPage(XMLDocument responseDoc) {
        try {
            XMLDocument tempDoc = ReferenceServiceBO.getAllCompanies();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllPricingCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllServiceFeeCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = SecurityServiceBO.getUsers(MarketingConstants.AAS_USERS_MKT_DEPARTMENT_ID);
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = SecurityServiceBO.getUsers(MarketingConstants.AAS_USERS_OPS_DEPARTMENT_ID);
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }*/
}
