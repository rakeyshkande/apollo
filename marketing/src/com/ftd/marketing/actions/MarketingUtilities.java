package com.ftd.marketing.actions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class MarketingUtilities {
    private static Log logger = LogFactory.getLog(MarketingUtilities.class.getName());

    public MarketingUtilities() {
    }

    public static boolean searchForString(String inString, String searchString) {
        int index = -1;
        boolean result = false;
        index = inString.indexOf(searchString);

        if (index >= 0) {
            result = true;
        }

        return result;
    }
}
