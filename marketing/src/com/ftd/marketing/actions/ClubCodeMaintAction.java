package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.AaaClubVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.marketing.bo.PartnerProgramServiceBO;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This handles requests for the AAA Maintenance screen.
 *
 * @author Tim Schmig
 */
public class ClubCodeMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(
            "com.ftd.marketing.actions.ClubCodeMaintAction");

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response
            responseDoc = DOMUtil.getDocument();
            
            // Build list of request parameters
            cleanFtdParams = cleanHttpParameters(request);
            logger.debug(cleanFtdParams);
            
            // get input parameter
            String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
            //logger.debug(actionType);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }
            
            if (actionType.equals("update")) {
                logger.debug("update");
                cleanFtdParams.put("action_type","update");
                
                String clubCode = (String) cleanFtdParams.get("clubCode");
                String memberName = (String) cleanFtdParams.get("memberName");
                
                AaaClubVO clubVO = new AaaClubVO(clubCode, memberName);
                PartnerProgramServiceBO.recordAaaClub(clubVO);
                
                cleanFtdParams.put("result","1");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                  printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "ClubCodeMaint");

                return null;
            }
            if (actionType.equals("edit") || actionType.equals("insert")) {
                logger.debug("edit");
                logger.debug(cleanFtdParams.get("clubCode"));
                cleanFtdParams.put("action_type","edit");
                appendPageDataToXml(responseDoc, cleanFtdParams);
                
                buildPage(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) 
                {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "ClubCodeMaint");

                return null;
            }

            logger.debug("No action");

            cleanFtdParams.put("action_type","search");
            appendPageDataToXml(responseDoc, cleanFtdParams);

            Document tempDoc = PartnerProgramServiceBO.getAllAaaClubs();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            if (logger.isDebugEnabled()) 
            {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc,
                "ClubCodeMaint");

            return null;
                        
        } catch (Exception de) { // should be FtdDataException
            logger.error("de: " + de);

                cleanFtdParams.put("error_message", de.toString());
                appendPageDataToXml(responseDoc, cleanFtdParams);
                callPage = ERROR_PAGE;
        } catch (Throwable t) {
            logger.error("t: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
            throw new RuntimeException(t);
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
        }
 
    private void buildPage(Document responseDoc, HashMap cleanFtdParams) {
        try {
            String clubCode = (String) cleanFtdParams.get("clubCode");
            
            Document xmlResult = PartnerProgramServiceBO.getAaaClub(clubCode);
            DOMUtil.addSection(responseDoc, xmlResult.getChildNodes());
                    
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}