package com.ftd.marketing.actions;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberRange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.enterprise.vo.GuiPaginationVO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;
import com.ftd.marketing.vo.SourceRequestVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author Tim Schmig & JP Puzon
 */
public class SourceCodeSearchAction extends BaseAction {
    private static Log logger = LogFactory.getLog(SourceCodeSearchAction.class.getName());
    private static final String _REQUEST_KEY_ACTION_TYPE = "scActionType";
    private static final String _REQUEST_KEY_RECORD_SORT_BY = "scRecordSortBy";
    private static final String _REQUEST_VALUE_SEARCH_10_RECORDS = "search10Records";
    private static final String _REQUEST_VALUE_SEARCH_24_HOURS = "search24Hours";

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Compose common page elements.
            buildPage(responseDoc);

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);
            
            // Provide default parameter values.
            String actionType = (String) cleanFtdParams.get(_REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_NEW;
            }

            String resultsSortedBy = (String) cleanFtdParams.get(_REQUEST_KEY_RECORD_SORT_BY);

            if (resultsSortedBy == null) {
                resultsSortedBy = "sourceCode";
            }

            // Initialize.
            // Note that the -1 denotes on the XSL that the screen
            // is initializing.
            GuiPaginationVO gpVO = new GuiPaginationVO(new Integer(0),
                    new Integer(0), new Integer(1), new Integer(-1));

            if (!actionType.equals(REQUEST_VALUE_ACTION_TYPE_NEW)) {
                // Obtain the search metadata being requested.
                gpVO = extractGuiPagination(cleanFtdParams);
            }

            // Obtain the user session Id.
            String sessionId = extractSessionId(cleanFtdParams);
            
            // Include the user's field permissions in the response page.
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_DEFINITION_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_BILLING_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PAYMENT_METHOD_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PCARD_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PRICE_HEADER_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_PROMOTION_CODE_DATA,
                SecurityPermission.UPDATE);
            appendSecurityPermission(cleanFtdParams,
                SecurityResource.SOURCE_SNH_DATA, SecurityPermission.UPDATE);

            // Note that this action_type parameter check is the one used
            // by the Maintenance page, not the search page.
            if ((cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE) != null) &&
                    ((String) cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE)).equals(
                        REQUEST_VALUE_ACTION_TYPE_LOAD)) {
                try {
                    // This indicates a request to edit/add a Source record.
                    // Determine whether insert or update.
                	Document sourceDoc = SourceServiceBO.getSource((String) cleanFtdParams.get(
                                "requestSourceCode"), sessionId);

                    if (sourceDoc.getFirstChild().getChildNodes().getLength() < 1) {
                        // This is a new insert request. Verify all user permissions
                        // for required fields.
                        if (SecurityServiceBO.assertPermission(sessionId,
                                    SecurityResource.SOURCE_PRICE_HEADER_DATA,
                                    SecurityPermission.ADD) &&
                                SecurityServiceBO.assertPermission(sessionId,
                                    SecurityResource.SOURCE_SNH_DATA,
                                    SecurityPermission.ADD) &&
                                SecurityServiceBO.assertPermission(sessionId,
                                    SecurityResource.SOURCE_PAYMENT_METHOD_DATA,
                                    SecurityPermission.ADD)) {
                        	// #13046 - User should not be allowed to create a new SC which falls in the range reserved for sympathy sites
                        	String requestedSC = (String) cleanFtdParams.get("requestSourceCode");
                        	if(StringUtils.isNumeric(requestedSC)) {
                        		if(isInSympathyReservedRange(Integer.parseInt(requestedSC))) {
                        			throw new FtdSecurityException("execute: Requested Source Codes is in the range reserved for sympathy");
                        		}
                        	} 

                            // Forward to the Source Maintenance page.
                            // See below.
                        } else {
                            throw new FtdSecurityException(
                                "execute: You do not have permissions to CREATE Source Data.");
                        }
                    }

                    return mapping.findForward("SourceCodeMaint");
                } catch (FtdSecurityException fse) {
                    // Set the exception in the return data.
                	if(fse.getMessage().indexOf("reserved for sympathy") > 0) {
                		cleanFtdParams.put("result",
                                FtdSecurityException.ERROR_CODE_RESERVED_VIOLATION);
                	} else {
                    cleanFtdParams.put("result",
                        FtdSecurityException.ERROR_CODE_USER_SECURITY_VIOLATION);
                	}
                }
            } else if (actionType.equals(_REQUEST_VALUE_SEARCH_24_HOURS)) {
                logger.debug("search last 24 hours");

                // Create the search Template object.
                SourceVO sVO = new SourceVO();
                sVO.setUpdatedBy(SecurityServiceBO.getUserInfo(sessionId)
                                                  .getUserID());

                //                sVO.setUpdatedBy(userVO.getId());
                // Determine a span of the last 24 hours.
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1);
                sVO.setUpdatedOnEnd(new Date());
                sVO.setUpdatedOnStart(calendar.getTime());

                SourceRequestVO srVO = new SourceRequestVO(sVO, sessionId);

                // Submit the template to the business object.
                // IMPORTANT: NOTE how the query parameters are driven
                // by the GUI parameters of record number and max records
                // per page.
                List resultRecordCount = new ArrayList(1);
                Document searchResults = SourceServiceBO.getSourcesByUser(srVO,
                        gpVO.getRecordNumber().intValue(),
                        gpVO.getPageMaxRecordCount().intValue(),
                        resultRecordCount, resultsSortedBy);

                // Add the results to the response.
                DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                // Record the new db stats.
                gpVO.setRecordCountTotal((Integer) resultRecordCount.get(0));
                gpVO.setPageNumber(new Integer(
                        new Double(Math.ceil(
                                gpVO.getRecordNumber().doubleValue() / gpVO.getPageMaxRecordCount()
                                                                           .doubleValue())).intValue()));

                // Clear any existing parameters.
//                cleanFtdParams.clear();
                
                // Preserve the action type originally requested.
                // This is needed to perform 'search results' column heading sorting.
//                cleanFtdParams.put(_REQUEST_KEY_ACTION_TYPE, _REQUEST_VALUE_SEARCH_24_HOURS);

                // Add the pagination data to the parameter map.
                cleanFtdParams.putAll(gpVO.getDataMap());

                // Include new input parameters to the next page.
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceCodeSearch");

                return null;
            } else if (actionType.equals(_REQUEST_VALUE_SEARCH_10_RECORDS)) {
                logger.debug("search last 10 records");

                // Create the search Template object.
                SourceVO sVO = new SourceVO();
                sVO.setUpdatedBy(SecurityServiceBO.getUserInfo(sessionId)
                                                  .getUserID());

                //                sVO.setUpdatedBy(userVO.getId());
                SourceRequestVO srVO = new SourceRequestVO(sVO, sessionId);

                // Submit the template to the business object.
                // IMPORTANT: NOTE how the query parameters are driven
                // by the GUI parameters of record number and max records
                // per page.
                List resultRecordCount = new ArrayList(1);
                Document searchResults = SourceServiceBO.getSourcesByUser(srVO, 1,
                        10, resultRecordCount, resultsSortedBy);

                // Add the results to the response.
                DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                // Record the new db stats.
                Integer screenTotalRecordCount = (Integer) resultRecordCount.get(0);

                if (screenTotalRecordCount.intValue() > 10) {
                    screenTotalRecordCount = new Integer(10);
                }

                gpVO.setRecordCountTotal(screenTotalRecordCount);
                gpVO.setPageNumber(new Integer(
                        new Double(Math.ceil(
                                gpVO.getRecordNumber().doubleValue() / gpVO.getPageMaxRecordCount()
                                                                           .doubleValue())).intValue()));

                // Clear any existing parameters.
//                cleanFtdParams.clear();
                
                // Preserve the action type originally requested.
                // This is needed to perform 'search results' column heading sorting.
//                cleanFtdParams.put(_REQUEST_KEY_ACTION_TYPE, _REQUEST_VALUE_SEARCH_10_RECORDS);

                // Add the pagination data to the parameter map.
                cleanFtdParams.putAll(gpVO.getDataMap());

                // Include new input parameters to the next page.
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceCodeSearch");

                return null;
            } else if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_SEARCH)) {
                // Create the search Template object.
                SourceVO sVO = new SourceVO();
                sVO.setId((String) cleanFtdParams.get("scSourceCode"));
                sVO.setUpdatedBy((String) cleanFtdParams.get("scUpdatedBy"));
                sVO.setRequestedBy((String) cleanFtdParams.get("scRequestedBy"));
                sVO.setCompanyId((String) cleanFtdParams.get("scCompanyId"));
                sVO.setSourceType((String) cleanFtdParams.get("scSourceType"));
                sVO.setPriceHeaderId((String) cleanFtdParams.get(
                        "scPriceHeaderId"));
                sVO.setSnhId((String) cleanFtdParams.get("scSnhId"));

                sVO.setProgramWebsiteUrl((String) cleanFtdParams.get(
                        "scProgramWebsiteUrl"));
                sVO.setPromotionCode((String) cleanFtdParams.get(
                        "scPromotionCode"));
                sVO.setDefaultSourceCodeFlag((String) cleanFtdParams.get(
                        "scDefaultSourceCodeFlag"));
                sVO.setJcpenneyFlag((String) cleanFtdParams.get(
                        "scJcpenneyFlag"));
                sVO.setOver21Flag((String) cleanFtdParams.get("scOver21Flag"));
                sVO.setIotwFlag((String) cleanFtdParams.get("scIotwFlag"));

                // Date handling
                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

                if (((String) cleanFtdParams.get("scEndDate") != null) &&
                        (((String) cleanFtdParams.get("scEndDate")).length() > 0)) {
                    sVO.setEndDate(df.parse(request.getParameter("scEndDate")));
                }

                if (((String) cleanFtdParams.get("scUpdatedOnStart") != null) &&
                        (((String) cleanFtdParams.get("scUpdatedOnStart")).length() > 0)) {
                    sVO.setUpdatedOnStart(df.parse(request.getParameter(
                                "scUpdatedOnStart")));
                }

                if (((String) cleanFtdParams.get("scUpdatedOnEnd") != null) &&
                        (((String) cleanFtdParams.get("scUpdatedOnEnd")).length() > 0)) {
                    sVO.setUpdatedOnEnd(df.parse(request.getParameter(
                                "scUpdatedOnEnd")));
                }

                SourceRequestVO srVO = new SourceRequestVO(sVO, sessionId);

                // Add any user-specified program name metric.
                srVO.setProgramName((String) cleanFtdParams.get("scProgramName"));

                // Submit the template to the business object.
                // IMPORTANT: NOTE how the query parameters are driven
                // by the GUI parameters of record number and max records
                // per page.
                List resultRecordCount = new ArrayList(1);
                Document searchResults = SourceServiceBO.getSources(srVO,
                        gpVO.getRecordNumber().intValue(),
                        gpVO.getPageMaxRecordCount().intValue(),
                        resultRecordCount, resultsSortedBy);

                // Add the results to the response.
                DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                // Record the new db stats.
                gpVO.setRecordCountTotal((Integer) resultRecordCount.get(0));
                gpVO.setPageNumber(new Integer(
                        new Double(Math.ceil(
                                gpVO.getRecordNumber().doubleValue() / gpVO.getPageMaxRecordCount()
                                                                           .doubleValue())).intValue()));

                // Add the pagination data to the parameter map.
                cleanFtdParams.putAll(gpVO.getDataMap());

                // Include any original and new input parameters to the next page.
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceCodeSearch");

                return null;
            }

            logger.debug("NEW");

            //                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            //                cleanFtdParams.put(REQUEST_KEY_ACTION_TYPE, actionType);
            //
            //                cleanFtdParams.putAll(gpVO.getDataMap());
            // Include any new and existing data parameters in the next page.        
            //                appendPageDataToXml(responseDoc, cleanFtdParams);
            // Include any new and existing data parameters in the next page.
            cleanFtdParams.putAll(gpVO.getDataMap());
            appendPageDataToXml(responseDoc, cleanFtdParams);

            if (logger.isDebugEnabled()) {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc, "SourceCodeSearch");

            return null;
        } catch (Throwable t) {
            logger.error("execute: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    private void buildPage(Document responseDoc) {
        try {
        	Document tempDoc = ReferenceServiceBO.getAllCompanies();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllPricingCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllServiceFeeCodes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllRedemptionRates();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
            
            tempDoc = SecurityServiceBO.getUsersByRole(MarketingConstants.AAS_USERS_MKT_ROLE_NAME);
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = SecurityServiceBO.getUsers(MarketingConstants.AAS_USERS_OPS_DEPARTMENT_ID);
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    
    /** Check if the requested SC falls in the range specified for Sympathy Sites.
     * @param requestedSrcCode
     * @return
     */
    private boolean isInSympathyReservedRange(int requestedSrcCode) {
    	GlobalParmHandler paramHandler = (GlobalParmHandler) CacheManager
		.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");

		String scMinValue = paramHandler.getGlobalParm(SourceCodeConstants.MARKETING_CONFIG_CONTEXT,
		SourceCodeConstants.SYMPATHY_SC_MIN_VAL);
		
		String scMaxValue = paramHandler.getGlobalParm(SourceCodeConstants.MARKETING_CONFIG_CONTEXT,
				SourceCodeConstants.SYMPATHY_SC_MAX_VAL);
		
		if(scMinValue != null && scMaxValue != null) {
			if(StringUtils.isNumeric(scMinValue) && StringUtils.isNumeric(scMaxValue)) {
				NumberRange range = new NumberRange(Integer.parseInt(scMinValue), Integer.parseInt(scMaxValue));
				
				if(range.containsNumber(requestedSrcCode)) {
					return true;
				}
			}
		}
		return false;
	}

}
