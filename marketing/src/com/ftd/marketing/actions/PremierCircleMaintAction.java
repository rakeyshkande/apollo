package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;

import com.ftd.marketing.exception.FtdDataException;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.util.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import com.ftd.marketing.constants.MarketingConstants;

/**
 * This handles requests for the Source Type Maintenance screen.
 *
 * @author Shilpa Noone
 */

public class PremierCircleMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(PremierCircleMaintAction.class.getName());
    private static final String ERROR_ACTION = "Error";
    private HashMap pageData= new HashMap();
    private File errorStylesheet;
    private String errorStylesheetName;
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String context = (String)request.getParameter("context");
        String securityToken = (String)request.getParameter("securitytoken");
        Document responseDoc = null;
        HashMap cleanFtdParams = new HashMap();
        //TraxUtil traxUtil = null;
        
    	try {        
    		responseDoc = DOMUtil.getDocument();

    		cleanFtdParams = cleanHttpParameters(request);
            logger.debug(cleanFtdParams);
            
        	if (ServletHelper.isValidToken(context, securityToken)) {
            String  campURL = ConfigurationUtil.getInstance().getFrpGlobalParm(
               MarketingConstants.PREMIER_CIRCLE_CONTEXT, MarketingConstants.PREMIER_CIRCLE_CAMP_URL);
	            if(campURL != null) {
	            	campURL += "?token="+securityToken+"&frame=1";
	            	logger.debug(" campURL " + campURL);
	            	this.pageData.put("campURL",campURL);
	            	DOMUtil.addSection(responseDoc, "pageData", "data", pageData, false);
	            	cleanFtdParams.put("result",campURL);
	            	//cleanFtdParams.put("campURL",campURL);
	                appendPageDataToXml(responseDoc, cleanFtdParams);
	                if (logger.isDebugEnabled()) {
	                    printXml(responseDoc);
	                }
	                goToPage(mapping, request, response, responseDoc,"premierCircleMaint");	
	                //return mapping.findForward(MarketingConstants.XSL_PREMIER_CIRCLE_MAINT);	                
	            }
	            //TO DO: else handler if the CAMP URL is null
           } else {
                logger.error("Invalid security token " + securityToken);
                ServletHelper.redirectToLogin(request, response);
           }
        } catch (Exception e) {
            logger.error("Could not obtain "+ MarketingConstants.PREMIER_CIRCLE_CAMP_URL +
                         " from " + MarketingConstants.PREMIER_CIRCLE_CONTEXT + " due to error: " + e);
        }
        return null;
    }
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}
