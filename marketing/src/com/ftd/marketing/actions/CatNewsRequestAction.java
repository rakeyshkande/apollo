package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.CompanyVO;
import com.ftd.enterprise.vo.CustomerVO;
import com.ftd.enterprise.vo.EmailVO;
import com.ftd.enterprise.vo.PhoneType;
import com.ftd.enterprise.vo.PhoneVO;
import com.ftd.marketing.bo.CustomerServiceBO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.EntityLockedException;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.CatalogRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author Tim Schmig
 */
public class CatNewsRequestAction extends BaseAction {
    private static Log logger = LogFactory.getLog(CatNewsRequestAction.class.getName());

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // get input parameter
            String actionType = (String) cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_INSERT)) {
                // Since an input customer has been supplied, proceed to
                // insert the new record.
                // build PhoneVO
                PhoneVO dayPhoneVO = new PhoneVO(PhoneType.DAYTIME_PHONE_TYPE,
                        (String) cleanFtdParams.get("daytimePhone"),
                        (String) cleanFtdParams.get("daytimeExt"));
                PhoneVO eveningPhoneVO = new PhoneVO(PhoneType.EVENING_PHONE_TYPE,
                        (String) cleanFtdParams.get("eveningPhone"),
                        (String) cleanFtdParams.get("eveningExt"));

                // build EmailVO
                EmailVO emailVO = new EmailVO();
                emailVO.setEmailAddress((String) cleanFtdParams.get(
                        "emailAddress"));

                // build CustomerVO
                String custId = null;
                CustomerVO custVO = new CustomerVO(custId);
                custVO.setFirstName((String) cleanFtdParams.get("firstName"));
                custVO.setLastName((String) cleanFtdParams.get("lastName"));
                custVO.setAddress1((String) cleanFtdParams.get("address1"));
                custVO.setAddress2((String) cleanFtdParams.get("address2"));
                custVO.setCity((String) cleanFtdParams.get("city"));
                custVO.setState((String) cleanFtdParams.get("state"));
                custVO.setZipCode((String) cleanFtdParams.get("zipCode"));
                custVO.setCountry((String) cleanFtdParams.get("country"));
                custVO.addPhone(dayPhoneVO);
                custVO.addPhone(eveningPhoneVO);
                custVO.setEmailVO(emailVO);

                // build CompanyVO
                CompanyVO companyVO = new CompanyVO((String) cleanFtdParams.get(
                            "companyCode"));

                CatalogRequestVO catRequestVO = new CatalogRequestVO(custVO,
                        companyVO, extractSessionId(cleanFtdParams));

                try {
                    CustomerServiceBO.subscribeCustomer(catRequestVO);

                    // Return the number of records added.  Will always be 1.
                    cleanFtdParams.put("result", "1");
                    cleanFtdParams.put("customer_id", custVO.getId());
                } catch (FtdDataException fde) {
                    // Special handling for user-presentable errors.
                    if (fde.isUniquenessViolation()) {
                        // Set the appropriate response disposition
                        // for a primary key violation.
                        cleanFtdParams.put("customer_id", custVO.getId());
                        cleanFtdParams.put(REQUEST_KEY_RESULT,
                            FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                    } else {
                        throw fde;
                    }
                }
            }

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_UPDATE)) {
                /* update Customer screen */
                String lockResult = "";
                ActionForward actionForward = new ActionForward();
                boolean updateCustomer = true;

                String customerId = (String) cleanFtdParams.get("customer_id");
                String sessionId = extractSessionId(cleanFtdParams);

                try {
                    CustomerServiceBO.lockCustomerRecord(customerId, sessionId);
                } catch (EntityLockedException e) {
                    updateCustomer = false;
                    lockResult = e.getMessage();
                }

                /* attempt to lock customer record */
                if (updateCustomer == true) {
                    actionForward = mapping.findForward("UpdateCustomer");

                    String path = retrieveBaseURL() + actionForward.getPath();

                    String context = (String) cleanFtdParams.get(MarketingConstants.CONTEXT);

                    if (context == null) {
                        /* attempt to retrieve from request */
                        context = request.getParameter(MarketingConstants.CONTEXT);
                    }

                    path = path + "?customer_id=" + customerId + "&" +
                        MarketingConstants.CONTEXT + "=" + context + "&" +
                        MarketingConstants.SEC_TOKEN + "=" + sessionId +
                        "&display_page=MAIN";
                    logger.debug(path);

                    /* queue screen */
                    actionForward = new ActionForward(path, true);

                    return actionForward;
                } else {
                    cleanFtdParams.put("lock_error", lockResult);
                    cleanFtdParams.put("result", "-2");
                }
            }

            // Compose common page elements.
            buildPage(responseDoc);

            appendPageDataToXml(responseDoc, cleanFtdParams);

            if (logger.isDebugEnabled()) {
                printXml(responseDoc);
            }

            goToPage(mapping, request, response, responseDoc, "CatNewsRequest");

            return null;
        } catch (Throwable t) {
            logger.error(t);
            cleanFtdParams.put(REQUEST_KEY_ERROR_MESSAGE, t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    private void buildPage(Document responseDoc) {
        try {
            Document tempDoc = ReferenceServiceBO.getAllCompanies();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllStates();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

            tempDoc = ReferenceServiceBO.getAllCountries();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
