package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.exception.IFtdException;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedWriter;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * This handles requests for the Master Source Code List Maint screen.
 *
 * @author JP Puzon
 */
public class SourceListMaintAction extends BaseAction {
    private static Log logger = LogFactory.getLog(SourceListMaintAction.class.getName());
    protected static final String _REQUEST_VALUE_ACTION_TYPE_RESERVE = "reserve";

    /**
     *
     *
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {
        String callPage = null;
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Compose common page elements.
            buildPage(responseDoc);

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // Provide default parameter values.
            String actionType = (String) cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
            }

            if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_INSERT)) {
                // This will record the number of source code records
                // reserved or released.
                int result = 0;

                // Verify input data.
                String sourceCodeStart = (String) cleanFtdParams.get(
                        "sourceCodeStart");

                if (sourceCodeStart == null) {
                    throw new FtdDataException(
                        "execute: sourceCodeStart can not be null.");
                }

                String sourceCodeEnd = (String) cleanFtdParams.get(
                        "sourceCodeEnd");

                String radioAction = (String) cleanFtdParams.get("radioAction");

                if (radioAction == null) {
                    throw new FtdDataException(
                        "execute: radioAction must be not null.");
                }

                // Record the number of sources affected.
                cleanFtdParams.put(REQUEST_KEY_RESULT,
                    SourceServiceBO.countSourceCodes(sourceCodeStart,
                        sourceCodeEnd) + "");

                // Create the template object.
                SourceVO sVO = new SourceVO(sourceCodeStart);

                Document conflictSourcesXml = null;

                if (radioAction.equals(_REQUEST_VALUE_ACTION_TYPE_RESERVE)) {
                    // Verify the supporting data.
                    String description = (String) cleanFtdParams.get(
                            "description");
                    String sourceType = (String) cleanFtdParams.get(
                            "sourceType");

                    if ((description == null) || (sourceType == null)) {
                        throw new FtdDataException(
                            "execute: description and sourceType must be not null.");
                    }

                    sVO.setDescription(description);
                    sVO.setSourceType(sourceType);
                    sVO.setUpdatedBy(lookupCurrentUserId(extractSessionId(cleanFtdParams)));


                    // Reserve the requested source code(s).
                    // The returned collection are those records that
                    // already have been reserved, and are therefore
                    // in conflict with the request.
                    conflictSourcesXml = SourceServiceBO.reserveSourceCodes(sVO,
                            sourceCodeStart, sourceCodeEnd);

                    if (conflictSourcesXml == null) {
                    // This indicates that no matching master records could be evaluated.
                    cleanFtdParams.put(REQUEST_KEY_RESULT, IFtdException.ERROR_CODE_REFERENTIAL_INTEGRITY_VIOLATION);
                    } else if (conflictSourcesXml.getFirstChild().getChildNodes()
                                              .getLength() > 0) {
                        // If any records are returned, present to the client as an error.
                        cleanFtdParams.put(REQUEST_KEY_RESULT,
                            FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                        DOMUtil.addSection(responseDoc,
                            conflictSourcesXml.getChildNodes());
                    }
                } else {
                    // Release a range of source codes.
                    // The returned collection are those records that
                    // already have been created, and are therefore
                    // in conflict with the request.
                    conflictSourcesXml = SourceServiceBO.releaseSourceCodes(sVO,
                            sourceCodeStart, sourceCodeEnd);
                    if (conflictSourcesXml == null) {
                    // This indicates that no matching master records could be evaluated.
                    cleanFtdParams.put(REQUEST_KEY_RESULT, IFtdException.ERROR_CODE_REFERENTIAL_INTEGRITY_VIOLATION);
                    } else if (conflictSourcesXml.getFirstChild().getChildNodes()
                                              .getLength() > 0) {
                        // If any records are returned, present to the client as an error.
                        cleanFtdParams.put(REQUEST_KEY_RESULT,
                            FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
                        DOMUtil.addSection(responseDoc,
                            conflictSourcesXml.getChildNodes());
                    }
                }

                // Include input parameters to the next page.
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceListMaint");

                return null;
            } else if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_SEARCH)) {
                CachedResultSet tuples = SourceServiceBO.getAllReservedSourceCodes();

                // Compose the collection of records for presentation,
                // a delimited list of values.
                StringBuffer sb = new StringBuffer("SOURCE CODE\tSOURCE TYPE\t");
                sb.append("DESCRIPTION\tRESERVED\tORDER SOURCE\tWEBSITE URL\n");

                // tab-delimited so Excel will provide automatic formatting.
                while (tuples.next()) {
                    sb.append((String) tuples.getObject(1)); // reserve source code
                    sb.append("\t");

                    // Check if the reserve has a corresponding source record.
                    if (tuples.getObject(6) == null) {
                        // Actual source record has NOT been created yet.
                        sb.append((String) tuples.getObject(3)); // reserve source type
                        sb.append("\t");
                        sb.append((String) tuples.getObject(2)); // reserve description
                        sb.append("\t");
                        sb.append("Reserved");
                    } else {
                        // Actual source record HAS been created.
                        sb.append((String) tuples.getObject(5)); // source type
                        sb.append("\t");
                        sb.append((String) tuples.getObject(4)); // source description
                        sb.append("\t");
                        sb.append("\t");
                        sb.append((String) tuples.getObject(6)); // source order source

                        if (tuples.getObject(7) != null) {
                            sb.append("\t");
                            sb.append((String) tuples.getObject(7)); // source website url
                        }
                    }

                    sb.append("\n");
                }

                // Set content type and other response header fields first
                // Set the output data's mime type.
                response.setContentType("application/vnd.ms-excel"); // MIME type for excel sheet

                BufferedWriter writer = new BufferedWriter(response.getWriter());

                try {
                    writer.write(sb.toString());
                } finally {
                    if (writer != null) {
                        writer.flush();
                        writer.close();
                    }
                }

                return null;
            } else {
                logger.debug("load");

                // Initialize the behavior of the page.
                cleanFtdParams.put(REQUEST_KEY_RESULT, "0");
                cleanFtdParams.put("radioAction",
                    _REQUEST_VALUE_ACTION_TYPE_RESERVE);

                // Include any new and existing data parameters in the next page.        
                appendPageDataToXml(responseDoc, cleanFtdParams);

                if (logger.isDebugEnabled()) {
                    printXml(responseDoc);
                }

                goToPage(mapping, request, response, responseDoc,
                    "SourceListMaint");

                return null;
            }
        } catch (Throwable t) {
            logger.error("execute: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;

            //            throw new RuntimeException(t);
        }

        logger.debug("callPage=" + callPage);

        if ((callPage != null) && (callPage.trim().length() > 0)) {
            goToPage(mapping, request, response, responseDoc, callPage);

            return null;
        } else {
            return null;
        }
    }

    private void buildPage(Document responseDoc) {
        try {
        	Document tempDoc = SourceServiceBO.getAllSourceTypes();
            DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
