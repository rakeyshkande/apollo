package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.SourceDefaultOrderInfoVO;
import com.ftd.marketing.bo.ReferenceServiceBO;
import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * This handles requests for the Source Code Default Order Info Maintenance Request screen.
 *
 */
public class SourceCodeDefaultOrderInfoMaintAction
  extends BaseAction
{
  private static Log logger = LogFactory.getLog(SourceCodeDefaultOrderInfoMaintAction.class.getName());
  private static final String _REQUEST_KEY_SOURCE_CODE = "requestSourceCode";

  /**
   * 
   * 
   * @param mapping - ActionMapping used to select this instance
   * @param form - ActionForm (optional) bean for this request
   * @param request - HTTP Request we are processing
   * @param response - HTTP Response we are processing
   * 
   * @return forwarding action - next action to "process" request
   * 
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                               HttpServletResponse response)
    throws IOException, ServletException
  {
    String callPage = null;
    HashMap cleanFtdParams = new HashMap();
    Document responseDoc = null;

    try
    {
      // initialize the response content.
      responseDoc = DOMUtil.getDocument();

      // Clean the http parameters by stripping out nulls and empty
      // strings.
      cleanFtdParams = cleanHttpParameters(request);

      // Provide default parameter values.
      String actionType = request.getParameter(REQUEST_KEY_ACTION_TYPE);
      logger.debug("action=" + actionType);

      if (StringUtils.isBlank(actionType))
      {
        actionType = REQUEST_VALUE_ACTION_TYPE_LOAD;
      }

      // This is the source code value passed from the originating page.
      String requestSourceCode = 
        (request.getParameter(_REQUEST_KEY_SOURCE_CODE) == null)? null: request.getParameter(_REQUEST_KEY_SOURCE_CODE).toUpperCase();

      if ((requestSourceCode == null) || (requestSourceCode.length() == 0))
      {
        throw new FtdDataException("execute: The requested source code can not be null.");
      }
      
      if(REQUEST_VALUE_ACTION_TYPE_UPDATE.equalsIgnoreCase(actionType)) 
      {
        // handle save scenario
        SourceDefaultOrderInfoVO sourceDefaultOrderInfoVO = buildOrderInfoFromRequest(cleanFtdParams);
        SourceServiceBO.persistSourceDefaultOrderInfo(sourceDefaultOrderInfoVO);
        cleanFtdParams.put("SUBMIT_ACTION", "true");
      } else if(REQUEST_VALUE_ACTION_TYPE_LOAD.equalsIgnoreCase(actionType) == false)
      {
        throw new FtdDataException("execute: The actionType requested is not supported: " + actionType);
      }
      
      // Compose common page elements.
      buildPage(responseDoc);
      
      // Retrieve the latest version of the information from the database
      SourceDefaultOrderInfoVO sourceDefaultOrderInfoVO = SourceServiceBO.getSourceDefaultOrderInfo(requestSourceCode);

      // The page needs to load db record data.
      // XMLDocument xmlResultSource = SourceServiceBO.getSource(requestSourceCode, extractSessionId(cleanFtdParams));
      DOMUtil.addSection(responseDoc, sourceDefaultOrderInfoVO.toXMLDocument().getChildNodes());

      // Include any original and new input parameters to the next page.
      appendPageDataToXml(responseDoc, cleanFtdParams);

      if (logger.isDebugEnabled())
      {
        printXml(responseDoc);
      }


      setPageNoCache(response); // Always fetch content from server, do not cache contents of this page on client side
      goToPage(mapping, request, response, responseDoc, "sourceCodeDefaultOrderInfoMaint");

      return null;
    }
    catch (Throwable t)
    {
      logger.error("", t);
      cleanFtdParams.put(REQUEST_KEY_ERROR_MESSAGE, t.getMessage());
      appendPageDataToXml(responseDoc, cleanFtdParams);
      callPage = ERROR_PAGE;
    }

    logger.debug("callPage=" + callPage);

    if ((callPage != null) && (callPage.trim().length() > 0))
    {
      goToPage(mapping, request, response, responseDoc, callPage);

      return null;
    }
    else
    {
      return null;
    }
  }


  /**
   * Inserts reference data into the response document
   * @param responseDoc
   */
  private void buildPage(Document responseDoc)
  {
    try
    {
      Document tempDoc = ReferenceServiceBO.getCountryListOE();
      DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

      tempDoc = ReferenceServiceBO.getStateListOE();
      DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());
      
      tempDoc = ReferenceServiceBO.getAddressTypes();
      DOMUtil.addSection(responseDoc, tempDoc.getChildNodes());

    }
    catch (Exception e)
    {
      throw new RuntimeException(e);
    }
    
  }

  /** Add no cache headers to the Page to ensure the response is not browser cached **/
  private void setPageNoCache(HttpServletResponse response)
  {
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", 1);  
  }

  /**
   * Parses the request parameters and builds a SourceDefaultOrderInfoVO instance from it
   * @param sourceCode
   * @param requestParameters
   * @return
   */
  private SourceDefaultOrderInfoVO buildOrderInfoFromRequest(HashMap requestParameters) throws Exception
  {
    SourceDefaultOrderInfoVO retVal = new SourceDefaultOrderInfoVO();
    
    retVal.setCustAddress((String) requestParameters.get("customerAddress"));
    retVal.setCustCity((String) requestParameters.get("customerCity"));
    retVal.setCustCountryId((String) requestParameters.get("customerCountry"));
    retVal.setCustDaytimePhone((String) requestParameters.get("customerDaytimePhone"));
    retVal.setCustDaytimePhoneExt((String) requestParameters.get("customerDaytimePhoneExt"));
    retVal.setCustEmailAddress((String) requestParameters.get("customerEmailAddress"));
    retVal.setCustEveningPhone((String) requestParameters.get("customerEveningPhone"));
    retVal.setCustEveningPhoneExt((String) requestParameters.get("customerEveningPhoneExt"));
    retVal.setCustFirstName((String) requestParameters.get("customerFirstName"));
    retVal.setCustLastName((String) requestParameters.get("customerLastName"));
    retVal.setCustStateId((String) requestParameters.get("customerState"));
    retVal.setCustZipCode((String) requestParameters.get("customerZipCode"));
    retVal.setRecptAddress((String) requestParameters.get("recipientAddress"));
    retVal.setRecptBusinessName((String) requestParameters.get("recipientBusinessName"));
    retVal.setRecptCity((String) requestParameters.get("recipientCity"));
    retVal.setRecptCountryId((String) requestParameters.get("recipientCountry"));
    retVal.setRecptLocationDetail((String) requestParameters.get("recipientLocationDetail"));
    retVal.setRecptLocationType((String) requestParameters.get("recipientLocationType"));
    retVal.setRecptPhone((String) requestParameters.get("recipientPhone"));
    retVal.setRecptPhoneExt((String) requestParameters.get("recipientPhoneExt"));
    retVal.setRecptStateId((String) requestParameters.get("recipientState"));
    retVal.setRecptZipCode((String) requestParameters.get("recipientZipCode"));
    retVal.setSourceCode((String) requestParameters.get("requestSourceCode"));
    
    String userId = lookupCurrentUserId(extractSessionId(requestParameters));
    retVal.setUpdatedBy(userId);

    return retVal;    
  }
}
