package com.ftd.marketing.actions;

import com.ftd.enterprise.vo.GuiPaginationVO;
import com.ftd.enterprise.vo.ProductFeedVO;

import com.ftd.marketing.bo.ProductServiceBO;
import com.ftd.marketing.constants.MarketingConstants;

import com.ftd.marketing.exception.IFtdException;
import com.ftd.osp.utilities.xml.DOMUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This handles requests for the Catalog/Newsletter Request screen.
 *
 * @author JP Puzon
 */
public class ProductFeedSearchAction extends BaseAction {
    private static Log logger = 
        LogFactory.getLog(ProductFeedSearchAction.class.getName());
    private static final String _REQUEST_KEY_ACTION_TYPE = "scActionType";
    private static final String _REQUEST_KEY_RECORD_SORT_BY = "scRecordSortBy";
    private static final String _REQUEST_KEY_PRODUCT_ID = "scProductId";
    private static final String _REQUEST_KEY_NOVATOR_ID = "scNovatorId";
    private static final String _REQUEST_VALUE_ACTION_TYPE_GENERATE_FILE = 
        "generateFile";

    /**
     * @param mapping            - ActionMapping used to select this instance
     * @param form               - ActionForm (optional) bean for this request
     * @param request            - HTTP Request we are processing
     * @param response           - HTTP Response we are processing
    *
     * @return forwarding action - next action to "process" request
    *
     * @throws IOException
     * @throws ServletException
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException {
        String callPage = "ProductFeedSearch";
        HashMap cleanFtdParams = new HashMap();
        Document responseDoc = null;

        try {
            // initialize the response content.
            responseDoc = DOMUtil.getDocument();

            // Clean the http parameters by stripping out nulls and empty
            // strings.
            cleanFtdParams = cleanHttpParameters(request);

            // Provide default parameter values.
            String actionType = 
                (String)cleanFtdParams.get(_REQUEST_KEY_ACTION_TYPE);

            if (actionType == null) {
                actionType = REQUEST_VALUE_ACTION_TYPE_NEW;
            }

            String resultsSortedBy = 
                (String)cleanFtdParams.get(_REQUEST_KEY_RECORD_SORT_BY);

            if (resultsSortedBy == null) {
                resultsSortedBy = "productId";
            }

            // Initialize.
            // Note that the -1 denotes on the XSL that the screen
            // is initializing.
            GuiPaginationVO gpVO = 
                new GuiPaginationVO(new Integer(0), new Integer(0), 
                                    new Integer(1), new Integer(-1));

            if (!actionType.equals(REQUEST_VALUE_ACTION_TYPE_NEW)) {
                // Obtain the search metadata being requested.
                gpVO = extractGuiPagination(cleanFtdParams);
                logger.debug("extract: Original search metrics: scRecordCountTotal=" + gpVO.getRecordCountTotal());
            }
            
            String scProductId = (String)cleanFtdParams.get(_REQUEST_KEY_PRODUCT_ID);
            String scNovatorId = (String)cleanFtdParams.get(_REQUEST_KEY_NOVATOR_ID);

            // Note that this action_type parameter check is the one used
            // by the Maintenance page, not the search page.
            if ((cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE) != null) && 
                ((String)cleanFtdParams.get(REQUEST_KEY_ACTION_TYPE)).equals(REQUEST_VALUE_ACTION_TYPE_RELOAD)) {
                
                logger.debug("execute: Original search criteria: scProductId=" + 
                scProductId + " scNovatorId=" + scNovatorId);
                // If the original search page had a result set, redisplay it. 
                if ((scProductId == null || scProductId == "") && 
                (scNovatorId == null || scNovatorId == "")) {
                
                    logger.debug("extract: Restoring original search results.");

                    List resultRecordCount = new ArrayList(1);
                    Document searchResults = 
                        ProductServiceBO.getFeedProducts(new ProductFeedVO(), 
                                                         gpVO.getRecordNumber().intValue(), 
                                                         gpVO.getPageMaxRecordCount().intValue(), 
                                                         resultRecordCount, 
                                                         resultsSortedBy);

                    // Add the results to the response.
                    DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                    // Record the new db stats.
                    gpVO.setRecordCountTotal((Integer)resultRecordCount.get(0));
                    gpVO.setPageNumber(new Integer(new Double(Math.ceil(gpVO.getRecordNumber().doubleValue() / 
                                                                        gpVO.getPageMaxRecordCount().doubleValue())).intValue()));    
                } else {
                    logger.debug("extract: Clearing original search results.");
                    // Clear the values for the next workflow.
                     cleanFtdParams.put(_REQUEST_KEY_PRODUCT_ID, "");
                    cleanFtdParams.put(_REQUEST_KEY_NOVATOR_ID, "");
                    gpVO.setRecordCountTotal(new Integer(-1));
                }
            } else if (actionType.equals(REQUEST_VALUE_ACTION_TYPE_SEARCH)) {
                // Submit the template to the business object.
                // IMPORTANT: NOTE how the query parameters are driven
                // by the GUI parameters of record number and max records
                // per page.
                ProductFeedVO pfVO = new ProductFeedVO();
                pfVO.setId(scProductId);
                pfVO.setNovatorId(scNovatorId);
                //pfVO.setActiveFlag((String) cleanFtdParams.get("scActiveFlag"));

                List resultRecordCount = new ArrayList(1);
                Document searchResults = 
                    ProductServiceBO.getFeedProducts(pfVO, 
                                                     gpVO.getRecordNumber().intValue(), 
                                                     gpVO.getPageMaxRecordCount().intValue(), 
                                                     resultRecordCount, 
                                                     resultsSortedBy);

                // Add the results to the response.
                DOMUtil.addSection(responseDoc, searchResults.getChildNodes());

                // Record the new db stats.
                gpVO.setRecordCountTotal((Integer)resultRecordCount.get(0));
                gpVO.setPageNumber(new Integer(new Double(Math.ceil(gpVO.getRecordNumber().doubleValue() / 
                                                                    gpVO.getPageMaxRecordCount().doubleValue())).intValue()));

            } else if (actionType.equals(_REQUEST_VALUE_ACTION_TYPE_GENERATE_FILE)) {
                try {
                    logger.debug("Generating product feed file....");
                    ProductServiceBO.activateProductFeed();
                    // Add the results to the response for SUCCESS.
                    gpVO.setRecordCountTotal(new Integer(MarketingConstants.CONS_RESULT_SUCCESS));
                } catch (Exception e) {
                    logger.error("execute: Product feed file generation failed.", 
                                 e);
                    // Add the results to the response for FAILURE.
                    gpVO.setRecordCountTotal(new Integer(IFtdException.ERROR_CODE_MANUAL_INTERVENTION_REQUIRED));
                }

            }

            // Include any new and existing data parameters in the next page.
            cleanFtdParams.putAll(gpVO.getDataMap());
            appendPageDataToXml(responseDoc, cleanFtdParams);

        } catch (Throwable t) {
            logger.error("execute: " + t);
            cleanFtdParams.put("error_message", t.getMessage());
            appendPageDataToXml(responseDoc, cleanFtdParams);
            callPage = ERROR_PAGE;
        }

        logger.debug("callPage=" + callPage);

        if (logger.isDebugEnabled()) {
            try {
                printXml(responseDoc);
            } catch (Exception e) {
                logger.error("execute: Could not debug-output result XML.", e);
            }
        }

        goToPage(mapping, request, response, responseDoc, callPage);

        return null;
    }
}
