package com.ftd.marketing.bo;

import com.ftd.enterprise.dao.SecurityDAO;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;

import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.exception.FtdSecurityException;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;


public class SecurityServiceBO {
    protected static Log logger = LogFactory.getLog(SecurityServiceBO.class.getName());

    /**
     * Constructor to prevent instantiation of this class.
     */
    private SecurityServiceBO() {
        super();
    }

    public static Document getUsers(String departmentId)
        throws FtdDataException, Exception {
        // get list of source type codes
        return SecurityDAO.getUsers(departmentId);
    }
    
    public static Document getUsersByRole(String roleName)
        throws FtdDataException, Exception {
        // get list of source type codes
        return SecurityDAO.getUsersByRole(roleName);
    }

    public static boolean assertPermission(String sessionId,
        SecurityResource resource, SecurityPermission permission)
        throws FtdSecurityException {
        try {
            return SecurityManager.getInstance().assertPermission("Order Proc",
                sessionId, resource.toString(), permission.toString());
        } catch (Exception e) {
            logger.error(e);
            throw new FtdSecurityException("assertPermission: Failed.", e);
        }
    }

    public static UserInfo getUserInfo(String sessionId)
        throws FtdSecurityException {
        UserInfo userInfo = null;

        try {
            // get user ID
            // use security manager to get user info object. extract csr_id from it.
            // check for null pointers
            userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
        } catch (Exception e) {
            throw new FtdSecurityException(
                "getUserInfo: User session ID verification failed: " +
                sessionId, e);
        }

        if (userInfo == null) {
            throw new FtdSecurityException(
                "getUserInfo: User session ID record could not be found: " +
                sessionId);
        }

        return userInfo;
    }
}
