package com.ftd.marketing.bo;

import com.ftd.enterprise.dao.PricingDAO;
import com.ftd.enterprise.dao.ProductDAO;
import com.ftd.enterprise.vo.PricingCodeVO;
import com.ftd.enterprise.vo.ProductFeedVO;

import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.ProductRequestVO;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;


public class ProductServiceBO {
    protected static Log logger = LogFactory.getLog(ProductServiceBO.class.getName());

    /**
     * Constructor to prevent instantiation of this class.
     */
    private ProductServiceBO() {
        super();
    }

    public static Document getPricingCodeDetails(String pricingCode)
        throws FtdDataException, Exception {
        // get list of source type codes
        return PricingDAO.queryPricingCodeDetails(pricingCode);
    }

    public static PricingCodeVO recordPricingCode(PricingCodeVO pricingCodeVO)
        throws FtdDataException, Exception {
        // get list of source type codes
        return PricingDAO.persistPricingCode(pricingCodeVO);
    }

    public static ProductRequestVO recordFeedProduct(
        ProductRequestVO productRequestVO) throws FtdDataException, Exception {
        // get list of source type codes
        return ProductDAO.persistFeedProduct(productRequestVO);
    }

    public static Document getFeedProduct(String productId)
        throws FtdDataException, Exception {
        return ProductDAO.queryFeedProduct(new ProductFeedVO(productId), 1, 1,
            new ArrayList());
    }
    
    public static Document getFeedProductNovator(String novatorId)
        throws FtdDataException, Exception {
        ProductFeedVO pfVO = new ProductFeedVO();
        pfVO.setNovatorId(novatorId);
        return ProductDAO.queryFeedProduct(pfVO, 1, 1,
            new ArrayList());
    }

    public static Document getFeedProducts(ProductFeedVO productFeedVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy) throws FtdDataException, Exception {
        return ProductDAO.queryFeedProduct(productFeedVO, recordNumberStart,
            maxRecordCount, resultRecordCount, sortBy);
    }
    
    public static void activateProductFeed() throws Exception {
        ProductDAO.dispatchProductFeedMessage();
    }
}
