package com.ftd.marketing.bo;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ftd.enterprise.dao.GlobalDAO;
import com.ftd.enterprise.dao.SourceDAO;
import com.ftd.enterprise.vo.CpcVO;
import com.ftd.enterprise.vo.RecordStatus;
import com.ftd.enterprise.vo.SecurityPermission;
import com.ftd.enterprise.vo.SecurityResource;
import com.ftd.enterprise.vo.SourceDefaultOrderInfoVO;
import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.marketing.sourcecode.model.Florist;
import com.ftd.marketing.sourcecode.model.SourceCodeRequest;
import com.ftd.marketing.sourcecode.model.UpdateSourceCodeAttributesRequest;
import com.ftd.marketing.vo.SourceProgramRequestVO;
import com.ftd.marketing.vo.SourceRequestVO;
import com.ftd.marketing.vo.SourceTypeRequestVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This class assembles and applies any business rules to Source-related data.
 *
 * @author JP Puzon
 */
public class SourceServiceBO {
    protected static Log logger = LogFactory.getLog(SourceServiceBO.class.getName());
    private static final String _XPATH_SOURCE_NODE = "/sourceList/source";
    private static final String _XPATH_CPC_NODE = "/cpcList/cpc";
    private static final String _ORACLE_NO_UPDATE_ERROR_MSG = "No source_master_reserve records were updated";

    /**
     * Constructor to prevent instantiation of this class.
     */
    private SourceServiceBO() {
        super();
    }

    /**
     * Obtain all source type codes.
     * @throws java.lang.Exception
     * @return Document
     */
    public static Document getAllSourceTypes()
        throws FtdDataException, Exception {
        // get list of source type codes
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SOURCE_TYPE);
    }

    /**
     * Obtain all source type codes grouped by source code.
     * @throws java.lang.Exception
     * @return Document
     */
    public static CachedResultSet getAllGroupedSourceTypes()
        throws FtdDataException, Exception {
        // get list of source type codes
        return SourceDAO.queryAllGroupedSourceTypes();
    }

    /**
     * Records a new source type code.
     * @throws java.lang.Exception
     * @return SourceTypeVO containing additional metrics of persistence.
     * @param sourceTypeVO - new source type code
     */
    public static SourceTypeRequestVO recordSourceType(
        SourceTypeRequestVO sourceTypeRequestVO)
        throws FtdDataException, Exception {
        // record the new source type code.
        return SourceDAO.persistSourceType(sourceTypeRequestVO);
    }

    public static SourceRequestVO recordSource(SourceRequestVO sourceRequestVO)
        throws FtdDataException, FtdSecurityException, Exception {
        String sessionId = sourceRequestVO.getUserSessionId();

        // Read the existing Source record from the database b/c
        // if the user is restricted from editing certain fields,
        // we will have to provide default values to the DAO for persistence.
        SourceVO sVO = sourceRequestVO.getSourceVO();
        Document existingSourceDoc = SourceServiceBO.getSource(sVO.getId(),
                sessionId);
        
        logger.debug("Length of nodes in xml document existingSourceDoc: " + existingSourceDoc.getFirstChild().getChildNodes().getLength());

        //Had to change this to > 2 as there will always be one node representing the bin number data and another for source florist priority list
        if (existingSourceDoc.getFirstChild().getChildNodes().getLength() > 2) {
            NamedNodeMap attributeMap = DOMUtil.selectSingleNode(existingSourceDoc, _XPATH_SOURCE_NODE)
                                                         .getAttributes();

            // This means this operation is an update.
//            if (!SecurityServiceBO.assertPermission(sessionId,
//                        SecurityResource.SOURCE_BILLING_DATA,
//                        SecurityPermission.UPDATE)) {
//                // Substitute the metrics from the existing record.
//                sVO.setBillingInfoLogic(attributeMap.getNamedItem(
//                        "billing_info_logic").getNodeValue());
//                sVO.setBillingInfoPrompt(attributeMap.getNamedItem(
//                        "billing_info_prompt").getNodeValue());
//            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PAYMENT_METHOD_DATA,
                        SecurityPermission.UPDATE)) {
                // Substitute the metrics from the existing record.
                sVO.setPaymentMethodId(attributeMap.getNamedItem(
                        "payment_method_id").getNodeValue());
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PRICE_HEADER_DATA,
                        SecurityPermission.UPDATE)) {
                // Substitute the metrics from the existing record.
                sVO.setPriceHeaderId(attributeMap.getNamedItem(
                        "price_header_id").getNodeValue());
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PROMOTION_CODE_DATA,
                        SecurityPermission.UPDATE)) {
                // Substitute the metrics from the existing record.
                sVO.setPromotionCode(attributeMap.getNamedItem("promotion_code")
                                                 .getNodeValue());
                sVO.setBonusPromotionCode(attributeMap.getNamedItem(
                        "bonus_promotion_code").getNodeValue());
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_SNH_DATA,
                        SecurityPermission.UPDATE)) {
                // Substitute the metrics from the existing record.
                sVO.setSnhId(attributeMap.getNamedItem("snh_id").getNodeValue());
            }
            
            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.RDMPTN_RATE_DATA,
                        SecurityPermission.UPDATE)) {
                // Substitute the metrics from the existing record.
                sVO.setMpRedemptionRateId(attributeMap.getNamedItem("mp_redemption_rate_id").getNodeValue());
            }            

            // Special treatment for CPC info since this is a child table to Source.
            Document existingCpcDoc = SourceServiceBO.getCpc(sVO.getId());
            if (existingCpcDoc.getFirstChild().getChildNodes().getLength() > 0) {
                NamedNodeMap attributeMap2 = DOMUtil.selectSingleNode(existingCpcDoc, _XPATH_CPC_NODE)
                                                           .getAttributes();

                if (!SecurityServiceBO.assertPermission(sessionId,
                            SecurityResource.SOURCE_PCARD_DATA,
                            SecurityPermission.UPDATE)) {
                    // Substitute the metrics from the existing record.
                    String creditCardType = attributeMap2.getNamedItem(
                            "credit_card_type").getNodeValue();
                    String creditCardNumber = attributeMap2.getNamedItem(
                            "credit_card_number").getNodeValue();
                    String creditCardExpirationDate = attributeMap2.getNamedItem(
                            "credit_card_expiration_date").getNodeValue();

                    CpcVO cpcVO = new CpcVO(creditCardType, creditCardNumber,
                            creditCardExpirationDate);

                    sVO.getCpcCollection().clear();
                    sVO.addCpc(cpcVO);
                } else if (!sourceRequestVO.isCcNumberUpdated()){
                    // Defect 2655.
                    // Credit card number is not updated. Reset with existing value.
                    CpcVO cpcVO = (CpcVO)sVO.getCpcCollection().get(0);
                    String creditCardNumber = attributeMap2.getNamedItem(
                            "credit_card_number").getNodeValue();
                    cpcVO.setCreditCardNumber(creditCardNumber);
                    sVO.getCpcCollection().clear();
                    sVO.addCpc(cpcVO);
                }
            }
        } else {
            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PAYMENT_METHOD_DATA,
                        SecurityPermission.ADD)) {
                // Since this is a required metric of a new record, error.
                throw new FtdSecurityException(
                    "recordSource: You do not have security permission to CREATE Source Payment Method Data.");
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PRICE_HEADER_DATA,
                        SecurityPermission.ADD)) {
                // Since this is a required metric of a new record, error.
                throw new FtdSecurityException(
                    "recordSource: You do not have security permission to CREATE Source Price Code Data.");
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_SNH_DATA, SecurityPermission.ADD)) {
                // Since this is a required metric of a new record, error.
                throw new FtdSecurityException(
                    "recordSource: You do not have security permission to CREATE Source Service Fee Data.");
            }
            
            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.RDMPTN_RATE_DATA, SecurityPermission.ADD)) {
                // Since this is a required metric of a new record, error.
                throw new FtdSecurityException(
                    "recordSource: You do not have security permission to CREATE Redemption Rate Data.");
            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PROMOTION_CODE_DATA,
                        SecurityPermission.ADD)) {
                // Ignore the user input; substitute no entry.
                sVO.setPromotionCode(null);
                sVO.setBonusPromotionCode(null);
            }

            // This indicates a new source record. Verify permissions.
//            if (!SecurityServiceBO.assertPermission(sessionId,
//                        SecurityResource.SOURCE_BILLING_DATA,
//                        SecurityPermission.ADD)) {
//                // Ignore the user input; substitute no entry.
//                sVO.setBillingInfoLogic(null);
//                sVO.setBillingInfoPrompt(null);
//            }

            if (!SecurityServiceBO.assertPermission(sessionId,
                        SecurityResource.SOURCE_PCARD_DATA,
                        SecurityPermission.ADD)) {
                // Ignore the user input; substitute no entry.
                sVO.getCpcCollection().clear();
            }
        }

        // record the new source type code.
        return SourceDAO.persistCompositeSource(sourceRequestVO);
    }

    public static Document getSources(SourceRequestVO sourceRequestVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy) throws FtdDataException, Exception {
        return SourceDAO.querySource(sourceRequestVO, recordNumberStart,
            maxRecordCount, resultRecordCount, sortBy);
    }

    public static Document getSourcesByUser(
        SourceRequestVO sourceRequestVO, int recordNumberStart,
        int maxRecordCount, List resultRecordCount, String sortBy)
        throws FtdDataException, Exception {
        return SourceDAO.querySourceByUser(sourceRequestVO, recordNumberStart,
            maxRecordCount, resultRecordCount, sortBy);
    }

    public static Document getSource(String sourceCode, String userSessionId)
        throws FtdDataException, Exception {
    	Document matchedSourceXml = SourceDAO.querySource(new SourceRequestVO(
                    new SourceVO(sourceCode), userSessionId), 1, 1,
                new ArrayList());

        // If no match is found, check to see if the source code has
        // been reserved.
        if (DOMUtil.selectSingleNode(matchedSourceXml, _XPATH_SOURCE_NODE) == null) {
            matchedSourceXml = SourceServiceBO.getReservedSourceCode(sourceCode);
        }

        return matchedSourceXml;
    }

    public static Document getSourcePrograms(String sourceCode)
        throws FtdDataException, Exception {
    	Document rsXML = SourceDAO.querySourcePrograms(sourceCode);

        // Append the derived expiration dates to the returned data.
        NodeList rs = rsXML.getFirstChild().getChildNodes();

        // Only need to add expiration dates if more than one record.
        if (rs.getLength() > 1) {
            for (int index = 0; index < rs.getLength(); index++) {
                // Get a reference to the current record.
                Node currentNode = rs.item(index);

                // Set the expiration date of the current record to the start
                // date of the next record.
                // Note that this accounts for zero-hour expiration and start.
                if ((index + 1) < rs.getLength()) {
                    Node nextNode = rs.item(index + 1);

                    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

                    Date date = df.parse(nextNode.getAttributes()
                                                 .getNamedItem("start_date")
                                                 .getNodeValue());
                    Date yesterday = new Date(date.getTime() -
                            (1000 * 60 * 60 * 24));

                    Node attr = rsXML.createAttribute("end_date");
                    attr.setNodeValue(sdf.format(yesterday));
                    currentNode.getAttributes().setNamedItem(attr);
                }
            }
        }

        return rsXML;
    }

    public static Document getSourceProgram(String id)
        throws FtdDataException, Exception {
        return SourceDAO.querySourceProgram(id);
    }

    public static SourceProgramRequestVO recordSourceProgram(
        SourceProgramRequestVO sourceProgramRequestVO)
        throws FtdDataException, Exception {
        // record the new source type code.
        return SourceDAO.persistSourceProgram(sourceProgramRequestVO);
    }

    public static Document getCpc(String sourceCode)
        throws FtdDataException, Exception {
        return SourceDAO.queryCpc(sourceCode);
    }

    public static String getSourcePartnerBank(String sourceCode)
        throws FtdDataException, Exception {
        String partnerBankId = null;
        CachedResultSet rows = SourceDAO.querySourcePartnerBanks(sourceCode);

        // We are only interested in a single bank record.
        // Maybe in the future, the GUI will support more than one bank
        // per partner.
        if (rows.next()) {
            partnerBankId = ((BigDecimal) rows.getObject(1)).setScale(0,
                    BigDecimal.ROUND_HALF_UP).toString();
        }

        return partnerBankId;
    }

    public static Document reserveSourceCode(SourceVO sourceVO)
        throws FtdDataException, Exception {
        return SourceServiceBO.reserveSourceCodes(sourceVO, null, null);
    }

    /**
    *
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.FtdDataException
    * @return An XMLDocument of conflict records. If there are child entries, no records were persisted.
    * @param sourceCodeEnd
    * @param sourceCodeStart
    * @param sourceVO
    */
    public static Document reserveSourceCodes(SourceVO sourceVO,
        String sourceCodeStart, String sourceCodeEnd)
        throws FtdDataException, Exception {
    	Document reservedSourceXml = null;

        // The DAOs support two use-cases:
        // 1. a single alphanumeric source code
        // 2. a range of numeric source codes
        // Test which one we need to use.
        if ((sourceCodeEnd == null) || (sourceCodeEnd.length() == 0)) {
            // This indicates interest in only a single source code.
            // First, verify that the source code is not already reserved.
            reservedSourceXml = SourceServiceBO.getReservedSourceCode(sourceVO.getId());

            if (reservedSourceXml.getFirstChild().getChildNodes().getLength() > 0) {
                // Return the conflict records.
                return reservedSourceXml;
            }

            // Proceed to update the existing master recordS.
            try {
                SourceDAO.updateSourceCode(sourceVO, RecordStatus.ACTIVE);
            } catch (FtdDataException e) {
                // Parse the error message to determine if no records existed to update.
                if (e.getMessage().indexOf(_ORACLE_NO_UPDATE_ERROR_MSG) != -1) {
                    // Return null, indicating that no matching records were updated.
                    return null;
                } else {
                    throw e;
                }
            }

            return reservedSourceXml;
        }

        // Verify that source code(s) within the numerical range are not
        // already reserved.
        reservedSourceXml = SourceServiceBO.getReservedSourceCodes(sourceCodeStart,
                sourceCodeEnd);

        if (reservedSourceXml.getFirstChild().getChildNodes().getLength() > 0) {
            // Return the conflict records.
            return reservedSourceXml;
        }

        // Proceed to insert/update the reservations.
        int sourceCodeStartNum;
        int sourceCodeEndNum;

        try {
            sourceCodeStartNum = Integer.parseInt(sourceCodeStart);
            sourceCodeEndNum = Integer.parseInt(sourceCodeEnd);
        } catch (NumberFormatException e) {
            throw new FtdDataException("reserveSourceCodes: sourceCodeStart(" +
                sourceCodeStart + ") and sourceCodeEnd(" + sourceCodeEnd +
                ") must be numeric values.");
        }

        try {
            SourceDAO.updateSourceCodes(sourceVO, RecordStatus.ACTIVE,
                sourceCodeStart, sourceCodeEnd);
        } catch (FtdDataException e) {
            // Parse the error message to determine if no records existed to update.
            if (e.getMessage().indexOf(_ORACLE_NO_UPDATE_ERROR_MSG) != -1) {
                // Return null, indicating that no matching records were updated.
                return null;
            } else {
                throw e;
            }
        }

        return reservedSourceXml;
    }

    public static Document releaseSourceCodes(SourceVO sourceVO)
        throws FtdDataException, Exception {
        return SourceServiceBO.releaseSourceCodes(sourceVO, null, null);
    }

    public static Document releaseSourceCodes(SourceVO sourceVO,
        String sourceCodeStart, String sourceCodeEnd)
        throws FtdDataException, Exception {
    	Document createdSourceXml = null;

        // The DAOs support two use-cases:
        // 1. a single alphanumeric source code
        // 2. a range of numeric source codes
        // Test which one we need to use.
        if ((sourceCodeEnd == null) || (sourceCodeEnd.length() == 0)) {
            // This indicates interest in only a single source code.
            // First, verify that the source code is not already created.
            createdSourceXml = SourceDAO.queryCreatedSourceCode(sourceVO.getId());

            if (createdSourceXml.getFirstChild().getChildNodes().getLength() > 0) {
                // Return the conflict records.
                return createdSourceXml;
            }

            // Proceed to update the existing master recordS.
            try {
                SourceDAO.updateSourceCode(sourceVO, RecordStatus.INACTIVE);
            } catch (FtdDataException e) {
                // Parse the error message to determine if no records existed to update.
                if (e.getMessage().indexOf(_ORACLE_NO_UPDATE_ERROR_MSG) != -1) {
                    // Return null, indicating that no matching records were updated.
                    return null;
                } else {
                    throw e;
                }
            }

            return createdSourceXml;
        }

        try {
            Integer.parseInt(sourceCodeStart);
            Integer.parseInt(sourceCodeEnd);
        } catch (NumberFormatException e) {
            throw new FtdDataException("releaseSourceCodes: sourceCodeStart(" +
                sourceCodeStart + ") and sourceCodeEnd(" + sourceCodeEnd +
                ") must be numeric values.");
        }

        // Verify that source code(s) within the numerical range are not
        // already reserved.
        createdSourceXml = SourceDAO.queryCreatedSourceCodes(sourceCodeStart,
                sourceCodeEnd);

        if (createdSourceXml.getFirstChild().getChildNodes().getLength() > 0) {
            // Return the conflict records.
            return createdSourceXml;
        }

        // Proceed to insert/update the releases.
        try {
            SourceDAO.updateSourceCodes(sourceVO, RecordStatus.INACTIVE,
                sourceCodeStart, sourceCodeEnd);
        } catch (FtdDataException e) {
            // Parse the error message to determine if no records existed to update.
            if (e.getMessage().indexOf(_ORACLE_NO_UPDATE_ERROR_MSG) != -1) {
                // Return null, indicating that no matching records were updated.
                return null;
            } else {
                throw e;
            }
        }

        return createdSourceXml;
    }

    public static CachedResultSet getAllReservedSourceCodes()
        throws FtdDataException, Exception {
        // get list of source type codes
        return SourceDAO.queryAllReservedSourceCodes();
    }

    //    public static boolean isNumericSourceCodeReserved(String sourceCode)
    //        throws FtdDataException, NumberFormatException, Exception {
    //        return SourceServiceBO.areNumericSourceCodesReserved(sourceCode,
    //            sourceCode);
    //    }
    //
    //    public static boolean areNumericSourceCodesReserved(
    //        String sourceCodeStart, String sourceCodeEnd)
    //        throws FtdDataException, NumberFormatException, Exception {
    //        // Only numeric source codes can be reserved.
    //        int sourceCodeStartNum = Integer.parseInt(sourceCodeStart);
    //        int sourceCodeEndNum = Integer.parseInt(sourceCodeEnd);
    //
    //        // Proceed to verify if the numeric source code has a reserved
    //        // record.
    //        XMLDocument reservedSourceXml = SourceServiceBO.getReservedSourceCodes(sourceCodeStartNum,
    //                sourceCodeEndNum);
    //
    //        if (reservedSourceXml.getFirstChild().getChildNodes().getLength() > 0) {
    //            // A reserved record does exist for the source code.
    //            return true;
    //        }
    //
    //        return false;
    //    }
    public static Document getReservedSourceCode(String sourceCode)
        throws FtdDataException, Exception {
        return SourceServiceBO.getReservedSourceCodes(sourceCode, null);
    }

    public static Document getReservedSourceCodes(String sourceCodeStart,
        String sourceCodeEnd) throws FtdDataException, Exception {
        // The DAOs support two use-cases:
        // 1. a single alphanumeric source code
        // 2. a range of numeric source codes
        // Test which one we need to use.
        if ((sourceCodeEnd == null) || (sourceCodeEnd.length() == 0)) {
            // This indicates interest in only a single source code.
            return SourceDAO.queryReservedSourceCode(sourceCodeStart);
        }

        // Otherwise, we are interested in a range of numeric source codes.
        try {
            Integer.parseInt(sourceCodeStart);
            Integer.parseInt(sourceCodeEnd);
        } catch (NumberFormatException e) {
            throw new FtdDataException(
                "getReservedSourceCodes: sourceCodeStart(" + sourceCodeStart +
                ") and sourceCodeEnd(" + sourceCodeEnd +
                ") must be numeric values.");
        }

        return SourceDAO.queryReservedSourceCodes(sourceCodeStart, sourceCodeEnd);
    }

    public static int countSourceCodes(String sourceCodeStart,
        String sourceCodeEnd) {
        if ((sourceCodeEnd == null) || (sourceCodeEnd.length() == 0)) {
            // This indicates interest in only a single source code.
            return 1;
        }

        int sourceCodeStartNum;
        int sourceCodeEndNum;

        try {
            sourceCodeStartNum = Integer.parseInt(sourceCodeStart);
            sourceCodeEndNum = Integer.parseInt(sourceCodeEnd);
        } catch (NumberFormatException e) {
            return 1;
        }

        return sourceCodeEndNum - sourceCodeStartNum + 1;
    }


    public static CachedResultSet getFloristDetails(String floristId)
        throws FtdDataException, Exception {
        return SourceDAO.getFloristDetails(floristId);
    }
    
    
   /**
   * Persists the Default Order information for a Source Code
   * @param sourceDefaultOrderInfoVO
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws FtdDataException
   * @throws Exception
   */    
    public static void persistSourceDefaultOrderInfo(SourceDefaultOrderInfoVO sourceDefaultOrderInfoVO)
        throws FtdDataException, Exception {
        SourceDAO.persistSourceDefaultOrderInfo(sourceDefaultOrderInfoVO);
    }
    
   /**
   * Retrieves the Source Order Info given the source code
   * @param sourceCode
   * @return
   * @throws FtdDataException
   * @throws Exception
   */
    public static SourceDefaultOrderInfoVO getSourceDefaultOrderInfo(String sourceCode)
        throws FtdDataException, Exception {
        return SourceDAO.getSourceDefaultOrderInfo(sourceCode);
    }
    
   /** Returns the default source code detail referring to various tables.
	 * @param sourceCode
	 * @param orderSource
	 * @return
	 * @throws FtdDataException
	 * @throws Exception
	 */
	public static SourceVO getSourceCodeDetail(String sourceCode, String orderSource) throws FtdDataException, Exception {
		return SourceDAO.getSourceCodeDetail(sourceCode, orderSource);
	}

	/** Returns the next number that can be used as sympathy source code depending on the partner.
	 * @param partner
	 * @return
	 * @throws FtdDataException
	 * @throws Exception
	 */
	public static int generateInternetSourceCode(String partner) throws FtdDataException, Exception {
		return SourceDAO.generateInternetSourceCode(partner);
	}

	/** Record or inserts source code detail into various tables.
	 * @param sourceRequestVO
	 * @param sourceProgramRequestVO
	 * @param programId
	 * @throws FtdDataException
	 * @throws FtdSecurityException
	 * @throws Exception
	 */
	public static String recordSource(SourceRequestVO sourceRequestVO, SourceProgramRequestVO sourceProgramRequestVO, String programId)
			throws FtdDataException, FtdSecurityException, Exception {
		SourceDAO.persistCompositeSource(sourceRequestVO, sourceProgramRequestVO, programId);
		if (!StringUtils.isEmpty(programId)) {
			try {
				SourceDAO.updateDefaultSourceEmailProgram(programId, sourceRequestVO.getSourceVO().getId());
			} catch(Exception e) {
				return "Error caught: " + e.getMessage();
			}
		}
		return null;
	}
	
	
	/** Updates source code primary and secondary florists.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void updateSCFlorists(SourceVO sourceVO) throws Exception {
		SourceDAO.updateSCFlorists(sourceVO);
	}
	
	/** Adds source code primary and secondary florists.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void addSCFlorists(SourceVO sourceVO) throws Exception {
		SourceDAO.addSCFlorists(sourceVO);
	}
	
	/** Deletes source code primary and secondary florists.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void deleteSCFlorists(SourceVO sourceVO) throws Exception {
		SourceDAO.deleteSCFlorists(sourceVO);
	}
	
	/** Retrieves primary and secondary florists information for a given source code.
	 * @param sourceCode
	 * @return
	 * @throws Exception
	 */
	public static List<Florist> getSCFlorists(String sourceCode) throws Exception {
		return SourceDAO.getSCFlorists(sourceCode);
	}
	
	public static Document getApeBaseSourceCodes(String requestSourceCode) throws FtdDataException, Exception {
		ArrayList baseSourceCodesList = SourceDAO.getApeBaseSourceCodes(requestSourceCode);
		Collections.sort(baseSourceCodesList);
		StringBuffer xmlString = new StringBuffer("<apeSourceCodes><base_source_code ");
		xmlString.append("value=\"");
		for( int i=0; i<baseSourceCodesList.size(); i++){
			if(i>0){
				xmlString.append(" ");
			}
			xmlString.append(baseSourceCodesList.get(i));
		}
		xmlString.append("\" /></apeSourceCodes>");
		DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(xmlString.toString())));

		return document;
	}
	
	public static HashMap updateApeBaseSourceCodes(String requestSourceCode,
			String apeBaseSourceCodes, String updatedBy) throws FtdDataException, Exception {
		//get base source codes from database
		ArrayList<String> baseSourceCodesList = SourceDAO.getApeBaseSourceCodes(requestSourceCode);
		ArrayList<String> inbaseSourceCodesList = new ArrayList<String>();
		ArrayList<String> invalidSourceCodesList = new ArrayList<String>();
		ArrayList<String> iotwSourceCodesList = new ArrayList<String>();
		ArrayList<String> apeEnabledSourceCodesList = new ArrayList<String>();
		ArrayList<String> milesPointsSourceCodesList = new ArrayList<String>();
		
		HashMap<String, ArrayList<String>> resultMap = new HashMap<String, ArrayList<String>>();
		String deleteSourceCodesList = "";
		String insertSourceCodesList = "";
		
		if(baseSourceCodesList.size()>0 || (apeBaseSourceCodes!=null || !StringUtils.isEmpty(apeBaseSourceCodes))){
				if(apeBaseSourceCodes!=null || !StringUtils.isEmpty(apeBaseSourceCodes))
				{
					StringTokenizer st = new StringTokenizer(apeBaseSourceCodes);
					while (st.hasMoreElements()) {
						String inSC = (String) st.nextElement();
						
						//should be valid i.e. should exist in source_master
						if(SourceDAO.isInvalidSourceCode(inSC)){
							invalidSourceCodesList.add(inSC);
						}
						//should not have APE enabled
						if(SourceDAO.isApeEnabledSourceCode(inSC)){
							apeEnabledSourceCodesList.add(inSC);
						}
						//should not be IOTW
						if(SourceDAO.isIOTWSourceCode(inSC)){
							iotwSourceCodesList.add(inSC);
						}
						//should not have miles/points promotions
						if(SourceDAO.milesPointsAssigned(inSC)){
							milesPointsSourceCodesList.add(inSC);
						}
						else{
							inbaseSourceCodesList.add(inSC);
						}
					}
				}
		}
		
		for(int i=0; i<inbaseSourceCodesList.size(); i++){
			String inputSourceCode = (String) inbaseSourceCodesList.get(i);
			if(baseSourceCodesList.contains(inputSourceCode)){
				baseSourceCodesList.remove(inputSourceCode);
				inbaseSourceCodesList.remove(inputSourceCode);
				i = 0;
			}
		}
		
		if(invalidSourceCodesList.size()>0){
			resultMap.put("INVALID", invalidSourceCodesList);
		}
		if(apeEnabledSourceCodesList.size()>0){
			resultMap.put("APEN", apeEnabledSourceCodesList);
		}
		if(iotwSourceCodesList.size()>0){
			resultMap.put("IOTW", iotwSourceCodesList);
		}
		if(milesPointsSourceCodesList.size()>0){
			resultMap.put("MILES", milesPointsSourceCodesList);
		}
		
		if(!(resultMap.size()>0)){
			//delete source codes remaining in baseSourceCodesList
			if(baseSourceCodesList.size()>0){
				for( int i=0; i<baseSourceCodesList.size(); i++){
					SourceDAO.deleteApeBaseSourceCodes(requestSourceCode, baseSourceCodesList.get(i), updatedBy);
				}
			}
			//insert source codes remaining in inbaseSourceCodesList
			if(inbaseSourceCodesList.size()>0){
				for( int i=0; i<inbaseSourceCodesList.size(); i++){
					SourceDAO.insertApeBaseSourceCodes(requestSourceCode, inbaseSourceCodesList.get(i), updatedBy);
				}
			}
		}
		return resultMap;
	}

	public static HashMap checkBaseSourceCode(String requestSourceCode) throws FtdDataException, Exception{
		 HashMap<String, ArrayList<String>> resultMap = new HashMap<String, ArrayList<String>>();
		 ArrayList<String> baseSourceCodesList = SourceDAO.checkBaseSourceCode(requestSourceCode);
		 if(baseSourceCodesList.size() > 0){
			 //set AUTO_PROMOTION_ENGINE to 'N' in source_master
			 SourceDAO.setAutoPromotionEngineFlag(requestSourceCode);
			 resultMap.put("BASE", baseSourceCodesList);
		 }
		 return resultMap;
	}

	public static void deleteSCdependencies(SourceCodeRequest request) throws FtdDataException, Exception {
		SourceDAO.deleteSCDependencies(request);
		
	}

	public static void updateSCAttributes(
			UpdateSourceCodeAttributesRequest request) throws FtdDataException, Exception{
		SourceDAO.updateSCAttributes(request);
	}
}
