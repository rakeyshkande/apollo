package com.ftd.marketing.bo.test;

import com.ftd.enterprise.dao.UtilDAO;

import com.ftd.marketing.bo.SecurityServiceBO;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class SecurityServiceBOTest extends TestCase {
    protected Log logger = LogFactory.getLog(SecurityServiceBOTest.class.getName());

    public SecurityServiceBOTest(String sTestName) {
        super(sTestName);
    }

    /**
     * XMLDocument getUsers(String)
     */
    public void testgetUsers() {
        try {
            UtilDAO.printXml(SecurityServiceBO.getUsers("MKT"));
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
