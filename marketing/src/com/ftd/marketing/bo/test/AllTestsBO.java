package com.ftd.marketing.bo.test;

import com.ftd.marketing.bo.test.CustomerServiceBOTest;
import com.ftd.marketing.bo.test.ProductServiceBOTest;
import com.ftd.marketing.bo.test.ReferenceServiceBOTest;
import com.ftd.marketing.bo.test.SourceServiceBOTest;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTestsBO {
    public static Test suite() {
        TestSuite suite;
        suite = new TestSuite(AllTestsBO.class.getName());

        suite.addTestSuite(com.ftd.marketing.bo.test.CustomerServiceBOTest.class);
        suite.addTestSuite(com.ftd.marketing.bo.test.SourceServiceBOTest.class);
        suite.addTestSuite(com.ftd.marketing.bo.test.ReferenceServiceBOTest.class);
        suite.addTestSuite(com.ftd.marketing.bo.test.ProductServiceBOTest.class);
        suite.addTestSuite(com.ftd.marketing.bo.test.SecurityServiceBOTest.class);

        return suite;
    }

    public static void main(String[] args) {
        String[] args2 = {
                "-noloading", "com.ftd.marketing.bo.test.AllTestsBO"
            };

        junit.swingui.TestRunner.main(args2);
    }
}
