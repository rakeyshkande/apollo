package com.ftd.marketing.bo.test;

import com.ftd.enterprise.vo.PhoneType;
import com.ftd.enterprise.vo.PhoneVO;

import com.ftd.marketing.bo.CustomerServiceBO;
import com.ftd.marketing.test.CompanyFixture;
import com.ftd.marketing.test.CustomerFixture;
import com.ftd.marketing.test.UserFixture;
import com.ftd.marketing.vo.CatalogRequestVO;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Iterator;


public class CustomerServiceBOTest extends TestCase {
    protected Log logger = LogFactory.getLog(CustomerServiceBOTest.class.getName());
    UserFixture userFixture = new UserFixture();
    CustomerFixture customerFixture = new CustomerFixture();
    CompanyFixture companyFixture = new CompanyFixture();

    public CustomerServiceBOTest(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception {
        companyFixture.setUp();
        customerFixture.setUp();
        userFixture.setUp();
    }

    public void tearDown() throws Exception {
        companyFixture.tearDown();
        customerFixture.tearDown();
        userFixture.tearDown();
    }

    /**
     * CatalogRequestVO insertCatalogSubscription(CatalogRequestVO)
     */
    public void testinsertCatalogSubscription() {
        try {
            // Build the value object havinag a customer with email.
            CatalogRequestVO catalogRequestVO = new CatalogRequestVO(customerFixture.getCustomerVOWithEmail(),
                    companyFixture.getCompanyVO(),
                    userFixture.getUserVO().getUserID());

            // Submit the value for persistence.
            catalogRequestVO = CustomerServiceBO.subscribeCustomer(catalogRequestVO);

            logger.debug("CustomerID=" +
                catalogRequestVO.getCustomerVO().getId());
            assertNotNull(catalogRequestVO.getCustomerVO().getId());
            logger.debug("EmailID=" +
                catalogRequestVO.getCustomerVO().getEmailVO().getId());
            assertNotNull(catalogRequestVO.getCustomerVO().getEmailVO().getId());

            // Iterate through all customer phones, checking for the id.      
            for (Iterator i = catalogRequestVO.getCustomerVO()
                                              .getAllCustomerPhones().keySet()
                                              .iterator(); i.hasNext();) {
                PhoneType phoneType = (PhoneType) i.next();
                PhoneVO phoneVO = catalogRequestVO.getCustomerVO().getPhone(phoneType);
                logger.debug("PhoneID=" + phoneVO.getId());
                assertNotNull(phoneVO.getId());
            }
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
