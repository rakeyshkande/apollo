package com.ftd.marketing.bo.test;

import com.ftd.enterprise.dao.GlobalDAO;
import com.ftd.enterprise.dao.UtilDAO;

import com.ftd.marketing.bo.ReferenceServiceBO;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;


public class ReferenceServiceBOTest extends TestCase {
    protected Log logger = LogFactory.getLog(ReferenceServiceBOTest.class.getName());

    public ReferenceServiceBOTest(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception {
    }

    public void tearDown() throws Exception {
    }

    /**
     * XMLDocument getAllCodeList()
     */
    public void testgetAllCodeList() {
        try {
            UtilDAO.printXml(ReferenceServiceBO.getAllStates());
            UtilDAO.printXml(ReferenceServiceBO.getAllCountries());
            UtilDAO.printXml(ReferenceServiceBO.getAllPaymentMethods());
            UtilDAO.printXml(ReferenceServiceBO.getAllPricingCodes());
            UtilDAO.printXml(ReferenceServiceBO.getAllPartners());
            UtilDAO.printXml(ReferenceServiceBO.getAllCompanies());
            UtilDAO.printXml(ReferenceServiceBO.getAllShipMethods());
            UtilDAO.printXml(ReferenceServiceBO.getAllMileageCalcSources());
            UtilDAO.printXml(ReferenceServiceBO.getAllOrderSources());
            UtilDAO.printXml(ReferenceServiceBO.getAllBonusTypes());
            UtilDAO.printXml(ReferenceServiceBO.getAllDiscountTypes());
            UtilDAO.printXml(ReferenceServiceBO.getAllServiceFeeCodes());
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
