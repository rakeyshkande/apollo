package com.ftd.marketing.bo.test;

import com.ftd.marketing.bo.SourceServiceBO;
import com.ftd.marketing.test.SourceTypeFixture;
import com.ftd.marketing.test.UserFixture;
import com.ftd.marketing.vo.SourceTypeRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.w3c.dom.Document;

import junit.framework.TestCase;


public class SourceServiceBOTest extends TestCase {
    UserFixture userFixture = new UserFixture();
    SourceTypeFixture sourceTypeFixture = new SourceTypeFixture();

    public SourceServiceBOTest(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception {
        userFixture.setUp();
        sourceTypeFixture.setUp();
    }

    public void tearDown() throws Exception {
        userFixture.tearDown();
        sourceTypeFixture.tearDown();
    }

    /**
     * Document getAllSourceTypes()
     */
    public void testgetAllSourceTypes() {
        try {
            Document xmlDocument = SourceServiceBO.getAllSourceTypes();
            StringWriter sw = new StringWriter(); //string representation of xml document
            DOMUtil.print(xmlDocument, new PrintWriter(sw));
            System.out.println("testgetAllSourceTypes: XML Source Type List:\n" +
                sw.toString());
            sw.flush();
            sw.close();
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    /**
     * SourceTypeRequestVO insertSourceType(SourceTypeRequestVO)
     */
    public void testinsertSourceType() {
        // Build the value object.
        SourceTypeRequestVO sourceTypeRequestVO = new SourceTypeRequestVO(sourceTypeFixture.getSourceTypeVO(),
                userFixture.getUserVO());

        try {
            // Submit the value for persistence.
            SourceTypeRequestVO resultRequestVO = SourceServiceBO.recordSourceType(sourceTypeRequestVO);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
