package com.ftd.marketing.bo.test;

import com.ftd.enterprise.dao.PricingDAO;
import com.ftd.enterprise.dao.UtilDAO;
import com.ftd.enterprise.vo.PricingCodeVO;

import com.ftd.marketing.bo.ProductServiceBO;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.test.PricingCodeFixture;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;


public class ProductServiceBOTest extends TestCase {
    protected static final String PRICE_CODE_1 = "M1";
    protected static final String PRICE_CODE_2 = "X9";
    protected Log logger = LogFactory.getLog(ProductServiceBOTest.class.getName());
    protected PricingCodeFixture pricingCodeFixture = new PricingCodeFixture();

    public ProductServiceBOTest(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception {
        pricingCodeFixture.setUp();
    }

    public void tearDown() throws Exception {
        pricingCodeFixture.tearDown();
    }

    /**
     * PricingCodeVO insertPricingCode(PricingCodeVO)
     */
    public void testinsertPricingCode() {
        // Dollar use case.
        PricingCodeVO pricingCodeVOPercent = pricingCodeFixture.getPricingCodeVO(PRICE_CODE_2,
                "P");

        try {
            ProductServiceBO.recordPricingCode(pricingCodeVOPercent);
        } catch (Throwable e) {
            logger.error("testinsertPricingCode: Failed. ", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * XMLDocument getPricingCodeDetails(String)
     */
    public void testgetPricingCodeDetails() {
        try {
            UtilDAO.printXml(PricingDAO.queryPricingCodeDetails(PRICE_CODE_1));
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
