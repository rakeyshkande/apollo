package com.ftd.marketing.bo;

import com.ftd.enterprise.dao.CustomerDAO;
import com.ftd.enterprise.vo.EmailVO;
import com.ftd.enterprise.vo.SubscribeStatus;

import com.ftd.marketing.exception.EntityLockedException;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.CatalogRequestVO;

import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJB;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBHome;
import com.ftd.newsletterevents.vo.StatusType;

import com.ftd.security.cache.vo.UserInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.rmi.PortableRemoteObject;


/**
 * Services for subscribing customers to the catalog and newsletter.
 */
public class CustomerServiceBO {
    protected static Log logger = LogFactory.getLog(CustomerServiceBO.class.getName());
    private static final String _ORACLE_NO_UPDATE_ERROR_MSG = "No email records were updated";

    /**
     * Constructor to prevent instantiation of this class.
     */
    private CustomerServiceBO() {
        super();
    }

    protected static boolean isStringEmpty(String target) {
        return ((target == null) || (target.length() == 0)) ? true : false;
    }

    /**
    * Subscribe customers to the catalog and newsletter.
    *
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.DataException
    * @return a complete CatalogRequestVO object
    * @param catalogRequestVO - customer contact metrics
    */
    public static CatalogRequestVO subscribeCustomer(
        CatalogRequestVO catalogRequestVO) throws FtdDataException, Exception {
        // Process the subscribe request.
        // Persist to the database.
        CatalogRequestVO crVO = CustomerDAO.insertCatalogSubscription(catalogRequestVO);

        // Next, publish any newsletter subscription as an event.
        if ((catalogRequestVO.getCompanyVO() != null) &&
                (catalogRequestVO.getCustomerVO() != null) &&
                (catalogRequestVO.getCustomerVO().getEmailVO() != null) &&
                !isStringEmpty(catalogRequestVO.getCustomerVO().getEmailVO()
                                                   .getEmailAddress())) {
            Context jndiContext = new InitialContext();

            try {
                NewsletterPubEJBHome home = (NewsletterPubEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                            "NewsletterPubEJB"), NewsletterPubEJBHome.class);

                // create an EJB instance 
                NewsletterPubEJB npEjb = home.create();
                npEjb.process(catalogRequestVO.getCustomerVO().getEmailVO()
                                              .getEmailAddress(),
                    catalogRequestVO.getCompanyVO().getId(),
                    StatusType.SUBSCRIBED,
                    catalogRequestVO.getUserVO().getUserID(), new Date());
            } finally {
                jndiContext.close();
            }
        }

        return crVO;
    }

    public static Document getSubscriptionHistory(String customerId,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy) throws FtdDataException, Exception {
        return CustomerDAO.querySubscriptionHistory(customerId,
            recordNumberStart, maxRecordCount, resultRecordCount, sortBy);
    }

    public static void lockCustomerRecord(String customerId, String sessionId)
        throws EntityLockedException, Exception {
        if ((customerId == null) || (sessionId == null)) {
            throw new Exception(
                "lockCustomerRecord: Input values customerId and sessionId can not be null.");
        }

        /* lock customer record */
        UserInfo user = SecurityServiceBO.getUserInfo(sessionId);

        if (user != null) {
            CustomerDAO.insertCustomerRecordLock(customerId, sessionId, user);
        } else {
            throw new Exception(
                "lockCustomerRecord: Session ID does not correspond with a valid user: " +
                sessionId);
        }
    }

    /**
    * Returns the customer IDs associated with an email address.
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.FtdDataException
    * @return XMLDocument of the matched records; empty XMLDocument if no match found.
    * @param emailAddress
    */
    public static Document getCustomerEmail(String emailAddress)
        throws FtdDataException, Exception {
        return CustomerDAO.queryCustomerEmail(emailAddress);
    }

    /**
    * Returns the metrics associated with an email address.
    * Note: no special data verification is performed within this method:
    * the specified email address data is simply returned.
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.FtdDataException
    * @return XMLDocument of the matched record; empty XMLDocument if no match found.
    * @param emailAddress
    */
    public static Document getEmail(String emailAddress)
        throws FtdDataException, Exception {
        return CustomerDAO.queryEmail(emailAddress);
    }

    /**
    * Performs a strict update of the metrics associated with an unattached email address.
    * Workflow verifies:
    * (1) that no customer IDs are attached to the email address; and
    * (2) that the email address already exists in database for update.
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.FtdDataException
    * @return XMLDocument of the conflict customer-attached IDs; null if email address does not exist; empty XMLDocuemnt if update successful.
    * @param emailVO
    */
    public static Document updateUnattachedEmail(EmailVO emailVO)
        throws FtdDataException, Exception {
        // First, verify that the specified email address is, in fact, unattached
        // to a customer.
    	Document customerEmail = getCustomerEmail(emailVO.getEmailAddress());

        if (customerEmail.getFirstChild().getChildNodes().getLength() > 0) {
            // Return the conflict records.
            return customerEmail;
        }

        // Proceed to update the email records by publishing the event.

        
        try {
            CustomerDAO.updateEmail(emailVO);
        } catch (FtdDataException e) {
            // Parse the error message to determine if no records existed to update.
            if (e.getMessage().indexOf(_ORACLE_NO_UPDATE_ERROR_MSG) != -1) {
                // Return null, indicating that no matching records were updated.
                return null;
            } else {
                throw e;
            }
        }
        
        Context jndiContext = new InitialContext();

        try {
            NewsletterPubEJBHome home = (NewsletterPubEJBHome) PortableRemoteObject.narrow(jndiContext.lookup(
                        "NewsletterPubEJB"), NewsletterPubEJBHome.class);

            // create an EJB instance 
            NewsletterPubEJB npEjb = home.create();
            npEjb.process(emailVO.getEmailAddress(), emailVO.getCompanyId(),
                emailVO.getSubscribeStatus().equals(SubscribeStatus.SUBSCRIBE)
                ? StatusType.SUBSCRIBED : StatusType.UNSUBSCRIBED,
                emailVO.getUpdatedBy(), new Date());
        } finally {
            jndiContext.close();
        }

        return customerEmail;
    }
}
