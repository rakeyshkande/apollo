package com.ftd.marketing.bo;

import com.ftd.enterprise.dao.GlobalDAO;
import com.ftd.enterprise.dao.PartnerProgramDAO;
import com.ftd.enterprise.vo.AaaClubVO;
import com.ftd.enterprise.vo.CostCenterVO;

import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.ProgramRewardRequestVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;


public class PartnerProgramServiceBO {
    protected static Log logger = LogFactory.getLog(PartnerProgramServiceBO.class.getName());
//    private static final String _XPATH_PROGRAM_REWARD_BONUS_NODE = "/programRewardBonusList/programRewardBonus";

    /**
     * Constructor to prevent instantiation of this class.
     */
    private PartnerProgramServiceBO() {
        super();
    }

    /*
        public static XMLDocument getAllSourceTypes()
            throws FtdDataException, Exception {
            // get list of source type codes
            return SourceDAO.getAllSourceTypes();
        }

        public static CachedResultSet getAllGroupedSourceTypes()
            throws FtdDataException, Exception {
            // get list of source type codes
            return SourceDAO.getAllGroupedSourceTypes();
        }
    */
    public static ProgramRewardRequestVO recordRewardProgram(
        ProgramRewardRequestVO programRewardRequestVO)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.persistCompositeRewardProgram(programRewardRequestVO);
    }

    /*    public static ProgramRewardBonusRequestVO recordRewardProgramBonus(
            ProgramRewardBonusRequestVO programRewardBonusRequestVO)
            throws FtdDataException, FtdSecurityException, Exception {
            String sessionId = programRewardBonusRequestVO.getUserSessionId();

            // Read the existing Bonus record from the database b/c
            // if the user is restricted from editing certain fields,
            // we will have to provide default values to the DAO for persistence.
            ProgramRewardBonusVO prbVO = programRewardBonusRequestVO.getProgramRewardBonusVO();
            XMLDocument existingBonusDoc = PartnerProgramServiceBO.getRewardProgramBonus(programRewardBonusRequestVO);

            if (existingBonusDoc.getFirstChild().getChildNodes().getLength() > 0) {
                NamedNodeMap attributeMap = existingBonusDoc.selectSingleNode(_XPATH_PROGRAM_REWARD_BONUS_NODE)
                                                            .getAttributes();

                // This means this operation is an update.
                if (!SecurityServiceBO.assertPermission(sessionId,
                            SecurityResource.PROGRAM_REWARD_BONUS_DATA,
                            SecurityPermission.UPDATE)) {
                    throw new FtdSecurityException(
                        "recordRewardProgramBonus: You do not have security permission to UPDATE Program Bonus Data.");

                    // Substitute the metrics from the existing record.
                    //                prbVO.setId(attributeMap.getNamedItem("program_name")
                    //                                        .getNodeValue());
                    //                prbVO.setCalculationBasis(attributeMap.getNamedItem(
                    //                        "calculation_basis").getNodeValue());
                    //                prbVO.setPoints(new Float(attributeMap.getNamedItem("points")
                    //                                                      .getNodeValue()));
                    //                prbVO.setSeparateData(attributeMap.getNamedItem("separate_data")
                    //                                                  .getNodeValue());
                    //
                    //                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
                    //                String endDate = attributeMap.getNamedItem("end_date")
                    //                                             .getNodeValue();
                    //
                    //                if ((endDate != null) & (endDate.length() > 0)) {
                    //                    prbVO.setEndDate(df.parse(endDate));
                    //                }
                    //
                    //                String startDate = attributeMap.getNamedItem("start_date")
                    //                                               .getNodeValue();
                    //
                    //                if ((startDate != null) & (startDate.length() > 0)) {
                    //                    prbVO.setStartDate(df.parse(startDate));
                    //                }
                }
            } else {
                // This indicates a new record.  Verify permissions.
                if (!SecurityServiceBO.assertPermission(sessionId,
                            SecurityResource.PROGRAM_REWARD_BONUS_DATA,
                            SecurityPermission.ADD)) {
                    throw new FtdSecurityException(
                        "recordRewardProgramBonus: You do not have security permission to CREATE Program Bonus Data.");
                }
            }

            return PartnerProgramDAO.persistProgramRewardBonus(programRewardBonusRequestVO);
        }
    */
    public static Document getAllCalculationBases()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_CALCULATION_BASIS);
    }

    public static Document getAllRewardTypes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_REWARD_TYPE);
    }

    public static Document getAllProgramTypes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PROGRAM_TYPE);
    }

    public static Document getAllPartners()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PARTNER);
    }

    public static Document getAllPartnerPrograms()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PARTNER_PROGRAM);
    }

    public static Document getAllAaaClubs()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_AAA_MEMBERS);
    }

    public static Document getRewardProgram(
        ProgramRewardRequestVO programRewardRequestVO)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.queryProgramReward(programRewardRequestVO);
    }

    public static Document getRewardProgramRange(
        ProgramRewardRequestVO programRewardRequestVO)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.queryProgramRewardRange(programRewardRequestVO);
    }

    /*    public static XMLDocument getRewardProgramBonus(
            ProgramRewardBonusRequestVO programRewardBonusRequestVO)
            throws FtdDataException, Exception {
            return PartnerProgramDAO.queryProgramRewardBonus(programRewardBonusRequestVO);
        }
    */
    public static AaaClubVO recordAaaClub(AaaClubVO aaaClubVO)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.persistAaaClub(aaaClubVO);
    }

    public static Document getAaaClub(String clubCode)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.queryAaaClub(clubCode);
    }
    
    public static Document getPartnerPrograms(String partnerName)
        throws FtdDataException, Exception {
        return PartnerProgramDAO.queryPartnerPrograms(partnerName);
    }

    public static CostCenterVO recordCostCenter(
        CostCenterVO costCenterVO) throws FtdDataException, Exception {
        // First, verify that the record does not already exist.
    	Document rsXML = PartnerProgramDAO.queryCostCenter(costCenterVO);

        if (rsXML.getFirstChild().getChildNodes().getLength() > 0) {
            throw new FtdDataException("recordCostCenter: Cost center record already exists.",
                FtdDataException.ERROR_CODE_UNIQUENESS_VIOLATION);
        }

        return PartnerProgramDAO.persistCostCenter(costCenterVO);
    }
}
