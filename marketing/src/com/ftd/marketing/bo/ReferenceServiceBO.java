package com.ftd.marketing.bo;

import com.ftd.enterprise.dao.GlobalDAO;
import com.ftd.marketing.exception.FtdDataException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;


public class ReferenceServiceBO {
    protected static Log logger = LogFactory.getLog(ReferenceServiceBO.class.getName());

    private ReferenceServiceBO() {
    }

    public static Document getAllStates() throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_STATE);
    }

    public static Document getAllCountries()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_COUNTRY);
    }

    public static Document getAllPaymentMethods()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PAYMENT_METHOD);

        // Manually add the Corporate Purchase Card payment type.
        //<payMethod num="11" payment_method_id="VI" description="Visa" payment_type="C" has_expiration_date="Y"/>
//        Element pcRow = doc.createElement(doc.getFirstChild().getFirstChild()
//                                             .getNodeName());
//        pcRow.setAttribute("num", "9999");
//        pcRow.setAttribute("payment_method_id", "PC");
//        pcRow.setAttribute("description", "Corporate PCard");
//        pcRow.setAttribute("payment_type", "C");
//        pcRow.setAttribute("has_expiration_date", "Y");
//
//        doc.getFirstChild().appendChild(pcRow);
//
//        return doc;
    }

    public static Document getAllShipMethods()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SHIP_METHOD);
    }
    
    public static Document getAllComplimentaryAddons()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_COMPLIMENTARY_ADDON);
    }  
  
    public static Document getAllBinCheckPartners()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SOURCE_PARTNER_BIN);
    }

    public static Document getAllPartners()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PARTNER);
    }

    public static Document getAllCompanies()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_COMPANY);
    }

    public static Document getAllPricingCodes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PRICING_CODE);
    }

    public static Document getAllMileageCalcSources()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_MILEAGE_CALC_SOURCE);
    }

    public static Document getAllOrderSources()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_ORDER_SOURCE);
    }

    public static Document getAllBonusTypes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_BONUS_TYPE);
    }

    public static Document getAllDiscountTypes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_DISCOUNT_TYPE);
    }

    public static Document getAllServiceFeeCodes()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SERVICE_FEE_CODE);
    }
    
    public static Document getAllRedemptionRates()
        throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_REDEMPTION_RATE);
    }  
    
    public static Document getAllProdAttributes()
    throws FtdDataException, Exception {
        return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_PROD_ATTRIBUTES);
    }
    
    public static Document getAllDeliveryFeeCodes()
    throws FtdDataException, Exception {
    return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_DELIVERY_FEE_CODE);
}
    
  /**
   * Returns the Address Types from the FRP.ADDRESS_TYPE table 
   * @return
   */
    public static Document getAddressTypes() 
    throws FtdDataException, Exception {    
      return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_ADDRESS_TYPES);
    }
 
 
  /**
   * Returns the Country List that is used for Order Entry
   * @return
   */
    public static Document getCountryListOE() 
    throws FtdDataException, Exception {    
      return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_COUNTRY_LIST_OE);
    }
 
  /**
   * Returns the State List that is used for Order Entry
   * @return
   */
    public static Document getStateListOE() 
    throws FtdDataException, Exception {    
      return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_STATE_LIST_OE);
    }
    
    /**
     * This method will return a Document which contains all the member levels,associated source codes and descriptions
     * @return Document
     */
    public static Document getAllSourceCodesAndMemberlevels() 
    throws FtdDataException, Exception {
    	return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_SRC_CODES_AND_MEMBER_LEVEL);
    }
    
    /**
     * This method will return a Document which contains all the member levels,associated source codes and descriptions
     * @return Document
     */
    public static Document getAllOSCARScenarioGroups() 
    throws FtdDataException, Exception {
    	return GlobalDAO.getAllCodeList(GlobalDAO.DB_SELECT_ALL_OSCAR_SCENARIO_GROUPS);
    }
}

