package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.CompanyVO;
import com.ftd.enterprise.vo.CustomerVO;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.security.cache.vo.UserInfo;

public class CatalogRequestVO extends BaseRequestVO {
    private CustomerVO customerVO;
    private CompanyVO companyVO;
    private UserInfo userVO;

    public CatalogRequestVO(CustomerVO customerVO, CompanyVO companyVO, String userSessionId) throws FtdSecurityException {
        setCustomerVO(customerVO);
        setCompanyVO(companyVO);
        setUserSessionId(userSessionId);
    }

    public CustomerVO getCustomerVO() {
        return customerVO;
    }
    
    private void setCustomerVO(CustomerVO customerVO) {
        this.customerVO = customerVO;
    }
    
    public CompanyVO getCompanyVO() {
        return companyVO;
    }

    private void setCompanyVO(CompanyVO companyVO) {
        this.companyVO = companyVO;
    }
}
