package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.ProductVO;
import com.ftd.marketing.exception.FtdSecurityException;


public class ProductRequestVO extends BaseRequestVO {
    private ProductVO productVO;

    public ProductRequestVO(ProductVO productVO, String userSessionId)
        throws FtdSecurityException {
        setProductVO(productVO);
        setUserSessionId(userSessionId);
    }

    public ProductVO getProductVO() {
        return productVO;
    }

    private void setProductVO(ProductVO productVO) {
        this.productVO = productVO;
    }
}
