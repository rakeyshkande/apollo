package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.SourceProgramRefVO;
import com.ftd.marketing.exception.FtdSecurityException;

public class SourceProgramRequestVO extends BaseRequestVO {
    private SourceProgramRefVO sourceProgramRefVO;

    public SourceProgramRequestVO(SourceProgramRefVO sourceProgramRefVO,
        String userSessionId) throws FtdSecurityException {
        setSourceProgramRefVO(sourceProgramRefVO);
        setUserSessionId(userSessionId);
    }

    public SourceProgramRefVO getSourceProgramRefVO() {
        return sourceProgramRefVO;
    }

    private void setSourceProgramRefVO(SourceProgramRefVO sourceProgramRefVO) {
        this.sourceProgramRefVO = sourceProgramRefVO;
    }
}
