package com.ftd.marketing.vo;

import com.ftd.marketing.bo.SecurityServiceBO;
import com.ftd.marketing.exception.FtdSecurityException;

import com.ftd.security.cache.vo.UserInfo;


public abstract class BaseRequestVO {
    private UserInfo userVO;
    private String userSessionId;

    public UserInfo getUserVO() {
        return userVO;
    }

    protected void setUserVO(UserInfo userVO) {
        this.userVO = userVO;
    }

    public String getUserSessionId() {
        return userSessionId;
    }

    protected void setUserSessionId(String userSessionId)
        throws FtdSecurityException {
        this.userSessionId = userSessionId;
        
        // Sympathy - For web portal requests to create a new source code
        if(userSessionId.equalsIgnoreCase("Website Portal")) {        	
        	UserInfo user = new UserInfo();
        	user.setUserID(userSessionId);
        	setUserVO(user);
        	return;
        }
        
        // Set the user info.
        setUserVO(SecurityServiceBO.getUserInfo(userSessionId));
    }
}
