package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.SourceTypeVO;
import com.ftd.security.cache.vo.UserInfo;

public class SourceTypeRequestVO {
    private SourceTypeVO sourceTypeVO;
    private UserInfo userVO;

    public SourceTypeRequestVO(SourceTypeVO sourceTypeVO, UserInfo userVO) {
        setSourceTypeVO(sourceTypeVO);
        setUserVO(userVO);
    }

    public UserInfo getUserVO() {
        return userVO;
    }

    private void setUserVO(UserInfo userVO) {
        this.userVO = userVO;
    }

    public SourceTypeVO getSourceTypeVO() {
        return sourceTypeVO;
    }

    private void setSourceTypeVO(SourceTypeVO sourceTypeVO) {
        this.sourceTypeVO = sourceTypeVO;
    }
}
