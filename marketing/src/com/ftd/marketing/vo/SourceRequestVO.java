package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.exception.FtdSecurityException;


public class SourceRequestVO extends BaseRequestVO {
    private SourceVO sourceVO;
    private String programName;
    private boolean ccNumberUpdated;

    public SourceRequestVO(SourceVO sourceVO, String userSessionId)
        throws FtdSecurityException {
        setSourceVO(sourceVO);
        setUserSessionId(userSessionId);
    }

    public SourceVO getSourceVO() {
        return sourceVO;
    }

    private void setSourceVO(SourceVO sourceVO) {
        this.sourceVO = sourceVO;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public void setCcNumberUpdated(boolean ccNumberUpdated) {
        this.ccNumberUpdated = ccNumberUpdated;
    }

    public boolean isCcNumberUpdated() {
        return ccNumberUpdated;
    }
}
