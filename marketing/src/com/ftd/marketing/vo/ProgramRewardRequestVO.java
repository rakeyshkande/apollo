package com.ftd.marketing.vo;

import com.ftd.enterprise.vo.ProgramRewardVO;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.security.cache.vo.UserInfo;


public class ProgramRewardRequestVO extends BaseRequestVO {
    private ProgramRewardVO programRewardVO;

    public ProgramRewardRequestVO(ProgramRewardVO programRewardVO, String userSessionId) throws FtdSecurityException  {
        setProgramRewardVO(programRewardVO);
        setUserSessionId(userSessionId);
    }

    public ProgramRewardVO getProgramRewardVO() {
        return programRewardVO;
    }

    private void setProgramRewardVO(ProgramRewardVO programRewardVO) {
        this.programRewardVO = programRewardVO;
    }
}
