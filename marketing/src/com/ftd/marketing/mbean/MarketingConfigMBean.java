package com.ftd.marketing.mbean;

public interface MarketingConfigMBean {
    public void setXslFileMapping(String key, String value);
}
