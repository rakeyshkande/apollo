package com.ftd.marketing.constants;


/**
 * Application-wide constants resource.
 * @todo We need to explore binding these values to JNDI rather than flat files.
 * @author JP Puzon
 */
public class MarketingConstants {
    /*********************************************************************************************
    //Constants
    *********************************************************************************************/
    public static final String CONS_APP_PARAMETERS = "parameters";
    public static final String CONS_RESULT_SUCCESS = "-100";

    /*********************************************************************************************
    //config file locations
    *********************************************************************************************/
    public static final String PROPERTY_FILE = "marketing_config.xml";
    public static final String MARKETING_CONFIG_CONTEXT = "MARKETING_CONFIG";

    // constants to be used against the property file
    public static final String SECURITY_MAIN_MENU_REDIRECT = "SECURITY_MAIN_MENU_REDIRECT";
    public static final String DATASOURCE_NAME = "DATASOURCE_NAME";

    public static final String PREMIER_CIRCLE_CONTEXT = "PREMIER_CIRCLE";
    public static final String PREMIER_CIRCLE_CAMP_URL = "CAMP_PORTAL_URL";
    
    /*********************************************************************************************
    //Valid values for XSL Files
    *********************************************************************************************/
    public static final String XSL_SOURCE_TYPE_CODE = "SourceTypeMaint";
    public static final String XSL_SOURCE_TYPE_LIST = "SourceTypeList";
    public static final String XSL_PREMIER_CIRCLE_MAINT = "premierCircleMaint";
 
    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT = "context";
    public static final String SEC_TOKEN = "securitytoken";
    public static final String SECURITY_IS_ON = "SECURITY_IS_ON";
    public static final String AAS_USERS_MKT_ROLE_NAME = "Marketing";
//    public static final String AAS_USERS_MKT_DEPARTMENT_ID = "MKT";
    public static final String AAS_USERS_OPS_DEPARTMENT_ID = "OPS";

    /*********************************************************************************************
    //custom parameters returned from database procedure
    *********************************************************************************************/
    public static final String DB_STATUS_PARAM = "OUT_STATUS";
    public static final String DB_MESSAGE_PARAM = "OUT_MESSAGE";
    public static final String COMMON_VALUE_YES = "Y";
    public static final String COMMON_VALUE_NO = "N";
    
    public static final String CUSTOMER_LOCK_ENTITY_TYPE = "CUSTOMER"; // for locking

}
