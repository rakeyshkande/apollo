package com.ftd.marketing.test;

import com.ftd.enterprise.vo.CompanyVO;


public class CompanyFixture {
    CompanyVO companyVOPlain;

    public CompanyFixture() {
    }

    public void setUp() {
        companyVOPlain = new CompanyVO("FTDFS");
    }

    public void tearDown() {
    }

    public CompanyVO getCompanyVO() {
        return companyVOPlain;
    }
}
