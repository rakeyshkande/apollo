package com.ftd.marketing.test;

import com.ftd.security.cache.vo.UserInfo;


public class UserFixture {
    private UserInfo userVOPlain;

    public UserFixture() {
    }

    public void setUp() {
        userVOPlain = new UserInfo();
                userVOPlain.setUserID("testUser");
    }

    public void tearDown() {
    }

    public UserInfo getUserVO() {
        return userVOPlain;
    }
}
