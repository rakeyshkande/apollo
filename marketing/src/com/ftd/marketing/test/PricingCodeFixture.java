package com.ftd.marketing.test;

import com.ftd.enterprise.vo.DiscountTierVO;
import com.ftd.enterprise.vo.PricingCodeVO;


public class PricingCodeFixture {
    public PricingCodeFixture() {
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public PricingCodeVO getPricingCodeVO(String pricingCode,
        String discountType) {
        PricingCodeVO pricingCodeVO = new PricingCodeVO(pricingCode,
                "Marketing test description", discountType);

        pricingCodeVO.addDiscountTier(new DiscountTierVO(new Float(0),
                new Float(10), new Float(15.5)));
        pricingCodeVO.addDiscountTier(new DiscountTierVO(new Float(10.01),
                new Float(20), new Float(25.5)));
        pricingCodeVO.addDiscountTier(new DiscountTierVO(new Float(20.01),
                new Float(30), new Float(35.5)));

        return pricingCodeVO;
    }
}
