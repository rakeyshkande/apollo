package com.ftd.marketing.test;

import com.ftd.enterprise.vo.CustomerVO;
import com.ftd.enterprise.vo.EmailVO;
import com.ftd.enterprise.vo.PhoneVO;


public class CustomerFixture {
    CustomerVO customerVONoEmail;
    CustomerVO customerVOWithEmail;

    public CustomerFixture() {
    }

    public void setUp() {
        // Initialize the customer without email.
        customerVOWithEmail = new CustomerVO();
        customerVOWithEmail.setFullName("Marketing", "TestNoEmail");
        customerVOWithEmail.setFullAddress("100 Clark St.", "Suite 2032",
            "Chicago", "IL", "60559", "AM");

        customerVOWithEmail.addPhone(new PhoneVO(
                com.ftd.enterprise.vo.PhoneType.DAYTIME_PHONE_TYPE,
                "3126451941", "345"));
        customerVOWithEmail.addPhone(new PhoneVO(
                com.ftd.enterprise.vo.PhoneType.EVENING_PHONE_TYPE, "6308459123"));

        // Initialize the customer with email.
        customerVOWithEmail = new CustomerVO();
        customerVOWithEmail.setFullName("Marketing", "TestWithEmail");
        customerVOWithEmail.setFullAddress("100 Clark St.", "Suite 2032",
            "Chicago", "IL", "60559", "AM");

        customerVOWithEmail.addPhone(new PhoneVO(
                com.ftd.enterprise.vo.PhoneType.DAYTIME_PHONE_TYPE,
                "3126451941", "345"));
        customerVOWithEmail.addPhone(new PhoneVO(
                com.ftd.enterprise.vo.PhoneType.EVENING_PHONE_TYPE, "6308459123"));

        EmailVO emailVO = new EmailVO();
        emailVO.setEmailAddress("whatever@aol.com");
        customerVOWithEmail.setEmailVO(emailVO);
    }

    public void tearDown() {
    }

    public CustomerVO getCustomerVO() {
        return customerVONoEmail;
    }

    public CustomerVO getCustomerVOWithEmail() {
        return customerVOWithEmail;
    }
}
