package com.ftd.marketing.test;

import com.ftd.enterprise.vo.SourceTypeVO;

import java.util.Calendar;


public class SourceTypeFixture {
    SourceTypeVO sourceTypeVOPlain;

    public SourceTypeFixture() {
    }

    public void setUp() {
        sourceTypeVOPlain = new SourceTypeVO("Marketing TestSourceType" +
                Calendar.getInstance().getTimeInMillis());
    }

    public void tearDown() {
    }

    public SourceTypeVO getSourceTypeVO() {
        return sourceTypeVOPlain;
    }
}
