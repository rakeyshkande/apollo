package com.ftd.marketing.exception;

public interface IFtdException {
    
    // Data-related errors
    public static final String ERROR_CODE_UNIQUENESS_VIOLATION = "-1";
    public static final String ERROR_CODE_REFERENTIAL_INTEGRITY_VIOLATION = "-2";
    public static final String ERROR_CODE_RESERVED_VIOLATION = "-3";
    
    // Security-related errors
    public static final String ERROR_CODE_USER_SECURITY_VIOLATION = "-11";
    
    // Processing errors
     public static final String ERROR_CODE_MANUAL_INTERVENTION_REQUIRED = "-20";
}
