package com.ftd.marketing.exception;

public class FtdSecurityException extends Exception implements IFtdException {
    public FtdSecurityException(String message) {
        super(message);
    }

    public FtdSecurityException(String message, Throwable t) {
        super(message, t);
    }
}