package com.ftd.marketing.exception;

import com.ftd.marketing.actions.MarketingUtilities;


/**
 * Application-specific exception which documents issues pertaining to
 * Marketing database data.
 *
 * @author JP Puzon
 */
public class FtdDataException extends Exception implements IFtdException {
    private static final String UNIQUE_CONSTRAINT_VIOLATED = "ORA-00001";
    private String errorCode = "";

    public FtdDataException(String message) {
        super(message);
    }

    public FtdDataException(String message, String errorCode) {
        this(message);
        this.errorCode = errorCode;
    }

    public FtdDataException(String message, Throwable t) {
        super(message, t);
    }

    public boolean isUniquenessViolation() {
        if (errorCode.equals(this.ERROR_CODE_UNIQUENESS_VIOLATION)) {
            return true;
        }

        return MarketingUtilities.searchForString(getMessage(),
            UNIQUE_CONSTRAINT_VIOLATED);
    }
}
