package com.ftd.enterprise.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SourceVO extends BaseVO {
    private String description;
    private String snhId;
    private String priceHeaderId;
    private String sourceType;
    private String paymentMethodId;
    private Date startDate;
    private Date endDate;
    private String defaultSourceCodeFlag;
//    private String billingInfoPrompt;
//    private String billingInfoLogic;
    private String marketingGroup;
    private String externalCallCenterFlag;
    private String highlightDescriptionFlag;
    private String discountAllowedFlag;
    private String binNumberCheckFlag;
    private String orderSource;
    private String jcpenneyFlag;
    private String companyId;
    private String sendToScrub;
    private String fraudFlag;
    private String enableLpProcessing;
    private String emergencyTextFlag;
    private String requiresDeliveryConfirmation;
    private String webloyaltyFlag;
    private String promotionCode;
    private String bonusPromotionCode;
    private String requestedBy;
    private String relatedSourceCode;
    private String over21Flag;
    private String programWebsiteUrl;
    private Date updatedOnStart;
    private Date updatedOnEnd;
    private List cpcCollection = new ArrayList();
    private String commentText;
    private String partnerBankId;
    private String iotwFlag;
    private String invoicePassword;
    private String billingInfoFlag;
    private String giftCertificateFlag;
    private String mpRedemptionRateId;
    private String addOnFreeId;
    private String displayServiceFeeCode;
    private String displayShippingFeeCode;
    private String randomWeightedFlag;
    private String applySurchargeCode;
    private double surchargeAmount;
    private String surchargeDescription;
    private String displaySurcharge;
    private String allowFreeShippingFlag;

    private ArrayList <BinCheckPartnerVO> binCheckPartnerList;
    private ArrayList <ProductAttrExclusionVO> productAttrExclList;
    private ArrayList <SourceFloristPriorityVO> sourceFloristPriorityList;
    
    private ArrayList <MpMemberLevelVO> mpMemberLevelList;
    // Defect - 8276 - Same Day Upcharge - Iteration 1 changes
    private String sameDayUpcharge;
    
    private String displaySameDayUpcharge; 
    private String morningDeliveryFlag; 
    private String morningDeliveryToFSMembers;
    private String deliveryFeeId;
    
    // OSCAR fields with default values.
    private String oscarSelectionEnabledFlag = "N";
    private String oscarScenarioGroupId = "1";
    
    //Sympathy Controls with default values
    private String funeralCemeteryLocChk = "N";
    private String hospitalLocChck = "N";
    private String funeralLeadTimeChck = "Y";
    private String funeralLeadTime = "4";
    private String boHrsMonFriStart = "9:00";
    private String boHrsMonFriEnd = "18:00";
    private String boHrsSatStart = "9:00";
    private String boHrsSatEnd = "18:00";
    private String boHrsSunStart = "9:00";
    private String boHrsSunEnd = "17:00";
    
    //Legacy Id for Source Code
    private String legacyId;
    private String merchAmtFullRefundFlag;
    
    
    //Automated Promotion engine fields
    private String automatedPromotionEngineFlag = "N"; 
    private String apeProductCatalog;
    private String apeBaseSourceCodes;
    
    // #13046 - fields introduced with sympathy I1 
    private String emailProgramId;
    private ArrayList <SourceProgramRefVO> srcProgramList;
    
    private String calculateTaxFlag;
    
    private String shippingCarrierFlag;
  
    //SGC-2 Same Day Upcharge FS Member Controls
    private String sameDayUpchargeFS;
  
    
    private String recptAddress;
    private String recptZipcode;
    private String recptCity;
    private String recptState;
    private String recptCountry;
    private String recptPhoneNumber;
    
    
    public SourceVO() {
        super();
    }

    public SourceVO(String sourceCode) {
        super((sourceCode == null) ? sourceCode : sourceCode.toUpperCase());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSnhId() {
        return snhId;
    }

    public void setSnhId(String snhId) {
        this.snhId = snhId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setPriceHeaderId(String priceHeaderId) {
        this.priceHeaderId = priceHeaderId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDefaultSourceCodeFlag() {
        return defaultSourceCodeFlag;
    }

    public void setDefaultSourceCodeFlag(String defaultSourceCodeFlag) {
        this.defaultSourceCodeFlag = defaultSourceCodeFlag;
    }

//    public String getBillingInfoPrompt() {
//        return billingInfoPrompt;
//    }
//
//    public void setBillingInfoPrompt(String billingInfoPrompt) {
//        this.billingInfoPrompt = (billingInfoPrompt == null)
//            ? billingInfoPrompt : billingInfoPrompt.toUpperCase();
//    }
//
//    public String getBillingInfoLogic() {
//        return billingInfoLogic;
//    }
//
//    public void setBillingInfoLogic(String billingInfoLogic) {
//        this.billingInfoLogic = (billingInfoLogic == null) ? billingInfoLogic
//                                                           : billingInfoLogic.toUpperCase();
//    }

    public String getMarketingGroup() {
        return marketingGroup;
    }

    public void setMarketingGroup(String marketingGroup) {
        this.marketingGroup = marketingGroup;
    }

    public String getExternalCallCenterFlag() {
        return externalCallCenterFlag;
    }

    public void setExternalCallCenterFlag(String externalCallCenterFlag) {
        this.externalCallCenterFlag = externalCallCenterFlag;
    }

    public String getHighlightDescriptionFlag() {
        return highlightDescriptionFlag;
    }

    public void setHighlightDescriptionFlag(String highlightDescriptionFlag) {
        this.highlightDescriptionFlag = highlightDescriptionFlag;
    }

    public String getDiscountAllowedFlag() {
        return discountAllowedFlag;
    }

    public void setDiscountAllowedFlag(String discountAllowedFlag) {
        this.discountAllowedFlag = discountAllowedFlag;
    }

    public String getBinNumberCheckFlag() {
        return binNumberCheckFlag;
    }

    public void setBinNumberCheckFlag(String binNumberCheckFlag) {
        this.binNumberCheckFlag = binNumberCheckFlag;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getJcpenneyFlag() {
        return jcpenneyFlag;
    }

    public void setJcpenneyFlag(String jcpenneyFlag) {
        this.jcpenneyFlag = jcpenneyFlag;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSendToScrub() {
        return sendToScrub;
    }

    public void setSendToScrub(String sendToScrub) {
        this.sendToScrub = sendToScrub;
    }

    public String getFraudFlag() {
        return fraudFlag;
    }

    public void setFraudFlag(String fraudFlag) {
        this.fraudFlag = fraudFlag;
    }

    public String getEnableLpProcessing() {
        return enableLpProcessing;
    }

    public void setEnableLpProcessing(String enableLpProcessing) {
        this.enableLpProcessing = enableLpProcessing;
    }

    public String getEmergencyTextFlag() {
        return emergencyTextFlag;
    }

    public void setEmergencyTextFlag(String emergencyTextFlag) {
        this.emergencyTextFlag = emergencyTextFlag;
    }

    public String getRequiresDeliveryConfirmation() {
        return requiresDeliveryConfirmation;
    }

    public void setRequiresDeliveryConfirmation(
        String requiresDeliveryConfirmation) {
        this.requiresDeliveryConfirmation = requiresDeliveryConfirmation;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = (promotionCode == null) ? promotionCode
                                                     : promotionCode.toUpperCase();
    }

    public String getBonusPromotionCode() {
        return bonusPromotionCode;
    }

    public void setBonusPromotionCode(String bonusPromotionCode) {
        this.bonusPromotionCode = (bonusPromotionCode == null)
            ? bonusPromotionCode : bonusPromotionCode.toUpperCase();
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRelatedSourceCode() {
        return relatedSourceCode;
    }

    public void setRelatedSourceCode(String relatedSourceCode) {
        this.relatedSourceCode = (relatedSourceCode == null)
            ? relatedSourceCode : relatedSourceCode.toUpperCase();
    }

    public String getOver21Flag() {
        return over21Flag;
    }

    public void setOver21Flag(String over21Flag) {
        this.over21Flag = over21Flag;
    }

    public String getProgramWebsiteUrl() {
        return programWebsiteUrl;
    }

    public void setProgramWebsiteUrl(String programWebsiteUrl) {
        this.programWebsiteUrl = programWebsiteUrl;
    }

    public Date getUpdatedOnStart() {
        return updatedOnStart;
    }

    public void setUpdatedOnStart(Date updatedOnStart) {
        this.updatedOnStart = updatedOnStart;
    }

    public Date getUpdatedOnEnd() {
        return updatedOnEnd;
    }

    public void setUpdatedOnEnd(Date updatedOnEnd) {
        this.updatedOnEnd = updatedOnEnd;
    }

    public List getCpcCollection() {
        return cpcCollection;
    }

    public void addCpc(CpcVO cpcVO) {
        cpcCollection.add(cpcVO);
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getPartnerBankId() {
        return partnerBankId;
    }

    public void setPartnerBankId(String partnerBankId) {
        this.partnerBankId = partnerBankId;
    }

  public void setWebloyaltyFlag(String webloyaltyFlag)
  {
    this.webloyaltyFlag = webloyaltyFlag;
  }

  public String getWebloyaltyFlag()
  {
    return webloyaltyFlag;
  }

    public void setIotwFlag(String iotwFlag) {
        this.iotwFlag = iotwFlag;
    }

    public String getIotwFlag() {
        return iotwFlag;
    }

    public void setInvoicePassword(String invoicePassword) {
        this.invoicePassword = invoicePassword;
    }

    public String getInvoicePassword() {
        return invoicePassword;
    }

    public void setBillingInfoFlag(String billingInfoFlag) {
        this.billingInfoFlag = billingInfoFlag;
    }

    public String getBillingInfoFlag() {
        return billingInfoFlag;
    }

    public void setGiftCertificateFlag(String giftCertificateFlag) {
        this.giftCertificateFlag = giftCertificateFlag;
    }

    public String getGiftCertificateFlag() {
        return giftCertificateFlag;
    }

    public void setMpRedemptionRateId(String mpRedemptionRateId) {
        this.mpRedemptionRateId = mpRedemptionRateId;
    }

    public String getMpRedemptionRateId() {
        return mpRedemptionRateId;
    }

    public String getAddOnFreeId() {
        return addOnFreeId;
    }
  
    public void setAddOnFreeId(String addOnFreeId) {
        this.addOnFreeId = addOnFreeId;
    }
     
    public String getDisplayServiceFeeCode()
    {
        return displayServiceFeeCode;
    }

    public void setDisplayServiceFeeCode(String displayServiceFeeCode)
    {
        this.displayServiceFeeCode = displayServiceFeeCode;
    }

    public String getDisplayShippingFeeCode()
    {
        return displayShippingFeeCode;
    }

    public void setDisplayShippingFeeCode(String displayShippingFeeCode)
    {
        this.displayShippingFeeCode = displayShippingFeeCode;
    }
    
    public ArrayList<BinCheckPartnerVO> getBinCheckPartnerList() {
        return binCheckPartnerList;
    }
  
    public void setBinCheckPartnerList(ArrayList<BinCheckPartnerVO> binCheckPartnerList) {
        this.binCheckPartnerList = binCheckPartnerList;
    }

    public void setProductAttrExclList(ArrayList<ProductAttrExclusionVO> productAttrExclList) {
        this.productAttrExclList = productAttrExclList;
    }

    public ArrayList<ProductAttrExclusionVO> getProductAttrExclList() {
        return productAttrExclList;
    }

    public void setSourceFloristPriorityList(ArrayList<SourceFloristPriorityVO> sourceFloristPriorityList) {
        this.sourceFloristPriorityList = sourceFloristPriorityList;
    }

    public ArrayList<SourceFloristPriorityVO> getSourceFloristPriorityList() {
    	if(sourceFloristPriorityList == null) {
    		sourceFloristPriorityList = new ArrayList<SourceFloristPriorityVO>();
    	}
        return sourceFloristPriorityList;
    }

    public void setRandomWeightedFlag(String randomWeightedFlag) {
        this.randomWeightedFlag = randomWeightedFlag;
    }
  
    public String getRandomWeightedFlag() {
        return randomWeightedFlag;
    }

  public void setApplySurchargeCode(String applySurcharge)
  {
    this.applySurchargeCode = applySurcharge;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }

  public void setSurchargeAmount(double surchargeAmount)
  {
    this.surchargeAmount = surchargeAmount;
  }

  public double getSurchargeAmount()
  {
    return surchargeAmount;
  }

  public void setSurchargeDescription(String surchargeDescription)
  {
    this.surchargeDescription = surchargeDescription;
  }

  public String getSurchargeDescription()
  {
    return surchargeDescription;
  }

  public void setDisplaySurcharge(String displaySurcharge)
  {
    this.displaySurcharge = displaySurcharge;
  }

  public String getDisplaySurcharge()
  {
    return displaySurcharge;
  }

    public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

    public String getAllowFreeShippingFlag() {
        return allowFreeShippingFlag;
    }

    // Defect - 8276 - Same Day Upcharge - Iteration 1 changes
	public String getSameDayUpcharge() {
		return sameDayUpcharge;
	}

	public void setSameDayUpcharge(String sameDayUpcharge) {
		this.sameDayUpcharge = sameDayUpcharge;
	}

	public String getDisplaySameDayUpcharge() {
		return displaySameDayUpcharge;
	}

	public void setDisplaySameDayUpcharge(String displaySameDayUpcharge) {
		this.displaySameDayUpcharge = displaySameDayUpcharge;
	}

	public ArrayList<MpMemberLevelVO> getMpMemberLevelList() {
		return mpMemberLevelList;
	}

	public void setMpMemberLevelList(ArrayList<MpMemberLevelVO> mpMemberLevelList) {
		this.mpMemberLevelList = mpMemberLevelList;
	}

	public void setMorningDeliveryFlag(String morningDeliveryFlag) {
		this.morningDeliveryFlag = morningDeliveryFlag;
	}

	public String getMorningDeliveryFlag() {
		return morningDeliveryFlag;
	}

	public void setMorningDeliveryToFSMembers(String morningDeliveryToFSMembers) {
		this.morningDeliveryToFSMembers = morningDeliveryToFSMembers;
	}

	public String getMorningDeliveryToFSMembers() {
		return morningDeliveryToFSMembers;
	}

	public void setDeliveryFeeId(String deliveryFeeId) {
		this.deliveryFeeId = deliveryFeeId;
	}

	public String getDeliveryFeeId() {
		return deliveryFeeId;
	}

	public String getEmailProgramId() {
		return emailProgramId;
	}

	public void setEmailProgramId(String emailProgramId) {
		this.emailProgramId = emailProgramId;
	}

	public ArrayList<SourceProgramRefVO> getSrcProgramList() {
		if(srcProgramList == null) {
			srcProgramList = new ArrayList<SourceProgramRefVO>();
		}
		return srcProgramList;
	}
	
    public void setOscarSelectionEnabledFlag(String oscarSelectionEnabledFlag) {
        this.oscarSelectionEnabledFlag = oscarSelectionEnabledFlag;
    }
  
    public String getOscarSelectionEnabledFlag() {
        return oscarSelectionEnabledFlag;
    }
    
    public void setOscarScenarioGroupId(String oscarScenarioGroupId) {
        this.oscarScenarioGroupId = oscarScenarioGroupId;
    }
  
    public String getOscarScenarioGroupId() {
        return oscarScenarioGroupId;
    }
    
 //Sympathy Controls
    
    public void setFuneralCemeteryLocChk(String funeralCemeteryLocChk) {
        this.funeralCemeteryLocChk = funeralCemeteryLocChk;
    }
    
    public String getFuneralCemeteryLocChk() {
        return funeralCemeteryLocChk;
    }
    
    public void setHospitalLocChck(String hospitalLocChck) {
        this.hospitalLocChck = hospitalLocChck;
    }
    
    public String getHospitalLocChck() {
        return hospitalLocChck;
    }
    
    public void setFuneralLeadTimeChck(String funeralLeadTimeChck) {
        this.funeralLeadTimeChck = funeralLeadTimeChck;
    }
    
    public String getFuneralLeadTimeChck() {
        return funeralLeadTimeChck;
    }
    
    public void setFuneralLeadTime(String funeralLeadTime) {
        this.funeralLeadTime = funeralLeadTime;
    }
    
    public String getFuneralLeadTime() {
        return funeralLeadTime;
    }
    
    public void setBoHrsMonFriStart(String boHrsMonFriStart) {
        this.boHrsMonFriStart = boHrsMonFriStart;
    }
    
    public String getBoHrsMonFriStart() {
        return boHrsMonFriStart;
    }
    
    public void setBoHrsMonFriEnd(String boHrsMonFriEnd) {
        this.boHrsMonFriEnd = boHrsMonFriEnd;
    }
    
    public String getBoHrsMonFriEnd() {
        return boHrsMonFriEnd;
    }
    
    public void setBoHrsSatStart(String boHrsSatStart) {
        this.boHrsSatStart = boHrsSatStart;
    }
    
    public String getBoHrsSatStart() {
        return boHrsSatStart;
    }
    
    public void setBoHrsSatEnd(String boHrsSatEnd) {
        this.boHrsSatEnd = boHrsSatEnd;
    }
    
    public String getBoHrsSatEnd() {
        return boHrsSatEnd;
    }
    
    public void setBoHrsSunStart(String boHrsSunStart) {
        this.boHrsSunStart = boHrsSunStart;
    }
    
    public String getBoHrsSunStart() {
        return boHrsSunStart;
    }
    
    public void setBoHrsSunEnd(String boHrsSunEnd) {
        this.boHrsSunEnd = boHrsSunEnd;
    }
    
    public String getBoHrsSunEnd() {
        return boHrsSunEnd;
    }

	public void setAutomatedPromotionEngineFlag(
			String automatedPromotionEngineFlag) {
		this.automatedPromotionEngineFlag = automatedPromotionEngineFlag;
	}

	public String getAutomatedPromotionEngineFlag() {
		return automatedPromotionEngineFlag;
	}

	public void setApeProductCatalog(String apeProductCatalog) {
		this.apeProductCatalog = apeProductCatalog;
	}

	public String getApeProductCatalog() {
		return apeProductCatalog;
	}

	public void setApeBaseSourceCodes(String apeBaseSourceCodes) {
		this.apeBaseSourceCodes = apeBaseSourceCodes;
	}

	public String getApeBaseSourceCodes() {
		return apeBaseSourceCodes;
	}

	public String getLegacyId() {
		return legacyId;
	}

	public void setLegacyId(String legacyId) {
		this.legacyId = legacyId;
	}

	/**
	 * @return the calculateTax
	 */
	public String getCalculateTaxFlag() {
		return calculateTaxFlag;
	}

	/**
	 * @param calculateTax the calculateTax to set
	 */
	public void setCalculateTaxFlag(String calculateTax) {
		this.calculateTaxFlag = calculateTax;
	}
	/**
	 * @return the calculateTax
	 */
	public String getShippingCarrierFlag() {
		return shippingCarrierFlag;
	}

	/**
	 * @param calculateTax
	 *            the calculateTax to set
	 */
	public void setShippingCarrierFlag(String shippingCarrierFlag) {
		this.shippingCarrierFlag = shippingCarrierFlag;
	}

	/**
	 * @return
	 */
	public String getMerchAmtFullRefundFlag() {
		return merchAmtFullRefundFlag;
	}

	/**
	 * @param mechAmtFullRefundFlag
	 */
	public void setMerchAmtFullRefundFlag(String merchAmtFullRefundFlag) {
		this.merchAmtFullRefundFlag = merchAmtFullRefundFlag;
	}
	
	//#SGC-2
	public String getSameDayUpchargeFS() {
		return sameDayUpchargeFS;
	}

	public void setSameDayUpchargeFS(String sameDayUpchargeFS) {
		this.sameDayUpchargeFS = sameDayUpchargeFS;
	}

	public String getRecptAddress() {
		return recptAddress;
	}

	public void setRecptAddress(String recptAddress) {
		this.recptAddress = recptAddress;
	}

	public String getRecptZipcode() {
		return recptZipcode;
	}

	public void setRecptZipcode(String recptZipcode) {
		this.recptZipcode = recptZipcode;
	}

	public String getRecptCity() {
		return recptCity;
	}

	public void setRecptCity(String recptCity) {
		this.recptCity = recptCity;
	}

	public String getRecptState() {
		return recptState;
	}

	public void setRecptState(String recptState) {
		this.recptState = recptState;
	}

	public String getRecptCountry() {
		return recptCountry;
	}

	public void setRecptCountry(String recptCountry) {
		this.recptCountry = recptCountry;
	}

	public String getRecptPhoneNumber() {
		return recptPhoneNumber;
	}

	public void setRecptPhoneNumber(String recptPhoneNumber) {
		this.recptPhoneNumber = recptPhoneNumber;
	}

	
	
}
