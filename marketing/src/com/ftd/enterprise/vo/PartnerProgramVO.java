package com.ftd.enterprise.vo;

public class PartnerProgramVO extends BaseVO {
    private String partnerName;
    private String programType;
    private String emailExcludeFlag;
    private String programLongName;
    private String membershipDataRequired;

    public PartnerProgramVO() {
        super();
    }

    public PartnerProgramVO(String programName) {
        super(programName);
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getEmailExcludeFlag() {
        return emailExcludeFlag;
    }

    public void setEmailExcludeFlag(String emailExcludeFlag) {
        this.emailExcludeFlag = emailExcludeFlag;
    }

    public String getProgramLongName() {
        return programLongName;
    }

    public void setProgramLongName(String programLongName) {
        this.programLongName = programLongName;
    }
    
    public String getMembershipDataRequired() {
        return membershipDataRequired;
    }
    
    public void setMembershipDataRequired(String membershipDataRequired) {
        this.membershipDataRequired = membershipDataRequired;
    }
    
    public String getProgramName() {
        return getId();
    }
}
