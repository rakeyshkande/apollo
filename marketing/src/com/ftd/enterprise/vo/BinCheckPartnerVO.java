package com.ftd.enterprise.vo;

public class BinCheckPartnerVO
{
  private String partnerName;
  private Boolean active;

  public String getPartnerName()
  {
    return partnerName;
  }

  public void setPartnerName(String partnerName)
  {
    this.partnerName = partnerName;
  }

  public Boolean isActive()
  {
    return active;
  }

  public void setActive(Boolean active)
  {
    this.active = active;
  }
}
