package com.ftd.enterprise.vo;

import java.util.ArrayList;
import java.util.List;


public class PricingCodeVO extends BaseVO {
    protected List discountTiers = new ArrayList();
    protected String discountType;
    protected String description;

    public PricingCodeVO() {
        super();
    }

    public PricingCodeVO(String pricingCodeId, String description,
        String discountType) {
        super(pricingCodeId);
        this.setDescription(description);
        this.setDiscountType(discountType);
    }

    public void addDiscountTier(DiscountTierVO discountTierVO) {
        discountTiers.add(discountTierVO);
    }

    public int getDiscountTierCount() {
        return discountTiers.size();
    }

    public List getDiscountTiers() {
        return discountTiers;
    }

    public String getDiscountType() {
        return discountType;
    }

    protected void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDescription() {
        return description;
    }

    protected void setDescription(String description) {
        this.description = description;
    }
}
