package com.ftd.enterprise.vo;
/**
 * This class will hold the member level details like mp_member_level_id.
 */
public class MpMemberLevelVO {
	
	private String mpMemberLevelId;	
		
	public String getMpMemberLevelId() {
		return mpMemberLevelId;
	}
	public void setMpMemberLevelId(String mpMemberLevelId) {
		this.mpMemberLevelId = mpMemberLevelId;
	}	
	
	
	
}
