package com.ftd.enterprise.vo;

public class DiscountTierVO extends BaseVO {
    protected Float minDollarBound;
    protected Float maxDollarBound;
    protected Float discountAmount;

    // prevent instantiation without full qualification.
    private DiscountTierVO() {
        super();
    }

    private DiscountTierVO(String id) {
        super(id);
    }

    public DiscountTierVO(Float minDollarBound, Float maxDollarBound,
        Float discountAmount) {
        this.minDollarBound = minDollarBound;
        this.maxDollarBound = maxDollarBound;
        this.discountAmount = discountAmount;
    }

    public Float getMinDollarBound() {
        return minDollarBound;
    }

    public Float getMaxDollarBound() {
        return maxDollarBound;
    }

    public Float getDiscountAmount() {
        return discountAmount;
    }
}
