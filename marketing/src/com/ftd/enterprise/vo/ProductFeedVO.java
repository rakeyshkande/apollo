package com.ftd.enterprise.vo;

public class ProductFeedVO extends ProductVO {
    private String buyUrl;
    private String imageUrl;
    private String importantProductFlag;
    private String promotionalText;
    private String shippingPromotionalText;

    public ProductFeedVO() {
        super();
    }

    public ProductFeedVO(String productId) {
        super((productId == null) ? productId : productId.toUpperCase());
    }

    public String getBuyUrl() {
        return buyUrl;
    }

    public void setBuyUrl(String buyUrl) {
        this.buyUrl = buyUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImportantProductFlag() {
        return importantProductFlag;
    }

    public void setImportantProductFlag(String importantProductFlag) {
        this.importantProductFlag = importantProductFlag;
    }

    public String getPromotionalText() {
        return promotionalText;
    }

    public void setPromotionalText(String promotionalText) {
        this.promotionalText = promotionalText;
    }

    public String getShippingPromotionalText() {
        return shippingPromotionalText;
    }

    public void setShippingPromotionalText(String shippingPromotionalText) {
        this.shippingPromotionalText = shippingPromotionalText;
    }
}
