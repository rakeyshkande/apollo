package com.ftd.enterprise.vo;

public class SecurityResource implements IEnum {
    public static final SecurityResource SOURCE_PAYMENT_METHOD_DATA = new SecurityResource("SourcePaymentMethodData");
    public static final SecurityResource SOURCE_PRICE_HEADER_DATA = new SecurityResource("SourcePriceHeaderData");
    public static final SecurityResource SOURCE_PROMOTION_CODE_DATA = new SecurityResource("SourcePromotionCodeData");
    public static final SecurityResource SOURCE_PCARD_DATA = new SecurityResource("SourcePCardData");
    public static final SecurityResource SOURCE_SNH_DATA = new SecurityResource("SourceSnhData");
    public static final SecurityResource SOURCE_BILLING_DATA = new SecurityResource("SourceBillingData");
    public static final SecurityResource PARTNER_PROMOTION_DATA = new SecurityResource("PartnerPromotionData");
    public static final SecurityResource PRICING_CODE_DATA = new SecurityResource("PricingCodeData");
    public static final SecurityResource SOURCE_DEFINITION_DATA = new SecurityResource("SourceDefinitionData");
    public static final SecurityResource SOURCE_TYPE_DATA = new SecurityResource("SourceTypeData");
    public static final SecurityResource RDMPTN_RATE_DATA = new SecurityResource("RdmptnRateData");
    
    private String type;

    private SecurityResource(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof SecurityResource) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}