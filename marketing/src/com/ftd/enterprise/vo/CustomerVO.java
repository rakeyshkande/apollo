package com.ftd.enterprise.vo;

import com.ftd.enterprise.vo.PhoneType;
import com.ftd.enterprise.vo.PhoneVO;

import java.util.HashMap;
import java.util.Map;


public class CustomerVO extends BaseVO {
    String firstName;
    String lastName;
    String address1;
    String address2;
    String city;
    String state;
    String zipCode;
    String country;
    Map phones = new HashMap();
    EmailVO emailVO;

    public CustomerVO() {
        super();
    }

    public CustomerVO(String customerId) {
        super(customerId);
    }

    public void setFullName(String firstName, String lastName) {
        setFirstName(firstName);
        setLastName(lastName);
    }

    public void setFullAddress(String address1, String address2, String city,
        String state, String zipCode, String country) {
        setAddress1(address1);
        setAddress2(address2);
        setCity(city);
        setState(state);
        setZipCode(zipCode);
        setCountry(country);
    }

    public void addPhone(PhoneVO phoneVO) {
        // The key is the phone type.
        phones.put(phoneVO.getType(), phoneVO);
    }

    public PhoneVO getPhone(PhoneType phoneType) {
        // The key is the phone type.
        return (PhoneVO) phones.get(phoneType);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (valueChanged(this.firstName, firstName)) {
            setChanged(true);
            this.firstName = firstName;
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (valueChanged(this.lastName, lastName)) {
            setChanged(true);
            this.lastName = lastName;
        }
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        if (valueChanged(this.address1, address1)) {
            setChanged(true);
            this.address1 = address1;
        }
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        if (valueChanged(this.address2, address2)) {
            setChanged(true);
            this.address2 = address2;
        }
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (valueChanged(this.city, city)) {
            setChanged(true);
            this.city = city;
        }
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        if (valueChanged(this.state, state)) {
            setChanged(true);
            this.state = state;
        }
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        if (valueChanged(this.zipCode, zipCode)) {
            setChanged(true);
            this.zipCode = zipCode;
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (valueChanged(this.country, country)) {
            setChanged(true);
            this.country = country;
        }
    }

    public void setEmailVO(EmailVO emailVO) {
        this.emailVO = emailVO;
    }

    public EmailVO getEmailVO() {
        return emailVO;
    }

    public Map getAllCustomerPhones() {
        return phones;
    }
}
