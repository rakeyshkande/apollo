package com.ftd.enterprise.vo;


/**
 * Typesafe enum for phone number types.
 */
public final class PhoneType implements IEnum {
    public static final PhoneType DAYTIME_PHONE_TYPE = new PhoneType("Day");
    public static final PhoneType EVENING_PHONE_TYPE = new PhoneType("Evening");
    private String type;

    private PhoneType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof PhoneType) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}
