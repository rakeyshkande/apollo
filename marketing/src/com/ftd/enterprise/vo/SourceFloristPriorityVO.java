package com.ftd.enterprise.vo;

public class SourceFloristPriorityVO extends BaseVO {
    private String floristId;
    private int priority;

    public SourceFloristPriorityVO() {
        super();
    }

    public void setFloristId(String floristId) {
        this.floristId = floristId;
    }

    public String getFloristId() {
        return floristId;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

	/** parameterized Constructor
	 * @param floristId
	 * @param priority
	 */
	public SourceFloristPriorityVO(String floristId, int priority) {	
		this.floristId = floristId;
		this.priority = priority;
	}    
}
