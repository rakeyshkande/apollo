package com.ftd.enterprise.vo;

public class ProgramRewardRangeVO extends BaseVO {
  private Float minDollar;
  private Float maxDollar;
    private String calculationBasis;
  private Float points;

    public ProgramRewardRangeVO() {
        super();
    }

    public ProgramRewardRangeVO(String programName) {
        super(programName == null? programName: programName.toUpperCase());
    }

    public Float getMinDollar() {
        return minDollar;
    }

    public void setMinDollar(Float minDollar) {
        this.minDollar = minDollar;
    }

    public Float getMaxDollar() {
        return maxDollar;
    }

    public void setMaxDollar(Float maxDollar) {
        this.maxDollar = maxDollar;
    }

    public String getCalculationBasis() {
        return calculationBasis;
    }

    public void setCalculationBasis(String calculationBasis) {
        this.calculationBasis = calculationBasis;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }
}
