package com.ftd.enterprise.vo;

import com.ftd.enterprise.vo.PhoneType;


public class PhoneVO extends BaseVO {
    String phoneNumber;
    String extension;
    PhoneType type;

    public PhoneVO(String phoneId) {
        super(phoneId);
    }

    public PhoneVO(PhoneType type, String phoneNumber, String extension) {
        this(type, phoneNumber);
        setExtension(extension);
    }

    public PhoneVO(PhoneType type, String phoneNumber) {
        setType(type);
        setPhoneNumber(phoneNumber);
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    private void setPhoneNumber(String phoneNumber) {
        if (valueChanged(this.phoneNumber, phoneNumber)) {
            setChanged(true);
            this.phoneNumber = phoneNumber;
        }
    }

    public String getExtension() {
        return extension;
    }

    private void setExtension(String extension) {
        if (valueChanged(this.extension, extension)) {
            setChanged(true);
            this.extension = extension;
        }
    }

    public PhoneType getType() {
        return type;
    }

    private void setType(PhoneType type) {
        if ((this.type == null) ||
                valueChanged(this.type.toString(), type.toString())) {
            setChanged(true);
            this.type = type;
        }
    }
}
