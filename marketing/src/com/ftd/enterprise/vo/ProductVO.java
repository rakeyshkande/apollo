package com.ftd.enterprise.vo;

public class ProductVO extends BaseVO {

    private String novatorId;
    private String name;
    private Float price;
    private String availability;

    public ProductVO() {
        super();
    }

    public ProductVO(String productId) {
        super((productId == null) ? productId : productId.toUpperCase());
    }
    
    public String getNovatorId() {
        return novatorId;
    }

    public void setNovatorId(String novatorId) {
        this.novatorId = novatorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }
}
