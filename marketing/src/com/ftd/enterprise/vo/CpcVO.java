package com.ftd.enterprise.vo;

public class CpcVO extends BaseVO {
    private String creditCardType;
    private String creditCardNumber;
    private String creditCardExpirationMonth;
    private String creditCardExpirationYear;

    public CpcVO(String creditCardType, String creditCardNumber,
        String creditCardExpirationMonth, String creditCardExpirationYear) {
        super(creditCardNumber);
        this.creditCardType = creditCardType;
        this.creditCardNumber = creditCardNumber;
        this.creditCardExpirationMonth = creditCardExpirationMonth;
        this.creditCardExpirationYear = creditCardExpirationYear;
    }

    public CpcVO(String creditCardType, String creditCardNumber,
        String creditCardExpirationDate) {
        super(creditCardNumber);
        this.creditCardType = creditCardType;
        this.creditCardNumber = creditCardNumber;

        this.creditCardExpirationMonth = creditCardExpirationDate.substring(0,
                creditCardExpirationDate.indexOf("."));
        this.creditCardExpirationYear = creditCardExpirationDate.substring(creditCardExpirationDate.indexOf(
                    ".") + 1, creditCardExpirationDate.length());
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getCreditCardExpirationMonth() {
        return creditCardExpirationMonth;
    }

    public String getCreditCardExpirationYear() {
        return creditCardExpirationYear;
    }

    public String getCreditCardExpirationDate() {
        return getCreditCardExpirationMonth() + "." +
        getCreditCardExpirationYear();
    }
    
    public void setCreditCardNumber(String ccNumber) {
        this.creditCardNumber = ccNumber;
    }
}
