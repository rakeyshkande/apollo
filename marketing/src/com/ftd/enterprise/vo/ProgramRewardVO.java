package com.ftd.enterprise.vo;

public class ProgramRewardVO extends PartnerProgramVO {
    private String calculationBasis;
    private Float points;
    private String rewardType;
    private String customerInfo;
    private String rewardName;
    private String participantIdLengthCheck;
    private String participantEmailCheck;
    private ProgramRewardRangeVO programRewardRangeVO;
    private Float maximumPoints;
    private String bonusCalculationBasis;
    private String bonusSeparateData;
    private Float bonusPoints;

    //    private ProgramRewardBonusVO programBonusVO;
    public ProgramRewardVO() {
        super();
    }

    public ProgramRewardVO(String programName) {
        super((programName == null) ? programName : programName.toUpperCase());
    }

    public String getCalculationBasis() {
        return calculationBasis;
    }

    public void setCalculationBasis(String calculationBasis) {
        this.calculationBasis = calculationBasis;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(String customerInfo) {
        this.customerInfo = customerInfo;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getParticipantIdLengthCheck() {
        return participantIdLengthCheck;
    }

    public void setParticipantIdLengthCheck(String participantIdLengthCheck) {
        this.participantIdLengthCheck = participantIdLengthCheck;
    }

    public String getParticipantEmailCheck() {
        return participantEmailCheck;
    }

    public void setParticipantEmailCheck(String participantEmailCheck) {
        this.participantEmailCheck = participantEmailCheck;
    }

    public ProgramRewardRangeVO getProgramRewardRangeVO() {
        return programRewardRangeVO;
    }

    public void setProgramRewardRangeVO(
        ProgramRewardRangeVO programRewardRangeVO) {
        this.programRewardRangeVO = programRewardRangeVO;
    }

    public Float getMaximumPoints() {
        return maximumPoints;
    }

    public void setMaximumPoints(Float maximumPoints) {
        this.maximumPoints = maximumPoints;
    }

    public String getBonusCalculationBasis() {
        return bonusCalculationBasis;
    }

    public void setBonusCalculationBasis(String bonusCalculationBasis) {
        this.bonusCalculationBasis = bonusCalculationBasis;
    }

    public String getBonusSeparateData() {
        return bonusSeparateData;
    }

    public void setBonusSeparateData(String bonusSeparateData) {
        this.bonusSeparateData = bonusSeparateData;
    }

    public Float getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(Float bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    //    public ProgramRewardBonusVO getProgramRewardBonusVO() {
    //        return programBonusVO;
    //    }
    //
    //    public void setProgramRewardBonusVO(ProgramRewardBonusVO programBonusVO) {
    //        this.programBonusVO = programBonusVO;
    //    }
}
