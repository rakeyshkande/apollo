package com.ftd.enterprise.vo;


/**
 * Value object that represents source type data.
 * @author JP Puzon
 */
public class SourceTypeVO extends BaseVO {
    public SourceTypeVO(String sourceType) {
        super(sourceType);
    }
}
