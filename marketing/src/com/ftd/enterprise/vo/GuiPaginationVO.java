package com.ftd.enterprise.vo;

import java.util.HashMap;
import java.util.Map;


public class GuiPaginationVO {
    public static String MAP_KEY_PAGE_NUMBER = "pageNumber";
    public static String MAP_KEY_PAGE_MAX_RECORD_COUNT = "scPageMaxRecordCount";
    public static String MAP_KEY_PAGE_COUNT_TOTAL = "pageCountTotal";
    public static String MAP_KEY_RECORD_COUNT_TOTAL = "recordCountTotal";
    public static String MAP_KEY_RECORD_NUMBER = "scRecordNumber";
    private Integer pageNumber;
    private Integer pageCountTotal;
    private Integer recordCountTotal;
    private Integer pageMaxRecordCount;
  private Integer recordNumber;

    public GuiPaginationVO(Integer pageNumber, Integer pageMaxRecordCount,
        Integer recordNumber, Integer recordCountTotal) {
        this.pageNumber = pageNumber;
        this.pageMaxRecordCount = pageMaxRecordCount;
        this.recordCountTotal = recordCountTotal;
        this.recordNumber = recordNumber;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }
    
    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageCountTotal() {
        int lastPage = 0;

        Integer result = new Integer(0);

        if ((getRecordCountTotal() != null) &&
                (getPageMaxRecordCount() != null) &&
                (getPageMaxRecordCount().intValue() != 0)) {

            result = new Integer(new Double(Math.ceil(getRecordCountTotal().doubleValue() / getPageMaxRecordCount().doubleValue())).intValue());
        }

        return result;
    }

    public Integer getRecordCountTotal() {
        return recordCountTotal;
    }
    
    public void setRecordCountTotal(Integer recordCountTotal) {
        this.recordCountTotal = recordCountTotal;
    }

    public Integer getPageMaxRecordCount() {
        return pageMaxRecordCount;
    }

    // This method allows XML representation of this class using
    // FTD DOMUtil.
    public HashMap getDataMap() {
        HashMap dataMap = new HashMap(4);
        dataMap.put(MAP_KEY_PAGE_NUMBER, getPageNumber().toString());
        dataMap.put(MAP_KEY_PAGE_MAX_RECORD_COUNT, getPageMaxRecordCount().toString());
        dataMap.put(MAP_KEY_PAGE_COUNT_TOTAL, getPageCountTotal().toString());
        dataMap.put(MAP_KEY_RECORD_COUNT_TOTAL, getRecordCountTotal().toString());
        dataMap.put(MAP_KEY_RECORD_NUMBER, getRecordNumber().toString());
        
        return dataMap;
    }

  public Integer getRecordNumber()
  {
    return recordNumber;
  }
}
