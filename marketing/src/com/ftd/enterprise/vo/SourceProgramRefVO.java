package com.ftd.enterprise.vo;

import java.util.Date;


public class SourceProgramRefVO extends BaseVO {
    private String sourceCode;
    private String programName;
    private Date startDate;

    public SourceProgramRefVO(String sourceCode, String programName,
        Date startDate) {
        this(sourceCode, programName, startDate, null);
    }

    public SourceProgramRefVO(String sourceCode, String programName,
        Date startDate, String id) {
        super(id);
        this.sourceCode = sourceCode;
        this.programName = programName;
        this.startDate = startDate;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public String getProgramName() {
        return programName;
    }

    public Date getStartDate() {
        return startDate;
    }
}
