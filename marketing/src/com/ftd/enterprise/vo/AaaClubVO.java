package com.ftd.enterprise.vo;

public class AaaClubVO extends BaseVO {
    private String clubName;

    public AaaClubVO(String clubCode, String clubName) {
        super(clubCode);
        setClubName(clubName);
    }
    
    public String getClubCode() {
        return getId();
    }

    public String getClubName() {
        return clubName;
    }

    private void setClubName(String clubName) {
        this.clubName = clubName;
    }
}
