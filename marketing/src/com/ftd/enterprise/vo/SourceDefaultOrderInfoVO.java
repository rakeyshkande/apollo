package com.ftd.enterprise.vo;

import java.lang.reflect.Field;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This VO contains the default order information that is defined on a Source
 * Code. The setters will store fields that are blank as null.
 */
public class SourceDefaultOrderInfoVO extends BaseVO {
	private String sourceCode;
	private String recptLocationType;
	private String recptBusinessName;
	private String recptLocationDetail;
	private String recptAddress;
	private String recptZipCode;
	private String recptCity;
	private String recptStateId;
	private String recptCountryId;
	private String recptPhone;
	private String recptPhoneExt;
	private String custFirstName;
	private String custLastName;
	private String custDaytimePhone;
	private String custDaytimePhoneExt;
	private String custEveningPhone;
	private String custEveningPhoneExt;
	private String custAddress;
	private String custZipCode;
	private String custCity;
	private String custStateId;
	private String custCountryId;
	private String custEmailAddress;
	private String requestedBy;

	public SourceDefaultOrderInfoVO() {
		super();
	}

	/**
	 * Generates an XML Document with the root node: sourceDefaultOrderInfoVO,
	 * and field values as child elements with text nodes
	 * 
	 * @return
	 */
	public Document toXMLDocument() {
		StringBuffer sb = new StringBuffer(1024);
		sb.append("<sourceDefaultOrderInfoVO>");

		// Marshall the fields to XML. Just use reflection since all the fields
		// are in this class as strings
		Field[] fields = getClass().getDeclaredFields();

		for (int iLoop = 0; iLoop < fields.length; iLoop++) {
			Object val = null;
			try {
				val = fields[iLoop].get(this);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			if (val == null) {
				sb.append("<").append(fields[iLoop].getName()).append(" />");
				continue;
			}

			sb.append("<").append(fields[iLoop].getName()).append(">");
			sb.append("<![CDATA[").append(val.toString()).append("]]>");
			sb.append("</").append(fields[iLoop].getName()).append(">");
		}

		sb.append("</sourceDefaultOrderInfoVO>");

		try {
			return DOMUtil.getDocument(sb.toString());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// ----------------------- Java bean Getters/Setters -------------------------------

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setRecptLocationType(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptLocationType = val;
	}

	public String getRecptLocationType() {
		return recptLocationType;
	}

	public void setRecptBusinessName(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptBusinessName = val;
	}

	public String getRecptBusinessName() {
		return recptBusinessName;
	}

	public void setRecptLocationDetail(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptLocationDetail = val;
	}

	public String getRecptLocationDetail() {
		return recptLocationDetail;
	}

	public void setRecptAddress(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptAddress = val;
	}

	public String getRecptAddress() {
		return recptAddress;
	}

	public void setRecptZipCode(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptZipCode = val;
	}

	public String getRecptZipCode() {
		return recptZipCode;
	}

	public void setRecptCity(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptCity = val;
	}

	public String getRecptCity() {
		return recptCity;
	}

	public void setRecptStateId(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptStateId = val;
	}

	public String getRecptStateId() {
		return recptStateId;
	}

	public void setRecptCountryId(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptCountryId = val;
	}

	public String getRecptCountryId() {
		return recptCountryId;
	}

	public void setRecptPhone(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptPhone = val;
	}

	public String getRecptPhone() {
		return recptPhone;
	}

	public void setRecptPhoneExt(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.recptPhoneExt = val;
	}

	public String getRecptPhoneExt() {
		return recptPhoneExt;
	}

	public void setCustFirstName(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custFirstName = val;
	}

	public String getCustFirstName() {
		return custFirstName;
	}

	public void setCustLastName(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custLastName = val;
	}

	public String getCustLastName() {
		return custLastName;
	}

	public void setCustDaytimePhone(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custDaytimePhone = val;
	}

	public String getCustDaytimePhone() {
		return custDaytimePhone;
	}

	public void setCustDaytimePhoneExt(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custDaytimePhoneExt = val;
	}

	public String getCustDaytimePhoneExt() {
		return custDaytimePhoneExt;
	}

	public void setCustEveningPhone(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custEveningPhone = val;
	}

	public String getCustEveningPhone() {
		return custEveningPhone;
	}

	public void setCustEveningPhoneExt(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custEveningPhoneExt = val;
	}

	public String getCustEveningPhoneExt() {
		return custEveningPhoneExt;
	}

	public void setCustAddress(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custAddress = val;
	}

	public String getCustAddress() {
		return custAddress;
	}

	public void setCustZipCode(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custZipCode = val;
	}

	public String getCustZipCode() {
		return custZipCode;
	}

	public void setCustCity(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}

		this.custCity = val;
	}

	public String getCustCity() {
		return custCity;
	}

	public void setCustStateId(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}

		this.custStateId = val;
	}

	public String getCustStateId() {
		return custStateId;
	}

	public void setCustCountryId(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}
		this.custCountryId = val;
	}

	public String getCustCountryId() {
		return custCountryId;
	}

	public void setCustEmailAddress(String val) {
		if (StringUtils.isBlank(val)) {
			val = null;
		}

		this.custEmailAddress = val;
	}

	public String getCustEmailAddress() {
		return custEmailAddress;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

}
