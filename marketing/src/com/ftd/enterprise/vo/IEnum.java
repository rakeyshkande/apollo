package com.ftd.enterprise.vo;


/**
 * Required behavior of all typesafe enums.
 */
public interface IEnum {
    public String toString();

    public int hashCode();

    public boolean equals(Object that);
}
