package com.ftd.enterprise.vo;

public class SubscribeStatus implements IEnum {
    public static final SubscribeStatus SUBSCRIBE = new SubscribeStatus("Subscribe");
    public static final SubscribeStatus UNSUBSCRIBE = new SubscribeStatus("Unsubscribe");
    private String type;

    private SubscribeStatus(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof SubscribeStatus) {
            return this.toString().equals(that.toString());
        }

        return false;
    } 
}