package com.ftd.enterprise.vo;

public class CostCenterVO extends BaseVO
{
  protected String partnerName;
  protected String concatId;
  protected String sourceCode;
  
  public CostCenterVO()
  {
    super();
  }
  
  public CostCenterVO(String partnerName, String concatId) 
  {
    this.concatId = concatId;
    this.partnerName = partnerName;
  }

  public String getConcatId() 
  {
    return concatId;
  }
  
  public String getPartnerName() 
  {
    return partnerName;
  }
  
  public String getSourceCode() 
  {
    return sourceCode;
  }
  
  public void setSourceCode(String sourceCode) 
  {
    this.sourceCode = sourceCode;
  }
}