package com.ftd.enterprise.vo;

public class ProductAttrExclusionVO extends BaseVO {

  private String productAttributeId;
  private String productAttributeName;
  private String productAttributeDesc;
  private String excludedFlag;

    public void setProductAttributeId(String productAttributeId) {
        this.productAttributeId = productAttributeId;
    }

    public String getProductAttributeId() {
        return productAttributeId;
    }

    public void setProductAttributeName(String productAttributeName) {
        this.productAttributeName = productAttributeName;
    }

    public String getProductAttributeName() {
        return productAttributeName;
    }

    public void setExcludedFlag(String excludedFlag) {
        this.excludedFlag = excludedFlag;
    }

    public String getExcludedFlag() {
        return excludedFlag;
    }

    public void setProductAttributeDesc(String productAttributeDesc) {
        this.productAttributeDesc = productAttributeDesc;
    }

    public String getProductAttributeDesc() {
        return productAttributeDesc;
    }
}
