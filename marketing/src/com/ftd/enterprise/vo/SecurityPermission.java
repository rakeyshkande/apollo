package com.ftd.enterprise.vo;

public class SecurityPermission implements IEnum {
    public static final SecurityPermission UPDATE = new SecurityPermission("Update");
    public static final SecurityPermission ADD = new SecurityPermission("Add");
    
    private String type;

    private SecurityPermission(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof SecurityPermission) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}