package com.ftd.enterprise.vo;

public class RecordStatus implements IEnum {
    public static final RecordStatus ACTIVE = new RecordStatus("Active");
    public static final RecordStatus INACTIVE = new RecordStatus("Inactive");
    private String type;

    private RecordStatus(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof RecordStatus) {
            return this.toString().equals(that.toString());
        }

        return false;
    } 
}