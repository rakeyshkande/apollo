package com.ftd.enterprise.vo;

import java.util.Calendar;


public class EmailVO extends BaseVO {
    private String emailAddress;
//    private String activeIndicator;
    private SubscribeStatus subscribeStatus;
    private Calendar subscribeDate;
//    private Calendar createdOn;
//    private Calendar updatedOn;
    private String updatedBy;
    private String companyId;

    public EmailVO(String emailId) {
        super(emailId);
    }

    public EmailVO() {
        super();
    }

    public void setEmailAddress(String emailAddress) {
        if (valueChanged(this.emailAddress, emailAddress)) {
            setChanged(true);
        }

        this.emailAddress = trim(emailAddress);
    }

    public String getEmailAddress() {
        return emailAddress;
    }

//    public void setActiveIndicator(String activeIndicator) {
//        if (valueChanged(this.activeIndicator, activeIndicator)) {
//            setChanged(true);
//        }
//
//        this.activeIndicator = activeIndicator;
//    }
//
//    public String getActiveIndicator() {
//        return activeIndicator;
//    }

    public void setSubscribeStatus(SubscribeStatus subscribeStatus) {
        if ((this.subscribeStatus == null) ||(valueChanged(this.subscribeStatus.toString(), subscribeStatus.toString()))) {
            setChanged(true);
        }

        this.subscribeStatus = subscribeStatus;
    }

    public SubscribeStatus getSubscribeStatus() {
        return subscribeStatus;
    }

    public void setSubscribeDate(Calendar subscribeDate) {
        if (valueChanged(this.subscribeDate, subscribeDate)) {
            setChanged(true);
        }

        this.subscribeDate = subscribeDate;
    }

    public Calendar getSubscribeDate() {
        return subscribeDate;
    }

//    public void setCreatedOn(Calendar createdOn) {
//        if (valueChanged(this.createdOn, createdOn)) {
//            setChanged(true);
//        }
//
//        this.createdOn = createdOn;
//    }
//
//    public Calendar getCreatedOn() {
//        return createdOn;
//    }
//
//    public void setUpdatedOn(Calendar updatedOn) {
//        if (valueChanged(this.updatedOn, updatedOn)) {
//            setChanged(true);
//        }
//
//        this.updatedOn = updatedOn;
//    }
//
//    public Calendar getUpdatedOn() {
//        return updatedOn;
//    }

    public void setUpdatedBy(String updatedBy) {
        if (valueChanged(this.updatedBy, updatedBy)) {
            setChanged(true);
        }

        this.updatedBy = trim(updatedBy);
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        if (valueChanged(this.companyId, companyId)) {
            setChanged(true);
        }

        this.companyId = trim(companyId);
    }

    /**
      * This method uses the Reflection API to generate an XML string that will be
      * passed back to the calling module.
      * The XML string will contain all the fields within this VO, including the
      * variables as well as (a collection of) ValueObjects.
      *
      * @param  None
      * @return XML string
     **/

    //    public String toXML() {
    //        StringBuffer sb = new StringBuffer();
    //
    //        try {
    //            sb.append("<EmailVO>");
    //
    //            Field[] fields = this.getClass().getDeclaredFields();
    //
    //            for (int i = 0; i < fields.length; i++) {
    //                //if the field retrieved was a list of VO
    //                if (fields[i].getType().equals(Class.forName("java.util.List"))) {
    //                    List list = (List) fields[i].get(this);
    //
    //                    if (list != null) {
    //                        for (int j = 0; j < list.size(); j++) {
    //                            XMLInterface xmlInt = (XMLInterface) list.get(j);
    //                            String sXmlVO = xmlInt.toXML();
    //                            sb.append(sXmlVO);
    //                        }
    //                    }
    //                } else {
    //                    //if the field retrieved was a VO
    //                    if (fields[i].getType().toString().matches("(?i).*vo")) {
    //                        XMLInterface xmlInt = (XMLInterface) fields[i].get(this);
    //                        String sXmlVO = xmlInt.toXML();
    //                        sb.append(sXmlVO);
    //                    }
    //                    //if the field retrieved was a Calendar object
    //                    else if (fields[i].getType().toString().matches("(?i).*calendar")) {
    //                        Date date;
    //                        String fDate = null;
    //
    //                        if (fields[i].get(this) != null) {
    //                            date = (((GregorianCalendar) fields[i].get(this)).getTime());
    //
    //                            SimpleDateFormat sdf = new SimpleDateFormat(
    //                                    "yyyy-MM-dd hh:mm:ss a zzz");
    //                            fDate = sdf.format(date).toString();
    //                        }
    //
    //                        sb.append("<" + fields[i].getName() + ">");
    //                        sb.append(fDate);
    //                        sb.append("</" + fields[i].getName() + ">");
    //                    } else {
    //                        sb.append("<" + fields[i].getName() + ">");
    //                        sb.append(fields[i].get(this));
    //                        sb.append("</" + fields[i].getName() + ">");
    //                    }
    //                }
    //            }
    //
    //            sb.append("</EmailVO>");
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //
    //        return sb.toString();
    //    }
}
