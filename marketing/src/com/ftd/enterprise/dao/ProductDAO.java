package com.ftd.enterprise.dao;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.enterprise.dao.UtilDAO;
import com.ftd.enterprise.vo.ProductFeedVO;

import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.ProductRequestVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import javax.xml.parsers.ParserConfigurationException;


/**
 * This Data Access Object is responsible for reading and persisting data to
 * the database.
 *
 * @author JP Puzon
 */
public class ProductDAO {
    protected static Log logger = LogFactory.getLog(ProductDAO.class.getName());
    protected final static String DB_SELECT_PRODUCT = "MARKETING.SELECT_PRODUCT_FEED";
    protected final static String DB_UPDATE_PRODUCT_EXTERNAL_FEED = "MARKETING.UPDATE_PRODUCT_EXTERNAL_FEED";

    // Note: The column lookups below are a SUBSET of the physical table.
    // There are several physical "legacy" columns which are not used or referenced
    // in this app.
    protected final static String DB_IN_PRODUCT_ID = "IN_PRODUCT_ID";
    protected final static String DB_IN_NOVATOR_ID = "IN_NOVATOR_ID";
    protected final static String DB_IN_BUY_URL = "IN_BUY_URL";
    protected final static String DB_IN_IMAGE_URL = "IN_IMAGE_URL";
    protected final static String DB_IN_IMPORTANT_PRODUCT_FLAG = "IN_IMPORTANT_PRODUCT_FLAG";
    protected final static String DB_IN_PROMOTIONAL_TEXT = "IN_PROMOTIONAL_TEXT";
    protected final static String DB_IN_SHIPPING_PROMOTIONAL_TEXT = "IN_SHIPPING_PROMOTIONAL_TEXT";
    protected final static String DB_IN_UPDATED_BY = "IN_UPDATED_BY";
    protected final static String DB_IN_START_POSITION = "IN_START_POSITION";
    protected final static String DB_IN_END_POSITION = "IN_END_POSITION";
    protected final static String DB_IN_SORT_BY = "IN_SORT_BY";
    protected final static String DB_OUT_CURSOR = "OUT_CURSOR";
    protected final static String DB_OUT_MAX_ROWS = "OUT_MAX_ROWS";
    protected final static Properties COLUMN_NAME_MAP = new Properties();

    static {
        // Compose a translation map for sorted queries.
        COLUMN_NAME_MAP.setProperty("productId", "product_id");
        COLUMN_NAME_MAP.setProperty("novatorId", "novator_id");
        COLUMN_NAME_MAP.setProperty("productName", "product_name");
        COLUMN_NAME_MAP.setProperty("price", "standard_price");
        COLUMN_NAME_MAP.setProperty("status", "status");
        COLUMN_NAME_MAP.setProperty("importantProductFlag", "important_product_flag");
    }

    /**
     * constructor to prevent clients from instantiating.
     */
    private ProductDAO() {
        super();
    }

    public static ProductRequestVO persistFeedProduct(
        ProductRequestVO productRequestVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap();
        ProductFeedVO productFeedVO = (ProductFeedVO) productRequestVO.getProductVO();

        if (productFeedVO == null) {
            throw new RuntimeException(
                "persistFeedProduct: ProductFeedVO input value can not be null.");
        }
   
        inputParams.put(DB_IN_PRODUCT_ID, productFeedVO.getId());
        inputParams.put(DB_IN_BUY_URL, productFeedVO.getBuyUrl());
        inputParams.put(DB_IN_IMAGE_URL,
            productFeedVO.getImageUrl());
        if (productFeedVO.getImportantProductFlag() != null && !productFeedVO.getImportantProductFlag().equals("0")) {
        inputParams.put(DB_IN_IMPORTANT_PRODUCT_FLAG, productFeedVO.getImportantProductFlag());
        }
        inputParams.put(DB_IN_PROMOTIONAL_TEXT, productFeedVO.getPromotionalText());
        inputParams.put(DB_IN_SHIPPING_PROMOTIONAL_TEXT, productFeedVO.getShippingPromotionalText());
        inputParams.put(DB_IN_UPDATED_BY,
            productRequestVO.getUserVO().getUserID());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PRODUCT_EXTERNAL_FEED,
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "persistFeedProduct: Could not update/insert product into database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        // Since the result is the source code itself, simply return the input
        return productRequestVO;
    }

    public static Document queryFeedProduct(ProductFeedVO productFeedVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        return queryFeedProduct(productFeedVO, recordNumberStart,
            maxRecordCount, resultRecordCount, null);
    }

    public static Document queryFeedProduct(ProductFeedVO productFeedVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
    	Document result = DOMUtil.getDefaultDocument();

        if (productFeedVO != null) {
            Map inputParams = new HashMap();

            UtilDAO.addSearchFilter(inputParams, DB_IN_PRODUCT_ID,
                productFeedVO.getId());
            UtilDAO.addSearchFilter(inputParams, DB_IN_NOVATOR_ID,
                productFeedVO.getNovatorId());

            // Specify the sort column.
            if ((sortBy != null) && (sortBy.length() > 0)) {
                UtilDAO.addSearchFilter(inputParams, DB_IN_SORT_BY,
                    COLUMN_NAME_MAP.getProperty(sortBy));
            }

            // Add the pagination metrics.
            // Note that we have to decrement 1 from the number of records
            // being requested since the storec proc API needs the start
            // and end record numbers.
            inputParams.put(DB_IN_START_POSITION, new Long(recordNumberStart));
            inputParams.put(DB_IN_END_POSITION,
                new Long((recordNumberStart + maxRecordCount) - 1));

            // Submit the request to the database.
            try {
                Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_PRODUCT,
                        inputParams, false);
                result = (Document) compositeResult.get(DB_OUT_CURSOR);

                // Record the total records returned.
                // Verify that the total record count variable
                // was populated.
                resultRecordCount.add(0,
                    UtilDAO.extractTotalRecordCount(compositeResult,
                        DB_OUT_MAX_ROWS));

                if (resultRecordCount.get(0) == null) {
                    throw new FtdDataException(
                        "queryProduct: Could not determine record count of result set.");
                }

                //DOMUtil.addSection(result, ((XMLDocument) compositeResult.get(RESULT_MAP_KEY_OUT_PARAMETERS)).getChildNodes());                
            } catch (SQLException e) {
                String errorMsg = "queryProduct: Could not obtain product from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        }

        return result;
    }
    
    public static void dispatchProductFeedMessage() throws Exception {
        MessageToken token = new MessageToken();
        token.setMessage("<?xml version=\"1.0\" encoding=\"utf-8\"?><event xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"event.xsd\"><event-name>LOAD-PRODUCT-DATA-FEED</event-name><context>PARTNER-REWARD</context><payload><![CDATA[PRODUCTFEED]]></payload></event>"); 
        token.setStatus("ACTIVATE-PRODUCT-FEED"); 
        
        Dispatcher dispatcher = Dispatcher.getInstance();
        InitialContext initContext = new InitialContext();
        dispatcher.dispatchTextMessage(initContext, token);
    }
}
