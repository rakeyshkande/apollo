package com.ftd.enterprise.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.enterprise.vo.CustomerVO;
import com.ftd.enterprise.vo.EmailVO;
import com.ftd.enterprise.vo.PhoneType;
import com.ftd.enterprise.vo.PhoneVO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.EntityLockedException;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.util.LockUtil;
import com.ftd.marketing.vo.CatalogRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;


/**
 * Persistence services for customer-related data.
 */
public class CustomerDAO {
    protected static Log logger = LogFactory.getLog(CustomerDAO.class.getName());

    //database:  stored procedure names - CUSTOMER_MAINT_PKG
    protected final static String DB_INSERT_CUSTOMER = "MARKETING.INSERT_CUSTOMER";
    protected final static String DB_INSERT_CUSTOMER_PHONES = "MARKETING.INSERT_CUSTOMER_PHONES";
    protected final static String DB_INSERT_DIRECT_MAIL = "MARKETING.INSERT_DIRECT_MAIL";
    protected final static String DB_INSERT_UNIQUE_EMAIL = "MARKETING.INSERT_UNIQUE_EMAIL";
    protected final static String DB_SELECT_SUBSCRIPTION_HISTORY = "MARKETING.SELECT_SUBSCRIPTION_HISTORY";
    protected final static String DB_SELECT_CUSTOMER_EMAIL = "MARKETING.SELECT_CUSTOMER_EMAIL";
    protected final static String DB_SELECT_EMAIL = "MARKETING.SELECT_EMAIL";
    protected final static String DB_UPDATE_EMAIL = "MARKETING.UPDATE_EMAIL";

    //database:  db INSERT CUSTOMER stored procedure input parms
    protected final static String DB_IN_FIRST_NAME = "IN_FIRST_NAME";
    protected final static String DB_IN_LAST_NAME = "IN_LAST_NAME";
    protected final static String DB_IN_BUSINESS_NAME = "IN_BUSINESS_NAME";
    protected final static String DB_IN_ADDRESS_1 = "IN_ADDRESS_1";
    protected final static String DB_IN_ADDRESS_2 = "IN_ADDRESS_2";
    protected final static String DB_IN_CITY = "IN_CITY";
    protected final static String DB_IN_STATE = "IN_STATE";
    protected final static String DB_IN_ZIP_CODE = "IN_ZIP_CODE";
    protected final static String DB_IN_COUNTRY = "IN_COUNTRY";
    protected final static String DB_IN_ADDRESS_TYPE = "IN_ADDRESS_TYPE";
    protected final static String DB_IN_PREFERRED_CUSTOMER = "IN_PREFERRED_CUSTOMER";
    protected final static String DB_IN_BUYER_INDICATOR = "IN_BUYER_INDICATOR";
    protected final static String DB_IN_RECIPIENT_INDICATOR = "IN_RECIPIENT_INDICATOR";
    protected final static String DB_IN_ORIGIN_ID = "IN_ORIGIN_ID";
    protected final static String DB_IN_FIRST_ORDER_DATE = "IN_FIRST_ORDER_DATE";
    protected final static String DB_IN_CREATED_BY = "IN_CREATED_BY";
    protected final static String DB_IN_BIRTHDAY = "IN_BIRTHDAY";
    protected final static String DB_IN_COUNTY = "IN_COUNTY";

    //database:  db UPDATE CUSTOMER PHONES stored procedure input parms
    protected final static String DB_IN_CUSTOMER_ID = "IN_CUSTOMER_ID";
    protected final static String DB_IN_PHONE_TYPE = "IN_PHONE_TYPE";
    protected final static String DB_IN_PHONE_NUMBER = "IN_PHONE_NUMBER";
    protected final static String DB_IN_EXTENSION = "IN_EXTENSION";
    protected final static String DB_IN_UPDATED_BY = "IN_UPDATED_BY";

    //database:  db UPDATE DIRECT MAIL stored procedure input parms
    protected final static String DB_IN_COMPANY_ID = "IN_COMPANY_ID";
    protected final static String DB_IN_SUBSCRIBE_STATUS = "IN_SUBSCRIBE_STATUS";

    //database:  db UPDATE EMAIL stored procedure input parms
    protected final static String DB_IN_EMAIL_ADDRESS = "IN_EMAIL_ADDRESS";
    protected final static String DB_IN_ACTIVE_INDICATOR = "IN_ACTIVE_INDICATOR";
    protected final static String DB_IN_MOST_RECENT_BY_COMPANY = "IN_MOST_RECENT_BY_COMPANY";
    protected final static String DB_IN_SUBSCRIBE_DATE = "IN_SUBSCRIBE_DATE";
    protected final static String DB_IN_IO_EMAIL_ID = "IN_IO_EMAIL_ID";
    protected final static String DB_IN_UPDATE_SCRUB = "IN_UPDATE_SCRUB";

    //database:  db SELECT SUBSCRIPTION HISTORY stored procedure input parms
    protected final static String DB_IN_START_POSITION = "IN_START_POSITION";
    protected final static String DB_IN_END_POSITION = "IN_END_POSITION";
    protected final static String DB_IN_SORT_BY = "IN_SORT_BY";
    protected final static String DB_OUT_CURSOR = "OUT_CURSOR";
    protected final static String DB_OUT_MAX_ROWS = "OUT_MAX_ROWS";
    protected final static Properties COLUMN_NAME_MAP = new Properties();

    static {
        // Compose a translation map for sorted queries.
        COLUMN_NAME_MAP.setProperty("companyId", "company_id");
        COLUMN_NAME_MAP.setProperty("target", "target");
        COLUMN_NAME_MAP.setProperty("updatedBy", "updated_by");
        COLUMN_NAME_MAP.setProperty("updatedOn", "updated_on");
    }

    /**
     * constructor to prevent clients from instantiating.
     */
    private CustomerDAO() {
        super();
    }

    /**
    * Persist the catalog subscription data.
    *
    * @throws java.lang.Exception
    * @throws com.ftd.marketing.exception.FtdDataException
    * @throws org.xml.sax.SAXException
    * @throws javax.xml.parsers.ParserConfigurationException
    * @throws java.io.IOException
    * @return a complete CatalogRequestVO object
    * @param catalogRequestVO - customer contact metrics
    */
    public static CatalogRequestVO insertCatalogSubscription(
        CatalogRequestVO catalogRequestVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Connection dbConn = UtilDAO.getDbConnection();

        try {
            // Insert records into the appropriate customer tables
            // based on referential integrity.
            logger.debug("insertCatalogSubscription: Inserting Customer....");
            insertCustomer(catalogRequestVO, dbConn);

            // Insert the customer phone records.
            // Daytime phone.
            logger.debug("insertCatalogSubscription: Inserting Day Phone....");
            insertPhone(catalogRequestVO, PhoneType.DAYTIME_PHONE_TYPE, dbConn);

            // Persist the evening phone number.
            logger.debug(
                "insertCatalogSubscription: Inserting Evening Phone....");
            insertPhone(catalogRequestVO, PhoneType.EVENING_PHONE_TYPE, dbConn);

            // Insert the direct mail record; no unique ID is returned.
            logger.debug("insertCatalogSubscription: Inserting Mail....");
            insertDirectMail(catalogRequestVO, dbConn);

            // Insert the email record.
            logger.debug("insertCatalogSubscription: Inserting EMail....");
            insertUniqueEmail(catalogRequestVO, dbConn);

            dbConn.commit();
        } catch (Exception e) {
            dbConn.rollback();
            throw e;
        } finally {
            dbConn.close();
        }

        return catalogRequestVO;
    }

    protected static Map insertCustomer(CatalogRequestVO catalogRequestVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        // Build db stored procedure input parms
        CustomerVO customerVO = catalogRequestVO.getCustomerVO();

        if (customerVO != null) {
            Map inputParams = new HashMap(9);
            inputParams.put(DB_IN_FIRST_NAME, customerVO.getFirstName());
            inputParams.put(DB_IN_LAST_NAME, customerVO.getLastName());
            inputParams.put(DB_IN_ADDRESS_1, customerVO.getAddress1());
            inputParams.put(DB_IN_ADDRESS_2, customerVO.getAddress2());
            inputParams.put(DB_IN_CITY, customerVO.getCity());
            inputParams.put(DB_IN_STATE, customerVO.getState());
            inputParams.put(DB_IN_ZIP_CODE, customerVO.getZipCode());
            inputParams.put(DB_IN_COUNTRY, customerVO.getCountry());
            inputParams.put(DB_IN_CREATED_BY,
                catalogRequestVO.getUserVO().getUserID());

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_INSERT_CUSTOMER,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "insertCustomer: Could not insert customer into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            // Record the customer id.
            if (result.get("OUT_CUSTOMER_ID") == null) {
                throw new FtdDataException(
                    "insertCustomer: Could not obtain the customerId value from database.");
            }

            catalogRequestVO.getCustomerVO().setId(result.get("OUT_CUSTOMER_ID")
                                                         .toString());

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        }

        return result;
    }

    protected static boolean isStringEmpty(String target) {
        return ((target == null) || (target.length() == 0)) ? true : false;
    }

    protected static Map insertPhone(CatalogRequestVO catalogRequestVO,
        PhoneType phoneType, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        // Insert the phone record.
        if (catalogRequestVO.getCustomerVO() != null) {
            PhoneVO phoneVO = catalogRequestVO.getCustomerVO().getPhone(phoneType);

            if ((phoneVO != null) && !isStringEmpty(phoneVO.getPhoneNumber())) {
                // Build db stored procedure input parms
                Map inputParams = new HashMap(5);
                inputParams.put(DB_IN_CUSTOMER_ID,
                    catalogRequestVO.getCustomerVO().getId());
                inputParams.put(DB_IN_PHONE_TYPE, phoneType.toString());
                inputParams.put(DB_IN_PHONE_NUMBER, phoneVO.getPhoneNumber());
                inputParams.put(DB_IN_UPDATED_BY,
                    catalogRequestVO.getUserVO().getUserID());

                String extension = phoneVO.getExtension();

                if (!isStringEmpty(phoneVO.getExtension())) {
                    inputParams.put(DB_IN_EXTENSION, phoneVO.getExtension());
                }

                // Insert the phone record.
                try {
                    result = (Map) UtilDAO.submitDbRequest(DB_INSERT_CUSTOMER_PHONES,
                            inputParams, dbConn);
                } catch (SQLException e) {
                    String errorMsg = "insertPhone: Could not insert customer phone into database.";
                    logger.error(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }

                // Record the new phone id.
                catalogRequestVO.getCustomerVO().getPhone(phoneType).setId(result.get(
                        "OUT_PHONE_ID").toString());
            }
        }

        return result;
    }

    protected static Map insertDirectMail(CatalogRequestVO catalogRequestVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        // Build db stored procedure input parms
        if ((catalogRequestVO.getCustomerVO() != null) &&
                (catalogRequestVO.getCompanyVO() != null)) {
            Map inputParams = new HashMap(3);
            inputParams.put(DB_IN_CUSTOMER_ID,
                catalogRequestVO.getCustomerVO().getId());
            inputParams.put(DB_IN_COMPANY_ID,
                catalogRequestVO.getCompanyVO().getId());
            inputParams.put(DB_IN_CREATED_BY,
                catalogRequestVO.getUserVO().getUserID());

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_INSERT_DIRECT_MAIL,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "insertDirectMail: Could not insert customer direct mail into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        }

        return result;
    }

    protected static Map insertUniqueEmail(CatalogRequestVO catalogRequestVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        if ((catalogRequestVO.getCompanyVO() != null) &&
                (catalogRequestVO.getCustomerVO() != null) &&
                (catalogRequestVO.getCustomerVO().getEmailVO() != null) &&
                !isStringEmpty(catalogRequestVO.getCustomerVO().getEmailVO()
                                                   .getEmailAddress())) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap(8);
            logger.debug("insertUniqueEmail: Setting parms....");
            inputParams.put(DB_IN_CUSTOMER_ID,
                catalogRequestVO.getCustomerVO().getId());
            inputParams.put(DB_IN_COMPANY_ID,
                catalogRequestVO.getCompanyVO().getId());
            inputParams.put(DB_IN_EMAIL_ADDRESS,
                catalogRequestVO.getCustomerVO().getEmailVO().getEmailAddress());
            inputParams.put(DB_IN_ACTIVE_INDICATOR, "Y");
            inputParams.put(DB_IN_SUBSCRIBE_STATUS, "Subscribe");
            inputParams.put(DB_IN_MOST_RECENT_BY_COMPANY, "Y");
            inputParams.put(DB_IN_UPDATED_BY,
                catalogRequestVO.getUserVO().getUserID());
            inputParams.put(DB_IN_UPDATE_SCRUB, "N");

            logger.debug("insertUniqueEmail: Done setting parms....");

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_INSERT_UNIQUE_EMAIL,
                        inputParams, dbConn);
                logger.debug("insertUniqueEmail: Insert result: " + result);
            } catch (SQLException e) {
                String errorMsg = "insertUniqueEmail: Could not insert customer email into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }

            //            catalogRequestVO.getCustomerVO().getEmailVO().setId(result.get(
            //                    "OUT_EMAIL_ID").toString());
        }

        return result;
    }

    public static Document querySubscriptionHistory(String customerId,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Document result = DOMUtil.getDefaultDocument();

        if (customerId != null) {
            Map inputParams = new HashMap(1);
            UtilDAO.addSearchFilter(inputParams, DB_IN_CUSTOMER_ID, customerId);

            // Specify the sort column.
            if ((sortBy != null) && (sortBy.length() > 0)) {
                UtilDAO.addSearchFilter(inputParams, DB_IN_SORT_BY,
                    COLUMN_NAME_MAP.getProperty(sortBy));
            }

            // Add the pagination metrics.
            // Note that we have to decrement 1 from the number of records
            // being requested since the storec proc API needs the start
            // and end record numbers.
            inputParams.put(DB_IN_START_POSITION, new Long(recordNumberStart));
            inputParams.put(DB_IN_END_POSITION,
                new Long((recordNumberStart + maxRecordCount) - 1));

            // Submit the request to the database.
            try {
                Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_SUBSCRIPTION_HISTORY,
                        inputParams, false);
                result = (Document) compositeResult.get(DB_OUT_CURSOR);

                // Record the total records returned.
                // Verify that the total record count variable
                // was populated.
                resultRecordCount.add(0,
                    UtilDAO.extractTotalRecordCount(compositeResult,
                        DB_OUT_MAX_ROWS));

                if (resultRecordCount.get(0) == null) {
                    throw new FtdDataException(
                        "querySubscriptionHistory: Could not determine record count of result set.");
                }
            } catch (SQLException e) {
                String errorMsg = "querySubscriptionHistory: Could not obtain subscription history from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        } else {
            throw new Exception(
                "querySubscriptionHistory: Input parameter customerId can not be null.");
        }

        return result;
    }

    public static void insertCustomerRecordLock(String customerId,
        String sessionId, UserInfo user)
        throws EntityLockedException, Exception {
        if ((customerId == null) || (sessionId == null) || (user == null)) {
            throw new Exception(
                "insertCustomerRecordLock: Input parameters customerId, sessionId, and userInfo can not be null.");
        }

        Connection dbConn = UtilDAO.getDbConnection();

        try {
            LockUtil lockUtil = new LockUtil(dbConn);

            lockUtil.obtainLock(MarketingConstants.CUSTOMER_LOCK_ENTITY_TYPE,
                customerId, user.getUserID(), sessionId);

            dbConn.commit();
        } catch (Exception e) {
            dbConn.rollback();
            throw e;
        } finally {
            dbConn.close();
        }
    }

    public static void updateEmail(EmailVO emailVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(5);
        inputParams.put(DB_IN_COMPANY_ID, emailVO.getCompanyId());
        inputParams.put(DB_IN_EMAIL_ADDRESS, emailVO.getEmailAddress());
        inputParams.put(DB_IN_SUBSCRIBE_STATUS,
            emailVO.getSubscribeStatus().toString());

        inputParams.put(DB_IN_SUBSCRIBE_DATE,
            new java.sql.Date(emailVO.getSubscribeDate().getTimeInMillis()));
        inputParams.put(DB_IN_UPDATED_BY, emailVO.getUpdatedBy());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_EMAIL,
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "updateEmail: Could not update email in database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
    }

    public static Document queryEmail(String emailAddress)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_EMAIL_ADDRESS, emailAddress);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_EMAIL,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "queryEmail: Could not obtain email data from database for source code";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document queryCustomerEmail(String emailAddress)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_EMAIL_ADDRESS, emailAddress);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_CUSTOMER_EMAIL,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "queryCustomerEmail: Could not obtain customer email data from database for source code";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }
}
