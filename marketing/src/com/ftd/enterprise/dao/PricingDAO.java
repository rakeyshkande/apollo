package com.ftd.enterprise.dao;

import com.ftd.enterprise.vo.DiscountTierVO;
import com.ftd.enterprise.vo.PricingCodeVO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdDataException;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class PricingDAO {
    protected static Log logger = LogFactory.getLog(PricingDAO.class.getName());

    //database:  stored procedure names
    protected final static String DB_SELECT_PRICING_CODE_DETAILS = "MARKETING.SELECT_PRICING_CODE_DETAILS";
    protected final static String DB_UPDATE_PRICING_CODE_HEADER = "MARKETING.UPDATE_PRICE_HEADER";
    protected final static String DB_INSERT_PRICING_CODE_DETAILS = "MARKETING.INSERT_PRICE_HEADER_DETAILS";
    protected final static String DB_DELETE_PRICING_CODE_DETAILS = "MARKETING.DELETE_PRICE_HEADER_DETAILS";

    //database:  db INSERT PRICE_HEADER stored procedure input parms
    protected final static String DB_IN_PRICE_HEADER_ID = "IN_PRICE_HEADER_ID";
    protected final static String DB_IN_DESCRIPTION = "IN_DESCRIPTION";

    //database:  db INSERT PRICE_HEADER DETAILS stored procedure input parms
    protected final static String DB_IN_MIN_DOLLAR_AMT = "IN_MIN_DOLLAR_AMT";
    protected final static String DB_IN_MAX_DOLLAR_AMT = "IN_MAX_DOLLAR_AMT";
    protected final static String DB_IN_DISCOUNT_TYPE = "IN_DISCOUNT_TYPE";
    protected final static String DB_IN_DISCOUNT_AMT = "IN_DISCOUNT_AMT";

    /**
     * constructor to prevent clients from instantiating.
     */
    private PricingDAO() {
        super();
    }

    public static Document queryPricingCodeDetails(String pricingCode)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(1);
        inputParams.put(DB_IN_PRICE_HEADER_ID, pricingCode);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_PRICING_CODE_DETAILS,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getPricingCodeDetails: Could not obtain pricing code details from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static PricingCodeVO persistPricingCode(PricingCodeVO pricingCodeVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Connection dbConn = UtilDAO.getDbConnection();

        try {
            // Insert records into the appropriate pricing tables
            // based on referential integrity.
            updatePriceCodeHeader(pricingCodeVO, dbConn);
            
            // Delete any existing child records in price_header_details.
            deletePriceCodeDetails(pricingCodeVO, dbConn);

            // Insert the discount tier details.
            insertPriceCodeDetails(pricingCodeVO, dbConn);

            dbConn.commit();
        } catch (Exception e) {
            dbConn.rollback();
            throw e;
        } finally {
            dbConn.close();
        }

        return pricingCodeVO;
    }

    protected static Map updatePriceCodeHeader(PricingCodeVO pricingCodeVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        if ((pricingCodeVO != null) && (pricingCodeVO.getId() != null)) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap();
            inputParams.put(DB_IN_PRICE_HEADER_ID, pricingCodeVO.getId());
            inputParams.put(DB_IN_DESCRIPTION, pricingCodeVO.getDescription());
            inputParams.put("IN_UPDATED_BY", pricingCodeVO.getUpdatedBy());
            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PRICING_CODE_HEADER,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "insertPriceCodeHeader: Could not insert pricing code header into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        }

        return result;
    }
    
    protected static Map deletePriceCodeDetails(PricingCodeVO pricingCodeVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        if ((pricingCodeVO != null) && (pricingCodeVO.getId() != null)) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap();
            inputParams.put(DB_IN_PRICE_HEADER_ID, pricingCodeVO.getId());
            inputParams.put("IN_UPDATED_BY", pricingCodeVO.getUpdatedBy());

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_DELETE_PRICING_CODE_DETAILS,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "deletePriceCodeDetails: Could not delete pricing code header details from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        }

        return result;
    }

    protected static void insertPriceCodeDetails(PricingCodeVO pricingCodeVO,
        Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        if ((pricingCodeVO != null) &&
                (pricingCodeVO.getDiscountTierCount() > 0)) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap();
            inputParams.put(DB_IN_PRICE_HEADER_ID, pricingCodeVO.getId());
//            inputParams.put(DB_IN_DISCOUNT_TYPE, pricingCodeVO.getDiscountType());

            for (Iterator i = pricingCodeVO.getDiscountTiers().iterator();
                    i.hasNext();) {
                DiscountTierVO discountTierVO = (DiscountTierVO) i.next();
                inputParams.put(DB_IN_MIN_DOLLAR_AMT,
                    discountTierVO.getMinDollarBound());
                inputParams.put(DB_IN_MAX_DOLLAR_AMT,
                    discountTierVO.getMaxDollarBound());
                inputParams.put(DB_IN_DISCOUNT_AMT,
                    discountTierVO.getDiscountAmount());
                inputParams.put(DB_IN_DISCOUNT_TYPE, pricingCodeVO.getDiscountType());
                inputParams.put(DB_IN_DISCOUNT_TYPE, pricingCodeVO.getDiscountType());
                inputParams.put("IN_CREATED_BY", pricingCodeVO.getUpdatedBy());
    
                logger.debug("insertPriceCodeDetails: Min=" +
                    discountTierVO.getMinDollarBound() + " Max=" +
                    discountTierVO.getMaxDollarBound() + " Discount=" +
                    discountTierVO.getDiscountAmount());

                // Submit the request to the database.
                try {
                    result = (Map) UtilDAO.submitDbRequest(DB_INSERT_PRICING_CODE_DETAILS,
                            inputParams, dbConn);
                } catch (SQLException e) {
                    String errorMsg = "insertPriceCodeDetails: Could not insert pricing code details into database.";
                    logger.error(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }
            } // end of FOR loop
        }
    }
}
