package com.ftd.enterprise.dao;

import com.ftd.marketing.exception.FtdDataException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;


public class SecurityDAO {
    protected static Log logger = LogFactory.getLog(SecurityDAO.class.getName());

    //database:  stored procedure names
    protected final static String DB_SELECT_USERS_BY_DEPARTMENT = "MARKETING.SELECT_USERS_BY_DEPARTMENT";
    protected final static String DB_SELECT_USERS_BY_ROLE = "MARKETING.SELECT_USERS_BY_ROLE"; 

    //database: stored procedure input parms
    protected final static String DB_IN_DEPARTMENT_ID = "IN_DEPARTMENT_ID";
    protected final static String DB_IN_ROLE_NAME = "IN_ROLE_NAME";

    /**
     * constructor to prevent clients from instantiating.
     */
    private SecurityDAO() {
        super();
    }

    public static Document getUsers(String departmentId)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(1);
        inputParams.put(DB_IN_DEPARTMENT_ID, departmentId);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_USERS_BY_DEPARTMENT,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getUsers: Could not obtain users from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }
    
    public static Document getUsersByRole(String roleName)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(1);
        inputParams.put(DB_IN_ROLE_NAME, roleName);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_USERS_BY_ROLE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getUsersByRole: Could not obtain users from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }
    
}
