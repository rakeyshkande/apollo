package com.ftd.enterprise.dao;

import com.ftd.marketing.exception.FtdDataException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;


/**
 * Used for obtaining reference and lookup codes that are universal to FTD.
 */
public class GlobalDAO {
    protected static Log logger = LogFactory.getLog(GlobalDAO.class.getName());
    public final static String DB_SELECT_ALL_STATE = "MARKETING.SELECT_ALL_STATE";
    public final static String DB_SELECT_ALL_COUNTRY = "MARKETING.SELECT_ALL_COUNTRY";
    public final static String DB_SELECT_ALL_PAYMENT_METHOD = "MARKETING.SELECT_ALL_PAYMENT_METHOD";
    public final static String DB_SELECT_ALL_SHIP_METHOD = "MARKETING.SELECT_ALL_SHIP_METHOD";
    public final static String DB_SELECT_ALL_COMPLIMENTARY_ADDON  = "MARKETING.SELECT_ALL_COMPLIMENTARY_ADDON";
    public final static String DB_SELECT_ALL_SOURCE_PARTNER_BIN = "MARKETING.SELECT_ALL_SOURCE_PARTNER_BIN";

    public final static String DB_SELECT_ALL_COMPANY = "MARKETING.SELECT_ALL_COMPANY";
    public final static String DB_SELECT_ALL_PRICING_CODE = "MARKETING.SELECT_ALL_PRICING_CODE";
    public final static String DB_SELECT_ALL_BONUS_TYPE = "MARKETING.SELECT_ALL_BONUS_TYPE";
    public final static String DB_SELECT_ALL_ORDER_SOURCE = "MARKETING.SELECT_ALL_ORDER_SOURCE";
    public final static String DB_SELECT_ALL_MILEAGE_CALC_SOURCE = "MARKETING.SELECT_ALL_MILEAGE_CALC_SOURCE";
    public final static String DB_SELECT_ALL_DISCOUNT_TYPE = "MARKETING.SELECT_ALL_DISCOUNT_TYPE";
    public final static String DB_SELECT_ALL_SERVICE_FEE_CODE = "MARKETING.SELECT_ALL_SERVICE_FEE_CODE";
    public final static String DB_SELECT_ALL_REDEMPTION_RATE = "MARKETING.SELECT_ALL_REDEMPTION_RATE";
    
    public final static String DB_SELECT_ALL_SOURCE_TYPE = "MARKETING.SELECT_ALL_SOURCE_TYPE";
    
    public final static String DB_SELECT_ALL_CALCULATION_BASIS = "MARKETING.SELECT_ALL_CALCULATION_BASIS";
    public final static String DB_SELECT_ALL_REWARD_TYPE = "MARKETING.SELECT_ALL_REWARD_TYPE";
    public final static String DB_SELECT_ALL_PROGRAM_TYPE = "MARKETING.SELECT_ALL_PROGRAM_TYPE";
    public final static String DB_SELECT_ALL_PARTNER = "MARKETING.SELECT_ALL_PARTNER";
    public final static String DB_SELECT_ALL_PARTNER_PROGRAM = "MARKETING.SELECT_ALL_PARTNER_PROGRAM";
    public final static String DB_SELECT_ALL_AAA_MEMBERS = "MARKETING.SELECT_ALL_AAA_MEMBERS";
    public final static String DB_SELECT_ALL_PROD_ATTRIBUTES = "MARKETING.SELECT_ALL_PROD_ATTRIBUTES";
    public final static String DB_SELECT_ALL_SRC_CODES_AND_MEMBER_LEVEL = "MARKETING.SELECT_ALL_SOURCE_MP_MEMBER_LEVEL_INFO";
    
    public final static String DB_SELECT_ALL_ADDRESS_TYPES = "MARKETING.SELECT_ALL_ADDRESS_TYPES";
    public final static String DB_SELECT_COUNTRY_LIST_OE = "MARKETING.SELECT_COUNTRY_LIST_ORDER_ENTRY";
    public final static String DB_SELECT_STATE_LIST_OE = "MARKETING.SELECT_STATE_LIST_ORDER_ENTRY";
    public final static String DB_SELECT_ALL_DELIVERY_FEE_CODE = "MARKETING.SELECT_ALL_DELIVERY_FEE_CODE";
    public final static String DB_SELECT_ALL_OSCAR_SCENARIO_GROUPS = "MARKETING.GET_OSCAR_SCENARIO_GROUPS";

    /**
     * constructor to prevent clients from instantiating.
     */
    private GlobalDAO() {
        super();
    }

    /**
    * get a list of codes
    *
    * @param none
    *
    * @return Document - list of source type codes
    *
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public static Document getAllCodeList(String dbProcName)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        Map inputParams = new HashMap(0);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(dbProcName,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getAllCodeList: Could not obtain " + dbProcName +
                " from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }
}
