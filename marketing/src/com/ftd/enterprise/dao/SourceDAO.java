package com.ftd.enterprise.dao;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.enterprise.vo.BinCheckPartnerVO;
import com.ftd.enterprise.vo.CpcVO;
import com.ftd.enterprise.vo.MpMemberLevelVO;
import com.ftd.enterprise.vo.ProductAttrExclusionVO;
import com.ftd.enterprise.vo.RecordStatus;
import com.ftd.enterprise.vo.SourceDefaultOrderInfoVO;
import com.ftd.enterprise.vo.SourceFloristPriorityVO;
import com.ftd.enterprise.vo.SourceProgramRefVO;
import com.ftd.enterprise.vo.SourceVO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.exception.FtdSecurityException;
import com.ftd.marketing.sourcecode.constants.SourceCodeConstants;
import com.ftd.marketing.sourcecode.model.Florist;
import com.ftd.marketing.sourcecode.model.SourceCodeRequest;
import com.ftd.marketing.sourcecode.model.UpdateSourceCodeAttributesRequest;
import com.ftd.marketing.vo.SourceProgramRequestVO;
import com.ftd.marketing.vo.SourceRequestVO;
import com.ftd.marketing.vo.SourceTypeRequestVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This Data Access Object is responsible for reading and persisting data to
 * the database.
 *
 * @author JP Puzon
 */
public class SourceDAO {
    protected static Log logger = LogFactory.getLog(SourceDAO.class.getName());
    protected final static String DB_SELECT_ALL_GROUPED_SOURCE_TYPE = "MARKETING.SELECT_ALL_GROUPED_SOURCE_TYPE";
    protected final static String DB_INSERT_SOURCE_TYPE = "MARKETING.INSERT_SOURCE_TYPE";
    protected final static String DB_UPDATE_SOURCE_MASTER = "MARKETING.UPDATE_SOURCE_MASTER";
    protected final static String DB_UPDATE_SOURCE_LEGACY_MAPPING = "MARKETING.UPDATE_SOURCE_LEGACY_MAPPING";
    protected final static String DB_SELECT_SOURCE = "MARKETING.SELECT_SOURCE";
    protected final static String DB_SELECT_SOURCE_BY_UPDATED_BY = "MARKETING.SELECT_SOURCE_BY_UPDATED_BY";

    protected final static String DB_SELECT_ALL_RESERVED_SOURCE = "MARKETING.SELECT_ALL_RESERVED_SOURCE";
    protected final static String DB_SELECT_CREATED_SOURCE_CODES_BY_RANGE = "MARKETING.SELECT_CREATED_SOURCE_CODES_BY_RANGE";
    protected final static String DB_SELECT_RESERVED_SOURCE_CODES_BY_RANGE = "MARKETING.SELECT_RESERVED_SOURCE_CODES_BY_RANGE";
    protected final static String DB_UPDATE_INSERT_SOURCE_MASTER_RESERVE = "MARKETING.UPDATE_INSERT_SOURCE_MASTER_RESERVE";
    protected final static String DB_UPDATE_SOURCE_MASTER_RESERVE = "MARKETING.UPDATE_SOURCE_MASTER_RESERVE";
    protected final static String DB_UPDATE_CPC_INFO = "MARKETING.UPDATE_CPC_INFO";
    protected final static String DB_SELECT_CPC_INFO = "MARKETING.SELECT_CPC_INFO";
    protected final static String DB_UPDATE_SOURCE_PROGRAM_REF = "MARKETING.UPDATE_SOURCE_PROGRAM_REF";
    protected final static String DB_INSERT_SOURCE_PROGRAM_REF = "MARKETING.INSERT_SOURCE_PROGRAM_REF";
    protected final static String DB_SELECT_SOURCE_PROGRAM_REF = "MARKETING.SELECT_SOURCE_PROGRAM_REF";
    protected final static String DB_SELECT_SOURCE_PROGRAM_REF_BY_SOURCE = "MARKETING.SELECT_SOURCE_PROGRAM_REF_BY_SOURCE";
    protected final static String DB_SELECT_SOURCE_PARTNER_BANKS = "MARKETING.SELECT_SOURCE_PARTNER_BANKS";
    protected final static String DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING = "MARKETING.UPDATE_SOURCE_PARTNER_BIN_MAPPING";
    protected final static String DB_SELECT_PARTNER_BIN_MAPPING = "MARKETING.SELECT_PARTNER_BIN_MAPPING";
    protected final static String DB_SELECT_PRODUCT_ATTR_EXCLUSIONS = "MARKETING.SELECT_PROD_ATTR_EXCLUSIONS";
    protected final static String DB_REMOVE_PRODUCT_ATTR_EXLUSIONS = "MARKETING.REMOVE_PROD_ATTR_EXCLUSIONS";
    protected final static String DB_INSERT_PRODUCT_ATTR_EXLUSIONS = "MARKETING.INSERT_PROD_ATTR_EXCLUSIONS";
    protected final static String DB_GET_SOURCE_FLORIST_PRIORITY = "MARKETING.GET_SOURCE_FLORIST_PRIORITY";
    protected final static String DB_GET_SOURCE_FLORIST_PRIORITY_LIST = "MARKETING.GET_SOURCE_FLORIST_PRIORITY_LIST";
    protected final static String DB_INSERT_SOURCE_FLORIST_PRIORITY = "MARKETING.INSERT_SOURCE_FLORIST_PRIORITY";
    protected final static String DB_DELETE_SOURCE_FLORIST_PRIORITY = "MARKETING.DELETE_SOURCE_FLORIST_PRIORITY";
    protected final static String DB_VIEW_FLORIST_MASTER = "MARKETING.VIEW_FLORIST_MASTER";
    protected final static String DB_UPDATE_SOURCE_MP_MEMBER_LEVEL_INFO = "MARKETING.UPDATE_SOURCE_MP_MEMBER_LEVEL_INFO";  
    protected final static String DB_UPDATE_SC_ATTRIBUTES = "MARKETING.UPDATE_SC_ATTRIBUTES";


    // Note: The column lookups below are a SUBSET of the physical table.
    // There are several physical "legacy" columns which are not used or referenced
    // in this app.
    protected final static String DB_IN_SOURCE_TYPE = "IN_SOURCE_TYPE";
    protected final static String DB_IN_CREATED_BY = "IN_CREATED_BY";
    protected final static String DB_IN_UPDATED_BY = "IN_UPDATED_BY";
    protected final static String DB_IN_UPDATED_ON_START = "IN_UPDATED_ON_START";
    protected final static String DB_IN_UPDATED_ON_END = "IN_UPDATED_ON_END";
    protected final static String DB_IN_SOURCE_CODE = "IN_SOURCE_CODE";
    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
    protected final static String DB_IN_DESCRIPTION = "IN_DESCRIPTION";
    protected final static String DB_IN_SNH_ID = "IN_SNH_ID";
    protected final static String DB_IN_PRICE_HEADER_ID = "IN_PRICE_HEADER_ID";
    protected final static String DB_IN_START_DATE = "IN_START_DATE";
    protected final static String DB_IN_END_DATE = "IN_END_DATE";
    protected final static String DB_IN_PAYMENT_METHOD_ID = "IN_PAYMENT_METHOD_ID";
    protected final static String DB_IN_DEFAULT_SOURCE_CODE_FLAG = "IN_DEFAULT_SOURCE_CODE_FLAG";
//    protected final static String DB_IN_BILLING_INFO_PROMPT = "IN_BILLING_INFO_PROMPT";
//    protected final static String DB_IN_BILLING_INFO_LOGIC = "IN_BILLING_INFO_LOGIC";
    protected final static String DB_IN_EXTERNAL_CALL_CENTER = "IN_EXTERNAL_CALL_CENTER";
    protected final static String DB_IN_HIGHLIGHT_DESCRIPTION_FLAG = "IN_HIGHLIGHT_DESCRIPTION_FLAG";
    protected final static String DB_IN_DISCOUNT_ALLOWED_FLAG = "IN_DISCOUNT_ALLOWED_FLAG";
    protected final static String DB_IN_BIN_NUMBER_CHECK_FLAG = "IN_BIN_NUMBER_CHECK_FLAG";
    protected final static String DB_IN_ORDER_SOURCE = "IN_ORDER_SOURCE";
    protected final static String DB_IN_JCPENNEY_FLAG = "IN_JCPENNEY_FLAG";
    protected final static String DB_IN_COMMENT_TEXT = "IN_COMMENT_TEXT";
    protected final static String DB_IN_SOURCE_PROGRAM_REF_ID = "IN_SOURCE_PROGRAM_REF_ID";
    protected final static String DB_IN_PARTNER_BANK_ID = "IN_PARTNER_BANK_ID";
    //    protected final static String DB_IN_OE_DEFAULT_FLAG = "IN_OE_DEFAULT_FLAG";
    protected final static String DB_IN_COMPANY_ID = "IN_COMPANY_ID";
    protected final static String DB_IN_SEND_TO_SCRUB = "IN_SEND_TO_SCRUB";
    protected final static String DB_IN_FRAUD_FLAG = "IN_FRAUD_FLAG";
    protected final static String DB_IN_ENABLE_LP_PROCESSING = "IN_ENABLE_LP_PROCESSING";
    protected final static String DB_IN_EMERGENCY_TEXT_FLAG = "IN_EMERGENCY_TEXT_FLAG";
    protected final static String DB_IN_REQUIRES_DELIVERY_CONFIRMATION = "IN_REQUIRES_DELIVERY_CONFIRMATION";
    protected final static String DB_IN_WEBLOYALTY_FLAG = "IN_WEBLOYALTY_FLAG";
    protected final static String DB_IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
    protected final static String DB_IN_PROMOTION_CODE = "IN_PROMOTION_CODE";
    protected final static String DB_IN_BONUS_PROMOTION_CODE = "IN_BONUS_PROMOTION_CODE";
    protected final static String DB_IN_REQUESTED_BY = "IN_REQUESTED_BY";
    protected final static String DB_IN_OVER_21_FLAG = "IN_OVER_21_FLAG";
    protected final static String DB_IN_RELATED_SOURCE_CODE = "IN_RELATED_SOURCE_CODE";
    protected final static String DB_IN_PROGRAM_WEBSITE_URL = "IN_PROGRAM_WEBSITE_URL";
    protected final static String DB_IN_IOTW_FLAG = "IN_IOTW_FLAG";
    protected final static String DB_IN_INVOICE_PASSWORD = "IN_INVOICE_PASSWORD";
    protected final static String DB_IN_BILLING_INFO_FLAG = "IN_BILLING_INFO_FLAG";
    protected final static String DB_IN_GIFT_CERTIFICATE_FLAG = "IN_GIFT_CERTIFICATE_FLAG";
    protected final static String DB_IN_START_POSITION = "IN_START_POSITION";
    protected final static String DB_IN_END_POSITION = "IN_END_POSITION";
    protected final static String DB_IN_SORT_BY = "IN_SORT_BY";
    protected final static String DB_IN_SOURCE_CODE_BEGIN = "IN_SOURCE_CODE_BEGIN";
    protected final static String DB_IN_SOURCE_CODE_END = "IN_SOURCE_CODE_END";
    protected final static String DB_IN_STATUS = "IN_STATUS";
    protected final static String DB_IN_SOURCE_CODE_ID = "IN_SOURCE_CODE_ID";
    protected final static String DB_IN_CREDIT_CARD_TYPE = "IN_CREDIT_CARD_TYPE";
    protected final static String DB_IN_CREDIT_CARD_NUMBER = "IN_CREDIT_CARD_NUMBER";
    protected final static String DB_IN_CREDIT_CARD_EXPIRATION_DATE = "IN_CREDIT_CARD_EXPIRATION_DATE";
    protected final static String DB_IN_MP_REDEMPTION_RATE_ID = "IN_MP_REDEMPTION_RATE_ID";
    protected final static String DB_IN_ADD_ON_FREE_ID = "IN_ADD_ON_FREE_ID";
    protected final static String DB_IN_BIN_PROCESSING_ACTIVE_FLAG = "IN_BIN_PROC_ACTIVE_FLAG";
    protected final static String DB_IN_PRODUCT_ATTRIBUTE_ID = "IN_PRODUCT_ATTR_RESTR_ID";
    protected final static String DB_IN_DISPLAY_SERVICE_FEE_CODE = "IN_DISPLAY_SERVICE_FEE_CODE";
    protected final static String DB_IN_DISPLAY_SHIPPING_FEE_CODE = "IN_DISPLAY_SHIPPING_FEE_CODE";
    protected final static String DB_IN_FLORIST_ID = "IN_FLORIST_ID";
    protected final static String DB_IN_PRIORITY = "IN_PRIORITY";
    protected final static String DB_IN_PRIMARY_BACKUP_RWD_FLAG = "IN_PRIMARY_BACKUP_RWD_FLAG";
    protected final static String DB_IN_ALLOW_FREE_SHIPPING_FLAG = "IN_ALLOW_FREE_SHIPPING_FLAG";
    
    protected final static String DB_IN_APPLY_SURCHARGE_CODE = "IN_APPLY_SURCHARGE_CODE";
    protected final static String DB_IN_SURCHARGE_AMOUNT = "IN_SURCHARGE_AMOUNT";
    protected final static String DB_IN_SURCHARGE_DESCRIPTION = "IN_SURCHARGE_DESCRIPTION";
    protected final static String DB_IN_DISPLAY_SURCHARGE = "IN_DISPLAY_SURCHARGE";
    protected final static String DB_IN_MP_MEMBER_LEVEL_ID = "IN_MP_MEMBER_LEVEL_ID";
    
    protected final static String DB_OUT_CURSOR = "OUT_CURSOR";
    protected final static String DB_OUT_MAX_ROWS = "OUT_MAX_ROWS";
    protected final static String DB_OUT_STATUS = "OUT_STATUS";
    protected final static String DB_OUT_MESSAGE = "OUT_MESSAGE";
    protected final static Properties COLUMN_NAME_MAP = new Properties();
    
    // Defect - 8276 - Same Day Upcharge - Iteration 1 changes
    protected final static String DB_IN_SAME_DAY_UPCHARGE = "IN_SAME_DAY_UPCHARGE";
    protected final static String DB_IN_DISPLAY_SAME_DAY_UPCHARGE = "IN_DISPLAY_SAME_DAY_UPCHARGE"; 
    
    protected final static String DB_IN_MORNING_DELIVERY_FLAG = "IN_MORNING_DELIVERY_FLAG";
    protected final static String DB_IN_CALCULATE_TAX_FLAG = "IN_CALCULATE_TAX_FLAG";
    protected final static String DB_IN_MERCH_AMT_FULL_REFUND_FLAG = "IN_MERCH_AMT_FULL_REFUND_FLAG";
    protected final static String DB_IN_SHIPPING_CARRIER_FALG = "IN_SHIPPING_CARRIER_FALG";
    protected final static String DB_IN_MORNING_DELIVERY_TO_FS_MEMBERS = "IN_MORNING_DELIVERY_FS_MEMBERS";
    protected final static String DB_IN_DELIVERY_FEE_ID = "IN_DELIVERY_FEE_ID";
    
    //Automated Promotion Engine input parameters
    protected final static String DB_IN_AUTO_PROMO_ENGINE_FLAG = "IN_AUTO_PROMO_ENGINE_FLAG";
    protected final static String DB_IN_APE_PRODUCT_CATALOG = "IN_APE_PRODUCT_CATALOG";
    static {
        // Compose a translation map for sorted queries.
        COLUMN_NAME_MAP.setProperty("sourceCode", "source_code");
        COLUMN_NAME_MAP.setProperty("description", "description");
        COLUMN_NAME_MAP.setProperty("sourceType", "source_type");
        COLUMN_NAME_MAP.setProperty("updatedBy", "updated_by");
        COLUMN_NAME_MAP.setProperty("updatedOn", "updated_on");
    }
    
    // 13046 - New statements to get source code detail, update default email program mapping, generate the sympathy source code.
    protected final static String GET_SOURCE_CODE_DETAIL = "MARKETING.GET_SOURCE_CODE_DETAIL";
    protected final static String DB_INS_UPD_PRGM_TO_SRC_MAPPING = "MARKETING.INST_UPD_PROG_SRC_MAPPING";
    
    // 15313 - OSCAR fields
    protected final static String DB_IN_OSCAR_SELECTION_FLAG = "IN_OSCAR_SELECTION_FLAG";
    protected final static String DB_IN_OSCAR_SCENARIO_GROUP_ID = "IN_OSCAR_SCENARIO_GROUP_ID";
    
  //Sympathy Controls
    protected final static String DB_IN_FUNERAL_CEMETERY_LOC_CHK = "IN_FUNERAL_CEMETERY_LOC_CHK";
    protected final static String DB_IN_HOSPITAL_LOC_CHCK = "IN_HOSPITAL_LOC_CHCK";
    protected final static String DB_IN_FUNERAL_CEMETERY_LEAD_TIME_CHK = "IN_SYMPATHY_LEAD_TIME_CHK";
    protected final static String DB_IN_FUNERAL_CEMETERY_LEAD_TIME = "IN_SYMPATHY_LEAD_TIME";
    protected final static String DB_IN_BO_HRS_MON_FRI_START = "IN_BO_HRS_MON_FRI_START";
    protected final static String DB_IN_BO_HRS_MON_FRI_END = "IN_BO_HRS_MON_FRI_END";
    protected final static String DB_IN_BO_HRS_SAT_START = "IN_BO_HRS_SAT_START";
    protected final static String DB_IN_BO_HRS_SAT_END = "IN_BO_HRS_SAT_END";
    protected final static String DB_IN_BO_HRS_SUN_START = "IN_BO_HRS_SUN_START";
    protected final static String DB_IN_BO_HRS_SUN_END = "IN_BO_HRS_SUN_END";
    protected final static String DB_IN_LEGACY_ID = "IN_LEGACY_ID";
    
    
    protected final static String DB_IN_RECPT_ADDRESS =  "IN_RECPT_ADDRESS";
    protected final static String DB_IN_RECPT_CITY = "IN_RECPT_CITY";
    protected final static String DB_IN_RECPT_STATE = "IN_RECPT_STATE";
    protected final static String DB_IN_RECPT_COUNTRY = "IN_RECPT_COUNTRY";
    protected final static String DB_IN_RECPT_PHONE_NUMBER = "IN_RECPT_PHONE_NUMBER";
    protected final static String DB_IN_RECPT_ZIP_CODE = "IN_RECPT_ZIP_CODE";
    
    //SGC-2 Same Day Upcharge FS Member Controls
    
    protected final static String DB_IN_SAME_DAY_UPCHARGE_FS = "IN_SAME_DAY_UPCHARGE_FS";


    /**
     * constructor to prevent clients from instantiating.
     */
    private SourceDAO() {
        super();
    }

    public static CachedResultSet queryAllGroupedSourceTypes()
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        // Build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        Map inputParams = new HashMap(0);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ALL_GROUPED_SOURCE_TYPE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getAllGroupedSourceTypes: Could not obtain grouped source types from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static CachedResultSet queryAllReservedSourceCodes()
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        // Build db stored procedure input parms
        // *****  stored procedure takes no input parms  *****
        Map inputParams = new HashMap(0);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_ALL_RESERVED_SOURCE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getAllReservedSources: Could not obtain all reserved sources from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document queryReservedSourceCodes(String sourceCodeBegin,
        String sourceCodeEnd)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Document result = DOMUtil.getDefaultDocument();

        Map inputParams = new HashMap(2);

        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_BEGIN,
            sourceCodeBegin);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_END,
            sourceCodeEnd);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_RESERVED_SOURCE_CODES_BY_RANGE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "getReservedSourceCodes: Could not obtain reserved sources from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document queryReservedSourceCode(String sourceCode)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        return queryReservedSourceCodes(sourceCode, null);

        //        XMLDocument result = new XMLDocument();
        //
        //        Map inputParams = new HashMap(1);
        //
        //        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_BEGIN, sourceCode);
        //
        //        // Submit the request to the database.
        //        try {
        //            return (XMLDocument) UtilDAO.submitDbRequest(DB_SELECT_RESERVED_SOURCE_CODES_BY_RANGE,
        //                inputParams, false);
        //        } catch (SQLException e) {
        //            String errorMsg = "queryReservedSourceCode: Could not obtain reserved source from database.";
        //            logger.error(errorMsg, e);
        //            throw new FtdDataException(errorMsg, e);
        //        }
    }

    public static Document queryCreatedSourceCodes(String sourceCodeBegin,
        String sourceCodeEnd)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Document result = DOMUtil.getDefaultDocument();

        Map inputParams = new HashMap(2);

        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_BEGIN,
            sourceCodeBegin);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_END,
            sourceCodeEnd);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_CREATED_SOURCE_CODES_BY_RANGE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg = "queryCreatedSourceCodes: Could not obtain created sources from database.";
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document queryCreatedSourceCode(String sourceCode)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        return queryCreatedSourceCodes(sourceCode, null);

        //        XMLDocument result = new XMLDocument();
        //
        //        Map inputParams = new HashMap(1);
        //
        //        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_BEGIN, sourceCode);
        //
        //        // Submit the request to the database.
        //        try {
        //            return (XMLDocument) UtilDAO.submitDbRequest(DB_SELECT_CREATED_SOURCE_CODES_BY_RANGE,
        //                inputParams, false);
        //        } catch (SQLException e) {
        //            String errorMsg = "queryReservedSourceCode: Could not obtain reserved source from database.";
        //            logger.error(errorMsg, e);
        //            throw new FtdDataException(errorMsg, e);
        //        }
    }

    /**
    * add an entry to the source type table.
    *     1. build db stored procedure input parms
    *     2. get db data
    *
    * @param String  - source type code
    *
    * @return SourceVO - value object with the database-assigned ID.
    *
    * @throws IOException
    * @throws ParserConfigurationException
    * @throws SAXException
    * @throws SQLException
    */
    public static SourceTypeRequestVO persistSourceType(
        SourceTypeRequestVO sourceTypeRequestVO)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_SOURCE_TYPE,
            sourceTypeRequestVO.getSourceTypeVO().getId());
        inputParams.put(DB_IN_CREATED_BY,
            sourceTypeRequestVO.getUserVO().getUserID());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_TYPE,
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "persistSourceType: Could not insert source type into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        // Since the result is the source code itself, simply return the input
        return sourceTypeRequestVO;
    }

    public static void persistReservedSourceCodes(SourceVO sourceVO,
        RecordStatus recordStatus, String sourceCodeStart, String sourceCodeEnd)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        persistReservedSourceCodes(sourceVO, recordStatus, sourceCodeStart,
            sourceCodeEnd, null);
    }

    public static void updateSourceCode(SourceVO sourceVO,
        RecordStatus recordStatus)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        updateSourceCodes(sourceVO, recordStatus, sourceVO.getId(),
            null, null);
    }

    public static void updateSourceCodes(SourceVO sourceVO,
        RecordStatus recordStatus, String sourceCodeStart, String sourceCodeEnd)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        updateSourceCodes(sourceVO, recordStatus, sourceCodeStart,
            sourceCodeEnd, null);
    }

    public static void updateSourceCodes(SourceVO sourceVO,
        RecordStatus recordStatus, String sourceCodeStart,
        String sourceCodeEnd, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(5);
        inputParams.put(DB_IN_SOURCE_CODE_BEGIN, sourceCodeStart);
        inputParams.put(DB_IN_SOURCE_CODE_END, sourceCodeEnd);
        inputParams.put(DB_IN_STATUS, recordStatus.toString());

//        String sourceType = sourceVO.getSourceType();
//        String description = sourceVO.getDescription();

        // Special RELEASE handling of dummy values since these columns are not nullable
        // on the database.
//        if (recordStatus == RecordStatus.INACTIVE) {
//            sourceType = "NONE";
//            description = "released";
//        }

        inputParams.put(DB_IN_SOURCE_TYPE, sourceVO.getSourceType());
        inputParams.put(DB_IN_DESCRIPTION, sourceVO.getDescription());
        inputParams.put("IN_UPDATED_BY", sourceVO.getUpdatedBy());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            if (dbConn != null) {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER_RESERVE,
                        inputParams, dbConn);
            } else {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER_RESERVE,
                        inputParams, true);
            }
        } catch (SQLException e) {
            String errorMsg = "persistReservedSourceCodes: Could not insert reserved source codes into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
    }

    public static void persistReservedSourceCodes(SourceVO sourceVO,
        RecordStatus recordStatus, String sourceCodeStart,
        String sourceCodeEnd, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(5);
        inputParams.put(DB_IN_SOURCE_CODE_BEGIN, sourceCodeStart);
        inputParams.put(DB_IN_SOURCE_CODE_END, sourceCodeEnd);
        inputParams.put(DB_IN_STATUS, recordStatus.toString());

//        String sourceType = sourceVO.getSourceType();
//        String description = sourceVO.getDescription();

        // Special RELEASE handling of dummy values since these columns are not nullable
        // on the database.
//        if (recordStatus == RecordStatus.INACTIVE) {
//            sourceType = "NONE";
//            description = "released";
//        }

        inputParams.put(DB_IN_SOURCE_TYPE, sourceVO.getSourceType());
        inputParams.put(DB_IN_DESCRIPTION, sourceVO.getDescription());
        inputParams.put("IN_UPDATED_BY", sourceVO.getUpdatedBy() );

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            if (dbConn != null) {
               result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_INSERT_SOURCE_MASTER_RESERVE,
            		   inputParams, dbConn);
            } else {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_INSERT_SOURCE_MASTER_RESERVE,
                		inputParams, true);
            }
        } catch (SQLException e) {
            String errorMsg = "persistReservedSourceCodes: Could not insert reserved source codes into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
       String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
    }

    public static void persistReservedSourceCode(SourceVO sourceVO,
        RecordStatus recordStatus)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        persistReservedSourceCodes(sourceVO, recordStatus, sourceVO.getId(),
            null, null);
    }

    protected static void persistReservedSourceCode(SourceVO sourceVO,
        RecordStatus recordStatus, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        persistReservedSourceCodes(sourceVO, recordStatus, sourceVO.getId(),
            null, dbConn);
    }

    //    protected static void persistReservedSourceCode(SourceVO sourceVO,
    //        RecordStatus recordStatus, Connection dbConn)
    //        throws IOException, ParserConfigurationException, SAXException,
    //            FtdDataException, Exception {
    //        // Build db stored procedure input parms
    //        Map inputParams = new HashMap(4);
    //        inputParams.put(DB_IN_SOURCE_CODE_BEGIN, sourceVO.getId());
    //        inputParams.put(DB_IN_SOURCE_TYPE, sourceVO.getSourceType());
    //        inputParams.put(DB_IN_DESCRIPTION, sourceVO.getDescription());
    //        inputParams.put(DB_IN_STATUS, recordStatus.toString());
    //
    //        // Submit the request to the database.
    //        Map result = new HashMap(0);
    //
    //        try {
    //            if (dbConn != null) {
    //                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER_RESERVE,
    //                        inputParams, dbConn);
    //            } else {
    //                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER_RESERVE,
    //                        inputParams, true);
    //            }
    //        } catch (SQLException e) {
    //            String errorMsg = "persistReservedSourceCode: Could not insert reserved source code into database.";
    //            logger.warn(errorMsg, e);
    //            throw new FtdDataException(errorMsg, e);
    //        }
    //
    //        // Check the return code from the stored procedure.
    //        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
    //
    //        if ((status != null) && status.equals("N")) {
    //            throw new FtdDataException((String) result.get(
    //                    MarketingConstants.DB_MESSAGE_PARAM));
    //        }
    //    }
    public static SourceRequestVO persistCompositeSource(
        SourceRequestVO sourceRequestVO)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Connection dbConn = UtilDAO.getDbConnection();

        try {
            // Before inserting the complete source record,
            // insert the reserve record to ensure that this
            // source code can NEVER be released.
            // Note that if the code is already reserved,
            // we proceed with the insertion.
            persistReservedSourceCode(sourceRequestVO.getSourceVO(),
                RecordStatus.ACTIVE, dbConn);

            // Insert records into the appropriate partner program tables
            // based on referential integrity.
            persistSource(sourceRequestVO, dbConn);
            persistCpc(sourceRequestVO, dbConn);

            dbConn.commit();
        } catch (Exception e) {
            dbConn.rollback();
            throw e;
        } finally {
            dbConn.close();
        }

        return sourceRequestVO;
    }

    protected static SourceRequestVO persistCpc(
        SourceRequestVO sourceRequestVO, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        if ((sourceRequestVO.getSourceVO() != null) &&
                (sourceRequestVO.getSourceVO().getCpcCollection() != null) &&
                (sourceRequestVO.getSourceVO().getCpcCollection().size() > 0)) {
            // For now, only 1 cpc record is of interest.
            CpcVO cpcVO = (CpcVO) sourceRequestVO.getSourceVO()
                                                 .getCpcCollection().get(0);

            // Build db stored procedure input parms.
            Map inputParams = new HashMap(4);
            inputParams.put(DB_IN_SOURCE_CODE_ID,
                sourceRequestVO.getSourceVO().getId());
            inputParams.put(DB_IN_CREDIT_CARD_TYPE, cpcVO.getCreditCardType());
            inputParams.put(DB_IN_CREDIT_CARD_NUMBER,
                cpcVO.getCreditCardNumber());
            inputParams.put(DB_IN_CREDIT_CARD_EXPIRATION_DATE,
                cpcVO.getCreditCardExpirationDate());

            // Submit the request to the database.
            Map result = new HashMap(0);

            try {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_CPC_INFO,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "persistCpc: Could not insert cpc into database.";
                logger.warn(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            // Check the return code from the stored procedure.
            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        }

        // Since the result is the record itself, simply return the input
        return sourceRequestVO;
    }

    protected static SourceRequestVO persistSource(
        SourceRequestVO sourceRequestVO, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        boolean connAutoCommitOldValue = dbConn.getAutoCommit();
        dbConn.setAutoCommit(false);

        // Build db stored procedure input parms
        Map inputParams = new HashMap();
        inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
        inputParams.put(DB_IN_SOURCE_TYPE,
            sourceRequestVO.getSourceVO().getSourceType());
        inputParams.put(DB_IN_BONUS_PROMOTION_CODE,
            sourceRequestVO.getSourceVO().getBonusPromotionCode());
        inputParams.put(DB_IN_PROMOTION_CODE,
            sourceRequestVO.getSourceVO().getPromotionCode());
        inputParams.put(DB_IN_REQUIRES_DELIVERY_CONFIRMATION,
            sourceRequestVO.getSourceVO().getRequiresDeliveryConfirmation());
        inputParams.put(DB_IN_WEBLOYALTY_FLAG,
            sourceRequestVO.getSourceVO().getWebloyaltyFlag());
        inputParams.put(DB_IN_EMERGENCY_TEXT_FLAG,
            sourceRequestVO.getSourceVO().getEmergencyTextFlag());
        inputParams.put(DB_IN_ENABLE_LP_PROCESSING,
            sourceRequestVO.getSourceVO().getEnableLpProcessing());
        inputParams.put(DB_IN_FRAUD_FLAG,
            sourceRequestVO.getSourceVO().getFraudFlag());
        inputParams.put(DB_IN_SEND_TO_SCRUB,
            sourceRequestVO.getSourceVO().getSendToScrub());
        inputParams.put(DB_IN_COMPANY_ID,
            sourceRequestVO.getSourceVO().getCompanyId());
        inputParams.put(DB_IN_JCPENNEY_FLAG,
            sourceRequestVO.getSourceVO().getJcpenneyFlag());
        inputParams.put(DB_IN_ORDER_SOURCE,
            sourceRequestVO.getSourceVO().getOrderSource());
        inputParams.put(DB_IN_BIN_NUMBER_CHECK_FLAG,
            sourceRequestVO.getSourceVO().getBinNumberCheckFlag());
        inputParams.put(DB_IN_DISCOUNT_ALLOWED_FLAG,
            sourceRequestVO.getSourceVO().getDiscountAllowedFlag());
        inputParams.put(DB_IN_HIGHLIGHT_DESCRIPTION_FLAG,
            sourceRequestVO.getSourceVO().getHighlightDescriptionFlag());
        inputParams.put(DB_IN_EXTERNAL_CALL_CENTER,
            sourceRequestVO.getSourceVO().getExternalCallCenterFlag());
//        inputParams.put(DB_IN_BILLING_INFO_LOGIC,
//            sourceRequestVO.getSourceVO().getBillingInfoLogic());
//        inputParams.put(DB_IN_BILLING_INFO_PROMPT,
//            sourceRequestVO.getSourceVO().getBillingInfoPrompt());
        inputParams.put(DB_IN_DEFAULT_SOURCE_CODE_FLAG,
            sourceRequestVO.getSourceVO().getDefaultSourceCodeFlag());
        inputParams.put(DB_IN_PAYMENT_METHOD_ID,
            sourceRequestVO.getSourceVO().getPaymentMethodId());
        inputParams.put(DB_IN_PRICE_HEADER_ID,
            sourceRequestVO.getSourceVO().getPriceHeaderId());
        inputParams.put(DB_IN_SNH_ID, sourceRequestVO.getSourceVO().getSnhId());
        inputParams.put(DB_IN_DESCRIPTION,
            sourceRequestVO.getSourceVO().getDescription());
        inputParams.put(DB_IN_REQUESTED_BY,
            sourceRequestVO.getSourceVO().getRequestedBy());
        inputParams.put(DB_IN_RELATED_SOURCE_CODE,
            sourceRequestVO.getSourceVO().getRelatedSourceCode());
        inputParams.put(DB_IN_PROGRAM_WEBSITE_URL,
            sourceRequestVO.getSourceVO().getProgramWebsiteUrl());
        inputParams.put(DB_IN_COMMENT_TEXT,
            sourceRequestVO.getSourceVO().getCommentText());
        inputParams.put(DB_IN_PARTNER_BANK_ID,
            sourceRequestVO.getSourceVO().getPartnerBankId());
        inputParams.put(DB_IN_UPDATED_BY,
        		sourceRequestVO.getUserVO().getUserID());
        inputParams.put(DB_IN_INVOICE_PASSWORD,
            sourceRequestVO.getSourceVO().getInvoicePassword());
        inputParams.put(DB_IN_IOTW_FLAG,
            sourceRequestVO.getSourceVO().getIotwFlag());
        inputParams.put(DB_IN_GIFT_CERTIFICATE_FLAG,
            sourceRequestVO.getSourceVO().getGiftCertificateFlag());
        inputParams.put(DB_IN_BILLING_INFO_FLAG,
            sourceRequestVO.getSourceVO().getBillingInfoFlag());
        inputParams.put(DB_IN_MP_REDEMPTION_RATE_ID,
            sourceRequestVO.getSourceVO().getMpRedemptionRateId());
        inputParams.put(DB_IN_ADD_ON_FREE_ID,
            sourceRequestVO.getSourceVO().getAddOnFreeId());
        inputParams.put(DB_IN_DISPLAY_SERVICE_FEE_CODE,
            sourceRequestVO.getSourceVO().getDisplayServiceFeeCode());
        inputParams.put(DB_IN_DISPLAY_SHIPPING_FEE_CODE,
            sourceRequestVO.getSourceVO().getDisplayShippingFeeCode());
        inputParams.put(DB_IN_PRIMARY_BACKUP_RWD_FLAG,
            sourceRequestVO.getSourceVO().getRandomWeightedFlag());
        inputParams.put(DB_IN_ALLOW_FREE_SHIPPING_FLAG,
            sourceRequestVO.getSourceVO().getAllowFreeShippingFlag());

        inputParams.put(DB_IN_APPLY_SURCHARGE_CODE,
            sourceRequestVO.getSourceVO().getApplySurchargeCode());
        inputParams.put(DB_IN_SURCHARGE_AMOUNT,
            sourceRequestVO.getSourceVO().getSurchargeAmount());
        inputParams.put(DB_IN_SURCHARGE_DESCRIPTION,
            sourceRequestVO.getSourceVO().getSurchargeDescription());
        inputParams.put(DB_IN_DISPLAY_SURCHARGE,
            sourceRequestVO.getSourceVO().getDisplaySurcharge());        
      
        inputParams.put(DB_IN_SAME_DAY_UPCHARGE, sourceRequestVO.getSourceVO().getSameDayUpcharge());
        inputParams.put(DB_IN_DISPLAY_SAME_DAY_UPCHARGE, sourceRequestVO.getSourceVO().getDisplaySameDayUpcharge());
        inputParams.put(DB_IN_MORNING_DELIVERY_FLAG, sourceRequestVO.getSourceVO().getMorningDeliveryFlag());
        inputParams.put(DB_IN_CALCULATE_TAX_FLAG, sourceRequestVO.getSourceVO().getCalculateTaxFlag());
        inputParams.put(DB_IN_SHIPPING_CARRIER_FALG, sourceRequestVO.getSourceVO().getShippingCarrierFlag());
        inputParams.put(DB_IN_MORNING_DELIVERY_TO_FS_MEMBERS,sourceRequestVO.getSourceVO().getMorningDeliveryToFSMembers());
        inputParams.put(DB_IN_DELIVERY_FEE_ID, sourceRequestVO.getSourceVO().getDeliveryFeeId());
        
        //#SGC-2
        inputParams.put(DB_IN_SAME_DAY_UPCHARGE_FS, sourceRequestVO.getSourceVO().getSameDayUpchargeFS());
        
        
        //logger.info("sourceRequestVO.getSourceVO().getMechAmtFullRefundFlag()) "+sourceRequestVO.getSourceVO().getMechAmtFullRefundFlag());
        inputParams.put(DB_IN_MERCH_AMT_FULL_REFUND_FLAG, sourceRequestVO.getSourceVO().getMerchAmtFullRefundFlag());
        
        
        inputParams.put(DB_IN_RECPT_ADDRESS,sourceRequestVO.getSourceVO().getRecptAddress());
        inputParams.put(DB_IN_RECPT_CITY,sourceRequestVO.getSourceVO().getRecptCity());
        inputParams.put(DB_IN_RECPT_STATE,sourceRequestVO.getSourceVO().getRecptState());
        inputParams.put(DB_IN_RECPT_ZIP_CODE,sourceRequestVO.getSourceVO().getRecptZipcode());
        inputParams.put(DB_IN_RECPT_PHONE_NUMBER,sourceRequestVO.getSourceVO().getRecptPhoneNumber());
        inputParams.put(DB_IN_RECPT_COUNTRY,sourceRequestVO.getSourceVO().getRecptCountry());
        
        
      logger.debug("DB_IN_APPLY_SURCHARGE_CODE is:" + sourceRequestVO.getSourceVO().getApplySurchargeCode());
      logger.debug("DB_IN_SURCHARGE_AMOUNT is:" + sourceRequestVO.getSourceVO().getSurchargeAmount());
      logger.debug("DB_IN_SURCHARGE_DESCRIPTION is:" + sourceRequestVO.getSourceVO().getSurchargeDescription());
      logger.debug("DB_IN_DISPLAY_SURCHARGE is:" +  sourceRequestVO.getSourceVO().getDisplaySurcharge());

      inputParams.put(DB_IN_AUTO_PROMO_ENGINE_FLAG, sourceRequestVO.getSourceVO().getAutomatedPromotionEngineFlag());
      inputParams.put(DB_IN_APE_PRODUCT_CATALOG, sourceRequestVO.getSourceVO().getApeProductCatalog());
        // Date Handling.
        if (sourceRequestVO.getSourceVO().getEndDate() != null) {
            inputParams.put(DB_IN_END_DATE,
                new java.sql.Date(sourceRequestVO.getSourceVO().getEndDate()
                                                 .getTime()));
        }

        if (sourceRequestVO.getSourceVO().getStartDate() != null) {
            inputParams.put(DB_IN_START_DATE,
                new java.sql.Date(sourceRequestVO.getSourceVO().getStartDate()
                                                 .getTime()));
        }

        inputParams.put(DB_IN_OSCAR_SELECTION_FLAG,
                sourceRequestVO.getSourceVO().getOscarSelectionEnabledFlag());
        inputParams.put(DB_IN_OSCAR_SCENARIO_GROUP_ID,
                sourceRequestVO.getSourceVO().getOscarScenarioGroupId());
        
      //Sympathy Controls
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LOC_CHK,
                sourceRequestVO.getSourceVO().getFuneralCemeteryLocChk());
        inputParams.put(DB_IN_HOSPITAL_LOC_CHCK,
                sourceRequestVO.getSourceVO().getHospitalLocChck());
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LEAD_TIME_CHK,
                sourceRequestVO.getSourceVO().getFuneralLeadTimeChck());
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LEAD_TIME,
                sourceRequestVO.getSourceVO().getFuneralLeadTime());
        inputParams.put(DB_IN_BO_HRS_MON_FRI_START,
                sourceRequestVO.getSourceVO().getBoHrsMonFriStart());
        inputParams.put(DB_IN_BO_HRS_MON_FRI_END,
                sourceRequestVO.getSourceVO().getBoHrsMonFriEnd());
        inputParams.put(DB_IN_BO_HRS_SAT_START,
                sourceRequestVO.getSourceVO().getBoHrsSatStart());
        inputParams.put(DB_IN_BO_HRS_SAT_END,
                sourceRequestVO.getSourceVO().getBoHrsSatEnd());
        inputParams.put(DB_IN_BO_HRS_SUN_START,
                sourceRequestVO.getSourceVO().getBoHrsSunStart());
        inputParams.put(DB_IN_BO_HRS_SUN_END,
                sourceRequestVO.getSourceVO().getBoHrsSunEnd());
        
        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER,
                    inputParams, dbConn);
        } catch (SQLException e) {
            String errorMsg = "insertSource: Could not insert source into database.";
            logger.error(errorMsg, e);
            dbConn.rollback();
            throw new FtdDataException(errorMsg, e);
        } finally {
            dbConn.setAutoCommit(connAutoCommitOldValue);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        // Submit the request to the database to retrive any preferred (i.e., primary/backup) florists for the sourcecode 
        // so we can determine if any florists have been removed.
        CachedResultSet existingList = null;
        HashSet<String> existingSet = new HashSet<String>();
        try {
            inputParams = new HashMap();
            inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
            existingList =  (CachedResultSet) UtilDAO.submitDbRequest(DB_GET_SOURCE_FLORIST_PRIORITY_LIST, inputParams, dbConn); 
            while (existingList.next()) {
                existingSet.add((String) existingList.getObject(1));
            }
        } catch (SQLException e) {
            String errorMsg = "insertSourcePartnerBinMapping: Could not obtain existing source florist priorities from database.";
            logger.error(errorMsg, e);
            dbConn.rollback();
            throw new FtdDataException(errorMsg, e);
        } finally {
            dbConn.setAutoCommit(connAutoCommitOldValue);
        }

        // Loop over passed list of source/florist priorties, inserting or modifying new entries 
        ArrayList requestedList = sourceRequestVO.getSourceVO().getSourceFloristPriorityList();
        SourceFloristPriorityVO sfpvo = null;
        if (requestedList != null && !requestedList.isEmpty()) {
            Iterator it = requestedList.iterator();
            while (it.hasNext()) {
                sfpvo = (SourceFloristPriorityVO) it.next();
                existingSet.remove(sfpvo.getFloristId());    // Remove this florist from set, so only missing ones will remain
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Updating or inserting source/florist in source_florist_priority: " + sourceRequestVO.getSourceVO().getId() + "/" + sfpvo.getFloristId());
                    }
                    inputParams = new HashMap();
                    inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
                    inputParams.put(DB_IN_FLORIST_ID, sfpvo.getFloristId());
                    inputParams.put(DB_IN_PRIORITY, sfpvo.getPriority());
                    inputParams.put(DB_IN_UPDATED_BY, sourceRequestVO.getUserVO().getUserID());
                    result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_FLORIST_PRIORITY, inputParams, dbConn); 
                    status = (String) result.get(DB_OUT_STATUS);
                    if ((status != null) && status.equals("N")) {
                        String errorMsg = "insertSourceFloristPriority: Error when inserting or updating source/florist priority record into database: " + 
                                          result.get(DB_OUT_MESSAGE);
                        logger.error(errorMsg);
                        dbConn.rollback();
                        throw new FtdDataException((String) result.get(DB_OUT_MESSAGE));
                    }
                } catch (SQLException e) {
                    String errorMsg = "insertSourceFloristPriority: Could not insert or update source/florist priority record into database.";
                    logger.error(errorMsg, e);
                    dbConn.rollback();
                    throw new FtdDataException(errorMsg, e);
                } finally {
                    dbConn.setAutoCommit(connAutoCommitOldValue);
                }
            }
            
        }
        // Now remove any source/florist priorities that were not included in request from user
        if (!existingSet.isEmpty()) {
            Iterator it = existingSet.iterator();
            while (it.hasNext()) {
                String delFlorist = (String) it.next();
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Removing source/florist from source_florist_priority: " + sourceRequestVO.getSourceVO().getId() + "/" + delFlorist);
                    }
                    inputParams = new HashMap();
                    inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
                    inputParams.put(DB_IN_FLORIST_ID, delFlorist);
                    inputParams.put(DB_IN_UPDATED_BY, sourceRequestVO.getUserVO().getUserID());
                    result = (Map) UtilDAO.submitDbRequest(DB_DELETE_SOURCE_FLORIST_PRIORITY, inputParams, dbConn); 
                    status = (String) result.get(DB_OUT_STATUS);
                    if ((status != null) && status.equals("N")) {
                        String errorMsg = "insertSourcePartnerBinMapping: Error when deleting source/florist priority record from database: " + 
                                          result.get(DB_OUT_MESSAGE);
                        logger.error(errorMsg);
                        dbConn.rollback();
                        throw new FtdDataException((String) result.get(DB_OUT_MESSAGE));
                    }
                } catch (SQLException e) {
                    String errorMsg = "insertSourcePartnerBinMapping: Could not delete source/florist priority record from database.";
                    logger.error(errorMsg, e);
                    dbConn.rollback();
                    throw new FtdDataException(errorMsg, e);
                } finally {
                    dbConn.setAutoCommit(connAutoCommitOldValue);
                }
            }
        }

        for (BinCheckPartnerVO binCheckPartnerVO : sourceRequestVO.getSourceVO().getBinCheckPartnerList())  {
           // Submit the request to the database to store the partner bin mappings.
           inputParams = new HashMap();
           inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
           inputParams.put(DB_IN_PARTNER_NAME,binCheckPartnerVO.getPartnerName());
           inputParams.put(DB_IN_BIN_PROCESSING_ACTIVE_FLAG,binCheckPartnerVO.isActive() ? "Y" : "N");
           inputParams.put(DB_IN_UPDATED_BY,sourceRequestVO.getUserVO().getUserID());
          
           if (logger.isDebugEnabled()) {
               logger.debug("DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING: " + DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING);
               logger.debug("DB_IN_SOURCE_CODE: " + inputParams.get(DB_IN_SOURCE_CODE));
               logger.debug("DB_IN_PARTNER_NAME: " + inputParams.get(DB_IN_PARTNER_NAME));
               logger.debug("DB_IN_BIN_PROCESSING_ACTIVE_FLAG: " + inputParams.get(DB_IN_BIN_PROCESSING_ACTIVE_FLAG));
               logger.debug("DB_IN_UPDATED_BY: " + inputParams.get(DB_IN_UPDATED_BY));
           }
           try {
               result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING, inputParams, dbConn);
           } catch (SQLException e) {
               String errorMsg = "insertSourcePartnerBinMapping: Could not insert source into database.";
               logger.error(errorMsg, e);
               dbConn.rollback();
               throw new FtdDataException(errorMsg, e);
           } finally {
               dbConn.setAutoCommit(connAutoCommitOldValue);
           }
          
           dbConn.commit();
  
           // Check the return code from the stored procedure.
           String binStatus = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
  
           if ((binStatus != null) && binStatus.equals("N")) {
               throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
           }
        }
        //dbConn.setAutoCommit(connAutoCommitOldValue);

        for (ProductAttrExclusionVO paeVO : sourceRequestVO.getSourceVO().getProductAttrExclList())  {
            inputParams = new HashMap();
            result = new HashMap();
            inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
            inputParams.put(DB_IN_PRODUCT_ATTRIBUTE_ID, Long.valueOf(paeVO.getProductAttributeId()));
            
            String excludeFlag = paeVO.getExcludedFlag();
            String dbProcess = null;
            if (excludeFlag == null || excludeFlag.equals("N")) {
                dbProcess = DB_REMOVE_PRODUCT_ATTR_EXLUSIONS;
                
            } else {
                dbProcess = DB_INSERT_PRODUCT_ATTR_EXLUSIONS;
                inputParams.put(DB_IN_UPDATED_BY,sourceRequestVO.getUserVO().getUserID());
            }
            logger.debug("DB Process: " + dbProcess);
            logger.debug("DB_IN_SOURCE_CODE: " + inputParams.get(DB_IN_SOURCE_CODE));
            logger.debug("DB_IN_PRODUCT_ATTRIBUTE_ID: " + inputParams.get(DB_IN_PRODUCT_ATTRIBUTE_ID));
            logger.debug("Excluded flag: " + excludeFlag);
            try {
                result = (Map) UtilDAO.submitDbRequest(dbProcess, inputParams, dbConn);
            } catch (Exception e) {
                String errorMsg = "Could not insert source into database.";
                logger.error(errorMsg, e);
                //dbConn.rollback();
                throw new FtdDataException(errorMsg, e);
            } finally {
                //dbConn.setAutoCommit(connAutoCommitOldValue);
            }
            //dbConn.commit();
            // Check the return code from the stored procedure.
            String productAttributeStatus = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
            if ((productAttributeStatus != null) && productAttributeStatus.equals("N")) {
                logger.error((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
                throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
            }
        }
        //dbConn.setAutoCommit(connAutoCommitOldValue);
        
        // Save Member level data
        for (MpMemberLevelVO memberLevelVO : sourceRequestVO.getSourceVO().getMpMemberLevelList())  {
            inputParams = new HashMap();
            result = new HashMap();
            inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
            inputParams.put(DB_IN_MP_MEMBER_LEVEL_ID,memberLevelVO.getMpMemberLevelId());
                        
            String dbProcess = null;                                
            dbProcess = DB_UPDATE_SOURCE_MP_MEMBER_LEVEL_INFO;
            inputParams.put(DB_IN_UPDATED_BY,sourceRequestVO.getUserVO().getUserID());
            
            logger.debug("DB Process: " + dbProcess);
            logger.debug("DB_IN_SOURCE_CODE: " + inputParams.get(DB_IN_SOURCE_CODE));
            logger.debug("DB_IN_MP_MEMBER_LEVEL_ID: " + inputParams.get(DB_IN_MP_MEMBER_LEVEL_ID));
            
            try {
            	result = (Map) UtilDAO.submitDbRequest(dbProcess, inputParams, dbConn);
            } catch (Exception e) {
                String errorMsg = "Could not insert source into database.";
                logger.error(errorMsg, e);
                //dbConn.rollback();
                throw new FtdDataException(errorMsg, e);
            } finally {
                //dbConn.setAutoCommit(connAutoCommitOldValue);
            }
            //dbConn.commit();
            // Check the return code from the stored procedure.
            String memberLevelStatus = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
            if ((memberLevelStatus != null) && memberLevelStatus.equals("N")) {
                logger.error((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
                throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
            }
        }
        // Since the result is the source code itself, simply return the input
        return sourceRequestVO;
    }

    public static Document querySource(SourceRequestVO sourceRequestVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        return SourceDAO.querySource(sourceRequestVO, recordNumberStart,
            maxRecordCount, resultRecordCount, null);
    }

    public static Document querySource(SourceRequestVO sourceRequestVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Document result = DOMUtil.getDefaultDocument();
        Document binQueryResult = DOMUtil.getDefaultDocument();

        if ((sourceRequestVO != null) &&
                (sourceRequestVO.getSourceVO() != null)) {
            SourceVO sourceVO = sourceRequestVO.getSourceVO();
            Map inputParams = new HashMap();

            UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE,
                sourceVO.getId());
            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_BY,
                sourceVO.getUpdatedBy());
            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_ON_START,
                sourceVO.getUpdatedOnStart());
            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_ON_END,
                sourceVO.getUpdatedOnEnd());
            UtilDAO.addSearchFilter(inputParams, DB_IN_REQUESTED_BY,
                sourceVO.getRequestedBy());
            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_WEBSITE_URL,
                sourceVO.getProgramWebsiteUrl());
            UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_TYPE,
                sourceVO.getSourceType());
            UtilDAO.addSearchFilter(inputParams, DB_IN_PROMOTION_CODE,
                sourceVO.getPromotionCode());
            UtilDAO.addSearchFilter(inputParams, DB_IN_PRICE_HEADER_ID,
                sourceVO.getPriceHeaderId());
            UtilDAO.addSearchFilter(inputParams, DB_IN_SNH_ID,
                sourceVO.getSnhId());
            UtilDAO.addSearchFilter(inputParams, DB_IN_END_DATE,
                sourceVO.getEndDate());
            UtilDAO.addSearchFilter(inputParams, DB_IN_COMPANY_ID,
                sourceVO.getCompanyId());
            UtilDAO.addSearchFilter(inputParams,
                DB_IN_DEFAULT_SOURCE_CODE_FLAG,
                sourceVO.getDefaultSourceCodeFlag());
            UtilDAO.addSearchFilter(inputParams, DB_IN_JCPENNEY_FLAG,
                sourceVO.getJcpenneyFlag());
            UtilDAO.addSearchFilter(inputParams, DB_IN_OVER_21_FLAG,
                sourceVO.getOver21Flag());
            UtilDAO.addSearchFilter(inputParams, DB_IN_IOTW_FLAG,
                sourceVO.getIotwFlag());

            // Program filter.
            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_NAME,
                sourceRequestVO.getProgramName());

            // Specify the sort column.
            if ((sortBy != null) && (sortBy.length() > 0)) {
                UtilDAO.addSearchFilter(inputParams, DB_IN_SORT_BY,
                    COLUMN_NAME_MAP.getProperty(sortBy));
            }

            // Add the pagination metrics.
            // Note that we have to decrement 1 from the number of records
            // being requested since the storec proc API needs the start
            // and end record numbers.
            inputParams.put(DB_IN_START_POSITION, new Long(recordNumberStart));
            inputParams.put(DB_IN_END_POSITION,
                new Long((recordNumberStart + maxRecordCount) - 1));

            // Submit the request to the database.
            try {
                Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_SOURCE,
                        inputParams, false);
                result = (Document) compositeResult.get(DB_OUT_CURSOR);
                logger.debug("Showing all result in xml form from Source table from MARKETING.SELECT_SOURCE query");
                // Record the total records returned.
                // Verify that the total record count variable
                // was populated.
                resultRecordCount.add(0,
                    UtilDAO.extractTotalRecordCount(compositeResult,
                        DB_OUT_MAX_ROWS));  

                if (resultRecordCount.get(0) == null) {
                    throw new FtdDataException(
                        "querySources: Could not determine record count of result set.");
                }

                //DOMUtil.addSection(result, ((XMLDocument) compositeResult.get(RESULT_MAP_KEY_OUT_PARAMETERS)).getChildNodes());
            } catch (SQLException e) {
                String errorMsg = "querySource: Could not obtain source from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            // Submit the request to the database to retrive the bin mappings     
            try {
                inputParams = new HashMap();
                inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
                binQueryResult =  (Document) UtilDAO.submitDbRequest(DB_SELECT_PARTNER_BIN_MAPPING, inputParams, false); 
                DOMUtil.addSection(result, binQueryResult.getChildNodes());

            } catch (SQLException e) {
                String errorMsg = "querySource: Could not obtain source bin mappings from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            try {
                inputParams = new HashMap();
                inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
                Document prodAttrExclusions =  (Document) UtilDAO.submitDbRequest(DB_SELECT_PRODUCT_ATTR_EXCLUSIONS, inputParams, false); 
                DOMUtil.addSection(result, prodAttrExclusions.getChildNodes());

            } catch (SQLException e) {
                String errorMsg = "querySource: Could not obtain product attribute exclusions from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            // Submit the request to the database to retrive any preferred (i.e., primary/backup) florists for the sourcecode     
            try {
                inputParams = new HashMap();
                inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
                binQueryResult =  (Document) UtilDAO.submitDbRequest(DB_GET_SOURCE_FLORIST_PRIORITY, inputParams, false); 
                DOMUtil.addSection(result, binQueryResult.getChildNodes());

            } catch (SQLException e) {
                String errorMsg = "querySource: Could not obtain source florist priorities from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            } 
        }
        return result;
    }

    public static Document querySourceByUser(SourceRequestVO sourceRequestVO,
        int recordNumberStart, int maxRecordCount, List resultRecordCount,
        String sortBy)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Document result = DOMUtil.getDefaultDocument();

        if ((sourceRequestVO != null) &&
                (sourceRequestVO.getSourceVO() != null)) {
            SourceVO sourceVO = sourceRequestVO.getSourceVO();
            Map inputParams = new HashMap(6);

            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_BY,
                sourceVO.getUpdatedBy());
            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_ON_START,
                sourceVO.getUpdatedOnStart());
            UtilDAO.addSearchFilter(inputParams, DB_IN_UPDATED_ON_END,
                sourceVO.getUpdatedOnEnd());

            // Specify the sort column.
            if ((sortBy != null) && (sortBy.length() > 0)) {
                UtilDAO.addSearchFilter(inputParams, DB_IN_SORT_BY,
                    COLUMN_NAME_MAP.getProperty(sortBy));
            }

            // Add the pagination metrics.
            // Note that we have to decrement 1 from the number of records
            // being requested since the storec proc API needs the start
            // and end record numbers.
            inputParams.put(DB_IN_START_POSITION, new Long(recordNumberStart));
            inputParams.put(DB_IN_END_POSITION,
                new Long((recordNumberStart + maxRecordCount) - 1));

            // Submit the request to the database.
            try {
                Map compositeResult = (Map) UtilDAO.submitDbRequest(DB_SELECT_SOURCE_BY_UPDATED_BY,
                        inputParams, false);
                result = (Document) compositeResult.get(DB_OUT_CURSOR);

                // Record the total records returned.
                // Verify that the total record count variable
                // was populated.
                resultRecordCount.add(0,
                    UtilDAO.extractTotalRecordCount(compositeResult,
                        DB_OUT_MAX_ROWS));

                if (resultRecordCount.get(0) == null) {
                    throw new FtdDataException(
                        "querySources: Could not determine record count of result set.");
                }

                //DOMUtil.addSection(result, ((XMLDocument) compositeResult.get(RESULT_MAP_KEY_OUT_PARAMETERS)).getChildNodes());
            } catch (SQLException e) {
                String errorMsg = "querySource: Could not obtain source from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        }

        return result;
    }

    public static Document queryCpc(String sourceCode)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE_ID, sourceCode);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_CPC_INFO,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg =
                "queryCpc: Could not obtain cpc info from database for source code " +
                sourceCode;
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static CachedResultSet querySourcePartnerBanks(String sourceCode)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE, sourceCode);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_SELECT_SOURCE_PARTNER_BANKS,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg =
                "querySourcePrograms: Could not obtain source programs ref info from database for source code " +
                sourceCode;
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document querySourcePrograms(String sourceCode)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_CODE, sourceCode);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_SOURCE_PROGRAM_REF_BY_SOURCE,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg =
                "querySourcePrograms: Could not obtain source programs ref info from database for source code " +
                sourceCode;
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static Document querySourceProgram(String id)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_SOURCE_PROGRAM_REF_ID, id);

        // Submit the request to the database.
        try {
            return (Document) UtilDAO.submitDbRequest(DB_SELECT_SOURCE_PROGRAM_REF,
                inputParams, false);
        } catch (SQLException e) {
            String errorMsg =
                "querySourceProgram: Could not obtain source programs ref info from database for id " +
                id;
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }

    public static SourceProgramRequestVO persistSourceProgram(
        SourceProgramRequestVO sourceProgramRequestVO)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        SourceProgramRefVO sprVO = sourceProgramRequestVO.getSourceProgramRefVO();

        if (sprVO == null) {
            throw new FtdDataException(
                "persistSourceProgram: input value object SourceProgramRefVO can not be null.");
        }

        // Build db stored procedure input parms
        Map inputParams = new HashMap(5);
        inputParams.put(DB_IN_SOURCE_CODE, sprVO.getSourceCode());

        inputParams.put(DB_IN_PROGRAM_NAME, sprVO.getProgramName());

        // Date Handling.
        if (sprVO.getStartDate() != null) {
            inputParams.put(DB_IN_START_DATE,
                new java.sql.Date(sprVO.getStartDate().getTime()));
        }

        // Submit the request to the database.
        Map result = new HashMap(0);
        String sqlStatement = null;

        try {
            if (sprVO.getId() == null) {
                inputParams.put(DB_IN_CREATED_BY,
                    sourceProgramRequestVO.getUserVO().getUserID());

                sqlStatement = DB_INSERT_SOURCE_PROGRAM_REF;
            } else {
                inputParams.put(DB_IN_SOURCE_PROGRAM_REF_ID, sprVO.getId());
                inputParams.put(DB_IN_UPDATED_BY,
                    sourceProgramRequestVO.getUserVO().getUserID());

                sqlStatement = DB_UPDATE_SOURCE_PROGRAM_REF;
            }

            result = (Map) UtilDAO.submitDbRequest(sqlStatement, inputParams,
                    true);
        } catch (SQLException e) {
            String errorMsg = "persistSourceProgram: Could not insert/update source program ref into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        return sourceProgramRequestVO;
    }


    public static CachedResultSet getFloristDetails(String floristId)
        throws IOException, ParserConfigurationException, SAXException,
            FtdDataException, Exception {
        Map inputParams = new HashMap(1);
        UtilDAO.addSearchFilter(inputParams, DB_IN_FLORIST_ID, floristId);

        // Submit the request to the database.
        try {
            return (CachedResultSet) UtilDAO.submitDbRequest(DB_VIEW_FLORIST_MASTER, inputParams, false);
        } catch (SQLException e) {
            String errorMsg =
                "getFloristDetails: Could not obtain florist details from database for florist ID: " + floristId;
            logger.error(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }
    }



   /**
   * Persists the Default Order information for a Source Code
   * @param sourceDefaultOrderInfoVO
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws FtdDataException
   * @throws Exception
   */
    public static void persistSourceDefaultOrderInfo(SourceDefaultOrderInfoVO sourceDefaultOrderInfoVO)
        throws FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(24);
        inputParams.put("IN_SOURCE_CODE", sourceDefaultOrderInfoVO.getSourceCode());
        inputParams.put("IN_RECPT_LOCATION_TYPE", sourceDefaultOrderInfoVO.getRecptLocationType());
        inputParams.put("IN_RECPT_BUSINESS_NAME", sourceDefaultOrderInfoVO.getRecptBusinessName());
        inputParams.put("IN_RECPT_LOCATION_DETAIL", sourceDefaultOrderInfoVO.getRecptLocationDetail());
        inputParams.put("IN_RECPT_ADDRESS", sourceDefaultOrderInfoVO.getRecptAddress());
        inputParams.put("IN_RECPT_ZIP_CODE", sourceDefaultOrderInfoVO.getRecptZipCode());
        inputParams.put("IN_RECPT_CITY", sourceDefaultOrderInfoVO.getRecptCity());
        inputParams.put("IN_RECPT_STATE_ID", sourceDefaultOrderInfoVO.getRecptStateId());
        inputParams.put("IN_RECPT_COUNTRY_ID", sourceDefaultOrderInfoVO.getRecptCountryId());
        inputParams.put("IN_RECPT_PHONE", sourceDefaultOrderInfoVO.getRecptPhone());
        inputParams.put("IN_RECPT_PHONE_EXT", sourceDefaultOrderInfoVO.getRecptPhoneExt());
        inputParams.put("IN_CUST_FIRST_NAME", sourceDefaultOrderInfoVO.getCustFirstName());
        inputParams.put("IN_CUST_LAST_NAME", sourceDefaultOrderInfoVO.getCustLastName());
        inputParams.put("IN_CUST_DAYTIME_PHONE", sourceDefaultOrderInfoVO.getCustDaytimePhone());
        inputParams.put("IN_CUST_DAYTIME_PHONE_EXT", sourceDefaultOrderInfoVO.getCustDaytimePhoneExt());
        inputParams.put("IN_CUST_EVENING_PHONE", sourceDefaultOrderInfoVO.getCustEveningPhone());
        inputParams.put("IN_CUST_EVENING_PHONE_EXT", sourceDefaultOrderInfoVO.getCustEveningPhoneExt());
        inputParams.put("IN_CUST_ADDRESS", sourceDefaultOrderInfoVO.getCustAddress());
        inputParams.put("IN_CUST_ZIP_CODE", sourceDefaultOrderInfoVO.getCustZipCode());
        inputParams.put("IN_CUST_CITY", sourceDefaultOrderInfoVO.getCustCity());
        inputParams.put("IN_CUST_STATE_ID", sourceDefaultOrderInfoVO.getCustStateId());
        inputParams.put("IN_CUST_COUNTRY_ID", sourceDefaultOrderInfoVO.getCustCountryId());
        inputParams.put("IN_CUST_EMAIL_ADDRESS", sourceDefaultOrderInfoVO.getCustEmailAddress());
        inputParams.put("IN_UPDATED_BY", sourceDefaultOrderInfoVO.getUpdatedBy());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest("MARKETING.UPDATE_SOURCE_MASTER_DEFAULT_ORDER_INFO",
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "persistSourceType: Could not insert source type into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
    }
    
   /**
   * Retrieves the Source Order Info given the source code
   * @param sourceCode
   * @return
   * @throws FtdDataException
   * @throws Exception
   */
    public static SourceDefaultOrderInfoVO getSourceDefaultOrderInfo(String sourceCode) 
        throws FtdDataException, Exception {
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
      
        SourceDefaultOrderInfoVO sourceDefaultOrderInfoVO = new SourceDefaultOrderInfoVO();
        sourceDefaultOrderInfoVO.setSourceCode(sourceCode);
        
        try {
          CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.GET_SOURCE_MASTER_DEFAULT_ORDER_INFO",
                    inputParams, false);
          
          if(result.next()) {
            sourceDefaultOrderInfoVO.setRecptLocationType(result.getString("RECPT_LOCATION_TYPE"));
            sourceDefaultOrderInfoVO.setRecptBusinessName(result.getString("RECPT_BUSINESS_NAME"));
            sourceDefaultOrderInfoVO.setRecptLocationDetail(result.getString("RECPT_LOCATION_DETAIL"));
            sourceDefaultOrderInfoVO.setRecptAddress(result.getString("RECPT_ADDRESS"));
            sourceDefaultOrderInfoVO.setRecptZipCode(result.getString("RECPT_ZIP_CODE"));
            sourceDefaultOrderInfoVO.setRecptCity(result.getString("RECPT_CITY"));
            sourceDefaultOrderInfoVO.setRecptStateId(result.getString("RECPT_STATE_ID"));
            sourceDefaultOrderInfoVO.setRecptCountryId(result.getString("RECPT_COUNTRY_ID"));
            sourceDefaultOrderInfoVO.setRecptPhone(result.getString("RECPT_PHONE"));
            sourceDefaultOrderInfoVO.setRecptPhoneExt(result.getString("RECPT_PHONE_EXT"));
            sourceDefaultOrderInfoVO.setCustFirstName(result.getString("CUST_FIRST_NAME"));
            sourceDefaultOrderInfoVO.setCustLastName(result.getString("CUST_LAST_NAME"));
            sourceDefaultOrderInfoVO.setCustDaytimePhone(result.getString("CUST_DAYTIME_PHONE"));
            sourceDefaultOrderInfoVO.setCustDaytimePhoneExt(result.getString("CUST_DAYTIME_PHONE_EXT"));
            sourceDefaultOrderInfoVO.setCustEveningPhone(result.getString("CUST_EVENING_PHONE"));
            sourceDefaultOrderInfoVO.setCustEveningPhoneExt(result.getString("CUST_EVENING_PHONE_EXT"));
            sourceDefaultOrderInfoVO.setCustAddress(result.getString("CUST_ADDRESS"));
            sourceDefaultOrderInfoVO.setCustZipCode(result.getString("CUST_ZIP_CODE"));
            sourceDefaultOrderInfoVO.setCustCity(result.getString("CUST_CITY"));
            sourceDefaultOrderInfoVO.setCustStateId(result.getString("CUST_STATE_ID"));
            sourceDefaultOrderInfoVO.setCustCountryId(result.getString("CUST_COUNTRY_ID"));
            sourceDefaultOrderInfoVO.setCustEmailAddress(result.getString("CUST_EMAIL_ADDRESS"));   
            sourceDefaultOrderInfoVO.setRequestedBy(result.getString("REQUESTED_BY")); 
          }                    
        } catch (Exception e) {
            String errorMsg = "getSourceDefaultOrderInfo: Could not retrieve source default order info from database for source code: " + sourceCode;
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        return sourceDefaultOrderInfoVO;
    }
    
    /**************** #13046 Sympathy Code Changes start here ****************/
    
    /** Inserts the source code details into various tables.
     * @param sourceRequestVO
     * @param sourceProgramRequestVO
     * @param programId
     * @throws FtdDataException
     * @throws FtdSecurityException
     * @throws Exception
     */
    public static void persistCompositeSource(SourceRequestVO sourceRequestVO,
			SourceProgramRequestVO sourceProgramRequestVO, String programId) throws FtdDataException, FtdSecurityException, Exception {
		Connection dbConn = UtilDAO.getDbConnection();
		dbConn.setAutoCommit(false);
		try {
			// reserve the source code.
			persistReservedSourceCode(sourceRequestVO.getSourceVO(), RecordStatus.ACTIVE, dbConn);
			
			// reserve the source code detail
			persistSourceCodeDetail(sourceRequestVO, sourceProgramRequestVO, dbConn);			
		
			dbConn.commit();
			
		} catch (Exception e) {
			logger.error(e);
			dbConn.rollback();
			throw e;
		} finally {
			try {
				if (dbConn != null) {
					dbConn.close();
				}
			} catch (Exception e) {
				logger.error("Unable to close connection: " + e);
			}			
		}
	}
    
    /** Inserts the source code details into
     * source_master_reserve, source_master, source_master_bin_mapping (if bin_check is enabled for USAA partner), source_program_reference
     * @param sourceRequestVO
     * @param sourceProgramRequestVO
     * @param dbConn
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws FtdDataException
     * @throws Exception
     */
    protected static void persistSourceCodeDetail(SourceRequestVO sourceRequestVO, SourceProgramRequestVO sourceProgramRequestVO, Connection dbConn)
			throws IOException, ParserConfigurationException, SAXException, FtdDataException, Exception {
		
		Map inputParams = null;
		Map result = new HashMap(0);
		
		/****************** INSERT/UDPATE SOURCE_MASTER ******************/
		
		// Submit the request to insert to update source master table.		
		try {
			inputParams = setPersistSourceMasterInput(sourceRequestVO.getSourceVO(), sourceRequestVO.getUserVO().getUserID());	
			result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_MASTER,	inputParams, dbConn);
			
			// Check the return code from the stored procedure.
			String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
			if ((status != null) && status.equals("N")) {
				throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));			
			}
		} catch (SQLException e) {					
			throw new FtdDataException("insertSource: Could not insert source into database.", e);
		} 
		
		
		/****************** INSERT/UDPATE SOURCE_LEGACY_ID_MAPPING ******************/
		if(sourceRequestVO.getSourceVO().getId()!=null && sourceRequestVO.getSourceVO().getLegacyId()!=null && sourceRequestVO.getSourceVO().getLegacyId().length() > 0){
			
			try {
				inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
				inputParams.put(DB_IN_LEGACY_ID, sourceRequestVO.getSourceVO().getLegacyId());
				result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_LEGACY_MAPPING,	inputParams, dbConn);
				
				// Check the return code from the stored procedure.
				String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
				if ((status != null) && status.equals("N")) {
					throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));			
				}
			} catch (SQLException e) {					
				throw new FtdDataException("insertSource: Could not insert source code and legacy_id mapping into database.", e);
			} 
		}
		
		
		
		/****************** INSERT/UDPATE SOURCE_PARTNER_BIN_MAPPING ******************/	
		if(sourceRequestVO.getSourceVO().getBinCheckPartnerList() != null && sourceRequestVO.getSourceVO().getBinCheckPartnerList().size() > 0) {
			for (BinCheckPartnerVO binCheckPartnerVO : sourceRequestVO.getSourceVO().getBinCheckPartnerList()) {
				if(binCheckPartnerVO.isActive()) {
					// Submit the request to the database to store the partner bin mappings.
					try {
						inputParams = setPersistSourceBinMappingInput(sourceRequestVO, binCheckPartnerVO);				
						result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING, inputParams, dbConn);
					} catch (SQLException e) {				
						throw new FtdDataException("insertSourcePartnerBinMapping: Could not insert source into database.", e);
					} 
					
					// Check the return code from the stored procedure.
					String binStatus = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
			
					if ((binStatus != null) && binStatus.equals("N")) {
						throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
					}
				}
			}
		}
		
		/****************** INSERT/UDPATE SOURCE_PROGRAM_REF ******************/
		if (sourceProgramRequestVO != null && sourceProgramRequestVO.getSourceProgramRefVO() != null) {			
			try {			
				inputParams = setPersistSourcePrgmRefInput(sourceProgramRequestVO.getSourceProgramRefVO(), 
						sourceProgramRequestVO.getUserVO().getUserID());			
				String sqlStatement = null;
				
				if (sourceProgramRequestVO.getSourceProgramRefVO().getId() == null) {				
					sqlStatement = DB_INSERT_SOURCE_PROGRAM_REF;
				} else {				
					sqlStatement = DB_UPDATE_SOURCE_PROGRAM_REF;
				}			
				result = (Map) UtilDAO.submitDbRequest(sqlStatement, inputParams, dbConn);
				
				String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
				if ((status != null) && status.equals("N")) {
					throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
				}
			} catch (SQLException e) {			
				throw new FtdDataException("persistSourceProgram: Could not insert/update source program ref into database.", e);
			}
		}
    }
    
    /** Sets the required input parameters to insert data into source_master table
     * @param sourceRequestVO
     * @return
     */
    private static Map setPersistSourceMasterInput(SourceVO sourceVO, String userId) {

		Map inputParams = new HashMap();
		inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
        inputParams.put(DB_IN_SOURCE_TYPE, sourceVO.getSourceType());
        inputParams.put(DB_IN_BONUS_PROMOTION_CODE, sourceVO.getBonusPromotionCode());
        inputParams.put(DB_IN_PROMOTION_CODE, sourceVO.getPromotionCode());
        inputParams.put(DB_IN_REQUIRES_DELIVERY_CONFIRMATION, sourceVO.getRequiresDeliveryConfirmation());
        inputParams.put(DB_IN_WEBLOYALTY_FLAG, sourceVO.getWebloyaltyFlag());
        inputParams.put(DB_IN_EMERGENCY_TEXT_FLAG, sourceVO.getEmergencyTextFlag());
        inputParams.put(DB_IN_ENABLE_LP_PROCESSING, sourceVO.getEnableLpProcessing());
        inputParams.put(DB_IN_FRAUD_FLAG, sourceVO.getFraudFlag());
        inputParams.put(DB_IN_SEND_TO_SCRUB, sourceVO.getSendToScrub());
        inputParams.put(DB_IN_COMPANY_ID, sourceVO.getCompanyId());
        inputParams.put(DB_IN_JCPENNEY_FLAG, sourceVO.getJcpenneyFlag());
        inputParams.put(DB_IN_ORDER_SOURCE, sourceVO.getOrderSource());
        inputParams.put(DB_IN_BIN_NUMBER_CHECK_FLAG, sourceVO.getBinNumberCheckFlag());
        inputParams.put(DB_IN_DISCOUNT_ALLOWED_FLAG, sourceVO.getDiscountAllowedFlag());
        inputParams.put(DB_IN_HIGHLIGHT_DESCRIPTION_FLAG, sourceVO.getHighlightDescriptionFlag());
        inputParams.put(DB_IN_EXTERNAL_CALL_CENTER, sourceVO.getExternalCallCenterFlag());
        inputParams.put(DB_IN_DEFAULT_SOURCE_CODE_FLAG, sourceVO.getDefaultSourceCodeFlag());
        inputParams.put(DB_IN_PAYMENT_METHOD_ID, sourceVO.getPaymentMethodId());
        inputParams.put(DB_IN_PRICE_HEADER_ID, sourceVO.getPriceHeaderId());
        inputParams.put(DB_IN_SNH_ID, sourceVO.getSnhId());
        inputParams.put(DB_IN_DESCRIPTION, sourceVO.getDescription());
        inputParams.put(DB_IN_REQUESTED_BY, sourceVO.getRequestedBy());
        inputParams.put(DB_IN_RELATED_SOURCE_CODE, sourceVO.getRelatedSourceCode());
        inputParams.put(DB_IN_PROGRAM_WEBSITE_URL, sourceVO.getProgramWebsiteUrl());
        inputParams.put(DB_IN_COMMENT_TEXT, sourceVO.getCommentText());
        inputParams.put(DB_IN_PARTNER_BANK_ID, sourceVO.getPartnerBankId());
        inputParams.put(DB_IN_UPDATED_BY, userId);
        inputParams.put(DB_IN_INVOICE_PASSWORD, sourceVO.getInvoicePassword());
        inputParams.put(DB_IN_IOTW_FLAG, sourceVO.getIotwFlag());
        inputParams.put(DB_IN_GIFT_CERTIFICATE_FLAG, sourceVO.getGiftCertificateFlag());
        inputParams.put(DB_IN_BILLING_INFO_FLAG, sourceVO.getBillingInfoFlag());
        inputParams.put(DB_IN_MP_REDEMPTION_RATE_ID, sourceVO.getMpRedemptionRateId());
        inputParams.put(DB_IN_ADD_ON_FREE_ID, sourceVO.getAddOnFreeId());
        inputParams.put(DB_IN_DISPLAY_SERVICE_FEE_CODE, sourceVO.getDisplayServiceFeeCode());
        inputParams.put(DB_IN_DISPLAY_SHIPPING_FEE_CODE, sourceVO.getDisplayShippingFeeCode());
        inputParams.put(DB_IN_PRIMARY_BACKUP_RWD_FLAG, sourceVO.getRandomWeightedFlag());
        inputParams.put(DB_IN_ALLOW_FREE_SHIPPING_FLAG, sourceVO.getAllowFreeShippingFlag());
        inputParams.put(DB_IN_APPLY_SURCHARGE_CODE, sourceVO.getApplySurchargeCode());
        inputParams.put(DB_IN_SURCHARGE_AMOUNT, sourceVO.getSurchargeAmount());
        inputParams.put(DB_IN_SURCHARGE_DESCRIPTION, sourceVO.getSurchargeDescription());
        inputParams.put(DB_IN_DISPLAY_SURCHARGE, sourceVO.getDisplaySurcharge()); 
        inputParams.put(DB_IN_SAME_DAY_UPCHARGE, sourceVO.getSameDayUpcharge());
        inputParams.put(DB_IN_DISPLAY_SAME_DAY_UPCHARGE, sourceVO.getDisplaySameDayUpcharge());
        inputParams.put(DB_IN_MORNING_DELIVERY_FLAG, sourceVO.getMorningDeliveryFlag());
        inputParams.put(DB_IN_MORNING_DELIVERY_TO_FS_MEMBERS,sourceVO.getMorningDeliveryToFSMembers());
        inputParams.put(DB_IN_DELIVERY_FEE_ID, sourceVO.getDeliveryFeeId());
		        
        inputParams.put(DB_IN_OSCAR_SELECTION_FLAG, sourceVO.getOscarSelectionEnabledFlag());
        inputParams.put(DB_IN_OSCAR_SCENARIO_GROUP_ID, sourceVO.getOscarScenarioGroupId());
        
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LOC_CHK, sourceVO.getFuneralCemeteryLocChk());
        inputParams.put(DB_IN_HOSPITAL_LOC_CHCK, sourceVO.getHospitalLocChck());
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LEAD_TIME_CHK, sourceVO.getFuneralLeadTimeChck());
        inputParams.put(DB_IN_FUNERAL_CEMETERY_LEAD_TIME, sourceVO.getFuneralLeadTime());
        inputParams.put(DB_IN_BO_HRS_MON_FRI_START, sourceVO.getBoHrsMonFriStart());
        inputParams.put(DB_IN_BO_HRS_MON_FRI_END, sourceVO.getBoHrsMonFriEnd());
        inputParams.put(DB_IN_BO_HRS_SAT_START, sourceVO.getBoHrsSatStart());
        inputParams.put(DB_IN_BO_HRS_SAT_END, sourceVO.getBoHrsSatEnd());
        inputParams.put(DB_IN_BO_HRS_SUN_START, sourceVO.getBoHrsSunStart());
        inputParams.put(DB_IN_BO_HRS_SUN_END, sourceVO.getBoHrsSunEnd());
		inputParams.put(DB_IN_CALCULATE_TAX_FLAG, sourceVO.getCalculateTaxFlag() == null ? "Y" : sourceVO.getCalculateTaxFlag());
       
        inputParams.put(DB_IN_LEGACY_ID,sourceVO.getLegacyId());
        
        inputParams.put(DB_IN_RECPT_ADDRESS,sourceVO.getRecptAddress());
        inputParams.put(DB_IN_RECPT_CITY,sourceVO.getRecptCity());
        inputParams.put(DB_IN_RECPT_STATE,sourceVO.getRecptState());
        inputParams.put(DB_IN_RECPT_ZIP_CODE,sourceVO.getRecptZipcode());
        inputParams.put(DB_IN_RECPT_PHONE_NUMBER,sourceVO.getRecptPhoneNumber());
        inputParams.put(DB_IN_RECPT_COUNTRY,sourceVO.getRecptCountry());
        
        //#SGC-2
        inputParams.put(DB_IN_SAME_DAY_UPCHARGE_FS, sourceVO.getSameDayUpchargeFS());
        
        logger.info("The input legacy id is : "+sourceVO.getLegacyId());
        
		logger.debug("DB_IN_APPLY_SURCHARGE_CODE is:" + sourceVO.getApplySurchargeCode());
        logger.debug("DB_IN_SURCHARGE_AMOUNT is:" + sourceVO.getSurchargeAmount());
        logger.debug("DB_IN_SURCHARGE_DESCRIPTION is:" + sourceVO.getSurchargeDescription());
        logger.debug("DB_IN_DISPLAY_SURCHARGE is:" +  sourceVO.getDisplaySurcharge());
        
        inputParams.put(DB_IN_AUTO_PROMO_ENGINE_FLAG, sourceVO.getAutomatedPromotionEngineFlag());
        inputParams.put(DB_IN_APE_PRODUCT_CATALOG, sourceVO.getApeProductCatalog());
		// Date Handling.
		if (sourceVO.getEndDate() != null) {
			inputParams.put(DB_IN_END_DATE, new java.sql.Date(sourceVO.getEndDate().getTime()));
		}

		if (sourceVO.getStartDate() != null) {
			inputParams.put(DB_IN_START_DATE, new java.sql.Date(sourceVO.getStartDate().getTime()));
		}
		
		return inputParams;
	}
	
	/** Sets the required input parameters to insert data into source_master_bin_mapping table
	 * @param sourceRequestVO
	 * @param binCheckPartnerVO
	 * @return
	 */
	private static Map setPersistSourceBinMappingInput(SourceRequestVO sourceRequestVO, BinCheckPartnerVO binCheckPartnerVO) {
		Map inputParams = new HashMap();
		inputParams.put(DB_IN_SOURCE_CODE, sourceRequestVO.getSourceVO().getId());
		inputParams.put(DB_IN_PARTNER_NAME,	binCheckPartnerVO.getPartnerName());
		inputParams.put(DB_IN_BIN_PROCESSING_ACTIVE_FLAG, binCheckPartnerVO.isActive() ? "Y" : "N");
		inputParams.put(DB_IN_UPDATED_BY, sourceRequestVO.getUserVO().getUserID());

		if (logger.isDebugEnabled()) {
			logger.debug("DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING: " + DB_UPDATE_SOURCE_PARTNER_BIN_MAPPING);
			logger.debug("DB_IN_SOURCE_CODE: " + inputParams.get(DB_IN_SOURCE_CODE));
			logger.debug("DB_IN_PARTNER_NAME: " + inputParams.get(DB_IN_PARTNER_NAME));
			logger.debug("DB_IN_BIN_PROCESSING_ACTIVE_FLAG: " + inputParams.get(DB_IN_BIN_PROCESSING_ACTIVE_FLAG));
			logger.debug("DB_IN_UPDATED_BY: " + inputParams.get(DB_IN_UPDATED_BY));
		}		
		return inputParams;
	}
	
	/** Sets the required input parameters to insert data into source_program_ref table
	 * @param sourceProgramRef
	 * @param userId
	 * @return
	 */
	private static Map setPersistSourcePrgmRefInput(SourceProgramRefVO sourceProgramRef, String userId) {
		Map inputParams = new HashMap();
		inputParams.put(DB_IN_SOURCE_CODE, sourceProgramRef.getSourceCode());
		inputParams.put(DB_IN_PROGRAM_NAME, sourceProgramRef.getProgramName());

		if (sourceProgramRef.getStartDate() != null) {			
			inputParams.put(DB_IN_START_DATE, new java.sql.Date(sourceProgramRef.getStartDate().getTime()));
		}

		if (sourceProgramRef.getId() == null) {
			inputParams.put(DB_IN_CREATED_BY, userId);
		} else {
			inputParams.put(DB_IN_SOURCE_PROGRAM_REF_ID, sourceProgramRef.getId());
			inputParams.put(DB_IN_UPDATED_BY, userId);
		}
		return inputParams;
	}
	
	/** Update the default Email program mapping to each source code inserted (default program mapping is done by trigger)
	 * @param emailProgramId
	 * @param sourceCode
	 * @throws Exception
	 */
	public static void updateDefaultSourceEmailProgram(String emailProgramId, String sourceCode) throws Exception {
		Map inputParams = new HashMap(2);
		inputParams.put(DB_IN_SOURCE_CODE, sourceCode);
		inputParams.put("IN_PROGRAM_ID", emailProgramId);
		try {			
			Map result = new HashMap(0);
			String sqlStatement = null;
			sqlStatement = DB_INS_UPD_PRGM_TO_SRC_MAPPING;
			
			result = (Map) UtilDAO.submitDbRequest(sqlStatement, inputParams, true);			 

			String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
			if ((status != null) && status.equals("N")) {
				throw new FtdDataException((String) result.get(MarketingConstants.DB_MESSAGE_PARAM));
			}

		} catch (SQLException e) {			
			throw new FtdDataException("persistSourceProgram: Could not update email program and source code mapping into database.", e);
		}
	}
	
	/** Generates the next sympathy source code using sequence defined for each partner.
	 * @param partner
	 * @return
	 * @throws Exception
	 */
	public static int generateInternetSourceCode(String partner) throws Exception {
		Map inputParams = new HashMap(1);
		BigDecimal sympathySC  = new BigDecimal(0);
		DataRequest dataRequest = new DataRequest();
		inputParams.put("IN_PARTNER_NAME", partner);
		try {
			sympathySC = (BigDecimal) UtilDAO.submitDbRequest("GENERATE_NEXT_SYMPATHY_SC_SEQ",	inputParams, true);

		} catch (Exception e) {
			logger.error("Error caught generating sympathy SC: " + e.getMessage());
			throw new Exception("Error caught generating sympathy SC : " + e.getMessage());
		}
		return sympathySC.intValue();
	}
	
	/** Gets the default source code detail.
	 * @param sourceCode
	 * @param orderSource
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws FtdDataException
	 * @throws Exception
	 */
	public static SourceVO getSourceCodeDetail(String sourceCode, String orderSource) throws IOException,
		ParserConfigurationException, SAXException, FtdDataException, Exception {
	
		logger.info(" :: getSourceCodeDetail ::");
		
		Map inputParams = new HashMap();
		inputParams.put("IN_SOURCE_CODE", sourceCode);
		inputParams.put("IN_ORDER_SOURCE", orderSource);
		
		SourceVO sourceVO = null;
		try {
			CachedResultSet rs = (CachedResultSet) UtilDAO.submitDbRequest(GET_SOURCE_CODE_DETAIL, inputParams, false);
			if (rs.next()) {
				sourceVO = new SourceVO();
				populateSourceMasterDetail(rs, sourceVO);
				populateSourcePartnerBinDetail(rs, sourceVO);
				populateSourceProgRefDetail(rs, sourceVO);
			}
		} catch (Exception e) {
			String errorMsg = "getSourceCodeDetail: Could not retrieve source code detail from database for source code: " + sourceCode;
			logger.warn(errorMsg, e);
			throw new FtdDataException(errorMsg, e);
		}
		return sourceVO;
	}
	
	/** Populates SourceVO using cursor returned.
	 * @param result
	 * @param sourceVO
	 */
	private static void populateSourceMasterDetail(CachedResultSet result, SourceVO sourceVO) {
		sourceVO.setId(result.getString("SOURCE_CODE")); 
		sourceVO.setDescription(result.getString("DESCRIPTION"));                                                                      
		sourceVO.setSnhId(result.getString("SNH_ID")); 
		sourceVO.setPriceHeaderId(result.getString("PRICE_HEADER_ID")); 
		sourceVO.setSendToScrub(result.getString("SEND_TO_SCRUB")); 
		sourceVO.setFraudFlag(result.getString("FRAUD_FLAG")); 
		sourceVO.setEnableLpProcessing(result.getString("FRAUD_FLAG"));
		sourceVO.setEmergencyTextFlag(result.getString("EMERGENCY_TEXT_FLAG")); 
		sourceVO.setRequiresDeliveryConfirmation(result.getString("REQUIRES_DELIVERY_CONFIRMATION")); 
		sourceVO.setUpdatedOnEnd(result.getDate("UPDATED_ON")); 
		sourceVO.setUpdatedBy(result.getString("UPDATED_BY")); 
		sourceVO.setSourceType(result.getString("SOURCE_TYPE"));
		sourceVO.setStartDate(result.getDate("START_DATE")); 
		sourceVO.setEndDate(result.getDate("END_DATE"));  
		sourceVO.setPaymentMethodId(result.getString("PAYMENT_METHOD_ID"));
		sourceVO.setDefaultSourceCodeFlag(result.getString("DEFAULT_SOURCE_CODE_FLAG")); 
		sourceVO.setHighlightDescriptionFlag(result.getString("HIGHLIGHT_DESCRIPTION_FLAG")); 
		sourceVO.setDiscountAllowedFlag(result.getString("DISCOUNT_ALLOWED_FLAG")); 
		sourceVO.setBinNumberCheckFlag(result.getString("BIN_NUMBER_CHECK_FLAG")); 
		sourceVO.setOrderSource(result.getString("ORDER_SOURCE")); 
		sourceVO.setCompanyId(result.getString("COMPANY_ID"));   
		sourceVO.setPromotionCode(result.getString("PROMOTION_CODE")); 
		sourceVO.setBonusPromotionCode(result.getString("BONUS_PROMOTION_CODE")); 
		sourceVO.setRequestedBy(result.getString("REQUESTED_BY")); 
		sourceVO.setRelatedSourceCode(result.getString("RELATED_SOURCE_CODE")); 
		sourceVO.setProgramWebsiteUrl(result.getString("PROGRAM_WEBSITE_URL"));
		sourceVO.setCommentText(result.getString("COMMENT_TEXT"));           
		sourceVO.setPartnerBankId(result.getString("PARTNER_BANK_ID"));        
		sourceVO.setWebloyaltyFlag(result.getString("WEBLOYALTY_FLAG")); 
		sourceVO.setIotwFlag(result.getString("IOTW_FLAG"));
		sourceVO.setInvoicePassword(result.getString("INVOICE_PASSWORD"));
		sourceVO.setMpRedemptionRateId(result.getString("MP_REDEMPTION_RATE_ID")); 
		sourceVO.setAddOnFreeId(result.getString("ADD_ON_FREE_ID")); 
		sourceVO.setDisplayServiceFeeCode(result.getString("DISPLAY_SERVICE_FEE_CODE"));
		sourceVO.setDisplayShippingFeeCode(result.getString("DISPLAY_SHIPPING_FEE_CODE"));
		sourceVO.setRandomWeightedFlag(result.getString("PRIMARY_BACKUP_RWD_FLAG"));     
		sourceVO.setSurchargeDescription(result.getString("SURCHARGE_DESCRIPTION"));
		sourceVO.setDisplaySurcharge(result.getString("DISPLAY_SURCHARGE_FLAG")); 
		sourceVO.setSurchargeAmount(result.getDouble("SURCHARGE_AMOUNT"));       
		sourceVO.setApplySurchargeCode(result.getString("APPLY_SURCHARGE_CODE")); 
		sourceVO.setAllowFreeShippingFlag(result.getString("ALLOW_FREE_SHIPPING_FLAG")); 
		sourceVO.setSameDayUpcharge(result.getString("SAME_DAY_UPCHARGE")); 
		sourceVO.setDisplaySameDayUpcharge(result.getString("DISPLAY_SAME_DAY_UPCHARGE")); 
		sourceVO.setMorningDeliveryFlag(result.getString("MORNING_DELIVERY_FLAG"));
		sourceVO.setCalculateTaxFlag(result.getString("CALCULATE_TAX_FLAG")); 
		sourceVO.setShippingCarrierFlag(result.getString("CUSTOM_SHIPPING_CARRIER"));
		sourceVO.setMorningDeliveryToFSMembers(result.getString("MORNING_DELIVERY_FREE_SHIPPING")); 
		sourceVO.setDeliveryFeeId(result.getString("DELIVERY_FEE_ID")); 
		sourceVO.setAutomatedPromotionEngineFlag(result.getString("AUTO_PROMOTION_ENGINE"));
		sourceVO.setEmailProgramId(result.getString("EMAIL_PROGRAM_ID"));
		sourceVO.setOscarSelectionEnabledFlag(result.getString("OSCAR_SELECTION_ENABLED_FLAG"));
		sourceVO.setOscarScenarioGroupId(result.getString("OSCAR_SCENARIO_GROUP_ID"));
		sourceVO.setGiftCertificateFlag(result.getString("GIFT_CERTIFICATE_FLAG"));
		sourceVO.setBillingInfoFlag(result.getString("BILLING_INFO_FLAG"));
		sourceVO.setEnableLpProcessing(result.getString("ENABLE_LP_PROCESSING"));
		sourceVO.setFuneralCemeteryLocChk(result.getString("FUNERAL_CEMETERY_LOC_CHK"));
		sourceVO.setFuneralLeadTimeChck(result.getString("FUNERAL_CEMETERY_LEAD_TIME_CHK"));
		sourceVO.setHospitalLocChck(result.getString("HOSPITAL_LOC_CHCK"));
		sourceVO.setFuneralLeadTime(result.getString("FUNERAL_CEMETERY_LEAD_TIME"));
		sourceVO.setBoHrsMonFriStart(result.getString("BO_HRS_MON_FRI_START"));
		sourceVO.setBoHrsMonFriEnd(result.getString("BO_HRS_MON_FRI_END"));
		sourceVO.setBoHrsSatStart(result.getString("BO_HRS_SAT_START"));
		sourceVO.setBoHrsSatEnd(result.getString("BO_HRS_SAT_END"));
		sourceVO.setBoHrsSunStart(result.getString("BO_HRS_SUN_START"));
		sourceVO.setBoHrsSunEnd(result.getString("BO_HRS_SUN_END"));
		sourceVO.setLegacyId(result.getString("LEGACY_ID"));
		sourceVO.setMerchAmtFullRefundFlag(result.getString("MERCH_AMT_FULL_REFUND_FLAG"));
		//#SGC-2
		sourceVO.setSameDayUpchargeFS(result.getString("SAME_DAY_UPCHARGE_FS"));
		//logger.info(" setMechAmtFullRefundFlag "+result.getString("MERCH_AMT_FULL_REFUND_FLAG"));
	}
	
	/** Populates sourceVO (Bin mapping details) using cursor returned.
	 * @param rs
	 * @param sourceVO
	 */
	private static void populateSourcePartnerBinDetail(CachedResultSet rs, SourceVO sourceVO) {
		BinCheckPartnerVO partnerVO = new BinCheckPartnerVO();
		partnerVO.setPartnerName(rs.getString("PARTNER_NAME"));
		partnerVO.setActive("Y".equals(rs.getString("BIN_PROCESSING_ACTIVE_FLAG")) ? true : false);
		if(sourceVO.getBinCheckPartnerList() == null) {
			sourceVO.setBinCheckPartnerList(new ArrayList<BinCheckPartnerVO>());
		}
		sourceVO.getBinCheckPartnerList().add(partnerVO);	
	}	
	
	/** Populates sourceVO (source program reference mapping) using cursor returned.
	 * @param rs
	 * @param sourceVO
	 */
	private static void populateSourceProgRefDetail(CachedResultSet rs, SourceVO sourceVO) {
		if(rs.getString("PROGRAM_NAME") != null) {
			SourceProgramRefVO programRefVO = new SourceProgramRefVO(sourceVO.getId(), 
					rs.getString("PROGRAM_NAME"), rs.getDate("START_DATE"), rs.getString("SOURCE_PROGRAM_REF_ID"));
			sourceVO.getSrcProgramList().add(programRefVO);
		}
	}	
	
	
	/** Updates the primary and secondary florists information for a given source code.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void updateSCFlorists(SourceVO sourceVO) throws Exception {
		
		logger.info(" :: updateSCFlorists ::");	
		
		Connection dbConn = UtilDAO.getDbConnection();
		dbConn.setAutoCommit(false);
		
		Map inputParams = new HashMap();
		Map result = new HashMap(0);
		String status = null;
				
		
		ArrayList<SourceFloristPriorityVO> scFloristsList = sourceVO.getSourceFloristPriorityList();
				
		try {
			// Delete the Existing florists mapping of a source code and map new florists.
			inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
			if(logger.isDebugEnabled()) {
				logger.debug("Deleting Florists associated with source code : " + sourceVO.getId());
			}			
			result = (Map) UtilDAO.submitDbRequest("MARKETING.DELETE_SC_FLORISTS", inputParams, dbConn);
			if (StringUtils.isEmpty((String) result.get(DB_OUT_STATUS))	|| result.get(DB_OUT_STATUS).equals("N")) {
				//Skip the error SQL not found
				if(result.get(DB_OUT_MESSAGE).toString().indexOf("SQL NOT FOUND") < 0) {
					throw new Exception("updateSCFlorists: Could not delete existing source florist priority record from database."	+ result.get(DB_OUT_MESSAGE));
				}				
			}
			
			if(logger.isDebugEnabled()) {
				logger.debug("Inserting new Florists for source code : " + sourceVO.getId());
			}
			for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
				inputParams = new HashMap();
				inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
				inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
				inputParams.put(DB_IN_PRIORITY, sourceFloristPriorityVO.getPriority());
				inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
				result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_FLORIST_PRIORITY, inputParams, dbConn);
				
				status = (String) result.get(DB_OUT_STATUS);
				if ((status != null) && status.equals("N")) {
					dbConn.rollback();
					throw new Exception("updateSCFlorists: Could not insert florists priority record into database."	+ result.get(DB_OUT_MESSAGE));					
				}
			}
			
			// Delete the Existing florists mapping of a related source code and map new florists, if the source code is not of Legacy type.
			if(sourceVO.getSourceType() != null && SourceCodeConstants.LEGACY_PARTNER.indexOf(sourceVO.getSourceType()) < 0) {
				inputParams = new HashMap();			
				inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getRelatedSourceCode());
				if(logger.isDebugEnabled()) {
					logger.debug("Deleting Florists associated with related phone source code : " + sourceVO.getRelatedSourceCode());
				}			
				result = (Map) UtilDAO.submitDbRequest("MARKETING.DELETE_SC_FLORISTS", inputParams, dbConn);
				if (StringUtils.isEmpty((String) result.get(DB_OUT_STATUS))	|| result.get(DB_OUT_STATUS).equals("N")) {
					//Skip the error SQL not found
					if(result.get(DB_OUT_MESSAGE).toString().indexOf("SQL NOT FOUND") < 0) {
						throw new Exception("updateSCFlorists: Could not delete existing source florist priority record from database for related source code."	+ result.get(DB_OUT_MESSAGE));
					}				
				}				
			
				if(logger.isDebugEnabled()) {
					logger.debug("Inserting new Florists for realted source code : " + sourceVO.getRelatedSourceCode());
				}
				for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
					inputParams = new HashMap();
					inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getRelatedSourceCode());
					inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
					inputParams.put(DB_IN_PRIORITY, sourceFloristPriorityVO.getPriority());
					inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
					result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_FLORIST_PRIORITY, inputParams, dbConn);
					
					status = (String) result.get(DB_OUT_STATUS);
					if ((status != null) && status.equals("N")) {
						dbConn.rollback();
						throw new Exception("updateSCFlorists: Could not insert florists priority record into database for related phone source code."	+ result.get(DB_OUT_MESSAGE));					
					}
				}
			}
			dbConn.commit();		

		} catch (SQLException e) {
			dbConn.rollback();
			throw new Exception("updateSCFlorists: Could not insert/update existing source florist priority record from database.", e);			
		} 
	}
	
	/** Adds the primary and secondary florists information for a given source code.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void addSCFlorists(SourceVO sourceVO) throws Exception {
		
		logger.info(" :: addSCFlorists ::");	
		
		Connection dbConn = UtilDAO.getDbConnection();
		dbConn.setAutoCommit(false);
		
		Map inputParams = new HashMap();
		Map result = new HashMap(0);
		String status = null;				
		
		ArrayList<SourceFloristPriorityVO> scFloristsList = sourceVO.getSourceFloristPriorityList();
				
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("Adding new Florists for source code : " + sourceVO.getId());
			}
			// Insert the florists chosen into source_florist_priority
			for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
				inputParams = new HashMap();
				inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
				inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
				inputParams.put(DB_IN_PRIORITY, sourceFloristPriorityVO.getPriority());
				inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
				result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_FLORIST_PRIORITY, inputParams, dbConn);
				
				status = (String) result.get(DB_OUT_STATUS);
				if ((status != null) && status.equals("N")) {
					dbConn.rollback();
					throw new Exception("addSCFlorists: Could not insert florists priority record into database."	+ result.get(DB_OUT_MESSAGE));					
				}
			}
			
			// Insert the florists chosen into source_florist_priority for related phone source code also, if the source code is not of the type legacy.
			if(sourceVO.getSourceType() != null && SourceCodeConstants.LEGACY_PARTNER.indexOf(sourceVO.getSourceType()) < 0) {
				if(logger.isDebugEnabled()) {
					logger.debug("Adding new Florists for realted source code : " + sourceVO.getRelatedSourceCode());
				}
				
				for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
					inputParams = new HashMap();
					inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getRelatedSourceCode());
					inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
					inputParams.put(DB_IN_PRIORITY, sourceFloristPriorityVO.getPriority());
					inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
					result = (Map) UtilDAO.submitDbRequest(DB_INSERT_SOURCE_FLORIST_PRIORITY, inputParams, dbConn);
					
					status = (String) result.get(DB_OUT_STATUS);
					if ((status != null) && status.equals("N")) {
						dbConn.rollback();
						throw new Exception("addSCFlorists: Could not insert florists priority record into database."	+ result.get(DB_OUT_MESSAGE));					
					}
				}
			}
			dbConn.commit();		

		} catch (SQLException e) {
			dbConn.rollback();
			throw new Exception("addSCFlorists: Could not insert source florist priority record from database.", e);			
		} 
	}
	
	/** Deletes the primary or secondary florists information for a given source code.
	 * @param sourceVO
	 * @throws Exception
	 */
	public static void deleteSCFlorists(SourceVO sourceVO) throws Exception {
		
		logger.info(" :: deleteSCFlorists ::");	
		
		Connection dbConn = UtilDAO.getDbConnection();
		dbConn.setAutoCommit(false);
		
		Map inputParams = new HashMap();
		Map result = new HashMap(0);
		String status = null;
		
		ArrayList<SourceFloristPriorityVO> scFloristsList = sourceVO.getSourceFloristPriorityList();
		
		try{
			if (logger.isDebugEnabled()) {
                logger.debug("Deleting all the florists associated with : " + sourceVO.getId());
            }
			for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
				if (logger.isDebugEnabled()) {
	                logger.debug("Deleting sourcecode/florist : " + sourceVO.getId() + "/" + sourceFloristPriorityVO.getFloristId());
	            }
				
				 inputParams = new HashMap();
				 inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getId());
				 inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
				 inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
				 result = (Map) UtilDAO.submitDbRequest(DB_DELETE_SOURCE_FLORIST_PRIORITY, inputParams, dbConn); 
				 status = (String) result.get(DB_OUT_STATUS);
				 
				 if ((status != null) && status.equals("N")) {
					 String errorMsg = "deleteSCFlorists: Error caught deleting florist priority record from database: " + result.get(DB_OUT_MESSAGE);
				     logger.error(errorMsg);
				     throw new FtdDataException((String) result.get(DB_OUT_MESSAGE));
				 }							
			}   
			
			if(sourceVO.getSourceType() != null && SourceCodeConstants.LEGACY_PARTNER.indexOf(sourceVO.getSourceType()) < 0) {
				if (logger.isDebugEnabled()) {
	                logger.debug("Deleting all the florists associated with realted source code: " + sourceVO.getRelatedSourceCode());
	            }
				for (SourceFloristPriorityVO sourceFloristPriorityVO : scFloristsList) {				
					if (logger.isDebugEnabled()) {
		                logger.debug("Deleting sourcecode/florist : " + sourceVO.getId() + "/" + sourceFloristPriorityVO.getFloristId());
		            }
					
					 inputParams = new HashMap();
					 inputParams.put(DB_IN_SOURCE_CODE, sourceVO.getRelatedSourceCode());
					 inputParams.put(DB_IN_FLORIST_ID, sourceFloristPriorityVO.getFloristId());
					 inputParams.put(DB_IN_UPDATED_BY, sourceVO.getUpdatedBy());
					 result = (Map) UtilDAO.submitDbRequest(DB_DELETE_SOURCE_FLORIST_PRIORITY, inputParams, dbConn); 
					 status = (String) result.get(DB_OUT_STATUS);
					 
					 if ((status != null) && status.equals("N")) {
						 String errorMsg = "deleteSCFlorists: Error caught deleting florist priority record from database for related soruce code: " + result.get(DB_OUT_MESSAGE);
					     logger.error(errorMsg);
					     throw new FtdDataException((String) result.get(DB_OUT_MESSAGE));
					 }							
				} 
			}
			dbConn.commit();
		} catch (SQLException e) {
            String errorMsg = "deleteSCFlorists: Could not delete florist priority record from database.";
            logger.error(errorMsg, e);               
            throw new FtdDataException(errorMsg, e);
        }
       
	}
	
	/** Retrieves primary and secondary florists for a given source code.
	 * @param sourceCode
	 * @return
	 * @throws Exception
	 */
	public static List<Florist> getSCFlorists(String sourceCode) throws Exception {
		logger.info(" :: getSCFlorists ::");

		Map inputParams = new HashMap();
		inputParams.put("IN_SOURCE_CODE", sourceCode);

		List<Florist> florists = new ArrayList<Florist>();
		Florist florist = null;
		try {
			CachedResultSet rs = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.GET_SOURCE_CODE_FLORISTS", inputParams, false);
			while (rs.next()) {
				florist = new Florist();
				florist.setFloristMemeberId(rs.getString("florist_id"));
				florist.setFloristName(rs.getString("florist_name"));
				florist.setAddressLine(rs.getString("address"));
				florist.setCity(rs.getString("city"));
				florist.setState(rs.getString("state"));
				florist.setZipCode(rs.getString("zip_code"));
				florist.setPhoneNumber(rs.getString("phone_number"));
				florist.setLatitude(rs.getString("latitude"));
				florist.setLongitude(rs.getString("longitude"));
				try{
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");				
					florist.setLastUpdatedOn(df.parse(rs.getString("last_updated_date")));
				} catch(Exception e) {
					logger.error("getSCFlorists:Unable to parse date : "+ rs.getString("last_updated_date"));
				}
				
				if(rs.getString("priority").equals("1")) {
					florist.setPrimary(true);
				}
				florists.add(florist);
			}
		} catch (Exception e) {
			String errorMsg = "getSCFlorists: Could not retrieve source code florist detail from database for source code: "
					+ sourceCode;
			logger.warn(errorMsg, e);
			throw new Exception(errorMsg, e);
		}
		return florists;
	}


    
    /****************** Sympathy Code changes end here ***********************/
	
    public static ArrayList getApeBaseSourceCodes(String sourceCode) 
    throws FtdDataException, Exception {
    
	    Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    ArrayList baseSourceCodesList = new ArrayList();
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.GET_APE_BASE_SOURCE_CODES",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  baseSourceCodesList.add(result.getString("BASE_SOURCE_CODE"));
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "getApeBaseSourceCodes: Could not retrieve APE base source codes from database for source code: " + sourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return baseSourceCodesList;
    }
    
    public static void insertApeBaseSourceCodes(
            String sourceCode, String apeBaseSourceCodes, String updatedBy)
            throws FtdDataException, Exception {
           
                // Build db stored procedure input parms.
                Map inputParams = new HashMap();
                inputParams.put("IN_SOURCE_CODE", sourceCode);
                inputParams.put("IN_BASE_SOURCE_CODE", apeBaseSourceCodes);
                inputParams.put("IN_UPDATED_BY", updatedBy);

                // Submit the request to the database.
                Map result = new HashMap(0);

                try {
                    result = (Map) UtilDAO.submitDbRequest("MARKETING.INSERT_APE_BASE_SOURCE_CODES",
                            inputParams, true);
                } catch (SQLException e) {
                    String errorMsg = "insertApeBaseSourceCodes: Could not insert base source codes into database.";
                    logger.warn(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                // Check the return code from the stored procedure.
                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }

    }
    
    public static void deleteApeBaseSourceCodes(
            String sourceCode, String apeBaseSourceCodes, String updatedBy)
            throws FtdDataException, Exception {
           
                // Build db stored procedure input parms.
                Map inputParams = new HashMap();
                inputParams.put("IN_SOURCE_CODE", sourceCode);
                inputParams.put("IN_BASE_SOURCE_CODE", apeBaseSourceCodes);
                // Submit the request to the database.
                Map result = new HashMap(0);

                try {
                    result = (Map) UtilDAO.submitDbRequest("MARKETING.DELETE_APE_BASE_SOURCE_CODES",
                            inputParams, true);
                } catch (SQLException e) {
                    String errorMsg = "deleteApeBaseSourceCodes: Could not delete base source codes from database.";
                    logger.warn(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                // Check the return code from the stored procedure.
                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }

    }

	public static boolean isIOTWSourceCode(String sourceCode) throws FtdDataException, Exception {
    
	    Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    boolean iowtSourceCode = false;
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.IS_IOTW_SOURCE_CODE",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  iowtSourceCode = true;
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "isIOTWSourceCode: Could not check if source code is IOTW: " + sourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return iowtSourceCode;
	}
	
	public static boolean isInvalidSourceCode(String sourceCode) throws FtdDataException, Exception {
	    
	    Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    boolean invalidSourceCode = true;
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.IS_INVALID_SOURCE_CODE",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  invalidSourceCode = false;
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "isInvalidSourceCode: Could not check if source code is valid: " + sourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return invalidSourceCode;
	}
	
	public static boolean isApeEnabledSourceCode(String sourceCode) throws FtdDataException, Exception {
	    
	    Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    boolean apeEnabledSourceCode = false;
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.IS_APE_ENABLED_SOURCE_CODE",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  apeEnabledSourceCode = true;
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "isApeEnabledSourceCode: Could not check if source code is ape enabled: " + sourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return apeEnabledSourceCode;
	}
	
	public static boolean milesPointsAssigned(String sourceCode) throws FtdDataException, Exception {
	    
	    Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", sourceCode);
	    boolean milesPoints = false;
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.IS_MILES_POINTS_SOURCE_CODE",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  milesPoints = true;
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "milesPointsAssigned : Could not check if source code has miles/points assigned: " + sourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return milesPoints;
	}

	public static ArrayList checkBaseSourceCode(String requestSourceCode) throws FtdDataException, Exception{
		Map inputParams = new HashMap();
	    inputParams.put("IN_SOURCE_CODE", requestSourceCode);
	    ArrayList<String> masterSourceCodesList = new ArrayList<String>();
	    try {
	      CachedResultSet result = (CachedResultSet) UtilDAO.submitDbRequest("MARKETING.CHECK_BASE_SOURCE_CODE",
	                inputParams, false);
	      
	      while(result!=null && result.next()) {
	    	  masterSourceCodesList.add(result.getString("MASTER_SOURCE_CODE"));
	      }                    
	    } catch (Exception e) {
	        String errorMsg = "checkBaseSourceCode : Could not check if source code is a base source code: " + requestSourceCode;
	        logger.warn(errorMsg, e);
	        throw new FtdDataException(errorMsg, e);
	    }
	
	    return masterSourceCodesList;
	}

	public static void setAutoPromotionEngineFlag(String requestSourceCode) throws FtdDataException, Exception {
        
        // Build db stored procedure input parms.
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", requestSourceCode);
        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest("MARKETING.UPDATE_APE_FLAG",
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "setAutoPromotionEngineFlag: Could not set auto promotion engine flag for: "+requestSourceCode;
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
	}

	public static void deleteSCDependencies(SourceCodeRequest request) throws FtdDataException,Exception {
		Map inputParams = new HashMap();
		String status = null;
        inputParams.put("IN_SOURCE_CODE", request.getSourceCode());
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest("MARKETING.DELETE_SC_DEPENDENCY",
                    inputParams, true);
            // Check the return code from the stored procedure.
            status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
        } catch (SQLException e) {
            String errorMsg = "Error deleting dependency for the source code : "+request.getSourceCode();
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
        
        
	}

	public static void updateSCAttributes(
			UpdateSourceCodeAttributesRequest request) throws FtdDataException,Exception {
		Map inputParams = new HashMap();
		String status = null;
        inputParams.put("IN_SOURCE_CODE", request.getSourceCode());
        inputParams.put("IN_LOCATION", request.getLocation());
        inputParams.put("IN_ADDRESS", request.getAddress());
        inputParams.put("IN_CITY", request.getCity());
        inputParams.put("IN_STATE", request.getState());
        inputParams.put("IN_ZIPCODE", request.getZipcode());
        inputParams.put("IN_COUNTRY", request.getCountry());
        inputParams.put("IN_PHONE_NUMBER", request.getPhoneNumber());
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_SC_ATTRIBUTES,
                    inputParams, true);
            // Check the return code from the stored procedure.
            status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);
        } catch (SQLException e) {
            String errorMsg = "Error updating attributes for the source code : "+request.getSourceCode();
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }
		
	}
}
