package com.ftd.enterprise.dao;

import com.ftd.enterprise.vo.AaaClubVO;
import com.ftd.enterprise.vo.CostCenterVO;
import com.ftd.enterprise.vo.ProgramRewardRangeVO;
import com.ftd.enterprise.vo.ProgramRewardVO;
import com.ftd.marketing.constants.MarketingConstants;
import com.ftd.marketing.exception.FtdDataException;
import com.ftd.marketing.vo.ProgramRewardRequestVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class PartnerProgramDAO {
    protected static Log logger = LogFactory.getLog(PartnerProgramDAO.class.getName());
    protected final static String DB_UPDATE_AAA_MEMBERS = "MARKETING.UPDATE_AAA_MEMBERS";
    protected final static String DB_SELECT_AAA_MEMBERS = "MARKETING.SELECT_AAA_MEMBERS";
    protected final static String DB_UPDATE_PARTNER_PROGRAM = "MARKETING.UPDATE_PARTNER_PROGRAM";
    protected final static String DB_UPDATE_PROGRAM_REWARD = "MARKETING.UPDATE_PROGRAM_REWARD";
    protected final static String DB_UPDATE_PROGRAM_REWARD_RANGE = "MARKETING.UPDATE_PROGRAM_REWARD_RANGE";
    protected final static String DB_SELECT_PARTNER_PROGRAM = "MARKETING.SELECT_PARTNER_PROGRAM";

    //    protected final static String DB_UPDATE_PROGRAM_REWARD_BONUS = "MARKETING.UPDATE_PROGRAM_REWARD_BONUS";
    protected final static String DB_SELECT_PROGRAM_REWARD = "MARKETING.SELECT_PROGRAM_REWARD";
    protected final static String DB_SELECT_PROGRAM_REWARD_RANGE = "MARKETING.SELECT_PROGRAM_REWARD_RANGE";

    //    protected final static String DB_SELECT_PROGRAM_REWARD_BONUS = "MARKETING.SELECT_PROGRAM_REWARD_BONUS";
    //    protected final static String DB_SELECT_PROGRAM_REWARD_BONUS_GROUP = "MARKETING.SELECT_PROGRAM_REWARD_BONUS_GROUP";
    protected final static String DB_IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
    protected final static String DB_IN_PROGRAM_LONG_NAME = "IN_PROGRAM_LONG_NAME";
    protected final static String DB_IN_PARTNER_NAME = "IN_PARTNER_NAME";
    protected final static String DB_IN_PROGRAM_TYPE = "IN_PROGRAM_TYPE";
    protected final static String DB_IN_EMAIL_EXCLUDE_FLAG = "IN_EMAIL_EXCLUDE_FLAG";
    protected final static String DB_IN_MEMBERSHIP_DATA_REQUIRED = "IN_MEMBERSHIP_DATA_REQUIRED";
    protected final static String DB_IN_CALCULATION_BASIS = "IN_CALCULATION_BASIS";
    protected final static String DB_IN_POINTS = "IN_POINTS";
    protected final static String DB_IN_REWARD_TYPE = "IN_REWARD_TYPE";
    protected final static String DB_IN_CUSTOMER_INFO = "IN_CUSTOMER_INFO";
    protected final static String DB_IN_REWARD_NAME = "IN_REWARD_NAME";
    protected final static String DB_IN_PARTICIPANT_ID_LENGTH_CHECK = "IN_PARTICIPANT_ID_LENGTH_CHECK";
    protected final static String DB_IN_PARTICIPANT_EMAIL_CHECK = "IN_PARTICIPANT_EMAIL_CHECK";
    protected final static String DB_IN_MIN_DOLLAR = "IN_MIN_DOLLAR";
    protected final static String DB_IN_MAX_DOLLAR = "IN_MAX_DOLLAR";
    protected final static String DB_IN_START_DATE = "IN_START_DATE";
    protected final static String DB_IN_DATE = "IN_DATE";
    protected final static String DB_IN_END_DATE = "IN_END_DATE";
    protected final static String DB_IN_MAXIMUM_POINTS = "IN_MAXIMUM_POINTS";
    protected final static String DB_IN_UPDATED_BY = "IN_UPDATED_BY";

    // AAA Club metrics.
    protected final static String DB_IN_AAA_MEMBER_ID = "IN_AAA_MEMBER_ID";
    protected final static String DB_IN_MEMBER_NAME = "IN_MEMBER_NAME";

    // Cost Center metrics.
    protected final static String DB_INSERT_COST_CENTERS = "MARKETING.INSERT_COST_CENTERS";
    protected final static String DB_SELECT_COST_CENTERS = "MARKETING.SELECT_COST_CENTERS";
    protected final static String DB_IN_COST_CENTER_ID = "IN_COST_CENTER_ID";
    protected final static String DB_IN_PARTNER_ID = "IN_PARTNER_ID";
    protected final static String DB_IN_SOURCE_CODE_ID = "IN_SOURCE_CODE_ID";

    // Bonus metrics
    protected final static String DB_IN_BONUS_CALCULATION_BASIS = "IN_BONUS_CALCULATION_BASIS";
    protected final static String DB_IN_BONUS_POINTS = "IN_BONUS_POINTS";
    protected final static String DB_IN_BONUS_SEPARATE_DATA = "IN_BONUS_SEPARATE_DATA";

    /**
     * constructor to prevent clients from instantiating.
     */
    private PartnerProgramDAO() {
        super();
    }

    public static Document queryProgramReward(
        ProgramRewardRequestVO programRewardRequestVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
    	Document result = DOMUtil.getDefaultDocument();

        if ((programRewardRequestVO != null) &&
                (programRewardRequestVO.getProgramRewardVO() != null)) {
            ProgramRewardVO prVO = programRewardRequestVO.getProgramRewardVO();
            Map inputParams = new HashMap(5);

            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_NAME,
                prVO.getProgramName());
            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_TYPE,
                prVO.getProgramType());
            UtilDAO.addSearchFilter(inputParams, DB_IN_CALCULATION_BASIS,
                prVO.getCalculationBasis());
            UtilDAO.addSearchFilter(inputParams, DB_IN_POINTS, prVO.getPoints());
            UtilDAO.addSearchFilter(inputParams, DB_IN_REWARD_TYPE,
                prVO.getRewardType());

            // Submit the request to the database.
            try {
                result = (Document) UtilDAO.submitDbRequest(DB_SELECT_PROGRAM_REWARD,
                        inputParams, false);
            } catch (SQLException e) {
                String errorMsg = "queryProgramReward: Could not obtain program reward from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        }

        return result;
    }

    public static Document queryProgramRewardRange(
        ProgramRewardRequestVO programRewardRequestVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
    	Document result =  DOMUtil.getDefaultDocument();

        if ((programRewardRequestVO != null) &&
                (programRewardRequestVO.getProgramRewardVO() != null)) {
            ProgramRewardVO prVO = programRewardRequestVO.getProgramRewardVO();
            Map inputParams = new HashMap(1);

            UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_NAME,
                prVO.getProgramName());

            // Submit the request to the database.
            try {
                return (Document) UtilDAO.submitDbRequest(DB_SELECT_PROGRAM_REWARD_RANGE,
                    inputParams, false);
            } catch (SQLException e) {
                String errorMsg = "queryProgramRewardRange: Could not obtain program reward range from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        }

        return result;
    }

    /*    public static XMLDocument queryProgramRewardBonus(
            ProgramRewardBonusRequestVO programRewardBonusRequestVO)
            throws IOException, ParserConfigurationException, SAXException,
                FtdDataException, Exception {
            XMLDocument result = new XMLDocument();

            if ((programRewardBonusRequestVO != null) &&
                    (programRewardBonusRequestVO.getProgramRewardBonusVO() != null)) {
                ProgramRewardBonusVO prbVO = programRewardBonusRequestVO.getProgramRewardBonusVO();
                Map inputParams = new HashMap(2);

                UtilDAO.addSearchFilter(inputParams, DB_IN_PROGRAM_NAME,
                    prbVO.getId());

                String statementId = DB_SELECT_PROGRAM_REWARD_BONUS_GROUP;

                if (prbVO.getStartDate() != null) {
                    UtilDAO.addSearchFilter(inputParams, DB_IN_DATE,
                        new java.sql.Date(prbVO.getStartDate().getTime()));

                    statementId = DB_SELECT_PROGRAM_REWARD_BONUS;
                }

                // Submit the request to the database.
                try {
                    return (XMLDocument) UtilDAO.submitDbRequest(statementId,
                        inputParams, false);
                } catch (SQLException e) {
                    String errorMsg = "queryProgramRewardBonus: Could not obtain program reward bonus from database.";
                    logger.error(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }
            }

            return result;
        }
    */
    public static ProgramRewardRequestVO persistCompositeRewardProgram(
        ProgramRewardRequestVO programRewardRequestVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Connection dbConn = UtilDAO.getDbConnection();

        try {
            // Insert records into the appropriate partner program tables
            // based on referential integrity.
            persistPartnerProgram(programRewardRequestVO, dbConn);

            // Test if the record is a rewards program.
            if ((programRewardRequestVO.getProgramRewardVO() != null) &&
                    (programRewardRequestVO.getProgramRewardVO().getPoints() != null)) {
                persistProgramReward(programRewardRequestVO, dbConn);
                persistProgramRewardRange(programRewardRequestVO, dbConn);
            }

            dbConn.commit();
        } catch (Exception e) {
            dbConn.rollback();
            throw e;
        } finally {
            dbConn.close();
        }

        return programRewardRequestVO;
    }

    protected static Map persistPartnerProgram(
        ProgramRewardRequestVO programRewardRequestVO, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        ProgramRewardVO programRewardVO = programRewardRequestVO.getProgramRewardVO();

        if ((programRewardVO != null) &&
                (programRewardVO.getProgramName() != null)) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap(5);
            inputParams.put(DB_IN_PROGRAM_NAME, programRewardVO.getProgramName());
            inputParams.put(DB_IN_PARTNER_NAME, programRewardVO.getPartnerName());
            inputParams.put(DB_IN_PROGRAM_TYPE, programRewardVO.getProgramType());
            inputParams.put(DB_IN_EMAIL_EXCLUDE_FLAG,
                programRewardVO.getEmailExcludeFlag());
            inputParams.put(DB_IN_UPDATED_BY,
                programRewardRequestVO.getUserVO().getUserID());
            inputParams.put(DB_IN_PROGRAM_LONG_NAME, programRewardVO.getProgramLongName());
            inputParams.put(DB_IN_MEMBERSHIP_DATA_REQUIRED, programRewardVO.getMembershipDataRequired());

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PARTNER_PROGRAM,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "insertPartnerProgram: Could not insert partner program into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        } else {
            throw new FtdDataException(
                "insertPartnerProgram: Value object has a null required column.");
        }

        return result;
    }

    protected static Map persistProgramReward(
        ProgramRewardRequestVO programRewardRequestVO, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        ProgramRewardVO programRewardVO = programRewardRequestVO.getProgramRewardVO();

        if ((programRewardVO != null) &&
                (programRewardVO.getProgramName() != null)) {
            // Build db stored procedure input parms
            Map inputParams = new HashMap(9);
            inputParams.put(DB_IN_PROGRAM_NAME, programRewardVO.getProgramName());
            inputParams.put(DB_IN_CALCULATION_BASIS,
                programRewardVO.getCalculationBasis());
            inputParams.put(DB_IN_POINTS, programRewardVO.getPoints());
            inputParams.put(DB_IN_REWARD_TYPE, programRewardVO.getRewardType());
            inputParams.put(DB_IN_CUSTOMER_INFO,
                programRewardVO.getCustomerInfo());
            inputParams.put(DB_IN_REWARD_NAME, programRewardVO.getRewardName());
            inputParams.put(DB_IN_PARTICIPANT_ID_LENGTH_CHECK,
                programRewardVO.getParticipantIdLengthCheck());
            inputParams.put(DB_IN_PARTICIPANT_EMAIL_CHECK,
                programRewardVO.getParticipantEmailCheck());

            // Bonus data.
            if (programRewardVO.getBonusPoints() != null) {
                inputParams.put(DB_IN_BONUS_CALCULATION_BASIS,
                    programRewardVO.getBonusCalculationBasis());
                inputParams.put(DB_IN_BONUS_POINTS,
                    programRewardVO.getBonusPoints());
                inputParams.put(DB_IN_BONUS_SEPARATE_DATA,
                    programRewardVO.getBonusSeparateData());
            }

            if (programRewardVO.getMaximumPoints() != null) {
                inputParams.put(DB_IN_MAXIMUM_POINTS,
                    programRewardVO.getMaximumPoints().toString());
            }

            inputParams.put(DB_IN_UPDATED_BY,
                programRewardRequestVO.getUserVO().getUserID());

            // Submit the request to the database.
            try {
                result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PROGRAM_REWARD,
                        inputParams, dbConn);
            } catch (SQLException e) {
                String errorMsg = "insertProgramReward: Could not insert program reward into database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }

            String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

            if ((status != null) && status.equals("N")) {
                throw new FtdDataException((String) result.get(
                        MarketingConstants.DB_MESSAGE_PARAM));
            }
        } else {
            throw new FtdDataException(
                "insertProgramReward: Value object has a null required column.");
        }

        return result;
    }

    protected static Map persistProgramRewardRange(
        ProgramRewardRequestVO programRewardRequestVO, Connection dbConn)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        Map result = new HashMap(0);

        ProgramRewardVO programRewardVO = programRewardRequestVO.getProgramRewardVO();

        if ((programRewardVO != null) &&
                (programRewardVO.getProgramName() != null) &&
                (programRewardVO.getProgramRewardRangeVO() != null)) {
            if (programRewardVO.getProgramRewardRangeVO().getMaxDollar() != null) {
                ProgramRewardRangeVO programRewardRangeVO = programRewardVO.getProgramRewardRangeVO();

                // Build db stored procedure input parms
                Map inputParams = new HashMap(6);
                inputParams.put(DB_IN_PROGRAM_NAME,
                    programRewardVO.getProgramName());

                // If the calculation basis is null, inherit the value from the
                // containing object.
                inputParams.put(DB_IN_CALCULATION_BASIS,
                    (programRewardRangeVO.getCalculationBasis() == null)
                    ? programRewardVO.getCalculationBasis()
                    : programRewardRangeVO.getCalculationBasis());

                // Currently, the business need is only to support a single-range max
                // dollar value (for United Airlines).
                Float minDollar = (programRewardRangeVO.getMinDollar() == null)
                    ? new Float(0) : programRewardRangeVO.getMinDollar();
                inputParams.put(DB_IN_MIN_DOLLAR, minDollar);

                inputParams.put(DB_IN_MAX_DOLLAR,
                    programRewardRangeVO.getMaxDollar());
                inputParams.put(DB_IN_POINTS, programRewardRangeVO.getPoints());
                inputParams.put(DB_IN_UPDATED_BY,
                    programRewardRequestVO.getUserVO().getUserID());

                // Submit the request to the database.
                try {
                    result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PROGRAM_REWARD_RANGE,
                            inputParams, dbConn);
                } catch (SQLException e) {
                    String errorMsg = "insertProgramRewardRange: Could not insert program reward range into database.";
                    logger.error(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }
            } else {
                throw new FtdDataException(
                    "insertProgramRewardRange: Value object has a null required column.");
            }
        }

        return result;
    }

    /*    public static ProgramRewardBonusRequestVO persistProgramRewardBonus(
            ProgramRewardBonusRequestVO programRewardBonusRequestVO)
            throws IOException, ParserConfigurationException, SAXException,
                FtdDataException, Exception {
            Map result = new HashMap(0);

            ProgramRewardBonusVO prbVO = programRewardBonusRequestVO.getProgramRewardBonusVO();

            //        if ((programRewardVO != null) &&
            //                (programRewardVO.getProgramName() != null) &&
            //                (programRewardVO.getProgramRewardBonusVO() != null)) {
            if ((prbVO != null) && (prbVO.getStartDate() != null) &&
                    (programRewardBonusRequestVO.getUserVO() != null)) {
                //                ProgramRewardBonusVO programRewardBonusVO = programRewardVO.getProgramRewardBonusVO();
                // Build db stored procedure input parms
                Map inputParams = new HashMap(7);
                inputParams.put(DB_IN_PROGRAM_NAME, prbVO.getId());

                // If the calculation basis is null, inherit the value from the
                // containing object.
                inputParams.put(DB_IN_CALCULATION_BASIS, prbVO.getCalculationBasis());

                inputParams.put(DB_IN_START_DATE,
                    new java.sql.Date(prbVO.getStartDate().getTime()));

                if (prbVO.getEndDate() != null) {
                    inputParams.put(DB_IN_END_DATE,
                        new java.sql.Date(prbVO.getEndDate().getTime()));
                }

                inputParams.put(DB_IN_POINTS, prbVO.getPoints());
                inputParams.put(DB_IN_SEPARATE_DATA, prbVO.getSeparateData());
                inputParams.put(DB_IN_UPDATED_BY,
                    programRewardBonusRequestVO.getUserVO().getUserID());

                // Submit the request to the database.
                try {
                    result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_PROGRAM_REWARD_BONUS,
                            inputParams, true);
                } catch (SQLException e) {
                    String errorMsg = "persistProgramRewardBonus: Could not insert program reward bonus into database.";
                    logger.error(errorMsg, e);
                    throw new FtdDataException(errorMsg, e);
                }

                String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

                if ((status != null) && status.equals("N")) {
                    throw new FtdDataException((String) result.get(
                            MarketingConstants.DB_MESSAGE_PARAM));
                }
            } else {
                throw new FtdDataException(
                    "persistProgramRewardBonus: Value object has a null required column.");
            }

            //        }
            return programRewardBonusRequestVO;
        }
    */
    public static AaaClubVO persistAaaClub(AaaClubVO aaaClubVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(2);
        inputParams.put(DB_IN_AAA_MEMBER_ID, aaaClubVO.getClubCode());
        inputParams.put(DB_IN_MEMBER_NAME, aaaClubVO.getClubName());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_UPDATE_AAA_MEMBERS,
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "persistAaaClub: Could not insert AAA Club into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        // Since the result is the source code itself, simply return the input
        return aaaClubVO;
    }

    public static Document queryAaaClub(String clubCode)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {

        if (clubCode != null) {
            Map inputParams = new HashMap(1);
            UtilDAO.addSearchFilter(inputParams, DB_IN_AAA_MEMBER_ID, clubCode);

            // Submit the request to the database.
            try {
                return (Document) UtilDAO.submitDbRequest(DB_SELECT_AAA_MEMBERS,
                    inputParams, false);
            } catch (SQLException e) {
                String errorMsg = "queryAaaClub: Could not obtain AAA club from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        } else {
            throw new RuntimeException(
                "queryAaaClub: Input paramter clubCode can not be null.");
        }
    }
    
    public static Document queryPartnerPrograms(String partnerName)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {

        if (partnerName != null) {
            Map inputParams = new HashMap(1);
            UtilDAO.addSearchFilter(inputParams, DB_IN_PARTNER_NAME, partnerName);

            // Submit the request to the database.
            try {
                return (Document) UtilDAO.submitDbRequest(DB_SELECT_PARTNER_PROGRAM,
                    inputParams, false);
            } catch (SQLException e) {
                String errorMsg = "queryPartnerPrograms: Could not obtain partner program from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        } else {
            throw new RuntimeException(
                "queryPartnerPrograms: Input paramter partnerName can not be null.");
        }
    }

    public static CostCenterVO persistCostCenter(
        CostCenterVO costCenterVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {
        // Build db stored procedure input parms
        Map inputParams = new HashMap(3);
        inputParams.put(DB_IN_COST_CENTER_ID, costCenterVO.getConcatId());
        inputParams.put(DB_IN_PARTNER_ID, costCenterVO.getPartnerName());
        inputParams.put(DB_IN_SOURCE_CODE_ID, costCenterVO.getSourceCode());

        // Submit the request to the database.
        Map result = new HashMap(0);

        try {
            result = (Map) UtilDAO.submitDbRequest(DB_INSERT_COST_CENTERS,
                    inputParams, true);
        } catch (SQLException e) {
            String errorMsg = "persistCostCenter: Could not insert Cost Center into database.";
            logger.warn(errorMsg, e);
            throw new FtdDataException(errorMsg, e);
        }

        // Check the return code from the stored procedure.
        String status = (String) result.get(MarketingConstants.DB_STATUS_PARAM);

        if ((status != null) && status.equals("N")) {
            throw new FtdDataException((String) result.get(
                    MarketingConstants.DB_MESSAGE_PARAM));
        }

        // Since the result is the source code itself, simply return the input
        return costCenterVO;
    }

    public static Document queryCostCenter(CostCenterVO costCenterVO)
        throws IOException, ParserConfigurationException, SAXException, 
            FtdDataException, Exception {

        if (costCenterVO != null) {
            Map inputParams = new HashMap(2);
            UtilDAO.addSearchFilter(inputParams, DB_IN_COST_CENTER_ID,
                costCenterVO.getConcatId());
            UtilDAO.addSearchFilter(inputParams, DB_IN_PARTNER_ID,
                costCenterVO.getPartnerName());

            // Submit the request to the database.
            try {
                return (Document) UtilDAO.submitDbRequest(DB_SELECT_COST_CENTERS,
                    inputParams, false);
            } catch (SQLException e) {
                String errorMsg = "queryCostCenter: Could not obtain cost center from database.";
                logger.error(errorMsg, e);
                throw new FtdDataException(errorMsg, e);
            }
        } else {
            throw new RuntimeException(
                "queryCostCenter: Input parameter CostCenterVO can not be null.");
        }
    }
}
