package com.ftd.fj.flowerjewel;

import com.ftd.fj.flowerjewel.handler.FlowerJewelInboundHandler;
import com.ftd.fj.flowerjewel.handler.FlowerJewelOutboundHandler;
import com.ftd.osp.utilities.plugins.Logger;

import javax.ejb.MessageDrivenBean;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;

public class FlowerJewelsMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private Logger logger;
  private static String logContext = "com.ftd.fj.flowerjewel.FlowerJewelsMDB";
  private String INBOUND_QUEUE = "INBOUND";
  private String OUTBOUND_QUEUE = "OUTBOUND";
  
  public void ejbCreate() {
  }

  public void onMessage(Message msg) {
      
    try {
      
        //get message and extract order detail_id
        TextMessage textMessage = (TextMessage)msg;
        String msgData = textMessage.getText();

        logger.info("msgData: " + msgData);

        if (msgData != null) {
            if (msgData.equalsIgnoreCase(INBOUND_QUEUE)) {
                logger.info("Inbound MDB");
                FlowerJewelInboundHandler fjInbound = new FlowerJewelInboundHandler();
                fjInbound.invoke();
            } else if (msgData.equalsIgnoreCase(OUTBOUND_QUEUE)) {
                logger.info("Outbound MDB");
                FlowerJewelOutboundHandler fjOutbound = new FlowerJewelOutboundHandler();
                fjOutbound.invoke();
            }
        }
        logger.info("Finished");
    } catch (Exception e) { 
        logger.error(e);
    } catch (Throwable t) {
        logger.error("Thrown error: " + t);
    }
  }

  public void ejbRemove() {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx) {
      this.context = ctx;
      logger = new Logger(logContext);  
  }
}