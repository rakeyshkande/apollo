package com.ftd.fj.flowerjewel.constants;

public class FlowerJewelConstants 
{

    public static final String FLOWER_JEWEL_CONFIG_FILE = "flower_jewel_config.xml";
    public static final String FLOWER_JEWEL_CONFIG_CONTEXT = "FLOWER_JEWEL_CONFIG";

    // Secure Configuration Context
    public static final String SECURE_CONFIG_CONTEXT = "flower_jewel";

    /* custom parameters returned from database procedure */
    public static final String STATUS_PARAM = "OUT_STATUS";
    public static final String MESSAGE_PARAM = "OUT_MESSAGE";
  
    public FlowerJewelConstants() {
    }
}