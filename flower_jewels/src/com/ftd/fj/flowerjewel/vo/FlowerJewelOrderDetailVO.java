package com.ftd.fj.flowerjewel.vo;

import java.util.Date;

public class FlowerJewelOrderDetailVO
{
    private int flowerJewelsId;
    private int orderAddOnID;
    private Date transmissionTimestamp;
    private Date acknowledgementTimestamp;
    private Date shipConfirmationTimestamp;
    private int orderDetailId;
    private String externalOrderNumber;
    private Date orderDeliveryDate;
    private String orderGuid;
    private int recipientId;
    private int customerId;
    
  public FlowerJewelOrderDetailVO()
  {
  }

    public void setFlowerJewelsId(int flowerJewelsId) {
        this.flowerJewelsId = flowerJewelsId;
    }

    public int getFlowerJewelsId() {
        return flowerJewelsId;
    }

    public void setOrderAddOnID(int orderAddOnID) {
        this.orderAddOnID = orderAddOnID;
    }

    public int getOrderAddOnID() {
        return orderAddOnID;
    }

    public void setTransmissionTimestamp(Date transmissionTimestamp) {
        this.transmissionTimestamp = transmissionTimestamp;
    }

    public Date getTransmissionTimestamp() {
        return transmissionTimestamp;
    }

    public void setAcknowledgementTimestamp(Date acknowledgementTimestamp) {
        this.acknowledgementTimestamp = acknowledgementTimestamp;
    }

    public Date getAcknowledgementTimestamp() {
        return acknowledgementTimestamp;
    }

    public void setShipConfirmationTimestamp(Date shipConfirmationTimestamp) {
        this.shipConfirmationTimestamp = shipConfirmationTimestamp;
    }

    public Date getShipConfirmationTimestamp() {
        return shipConfirmationTimestamp;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setExternalOrderNumber(String externalOrderNumber) {
        this.externalOrderNumber = externalOrderNumber;
    }

    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }

    public void setOrderDeliveryDate(Date orderDeliveryDate) {
        this.orderDeliveryDate = orderDeliveryDate;
    }

    public Date getOrderDeliveryDate() {
        return orderDeliveryDate;
    }

    public void setOrderGuid(String orderGuid) {
        this.orderGuid = orderGuid;
    }

    public String getOrderGuid() {
        return orderGuid;
    }

    public void setRecipientId(int recipientId) {
        this.recipientId = recipientId;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setCustomerId(int customerId)
    {
      this.customerId = customerId;
    }
  
    public int getCustomerId()
    {
      return customerId;
    }
}
