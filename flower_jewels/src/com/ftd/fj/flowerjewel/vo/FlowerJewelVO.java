package com.ftd.fj.flowerjewel.vo;

import java.util.Date;

public class FlowerJewelVO
{
    private int flowerJewelsId;
    private int orderAddOnID;
    private Date transmissionTimestamp;
    private Date acknowledgementTimestamp;
    private Date shipConfirmationTimestamp;
    private Date shipDate;
    private Date deliveryDate;
    private String carrierID;
    private String shipMethod;
    private String trackingNumber;

  public FlowerJewelVO()
  {
  }

  public void setFlowerJewelsId(int flowerJewelsId)
  {
    this.flowerJewelsId = flowerJewelsId;
  }

  public int getFlowerJewelsId()
  {
    return flowerJewelsId;
  }

  public void setOrderAddOnID(int orderAddOnID)
  {
    this.orderAddOnID = orderAddOnID;
  }

  public int getOrderAddOnID()
  {
    return orderAddOnID;
  }

  public void setTransmissionTimestamp(Date transmissionTimestamp)
  {
    this.transmissionTimestamp = transmissionTimestamp;
  }

  public Date getTransmissionTimestamp()
  {
    return transmissionTimestamp;
  }

  public void setAcknowledgementTimestamp(Date acknowledgementTimestamp)
  {
    this.acknowledgementTimestamp = acknowledgementTimestamp;
  }

  public Date getAcknowledgementTimestamp()
  {
    return acknowledgementTimestamp;
  }

  public void setShipConfirmationTimestamp(Date shipConfirmationTimestamp)
  {
    this.shipConfirmationTimestamp = shipConfirmationTimestamp;
  }

  public Date getShipConfirmationTimestamp()
  {
    return shipConfirmationTimestamp;
  }

  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }

  public Date getShipDate()
  {
    return shipDate;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setCarrierID(String carrierID)
  {
    this.carrierID = carrierID;
  }

  public String getCarrierID()
  {
    return carrierID;
  }

  public void setShipMethod(String shipMethod)
  {
    this.shipMethod = shipMethod;
  }

  public String getShipMethod()
  {
    return shipMethod;
  }

  public void setTrackingNumber(String trackingNumber)
  {
    this.trackingNumber = trackingNumber;
  }

  public String getTrackingNumber()
  {
    return trackingNumber;
  }
}
