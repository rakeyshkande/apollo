package com.ftd.fj.flowerjewel.vo;

import java.util.Date;

public class FlowerJewelOutboundVO {
    private int orderAddOnID;
    private String addOnCode;
    private int addOnQty;
    private String externalOrderNumber;
    private Date deliveryDate;
    private String recipFirstName;
    private String recipLastName;
    private String recipBusinessName;
    private String recipAddress1;
    private String recipAddress2;
    private String recipCity;
    private String recipState;
    private String recipZipCode;
    private String recipCountry;
    private String recipDayPhone;
    private String recipDayExt;
    private String recipEveningPhone;
    private String recipEveningExt;
    private int flowerJewelsId;

    public FlowerJewelOutboundVO() {
    }

    public void setOrderAddOnID(int orderAddOnID) {
        this.orderAddOnID = orderAddOnID;
    }

    public int getOrderAddOnID() {
        return orderAddOnID;
    }

    public void setAddOnCode(String addOnCode) {
        this.addOnCode = addOnCode;
    }

    public String getAddOnCode() {
        return addOnCode;
    }

    public void setAddOnQty(int addOnQty) {
        this.addOnQty = addOnQty;
    }

    public int getAddOnQty() {
        return addOnQty;
    }

    public void setExternalOrderNumber(String externalOrderNumber) {
        this.externalOrderNumber = externalOrderNumber;
    }

    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setRecipFirstName(String recipFirstName) {
        this.recipFirstName = recipFirstName;
    }

    public String getRecipFirstName() {
        return recipFirstName;
    }

    public void setRecipLastName(String recipLastName) {
        this.recipLastName = recipLastName;
    }

    public String getRecipLastName() {
        return recipLastName;
    }

    public void setRecipBusinessName(String recipBusinessName) {
        this.recipBusinessName = recipBusinessName;
    }

    public String getRecipBusinessName() {
        return recipBusinessName;
    }

    public void setRecipAddress1(String recipAddress1) {
        this.recipAddress1 = recipAddress1;
    }

    public String getRecipAddress1() {
        return recipAddress1;
    }

    public void setRecipAddress2(String recipAddress2) {
        this.recipAddress2 = recipAddress2;
    }

    public String getRecipAddress2() {
        return recipAddress2;
    }

    public void setRecipCity(String recipCity) {
        this.recipCity = recipCity;
    }

    public String getRecipCity() {
        return recipCity;
    }

    public void setRecipState(String recipState) {
        this.recipState = recipState;
    }

    public String getRecipState() {
        return recipState;
    }

    public void setRecipZipCode(String recipZipCode) {
        this.recipZipCode = recipZipCode;
    }

    public String getRecipZipCode() {
        return recipZipCode;
    }

    public void setRecipCountry(String recipCountry) {
        this.recipCountry = recipCountry;
    }

    public String getRecipCountry() {
        return recipCountry;
    }

    public void setRecipDayPhone(String recipDayPhone) {
        this.recipDayPhone = recipDayPhone;
    }

    public String getRecipDayPhone() {
        return recipDayPhone;
    }

    public void setRecipDayExt(String recipDayExt) {
        this.recipDayExt = recipDayExt;
    }

    public String getRecipDayExt() {
        return recipDayExt;
    }

    public void setRecipEveningPhone(String recipEveningPhone) {
        this.recipEveningPhone = recipEveningPhone;
    }

    public String getRecipEveningPhone() {
        return recipEveningPhone;
    }

    public void setRecipEveningExt(String recipEveningExt) {
        this.recipEveningExt = recipEveningExt;
    }

    public String getRecipEveningExt() {
        return recipEveningExt;
    }

    public void setFlowerJewelsId(int flowerJewelsId) {
        this.flowerJewelsId = flowerJewelsId;
    }

    public int getFlowerJewelsId() {
        return flowerJewelsId;
    }
}
