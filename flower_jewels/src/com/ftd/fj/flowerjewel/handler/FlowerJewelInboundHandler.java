package com.ftd.fj.flowerjewel.handler;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;

import com.ftd.fj.common.framework.util.CommonUtils;
import com.ftd.fj.flowerjewel.constants.FlowerJewelConstants;
import com.ftd.fj.flowerjewel.dao.FlowerJewelDAO;

import com.ftd.fj.flowerjewel.vo.FlowerJewelOrderDetailVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;

import java.sql.Connection;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class handles inbound files sent from Ambras and updates
 * the Flower Jewel table.  Order receipt acknowledgements
 * and tracking number/ship date are received as separate records.
 *
 * @author Michael Giovenco
 */
public class FlowerJewelInboundHandler{
    
    private Connection conn;        // Database connection
    private Logger logger;          // Log4J logger

     
    public FlowerJewelInboundHandler() throws Exception {
        this.logger = new Logger("com.ftd.fj.flowerjewel.handler.FlowerJewelInboundHandler");  
    }

    /** 
     * This method is invoked by the event handler framework. 
     */
    public void invoke() throws Throwable {

        conn = CommonUtils.getConnection();
        try {
            if(logger.isInfoEnabled()) 
                logger.info("Beginning processing of Flower Jewels inbound files...");
            process();
        }
        catch (Throwable t) {
            String msg = "Error occurred while processing Ambras inbound files:" + t.getMessage();
            logger.error(msg, t);
            CommonUtils.sendSystemMessage(msg);
        }
        finally {
            if(conn != null) {
                conn.close();
                conn = null;
            }
        }
    }
    
    /**
     * This method handles the invocation to decide which type of record is
     * incoming, acknowledgement or delivery confirmation (tracking).  The 
     * method then calls the proper sub-methods to process the incoming file.
     * @throws Throwable
     */
    private void process() throws Throwable {
        // Get a list of all records
        FlowerJewelDAO flowerJewelDAO = new FlowerJewelDAO(conn);
        List fileList = getFilesFromFTP();
          
        if(fileList == null || fileList.size() == 0) {
            if(logger.isInfoEnabled())
                logger.info("There are no new files from vendor Ambras");
        }
        else {
            logger.info("Number of files to be processed: " + fileList.size());
            File inputFile = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            for(int j = 0; j < fileList.size(); j++) {
                try {
                    inputFile = (File)fileList.get(j);            
                    logger.debug("Beginning processing of: " + inputFile.getName());     
                        try {    
                              //Parse the current file
                              Document doc = db.parse(inputFile);
                              //Normalize the current file
                              doc.getDocumentElement().normalize();
                              
                              //If the document's root node is "order-acknowledgements", this means it's an Acknowledgement file.
                              if(doc.getDocumentElement().getNodeName().equals("order-acknowledgements")){
                                //Process acknowledgement.
                                logger.debug("Document is an Acknowledgements file, calling processAcknowledgement method");
                                processAcknowledgement(flowerJewelDAO, doc);
                              }
                              //If the document's root node is "orders", this means it's a Tracking/Delivery file.
                              else if (doc.getDocumentElement().getNodeName().equals("order-shipments")){
                                //Process tracking.
                                logger.debug("Document is a Tracking/Delivery file, calling processTracking method");
                                processTracking(flowerJewelDAO, doc);
                              }                            
                        }
                        catch(Exception e)
                        {
                            String msg = "Error processing xml file.  Investigate: " + e;
                            logger.error(msg, e);
                            CommonUtils.sendSystemMessage(msg);
                        }
                }
                catch(Exception e)
                {
                    String msg = "Error processing info from Ambras inbound file " + inputFile.getName() + ".  Investigate: " + e;
                    logger.error(msg, e);
                    CommonUtils.sendSystemMessage(msg);
                }
            }
        }
    }
    
    /** 
     * This record retrieves the inbound file from the FTP server.
     * @return List - List of files
     * @throws Exception
     */
    private List getFilesFromFTP() throws Exception {
        List fileList = new ArrayList();
    
        if(logger.isInfoEnabled())
            logger.info("Inbound FTP processing for Ambras");
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String remoteLocation = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"REMOTE_LOCATION_AMBRAS");                      
        String ftpLogon = configUtil.getSecureProperty(FlowerJewelConstants.SECURE_CONFIG_CONTEXT,"FTP_LOGON_AMBRAS");
        String ftpPassword = configUtil.getSecureProperty(FlowerJewelConstants.SECURE_CONFIG_CONTEXT,"FTP_PASSWORD_AMBRAS");
        String remoteDir = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"INBOUND_REMOTE_DIRECTORY_AMBRAS");
        String localDirInbound = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"INBOUND_ARCHIVE_DIRECTORY_AMBRAS");
        String remoteFilename = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"REMOTE_FILENAME_AMBRAS");                      
                        
        //get list of files in directory       
        FTPClient ftpClient = new FTPClient(remoteLocation);
        ftpClient.login(ftpLogon,ftpPassword);
        ftpClient.chdir(remoteDir);
        
        String[] files = null;
        try{
            //When files do not exist in the specified dir this object throws
            //an exception.  We do not consider this an error because this
            //dir will not always contain files.
            files = ftpClient.dir(remoteFilename); 
        }
        catch(FTPException ftpe)
        {
            logger.info("Files do not exist in directory.");
            logger.info(ftpe);
        }
        
        //if nothing was found exit
        if(files == null || files.length == 0)
        {
            return null;
        }
            
        //get current date and time to be used in file name.
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHmmss");
        String dateString = sdf.format(new java.util.Date());
        
        //process each file
        for(int i = 0;i < files.length;i++)
        {
            //get the file from the remote location
            String localFile = localDirInbound + "FTD_" + "(" + i + ")_"+ dateString + ".xml";
            
            if(logger.isInfoEnabled()) {
                logger.info("Local File:" + localFile);
                logger.info("Remote File:" + files[i]);
            }
            
            try {
                ftpClient.get(localFile, files[i]); 
                //remove the file from the remote directory
                ftpClient.delete(files[i]);
            
                File file = new File(localFile);
                fileList.add(file);
            } catch(Exception e) {
                String msg = "Error FTP'ing Flower Jewels file: " + files[i] + " so skipping and moving on.  Exception: " + e;
                logger.error(msg, e);
                CommonUtils.sendSystemMessage(msg);
            }
        }
        
        return fileList;
    }
    
    /**
     * This method will update the FLOWER JEWEL table with today as acknowledgement date
     * for orders with matching ftd-reference-ids.
     * @param fjdao Data access object used for database interaction.
     * @param doc Document object containg inbound order information of acknowledgement
     * @throws Exception
     */
    private void processAcknowledgement(FlowerJewelDAO fjdao, Document doc) throws Exception {   
        NodeList nodeLst = doc.getElementsByTagName("order");
        NodeList nmElmftdReferenceNumbers = null;

        String ftdReferenceNumber = null;
        String msg = "";
        String ordersWithAckTimeStamps = "";
        String ordersNotInTable = "";
        boolean blankOrderNumbers = false;
        FlowerJewelOrderDetailVO flowerJewelOrderDetailVO = null;
        
        for (int z = 0; z < nodeLst.getLength(); z++) {
          Node fstNode = nodeLst.item(z);
          if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
            Element fstElmnt = (Element) fstNode;
            
            //Assign a nodelist containing all the "ftd-reference-number" nodes for that specific order (should only be one).
            nmElmftdReferenceNumbers = fstElmnt.getElementsByTagName("ftd-reference-number");
            //If the first node is not null, assign its value to nodelist nmElmftdReferenceNumbers.
            if(nmElmftdReferenceNumbers.item(0).getFirstChild() != null)
              ftdReferenceNumber = nmElmftdReferenceNumbers.item(0).getFirstChild().getNodeValue();
            //If ftdReferenceNumber is not null and does not equal "", populate orderInfo with all the order information for the order with that specific ftdReferenceNumber.
            if(ftdReferenceNumber != null){
                flowerJewelOrderDetailVO = fjdao.getFlowerJewelOrderDetails(ftdReferenceNumber);
                //If orderInfo list is null, send a system error message
                if(flowerJewelOrderDetailVO == null){
                    if(ordersNotInTable.equals(""))
                      ordersNotInTable += ftdReferenceNumber;
                    else
                      ordersNotInTable += ("," + ftdReferenceNumber);
                }
                else{
                    Date ackDate = flowerJewelOrderDetailVO.getAcknowledgementTimestamp();
                    if(ackDate == null){
                      //Calls the stored procedure to do the Acknowledgement Stored Procedure call.
                      fjdao.updateFlowerJewelAck(ftdReferenceNumber);
                    }
                    else{
                      if(ordersWithAckTimeStamps.equals(""))
                        ordersWithAckTimeStamps += ftdReferenceNumber;
                      else
                        ordersWithAckTimeStamps += ("," + ftdReferenceNumber);  
                    }
                }   
              }
          else
            blankOrderNumbers = true;
          }
        }
            if(!ordersWithAckTimeStamps.equals("")){
              msg = "The following orders with ftd-reference-numbers: " + ordersWithAckTimeStamps + " already has an acknowledgement timestamps.  " +
              "No further acknowledgements should have been received for these orders.  Please investigate.";
              logger.error(msg);
              CommonUtils.sendSystemMessage(msg);
            }
            
            if(!ordersNotInTable.equals("")) {
              msg = "The following orders with ftd-reference-numbers: " + ordersNotInTable + " do not have a matching jewelry order in the FLOWER JEWEL table. " +
              "Please invesigate.";
              logger.error(msg);
              CommonUtils.sendSystemMessage(msg);   
            }
            
            if(blankOrderNumbers)
            {
              msg = "There were blank fields for ftd-reference-number in the file.  Please investigate.";
              logger.error(msg);
              CommonUtils.sendSystemMessage(msg);
            }
    }
         
    /**
     * This method will update the FLOWER JEWEL table with the tracking information
     * for the order sent my Ambras.
     * @param fjdao Data access object used for database interaction.
     * @param doc Document object containg inbound order information of tracking
     * @throws Exception
     */
    private void processTracking(FlowerJewelDAO fjdao, Document doc) throws Exception {
        NodeList nodeLst = doc.getElementsByTagName("order");
        NodeList nmElmftdReferenceNumbers = null;
        NodeList nmElmShipDates = null;
        NodeList nmElmDeliveryDates = null;
        NodeList nmElmCarrierIds = null;
        NodeList nmElmShipMethods = null;
        NodeList nmElmTrackingNumbers = null;
        
        String ftdReferenceNumber = null;
        String carrierID = null;
        String shipMethod = null;
        String trackingNumber = null;
        Date shipDate = null;
        Date deliveryDate = null;
        String ordersWithMissingTrackingInfo = "";
        String ordersWithShipConfTimeStamps = "";
        String ordersNotInTable = "";
        String msg = "";
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        FlowerJewelOrderDetailVO flowerJewelOrderDetailVO = null;
        
        for (int z = 0; z < nodeLst.getLength(); z++) {
          Node fstNode = nodeLst.item(z);
          
          if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
            Element fstElmnt = (Element) fstNode;
            
            //Assign a nodelist containing all the specific nodes for that order (should only be one).
            nmElmftdReferenceNumbers = fstElmnt.getElementsByTagName("ftd-reference-number");
            nmElmShipDates = fstElmnt.getElementsByTagName("ship-date");
            nmElmDeliveryDates = fstElmnt.getElementsByTagName("delivery-date");
            nmElmCarrierIds = fstElmnt.getElementsByTagName("carrier");
            nmElmShipMethods = fstElmnt.getElementsByTagName("ship-method");
            nmElmTrackingNumbers = fstElmnt.getElementsByTagName("tracking-number");
            
            //If the first node is not null, assign its value to a variable.
            if(nmElmftdReferenceNumbers.item(0).getFirstChild() != null)
              ftdReferenceNumber = nmElmftdReferenceNumbers.item(0).getFirstChild().getNodeValue();
            else
              ftdReferenceNumber = null;
            if(nmElmCarrierIds.item(0).getFirstChild() != null)
              carrierID = nmElmCarrierIds.item(0).getFirstChild().getNodeValue();
            else
              carrierID = null;
            if(nmElmShipMethods.item(0).getFirstChild() != null)
              shipMethod = nmElmShipMethods.item(0).getFirstChild().getNodeValue();
            else
              shipMethod = null;
            if(nmElmTrackingNumbers.item(0).getFirstChild() != null)
              trackingNumber = nmElmTrackingNumbers.item(0).getFirstChild().getNodeValue();
            else
              trackingNumber = null;
            
            try{
                 //If the first node is not null, assign its parse the value as a date and assign it to a variable.
                if(nmElmShipDates.item(0).getFirstChild() != null)
                  shipDate = df.parse(nmElmShipDates.item(0).getFirstChild().getNodeValue());
                else
                  shipDate = null;
                if(nmElmDeliveryDates.item(0).getFirstChild() != null)
                  deliveryDate = df.parse(nmElmDeliveryDates.item(0).getFirstChild().getNodeValue());
                else
                  deliveryDate = null;
            }  
            catch (ParseException e)
            {
                 logger.error(e);
                 CommonUtils.sendSystemMessage("Error parsing date from XML: " + e);
            }
  
            if(ftdReferenceNumber != null)
              flowerJewelOrderDetailVO = fjdao.getFlowerJewelOrderDetails(ftdReferenceNumber);
                  
            //If orderInfo list is null, print out 
            //system error that there was no matching order.
            if(flowerJewelOrderDetailVO == null){
                if(ordersNotInTable.equals(""))
                  ordersNotInTable += ftdReferenceNumber;
                else
                  ordersNotInTable += ("," + ftdReferenceNumber);  
            }
            else{
                Date shipConfDate = flowerJewelOrderDetailVO.getShipConfirmationTimestamp();
                  
                if(shipConfDate == null){
                  //Calls stored procedure for updating tracking, if fields are not null.
                  if(ftdReferenceNumber != null && shipDate != null && deliveryDate != null && carrierID != null && shipMethod != null && trackingNumber != null){
                    //Call to stored procedure that does update to Flower_jewels table with tracking information.
                    fjdao.updateFlowerJewelShip(ftdReferenceNumber, shipDate, deliveryDate, carrierID, shipMethod, trackingNumber);
                                        
                    //Call to stored procedure that does comment insertion.
                    fjdao.insertComment(flowerJewelOrderDetailVO.getRecipientId(), flowerJewelOrderDetailVO.getOrderGuid(), 
                    flowerJewelOrderDetailVO.getOrderDetailId(), "Tracking information received from Ambras.  Shipping Method: " + shipMethod + 
                    ", Shipping Date: " + shipDate + ", Shipper: " + carrierID + ", Shipping Tracking Number: " + trackingNumber, "Order", "SYS");
                  }
                  else{
                    if(ordersWithMissingTrackingInfo.equals(""))
                      ordersWithMissingTrackingInfo += ftdReferenceNumber;
                    else
                      ordersWithMissingTrackingInfo += ("," + ftdReferenceNumber);  
                  }
                }
                else{
                  if(ordersWithShipConfTimeStamps.equals(""))
                      ordersWithShipConfTimeStamps += ftdReferenceNumber;
                  else
                      ordersWithShipConfTimeStamps += ("," + ftdReferenceNumber);
                }
            }
        }
    }    
            if(!ordersNotInTable.equals("")) {
                msg = "The following orders with ftd-reference-numbers: " + ordersNotInTable + " do not have a matching jewelry order in the FLOWER JEWEL table. " +
                "Please invesigate.";
                logger.error(msg);
                CommonUtils.sendSystemMessage(msg);   
            }
            
            if(!ordersWithMissingTrackingInfo.equals("")){         
                msg = "The following orders with ftd-reference-numbers: " + ordersWithMissingTrackingInfo + " have missing tracking information.  Please investigate.";
                logger.info(msg);
                CommonUtils.sendSystemMessage(msg);   
            }
            
            if(!ordersWithShipConfTimeStamps.equals("")){
                msg = "The following orders with ftd-reference-numbers: " + ordersWithShipConfTimeStamps + " already has a ship confirmation timestamp.  Please investigate.";
                logger.info(msg);
                CommonUtils.sendSystemMessage(msg);   
            }
  }
}
