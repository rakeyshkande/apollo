package com.ftd.fj.flowerjewel.handler;

import com.enterprisedt.net.ftp.FTPClient;

import com.ftd.fj.common.framework.util.CommonUtils;
import com.ftd.fj.flowerjewel.constants.FlowerJewelConstants;
import com.ftd.fj.flowerjewel.dao.FlowerJewelDAO;
import com.ftd.fj.flowerjewel.vo.FlowerJewelOrderDetailVO;
import com.ftd.fj.flowerjewel.vo.FlowerJewelOutboundVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
  * This class handles outbound files sent to Ambras.
  *
  * @author Tim Schmig
  */
public class FlowerJewelOutboundHandler {
    
    private Logger logger;              // Log4J logger
    private Connection conn;            // Database connection
     
    public FlowerJewelOutboundHandler() throws Exception {
        this.logger = new Logger("com.ftd.fj.flowerjewel.handler.FlowerJewelsOutboundHandler");  
    }


    /** 
     * This method is invoked by the event handler framework. 
     */
    public void invoke() throws Throwable {
        
        conn = CommonUtils.getConnection();
        
        try {
            if(logger.isInfoEnabled()) {
                logger.info("Executing Flower Jewels outbound processing");
            }
            processOrders();
        }
        catch (Throwable t) {
            String msg = "Flower Jewel outbound file was not created.  " + t.getMessage();
            logger.error(msg, t);
            CommonUtils.sendSystemMessage(msg);
        }
        finally {
            conn.close();
            conn = null;
        }
    }
  
  
    /**
     * Creates and FTPs Flower Jewel outbound file 
     * @throws Exception
     */
    public void processOrders()throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy_HHmmss");
        FlowerJewelDAO flowerJewelDAO = new FlowerJewelDAO(conn);

        ArrayList flowerJewelOrders = flowerJewelDAO.getFlowerJewelOrders();
        ArrayList successList = new ArrayList();
        ArrayList errorList = new ArrayList();

        Document orderDocument = JAXPUtil.createDocument();
        Element orders = orderDocument.createElement("orders");
        orderDocument.appendChild(orders);

        for (int i=0; i<flowerJewelOrders.size(); i++) {
            FlowerJewelOutboundVO fjVO = (FlowerJewelOutboundVO) flowerJewelOrders.get(i);
            try {
                logger.info(fjVO.getExternalOrderNumber() + " " + fjVO.getAddOnCode() + " " + fjVO.getAddOnQty());
                for (int j=0; j<fjVO.getAddOnQty(); j++) {
                    int flowerJewelsId = flowerJewelDAO.insertFlowerJewels(fjVO.getOrderAddOnID());
                    fjVO.setFlowerJewelsId(flowerJewelsId);
                    boolean success = createOrderXML(orderDocument, orders, fjVO);
                    if (success) {
                        logger.info("success");
                        successList.add(Integer.toString(flowerJewelsId));
                    } else {
                        logger.info("error");
                        errorList.add(Integer.toString(flowerJewelsId));
                    }
                }
            } catch (Exception e) {
                logger.error(e);
                errorList.add(Integer.toString(fjVO.getFlowerJewelsId()));
            }
        }
        logger.info(JAXPUtil.toString(orderDocument));
        if (errorList.size() > 0) {
            String msg = "The following flower_jewels_ids were not transmitted to Ambras:\n";
            for (int j=0; j<errorList.size(); j++) {
                String flowerJewelsId = (String) successList.get(j);
                msg = msg + flowerJewelsId + "\n";
            }
            logger.error(msg);
            CommonUtils.sendSystemMessage(msg);
        }
        
        if (successList.size() > 0) {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         

            String remoteLocation = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"REMOTE_LOCATION_AMBRAS");                      
            String ftpLogon = configUtil.getSecureProperty(FlowerJewelConstants.SECURE_CONFIG_CONTEXT,"FTP_LOGON_AMBRAS");
            String ftpPassword = configUtil.getSecureProperty(FlowerJewelConstants.SECURE_CONFIG_CONTEXT,"FTP_PASSWORD_AMBRAS");

            String remoteDir = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"OUTBOUND_REMOTE_DIRECTORY_AMBRAS");
            String localDir = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT,"LOCAL_DIRECTORY_AMBRAS");
            String archiveDir = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT, "OUTBOUND_ARCHIVE_DIRECTORY_AMBRAS");
            
            String fileNamePrefix = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT, "OUTBOUND_FILENAME_PREFIX_AMBRAS");
            String fileNameType = configUtil.getFrpGlobalParm(FlowerJewelConstants.FLOWER_JEWEL_CONFIG_CONTEXT, "OUTBOUND_FILENAME_TYPE_AMBRAS");
            String today = sdf.format(new Date());
            String fileName = fileNamePrefix + "_" + today + "." + fileNameType;

            File file = new File(localDir + fileName);
            FileWriter out = new FileWriter(file);
            
            out.write(JAXPUtil.toString(orderDocument));
            out.close();
            
            try {
                //logon to ftp server
                logger.debug("Connecting to " + remoteLocation);
                FTPClient ftpClient = new FTPClient(remoteLocation);
                ftpClient.login(ftpLogon,ftpPassword);
                
                String localFile = localDir + fileName;
                String remoteFile = remoteDir + "/" + fileName;
                logger.debug("localFile: " + localFile);
                logger.debug("remoteFile: " + remoteFile);
                ftpClient.put(localFile, remoteFile);
                
                // move local file to archive dir //
                file.renameTo(new File(archiveDir + fileName));

            }
            catch (Exception ex) {
                String msg = fileName + " could not be FTPed.  File must be manually sent to Ambras. " + (localDir + fileName) + ": " + ex.toString();
                logger.error(msg, ex);
                CommonUtils.sendSystemMessage(msg); 
            }
            
            try {
                for (int j=0; j<successList.size(); j++) {
                    String flowerJewelsId = (String) successList.get(j);
                    try {
                        flowerJewelDAO.updateFlowerJewelTransmission(flowerJewelsId);
                    } catch (Exception e) {
                        String msg = "Unable to update transmission timestamp for id " + flowerJewelsId;
                        logger.error(msg + "\n" + e);
                        CommonUtils.sendSystemMessage(msg); 
                    }
                    try {
                        FlowerJewelOrderDetailVO fjodVO = flowerJewelDAO.getFlowerJewelOrderDetails(flowerJewelsId);
                        String commentText = "Flower Jewels Id " + flowerJewelsId + " has been transmitted to Ambras";
                        flowerJewelDAO.insertComment(fjodVO.getRecipientId(), fjodVO.getOrderGuid(), fjodVO.getOrderDetailId(),
                                commentText, "Order", "SYS");
                    } catch (Exception e) {
                        String msg = "Unable to update comments for id " + flowerJewelsId;
                        logger.error(msg + "\n" + e);
                        CommonUtils.sendSystemMessage(msg); 
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }

    }
    
    
    /**
     * Create XML document detailing Jewelry orders 
     */  
    private boolean createOrderXML(Document orderDocument, Element orders, FlowerJewelOutboundVO fjVO) throws Exception {
        boolean success = true;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        try {
            Element order = orderDocument.createElement("order");

            Element temp = orderDocument.createElement("ftd-reference-number");
            temp.appendChild(orderDocument.createTextNode(Integer.toString(fjVO.getFlowerJewelsId())));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("confirmation-number");
            temp.appendChild(orderDocument.createTextNode(fjVO.getExternalOrderNumber()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-date");
            temp.appendChild(orderDocument.createTextNode(sdf.format(fjVO.getDeliveryDate())));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-first-name");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipFirstName()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-last-name");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipLastName()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-business-name");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipBusinessName()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-address1");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipAddress1()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-address2");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipAddress2()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-city");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipCity()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-state");
            temp.appendChild(orderDocument.createTextNode(fjVO.getRecipState()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-zip");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipZipCode()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-country");
            temp.appendChild(orderDocument.createTextNode(fjVO.getRecipCountry()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-day-phone");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipDayPhone()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-day-ext");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipDayExt()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-evening-phone");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipEveningPhone()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("delivery-evening-ext");
            temp.appendChild(orderDocument.createCDATASection(fjVO.getRecipEveningExt()));
            order.appendChild(temp);
            
            temp = orderDocument.createElement("ftd-product-id");
            temp.appendChild(orderDocument.createTextNode(fjVO.getAddOnCode()));
            order.appendChild(temp);
            
            orders.appendChild(order);

        } catch (Exception e) {
            logger.error(e);
            success = false;
        }
        return success;
    }

}