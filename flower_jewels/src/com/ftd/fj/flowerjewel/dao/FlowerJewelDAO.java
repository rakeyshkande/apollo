package com.ftd.fj.flowerjewel.dao;

import com.ftd.fj.flowerjewel.vo.FlowerJewelOrderDetailVO;
import com.ftd.fj.flowerjewel.vo.FlowerJewelOutboundVO;
import com.ftd.fj.flowerjewel.vo.FlowerJewelVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import java.math.BigDecimal;

import org.xml.sax.SAXException;


/**
 *
 *
 * @author Michael Giovenco
 */
public class FlowerJewelDAO
{

  Connection conn;

    private Logger logger;
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE";
    private static final String FLOWER_JEWELS_ID_PARAM = "OUT_FLOWER_JEWELS_ID";


  public FlowerJewelDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.fj.flowerjewel.dao.FlowerJewelDAO");
  }
  
    /**
     *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG_.GET_ORDERS_FOR_TRANSMISSION
     *  stored procedure. Get a list of jewelry orders that need to be processed.
     *
     * @return List
     */
    public ArrayList getFlowerJewelOrders() throws SAXException, ParserConfigurationException, IOException, SQLException, Exception {

        logger.debug("getFlowerJewelOrders()");

        /* build DataRequest object */
        ArrayList <FlowerJewelOutboundVO> flowerJewelVOList = new ArrayList <FlowerJewelOutboundVO> ();

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDERS_FOR_TRANSMISSION");
        Map inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (cr.next()) {
            FlowerJewelOutboundVO flowerJewelVO = new FlowerJewelOutboundVO();
            flowerJewelVO.setOrderAddOnID(cr.getInt("order_add_on_id"));
            flowerJewelVO.setAddOnCode(cr.getString("add_on_code"));
            flowerJewelVO.setAddOnQty(cr.getInt("add_on_quantity"));
            flowerJewelVO.setExternalOrderNumber(cr.getString("external_order_number"));
            flowerJewelVO.setDeliveryDate(cr.getDate("delivery_date"));
            flowerJewelVO.setRecipFirstName(cr.getString("first_name"));
            flowerJewelVO.setRecipLastName(cr.getString("last_name"));
            flowerJewelVO.setRecipBusinessName(cr.getString("business_name"));
            flowerJewelVO.setRecipAddress1(cr.getString("address_1"));
            flowerJewelVO.setRecipAddress2(cr.getString("address_2"));
            flowerJewelVO.setRecipCity(cr.getString("city"));
            flowerJewelVO.setRecipState(cr.getString("state"));
            flowerJewelVO.setRecipZipCode(cr.getString("zip_code"));
            flowerJewelVO.setRecipCountry(cr.getString("country"));
            flowerJewelVO.setRecipDayPhone(cr.getString("day_phone"));
            flowerJewelVO.setRecipDayExt(cr.getString("day_extension"));
            flowerJewelVO.setRecipEveningPhone(cr.getString("evening_phone"));
            flowerJewelVO.setRecipEveningExt(cr.getString("evening_extension"));
            
            flowerJewelVOList.add(flowerJewelVO);
        }
        return flowerJewelVOList;
        
    }


    public int insertFlowerJewels(int orderAddOnId) throws Exception {
        
        logger.debug("insertFlowerJewels()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_FLOWER_JEWELS");
        Map inputParams = new HashMap();
        inputParams.put("IN_ORDER_ADD_ON_ID", Integer.toString(orderAddOnId));
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        String status = (String) outputMap.get(STATUS_PARAM);
        String message = (String) outputMap.get(MESSAGE_PARAM);
        if (status == null || !status.equalsIgnoreCase("Y")) {
            logger.error(message);
            // Throw an exception?
        }
        BigDecimal flowerJewelsId = (BigDecimal) outputMap.get(FLOWER_JEWELS_ID_PARAM);
        return flowerJewelsId.intValue();
    }

    /**
    *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG.UPDATE_FLOWER_JEWELS_TRANS
    *  stored procedure.
    *
    */
    public void updateFlowerJewelTransmission(String ftdReferenceNumber)throws Exception {

        logger.debug("updateFlowerJewelTransmission(" + ftdReferenceNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_FLOWER_JEWELS_TRANS");
        Map inputParams = new HashMap();
        inputParams.put("IN_FLOWER_JEWELS_ID", ftdReferenceNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        String status = (String) outputMap.get(STATUS_PARAM);
        String message = (String) outputMap.get(MESSAGE_PARAM);
        if (status == null || !status.equalsIgnoreCase("Y")) {
            logger.error(message);
            // Throw an exception?
        }
    }
    
     /**
     *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG.UPDATE_FLOWER_JEWELS_ACK
     *  stored procedure.
     *
     */
     public void updateFlowerJewelAck(String ftdReferenceNumber)throws Exception {

         logger.debug("updateFlowerJewelAck(" + ftdReferenceNumber + ")");

         /* build DataRequest object */
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("UPDATE_FLOWER_JEWELS_ACK");
         Map inputParams = new HashMap();
         inputParams.put("IN_FLOWER_JEWELS_ID", ftdReferenceNumber);
         dataRequest.setInputParams(inputParams);

         /* execute the stored procedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
         HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

         String status = (String) outputMap.get(STATUS_PARAM);
         String message = (String) outputMap.get(MESSAGE_PARAM);
         if (status == null || !status.equalsIgnoreCase("Y")) {
             logger.error(message);
             // Throw an exception?
         }
     }
    
     /**
     *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG.UPDATE_FLOWER_JEWELS_SHIP
     *  stored procedure.
     *
     */
     public void updateFlowerJewelShip(String ftdReferenceNumber, Date shipDate, Date deliveryDate, String carrierId, String shipMethod, String trackingNumber)throws Exception {

         logger.debug("updateFlowerJewelShip(" + ftdReferenceNumber + ")");

         /* build DataRequest object */
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("UPDATE_FLOWER_JEWELS_SHIP");
         Map inputParams = new HashMap();
         inputParams.put("IN_FLOWER_JEWELS_ID", ftdReferenceNumber);
         inputParams.put("IN_TRACKING_NUMBER", trackingNumber);
         inputParams.put("IN_CARRIER_ID", carrierId);
         inputParams.put("IN_SHIP_METHOD", shipMethod);

         java.sql.Date vendorShipDate = null;
         if (shipDate != null) vendorShipDate = new java.sql.Date(shipDate.getTime());
         java.sql.Date vendorDeliveryDate = null;
         if (deliveryDate != null) vendorDeliveryDate = new java.sql.Date(deliveryDate.getTime());
         inputParams.put("IN_VENDOR_SHIP_DATE", vendorShipDate);
         inputParams.put("IN_VENDOR_DELIVERY_DATE", vendorDeliveryDate);
         dataRequest.setInputParams(inputParams);

         /* execute the stored procedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
         HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

         String status = (String) outputMap.get(STATUS_PARAM);
         String message = (String) outputMap.get(MESSAGE_PARAM);
         if (status == null || !status.equalsIgnoreCase("Y")) {
             logger.error(message);
             // Throw an exception?
         }
     }
     
     /**
     *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG_.GET_FLOWER_JEWEL_DETAILS
     *  stored procedure. Gets a list of order information on an order with a specific ftd-reference-number
     *
     * @return List
     */
     public FlowerJewelOrderDetailVO getFlowerJewelOrderDetails(String ftdReferenceId) throws SAXException, ParserConfigurationException, IOException, SQLException, Exception {

         logger.debug("getFlowerJewelOrderDetails(" + ftdReferenceId + ")");

         /* build DataRequest object */
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("GET_FLOWER_JEWEL_DETAILS");
         Map inputParams = new HashMap();
         inputParams.put("IN_FLOWER_JEWELS_ID", ftdReferenceId);
         dataRequest.setInputParams(inputParams);

         /* execute the stored procedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
         CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
         
         FlowerJewelOrderDetailVO flowerJewelVO = null;
         while (cr.next()) {
             flowerJewelVO = new FlowerJewelOrderDetailVO();
             flowerJewelVO.setFlowerJewelsId(cr.getInt("flower_jewels_id"));
             flowerJewelVO.setOrderAddOnID(cr.getInt("order_add_on_id"));
             flowerJewelVO.setOrderDetailId(cr.getInt("order_detail_id"));
             flowerJewelVO.setExternalOrderNumber("external_order_number");
             flowerJewelVO.setOrderDeliveryDate(cr.getDate("delivery_date"));
             flowerJewelVO.setTransmissionTimestamp(cr.getDate("transmission_timestamp"));
             flowerJewelVO.setAcknowledgementTimestamp(cr.getDate("acknowledgement_timestamp"));
             flowerJewelVO.setShipConfirmationTimestamp(cr.getDate("ship_confirmation_timestamp"));
             flowerJewelVO.setOrderGuid(cr.getString("order_guid"));
             flowerJewelVO.setRecipientId(cr.getInt("recipient_id"));
         }
         
         return flowerJewelVO;
     }
     
          /**
     *  This method serves as a wrapper for the CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
     *  stored procedure.
     *
     */
     public void insertComment(int customerID, String orderGUID, int orderDetailID, String commentText, String commentType, String createdBy)throws Exception {
           
          logger.debug("INSERT_COMMENTS");
          logger.debug("customerId: " + customerID);
          logger.debug("orderGUID: " + orderGUID);
          logger.debug("orderDetailID: " + orderDetailID);
          logger.debug("commentType: " + commentType);
          logger.debug("commentText: " + commentText);
         
          /* build DataRequest object */
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("INSERT_COMMENTS");
          Map inputParams = new HashMap();
          inputParams.put("IN_CUSTOMER_ID", Integer.toString(customerID));
          inputParams.put("IN_ORDER_GUID", orderGUID);
          inputParams.put("IN_ORDER_DETAIL_ID", Integer.toString(orderDetailID));
          inputParams.put("IN_COMMENT_ORIGIN", "OrderProc");
          inputParams.put("IN_REASON",  null);
          inputParams.put("IN_DNIS_ID", null);
          inputParams.put("IN_COMMENT_TEXT", commentText);
          inputParams.put("IN_COMMENT_TYPE", commentType);
          inputParams.put("IN_CSR_ID", createdBy);
          dataRequest.setInputParams(inputParams);

           /* execute the stored procedure */
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
           HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);
  
           String status = (String) outputMap.get(STATUS_PARAM);
           String message = (String) outputMap.get(MESSAGE_PARAM);
           if (status == null || !status.equalsIgnoreCase("Y")) {
               logger.error(message);
               // Throw an exception?
           }
     }
     
     
     /**
     *  This method serves as a wrapper for the CLEAN.FLOWER_JEWEL_PKG_.GET_UNACKNOWLEDGED_ORDERS
     *  stored procedure. Gets a list of orders that have no yet been acknowledged by Ambras.
     *
     * @return List
     */
     public ArrayList getUnacknowledgedOrders() throws SAXException, ParserConfigurationException, IOException, SQLException, Exception {

         logger.debug("getUnacknowledgedOrders()");

         ArrayList <FlowerJewelVO> flowerJewelVOList = new ArrayList <FlowerJewelVO>();

         /* build DataRequest object */
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("GET_UNACKNOWLEDGED_ORDERS");
         Map inputParams = new HashMap();
         dataRequest.setInputParams(inputParams);
         /* execute the stored procedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

         while (cr.next()) {
              FlowerJewelVO flowerJewelVO = new FlowerJewelVO();
              flowerJewelVO.setFlowerJewelsId(cr.getInt("flower_jewels_id"));
              flowerJewelVO.setOrderAddOnID(cr.getInt("order_add_on_id"));
              flowerJewelVO.setTransmissionTimestamp(cr.getDate("transmission_timestamp"));
              flowerJewelVO.setAcknowledgementTimestamp(cr.getDate("acknowledgement_timestamp"));
              flowerJewelVO.setShipConfirmationTimestamp(cr.getDate("ship_confirmation_timestamp"));
              flowerJewelVO.setShipDate(cr.getDate("vendor_ship_date"));
              flowerJewelVO.setDeliveryDate(cr.getDate("vendor_delivery_date"));
              flowerJewelVO.setCarrierID(cr.getString("carrier_id"));
              flowerJewelVO.setShipMethod(cr.getString("ship_method"));
              flowerJewelVO.setTrackingNumber(cr.getString("tracking_number"));
              
              flowerJewelVOList.add(flowerJewelVO);
         }
         
        return flowerJewelVOList;
     }    
}


