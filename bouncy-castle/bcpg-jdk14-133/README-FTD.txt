This readme is intended to describe the FTD-specific usage of the Bouncy Castle (BC) library.  It is not intended as an introduction to PGP encryption concepts nor the BC API.  You are strongly advised to familiarize yourself with the BC API and documentation. 

FTD-specific changes have been made to the following example classes to create a usable, working command-based utility for Apollo-iSeries interaction:

org.bouncycastle.openpgp.examples.KeyBasedFileProcessor
org.bouncycastle.openpgp.examples.KeyBasedLargeFileProcessor
org.bouncycastle.openpgp.examples.RSAKeyPairGenerator.java

To execute the utility, simply compile this project's classes, and designate them first in your execution classpath (see below).  Please note that you will also need the runtime library FTD/lib/bcprov-jdk14-133.jar.

Please use the following examples as guides, not as literal translations.  You will have to specify the appropriate paths, directories, etc., for your specific environment.

Example 1:
To generate a PGP key-pair:

java -classpath "classes;bcprov-jdk14-133.jar" org.bouncycastle.openpgp.examples.RSAKeyPairGenerator -a [output-key-name] [pass-phrase]

By FTD-code default, the two generated keys will have names [output-key-name]-pub.asc and [output-key-name]-secret.asc for ASCII-armored keys.  Otherwise, the names will be [output-key-name]-pub.bpg and [output-key-name]-secret.bpg for non-ASCII-armored keys.

Example 2:
To PGP-encrypt a file:

java -classpath "classes;bcprov-jdk14-133.jar" org.bouncycastle.openpgp.examples.KeyBasedFileProcessor -e -a [target-file-name] [public-key-file]

Example 3:
To PGP-decrypt a file:

java -classpath "classes;bcprov-jdk14-133.jar" org.bouncycastle.openpgp.examples.KeyBasedFileProcessor -d [encrypted-file-name] [private-key-file] [pass-phrase]