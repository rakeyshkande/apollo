#!/usr/bin/perl
#
# This script will tail and parse the passed logfile, looking for OrderService calls.
# Each will be appended to a file (created each elapsed minute).  The companion script orderServiceSummary.pl 
# will read and send the data to the dashboard app (then remove the files).
#
use strict;
use warnings;

my $MIN_LINES_PER_FILE = 20;   # Min number of lines before a file is written
my $DELIM = "~~";

my %entriesHash;
my @entriesArray = ("");
my $curEntry;
my $lineCntr = 0;

my $hostname = `hostname`;
chomp($hostname);
my $logfile = $ARGV[0];

open FH, "tail -f ./$logfile|";
while( <FH> ) {
   my ($action, $node, $client, $status, $curEntry) = "";
   if (m/APPLICATION=ORDER_SERVICE/) {
     ($node,$client,$action) = m/NODE=(.+);CLIENT_ID=(.+);ACTION_TYPE=(.+);STATS_CATEGORY=/;
     
     if ($action eq "getOccasion" || $action eq "getProduct" || $action eq "getAddon" || $action eq "getProductByCategory" || $action eq "getDeliveryDate") {
         $lineCntr++;
         ($status) = m/;STATUS=(\w+)[;]*/;
         $curEntry = $action . $DELIM . $node . $DELIM . $client . $DELIM . $status;
         push(@entriesArray, $curEntry);
      }
   }

   if ($lineCntr >= $MIN_LINES_PER_FILE) {
       my ($sec,$min,$hour,$day,$month,$year,$wday,$yday,$isdst) = localtime(time);
       $year += 1900; 
       $month += 1;
    
       my $outfilename = sprintf "outOrderService.%s.%02d%02d%4d.%02d%02d", $hostname, $month, $day, $year, $hour, $min;
       print "$outfilename\n";
       open (STATFILE, ">> $outfilename" );
       foreach (@entriesArray) {
          if (length($_) > 0) {
            print STATFILE $_;
            print STATFILE "\n";
          }
       }
       close (STATFILE);
       $lineCntr = 0;
       @entriesArray = ("");
   }
}

exit;
