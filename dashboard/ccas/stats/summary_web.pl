use HTTP::Date;
use Term::ANSIColor;

$gpct=0;
$amex=0;
$dec=0;
$adec=0;
$hostname = `hostname`;
chomp($hostname);
$sourceClient = $ARGV[0];

open FH, "tail -f ../log/test_$sourceClient.log|";
#open FH, "tail -1000 test_$sourceClient.log|";
while( <FH> ) {
   $old_gpct = $gpct;
   if ( m/GPCT Purchase Approved/ ) {
      $gpct++;
   }

   $old_amex = $amex;
   if ( m/AMEX Purchase Approved/ ) {
      $amex++;
   }

   $old_dec = $dec;
   if ( m/GPCT Purchase Declined/ ) {
      $dec++;
   }

   $old_adec = $adec;
   if ( m/AMEX Purchase Declined/ ) {
      $adec++;
   }

   if ( $old_gpct == $gpct && $old_amex == $amex && $old_dec == $dec && $old_adec == $adec ) {
      # We were fed a line that has nothing to do with a CC being explicitly approved
      # or denied, so therefore just loop.
      next;
   }

   my ($date, $time) = split(" ", HTTP::Date::time2iso());
   my ($year, $month, $day) = split("-",$date);
   my ($hour, $min) = split(":",$time);

   print  "outfile.$hostname.$sourceClient.$month$day$year.$hour$min\n" ;
   open (STATFILE, ">>outfile.$hostname.$sourceClient.$month$day$year.$hour$min" );


   if ( $old_amex != $amex) {
      print STATFILE "AMEX|APPROVED\n";
   }
   
   if ( $old_gpct != $gpct ) {
      print STATFILE "GPCT|APPROVED\n";
   }

   if ( $old_dec != $dec ) {
      print STATFILE "GPCT|DECLINED\n";
   }
 
   if ( $old_adec != $adec ) {
      print STATFILE "AMEX|DECLINED\n";
   }
   
   close (STATFILE);
}
