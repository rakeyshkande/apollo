  use strict;
  use warnings;
  use LWP 5.64;

  while ()
  {
    processFiles();
    sleep(30);
  }


  sub processFiles
  {
  opendir(DIR, ".");
  my @files = grep(/outfile/,readdir(DIR));
  closedir(DIR);

  my $file;
  foreach $file (@files) 
  {
    print "$file\n";
    open (STATFILE, "$file");

    my @filenameSplit = split(/\./,$file);

    my $authServer = $filenameSplit[1];
    my $authClient = $filenameSplit[4];
    my $authDate = $filenameSplit[5];
    my $authTime = $filenameSplit[6];
    my $authDateTime = "$authDate$authTime";

    # Get the current dateTime
    # if the current is the same as the file, skip
    my ($date, $time) = split(" ", HTTP::Date::time2iso());
    my ($year, $month, $day) = split("-",$date);
    my ($hour, $min) = split(":",$time);
    my $currentDateTime = "$month$day$year$hour$min";
    print "dates = $currentDateTime, $authDateTime\n";
    if ($currentDateTime == $authDateTime)
    {
      print "Current date time, skipping";
      next;
    }

    my $record;
    my $GPapprovals = 0;
    my $GPdeclines = 0;
    my $AMapprovals = 0;
    my $AMdeclines = 0;

    while ($record = <STATFILE>) 
    {
      my @fields = split(/\|/,$record);
      my $cardType = $fields[0];
      my $authType = $fields[1];

      if ($authType =~ /APPROVED/)
      {
          if ($cardType =~ /GPCT/)
          {
            $GPapprovals++;
          }
          else
          {
            $AMapprovals++;
          }
      }
      else
      {
          if ($cardType =~ /GPCT/)
          {
            $GPdeclines++;
          }
          else
          {
            $AMdeclines++;
          }
      }
    }
    my $amexPost = &postResults($authServer,$authClient,$authDateTime,"AMEX",$AMapprovals,$AMdeclines);
    my $gpPost = &postResults($authServer,$authClient,$authDateTime,"GPCT",$GPapprovals,$GPdeclines);

    close(STATFILE);

    if ($amexPost || $gpPost)
    {
      print "delete $file";
      unlink($file);
    }
    else
    {
      print "dont delete $file $amexPost $gpPost";
    }
  }
  }

  # Post the results of a file to apollo to save in the db
  sub postResults {
    my $authServer = $_[0];
    my $authClient = $_[1];
    my $authTime = $_[2];
    my $cardType = $_[3];
    my $approvals = $_[4];
    my $declines = $_[5];

    print "$authServer $authClient $authTime $cardType $approvals $declines\n";

  my $browser = LWP::UserAgent->new;
  
  my $url = 'http://cobalt3.ftdi.com:7778/dashboard/AuthorizationStats.do';
  my $response = $browser->post( $url,
    [ 'authServer' => "$authServer",
      'authClient' => "$authClient", 
      'cardType' => "$cardType", 
      'approvals' => "$approvals",
      'declines' => "$declines",
      'authorizationTime' => "$authTime",
    ]
  );

    # return the results of the post
    ( $response->content =~ m{SUCCESS} );
  }


