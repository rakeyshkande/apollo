<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">

    <script type="text/javascript" src="js/dashboard.js"></script>

    <script language="javascript">
        setTimeout("document.orderserviceform.submit()",60000);
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="OrderService" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <c:set var="leftOrderServiceRangeText" value="${dashboardForm.leftRangeStartDate} ${dashboardForm.leftRangeStartTime} - ${dashboardForm.leftRangeEndDate} ${dashboardForm.leftRangeEndTime}" />
    <c:set var="rightOrderServiceRangeText" value="${dashboardForm.rightRangeStartDate} ${dashboardForm.rightRangeStartTime} - ${dashboardForm.rightRangeEndDate} ${dashboardForm.rightRangeEndTime}" />

    <div class="blackdashboard"> <!-- Whole page -->
      <form name="orderserviceform" action="OrderServiceDashboard.do" method="post" >
        <input type="hidden" name="securitytoken" value="${dashboardForm.securitytoken}" />
        <input type="hidden" name="context" value="${dashboardForm.context}" />
        <div class="full">  <!-- Filter Row -->
          <div class="triSectionLeft" >  <!-- left selection -->
            <input type="radio" name="groupBy" value="client_id" ${dashboardForm.clientIdGroupBySelected}>Client ID</input>
            <input type="radio" name="groupBy" value="action_type" ${dashboardForm.actionTypeGroupBySelected}>Action Type</input>
            <input type="radio" name="groupBy" value="node" ${dashboardForm.nodeGroupBySelected}>Node</input>
            <input type="radio" name="groupBy" value="status_code" ${dashboardForm.statusCodeGroupBySelected}>Status Code</input>
          </div>
          <div id="buttonSelection"> <!-- Button Row -->
              <input type="submit" name="applyFilter" value="Apply Filter" />
          </div>
        </div>
        <div class="full">  <!-- Selection Row -->
          <div class="leftSide" onclick="return toggleRangeSelect()">  <!-- left selection -->
            <center><h3>Date Range<br>${leftOrderServiceRangeText}</h3></center>
          </div>
          <div class="rightSide" onclick="return toggleRangeSelect()">  <!-- right selection -->
            <center><h3>Date Range<BR>${rightOrderServiceRangeText}</h3></center>
          </div>
        </div>
        <div id="selectionDiv" class="displayNone">  <!-- Selection Panel -->
          <div class="full">  <!-- Selection Row -->
            <div class="leftSide" id="leftSelection">  <!-- left selection -->
              <table>
                <tr>
                  <td colspan="3">
                    <input type="radio" name="leftRangeSelect" value="1" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedLastHour}>Last Hour</input>
                    <input type="radio" name="leftRangeSelect" value="2" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedLastDay}>Today</input>
                    <input type="radio" name="leftRangeSelect" value="3" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedYesterday}>Yesterday</input>
                    <input type="radio" name="leftRangeSelect" value="0" onclick="return openLeftCustomRange()" ${dashboardForm.leftSelectedCustom}>Custom</input>
                  </td>
                </tr>
                <tr id="selectionLeftRangeHeaderRow" class="displayNone">
                  <td>&nbsp;</td>
                  <td>Date (mm/dd/yyy)</td>
                  <td>Time</td>
                </tr>
                <tr id="selectionLeftRangeFromRow" class="displayNone">
                  <td>From:</td>
                  <td>
                    <input type="text" name="leftRangeStartDate" size="10" value="${dashboardForm.leftRangeStartDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeStartTime" size="4" value="${dashboardForm.leftRangeStartTime}"/>
                  </td>
                </tr>
                <tr id="selectionLeftRangeToRow" class="displayNone">
                  <td>To:</td>
                  <td>
                    <input type="text" name="leftRangeEndDate" size="10" value="${dashboardForm.leftRangeEndDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeEndTime" size="4" value="${dashboardForm.leftRangeEndTime}"/>
                  </td>
                </tr>
              </table>
            </div>
            <div class="rightSide" id="rightSelection">  <!-- right selection -->
              <table>
                <tr>
                  <td colspan="3">
                    <input type="radio" name="rightRangeSelect" value="1" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedLastHour}>Last Hour</input>
                    <input type="radio" name="rightRangeSelect" value="2" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedLastDay}>Today</input>
                    <input type="radio" name="rightRangeSelect" value="3" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedYesterday}>Yesterday</input>
                    <input type="radio" name="rightRangeSelect" value="0" onclick="return openRightCustomRange()" ${dashboardForm.rightSelectedCustom}>Custom</input>
                  </td>
                </tr>
                <tr id="selectionRightRangeHeaderRow" class="displayNone">
                  <td>&nbsp;</td>
                  <td>Date (mm/dd/yyy)</td>
                  <td>Time</td>
                </tr>
                <tr id="selectionRightRangeFromRow" class="displayNone">
                  <td>From:</td>
                  <td>
                    <input type="text" name="rightRangeStartDate" size="10" value="${dashboardForm.rightRangeStartDate}"/>
                  </td>
                  <td>
                    <input type="text" name="rightRangeStartTime" size="4" value="${dashboardForm.rightRangeStartTime}"/>
                  </td>
                </tr>
                <tr id="selectionRightRangeToRow" class="displayNone">
                  <td>To:</td>
                  <td>
                    <input type="text" name="rightRangeEndDate" size="10" value="${dashboardForm.rightRangeEndDate}"/>
                  </td>
                  <td>
                    <input type="text" name="rightRangeEndTime" size="4" value="${dashboardForm.rightRangeEndTime}"/>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="full">
            <div class="leftSide" id="buttonSelection"> <!-- Button Row -->
                <input type="submit" name="changeDates" value="Change Dates" />
            </div>
            <div class="rightSide" id="buttonSelection"> <!-- Button Row -->
                <input type="submit" name="changeDates" value="Change Dates" />
            </div>
          </div>
        </div>
      </form>
      <div> <!-- Top graph Row -->
        <div class="leftSide">
            <cewolf:chart
                id="LeftOrderServiceXYChart"
                type="timeseries"
                yaxislabel="Order Count"
                title="Order Service Detail">
                <cewolf:gradientpaint>
                    <cewolf:point x="0" y="0" color="#ffe87c"/>
                    <cewolf:point x="0" y="500" color="#faf8cc"/>
                </cewolf:gradientpaint>
                <cewolf:data>
                    <cewolf:producer id="leftOrderServiceDataset"/>
                </cewolf:data>
                <cewolf:chartpostprocessor id="leftOrderServiceChartPostProcessor">
                </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="LeftOrderServiceXYChart" renderer="cewolf" width="500" height="500" ondbclick="" >
                    <cewolf:map tooltipgeneratorid="leftOrderServiceDataset" />
            </cewolf:img>
        </div>
        <div class="rightSide">
            <cewolf:chart
                id="RightOrderServiceXYChart"
                type="timeseries"
                yaxislabel="Order Count"
                title="Order Service Detail">
                <cewolf:gradientpaint>
                    <cewolf:point x="0" y="0" color="#ffe87c"/>
                    <cewolf:point x="0" y="500" color="#faf8cc"/>
                </cewolf:gradientpaint>
                <cewolf:data>
                    <cewolf:producer id="rightOrderServiceDataset"/>
                </cewolf:data>
                <cewolf:chartpostprocessor id="rightOrderServiceChartPostProcessor">
                </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="RightOrderServiceXYChart" renderer="cewolf" width="500" height="500" ondbclick="" >
                    <cewolf:map tooltipgeneratorid="RightOrderServiceDataset" />
            </cewolf:img>
        </div>
      </div>
    </div>
    <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
