<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    <script type="text/javascript" src="js/dashboard.js"></script>
  </head>

  <body bgcolor="#FFFFFF"  >
    <div class="maindiv">
      <c:set var="pageName" value="Oracle" />
      <%@ include file="header.jsp" %>
      <%@ include file="menu.jsp" %>

      <div class="blackdashboard">
        <div id="summary" class="oracleGraphRow" >
          <div class="oracleGraphRowSidebar${dashboardForm.oracleStatusVO.overallStatus}" >
            <p><b>Cluster</b></p>
            <center><img src="images/stoplight-${dashboardForm.oracleStatusVO.overallStatus}.gif" onclick="toggleDivDisplay('clusterDiv')"/></center>
          </div>
          <c:forEach var="instance" items="${instances}">
            <div class="oracleGraphRowSidebar${dashboardForm.oracleStatusVO.oracleInstanceStatusMap[instance].overallStatus}" >
              <p><b>${instance}</b></p>
              <center><img src="images/stoplight-${dashboardForm.oracleStatusVO.oracleInstanceStatusMap[instance].overallStatus}.gif" onclick="toggleDivDisplay('instance${instance}Div')"/></center>
            </div>
          </c:forEach>
          <div class="oracleGraphRowSidebar${dashboardForm.oracleStatusVO.storageStatus}" >
            <p><b>Storage</b></p>
            <center><img src="images/stoplight-${dashboardForm.oracleStatusVO.storageStatus}.gif" onclick="toggleDivDisplay('storageDiv')"/></center>
          </div>
        </div>
        <div id="clusterDiv" class="oracleGraphRow" >
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleLocksChart"
                type="thermometer"
                xaxislabel=""
                yaxislabel=""
                title="Locks"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleLocksData"/>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleThermometerPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"locks\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleLocksChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleLocksData" />
            </cewolf:img>
          </div>
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleLocksAgedChart"
                type="thermometer"
                xaxislabel=""
                yaxislabel=""
                title="Aged Locks"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleLocksAgedData"/>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleThermometerPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"agedlocks\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleLocksAgedChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleLocksAgedData" />
            </cewolf:img>
          </div>
          <div class="leftSideGraph">
            <!-- sessions/instance -->
            <cewolf:chart 
                id="OracleSessionsChart"
                type="verticalbar"
                xaxislabel="Instance"
                yaxislabel="Session Count"
                title="Oracle Sessions"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleInstanceData" >
                  <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"sessions\" %>" />
                </cewolf:producer>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleInstanceBarChartPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"sessions\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleSessionsChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleSessionsData" />
            </cewolf:img>
          </div>
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleGCRemasterChart"
                type="verticalbar"
                xaxislabel="Instance"
                yaxislabel="Wait"
                title="GC Remaster Wait"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleInstanceData" >
                  <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"gcremaster\" %>" />
                </cewolf:producer>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleInstanceBarChartPostProcessor">
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleGCRemasterChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oraclegcremasterData" />
            </cewolf:img>
          </div>
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleLogFileSyncChart"
                type="verticalbar"
                xaxislabel="Instance"
                yaxislabel="Wait"
                title="Log File Sync wait(ms)"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleInstanceData" >
                  <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"logfileresync\" %>" />
                </cewolf:producer>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleInstanceBarChartPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"logfilesync\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleLogFileSyncChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleLogFileSyncData" />
            </cewolf:img>
          </div>
        </div>
        <c:forEach var="instance" items="${instances}">
          <div id="instance${instance}Div" class="oracleGraphRow" >
            <div class="oracleGraphRowSidebar${dashboardForm.oracleStatusVO.oracleInstanceStatusMap[instance].overallStatus}" >
              <p>${instance}</p>
              <img src="images/m4000-${dashboardForm.oracleStatusVO.oracleInstanceStatusMap[instance].overallStatus}.gif" />
            </div>
            <c:if test='${dashboardForm.showLoadGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleLoad${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="Load"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"load\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"load\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleLoad${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleCache${instance}Chart"
                    type="verticalbar"
                    xaxislabel=""
                    yaxislabel="% Read"
                    title="Cache"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"cache\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleInstanceBarChartPostProcessor">
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleCache${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="CacheInstanceData" />
                </cewolf:img>
              </div>
            </div>
            <c:if test='${dashboardForm.showPhysicalReadsGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OraclePhysicalReads${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="Physical Reads"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"physicalreads\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"physicalreads\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OraclePhysicalReads${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
            <c:if test='${dashboardForm.showLogicalReadsGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleLogicalReads${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="Logical Reads"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"logicalreads\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"logicalreads\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleLogicalReads${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
            <c:if test='${dashboardForm.showTransactionsGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleTransactions${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="Transactions"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"transactions\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"transactions\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleTransactions${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
            <c:if test='${dashboardForm.showUserCallsGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleUserCalls${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="UserCalls"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"usercalls\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"usercalls\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleUserCalls${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
            <c:if test='${dashboardForm.showMemoryGraph=="TRUE"}' >
            <div class="instanceSection" >
              <div class="instanceSectionGraph">
                <cewolf:chart 
                    id="OracleMemory${instance}Chart"
                    type="meter"
                    xaxislabel="Instance"
                    yaxislabel="Wait"
                    title="Memory Used"
                    showlegend="false"> 
                  <cewolf:data>
                    <cewolf:producer id="oracleInstanceData">
                      <cewolf:param name="dataset" value="<%=(java.io.Serializable)\"memoryused\" %>" />
                      <cewolf:param name="instance" value="${instance}" />
                    </cewolf:producer>
                  </cewolf:data>
                  <cewolf:colorpaint color="#000000" >
                  </cewolf:colorpaint>
                  <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                      <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"memoryused\" %>" />
                  </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="OracleMemory${instance}Chart" renderer="cewolf" width="${dashboardForm.smallGraphSize}" height="${dashboardForm.smallGraphSize}" >
                  <cewolf:map tooltipgeneratorid="oracleInstanceData" />
                </cewolf:img>
              </div>
            </div>
            </c:if>
          </div>
        </c:forEach>
        <div id="storageDiv" class="oracleGraphRow" >
          <c:if test='${dashboardForm.showDataDiskgroupGraph=="TRUE"}' >
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleDataDiskgroupChart"
                type="meter"
                xaxislabel=""
                yaxislabel=""
                title="Data Diskgroup Free"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleDataDiskgroupData"/>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"datadiskgroup\" %>" />
                  <cewolf:param name="thresholdType" value="<%=(java.io.Serializable)\"reverse\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleDataDiskgroupChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleDataDiskgroupData" />
            </cewolf:img>
          </div>
          </c:if>
          <c:if test='${dashboardForm.showFlashDiskgroupGraph=="TRUE"}' >
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleFlashDiskgroupChart"
                type="meter"
                xaxislabel=""
                yaxislabel=""
                title="Flash Diskgroup Free"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleFlashDiskgroupData"/>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"flashdiskgroup\" %>" />
                  <cewolf:param name="thresholdType" value="<%=(java.io.Serializable)\"reverse\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleFlashDiskgroupChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleFlashDiskgroupData" />
            </cewolf:img>
          </div>
          </c:if>
          <c:if test='${dashboardForm.showTablespaceFreeGraph=="TRUE"}' >
          <div class="leftSideGraph">
            <cewolf:chart 
                id="OracleTablespaceFreeChart"
                type="meter"
                xaxislabel=""
                yaxislabel=""
                title="Tablespace Free %"
                showlegend="false"> 
              <cewolf:data>
                <cewolf:producer id="oracleTablespaceFreeData"/>
              </cewolf:data>
              <cewolf:colorpaint color="#000000" >
              </cewolf:colorpaint>
              <cewolf:chartpostprocessor id="oracleMeterChartPostProcessor">
                  <cewolf:param name="threshold" value="<%=(java.io.Serializable)\"tablespacefree\" %>" />
                  <cewolf:param name="thresholdType" value="<%=(java.io.Serializable)\"reverse\" %>" />
              </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OracleTablespaceFreeChart" renderer="cewolf" width="${dashboardForm.largeGraphSize}" height="${dashboardForm.largeGraphSize}" >
              <cewolf:map tooltipgeneratorid="oracleTablespaceFreeData" />
            </cewolf:img>
          </div>
          </c:if>
        </div>
      </div>
      <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
