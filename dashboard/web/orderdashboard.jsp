<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  	<script  src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="Order" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <div>
            <cewolf:chart 
                id="OrderOriginXYChart"
                type="timeseries"
                yaxislabel="Order Count"
                title="Order Flow Detail"> 
                <cewolf:gradientpaint>
                    <cewolf:point x="0" y="0" color="#ffe87c"/>
                    <cewolf:point x="0" y="300" color="#faf8cc"/>
                </cewolf:gradientpaint>
                <cewolf:data>
                    <cewolf:producer id="orderOriginXyData"/>
                </cewolf:data>
                <cewolf:chartpostprocessor id="orderOriginChartPostProcessor">
                </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OrderOriginXYChart" renderer="cewolf" width="550" height="350" ondbclick="" style="width:55%; height: auto">
                    <cewolf:map tooltipgeneratorid="orderOriginXyData" />
            </cewolf:img>
            
            <div class="rightSideSmallTable" style="width:40%">
					<display:table id="orderSetupVO" name="orderTableStatsMap" class="altstripe smalltable" style="width:90%; margin-top:100px;">
			            <display:column property="attributeName" title=""/>
			            
			            <display:column title="Status">
			              <c:set var="imageName" value="images/green.jpg" />
			              <c:if test="${  orderSetupVO.previousHourPercentage < orderSetupVO.criticalThreshold }">
			                <c:set var="imageName" value="images/red.jpg" />
			              </c:if>
			              <c:if test="${orderSetupVO.attributeName == 'Total'}">
			              	<c:set var="imageName" value=""/>
		           		  </c:if>
			            
			              <img src="${imageName}" id="imagen" alt=""  />
			              
			              <script type="text/javascript">
			           	        $(document).ready(function(){
			           	            if ($("#imagen").attr("src") == "") {
			           	                $("#imagen").hide();
			           	            }
			           	            else {
			           	                $("#imagen").show();
			           	            }
			           	          });
			           	        
		           			</script>
			              
			            </display:column>
			            <display:column property="currentHourPercentage" title="Order Count (Current)"/>
			          	<display:column  title="% Change Since Previous Hour">
			            	<c:out value = "${orderSetupVO.previousHourPercentage}"/>
		           		 	<c:out value = "%"/>
			            </display:column>
			          	<display:column title="&nbsp;Max % change in decrease of Orders &nbsp;">
			            	<c:if test="${orderSetupVO.attributeName != 'Total'}">
		           		 		<c:out value = "${orderSetupVO.criticalThreshold}"/>
		           		 		<c:out value = "%"/>
		           		 	</c:if>
			            </display:column>
			          </display:table>
                </div>
 
    </div>
    <%@ include file="footer.jsp" %>
  </body>
</html>
