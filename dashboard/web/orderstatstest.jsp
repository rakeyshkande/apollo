 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Order Service Stats Test</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  </head>

  <body>
  <form method="post" action="OrderServiceStats.do">
	node <input type="text" name="node"/><br/>
  	clientId <input type="text" name="clientId"/><br/>
  	actionType <input type="text" name="actionType"/><br/>
  	count <input type="text" name="count"/><br/>
  	statusCode <input type="text" name="statusCode" value="success|failure|partial_success"/><br/>
  	statsTimestamp <input type="text" name="statsTimestamp" value="MMddyyyyHHmm"/><br/>
<input type="Submit"/>
  </form>
</body>
</html>