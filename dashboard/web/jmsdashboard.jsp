<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="JMS" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <div>
        <div class="leftSideGraph">
          <cewolf:chart 
            id="JMSOrderFlowBarChart"
            type="verticalbar"
            xaxislabel="JMS Queue"
            yaxislabel="Queue Count"
            title="JMS Order Flow Queue"
            showlegend="false"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="jmsOrderFlowBarData"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="jmsOrderFlowChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="JMSOrderFlowBarChart" renderer="cewolf" width="700" height="300" style="width:55%; height:auto" >
            <cewolf:map tooltipgeneratorid="jmsOrderFlowBarData" />
          </cewolf:img>
        
        <div class="rightSideSmallTable" style="width:40%; margin-top:30px; margin-right:30px;">
          <display:table id="statsOrderVO" name="dashboardForm.orderStatsMap" class="altstripe smalltable" style="width:101%; height: auto;">
            <display:caption>JMS Order Queue Counts</display:caption>
            <display:column property="setupVO.queueShortName" title="Name"/>
            <display:column property="queueName" title="Queue Name"/>
            <display:column property="queueCount" title="Count"/>
            <display:column title="Status">
              <c:set var="imageName" value="images/green.jpg" />
              <c:if test="${statsOrderVO.queueCount > statsOrderVO.setupVO.warningThreshold}">
                <c:set var="imageName" value="images/yellow.jpg" />
              </c:if>
              <c:if test="${statsOrderVO.queueCount > statsOrderVO.setupVO.criticalThreshold}">
                <c:set var="imageName" value="images/red.jpg" />
              </c:if>
              <img src="${imageName}" alt=""  />
            </display:column>
            <display:column property="setupVO.warningThreshold" title="Warn"/>
            <display:column property="setupVO.criticalThreshold" title="Crit"/>
            <display:column property="oldestMessage" title="Oldest"/>
          </display:table>
        </div>
      </div>
      </div>
      <div>
        <div class="leftSideGraph">
          <cewolf:chart 
            id="JMSOtherChart"
            type="verticalbar"
            xaxislabel="JMS Queue"
            yaxislabel="Queue Count"
            title="JMS Queue"
            showlegend="false"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="jmsOtherBarData"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="jmsOtherChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="JMSOtherChart" renderer="cewolf" width="700" height="300" style="width:55%; height: auto" >
            <cewolf:map tooltipgeneratorid="jmsOtherBarData" />
          </cewolf:img>
        <div class="rightSideSmallTable" style="width:40%; margin-right:30px;" >
          <display:table id="statsOtherVO" name="dashboardForm.otherStatsMap" class="altstripe smalltable" style="width:101%; height:auto">
            <display:caption>JMS Other Queue Counts</display:caption>
            <display:column property="setupVO.queueShortName" title="Name"/>
            <display:column property="queueName" title="Queue Name"/>
            <display:column property="queueCount" title="Count"/>
            <display:column title="Status">
              <c:set var="imageName" value="images/green.jpg" />
              <c:if test="${statsOtherVO.queueCount > statsOtherVO.setupVO.warningThreshold}">
                <c:set var="imageName" value="images/yellow.jpg" />
              </c:if>
              <c:if test="${statsOtherVO.queueCount > statsOtherVO.setupVO.criticalThreshold}">
                <c:set var="imageName" value="images/red.jpg" />
              </c:if>
              <img src="${imageName}" alt=""  />
            </display:column>
            <display:column property="setupVO.warningThreshold" title="Warn"/>
            <display:column property="setupVO.criticalThreshold" title="Crit"/>
            <display:column property="oldestMessage" title="Oldest"/>
          </display:table>
        </div>
      </div>
    </div>
    <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
