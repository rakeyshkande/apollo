    <div class="menu">
      <a href="Dashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Dashboard</a>
      <a href="QueueDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Queue</a>
      <a href="FloristDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Florist</a>
      <a href="OrderDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Order</a>
      <a href="InventoryDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Inventory</a>
      <a href="JMSDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">JMS</a>
      <a href="PaymentDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Payment</a>
      <a href="OracleDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">Oracle</a>
      <a href="OrderServiceDashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}">OrderService</a>
      <a href="/secadmin/security/Login.do">Logout</a>
    </div>
