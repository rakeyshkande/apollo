    <div class="header">
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" align="left"><img border="0" height="79" src="images/brand_header_logo.jpg" width="84"/></td>
            <td width="50%" align="center" colspan="1" class="Header"> ${pageName} Dashboard</td>
            <td width="30%" align="left"><b>Last Updated: <fmt:formatDate value="${dashboardForm.lastUpdated}" pattern="MM/dd/yyyy HH:mm" /></b></td>
        </tr>
        <tr>
            <td colspan="3"><hr/></td>
        </tr>
      </table>
    </div>
