<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="Florist" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <div>
      <div>
        <cewolf:chart 
            id="FloristGotoBarChart"
            type="verticalbar3d"
            xaxislabel="Region"
            yaxislabel="Goto Florist Shutdown Count"
            title="Goto Florist Shutdown"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="floristGotoBarData"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="floristGotoBarChartPostProcessor">
            </cewolf:chartpostprocessor>
        </cewolf:chart>
        <cewolf:img chartid="FloristGotoBarChart" renderer="cewolf" width="900" height="300" >
            <cewolf:map tooltipgeneratorid="floristGotoBarData" />
        </cewolf:img>
      </div>
      <div>
        <cewolf:chart 
            id="FloristBarChart"
            type="verticalbar3d"
            yaxislabel="Florist Shutdown Percent"
            xaxislabel="Region"
            title="Florist Shutdown Percent"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="floristBarData"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="floristBarChartPostProcessor">
            </cewolf:chartpostprocessor>
        </cewolf:chart>
        <cewolf:img chartid="FloristBarChart" renderer="cewolf" width="900" height="300" >
            <cewolf:map tooltipgeneratorid="floristBarData" />
        </cewolf:img>
      </div>
    </div>
    <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
