<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <meta http-equiv="X-UA-Compatible" content="IE=11" />
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    <script  src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="js/dashboard.js"></script>
  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv"  style="width:99%">
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <!-- 1st row start -->
    <div>
        <!-- 1st row left side start --> 
        <div class="leftSide" style="width:49%;">
                <cewolf:chart 
                    id="FloristXYChart"
                    type="timeseries"
                    yaxislabel="Percent %"
                    title="Florist Status"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="floristXyData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="floristChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="FloristXYChart" renderer="cewolf" width="450" height="300" style = "width:49%; height:auto">
                    <cewolf:map tooltipgeneratorid="floristXyData" />
                </cewolf:img>
                
                <!-- 1st row left small table --> 
        		<div class="rightSideSmallTable">
					<display:table id="floristSetupVO" name="floristTableStatsMap" class="altstripe smalltable">
			            <display:column property="attributeName" title=""/>
			            
			            <display:column title="Status">
			              <c:set var="imageName" value="images/green.jpg" />
			              <c:if test="${floristSetupVO.currentHourPercentage > floristSetupVO.warningThreshold}">
			                <c:set var="imageName" value="images/yellow.jpg" />
			              </c:if>
			              <c:if test="${floristSetupVO.currentHourPercentage > floristSetupVO.criticalThreshold}">
			                <c:set var="imageName" value="images/red.jpg" />
			              </c:if>
			              
			              <img src="${imageName}" alt=""  />
			              
			            </display:column>
			            <display:column title="Current Hour">
		           		 	<c:out value = "${floristSetupVO.currentHourPercentage}%"/>
		           		</display:column>
			            <display:column  title="Previous Hour">
			            	<c:out value = "${floristSetupVO.previousHourPercentage}%"/>
			            </display:column>
			            
			        	<display:column title="Warn">
		           		 	<c:out value = "${floristSetupVO.warningThreshold}%"/>
		           		</display:column>
			            <display:column  title="&nbsp;Crit&nbsp;">
			            	<c:out value = "${floristSetupVO.criticalThreshold}%"/>
			            </display:column>
			          </display:table>
                </div>
                <!-- 1st row left small table end --> 
        </div>
        <!-- 1st row left side End -->
        <!-- 1st row right side Start -->
		<div class="rightSide" style="width:49%;">
            <cewolf:chart 
                id="OrderOriginXYChart"
                type="timeseries"
                yaxislabel="Order Count"
                title="Order Flow"> 
                <cewolf:gradientpaint>
                    <cewolf:point x="0" y="0" color="#ffe87c"/>
                    <cewolf:point x="0" y="300" color="#faf8cc"/>
                </cewolf:gradientpaint>
                <cewolf:data>
                    <cewolf:producer id="orderOriginXyData"/>
                </cewolf:data>
                <cewolf:chartpostprocessor id="orderOriginChartPostProcessor">
                </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OrderOriginXYChart" renderer="cewolf" width="450" height="300" style = "width:49%; height:auto">
                <cewolf:map tooltipgeneratorid="orderOriginXyData" />
            </cewolf:img>
	    	
	    	 <!-- 1st row 2nd half right small table --> 
	    	<div class="rightSideSmallTable">
					<display:table id="orderSetupVO" name="orderTableStatsMap" class="altstripe smalltable">
			            <display:column property="attributeName" title=""/>
			            
			            <display:column title="Status">
			              <c:set var="imageName" value="images/green.jpg" />
			              <c:if test="${  orderSetupVO.previousHourPercentage < orderSetupVO.criticalThreshold }">
			                <c:set var="imageName" value="images/red.jpg" />
			              </c:if>
			              <c:if test="${orderSetupVO.attributeName == 'Total'}">
			              	<c:set var="imageName" value=""/>
		           		  </c:if>
			            
			              <img src="${imageName}" id="imagen" alt=""  />
			              
			              <script type="text/javascript">
			           	        $(document).ready(function(){
			           	            if ($("#imagen").attr("src") == "") {
			           	                $("#imagen").hide();
			           	            }
			           	            else {
			           	                $("#imagen").show();
			           	            }
			           	        });
		           			</script>
			              
			            </display:column>
			            <display:column property="currentHourPercentage" title="Order Count (Current)"/>
			            <display:column  title="% Change Since Previous Hour">
			            	<c:out value = "${orderSetupVO.previousHourPercentage}%"/>
			            </display:column>
			            <display:column title="&nbsp;Max % change in decrease of Orders &nbsp;">
			            	<c:if test="${orderSetupVO.attributeName != 'Total'}">
		           		 		<c:out value = "${orderSetupVO.criticalThreshold}%"/>
		           		 	</c:if>
			            </display:column>
			          </display:table>
                </div>
                
	         <!-- 1st row 2nd half right small table end --> 
	    </div>         
        <!-- 1st row right side End --> 
    </div>
    <!-- 1st row End -->
    <!-- 2nd row start-->
    <div>
        <div class="leftSide" style="width:49%;">
                <cewolf:chart 
                    id="QueueXYChart"
                    type="timeseries"
                    yaxislabel="Queue Count"
                    title="Queue Status"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="queueXyData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="queueChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="QueueXYChart" renderer="cewolf" width="450" height="300"  style = "width:49%; height:auto">
                    <cewolf:map tooltipgeneratorid="queueXyData" />
                </cewolf:img>
        		
        	<!-- 2nd row left small table --> 
	    	<div class="rightSideSmallTable">
					<display:table id="queueSetupVO" name="queueTableStatsMap" class="altstripe smalltable">
			            <display:column property="attributeName" title=""/>
			            
			            <display:column title="Status">
			              <c:set var="imageName" value="images/green.jpg" />
			              <c:if test="${ queueSetupVO.previousHourPercentage > queueSetupVO.warningThreshold}">
			                <c:set var="imageName" value="images/yellow.jpg" />
			              </c:if>
			              <c:if test="${queueSetupVO.previousHourPercentage > queueSetupVO.criticalThreshold}">
			                <c:set var="imageName" value="images/red.jpg" />
			              </c:if>
			              <c:if test="${queueSetupVO.attributeName == 'Total'}">
			              	<c:set var="imageName" value=""/>
		           		  </c:if>
			            
			              <img src="${imageName}" id="imagen" alt=""  />
			              
			              <script type="text/javascript">
			           	        $(document).ready(function(){
			           	            if ($("#imagen").attr("src") == "") {
			           	                $("#imagen").hide();
			           	            }
			           	            else {
			           	                $("#imagen").show();
			           	            }
			           	        });
		           			</script>
			              
			            </display:column>
			            <display:column property="currentHourPercentage" title="Queue Count (Current)"/>
			            <display:column  title="% Change Since Previous Hour">
			            	<c:out value = "${queueSetupVO.previousHourPercentage}%"/>
			            </display:column>
			          	<display:column title="Warn">
		           		 	<c:if test="${queueSetupVO.attributeName != 'Total'}">
		           		 		<c:out value = "${queueSetupVO.warningThreshold}%"/>
		           		 	</c:if>
		           		</display:column>
			            <display:column  title="&nbsp;Crit&nbsp;">
			            	<c:if test="${queueSetupVO.attributeName != 'Total'}">
		           		 		<c:out value = "${queueSetupVO.criticalThreshold}%"/>
		           		 	</c:if>
			            </display:column>
			          </display:table>
                </div>
	         <!-- 2nd row left small table end --> 
        
        </div>
        
        <div class="rightSide" style="width:49%;">
            <cewolf:chart 
                id="OrderDeliveryXYChart"
                type="timeseries"
                yaxislabel="Order % Distribution"
                title="Inventory Flow"> 
                <cewolf:gradientpaint>
                    <cewolf:point x="0" y="0" color="#ffe87c"/>
                    <cewolf:point x="0" y="300" color="#faf8cc"/>
                </cewolf:gradientpaint>
                <cewolf:data>
                    <cewolf:producer id="orderDeliveryXyData"/>
                </cewolf:data>
                <cewolf:chartpostprocessor id="orderDeliveryChartPostProcessor">
                </cewolf:chartpostprocessor>
            </cewolf:chart>
            <cewolf:img chartid="OrderDeliveryXYChart" renderer="cewolf" width="450" height="300"  style = "width:49%; height:auto" >
                <cewolf:map tooltipgeneratorid="orderDeliveryXyData" />
            </cewolf:img>
            
            <div class="rightSideSmallTable">
				<display:table id="inventorySetupVO" name="inventoryTableStatsMap" class="altstripe smalltable">
					<display:column property="attributeName" title=""/>
					<display:column  title="Current Order % Distribution">
						<c:if test="${inventorySetupVO.currentHourPercentage.isDouble}">
	           		 		<c:out value = "${inventorySetupVO.currentHourPercentage.dvalue}%"/>
	           		 	</c:if>
	           		 	<c:if test="${!inventorySetupVO.currentHourPercentage.isDouble}">
	           		 		<c:out value = "${inventorySetupVO.currentHourPercentage.ivalue}%"/>
	           		 	</c:if>
	           		</display:column>
				   <display:column  title="Previous Hour Order % Distribution">
						<c:if test="${inventorySetupVO.previousHourPercentage.isDouble}">
	           		 		<c:out value = "${inventorySetupVO.previousHourPercentage.dvalue}%"/>
	           		 	</c:if>
	           		 	<c:if test="${!inventorySetupVO.previousHourPercentage.isDouble}">
	           		 		<c:out value = "${inventorySetupVO.previousHourPercentage.ivalue}%"/>
	           		 	</c:if>
	           		</display:column>
				</display:table>
			</div>
        </div>
    </div>
    <!-- 2nd row End-->
    <!-- 3rd row start -->
     <div>
        <!-- 3rd row left side start --> 
        <div class="leftSide" style="width:54%;">
          <cewolf:chart 
            id="JMSOrderFlowBarChart"
            type="verticalbar"
            xaxislabel="JMS Queue"
            yaxislabel="Queue Count"
            title="JMS Order Flow Queue"
            showlegend="false"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="jmsOrderFlowBarData"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="jmsOrderFlowChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="JMSOrderFlowBarChart" renderer="cewolf" width="450" height="300"  style = "width:49%; height:auto; margin-top:10px">
            <cewolf:map tooltipgeneratorid="jmsOrderFlowBarData" />
          </cewolf:img>
        
        <!-- 3rd row left small table --> 
        <div class="rightSideSmallTable">
          <display:table id="statsOrderVO" name="OrderStatsMap" class="altstripe smalltable" style="width:100%; height: auto; margin-top:10px">
            <display:column property="setupVO.queueShortName" title="Name"/>
            <display:column property="queueName" title="Queue Name"/>
            <display:column property="queueCount" title="Count"/>
            <display:column title="Status">
              <c:set var="imageName" value="images/green.jpg" />
              <c:if test="${statsOrderVO.queueCount > statsOrderVO.setupVO.warningThreshold}">
                <c:set var="imageName" value="images/yellow.jpg" />
              </c:if>
              <c:if test="${statsOrderVO.queueCount > statsOrderVO.setupVO.criticalThreshold}">
                <c:set var="imageName" value="images/red.jpg" />
              </c:if>
              <img src="${imageName}" alt=""  />
            </display:column>
            <display:column property="setupVO.warningThreshold" title="Warn"/>
            <display:column property="setupVO.criticalThreshold" title="Crit"/>
            <display:column property="oldestMessage" title="Oldest"/>
          </display:table>
         </div>
         <!-- 3rd row left small table end --> 
        </div>
        <!-- 3rd row left side End -->
    <div class="rightSide" style="width:42%; font-size:10px"> <!-- Whole page -->
    <c:set var="leftPaymentRangeText" value="${paymentDashboardForm.leftRangeStartDate} ${paymentDashboardForm.leftRangeStartTime} - ${paymentDashboardForm.leftRangeEndDate} ${paymentDashboardForm.leftRangeEndTime}" />
      <form name="paymentform" action="Dashboard.do?context=${dashboardForm.context}&securitytoken=${dashboardForm.securitytoken}" method="post" >
        <input type="hidden" name="securitytoken" value="${dashboardForm.securitytoken}" />
        <input type="hidden" name="context" value="${dashboardForm.context}" />

      <div class="triSection"> <!-- Top graph Row -->
        <div class="triSectionLeft">
          <cewolf:chart 
            id="LeftPaymentPieChart"
            type="pie"
            title="Payment"
            showlegend="true"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="leftPaymentDataset"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="leftPaymentPieChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="LeftPaymentPieChart" renderer="cewolf" width="250" height="250" style="width:130%; height:auto; margin-left:26px" >
            <cewolf:map tooltipgeneratorid="leftPaymentDataset" />
          </cewolf:img>
        </div>
         <!-- Selection Row -->
        <span style="float:right; text-align:center; margin-right:9%; font-size:10px" onclick="return toggleRangeSelect()">  <!-- left selection -->
          <b>Date Range<br> ${leftPaymentRangeText} </b>
        </span>
        <br>
        <div id="selectionDiv" class="displayNone">  <!-- Selection Panel -->
              <div class="triSectionLeft" id="leftSelection" style="width:200%; float:right; margin-right:-80%">
              <table style="float:right; margin-right:-18%">
                <tr>
                  <td colspan="3">
                    <input type="radio" name="leftRangeSelect" value="1" onclick="return closeLeftCustomRange()" ${paymentDashboardForm.leftSelectedLastHour}>Last Hour
                    <input type="radio" name="leftRangeSelect" value="2" onclick="return closeLeftCustomRange()" ${paymentDashboardForm.leftSelectedLastDay}>Today
                    <input type="radio" name="leftRangeSelect" value="3" onclick="return closeLeftCustomRange()" ${paymentDashboardForm.leftSelectedYesterday}>Yesterday
                    <input type="radio" name="leftRangeSelect" value="0" onclick="return openLeftCustomRange()" ${paymentDashboardForm.leftSelectedCustom}>Custom
                  </td>
                </tr>
                <tr id="selectionLeftRangeHeaderRow" class="displayNone">
                  <td>&nbsp;</td>
                  <td><b>Date (mm/dd/yyy)</b></td>
                  <td><b>&nbsp;&nbsp;Time</b></td>
                </tr>
                <tr id="selectionLeftRangeFromRow" class="displayNone">
                  <td>From:</td>
                  <td>
                    <input type="text" name="leftRangeStartDate" size="10" style="font-size:12px" value="${paymentDashboardForm.leftRangeStartDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeStartTime" size="4" style="font-size:12px" value="${paymentDashboardForm.leftRangeStartTime}"/>
                  </td>
                </tr>
                <tr id="selectionLeftRangeToRow" class="displayNone">
                  <td>To:&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td>
                    <input type="text" name="leftRangeEndDate" size="10" style="font-size:12px" value="${paymentDashboardForm.leftRangeEndDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeEndTime" size="4" style="font-size:12px" value="${paymentDashboardForm.leftRangeEndTime}"/>
                  </td>
                </tr>
              </table>
              </div>
          <span id="buttonSelection" style="float:right; margin-right:-80%"> <!-- Button Row -->
                <input type="submit" name="changeDates" style="font-size:12px" value="Change Dates" />
          </span>
      </div>
        
	      <div class="triSectionRight" style="float:right; margin-right:12%">
	        <display:table id="paymentCombinedStatsVO" name="combinedPaymentMap" class="altstripe smalltable">
	       	  <display:column title="Pay Type">
		           	<c:out value = "${paymentCombinedStatsVO.paymentType}"/>
	          </display:column>
	          <display:column title="Count">
		           	<c:out value = "${paymentCombinedStatsVO.leftPaymentStatsVO.count}"/>
	          </display:column>
	          <display:column  title="%">
					<c:if test="${paymentCombinedStatsVO.leftPaymentStatsVO.paymentTypePercentage.isDouble}">
	          		 	<c:out value = "${paymentCombinedStatsVO.leftPaymentStatsVO.paymentTypePercentage.dvalue}%"/>
	          		</c:if>
	          		<c:if test="${!paymentCombinedStatsVO.leftPaymentStatsVO.paymentTypePercentage.isDouble}">
	          		 	<c:out value = "${paymentCombinedStatsVO.leftPaymentStatsVO.paymentTypePercentage.ivalue}%"/>
	          		</c:if>
	          </display:column>
	          
	        </display:table>
	        
	      </div>
       </form>
      </div>
    </div>
    <!-- 3rd row End -->
    <!-- 4th row start-->
    <div>
        <div class="leftSideSmallGraph" style="width:49%;">
                <cewolf:chart 
					id="JMSOtherChart"
					type="verticalbar"
					xaxislabel="JMS Queue"
					yaxislabel="Queue Count"
					title="JMS Queue"
					showlegend="false"> 
					<cewolf:gradientpaint>
						<cewolf:point x="0" y="0" color="#ffe87c"/>
						<cewolf:point x="0" y="300" color="#faf8cc"/>
					</cewolf:gradientpaint>
					<cewolf:data>
						<cewolf:producer id="jmsOtherBarData"/>
					</cewolf:data>
					<cewolf:chartpostprocessor id="jmsOtherChartPostProcessor">
					</cewolf:chartpostprocessor>
				  </cewolf:chart>
				  <cewolf:img chartid="JMSOtherChart" renderer="cewolf"  width="450" height="300"  style = "width:49%; height:auto; margin-top:10px">
					<cewolf:map tooltipgeneratorid="jmsOtherBarData" />
				  </cewolf:img>
        		
        	<!-- 4th row left small table --> 
	    	<div class="rightSideSmallTable">
					<display:table id="statsOtherVO" name="OtherStatsMap" class="altstripe smalltable" style="width:100%; height:auto; margin-top:10px">
						<display:column property="setupVO.queueShortName" title="Name"/>
						<display:column property="queueName" title="Queue Name"/>
						<display:column property="queueCount" title="Count"/>
						<display:column title="Status">
						  <c:set var="imageName" value="images/green.jpg" />
						  <c:if test="${statsOtherVO.queueCount > statsOtherVO.setupVO.warningThreshold}">
							<c:set var="imageName" value="images/yellow.jpg" />
						  </c:if>
						  <c:if test="${statsOtherVO.queueCount > statsOtherVO.setupVO.criticalThreshold}">
							<c:set var="imageName" value="images/red.jpg" />
						  </c:if>
						  <img src="${imageName}" alt=""  />
						</display:column>
						<display:column property="setupVO.warningThreshold" title="Warn"/>
						<display:column property="setupVO.criticalThreshold" title="Crit"/>
						<display:column property="oldestMessage" title="Oldest"/>
					</display:table>
                </div>
	         <!-- 4th row left small table end --> 
        </div>
    </div>
    <!-- 4th row End-->
    <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
