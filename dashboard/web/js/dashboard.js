
function toggleRangeSelect()
{
    toggleDivDisplay("selectionDiv");
}
	
function toggleDivDisplay(divId)
{
    var selectionDiv = document.getElementById(divId);

    if (selectionDiv.style.display == "none")
    {
        selectionDiv.style.display = "block";    
    }
    else
    {
        selectionDiv.style.display = "none";    
    }
}


function openLeftCustomRange()
{
    modifyElementDisplay("selectionLeftRangeHeaderRow","block");
    modifyElementDisplay("selectionLeftRangeFromRow","block");
    modifyElementDisplay("selectionLeftRangeToRow", "block");
}

function closeLeftCustomRange()
{
    modifyElementDisplay("selectionLeftRangeHeaderRow","none");
    modifyElementDisplay("selectionLeftRangeFromRow","none");
    modifyElementDisplay("selectionLeftRangeToRow", "none");
}

function openRightCustomRange()
{
    modifyElementDisplay("selectionRightRangeHeaderRow","block");
    modifyElementDisplay("selectionRightRangeFromRow","block");
    modifyElementDisplay("selectionRightRangeToRow", "block");
}

function closeRightCustomRange()
{
    modifyElementDisplay("selectionRightRangeHeaderRow","none");
    modifyElementDisplay("selectionRightRangeFromRow","none");
    modifyElementDisplay("selectionRightRangeToRow", "none");
}

function modifyElementDisplay(elementName, displayValue)
{
    var selectionDiv = document.getElementById(elementName);
    selectionDiv.style.display = displayValue;    
}