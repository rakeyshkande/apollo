<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="refresh" content="60" >
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="Inventory" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <div>
        <form action="InventoryDashboard.do">
            <select name="numberProductsToShow" onchange="document.forms[0].submit();">
                <option value="10">Select Number of Products to Show</option>
                <option value="1">Show Top Product</option>
                <option value="3">Show Top Three Products</option>
                <option value="5">Show Top Five Products</option>
                <option value="10">Show Top Ten Products</option>
                <option value="20">Show Top Twenty Products</option>
            </select>
            <input type="hidden" name="securitytoken" value="${dashboardForm.securitytoken}"/>
            <input type="hidden" name="context" value="${dashboardForm.context}"/>
        </form>
    </div>
    <div>
        <div class="leftSide">
                <cewolf:chart 
                    id="FloristCumlativeXYChart"
                    type="timeseries"
                    yaxislabel="Order Count"
                    title="Florist Product Cumulative"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="floristCumulativeXYData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="inventoryChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="FloristCumlativeXYChart" renderer="cewolf" width="450" height="300" >
                    <cewolf:map tooltipgeneratorid="floristCumulativeXYData" />
                </cewolf:img>
        </div>
        <div class="rightSide">
                <cewolf:chart 
                    id="FloristRateXYChart"
                    type="timeseries"
                    yaxislabel="Order Count"
                    title="Florist Product Rate"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="floristRateXYData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="inventoryChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="FloristRateXYChart" renderer="cewolf" width="450" height="300" >
                    <cewolf:map tooltipgeneratorid="floristRateXYData" />
                </cewolf:img>
        </div>
    </div>
    <div>
        <div class="leftSide">
                <cewolf:chart 
                    id="VendorCumlativeXYChart"
                    type="timeseries"
                    yaxislabel="Order Count"
                    title="Vendor Product Cumulative"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="vendorCumulativeXYData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="inventoryChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="VendorCumlativeXYChart" renderer="cewolf" width="450" height="300" >
                    <cewolf:map tooltipgeneratorid="vendorCumulativeXYData" />
                </cewolf:img>
        </div>
        <div class="rightSide">
                <cewolf:chart 
                    id="VendorRateXYChart"
                    type="timeseries"
                    yaxislabel="Order Count"
                    title="Vendor Product Rate"> 
                    <cewolf:gradientpaint>
                        <cewolf:point x="0" y="0" color="#ffe87c"/>
                        <cewolf:point x="0" y="300" color="#faf8cc"/>
                    </cewolf:gradientpaint>
                    <cewolf:data>
                        <cewolf:producer id="vendorRateXYData"/>
                    </cewolf:data>
                    <cewolf:chartpostprocessor id="inventoryChartPostProcessor">
                    </cewolf:chartpostprocessor>
                </cewolf:chart>
                <cewolf:img chartid="VendorRateXYChart" renderer="cewolf" width="450" height="300" >
                    <cewolf:map tooltipgeneratorid="vendorRateXYData" />
                </cewolf:img>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
  </body>
</html>
