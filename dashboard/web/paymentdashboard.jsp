<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="cewolf" uri="WEB-INF/lib/cewolf-1.1-ulf.tld" %>

<html>
  <head>  
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
	<meta http-equiv="X-UA-Compatible" content="IE=11" />
    <script type="text/javascript" src="js/dashboard.js"></script>

    <script language="javascript">
        setTimeout("document.paymentform.submit()",60000);
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <div class="maindiv">
    <c:set var="pageName" value="Payment" />
    <%@ include file="header.jsp" %>
    <%@ include file="menu.jsp" %>

    <c:set var="leftPaymentRangeText" value="${dashboardForm.leftRangeStartDate} ${dashboardForm.leftRangeStartTime} - ${dashboardForm.leftRangeEndDate} ${dashboardForm.leftRangeEndTime}" />
    <c:set var="rightPaymentRangeText" value="${dashboardForm.rightRangeStartDate} ${dashboardForm.rightRangeStartTime} - ${dashboardForm.rightRangeEndDate} ${dashboardForm.rightRangeEndTime}" />

    <div> <!-- Whole page -->
      <form name="paymentform" action="PaymentDashboard.do" method="post" >
        <input type="hidden" name="securitytoken" value="${dashboardForm.securitytoken}" />
        <input type="hidden" name="context" value="${dashboardForm.context}" />
      <div class="triSection">  <!-- Selection Row -->
        <div class="triSectionLeft highlight" onclick="return toggleRangeSelect()">  <!-- left selection -->
          <center><h3>Date Range<br>${leftPaymentRangeText}</h3></center>
        </div>
        <div class="triSectionRight highlight" onclick="return toggleRangeSelect()">  <!-- right selection -->
          <center><h3>Date Range<BR>${rightPaymentRangeText}</h3></center>
        </div>
      </div>
      <div id="selectionDiv" class="displayNone">  <!-- Selection Panel -->
          <div class="triSection">  <!-- Selection Row -->
            <div class="triSectionLeft" id="leftSelection">  <!-- left selection -->
              <table>
                <tr>
                  <td colspan="3">
                    <input type="radio" name="leftRangeSelect" value="1" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedLastHour}>Last Hour</input>
                    <input type="radio" name="leftRangeSelect" value="2" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedLastDay}>Today</input>
                    <input type="radio" name="leftRangeSelect" value="3" onclick="return closeLeftCustomRange()" ${dashboardForm.leftSelectedYesterday}>Yesterday</input>
                    <input type="radio" name="leftRangeSelect" value="0" onclick="return openLeftCustomRange()" ${dashboardForm.leftSelectedCustom}>Custom</input>
                  </td>
                </tr>
                <tr id="selectionLeftRangeHeaderRow" class="displayNone">
                  <td>&nbsp;</td>
                  <td>Date (mm/dd/yyy)</td>
                  <td>Time</td>
                </tr>
                <tr id="selectionLeftRangeFromRow" class="displayNone">
                  <td>From:</td>
                  <td>
                    <input type="text" name="leftRangeStartDate" size="10" value="${dashboardForm.leftRangeStartDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeStartTime" size="4" value="${dashboardForm.leftRangeStartTime}"/>
                  </td>
                </tr>
                <tr id="selectionLeftRangeToRow" class="displayNone">
                  <td>To:</td>
                  <td>
                    <input type="text" name="leftRangeEndDate" size="10" value="${dashboardForm.leftRangeEndDate}"/>
                  </td>
                  <td>
                    <input type="text" name="leftRangeEndTime" size="4" value="${dashboardForm.leftRangeEndTime}"/>
                  </td>
                </tr>
              </table>
            </div>
            <div class="triSectionRight" id="rightSelection">  <!-- right selection -->
              <table>
                <tr>
                  <td colspan="3">
                    <input type="radio" name="rightRangeSelect" value="1" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedLastHour}>Last Hour</input>
                    <input type="radio" name="rightRangeSelect" value="2" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedLastDay}>Today</input>
                    <input type="radio" name="rightRangeSelect" value="3" onclick="return closeRightCustomRange()" ${dashboardForm.rightSelectedYesterday}>Yesterday</input>
                    <input type="radio" name="rightRangeSelect" value="0" onclick="return openRightCustomRange()" ${dashboardForm.rightSelectedCustom}>Custom</input>
                  </td>
                </tr>
                <tr id="selectionRightRangeHeaderRow" class="displayNone">
                  <td>&nbsp;</td>
                  <td>Date (mm/dd/yyy)</td>
                  <td>Time</td>
                </tr>
                <tr id="selectionRightRangeFromRow" class="displayNone">
                  <td>From:</td>
                  <td>
                    <input type="text" name="rightRangeStartDate" size="10" value="${dashboardForm.rightRangeStartDate}"/>
                  </td>
                  <td>
                    <input type="text" name="rightRangeStartTime" size="4" value="${dashboardForm.rightRangeStartTime}"/>
                  </td>
                </tr>
                <tr id="selectionRightRangeToRow" class="displayNone">
                  <td>To:</td>
                  <td>
                    <input type="text" name="rightRangeEndDate" size="10" value="${dashboardForm.rightRangeEndDate}"/>
                  </td>
                  <td>
                    <input type="text" name="rightRangeEndTime" size="4" value="${dashboardForm.rightRangeEndTime}"/>
                  </td>
                </tr>
              </table>
            </div>
            <div class="triSectionRight" ></div>
          </div>
          <div class="triSection">
            <div id="buttonSelection"> <!-- Button Row -->
                <input type="submit" name="changeDates" value="Change Dates" />
            </div>
            <div id="buttonSelection"> <!-- Button Row -->
                <input type="submit" name="changeDates" value="Change Dates" />
            </div>
            <div class="triSectionRight" ></div>
          </div>
        </form>
      </div>
      <div class="triSection"> <!-- Top graph Row -->
        <div class="triSectionLeft">
          <cewolf:chart 
            id="LeftPaymentPieChart"
            type="pie"
            title="Payment"
            showlegend="true"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="leftPaymentDataset"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="leftPaymentPieChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="LeftPaymentPieChart" renderer="cewolf" width="300" height="300" >
            <cewolf:map tooltipgeneratorid="leftPaymentDataset" />
          </cewolf:img>
        </div>
        <div class="triSectionMid">
          <cewolf:chart 
            id="RightPaymentPieChart"
            type="pie"
            title="Payment"
            showlegend="true"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="rightPaymentDataset"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="rightPaymentPieChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="RightPaymentPieChart" renderer="cewolf" width="300" height="300" >
            <cewolf:map tooltipgeneratorid="rightPaymentDataset" />
          </cewolf:img>
        </div>
        <div class="triSectionRight">
          <display:table id="paymentCombinedStatsVO" name="dashboardForm.combinedPaymentMap" class="altstripe smalltable">
            <display:caption>Payment Type Counts</display:caption>
            <display:column property="paymentType" title="Pay Type"/>
            <display:column property="leftPaymentStatsVO.count" title="Left"/>
            <display:column property="rightPaymentStatsVO.count" title="Right"/>
          </display:table>
        </div>
      </div>
      <div class="triSection">
        <div class="triSectionLeft">
          <cewolf:chart 
            id="LeftAuthorizationChart"
            type="stackedhorizontalbar"
            xaxislabel="Auth Type"
            yaxislabel="Authorization Counts"
            title="Authorization"
            showlegend="true"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="leftAuthorizationDataset"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="leftAuthorizationChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="LeftAuthorizationChart" renderer="cewolf" width="300" height="300" >
            <cewolf:map tooltipgeneratorid="leftAuthorizationDataset" />
          </cewolf:img>
        </div>
        <div class="triSectionMid">
          <cewolf:chart 
            id="RightAuthorizationChart"
            type="stackedhorizontalbar"
            xaxislabel="Auth Type"
            yaxislabel="Authorization Counts"
            title="Authorization"
            showlegend="true"> 
            <cewolf:gradientpaint>
                <cewolf:point x="0" y="0" color="#ffe87c"/>
                <cewolf:point x="0" y="300" color="#faf8cc"/>
            </cewolf:gradientpaint>
            <cewolf:data>
                <cewolf:producer id="rightAuthorizationDataset"/>
            </cewolf:data>
            <cewolf:chartpostprocessor id="rightAuthorizationChartPostProcessor">
            </cewolf:chartpostprocessor>
          </cewolf:chart>
          <cewolf:img chartid="RightAuthorizationChart" renderer="cewolf" width="300" height="300" >
            <cewolf:map tooltipgeneratorid="rightAuthorizationDataset" />
          </cewolf:img>
        </div>
        <div class="triSectionRight">
          <display:table id="authorizationCombinedStatsVO" name="dashboardForm.combinedAuthorizationMap" class="altstripe smalltable">
            <display:caption>Authorization Counts</display:caption>
            <display:column property="cardType" title="Card Type"/>
            <display:column title="Declines">${authorizationCombinedStatsVO.leftAuthorizationStatsVO.declines} / ${authorizationCombinedStatsVO.rightAuthorizationStatsVO.declines}</display:column>
            <display:column title="Approvals">${authorizationCombinedStatsVO.leftAuthorizationStatsVO.approvals} / ${authorizationCombinedStatsVO.rightAuthorizationStatsVO.approvals}</display:column>
            <display:column title="Total">${authorizationCombinedStatsVO.leftAuthorizationStatsVO.totalAuths} / ${authorizationCombinedStatsVO.rightAuthorizationStatsVO.totalAuths}</display:column>
            <display:column title="% Declines">${authorizationCombinedStatsVO.leftAuthorizationStatsVO.declinePercent} / ${authorizationCombinedStatsVO.rightAuthorizationStatsVO.declinePercent}</display:column>
          </display:table>
        </div>
      </div>      
    </div>
    <%@ include file="footer.jsp" %>
    </div>
  </body>
</html>
