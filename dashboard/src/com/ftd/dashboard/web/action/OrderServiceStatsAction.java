package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.web.form.OrderServiceStatsPostForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

/**
 * Action for receiving the post data from the order stats.
 */
public class OrderServiceStatsAction extends AbstractDashboardAction
{

    private static Logger logger = new Logger("com.ftd.dashboard.web.action.OrderServiceStatsAction");
  
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        Connection con = null;
        try
        {
            OrderServiceStatsPostForm authorizationStatsPostForm = (OrderServiceStatsPostForm)form;
            logger.debug("Start ");

            con = getNewConnection();

            DashboardBO dashboardBO = new DashboardBO();
            
            dashboardBO.saveOrderServiceStats(con, authorizationStatsPostForm);

            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("failure");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }


    }
}
