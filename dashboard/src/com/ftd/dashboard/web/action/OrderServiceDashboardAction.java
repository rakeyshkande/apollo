package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.web.form.OrderServiceDashboardForm;
import com.ftd.dashboard.web.form.PaymentDashboardForm;
import com.ftd.dashboard.web.util.AuthorizationZeroAxisChartPostProcessor;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.dashboard.web.util.PaymentPieChartPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

/**
 * Action for dashboard.
 */
public class OrderServiceDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.action.OrderServiceDashboardAction");

    /**
     * Display the Payment Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        Connection con = null;
        try
        {
            OrderServiceDashboardForm orderServiceDashboardForm = (OrderServiceDashboardForm)form;
            logger.debug("Start ");
    
            con = getNewConnection();
    
            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.calculateDates(orderServiceDashboardForm);
            dashboardBO.populateOrderServiceDetail(con, orderServiceDashboardForm);
    
            orderServiceDashboardForm.setLastUpdated(new Date());
    
            request.setAttribute("dashboardForm", orderServiceDashboardForm);
            request.setAttribute("leftOrderServiceDataset", orderServiceDashboardForm.getLeftOrderServiceDataset());
            request.setAttribute("rightOrderServiceDataset", orderServiceDashboardForm.getRightOrderServiceDataset());

            DefaultTimeSeriesChartPostProcessor leftOrderServiceChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
            DefaultTimeSeriesChartPostProcessor rightOrderServiceChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
    
            request.setAttribute("leftOrderServiceChartPostProcessor", leftOrderServiceChartPostProcessor);
            request.setAttribute("rightOrderServiceChartPostProcessor", rightOrderServiceChartPostProcessor);

            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
        
    }


}
