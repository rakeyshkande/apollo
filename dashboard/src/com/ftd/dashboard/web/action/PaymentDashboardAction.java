package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.bo.DashboardJMSBO;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.web.form.DashboardForm;
import com.ftd.dashboard.web.form.FloristDashboardForm;
import com.ftd.dashboard.web.form.JMSDashboardForm;
import com.ftd.dashboard.web.form.PaymentDashboardForm;
import com.ftd.dashboard.web.util.AuthorizationZeroAxisChartPostProcessor;
import com.ftd.dashboard.web.util.DefaultBarChartPostProcessor;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.dashboard.web.util.FloristChartPostProcessor;
import com.ftd.dashboard.web.util.JMSBarChartPostProcessor;
import com.ftd.dashboard.web.util.PaymentPieChartPostProcessor;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for dashboard.
 */
public class PaymentDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.action.PaymentDashboardAction");

    /**
     * Display the Payment Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        Connection con = null;
        try
        {
            PaymentDashboardForm paymentDashboardForm = (PaymentDashboardForm)form;
            logger.debug("Start ");
    
            con = getNewConnection();
    
            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.calculateDates(paymentDashboardForm);
            dashboardBO.populatePaymentDetail(con, paymentDashboardForm, 1);
    
            paymentDashboardForm.setLastUpdated(new Date());
    
            request.setAttribute("dashboardForm", paymentDashboardForm);
            request.setAttribute("leftAuthorizationDataset", paymentDashboardForm.getLeftAuthorizationDataset());
            request.setAttribute("rightAuthorizationDataset", paymentDashboardForm.getRightAuthorizationDataset());
            request.setAttribute("leftPaymentDataset", paymentDashboardForm.getLeftPaymentDataset());
            request.setAttribute("rightPaymentDataset", paymentDashboardForm.getRightPaymentDataset());
    
            // Setup the chart post processors
            PaymentPieChartPostProcessor leftPaymentPieChartPostProcessor = new PaymentPieChartPostProcessor();
            PaymentPieChartPostProcessor rightPaymentPieChartPostProcessor = new PaymentPieChartPostProcessor();
    
            request.setAttribute("leftPaymentPieChartPostProcessor", leftPaymentPieChartPostProcessor);
            request.setAttribute("rightPaymentPieChartPostProcessor", rightPaymentPieChartPostProcessor);
    
            AuthorizationZeroAxisChartPostProcessor leftAuthChartPostProcessor = new AuthorizationZeroAxisChartPostProcessor();
            AuthorizationZeroAxisChartPostProcessor rightAuthChartPostProcessor = new AuthorizationZeroAxisChartPostProcessor();
    
            request.setAttribute("leftAuthorizationChartPostProcessor", leftAuthChartPostProcessor);
            request.setAttribute("rightAuthorizationChartPostProcessor", rightAuthChartPostProcessor);

            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
        
    }


}
