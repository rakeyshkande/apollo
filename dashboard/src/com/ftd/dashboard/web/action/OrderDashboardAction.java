package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.web.form.OrderDashboardForm;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.dashboard.web.util.OrderProjectionDialChartPostProcessor;
import com.ftd.dashboard.web.util.OrderProjectionThermometerChartPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for dashboard.
 */
public class OrderDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = 
        new Logger("com.ftd.dashboard.web.action.QueueDashboardAction");

    /**
     * Display the Queue Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException, 
                                                                      Exception
    {
        OrderDashboardForm dashboardForm = (OrderDashboardForm)form;
        logger.debug("Start ");

        Connection con = null;
        try
        {
            con = getNewConnection();

            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.populateOrderDetail(con, dashboardForm);
    
            dashboardForm.setLastUpdated(new Date());
    
            request.setAttribute("dashboardForm", dashboardForm);
            request.setAttribute("orderOriginXyData", dashboardForm.getOrderOriginXyData());
            request.setAttribute("orderProjectionMeterData", dashboardForm.getOrderProjectionMeterData());
            request.setAttribute("orderProjectionDialData", dashboardForm.getOrderProjectionMeterData());
            request.setAttribute("orderTableStatsMap", dashboardForm.getSetupVO());
            
            DefaultTimeSeriesChartPostProcessor orderOriginChartPostProcessor = 
                new DefaultTimeSeriesChartPostProcessor();
            request.setAttribute("orderOriginChartPostProcessor", orderOriginChartPostProcessor);
    
            OrderProjectionDialChartPostProcessor orderProjectionDialChartPostProcessor = 
                new OrderProjectionDialChartPostProcessor();
            request.setAttribute("orderProjectionDialChartPostProcessor", orderProjectionDialChartPostProcessor);
            
            OrderProjectionThermometerChartPostProcessor orderProjectionThermometerChartPostProcessor = 
                new OrderProjectionThermometerChartPostProcessor();
            request.setAttribute("orderProjectionThermometerChartPostProcessor", orderProjectionThermometerChartPostProcessor);
    
            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
        
    }


}
