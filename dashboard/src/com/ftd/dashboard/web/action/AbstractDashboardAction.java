package com.ftd.dashboard.web.action;

import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import org.apache.struts.action.Action;

public class AbstractDashboardAction extends Action
{
    private static Logger logger  = new Logger(AbstractDashboardAction.class.getName());
    
    public AbstractDashboardAction()
    {
    }

    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection      
    
        String datasource = ConfigurationUtil.getInstance().getProperty(DashboardConstants.PROPERTY_FILE, DashboardConstants.DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }

    /**
     * Closes the connection quietly
     * @param con
     */
    protected void closeConnection(Connection con)
    {
        try
        {
            if(con != null)
            {
                con.close();
            }
        } catch (Throwable t)
        {
            // NOP
            logger.warn("Error closing a connection" + t.getMessage(), t);
        }
        
    }
}
