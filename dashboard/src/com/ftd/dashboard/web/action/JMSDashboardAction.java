package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.bo.DashboardJMSBO;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.web.form.DashboardForm;
import com.ftd.dashboard.web.form.FloristDashboardForm;
import com.ftd.dashboard.web.form.JMSDashboardForm;
import com.ftd.dashboard.web.util.DefaultBarChartPostProcessor;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.dashboard.web.util.FloristChartPostProcessor;
import com.ftd.dashboard.web.util.JMSBarChartPostProcessor;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for dashboard.
 */
public class JMSDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.action.JSMDashboardAction");

    /**
     * Display the Queue Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        JMSDashboardForm jmsDashboardForm = (JMSDashboardForm)form;
        logger.debug("Start ");

        Connection con = null;
        try
        {
            con = getNewConnection();
            

            DashboardJMSBO dashboardJMSBO = new DashboardJMSBO();
            dashboardJMSBO.populateJMSDetail(con, jmsDashboardForm);
    
            jmsDashboardForm.setLastUpdated(new Date());
    
            request.setAttribute("dashboardForm", jmsDashboardForm);
            request.setAttribute("jmsOrderFlowBarData", jmsDashboardForm.getJmsOrderFlowBarData());
            request.setAttribute("jmsOtherBarData", jmsDashboardForm.getJmsOtherBarData());
    
            JMSBarChartPostProcessor jmsOrderFlowChartPostProcessor = new JMSBarChartPostProcessor();
            JMSBarChartPostProcessor jmsOtherChartPostProcessor = new JMSBarChartPostProcessor();
            jmsOrderFlowChartPostProcessor.setThresholdMap(jmsDashboardForm.getOrderThresholdMap());
            jmsOtherChartPostProcessor.setThresholdMap(jmsDashboardForm.getOtherThresholdMap());
               
            request.setAttribute("jmsOrderFlowChartPostProcessor", jmsOrderFlowChartPostProcessor);
            request.setAttribute("jmsOtherChartPostProcessor", jmsOtherChartPostProcessor);
    
            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
        
    }


}
