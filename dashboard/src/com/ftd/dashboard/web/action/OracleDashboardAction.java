package com.ftd.dashboard.web.action;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.dashboard.bo.DashboardOracleBO;
import com.ftd.dashboard.web.form.OracleDashboardForm;
import com.ftd.dashboard.web.util.OracleInstanceBarChartPostProcessor;
import com.ftd.dashboard.web.util.OracleMeterChartPostProcessor;
import com.ftd.dashboard.web.util.OraclePieChartPostProcessor;
import com.ftd.dashboard.web.util.OracleThermometerPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Action for dashboard.
 */
public class OracleDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.action.OracleDashboardAction");

    /**
     * Display the Payment Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        Connection con = null;
        try
        {
            OracleDashboardForm oracleDashboardForm = (OracleDashboardForm)form;
            logger.debug("Start ");
    
            con = getNewConnection();
    
            DashboardOracleBO dashboardBO = new DashboardOracleBO();
    
            oracleDashboardForm.setLastUpdated(new Date());
            dashboardBO.populateOracleDetail(con, oracleDashboardForm);
    
            request.setAttribute("dashboardForm", oracleDashboardForm);
            
            // Data sets
            request.setAttribute("oracleLocksData", oracleDashboardForm.getOracleLocksDataset());
            request.setAttribute("oracleLocksAgedData", oracleDashboardForm.getOracleLocksAgedDataset());
            request.setAttribute("oracleDataDiskgroupData", oracleDashboardForm.getOracleDataDiskgroupDataset());
            request.setAttribute("oracleFlashDiskgroupData", oracleDashboardForm.getOracleFlashDiskgroupDataset());
            request.setAttribute("oracleTablespaceFreeData", oracleDashboardForm.getOracleTablespaceFreeDataset());
            
            request.setAttribute("oracleInstanceData", oracleDashboardForm.getOracleInstanceDataset());
            
            // List of instances
            request.setAttribute("instances", oracleDashboardForm.getInstances());
            
    
            OracleInstanceBarChartPostProcessor oracleInstanceBarChartPostProcessor = dashboardBO.createOracleBarChartPostProcessor();
            request.setAttribute("oracleInstanceBarChartPostProcessor", oracleInstanceBarChartPostProcessor);
            
            OracleThermometerPostProcessor oracleThermometerPostProcessor = dashboardBO.createOracleThermometerPostProcessor();
            request.setAttribute("oracleThermometerPostProcessor", oracleThermometerPostProcessor);
            
            OraclePieChartPostProcessor oraclePieChartPostProcessor = new OraclePieChartPostProcessor();
            request.setAttribute("oraclePieChartPostProcessor", oraclePieChartPostProcessor);
            
            OracleMeterChartPostProcessor oracleMeterChartPostProcessor = dashboardBO.createOracleMeterPostProcessor();
            request.setAttribute("oracleMeterChartPostProcessor", oracleMeterChartPostProcessor);
 
            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
        
    }


}
