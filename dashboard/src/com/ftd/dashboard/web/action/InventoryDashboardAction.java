package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.web.form.InventoryDashboardForm;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for dashboard.
 */
public class InventoryDashboardAction extends AbstractDashboardAction
{
    private static Logger logger = 
        new Logger("com.ftd.dashboard.web.action.InventoryDashboardAction");

    /**
     * Display the Queue Dashboard detail page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException, 
                                                                      Exception
    {
        Connection con = null;
        try
        {
            InventoryDashboardForm inventoryDashboardForm = (InventoryDashboardForm)form;
            logger.debug("Start ");
    
            con = getNewConnection();
    
            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.populateInventoryDetail(con, inventoryDashboardForm);
    
            inventoryDashboardForm.setLastUpdated(new Date());
    
            request.setAttribute("dashboardForm", inventoryDashboardForm);
            request.setAttribute("floristCumulativeXYData", inventoryDashboardForm.getFloristCumulativeXYData());
            request.setAttribute("vendorCumulativeXYData", inventoryDashboardForm.getVendorCumulativeXYData());
            request.setAttribute("floristRateXYData", inventoryDashboardForm.getFloristRateXYData());
            request.setAttribute("vendorRateXYData", inventoryDashboardForm.getVendorRateXYData());
    
            DefaultTimeSeriesChartPostProcessor inventoryChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
    
            request.setAttribute("inventoryChartPostProcessor", inventoryChartPostProcessor);
    
            return mapping.findForward("success");
        }
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } 
        finally
        {
            //close the connection
            try
            {
                con.close();
            } catch (Exception e)
            {
                logger.error("Exception processing",e);
            }
        }
    }


}
