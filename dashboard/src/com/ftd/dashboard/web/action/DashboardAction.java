package com.ftd.dashboard.web.action;

import com.ftd.dashboard.bo.DashboardBO;
import com.ftd.dashboard.web.form.DashboardForm;
import com.ftd.dashboard.web.form.PaymentDashboardForm;
import com.ftd.dashboard.web.util.DefaultTimeSeriesChartPostProcessor;
import com.ftd.dashboard.web.util.FloristChartPostProcessor;
import com.ftd.dashboard.web.util.PaymentPieChartPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.dashboard.bo.DashboardJMSBO;
import com.ftd.dashboard.web.form.JMSDashboardForm;
import com.ftd.dashboard.web.util.JMSBarChartPostProcessor;
/**
 * Action for dashboard.
 */
public class DashboardAction extends AbstractDashboardAction
{
    private static Logger logger  = new Logger("com.ftd.dashboard.web.action.DashboardAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
    {
        DashboardForm dashboardForm = (DashboardForm) form;
        logger.debug("Start " + dashboardForm);
        
        Connection con = null;
        
        try
        {
            con = getNewConnection();
            
            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.populateSummaryData(con,dashboardForm);
            
            dashboardForm.setLastUpdated(new Date());
            
            request.setAttribute("dashboardForm",dashboardForm);
            request.setAttribute("orderDeliveryXyData",dashboardForm.getOrderDeliveryXyData());
            request.setAttribute("floristXyData",dashboardForm.getFloristXyData());
            request.setAttribute("orderOriginXyData",dashboardForm.getOrderOriginXyData());
            request.setAttribute("queueXyData",dashboardForm.getQueueXyData());
            request.setAttribute("floristTableStatsMap",dashboardForm.getFloristDashboardForm().getSetupVO() );
            request.setAttribute("orderTableStatsMap",dashboardForm.getOrderDashboardForm().getSetupVO() );
            request.setAttribute("queueTableStatsMap",dashboardForm.getQueueDashboardForm().getSetupVO() );
            request.setAttribute("inventoryTableStatsMap",dashboardForm.getInventoryDashboardForm().getSetupVO() );
            
            FloristChartPostProcessor floristChartPostProcessor = new FloristChartPostProcessor();
            DefaultTimeSeriesChartPostProcessor orderOriginChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
            DefaultTimeSeriesChartPostProcessor queueChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
            DefaultTimeSeriesChartPostProcessor orderDeliveryChartPostProcessor = new DefaultTimeSeriesChartPostProcessor();
            
            request.setAttribute("floristChartPostProcessor", floristChartPostProcessor);
            request.setAttribute("orderOriginChartPostProcessor", orderOriginChartPostProcessor);
            request.setAttribute("queueChartPostProcessor", queueChartPostProcessor);
            request.setAttribute("orderDeliveryChartPostProcessor", orderDeliveryChartPostProcessor);
            
            /* JMS Details*/
            DashboardJMSBO dashboardJMSBO = new DashboardJMSBO();
            JMSDashboardForm jmsDashboardForm =dashboardForm.getJmsDashboardForm();
            dashboardJMSBO.populateJMSDetail(con, dashboardForm.getJmsDashboardForm());
            request.setAttribute("jmsOrderFlowBarData", jmsDashboardForm.getJmsOrderFlowBarData());
            request.setAttribute("jmsOtherBarData", jmsDashboardForm.getJmsOtherBarData());
    
            JMSBarChartPostProcessor jmsOrderFlowChartPostProcessor = new JMSBarChartPostProcessor();
            JMSBarChartPostProcessor jmsOtherChartPostProcessor = new JMSBarChartPostProcessor();
            jmsOrderFlowChartPostProcessor.setThresholdMap(jmsDashboardForm.getOrderThresholdMap());
            jmsOtherChartPostProcessor.setThresholdMap(jmsDashboardForm.getOtherThresholdMap());
               
            request.setAttribute("jmsOrderFlowChartPostProcessor", jmsOrderFlowChartPostProcessor);
            request.setAttribute("jmsOtherChartPostProcessor", jmsOtherChartPostProcessor);
            
            request.setAttribute("OrderStatsMap",dashboardForm.getJmsDashboardForm().getOrderStatsMap() );
            request.setAttribute("OtherStatsMap",dashboardForm.getJmsDashboardForm().getOtherStatsMap() );
            
            /* Payment Details */

            request.setAttribute("paymentDashboardForm", dashboardForm.getPaymentDashboardForm());
            request.setAttribute("leftPaymentDataset", dashboardForm.getPaymentDashboardForm().getLeftPaymentDataset());            
            request.setAttribute("combinedPaymentMap", dashboardForm.getPaymentDashboardForm().getCombinedPaymentMap());

            // Setup the chart post processors
            PaymentPieChartPostProcessor leftPaymentPieChartPostProcessor = new PaymentPieChartPostProcessor();
            request.setAttribute("leftPaymentPieChartPostProcessor", leftPaymentPieChartPostProcessor);
            
            return mapping.findForward("success");
        } 
        catch (Exception e)
        {
            logger.error("Exception", e);
            return mapping.findForward("error");
        } finally
        {
            closeConnection(con);
        }
    }
       
    

}
