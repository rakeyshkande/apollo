package com.ftd.dashboard.web.util;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.MeterPlot;
import org.jfree.chart.plot.ThermometerPlot;

import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.ThresholdVO;
import com.ftd.osp.utilities.plugins.Logger;


public class OracleThermometerPostProcessor extends DefaultMeterChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.OracleThermometerPostProcessor");
    
    private Map<String, ThresholdVO> thresholdMap = new HashMap<String, ThresholdVO>();
    
    public void processChart(Object chartObject, Map paramMap)
    {
        JFreeChart chart = (JFreeChart)chartObject;
        ThermometerPlot plot = (ThermometerPlot) chart.getPlot();

        ChartTheme darkTheme = ChartThemeFactory.getDarkTheme();
        darkTheme.apply(chart);
        
        // Make it look like a cylinder, not a thermometer
        plot.setBulbRadius(0);
        plot.setUnits(ThermometerPlot.UNITS_NONE);
        plot.setGap(0);
        plot.setValuePaint(Color.BLACK);
        
        addThresholdColors(plot,paramMap);
    }
    
    
    public void setThresholdMap(Map<String, ThresholdVO> thresholdMap)
    {
        this.thresholdMap = thresholdMap;
    }
    
    private void addThresholdColors(ThermometerPlot plot, Map paramMap)
    {
        // Look for the threshold parameter
        ThresholdVO thresholdVO = null;
        String thresholdName = (String)paramMap.get(DashboardConstants.THRESHOLD);
        if (thresholdName != null)
        {
            thresholdVO = thresholdMap.get(thresholdName);
        }
        if (thresholdVO == null)
        {
            plot.setSubrange(ThermometerPlot.NORMAL, 0, 150);
            plot.setSubrange(ThermometerPlot.WARNING, 151, 300);
            plot.setSubrange(ThermometerPlot.CRITICAL, 300, 500);
            plot.setUpperBound(500);
        }
        else
        {
            plot.setSubrange(ThermometerPlot.NORMAL, 0, thresholdVO.getWarningThreshold());
            plot.setSubrange(ThermometerPlot.WARNING, thresholdVO.getWarningThreshold(), thresholdVO.getCriticalThreshold());
            plot.setSubrange(ThermometerPlot.CRITICAL, thresholdVO.getCriticalThreshold(), thresholdVO.getMaxDisplayValue());
            plot.setUpperBound(thresholdVO.getMaxDisplayValue());
        }
        
    }

}
