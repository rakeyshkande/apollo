package com.ftd.dashboard.web.util;

import com.ftd.osp.utilities.plugins.Logger;

import java.text.AttributedString;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.PieDataset;

public class PaymentPieChartPostProcessor extends DefaultPieChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.PaymentPieChartPostProcessor");


    public PaymentPieChartPostProcessor()
    {
        
    }
    
    public void processChart(Object chartObject, Map map)
    {
        logger.debug("processChart START");

        JFreeChart chart = (JFreeChart)chartObject;
        PiePlot piePlot = (PiePlot) chart.getPlot();
        
        StandardPieSectionLabelGenerator labelGenerator;
        labelGenerator = new StandardPieSectionLabelGenerator("{0} {1} {2}");
        
        piePlot.setLabelGenerator(labelGenerator);
        
        /**
        piePlot.setExplodePercent("MS",0.30);
        piePlot.setExplodePercent("BM",0.30);
        piePlot.setExplodePercent("DC",0.30);
        piePlot.setExplodePercent("IN",0.30);
        piePlot.setExplodePercent("UA",0.30);
        piePlot.setExplodePercent("DI",0.30);
        piePlot.setExplodePercent("GC",0.30);
        **/
    }
    
    
}
