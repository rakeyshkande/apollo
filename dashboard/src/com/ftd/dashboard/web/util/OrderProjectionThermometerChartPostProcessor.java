package com.ftd.dashboard.web.util;

import de.laures.cewolf.ChartPostProcessor;
import de.laures.cewolf.cpp.ThermometerEnhancer;

import java.io.Serializable;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.ThermometerPlot;

public class OrderProjectionThermometerChartPostProcessor implements ChartPostProcessor, Serializable
{
    public OrderProjectionThermometerChartPostProcessor()
    {
    }

    public void processChart (Object chart, Map params)
    {
        Plot plot = ((JFreeChart) chart).getPlot();
        if (plot instanceof ThermometerPlot) 
        {
            ThermometerPlot tplot = (ThermometerPlot) plot;
            tplot.setUnits(ThermometerPlot.UNITS_NONE);
            tplot.setUseSubrangePaint(false);
            tplot.setRange(0.0,100.0);

            tplot.setSubrange(ThermometerPlot.NORMAL,0.0,0.0);
            tplot.setSubrange(ThermometerPlot.WARNING,0.0,0.0);
            tplot.setSubrange(ThermometerPlot.CRITICAL,0.0,100.0);
        }

    }

}
