package com.ftd.dashboard.web.util;

import java.util.Map;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;

import com.ftd.osp.utilities.plugins.Logger;

public class OracleInstanceBarChartPostProcessor extends ThresholdBarChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.OracleInstanceBarChartPostProcessor");
    

    public void processChart(Object chartObject, Map map)
    {
        // Do the normal threshold chart processing
        super.processChart(chartObject, map);
        
        JFreeChart chart = (JFreeChart)chartObject;
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        
        // Handle the black background of these charts
        ChartTheme darkTheme = ChartThemeFactory.getDarkTheme();
        darkTheme.apply(chart);
        
    }
    
    
    
    
}
