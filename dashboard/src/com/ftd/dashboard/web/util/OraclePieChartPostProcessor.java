package com.ftd.dashboard.web.util;

import com.ftd.osp.utilities.plugins.Logger;

import java.text.AttributedString;

import java.util.Map;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.PieDataset;

public class OraclePieChartPostProcessor extends DefaultPieChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.OraclePieChartPostProcessor");


    public OraclePieChartPostProcessor()
    {
        
    }
    
    public void processChart(Object chartObject, Map map)
    {
        logger.debug("processChart START");

        JFreeChart chart = (JFreeChart)chartObject;
        PiePlot piePlot = (PiePlot) chart.getPlot();
        
        ChartTheme darkTheme = ChartThemeFactory.getDarkTheme();
        darkTheme.apply(chart);
        
        StandardPieSectionLabelGenerator labelGenerator;
        labelGenerator = new StandardPieSectionLabelGenerator("{0} {1} {2}");
        
        piePlot.setLabelGenerator(labelGenerator);
        
    }
    
    
}
