package com.ftd.dashboard.web.util;

import de.laures.cewolf.ChartPostProcessor;

import java.awt.Color;

import java.awt.Font;

import java.io.Serializable;

import java.text.NumberFormat;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.MeterInterval;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.dial.ArcDialFrame;
import org.jfree.chart.plot.dial.DialCap;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.DialValueIndicator;
import org.jfree.chart.plot.dial.StandardDialFrame;
import org.jfree.chart.plot.dial.StandardDialRange;
import org.jfree.chart.plot.dial.StandardDialScale;

public class OrderProjectionDialChartPostProcessor implements ChartPostProcessor, Serializable
{
    private final static double MIN_RANGE = 70.0;
    private final static double YELLOW_RED_VALUE = 90.0;
    private final static double GREEN_YELLOW_VALUE = 100.0;
    private final static double MAX_RANGE = 130.0;
    
    public OrderProjectionDialChartPostProcessor()
    {
    }
    
    

    public void processChart(Object chart, Map map)
    {
        Plot plot = ((JFreeChart) chart).getPlot();

        if (plot instanceof DialPlot)
        {
            DialPlot dialPlot = (DialPlot)plot;
            
            StandardDialFrame frame = new StandardDialFrame();
            dialPlot.setDialFrame(frame);;

            StandardDialScale scale = (StandardDialScale) dialPlot.getScale(0);
            scale.setLowerBound(MIN_RANGE);
            scale.setUpperBound(MAX_RANGE);
            scale.setMajorTickIncrement(10.0);
            scale.setMinorTickCount(1);
            scale.setTickLabelFormatter(NumberFormat.getIntegerInstance());

            
            StandardDialRange greenDial = new StandardDialRange(GREEN_YELLOW_VALUE,MAX_RANGE,Color.GREEN);
            dialPlot.addLayer(greenDial);
            StandardDialRange yellowDial = new StandardDialRange(YELLOW_RED_VALUE,GREEN_YELLOW_VALUE,Color.YELLOW);
            dialPlot.addLayer(yellowDial);
            StandardDialRange redDial = new StandardDialRange(MIN_RANGE,YELLOW_RED_VALUE,Color.RED);
            dialPlot.addLayer(redDial);

            DialValueIndicator dvi = new DialValueIndicator(0);
            dvi.setFont(new Font("Dialog", 0, 10));
            dvi.setOutlinePaint(Color.darkGray);
            dvi.setRadius(0.59999999999999998D);
            dvi.setAngle(-90.0);
            
            dialPlot.addLayer(dvi);

            DialPointer needle = new DialPointer.Pointer();
            dialPlot.addPointer(needle);
                
            DialCap cap = new DialCap();
            cap.setRadius(0.1);
            dialPlot.setCap(cap);                
        }
    }
}
