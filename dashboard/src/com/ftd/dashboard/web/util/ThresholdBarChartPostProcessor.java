package com.ftd.dashboard.web.util;

import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.ThresholdVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.awt.Color;
import java.awt.Paint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;


public class ThresholdBarChartPostProcessor extends DefaultBarChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.ThresholdBarChartPostProcessor");
    private Map<String, ThresholdVO> thresholdMap = new HashMap<String, ThresholdVO>();

    public ThresholdBarChartPostProcessor()
    {
    }

    public void processChart(Object chartObject, Map map)
    {
        JFreeChart chart = (JFreeChart)chartObject;
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        CategoryDataset categoryDataset = categoryPlot.getDataset();

        // Check for a threshold value in the map
        String thresholdName = (String)map.get(DashboardConstants.THRESHOLD);
        addThresholdColors(categoryPlot, categoryDataset, thresholdName);
        addIntegerTickMarks(categoryPlot);
        setChartMax(categoryPlot, thresholdName);
        
    }
    
    protected void setChartMax(CategoryPlot categoryPlot, String thresholdName)
    {
        CategoryDataset categoryDataset = categoryPlot.getDataset();
        int thresholdMax = 1;
        List categories = categoryPlot.getCategories();
        for (int i=0; i < categories.size();i++)
        {
            Object category = categories.get(i);
            int value = categoryDataset.getValue(0,i).intValue();
            ThresholdVO vo = thresholdMap.get(category);
            if (thresholdName != null && thresholdName.length() > 0)
            {
                vo = thresholdMap.get(thresholdName);
            }
            else
            {
                vo = thresholdMap.get(category);
            }
            if (vo != null)
            {
                if (vo.getMaxDisplayValue() > thresholdMax)
                {
                    thresholdMax = vo.getMaxDisplayValue();
                }
            }
            if (value > thresholdMax)
            {
                thresholdMax = value;
            }
        }
        ValueAxis valueAxis = categoryPlot.getRangeAxis();
        valueAxis.setUpperBound(thresholdMax);
    }
    
    protected void addThresholdColors(CategoryPlot categoryPlot, CategoryDataset categoryDataset, String thresholdName)
    {
        // Green by default.
        // Yellow if over the warning threshold
        // Red if over the critical threshold
        List<Paint> colors = new ArrayList<Paint>();
        List categories = categoryPlot.getCategories();
        for (int i=0; i < categories.size();i++)
        {
            Object category = categories.get(i);
            
            int value = categoryDataset.getValue(0,i).intValue();
            ThresholdVO vo = thresholdMap.get(category);
            if (thresholdName != null && thresholdName.length() > 0)
            {
                vo = thresholdMap.get(thresholdName);
            }
            else
            {
                vo = thresholdMap.get(category);
            }
            logger.debug("category/value/vo = " + category + " " + value + " " + vo);
            
            if (vo == null)
            {
                // aaaahhh, not supposed to happen
                colors.add(Color.BLUE);
            }
            else
            {
                if (value > vo.getCriticalThreshold())
                {
                    colors.add(Color.RED);
                }
                else if (value > vo.getWarningThreshold())
                {
                    colors.add(Color.YELLOW);
                }
                else
                {
                    colors.add(Color.GREEN);
                }
            }
        }
        
        final CustomRenderer customRenderer = new CustomRenderer();
        customRenderer.setColorList(colors);
        categoryPlot.setRenderer(customRenderer);

    }
    
    protected void addIntegerTickMarks(CategoryPlot categoryPlot)
    {
        // change the auto tick unit selection to integer units only...
        NumberAxis rangeAxis = (NumberAxis)categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    }


    public void setThresholdMap(Map<String, ThresholdVO> thresholdMap)
    {
        this.thresholdMap = thresholdMap;
    }


    /**
     * A custom renderer that returns a different color for each item in a single series.
     */
    class CustomRenderer extends BarRenderer 
    {

        /** The colors. */
        List<Paint> colorList;

        /**
         * Creates a new renderer.
         *
         */
        public CustomRenderer() 
        {
            super();
        }

        /**
         * Returns the paint for an item.  Overrides the default behaviour inherited from
         * AbstractSeriesRenderer.
         *
         * @param row  the series.
         * @param column  the category.
         *
         * @return The item color.
         */
        public Paint getItemPaint(final int row, final int column) 
        {
            Paint color = colorList.get(column);
            return color;
        }
        
        public void setColorList(List<Paint> colors)
        {
            this.colorList = colors;
        }
    }

}
