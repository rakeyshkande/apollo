package com.ftd.dashboard.web.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.DialShape;
import org.jfree.chart.plot.MeterInterval;
import org.jfree.chart.plot.MeterPlot;
import org.jfree.data.Range;
import org.jfree.data.general.ValueDataset;

import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.ThresholdVO;
import com.ftd.osp.utilities.plugins.Logger;

public class OracleMeterChartPostProcessor extends DefaultMeterChartPostProcessor
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.OracleMeterChartPostProcessor");
    
    private Map<String,ThresholdVO> thresholdMap;

    public OracleMeterChartPostProcessor()
    {
        super();
        thresholdMap = new HashMap<String,ThresholdVO>();
    }
    
    public void processChart(Object chartObject, Map paramMap)
    {
        try
        {
            logger.debug("processChart START");

            JFreeChart chart = (JFreeChart)chartObject;
            MeterPlot meterPlot = (MeterPlot) chart.getPlot();

            ChartTheme darkTheme = ChartThemeFactory.getDarkTheme();
            darkTheme.apply(chart);

            meterPlot.setUnits("");

            ValueDataset valueDataset = meterPlot.getDataset();
            int plotValue = valueDataset.getValue().intValue();

            addThresholdColors(meterPlot,paramMap,plotValue);    
            
            // Something is turning legends on, so turn it back off
            chart.removeLegend();
            
            // Make the label of the value more readable
            meterPlot.setValuePaint(Color.WHITE);
            Font valueFont = meterPlot.getValueFont();
            valueFont = valueFont.deriveFont(Font.BOLD);
            meterPlot.setValueFont(valueFont);
            
            meterPlot.setDialShape(DialShape.CHORD);
            meterPlot.setMeterAngle(240);
            
            logger.debug("done");
        }
        catch (Throwable t)
        {
            logger.error("Ahhh",t);
        }
        
    }

    public void setThresholdMap(Map<String, ThresholdVO> thresholdMap)
    {
        this.thresholdMap = thresholdMap;
    }
    
    private void addThresholdColors(MeterPlot plot, Map paramMap, int plotValue)
    {
        // Look for the threshold parameter
        ThresholdVO thresholdVO = null;
        String thresholdName = (String)paramMap.get(DashboardConstants.THRESHOLD);
        if (thresholdName != null)
        {
            thresholdVO = thresholdMap.get(thresholdName);
        }
        if (thresholdVO != null)
        {
            Stroke stroke = new BasicStroke(6.0f);
            
            // Add coloring for the current value versus the range
            int currentThreshold = DashboardConstants.NORMAL_THRESHOLD;
            
            String thresholdType = (String)paramMap.get(DashboardConstants.THRESHOLD_TYPE);
            Range normalRange = null;
            Range warningRange = null;
            Range criticalRange = null;
            if (thresholdType != null && thresholdType.equals(DashboardConstants.THRESHOLD_TYPE_REVERSE)) 
            {
                criticalRange = new Range(0,thresholdVO.getCriticalThreshold());
                warningRange = new Range(thresholdVO.getCriticalThreshold(),thresholdVO.getWarningThreshold());
                normalRange = new Range(thresholdVO.getWarningThreshold(),thresholdVO.getMaxDisplayValue());
                
                if (plotValue < thresholdVO.getWarningThreshold())
                {
                    if (plotValue < thresholdVO.getCriticalThreshold())
                    {
                        currentThreshold = DashboardConstants.CRITICAL_THRESHOLD;
                    }
                    else
                    {
                        currentThreshold = DashboardConstants.WARNING_THRESHOLD;
                    }
                }
            }
            else
            {
                normalRange = new Range(0,thresholdVO.getWarningThreshold());
                warningRange = new Range(thresholdVO.getWarningThreshold(),thresholdVO.getCriticalThreshold());
                criticalRange = new Range(thresholdVO.getCriticalThreshold(),thresholdVO.getMaxDisplayValue());
                
                if (plotValue > thresholdVO.getWarningThreshold())
                {
                    if (plotValue > thresholdVO.getCriticalThreshold())
                    {
                        currentThreshold = DashboardConstants.CRITICAL_THRESHOLD;
                    }
                    else
                    {
                        currentThreshold = DashboardConstants.WARNING_THRESHOLD;
                    }
                }
            }
            
            // Color the threshold
            Color meterBackgroundColor = Color.BLACK;
            
            plot.setDialBackgroundPaint(DashboardConstants.COLOR_CYAN);
            meterBackgroundColor = DashboardConstants.COLOR_CYAN;
            
            Color needleColor = Color.GREEN;
            if (currentThreshold == DashboardConstants.WARNING_THRESHOLD)
            {
                needleColor = Color.YELLOW;
            }
            else if (currentThreshold == DashboardConstants.CRITICAL_THRESHOLD)
            {
                needleColor = Color.RED;
            }
            plot.setNeedlePaint(needleColor);
            
            
            MeterInterval normalInterval = new MeterInterval( "Normal", normalRange,  Color.GREEN, stroke, meterBackgroundColor);
            plot.addInterval(normalInterval);
            
            MeterInterval warningInterval = new MeterInterval( "Warning", warningRange,  Color.YELLOW, stroke, meterBackgroundColor );
            plot.addInterval(warningInterval);
            
            MeterInterval criticalInterval = new MeterInterval( "Critical", criticalRange,  Color.RED, stroke, meterBackgroundColor );
            plot.addInterval(criticalInterval);
            
            int maxValue = thresholdVO.getMaxDisplayValue();
            if (plotValue > maxValue)
            {
                maxValue = plotValue;    
            }
            Range totalRange = new Range(0,maxValue);
            plot.setRange(totalRange);
            
        }
        
    }

}
