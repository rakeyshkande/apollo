package com.ftd.dashboard.web.util;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.StandardChartTheme;

public class ChartThemeFactory
{
    private final static Color  GOLD_COLOR = new Color(255,215,0);
    
    private static StandardChartTheme darkChartTheme = null;
    
    public static ChartTheme getDarkTheme()
    {
        if (darkChartTheme == null)
        {
            // Black and Gold theme
            darkChartTheme = new StandardChartTheme("DARK");
        
            darkChartTheme.setTitlePaint(GOLD_COLOR);
            darkChartTheme.setChartBackgroundPaint(Color.black); 
            
            darkChartTheme.setAxisLabelPaint(GOLD_COLOR);
            darkChartTheme.setCrosshairPaint(GOLD_COLOR);
            darkChartTheme.setDomainGridlinePaint(GOLD_COLOR);
            darkChartTheme.setGridBandPaint(GOLD_COLOR);
            darkChartTheme.setItemLabelPaint(GOLD_COLOR);
            darkChartTheme.setPlotBackgroundPaint(Color.black);
            darkChartTheme.setPlotOutlinePaint(GOLD_COLOR);
            darkChartTheme.setRangeGridlinePaint(GOLD_COLOR);
            darkChartTheme.setShadowPaint(Color.gray);
            darkChartTheme.setThermometerPaint(GOLD_COLOR);
            darkChartTheme.setWallPaint(Color.black);
            darkChartTheme.setLabelLinkPaint(GOLD_COLOR);
            darkChartTheme.setTickLabelPaint(GOLD_COLOR);
            
            // Setup the fonts to be a bit smaller
            Font smallFont = new Font("Tahoma",Font.PLAIN,8);
            Font regularFont = new Font("Tahoma",Font.PLAIN,10);
            Font largeFont = new Font("Tahoma",Font.PLAIN,12);
            darkChartTheme.setRegularFont(smallFont);
            darkChartTheme.setLargeFont(regularFont);
            darkChartTheme.setExtraLargeFont(largeFont);
            
        }
        
        
        return darkChartTheme;
    }
 
}
