package com.ftd.dashboard.web.util;

import de.laures.cewolf.ChartPostProcessor;

import java.io.Serializable;

import java.util.Map;

public class DefaultZeroAxisChartPostProcessor implements ChartPostProcessor, Serializable
{
    public DefaultZeroAxisChartPostProcessor()
    {
    }

    public void processChart(Object object, Map map)
    {
    }
}
