package com.ftd.dashboard.web.util;

import com.ftd.dashboard.util.SeriesColor;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.ChartPostProcessor;

import java.awt.Color;

import java.awt.Font;

import java.awt.Paint;

import java.io.Serializable;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;

public class DefaultBarChartPostProcessor implements ChartPostProcessor, Serializable
{
    private static Logger logger = new Logger("com.ftd.dashboard.web.util.DefaultBarChartPostProcessor");

    public DefaultBarChartPostProcessor()
    {
    }

    public void processChart(Object chartObject, Map map)
    {
        JFreeChart chart = (JFreeChart)chartObject;
        CategoryPlot categoryPlot = chart.getCategoryPlot();

        CategoryItemRenderer renderer = categoryPlot.getRenderer();
        for (int i=0; i < 6; i++)
        {
            renderer.setSeriesPaint(i, SeriesColor.getInstance().getSeriesColor(i));
        }
        

        // change the auto tick unit selection to integer units only...
        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        /* Remove item label for now
        renderer.setBaseItemLabelsVisible(true);
        
        CategoryItemLabelGenerator labelGenerator = new StandardCategoryItemLabelGenerator();
        renderer.setBaseItemLabelGenerator(labelGenerator);

        // Change the item label to be above the bar
        ItemLabelPosition itemLabelPosition = renderer.getPositiveItemLabelPosition();
        
        itemLabelPosition = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, itemLabelPosition.getTextAnchor());
        renderer.setBasePositiveItemLabelPosition(itemLabelPosition);

        ItemLabelAnchor itemLabelAnchor = itemLabelPosition.getItemLabelAnchor();

        logger.debug("itemLabelAnchor = " + itemLabelAnchor);        
        */
    }


}
