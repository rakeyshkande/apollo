package com.ftd.dashboard.web.util;

import com.ftd.dashboard.util.SeriesColor;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.ChartPostProcessor;

import java.awt.Color;

import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;

public class DefaultTimeSeriesChartPostProcessor implements ChartPostProcessor, Serializable
{
    private static Logger logger  = new Logger("com.ftd.dashboard.web.util.DefaultChartPostProcessor");

    public DefaultTimeSeriesChartPostProcessor()
    {
    }

    public void processChart(Object chartObject, Map map)
    {
        JFreeChart chart = (JFreeChart)chartObject;   
        XYPlot xyPlot = chart.getXYPlot();
        
        XYItemRenderer xyRenderer = xyPlot.getRenderer();
        for (int i=0; i < 6; i++)
        {
            xyRenderer.setSeriesPaint(i, SeriesColor.getInstance().getSeriesColor(i));
            
        }
        
        SimpleDateFormat hourFormat = new SimpleDateFormat("HHmm");

        ValueAxis valueAxis = xyPlot.getDomainAxis();
        DateAxis xAxis = (DateAxis) xyPlot.getDomainAxis(); 
        xAxis.setAutoTickUnitSelection(false);
        
        DateTickUnit hourTickUnit = new DateTickUnit(DateTickUnit.HOUR,2,hourFormat);
        xAxis.setTickUnit(hourTickUnit);
    }
    
    
}
