package com.ftd.dashboard.web.util;

import com.ftd.dashboard.util.SeriesColor;

import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;

public class AuthorizationZeroAxisChartPostProcessor extends DefaultZeroAxisChartPostProcessor
{
    public AuthorizationZeroAxisChartPostProcessor()
    {
    }
    
    public void processChart(Object chartObject, Map map)
    {
        JFreeChart chart = (JFreeChart)chartObject;
        CategoryPlot categoryPlot = chart.getCategoryPlot();

        
        CategoryItemRenderer renderer = categoryPlot.getRenderer();
        for (int i=0; i < 6; i++)
        {
            renderer.setSeriesPaint(i, SeriesColor.getInstance().getSeriesColor(i));
        }

        // change the auto tick unit selection to integer units only...
        NumberAxis rangeAxis = (NumberAxis) categoryPlot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        // Show item values
        renderer.setBaseItemLabelsVisible(true);
        CategoryItemLabelGenerator labelGenerator = new StandardCategoryItemLabelGenerator();
        renderer.setBaseItemLabelGenerator(labelGenerator);
        
    }
    
}
