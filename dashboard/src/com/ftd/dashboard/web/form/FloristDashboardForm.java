package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.tables.BaseTableVO;
import java.util.HashMap;
import java.util.Map;

public class FloristDashboardForm extends BaseDashboardForm
{
    private FloristDetailDataset floristGotoBarData;
    private FloristDetailDataset floristBarData;
    private Map<String, BaseTableVO> setupVOMap = new HashMap<String,BaseTableVO>();

    public FloristDashboardForm()
    {
    }

    public void setFloristGotoBarData(FloristDetailDataset floristGotoBarData)
    {
        this.floristGotoBarData = floristGotoBarData;
    }

    public FloristDetailDataset getFloristGotoBarData()
    {
        return floristGotoBarData;
    }

    public void setFloristBarData(FloristDetailDataset floristBarData)
    {
        this.floristBarData = floristBarData;
    }

    public FloristDetailDataset getFloristBarData()
    {
        return floristBarData;
    }
    
    public Map<String, BaseTableVO> getSetupVO() {
		return setupVOMap;
	}

	public void setSetupVO(Map<String,BaseTableVO> setupVO) {
		this.setupVOMap = setupVO;
	}
}
