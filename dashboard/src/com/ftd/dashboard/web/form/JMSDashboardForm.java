package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.dataset.JMSDetailDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.dataset.QueueDetailDataset;

import com.ftd.dashboard.vo.JMSQueueSetupVO;
import com.ftd.dashboard.vo.ThresholdVO;

import com.ftd.dashboard.vo.JMSStatsVO;

import java.util.Date;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts.action.ActionForm;

public class JMSDashboardForm extends BaseDashboardForm
{
    private JMSDetailDataset jmsOrderFlowBarData;
    private JMSDetailDataset jmsOtherBarData;
    Map<String, JMSQueueSetupVO> orderSetupMap = new HashMap<String, JMSQueueSetupVO>();
    Map<String, JMSQueueSetupVO> otherSetupMap = new HashMap<String, JMSQueueSetupVO>();
    Map<String, JMSStatsVO> orderStatsMap = new HashMap<String, JMSStatsVO>();
    Map<String, JMSStatsVO> otherStatsMap = new HashMap<String, JMSStatsVO>();
    
    
    public JMSDashboardForm()
    {
    }

    public void setJmsOrderFlowBarData(JMSDetailDataset jmsOrderFlowBarData)
    {
        this.jmsOrderFlowBarData = jmsOrderFlowBarData;
    }

    public JMSDetailDataset getJmsOrderFlowBarData()
    {
        return jmsOrderFlowBarData;
    }

    public void setJmsOtherBarData(JMSDetailDataset jmsOtherBarData)
    {
        this.jmsOtherBarData = jmsOtherBarData;
    }

    public JMSDetailDataset getJmsOtherBarData()
    {
        return jmsOtherBarData;
    }

    public void setOrderSetupMap(Map<String, JMSQueueSetupVO> orderSetupMap)
    {
        this.orderSetupMap = orderSetupMap;
    }

    public Map<String, JMSQueueSetupVO> getOrderSetupMap()
    {
        return orderSetupMap;
    }
    public Map<String, ThresholdVO> getOrderThresholdMap()
    {
        return (Map) orderSetupMap;
    }
    public Map<String, JMSQueueSetupVO> getOrderMap()
    {
        return orderSetupMap;
    }

    public void setOtherSetupMap(Map<String, JMSQueueSetupVO> otherSetupMap)
    {
        this.otherSetupMap = otherSetupMap;
    }

    public Map<String, JMSQueueSetupVO> getOtherSetupMap()
    {
        return otherSetupMap;
    }
    
    public Map<String, ThresholdVO> getOtherThresholdMap()
    {
        return (Map) otherSetupMap;
    }


    public void setOrderStatsMap(Map<String, JMSStatsVO> orderStatsMap)
    {
        this.orderStatsMap = orderStatsMap;
    }

    public Map<String, JMSStatsVO> getOrderStatsMap()
    {
        return orderStatsMap;
    }
    
    public void setOtherStatsMap(Map<String, JMSStatsVO> otherStatsMap)
    {
        this.otherStatsMap = otherStatsMap;
    }

    public Map<String, JMSStatsVO> getOtherStatsMap()
    {
        return otherStatsMap;
    }
}
