package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.dataset.OrderOriginDataset;
import com.ftd.dashboard.dataset.OrderProjectionDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.dataset.QueueDetailDataset;

import java.util.Date;

import org.apache.struts.action.ActionForm;

public class AuthorizationStatsPostForm extends BaseDashboardForm
{
    private String authServer;
    private String authClient;
    private String cardType;
    private long   approvals;
    private long   declines;
    private String authorizationTime;
    
    public AuthorizationStatsPostForm()
    {
    }

    public void setAuthServer(String authServer)
    {
        this.authServer = authServer;
    }

    public String getAuthServer()
    {
        return authServer;
    }

    public void setAuthClient(String authClient)
    {
        this.authClient = authClient;
    }

    public String getAuthClient()
    {
        return authClient;
    }

    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public String getCardType()
    {
        return cardType;
    }

    public void setApprovals(long approvals)
    {
        this.approvals = approvals;
    }

    public long getApprovals()
    {
        return approvals;
    }

    public void setDeclines(long declines)
    {
        this.declines = declines;
    }

    public long getDeclines()
    {
        return declines;
    }

    public void setAuthorizationTime(String authorizationTime)
    {
        this.authorizationTime = authorizationTime;
    }

    public String getAuthorizationTime()
    {
        return authorizationTime;
    }
}
