package com.ftd.dashboard.web.form;

public interface DateRangeDashboardForm
{
    public void setLeftRangeSelect(int leftRangeSelect);
    public int getLeftRangeSelect();
    public void setLeftRangeStartDate(String leftRangeStartDate);
    public String getLeftRangeStartDate();
    public void setLeftRangeStartTime(String leftRangeStartTime);
    public String getLeftRangeStartTime();
    public void setLeftRangeEndDate(String leftRangeEndDate);
    public String getLeftRangeEndDate();
    public void setLeftRangeEndTime(String leftRangeEndTime);
    public String getLeftRangeEndTime();
    public void setRightRangeSelect(int rightRangeSelect);
    public int getRightRangeSelect();
    public void setRightRangeStartDate(String rightRangeStartDate);
    public String getRightRangeStartDate();
    public void setRightRangeStartTime(String rightRangeStartTime);
    public String getRightRangeStartTime();
    public void setRightRangeEndDate(String rightRangeEndDate);
    public String getRightRangeEndDate();
    public void setRightRangeEndTime(String rightRangeEndTime);
    public String getRightRangeEndTime();
    public String getLeftSelectedLastHour();
    public String getLeftSelectedLastDay();
    public String getLeftSelectedYesterday();
    public String getLeftSelectedCustom();
    public String getRightSelectedLastHour();
    public String getRightSelectedLastDay();
    public String getRightSelectedYesterday();
    public String getRightSelectedCustom();

}
