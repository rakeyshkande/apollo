package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.AuthorizationDataset;
import com.ftd.dashboard.dataset.OrderServiceDataset;
import com.ftd.dashboard.dataset.PaymentDataset;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.AuthorizationCombinedStatsVO;
import com.ftd.dashboard.vo.PaymentCombinedStatsVO;

import java.util.List;
import java.util.Map;

public class OrderServiceDashboardForm extends AbstractDateRangeDashboardForm
{
    private OrderServiceDataset leftOrderServiceDataset;
    private OrderServiceDataset rightOrderServiceDataset;

    private String groupBy;

    public String getGroupBy()
    {
        return groupBy;
    }

    public void setGroupBy(String groupBy)
    {
        this.groupBy = groupBy;
    }

    public OrderServiceDashboardForm()
    {
        super();
    }

    public void setLeftOrderServiceDataset(OrderServiceDataset leftOrderServiceDataset)
    {
        this.leftOrderServiceDataset = leftOrderServiceDataset;
    }

    public OrderServiceDataset getLeftOrderServiceDataset()
    {
        return leftOrderServiceDataset;
    }

    public void setRightOrderServiceDataset(OrderServiceDataset rightOrderServiceDataset)
    {
        this.rightOrderServiceDataset = rightOrderServiceDataset;
    }

    public OrderServiceDataset getRightOrderServiceDataset()
    {
        return rightOrderServiceDataset;
    }

    public String getClientIdGroupBySelected()
    {
        if (isClientIdGroupBy())
        {
            return DashboardConstants.CHECKED;
        }
        return "";
    }
    public String getNodeGroupBySelected()
    {
        if (isNodeGroupBy())
        {
            return DashboardConstants.CHECKED;
        }
        return "";
    }
    public String getActionTypeGroupBySelected()
    {
        if (isActionTypeGroupBy())
        {
            return DashboardConstants.CHECKED;
        }
        return "";
    }
    public String getStatusCodeGroupBySelected()
    {
        if (isStatusCodeGroupBy())
        {
            return DashboardConstants.CHECKED;
        }
        return "";
    }

    public boolean isClientIdGroupBy()
    {
        if (groupBy != null && groupBy.equals(DashboardConstants.CLIENT_ID))
        {
            return true;
        }
        return false;
    }
    public boolean isActionTypeGroupBy()
    {
        if (groupBy != null && groupBy.equals(DashboardConstants.ACTION_TYPE))
        {
            return true;
        }
        return false;
    }
    public boolean isNodeGroupBy()
    {
        if (groupBy != null && groupBy.equals(DashboardConstants.NODE))
        {
            return true;
        }
        return false;
    }
    public boolean isStatusCodeGroupBy()
    {
        if (groupBy != null && groupBy.equals(DashboardConstants.STATUS_CODE))
        {
            return true;
        }
        return false;
    }

}
