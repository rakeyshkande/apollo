package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.FloristDataset;
import com.ftd.dashboard.dataset.OrderDeliveryDataset;
import com.ftd.dashboard.dataset.OrderOriginDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.util.DashboardConstants;

public class DashboardForm extends BaseDashboardForm
{
    
    // Form Stuff
    private FloristDataset floristXyData;
    private OrderOriginDataset orderOriginXyData;
    private OrderDeliveryDataset orderDeliveryXyData;
    private QueueDataset queueXyData;
    private FloristDashboardForm floristDashboardForm;
    private OrderDashboardForm orderDashboardForm;
    private QueueDashboardForm queueDashboardForm;
    private InventoryDashboardForm inventoryDashboardForm;
    private JMSDashboardForm jmsDashboardForm;
    private PaymentDashboardForm paymentDashboardForm;
    private int    leftRangeSelect;
    private String leftRangeStartDate;
    private String leftRangeStartTime;
    private String leftRangeEndDate;
    private String leftRangeEndTime;
    
    public DashboardForm()
    {
        floristXyData = new FloristDataset();
        orderOriginXyData = new OrderOriginDataset();
        orderDeliveryXyData = new OrderDeliveryDataset();
        queueXyData = new QueueDataset();
        floristDashboardForm = new FloristDashboardForm();
        orderDashboardForm = new OrderDashboardForm();
        queueDashboardForm = new QueueDashboardForm();
        inventoryDashboardForm = new InventoryDashboardForm();
        jmsDashboardForm = new JMSDashboardForm();
        paymentDashboardForm = new PaymentDashboardForm();
        leftRangeSelect = DashboardConstants.RANGE_LAST_HOUR;
    }

    public void setFloristXyData(FloristDataset floristXyData)
    {
        this.floristXyData = floristXyData;
    }

    public FloristDataset getFloristXyData()
    {
        return floristXyData;
    }
    
    public OrderOriginDataset getOrderOriginXyData()
    {
        return orderOriginXyData;
    }

    public void setOrderOriginXyData(OrderOriginDataset orderOriginXyData)
    {
        this.orderOriginXyData = orderOriginXyData;
    }

    public OrderDeliveryDataset getOrderDeliveryXyData()
    {
        return orderDeliveryXyData;
    }

    public void setOrderDeliveryXyData(OrderDeliveryDataset orderDeliveryXyData)
    {
        this.orderDeliveryXyData = orderDeliveryXyData;
    }

    public void setQueueXyData(QueueDataset queueXyData)
    {
        this.queueXyData = queueXyData;
    }

    public QueueDataset getQueueXyData()
    {
        return queueXyData;
    }

	public FloristDashboardForm getFloristDashboardForm() {
		return floristDashboardForm;
	}

	public void setFloristDashboardForm(FloristDashboardForm floristDashboardForm) {
		this.floristDashboardForm = floristDashboardForm;
	}
	public OrderDashboardForm getOrderDashboardForm() {
		return orderDashboardForm;
	}

	public void setOrderDashboardForm(OrderDashboardForm orderDashboardForm) {
		this.orderDashboardForm = orderDashboardForm;
	}

	public QueueDashboardForm getQueueDashboardForm() {
		return queueDashboardForm;
	}

	public void setQueueDashboardForm(QueueDashboardForm queueDashboardForm) {
		this.queueDashboardForm = queueDashboardForm;
	}
	
	public InventoryDashboardForm getInventoryDashboardForm() {
		return inventoryDashboardForm;
	}

	public void setInventoryDashboardForm(InventoryDashboardForm inventoryDashboardForm) {
		this.inventoryDashboardForm = inventoryDashboardForm;
	}
	
	public JMSDashboardForm getJmsDashboardForm() {
		return jmsDashboardForm;
	}

	public void setJmsDashboardForm(JMSDashboardForm jmsDashboardForm) {
		this.jmsDashboardForm = jmsDashboardForm;
	}
	
	public PaymentDashboardForm getPaymentDashboardForm() {
		paymentDashboardForm.setLeftRangeSelect(leftRangeSelect);
		return paymentDashboardForm;
	}

	public void setPaymentDashboardForm(PaymentDashboardForm paymentDashboardForm) {
		this.paymentDashboardForm = paymentDashboardForm;
	}

	public int getLeftRangeSelect() {
		return leftRangeSelect;
	}

	public void setLeftRangeSelect(int leftRangeSelect) {
		this.leftRangeSelect = leftRangeSelect;
	}

	public String getLeftRangeStartDate() {
		return leftRangeStartDate;
	}

	public void setLeftRangeStartDate(String leftRangeStartDate) {
		this.leftRangeStartDate = leftRangeStartDate;
	}

	public String getLeftRangeStartTime() {
		return leftRangeStartTime;
	}

	public void setLeftRangeStartTime(String leftRangeStartTime) {
		this.leftRangeStartTime = leftRangeStartTime;
	}

	public String getLeftRangeEndDate() {
		return leftRangeEndDate;
	}

	public void setLeftRangeEndDate(String leftRangeEndDate) {
		this.leftRangeEndDate = leftRangeEndDate;
	}

	public String getLeftRangeEndTime() {
		return leftRangeEndTime;
	}

	public void setLeftRangeEndTime(String leftRangeEndTime) {
		this.leftRangeEndTime = leftRangeEndTime;
	}
	
}
