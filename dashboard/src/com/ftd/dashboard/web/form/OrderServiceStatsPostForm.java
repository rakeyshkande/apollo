package com.ftd.dashboard.web.form;

public class OrderServiceStatsPostForm extends BaseDashboardForm
{

  private String node;
  private String clientId;
  private String actionType;
  private long count;
  private String statusCode;
  private String statsTimestamp;

  public OrderServiceStatsPostForm()
  {
  }

  public void setNode(String node)
  {
    this.node = node;
  }

  public String getNode()
  {
    return node;
  }

  public void setClientId(String clientId)
  {
    this.clientId = clientId;
  }

  public String getClientId()
  {
    return clientId;
  }

  public void setActionType(String actionType)
  {
    this.actionType = actionType;
  }

  public String getActionType()
  {
    return actionType;
  }

  public void setCount(long count)
  {
    this.count = count;
  }

  public long getCount()
  {
    return count;
  }

  public void setStatusCode(String statusCode)
  {
    this.statusCode = statusCode;
  }

  public String getStatusCode()
  {
    return statusCode;
  }

  public void setStatsTimestamp(String statsTimestamp)
  {
    this.statsTimestamp = statsTimestamp;
  }

  public String getStatsTimestamp()
  {
    return statsTimestamp;
  }
}
