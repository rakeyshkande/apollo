package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.ProductCumulativeDataset;
import com.ftd.dashboard.dataset.ProductRateDataset;
import com.ftd.dashboard.tables.InventoryTableVO;

import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.struts.action.ActionForm;


public class InventoryDashboardForm extends BaseDashboardForm
{

    // Form Stuff
    private ProductCumulativeDataset floristCumulativeXYData;
    private ProductCumulativeDataset vendorCumulativeXYData;
    private ProductRateDataset floristRateXYData;
    private ProductRateDataset vendorRateXYData;
    private LinkedHashMap<String, InventoryTableVO> setupVOMap = new LinkedHashMap<String,InventoryTableVO>();
    private int  numberProductsToShow;

    public InventoryDashboardForm()
    {
        floristCumulativeXYData = new ProductCumulativeDataset();
        vendorCumulativeXYData = new ProductCumulativeDataset();
        floristRateXYData = new ProductRateDataset();
        vendorRateXYData = new ProductRateDataset();
        
        numberProductsToShow = 5;
    }

    
    public LinkedHashMap<String, InventoryTableVO> getSetupVO() {
		return setupVOMap;
	}

	public void setSetupVO(LinkedHashMap<String, InventoryTableVO> setupVO) {
		this.setupVOMap = setupVO;
	}

    public void setFloristCumulativeXYData(ProductCumulativeDataset floristCumulativeXYData)
    {
        this.floristCumulativeXYData = floristCumulativeXYData;
    }

    public ProductCumulativeDataset getFloristCumulativeXYData()
    {
        return floristCumulativeXYData;
    }

    public void setVendorCumulativeXYData(ProductCumulativeDataset vendorCumulativeXYData)
    {
        this.vendorCumulativeXYData = vendorCumulativeXYData;
    }

    public ProductCumulativeDataset getVendorCumulativeXYData()
    {
        return vendorCumulativeXYData;
    }

    public void setFloristRateXYData(ProductRateDataset floristRateXYData)
    {
        this.floristRateXYData = floristRateXYData;
    }

    public ProductRateDataset getFloristRateXYData()
    {
        return floristRateXYData;
    }

    public void setVendorRateXYData(ProductRateDataset vendorRateXYData)
    {
        this.vendorRateXYData = vendorRateXYData;
    }

    public ProductRateDataset getVendorRateXYData()
    {
        return vendorRateXYData;
    }

    public void setNumberProductsToShow(int numberProductsToShow)
    {
        this.numberProductsToShow = numberProductsToShow;
    }

    public int getNumberProductsToShow()
    {
        return numberProductsToShow;
    }
}
