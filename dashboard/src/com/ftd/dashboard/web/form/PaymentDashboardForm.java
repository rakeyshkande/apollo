package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.AuthorizationDataset;
import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.dataset.JMSDetailDataset;
import com.ftd.dashboard.dataset.PaymentDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.dataset.QueueDetailDataset;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.AuthorizationCombinedStatsVO;
import com.ftd.dashboard.vo.AuthorizationStatsVO;
import com.ftd.dashboard.vo.JMSQueueSetupVO;
import com.ftd.dashboard.vo.JMSStatsVO;

import com.ftd.dashboard.vo.PaymentCombinedStatsVO;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.ActionForm;

public class PaymentDashboardForm extends  AbstractDateRangeDashboardForm
{
    private Map<String,PaymentCombinedStatsVO> combinedPaymentMap;
    private PaymentDataset leftPaymentDataset;
    private PaymentDataset rightPaymentDataset;
        
    private Map<String,AuthorizationCombinedStatsVO> combinedAuthorizationMap;
    private AuthorizationDataset leftAuthorizationDataset;
    private AuthorizationDataset rightAuthorizationDataset;


    public PaymentDashboardForm()
    {
        super();
    }

    public void setLeftPaymentDataset(PaymentDataset leftPaymentDataset)
    {
        this.leftPaymentDataset = leftPaymentDataset;
    }

    public PaymentDataset getLeftPaymentDataset()
    {
        return leftPaymentDataset;
    }

    public void setRightPaymentDataset(PaymentDataset rightPaymentDataset)
    {
        this.rightPaymentDataset = rightPaymentDataset;
    }

    public PaymentDataset getRightPaymentDataset()
    {
        return rightPaymentDataset;
    }

    public void setLeftAuthorizationDataset(AuthorizationDataset leftAuthorizationDataset)
    {
        this.leftAuthorizationDataset = leftAuthorizationDataset;
    }

    public AuthorizationDataset getLeftAuthorizationDataset()
    {
        return leftAuthorizationDataset;
    }

    public void setRightAuthorizationDataset(AuthorizationDataset rightAuthorizationDataset)
    {
        this.rightAuthorizationDataset = rightAuthorizationDataset;
    }

    public AuthorizationDataset getRightAuthorizationDataset()
    {
        return rightAuthorizationDataset;
    }

    public void setCombinedPaymentMap(Map<String,PaymentCombinedStatsVO> combinedPaymentMap)
    {
        this.combinedPaymentMap = combinedPaymentMap;
    }

    public Map<String,PaymentCombinedStatsVO> getCombinedPaymentMap()
    {
        return combinedPaymentMap;
    }

    public void setCombinedAuthorizationMap(Map<String, AuthorizationCombinedStatsVO> combinedAuthorizationMap)
    {
        this.combinedAuthorizationMap = combinedAuthorizationMap;
    }

    public Map<String, AuthorizationCombinedStatsVO> getCombinedAuthorizationMap()
    {
        return combinedAuthorizationMap;
    }
    
}
