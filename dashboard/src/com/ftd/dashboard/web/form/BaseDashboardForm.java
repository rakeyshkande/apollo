package com.ftd.dashboard.web.form;

import java.util.Date;

import org.apache.struts.action.ActionForm;

public class BaseDashboardForm extends ActionForm
{
    protected String securitytoken;
    protected String context;
    protected Date lastUpdated;
    
    public BaseDashboardForm()
    {
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }

    public void setLastUpdated(Date lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }

    public Date getLastUpdated()
    {
        return lastUpdated;
    }

    public void setSecuritytoken(String securitytoken)
    {
        this.securitytoken = securitytoken;
    }

    public String getSecuritytoken()
    {
        return securitytoken;
    }
}
