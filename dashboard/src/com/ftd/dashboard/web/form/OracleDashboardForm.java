package com.ftd.dashboard.web.form;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.general.DefaultValueDataset;

import com.ftd.dashboard.dataset.OracleClusterDataset;
import com.ftd.dashboard.dataset.OracleInstanceDataset;
import com.ftd.dashboard.vo.OracleStatusVO;

public class OracleDashboardForm extends BaseDashboardForm
{
    private OracleInstanceDataset oracleInstanceDataset;
    private OracleClusterDataset  oracleLocksDataset;
    private OracleClusterDataset  oracleLocksAgedDataset;
    private OracleClusterDataset  oracleDataDiskgroupDataset;
    private OracleClusterDataset  oracleFlashDiskgroupDataset;
    private OracleClusterDataset  oracleTablespaceFreeDataset;
        
    private List<String> instances;
    
    private OracleStatusVO oracleStatusVO;
    
    private String showMemoryGraph;
    private String showUserCallsGraph;
    private String showTransactionsGraph;
    private String showLogicalReadsGraph;
    private String showPhysicalReadsGraph;
    private String showLoadGraph;
    private String showDataDiskgroupGraph;
    private String showFlashDiskgroupGraph;
    private String showTablespaceFreeGraph;
    
    private String showTickMarks;
    
    private String smallGraphSize;
    private String largeGraphSize;
    
    
    public OracleDashboardForm()
    {
        instances = new ArrayList<String>();
        
        // TODO DELETE, sample data
        oracleInstanceDataset = new OracleInstanceDataset("zeus");
        
        oracleLocksDataset = new OracleClusterDataset("Locks");
        oracleLocksDataset.setValue(0);
        oracleLocksAgedDataset = new OracleClusterDataset("Aged Locks");
        oracleLocksAgedDataset.setValue(0);
        
        oracleDataDiskgroupDataset = new OracleClusterDataset("Data");
        oracleDataDiskgroupDataset.setValue(0);
        oracleFlashDiskgroupDataset = new OracleClusterDataset("Flash");
        oracleFlashDiskgroupDataset.setValue(0);
        oracleTablespaceFreeDataset = new OracleClusterDataset("% Free");
        oracleTablespaceFreeDataset.setValue(0);
        
        instances.add("plutonium");
        instances.add("uranium");
        instances.add("tritium");
        instances.add("deuterium");
        
        oracleInstanceDataset.addInstanceLoadStat("plutonium", 1.0);
        oracleInstanceDataset.addInstanceLoadStat("uranium", 1.0);
        oracleInstanceDataset.addInstanceLoadStat("tritium", 1.0);
        oracleInstanceDataset.addInstanceLoadStat("deuterium", 1.0);
        
        smallGraphSize = "120";
        largeGraphSize = "150";
        
        showMemoryGraph         = "TRUE";
        showUserCallsGraph      = "TRUE";
        showTransactionsGraph   = "TRUE";
        showLogicalReadsGraph   = "TRUE";
        showPhysicalReadsGraph  = "TRUE";
        showLoadGraph           = "TRUE";
        showDataDiskgroupGraph  = "TRUE";
        showFlashDiskgroupGraph = "TRUE";
        showTablespaceFreeGraph = "TRUE";
    }
    
    public OracleInstanceDataset getOracleInstanceDataset()
    {
        return oracleInstanceDataset;
    }

    public void setOracleInstanceDataset(OracleInstanceDataset oracleInstanceDataset)
    {
        this.oracleInstanceDataset = oracleInstanceDataset;
    }

    public OracleClusterDataset getOracleLocksDataset()
    {
        return oracleLocksDataset;
    }
    
    public OracleClusterDataset getOracleLocksAgedDataset()
    {
        return oracleLocksAgedDataset;
    }
    
    public OracleClusterDataset getOracleDataDiskgroupDataset()
    {
        return oracleDataDiskgroupDataset;
    }
    
    public OracleClusterDataset getOracleFlashDiskgroupDataset()
    {
        return oracleFlashDiskgroupDataset;
    }
    public OracleClusterDataset getOracleTablespaceFreeDataset()
    {
        return oracleTablespaceFreeDataset;
    }

    public String getSmallGraphSize()
    {
        return smallGraphSize;
    }

    public void setSmallGraphSize(String smallGraphSize)
    {
        this.smallGraphSize = smallGraphSize;
    }

    public String getLargeGraphSize()
    {
        return largeGraphSize;
    }

    public void setLargeGraphSize(String largeGraphSize)
    {
        this.largeGraphSize = largeGraphSize;
    }

    public String getShowMemoryGraph()
    {
        return showMemoryGraph;
    }

    public void setShowMemoryGraph(String showMemoryGraph)
    {
        this.showMemoryGraph = showMemoryGraph;
    }

    public String getShowUserCallsGraph()
    {
        return showUserCallsGraph;
    }

    public void setShowUserCallsGraph(String showUserCallsGraph)
    {
        this.showUserCallsGraph = showUserCallsGraph;
    }

    public String getShowTransactionsGraph()
    {
        return showTransactionsGraph;
    }

    public void setShowTransactionsGraph(String showTransactionsGraph)
    {
        this.showTransactionsGraph = showTransactionsGraph;
    }

    public String getShowLogicalReadsGraph()
    {
        return showLogicalReadsGraph;
    }

    public void setShowLogicalReadsGraph(String showLogicalReadsGraph)
    {
        this.showLogicalReadsGraph = showLogicalReadsGraph;
    }

    public String getShowPhysicalReadsGraph()
    {
        return showPhysicalReadsGraph;
    }

    public void setShowPhysicalReadsGraph(String showPhysicalReadsGraph)
    {
        this.showPhysicalReadsGraph = showPhysicalReadsGraph;
    }

    public String getShowDataDiskgroupGraph()
    {
        return showDataDiskgroupGraph;
    }

    public void setShowDataDiskgroupGraph(String showDataDiskgroupGraph)
    {
        this.showDataDiskgroupGraph = showDataDiskgroupGraph;
    }

    public String getShowFlashDiskgroupGraph()
    {
        return showFlashDiskgroupGraph;
    }

    public void setShowFlashDiskgroupGraph(String showFlashDiskgroupGraph)
    {
        this.showFlashDiskgroupGraph = showFlashDiskgroupGraph;
    }

    public String getShowTablespaceFreeGraph()
    {
        return showTablespaceFreeGraph;
    }

    public void setShowTablespaceFreeGraph(String showTablespaceFreeGraph)
    {
        this.showTablespaceFreeGraph = showTablespaceFreeGraph;
    }

    public OracleStatusVO getOracleStatusVO()
    {
        return oracleStatusVO;
    }

    public void setOracleStatusVO(OracleStatusVO oracleStatusVO)
    {
        this.oracleStatusVO = oracleStatusVO;
    }

    public List<String> getInstances()
    {
        return instances;
    }

    public void setInstances(List<String> instances)
    {
        this.instances = instances;
    }

    public String getShowLoadGraph()
    {
        return showLoadGraph;
    }

    public void setShowLoadGraph(String showLoadGraph)
    {
        this.showLoadGraph = showLoadGraph;
    }

    public String getShowTickMarks()
    {
        return showTickMarks;
    }

    public void setShowTickMarks(String showTickMarks)
    {
        this.showTickMarks = showTickMarks;
    }

}
