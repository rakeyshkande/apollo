package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.AuthorizationDataset;
import com.ftd.dashboard.dataset.PaymentDataset;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.AuthorizationCombinedStatsVO;
import com.ftd.dashboard.vo.PaymentCombinedStatsVO;

import java.util.List;
import java.util.Map;

public abstract class AbstractDateRangeDashboardForm extends BaseDashboardForm implements DateRangeDashboardForm
{
    private String compareToSelection;
    private List<String> compareToSelectionList;

    private int    leftRangeSelect;
    private String leftRangeStartDate;
    private String leftRangeStartTime;
    private String leftRangeEndDate;
    private String leftRangeEndTime;

    private int    rightRangeSelect;
    private String rightRangeStartDate;
    private String rightRangeStartTime;
    private String rightRangeEndDate;
    private String rightRangeEndTime;


    public AbstractDateRangeDashboardForm()
    {
        // Set some defaults
        leftRangeSelect = DashboardConstants.RANGE_LAST_HOUR;
        rightRangeSelect = DashboardConstants.RANGE_LAST_DAY;
    }

    public void setCompareToSelection(String compareToSelection)
    {
        this.compareToSelection = compareToSelection;
    }

    public String getCompareToSelection()
    {
        return compareToSelection;
    }

    public void setCompareToSelectionList(List<String> compareToSelectionList)
    {
        this.compareToSelectionList = compareToSelectionList;
    }

    public List<String> getCompareToSelectionList()
    {
        return compareToSelectionList;
    }

    public void setLeftRangeSelect(int leftRangeSelect)
    {
        this.leftRangeSelect = leftRangeSelect;
    }

    public int getLeftRangeSelect()
    {
        return leftRangeSelect;
    }

    public void setLeftRangeStartDate(String leftRangeStartDate)
    {
        this.leftRangeStartDate = leftRangeStartDate;
    }

    public String getLeftRangeStartDate()
    {
        return leftRangeStartDate;
    }

    public void setLeftRangeStartTime(String leftRangeStartTime)
    {
        this.leftRangeStartTime = leftRangeStartTime;
    }

    public String getLeftRangeStartTime()
    {
        return leftRangeStartTime;
    }

    public void setLeftRangeEndDate(String leftRangeEndDate)
    {
        this.leftRangeEndDate = leftRangeEndDate;
    }

    public String getLeftRangeEndDate()
    {
        return leftRangeEndDate;
    }

    public void setLeftRangeEndTime(String leftRangeEndTime)
    {
        this.leftRangeEndTime = leftRangeEndTime;
    }

    public String getLeftRangeEndTime()
    {
        return leftRangeEndTime;
    }

    public void setRightRangeSelect(int rightRangeSelect)
    {
        this.rightRangeSelect = rightRangeSelect;
    }

    public int getRightRangeSelect()
    {
        return rightRangeSelect;
    }

    public void setRightRangeStartDate(String rightRangeStartDate)
    {
        this.rightRangeStartDate = rightRangeStartDate;
    }

    public String getRightRangeStartDate()
    {
        return rightRangeStartDate;
    }

    public void setRightRangeStartTime(String rightRangeStartTime)
    {
        this.rightRangeStartTime = rightRangeStartTime;
    }

    public String getRightRangeStartTime()
    {
        return rightRangeStartTime;
    }

    public void setRightRangeEndDate(String rightRangeEndDate)
    {
        this.rightRangeEndDate = rightRangeEndDate;
    }

    public String getRightRangeEndDate()
    {
        return rightRangeEndDate;
    }

    public void setRightRangeEndTime(String rightRangeEndTime)
    {
        this.rightRangeEndTime = rightRangeEndTime;
    }

    public String getRightRangeEndTime()
    {
        return rightRangeEndTime;
    }

    public String getLeftSelectedLastHour()
    {
        return getRangeSelected(DashboardConstants.RANGE_LAST_HOUR, leftRangeSelect);
    }
    public String getLeftSelectedLastDay()
    {
        return getRangeSelected(DashboardConstants.RANGE_LAST_DAY, leftRangeSelect);
    }
    public String getLeftSelectedYesterday()
    {
        return getRangeSelected(DashboardConstants.RANGE_YESTERDAY, leftRangeSelect);
    }
    public String getLeftSelectedCustom()
    {
        return getRangeSelected(DashboardConstants.RANGE_CUSTOM, leftRangeSelect);
    }
    public String getRightSelectedLastHour()
    {
        return getRangeSelected(DashboardConstants.RANGE_LAST_HOUR, rightRangeSelect);
    }
    public String getRightSelectedLastDay()
    {
        return getRangeSelected(DashboardConstants.RANGE_LAST_DAY, rightRangeSelect);
    }
    public String getRightSelectedYesterday()
    {
        return getRangeSelected(DashboardConstants.RANGE_YESTERDAY, rightRangeSelect);
    }
    public String getRightSelectedCustom()
    {
        return getRangeSelected(DashboardConstants.RANGE_CUSTOM, rightRangeSelect);
    }
    public String getRangeSelected(int field, int rangeSelect)
    {
        if (field == rangeSelect)
            return "checked";
        else
            return "";
    }
}
