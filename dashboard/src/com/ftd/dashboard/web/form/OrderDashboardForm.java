package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.OrderOriginDataset;
import com.ftd.dashboard.dataset.OrderProjectionDataset;
import com.ftd.dashboard.tables.BaseTableVO;

import java.util.LinkedHashMap;

public class OrderDashboardForm extends BaseDashboardForm
{
    private OrderOriginDataset orderOriginXyData;
    private OrderProjectionDataset orderProjectionMeterData;
    private LinkedHashMap<String, BaseTableVO> setupVOMap = new LinkedHashMap<String,BaseTableVO>();

    public OrderDashboardForm()
    {
    }

    public LinkedHashMap<String, BaseTableVO> getSetupVO() {
		return setupVOMap;
	}

	public void setSetupVO(LinkedHashMap<String, BaseTableVO> setupVO) {
		this.setupVOMap = setupVO;
	}

    public OrderOriginDataset getOrderOriginXyData()
    {
        return orderOriginXyData;
    }

    public void setOrderOriginXyData(OrderOriginDataset orderOriginXyData)
    {
        this.orderOriginXyData = orderOriginXyData;
    }

    public void setOrderProjectionMeterData(OrderProjectionDataset orderProjectionMeterData)
    {
        this.orderProjectionMeterData = orderProjectionMeterData;
    }

    public OrderProjectionDataset getOrderProjectionMeterData()
    {
        return orderProjectionMeterData;
    }
}
