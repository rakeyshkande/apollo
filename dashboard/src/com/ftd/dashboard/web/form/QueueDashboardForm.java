package com.ftd.dashboard.web.form;

import com.ftd.dashboard.dataset.QueueDetailDataset;
import com.ftd.dashboard.tables.BaseTableVO;

import java.util.LinkedHashMap;

public class QueueDashboardForm extends BaseDashboardForm
{
    private QueueDetailDataset queueXyData;
    private LinkedHashMap<String, BaseTableVO> setupVOMap = new LinkedHashMap<String,BaseTableVO>();
    
    public LinkedHashMap<String, BaseTableVO> getSetupVO() {
		return setupVOMap;
	}

	public void setSetupVO(LinkedHashMap<String, BaseTableVO> setupVO) {
		this.setupVOMap = setupVO;
	}
    
    public QueueDashboardForm()
    {
    }

    public void setQueueXyData(QueueDetailDataset queueXyData)
    {
        this.queueXyData = queueXyData;
    }

    public QueueDetailDataset getQueueXyData()
    {
        return queueXyData;
    }

}
