package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class FloristDetailDataset implements DatasetProducer, Serializable, 
                                             CategoryToolTipGenerator
{
    private static Logger logger = 
        new Logger("com.ftd.dashboard.dataset.FloristDetailDataset");

    private DefaultCategoryDataset gotoFloristShutdownDataset;

    private List<String> seriesValues;
    
    private NumberFormat defaultToolTipNumberFormat = new DecimalFormat("########0");
    private NumberFormat toolTipNumberFormat = null;
    
    public FloristDetailDataset(int hoursBack)
    {
        gotoFloristShutdownDataset = new DefaultCategoryDataset();

        seriesValues = new ArrayList<String>();
        
        for (int i=0; i < hoursBack; i++)
        {
            if (i==0)
            {
                seriesValues.add("Now");
            }
            else if (i==1)
            {
                seriesValues.add("1 Hour Ago");
            }
            else
            {
                seriesValues.add(i + " Hours Ago");
            }
        }
    }


    public Object produceDataset(Map map)
    {
        return gotoFloristShutdownDataset;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(long dataValue, int hoursBack, String region)
    {
        String series = seriesValues.get(hoursBack);
        
        gotoFloristShutdownDataset.addValue(dataValue,series,region);
    }

    public void addStat(double dataValue, int hoursBack, String region)
    {
        String series = seriesValues.get(hoursBack);

        gotoFloristShutdownDataset.addValue(dataValue,series,region);
    }

    public void setToolTipValueFormatter(NumberFormat nf)
    {
        this.toolTipNumberFormat = nf;
    }
    
    public String generateToolTip(CategoryDataset categoryDataset, int row, int column)
    {
        StringBuilder sb = new StringBuilder();
        
        Number value = categoryDataset.getValue(row,column);
        String regionName = (String) categoryDataset.getColumnKey(column);
        String seriesName = (String) categoryDataset.getRowKey(row);

        if (toolTipNumberFormat == null)
        {
            sb.append(defaultToolTipNumberFormat.format(value));        
        }
        else
        {
            sb.append(toolTipNumberFormat.format(value));        
        }
        sb.append(" Shutdown");
        sb.append("\nRegion:  ");
        sb.append(regionName);
        sb.append("\n");
        sb.append(seriesName);
        
        return sb.toString();
    }

}
