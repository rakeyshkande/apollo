package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.QueueDetailStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class QueueDetailDataset implements DatasetProducer, Serializable, 
                                           XYToolTipGenerator
{
    private static Logger logger = 
        new Logger("com.ftd.dashboard.dataset.QueueDetailDataset");

    private TimeSeriesCollection seriesCollection;

    private HashMap<String,TimeSeries>  queueSeriesMap;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");

    public QueueDetailDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        queueSeriesMap = new HashMap<String,TimeSeries>();
    }


    public Object produceDataset(Map map)
    {
        for (String key : queueSeriesMap.keySet())
        {
            TimeSeries timeSeries = queueSeriesMap.get(key);
            seriesCollection.addSeries(timeSeries);
        }
        
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(QueueDetailStatsVO queueDetailStatsVO, 
                        RegularTimePeriod timePeriod)
    {
        TimeSeries timeSeries = queueSeriesMap.get(queueDetailStatsVO.getQueueType());
        
        if (timeSeries == null)
        {
            timeSeries = new TimeSeries(queueDetailStatsVO.getQueueType(), Minute.class);
            queueSeriesMap.put(queueDetailStatsVO.getQueueType(), timeSeries);
        }
        
        timeSeries.add(timePeriod, queueDetailStatsVO.getQueueCount());
    }

    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection)data;
        TimeSeries timeSeries = collection.getSeries(series);

        StringBuilder sb = new StringBuilder();

        double xValue = data.getXValue(series, item);
        Number xTime = collection.getX(series, item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series, item);

        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValue);

        return sb.toString();
    }
}
