package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.dashboard.vo.JMSQueueSetupVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class JMSDetailDataset implements DatasetProducer, Serializable, CategoryToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.JMSDetailDataset");

    private DefaultCategoryDataset jmsQueueDataset;

    private List<String> seriesValues;
    private final static String DEFAULT_SERIES = "JMS Queue Count";

    private NumberFormat defaultToolTipNumberFormat = new DecimalFormat("########0");
    private NumberFormat toolTipNumberFormat = null;

    private HashMap<String,JMSQueueSetupVO> setupMap;
    
    public JMSDetailDataset()
    {
        jmsQueueDataset = new DefaultCategoryDataset();
        setupMap = new HashMap<String,JMSQueueSetupVO>();
    }


    public Object produceDataset(Map map)
    {
        return jmsQueueDataset;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(long dataValue, String queueName)
    {
        jmsQueueDataset.addValue(dataValue, DEFAULT_SERIES, queueName);
    }

    public void addStat(double dataValue, String queueName)
    {
        jmsQueueDataset.addValue(dataValue, DEFAULT_SERIES, queueName);
    }

    public void setToolTipValueFormatter(NumberFormat nf)
    {
        this.toolTipNumberFormat = nf;
    }

    public String generateToolTip(CategoryDataset categoryDataset, int row, int column)
    {
        StringBuilder sb = new StringBuilder();

        Number value = categoryDataset.getValue(row, column);
        String queueName = (String)categoryDataset.getColumnKey(column);

        if (toolTipNumberFormat == null)
        {
            sb.append(defaultToolTipNumberFormat.format(value));
        } else
        {
            sb.append(toolTipNumberFormat.format(value));
        }
        sb.append(" in ");
        sb.append(queueName);

        return sb.toString();
    }


    public void addSetup(JMSQueueSetupVO setupVO)
    {
        setupMap.put(setupVO.getQueueName(), setupVO);
    }
}
