package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.PaymentStatsVO;

import de.laures.cewolf.DatasetProducer;

import de.laures.cewolf.tooltips.PieToolTipGenerator;

import java.io.Serializable;

import java.util.Date;
import java.util.Map;

import org.jfree.data.general.DefaultPieDataset;

public class PaymentDataset implements DatasetProducer, Serializable, PieToolTipGenerator
{
    private DefaultPieDataset pieDataset;

    public PaymentDataset()
    {
        pieDataset = new DefaultPieDataset();
    }

    public Object produceDataset(Map map)
    {
        return pieDataset;
    }

    public boolean hasExpired(Map map, Date date)
    {
        return false;
    }

    public String getProducerId()
    {
        return null;
    }
    
    public void addStat(PaymentStatsVO paymentStatsVO)
    {
        pieDataset.setValue(paymentStatsVO.getPaymentType(), paymentStatsVO.getCount());    
    }

    public String generateToolTip(org.jfree.data.general.PieDataset pieDataset, Comparable comparable, int i)
    {
        return "blah";
    }
}
