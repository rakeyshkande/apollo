package com.ftd.dashboard.dataset;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Map;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultValueDataset;

import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

public class OracleClusterDataset implements DatasetProducer, Serializable, CategoryToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.OracleInstanceDataset");

    private DefaultValueDataset oracleInstanceDataset;

    private final static String DEFAULT_SERIES = "Sessions";

    private NumberFormat defaultToolTipNumberFormat = new DecimalFormat("########0");
    private NumberFormat toolTipNumberFormat = null;

    private String seriesName;
    
    public OracleClusterDataset(String seriesName)
    {
        oracleInstanceDataset = new DefaultValueDataset();
        this.seriesName = seriesName;
        
        // TODO Delete me
        oracleInstanceDataset.setValue(100);
    }

    public void setValue(long value)
    {
        oracleInstanceDataset.setValue(value);
    }
    public void setValue(double value)
    {
        oracleInstanceDataset.setValue(value);
    }

    public Object produceDataset(Map map)
    {
        return oracleInstanceDataset;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "OracleInstanceData DatasetProducer";
    }

    public void setToolTipValueFormatter(NumberFormat nf)
    {
        this.toolTipNumberFormat = nf;
    }

    public String generateToolTip(CategoryDataset categoryDataset, int row, int column)
    {
        StringBuilder sb = new StringBuilder();

        Number value = categoryDataset.getValue(row, column);
        String queueName = (String)categoryDataset.getColumnKey(column);

        if (toolTipNumberFormat == null)
        {
            sb.append(defaultToolTipNumberFormat.format(value));
        } else
        {
            sb.append(toolTipNumberFormat.format(value));
        }
        sb.append(" in ");
        sb.append(queueName);

        return sb.toString();
    }

}
