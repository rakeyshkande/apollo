package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;

import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class OrderDeliveryDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.OrderDeliveryDataset");

    private TimeSeriesCollection seriesCollection;

    private TimeSeries orderCountSeries;
    private TimeSeries floristOrderCountSeries;
    private TimeSeries vendorOrderCountSeries;
    private TimeSeries floristOrderPercentSeries;
    private TimeSeries vendorOrderPercentSeries;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");
    private NumberFormat nf = new DecimalFormat("#0.00");

    public OrderDeliveryDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        orderCountSeries = new TimeSeries("Total", Minute.class);
        floristOrderCountSeries = new TimeSeries("Florist", Minute.class);
        vendorOrderCountSeries = new TimeSeries("Vendor", Minute.class);
        floristOrderPercentSeries = new TimeSeries("% Florist", Minute.class);
        vendorOrderPercentSeries = new TimeSeries("% Vendor", Minute.class);

//        seriesCollection.addSeries(orderCountSeries);
//        seriesCollection.addSeries(floristOrderCountSeries);
//        seriesCollection.addSeries(vendorOrderCountSeries);
        seriesCollection.addSeries(floristOrderPercentSeries);
        seriesCollection.addSeries(vendorOrderPercentSeries);
    }


    public TimeSeriesCollection getSeriesCollection(){
    	
    	return seriesCollection;
    }
    
    
    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(OrderStatsVO orderStatsVO, 
                        RegularTimePeriod timePeriod)
    {
        orderCountSeries.add(timePeriod, orderStatsVO.getTotalOrderCount());
        floristOrderCountSeries.add(timePeriod, orderStatsVO.getFloristOrderCount());
        vendorOrderCountSeries.add(timePeriod, orderStatsVO.getVendorOrderCount());

        double floristPercent = (double) (100 * orderStatsVO.getFloristOrderCount()) / (double) orderStatsVO.getTotalOrderCount();
        double vendorPercent = (double) (100 * orderStatsVO.getVendorOrderCount()) / (double) orderStatsVO.getTotalOrderCount();
        floristOrderPercentSeries.add(timePeriod, floristPercent);
        vendorOrderPercentSeries.add(timePeriod, vendorPercent);

    }
    
    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection) data;
        TimeSeries timeSeries = collection.getSeries(series);
        
        StringBuilder sb = new StringBuilder();
        
        double xValue = data.getXValue(series,item);
        Number xTime = collection.getX(series,item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series,item);
        String yValueString = nf.format(yValue);
        
        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValueString);
        
        return sb.toString();
    }    
}
