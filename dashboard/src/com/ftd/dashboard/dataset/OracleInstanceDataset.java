package com.ftd.dashboard.dataset;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.DefaultValueDataset;

import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

public class OracleInstanceDataset implements DatasetProducer, Serializable, CategoryToolTipGenerator
{
    public final static String GLOBAL_PERCENT  = "global %";
    public final static String LOCAL_PERCENT   = "local %";
    public final static String DISK_PERCENT    = "disk %";
    
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.OracleInstanceDataset");

    private final static String DEFAULT_SERIES = "Sessions";
    private final static String SESSIONS       = "sessions";
    private final static String GCREMASTER     = "gcremaster";
    private final static String LOGFILERESYNC  = "logfileresync";
    private final static String CACHE          = "cache";
    private final static String PHYSICAL_READS = "physicalreads";
    private final static String LOGICAL_READS  = "logicalreads";
    private final static String TRANSACTIONS   = "transactions";
    private final static String USER_CALLS     = "usercalls";
    private final static String LOAD           = "load";
    private final static String MEMORY_USED    = "memoryused";
    

    private NumberFormat defaultToolTipNumberFormat = new DecimalFormat("########0");
    private NumberFormat toolTipNumberFormat = null;

    private String seriesName;
    
    private Map<String,Object> datasetMap;
    
    private Map<String,String> instanceNameSymbolMap;
    
    public OracleInstanceDataset(String seriesName)
    {
        this.seriesName = seriesName;
        
        
        // Add all the datasets to the map for easy retrieval
        this.datasetMap = new HashMap<String,Object>();
        
        // Oracle Session dataset 
        DefaultCategoryDataset oracleSessionDataset = new DefaultCategoryDataset();
        datasetMap.put(SESSIONS, oracleSessionDataset);
        DefaultCategoryDataset oraclegcremasterDataset = new DefaultCategoryDataset();
        datasetMap.put(GCREMASTER, oraclegcremasterDataset);
        DefaultCategoryDataset oraclelogfileresyncDataset = new DefaultCategoryDataset();
        datasetMap.put(LOGFILERESYNC, oraclelogfileresyncDataset);
        
        // Oracle cache instance dataset Map
        Map<String, Object> cacheInstanceDataset = new HashMap<String,Object>();
        datasetMap.put("cache",cacheInstanceDataset);
        
        DefaultPieDataset oracleCacheDataset = new DefaultPieDataset();
        cacheInstanceDataset.put("zeus0", oracleCacheDataset);
        
        buildInstanceNameSymbol();
    }


    public Object produceDataset(Map map)
    {
        String datasetName = (String)map.get("dataset");
        String instanceName = (String) map.get("instance");
        
        logger.debug("getting dataset " + datasetName + " "  + instanceName);
        Object dataset  = null;
        Object instanceDataset = null;
        
        if (datasetName != null)
        {
            dataset  = datasetMap.get(datasetName);
            
            if (instanceName != null && dataset != null && dataset instanceof Map)
            {
                instanceDataset = ((Map)dataset).get(instanceName);
            }
            
            if (dataset != null && instanceDataset == null)
            {
                instanceDataset = dataset;     
            }
        }
        
        return instanceDataset;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "OracleSessionData DatasetProducer";
    }

    public void addSessionStat(long dataValue, String instance)
    {
        addCategoryStat(SESSIONS,dataValue,instance);
    }

    public void addSessionStat(double dataValue, String instance)
    {
        addCategoryStat(SESSIONS,dataValue,instance);
    }
    public void addGCRemasterStat(long dataValue, String instance)
    {
        addCategoryStat(GCREMASTER,dataValue,instance);
    }
    public void addLogfileResyncStat(long dataValue, String instance)
    {
        addCategoryStat(LOGFILERESYNC,dataValue,instance);
    }
    public void addInstanceCacheStat(String instance, long dataValue, String valueName)
    {
        addInstanceCategoryStat(instance, CACHE, dataValue, valueName);
    }
    public void addInstanceCacheStat(String instance, double dataValue, String valueName)
    {
        addInstanceCategoryStat(instance, CACHE, dataValue, valueName);
    }
    public void addInstancePhysicalReadsStat(String instance, long dataValue)
    {
        addInstanceValueStat(instance, PHYSICAL_READS, dataValue);
    }
    public void addInstanceLogicalReadsStat(String instance, long dataValue)
    {
        addInstanceValueStat(instance, LOGICAL_READS, dataValue);
    }
    public void addInstanceTransactionsStat(String instance, long dataValue)
    {
        addInstanceValueStat(instance, TRANSACTIONS, dataValue);
    }
    public void addInstanceUserCallsStat(String instance, long dataValue)
    {
        addInstanceValueStat(instance, USER_CALLS, dataValue);
    }
    public void addInstanceLoadStat(String instance, double dataValue)
    {
        addInstanceValueStat(instance, LOAD, dataValue);
    }
    public void addInstanceMemoryUsed(String instance, long dataValue)
    {
        addInstanceValueStat(instance, MEMORY_USED, dataValue);
    }
    
    public void addInstanceValueStat(String instance, String statCategory, double dataValue)
    {
        Map instanceDatasetMap = (Map)datasetMap.get(statCategory);
        if (instanceDatasetMap == null)
        {
            instanceDatasetMap = new HashMap<String,Object>();
            datasetMap.put(statCategory, instanceDatasetMap);
        }
        DefaultValueDataset pieDataset = (DefaultValueDataset) instanceDatasetMap.get(instance);
        if (pieDataset == null)
        {
            pieDataset = new DefaultValueDataset();
            instanceDatasetMap.put(instance, pieDataset);
        }
        
        pieDataset.setValue(dataValue);
    }
    public void addInstanceValueStat(String instance, String statCategory, long dataValue)
    {
        Map instanceDatasetMap = (Map)datasetMap.get(statCategory);
        if (instanceDatasetMap == null)
        {
            instanceDatasetMap = new HashMap<String,Object>();
            datasetMap.put(statCategory, instanceDatasetMap);
        }
        DefaultValueDataset pieDataset = (DefaultValueDataset) instanceDatasetMap.get(instance);
        if (pieDataset == null)
        {
            pieDataset = new DefaultValueDataset();
            instanceDatasetMap.put(instance, pieDataset);
        }
        
        pieDataset.setValue(dataValue);
    }
    
    public void addInstanceCategoryStat(String instance, String statCategory, long dataValue, String valueName)
    {
        Map instanceDatasetMap = (Map)datasetMap.get(statCategory);
        if (instanceDatasetMap == null)
        {
            instanceDatasetMap = new HashMap<String,Object>();
            datasetMap.put(statCategory, instanceDatasetMap);
        }
        DefaultCategoryDataset categoryDataset = (DefaultCategoryDataset) instanceDatasetMap.get(instance);
        if (categoryDataset == null)
        {
            categoryDataset = new DefaultCategoryDataset();
            instanceDatasetMap.put(instance, categoryDataset);
        }
        
        categoryDataset.addValue(dataValue,statCategory,valueName);
    }
    public void addInstanceCategoryStat(String instance, String statCategory, double dataValue, String valueName)
    {
        Map instanceDatasetMap = (Map)datasetMap.get(statCategory);
        if (instanceDatasetMap == null)
        {
            instanceDatasetMap = new HashMap<String,Object>();
            datasetMap.put(statCategory, instanceDatasetMap);
        }
        DefaultCategoryDataset categoryDataset = (DefaultCategoryDataset) instanceDatasetMap.get(instance);
        if (categoryDataset == null)
        {
            categoryDataset = new DefaultCategoryDataset();
            instanceDatasetMap.put(instance, categoryDataset);
        }
        
        categoryDataset.addValue(dataValue,statCategory,valueName);
    }
    
    public void addCategoryStat(String stat, long dataValue, String instance)
    {
        DefaultCategoryDataset dataset = (DefaultCategoryDataset)datasetMap.get(stat);
        String instanceSymbol = convertInstanceName(instance);
        dataset.addValue(dataValue, stat, instanceSymbol);
    }
    public void addCategoryStat(String stat, double dataValue, String instance)
    {
        DefaultCategoryDataset dataset = (DefaultCategoryDataset)datasetMap.get(stat);
        String instanceSymbol = convertInstanceName(instance);
        dataset.addValue(dataValue, stat, instanceSymbol);
    }
    

    public void setToolTipValueFormatter(NumberFormat nf)
    {
        this.toolTipNumberFormat = nf;
    }

    public String generateToolTip(CategoryDataset categoryDataset, int row, int column)
    {
        StringBuilder sb = new StringBuilder();

        Number value = categoryDataset.getValue(row, column);
        String queueName = (String)categoryDataset.getColumnKey(column);

        if (toolTipNumberFormat == null)
        {
            sb.append(defaultToolTipNumberFormat.format(value));
        } else
        {
            sb.append(toolTipNumberFormat.format(value));
        }
        sb.append(" in ");
        sb.append(queueName);

        return sb.toString();
    }

    private void buildInstanceNameSymbol()
    {
        instanceNameSymbolMap = new HashMap<String,String>();
        
        instanceNameSymbolMap.put("uranium","U");
        instanceNameSymbolMap.put("plutonium","Pu");
        instanceNameSymbolMap.put("tritium","H3");
        instanceNameSymbolMap.put("deuterium","H2");
        instanceNameSymbolMap.put("hydrogen","H");
        instanceNameSymbolMap.put("helium","He");
        instanceNameSymbolMap.put("carbon","C");
        instanceNameSymbolMap.put("silicon","Si");
    }
    
    private String convertInstanceName(String instanceName)
    {
        String instanceSymbol = instanceNameSymbolMap.get(instanceName);
        
        if (instanceSymbol == null)
        {
            instanceSymbol = instanceName;
        }
        
        return instanceSymbol;
    }
}
