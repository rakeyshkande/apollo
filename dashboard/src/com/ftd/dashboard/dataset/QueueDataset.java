package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.QueueStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;

import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class QueueDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.QueueDataset");

    private TimeSeriesCollection seriesCollection;

    private TimeSeries workSeries;
    private TimeSeries emailSeries;
    private TimeSeries otherSeries;
    private TimeSeries totalSeries;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");

    public QueueDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        workSeries = new TimeSeries("Work", Minute.class);
        emailSeries = new TimeSeries("Email", Minute.class);
        otherSeries = new TimeSeries("Other", Minute.class);
        totalSeries = new TimeSeries("Total", Minute.class);

        seriesCollection.addSeries(workSeries);
        seriesCollection.addSeries(emailSeries);
        seriesCollection.addSeries(otherSeries);
        seriesCollection.addSeries(totalSeries);
    }
    
    public TimeSeriesCollection getSeriesCollection(){
    	TimeSeriesCollection tableSeriesCollection = new TimeSeriesCollection();
    	tableSeriesCollection.addSeries(workSeries);
    	tableSeriesCollection.addSeries(emailSeries);
    	tableSeriesCollection.addSeries(otherSeries);
    	return tableSeriesCollection;
    }
    


    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(QueueStatsVO queueStatsVO, 
                        RegularTimePeriod timePeriod)
    {
        workSeries.add(timePeriod, queueStatsVO.getWorkQueueCount());
        emailSeries.add(timePeriod, queueStatsVO.getEmailQueueCount());
        otherSeries.add(timePeriod, queueStatsVO.getOtherQueueCount());
        totalSeries.add(timePeriod, queueStatsVO.getTotalQueueCount());
    }

    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection) data;
        TimeSeries timeSeries = collection.getSeries(series);
        
        StringBuilder sb = new StringBuilder();
        
        double xValue = data.getXValue(series,item);
        Number xTime = collection.getX(series,item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series,item);
        
        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValue);
        
        return sb.toString();
    }
}
