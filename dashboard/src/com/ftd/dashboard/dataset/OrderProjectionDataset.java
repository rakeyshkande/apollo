package com.ftd.dashboard.dataset;

import de.laures.cewolf.DatasetProducer;

import java.io.Serializable;

import java.util.Date;
import java.util.Map;

import org.jfree.data.general.DefaultValueDataset;

public class OrderProjectionDataset implements DatasetProducer, Serializable
{
    DefaultValueDataset orderProjectionDataset;
    
    
    public Object produceDataset(Map map)
    {
        return orderProjectionDataset;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "OrderProjectionDataset DatasetProducer";
    }
    
    public void setValue(double value)
    {
        orderProjectionDataset = new DefaultValueDataset(value);
    }
}
