package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.AuthorizationStatsVO;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

import java.io.Serializable;

import java.util.Date;
import java.util.Map;

import org.jfree.data.category.DefaultCategoryDataset;


public class AuthorizationDataset implements DatasetProducer, Serializable, CategoryToolTipGenerator
{
    private static final String APPROVALS = "Approvals";
    private static final String DECLINES  = "Declines";
    
    private DefaultCategoryDataset authorizationDataset;
    
    public AuthorizationDataset()
    {
        authorizationDataset = new DefaultCategoryDataset();
    }

    public void addStat(AuthorizationStatsVO vo)
    {
        authorizationDataset.addValue(vo.getApprovals(), APPROVALS,vo.getCardType());
        authorizationDataset.addValue(-1 * vo.getDeclines(), DECLINES,vo.getCardType());
    }
    
    public Object produceDataset(Map map)
    {
        return authorizationDataset;
    }

    public boolean hasExpired(Map map, Date date)
    {
        return false;
    }

    public String getProducerId()
    {
        return null;
    }

    public String generateToolTip(org.jfree.data.category.CategoryDataset categoryDataset, int i, int i1)
    {
        return "blah";
    }
}
