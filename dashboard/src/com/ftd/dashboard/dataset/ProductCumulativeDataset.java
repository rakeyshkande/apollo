package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.dashboard.vo.ProductStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ProductCumulativeDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.ProductCumulativeDataset");

    private TimeSeriesCollection seriesCollection;

    private Map<String,TimeSeries> productSeriesMap;
    private Map<String,Long> productCumulativeValueMap;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");
    private NumberFormat nf = new DecimalFormat("#0");

    public ProductCumulativeDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        productSeriesMap = new HashMap<String,TimeSeries>();
        productCumulativeValueMap = new HashMap<String,Long>();
    }


    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(ProductStatsVO productStatVO, RegularTimePeriod timePeriod)
    {
        TimeSeries productTimeSeries = productSeriesMap.get(productStatVO.getProductId());
        
        if (productTimeSeries == null)
        {
            productTimeSeries = new TimeSeries(productStatVO.getProductId(), Minute.class);
            productSeriesMap.put(productStatVO.getProductId(), productTimeSeries);
            seriesCollection.addSeries(productTimeSeries);
        }
        Long productCumulativeValue = productCumulativeValueMap.get(productStatVO.getProductId());
        
        if (productCumulativeValue == null)
        {
            productCumulativeValue = new Long(0L);
        }
        
        long seriesValue = productCumulativeValue.longValue() + productStatVO.getOrderCount();
        productCumulativeValueMap.put(productStatVO.getProductId(), seriesValue);

        productTimeSeries.add(timePeriod, seriesValue);
    }

    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection)data;
        TimeSeries timeSeries = collection.getSeries(series);

        StringBuilder sb = new StringBuilder();

        double xValue = data.getXValue(series, item);
        Number xTime = collection.getX(series, item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series, item);
        String yValueString = nf.format(yValue);

        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValueString);

        return sb.toString();
    }
}
