package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.OrderServiceStatsVO;
import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.osp.utilities.plugins.Logger;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.XYToolTipGenerator;
import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class OrderServiceDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.OrderServiceDataset");

    private TimeSeriesCollection seriesCollection;

    private TimeSeries orderServiceCountSeries;

    private Map<String, TimeSeries> orderServiceCountSeriesMap;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");
    private NumberFormat nf = new DecimalFormat("#0.00");

    public OrderServiceDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        orderServiceCountSeries = new TimeSeries("Total", Minute.class);

        seriesCollection.addSeries(orderServiceCountSeries);

        orderServiceCountSeriesMap = new HashMap<String,TimeSeries>();
    }


    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "OrderServiceCountData DatasetProducer";
    }

    public void addStat(OrderServiceStatsVO orderServiceStatsVO,
                        RegularTimePeriod timePeriod, String groupBy)
    {
        orderServiceCountSeries.addOrUpdate(timePeriod, orderServiceStatsVO.getCount());

        TimeSeries series;
        if (orderServiceCountSeriesMap.containsKey(groupBy))
        {
            series = orderServiceCountSeriesMap.get(groupBy);
        }
        else
        {
            series = new TimeSeries(groupBy, Minute.class);
            orderServiceCountSeriesMap.put(groupBy, series);
            seriesCollection.addSeries(series);

        }
        series.addOrUpdate(timePeriod, orderServiceStatsVO.getCount());
    }
    
    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection) data;
        TimeSeries timeSeries = collection.getSeries(series);
        
        StringBuilder sb = new StringBuilder();
        
        double xValue = data.getXValue(series,item);
        Number xTime = collection.getX(series,item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series,item);
        String yValueString = nf.format(yValue);
        
        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValueString);
        
        return sb.toString();
    }
}
