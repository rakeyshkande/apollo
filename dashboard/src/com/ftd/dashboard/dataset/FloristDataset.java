package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.dashboard.vo.ZipStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;


public class FloristDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger  = new Logger("com.ftd.dashboard.dataset.FloristDataset");

    private static final String FLORIST_DETAIL_LINK = "dashboard.do";
    
    private final String[] seriesNames =    {"goto florists"};
    
    private TimeSeriesCollection seriesCollection;
    
    private TimeSeries gotoFloristSeries;
    private TimeSeries gotoFloristSuspendSeries;
    private TimeSeries gotoFloristShutdownSeries;
    private TimeSeries mercuryShopSeries;
    private TimeSeries mercuryShopShutdownSeries;
    private TimeSeries coveredZipSeries;
    private TimeSeries gnaddZipSeries;
    
    // Percent Series
    private TimeSeries gotoFloristShutdownPercentSeries;
    private TimeSeries gotoFloristSuspendPercentSeries;
    private TimeSeries mercuryShopShutdownPercentSeries;
    private TimeSeries gnaddZipPercentSeries;
    
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");
    private NumberFormat nf = new DecimalFormat("#0.00");

    public FloristDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        gotoFloristSeries = new TimeSeries("Goto Florist Count", Minute.class);
        gotoFloristSuspendSeries = new TimeSeries("Goto Florist Suspend", Minute.class);
        gotoFloristShutdownSeries = new TimeSeries("Goto Florist Shutdown", Minute.class);
        mercuryShopSeries = new TimeSeries("Florist", Minute.class);
        mercuryShopShutdownSeries = new TimeSeries("Florist Shutdown", Minute.class);
        coveredZipSeries = new TimeSeries("Covered Zip", Minute.class);
        gnaddZipSeries = new TimeSeries("GNADD Zip", Minute.class);
        
        gotoFloristSuspendPercentSeries = new TimeSeries("% Goto Florist Suspend", Minute.class);
        gotoFloristShutdownPercentSeries = new TimeSeries("% Goto Florist Shutdown", Minute.class);
        mercuryShopShutdownPercentSeries = new TimeSeries("% Florist Shutdown", Minute.class);
        gnaddZipPercentSeries = new TimeSeries("% GNADD Zip", Minute.class);

        //seriesCollection.addSeries(gotoFloristSeries);
        //seriesCollection.addSeries(gotoFloristSuspendSeries);
        //seriesCollection.addSeries(gotoFloristShutdownSeries);
        //seriesCollection.addSeries(mercuryShopSeries);
        //seriesCollection.addSeries(mercuryShopShutdownSeries);
        //seriesCollection.addSeries(coveredZipSeries);
        //seriesCollection.addSeries(gnaddZipSeries);
        //seriesCollection.addSeries(gotoFloristSuspendPercentSeries);
        
        seriesCollection.addSeries(gotoFloristShutdownPercentSeries);
        seriesCollection.addSeries(mercuryShopShutdownPercentSeries);
        seriesCollection.addSeries(gnaddZipPercentSeries);

    }

    public TimeSeriesCollection getSeriesCollection(){
    	return seriesCollection;
    }

    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime())  > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(FloristStatsVO floristStatsVO, RegularTimePeriod timePeriod)
    {
        gotoFloristSeries.add(timePeriod,floristStatsVO.getGotoFloristCount());
        gotoFloristSuspendSeries.add(timePeriod,floristStatsVO.getGotoFloristSuspendCount());
        gotoFloristShutdownSeries.add(timePeriod,floristStatsVO.getGotoFloristShutdownCount());
        mercuryShopSeries.add(timePeriod,floristStatsVO.getMercuryShopCount());
        mercuryShopShutdownSeries.add(timePeriod,floristStatsVO.getMercuryShopShutdownCount());
        
        gotoFloristSuspendPercentSeries.add(timePeriod,floristStatsVO.getPercentGotoFloristSuspend());
        gotoFloristShutdownPercentSeries.add(timePeriod,floristStatsVO.getPercentGotoFloristShutdown());
        mercuryShopShutdownPercentSeries.add(timePeriod,floristStatsVO.getPercentMercuryShopSuspend());
        
    }
    
    public void addZipStat(ZipStatsVO zipStatsVO, RegularTimePeriod timePeriod)
    {
        coveredZipSeries.add(timePeriod,zipStatsVO.getZipCoveredCount());
        gnaddZipSeries.add(timePeriod,zipStatsVO.getZipGnaddCount());
        double percentGZ = (double)(100 * zipStatsVO.getZipGnaddCount()) / (double)zipStatsVO.getZipCoveredCount();
        gnaddZipPercentSeries.add(timePeriod,percentGZ);
        
    }


    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection) data;
        TimeSeries timeSeries = collection.getSeries(series);
        
        StringBuilder sb = new StringBuilder();
        
        double xValue = data.getXValue(series,item);
        Number xTime = collection.getX(series,item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series,item);
        String yValueString = nf.format(yValue);
        
        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValueString);
        
        return sb.toString();
    }
}
