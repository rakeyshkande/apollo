package com.ftd.dashboard.dataset;

import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.osp.utilities.plugins.Logger;

import de.laures.cewolf.DatasetProducer;

import de.laures.cewolf.tooltips.XYToolTipGenerator;

import java.io.Serializable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;


public class OrderOriginDataset implements DatasetProducer, Serializable, XYToolTipGenerator
{
    private static Logger logger = new Logger("com.ftd.dashboard.dataset.OrderOriginDataset");

    private TimeSeriesCollection seriesCollection;

    private TimeSeries orderCountSeries;
    private TimeSeries internetOrderCountSeries;
    private TimeSeries phoneOrderCountSeries;
    private TimeSeries forecastOrderCountSeries;

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd:HHmm");
    private NumberFormat nf = new DecimalFormat("#0.00");

    public OrderOriginDataset()
    {
        seriesCollection = new TimeSeriesCollection();

        orderCountSeries = new TimeSeries("Total", Minute.class);
        internetOrderCountSeries = new TimeSeries("Web", Minute.class);
        phoneOrderCountSeries = new TimeSeries("Phone", Minute.class);
        forecastOrderCountSeries = new TimeSeries("Forecast", Minute.class);

        seriesCollection.addSeries(orderCountSeries);
        seriesCollection.addSeries(internetOrderCountSeries);
        seriesCollection.addSeries(phoneOrderCountSeries);
        seriesCollection.addSeries(forecastOrderCountSeries);
    }


    public TimeSeriesCollection getSeriesCollection(){
    	TimeSeriesCollection tableSeriesCollection = new TimeSeriesCollection();
    	tableSeriesCollection.addSeries(internetOrderCountSeries);
    	tableSeriesCollection.addSeries(phoneOrderCountSeries);
    	return tableSeriesCollection;
    }
    
    public Object produceDataset(Map map)
    {
        return seriesCollection;
    }

    public boolean hasExpired(Map map, Date since)
    {
        return (System.currentTimeMillis() - since.getTime()) > 5000;
    }

    public String getProducerId()
    {
        return "PageViewCountData DatasetProducer";
    }

    public void addStat(OrderStatsVO orderStatsVO, 
                        RegularTimePeriod timePeriod)
    {
        orderCountSeries.add(timePeriod, orderStatsVO.getTotalOrderCount());
        internetOrderCountSeries.add(timePeriod, orderStatsVO.getInternetOrderCount());
        phoneOrderCountSeries.add(timePeriod, orderStatsVO.getPhoneOrderCount());
        forecastOrderCountSeries.add(timePeriod, orderStatsVO.getForecastOrderCount());
    }
    
    public String generateToolTip(XYDataset data, int series, int item)
    {
        TimeSeriesCollection collection = (TimeSeriesCollection) data;
        TimeSeries timeSeries = collection.getSeries(series);
        
        StringBuilder sb = new StringBuilder();
        
        double xValue = data.getXValue(series,item);
        Number xTime = collection.getX(series,item);
        long milliseconds = xTime.longValue();
        Date xDate = new Date(milliseconds);
        String xDateString = sdf.format(xDate);
        double yValue = collection.getYValue(series,item);
        String yValueString = nf.format(yValue);
        
        sb.append(xDateString);
        sb.append("\n");
        sb.append(timeSeries.getKey());
        sb.append(" : ");
        sb.append(yValueString);
        
        return sb.toString();
    }
}
