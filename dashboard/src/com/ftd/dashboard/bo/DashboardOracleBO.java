package com.ftd.dashboard.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.ftd.dashboard.dao.OracleDashboardDAO;
import com.ftd.dashboard.dataset.OracleClusterDataset;
import com.ftd.dashboard.dataset.OracleInstanceDataset;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.OracleInstanceStatsVO;
import com.ftd.dashboard.vo.OracleInstanceStatusVO;
import com.ftd.dashboard.vo.OracleStatsVO;
import com.ftd.dashboard.vo.OracleStatusVO;
import com.ftd.dashboard.vo.ThresholdVO;
import com.ftd.dashboard.web.form.OracleDashboardForm;
import com.ftd.dashboard.web.util.OracleInstanceBarChartPostProcessor;
import com.ftd.dashboard.web.util.OracleMeterChartPostProcessor;
import com.ftd.dashboard.web.util.OracleThermometerPostProcessor;
import com.ftd.osp.utilities.plugins.Logger;

public class DashboardOracleBO extends BaseDashboardBO
{
    private static Logger logger = new Logger("com.ftd.dashboard.bo.DashboardOracleBO");
    
    private Map<String,ThresholdVO> thresholdMap = new HashMap<String,ThresholdVO>();

    public DashboardOracleBO()
    {
    }


    public void populateOracleDetail(Connection con, OracleDashboardForm dashboardForm) throws Exception
    {
        logger.debug("Start");
        
        OracleDashboardDAO dao = new OracleDashboardDAO();
        
        logger.debug("Getting oracle stats from dao");
        OracleStatsVO oracleStatsVO = dao.getAllOracleStats(con);
        logger.debug("Done Getting oracle stats from dao");
        
        // Set the Locks
        OracleClusterDataset oracleLocksDataset = dashboardForm.getOracleLocksDataset();
        oracleLocksDataset.setValue(oracleStatsVO.getLockCount());
        OracleClusterDataset oracleLocksAgedDataset = dashboardForm.getOracleLocksAgedDataset();
        oracleLocksAgedDataset.setValue(oracleStatsVO.getOldLockCount());
        
        // Set the Data stats for the cluster
        OracleClusterDataset oracleDataDiskgroupDataset = dashboardForm.getOracleDataDiskgroupDataset();
        oracleDataDiskgroupDataset.setValue(oracleStatsVO.getDataDiskgroupFreeSpace());
        OracleClusterDataset oracleFlashDiskgroupDataset = dashboardForm.getOracleFlashDiskgroupDataset();
        oracleFlashDiskgroupDataset.setValue(oracleStatsVO.getFlashDiskgroupFreeSpace());
        OracleClusterDataset oracleTablespaceFreeDataset = dashboardForm.getOracleTablespaceFreeDataset();
        oracleTablespaceFreeDataset.setValue(oracleStatsVO.getSmallestTablespacePctFree());
        
        // Set the session stats
        OracleInstanceDataset oracleInstanceDataset = dashboardForm.getOracleInstanceDataset();
        Collection<OracleInstanceStatsVO> instanceCollection = oracleStatsVO.getInstanceCollection();
        Iterator<OracleInstanceStatsVO> instanceIterator = instanceCollection.iterator();
        List<String> instances = new ArrayList<String>();
        dashboardForm.setInstances(instances);
        while (instanceIterator.hasNext())
        {
            OracleInstanceStatsVO instanceVO = instanceIterator.next();
            logger.debug("Adding instance stats for " + instanceVO.getHostName());
            instances.add(instanceVO.getHostName());
            oracleInstanceDataset.addSessionStat(instanceVO.getSessions(), instanceVO.getHostName()); 
            oracleInstanceDataset.addGCRemasterStat(instanceVO.getGcremaster(), instanceVO.getHostName()); 
            oracleInstanceDataset.addLogfileResyncStat(instanceVO.getLogfileresync(), instanceVO.getHostName()); 
            
            oracleInstanceDataset.addInstanceTransactionsStat(instanceVO.getHostName(),instanceVO.getTransactions());
            oracleInstanceDataset.addInstanceUserCallsStat(instanceVO.getHostName(), instanceVO.getUserCalls());
            oracleInstanceDataset.addInstanceMemoryUsed(instanceVO.getHostName(), instanceVO.getMemoryUsed());
            
            
            // Add Read Stats
            oracleInstanceDataset.addInstanceCacheStat(instanceVO.getHostName(), 
                                                       instanceVO.getGlobalCacheReadPercent(),
                                                       OracleInstanceDataset.GLOBAL_PERCENT);
            oracleInstanceDataset.addInstanceCacheStat(instanceVO.getHostName(), 
                                                       instanceVO.getNoCacheReadPercent(),
                                                       OracleInstanceDataset.DISK_PERCENT);
            oracleInstanceDataset.addInstanceCacheStat(instanceVO.getHostName(), 
                                                       instanceVO.getLocalCacheReadPercent(),
                                                       OracleInstanceDataset.LOCAL_PERCENT);
            oracleInstanceDataset.addInstancePhysicalReadsStat(instanceVO.getHostName(), instanceVO.getPhysicalReads());
            oracleInstanceDataset.addInstanceLogicalReadsStat(instanceVO.getHostName(), instanceVO.getLogicalReads());
            oracleInstanceDataset.addInstanceLoadStat(instanceVO.getHostName(), instanceVO.getLoad());
        }
        
        this.thresholdMap = getOracleThresholds();
        
        OracleStatusVO oracleStatusVO = getOracleStatus(oracleStatsVO);
        dashboardForm.setOracleStatusVO(oracleStatusVO); 

        setDisplayProperties(dashboardForm);
    }
    
    public OracleThermometerPostProcessor createOracleThermometerPostProcessor()
    {
        OracleThermometerPostProcessor postProcessor = new OracleThermometerPostProcessor();;
        
        postProcessor.setThresholdMap(thresholdMap);
        
        return postProcessor;
    }
    
    public OracleMeterChartPostProcessor createOracleMeterPostProcessor()
    {
        OracleMeterChartPostProcessor postProcessor = new OracleMeterChartPostProcessor();;
        
        postProcessor.setThresholdMap(thresholdMap);
        
        return postProcessor;
    }
    
    public OracleInstanceBarChartPostProcessor createOracleBarChartPostProcessor()
    {
        OracleInstanceBarChartPostProcessor postProcessor = new OracleInstanceBarChartPostProcessor();;
        
        postProcessor.setThresholdMap(thresholdMap);
        
        return postProcessor;
    }
    
    private Map<String,ThresholdVO> getOracleThresholds() 
    {
        Map<String,ThresholdVO> thresholdMap = new HashMap<String,ThresholdVO>();
        try
        {
            thresholdMap.put(DashboardConstants.LOCKS, getOracleSetup(DashboardConstants.LOCKS));
            thresholdMap.put(DashboardConstants.AGED_LOCKS, getOracleSetup(DashboardConstants.AGED_LOCKS));
            thresholdMap.put(DashboardConstants.PHYSICAL_READS, getOracleSetup(DashboardConstants.PHYSICAL_READS));
            thresholdMap.put(DashboardConstants.LOGICAL_READS, getOracleSetup(DashboardConstants.LOGICAL_READS));
            thresholdMap.put(DashboardConstants.TRANSACTIONS, getOracleSetup(DashboardConstants.TRANSACTIONS));
            thresholdMap.put(DashboardConstants.USER_CALLS, getOracleSetup(DashboardConstants.USER_CALLS));
            thresholdMap.put(DashboardConstants.LOAD, getOracleSetup(DashboardConstants.LOAD));
            thresholdMap.put(DashboardConstants.MEMORY_USED, getOracleSetup(DashboardConstants.MEMORY_USED));
            thresholdMap.put(DashboardConstants.GC_REMASTER_WAIT, getOracleSetup(DashboardConstants.GC_REMASTER_WAIT));
            thresholdMap.put(DashboardConstants.DATA_DISKGROUP, getOracleSetup(DashboardConstants.DATA_DISKGROUP));
            thresholdMap.put(DashboardConstants.FLASH_DISKGROUP, getOracleSetup(DashboardConstants.FLASH_DISKGROUP));
            thresholdMap.put(DashboardConstants.TABLESPACE_FREE, getOracleSetup(DashboardConstants.TABLESPACE_FREE));
            
            // Instance Category graph
            thresholdMap.put(OracleInstanceDataset.LOCAL_PERCENT, getOracleSetup(DashboardConstants.CACHE_LOCAL));
            thresholdMap.put(OracleInstanceDataset.GLOBAL_PERCENT, getOracleSetup(DashboardConstants.CACHE_GLOBAL));
            thresholdMap.put(OracleInstanceDataset.DISK_PERCENT, getOracleSetup(DashboardConstants.CACHE_DISK));
            
            // Cluster Category graph, instance category
            thresholdMap.put(DashboardConstants.SESSIONS, getOracleSetup(DashboardConstants.SESSIONS));
            thresholdMap.put(DashboardConstants.LOG_FILE_SYNC, getOracleSetup(DashboardConstants.LOG_FILE_SYNC));
        }
        catch (Exception e)
        {
           logger.error("Error getting Thresholds",e); 
        }
        
        return thresholdMap;
        
    }
    
    private ThresholdVO getOracleSetup(String statName) throws Exception
    {
        String queueParmName = "ORACLE_" + statName;
        String encodedParm = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,queueParmName);        
        
        ThresholdVO thresholdVO = null;
        if (encodedParm != null)
        {
            thresholdVO = new ThresholdVO();
            logger.debug("Got Encoded String of " + encodedParm);
            StringTokenizer tokenizer = new StringTokenizer(encodedParm,";");
            if (tokenizer.countTokens() != 4)
            {
                logger.info("Invalid number of tokens " + tokenizer.countTokens());
            }
            
            thresholdVO.setName(tokenizer.nextToken());
            thresholdVO.setWarningThreshold(Integer.parseInt(tokenizer.nextToken()));
            thresholdVO.setCriticalThreshold(Integer.parseInt(tokenizer.nextToken()));
            thresholdVO.setMaxDisplayValue(Integer.parseInt(tokenizer.nextToken()));
        }
        else
        {
            logger.debug("No parm for " + statName);
        }
        
        return thresholdVO;
    }

    private OracleStatusVO getOracleStatus(OracleStatsVO oracleStatsVO)
    {
        OracleStatusVO statusVO = new OracleStatusVO();
        
        // Check the thresholds for the oracle stats
        int threshold = DashboardConstants.NORMAL_THRESHOLD;
        
        threshold = calcThreshold(oracleStatsVO.getLockCount(), DashboardConstants.LOCKS,threshold);
        threshold = calcThreshold(oracleStatsVO.getOldLockCount(), DashboardConstants.AGED_LOCKS,threshold);
        statusVO.setOverallStatus(threshold);
        
        // Storage status
        int storageThreshold = DashboardConstants.NORMAL_THRESHOLD;
        threshold = calcThreshold(oracleStatsVO.getDataDiskgroupFreeSpace(), DashboardConstants.DATA_DISKGROUP,threshold);
        threshold = calcThreshold(oracleStatsVO.getFlashDiskgroupFreeSpace(), DashboardConstants.FLASH_DISKGROUP,threshold);
        threshold = calcThreshold(oracleStatsVO.getSmallestTablespacePctFree(), DashboardConstants.TABLESPACE_FREE,threshold);
        statusVO.setStorageStatus(storageThreshold);
       
        Collection<OracleInstanceStatsVO> oracleInstanceStatsCollection = oracleStatsVO.getInstanceCollection();
        Iterator<OracleInstanceStatsVO> oracleInstanceStatsIterator = oracleInstanceStatsCollection.iterator();
        HashMap<String,OracleInstanceStatusVO> instanceStatusMap = new HashMap<String,OracleInstanceStatusVO>();
        statusVO.setOracleInstanceStatusMap(instanceStatusMap);
        while (oracleInstanceStatsIterator.hasNext())
        {
            
            OracleInstanceStatsVO instanceStatsVO = oracleInstanceStatsIterator.next();
            OracleInstanceStatusVO instanceStatusVO = new OracleInstanceStatusVO();
            
            // Check the thresholds
            int instanceThreshold = DashboardConstants.NORMAL_THRESHOLD;
            instanceThreshold = calcThreshold(instanceStatsVO.getPhysicalReads(), DashboardConstants.PHYSICAL_READS,instanceThreshold);
            instanceThreshold = calcThreshold(instanceStatsVO.getLogicalReads(), DashboardConstants.LOGICAL_READS,instanceThreshold);
            instanceThreshold = calcThreshold(instanceStatsVO.getTransactions(), DashboardConstants.TRANSACTIONS,instanceThreshold);
            instanceThreshold = calcThreshold(instanceStatsVO.getUserCalls(), DashboardConstants.USER_CALLS,instanceThreshold);
            instanceThreshold = calcThreshold(instanceStatsVO.getLoad(), DashboardConstants.LOAD,instanceThreshold);
            instanceThreshold = calcThreshold(instanceStatsVO.getMemoryUsed(), DashboardConstants.MEMORY_USED,instanceThreshold);
            instanceStatusVO.setOverallStatus(instanceThreshold);
            
            instanceStatusMap.put(instanceStatsVO.getHostName(), instanceStatusVO); 
        }
       
        
        return statusVO;
    }
    
    private int calcThreshold(double value, String thresholdName, int currentThreshold)
    {
        return calcThreshold((long)value,thresholdName,currentThreshold);     
    }
    
    private int calcThreshold(long value, String thresholdName, int currentThreshold)
    {
        ThresholdVO thresholdVO = this.thresholdMap.get(thresholdName);
        if (thresholdVO != null)
        {
            int statThreshold = thresholdVO.getCurrentThreshold(value);
            if (statThreshold > currentThreshold)
            {
                currentThreshold = statThreshold;
            }
        }
        
        return currentThreshold;
    }
    
    private void setDisplayProperties(OracleDashboardForm oracleDashboardForm) throws Exception
    {
        oracleDashboardForm.setSmallGraphSize(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SMALL_GRAPH_SIZE));
        oracleDashboardForm.setLargeGraphSize(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.LARGE_GRAPH_SIZE));
        
        oracleDashboardForm.setShowMemoryGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_MEMORY_GRAPH));
        oracleDashboardForm.setShowUserCallsGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_USER_CALLS_GRAPH));
        oracleDashboardForm.setShowTransactionsGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_TRANSACTIONS_GRAPH));
        oracleDashboardForm.setShowLogicalReadsGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_LOGICAL_READS_GRAPH));
        oracleDashboardForm.setShowPhysicalReadsGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_PHYSICAL_READS_GRAPH));
        oracleDashboardForm.setShowLoadGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_LOAD_GRAPH));
        oracleDashboardForm.setShowDataDiskgroupGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_DATA_DISKGROUP_GRAPH));
        oracleDashboardForm.setShowFlashDiskgroupGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_FLASH_DISKGROUP_GRAPH));
        oracleDashboardForm.setShowTablespaceFreeGraph(getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,
                DashboardConstants.SHOW_TABLESPACE_FREE_GRAPH));
    }
}
