package com.ftd.dashboard.bo;

import com.ftd.dashboard.dao.DashboardDAO;
import com.ftd.dashboard.dataset.AuthorizationDataset;
import com.ftd.dashboard.dataset.FloristDataset;
import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.dataset.OrderDeliveryDataset;
import com.ftd.dashboard.dataset.OrderOriginDataset;
import com.ftd.dashboard.dataset.OrderProjectionDataset;
import com.ftd.dashboard.dataset.OrderServiceDataset;
import com.ftd.dashboard.dataset.PaymentDataset;
import com.ftd.dashboard.dataset.ProductCumulativeDataset;
import com.ftd.dashboard.dataset.ProductRateDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.dataset.QueueDetailDataset;
import com.ftd.dashboard.tables.BaseTableVO;
import com.ftd.dashboard.tables.InventoryTableVO;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.AuthorizationCombinedStatsVO;
import com.ftd.dashboard.vo.AuthorizationStatsVO;
import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.dashboard.vo.OrderServiceStatsVO;
import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.dashboard.vo.PaymentCombinedStatsVO;
import com.ftd.dashboard.vo.PaymentStatsVO;
import com.ftd.dashboard.vo.ProductStatsVO;
import com.ftd.dashboard.vo.QueueDetailStatsVO;
import com.ftd.dashboard.vo.QueueStatsVO;
import com.ftd.dashboard.vo.ZipStatsVO;
import com.ftd.dashboard.web.form.AuthorizationStatsPostForm;
import com.ftd.dashboard.web.form.DashboardForm;
import com.ftd.dashboard.web.form.DateRangeDashboardForm;
import com.ftd.dashboard.web.form.FloristDashboardForm;
import com.ftd.dashboard.web.form.InventoryDashboardForm;
import com.ftd.dashboard.web.form.OrderDashboardForm;
import com.ftd.dashboard.web.form.OrderServiceDashboardForm;
import com.ftd.dashboard.web.form.OrderServiceStatsPostForm;
import com.ftd.dashboard.web.form.PaymentDashboardForm;
import com.ftd.dashboard.web.form.QueueDashboardForm;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import com.ftd.dashboard.tables.PercentageRoundVO;


public class DashboardBO extends BaseDashboardBO
{
    private static Logger logger  = new Logger("com.ftd.dashboard.bo.DashboardBO");

    private static long DEFAULT_MAX_SECONDS_BACK = 86400L;
    private static int  DEFAULT_MAX_PRODUCTS = 10;
    private static int  DEFAULT_FLORIST_DETAILS_HOURS_BACK = 3;
    private static long ONE_HOUR_MILLISECONDS = 3600000L;
    
    private static final String MAX_SECONDS_BACK = "MAX_SECONDS_BACK";
    private static final String FLORIST_DETAILS_HOURS_BACK = "FLORIST_DETAILS_HOURS_BACK";
    private static final String QUEUE_INCLUDE_LIST = "QUEUE_INCLUDE_LIST";

    private long maxSecondsBack = DEFAULT_MAX_SECONDS_BACK;
    
    private int floristDetailsHoursBack = 0;
    
    private SimpleDateFormat sdfmmddyyyyhhmm;
    private SimpleDateFormat sdfhhmm;
    private SimpleDateFormat sdfmmddyyyy;
    
    public DashboardBO()
    {
        String maxSecondsBackString = "";
        try
        {
            maxSecondsBackString = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,MAX_SECONDS_BACK);
            maxSecondsBack = Long.valueOf(maxSecondsBackString);
            
            logger.debug("maxSecondsBackString = " + maxSecondsBackString);
        }
        catch (Exception nfe)
        {
            logger.error("Number format Exception " + maxSecondsBackString,nfe);
            maxSecondsBack = DEFAULT_MAX_SECONDS_BACK;
        }
    }

    public void populateSummaryData(Connection con, DashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();
        
        List<OrderStatsVO> orderStats = dao.getOrderStatsCumulative(con, maxSecondsBack);
        List<FloristStatsVO> floristSummaryStats = dao.getFloristStatsSummary(con, maxSecondsBack);
        List<QueueStatsVO> queueSummaryStats = dao.getQueueStatsSummary(con, maxSecondsBack);
        List<ZipStatsVO> zipStats = dao.getZipStatsSummary(con, maxSecondsBack);
        
        FloristDataset floristDataset = new FloristDataset();
        QueueDataset queueDataset = new QueueDataset();
        
        addFloristStats(floristDataset,floristSummaryStats, dashboardForm);
        addQueueStats(queueDataset,queueSummaryStats, dashboardForm);
        addZipStats(floristDataset,zipStats, dashboardForm);
        addOrderStats(orderStats, dashboardForm);
        
        //populate all tables data.
        populateFloristTableData(con,dashboardForm);
        populateOrderTableData(con,dashboardForm);
        populateQueueTableData(con,dashboardForm);
        populateInventoryTableData(con,dashboardForm);
        DashboardJMSBO dashboardJMSBO = new DashboardJMSBO();        
        dashboardJMSBO.populateJMSDetail(con, dashboardForm.getJmsDashboardForm());
        
        if(dashboardForm.getLeftRangeSelect()==0)
        {	
        	calculateCustomDates(dashboardForm.getPaymentDashboardForm(), dashboardForm);
        }
        else
        {
        	calculateDates(dashboardForm.getPaymentDashboardForm());
        }
        populatePaymentDetail(con, dashboardForm.getPaymentDashboardForm(), 0);
    }
    
    public void populateInventoryDetail(Connection con, InventoryDashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();

        Map<String,List<ProductStatsVO>> productStatsMap = dao.getProductStatsDetail(con, dashboardForm.getNumberProductsToShow(),maxSecondsBack);
        
        ProductCumulativeDataset floristProductCumulativeDataset = new ProductCumulativeDataset();
        ProductRateDataset floristProductRateDataset = new ProductRateDataset();
        
        List<ProductStatsVO> floristProductStats = productStatsMap.get("Florist");
        for (int i = 0; i < floristProductStats.size(); i++)
        {
            ProductStatsVO productStatsVO = floristProductStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,productStatsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);
            
            floristProductCumulativeDataset.addStat(productStatsVO, timePeriod);
            floristProductRateDataset.addStat(productStatsVO, timePeriod);
        }
        
        ProductCumulativeDataset vendorProductCumulativeDataset = new ProductCumulativeDataset();
        ProductRateDataset vendorProductRateDataset = new ProductRateDataset();
        
        List<ProductStatsVO> vendorProductStats = productStatsMap.get("Vendor");
        for (int i = 0; i < vendorProductStats.size(); i++)
        {
            ProductStatsVO productStatsVO = vendorProductStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,productStatsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);
            
            vendorProductCumulativeDataset.addStat(productStatsVO, timePeriod);
            vendorProductRateDataset.addStat(productStatsVO, timePeriod);
        }

        dashboardForm.setFloristCumulativeXYData(floristProductCumulativeDataset);
        dashboardForm.setFloristRateXYData(floristProductRateDataset);
        dashboardForm.setVendorCumulativeXYData(vendorProductCumulativeDataset);
        dashboardForm.setVendorRateXYData(vendorProductRateDataset);
    }

    public void populateQueueDetail(Connection con, QueueDashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();
        
        // Get the list of Queues to include
        String queueList = "";
        try
        {
            queueList = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,QUEUE_INCLUDE_LIST);
            logger.debug("queueList = " + queueList);
        }
        catch (Exception e)
        {
            logger.error("Error getting the queue include list from global parms",e);
        }
        
        
        List<QueueDetailStatsVO> queueDetailStats = dao.getQueueStatsDetail(con, maxSecondsBack, queueList);
        
        QueueDetailDataset queueDetailDataset = new QueueDetailDataset();
        
        addQueueDetailStats(queueDetailDataset,queueDetailStats, dashboardForm);
        DashboardForm mainDashboardForm = new DashboardForm();
        populateQueueTableData(con, mainDashboardForm);
        dashboardForm.setSetupVO(mainDashboardForm.getQueueDashboardForm().getSetupVO());
        
    }
    
    public void populateFloristDetail(Connection con, FloristDashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();
        FloristDetailDataset floristGotoBarData = new FloristDetailDataset(getFloristDetailsHoursBack());
        FloristDetailDataset floristBarData = new FloristDetailDataset(getFloristDetailsHoursBack());
        floristBarData.setToolTipValueFormatter(new DecimalFormat("##0.00'%'"));

        dashboardForm.setFloristGotoBarData(floristGotoBarData);
        dashboardForm.setFloristBarData(floristBarData);

        
        // Loop is backwards to sort it by time properly
        for (int i= getFloristDetailsHoursBack() - 1; i >= 0; i --)
        {
            List<FloristStatsVO> floristDetailList = dao.getFloristStatsDetail(con, i);
            addFloristDetailStats(floristGotoBarData,floristBarData,floristDetailList, i);
        }
    }

    public void populateOrderDetail(Connection con, OrderDashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();
        
        List<OrderStatsVO> orderDetailStats = dao.getOrderStatsCumulative(con, maxSecondsBack);
        
        addOrderOriginStats(orderDetailStats, dashboardForm);
        DashboardForm mainDashboardForm = new DashboardForm();
        populateOrderTableData(con, mainDashboardForm);
        dashboardForm .setSetupVO(mainDashboardForm.getOrderDashboardForm().getSetupVO());
        
    }

    public void populatePaymentDetail(Connection con, PaymentDashboardForm paymentDashboardForm, int pageFlag) throws Exception
    {
        
        AuthorizationDataset leftAuthorizationDataset = new AuthorizationDataset();
        AuthorizationDataset rightAuthorizationDataset = new AuthorizationDataset();
        paymentDashboardForm.setLeftAuthorizationDataset(leftAuthorizationDataset);
        paymentDashboardForm.setRightAuthorizationDataset(rightAuthorizationDataset);

        Date leftStartDate = calcDate(paymentDashboardForm.getLeftRangeStartDate(), paymentDashboardForm.getLeftRangeStartTime());
        Date leftEndDate = calcDate(paymentDashboardForm.getLeftRangeEndDate(), paymentDashboardForm.getLeftRangeEndTime());
        Date rightStartDate = calcDate(paymentDashboardForm.getRightRangeStartDate(), paymentDashboardForm.getRightRangeStartTime());
        Date rightEndDate = calcDate(paymentDashboardForm.getRightRangeEndDate(), paymentDashboardForm.getRightRangeEndTime());

        if(pageFlag==0){
        	addLeftPaymentStats(con, paymentDashboardForm,leftStartDate,leftEndDate);
        }
        else
        {
        	addPaymentStats(con, paymentDashboardForm,leftStartDate,leftEndDate,rightStartDate,rightEndDate);
        }
        addAuthorizationStats(con, paymentDashboardForm,leftStartDate,leftEndDate,rightStartDate,rightEndDate);
        
    }

    /**
     * Calculate the dates to use on the form.  The date is based on the radio selection.
     * The selection is used to set the dates in the form and those dates are used for the
     * later processing.
     * @param dateRangeDashboardForm
     */
    public void calculateDates(DateRangeDashboardForm dateRangeDashboardForm)
    {
        Calendar nowCalendar = Calendar.getInstance();
        Date     nowDate = new Date();
        nowCalendar.setTime(nowDate);
        Date     tempDate;
        
        switch (dateRangeDashboardForm.getLeftRangeSelect())
        {
            case DashboardConstants.RANGE_LAST_HOUR: 
                nowCalendar.setTime(nowDate);
                // Now
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // One hour ago
                nowCalendar.add(Calendar.HOUR,-1);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_LAST_DAY: 
                // 0001 - Now
                nowCalendar.setTime(nowDate);
                // Now
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // Start of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,0);
                nowCalendar.set(Calendar.MINUTE,0);
                nowCalendar.set(Calendar.SECOND,0);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_YESTERDAY: 
                // Yesterday 0001 - Today 0000
                nowCalendar.setTime(nowDate);
                nowCalendar.add(Calendar.DAY_OF_YEAR,-1);
                // End of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,23);
                nowCalendar.set(Calendar.MINUTE,59);
                nowCalendar.set(Calendar.SECOND,59);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // Start of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,0);
                nowCalendar.set(Calendar.MINUTE,0);
                nowCalendar.set(Calendar.SECOND,0);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setLeftRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setLeftRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_CUSTOM: 
                // Do nothing, dates will be used
                break;
        }
        switch (dateRangeDashboardForm.getRightRangeSelect())
        {
            case DashboardConstants.RANGE_LAST_HOUR: 
                nowCalendar.setTime(nowDate);
                // Now
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // One hour ago
                nowCalendar.add(Calendar.HOUR,-1);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_LAST_DAY: 
                // 0001 - Now
                nowCalendar.setTime(nowDate);
                // Now
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // Start of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,0);
                nowCalendar.set(Calendar.MINUTE,0);
                nowCalendar.set(Calendar.SECOND,0);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_YESTERDAY: 
                // Yesterday 0001 - Today 0000
                nowCalendar.setTime(nowDate);
                nowCalendar.add(Calendar.DAY_OF_YEAR,-1);
                // End of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,23);
                nowCalendar.set(Calendar.MINUTE,59);
                nowCalendar.set(Calendar.SECOND,59);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeEndTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeEndDate(getSDFmmddyyyy().format(tempDate));
                // Start of Day
                nowCalendar.set(Calendar.HOUR_OF_DAY,0);
                nowCalendar.set(Calendar.MINUTE,0);
                nowCalendar.set(Calendar.SECOND,0);
                tempDate = nowCalendar.getTime();
                dateRangeDashboardForm.setRightRangeStartTime(getSDFhhmm().format(tempDate));
                dateRangeDashboardForm.setRightRangeStartDate(getSDFmmddyyyy().format(tempDate));
                break;
            case DashboardConstants.RANGE_CUSTOM: 
                // Do nothing, dates will be used
                break;
        }
        
        
    }
    
    public void calculateCustomDates(DateRangeDashboardForm dateRangeDashboardForm,DashboardForm dashboardForm)
    {
    	dateRangeDashboardForm.setLeftRangeEndTime(dashboardForm.getLeftRangeEndTime());
    	dateRangeDashboardForm.setLeftRangeEndDate(dashboardForm.getLeftRangeEndDate());
    	
    	dateRangeDashboardForm.setLeftRangeStartTime(dashboardForm.getLeftRangeStartTime());
    	dateRangeDashboardForm.setLeftRangeStartDate(dashboardForm.getLeftRangeStartDate());
    }

    /**
     * Save the authorization stats to the stats schema.
     * @param con
     * @param authorizationStatsPostForm
     */
    public void saveAuthorizationStats(Connection con, AuthorizationStatsPostForm authorizationStatsPostForm) throws Exception
    {
        DashboardDAO dao = new DashboardDAO();
        
        // Convert form to VO
        AuthorizationStatsVO vo = new AuthorizationStatsVO();
        
        vo.setApprovals(authorizationStatsPostForm.getApprovals());
        vo.setAuthClient(authorizationStatsPostForm.getAuthClient());
        vo.setAuthServer(authorizationStatsPostForm.getAuthServer());
        vo.setCardType(authorizationStatsPostForm.getCardType());
        vo.setDeclines(authorizationStatsPostForm.getDeclines());
        vo.setApprovals(authorizationStatsPostForm.getApprovals());
        
        // SDF not thread safe, build a new one
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmm");
        Date authDate = sdf.parse(authorizationStatsPostForm.getAuthorizationTime());
        vo.setStatsDate(authDate);
        
        dao.insertAuthorizationStats(con, vo);
        
    }

    public void populateOrderServiceDetail(Connection con, OrderServiceDashboardForm orderServiceDashboardForm) throws Exception
    {
        OrderServiceDataset leftOrderServiceDataset = new OrderServiceDataset();
        OrderServiceDataset rightOrderServiceDataset = new OrderServiceDataset();
        orderServiceDashboardForm.setLeftOrderServiceDataset(leftOrderServiceDataset);
        orderServiceDashboardForm.setRightOrderServiceDataset(rightOrderServiceDataset);

        Date leftStartDate = calcDate(orderServiceDashboardForm.getLeftRangeStartDate(), orderServiceDashboardForm.getLeftRangeStartTime());
        Date leftEndDate = calcDate(orderServiceDashboardForm.getLeftRangeEndDate(), orderServiceDashboardForm.getLeftRangeEndTime());
        Date rightStartDate = calcDate(orderServiceDashboardForm.getRightRangeStartDate(), orderServiceDashboardForm.getRightRangeStartTime());
        Date rightEndDate = calcDate(orderServiceDashboardForm.getRightRangeEndDate(), orderServiceDashboardForm.getRightRangeEndTime());

        addOrderServiceStats(con, orderServiceDashboardForm,leftStartDate,leftEndDate, rightStartDate, rightEndDate);
    }

    private void addOrderServiceStats(Connection con, OrderServiceDashboardForm orderServiceDashboardForm,
                                       Date leftStartDate, Date leftEndDate,
                                       Date rightStartDate, Date rightEndDate
                                     ) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();

        if (orderServiceDashboardForm.getGroupBy() == null || orderServiceDashboardForm.getGroupBy().equals(""))
        {
           orderServiceDashboardForm.setGroupBy(DashboardConstants.STATUS_CODE); 
        }

        // Setup the times for the left side
        List<OrderServiceStatsVO> leftOrderServiceStats;
        leftOrderServiceStats = dao.getOrderServiceStatsSummary(con,leftStartDate, leftEndDate, orderServiceDashboardForm.getGroupBy());
        OrderServiceDataset leftOrderServiceDataset = new OrderServiceDataset();
        orderServiceDashboardForm.setLeftOrderServiceDataset(leftOrderServiceDataset);

        for (int i=0; i < leftOrderServiceStats.size();i++)
        {
            OrderServiceStatsVO vo = leftOrderServiceStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,vo.getStatsTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);
            leftOrderServiceDataset.addStat(vo, timePeriod, getGroupByValue(orderServiceDashboardForm, vo));
        }

        // Setup the times for the right side
        List<OrderServiceStatsVO> rightOrderServiceStats;
        rightOrderServiceStats = dao.getOrderServiceStatsSummary(con,rightStartDate, rightEndDate, orderServiceDashboardForm.getGroupBy());
        OrderServiceDataset rightOrderServiceDataset = new OrderServiceDataset();
        orderServiceDashboardForm.setRightOrderServiceDataset(rightOrderServiceDataset);

        for (int i=0; i < rightOrderServiceStats.size();i++)
        {
            OrderServiceStatsVO vo = rightOrderServiceStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,vo.getStatsTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);
            rightOrderServiceDataset.addStat(vo, timePeriod, getGroupByValue(orderServiceDashboardForm, vo));
        }

    }

    private String getGroupByValue(OrderServiceDashboardForm form, OrderServiceStatsVO vo)
    {
        if (form.isActionTypeGroupBy())
        {
            return vo.getActionType();
        }
        else if (form.isNodeGroupBy())
        {
            return vo.getNode();
        }
        else if (form.isActionTypeGroupBy())
        {
            return vo.getActionType();
        }
        else if (form.isStatusCodeGroupBy())
        {
            return vo.getStatusCode();
        }
        else
        {
            return "";
        }
    }

    private void addAuthorizationStats(Connection con, PaymentDashboardForm paymentDashboardForm,
                                       Date leftStartDate, Date leftEndDate,
                                       Date rightStartDate, Date rightEndDate
                                       ) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();

        Map<String,AuthorizationCombinedStatsVO> authorizationMap = new HashMap<String,AuthorizationCombinedStatsVO>();
        paymentDashboardForm.setCombinedAuthorizationMap(authorizationMap);

        // Setup the times for the left side
        List<AuthorizationStatsVO> leftAuthorizations;
        leftAuthorizations = dao.getAuthorizationStatsSummary(con,leftStartDate, leftEndDate);
        AuthorizationDataset leftAuthorizationDataset = new AuthorizationDataset();
        paymentDashboardForm.setLeftAuthorizationDataset(leftAuthorizationDataset);

        for (int i=0; i < leftAuthorizations.size();i++)
        {
            AuthorizationStatsVO vo = leftAuthorizations.get(i);
            leftAuthorizationDataset.addStat(vo);
            AuthorizationCombinedStatsVO combinedVO = new AuthorizationCombinedStatsVO();
            combinedVO.setLeftAuthorizationStatsVO(vo);
            combinedVO.setCardType(vo.getCardType());
            authorizationMap.put(vo.getCardType(), combinedVO);
        }
        
        // Setup the times for the right side
        List<AuthorizationStatsVO> rightAuthorizations;
        rightAuthorizations = dao.getAuthorizationStatsSummary(con,rightStartDate,rightEndDate);
        AuthorizationDataset rightAuthorizationDataset = new AuthorizationDataset();
        paymentDashboardForm.setRightAuthorizationDataset(rightAuthorizationDataset);

        for (int i=0; i < rightAuthorizations.size();i++)
        {
            AuthorizationStatsVO vo = rightAuthorizations.get(i);
            rightAuthorizationDataset.addStat(vo);

            AuthorizationCombinedStatsVO combinedVO;
            combinedVO = authorizationMap.get(vo.getCardType());
            if (combinedVO == null)
            {
                combinedVO = new AuthorizationCombinedStatsVO();
                combinedVO.setCardType(vo.getCardType());
                authorizationMap.put(vo.getCardType(), combinedVO);
            }
            combinedVO.setRightAuthorizationStatsVO(vo);
        }
        
    }
    
    private void addPaymentStats(Connection con, PaymentDashboardForm paymentDashboardForm, 
                                 Date leftStartDate, Date leftEndDate,
                                 Date rightStartDate, Date rightEndDate
                                 ) throws Exception
    {
        DashboardDAO dao = new  DashboardDAO();

        // Setup the times for the left side
        List<PaymentStatsVO> leftPayments;
        leftPayments = dao.getPaymentStatsSummary(con,leftStartDate, leftEndDate);

        // Setup the times for the right side
        List<PaymentStatsVO> rightPayments;
        rightPayments = dao.getPaymentStatsSummary(con,rightStartDate,rightEndDate);

        // Build the dataset models for the graph
        PaymentDataset leftPaymentDataset = new PaymentDataset();
        PaymentDataset rightPaymentDataset = new PaymentDataset();
        paymentDashboardForm.setLeftPaymentDataset(leftPaymentDataset);
        paymentDashboardForm.setRightPaymentDataset(rightPaymentDataset);

        // Create a combined payment map for the model
        Map<String,PaymentCombinedStatsVO> paymentMap = new HashMap<String,PaymentCombinedStatsVO>();
        paymentDashboardForm.setCombinedPaymentMap(paymentMap);
                                             
        // Process each of the payment list into the appropriate models                                             
        for (int i=0; i  < leftPayments.size();i++)
        {
            PaymentStatsVO vo = leftPayments.get(i);
            
            leftPaymentDataset.addStat(vo);
            // First time for each pay type, so add a new combined
            PaymentCombinedStatsVO combinedVO = new PaymentCombinedStatsVO();
            combinedVO.setLeftPaymentStatsVO(vo);
            combinedVO.setPaymentType(vo.getPaymentType());
            paymentMap.put(vo.getPaymentType(), combinedVO);
        }
        
	        int total = 0;
	        for (String key : paymentMap.keySet()) {
	     	  total += paymentMap.get(key).getLeftPaymentStatsVO().getCount();
	    	}
	        
	        long invidualCount = 0;
	                
	        for (String key : paymentMap.keySet()) {
	        	invidualCount = paymentMap.get(key).getLeftPaymentStatsVO().getCount();
	        	double d = getPercentageDistribution((int)invidualCount, total);
	        	PercentageRoundVO percentageRoundVO = new PercentageRoundVO();
	        	boolean flag;
            	
            	if(d-(int)d!=0){
            		flag = true;
            		percentageRoundVO.setIsDouble(flag);
            		percentageRoundVO.setDvalue(d);
            	}
            	else{
            		percentageRoundVO.setIvalue((int)d);
            	}
            	paymentMap.get(key).getLeftPaymentStatsVO().setPaymentTypePercentage(percentageRoundVO);
	    	}
        
        // Process each of the payment list into the appropriate models                                             
        for (int i=0; i  < rightPayments.size();i++)
        {
            PaymentStatsVO vo = rightPayments.get(i);
            
            rightPaymentDataset.addStat(vo);
            // Check for the payment type first, then add/update in map
            PaymentCombinedStatsVO combinedVO;
            combinedVO = paymentMap.get(vo.getPaymentType());
            if (combinedVO == null)
            {
                combinedVO = new PaymentCombinedStatsVO();
                combinedVO.setPaymentType(vo.getPaymentType());
                paymentMap.put(vo.getPaymentType(), combinedVO);
            }
            combinedVO.setRightPaymentStatsVO(vo);
        }
    }
    
    private void addLeftPaymentStats(Connection con, PaymentDashboardForm paymentDashboardForm, 
    		Date leftStartDate, Date leftEndDate) throws Exception
    {
    	DashboardDAO dao = new  DashboardDAO();

    	// Setup the times for the left side
    	List<PaymentStatsVO> leftPayments;
    	leftPayments = dao.getPaymentStatsSummary(con,leftStartDate, leftEndDate);

    	// Build the dataset models for the graph
    	PaymentDataset leftPaymentDataset = new PaymentDataset();
    	paymentDashboardForm.setLeftPaymentDataset(leftPaymentDataset);

    	// Create a combined payment map for the model
    	Map<String,PaymentCombinedStatsVO> paymentMap = new HashMap<String,PaymentCombinedStatsVO>();
    	paymentDashboardForm.setCombinedPaymentMap(paymentMap);

    	// Process each of the payment list into the appropriate models                                             
    	for (int i=0; i  < leftPayments.size();i++)
    	{
    		PaymentStatsVO vo = leftPayments.get(i);

    		leftPaymentDataset.addStat(vo);
    		// First time for each pay type, so add a new combined
    		PaymentCombinedStatsVO combinedVO = new PaymentCombinedStatsVO();
    		combinedVO.setLeftPaymentStatsVO(vo);
    		combinedVO.setPaymentType(vo.getPaymentType());
    		paymentMap.put(vo.getPaymentType(), combinedVO);
    	}

    	int total = 0;
    	for (String key : paymentMap.keySet()) {
    		total += paymentMap.get(key).getLeftPaymentStatsVO().getCount();
    	}

    	long invidualCount = 0;

    	for (String key : paymentMap.keySet()) {
    		invidualCount = paymentMap.get(key).getLeftPaymentStatsVO().getCount();
    		double d = getPercentageDistribution((int)invidualCount, total);
    		PercentageRoundVO percentageRoundVO = new PercentageRoundVO();
    		boolean flag;

    		if(d-(int)d!=0){
    			flag = true;
    			percentageRoundVO.setIsDouble(flag);
    			percentageRoundVO.setDvalue(d);
    		}
    		else{
    			percentageRoundVO.setIvalue((int)d);
    		}
    		paymentMap.get(key).getLeftPaymentStatsVO().setPaymentTypePercentage(percentageRoundVO);
    	}
                     
    }
    
    /**
     * Modify the date object to have the specific time.  
     * @param dateString
     * @param timeString
     * @return
     */
    private Date calcDate(String dateString, String timeString)
    {
        Date calculatedDate;
        // First parse the date object to get a date
        // Format is supposed to be mm/dd/yyyy
        SimpleDateFormat sdf = getSDFmmddyyyyhhmm();
        // Append the time portion, supposed to be hhmm
        String dateTimeString = dateString + timeString;
        try
        {
            calculatedDate =  sdf.parse(dateTimeString);
        }
        catch (ParseException pe)
        {
            logger.info("Date is not parseable, setting to today " + dateTimeString);
            calculatedDate = new Date();
        }
        logger.debug("Parsed date ended up being " + sdf.format(calculatedDate));
        
        return calculatedDate;
    }
    
    private SimpleDateFormat getSDFmmddyyyyhhmm()
    {
        if (sdfmmddyyyyhhmm == null)
        {
            sdfmmddyyyyhhmm = new SimpleDateFormat("MM/dd/yyyyHHmm");
        }
        return sdfmmddyyyyhhmm;
    }
    private SimpleDateFormat getSDFmmddyyyy()
    {
        if (sdfmmddyyyy == null)
        {
            sdfmmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
        }
        return sdfmmddyyyy;
    }
    private SimpleDateFormat getSDFhhmm()
    {
        if (sdfhhmm == null)
        {
            sdfhhmm = new SimpleDateFormat("HHmm");
        }
        return sdfhhmm;
    }
    
    private void addOrderStats(List<OrderStatsVO> orderStats, DashboardForm dashboardForm)
    {

        OrderOriginDataset orderOriginDataset = new OrderOriginDataset();
        OrderDeliveryDataset orderDeliveryDataset = new OrderDeliveryDataset();

        for (int i=0; i < orderStats.size(); i++)
        {
            OrderStatsVO orderStatsVO = orderStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,orderStatsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            orderOriginDataset.addStat(orderStatsVO, timePeriod);    
            orderDeliveryDataset.addStat(orderStatsVO, timePeriod);    
        }

        dashboardForm.setOrderOriginXyData(orderOriginDataset);
        dashboardForm.setOrderDeliveryXyData(orderDeliveryDataset);

    }

    private void addOrderOriginStats(List<OrderStatsVO> orderStats, OrderDashboardForm dashboardForm)
    {

        OrderOriginDataset orderOriginDataset = new OrderOriginDataset();

        for (int i=0; i < orderStats.size(); i++)
        {
            OrderStatsVO orderStatsVO = orderStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,orderStatsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            orderOriginDataset.addStat(orderStatsVO, timePeriod);    
        }

        dashboardForm.setOrderOriginXyData(orderOriginDataset);
        
        
        OrderProjectionDataset orderProjectionDataset = new OrderProjectionDataset();
        orderProjectionDataset.setValue(99);
        
        dashboardForm.setOrderProjectionMeterData(orderProjectionDataset);

    }

    private void addFloristStats(FloristDataset floristDataset, List<FloristStatsVO> floristSummaryStats, DashboardForm dashboardForm)
    {
        for (int i=0; i < floristSummaryStats.size(); i++)
        {
            FloristStatsVO statsVO = floristSummaryStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,statsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            floristDataset.addStat(statsVO, timePeriod);    
        }

        dashboardForm.setFloristXyData(floristDataset);
    }

    private void addQueueStats(QueueDataset queueDataset, List<QueueStatsVO> queueSummaryStats, DashboardForm dashboardForm)
    {
        for (int i=0; i < queueSummaryStats.size(); i++)
        {
            QueueStatsVO statsVO = queueSummaryStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,statsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            queueDataset.addStat(statsVO, timePeriod);    
        }

        dashboardForm.setQueueXyData(queueDataset);
    }

    private void addQueueDetailStats(QueueDetailDataset queueDetailDataset, List<QueueDetailStatsVO> queueDetailStats, QueueDashboardForm queueDashboardForm)
    {
        for (int i=0; i < queueDetailStats.size(); i++)
        {
            QueueDetailStatsVO statsVO = queueDetailStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,statsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            queueDetailDataset.addStat(statsVO, timePeriod);    
        }

        queueDashboardForm.setQueueXyData(queueDetailDataset);

    }

    private void addFloristDetailStats(FloristDetailDataset floristGotoBarData,
                                       FloristDetailDataset floristBarData,
                                       List<FloristStatsVO> statsVOList, 
                                       int hoursBack)
    {
        for (int i = 0; i < statsVOList.size(); i++)
        {
            FloristStatsVO statsVO = statsVOList.get(i);
            floristGotoBarData.addStat(statsVO.getGotoFloristShutdownCount(), hoursBack, statsVO.getRegion());    
            floristBarData.addStat(statsVO.getPercentMercuryShopSuspend(), hoursBack, statsVO.getRegion());    
        }
    }


    private void addZipStats(FloristDataset floristDataset, List<ZipStatsVO> zipStats, DashboardForm dashboardForm)
    {

        for (int i=0; i < zipStats.size(); i++)
        {
            ZipStatsVO statsVO = zipStats.get(i);
            RegularTimePeriod timePeriod = RegularTimePeriod.createInstance(Minute.class,statsVO.getStatTimestamp(),RegularTimePeriod.DEFAULT_TIME_ZONE);

            floristDataset.addZipStat(statsVO, timePeriod);    
        }

        dashboardForm.setFloristXyData(floristDataset);

    }
  
    protected int getFloristDetailsHoursBack()
    {
        if (floristDetailsHoursBack == 0)
        {
            String floristDetailsHoursBackString = "";
            try
            {
                floristDetailsHoursBackString = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,FLORIST_DETAILS_HOURS_BACK);
                floristDetailsHoursBack = Integer.valueOf(floristDetailsHoursBackString);
                
                logger.debug("floristDetailsHoursBackString = " + floristDetailsHoursBackString);
            }
            catch (Exception nfe)
            {
                logger.error("Number format Exception " + floristDetailsHoursBackString,nfe);
                floristDetailsHoursBack = DEFAULT_FLORIST_DETAILS_HOURS_BACK;
            }
        }  
        
        return floristDetailsHoursBack;
    }
    
    public void saveOrderServiceStats(Connection con, OrderServiceStatsPostForm orderServiceStatsForm) throws Exception
    {
        DashboardDAO dao = new DashboardDAO();
        
        // Convert form to VO
        OrderServiceStatsVO vo = new OrderServiceStatsVO();
        
        vo.setNode(orderServiceStatsForm.getNode());
        vo.setActionType(orderServiceStatsForm.getActionType());
        vo.setClientId(orderServiceStatsForm.getClientId());
        vo.setCount(orderServiceStatsForm.getCount());
        vo.setStatusCode(orderServiceStatsForm.getStatusCode());

        
        // SDF not thread safe, build a new one
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyHHmm");
        Date authDate = sdf.parse(orderServiceStatsForm.getStatsTimestamp());
        vo.setStatsTimestamp(authDate);
        
        dao.insertOrderServiceStats(con, vo);
    }

    public void populateFloristTableData(Connection con, DashboardForm dashboardForm) throws Exception
    {
    	FloristDashboardForm floristDashboardForm = new FloristDashboardForm();
    	DashboardDAO dao = new  DashboardDAO();
        FloristDataset floristDataset = new FloristDataset();
        TimeSeriesCollection floristTimeSeriesCollection = floristDataset.getSeriesCollection();
        List<TimeSeries> floristTimeSeriesList= floristTimeSeriesCollection.getSeries();
        Map<String,BaseTableVO> floristMapObject = new HashMap<String,BaseTableVO>();
       
        List<Integer> thresholdWarnPercentageList = getThresholdPercentages(DashboardConstants.FLORIST_STATUS_WARN_PERCENTAGES);
        List<Integer> thresholdCritPercentageList = getThresholdPercentages(DashboardConstants.FLORIST_STATUS_CRIT_PERCENTAGES);
        
        double current, previous;
        
        List<Long> currentList = new ArrayList<Long>();
        List<Long> previousList = new ArrayList<Long>();
        
        
        Map<String,List<Double>> hourlyPercentagesrp = dao.getFloristTableStats(con);
        
        /*loop for rounding up percentages and making a new Map with those rounded values*/
        Map<String,List<Long>> hourlyPercentages = new LinkedHashMap<String, List<Long>>(); 
        for (String key : hourlyPercentagesrp.keySet()) {
        	List<Long> l = new ArrayList<Long>(); 
        	
        	for(double d:hourlyPercentagesrp.get(key)){
        		l.add((long) getRoundUpValue(d));
        	}
        	hourlyPercentages.put(key, l);
    	}
        
        
        
        int i= 0;
        for (TimeSeries floristTimeSeries : floristTimeSeriesList)
        {
        	BaseTableVO floristTableVO = new BaseTableVO();
        	String attributeName = (String)floristTimeSeries.getKey();
        	floristTableVO.setAttributeName(attributeName);
       	
        	List<Integer> hourlyValues = new ArrayList<Integer>();
        	List<String> attrList = new ArrayList<String>();
        	
        	currentList = hourlyPercentages.get("CURRENT");
        	previousList = hourlyPercentages.get("PREVIOUS");
        	attrList.add("FLORIST STATS");
        	attrList.add(attributeName);
        	hourlyValues = getCurrentPreviousValues(currentList, previousList, attrList, i);
        	current = hourlyValues.get(0);
        	previous = hourlyValues.get(1);
        	
        	current = getRoundUpValue(current);
	        previous = getRoundUpValue(previous);
        	
        	floristTableVO.setCurrentHourPercentage((int)current);
        	floristTableVO.setPreviousHourPercentage((int)previous);
        	
        	floristTableVO.setWarningThreshold(thresholdWarnPercentageList.get(i));
	        floristTableVO.setCriticalThreshold(thresholdCritPercentageList.get(i));
	       
        	floristMapObject.put(attributeName,floristTableVO);
        	i++;
        }
        floristDashboardForm.setSetupVO(floristMapObject);
        
        dashboardForm.setFloristDashboardForm(floristDashboardForm);
    }
   
    public void populateOrderTableData(Connection con, DashboardForm dashboardForm) throws Exception
    {
    	DashboardDAO dao = new DashboardDAO();
    	OrderDashboardForm orderDashboardForm = new OrderDashboardForm();
    	
    	
    	int current, previous;
    	int currentTotal = 0, previousTotal = 0;
    	
        OrderOriginDataset orderDataset = new OrderOriginDataset();
        TimeSeriesCollection orderTimeSeriesCollection = orderDataset.getSeriesCollection();
        List<TimeSeries> orderTimeSeriesList= orderTimeSeriesCollection.getSeries();
        LinkedHashMap<String,BaseTableVO> orderMapObject = new LinkedHashMap<String, BaseTableVO>();
       
        List<Integer> thresholdPercentageList = getThresholdPercentages(DashboardConstants.ORDER_FLOW_THRESHOLD_PERCENTAGES);
       
        List<Long> currentList = new ArrayList<Long>();
        List<Long> previousList = new ArrayList<Long>();
        
        
		Map<String,List<Long>> hourlyPercentages = dao.getOrderTableStats(con);
        int i= 0;
        
        
        for (TimeSeries orderTimeSeries : orderTimeSeriesList)
        {
        	BaseTableVO orderTableVO = new BaseTableVO();
        	
        	
        	String attributeName = (String)orderTimeSeries.getKey();
        	orderTableVO.setAttributeName(attributeName);
        	
        	List<Integer> hourlyValues = new ArrayList<Integer>();
        	List<String> attrList = new ArrayList<String>();
        	
        	currentList = hourlyPercentages.get("CURRENT");
        	previousList = hourlyPercentages.get("PREVIOUS");
        	attrList.add("ORDER STATS");
        	attrList.add(attributeName);
        	hourlyValues = getCurrentPreviousValues(currentList, previousList, attrList, i);
        	current = hourlyValues.get(0);
        	previous = hourlyValues.get(1);
        	
        	previousTotal += previous;
        	
        	previous = getRoundUpValue(getPercentageChange(current, previous));
            
        	orderTableVO.setCurrentHourPercentage(current);
    		orderTableVO.setPreviousHourPercentage(previous);
    		orderTableVO.setCriticalThreshold(thresholdPercentageList.get(i));
        	
    		currentTotal += current;
        	
    		orderMapObject.put(attributeName, orderTableVO);
        	i++;
        }
        BaseTableVO orderTableVO = new BaseTableVO();
        
        previousTotal = getRoundUpValue(getPercentageChange(currentTotal, previousTotal));
        
        orderTableVO.setCurrentHourPercentage(currentTotal);
		orderTableVO.setPreviousHourPercentage(previousTotal);
    	orderTableVO.setAttributeName("Total");
    	orderMapObject.put("Total", orderTableVO);
        
        orderDashboardForm.setSetupVO(orderMapObject);
        dashboardForm.setOrderDashboardForm(orderDashboardForm);
    }
    
   
    public void populateQueueTableData(Connection con, DashboardForm dashboardForm) throws Exception
    {
    	DashboardDAO dao = new DashboardDAO();
    	QueueDashboardForm queueDashboardForm = new QueueDashboardForm();
    	
    	int current, previous;
    	int currentTotal = 0, previousTotal = 0;
    	
    	QueueDataset queueDataset = new QueueDataset();
        TimeSeriesCollection queueTimeSeriesCollection = queueDataset.getSeriesCollection();
        List<TimeSeries> queueTimeSeriesList= queueTimeSeriesCollection.getSeries();
        LinkedHashMap<String,BaseTableVO> queueMapObject = new LinkedHashMap<String, BaseTableVO>();
       
        List<Integer> thresholdWarnPercentageList = getThresholdPercentages(DashboardConstants.QUEUE_STATUS_WARN_PERCENTAGES);
        List<Integer> thresholdCritPercentageList = getThresholdPercentages(DashboardConstants.QUEUE_STATUS_CRIT_PERCENTAGES);
       
        List<Long> currentList = new ArrayList<Long>();
        List<Long> previousList = new ArrayList<Long>();
        
        
		Map<String,List<Long>> hourlyPercentages = dao.getQueueTableStats(con);
        int i= 0;
               
        for (TimeSeries queueTimeSeries : queueTimeSeriesList)
        {
        	BaseTableVO queueTableVO = new BaseTableVO();
        	
        	
        	String attributeName = (String)queueTimeSeries.getKey();
        	queueTableVO.setAttributeName(attributeName);
        	
        	List<Integer> hourlyValues = new ArrayList<Integer>();
        	List<String> attrList = new ArrayList<String>();
        	
        	currentList = hourlyPercentages.get("CURRENT");
        	previousList = hourlyPercentages.get("PREVIOUS");
        	attrList.add("QUEUE STATS");
        	attrList.add(attributeName);
        	hourlyValues = getCurrentPreviousValues(currentList, previousList, attrList, i);
        	current = hourlyValues.get(0);
        	previous = hourlyValues.get(1);
        	previousTotal += previous;
        	
        	previous = getRoundUpValue(getPercentageChange(current, previous));
        	
        	queueTableVO.setCurrentHourPercentage(current);
        	queueTableVO.setPreviousHourPercentage(previous);
        	queueTableVO.setWarningThreshold(thresholdWarnPercentageList.get(i));
        	queueTableVO.setCriticalThreshold(thresholdCritPercentageList.get(i));
        	currentTotal += current;

        	queueMapObject.put(attributeName, queueTableVO);
        	i++;
        }
        
        BaseTableVO queueTableVO = new BaseTableVO();
        
        previousTotal = getRoundUpValue(getPercentageChange(currentTotal, previousTotal));
        queueTableVO.setCurrentHourPercentage(currentTotal);
        queueTableVO.setPreviousHourPercentage(previousTotal);

        queueTableVO.setAttributeName("Total");
        queueMapObject.put("Total", queueTableVO);
        
        queueDashboardForm.setSetupVO(queueMapObject);
        dashboardForm.setQueueDashboardForm(queueDashboardForm);
    }
    
    
    public void populateInventoryTableData(Connection con, DashboardForm dashboardForm) throws Exception
    {
    	DashboardDAO dao = new DashboardDAO();
    	InventoryDashboardForm inventoryDashboardForm = new InventoryDashboardForm();
    	
    	double current, previous;
    	int currentTotal = 0, previousTotal = 0;;
    	
    	OrderDeliveryDataset inventoryDataset = new OrderDeliveryDataset();
        TimeSeriesCollection inventoryTimeSeriesCollection = inventoryDataset.getSeriesCollection();
        List<TimeSeries> inventoryTimeSeriesList= inventoryTimeSeriesCollection.getSeries();
        LinkedHashMap<String,InventoryTableVO> inventoryMapObject = new LinkedHashMap<String, InventoryTableVO>();
       
        List<Long> currentList = new ArrayList<Long>();
        List<Long> previousList = new ArrayList<Long>();
        
       
		Map<String,List<Long>> hourlyPercentages = dao.getInventoryTableStats(con);
        int i= 0;
               
        for (TimeSeries inventoryTimeSeries : inventoryTimeSeriesList)
        {
        	InventoryTableVO inventoryTableVO = new InventoryTableVO();
        	
        	
        	String attributeName = (String)inventoryTimeSeries.getKey();
        	inventoryTableVO.setAttributeName(attributeName);
        	
        	List<Integer> hourlyValues = new ArrayList<Integer>();
        	List<String> attrList = new ArrayList<String>();
        	
        	currentList = hourlyPercentages.get("CURRENT");
        	previousList = hourlyPercentages.get("PREVIOUS");
        	attrList.add("INVENTORY STATS");
        	attrList.add(attributeName);
        	hourlyValues = getCurrentPreviousValues(currentList, previousList, attrList, i);
        	current = hourlyValues.get(0);
        	previous = hourlyValues.get(1);
        		
        	
        	previousTotal += previous;
        	currentTotal += current;
        	
        	PercentageRoundVO currentPercentageRoundVO = new PercentageRoundVO(); 
        	PercentageRoundVO previousPercentageRoundVO = new PercentageRoundVO();
        	
        	currentPercentageRoundVO.setIvalue((int)current);
        	previousPercentageRoundVO.setIvalue((int)previous);
        	
        	inventoryTableVO.setCurrentHourPercentage(currentPercentageRoundVO);
        	inventoryTableVO.setPreviousHourPercentage(previousPercentageRoundVO);
        	
        	inventoryMapObject.put(attributeName, inventoryTableVO);
        	i++;
        }
        
        /*loop for setting percentage distribution*/
        
        for (String key : inventoryMapObject.keySet()) {
        		PercentageRoundVO currentPercentageRoundVO = new PercentageRoundVO(); 
        		PercentageRoundVO previousPercentageRoundVO = new PercentageRoundVO();
        	   
        		current = getPercentageDistribution(inventoryMapObject.get(key).getCurrentHourPercentage().getIvalue(), currentTotal);
        		previous = getPercentageDistribution(inventoryMapObject.get(key).getPreviousHourPercentage().getIvalue(), previousTotal);
        		
        		boolean c, p;
            	
            	if(current-(int)current!=0){
            		c = true;
            		currentPercentageRoundVO.setIsDouble(c);
            		currentPercentageRoundVO.setDvalue(current);
            	}
            	else{
            		currentPercentageRoundVO.setIvalue((int)current);
            	}
            	if(previous-(int)previous!=0){
            		p = true;
            		previousPercentageRoundVO.setIsDouble(p);
            		previousPercentageRoundVO.setDvalue(previous);
            	}
            	else{
            		previousPercentageRoundVO.setIvalue((int)previous);
            	}
            	
        	   inventoryMapObject.get(key).setCurrentHourPercentage(currentPercentageRoundVO);
        	   inventoryMapObject.get(key).setPreviousHourPercentage(previousPercentageRoundVO);
        	   
        	}
        
        inventoryDashboardForm.setSetupVO(inventoryMapObject);
        dashboardForm.setInventoryDashboardForm(inventoryDashboardForm);
    }
    
    
    public List<Integer> getCurrentPreviousValues(List<Long> currentList, List<Long> previousList, List<String> attrList, int i){
    	
    	int current, previous;
    	List<Integer> hourlyValues = new ArrayList<Integer>();
    	if( currentList == null )
    	{
    		if(previousList == null )
        	{
    			current = 0;
    			previous = 0;
    		    logger.info(attrList.get(0)+":: "+attrList.get(1)+" ::The scheduled job CREATE "+ attrList.get(0)+" is currently not running, setting the CURRENT and PREVIOUS hour percentages to 0 ");
        	}
    		else
			{
    			logger.info(attrList.get(0)+":: "+attrList.get(1)+" ::Setting the current hour stats to 0, as the job is not running from 1 hr");
    			current = 0;
     			previous = previousList.get(i).intValue();
			}
    	}
    	else if(previousList == null)
    	{
    		current = currentList.get(i).intValue();
    		logger.info(attrList.get(0)+":: "+attrList.get(1)+" ::Setting the previous hour stats to 0, as the job didnot run for previous hour/is not running from previous hour");
    		previous = 0;
    	}
    	else
    	{
    	   current = currentList.get(i).intValue();
           previous = previousList.get(i).intValue();
           logger.info(attrList.get(0)+":: "+attrList.get(1)+" :: inserted successfully");
    	}
    	hourlyValues.add(current);
    	hourlyValues.add(previous);
    	return hourlyValues;
    }
  
  
    public double getPercentageDistribution(int value, int total){
    	double percentage;
    	if(total == 0 || value == 0){
    		percentage = 0;
    	}
    	else{
    		percentage = value*100.0/total;
    	}
    	
    	String s = String.format("%.2f", percentage);
    	return Double.parseDouble(s);
    }
    
    public double getPercentageChange(int current, int previous)
    {
    	double percentage;
    	if(previous!= 0)
    	{
    		if(current!=0){
    			percentage =  ((current-previous)*100.0/previous);
    		}
    		else{
    			percentage = -100;
    		}
    	}
    	else
    	{
    		if(current!=0){
    			percentage = 100;
    		}
    		else{
    			percentage = 0;
    		}
    		
    	}
    	
    	return percentage;
    }
    
    public int getRoundUpValue(double a){
    	double c;
    	if(a >= 0)
    		c = a+0.5;
    	else
    		c = a-0.5;
        return (int)c;
    }
    
	public List<Integer> getThresholdPercentages(String paramName) throws Exception 
	{
		String encodedParm = getConfigurationUtil().getFrpGlobalParmNoNull(DashboardConstants.DASHBOARD_CONFIG_CONTEXT, paramName);
		if(encodedParm.isEmpty())
		{
			logger.error("Error occurred while retrieving the Global parm value for the CONTEXT :: "+DashboardConstants.DASHBOARD_CONFIG_CONTEXT +", NAME :: "+paramName);
		}
		
		List<Integer> thresholdPercentagesList = new ArrayList<Integer>();
		StringTokenizer tokenizer = new StringTokenizer(encodedParm,";");
        
        while(tokenizer.hasMoreTokens())
        {
        	thresholdPercentagesList.add(Integer.parseInt(tokenizer.nextToken()));
        }
        return thresholdPercentagesList;
    }

}
