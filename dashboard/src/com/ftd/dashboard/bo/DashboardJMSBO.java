package com.ftd.dashboard.bo;

import com.ftd.dashboard.dao.DashboardDAO;
import com.ftd.dashboard.dataset.FloristDataset;
import com.ftd.dashboard.dataset.FloristDetailDataset;
import com.ftd.dashboard.dataset.JMSDetailDataset;
import com.ftd.dashboard.dataset.OrderDeliveryDataset;
import com.ftd.dashboard.dataset.OrderOriginDataset;
import com.ftd.dashboard.dataset.OrderProjectionDataset;
import com.ftd.dashboard.dataset.ProductCumulativeDataset;
import com.ftd.dashboard.dataset.ProductRateDataset;
import com.ftd.dashboard.dataset.QueueDataset;
import com.ftd.dashboard.dataset.QueueDetailDataset;
import com.ftd.dashboard.util.DashboardConstants;
import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.dashboard.vo.JMSQueueSetupVO;
import com.ftd.dashboard.vo.JMSStatsVO;
import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.dashboard.vo.ProductStatsVO;
import com.ftd.dashboard.vo.QueueDetailStatsVO;
import com.ftd.dashboard.vo.QueueStatsVO;
import com.ftd.dashboard.vo.ZipStatsVO;
import com.ftd.dashboard.web.form.DashboardForm;
import com.ftd.dashboard.web.form.FloristDashboardForm;
import com.ftd.dashboard.web.form.InventoryDashboardForm;
import com.ftd.dashboard.web.form.JMSDashboardForm;
import com.ftd.dashboard.web.form.OrderDashboardForm;
import com.ftd.dashboard.web.form.QueueDashboardForm;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.DecimalFormat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.StringTokenizer;

import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;

public class DashboardJMSBO extends BaseDashboardBO
{
    private static Logger logger = new Logger("com.ftd.dashboard.bo.DashboardJMSBO");

    private final static String ORDER_CATEGORY = "ORDER";
    
    private ConfigurationUtil cu;

    public DashboardJMSBO()
    {
    }


    public void populateJMSDetail(Connection con, JMSDashboardForm dashboardForm) throws Exception
    {
        DashboardDAO dao = new DashboardDAO();
        JMSDetailDataset jmsOrderFlowBarData = new JMSDetailDataset();
        JMSDetailDataset jmsOtherBarData = new JMSDetailDataset();

        Map<String, JMSQueueSetupVO> orderSetupMap = new HashMap<String, JMSQueueSetupVO>();
        Map<String, JMSQueueSetupVO> otherSetupMap = new HashMap<String, JMSQueueSetupVO>();
        Map<String, JMSStatsVO> orderStatsMap = new HashMap<String, JMSStatsVO>();
        Map<String, JMSStatsVO> otherStatsMap = new HashMap<String, JMSStatsVO>();

        dashboardForm.setOrderSetupMap(orderSetupMap);
        dashboardForm.setOtherSetupMap(otherSetupMap);
        dashboardForm.setOrderStatsMap(orderStatsMap);
        dashboardForm.setOtherStatsMap(otherStatsMap);

        dashboardForm.setJmsOrderFlowBarData(jmsOrderFlowBarData);
        dashboardForm.setJmsOtherBarData(jmsOtherBarData);

        List<JMSStatsVO> jmsDetailList = dao.getJMSDetail(con);
        

        for (int i = 0; i < jmsDetailList.size(); i++)
        {
            JMSStatsVO vo = jmsDetailList.get(i);
            
            JMSQueueSetupVO setupVO = getJMSSetup(vo.getQueueName());
            vo.setSetupVO(setupVO);

            if (setupVO.isDisplay())
            {
                if (setupVO.getCategory().equalsIgnoreCase(ORDER_CATEGORY))
                {
                    jmsOrderFlowBarData.addStat(vo.getQueueCount(), setupVO.getQueueShortName());
                    jmsOrderFlowBarData.addSetup(setupVO);
                    orderSetupMap.put(setupVO.getQueueShortName(), setupVO);
                    orderStatsMap.put(setupVO.getQueueShortName(), vo);
                } 
                else
                {
                    jmsOtherBarData.addStat(vo.getQueueCount(), setupVO.getQueueShortName());
                    jmsOtherBarData.addSetup(setupVO);
                    otherSetupMap.put(setupVO.getQueueShortName(), setupVO);
                    otherStatsMap.put(setupVO.getQueueShortName(), vo);
                }
            }
        }
    }


    private JMSQueueSetupVO getJMSSetup(String queueName) throws Exception
    {
        String queueParmName = "JMS_QUEUE_" + queueName;
        String encodedParm = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,queueParmName);        
        
        JMSQueueSetupVO queueSetupVO = new JMSQueueSetupVO();
        if (encodedParm != null)
        {
            logger.debug("Got Encoded String of " + encodedParm);
            StringTokenizer tokenizer = new StringTokenizer(encodedParm,";");
            if (tokenizer.countTokens() != 6)
            {
                logger.info("Invalid number of tokens " + tokenizer.countTokens());
                queueSetupVO.setDefaults();
            }
            
            queueSetupVO.setCategory(tokenizer.nextToken());
            queueSetupVO.setQueueName(tokenizer.nextToken());
            queueSetupVO.setQueueShortName(tokenizer.nextToken());
            queueSetupVO.setWarningThreshold(Integer.parseInt(tokenizer.nextToken()));
            queueSetupVO.setCriticalThreshold(Integer.parseInt(tokenizer.nextToken()));
            queueSetupVO.setDisplay(Boolean.valueOf(tokenizer.nextToken()));
            queueSetupVO.setMaxDisplayValue(100);
        }
        else
        {
            logger.debug("No parm for " + queueName);
            // Default to something
            queueSetupVO.setDefaults();
        }
        
        
        return queueSetupVO;
    }

}
