package com.ftd.dashboard.bo;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;


public abstract class BaseDashboardBO
{
    private static Logger logger = new Logger("com.ftd.dashboard.bo.BaseDashboardBO");

    private ConfigurationUtil cu;
    
    public BaseDashboardBO()
    {
    }


    /**
     * Get the configurationUtil instance.  This is lazily initialized.
     * @return
     * @throws Exception
     */
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }



}
