package com.ftd.dashboard.dao;

import com.ftd.dashboard.vo.OracleStatsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;

public class OracleDashboardDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public OracleDashboardDAO()
    {
    }

    /**
     *    PROCEDURE get_all_stats (out_database_cur OUT types.ref_cursor,
     *                      out_sessions_cur OUT types.ref_cursor,
     *                      out_gcremaster_cur OUT types.ref_cursor,
     *                      out_logfilesync_cur OUT types.ref_cursor,
     *                      out_cache_eff_cur OUT types.ref_cursor,
     *                      out_transactions_cur OUT types.ref_cursor,
     *                      out_usercalls_cur OUT types.ref_cursor,
     *                      out_memory_cur OUT types.ref_cursor);
     * @param conn
     * @param maxSecondsBack
     * @return
     * @throws Exception
     */
    public OracleStatsVO getAllOracleStats(Connection conn) throws Exception 
    {
        OracleStatsVO vo;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("get_all_stats");
        dataRequest.setInputParams(inputParms);

        
        HashMap cursorMap = (HashMap)DataAccessUtil.getInstance().execute(dataRequest);
        CachedResultSet databaseResults = (CachedResultSet) cursorMap.get("OUT_DATABASE_CUR");                          
        CachedResultSet sessionResults = (CachedResultSet) cursorMap.get("OUT_SESSIONS_CUR");                          
        CachedResultSet gcremasterResults = (CachedResultSet) cursorMap.get("OUT_GCREMASTER_CUR");                          
        CachedResultSet logfileresyncResults = (CachedResultSet) cursorMap.get("OUT_LOGFILESYNC_CUR");                          
        CachedResultSet cacheEffResults = (CachedResultSet) cursorMap.get("OUT_CACHE_EFF_CUR");                          
        CachedResultSet transactionResults = (CachedResultSet) cursorMap.get("OUT_TRANSACTIONS_CUR");                          
        CachedResultSet usercallResults = (CachedResultSet) cursorMap.get("OUT_USERCALLS_CUR");                          
        CachedResultSet memoryResults = (CachedResultSet) cursorMap.get("OUT_MEMORY_CUR");                          
        CachedResultSet loadResults = (CachedResultSet) cursorMap.get("OUT_LOADAVG_CUR");                          
        
        vo = new OracleStatsVO();
        
        while ( databaseResults != null && databaseResults.next() ) 
        {
            /**
             * SELECT get_raw_database_stat('lock count') lock_count,
             *        get_raw_database_stat('old lock count') old_lock_count,
             *        'ZEUS_DATA_VK' data_diskgroup_name,
             *        get_raw_database_stat('free megabytes in ZEUS_DATA_VK') data_diskgroup_free_mbytes,
             *        'ZEUS_FLASH' flash_diskgroup_name,
             *        get_raw_database_stat('free megabytes in ZEUS_FLASH') flash_diskgroup_free_mbytes,
             *        get_raw_database_stat('tablespace percent free') smallest_tablespace_pct_free
             * FROM   dual;
             */
            vo.setLockCount(getNonNullLongValue(databaseResults,"LOCK_COUNT"));
            vo.setOldLockCount(getNonNullLongValue(databaseResults,"OLD_LOCK_COUNT"));
            vo.setDataDiskgroupName(databaseResults.getString("DATA_DISKGROUP_NAME"));
            vo.setDataDiskgroupFreeSpace(getNonNullLongValue(databaseResults,"DATA_DISKGROUP_FREE_MBYTES"));
            vo.setFlashDiskgroupName(databaseResults.getString("FLASH_DISKGROUP_NAME"));
            vo.setFlashDiskgroupFreeSpace(getNonNullLongValue(databaseResults,"FLASH_DISKGROUP_FREE_MBYTES"));
            vo.setSmallestTablespacePctFree(getNonNullDoubleValue(databaseResults,"SMALLEST_TABLESPACE_PCT_FREE"));
        }
        
        while ( sessionResults != null && sessionResults.next() ) 
        {
            /**
             * SELECT host_name, get_raw_instance_stat('sessions per instance',inst_id) sessions
             */
            vo.setInstanceSessions(sessionResults.getString("HOST_NAME"),
                                   getNonNullLongValue(sessionResults,"SESSIONS"));
        }
        while ( gcremasterResults != null && gcremasterResults.next() ) 
        {
            /**
             * SELECT host_name, get_instance_stat_rate('gc remaster wait events',inst_id) gc_remasters_per_sec
             */
            vo.setInstanceGCRemaster(gcremasterResults.getString("HOST_NAME"),
                                     getNonNullLongValue(gcremasterResults,"GC_REMASTERS_PER_SEC"));
        }
        while ( logfileresyncResults != null && logfileresyncResults.next() ) 
        {
            /**
             * SELECT host_name, get_instance_stat_rate('log file sync wait events',inst_id) log_file_syncs_per_sec
             */
            vo.setInstanceLogfileResync(logfileresyncResults.getString("HOST_NAME"),
                                        getNonNullLongValue(logfileresyncResults,"LOG_FILE_SYNC_AVG_WAIT_MSEC"));
        }
        while ( cacheEffResults != null && cacheEffResults.next() ) 
        {
            /**
             * SELECT a.host_name, b.global_cache_pct, b.local_cache_pct, b.noncache_pct, b.phys_reads_per_sec, b.logi_reads_per_sec
             */
            String hostName = cacheEffResults.getString("HOST_NAME");
            double globalCachePercent = getNonNullDoubleValue(cacheEffResults,("GLOBAL_CACHE_PCT"));
            double localCachePercent = getNonNullDoubleValue(cacheEffResults,("LOCAL_CACHE_PCT"));
            double noCachePercent = getNonNullDoubleValue(cacheEffResults,("NONCACHE_PCT"));
            long physicalReads = getNonNullLongValue(cacheEffResults,("PHYS_READS_PER_SEC"));
            long logicalReads = getNonNullLongValue(cacheEffResults,("LOGI_READS_PER_SEC"));
            vo.setInstanceCacheEfficiency(hostName,globalCachePercent,localCachePercent,noCachePercent,physicalReads,logicalReads);
        }
        while ( transactionResults != null && transactionResults.next() ) 
        {
            /**
             * SELECT host_name, get_instance_stat_rate('log file sync wait events',inst_id) log_file_syncs_per_sec
             */
            vo.setInstanceTransactions(transactionResults.getString("HOST_NAME"),
                                        getNonNullLongValue(transactionResults,"TRANSACTIONS_PER_SEC"));
        }
        while ( usercallResults != null && usercallResults.next() ) 
        {
            /**
             * SELECT host_name, get_instance_stat_rate('log file sync wait events',inst_id) log_file_syncs_per_sec
             */
            vo.setInstanceUsercalls(usercallResults.getString("HOST_NAME"),
                                        getNonNullLongValue(usercallResults,"USER_CALLS_PER_SEC"));
        }
        while ( memoryResults != null && memoryResults.next() ) 
        {
            /**
             * SELECT host_name, get_instance_stat_rate('log file sync wait events',inst_id) log_file_syncs_per_sec
             */
            vo.setInstanceMemoryUsed(memoryResults.getString("HOST_NAME"),
                                        getNonNullLongValue(memoryResults,"MEMORY_MBYTES"));
        }
        while ( loadResults != null && loadResults.next() ) 
        {
            vo.setInstanceLoad(loadResults.getString("HOST_NAME"),
                               getNonNullDoubleValue(loadResults,"LOAD_FACTOR"));
        }
        
        logger.debug(vo.toString());
        
        return vo;
    }
    
    private long getNonNullLongValue(CachedResultSet cachedResultSet, String name)
    {
        BigDecimal bigDecimal = cachedResultSet.getBigDecimal(name);
        if (bigDecimal != null)
        {
            return bigDecimal.longValue();
        }
        return 0;
    }
    private double getNonNullDoubleValue(CachedResultSet cachedResultSet, String name)
    {
        BigDecimal bigDecimal = cachedResultSet.getBigDecimal(name);
        if (bigDecimal != null)
        {
            return bigDecimal.doubleValue();
        }
        return 0;
    }
    
}
