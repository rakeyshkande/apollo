package com.ftd.dashboard.dao;

import com.ftd.dashboard.vo.AuthorizationStatsVO;
import com.ftd.dashboard.vo.CleanHourlyStatsVO;
import com.ftd.dashboard.vo.FloristStatsVO;
import com.ftd.dashboard.vo.JMSStatsVO;
import com.ftd.dashboard.vo.OrderServiceStatsVO;
import com.ftd.dashboard.vo.OrderStatsVO;
import com.ftd.dashboard.vo.PaymentStatsVO;
import com.ftd.dashboard.vo.ProductStatsVO;
import com.ftd.dashboard.vo.QueueDetailStatsVO;
import com.ftd.dashboard.vo.QueueStatsVO;
import com.ftd.dashboard.vo.ZipStatsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DashboardDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public DashboardDAO()
    {
    }

    public List<FloristStatsVO> getFloristStatsSummary(Connection conn,
                                             Long maxSecondsBack) throws Exception 
    {
        FloristStatsVO vo;
        List<FloristStatsVO> statsList = new ArrayList<FloristStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FLORIST_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new FloristStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setGotoFloristCount(results.getBigDecimal("GOTO_FLORIST_COUNT").longValue());
            vo.setGotoFloristSuspendCount(results.getBigDecimal("GOTO_FLORIST_SUSPEND_COUNT").longValue());
            vo.setGotoFloristShutdownCount(results.getBigDecimal("GOTO_FLORIST_SHUTDOWN_COUNT").longValue());
            vo.setMercuryShopCount(results.getBigDecimal("MERCURY_SHOP_COUNT").longValue());
            vo.setMercuryShopShutdownCount(results.getBigDecimal("MERCURY_SHOP_SHUTDOWN_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_FLORIST_STATS_SUMMARY");
        return statsList;
    }

    public List<FloristStatsVO> getFloristStatsDetail(Connection conn,
                                                Integer hoursBack) throws Exception 
    {
        FloristStatsVO vo = new FloristStatsVO();
        List<FloristStatsVO> statsList = new ArrayList<FloristStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_HOURS_BACK",  new BigDecimal(hoursBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FLORIST_STATS_DETAIL");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new FloristStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setRegion(results.getString("REGION"));
            vo.setGotoFloristCount(results.getBigDecimal("GOTO_FLORIST_COUNT").longValue());
            vo.setGotoFloristSuspendCount(results.getBigDecimal("GOTO_FLORIST_SUSPEND_COUNT").longValue());
            vo.setGotoFloristShutdownCount(results.getBigDecimal("GOTO_FLORIST_SHUTDOWN_COUNT").longValue());
            vo.setMercuryShopCount(results.getBigDecimal("MERCURY_SHOP_COUNT").longValue());
            vo.setMercuryShopShutdownCount(results.getBigDecimal("MERCURY_SHOP_SHUTDOWN_COUNT").longValue());

            statsList.add(vo);
        }
        logger.debug("Got " + statsList.size() + " values for GET_FLORIST_STATS_DETAIL");

        return statsList;
    }

    public List<QueueStatsVO> getQueueStatsSummary(Connection conn,
                                             Long maxSecondsBack) throws Exception 
    {
        QueueStatsVO vo;
        List<QueueStatsVO> statsList = new ArrayList<QueueStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_QUEUE_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new QueueStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setWorkQueueCount(results.getBigDecimal("WORK_QUEUE_COUNT").longValue());
            vo.setEmailQueueCount(results.getBigDecimal("EMAIL_QUEUE_COUNT").longValue());
            vo.setOtherQueueCount(results.getBigDecimal("OTHER_QUEUE_COUNT").longValue());
            vo.setTotalQueueCount(results.getBigDecimal("TOTAL_QUEUE_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_QUEUE_STATS_SUMMARY");
        return statsList;
    }

    public List<QueueDetailStatsVO> getQueueStatsDetail(Connection conn,
                                                        Long maxSecondsBack,
                                                        String queueList) throws Exception 
    {
        QueueDetailStatsVO vo;
        List<QueueDetailStatsVO> statsList = new ArrayList<QueueDetailStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));
        inputParms.put("IN_QUEUE_LIST",  queueList);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_QUEUE_STATS_DETAIL");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new QueueDetailStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setQueueType(results.getString("QUEUE_TYPE"));
            vo.setQueueCount(results.getBigDecimal("QUEUE_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_QUEUE_STATS_DETAIL");
        return statsList;
    }


    public List<ZipStatsVO> getZipStatsSummary(Connection conn,
                                             Long maxSecondsBack) throws Exception 
    {
        ZipStatsVO vo;
        List<ZipStatsVO> statsList = new ArrayList<ZipStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ZIP_STATS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ZipStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setZipCoveredCount(results.getBigDecimal("COVERED_ZIP_COUNT").longValue());
            vo.setZipGnaddCount(results.getBigDecimal("GNADD_ZIP_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_ZIP_STATS");
        return statsList;
    }

    public List<OrderStatsVO> getOrderStatsSummary(Connection conn,
                                             Long maxSecondsBack) throws Exception 
    {
        OrderStatsVO vo;
        List<OrderStatsVO> statsList = new ArrayList<OrderStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new OrderStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setTotalOrderCount(results.getBigDecimal("TOTAL_ORDER_COUNT").longValue());
            vo.setFloristOrderCount(results.getBigDecimal("FLORIST_ORDER_COUNT").longValue());
            vo.setVendorOrderCount(results.getBigDecimal("VENDOR_ORDER_COUNT").longValue());
            vo.setInternetOrderCount(results.getBigDecimal("INTERNET_ORDER_COUNT").longValue());
            vo.setPhoneOrderCount(results.getBigDecimal("PHONE_ORDER_COUNT").longValue());
            vo.setForecastOrderCount(results.getBigDecimal("FORECAST_ORDER_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_ORDER_STATS_SUMMARY");
        return statsList;
    }

    public List<OrderStatsVO> getOrderStatsCumulative(Connection conn,
                                             Long maxSecondsBack) throws Exception 
    {
        OrderStatsVO vo;
        List<OrderStatsVO> statsList = new ArrayList<OrderStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_STATS_CUMULATIVE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new OrderStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setTotalOrderCount(results.getBigDecimal("TOTAL_ORDER_COUNT").longValue());
            vo.setFloristOrderCount(results.getBigDecimal("FLORIST_ORDER_COUNT").longValue());
            vo.setVendorOrderCount(results.getBigDecimal("VENDOR_ORDER_COUNT").longValue());
            vo.setInternetOrderCount(results.getBigDecimal("INTERNET_ORDER_COUNT").longValue());
            vo.setPhoneOrderCount(results.getBigDecimal("PHONE_ORDER_COUNT").longValue());
            vo.setForecastOrderCount(results.getBigDecimal("FORECAST_ORDER_COUNT").longValue());

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_ORDER_STATS_SUMMARY");
        return statsList;
    }

    public Map<String,List<ProductStatsVO>> getProductStatsDetail(Connection conn,
                                                                  Integer maxProducts,
                                                        Long maxSecondsBack) throws Exception 
    {
        ProductStatsVO vo;
        List<ProductStatsVO> vendorStatsList = new ArrayList<ProductStatsVO>();
        List<ProductStatsVO> floristStatsList = new ArrayList<ProductStatsVO>();
        HashMap<String,List<ProductStatsVO>> statsMap = new HashMap<String,List<ProductStatsVO>>();

        statsMap.put("Florist",floristStatsList);
        statsMap.put("Vendor",vendorStatsList);

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_MAX_SECONDS_BACK",  new BigDecimal(maxSecondsBack));
        inputParms.put("IN_MAX_PRODUCTS",  new BigDecimal(maxProducts));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_STATS_DETAIL");
        dataRequest.setInputParams(inputParms);

        //CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        HashMap resultMap = (HashMap)DataAccessUtil.getInstance().execute(dataRequest);
        CachedResultSet floristResults = (CachedResultSet) resultMap.get("OUT_FLORIST_CUR");                          
        CachedResultSet vendorResults = (CachedResultSet) resultMap.get("OUT_VENDOR_CUR");                          

        while ( floristResults != null && floristResults.next() ) 
        {
            vo = new ProductStatsVO();

            vo.setStatTimestamp(floristResults.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setProductId(floristResults.getString("PRODUCT_ID"));
            vo.setOrderCount(floristResults.getBigDecimal("ORDER_COUNT").longValue());

            floristStatsList.add(vo);
        }
        while ( vendorResults != null && vendorResults.next() ) 
        {
            vo = new ProductStatsVO();

            vo.setStatTimestamp(vendorResults.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setProductId(vendorResults.getString("PRODUCT_ID"));
            vo.setOrderCount(vendorResults.getBigDecimal("ORDER_COUNT").longValue());

            vendorStatsList.add(vo);
        }

        logger.debug("Got " + statsMap.size() + "/" + floristStatsList.size() + "/" + vendorStatsList.size() + " values for GET_PRODUCT_STATS_DETAIL");
        
        return statsMap;
    }

    public List<JMSStatsVO> getJMSDetail(Connection conn) throws Exception
    {
        JMSStatsVO vo;
        List<JMSStatsVO> statsList = new ArrayList<JMSStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OJMS.GET_QUEUE_COUNTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new JMSStatsVO();

            vo.setStatTimestamp(results.getTimestamp("STAT_TIMESTAMP"));
            
            vo.setQueueName(results.getString("QUEUE_NAME"));
            vo.setQueueCount(results.getBigDecimal("QUEUE_COUNT").longValue());
            vo.setOldestMessage(results.getString("MIN_ENQ_TIME"));

            statsList.add(vo);
        }

        logger.debug("Got " + statsList.size() + " values for GET_JMS_STATS");
        return statsList;
    }

    public List<PaymentStatsVO> getPaymentStatsSummary(Connection conn,
                                                       Date startTime,
                                                       Date endTime) throws Exception 
    {
        PaymentStatsVO vo;
        List<PaymentStatsVO> paymentStatsList = new ArrayList<PaymentStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_START_TIMESTAMP",  new java.sql.Timestamp(startTime.getTime()));
        inputParms.put("IN_STOP_TIMESTAMP",  new java.sql.Timestamp(endTime.getTime()));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAYMENT_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet paymentResults = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( paymentResults != null && paymentResults.next() ) 
        {
            vo = new PaymentStatsVO();

            vo.setPaymentType(paymentResults.getString("PAYMENT_TYPE"));
            vo.setCount(paymentResults.getBigDecimal("PAYMENT_COUNT").longValue());

            paymentStatsList.add(vo);
        }

        logger.debug("Got " + paymentStatsList.size() + " values for GET_PAYMENT_STATS_SUMMARY");
        
        return paymentStatsList;
    }

    public List<AuthorizationStatsVO> getAuthorizationStatsSummary(Connection conn,
                                                                   Date startTime,
                                                                   Date endTime) throws Exception 
    {
        AuthorizationStatsVO vo;
        List<AuthorizationStatsVO> authorizationStatsList = new ArrayList<AuthorizationStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_START_TIMESTAMP",  new java.sql.Timestamp(startTime.getTime()));
        inputParms.put("IN_STOP_TIMESTAMP",  new java.sql.Timestamp(endTime.getTime()));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_AUTH_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet authorizationResults = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( authorizationResults != null && authorizationResults.next() ) 
        {
            vo = new AuthorizationStatsVO();

            vo.setCardType(authorizationResults.getString("CREDIT_CARD_TYPE"));
            vo.setApprovals(authorizationResults.getBigDecimal("APPROVAL_COUNT").longValue());
            vo.setDeclines(authorizationResults.getBigDecimal("DECLINE_COUNT").longValue());

            authorizationStatsList.add(vo);
        }

        logger.debug("Got " + authorizationStatsList.size() + " values for GET_AUTH_STATS_SUMMARY");
        
        return authorizationStatsList;
    }

    public List<OrderServiceStatsVO> getOrderServiceStatsSummary(Connection conn,
                                                                 Date startTime,
                                                                 Date endTime,
                                                                 String groupBy
                                                                 ) throws Exception
    {
        OrderServiceStatsVO vo;
        List<OrderServiceStatsVO> orderServiceStatsList = new ArrayList<OrderServiceStatsVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_START_TIMESTAMP",  new java.sql.Timestamp(startTime.getTime()));
        inputParms.put("IN_STOP_TIMESTAMP",  new java.sql.Timestamp(endTime.getTime()));
        inputParms.put("IN_GROUP_BY", groupBy);
        inputParms.put("IN_FILTER_ON",  "");

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_OS_STATS_SUMMARY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet statsResults = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( statsResults != null && statsResults.next() )
        {
            vo = new OrderServiceStatsVO();

            vo.setActionType(statsResults.getString("ACTION_TYPE"));
            vo.setClientId(statsResults.getString("CLIENT_ID"));
            vo.setNode(statsResults.getString("NODE"));
            vo.setStatusCode(statsResults.getString("STATUS_CODE"));
            vo.setCount(statsResults.getBigDecimal("STAT_COUNT").longValue());
            vo.setStatsTimestamp(statsResults.getTimestamp("STAT_TIMESTAMP"));

            orderServiceStatsList.add(vo);
        }

        logger.debug("Got " + orderServiceStatsList.size() + " values for GET_OS_STATS_SUMMARY");

        return orderServiceStatsList;
    }

    public void insertAuthorizationStats(Connection con, AuthorizationStatsVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("CREATE_AUTHORIZATION_STATS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_APPROVAL_COUNT", new BigDecimal(vo.getApprovals()));
        inputParams.put("IN_DECLINE_COUNT", new BigDecimal(vo.getDeclines()));
        inputParams.put("IN_AUTH_CLIENT", vo.getAuthClient());
        inputParams.put("IN_SERVER_NAME", vo.getAuthServer());
        inputParams.put("IN_CREDIT_CARD_TYPE", vo.getCardType());
        java.sql.Timestamp statsDate = new java.sql.Timestamp(vo.getStatsDate().getTime());
        inputParams.put("IN_STAT_TIMESTAMP", statsDate);

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public void insertOrderServiceStats(Connection con, OrderServiceStatsVO vo) throws Exception 
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("CREATE_ORDER_SERVICE_STATS");
        
        HashMap inputParams = new HashMap();
        inputParams.put("IN_NODE", vo.getNode());
        inputParams.put("IN_CLIENT_ID", vo.getClientId());
        inputParams.put("IN_ACTION_TYPE", vo.getActionType());
        inputParams.put("IN_STAT_COUNT", new BigDecimal(vo.getCount()));
        inputParams.put("IN_STATUS_CODE", vo.getStatusCode());
        java.sql.Timestamp statsDate = new java.sql.Timestamp(vo.getStatsTimestamp().getTime());
        inputParams.put("IN_STAT_TIMESTAMP", statsDate);

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    
    // Get the current and previous hour stats for florist 
    public Map<String,List<Double>> getFloristTableStats(Connection conn) throws Exception
    {
    	FloristStatsVO vo=null;
    	ZipStatsVO zvo = null;
        
        Map<String,List<Double>> hourlyStats = new HashMap<String,List<Double>>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FLORIST_TABLE_STATS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map hourStatsMap = (Map)dataAccessUtil.execute(dataRequest);
        Iterator it = hourStatsMap.entrySet().iterator();
        CachedResultSet rs = new CachedResultSet();
        
        while(it.hasNext()){
        	Map.Entry pair = (Map.Entry)it.next();
        	rs = (CachedResultSet) hourStatsMap.get(pair.getKey());
        	List<Double> hourlyStatsList = new ArrayList<Double>();
        	
        	 while(rs != null && rs.next() )
             {
             	  vo = new FloristStatsVO();
                  vo.setGotoFloristCount(rs.getBigDecimal("GOTO_FLORIST_COUNT").longValue());
                  vo.setGotoFloristShutdownCount(rs.getBigDecimal("GOTO_FLORIST_SHUTDOWN_COUNT").longValue());
                  vo.setMercuryShopCount(rs.getBigDecimal("MERCURY_SHOP_COUNT").longValue());
                  vo.setMercuryShopShutdownCount(rs.getBigDecimal("MERCURY_SHOP_SHUTDOWN_COUNT").longValue());
                 
                  hourlyStatsList.add((vo.getPercentGotoFloristShutdown()));
                  hourlyStatsList.add(vo.getPercentMercuryShopSuspend());
                  
                  zvo = new ZipStatsVO();
                  zvo.setZipCoveredCount(rs.getBigDecimal("COVERED_ZIP_COUNT").longValue());
                  zvo.setZipGnaddCount(rs.getBigDecimal("GNADD_ZIP_COUNT").longValue());
                  hourlyStatsList.add(zvo.getPercentGnaddCount());
                  
                  hourlyStats.put(rs.getString("v_hour"), hourlyStatsList);
             }
        	 rs.reset();
        }
       
     logger.info("The Hourly Stats for :: FLORIST STATUS \n " +hourlyStats);   
     return hourlyStats;   
     
     }
    
    // Get the current and previous hour stats for order flow
    public Map<String,List<Long>> getOrderTableStats(Connection conn) throws Exception
    {
        Map<String,List<Long>> hourlyStats = new HashMap<String,List<Long>>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_FLOW_TABLE_STATS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map hourStatsMap = (Map)dataAccessUtil.execute(dataRequest);
        Iterator it = hourStatsMap.entrySet().iterator();
        CachedResultSet rs = new CachedResultSet();
        
        while(it.hasNext()){
        	Map.Entry pair = (Map.Entry)it.next();
        	rs = (CachedResultSet) hourStatsMap.get(pair.getKey());
        	List<Long> hourlyStatsList = new ArrayList<Long>();
        	 while(rs != null && rs.next() )
             {
        		 hourlyStatsList.add(rs.getBigDecimal("internet_order_count").longValue());
                 hourlyStatsList.add(rs.getBigDecimal("phone_order_count").longValue());
                  
                  hourlyStats.put(rs.getString("v_hour"), hourlyStatsList);
             }
        	 rs.reset();
        }
        
        logger.info("The Hourly Stats for :: ORDER FLOW \n " +hourlyStats);
        
        return hourlyStats;   
     
     }
    
 // Get the current and previous hour stats for order flow
    public Map<String,List<Long>> getQueueTableStats(Connection conn) throws Exception
    {
    
        Map<String,List<Long>> hourlyStats = new HashMap<String,List<Long>>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_QUEUE_TABLE_STATS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map hourStatsMap = (Map)dataAccessUtil.execute(dataRequest);
        Iterator it = hourStatsMap.entrySet().iterator();
        CachedResultSet rs = new CachedResultSet();
        
        while(it.hasNext()){
        	Map.Entry pair = (Map.Entry)it.next();
        	rs = (CachedResultSet) hourStatsMap.get(pair.getKey());
        	List<Long> hourlyStatsList = new ArrayList<Long>();
        	 
        	 while(rs != null && rs.next() )
             {
                 hourlyStatsList.add(rs.getBigDecimal("WORK_QUEUE_COUNT").longValue());
                 hourlyStatsList.add(rs.getBigDecimal("EMAIL_QUEUE_COUNT").longValue());
                 hourlyStatsList.add(rs.getBigDecimal("OTHER_QUEUE_COUNT").longValue());
                 
                 hourlyStats.put(rs.getString("v_hour"), hourlyStatsList);
             }
        	 rs.reset();
        }
        
        logger.info("The Hourly Stats for :: QUEUE STATUS \n " +hourlyStats);
        
        return hourlyStats;   
     
     }
    
    
    
    public Map<String,List<Long>> getInventoryTableStats(Connection conn) throws Exception
    {
    
        Map<String,List<Long>> hourlyStats = new HashMap<String,List<Long>>();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_INVENTORY_TABLE_STATS");

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map hourStatsMap = (Map)dataAccessUtil.execute(dataRequest);
        Iterator it = hourStatsMap.entrySet().iterator();
        CachedResultSet rs = new CachedResultSet();
        
        while(it.hasNext()){
        	Map.Entry pair = (Map.Entry)it.next();
        	rs = (CachedResultSet) hourStatsMap.get(pair.getKey());
        	List<Long> hourlyStatsList = new ArrayList<Long>();
        	 
        	 while(rs != null && rs.next() )
             {
                 hourlyStatsList.add(rs.getBigDecimal("florist_order_count").longValue());
                 hourlyStatsList.add(rs.getBigDecimal("vendor_order_count").longValue());
                 hourlyStats.put(rs.getString("v_hour"), hourlyStatsList);
             }
        	 
        	 rs.reset();
        }
        
        logger.info("The Hourly Stats for :: INVENTORY STATUS \n " +hourlyStats);
        
        return hourlyStats;   
     
     }
}
