package com.ftd.dashboard.tables;

public class InventoryTableVO {
	private String attributeName;
	private PercentageRoundVO currentHourPercentage;
	private PercentageRoundVO previousHourPercentage;
	
	
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public PercentageRoundVO getCurrentHourPercentage() {
		return currentHourPercentage;
	}
	public void setCurrentHourPercentage(PercentageRoundVO currentHourPercentage) {
		this.currentHourPercentage = currentHourPercentage;
	}
	public PercentageRoundVO getPreviousHourPercentage() {
		return previousHourPercentage;
	}
	public void setPreviousHourPercentage(PercentageRoundVO previousHourPercentage) {
		this.previousHourPercentage = previousHourPercentage;
	}
	
}
