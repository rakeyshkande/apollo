package com.ftd.dashboard.tables;

public class PercentageRoundVO {

	double dvalue;
	int ivalue;
	boolean isDouble;
	public PercentageRoundVO(){
		
	}
	public double getDvalue() {
		return dvalue;
	}
	public void setDvalue(double dvalue) {
		this.dvalue = dvalue;
	}
	public int getIvalue() {
		return ivalue;
	}
	public void setIvalue(int ivalue) {
		this.ivalue = ivalue;
	}
	public boolean getIsDouble() {
		return isDouble;
	}
	public void setIsDouble(boolean flag) {
		this.isDouble = flag;
	}
	
}
