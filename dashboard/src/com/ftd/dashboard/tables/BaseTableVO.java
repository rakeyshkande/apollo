package com.ftd.dashboard.tables;

public class BaseTableVO {
	private String attributeName;
	private int currentHourPercentage;
	private int previousHourPercentage;
	private int warningThreshold;
	private int criticalThreshold;
	
	public int getWarningThreshold() {
		return warningThreshold;
	}
	public void setWarningThreshold(int warningThreshold) {
		this.warningThreshold = warningThreshold;
	}
	public int getCriticalThreshold() {
		return criticalThreshold;
	}
	public void setCriticalThreshold(int criticalThreshold) {
		this.criticalThreshold = criticalThreshold;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public int getCurrentHourPercentage() {
		return currentHourPercentage;
	}
	public void setCurrentHourPercentage(int currentHourPercentage) {
		this.currentHourPercentage = currentHourPercentage;
	}
	public int getPreviousHourPercentage() {
		return previousHourPercentage;
	}
	public void setPreviousHourPercentage(int previousHourPercentage) {
		this.previousHourPercentage = previousHourPercentage;
	}
	
}
