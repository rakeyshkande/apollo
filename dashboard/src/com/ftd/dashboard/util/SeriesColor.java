package com.ftd.dashboard.util;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.osp.utilities.plugins.Logger;

import java.awt.Color;

import java.lang.reflect.Field;

import java.util.HashMap;
import java.util.Map;

public class SeriesColor
{
    private static Logger logger  = new Logger("com.ftd.dashboard.util.SeriesColor");

    private final static String SERIES_COLOR = "SERIES_COLOR_";
    private Map<String,Color> colorMap;
    private ConfigurationUtil cu;
    
    private static SeriesColor instance = null;
    
    private SeriesColor()
    {
        colorMap = new HashMap<String,Color>();
    }
    
    public static SeriesColor getInstance()
    {
        if (instance == null)
        {
            instance = new SeriesColor();
        }
        
        return instance;
    }
    
    public Color getSeriesColor(int series)
    {
        Color seriesColor = null;
        
        String seriesColorName = "";
        try
        {
            seriesColorName = getConfigurationUtil().getFrpGlobalParm(DashboardConstants.DASHBOARD_CONFIG_CONTEXT,SERIES_COLOR + series);

            if (seriesColorName != null)
            {
                seriesColor = colorMap.get(seriesColorName);
                    
                if (seriesColor == null)
                {
                    seriesColor = getColor(seriesColorName);
                    if (seriesColor != null)
                    {
                        colorMap.put(seriesColorName,seriesColor);
                    }
                }
            }
        }
        catch (Exception nfe)
        {
            logger.error("Exception " + seriesColorName,nfe);
        }
        
        if (seriesColor == null)
        {
            seriesColor = Color.RED;
        }

        return seriesColor;
    }
    
    /**
     * Get the configurationUtil instance.  This is lazily initialized.
     * @return
     * @throws Exception
     */
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }

    protected Color getColor(String colorName)
    {
        try
        {
            // Find the field and value of colorName
            Field field = Class.forName("java.awt.Color").getField(colorName);
            return (Color)field.get(null);
        } catch (Exception e)
        {
            return null;
        }
    }
}
