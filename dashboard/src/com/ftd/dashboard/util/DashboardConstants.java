package com.ftd.dashboard.util;

import java.awt.Color;

public interface DashboardConstants
{
    public final static String PROPERTY_FILE = "dashboard-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    public static final String DASHBOARD_CONFIG_CONTEXT = "DASHBOARD_CONFIG";
    public static final String SECURE_CONFIG_CONTEXT = "DASHBOARD_CONFIG";
    
    public final static int RANGE_LAST_HOUR    = 1;
    public final static int RANGE_LAST_DAY     = 2;
    public final static int RANGE_YESTERDAY    = 3;
    public final static int RANGE_CUSTOM       = 0;
    
    public static int CRITICAL_THRESHOLD = 2;
    public static int WARNING_THRESHOLD  = 1;
    public static int NORMAL_THRESHOLD   = 0;
    
    public final static String THRESHOLD        = "threshold";
    public final static String THRESHOLD_TYPE   = "thresholdType";
    public final static String THRESHOLD_TYPE_REVERSE = "reverse";
    public final static String LOCKS            = "locks";
    public final static String AGED_LOCKS       = "agedlocks";
    public final static String PHYSICAL_READS   = "physicalreads";
    public final static String LOGICAL_READS    = "logicalreads";
    public final static String TRANSACTIONS     = "transactions";
    public final static String USER_CALLS       = "usercalls";
    public final static String LOAD             = "load";
    public final static String MEMORY_USED      = "memoryused";
    public final static String SESSIONS         = "sessions";
    public final static String GC_REMASTER_WAIT = "gcremasterwait";
    public final static String LOG_FILE_SYNC    = "logfilesync";
    public final static String DATA_DISKGROUP   = "datadiskgroup";
    public final static String FLASH_DISKGROUP  = "flashdiskgroup";
    public final static String TABLESPACE_FREE  = "tablespacefree";
    public final static String CACHE_LOCAL      = "cache_local";
    public final static String CACHE_GLOBAL     = "cache_global";
    public final static String CACHE_DISK       = "cache_disk";

    public final static Color  COLOR_CYAN = new Color(0x639893);
    
    public final static String SMALL_GRAPH_SIZE = "smallGraphSize";
    public final static String LARGE_GRAPH_SIZE = "largeGraphSize";
    public final static String SHOW_MEMORY_GRAPH          = "showMemoryGraph";
    public final static String SHOW_USER_CALLS_GRAPH      = "showUserCallsGraph";
    public final static String SHOW_TRANSACTIONS_GRAPH    = "showTransactionsGraph";
    public final static String SHOW_LOGICAL_READS_GRAPH   = "showLogicalReadsGraph";
    public final static String SHOW_PHYSICAL_READS_GRAPH  = "showPhysicalReadsGraph";
    public final static String SHOW_LOAD_GRAPH            = "showLoadGraph";
    public final static String SHOW_DATA_DISKGROUP_GRAPH  = "showDataDiskgroupGraph";
    public final static String SHOW_FLASH_DISKGROUP_GRAPH = "showFlashDiskgroupGraph";
    public final static String SHOW_TABLESPACE_FREE_GRAPH = "showTablespaceFreeGraph";

    public final static String CHECKED      = "checked";
    public final static String CLIENT_ID    = "client_id";
    public final static String STATUS_CODE  = "status_code";
    public final static String ACTION_TYPE  = "action_type";
    public final static String NODE         = "node";
    
    public final static String FLORIST_STATUS_WARN_PERCENTAGES         = "FLORIST_STATUS_WARN_PERCENTAGES";
    public final static String FLORIST_STATUS_CRIT_PERCENTAGES         = "FLORIST_STATUS_CRIT_PERCENTAGES";
    
    public final static String ORDER_FLOW_THRESHOLD_PERCENTAGES         = "ORDER_FLOW_THRESHOLD_PERCENTAGES";
    
    public final static String QUEUE_STATUS_WARN_PERCENTAGES         = "QUEUE_STATUS_WARN_PERCENTAGES";
    public final static String QUEUE_STATUS_CRIT_PERCENTAGES         = "QUEUE_STATUS_CRIT_PERCENTAGES";
    
}
