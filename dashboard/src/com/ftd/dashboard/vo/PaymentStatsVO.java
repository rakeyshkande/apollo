package com.ftd.dashboard.vo;

import com.ftd.dashboard.tables.PercentageRoundVO;

public class PaymentStatsVO
{
    private String paymentType;
    private long   count;
    private PercentageRoundVO  paymentTypePercentage;
    
    public PaymentStatsVO()
    {
    }
    
    public PaymentStatsVO(String paymentType, long count)
    {
        this.paymentType = paymentType;
        this.count = count;
    }

    public void setPaymentType(String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getPaymentType()
    {
        return paymentType;
    }

    public void setCount(long count)
    {
        this.count = count;
    }

    public long getCount()
    {
        return count;
    }
    
    public String toString()
    {
        return Long.toString(count);
    }

	public PercentageRoundVO getPaymentTypePercentage() {
		return paymentTypePercentage;
	}

	public void setPaymentTypePercentage(PercentageRoundVO paymentTypePercentage) {
		this.paymentTypePercentage = paymentTypePercentage;
	}
    
}
