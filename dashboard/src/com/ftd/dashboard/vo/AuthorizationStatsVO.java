package com.ftd.dashboard.vo;

import java.text.DecimalFormat;

import java.util.Date;

public class AuthorizationStatsVO
{
    private String authServer;
    private String authClient;
    private String cardType;
    private long   approvals;
    private long   declines;
    private Date   statsDate;
    
    private static DecimalFormat decimalFormat = new DecimalFormat("###%");
    
    public AuthorizationStatsVO()
    {
    }

    public void setAuthServer(String authServer)
    {
        this.authServer = authServer;
    }

    public String getAuthServer()
    {
        return authServer;
    }

    public void setAuthClient(String authClient)
    {
        this.authClient = authClient;
    }

    public String getAuthClient()
    {
        return authClient;
    }

    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public String getCardType()
    {
        return cardType;
    }

    public void setApprovals(long approvals)
    {
        this.approvals = approvals;
    }

    public long getApprovals()
    {
        return approvals;
    }

    public void setDeclines(long declines)
    {
        this.declines = declines;
    }

    public long getDeclines()
    {
        return declines;
    }
    
    public long getTotalAuths()
    {
        return approvals + declines;
    }
    
    public String getDeclinePercent()
    {
        double declinePercent;
        declinePercent = (double) declines / (double) (approvals + declines);
        
        String decimalDisplayString = decimalFormat.format(declinePercent);
            
        return decimalDisplayString;
    }

    public void addApproval()
    {
        approvals++;
    }
    
    public void addDecline()
    {
        declines++;
    }

    public void setStatsDate(Date statsDate)
    {
        this.statsDate = statsDate;
    }

    public Date getStatsDate()
    {
        return statsDate;
    }
}
