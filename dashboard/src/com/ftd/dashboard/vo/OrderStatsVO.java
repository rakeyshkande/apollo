package com.ftd.dashboard.vo;

import java.util.Date;

public class OrderStatsVO
{
    private Date statTimestamp;
    private long totalOrderCount;
    private long floristOrderCount;
    private long vendorOrderCount;
    private long internetOrderCount;
    private long phoneOrderCount;
    private long forecastOrderCount;

    public OrderStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setTotalOrderCount(long totalOrderCount)
    {
        this.totalOrderCount = totalOrderCount;
    }

    public long getTotalOrderCount()
    {
        return totalOrderCount;
    }

    public void setFloristOrderCount(long floristOrderCount)
    {
        this.floristOrderCount = floristOrderCount;
    }

    public long getFloristOrderCount()
    {
        return floristOrderCount;
    }

    public void setVendorOrderCount(long vendorOrderCount)
    {
        this.vendorOrderCount = vendorOrderCount;
    }

    public long getVendorOrderCount()
    {
        return vendorOrderCount;
    }

    public void setInternetOrderCount(long internetOrderCount)
    {
        this.internetOrderCount = internetOrderCount;
    }

    public long getInternetOrderCount()
    {
        return internetOrderCount;
    }

    public void setPhoneOrderCount(long phoneOrderCount)
    {
        this.phoneOrderCount = phoneOrderCount;
    }

    public long getPhoneOrderCount()
    {
        return phoneOrderCount;
    }

    public void setForecastOrderCount(long forecastOrderCount)
    {
        this.forecastOrderCount = forecastOrderCount;
    }

    public long getForecastOrderCount()
    {
        return forecastOrderCount;
    }
}
