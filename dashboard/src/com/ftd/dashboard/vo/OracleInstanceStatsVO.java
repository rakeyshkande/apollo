package com.ftd.dashboard.vo;

import java.util.Date;

public class OracleInstanceStatsVO
{
    private String hostName;
    private long   gcremaster = 0;
    private long   sessions = 0;
    private long   logfileresync = 0;
    private double globalCacheReadPercent = 0;
    private double localCacheReadPercent = 0;
    private double noCacheReadPercent = 0;
    private long   physicalReads = 0;
    private long   logicalReads = 0;
    private long   transactions = 0;
    private long   userCalls = 0;
    private long   memoryUsed = 0;
    private double load = 0;
    
    
    public double getLoad()
    {
        return load;
    }

    public void setLoad(double load)
    {
        this.load = load;
    }

    public long getGcremaster()
    {
        return gcremaster;
    }

    public void setGcremaster(long gcremaster)
    {
        this.gcremaster = gcremaster;
    }

    public long getLogfileresync()
    {
        return logfileresync;
    }

    public void setLogfileresync(long logfileresync)
    {
        this.logfileresync = logfileresync;
    }

    public double getGlobalCacheReadPercent()
    {
        return globalCacheReadPercent;
    }

    public void setGlobalCacheReadPercent(double globalCacheReadPercent)
    {
        this.globalCacheReadPercent = globalCacheReadPercent;
    }

    public double getLocalCacheReadPercent()
    {
        return localCacheReadPercent;
    }

    public void setLocalCacheReadPercent(double localCacheReadPercent)
    {
        this.localCacheReadPercent = localCacheReadPercent;
    }

    public double getNoCacheReadPercent()
    {
        return noCacheReadPercent;
    }

    public void setNoCacheReadPercent(double noCacheReadPercent)
    {
        this.noCacheReadPercent = noCacheReadPercent;
    }

    public long getPhysicalReads()
    {
        return physicalReads;
    }

    public void setPhysicalReads(long physicalReads)
    {
        this.physicalReads = physicalReads;
    }

    public long getLogicalReads()
    {
        return logicalReads;
    }

    public void setLogicalReads(long logicalReads)
    {
        this.logicalReads = logicalReads;
    }

    public long getTransactions()
    {
        return transactions;
    }

    public void setTransactions(long transactions)
    {
        this.transactions = transactions;
    }

    public long getUserCalls()
    {
        return userCalls;
    }

    public void setUserCalls(long userCalls)
    {
        this.userCalls = userCalls;
    }

    public long getMemoryUsed()
    {
        return memoryUsed;
    }

    public void setMemoryUsed(long memoryUsed)
    {
        this.memoryUsed = memoryUsed;
    }

    public String getHostName()
    {
        return hostName;
    }

    public void setHostName(String hostName)
    {
        this.hostName = hostName;
    }


    public OracleInstanceStatsVO(String hostName)
    {
        this.hostName = hostName;
    }

    public long getSessions()
    {
        return sessions;
    }

    public void setSessions(long sessions)
    {
        this.sessions = sessions;
    }

    public void toString(StringBuilder sb)
    {
        sb.append("\nhostName = ");
        sb.append(hostName);
        sb.append("\ngcremaster = ");
        sb.append(gcremaster);
        sb.append("\nsessions = ");
        sb.append(sessions);
        sb.append("\nlogfileresync = ");
        sb.append(logfileresync);
        sb.append("\nglobalCacheReadPercent = ");
        sb.append(globalCacheReadPercent);
        sb.append("\nlocalCacheReadPercent = ");
        sb.append(localCacheReadPercent);
        sb.append("\nnoCacheReadPercent = ");
        sb.append(noCacheReadPercent);
        sb.append("\nphysicalReads = ");
        sb.append(physicalReads);
        sb.append("\nlogicalReads = ");
        sb.append(logicalReads);
        sb.append("\ntransactions = ");
        sb.append(transactions);
        sb.append("\nuserCalls = ");
        sb.append(userCalls);
        sb.append("\nmemoryUsed = ");
        sb.append(memoryUsed);
        sb.append("\nload = ");
        sb.append(load);
    }
}
