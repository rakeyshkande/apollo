package com.ftd.dashboard.vo;

import java.util.Date;

public class ProductStatsVO
{
    private Date   statTimestamp;
    private String productId;
    private long   orderCount;
    
    public ProductStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setOrderCount(long orderCount)
    {
        this.orderCount = orderCount;
    }

    public long getOrderCount()
    {
        return orderCount;
    }
}
