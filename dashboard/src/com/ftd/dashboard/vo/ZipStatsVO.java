package com.ftd.dashboard.vo;

import java.util.Date;

public class ZipStatsVO
{
    private Date statTimestamp;
    private long zipCoveredCount;
    private long zipGnaddCount;
    
    public ZipStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setZipCoveredCount(long zipCoveredCount)
    {
        this.zipCoveredCount = zipCoveredCount;
    }

    public long getZipCoveredCount()
    {
        return zipCoveredCount;
    }

    public void setZipGnaddCount(long zipGnaddCount)
    {
        this.zipGnaddCount = zipGnaddCount;
    }

    public long getZipGnaddCount()
    {
        return zipGnaddCount;
    }
    public double getPercentGnaddCount()
    {
        double percentGFShutdown = (double)(100 * zipGnaddCount) / (double)zipCoveredCount;
        return percentGFShutdown;
    }
}
