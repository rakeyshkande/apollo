package com.ftd.dashboard.vo;

import java.io.Serializable;

public class JMSQueueSetupVO extends ThresholdVO
{
    private String category;
    private String queueShortName;
    private boolean display;
    
    public JMSQueueSetupVO()
    {
    }

    public void setQueueName(String queueName)
    {
        this.name = queueName;
    }

    public String getQueueName()
    {
        return name;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getCategory()
    {
        return category;
    }

    public void setQueueShortName(String queueShortName)
    {
        this.queueShortName = queueShortName;
    }

    public String getQueueShortName()
    {
        return queueShortName;
    }

    
    public void setDisplay(boolean display)
    {
        this.display = display;
    }

    public boolean isDisplay()
    {
        return display;
    }

    public void setDefaults()
    {
        name = "NULL";
        category = "Other";
        queueShortName = "NULL";
        warningThreshold = 10000;
        criticalThreshold = 50000;
        display = false;
        maxDisplayValue = 100000;
    }

}
