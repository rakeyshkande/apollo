package com.ftd.dashboard.vo;

import java.util.Date;
import java.util.HashMap;

public class QueueDetailStatsVO
{
    private Date statTimestamp;
    private String queueType;
    private long queueCount;
    
    
    public QueueDetailStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setQueueType(String queueType)
    {
        this.queueType = queueType;
    }

    public String getQueueType()
    {
        return queueType;
    }

    public void setQueueCount(long queueCount)
    {
        this.queueCount = queueCount;
    }

    public long getQueueCount()
    {
        return queueCount;
    }
}
