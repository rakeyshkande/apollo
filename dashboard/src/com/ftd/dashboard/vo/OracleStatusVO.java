package com.ftd.dashboard.vo;

import java.util.Map;

import com.ftd.dashboard.util.DashboardConstants;

/**
 * Value Object for holding the current status of the Oracle Cluster.
 * Status of different logical parts of the cluster and each instance are held.
 * @author mcampbell
 *
 */
public class OracleStatusVO
{
    private int overallStatus;
    private int storageStatus;

    private Map<String,OracleInstanceStatusVO> oracleInstanceStatusMap;
    
    public OracleStatusVO()
    {
        this.overallStatus = DashboardConstants.NORMAL_THRESHOLD;
        this.storageStatus = DashboardConstants.NORMAL_THRESHOLD;
    }

    public int getOverallStatus()
    {
        return overallStatus;
    }

    public void setOverallStatus(int overallStatus)
    {
        this.overallStatus = overallStatus;
    }

    public int getStorageStatus()
    {
        return storageStatus;
    }

    public void setStorageStatus(int storageStatus)
    {
        this.storageStatus = storageStatus;
    }

    public Map<String,OracleInstanceStatusVO> getOracleInstanceStatusMap()
    {
        return oracleInstanceStatusMap;
    }

    public void setOracleInstanceStatusMap(
            Map<String,OracleInstanceStatusVO> oracleInstanceStatusMap)
    {
        this.oracleInstanceStatusMap = oracleInstanceStatusMap;
    }
    
    
}
