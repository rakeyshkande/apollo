package com.ftd.dashboard.vo;

import java.util.Date;

public class JMSStatsVO
{
    private Date   statTimestamp;
    private String queueName;
    private long   queueCount;
    private String oldestMessage;
    
    private ThresholdVO setupVO;

    public JMSStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setQueueName(String queueName)
    {
        this.queueName = queueName;
    }

    public String getQueueName()
    {
        return queueName;
    }

    public void setQueueCount(long queueCount)
    {
        this.queueCount = queueCount;
    }

    public long getQueueCount()
    {
        return queueCount;
    }

    public void setOldestMessage(String oldestMessage)
    {
        this.oldestMessage = oldestMessage;
    }

    public String getOldestMessage()
    {
        return oldestMessage;
    }

    public void setSetupVO(ThresholdVO setupVO)
    {
        this.setupVO = setupVO;
    }

    public ThresholdVO getSetupVO()
    {
        return setupVO;
    }
}
