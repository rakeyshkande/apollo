package com.ftd.dashboard.vo;

import java.util.Date;

public class FloristStatsVO
{
    private Date statTimestamp;
    private String region;
    private long gotoFloristCount;
    private long gotoFloristSuspendCount;
    private long gotoFloristShutdownCount;
    private long mercuryShopCount;
    private long mercuryShopShutdownCount;
    
    
    public FloristStatsVO()
    {
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }

    public void setGotoFloristCount(long gotoFloristCount)
    {
        this.gotoFloristCount = gotoFloristCount;
    }

    public long getGotoFloristCount()
    {
        return gotoFloristCount;
    }

    public void setGotoFloristSuspendCount(long gotoFloristSuspendCount)
    {
        this.gotoFloristSuspendCount = gotoFloristSuspendCount;
    }

    public long getGotoFloristSuspendCount()
    {
        return gotoFloristSuspendCount;
    }

    public void setGotoFloristShutdownCount(long gotoFloristShutdownCount)
    {
        this.gotoFloristShutdownCount = gotoFloristShutdownCount;
    }

    public long getGotoFloristShutdownCount()
    {
        return gotoFloristShutdownCount;
    }

    public void setMercuryShopCount(long mercuryShopCount)
    {
        this.mercuryShopCount = mercuryShopCount;
    }

    public long getMercuryShopCount()
    {
        return mercuryShopCount;
    }

    public void setMercuryShopShutdownCount(long mercuryShopShutdownCount)
    {
        this.mercuryShopShutdownCount = mercuryShopShutdownCount;
    }

    public long getMercuryShopShutdownCount()
    {
        return mercuryShopShutdownCount;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public String getRegion()
    {
        return region;
    }

    public double getPercentGotoFloristShutdown()
    {
        double percentGFShutdown = (double)(100 * gotoFloristShutdownCount) / (double)gotoFloristCount;
        return percentGFShutdown;
    }

    public double getPercentGotoFloristSuspend()
    {
        double percentGFSuspend = (double)(100 * gotoFloristSuspendCount) / (double)gotoFloristCount;
        return percentGFSuspend;
    }
    
    public double getPercentMercuryShopSuspend()
    {
        double percentMSS = (double)(100 * mercuryShopShutdownCount) / (double)mercuryShopCount;
        return percentMSS;
    }

}
