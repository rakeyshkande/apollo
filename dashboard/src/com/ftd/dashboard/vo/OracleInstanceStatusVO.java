package com.ftd.dashboard.vo;

import com.ftd.dashboard.util.DashboardConstants;

/**
 * Value Object for holding the current status of the Oracle Cluster.
 * Status of different logical parts of the cluster and each instance are held.
 * @author mcampbell
 *
 */
public class OracleInstanceStatusVO
{
    private int overallStatus;

    public OracleInstanceStatusVO()
    {
        this.overallStatus = DashboardConstants.NORMAL_THRESHOLD;
    }
    
    public int getOverallStatus()
    {
        return overallStatus;
    }

    public void setOverallStatus(int overallStatus)
    {
        this.overallStatus = overallStatus;
    }

    
}
