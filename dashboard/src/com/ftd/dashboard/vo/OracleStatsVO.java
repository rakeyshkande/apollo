package com.ftd.dashboard.vo;

import java.awt.List;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


public class OracleStatsVO
{
    private long   lockCount = 0;
    private long   oldLockCount = 0;
    private String dataDiskgroupName;
    private long   dataDiskgroupFreeSpace = 0;
    private String flashDiskgroupName;
    private long   flashDiskgroupFreeSpace = 0;
    private double smallestTablespacePctFree = 0;
    
    private HashMap<String,OracleInstanceStatsVO> oracleStatsInstanceMap;
    
    public OracleStatsVO()
    {
        oracleStatsInstanceMap = new HashMap<String,OracleInstanceStatsVO>();
    }
    
    public String getDataDiskgroupName()
    {
        return dataDiskgroupName;
    }

    public void setDataDiskgroupName(String dataDiskgroupName)
    {
        this.dataDiskgroupName = dataDiskgroupName;
    }

    public long getDataDiskgroupFreeSpace()
    {
        return dataDiskgroupFreeSpace;
    }

    public void setDataDiskgroupFreeSpace(long dataDiskgroupFreeSpace)
    {
        this.dataDiskgroupFreeSpace = dataDiskgroupFreeSpace;
    }

    public String getFlashDiskgroupName()
    {
        return flashDiskgroupName;
    }

    public void setFlashDiskgroupName(String flashDiskgroupName)
    {
        this.flashDiskgroupName = flashDiskgroupName;
    }

    public long getFlashDiskgroupFreeSpace()
    {
        return flashDiskgroupFreeSpace;
    }

    public void setFlashDiskgroupFreeSpace(long flashDiskgroupFreeSpace)
    {
        this.flashDiskgroupFreeSpace = flashDiskgroupFreeSpace;
    }

    public double getSmallestTablespacePctFree()
    {
        return smallestTablespacePctFree;
    }

    public void setSmallestTablespacePctFree(double smallestTablespacePctFree)
    {
        this.smallestTablespacePctFree = smallestTablespacePctFree;
    }

    public long getLockCount()
    {
        return lockCount;
    }

    public void setLockCount(long lockCount)
    {
        this.lockCount = lockCount;
    }

    public long getOldLockCount()
    {
        return oldLockCount;
    }

    public void setOldLockCount(long oldLockCount)
    {
        this.oldLockCount = oldLockCount;
    }


    public OracleInstanceStatsVO getInstanceStats(String hostName)
    {
        OracleInstanceStatsVO vo = oracleStatsInstanceMap.get(hostName);
        if (vo == null)
        {
            vo = new OracleInstanceStatsVO(hostName); 
            oracleStatsInstanceMap.put(hostName, vo);
        }
        return vo;
    }
    
    public void setInstanceSessions(String hostName, long sessions)
    {
        getInstanceStats(hostName).setSessions(sessions);
    }
    
    public void setInstanceGCRemaster(String hostName, long gcremaster)
    {
        getInstanceStats(hostName).setGcremaster(gcremaster);
    }
    
    public void setInstanceLogfileResync(String hostName, long logfileresync)
    {
        getInstanceStats(hostName).setLogfileresync(logfileresync);
    }
    public void setInstanceTransactions(String hostName, long transactions)
    {
        getInstanceStats(hostName).setTransactions(transactions);
    }
    public void setInstanceUsercalls(String hostName, long usercalls)
    {
        getInstanceStats(hostName).setUserCalls(usercalls);
    }
    public void setInstanceMemoryUsed(String hostName, long memoryUsed)
    {
        getInstanceStats(hostName).setMemoryUsed(memoryUsed);
    }
    public void setInstanceLoad(String hostName, double load)
    {
        getInstanceStats(hostName).setLoad(load);
    }
    
    public Collection<OracleInstanceStatsVO> getInstanceCollection()
    {
        return oracleStatsInstanceMap.values();
    }

    public void setInstanceCacheEfficiency(String hostName,
            double globalCacheReadPercent, double localCacheReadPercent,
            double noCacheReadPercent, long physicalReads, long logicalReads)
    {
        OracleInstanceStatsVO instanceStatsVo = getInstanceStats(hostName);
        instanceStatsVo.setGlobalCacheReadPercent(globalCacheReadPercent);
        instanceStatsVo.setLocalCacheReadPercent(localCacheReadPercent);
        instanceStatsVo.setNoCacheReadPercent(noCacheReadPercent);
        instanceStatsVo.setPhysicalReads(physicalReads);
        instanceStatsVo.setLogicalReads(logicalReads);
        
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("\nlockCount = ");
        sb.append(lockCount);
        
        sb.append("\noldLockCount = ");
        sb.append(oldLockCount);
        sb.append("\ndataDiskgroupName = ");
        sb.append(dataDiskgroupName);
        sb.append("\ndataDiskgroupFreeSpace = ");
        sb.append(dataDiskgroupFreeSpace);
        sb.append("\nflashDiskgroupName = ");
        sb.append(flashDiskgroupName);
        sb.append("\nflashDiskgroupFreeSpace = ");
        sb.append(flashDiskgroupFreeSpace);
        sb.append("\nsmallestTablespacePctFree = ");
        sb.append(smallestTablespacePctFree);
        
        Iterator<OracleInstanceStatsVO> instanceIterator = oracleStatsInstanceMap.values().iterator();
        while (instanceIterator.hasNext())
        {
            sb.append("\n");
            instanceIterator.next().toString(sb);
        }
        
        return sb.toString(); 
    }
}
