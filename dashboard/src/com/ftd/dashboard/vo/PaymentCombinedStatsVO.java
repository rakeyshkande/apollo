package com.ftd.dashboard.vo;

public class PaymentCombinedStatsVO
{
    private String         paymentType;
    private PaymentStatsVO leftPaymentStatsVO;
    private PaymentStatsVO rightPaymentStatsVO;
    
    public PaymentCombinedStatsVO()
    {
    }

    public void setPaymentType(String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getPaymentType()
    {
        return paymentType;
    }

    public void setLeftPaymentStatsVO(PaymentStatsVO leftPaymentStatsVO)
    {
        this.leftPaymentStatsVO = leftPaymentStatsVO;
    }

    public PaymentStatsVO getLeftPaymentStatsVO()
    {
        return leftPaymentStatsVO;
    }

    public void setRightPaymentStatsVO(PaymentStatsVO rightPaymentStatsVO)
    {
        this.rightPaymentStatsVO = rightPaymentStatsVO;
    }

    public PaymentStatsVO getRightPaymentStatsVO()
    {
        return rightPaymentStatsVO;
    }
}
