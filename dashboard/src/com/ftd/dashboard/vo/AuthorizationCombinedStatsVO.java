package com.ftd.dashboard.vo;

public class AuthorizationCombinedStatsVO
{
    private String               cardType;
    private AuthorizationStatsVO leftAuthorizationStatsVO;
    private AuthorizationStatsVO rightAuthorizationStatsVO;
    
    public AuthorizationCombinedStatsVO()
    {
    }

    public void setRightAuthorizationStatsVO(AuthorizationStatsVO rightAuthorizationStatsVO)
    {
        this.rightAuthorizationStatsVO = rightAuthorizationStatsVO;
    }

    public AuthorizationStatsVO getRightAuthorizationStatsVO()
    {
        return rightAuthorizationStatsVO;
    }


    public void setLeftAuthorizationStatsVO(AuthorizationStatsVO leftAuthorizationStatsVO)
    {
        this.leftAuthorizationStatsVO = leftAuthorizationStatsVO;
    }

    public AuthorizationStatsVO getLeftAuthorizationStatsVO()
    {
        return leftAuthorizationStatsVO;
    }

    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public String getCardType()
    {
        return cardType;
    }
}
