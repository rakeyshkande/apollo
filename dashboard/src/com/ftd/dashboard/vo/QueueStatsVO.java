package com.ftd.dashboard.vo;

import java.util.Date;

public class QueueStatsVO
{
    private Date statTimestamp;

    private long workQueueCount;
    private long emailQueueCount;
    private long otherQueueCount;
    private long totalQueueCount;
    
    public QueueStatsVO()
    {
    }

    public void setWorkQueueCount(long workQueueCount)
    {
        this.workQueueCount = workQueueCount;
    }

    public long getWorkQueueCount()
    {
        return workQueueCount;
    }

    public void setEmailQueueCount(long emailQueueCount)
    {
        this.emailQueueCount = emailQueueCount;
    }

    public long getEmailQueueCount()
    {
        return emailQueueCount;
    }

    public void setOtherQueueCount(long otherQueueCount)
    {
        this.otherQueueCount = otherQueueCount;
    }

    public long getOtherQueueCount()
    {
        return otherQueueCount;
    }

    public void setTotalQueueCount(long totalQueueCount)
    {
        this.totalQueueCount = totalQueueCount;
    }

    public long getTotalQueueCount()
    {
        return totalQueueCount;
    }

    public void setStatTimestamp(Date statTimestamp)
    {
        this.statTimestamp = statTimestamp;
    }

    public Date getStatTimestamp()
    {
        return statTimestamp;
    }
}
