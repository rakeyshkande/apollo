package com.ftd.dashboard.vo;

import java.io.Serializable;

import com.ftd.dashboard.util.DashboardConstants;

public class ThresholdVO implements Serializable
{
    public final static int THRESHOLD_TYPE_NORMAL = 0;
    public final static int THRESHOLD_TYPE_REVERSE = 1;
    
    protected String name;
    protected int warningThreshold;
    protected int criticalThreshold;
    protected int maxDisplayValue;
    protected int thresholdType;
    
    public int getWarningThreshold()
    {
        return warningThreshold;
    }
    public void setWarningThreshold(int warningThreshold)
    {
        this.warningThreshold = warningThreshold;
    }
    public int getCriticalThreshold()
    {
        return criticalThreshold;
    }
    public void setCriticalThreshold(int criticalThreshold)
    {
        this.criticalThreshold = criticalThreshold;
    }
    public int getMaxDisplayValue()
    {
        return maxDisplayValue;
    }
    public void setMaxDisplayValue(int maxDisplayValue)
    {
        this.maxDisplayValue = maxDisplayValue;
    }
    public int getThresholdType()
    {
        return thresholdType;
    }
    public void setThresholdType(int thresholdType)
    {
        this.thresholdType = thresholdType;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    
    public int getCurrentThreshold(long value)
    {
        int threshold = DashboardConstants.NORMAL_THRESHOLD;    
        
        if (thresholdType == THRESHOLD_TYPE_NORMAL)
        {
            if (value > warningThreshold)
            {
                if (value > criticalThreshold)
                { 
                    threshold = DashboardConstants.CRITICAL_THRESHOLD;    
                }
                else
                {
                    threshold = DashboardConstants.WARNING_THRESHOLD;    
                }
            }
        }
        else
        {
            if (value < warningThreshold)
            {
                if (value < criticalThreshold)
                { 
                    threshold = DashboardConstants.CRITICAL_THRESHOLD;    
                }
                else
                {
                    threshold = DashboardConstants.WARNING_THRESHOLD;    
                }
            }
            
        }
        
        return threshold;
    }
    

}