package com.ftd.dashboard.vo;

import java.text.SimpleDateFormat;

import java.util.Date;

public class CleanHourlyStatsVO
{
    private Date cleanDate; 
    private long ordersTaken;
    private long gotoFloristCount;
    private long gotoFloristSuspend;
    private long gotoFloristShutdown;
    private long mercuryShopCount;
    private long mercuryShopShutdown;
    private long coveredZipCount;
    private long gnaddZipCount;
    private long FTD_QUEUE_END;
    private long FTD_QUEUE_INCOMING;
    private long FTD_QUEUE_TAGGED;
    private long FTDM_QUEUE_END;
    private long FTDM_QUEUE_INCOMING;
    private long FTDM_QUEUE_TAGGED;
    private long askQueueEnd;
    private long ASK_QUEUE_INCOMING;
    private long ASK_QUEUE_TAGGED;
    private long rejectQueueEnd;
    private long REJECT_QUEUE_INCOMING;
    private long REJECT_QUEUE_TAGGED;
    private long EMAIL_QUEUE_END;
    private long EMAIL_QUEUE_INCOMING;
    private long EMAIL_QUEUE_TAGGED;
    private long EMAIL_WORKED;
    private long REJECT_QUEUE_AUTO;
    private long MERC_QUEUE_END;
    private long MERC_QUEUE_INCOMING;
    private long MERC_QUEUE_TAGGED;
    private long EMAIL_AUTO_HANDLE;
    private long ANS_QUEUE_END;
    private long ANS_QUEUE_INCOMING;
    private long ANS_QUEUE_TAGGED;
    private long DEN_QUEUE_END;
    private long DEN_QUEUE_INCOMING;
    private long DEN_QUEUE_TAGGED;
    private long CON_QUEUE_END;
    private long CON_QUEUE_INCOMING;
    private long CON_QUEUE_TAGGED;
    private long CAN_QUEUE_END;
    private long CAN_QUEUE_INCOMING;
    private long CAN_QUEUE_TAGGED;
    private long GEN_QUEUE_END;
    private long GEN_QUEUE_INCOMING;
    private long GEN_QUEUE_TAGGED;
    private long zipQueueEnd;
    private long ZIP_QUEUE_INCOMING;
    private long ZIP_QUEUE_TAGGED;
    private long CRD_QUEUE_END;
    private long CRD_QUEUE_INCOMING;
    private long CRD_QUEUE_TAGGED;
    private long LP_QUEUE_END;
    private long LP_QUEUE_INCOMING;
    private long LP_QUEUE_TAGGED;
    private long ADJ_QUEUE_END;
    private long ADJ_QUEUE_INCOMING;
    private long ADJ_QUEUE_TAGGED;
    private long CX_QUEUE_END;
    private long CX_QUEUE_INCOMING;
    private long CX_QUEUE_TAGGED;
    private long MO_QUEUE_END;
    private long MO_QUEUE_INCOMING;
    private long MO_QUEUE_TAGGED;
    private long ND_QUEUE_END;
    private long ND_QUEUE_INCOMING;
    private long ND_QUEUE_TAGGED;
    private long QI_QUEUE_END;
    private long QI_QUEUE_INCOMING;
    private long QI_QUEUE_TAGGED;
    private long PD_QUEUE_END;
    private long PD_QUEUE_INCOMING;
    private long PD_QUEUE_TAGGED;
    private long BD_QUEUE_END;
    private long BD_QUEUE_INCOMING;
    private long BD_QUEUE_TAGGED;
    private long SE_QUEUE_END;
    private long SE_QUEUE_INCOMING;
    private long SE_QUEUE_TAGGED;
    private long DI_QUEUE_END;
    private long DI_QUEUE_INCOMING;
    private long DI_QUEUE_TAGGED;
    private long QC_QUEUE_END;
    private long QC_QUEUE_INCOMING;
    private long QC_QUEUE_TAGGED;
    private long OA_QUEUE_END;
    private long OA_QUEUE_INCOMING;
    private long OA_QUEUE_TAGGED;
    private long OT_QUEUE_END;
    private long OT_QUEUE_INCOMING;
    private long OT_QUEUE_TAGGED;

    public CleanHourlyStatsVO()
    {
    }

    public void setCleanDate(Date cleanDate)
    {
        this.cleanDate = cleanDate;
    }

    public Date getCleanDate()
    {
        return cleanDate;
    }
    
    public void setOrdersTaken(long ordersTaken)
    {
        this.ordersTaken = ordersTaken;
    }

    public long getOrdersTaken()
    {
        return ordersTaken;
    }

    public void setGotoFloristCount(long gotoFloristCount)
    {
        this.gotoFloristCount = gotoFloristCount;
    }

    public long getGotoFloristCount()
    {
        return gotoFloristCount;
    }

    public void setGotoFloristSuspend(long gotoFloristSuspend)
    {
        this.gotoFloristSuspend = gotoFloristSuspend;
    }

    public long getGotoFloristSuspend()
    {
        return gotoFloristSuspend;
    }

    public void setGotoFloristShutdown(long gotoFloristShutdown)
    {
        this.gotoFloristShutdown = gotoFloristShutdown;
    }

    public long getGotoFloristShutdown()
    {
        return gotoFloristShutdown;
    }

    public void setMercuryShopCount(long mercuryShopCount)
    {
        this.mercuryShopCount = mercuryShopCount;
    }

    public long getMercuryShopCount()
    {
        return mercuryShopCount;
    }

    public void setMercuryShopShutdown(long mercuryShopShutdown)
    {
        this.mercuryShopShutdown = mercuryShopShutdown;
    }

    public long getMercuryShopShutdown()
    {
        return mercuryShopShutdown;
    }

    public void setCoveredZipCount(long coveredZipCount)
    {
        this.coveredZipCount = coveredZipCount;
    }

    public long getCoveredZipCount()
    {
        return coveredZipCount;
    }

    public void setGnaddZipCount(long gnaddZipCount)
    {
        this.gnaddZipCount = gnaddZipCount;
    }

    public long getGnaddZipCount()
    {
        return gnaddZipCount;
    }

    public void setAskQueueEnd(long askQueueEnd)
    {
        this.askQueueEnd = askQueueEnd;
    }

    public long getAskQueueEnd()
    {
        return askQueueEnd;
    }

    public void setRejectQueueEnd(long rejectQueueEnd)
    {
        this.rejectQueueEnd = rejectQueueEnd;
    }

    public long getRejectQueueEnd()
    {
        return rejectQueueEnd;
    }

    public void setZipQueueEnd(long zipQueueEnd)
    {
        this.zipQueueEnd = zipQueueEnd;
    }

    public long getZipQueueEnd()
    {
        return zipQueueEnd;
    }
}
