package com.ftd.oe.mdb;

import com.ftd.newsletterevents.core.NewsletterEventVO;
import com.ftd.newsletterevents.core.XmlService;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.J2EEResourceProvider;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.util.OrderUtil;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.naming.InitialContext;
import java.sql.Connection;

import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;

import org.w3c.dom.Document;


/**
 * This a message driven bean.
 *
 * When the container is start the setMessageDrivenContext is executed.
 *
 */
public class OrderDispatcherMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private static String LOGGER_CATEGORY = "com.ftd.oe.mdb.OrderDispatcherMDB";

  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * onMesage...a JMS message was received
   * @param msg
   */
  public void onMessage(Message msg)
  {

      Logger logger = new Logger(LOGGER_CATEGORY);
      Connection qConn = null;
      J2EEResourceProvider resourceProvider = new J2EEResourceProvider();

      try {
          OrderUtil orderUtil = new OrderUtil();

          TextMessage textMessage = (TextMessage)msg;
          String masterOrderNumber = textMessage.getText();
          logger.debug("New message: " + masterOrderNumber);

          String dataSource = OrderEntryConstants.DATASOURCE;
          qConn = resourceProvider.getDatabaseConnection(dataSource);
          if (qConn == null) {
              throw new Exception("Unable to connect to database");
          }

          boolean success = false;
          OrderVO orderVO = orderUtil.getOrderFromDB(qConn, masterOrderNumber);

          if (orderVO.getMasterOrderNumber() != null) {
              Document doc = orderUtil.createOrderXML(orderVO);
              success = orderUtil.sendOrderToOrderGatherer(doc);
          }

          OrderEntryDAO orderEntryDAO = new OrderEntryDAO();
          if (success) {
              HashMap outputMap = orderEntryDAO.updateSentToApolloDate(qConn, masterOrderNumber);
              String status = (String) outputMap.get("OUT_STATUS");
              String message = (String) outputMap.get("OUT_MESSAGE");
              logger.debug("status: " + status + " - " + message);

              CustomerVO buyerVO = orderVO.getBuyerVO();
              String newsletterFlag = buyerVO.getNewsletterFlag();
              if (newsletterFlag != null && newsletterFlag.equalsIgnoreCase("Y")) {

                  try {

                      NewsletterEventVO neVO = new NewsletterEventVO(buyerVO.getEmailAddress(), orderVO.getCompany(),
                          "S", orderVO.getOrderCreatedBy(), "JOE", new Long(new Date().getTime()));

                      String jmsXML = XmlService.marshallToXmlString(com.ftd.newsletterevents.core.NewsletterEventVO.class, neVO);

                      InitialContext iContext = new InitialContext();
                      MessageToken token = null;
                      token = new MessageToken();
                      token.setStatus("JOE Newsletter");
                      token.setMessage(jmsXML);
                      orderUtil.dispatchJMSMessageWithProperty(iContext, token, "ORIGIN_APP_NAME_KEY", "JOE");
                  
                  } catch (Exception e) {
                      logger.error(e);
                  }

              }

          } else {
              HashMap outputMap = orderEntryDAO.updateSentToApolloRetryCount(qConn, masterOrderNumber);
              boolean resentJMS = false;
              String cnt = (String) outputMap.get("OUT_RETRY_COUNT");
              logger.debug("retry count: " + cnt);

              if (cnt != null && !cnt.equals("")) {

                  ConfigurationUtil cu = ConfigurationUtil.getInstance();
                  String orderSendRetryMax = cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                      OrderEntryConstants.ORDER_SEND_RETRY_MAX);
                  logger.debug("orderSendRetryMax: " + orderSendRetryMax);

                  int retryMax = 0;
                  int retryCount = 99;
                  try {
                      retryMax = Integer.parseInt(orderSendRetryMax);
                      retryCount = Integer.parseInt(cnt);

                      if (retryCount < retryMax) {
                          logger.debug("Resending JMS message");
                          InitialContext iContext = new InitialContext();
                          MessageToken token = null;
                          token = new MessageToken();
                          token.setStatus("JOE Order Dispatcher");
                          token.setMessage(masterOrderNumber);
                          orderUtil.dispatchJMSMessage(iContext, token);
                          resentJMS = true;
                      }
                  } catch (Exception e) {
                      logger.error("Invalid retry value");
                  }
              }
              if (!resentJMS) {
                  // Add to scrub.order_exceptions
                  String context = "JOE/Joe Order Dispatcher";
                  String errorMessage = "Error sending order to Order Gatherer. Retry count: " + cnt;
                  outputMap = orderEntryDAO.insertOrderExceptions(qConn, masterOrderNumber, context, errorMessage);
                  String status = (String) outputMap.get("OUT_STATUS");
                  String message = (String) outputMap.get("OUT_MESSAGE");
                  logger.debug("status: " + status + " - " + message);
              }
          }
      }
      catch(Exception e)
      {
          logger.error(e);
      } catch (Throwable t) {
          logger.error(t.getMessage());
          logger.error(t);
      } finally {
          if( qConn!=null ) {
              try {
                  if( !qConn.isClosed() ) {
                      qConn.close();
                  }
              } catch (SQLException sqle) {
                  logger.warn("Error closing database connection",sqle);
              }
          }
      }
    
  }

  public void ejbRemove()
  {
  }

  /**
   * This message is invoked when the container is started
   * @param ctx
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
    
  }
  
}