package com.ftd.oe.heartbeat;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.J2EEResourceProvider;
import com.ftd.osp.utilities.ping.servlet.BaseHeartBeatServlet;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import javax.servlet.*;
import javax.servlet.http.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class CheckPulseServlet
  extends BaseHeartBeatServlet
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private Logger logger = 
        new Logger("com.ftd.oe.heartbeat.CheckPulseServlet");

  public void init(ServletConfig config)
    throws ServletException
  {
    super.init(config);
  }
  /**
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    logger.debug("Entered doGet()"); 
    
    super.doGet(request, response);
    
    logger.debug("Leaving doGet()"); 
  }
  
   /**
   * Obtain connectivity with the database
   * @return Database Connection
   * @throws Exception
   */
  public Connection getDBConnection()
  throws Exception
  {
    logger.debug("Entered getDBConnection()");
    Connection conn = null;
    String dataSource = OrderEntryConstants.DATASOURCE;
    J2EEResourceProvider resProvider = new J2EEResourceProvider();
    conn = resProvider.getDatabaseConnection(dataSource);
    logger.debug("Established DBConnection");
    return conn;
  }
}
