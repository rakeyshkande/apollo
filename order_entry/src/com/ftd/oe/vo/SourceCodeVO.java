package com.ftd.oe.vo;

import java.util.ArrayList;
import java.util.List;


public class SourceCodeVO {
    
    private boolean iotwFlag = false;
    private String priceHeaderId = null;
    private List<PriceHeaderVO> priceHeaderList = new ArrayList<PriceHeaderVO>();
    private ProgramRewardVO programReward;


    public void setPriceHeaderList(List<PriceHeaderVO> newpriceHeaderList) {
        this.priceHeaderList = newpriceHeaderList;
    }

    public List<PriceHeaderVO> getPriceHeaderList() {
        return priceHeaderList;
    }

    public void setPriceHeaderId(String newpriceHeaderId) {
        this.priceHeaderId = newpriceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setProgramReward(ProgramRewardVO newprogramReward) {
        this.programReward = newprogramReward;
    }

    public ProgramRewardVO getProgramReward() {
        return programReward;
    }

    public void setIotwFlag(boolean newiotwFlag) {
        this.iotwFlag = newiotwFlag;
    }

    public boolean isIotwFlag() {
        return iotwFlag;
    }
}
