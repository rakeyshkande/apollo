package com.ftd.oe.vo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CrossSellVO
{
    private int crossSellMasterId;
    private String productId;
    private String upsellMasterId;
    private String status;
    private String over21Flag;
    private String updatedBy;
    private String productType;
    private List detailVO;
    private String novatorId;
    private String novatorName;

    private static Logger logger = new Logger("com.ftd.oe.vo.CrossSellVO");
    
    public CrossSellVO()
    {
        detailVO = new ArrayList();
    }

    public void setCrossSellMasterId(int crossSellMasterId) {
        this.crossSellMasterId = crossSellMasterId;
    }

    public int getCrossSellMasterId() {
        return crossSellMasterId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setUpsellMasterId(String upsellMasterId) {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId() {
        return upsellMasterId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setOver21Flag(String over21Flag) {
        this.over21Flag = over21Flag;
    }

    public String getOver21Flag() {
        return over21Flag;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setNovatorId(String novatorId) {
        this.novatorId = novatorId;
    }

    public String getNovatorId() {
        return novatorId;
    }

    public void setNovatorName(String novatorName) {
        this.novatorName = novatorName;
    }

    public String getNovatorName() {
        return novatorName;
    }

    public void setDetailVO(CrossSellDetailVO detailVO) {
        this.detailVO.add(detailVO);
    }

    public List getDetailVO() {
        return detailVO;
    }

    public Document toXMLDocument() {
        logger.debug("CrossSellVO.toXMLDocument()");
        Document doc = null;

        try {
            doc = JAXPUtil.createDocument();
                    
            Element root = doc.createElement("root");
            Element child1;
            Element child2;

            child1 = doc.createElement("cross_sell");
            root.appendChild(child1);

            child2 = JAXPUtil.buildSimpleXmlNode(doc, "cross_sell_master_id", this.getCrossSellMasterId());
            child1.appendChild(child2);
            
            child2 = JAXPUtil.buildSimpleXmlNode(doc, "product_id", this.getProductId());
            child1.appendChild(child2);
            
            child2 = JAXPUtil.buildSimpleXmlNode(doc, "upsell_master_id", this.getUpsellMasterId());
            child1.appendChild(child2);
            
            child2 = doc.createElement("cross_sell_details");
            for (int i=0; i<detailVO.size(); i++) {
                CrossSellDetailVO csdVO = (CrossSellDetailVO) detailVO.get(i);
                if(csdVO != null) {   
                    Document subDoc = csdVO.toXMLDocument();
                    Element detailElement = (Element)subDoc.getFirstChild().cloneNode(true);
                    child1.appendChild(doc.importNode(detailElement,true));
                }
            }
            child1.appendChild(child2);

            doc.appendChild(root);
            logger.debug(JAXPUtil.toString(doc));

        } catch ( Exception e ) {
            logger.error(e);
        }
    
        return doc;
    }

    public boolean isProductSameDay()
    {
        if (productType != null &&
            (productType.equals("SDFC") || productType.equals("FLORAL") || productType.equals("SDG"))
            )
        {
            return true;
        }
        
        return false;
    }

    public String getNormalizedProduct()
    {
        if (StringUtils.isBlank(productId))
        {
            if (StringUtils.isBlank(upsellMasterId))
            {
                return null;
            }
            else
            {
                return upsellMasterId;
            }
        }
        else
        {
            return productId;
        }
    }

}
