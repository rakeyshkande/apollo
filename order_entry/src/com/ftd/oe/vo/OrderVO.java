package com.ftd.oe.vo;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderVO {

    private String dnisId;
    private String sourceCode;
    private String masterOrderNumber;

    private String origin;
    private BigDecimal orderAmount;
    private int orderCount;
    private BigDecimal productAmount;
    private BigDecimal discountAmount;
    private BigDecimal milesPoints;
    private BigDecimal addonAmount;
    private BigDecimal serviceFeeAmount;
    private BigDecimal shippingFeeAmount;
    private BigDecimal salesTaxAmount;
    private Date orderDate;
    private String orderCreatedBy;
    private String fraudFlag;
    private String fraudId;
    private String fraudComments;
    private String buyerId;
    private String company;
    private List odVO;
    private CustomerVO buyerVO;
    private CreditCardVO ccVO;
    private List coBrandVO;
    private boolean bypassCCAuthFlag;
    private boolean hasLuxuryItem;
    private String surchargeDescription;
    private BigDecimal surchargeFee;
    private String applySurchargeCode;
    private BigDecimal serviceFeeTotalSavings;
    private BigDecimal shippingFeeTotalSavings;
    private String buyerHasFreeShippingFlag;
	private String languageId;
	private boolean buyerCamsVerified;
	private String buyerHasPCMembershipFlag;
	private String originalOrderGuid;
	private boolean updateOrderFlag;
	private boolean piModifyOrderFlag;
	private String piPartnerId;
	private BigDecimal piModifyOrderThreshold;
	private BigDecimal piOriginalMercTotal;
	private BigDecimal piOriginalPdbPrice;
	private String piOriginalFloristId;
	
    public OrderVO() {
        odVO = new ArrayList();
        coBrandVO = new ArrayList();
        orderCount = 0;
    }

    public void setDnisId(String dnisId) {
        this.dnisId = dnisId;
    }

    public String getDnisId() {
        return dnisId;
    }

    public void setBuyerCamsVerified(boolean buyerCamsVerified){
    	this.buyerCamsVerified = buyerCamsVerified;
    }
    
    public boolean getBuyerCamsVerified(){
    	return buyerCamsVerified;
    } 
    
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setOdVO(OrderDetailVO odVO) {
        this.odVO.add(odVO);
    }

    public List getOdVO() {
        return odVO;
    }

    public void setBuyerVO(CustomerVO buyerVO) {
        this.buyerVO = buyerVO;
    }

    public CustomerVO getBuyerVO() {
        return buyerVO;
    }

    public void setMasterOrderNumber(String masterOrderNumber) {
        this.masterOrderNumber = masterOrderNumber;
    }

    public String getMasterOrderNumber() {
        return masterOrderNumber;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setCcVO(CreditCardVO ccVO) {
        this.ccVO = ccVO;
    }

    public CreditCardVO getCcVO() {
        return ccVO;
    }

    public void setOrderCreatedBy(String orderCreatedBy) {
        this.orderCreatedBy = orderCreatedBy;
    }

    public String getOrderCreatedBy() {
        return orderCreatedBy;
    }

    public void setFraudFlag(String fraudFlag) {
        this.fraudFlag = fraudFlag;
    }

    public String getFraudFlag() {
        return fraudFlag;
    }

    public void setFraudId(String fraudId) {
        this.fraudId = fraudId;
    }

    public String getFraudId() {
        return fraudId;
    }

    public void setFraudComments(String fraudComments) {
        this.fraudComments = fraudComments;
    }

    public String getFraudComments() {
        return fraudComments;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setMilesPoints(BigDecimal milesPoints) {
        this.milesPoints = milesPoints;
    }

    public BigDecimal getMilesPoints() {
        return milesPoints;
    }

    public void setAddonAmount(BigDecimal addonAmount) {
        this.addonAmount = addonAmount;
    }

    public BigDecimal getAddonAmount() {
        return addonAmount;
    }

    public void setServiceFeeAmount(BigDecimal serviceFeeAmount) {
        this.serviceFeeAmount = serviceFeeAmount;
    }

    public BigDecimal getServiceFeeAmount() {
        return serviceFeeAmount;
    }

    public void setShippingFeeAmount(BigDecimal shippingFeeAmount) {
        this.shippingFeeAmount = shippingFeeAmount;
    }

    public BigDecimal getShippingFeeAmount() {
        return shippingFeeAmount;
    }

    public void setSalesTaxAmount(BigDecimal salesTaxAmount) {
        this.salesTaxAmount = salesTaxAmount;
    }

    public BigDecimal getSalesTaxAmount() {
        return salesTaxAmount;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setProductAmount(BigDecimal productAmount) {
        this.productAmount = productAmount;
    }

    public BigDecimal getProductAmount() {
        return productAmount;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setCoBrandVO(CoBrandVO coBrandVO) {
        if (coBrandVO == null) {
            this.coBrandVO = new ArrayList();
        } else {
            this.coBrandVO.add(coBrandVO);
        }
    }

    public List getCoBrandVO() {
        return coBrandVO;
    }

    public void setBypassCCAuthFlag(boolean bypassCCAuthFlag) {
        this.bypassCCAuthFlag = bypassCCAuthFlag;
    }

    public boolean isBypassCCAuthFlag() {
        return bypassCCAuthFlag;
    }

    public void setHasLuxuryItem(boolean hasLuxuryItem) {
        this.hasLuxuryItem = hasLuxuryItem;
    }

    public boolean isHasLuxuryItem() {
        return hasLuxuryItem;
    }

  public void setSurchargeDescription(String surchargeDescription)
  {
    this.surchargeDescription = surchargeDescription;
  }

  public String getSurchargeDescription()
  {
    return surchargeDescription;
  }


  public void setApplySurchargeCode(String applySurchargeCode)
  {
    this.applySurchargeCode = applySurchargeCode;
  }

  public String getApplySurchargeCode()
  {
    return applySurchargeCode;
  }


  public void setSurchargeFee(BigDecimal surchargeFee)
  {
    this.surchargeFee = surchargeFee;
  }

  public BigDecimal getSurchargeFee()
  {
    return surchargeFee;
  }

    public void setServiceFeeTotalSavings(BigDecimal serviceFeeTotalSavings) {
        this.serviceFeeTotalSavings = serviceFeeTotalSavings;
    }

    public BigDecimal getServiceFeeTotalSavings() {
        return serviceFeeTotalSavings;
    }

    public void setShippingFeeTotalSavings(BigDecimal shippingFeeTotalSavings) {
        this.shippingFeeTotalSavings = shippingFeeTotalSavings;
    }

    public BigDecimal getShippingFeeTotalSavings() {
        return shippingFeeTotalSavings;
    }

    public void setBuyerHasFreeShippingFlag(String buyerHasFreeShippingFlag) {
        this.buyerHasFreeShippingFlag = buyerHasFreeShippingFlag;
    }

    public String getBuyerHasFreeShippingFlag() {
        return buyerHasFreeShippingFlag;
    }
	
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	
	public String getLanguageId() { 
		return languageId;
	}

	public void setBuyerHasPCMembershipFlag(String buyerHasPCMembershipFlag) {
		this.buyerHasPCMembershipFlag = buyerHasPCMembershipFlag;
	}

	public String getBuyerHasPCMembershipFlag() {
		return buyerHasPCMembershipFlag;
	}

	public void setOriginalOrderGuid(String originalOrderGuid) {
		this.originalOrderGuid = originalOrderGuid;
	}

	public String getOriginalOrderGuid() {
		return originalOrderGuid;
	}

    public boolean isUpdateOrderFlag() {
        return updateOrderFlag;
    }

    public void setUpdateOrderFlag(boolean updateOrderFlag) {
        this.updateOrderFlag = updateOrderFlag;
    }

    public boolean isPiModifyOrderFlag() {
        return piModifyOrderFlag;
    }

    public void setPiModifyOrderFlag(boolean piModifyOrderFlag) {
        this.piModifyOrderFlag = piModifyOrderFlag;
    }

	public void setPiPartnerId(String piPartnerId) {
		this.piPartnerId = piPartnerId;
	}

	public String getPiPartnerId() {
		return piPartnerId;
	}

	public BigDecimal getPiModifyOrderThreshold() {
		return piModifyOrderThreshold;
	}

	public void setPiModifyOrderThreshold(BigDecimal piModifyOrderThreshold) {
		this.piModifyOrderThreshold = piModifyOrderThreshold;
	}

	public BigDecimal getPiOriginalMercTotal() {
		return piOriginalMercTotal;
	}

	public void setPiOriginalMercTotal(BigDecimal piOriginalMercTotal) {
		this.piOriginalMercTotal = piOriginalMercTotal;
	}

	public void setPiOriginalPdbPrice(BigDecimal piOriginalPdbPrice) {
		this.piOriginalPdbPrice = piOriginalPdbPrice;
	}

	public BigDecimal getPiOriginalPdbPrice() {
		return piOriginalPdbPrice;
	}

	public void setPiOriginalFloristId(String piOriginalFloristId) {
		this.piOriginalFloristId = piOriginalFloristId;
	}

	public String getPiOriginalFloristId() {
		return piOriginalFloristId;
	}

}
