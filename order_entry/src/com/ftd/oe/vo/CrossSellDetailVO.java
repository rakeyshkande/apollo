package com.ftd.oe.vo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CrossSellDetailVO
{
    private String detailProductId;
    private String detailUpsellId;
    private int displayOrderSeq;
    private String productType;
    private String status;
    private String over21Flag;
    private String price;
    private String novatorName;
    private String exceptionStartDate;
    private String exceptionEndDate;
    private String smallImage;
    private String largeImage;
    private String displayId;

    private static Logger logger = new Logger("com.ftd.oe.vo.CrossSellDetailVO");
    
    public CrossSellDetailVO()
    {
    }

    public void setDetailProductId(String detailProductId) {
        this.detailProductId = detailProductId;
    }

    public String getDetailProductId() {
        return detailProductId;
    }

    public void setDetailUpsellId(String detailUpsellId) {
        this.detailUpsellId = detailUpsellId;
    }

    public String getDetailUpsellId() {
        return detailUpsellId;
    }

    public void setDisplayOrderSeq(int displayOrderSeq) {
        this.displayOrderSeq = displayOrderSeq;
    }

    public int getDisplayOrderSeq() {
        return displayOrderSeq;
    }

    public void setProductType(String productType)
    {
        this.productType = productType;
    }

    public String getProductType()
    {
        return productType;
    }
    
    public boolean isProductSameDay()
    {
        if (productType != null &&
            (productType.equals("SDFC") || productType.equals("FLORAL") || productType.equals("SDG"))
            )
        {
            return true;
        }
        
        return false;
    }

    public Document toXMLDocument() {

        logger.debug("CrossSellVO.toXMLDocument()");
        Document doc = null;

        try {
            doc = JAXPUtil.createDocument();
                    
            Element root = doc.createElement("root");
            Element child1;

            child1 = JAXPUtil.buildSimpleXmlNode(doc, "detail_product_id", this.getDetailProductId());
            root.appendChild(child1);
            
            child1 = JAXPUtil.buildSimpleXmlNode(doc, "detail_upsell_id", this.getDetailUpsellId());
            root.appendChild(child1);
            
            doc.appendChild(root);
            logger.debug(JAXPUtil.toString(doc));

        } catch ( Exception e ) {
            logger.error(e);
        }
    
        return doc;
    }


    public void setOver21Flag(String over21Flag) {
        this.over21Flag = over21Flag;
    }

    public String getOver21Flag() {
        return over21Flag;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setNovatorName(String novatorName) {
        this.novatorName = novatorName;
    }

    public String getNovatorName() {
        return novatorName;
    }

    public void setExceptionStartDate(String exceptionStartDate) {
        this.exceptionStartDate = exceptionStartDate;
    }

    public String getExceptionStartDate() {
        return exceptionStartDate;
    }

    public void setExceptionEndDate(String exceptionEndDate) {
        this.exceptionEndDate = exceptionEndDate;
    }

    public String getExceptionEndDate() {
        return exceptionEndDate;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    public String getLargeImage() {
        return largeImage;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getDisplayId() {
        return displayId;
    }

    public String getNormalizedProduct()
    {
        if (StringUtils.isBlank(detailProductId))
        {
            if (StringUtils.isBlank(detailUpsellId))
            {
                return null;
            }
            else
            {
                return detailUpsellId;
            }
        }
        else
        {
            return detailProductId;
        }
    }

}
