package com.ftd.oe.vo;

import java.math.BigDecimal;

public class AddonVO {

    private String addonId;
    private int addonQuantity;
    private BigDecimal addonPrice;
    private String funeralBannerText;

    public void setAddonId(String addonId) {
        this.addonId = addonId;
    }

    public String getAddonId() {
        return addonId;
    }

    public void setAddonQuantity(int addonQuantity) {
        this.addonQuantity = addonQuantity;
    }

    public int getAddonQuantity() {
        return addonQuantity;
    }

    public void setAddonPrice(BigDecimal addonPrice) {
        this.addonPrice = addonPrice;
    }

    public BigDecimal getAddonPrice() {
        return addonPrice;
    }

    public void setFuneralBannerText(String funeralBannerText) {
        this.funeralBannerText = funeralBannerText;
    }

    public String getFuneralBannerText() {
        return funeralBannerText;
    }
}
