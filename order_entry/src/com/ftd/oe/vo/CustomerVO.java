package com.ftd.oe.vo;

public class CustomerVO {
    
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private String country;
    private String dayPhone;
    private String dayPhoneExt;
    private String eveningPhone;
    private String eveningPhoneExt;
    private String emailAddress;
    private String newsletterFlag;
    private String QMSAddress;
    private String QMSCity;
    private String QMSState;
    private String QMSZipCode;
    private String QMSResultCode;
    private String membershipType;
    private String membershipId;
    private String membershipFirstName;
    private String membershipLastName;
    private String buyerRecipientIndicator;
    private String addressType;
    private String businessName;
    private String businessInfo;
    private String recipientRoom;
    private String hoursStart;
    private String hoursEnd;
    private boolean timeOfServiceFlag = false;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setDayPhone(String dayPhone) {
        this.dayPhone = dayPhone;
    }

    public String getDayPhone() {
        return dayPhone;
    }

    public void setDayPhoneExt(String dayPhoneExt) {
        this.dayPhoneExt = dayPhoneExt;
    }

    public String getDayPhoneExt() {
        return dayPhoneExt;
    }

    public void setEveningPhone(String eveningPhone) {
        this.eveningPhone = eveningPhone;
    }

    public String getEveningPhone() {
        return eveningPhone;
    }

    public void setEveningPhoneExt(String eveningPhoneExt) {
        this.eveningPhoneExt = eveningPhoneExt;
    }

    public String getEveningPhoneExt() {
        return eveningPhoneExt;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setNewsletterFlag(String newsletterFlag) {
        this.newsletterFlag = newsletterFlag;
    }

    public String getNewsletterFlag() {
        return newsletterFlag;
    }

    public void setQMSAddress(String qMSAddress) {
        this.QMSAddress = qMSAddress;
    }

    public String getQMSAddress() {
        return QMSAddress;
    }

    public void setQMSCity(String qMSCity) {
        this.QMSCity = qMSCity;
    }

    public String getQMSCity() {
        return QMSCity;
    }

    public void setQMSState(String qMSState) {
        this.QMSState = qMSState;
    }

    public String getQMSState() {
        return QMSState;
    }

    public void setQMSZipCode(String qMSZipCode) {
        this.QMSZipCode = qMSZipCode;
    }

    public String getQMSZipCode() {
        return QMSZipCode;
    }

    public void setQMSResultCode(String qMSResultCode) {
        this.QMSResultCode = qMSResultCode;
    }

    public String getQMSResultCode() {
        return QMSResultCode;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipFirstName(String membershipFirstName) {
        this.membershipFirstName = membershipFirstName;
    }

    public String getMembershipFirstName() {
        return membershipFirstName;
    }

    public void setMembershipLastName(String membershipLastName) {
        this.membershipLastName = membershipLastName;
    }

    public String getMembershipLastName() {
        return membershipLastName;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setBuyerRecipientIndicator(String buyerRecipientIndicator) {
        this.buyerRecipientIndicator = buyerRecipientIndicator;
    }

    public String getBuyerRecipientIndicator() {
        return buyerRecipientIndicator;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessInfo(String businessInfo) {
        this.businessInfo = businessInfo;
    }

    public String getBusinessInfo() {
        return businessInfo;
    }

    public void setRecipientRoom(String recipientRoom) {
        this.recipientRoom = recipientRoom;
    }

    public String getRecipientRoom() {
        return recipientRoom;
    }

    public void setHoursStart(String hoursStart) {
        this.hoursStart = hoursStart;
    }

    public String getHoursStart() {
        return hoursStart;
    }

    public void setHoursEnd(String hoursEnd) {
        this.hoursEnd = hoursEnd;
    }

    public String getHoursEnd() {
        return hoursEnd;
    }
    
    public void setTimeOfServiceFlag(boolean timeOfServiceFlag) {
        this.timeOfServiceFlag = timeOfServiceFlag;
    }

    public boolean getTimeOfServiceFlag() {
        return timeOfServiceFlag;
    }
}
