package com.ftd.oe.vo;


public class ProgramRewardVO {
    
    private String priceHeaderId;
    private String programName;
    private String rewardType;


    public void setProgramName(String newprogramName) {
        this.programName = newprogramName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setPriceHeaderId(String newpriceHeaderId) {
        this.priceHeaderId = newpriceHeaderId;
    }

    public String getPriceHeaderId() {
        return priceHeaderId;
    }

    public void setRewardType(String newrewardType) {
        this.rewardType = newrewardType;
    }

    public String getRewardType() {
        return rewardType;
    }
}
