package com.ftd.oe.vo;

public class FloristLookupVO {
    private String floristId;
    private String floristName;
    private String floristAddress;
    private String phoneNumber;
    private String mercuryFlag;
    private String floristWeight;
    private String gotoFlag;
    private String sundayFlag;
    private String priority;

    public void setFloristId(String floristId) {
        this.floristId = floristId;
    }

    public String getFloristId() {
        return floristId;
    }

    public void setFloristName(String floristName) {
        this.floristName = floristName;
    }

    public String getFloristName() {
        return floristName;
    }

    public void setFloristAddress(String floristAddress) {
        this.floristAddress = floristAddress;
    }

    public String getFloristAddress() {
        return floristAddress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setMercuryFlag(String mercuryFlag) {
        this.mercuryFlag = mercuryFlag;
    }

    public String getMercuryFlag() {
        return mercuryFlag;
    }

    public void setFloristWeight(String floristWeight) {
        this.floristWeight = floristWeight;
    }

    public String getFloristWeight() {
        return floristWeight;
    }

    public void setGotoFlag(String gotoFlag) {
        this.gotoFlag = gotoFlag;
    }

    public String getGotoFlag() {
        return gotoFlag;
    }

    public void setSundayFlag(String sundayFlag) {
        this.sundayFlag = sundayFlag;
    }

    public String getSundayFlag() {
        return sundayFlag;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }
}
