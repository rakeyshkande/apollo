package com.ftd.oe.vo;

import java.util.Date;

public class DeliveryDateVO {
    private Date startDate;
    private Date endDate;
    private boolean dateRange=false;
    private String display;
    private String value;
    
    public DeliveryDateVO() {
    }

    public void setStartDate(Date param) {
        this.startDate = param;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date param) {
        this.endDate = param;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setDateRange(boolean param) {
        this.dateRange = param;
    }

    public boolean isDateRange() {
        return dateRange;
    }

    public void setDisplay(String param) {
        this.display = param;
    }

    public String getDisplay() {
        return display;
    }

    public void setValue(String param) {
        this.value = param;
    }

    public String getValue() {
        return value;
    }
}
