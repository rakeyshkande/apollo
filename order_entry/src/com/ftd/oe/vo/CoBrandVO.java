package com.ftd.oe.vo;

public class CoBrandVO {

    private String coBrandName;
    private String coBrandData;

    public void setCoBrandName(String coBrandName) {
        this.coBrandName = coBrandName;
    }

    public String getCoBrandName() {
        return coBrandName;
    }

    public void setCoBrandData(String coBrandData) {
        this.coBrandData = coBrandData;
    }

    public String getCoBrandData() {
        return coBrandData;
    }
}
