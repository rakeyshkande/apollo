package com.ftd.oe.vo.fresh;

public class FreshAddonsVO {
   private String addon_id;
   private String addon_text;
   private String addon_type;
   private String apollo_product_id;
   private String description;
   private String display_seq_num;
   private String florist_addon_flag;
   private String ftd_product_id;
   private String image;
   private String max_qty;
   private String pquad_accessory_id;
   private String price;
   private String vendor_addon_flag;
   
   
   public String getAddon_id() {
      return addon_id;
   }
   public void setAddon_id(String addon_id) {
      this.addon_id = addon_id;
   }
   public String getAddon_text() {
      return addon_text;
   }
   public void setAddon_text(String addon_text) {
      this.addon_text = addon_text;
   }
   public String getAddon_type() {
      return addon_type;
   }
   public void setAddon_type(String addon_type) {
      this.addon_type = addon_type;
   }
   public String getApollo_product_id() {
      return apollo_product_id;
   }
   public void setApollo_product_id(String apollo_product_id) {
      this.apollo_product_id = apollo_product_id;
   }
   public String getDescription() {
      return description;
   }
   public void setDescription(String description) {
      this.description = description;
   }
   public String getDisplay_seq_num() {
      return display_seq_num;
   }
   public void setDisplay_seq_num(String display_seq_num) {
      this.display_seq_num = display_seq_num;
   }
   public String getFlorist_addon_flag() {
      return florist_addon_flag;
   }
   public void setFlorist_addon_flag(String florist_addon_flag) {
      this.florist_addon_flag = florist_addon_flag;
   }
   public String getFtd_product_id() {
      return ftd_product_id;
   }
   public void setFtd_product_id(String ftd_product_id) {
      this.ftd_product_id = ftd_product_id;
   }
   public String getImage() {
      return image;
   }
   public void setImage(String image) {
      this.image = image;
   }
   public String getMax_qty() {
      return max_qty;
   }
   public void setMax_qty(String max_qty) {
      this.max_qty = max_qty;
   }
   public String getPquad_accessory_id() {
      return pquad_accessory_id;
   }
   public void setPquad_accessory_id(String pquad_accessory_id) {
      this.pquad_accessory_id = pquad_accessory_id;
   }
   public String getPrice() {
      return price;
   }
   public void setPrice(String price) {
      this.price = price;
   }
   public String getVendor_addon_flag() {
      return vendor_addon_flag;
   }
   public void setVendor_addon_flag(String vendor_addon_flag) {
      this.vendor_addon_flag = vendor_addon_flag;
   }

}
