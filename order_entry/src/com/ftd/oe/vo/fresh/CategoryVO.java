package com.ftd.oe.vo.fresh;

public class CategoryVO {
   private String level;
   private String categoryName;
   private String displayName;
   
   public String getLevel() {
      return level;
   }
   public void setLevel(String level) {
      this.level = level;
   }
   public String getCategoryName() {
      return categoryName;
   }
   public void setCategoryName(String categoryName) {
      this.categoryName = categoryName;
   }
   public String getDisplayName() {
      return displayName;
   }
   public void setDisplayName(String displayName) {
      this.displayName = displayName;
   }
}
