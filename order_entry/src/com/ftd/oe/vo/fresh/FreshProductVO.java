package com.ftd.oe.vo.fresh;

import java.util.List;
import com.ftd.oe.vo.fresh.UpsellsVO;

public class FreshProductVO {
   private String add_on_cards_flag;
   private String add_on_funeral_flag;
   private String addon_text;
   private String allow_free_shipping_flag;
   private String apollo_product_id;
   private String apollo_product_name;
   private String bullet_description;
   private String color_size_flag;
   private String custom_flag;
   private String delivery_type;
   private String deluxe_price;
   private String discount_allowed_flag;
   private String ftd_product_id;
   private String ftd_product_name;
   private String largeimage;
   private String long_description;
   private String no_tax_flag;
   private String over_21;
   private String pdb_category;
   private String personal_greeting_flag;
   private String pquad_product_id;
   private String premium_price;
   private String product_id;   // Preserved for backwards compatibility - same value as apollo_product_id
   private String product_is_addon_flag;
   private String product_sub_type;
   private String product_type;
   private String popularity_order_cnt;
   private String premier_collection_flag;
   private String short_description;
   private String ship_method_carrier;
   private String ship_method_florist;
   private String shipping_key;
   private String shipping_methods;
   private String shipping_system;
   private String smallimage;
   private String standard_price;
   private String status;
   private String upsell_master_name;
   private String upsell_master_prod_id;
   private String variable_price_max;
   private String weboe_blocked;
   private String mondayDeliveryFreshcut;
   private String morningDeliveryFlag;
   private String expressShippingOnly;
   private String nextDayUpgradeFlag;
   private String addonId;
   private String companies;
   private String upsellMasterCompanies;
   
   
   public String getAdd_on_cards_flag() {
      return add_on_cards_flag;
   }
   public void setAdd_on_cards_flag(String add_on_cards_flag) {
      this.add_on_cards_flag = add_on_cards_flag;
   }
   public String getAdd_on_funeral_flag() {
      return add_on_funeral_flag;
   }
   public void setAdd_on_funeral_flag(String add_on_funeral_flag) {
      this.add_on_funeral_flag = add_on_funeral_flag;
   }
   public String getAddon_text() {
      return addon_text;
   }
   public void setAddon_text(String addon_text) {
      this.addon_text = addon_text;
   }
   public String getAllow_free_shipping_flag() {
      return allow_free_shipping_flag;
   }
   public void setAllow_free_shipping_flag(String allow_free_shipping_flag) {
      this.allow_free_shipping_flag = allow_free_shipping_flag;
   }
   public String getApollo_product_id() {
      return apollo_product_id;
   }
   public void setApollo_product_id(String apollo_product_id) {
      this.apollo_product_id = apollo_product_id;
   }
   public String getApollo_product_name() {
      return apollo_product_name;
   }
   public void setApollo_product_name(String apollo_product_name) {
      this.apollo_product_name = apollo_product_name;
   }
   public String getColor_size_flag() {
      return color_size_flag;
   }
   public void setColor_size_flag(String color_size_flag) {
      this.color_size_flag = color_size_flag;
   }
   public String getCustom_flag() {
      return custom_flag;
   }
   public void setCustom_flag(String custom_flag) {
      this.custom_flag = custom_flag;
   }
   public String getDelivery_type() {
      return delivery_type;
   }
   public void setDelivery_type(String delivery_type) {
      this.delivery_type = delivery_type;
   }
   public String getDeluxe_price() {
      return deluxe_price;
   }
   public void setDeluxe_price(String deluxe_price) {
      this.deluxe_price = deluxe_price;
   }
   public String getDiscount_allowed_flag() {
      return discount_allowed_flag;
   }
   public void setDiscount_allowed_flag(String discount_allowed_flag) {
      this.discount_allowed_flag = discount_allowed_flag;
   }
   public String getFtd_product_id() {
      return ftd_product_id;
   }
   public void setFtd_product_id(String ftd_product_id) {
      this.ftd_product_id = ftd_product_id;
   }
   public String getProduct_is_addon_flag() {
      return product_is_addon_flag;
   }
   public void setProduct_is_addon_flag(String product_is_addon_flag) {
      this.product_is_addon_flag = product_is_addon_flag;
   }
   public String getProduct_sub_type() {
      return product_sub_type;
   }
   public void setProduct_sub_type(String product_sub_type) {
      this.product_sub_type = product_sub_type;
   }
   public String getFtd_product_name() {
      return ftd_product_name;
   }
   public void setFtd_product_name(String ftd_product_name) {
      this.ftd_product_name = ftd_product_name;
   }
   public String getLargeimage() {
      return largeimage;
   }
   public void setLargeimage(String largeimage) {
      this.largeimage = largeimage;
   }
   public String getLong_description() {
      return long_description;
   }
   public void setLong_description(String long_description) {
      this.long_description = long_description;
   }
   public String getNo_tax_flag() {
      return no_tax_flag;
   }
   public void setNo_tax_flag(String no_tax_flag) {
      this.no_tax_flag = no_tax_flag;
   }
   public String getOver_21() {
      return over_21;
   }
   public void setOver_21(String over_21) {
      this.over_21 = over_21;
   }
   public String getPdb_category() {
      return pdb_category;
   }
   public void setPdb_category(String pdb_category) {
      this.pdb_category = pdb_category;
   }
   public String getPersonal_greeting_flag() {
      return personal_greeting_flag;
   }
   public void setPersonal_greeting_flag(String personal_greeting_flag) {
      this.personal_greeting_flag = personal_greeting_flag;
   }
   public String getPquad_product_id() {
      return pquad_product_id;
   }
   public void setPquad_product_id(String pquad_product_id) {
      this.pquad_product_id = pquad_product_id;
   }
   public String getPremium_price() {
      return premium_price;
   }
   public void setPremium_price(String premium_price) {
      this.premium_price = premium_price;
   }
   public String getProduct_id() {
      return product_id;
   }
   public void setProduct_id(String product_id) {
      this.product_id = product_id;
   }
   public String getProduct_type() {
      return product_type;
   }
   public void setProduct_type(String product_type) {
      this.product_type = product_type;
   }
   public String getPopularity_order_cnt() {
      return popularity_order_cnt;
   }
   public void setPopularity_order_cnt(String popularity_order_cnt) {
      this.popularity_order_cnt = popularity_order_cnt;
   }
   public String getPremier_collection_flag() {
      return premier_collection_flag;
   }
   public void setPremier_collection_flag(String premier_collection_flag) {
      this.premier_collection_flag = premier_collection_flag;
   }
   public String getShort_description() {
      return short_description;
   }
   public void setShort_description(String short_description) {
      this.short_description = short_description;
   }
   public String getShip_method_carrier() {
      return ship_method_carrier;
   }
   public void setShip_method_carrier(String ship_method_carrier) {
      this.ship_method_carrier = ship_method_carrier;
   }
   public String getShip_method_florist() {
      return ship_method_florist;
   }
   public void setShip_method_florist(String ship_method_florist) {
      this.ship_method_florist = ship_method_florist;
   }
   public String getShipping_key() {
      return shipping_key;
   }
   public void setShipping_key(String shipping_key) {
      this.shipping_key = shipping_key;
   }
   public String getShipping_methods() {
      return shipping_methods;
   }
   public void setShipping_methods(String shipping_methods) {
      this.shipping_methods = shipping_methods;
   }
   public String getShipping_system() {
      return shipping_system;
   }
   public void setShipping_system(String shipping_system) {
      this.shipping_system = shipping_system;
   }
   public String getSmallimage() {
      return smallimage;
   }
   public void setSmallimage(String smallimage) {
      this.smallimage = smallimage;
   }
   public String getStandard_price() {
      return standard_price;
   }
   public void setStandard_price(String standard_price) {
      this.standard_price = standard_price;
   }
   public String getStatus() {
      return status;
   }
   public void setStatus(String status) {
      this.status = status;
   }
   public String getUpsell_master_name() {
      return upsell_master_name;
   }
   public void setUpsell_master_name(String upsell_master_name) {
      this.upsell_master_name = upsell_master_name;
   }
   public String getUpsell_master_prod_id() {
      return upsell_master_prod_id;
   }
   public void setUpsell_master_prod_id(String upsell_master_prod_id) {
      this.upsell_master_prod_id = upsell_master_prod_id;
   }
   public String getVariable_price_max() {
      return variable_price_max;
   }
   public void setVariable_price_max(String variable_price_max) {
      this.variable_price_max = variable_price_max;
   }
   public String getWeboe_blocked() {
      return weboe_blocked;
   }
   public void setWeboe_blocked(String weboe_blocked) {
      this.weboe_blocked = weboe_blocked;
   }
   public String getBullet_description() {
      return bullet_description;
   }
   public void setBullet_description(String bullet_description) {
      this.bullet_description = bullet_description;
   }
   public String getMondayDeliveryFreshcut() {
		return mondayDeliveryFreshcut;
   }
   public void setMondayDeliveryFreshcut(String mondayDeliveryFreshcut) {
		this.mondayDeliveryFreshcut = mondayDeliveryFreshcut;
   }
   public String getMorningDeliveryFlag() {
		return morningDeliveryFlag;
   }
   public void setMorningDeliveryFlag(String morningDeliveryFlag) {
		this.morningDeliveryFlag = morningDeliveryFlag;
   }
	public String getExpressShippingOnly() {
		return expressShippingOnly;
	}
	public void setExpressShippingOnly(String expressShippingOnly) {
		this.expressShippingOnly = expressShippingOnly;
	}
	public String getNextDayUpgradeFlag() {
		return nextDayUpgradeFlag;
	}
	public void setNextDayUpgradeFlag(String nextDayUpgradeFlag) {
		this.nextDayUpgradeFlag = nextDayUpgradeFlag;
	}
	public String getAddonId() {
		return addonId;
	}
	public void setAddonId(String addonId) {
		this.addonId = addonId;
	}
   public String getCompanies() {
      return companies;
   }
   public String getUpsellMasterCompanies() {
      return upsellMasterCompanies;
   }
   public void setCompanies(String companies) {
      this.companies = companies;
   }
   public void setUpsellMasterCompanies(String upsellMasterCompanies) {
      this.upsellMasterCompanies = upsellMasterCompanies;
   }
	   

}
