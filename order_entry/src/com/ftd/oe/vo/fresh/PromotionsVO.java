package com.ftd.oe.vo.fresh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PromotionsVO {
      private String code;
      private String description;
      private String sourceType;
      private PartnerVO partner;
      private String discountAmount;
      private String discountType;
      private String status;
      private String startDate;
      private String endDate;
      private String iotwFlag;
      private String discountDescription;
      private List<IotwProductsVO> iotwProducts;

      public String getCode() {
          return code;
      }

      public void setCode(String code) {
          this.code = code;
      }

      public String getDescription() {
          return description;
      }

      public void setDescription(String description) {
          this.description = description;
      }

      public PartnerVO getPartner() {
          return partner;
      }

      public void setPartner(PartnerVO partner) {
          this.partner = partner;
      }

      public String getSourceType() {
          return sourceType;
      }

      public void setSourceType(String sourceType) {
          this.sourceType = sourceType;
      }

      public String getDiscountAmount() {
          return discountAmount;
      }

      public void setDiscountAmount(String discountAmount) {
          this.discountAmount = discountAmount;
      }

      public String getDiscountType() {
          return discountType;
      }

      public void setDiscountType(String discountType) {
          this.discountType = discountType;
      }

      public String getStatus() {
          return status;
      }

      public void setStatus(String status) {
          this.status = status;
      }

      public String getStartDate() {
          return startDate;
      }

      public void setStartDate(String startDate) {
          this.startDate = startDate;
      }

      public String getEndDate() {
          return endDate;
      }

      public void setEndDate(String endDate) {
          this.endDate = endDate;
      }

      public String getIotwFlag() {
         return iotwFlag;
      }

      public void setIotwFlag(String iotwFlag) {
         this.iotwFlag = iotwFlag;
      }

      
      public String getDiscountDescription() {
  		return discountDescription;
      }

      public void setDiscountDescription(String discountDescription) {
  		this.discountDescription = discountDescription;
      }
      
      public List<IotwProductsVO> getIotwProducts() {
          return iotwProducts;
      }

      public void setIotwProducts(List<IotwProductsVO> iotwProducts) {
          this.iotwProducts = iotwProducts;
      }
}
