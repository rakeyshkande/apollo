package com.ftd.oe.vo.fresh;

public class UpsellsVO {
   private String upsell_detail_sequence;
   private String upsell_detail_name;
   private String upsell_master_id;
   private String default_sku_flag;
   private String upsell_detail_id;
   private String ftd_product_id;
   private String upsell_price;
   private String smallimage;
   
   public String getUpsell_detail_sequence() {
      return upsell_detail_sequence;
   }
   public void setUpsell_detail_sequence(String upsell_detail_sequence) {
      this.upsell_detail_sequence = upsell_detail_sequence;
   }
   public String getUpsell_detail_name() {
      return upsell_detail_name;
   }
   public void setUpsell_detail_name(String upsell_detail_name) {
      this.upsell_detail_name = upsell_detail_name;
   }
   public String getUpsell_master_id() {
      return upsell_master_id;
   }
   public void setUpsell_master_id(String upsell_master_id) {
      this.upsell_master_id = upsell_master_id;
   }
   public String getDefault_sku_flag() {
      return default_sku_flag;
   }
   public void setDefault_sku_flag(String default_sku_flag) {
      this.default_sku_flag = default_sku_flag;
   }
   public String getUpsell_detail_id() {
      return upsell_detail_id;
   }
   public void setUpsell_detail_id(String upsell_detail_id) {
      this.upsell_detail_id = upsell_detail_id;
   }
   public String getFtd_product_id() {
      return ftd_product_id;
   }
   public void setFtd_product_id(String ftd_product_id) {
      this.ftd_product_id = ftd_product_id;
   }
   public String getUpsell_price() {
      return upsell_price;
   }
   public void setUpsell_price(String upsell_price) {
      this.upsell_price = upsell_price;
   }
   public String getSmallimage() {
      return smallimage;
   }
   public void setSmallimage(String smallimage) {
      this.smallimage = smallimage;
   }

   
}
