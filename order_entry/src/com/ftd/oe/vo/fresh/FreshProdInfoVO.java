package com.ftd.oe.vo.fresh;

import java.util.List;

public class FreshProdInfoVO {
   private FreshProductVO product_details;
   private List<UpsellsVO> product_upsells;
   private List<FreshAddonsVO> product_addons;
   private List<FreshAddonsVO> product_vases;  
   private List<CategoriesVO> categories;
   private List<FreshMarkersVO> product_markers;
   private String message;
   private String status;
   private List<String> product_restrictions;
   private List<String> upsell_restrictions;

      
   public List<UpsellsVO> getProduct_upsells() {
      return product_upsells;
   }
   public void setProduct_upsells(List<UpsellsVO> product_upsells) {
      this.product_upsells = product_upsells;
   }
   public List<FreshAddonsVO> getProduct_addons() {
      return product_addons;
   }
   public void setProduct_addons(List<FreshAddonsVO> product_addons) {
      this.product_addons = product_addons;
   }
   public List<FreshAddonsVO> getProduct_vases() {
      return product_vases;
   }
   public void setProduct_vases(List<FreshAddonsVO> product_vases) {
      this.product_vases = product_vases;
   }
   public List<CategoriesVO> getCategories() {
      return categories;
   }
   public void setCategories(List<CategoriesVO> categories) {
      this.categories = categories;
   }
   public FreshProductVO getProduct_details() {
      return product_details;
   }
   public void setProduct_details(FreshProductVO product_details) {
      this.product_details = product_details;
   }
   public List<FreshMarkersVO> getProduct_markers() {
      return product_markers;
   }
   public void setProduct_markers(List<FreshMarkersVO> product_markers) {
      this.product_markers = product_markers;
   }
   public String getMessage() {
      return message;
   }
   public void setMessage(String message) {
      this.message = message;
   }
   public String getStatus() {
      return status;
   }
   public void setStatus(String status) {
      this.status = status;
   }
   public List<String> getProduct_restrictions() {
      return product_restrictions;
   }
   public void setProduct_restrictions(List<String> product_restrictions) {
      this.product_restrictions = product_restrictions;
   }
   public List<String> getUpsell_restrictions() {
      return upsell_restrictions;
   }
   public void setUpsell_restrictions(List<String> upsell_restrictions) {
      this.upsell_restrictions = upsell_restrictions;
   }

}
