package com.ftd.oe.vo.fresh;

import java.util.List;

public class IotwProductsVO {
      private String ftdProduct;
      private String discountAmount;
      private String discountType;
      private String status;
      private String startDate;
      private String endDate;
      private String iotwAssociatedCode;
      private String discountDescription;
      private List<String> discountDeliveryDates;

      public String getFtdProduct() {
          return ftdProduct;
      }

      public void setFtdProduct(String product) {
          this.ftdProduct = product;
      }

      public String getDiscountAmount() {
          return discountAmount;
      }

      public void setDiscountAmount(String discountAmount) {
          this.discountAmount = discountAmount;
      }

      public String getDiscountType() {
          return discountType;
      }

      public void setDiscountType(String discountType) {
          this.discountType = discountType;
      }

      public String getStatus() {
          return status;
      }

      public void setStatus(String status) {
          this.status = status;
      }

      public String getStartDate() {
          return startDate;
      }

      public void setStartDate(String startDate) {
          this.startDate = startDate;
      }

      public String getEndDate() {
          return endDate;
      }

      public void setEndDate(String endDate) {
          this.endDate = endDate;
      }

      public String getIotwAssociatedCode() {
         return iotwAssociatedCode;
      }

      public void setIotwAssociatedCode(String iotwAssociatedCode) {
         this.iotwAssociatedCode = iotwAssociatedCode;
      }

      public String getDiscountDescription() {
  		return discountDescription;
      }

      public void setDiscountDescription(String discountDescription) {
  		this.discountDescription = discountDescription;
      }
      
      public List<String> getDiscountDeliveryDates() {
          return discountDeliveryDates;
      }

      public void setDiscountDeliveryDates(List<String> discountDeliveryDates) {
          this.discountDeliveryDates = discountDeliveryDates;
      }
}
