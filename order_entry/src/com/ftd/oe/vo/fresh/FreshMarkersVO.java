package com.ftd.oe.vo.fresh;

public class FreshMarkersVO {
   private String markerId;
   private String markerType;
   private String description;
   private String orderBy;
   
   
   public String getMarkerId() {
      return markerId;
   }
   public void setMarkerId(String markerId) {
      this.markerId = markerId;
   }
   public String getMarkerType() {
      return markerType;
   }
   public void setMarkerType(String markerType) {
      this.markerType = markerType;
   }
   public String getDescription() {
      return description;
   }
   public void setDescription(String description) {
      this.description = description;
   }
   public String getOrderBy() {
      return orderBy;
   }
   public void setOrderBy(String orderBy) {
      this.orderBy = orderBy;
   }

}
