package com.ftd.oe.vo.fresh;

public class PartnerVO {
      public String getName() {
          return name;
      }

      public void setName(String name) {
          this.name = name;
      }

      public String getType() {
          return type;
      }

      public void setType(String type) {
          this.type = type;
      }

      private String name;
      private String type;
}
