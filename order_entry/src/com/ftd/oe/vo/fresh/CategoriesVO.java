package com.ftd.oe.vo.fresh;

import java.util.List;

public class CategoriesVO {
   private List<CategoryVO> category;

   public List<CategoryVO> getCategory() {
      return category;
   }

   public void setCategory(List<CategoryVO> category) {
      this.category = category;
   }

}
