package com.ftd.oe.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.lang.reflect.Method;

public class OrderDetailVO implements Cloneable {
    
    private Logger logger = new Logger("com.ftd.oe.vo.OrderDetailVO");
    
    private String productId;
    private Date deliveryDate;
    private String sizeIndicator;
    private String shipMethod;
    private String externalOrderNumber;
    private String orderDetailId;
    private String itemSourceCode;
    private String iotwFlag;
    private String occasion;
    private String cardMessage;
    private String specialInstructions;
    private String orderComments;
    private String floristId;
    private String firstColorChoice;
    private String secondColorChoice;
    private BigDecimal productPrice;
    private BigDecimal retailVariablePrice;
    private BigDecimal discountAmount;
    private BigDecimal serviceFee;
    private BigDecimal shippingFee;
    private BigDecimal sameDayUpcharge; 
    private BigDecimal lateCutoffCharge;
    private BigDecimal mondayUpcharge;
    private BigDecimal sundayUpcharge;
    private BigDecimal lineItemTotal;
    private BigDecimal milesPoints;
    private BigDecimal addonAmount;
    private String rewardType;
    private String productSubcodeId;
    private Date deliveryDateRangeEnd;
    private String recipientId;
    private String cartIndex;
    private CustomerVO recipientVO;
    private List addonVO;
    private BigDecimal alaskaHawaiiFee;
    private PersonalGreetingVO personalGreetingVO;
    private String binSourceChangedFlag;
    private String surchargeDescription;
    private BigDecimal surchargeAmount;
    // List taxVOs
    private ItemTaxVO itemTaxVO;
    boolean taxServicePerformed; 
    private AVSAddressVO avsAddressVO;
    private String originalExternalOrderNumber;
    private String originalOrderDetailId;
    private Date originalDeliveryDate;
    private String originalShipMethod;
    private String originalRecipientId;
    private String originalHasSDU;
    private String timeOfService;
    private String originalProductId;

    //SGC-7
    private BigDecimal internationalFee;
    private BigDecimal originalServiceFeeAmount;
    
    
    public OrderDetailVO() {
        addonVO = new ArrayList();
    }

    public Object clone() {
        OrderDetailVO newVO = null;
        try {
            newVO = (OrderDetailVO)super.clone();
        } catch(CloneNotSupportedException e) {
            logger.error("Clone didn't work");
            newVO = new OrderDetailVO();
        }
        return newVO;
    }
    
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setSizeIndicator(String sizeIndicator) {
        this.sizeIndicator = sizeIndicator;
    }

    public String getSizeIndicator() {
        return sizeIndicator;
    }

    public void setShipMethod(String shipMethod) {
        this.shipMethod = shipMethod;
    }

    public String getShipMethod() {
        return shipMethod;
    }

    public void setRecipientVO(CustomerVO recipientVO) {
        this.recipientVO = recipientVO;
    }

    public CustomerVO getRecipientVO() {
        return recipientVO;
    }

    public void setAddonVO(AddonVO addonVO) {
        this.addonVO.add(addonVO);
    }

    public List getAddonVO() {
        return addonVO;
    }

    public void setExternalOrderNumber(String externalOrderNumber) {
        this.externalOrderNumber = externalOrderNumber;
    }

    public String getExternalOrderNumber() {
        return externalOrderNumber;
    }

    public void setItemSourceCode(String itemSourceCode) {
        this.itemSourceCode = itemSourceCode;
    }

    public String getItemSourceCode() {
        return itemSourceCode;
    }

    public void setIotwFlag(String iotwFlag) {
        this.iotwFlag = iotwFlag;
    }

    public String getIotwFlag() {
        return iotwFlag;
    }

    public void setOccasion(String occasion) {
        this.occasion = occasion;
    }

    public String getOccasion() {
        return occasion;
    }

    public void setCardMessage(String cardMessage) {
        this.cardMessage = cardMessage;
    }

    public String getCardMessage() {
        return cardMessage;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setOrderComments(String orderComments) {
        this.orderComments = orderComments;
    }

    public String getOrderComments() {
        return orderComments;
    }

    public void setFloristId(String floristId) {
        this.floristId = floristId;
    }

    public String getFloristId() {
        return floristId;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setRetailVariablePrice(BigDecimal retailVariablePrice) {
        this.retailVariablePrice = retailVariablePrice;
    }

    public BigDecimal getRetailVariablePrice() {
        return retailVariablePrice;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

	public String getTaxAmount() {
		if (this.itemTaxVO != null) {
			return itemTaxVO.getTaxAmount();
		}
		return ItemTaxVO.ZERO_VALUE;
	}

	public void setTaxAmount(String newTaxAmount) {
		if (this.itemTaxVO == null) {
			itemTaxVO = new ItemTaxVO();
		} 
		this.itemTaxVO.setTaxAmount(newTaxAmount); 
	}

    public void setLineItemTotal(BigDecimal lineItemTotal) {
        this.lineItemTotal = lineItemTotal;
    }

    public BigDecimal getLineItemTotal() {
        return lineItemTotal;
    }

    public void setMilesPoints(BigDecimal milesPoints) {
        this.milesPoints = milesPoints;
    }

    public BigDecimal getMilesPoints() {
        return milesPoints;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setAddonAmount(BigDecimal addonAmount) {
        this.addonAmount = addonAmount;
    }

    public BigDecimal getAddonAmount() {
        return addonAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setFirstColorChoice(String firstColorChoice) {
        this.firstColorChoice = firstColorChoice;
    }

    public String getFirstColorChoice() {
        return firstColorChoice;
    }

    public void setSecondColorChoice(String secondColorChoice) {
        this.secondColorChoice = secondColorChoice;
    }

    public String getSecondColorChoice() {
        return secondColorChoice;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setProductSubcodeId(String productSubcodeId) {
        this.productSubcodeId = productSubcodeId;
    }

    public String getProductSubcodeId() {
        return productSubcodeId;
    }

    public void setDeliveryDateRangeEnd(Date deliveryDateRangeEnd) {
        this.deliveryDateRangeEnd = deliveryDateRangeEnd;
    }

    public Date getDeliveryDateRangeEnd() {
        return deliveryDateRangeEnd;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setCartIndex(String cartIndex) {
        this.cartIndex = cartIndex;
    }

    public String getCartIndex() {
        return cartIndex;
    }

    public void setAlaskaHawaiiFee(BigDecimal alaskaHawaiiFee) {
        this.alaskaHawaiiFee = alaskaHawaiiFee;
    }

    public BigDecimal getAlaskaHawaiiFee() {
        return alaskaHawaiiFee;
    }

    public void setPersonalGreetingVO(PersonalGreetingVO personalGreetingVO) {
        this.personalGreetingVO = personalGreetingVO;
    }

    public PersonalGreetingVO getPersonalGreetingVO() {
        return personalGreetingVO;
    }

    public void setBinSourceChangedFlag(String binSourceChangedFlag) {
        this.binSourceChangedFlag = binSourceChangedFlag;
    }

    public String getBinSourceChangedFlag() {
        return binSourceChangedFlag;
    }

  public void setSurchargeDescription(String surchargeDescription)
  {
    this.surchargeDescription = surchargeDescription;
  }

  public String getSurchargeDescription()
  {
    return surchargeDescription;
  }

  public void setSurchargeAmount(BigDecimal surchargeAmount)
  {
    this.surchargeAmount = surchargeAmount;
  }

  public BigDecimal getSurchargeAmount()
  {
    return surchargeAmount;
  }

public void setSameDayUpcharge(BigDecimal sameDayUpcharge) {
	this.sameDayUpcharge = sameDayUpcharge;
}

public BigDecimal getSameDayUpcharge() {
	return sameDayUpcharge;
}

public void setLateCutOffUpcharge(BigDecimal lateCutoffCharge) {
	this.lateCutoffCharge = lateCutoffCharge;
}

public BigDecimal getLateCutOffUpcharge() {
	return lateCutoffCharge;
}

public void setMondayUpcharge(BigDecimal mondayUpcharge) {
	this.mondayUpcharge = mondayUpcharge;
}

public BigDecimal getMondayUpcharge() {
	return mondayUpcharge;
}

public void setSundayUpcharge(BigDecimal sundayUpcharge) {
	this.sundayUpcharge = sundayUpcharge;
}

public BigDecimal getSundayUpcharge() {
	return sundayUpcharge;
}

public AVSAddressVO getAvsAddressVO() {
	return avsAddressVO;
}

public void setAvsAddressVO(AVSAddressVO avsAddressVO) {
	this.avsAddressVO = avsAddressVO;
}

public ItemTaxVO getItemTaxVO() {
	return itemTaxVO;
}

public void setItemTaxVO(ItemTaxVO itemTaxVo) {
	this.itemTaxVO = itemTaxVo;
}

public boolean isTaxServicePerformed() {
	return taxServicePerformed;
}

public void setTaxServicePerformed(boolean taxServicePerformed) {
	this.taxServicePerformed = taxServicePerformed;
}

public void setOriginalExternalOrderNumber(String originalExternalOrderNumber) {
	this.originalExternalOrderNumber = originalExternalOrderNumber;
}

public String getOriginalExternalOrderNumber() {
	return originalExternalOrderNumber;
}

public void setOriginalOrderDetailId(String originalOrderDetailId) {
	this.originalOrderDetailId = originalOrderDetailId;
}

public String getOriginalOrderDetailId() {
	return originalOrderDetailId;
}

public void setOriginalDeliveryDate(Date originalDeliveryDate) {
	this.originalDeliveryDate = originalDeliveryDate;
}

public Date getOriginalDeliveryDate() {
	return originalDeliveryDate;
}

public void setOriginalShipMethod(String originalShipMethod) {
	this.originalShipMethod = originalShipMethod;
}

public String getOriginalShipMethod() {
	return originalShipMethod;
}

public void setOriginalRecipientId(String originalRecipientId) {
	this.originalRecipientId = originalRecipientId;
}

public String getOriginalRecipientId() {
	return originalRecipientId;
}

public void setOriginalHasSDU(String originalHasSDU) {
	this.originalHasSDU = originalHasSDU;
}

public String getOriginalHasSDU() {
	return originalHasSDU;
}

public String getTimeOfService() {
	return timeOfService;
}

public void setTimeOfService(String timeOfService) {
	this.timeOfService = timeOfService;
}

public void setOriginalProductId(String originalProductId) {
	this.originalProductId = originalProductId;
}

public String getOriginalProductId() {
	return originalProductId;
}

public void setInternationalFee(BigDecimal internationalFee) {
    this.internationalFee = internationalFee;
}

public BigDecimal getInternationalFee() {
    return internationalFee;
}
public void setOriginalServiceFeeAmount(BigDecimal originalServiceFeeAmount) {
    this.originalServiceFeeAmount = originalServiceFeeAmount;
}

public BigDecimal getOriginalServiceFeeAmount() {
    return originalServiceFeeAmount;
}

}
