package com.ftd.oe.vo;

import java.text.SimpleDateFormat;

import java.util.Date;


public class IOTWDeliveryDateVO {
    
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    
    private String iotwId;
    private Date discountDate;
    protected String updatedBy;


    public void setIotwId(String iotwId) {
        this.iotwId = iotwId;
    }

    public String getIotwId() {
        return iotwId;
    }

    public void setDiscountDate(Date newdiscountDate) {
        this.discountDate = newdiscountDate;
    }

    public Date getDiscountDate() {
        return discountDate;
    }

    public String getFormattedDiscountDate() {
        if(getDiscountDate() != null){
            return sdf.format(getDiscountDate());
        } else {
           return "";
        }
    }

    public void setUpdatedBy(String newupdatedBy) {
        this.updatedBy = newupdatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }
}
