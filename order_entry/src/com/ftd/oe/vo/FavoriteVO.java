package com.ftd.oe.vo;

public class FavoriteVO implements Comparable
{
    private String productId;
    private String displaySequence;
    private String updatedBy;



    public void setUpdatedBy(String param)
    {
        this.updatedBy = param;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setDisplaySequence(String param)
    {
        this.displaySequence = param;
    }

    public String getDisplaySequence()
    {
        return displaySequence;
    }

    public int compareTo(Object o)
    {
        if (o instanceof FavoriteVO)
        {
            FavoriteVO vo = (FavoriteVO) o;
            return displaySequence.compareTo(vo.getDisplaySequence());
        }
        
        return -1;
    }
}
