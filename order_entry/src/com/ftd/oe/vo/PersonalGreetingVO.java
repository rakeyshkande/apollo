package com.ftd.oe.vo;

public class PersonalGreetingVO {

    private String recipientName;
    private String productId;
    private String productName;
    private String personalGreetingId;

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setPersonalGreetingId(String personalGreetingId) {
        this.personalGreetingId = personalGreetingId;
    }

    public String getPersonalGreetingId() {
        return personalGreetingId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }
}
