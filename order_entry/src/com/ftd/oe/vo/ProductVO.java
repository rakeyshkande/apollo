package com.ftd.oe.vo;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductVO {
    
    private String productId;
    private String novatorId;
    private String status;
    private String deliveryType;
    private String productType;
    private String weboeBlocked;
    private String productName;
    private String novatorName;
    private String colorSizeFlag;
    private String shortDescription;
    private String longDescription;
    private String addonCardFlag;
    private String addonFuneralFlag;
    private String shipMethodFlorist;
    private String shipMethodCarrier;
    private String noTaxFlag;
    private String shippingKey;
    private String shippingSystem;
    private String discountAllowedFlag;
    private BigDecimal productPrice;
    private BigDecimal discountedPrice;
    private BigDecimal discountAmount;
    private BigDecimal milesPoints;
    private String rewardType;
    private BigDecimal standardPrice;
    private BigDecimal deluxePrice;
    private BigDecimal premiumPrice;
    private BigDecimal netStandardPrice;
    private BigDecimal netDeluxePrice;
    private BigDecimal netPremiumPrice;
    private boolean hasSubcodes;
    private boolean hasUpsells;
    private String exceptionCode;
    private Date exceptionStartDate;
    private Date exceptionEndDate;
    private String exceptionMessage;
    private String smallImage;
    private String largeImage;
    private String customFlag;
    private String popularityOrderCount;
    private String productPageMessage;
    private String over21Flag;
    private List upsellDetailIds;
    private String personalGreetingFlag;
    private String premierCollectionFlag;
    private List addonIds;
    private String allowFreeShippingFlag;
    

    public ProductVO() {
        upsellDetailIds = new ArrayList();
        addonIds = new ArrayList();
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setWeboeBlocked(String weboeBlocked) {
        this.weboeBlocked = weboeBlocked;
    }

    public String getWeboeBlocked() {
        return weboeBlocked;
    }

    public void setShipMethodFlorist(String shipMethodFlorist) {
        this.shipMethodFlorist = shipMethodFlorist;
    }

    public String getShipMethodFlorist() {
        return shipMethodFlorist;
    }

    public void setShipMethodCarrier(String shipMethodCarrier) {
        this.shipMethodCarrier = shipMethodCarrier;
    }

    public String getShipMethodCarrier() {
        return shipMethodCarrier;
    }

    public void setNoTaxFlag(String noTaxFlag) {
        this.noTaxFlag = noTaxFlag;
    }

    public String getNoTaxFlag() {
        return noTaxFlag;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setDiscountedPrice(BigDecimal discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public BigDecimal getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setMilesPoints(BigDecimal milesPoints) {
        this.milesPoints = milesPoints;
    }

    public BigDecimal getMilesPoints() {
        return milesPoints;
    }

    public void setDiscountAllowedFlag(String discountAllowedFlag) {
        this.discountAllowedFlag = discountAllowedFlag;
    }

    public String getDiscountAllowedFlag() {
        return discountAllowedFlag;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setStandardPrice(BigDecimal standardPrice) {
        this.standardPrice = standardPrice;
    }

    public BigDecimal getStandardPrice() {
        return standardPrice;
    }

    public void setDeluxePrice(BigDecimal deluxePrice) {
        this.deluxePrice = deluxePrice;
    }

    public BigDecimal getDeluxePrice() {
        return deluxePrice;
    }

    public void setPremiumPrice(BigDecimal premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    public BigDecimal getPremiumPrice() {
        return premiumPrice;
    }

    public void setNetStandardPrice(BigDecimal netStandardPrice) {
        this.netStandardPrice = netStandardPrice;
    }

    public BigDecimal getNetStandardPrice() {
        return netStandardPrice;
    }

    public void setNetDeluxePrice(BigDecimal netDeluxePrice) {
        this.netDeluxePrice = netDeluxePrice;
    }

    public BigDecimal getNetDeluxePrice() {
        return netDeluxePrice;
    }

    public void setNetPremiumPrice(BigDecimal netPremiumPrice) {
        this.netPremiumPrice = netPremiumPrice;
    }

    public BigDecimal getNetPremiumPrice() {
        return netPremiumPrice;
    }

    public void setHasSubcodes(boolean hasSubcodes) {
        this.hasSubcodes = hasSubcodes;
    }

    public boolean isHasSubcodes() {
        return hasSubcodes;
    }

    public void setHasUpsells(boolean hasUpsells) {
        this.hasUpsells = hasUpsells;
    }

    public boolean isHasUpsells() {
        return hasUpsells;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionStartDate(Date exceptionStartDate) {
        this.exceptionStartDate = exceptionStartDate;
    }

    public Date getExceptionStartDate() {
        return exceptionStartDate;
    }

    public void setExceptionEndDate(Date exceptionEndDate) {
        this.exceptionEndDate = exceptionEndDate;
    }

    public Date getExceptionEndDate() {
        return exceptionEndDate;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setNovatorName(String novatorName) {
        this.novatorName = novatorName;
    }

    public String getNovatorName() {
        return novatorName;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    public String getLargeImage() {
        return largeImage;
    }

    public void setNovatorId(String novatorId) {
        this.novatorId = novatorId;
    }

    public String getNovatorId() {
        return novatorId;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setShippingKey(String shippingKey) {
        this.shippingKey = shippingKey;
    }

    public String getShippingKey() {
        return shippingKey;
    }

    public void setCustomFlag(String customFlag) {
        this.customFlag = customFlag;
    }

    public String getCustomFlag() {
        return customFlag;
    }

    public void setColorSizeFlag(String colorSizeFlag) {
        this.colorSizeFlag = colorSizeFlag;
    }

    public String getColorSizeFlag() {
        return colorSizeFlag;
    }

    public void setAddonCardFlag(String addonCardFlag) {
        this.addonCardFlag = addonCardFlag;
    }

    public String getAddonCardFlag() {
        return addonCardFlag;
    }

    public void setAddonFuneralFlag(String addonFuneralFlag) {
        this.addonFuneralFlag = addonFuneralFlag;
    }

    public String getAddonFuneralFlag() {
        return addonFuneralFlag;
    }

    public void setPopularityOrderCount(String popularityOrderCount) {
        this.popularityOrderCount = popularityOrderCount;
    }

    public String getPopularityOrderCount() {
        return popularityOrderCount;
    }

    public void setProductPageMessage(String productPageMessage) {
        this.productPageMessage = productPageMessage;
    }

    public String getProductPageMessage() {
        return productPageMessage;
    }

    public void setOver21Flag(String over21Flag) {
        this.over21Flag = over21Flag;
    }

    public String getOver21Flag() {
        return over21Flag;
    }

    public void setUpsellDetailIds(String upsellDetailId) {
        this.upsellDetailIds.add(upsellDetailId);
    }

    public List getUpsellDetailIds() {
        return upsellDetailIds;
    }

    public void setPersonalGreetingFlag(String personalGreetingFlag) {
        this.personalGreetingFlag = personalGreetingFlag;
    }

    public String getPersonalGreetingFlag() {
        return personalGreetingFlag;
    }

    public void setPremierCollectionFlag(String premierCollectionFlag) {
        this.premierCollectionFlag = premierCollectionFlag;
    }

    public String getPremierCollectionFlag() {
        return premierCollectionFlag;
    }

    public void setAddonIds(String addonId) {
        this.addonIds.add(addonId);
    }

    public List getAddonIds() {
        return addonIds;
    }

    public void setAllowFreeShippingFlag(String allowFreeShippingFlag) {
        this.allowFreeShippingFlag = allowFreeShippingFlag;
    }

    public String getAllowFreeShippingFlag() {
        return allowFreeShippingFlag;
    }

	public String getShippingSystem() {
		return shippingSystem;
	}

	public void setShippingSystem(String shippingSystem) {
		this.shippingSystem = shippingSystem;
	}
	
	
}
