package com.ftd.oe.vo;

public class ProductCheckVO
{
    private String productId;
    private String novatorId;
    private String upsellMasterId;
    
    public ProductCheckVO()
    {
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setUpsellMasterId(String upsellMasterId)
    {
        this.upsellMasterId = upsellMasterId;
    }

    public String getUpsellMasterId()
    {
        return upsellMasterId;
    }
    
    public boolean isProduct()
    {
        if (productId != null)
        {
            return true;
        }
        return false;
    }
    
    public boolean isUpsell()
    {
        if (upsellMasterId != null)
        {
            return true;
        }
        return false;
    }
    
    public String getNormalizedNovatorId()
    {
        if (novatorId != null)
        {
            return novatorId;
        }
        else
        {
            return upsellMasterId;
        }
    }

    public String getNormalizedId()
    {
        if (productId != null)
        {
            return productId;
        }
        else
        {
            return upsellMasterId;
        }
    }

    public void setNovatorId(String newnovatorId) {
        this.novatorId = newnovatorId;
    }

    public String getNovatorId() {
        return novatorId;
    }
}
