package com.ftd.oe.vo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CreditCardVO {

	private String creditCardType;
	private String creditCardNumber;
	private String creditCardExpiration;
	private String approvalAmount;
	private String approvalCode;
	private String approvalVerbiage;
	private String approvalActionCode;
	private String avsResultCode;
	private String acqReferenceData;
	private String giftCertificateId;
	private BigDecimal giftCertificateAmount;
	private String noChargeApprovalId;
	private String noChargeApprovalPassword;
	private String noChargeType;
	private String noChargeReason;
	private String noChargeOrderReference;
	private String noChargeOrderDetailId;
	private BigDecimal noChargeAmount;
	private String aafesTicketNumber;
	private String status;
	private String validationStatus;
	private boolean underMinAuthAmt;
	private String keyName;
	private boolean authorizationRequired;
	private String cscValue;
	private String cscOverrideFlag;
	private int cscFailedCount;
	private String cscResponseCode;
	private String cscValidateFlag;
	private String creditCardDnisType;
	private String ccAuthProvider;
	private String walletIndicator;
	private String merchantRefId;
	
	
	// To hold extra payment response elements 
	private Map<String, Object> paymentExtMap;


	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public String getCreditCardExpiration() {
		return creditCardExpiration;
	}

	public void setCreditCardExpiration(String creditCardExpiration) {
		this.creditCardExpiration = creditCardExpiration;
	}

	public void setApprovalAmount(String approvalAmount) {
		this.approvalAmount = approvalAmount;
	}

	public String getApprovalAmount() {
		return approvalAmount;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalVerbiage(String approvalVerbiage) {
		this.approvalVerbiage = approvalVerbiage;
	}

	public String getApprovalVerbiage() {
		return approvalVerbiage;
	}

	public void setApprovalActionCode(String approvalActionCode) {
		this.approvalActionCode = approvalActionCode;
	}

	public String getApprovalActionCode() {
		return approvalActionCode;
	}

	public void setAvsResultCode(String avsResultCode) {
		this.avsResultCode = avsResultCode;
	}

	public String getAvsResultCode() {
		return avsResultCode;
	}

	public void setAcqReferenceData(String acqReferenceData) {
		this.acqReferenceData = acqReferenceData;
	}

	public String getAcqReferenceData() {
		return acqReferenceData;
	}

	public void setGiftCertificateId(String giftCertificateId) {
		this.giftCertificateId = giftCertificateId;
	}

	public String getGiftCertificateId() {
		return giftCertificateId;
	}

	public void setGiftCertificateAmount(BigDecimal giftCertificateAmount) {
		this.giftCertificateAmount = giftCertificateAmount;
	}

	public BigDecimal getGiftCertificateAmount() {
		return giftCertificateAmount;
	}

	public void setNoChargeApprovalId(String noChargeApprovalId) {
		this.noChargeApprovalId = noChargeApprovalId;
	}

	public String getNoChargeApprovalId() {
		return noChargeApprovalId;
	}

	public void setNoChargeApprovalPassword(String noChargeApprovalPassword) {
		this.noChargeApprovalPassword = noChargeApprovalPassword;
	}

	public String getNoChargeApprovalPassword() {
		return noChargeApprovalPassword;
	}

	public void setNoChargeType(String noChargeType) {
		this.noChargeType = noChargeType;
	}

	public String getNoChargeType() {
		return noChargeType;
	}

	public void setNoChargeReason(String noChargeReason) {
		this.noChargeReason = noChargeReason;
	}

	public String getNoChargeReason() {
		return noChargeReason;
	}

	public void setNoChargeOrderReference(String noChargeOrderReference) {
		this.noChargeOrderReference = noChargeOrderReference;
	}

	public String getNoChargeOrderReference() {
		return noChargeOrderReference;
	}

	public void setNoChargeAmount(BigDecimal noChargeAmount) {
		this.noChargeAmount = noChargeAmount;
	}

	public BigDecimal getNoChargeAmount() {
		return noChargeAmount;
	}

	public void setAafesTicketNumber(String aafesTicketNumber) {
		this.aafesTicketNumber = aafesTicketNumber;
	}

	public String getAafesTicketNumber() {
		return aafesTicketNumber;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}

	public String getValidationStatus() {
		return validationStatus;
	}

	public void setUnderMinAuthAmt(boolean underMinAuthAmt) {
		this.underMinAuthAmt = underMinAuthAmt;
	}

	public boolean isUnderMinAuthAmt() {
		return underMinAuthAmt;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setNoChargeOrderDetailId(String noChargeOrderDetailId) {
		this.noChargeOrderDetailId = noChargeOrderDetailId;
	}

	public String getNoChargeOrderDetailId() {
		return noChargeOrderDetailId;
	}

	public void setAuthorizationRequired(boolean authorizationRequired) {
		this.authorizationRequired = authorizationRequired;
	}

	public boolean isAuthorizationRequired() {
		return authorizationRequired;
	}


	public void setCscValue(String cscValue) {
		this.cscValue = cscValue;
	}

	public String getCscValue() {
		return cscValue;
	}

	public void setCscOverrideFlag(String cscOverrideFlag) {
		this.cscOverrideFlag = cscOverrideFlag;
	}

	public String getCscOverrideFlag() {
		return cscOverrideFlag;
	}

	public void setCscFailedCount(int cscFailedCount) {
		this.cscFailedCount = cscFailedCount;
	}

	public int getCscFailedCount() {
		return cscFailedCount;
	}

	public void setCscResponseCode(String cscResponseCode) {
		this.cscResponseCode = cscResponseCode;
	}

	public String getCscResponseCode() {
		return cscResponseCode;
	}


	public void setCscValidateFlag(String cscValidateFlag) {
		this.cscValidateFlag = cscValidateFlag;
	}

	public String getCscValidateFlag() {
		return cscValidateFlag;
	}

	public void setCreditCardDnisType(String creditCardDnisType) {
		this.creditCardDnisType = creditCardDnisType;
	}

	public String getCreditCardDnisType() {
		return creditCardDnisType;
	}

	public Map<String, Object> getPaymentExtMap() {
		if(paymentExtMap == null){
			paymentExtMap = new HashMap<String, Object>();
		}
		return paymentExtMap;
	}

	public String getCcAuthProvider() {
		return ccAuthProvider;
	}

	public void setCcAuthProvider(String ccAuthProvider) {
		this.ccAuthProvider = ccAuthProvider;
	}
	
	public void setWalletIndicator(String newWalletIndicator)
	{
	  this.walletIndicator = newWalletIndicator;
	}

	public String getMerchantRefId() {
		return merchantRefId;
	}

	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	public String getWalletIndicator()
	{
	  return walletIndicator;
	}


}
