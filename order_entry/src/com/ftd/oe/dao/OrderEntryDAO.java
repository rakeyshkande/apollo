package com.ftd.oe.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.oe.vo.AddonVO;
import com.ftd.oe.vo.CoBrandVO;
import com.ftd.oe.vo.CreditCardVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.FloristLookupVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.PersonalGreetingVO;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.TaxSplitOrder;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;

public class OrderEntryDAO {

    public static final Logger logger = new Logger("com.ftd.oe.dao.OrderEntryDAO");
    protected final static String IN_DNIS_ID = "IN_DNIS_ID";
    protected final static String IN_SOURCE_CODE = "IN_SOURCE_CODE";
    protected final static String IN_VALID_UPSELL_IDS = "IN_VALID_UPSELL_IDS";
    protected final static String IN_ZIP_CODE = "IN_ZIP_CODE";
    protected final static String IN_PRODUCT_ID = "IN_PRODUCT_ID";
    protected final static String IN_PHONE_NUMBER = "IN_PHONE_NUMBER";
    protected final static String IN_COUNTRY_ID = "IN_COUNTRY_ID";
    protected final static String IN_CITY = "IN_CITY";
    protected final static String IN_STATE = "IN_STATE";
    protected final static String IN_GIFT_CERTIFICATE = "IN_GIFT_CERTIFICATE";
    protected final static String IN_FLORIST_ID = "IN_FLORIST_ID";
    protected final static String IN_COMPANY_ID = "IN_COMPANY_ID";
    protected final static String IN_FILTER_1_VALUE = "IN_FILTER_1_VALUE";
    protected final static String IN_CONTENT_CONTEXT = "IN_CONTENT_CONTEXT";
    protected final static String IN_CONTENT_NAME = "IN_CONTENT_NAME";
    protected final static String IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
    
    public OrderEntryDAO() {
    }
    
    public Document getIntroDataAjax(Connection conn) throws Exception {

        logger.debug("getIntroDataXML()");
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_INTRO_DATA");
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        String surchargeText = getContentWithFilter(conn, "TAX", "TAX_EXPLANATION", null, null);
        Element root = doc.getDocumentElement();
        Element surcharge_explanation = doc.createElement("surcharge-explanation");
        root.appendChild(surcharge_explanation);        
        surcharge_explanation.appendChild(doc.createCDATASection(surchargeText));
        
        return doc;
    }
    
    public CachedResultSet getDeliveryDateRanges(Connection conn) throws Exception {
        logger.debug("getDeliveryDateRanges()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES");

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;        
    }
    
    public Document getDnisAjax(Connection conn, String dnisId) throws Exception {
        
        logger.debug("getDnisXML(" + dnisId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_DNIS");
        Map inputParams = new HashMap();
        inputParams.put(IN_DNIS_ID, dnisId);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public Document getSourceCodeAjax(Connection conn, String sourceCode, String dnisId) throws Exception {
        
        logger.debug("getSourceCodeXML(" + sourceCode + ", " + dnisId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_SOURCE_CODE");
        Map inputParams = new HashMap();
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        inputParams.put(IN_DNIS_ID, dnisId);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public Document getZipCodeAjax(Connection conn, String zipCode) throws Exception {
        
        logger.debug("getZipCodeXML(" + zipCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_ZIP_CODE");
        Map inputParams = new HashMap();
        inputParams.put(IN_ZIP_CODE, zipCode);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
    
    public Document getStateByZipcodeAndCityAjax(Connection conn, String zipCode, String city) throws Exception{
    	logger.debug("getStateByZipcodeAndCity( ZipCode :" + zipCode + ")");
    	logger.debug("getStateByZipcodeAndCity( City : " + city + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_STATE_BY_ZIPCODE_CITY");
        Map inputParams = new HashMap();
        inputParams.put(IN_ZIP_CODE, zipCode);
        inputParams.put(IN_CITY, city);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    	
    }

    public Document getCustomerByPhoneNumberAjax(Connection conn, String phoneNumber, String companyId,
        String sourceCode) throws Exception {

        logger.debug("getCustomerByPhoneNumber(" + phoneNumber + ", " + companyId + ", " + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_CUSTOMER_BY_PHONE_NUMBER");
        Map inputParams = new HashMap();
        inputParams.put(IN_PHONE_NUMBER, phoneNumber);
        inputParams.put(IN_COMPANY_ID, companyId);
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
    
    public Document getProductDetailsAjax(Connection conn, String productId, String sourceCode) throws Exception 
    {
        logger.debug("getProductDetailsAjax(" + productId + ", " + sourceCode + ")");
        return getProductDetailsAjax(conn, productId, sourceCode, null);
    }
    
    public Document getProductDetailsAjax(Connection conn, String productId, String sourceCode, String validUpsellIds) throws Exception 
    {
        
        logger.debug("getProductDetailsAjax(" + productId + ", " + sourceCode + ", " + validUpsellIds + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_PRODUCT_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put(IN_PRODUCT_ID, productId);
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        inputParams.put(IN_VALID_UPSELL_IDS, validUpsellIds);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
    
    /* Temporary methods for Project Fresh
     */
    public Document getProductDetailsFresh(Connection conn, String productId, String sourceCode) throws Exception 
    {
        logger.debug("getProductDetailsFresh(" + productId + ", " + sourceCode + ")");
        return getProductDetailsFresh(conn, productId, sourceCode, null);
    }
    
    public Document getProductDetailsFresh(Connection conn, String productId, String sourceCode, String validUpsellIds) throws Exception 
    {
        
        logger.debug("getProductDetailsFresh(" + productId + ", " + sourceCode + ", " + validUpsellIds + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FRESH_GET_PRODUCT_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put(IN_PRODUCT_ID, productId);
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        inputParams.put(IN_VALID_UPSELL_IDS, validUpsellIds);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
    
    
    public Document getElementData(Connection conn, String application) throws Exception {
        
        logger.debug("getElementData(" + application + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_ELEMENT_DATA");
        Map inputParams = new HashMap();
        inputParams.put("IN_APP", application);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public Document getZipsByCityStateAjax(Connection conn, String city, String state) throws Exception {
        
        logger.debug("getZipsByCityStateAjax(" + city + ", " + state + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_ZIPS_BY_CITY_STATE");
        Map inputParams = new HashMap();
        inputParams.put(IN_CITY, city);
        inputParams.put(IN_STATE, state);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public CachedResultSet getCountryMasterById(Connection conn, String countryId) throws Exception {
        logger.debug("getCountryMasterById(" + countryId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_COUNTRY_MASTER_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put(IN_COUNTRY_ID, countryId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getSourceCodeById(Connection conn, String sourceCode) throws Exception {
        logger.debug("getSourceCodeById(" + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_SOURCE_CODE_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getPreferredPartnerBySourceCode(Connection conn, String sourceCode) throws Exception {
        logger.debug("getPreferredPartnerBySourceCode(" + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_PREFERRED_PART_BY_SOURCE");
        Map inputParams = new HashMap();
        inputParams.put(IN_SOURCE_CODE, sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }
    
  public CachedResultSet getPreferredPartnerInfoByPartner(Connection conn, String preferredPartner) throws Exception {
        logger.debug("getPreferredPartnerInfoByPartner(" + preferredPartner + ")");

      	/* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PREFERRED_PINFO");
        Map inputParams = new HashMap();
        inputParams.put(IN_FILTER_1_VALUE, preferredPartner);
        inputParams.put(IN_CONTENT_CONTEXT, "PREFERRED_PARTNER");
        inputParams.put(IN_CONTENT_NAME, "JOE_SCRIPT_TOKEN");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getGiftCertificateDetails(Connection conn, String giftCertificateId) throws Exception {
        logger.debug("getGiftCertificateDetails(" + giftCertificateId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_GIFT_CERTIFICATE_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put(IN_GIFT_CERTIFICATE, giftCertificateId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public Map getFloristValidationInfo(Connection conn, String floristId, String productId,
        String zipCode, Date deliveryDate) throws Exception {
        
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        logger.info("getFloristValidationInfo(" + floristId + ", " + productId + 
            ", " + zipCode + ", " + sdf.format(deliveryDate)+ ")");
        
        java.sql.Date thisDate = new java.sql.Date(deliveryDate.getTime());

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_FLORIST_VALIDATION_INFO");
        Map inputParams = new HashMap();
        inputParams.put(IN_FLORIST_ID, floristId);
        inputParams.put(IN_PRODUCT_ID, productId);
        inputParams.put(IN_ZIP_CODE, zipCode);
        inputParams.put("IN_DELIVERY_DATE", thisDate);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Map output = (Map) dataAccessUtil.execute(dataRequest);

        return output;

        }

    public CachedResultSet getPriceHeaderDetails(Connection conn, String priceHeaderId, BigDecimal productAmount) throws Exception {
        logger.debug("getPriceHeaderDetails(" + priceHeaderId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRICE_HEADER_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRICE_HEADER_ID",priceHeaderId);
        inputParams.put("IN_PRICE_AMT", new Double(productAmount.doubleValue()));
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getPartnerProgramTerms(Connection conn, String sourceCode, Date deliveryDate) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        logger.debug("getPartnerProgramTerms(" + sourceCode + ", " + sdf.format(deliveryDate) + ")");

        java.sql.Date thisDate = new java.sql.Date(deliveryDate.getTime());

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PARTNER_PROGRAM_TERMS");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        inputParams.put("IN_DATE", thisDate);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getProgramRewardRange(Connection conn, String programName) throws Exception {
        logger.debug("getProgramRewardRange(" + programName + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PROGRAM_REWARD_RANGE");
        Map inputParams = new HashMap();
        inputParams.put("IN_PROGRAM_NAME", programName);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getProductExcludedStatesByZip(Connection conn, String productId, String zipCode) throws Exception {
        logger.debug("getProductExcludedStatesByZip(" + productId + ", " + zipCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_EXCLUDED_STATES_BY_ZIP");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_ZIP_CODE", zipCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getBillingInfo(Connection conn, String sourceCode) throws Exception {
        logger.debug("getBillingInfo(" + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_BILLING_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE_ID", sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getBillingInfoOptions(Connection conn, String sourceCode, String infoDescription) throws Exception {
        logger.debug("getBillingInfoOptions(" + sourceCode + ", " + infoDescription  + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_BILLING_INFO_OPTIONS");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE_ID", sourceCode);
        inputParams.put("IN_INFO_DESCRIPTION", infoDescription);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public ArrayList <FloristLookupVO> getFloristInfo(Connection conn, String [] floristId, String sourceCode) throws Exception {
        logger.debug("getFloristInfo(" + floristId + ")");

        /* build DataRequest object */
        ArrayList <FloristLookupVO> floristLookupVOList = new ArrayList <FloristLookupVO> ();
        
        StringBuilder floristIdString = new StringBuilder();
        for (int i = 0; i < floristId.length; i++)
        {
            if (i == floristId.length -1)
            {
                floristIdString.append("'" + floristId[i] + "'");
            }
            else
            {
                floristIdString.append("'" + floristId[i] + "',");
            }    
        }
        
        sourceCode = "'" + sourceCode + "'";     
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_FLORIST_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_FLORIST_ID_LIST", floristIdString.toString());
        inputParams.put("IN_SOURCE_CODE_ID", sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (cr.next())
        {
            
            FloristLookupVO floristLookupVO = new FloristLookupVO();
            
            floristLookupVO.setFloristId(cr.getString("florist_id"));
            floristLookupVO.setFloristName(cr.getString("florist_name"));
            floristLookupVO.setFloristAddress(cr.getString("address"));
            floristLookupVO.setPhoneNumber(cr.getString("phone_number"));
            floristLookupVO.setMercuryFlag(cr.getString("mercury_flag"));
            floristLookupVO.setFloristWeight(cr.getString("florist_weight"));
            floristLookupVO.setGotoFlag(cr.getString("super_florist_flag"));
            floristLookupVO.setSundayFlag(cr.getString("sunday_delivery_flag"));
            floristLookupVO.setPriority(cr.getString("priority"));
            
            floristLookupVOList.add(floristLookupVO);
        }

        return floristLookupVOList;
    }

    public CachedResultSet getAddonById(Connection conn, String addonId) throws Exception {
        logger.debug("getAddonById(" + addonId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ADDON_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_ADDON_ID", addonId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getStateDetails(Connection conn, String stateId) throws Exception {
        logger.debug("getStateDetails(" + stateId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_STATE_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put("IN_STATE_MASTER_ID", stateId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getSNHById(Connection conn, String snhId, String deliveryDate) throws Exception {
        logger.debug("getSNHById(" + snhId + ", " + deliveryDate + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_SNH_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_SNH_ID", snhId);
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getShippingCost(Connection conn, String shippingKey, String shipMethod, double productPrice) throws Exception {
        logger.debug("getShippingCost(" + shippingKey + ", " + shipMethod + ", " + productPrice + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_SHIPPING_COST");
        Map inputParams = new HashMap();
        inputParams.put("IN_SHIPPING_KEY", shippingKey);
        inputParams.put("IN_SHIP_METHOD", shipMethod);
        inputParams.put("IN_PRODUCT_PRICE", productPrice);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getProductCrossSellAjax(Connection conn, String productId) throws Exception {
        
        logger.debug("getProductCrossSellAjax(" + productId + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_PRODUCT_CROSS_SELL");
        Map inputParams = new HashMap();
        inputParams.put(IN_PRODUCT_ID, productId);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getValidQMSResponse(Connection conn, String qmsResponseCode) throws Exception {
        logger.debug("getValidQMSResponse(" + qmsResponseCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_VALID_QMS_RESPONSE");
        Map inputParams = new HashMap();
        inputParams.put("IN_QMS_RESPONSE_CODE", qmsResponseCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getPaymentMethodById(Connection conn, String paymentMethodId) throws Exception {
        logger.debug("getPaymentMethodById(" + paymentMethodId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_PAYMENT_METHOD_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PAYMENT_METHOD_ID", paymentMethodId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet authenticateIdentity(Connection conn, String identityId, String credentials) throws Exception {
        logger.debug("authenticateIdentity(" + identityId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_AUTHENTICATE_IDENTITY");
        Map inputParams = new HashMap();
        inputParams.put("IN_IDENTITY_ID", identityId);
        inputParams.put("IN_CREDENTIALS", credentials);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public HashMap persistCustomer(Connection conn, CustomerVO custVO) throws Exception {
        logger.debug("persistBuyer()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_CUSTOMER");
        Map inputParams = new HashMap();
        inputParams.put("IN_FIRST_NAME", custVO.getFirstName());
        inputParams.put("IN_LAST_NAME", custVO.getLastName());
        inputParams.put("IN_ADDRESS", custVO.getAddress());
        inputParams.put("IN_CITY", custVO.getCity());
        inputParams.put("IN_STATE", custVO.getState());
        inputParams.put("IN_ZIP_CODE", custVO.getZipCode());
        inputParams.put("IN_COUNTRY", custVO.getCountry());
        inputParams.put("IN_BUYER_RECIP_IND", custVO.getBuyerRecipientIndicator());
        inputParams.put("IN_DAYTIME_PHONE", custVO.getDayPhone());
        inputParams.put("IN_DAYTIME_PHONE_EXT", custVO.getDayPhoneExt());
        inputParams.put("IN_EVENING_PHONE", custVO.getEveningPhone());
        inputParams.put("IN_EVENING_PHONE_EXT", custVO.getEveningPhoneExt());
        inputParams.put("IN_EMAIL_ADDRESS", custVO.getEmailAddress());
        inputParams.put("IN_NEWSLETTER_FLAG", custVO.getNewsletterFlag());
        inputParams.put("IN_QMS_RESPONSE_CODE", custVO.getQMSResultCode());
        inputParams.put("IN_ADDRESS_TYPE", custVO.getAddressType());
        inputParams.put("IN_BUSINESS_NAME", custVO.getBusinessName());
        inputParams.put("IN_BUSINESS_INFO", custVO.getBusinessInfo());
        inputParams.put("IN_MEMBERSHIP_ID", custVO.getMembershipId());
        inputParams.put("IN_MEMBERSHIP_FIRST_NAME", custVO.getMembershipFirstName());
        inputParams.put("IN_MEMBERSHIP_LAST_NAME", custVO.getMembershipLastName());
        inputParams.put("IN_PROGRAM_NAME", custVO.getMembershipType());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap persistPayments(Connection conn, CreditCardVO ccVO, String masterOrderNumber) throws Exception {
        logger.debug("persistPayments()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_PAYMENTS");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        inputParams.put("IN_PAYMENT_METHOD_ID", ccVO.getCreditCardType());
        inputParams.put("IN_CC_NUMBER", ccVO.getCreditCardNumber());
        inputParams.put("IN_KEY_NAME", ccVO.getKeyName());
        inputParams.put("IN_CC_EXPIRATION", ccVO.getCreditCardExpiration());
        inputParams.put("IN_AUTH_AMT", ccVO.getApprovalAmount());
        inputParams.put("IN_APPROVAL_CODE", ccVO.getApprovalCode());
        inputParams.put("IN_APPROVAL_VERBIAGE", ccVO.getApprovalVerbiage());
        inputParams.put("IN_APPROVAL_ACTION_CODE", ccVO.getApprovalActionCode());
        inputParams.put("IN_ACQ_REFERENCE_NUMBER", ccVO.getAcqReferenceData());
        inputParams.put("IN_AVS_RESULT_CODE", ccVO.getAvsResultCode());
        inputParams.put("IN_GC_COUPON_NUMBER", ccVO.getGiftCertificateId());
        inputParams.put("IN_GC_AMT", ccVO.getGiftCertificateAmount());
        inputParams.put("IN_AAFES_TICKET_NUMBER", ccVO.getAafesTicketNumber());
        inputParams.put("IN_NC_APPROVAL_IDENTITY_ID", ccVO.getNoChargeApprovalId());
        inputParams.put("IN_NC_TYPE_CODE", ccVO.getNoChargeType());
        inputParams.put("IN_NC_REASON_CODE", ccVO.getNoChargeReason());
        inputParams.put("IN_NC_ORDER_DETAIL_ID", ccVO.getNoChargeOrderDetailId());
        inputParams.put("IN_NC_AMT", ccVO.getNoChargeAmount());
        inputParams.put("IN_CSC_RESPONSE_CODE", ccVO.getCscResponseCode());
        inputParams.put("IN_CSC_VALIDATED_FLAG", ccVO.getCscValidateFlag());
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal(ccVO.getCscFailedCount()));
        inputParams.put("IN_CC_AUTH_PROVIDER",ccVO.getCcAuthProvider());
        
        String delimitedPaymentExtFields = null;
		if(ccVO.getPaymentExtMap()!=null && ccVO.getPaymentExtMap().size()>0){
        	Map<String,Object> paymentExtInfo = new HashMap(ccVO.getPaymentExtMap());
        	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
        	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
        		String deilimitedCardSpecificDetails = FieldUtils.getDelimitedStringFromMap(cardSpecificDetailsMap,":::","///");
        		if(deilimitedCardSpecificDetails!=null){
        			paymentExtInfo.put(PaymentExtensionConstants.CARD_SPECIFIC_GROUP, deilimitedCardSpecificDetails);
        		}        		
        	}
        	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
        }
        inputParams.put("IN_PAYMENT_EXT_INFO",delimitedPaymentExtFields);
        inputParams.put("IN_WALLET_INDICATOR",ccVO.getWalletIndicator());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap persistOrderDetails(Connection conn, OrderDetailVO odVO, String masterOrderNumber) throws Exception {
        logger.debug("persistOrderDetails()");

        java.sql.Date deliveryDate = null;
        java.sql.Date deliveryDateRangeEnd = null;
        if (odVO.getDeliveryDate() != null) deliveryDate = new java.sql.Date(odVO.getDeliveryDate().getTime());
        if (odVO.getDeliveryDateRangeEnd() != null) deliveryDateRangeEnd = new java.sql.Date(odVO.getDeliveryDateRangeEnd().getTime());
        String personalGreetingId = null;
        PersonalGreetingVO pgVO = odVO.getPersonalGreetingVO();
        if (pgVO != null) personalGreetingId = pgVO.getPersonalGreetingId();

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_ORDER_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", odVO.getExternalOrderNumber());
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        inputParams.put("IN_SOURCE_CODE", odVO.getItemSourceCode());
        inputParams.put("IN_DELIVERY_DATE", deliveryDate);
        inputParams.put("IN_RECIPIENT_ID", odVO.getRecipientId());
        inputParams.put("IN_PRODUCT_ID", odVO.getProductId());
        inputParams.put("IN_PRODUCT_SUBCODE_ID", odVO.getProductSubcodeId());
        inputParams.put("IN_FIRST_COLOR_CHOICE", odVO.getFirstColorChoice());
        inputParams.put("IN_SECOND_COLOR_CHOICE", odVO.getSecondColorChoice());
        inputParams.put("IN_IOTW_FLAG", odVO.getIotwFlag());
        inputParams.put("IN_OCCASION_ID", odVO.getOccasion());
        inputParams.put("IN_CARD_MESSAGE_SIGNATURE", odVO.getCardMessage());
        inputParams.put("IN_ORDER_CMNTS", odVO.getOrderComments());
        inputParams.put("IN_FLORIST_CMNTS", odVO.getSpecialInstructions());
        inputParams.put("IN_FLORIST_ID", odVO.getFloristId());
        inputParams.put("IN_SHIP_METHOD_ID", odVO.getShipMethod());
        inputParams.put("IN_DELIVERY_DATE_RANGE_END", deliveryDateRangeEnd);
        inputParams.put("IN_SIZE_IND", odVO.getSizeIndicator());
        inputParams.put("IN_PRODUCT_AMT", odVO.getRetailVariablePrice());
        inputParams.put("IN_ADD_ON_AMT", odVO.getAddonAmount());
        inputParams.put("IN_SHIPPING_FEE_AMT", odVO.getShippingFee());
        inputParams.put("IN_SERVICE_FEE_AMT", odVO.getServiceFee());
        inputParams.put("IN_DISCOUNT_AMT", odVO.getDiscountAmount());
        inputParams.put("IN_TAX_AMT", odVO.getTaxAmount().toString());        
        inputParams.put("IN_ITEM_TOTAL_AMT", odVO.getLineItemTotal());
        inputParams.put("IN_MILES_POINTS_QTY", odVO.getMilesPoints());
        inputParams.put("IN_PERSONAL_GREETING_ID", personalGreetingId);
        inputParams.put("IN_BIN_SOURCE_CHANGED_FLAG", odVO.getBinSourceChangedFlag());
        inputParams.put("IN_ORIGINAL_ORDER_HAS_SDU", odVO.getOriginalHasSDU());
        
        /*Persisting Taxes*/
        
        	List<TaxVO> itemTaxList  =  null;
        	
        	if(odVO.getItemTaxVO() != null && odVO.getItemTaxVO().getTaxSplit() != null && odVO.getItemTaxVO().getTaxSplit().size() > 0 ) {
        		itemTaxList = odVO.getItemTaxVO().getTaxSplit();
        		for (TaxVO taxVO : itemTaxList) {
                	int splitOrder = TaxSplitOrder.fromValue(taxVO.getDescription()).value();
                	inputParams.put("IN_TAX"+ splitOrder +"_NAME", taxVO.getName());
                	inputParams.put("IN_TAX"+ splitOrder +"_DESCRIPTION", taxVO.getDescription());
                	inputParams.put("IN_TAX"+ splitOrder +"_RATE", taxVO.getRate());
                	inputParams.put("IN_TAX"+ splitOrder +"_AMOUNT", taxVO.getAmount());
            	}
        	} 
    		
        	if(odVO.getItemTaxVO()!=null) {
        		inputParams.put("IN_TOTAL_TAX_DESCRIPTION", odVO.getItemTaxVO().getTotalTaxDescription());
        	} else {
        		//Defaulting it to Taxes
        		inputParams.put("IN_TOTAL_TAX_DESCRIPTION", "Taxes");
        	}
        	inputParams.put("IN_TOTAL_TAX_RATE", (odVO.getItemTaxVO()!=null) ? odVO.getItemTaxVO().getTotalTaxrate() : BigDecimal.ZERO);
        	String taxServicePerf = (odVO.isTaxServicePerformed() ? "Y" : "N");
        	inputParams.put("IN_TAX_SERVICE_PERFORMED_FLAG", taxServicePerf);
         /*!Persisting Taxes*/
        	inputParams.put("IN_TIME_OF_SERVICE", (!StringUtils.isEmpty(odVO.getTimeOfService()) ? String.valueOf(Integer.valueOf(odVO.getTimeOfService())): null));
        	
        	// Setting cutoff and upcharge fee
        	inputParams.put("IN_LATE_CUTOFF_FEE", odVO.getLateCutOffUpcharge());
            inputParams.put("IN_VENDOR_MON_UPCHARGE", odVO.getMondayUpcharge());
            inputParams.put("IN_VENDOR_SUN_UPCHARGE", odVO.getSundayUpcharge());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap persistOrders(Connection conn, OrderVO orderVO, String masterOrderNumber) throws Exception {
        logger.debug("persistOrders()");

        if (orderVO.getOrderDate() == null) orderVO.setOrderDate(new Date());
        java.sql.Timestamp thisDate = new java.sql.Timestamp(orderVO.getOrderDate().getTime());

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_ORDERS");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        inputParams.put("IN_BUYER_ID", orderVO.getBuyerId());
        inputParams.put("IN_DNIS_ID", orderVO.getDnisId());
        inputParams.put("IN_SOURCE_CODE", orderVO.getSourceCode());
        inputParams.put("IN_ORIGIN_ID", orderVO.getOrigin());
        inputParams.put("IN_ORDER_DATE", thisDate);
        inputParams.put("IN_ORDER_TAKEN_BY_IDENTITY_ID", orderVO.getOrderCreatedBy());
        inputParams.put("IN_ITEM_CNT", orderVO.getOrderCount());
        inputParams.put("IN_ORDER_AMT", orderVO.getOrderAmount());
        inputParams.put("IN_FRAUD_ID", orderVO.getFraudId());
        inputParams.put("IN_FRAUD_CMNT", orderVO.getFraudComments());
		inputParams.put("IN_LANGUAGE_ID", orderVO.getLanguageId());
		inputParams.put("IN_BUYER_HAS_FREE_SHIPPING", orderVO.getBuyerHasFreeShippingFlag());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap persistBillingInfo(Connection conn, CoBrandVO coBrandVO, String masterOrderNumber) throws Exception {
        logger.debug("persistBillingInfo()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_BILLING_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        inputParams.put("IN_BILLING_INFO_NAME", coBrandVO.getCoBrandName());
        inputParams.put("IN_BILLING_INFO_DATA", coBrandVO.getCoBrandData());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap persistAddons(Connection conn, AddonVO addonVO, String externalOrderNumber) throws Exception {
        logger.debug("persistAddons(" + addonVO.getAddonId() + ", " + addonVO.getAddonQuantity() + 
            ", " + addonVO.getFuneralBannerText() + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_ADD_ONS");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        inputParams.put("IN_ADD_ON_CODE", addonVO.getAddonId());
        inputParams.put("IN_ADD_ON_QTY", addonVO.getAddonQuantity());
        inputParams.put("IN_FUNERAL_BANNER_TXT", addonVO.getFuneralBannerText());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public String getMasterSequence(Connection conn) throws Exception {
        logger.debug("getMasterSequence()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_MASTER_SEQUENCE");

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        String cr = (String) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public String getOrderDetailSequence(Connection conn) throws Exception {
        logger.debug("getOrderDetailSequence()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_ORDER_DETAIL_SEQUENCE");

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        String cr = (String) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getJoeOrders(Connection conn, String masterOrderNumber) throws Exception {
        logger.debug("getJoeOrders(" + masterOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDERS");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public HashMap getJoeOrderPayments(Connection conn, String masterOrderNumber) throws Exception {
        logger.debug("getJoeOrderPayments(" + masterOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDER_PAYMENTS");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public CachedResultSet getJoeOrderBillingInfo(Connection conn, String masterOrderNumber) throws Exception {
        logger.debug("getJoeOrderBillingInfo(" + masterOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDER_BILLING_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getJoeOrderDetails(Connection conn, String masterOrderNumber) throws Exception {
        logger.debug("getJoeOrderDetails(" + masterOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDER_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getJoeOrderAddons(Connection conn, String externalOrderNumber) throws Exception {
        logger.debug("getJoeOrderAddons(" + externalOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDER_ADDONS");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getJoeOrderCustomer(Connection conn, String customerId) throws Exception {
        logger.debug("getJoeOrderCustomer(" + customerId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_ORDER_CUSTOMER");
        Map inputParams = new HashMap();
        inputParams.put("IN_CUSTOMER_ID", customerId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public void postAMessage(Connection conn, String queueName, String correlationId,
        String payload, Long delaySeconds) throws Exception {
        
        logger.debug("postAMessage(" + queueName + ", " + payload + ")");

        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_POST_A_MESSAGE");
        inputParams.put("IN_QUEUE_NAME", queueName);
        inputParams.put("IN_CORRELATION_ID", correlationId);
        inputParams.put("IN_PAYLOAD", payload);
        inputParams.put("IN_DELAY_SECONDS", delaySeconds);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
        
    }

    public CachedResultSet getJoeIOTW(Connection conn, String sourceCode, String productId) throws Exception {
        logger.debug("getJoeIOTW(" + sourceCode + ", " + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_IOTW");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getIOTWDeliveryDiscountDates(Connection conn, String iotwId) throws Exception {
        logger.debug("getIOTWDeliveryDiscountDates(" + iotwId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_IOTW_DELIVERY_DISCOUNT_DATES");
        Map inputParams = new HashMap();
        inputParams.put("IN_IOTW_ID", iotwId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getIOTWProducts(Connection conn, String sourceCode) throws Exception {
        logger.debug("getIOTWProducts(" + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_IOTW_PRODUCTS");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public HashMap updateSentToApolloDate(Connection qConn, String masterOrderNumber) throws Exception {
        logger.debug("updateSentToApolloDate(" + masterOrderNumber + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(qConn);
        dataRequest.setStatementID("UPDATE_SENT_TO_APOLLO_DATE");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;

    }

    public HashMap updateSentToApolloRetryCount(Connection qConn, String masterOrderNumber) throws Exception {
        logger.debug("updateSentToApolloRetryCount(" + masterOrderNumber + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(qConn);
        dataRequest.setStatementID("UPDATE_SENT_TO_APOLLO_RETRY_CNT");
        Map inputParams = new HashMap();
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;

    }
    
    public HashMap persistCallTimeHistory(Connection conn, String dnisId, String csrIdentityId,
        String masterOrderNumber, Date startDate, Date endDate) throws Exception {
        logger.debug("persistCallTimeHistory()");

        java.sql.Timestamp thisStartDate = new java.sql.Timestamp(startDate.getTime());
        java.sql.Timestamp thisEndDate = new java.sql.Timestamp(endDate.getTime());

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_PERSIST_CALL_TIME_HISTORY");
        Map inputParams = new HashMap();
        inputParams.put("IN_DNIS_ID", dnisId);
        inputParams.put("IN_CSR_IDENTITY_ID", csrIdentityId);
        inputParams.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
        inputParams.put("IN_START_DATETIME", thisStartDate);
        inputParams.put("IN_END_DATETIME", thisEndDate);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap validateExternalOrderNumber(Connection qConn, String externalOrderNumber) throws Exception {
        logger.debug("validateExternalOrderNumber(" + externalOrderNumber + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(qConn);
        dataRequest.setStatementID("VALIDATE_EXTERNAL_ORDER_NUMBER");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;

    }

    public CachedResultSet getAddressTypeByCode(Connection conn, String addressTypeCode) throws Exception {
        logger.debug("getAddressTypeByCode(" + addressTypeCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ADDRESS_TYPE_BY_CODE");
        Map inputParams = new HashMap();
        inputParams.put("IN_ADDRESS_TYPE_CODE", addressTypeCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getAAAMemberById(Connection conn, String aaaMemberId) throws Exception {
        logger.debug("getAAAMemberById(" + aaaMemberId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_AAA_MEMBERS");
        Map inputParams = new HashMap();
        inputParams.put("IN_AAA_MEMBER_ID", aaaMemberId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getCostCenterById(Connection conn, String costCenterId, String partnerId, String sourceCode) throws Exception {
        logger.debug("getCostCenterById(" + costCenterId + ", " + partnerId + ", " + sourceCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_COST_CENTER");
        Map inputParams = new HashMap();
        inputParams.put("IN_COST_CENTER_ID", costCenterId);
        inputParams.put("IN_PARTNER_ID", partnerId);
        inputParams.put("IN_SOURCE_CODE_ID", sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public CachedResultSet getFraudOrderDisposition(Connection conn, String fraudId) throws Exception {
        logger.debug("getFraudOrderDisposition(" + fraudId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_FRAUD_ORDER_DISPOSITION");
        Map inputParams = new HashMap();
        inputParams.put("IN_FRAUD_ID", fraudId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public HashMap insertOrderExceptions(Connection qConn, String masterOrderNumber, String context,
        String errorMessage) throws Exception {

        logger.debug("insertOrderExceptions(" + masterOrderNumber + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(qConn);
        dataRequest.setStatementID("INSERT_ORDER_EXCEPTIONS");
        Map inputParams = new HashMap();
        inputParams.put("IN_CONTEXT", context);
        inputParams.put("IN_MESSAGE", masterOrderNumber);
        inputParams.put("IN_ERROR", errorMessage);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;

    }

    /**
     * Returns configurable content given filter values
     * 
     * @param conn Database connection
     * @param context Content context
     * @param name Content name
     * @return Content value as a String
     * @throws Exception
     */
    public String getContentWithFilter(Connection conn, String context, String name, String filter1Value, String filter2Value) 
               throws Exception {

        String value = null;

        logger.debug("IN_CONTENT_CONTEXT:" + context);
        logger.debug("IN_CONTENT_NAME:" + name);
        logger.debug("IN_CONTENT_FILTER_1_VALUE:" + filter1Value);
        logger.debug("IN_CONTENT_FILTER_2_VALUE:" + filter2Value);

        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.addInputParam("IN_CONTENT_CONTEXT", context);
        dataRequest.addInputParam("IN_CONTENT_NAME", name);
        dataRequest.addInputParam("IN_CONTENT_FILTER_1_VALUE", filter1Value);
        dataRequest.addInputParam("IN_CONTENT_FILTER_2_VALUE", filter2Value);
        dataRequest.setStatementID("GET_CONTENT_TEXT_WITH_FILTER");
        value = (String)dau.execute(dataRequest); 
          
        return value;
    }

    /**
     * Retrieves the display name from the ACCOUNT.PROGRAM_MASTER table
     * 
     * @param conn Database connection
     * @param programName Program name
     * @return Content value as a String
     * @throws Exception
     */
    public String getProgramDisplayName(Connection conn, String programName) 
               throws Exception {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PROGRAM_MASTER_BY_NAME");
        
        dataRequest.addInputParam(IN_PROGRAM_NAME, programName);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        String status = (String) outputs.get("OUT_STATUS");
        if (status == null || !status.equals("Y")) {
            String errorMessage = (String) outputs.get("OUT_MESSAGE");
            throw new Exception("Error in GET_PROGRAM_MASTER_BY_NAME: " + errorMessage);
        }
        
        CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
        String programDisplayName = "";
        if (rs.next()) {
            programDisplayName = rs.getString("display_name");
        }
         
        logger.info("ProgramDisplayName: " + programName + "=" + programDisplayName);
        
        return programDisplayName;
    }
    
    
    public void persistAVSAddress(Connection conn, AVSAddressVO avsAddress, long recipId) throws Exception
    {
    	HashMap orderAVSAddress = null;
    	DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE.INSERT_AVS_ADDRESS");
        //check to see if there are any qms addresses
        if(avsAddress != null)
        {                    
              orderAVSAddress = new HashMap();

              orderAVSAddress.put("IN_RECIPIENT_ID",recipId);
              orderAVSAddress.put("IN_ADDRESS",avsAddress.getAddress());
              orderAVSAddress.put("IN_CITY",avsAddress.getCity());
              orderAVSAddress.put("IN_STATE",avsAddress.getStateProvince());
              orderAVSAddress.put("IN_ZIP",avsAddress.getPostalCode());
              orderAVSAddress.put("IN_COUNTRY",avsAddress.getCountry());
              orderAVSAddress.put("IN_LATITUDE",avsAddress.getLatitude());
              orderAVSAddress.put("IN_LONGITUDE",avsAddress.getLongitude());
              orderAVSAddress.put("IN_ENTITY_TYPE",avsAddress.getEntityType());
              orderAVSAddress.put("IN_OVERRIDE_FLAG",avsAddress.getOverrideflag());
              orderAVSAddress.put("IN_RESULT",avsAddress.getResult());
              orderAVSAddress.put("IN_AVS_PERFORMED",avsAddress.getAvsPerformed());
              
              dataRequest.setConnection(conn);
              dataRequest.setInputParams(orderAVSAddress);
              DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

              Map outputs = (Map) dataAccessUtil.execute(dataRequest);

              String status = (String) outputs.get("OUT_STATUS");
              if(status.equals("N"))
              {
                  String message = (String) outputs.get("OUT_MESSAGE");
                  throw new Exception(message);
              }
              else
              {
                  Long addressId = Long.parseLong(outputs.get("OUT_AVS_ADDRESS_ID").toString());
                  avsAddress.setAvsAddressId(addressId);
                                      
              }
          }//end if
    	
    }
    
    public void persistAvsAddressScores(Connection conn, AVSAddressVO avsAddress) throws Exception
    {
    	DataRequest dataRequest = new DataRequest();

        HashMap avsAddressScore = null;
        Collection recipients = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        Iterator itemIterator = null;
        Iterator recipientIterator = null;
        
    	if(avsAddress != null && avsAddress.getScores() != null)
        {                    
	        for (AVSAddressScoreVO score : avsAddress.getScores()) 
	        {
	            avsAddressScore = new HashMap();
	
	            avsAddressScore.put("IN_ADDRESS_ID",new Long(avsAddress.getAvsAddressId()));
	            avsAddressScore.put("IN_REASON",score.getReason());
	            avsAddressScore.put("IN_SCORE",score.getScore());
	
	            dataRequest.setConnection(conn);
	            dataRequest.setStatementID("JOE.INSERT_AVS_ADDRESS_SCORE");
	            dataRequest.setInputParams(avsAddressScore);
	            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
	
	            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	
	            String status = (String) outputs.get("OUT_STATUS");
	            if(status.equals("N"))
	            {
	                String message = (String) outputs.get("OUT_MESSAGE");
	                throw new Exception(message);
	            }
	        } // end of score loop
        }
    	
    }
    public CachedResultSet getAvsAddress(Connection conn, String externalOrderNumber) throws Exception{
    
    	 logger.debug("getJoeAvsAddress(" + externalOrderNumber + ")");

         /* build DataRequest object */
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
         dataRequest.setStatementID("JOE_GET_AVS_ADDRESS");
         Map inputParams = new HashMap();
         inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
         dataRequest.setInputParams(inputParams);

         /* execute the stored procedure */
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
         CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

         return cr;
    }
    
    public CachedResultSet getAvsAddressScores(Connection conn, String externalOrderNumber) throws Exception
    {
    	logger.debug("getAvsAddressScores(" + externalOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_AVS_ADDRESS_SCORES");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }
   
    public Document getUpdateOrderInfo(Connection conn, String externalOrderNumber) throws Exception {
        
        logger.debug("getUpdateOrderInfo(" + externalOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_UPDATE_ORDER_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
   // #695
 // Added the new method getMembershipIdInfo
    public Document getMembershipIdInfo(Connection conn, String zipCode) throws Exception {
        
        logger.debug("getMembershipIdInfo(" + zipCode + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_MEMBERSHIP_ID");
        Map inputParams = new HashMap();
        inputParams.put(IN_ZIP_CODE, zipCode);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public CachedResultSet getOrderFloristUsed(Connection conn, String orderDetailId) throws Exception {
        logger.debug("getOrderFloristUsed(" + orderDetailId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_FLORIST_USED");
        Map inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }

    public HashMap updateOrderDetails(Connection qConn, OrderDetailVO odVO, String csrId) throws Exception {
        logger.debug("updateOrderDetails()");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(qConn);
        dataRequest.setStatementID("UPDATE_ORDER_DETAILS");
        Map inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", odVO.getOriginalOrderDetailId());
        inputParams.put("IN_PRODUCT_ID", odVO.getProductId());
        inputParams.put("IN_CSR_ID", csrId);
        java.sql.Date thisDate = new java.sql.Date(odVO.getDeliveryDate().getTime());
        inputParams.put("IN_DELIVERY_DATE", thisDate);
        inputParams.put("IN_FLORIST_ID", odVO.getFloristId());
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;

    }

    public HashMap getOriginalOrderInfo(Connection conn, String externalOrderNumber) throws Exception {
        
        logger.debug("getUpdateOrderInfo(" + externalOrderNumber + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_UPDATE_ORDER_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
        dataRequest.setInputParams(inputParams);
    
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    }

    public HashMap insertPartnerProductUpdate(Connection conn, String orderDetailId, Date deliveryDate,
    		String origProductId, String newProductId, BigDecimal origMerchAmount, BigDecimal newMerchAmount,
    		String csrId) throws Exception {
        logger.debug("insertPartnerProductUpdate()");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_PARTNER_PRODUCT_UPDATE");
        Map inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
        java.sql.Date thisDate = new java.sql.Date(deliveryDate.getTime());
        inputParams.put("IN_DELIVERY_DATE", thisDate);
        dataRequest.setInputParams(inputParams);
        inputParams.put("IN_ORIGINAL_PRODUCT_ID", origProductId);
        inputParams.put("IN_NEW_PRODUCT_ID", newProductId);
        inputParams.put("IN_ORIGINAL_MERCH_AMOUNT", origMerchAmount);
        inputParams.put("IN_NEW_MERCH_AMOUNT", newMerchAmount);
        inputParams.put("IN_CSR_ID", csrId);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

        return outputMap;
    	
    }

    public void insertOrderFloristUsed(Connection conn, String orderDetailId, String floristId,
    		String selectionData) throws Exception {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;

      try {
        logger.info("insertOrderFloristUsed(" + orderDetailId + ", " + floristId + ", " +
                selectionData + ")");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("ORDER_DETAIL_ID", new Long(orderDetailId));
        inputParams.put("FLORIST_ID", floristId);
        inputParams.put("SELECTION_DATA", selectionData);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_INSERT_ORDER_FLORIST_USED");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        /* read store prodcedure output parameters to determine
         * if the procedure executed successfully */
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N")) {
          String message = (String) outputs.get("OUT_MESSAGE");
          throw new Exception(message);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      }
    }

    
    public CachedResultSet getGrouponSourcecodesAjax(Connection conn, String productId) throws Exception 
    {
        logger.debug("getGrouponSourcecodesAjax(" + productId + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GROUPON_SOURCE_CODES");
        Map inputParams = new HashMap();
        inputParams.put("IN_PROD_ID", productId);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return cr;
    }

    public CachedResultSet getPromotionsInfoAjax(Connection conn, String sourcecode) throws Exception 
    {
        logger.debug("getPromotionsInfoAjax(" + sourcecode + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_PROMOTIONS_INFO");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourcecode);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return cr;
    }

    public CachedResultSet getPromotionsIotwAjax(Connection conn, String sourcecode) throws Exception 
    {
        logger.debug("getPromotionsIotwAjax(" + sourcecode + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_PROMOTIONS_IOTW");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourcecode);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return cr;
    }

    public CachedResultSet getProductCategoriesFresh(Connection conn, String product, String company) throws Exception 
    {
        logger.debug("getProductCategoriesFresh(" + product + ", " + company + ")");
        
        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FRESH_GET_PRODUCT_CATEGORIES");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", product);
        inputParams.put("IN_COMPANY_ID", company);
        dataRequest.setInputParams(inputParams);
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        return cr;
    }
    
}
