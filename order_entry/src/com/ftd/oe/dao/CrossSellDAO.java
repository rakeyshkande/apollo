package com.ftd.oe.dao;

import com.ftd.oe.vo.CrossSellDetailVO;
import com.ftd.oe.vo.CrossSellVO;
import com.ftd.oe.vo.ProductCheckVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;

public class CrossSellDAO
{
    private static Logger logger = new Logger("com.ftd.oe.dao.CrossSellDAO");

    public CrossSellDAO()
    {
    }

    /**
     * Get the list of companies associated with products.
     * @param conn
     * @return document with the results
     */
    public Document getCompaniesAjax(Connection conn) throws Exception
    {
        logger.debug("getCompaniesAjax()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_COMPANIES");
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    /**
     * Get the cross sell products by company.
     * @param conn
     * @param companyId company to filter by
     * @return document with the results
     */
    public List<CrossSellVO> getCrossSellByCompany(Connection conn, String companyId) throws Exception
    {
        logger.debug("getCrossSellByCompany()");
        List<CrossSellVO> vos = new ArrayList<CrossSellVO>();

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CROSS_SELL_BY_COMPANY");
        Map inputParams = new HashMap();
        inputParams.put("IN_COMPANY_ID", companyId);
        dataRequest.setInputParams(inputParams);
                
        Map resultMap = (Map)DataAccessUtil.getInstance().execute(dataRequest);
        CachedResultSet results = (CachedResultSet) resultMap.get("OUT_CUR");
        int maxItems = 3;

        while ( results != null && results.next() ) 
        {
            CrossSellVO vo  = new CrossSellVO();
            
            vo.setCrossSellMasterId(results.getInt("CROSS_SELL_MASTER_ID"));
            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setUpsellMasterId(results.getString("UPSELL_MASTER_ID"));
            if (vo.getProductId() == null) {
                vo.setProductId(vo.getUpsellMasterId());
            }
            vo.setNovatorId(results.getString("NOVATOR_ID"));
            vo.setNovatorName(results.getString("NOVATOR_NAME"));
            vo.setOver21Flag(results.getString("OVER_21"));
            vo.setStatus(results.getString("STATUS"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            maxItems = results.getInt("max_items");
            
            dataRequest.setStatementID("GET_CROSS_SELL_DETAIL");
            inputParams = new HashMap();
            inputParams.put("IN_CROSS_SELL_MASTER_ID", vo.getCrossSellMasterId());
            dataRequest.setInputParams(inputParams);
                    
            resultMap = (Map)DataAccessUtil.getInstance().execute(dataRequest);
            CachedResultSet rs = (CachedResultSet) resultMap.get("CROSS_SELL_CUR");
            int detailItems = 0;

            while ( rs != null && rs.next() ) {

                CrossSellDetailVO csdVO = new CrossSellDetailVO();
                csdVO.setDetailProductId(rs.getString("CROSS_SELL_PRODUCT_ID"));
                csdVO.setDetailUpsellId(rs.getString("CROSS_SELL_UPSELL_ID"));
                csdVO.setDisplayId(rs.getString("CROSS_SELL_DISPLAY_ID"));
                csdVO.setStatus(rs.getString("STATUS"));
                csdVO.setProductType(rs.getString("PRODUCT_TYPE"));
                csdVO.setDisplayOrderSeq(rs.getInt("DISPLAY_ORDER_SEQ_NUM"));
                csdVO.setOver21Flag(rs.getString("OVER_21"));
                vo.setDetailVO(csdVO);
                detailItems = detailItems + 1;

            }
            
            // the front-end needs all CrossSellVO records to have the same amount of detail records
            // for the display and sorting to work properly
            for (int i=detailItems; i<maxItems; i++) {
                CrossSellDetailVO csdVO = new CrossSellDetailVO();
                csdVO.setDetailProductId("");
                csdVO.setDetailUpsellId("");
                csdVO.setDisplayId("");
                csdVO.setStatus("");
                csdVO.setProductType("");
                csdVO.setDisplayOrderSeq(i);
                csdVO.setOver21Flag("");
                vo.setDetailVO(csdVO);
            }

            vos.add(vo);

        }
        
        return vos;
    }

    /**
     * Get the cross sell products by  product.
     * @param conn
     * @param productId product to filter by
     * @return document with the results
     */
    public Document getCrossSellProductAjax(Connection conn, String productId) throws Exception
    {
        logger.debug("getCrossSellProductAjax()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_CROSS_SELL_PRODUCT");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    /**
     * Get the cross sell products by  product.
     * @param conn
     * @param productId product to filter by
     * @return VO with the results
     */
    public CrossSellVO getCrossSellProduct(Connection conn, String productId) throws Exception
    {
        logger.debug("getCrossSellProduct(" + productId + ")");
        CrossSellVO vo = null;

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CROSS_SELL_PRODUCT");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        HashMap results = (HashMap)DataAccessUtil.getInstance().execute(dataRequest);

        // There can be only one
        if ( results != null) 
        {
            // Get the product cursor
            CachedResultSet productCur = (CachedResultSet) results.get("PRODUCT_CUR");

            if ( productCur != null && productCur.next() ) 
            {
                vo = new CrossSellVO();
                vo.setProductId(productCur.getString("PRODUCT_ID"));
                vo.setUpsellMasterId(productCur.getString("UPSELL_MASTER_ID"));
                vo.setNovatorId(productCur.getString("NOVATOR_ID"));
                vo.setStatus(productCur.getString("STATUS"));
                vo.setOver21Flag(productCur.getString("OVER_21"));
                vo.setProductType(productCur.getString("PRODUCT_TYPE"));
                vo.setNovatorName(productCur.getString("NOVATOR_NAME"));
                logger.debug(vo.getCrossSellMasterId() + " " + vo.getProductId() + " " + vo.getUpsellMasterId());

                // Get the cross-sell detail cursor
                CachedResultSet cr = (CachedResultSet) results.get("CROSS_SELL_CUR");
                while (cr.next()) {
                    CrossSellDetailVO csd = new CrossSellDetailVO();
                    csd.setDetailProductId(cr.getString("PRODUCT_ID"));
                    csd.setDetailUpsellId(cr.getString("UPSELL_ID"));
                    csd.setStatus(cr.getString("STATUS"));
                    csd.setProductType(cr.getString("PRODUCT_TYPE"));
                    csd.setNovatorName(cr.getString("NOVATOR_NAME"));
                    csd.setPrice(cr.getString("STANDARD_PRICE"));
                    csd.setOver21Flag(cr.getString("OVER_21"));
                    csd.setExceptionStartDate(cr.getString("EXCEPTION_START_DATE"));
                    csd.setExceptionEndDate(cr.getString("EXCEPTION_END_DATE"));
                    csd.setSmallImage(cr.getString("SMALL_IMAGE"));
                    csd.setLargeImage(cr.getString("LARGE_IMAGE"));
                    logger.debug("detail: " + csd.getDetailProductId() + " " + csd.getDetailUpsellId());
                    
                    vo.setDetailVO(csd);
                    vo.setCrossSellMasterId(cr.getInt("CROSS_SELL_MASTER_ID"));
                }

            }
            
        }
        
        return vo;
    }

    /**
     * Get the cross sell products that contain a product.
     * @param conn
     * @param productId product to filter by
     * @return document with the results
     */
    public Document getMasterRecordsAjax(Connection conn, String productId) throws Exception
    {
        logger.debug("getMasterRecordsAjax()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_CROSS_SELL_MASTER");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public ProductCheckVO getProductId(Connection conn, String productId) throws Exception 
    {
        logger.debug("getProductid(" + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_ALLOW_UPSELLS", "Y");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);

        ProductCheckVO vo = new ProductCheckVO();
        vo.setProductId((String) results.get("OUT_PRODUCT_ID"));
        vo.setUpsellMasterId((String) results.get("OUT_UPSELL_MASTER_ID"));
                
        return vo;        
    }

    
    public String getNovatorId(Connection conn, String productId) throws Exception 
    {
        logger.debug("getNovatorId(" + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);

        String novatorId = (String) results.get("OUT_NOVATOR_ID");
                
        return novatorId;        
    }

    /**
     * Update the database for the cross sell products.  Insert or update is done.
     * @param conn
     * @param vo
     * @return document with the results
     */
    public Document updateCrossSell(Connection conn, CrossSellVO vo) throws Exception
    {
        logger.debug("updateCrossSell()");
        Document doc = null;

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        Map inputParams = new HashMap();

        int crossSellMasterId = vo.getCrossSellMasterId();
        logger.debug("crossSellMasterId: " + crossSellMasterId);
        if (crossSellMasterId <= 0) {
            // new Cross Sell, insert master record
            logger.debug("Inserting Cross Sell Master");
            dataRequest.setStatementID("INSERT_CROSS_SELL_MASTER");
            inputParams.put("IN_PRODUCT_ID", vo.getProductId());
            inputParams.put("IN_UPSELL_MASTER_ID", vo.getUpsellMasterId());
            inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());
        } else {
            // existing Cross Sell, delete detail records
            logger.debug("Removing detail records for id " + crossSellMasterId);
            dataRequest.setStatementID("DELETE_CROSS_SELL_DETAIL");
            inputParams.put("IN_CROSS_SELL_MASTER_ID", crossSellMasterId);
        }

        dataRequest.setInputParams(inputParams);
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);
        
        if (crossSellMasterId <= 0) {
            String temp = (String) results.get("OUT_CROSS_SELL_MASTER_ID");
            logger.debug("returned id: " + temp);
            crossSellMasterId = Integer.parseInt(temp);
        }
        
        // insert the detail records
        for (int i=0; i<vo.getDetailVO().size(); i++) {
            
            CrossSellDetailVO temp = (CrossSellDetailVO) vo.getDetailVO().get(i);
            String detailId = temp.getDetailProductId();
            String upsellId = temp.getDetailUpsellId();
            logger.debug("update: " + detailId + " "  + upsellId);

            dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            inputParams = new HashMap();
            dataRequest.setStatementID("INSERT_CROSS_SELL_DETAIL");
            inputParams.put("IN_CROSS_SELL_MASTER_ID", crossSellMasterId);
            inputParams.put("IN_CROSS_SELL_PRODUCT_ID", detailId);
            inputParams.put("IN_CROSS_SELL_UPSELL_ID", upsellId);
            inputParams.put("IN_DISPLAY_ORDER_SEQ", (i+1));
            inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());

            dataRequest.setInputParams(inputParams);
            /* execute the stored procedure */
            dataAccessUtil = DataAccessUtil.getInstance();
            doc = (Document) dataAccessUtil.execute(dataRequest);
        }
        
        return doc;
    }

    /**
     * Delete the cross sell products. 
     * @param conn
     * @param productId
     * @return document with the results
     */
    public Document deleteCrossSell(Connection conn, String productId) throws Exception
    {
        logger.debug("deleteCrossSell() " + productId);

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_CROSS_SELL_MASTER");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }
    
    /**
     * Delete the cross sell products. 
     * @param conn
     * @param vo
     * @return document with the results
     */
    public Document deleteCrossSell(Connection conn, CrossSellVO vo) throws Exception
    {
        String productId = vo.getProductId();
        if (StringUtils.isBlank(productId))
        {
            productId = vo.getUpsellMasterId();
        }
        return deleteCrossSell(conn,productId);
    }
}
