package com.ftd.oe.dao;

import com.ftd.oe.vo.FavoriteVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

public class FavoritesDAO 
{
    private static final Logger logger = new Logger("com.ftd.oe.bo.FavoritesDAO");
    
    public FavoritesDAO() 
    {
    }
    
    /**
     * Update the database for the favorites products.  Insert or update is done.
     * @param conn
     * @param vo
     * @return document with the results
     */
    public Document updateFavorite(Connection conn, FavoriteVO vo) throws Exception
    {
        logger.debug("updateFavorite()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_FAVORITE");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", vo.getProductId());
        inputParams.put("IN_DISPLAY_SEQUENCE", new BigDecimal(vo.getDisplaySequence()));
        inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    /**
     * Remove the favorite product from the database.
     * @param conn
     * @param displaySequence
     * @return document with the results
     */
    public Document deleteFavorite(Connection conn, String displaySequence) throws Exception
    {
        logger.debug("updateFavorite()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("DELETE_FAVORITE");
        Map inputParams = new HashMap();
        inputParams.put("IN_DISPLAY_SEQUENCE", new BigDecimal(displaySequence));
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    /**
     * Get the favorites.
     * @param conn
     * @return document with the results
     */
    public Document getFavorites(Connection conn) throws Exception
    {
        logger.debug("getFavorites()");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_FAVORITES");
        Map inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
                
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        Document doc = (Document) dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    public String getProductId(Connection conn, String productId) throws Exception 
    {
        logger.debug("getProductid(" + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_ALLOW_SUBCODES", "Y");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);

        String newProductId = (String) results.get("OUT_PRODUCT_ID");
                
        return newProductId;        
    }
    
}
