package com.ftd.oe.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;


/**
 * DAO layer for the JOE.CARD_MESSAGES table
 */
public class CardMessageDAO {
    private static Logger logger = new Logger("com.ftd.oe.dao.CardMessageDAO");
    
    public CardMessageDAO() {
    }

    /**
     * Retrieve all records from the JOE.CARD_MESSAGES table
     * @param conn database connection
     * @return XML representation of the returned results
     * @throws Exception
     */
    public Document getCardMessages(Connection conn) throws Exception {
        logger.debug("getCardMessages()");
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("AJAX_GET_ALL_CARD_MESSAGES");

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Document doc = (Document)dataAccessUtil.execute(dataRequest);
        
        return doc;
    }

    /**
     * Removes all records from the JOE.CARD_MESSAGES table
     * @param conn database connection
     * @throws Exception
     */
    public void removeAllCardMessages(Connection conn) throws Exception {
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("MAINT_DELETE_ALL_CARD_MESSAGES");
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Inserts a record into the JOE.CARD_MESSAGES table
     * @param conn database connection
     * @param userId that entered the data
     * @param message card message
     * @param displaySequence ordinal position for display purposes
     * @param activeFlag indicates if the record should be displayed
     * @throws Exception
     */
    public void insertCardMessage(Connection conn, String userId, String message, long displaySequence, String activeFlag ) throws Exception {
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("MAINT_INSERT_CARD_MESSAGE");
        inputParams.put("IN_DISPLAY_SEQUENCE", displaySequence);
        inputParams.put("IN_MESSAGE_TXT", message);
        inputParams.put("IN_ACTIVE_FLAG", activeFlag);
        inputParams.put("IN_USER_ID", userId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
}
