package com.ftd.oe.dao;

import com.ftd.oe.util.OrderUtil;
import com.ftd.oe.vo.IOTWDeliveryDateVO;
import com.ftd.oe.vo.IOTWVO;
import com.ftd.oe.vo.PriceHeaderVO;
import com.ftd.oe.vo.ProductCheckVO;
import com.ftd.oe.vo.ProgramRewardVO;
import com.ftd.oe.vo.SourceCodeVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;


/*
 * Data access methods used by the Item Of The Week Maintenance screen
 */
public class IOTWDAO {

    public static final Logger logger = new Logger("com.ftd.oe.dao.IOTWDAO");

    public IOTWDAO() {
    }

    /**
     * Obtains IOTW program data for a given IOTW Id.  If no IOTW Id is supplied all
     * IOTW programs are obtained.
     * @param conn Database connection
     * @param iotwId IOTW program Id
     * @return List of IOTW programs objects
     * @throws Exception
     */
    public List<IOTWVO> getIOTW(Connection conn, String iotwId, String productId, String inSourceCode, String iotwSourceCode, boolean expiredOnly) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getIOTW(Connection conn, String iotwId)");
            logger.debug("iotwId:" + iotwId);
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_IOTW");
        dataRequest.addInputParam("IN_IOTW_ID", iotwId);
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        dataRequest.addInputParam("IN_SOURCE_CODE", inSourceCode);
        dataRequest.addInputParam("IN_IOTW_SOURCE_CODE", iotwSourceCode);
        if (expiredOnly) {
            dataRequest.addInputParam("IN_EXPIRED_ONLY", "Y");
        }

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        logger.debug("status: " + status);
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            logger.error("SQL error: " + message);
            throw new Exception(message);
        }

        //Build price header/discount information
        CachedResultSet crs = (CachedResultSet)outputs.get("PRICE_HEADER_CUR");
        HashMap sourceCodes = new HashMap();
        String sourceCode = null;
        BigDecimal discountAmt = null;
        BigDecimal zeroAmt = new BigDecimal(0);
        ArrayList discountList = new ArrayList();
        String verbiage = null;
        String discountType = null;
        while (crs.next()) {
            discountAmt = crs.getBigDecimal("discount_amt");
            
            // If the discount amount is not equal to zero create the discount verbiage
            if(discountAmt.compareTo(zeroAmt) != 0){
                sourceCode = crs.getString("iotw_source_code");
                discountType = crs.getString("discount_type");

                // P is percentage and D is dollars
                if(discountType.equalsIgnoreCase("P")){
                    verbiage = crs.getString("discount_amt") + "%";
                } else {
                    verbiage = "$" + crs.getString("discount_amt");
                }
                verbiage += " OFF " + OrderUtil.formatAmount(crs.getString("min_dollar_amt")) + "-" + OrderUtil.formatAmount(crs.getString("max_dollar_amt"));
                
                // Store the discount information for each iotw source code
                if(sourceCodes.containsKey(sourceCode)){
                    ((ArrayList)sourceCodes.get(sourceCode)).add(verbiage);
                } else{
                    discountList = new ArrayList();
                    discountList.add(verbiage);
                    sourceCodes.put(sourceCode, discountList);
                }
            }
        }

        crs = (CachedResultSet)outputs.get("IOTW_CUR");
        IOTWVO iotwVO = null;
        IOTWDeliveryDateVO iotwDeliveryDateVO = null;
        HashMap iotwMap = new HashMap();
        while (crs.next()) {
            iotwVO = new IOTWVO();
            iotwVO.setIotwId(crs.getString("iotw_id"));
            iotwVO.setSourceCode(crs.getString("source_code"));
            iotwVO.setProductId(crs.getString("product_id"));
            iotwVO.setStartDate(crs.getDate("start_date"));
            iotwVO.setEndDate(crs.getDate("end_date"));
            iotwVO.setProductPageMessage(crs.getString("product_page_messaging_txt"));
            iotwVO.setCalendarMessage(crs.getString("calendar_messaging_txt"));
            iotwVO.setIotwSourceCode(crs.getString("iotw_source_code"));
            iotwVO.setSpecialOffer(crs.getString("special_offer_flag").equalsIgnoreCase("Y") ? true : false);
            iotwVO.setWordingColor(crs.getString("wording_color_txt"));
            iotwVO.setProductAvailable(crs.getString("product_available").equalsIgnoreCase("true") ? true : false);
            iotwVO.getDiscountOrProgram().add(crs.getString("promotion_discount"));
            iotwVO.setCreatedBy(crs.getString("created_by"));
            
            // Create value to be used for front-end sorting
            String sortVal = "00000" + iotwVO.getSourceCode();
            iotwVO.setSortableSourceCode(sortVal.substring(sortVal.length()-7, sortVal.length()));
            
            // Apply IOTW Source Code price header/discounts if they exist
            if(sourceCodes.containsKey(iotwVO.getIotwSourceCode())){
                discountList = (ArrayList)sourceCodes.get(iotwVO.getIotwSourceCode());
                for(Iterator<String> it = discountList.iterator(); it.hasNext();){
                    iotwVO.getDiscountOrProgram().add(it.next());
                }
            }
            
            iotwMap.put(iotwVO.getIotwId(), iotwVO);
        }

        crs = (CachedResultSet)outputs.get("DEL_DISC_DATE_CUR");
        while (crs.next()) {
            iotwDeliveryDateVO = new IOTWDeliveryDateVO();
            iotwDeliveryDateVO.setIotwId(crs.getString("iotw_id"));
            iotwDeliveryDateVO.setDiscountDate(crs.getDate("discount_date"));
            iotwVO = (IOTWVO)iotwMap.get(iotwDeliveryDateVO.getIotwId());
            if (iotwVO != null) {
                iotwVO.setDeliveryDiscountDatesExist(true);
                iotwVO.getDeliveryDiscountDates().add(iotwDeliveryDateVO);
            }
        }

        return new ArrayList<IOTWVO>(iotwMap.values());
    }

    /**
     * Obtain a list of IOTW programs for a given source code and product id
     * @param conn Database connection
     * @param sourceCode Source code
     * @param productId Product id
     * @return List of IOTW program objects
     * @throws Exception
     */
    public List<IOTWVO> getIOTWByProductAndSourceCode(Connection conn, String sourceCode, 
                                                String productId, String upsellMasterId) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getIOTWByProductAndSourceCode(String sourceCode, String productId, Connection conn)");
            logger.debug("sourceCode:" + sourceCode);
            logger.debug("productId:" + productId);
            logger.debug("upsellMasterId:" + upsellMasterId);
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_IOTW_BY_PROD_SRC_CDE");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        dataRequest.addInputParam("IN_UPSELL_MASTER_ID", upsellMasterId);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

        CachedResultSet crs = (CachedResultSet)outputs.get("IOTW_CUR");
        IOTWVO iotwVO = null;
        HashMap iotwMap = new HashMap();
        while (crs.next()) {
            iotwVO = new IOTWVO();
            iotwVO.setIotwId(crs.getString("iotw_id"));
            iotwVO.setSourceCode(crs.getString("source_code"));
            iotwVO.setProductId(crs.getString("product_id"));
            iotwVO.setProductId(crs.getString("upsell_master_id"));
            iotwVO.setStartDate(crs.getDate("start_date"));
            iotwVO.setEndDate(crs.getDate("end_date"));
            iotwVO.setProductPageMessage(crs.getString("product_page_messaging_txt"));
            iotwVO.setCalendarMessage(crs.getString("calendar_messaging_txt"));
            iotwVO.setIotwSourceCode(crs.getString("iotw_source_code"));
            iotwVO.setSpecialOffer(crs.getString("special_offer_flag").equalsIgnoreCase("Y") ? 
                                   true : false);
            iotwVO.setWordingColor(crs.getString("wording_color_txt"));
            iotwMap.put(iotwVO.getIotwId(), iotwVO);
        }

        return new ArrayList<IOTWVO>(iotwMap.values());
    }

    /**
     * Obtains IOTW program data for a given IOTW Id returned as XML.  If
     * no IOTW Id is supplied all IOTW programs are returned.  
     * @param conn Database connection
     * @param iotwId IOTW Id
     * @return Document containing IOTW program information
     * @throws Exception
     */
    public Document getIOTWAJAX(Connection conn, 
                                String iotwId) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getIOTWAJAX(Connection conn, String iotwId)");
            logger.debug("iotwId:" + iotwId);
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_IOTW_AJAX");
        dataRequest.addInputParam("IN_IOTW_ID", iotwId);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Document doc = (Document)dataAccessUtil.execute(dataRequest);

        return doc;
    }

    /**
     * Inserts/updates an IOTW program
     * @param conn Database connection
     * @param iotwVO IOTW program
     * @return IOTW Id
     * @throws Exception
     */
    public String insert(Connection conn, IOTWVO iotwVO) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("insert(IOTWVO iotwVO, Connection conn)");
            logger.debug("iotwId:" + iotwVO.getIotwId());
            logger.debug("sourceCode:" + iotwVO.getSourceCode());
            logger.debug("productId:" + iotwVO.getProductCheckVO().getProductId());
            logger.debug("upsellMasterId:" + iotwVO.getProductCheckVO().getUpsellMasterId());
            logger.debug("startDate:" + iotwVO.getStartDate());
            logger.debug("endDate:" + iotwVO.getEndDate());
            logger.debug("productPageMessage:" + iotwVO.getProductPageMessage());
            logger.debug("calendarMessage:" + iotwVO.getCalendarMessage());
            logger.debug("iotwSourceCode:" + iotwVO.getIotwSourceCode());
            logger.debug("specialOfferFlag:" + iotwVO.isSpecialOffer());
            logger.debug("wordingColor:" + iotwVO.getWordingColor());
            logger.debug("updatedBy:" + iotwVO.getUpdatedBy());
        }

        java.sql.Date startDate = 
            new java.sql.Date(iotwVO.getStartDate().getTime());

        java.sql.Date endDate = null;
        if (iotwVO.getEndDate() != null) {
            endDate = new java.sql.Date(iotwVO.getEndDate().getTime());
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_IOTW");
        dataRequest.addInputParam("IN_IOTW_ID", iotwVO.getIotwId());
        dataRequest.addInputParam("IN_SOURCE_CODE", iotwVO.getSourceCode());
        dataRequest.addInputParam("IN_PRODUCT_ID", iotwVO.getProductCheckVO().getProductId());
        dataRequest.addInputParam("IN_UPSELL_MASTER_ID", iotwVO.getProductCheckVO().getUpsellMasterId());
        dataRequest.addInputParam("IN_START_DATE", startDate);
        dataRequest.addInputParam("IN_END_DATE", endDate);
        dataRequest.addInputParam("IN_PRODUCT_PAGE_MESSAGING_TXT", 
                                  iotwVO.getProductPageMessage());
        dataRequest.addInputParam("IN_CALENDAR_MESSAGING_TXT", 
                                  iotwVO.getCalendarMessage());
        dataRequest.addInputParam("IN_IOTW_SOURCE_CODE", 
                                  iotwVO.getIotwSourceCode());
        dataRequest.addInputParam("IN_SPECIAL_OFFER_FLAG", 
                                  iotwVO.isSpecialOffer() ? "Y" : "N");
        dataRequest.addInputParam("IN_WORDING_COLOR_TXT", 
                                  iotwVO.getWordingColor());
        dataRequest.addInputParam("IN_UPDATED_BY", 
                                  iotwVO.getUpdatedBy());

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

        return (String)outputs.get("OUT_IOTW_ID");
    }

    /**
     * Inserts an IOTW delivery discount date
     * @param conn Database connection
     * @param iotwDeliveryDateVO IOTW delivery discount date information
     * @throws Exception
     */
    public void insertDeliveryDiscountDate(Connection conn, IOTWDeliveryDateVO iotwDeliveryDateVO) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("insertDeliveryDiscountDate(IOTWDeliveryDateVO iotwDeliveryDateVO, Connection conn)");
            logger.debug("iotwId:" + iotwDeliveryDateVO.getIotwId());
            logger.debug("discountDate:" + iotwDeliveryDateVO.getDiscountDate());
            logger.debug("updatedBy:" + iotwDeliveryDateVO.getUpdatedBy());
        }

        java.sql.Date discountdate = 
            new java.sql.Date(iotwDeliveryDateVO.getDiscountDate().getTime());

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_IOTW_DEL_DISC_DATE");
        dataRequest.addInputParam("IN_IOTW_ID", 
                                  iotwDeliveryDateVO.getIotwId());
        dataRequest.addInputParam("IN_DISCOUNT_DATE", discountdate);
        dataRequest.addInputParam("IN_UPDATED_BY", 
                                  iotwDeliveryDateVO.getUpdatedBy());

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Deletes an IOTW program
     * @param conn Database connection
     * @param iotwVO IOTW program
     * @throws Exception
     */
    public void delete(Connection conn, IOTWVO iotwVO) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("remove(IOTWVO iotwVO, Connection conn)");
            logger.debug("iotwId:" + iotwVO.getIotwId());
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("REMOVE_IOTW");
        dataRequest.addInputParam("IN_IOTW_ID", iotwVO.getIotwId());

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Deletes IOTW program delivery discount dates
     * @param conn Database connection
     * @param iotwVO IOTW program
     * @throws Exception
     */
    public void deleteDeliveryDiscountDates(Connection conn, IOTWVO iotwVO) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("removeDeliveryDiscountDates(IOTWVO iotwVO, Connection conn):");
            logger.debug("iotwId:" + iotwVO.getIotwId());
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("REMOVE_IOTW_DEL_DISC_DATES");
        dataRequest.addInputParam("IN_IOTW_ID", iotwVO.getIotwId());

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
     * Obtains source code information
     * @param conn Database connection
     * @param sourceCode Source code
     * @return Source Code object
     * @throws Exception
     */
    public SourceCodeVO getSourceCode(Connection conn, 
                                          String sourceCode) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getSourceCodeRewardType(Connection conn, String sourceCode)");
            logger.debug("sourceCode:" + sourceCode);
        }

        SourceCodeVO sourceCodeVO = null;
        ProgramRewardVO programRewardVO = null;

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("JOE_GET_SOURCE_CODE_BY_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet cr = 
            (CachedResultSet)dataAccessUtil.execute(dataRequest);

        if (cr.next()) {
            sourceCodeVO = new SourceCodeVO();
            programRewardVO = new ProgramRewardVO();
            programRewardVO.setRewardType(cr.getString("REWARD_TYPE"));
            programRewardVO.setProgramName(cr.getString("PARTNER_ID"));
            sourceCodeVO.setIotwFlag(cr.getString("IOTW_FLAG").equalsIgnoreCase("Y")?true:false);
            sourceCodeVO.setPriceHeaderId(cr.getString("PRICE_HEADER_ID"));
            sourceCodeVO.setProgramReward(programRewardVO);
            List<PriceHeaderVO> phList = getPriceHeaderDetails(conn, sourceCodeVO.getPriceHeaderId());
            sourceCodeVO.setPriceHeaderList(phList);
        }

        return sourceCodeVO;
    }

    /**
     * Obtains a product id, novator id and upsell master id based on the
     * passed in potential product id.
     * @param conn Database connection
     * @param productId Product id
     * @return Product id
     * @throws Exception
     */
    public ProductCheckVO getProductId(Connection conn, 
                               String productId) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getProductId(Connection conn, String productId)");
            logger.debug("productId:" + productId);
        }

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_ALLOW_UPSELLS", "Y");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap results = (HashMap)dataAccessUtil.execute(dataRequest);

        ProductCheckVO productCheckVO = new ProductCheckVO();
        productCheckVO.setProductId((String)results.get("OUT_PRODUCT_ID"));
        productCheckVO.setNovatorId((String)results.get("OUT_NOVATOR_ID"));
        productCheckVO.setUpsellMasterId((String)results.get("OUT_UPSELL_MASTER_ID"));

        return productCheckVO;
    }

    public String getNovatorId(Connection conn, String productId) throws Exception 
    {
        logger.debug("getNovatorId(" + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_ID");
        Map inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        HashMap results = (HashMap) dataAccessUtil.execute(dataRequest);

        String novatorId = (String) results.get("OUT_NOVATOR_ID");
        if(StringUtils.isBlank(novatorId)) 
        {
            throw new Exception("A Novator Id does not exist for product id " + productId);    
        }
                
        return novatorId;        
    }

    /**
     * Obtains product discount flag
     * @param conn Database connection
     * @param productId Prouduct Id
     * @return true if the discount flag is on else false if it is not
     * @throws Exception
     */
    public boolean getProductDiscountFlag(Connection conn, 
                                          String productId) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getProductDiscountFlag(Connection conn, String productId)");
            logger.debug("productId:" + productId);
        }

        boolean discountExists = false;

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SP_GET_PRODUCT_BY_ID_2447");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        dataRequest.addInputParam("IN_SOURCE", "PDB");

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        if (crs.next()) {
            discountExists = crs.getString("discountAllowedFlag").equalsIgnoreCase("Y") ? true : false;
        }

        return discountExists;
    }
    
    /**
     * Obtains price header details for a given price header id
     * @param conn Database connection
     * @param priceHeaderId Price header id
     * @return List of Price Header objects
     * @throws Exception
     */
    public List<PriceHeaderVO> getPriceHeaderDetails(Connection conn, 
                                          String priceHeaderId) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("getPriceHeaderDetails(Connection conn, String priceHeaderId)");
            logger.debug("priceHeaderId:" + priceHeaderId);
        }

        List phList = new ArrayList();
        PriceHeaderVO phVO = null;

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRICE_HEADER_DETAILS_BY_PHID");
        dataRequest.addInputParam("IN_PRICE_HEADER_ID", priceHeaderId);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        while(crs.next()) {
            phVO = new PriceHeaderVO();
            phVO.setPriceHeaderId(crs.getString("price_header_id"));
            phVO.setMinDollarAmt(crs.getString("min_dollar_amt"));
            phVO.setMaxDollarAmt(crs.getString("max_dollar_amt"));
            phVO.setDiscountType(crs.getString("discount_type"));
            phVO.setDiscountAmt(crs.getString("discount_amt"));
            phList.add(phVO);
        }
        
        return phList;
    }
}
