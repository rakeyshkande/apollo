package com.ftd.oe.mo.vo;

import java.util.Date;

public class MercuryVO
{
  private String mercuryId;
  private String mercuryMessageNumber;
  private String mercuryOrderNumber;
  private String mercuryStatus;
  private String msgType;
  private String outboundId;
  private String sendingFlorist;
  private String fillingFlorist;
  private Date orderDate;
  private String recipient;
  private String address;
  private String cityStateZip;
  private String phoneNumber;
  private Date deliveryDate;
  private String deliveryDateText;
  private String firstChoice;
  private String secondChoice;
  private Double price;
  private String cardMessage;
  private String occasion;
  private String specialInstructions;
  private String priority;
  private String operator;
  private String comments;
  private String sakText;
  private int ctseq;
  private int crseq;
  private Date transmissionDate;
  private String referenceNumber;
  private String productId;
  private String zipCode;
  private String askAnswerCode;
  private String sortValue;
  private String retrievalFlag;
  private String combinedReportNumber;
  private String rofNumber;
  private String adjReasonCode;
  private Double overUnderCharge;
  private String fromMessageNumber;
  private String toMessageNumber;
  private Date fromMessageDate;
  private Date toMessageDate;
  private Date createdOn;
  private String viewQueue;
  private String responsibleFlorist;
  private Date lockedOn;
  private String lockedBy;
  private String compOrder;
  private String requireConfirmation;
  private String suffix;
  private String orderSequence;
  private String adminSequence;
  private Double oldPrice;
  private String messageDirection;
  private Date manualReconcileDate;

  public MercuryVO()
  {
  }

  public Object clone()
  {
    return this;
  }

  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }

  public String getMercuryId()
  {
    return mercuryId;
  }

  public void setMercuryMessageNumber(String mercuryMessageNumber)
  {
    this.mercuryMessageNumber = mercuryMessageNumber;
  }

  public String getMercuryMessageNumber()
  {
    return mercuryMessageNumber;
  }

  public void setMercuryOrderNumber(String mercuryOrderNumber)
  {
    this.mercuryOrderNumber = mercuryOrderNumber;
  }

  public String getMercuryOrderNumber()
  {
    return mercuryOrderNumber;
  }

  public void setMercuryStatus(String mercuryStatus)
  {
    this.mercuryStatus = mercuryStatus;
  }

  public String getMercuryStatus()
  {
    return mercuryStatus;
  }

  public void setMsgType(String msgType)
  {
    this.msgType = msgType;
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setOutboundId(String outboundId)
  {
    this.outboundId = outboundId;
  }

  public String getOutboundId()
  {
    return outboundId;
  }

  public void setSendingFlorist(String sendingFlorist)
  {
    this.sendingFlorist = sendingFlorist;
  }

  public String getSendingFlorist()
  {
    return sendingFlorist;
  }

  public void setFillingFlorist(String fillingFlorist)
  {
    this.fillingFlorist = fillingFlorist;
  }

  public String getFillingFlorist()
  {
    return fillingFlorist;
  }

  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }

  public Date getOrderDate()
  {
    return orderDate;
  }

  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }

  public String getRecipient()
  {
    return recipient;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getAddress()
  {
    return address;
  }

  public void setCityStateZip(String cityStateZip)
  {
    this.cityStateZip = cityStateZip;
  }

  public String getCityStateZip()
  {
    return cityStateZip;
  }

  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setDeliveryDateText(String deliveryDateText)
  {
    this.deliveryDateText = deliveryDateText;
  }

  public String getDeliveryDateText()
  {
    return deliveryDateText;
  }

  public void setFirstChoice(String firstChoice)
  {
    this.firstChoice = firstChoice;
  }

  public String getFirstChoice()
  {
    return firstChoice;
  }

  public void setSecondChoice(String secondChoice)
  {
    this.secondChoice = secondChoice;
  }

  public String getSecondChoice()
  {
    return secondChoice;
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = cardMessage;
  }

  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setOccasion(String occasion)
  {
    this.occasion = occasion;
  }

  public String getOccasion()
  {
    return occasion;
  }

  public void setSpecialInstructions(String specialInstructions)
  {
    this.specialInstructions = specialInstructions;
  }

  public String getSpecialInstructions()
  {
    return specialInstructions;
  }

  public void setPriority(String priority)
  {
    this.priority = priority;
  }

  public String getPriority()
  {
    return priority;
  }

  public void setOperator(String operator)
  {
    this.operator = operator;
  }

  public String getOperator()
  {
    return operator;
  }

  public void setComments(String comments)
  {
    this.comments = comments;
  }

  public String getComments()
  {
    return comments;
  }

  public void setSakText(String sakText)
  {
    this.sakText = sakText;
  }

  public String getSakText()
  {
    return sakText;
  }

  public void setCtseq(int ctseq)
  {
    this.ctseq = ctseq;
  }

  public int getCtseq()
  {
    return ctseq;
  }

  public void setCrseq(int crseq)
  {
    this.crseq = crseq;
  }

  public int getCrseq()
  {
    return crseq;
  }

  public void setTransmissionDate(Date transmissionDate)
  {
    this.transmissionDate = transmissionDate;
  }

  public Date getTransmissionDate()
  {
    return transmissionDate;
  }

  public void setReferenceNumber(String referenceNumber)
  {
    this.referenceNumber = referenceNumber;
  }

  public String getReferenceNumber()
  {
    return referenceNumber;
  }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public String getProductId()
  {
    return productId;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setAskAnswerCode(String askAnswerCode)
  {
    this.askAnswerCode = askAnswerCode;
  }

  public String getAskAnswerCode()
  {
    return askAnswerCode;
  }

  public void setSortValue(String sortValue)
  {
    this.sortValue = sortValue;
  }

  public String getSortValue()
  {
    return sortValue;
  }

  public void setRetrievalFlag(String retrievalFlag)
  {
    this.retrievalFlag = retrievalFlag;
  }

  public String getRetrievalFlag()
  {
    return retrievalFlag;
  }

  public void setFromMessageNumber(String fromMessageNumber)
  {
    this.fromMessageNumber = fromMessageNumber;
  }

  public String getFromMessageNumber()
  {
    return fromMessageNumber;
  }

  public void setToMessageNumber(String toMessageNumber)
  {
    this.toMessageNumber = toMessageNumber;
  }

  public String getToMessageNumber()
  {
    return toMessageNumber;
  }

  public void setFromMessageDate(Date fromMessageDate)
  {
    this.fromMessageDate = fromMessageDate;
  }

  public Date getFromMessageDate()
  {
    return fromMessageDate;
  }

  public void setToMessageDate(Date toMessageDate)
  {
    this.toMessageDate = toMessageDate;
  }

  public Date getToMessageDate()
  {
    return toMessageDate;
  }

  public void setCombinedReportNumber(String combinedReportNumber)
  {
    this.combinedReportNumber = combinedReportNumber;
  }

  public String getCombinedReportNumber()
  {
    return combinedReportNumber;
  }

  public void setRofNumber(String rofNumber)
  {
    this.rofNumber = rofNumber;
  }

  public String getRofNumber()
  {
    return rofNumber;
  }

  public void setOverUnderCharge(Double overUnderCharge)
  {
    this.overUnderCharge = overUnderCharge;
  }

  public Double getOverUnderCharge()
  {
    return overUnderCharge;
  }

  public void setAdjReasonCode(String adjReasonCode)
  {
    this.adjReasonCode = adjReasonCode;
  }

  public String getAdjReasonCode()
  {
    return adjReasonCode;
  }

  public void setViewQueue(String viewQueue)
  {
    this.viewQueue = viewQueue;
  }

  public String getViewQueue()
  {
    return viewQueue;
  }

  public void setSuffix(String suffix)
  {
    this.suffix = suffix;
  }

  public String getSuffix()
  {
    return suffix;
  }

  public void setMessageDirection(String messageDirection)
  {
    this.messageDirection = messageDirection;
  }

  public String getMessageDirection()
  {
    return messageDirection;
  }

  public void setOrderSequence(String orderSequence)
  {
    this.orderSequence = orderSequence;
  }

  public String getOrderSequence()
  {
    return orderSequence;
  }

  public void setAdminSequence(String adminSequence)
  {
    this.adminSequence = adminSequence;
  }

  public String getAdminSequence()
  {
    return adminSequence;
  }

  public void setCompOrder(String compOrder)
  {
    this.compOrder = compOrder;
  }

  public String getCompOrder()
  {
    return compOrder;
  }

  public void setRequireConfirmation(String requireConfirmation)
  {
    this.requireConfirmation = requireConfirmation;
  }

  public String getRequireConfirmation()
  {
    return requireConfirmation;
  }

  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }

  public Double getOldPrice()
  {
    return oldPrice;
  }

  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setResponsibleFlorist(String responsibleFlorist)
  {
    this.responsibleFlorist = responsibleFlorist;
  }

  public String getResponsibleFlorist()
  {
    return responsibleFlorist;
  }

  public void setLockedOn(Date lockedOn)
  {
    this.lockedOn = lockedOn;
  }

  public Date getLockedOn()
  {
    return lockedOn;
  }

  public void setLockedBy(String lockedBy)
  {
    this.lockedBy = lockedBy;
  }

  public String getLockedBy()
  {
    return lockedBy;
  }

  public void setManualReconcileDate(Date manualReconcileDate)
  {
    this.manualReconcileDate = manualReconcileDate;
  }

  public Date getManualReconcileDate()
  {
    return manualReconcileDate;
  }

  public String getQueueMessageType()
  {
    String messageType = this.getMsgType();
    String manual = this.getMercuryStatus().equalsIgnoreCase("MM")? "M": "";
    String price = this.getAskAnswerCode() != null? this.getAskAnswerCode(): "";
    String inbound = this.getMessageDirection().equalsIgnoreCase("INBOUND")? "I": "";
    String comp = this.getCompOrder() != null? this.getCompOrder(): "";

    // output messagePMCI
    return messageType + price + manual + comp + inbound;
  }

}
