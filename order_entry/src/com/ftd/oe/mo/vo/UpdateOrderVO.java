/**
 * 
 */
package com.ftd.oe.mo.vo;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * @author bsurimen
 * 
 */
public class UpdateOrderVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3892232492496313897L;

	private String orderGuid;
	private String customerId;
	private String orderDetailId;
	private String externalOrderNumber;
	private String shipMethod;
	private String csrID;
	private String origin;
	private String refundDispCode;
	private String responsibleParty;
	private Date deliveryDate;
	private String floralFlag;

	public String getOrderGuid() {
		return orderGuid;
	}

	public void setOrderGuid(String orderGuid) {
		this.orderGuid = orderGuid;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public String getExternalOrderNumber() {
		return externalOrderNumber;
	}

	public void setExternalOrderNumber(String externalOrderNumber) {
		this.externalOrderNumber = externalOrderNumber;
	}

	public String getShipMethod() {
		return shipMethod;
	}

	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}

	public String getCsrID() {
		return csrID;
	}

	public void setCsrID(String csrID) {
		this.csrID = csrID;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getRefundDispCode() {
		return refundDispCode;
	}

	public void setRefundDispCode(String refundDispCode) {
		this.refundDispCode = refundDispCode;
	}

	public String getResponsibleParty() {
		return responsibleParty;
	}

	public void setResponsibleParty(String responsibleParty) {
		this.responsibleParty = responsibleParty;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getFloralFlag() {
		return floralFlag;
	}

	public void setFloralFlag(String floralFlag) {
		this.floralFlag = floralFlag;
	}

}
