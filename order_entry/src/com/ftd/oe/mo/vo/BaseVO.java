package com.ftd.oe.mo.vo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**Base VO
 * This class was written so we are only updating the tables in the DB that need it.
 * That way unnecessary updates will not be made to save time.
 * @author Ed Mueller */
public class BaseVO implements Serializable
{

    private static final String NULLDOUBLE = "0.0";
    private static final String NULLFLOAT = "0.0";
    private static final String NULLINT = "0";
    private static final String NULLLONG = "0";    

    private boolean changed = false;


  /* Returns 0.0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullDouble(String value)
  {
    return value == null ? NULLDOUBLE : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullInt(String value)
  {
    return value == null ? NULLINT : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullFloat(String value)
  {
    return value == null ? NULLFLOAT : value;
  }

  /* Returns 0 if the passed in value is null 
   * @param String value 
   * @return String */
  protected String checkNullLong(String value)
  {
    return value == null ? NULLLONG : value;
  }

    public boolean isChanged()
    {
        return changed;
    }

    protected void setChanged(boolean newChanged)
    {
        changed = newChanged;
    }

    protected boolean valueChanged(String origValue, String newValue)
    {
        boolean ret = false;
        if((newValue == null && origValue == null) || (newValue == null && origValue.equals("")) ||
           (origValue == null && newValue.equals("")))
        {
            // Do nothing
            ret = false;
        }
        else if(newValue != null && origValue != null && newValue.equals(origValue))
        {
            // Do nothing
            ret = false;
        }
        else
        {
            ret = true;
        }        

        return ret;
    }
    
    protected boolean valueChanged(Integer origValue, Integer newValue)
    {
        boolean ret = false;
        if((newValue == null && origValue == null))
        {
            // Do nothing
            ret = false;
        }
        else if(newValue != null && origValue != null && newValue.intValue() == origValue.intValue())
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     
        return ret;
    }
    
    protected boolean valueChanged(Long origValue, Long newValue)
    {
        boolean ret = false;
        if((newValue == null && origValue == null))
        {
            // Do nothing
            ret = false;
        }
        else if(newValue != null && origValue != null && newValue.longValue() == origValue.longValue())
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     
        return ret;
    }
    
    protected boolean valueChanged(Double origValue, Double newValue)
    {
        boolean ret = false;
        if((newValue == null && origValue == null))
        {
            // Do nothing
            ret = false;
        }
        else if(newValue != null && origValue != null && newValue.doubleValue() == origValue.doubleValue())
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     
        return ret;
    }

    protected boolean valueChanged(long origValue, long newValue)
    {
        boolean ret = false;
        if(newValue == origValue)
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     
        return ret;
    }


    protected boolean valueChanged(double origValue, double newValue)
    {
        boolean ret = false;
        if(newValue == origValue)
        {
            // Do nothing
            ret = false;
        }
        else
        {
            //changed
            ret = true;
        }
     
        return ret;
    }
    
    
    protected boolean valueChanged(Date origValue, Date newValue)
    {
        boolean ret = false;
        if(newValue == null && origValue == null)
        {
            // Do nothing
            ret = false;
        }
        else if((newValue == null && origValue != null) || (newValue != null && origValue == null))
        {
            // changed
            ret = true;
        }
        else if(newValue.compareTo(origValue) != 0)
        {
            ret = true;
        }        

        return ret;
    }


    protected boolean valueChanged(Calendar origValue, Calendar newValue)
    {
        boolean ret = false;
        if(newValue == null && origValue == null)
        {
            // Do nothing
            ret = false;
        }
        else if((newValue == null && origValue != null) || (newValue != null && origValue == null))
        {
            // changed
            ret = true;
        }
        else if(!newValue.equals(origValue))
        {
            ret = true;
        }        

        return ret;
    }


    protected boolean valueChanged(boolean origValue, boolean newValue)
    {
        boolean ret = false;
        if(newValue != origValue)
        {
            ret = true;
        }

        return ret;
    }



}