package com.ftd.oe.mo.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.oe.mo.constants.UpdateOrderConstants;
import com.ftd.oe.mo.vo.FTDMessageVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class UpdateOrderDAO {

	private static final Logger logger = new Logger(
			"com.ftd.oe.mo.dao.UpdateOrderDAO");
	private Connection connection = null;

	/**
	 * 
	 * @param conn
	 */
	public UpdateOrderDAO(Connection conn) {
		super();
		connection = conn;
	}

	/**
	 * This method returns the name, type and member number for the passed in
	 * vendor id.
	 * 
	 * @param String
	 *            - vendorId
	 * @return CachedResultSet
	 */
	public CachedResultSet getVendorInfo(String vendorId) throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("OP_GET_VENDOR");
		dataRequest.addInputParam("VENDOR_ID", vendorId);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		CachedResultSet rs = (CachedResultSet) dataAccessUtil
				.execute(dataRequest);

		return rs;

	}

	/**
	 * This method will check if an order has already been modified. If so, it
	 * will return a 'Y'
	 * 
	 * @throws javayeah.lang.Exception
	 * @param String
	 *            externalOrderNumber
	 */

	public String hasOrderBeenModified(String externalOrderNumber)
			throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("MO_HAS_ORDER_BEEN_MODIFIED");
		dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER",
				externalOrderNumber);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		String searchResults = (String) dataAccessUtil.execute(dataRequest);

		return searchResults;

	}

	/**
	 * @param comment
	 * @throws Exception
	 */
	public void insertComment(CommentsVO comment) throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("OP_INSERT_COMMENTS");
		dataRequest.addInputParam("IN_CUSTOMER_ID", comment.getCustomerId());
		dataRequest.addInputParam("IN_ORDER_GUID", comment.getOrderGuid());
		dataRequest.addInputParam("IN_ORDER_DETAIL_ID",
				comment.getOrderDetailId());
		dataRequest.addInputParam("IN_COMMENT_ORIGIN",
				comment.getCommentOrigin());
		dataRequest.addInputParam("IN_REASON", comment.getReason());
		dataRequest.addInputParam("IN_DNIS_ID", comment.getDnisId());
		dataRequest.addInputParam("IN_COMMENT_TEXT", comment.getComment());
		dataRequest.addInputParam("IN_COMMENT_TYPE", comment.getCommentType());
		dataRequest.addInputParam("IN_CSR_ID", comment.getUpdatedBy());

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		Map outputs = (Map) dataAccessUtil.execute(dataRequest);
		String status = (String) outputs.get(UpdateOrderConstants.STATUS_PARAM);
		if (status != null && status.equals("N")) {
			String message = (String) outputs
					.get(UpdateOrderConstants.MESSAGE_PARAM);
			throw new Exception(message);
		}

	}

	/**
	 * getOriginalDeliveryInfo()
	 * 
	 * Get original delivery info from database. This is used when comparing
	 * updated delivery info against original info.
	 * 
	 * @param a_orderDetailId
	 *            - Order detail ID
	 * @return hashmap containing cursors for order details
	 * @throws java.lang.Exception
	 */
	public CachedResultSet getOriginalDeliveryInfo(String a_orderDetailId)
			throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("GET_ORIGINAL_DELIVERY_INFO");
		dataRequest.addInputParam("IN_ORDER_DETAIL_ID", a_orderDetailId);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		CachedResultSet rs = (CachedResultSet) dataAccessUtil
				.execute(dataRequest);

		return rs;
	}

	/**
	 * Call stored procedure to return Message Value Object based on order
	 * detail id and message type.
	 * 
	 * Populates the following fields in the MessagseVO -sending florist
	 * -filling florist -message id
	 * 
	 */
	public FTDMessageVO getMessageDetailLastFTD(String orderDetailID,
			String messageType) throws Exception {
		FTDMessageVO messageVO = new FTDMessageVO();
		messageVO = getMessageDetailLastFTD(orderDetailID, messageType, "Y");

		return messageVO;

	}

	/**
	 * Overloaded method to call stored procedure to return Message Value Object
	 * based on order detail id and message type.
	 * 
	 * Populates the following fields in the MessagseVO -sending florist
	 * -filling florist -message id
	 * 
	 */
	public FTDMessageVO getMessageDetailLastFTD(String orderDetailID,
			String messageType, String compOrder) throws Exception {

		DataRequest request = new DataRequest();
		FTDMessageVO messageVO = new FTDMessageVO();
		CachedResultSet messageRS = null;

		try {

			/* setup store procedure input parameters */
			HashMap inputParams = new HashMap();
			inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
			inputParams.put("IN_MESSAGE_TYPE", messageType);
			inputParams.put("IN_COMP_ORDER", compOrder);

			/* build DataRequest object */
			request.setConnection(connection);
			request.reset();
			request.setInputParams(inputParams);
			request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");

			/* execute the store prodcedure */
			DataAccessUtil dau = DataAccessUtil.getInstance();
			messageRS = (CachedResultSet) dau.execute(request);
			SimpleDateFormat sdfOutput = new SimpleDateFormat(
					UpdateOrderConstants.MESSAGE_DATE_FORMAT);
			SimpleDateFormat sdfEndDeliveryOutput = new SimpleDateFormat(
					UpdateOrderConstants.MESSAGE_END_DELIVERY_DATE_FORMAT);

			String textDate = "";
			java.util.Date utilDate;
			if (messageRS.next()) {
				// check if florist of vendor order
				if (messageType.equals(UpdateOrderConstants.MERCURY_MESSAGE)) {
					messageVO.setFillingFloristCode(messageRS
							.getString("filling_florist"));
					messageVO.setSendingFloristCode(messageRS
							.getString("sending_florist"));
					messageVO.setCurrentPrice(getDouble(messageRS
							.getObject("price")));
					messageVO.setMessageOrderId(messageRS
							.getString("message_order_number"));
					messageVO.setDeliveryDate(messageRS
							.getDate("delivery_date"));
				} else {
					messageVO.setFillingFloristCode(messageRS
							.getString("filling_vendor"));
					messageVO.setSendingFloristCode(messageRS
							.getString("sending_vendor"));
					messageVO.setCurrentPrice(getDouble(messageRS
							.getObject("price")));
					messageVO.setMessageOrderId(messageRS
							.getString("message_order_number"));

					if (messageRS.getDate("ship_date") != null) {
						textDate = sdfOutput.format(messageRS
								.getDate("ship_date"));
						utilDate = sdfOutput.parse(textDate);
						messageVO.setShipDate(utilDate);
					}

					messageVO.setShippingSystem(messageRS
							.getString("shipping_system"));
					messageVO.setOrderDetailId(messageRS
							.getString("reference_number"));
					messageVO
							.setZoneJumpFlag(messageRS
									.getString("ZONE_JUMP_FLAG") == null ? false
									: true);
					messageVO.setZoneJumpLabelDate(getUtilDate(messageRS
							.getObject("ZONE_JUMP_LABEL_DATE")));
					messageVO.setSdsStatus(messageRS.getString("sds_status"));
					messageVO.setDeliveryDate(messageRS
							.getDate("delivery_date"));
					messageVO.setShipDate(messageRS.getDate("ship_date"));
				}

				messageVO.setMessageId(messageRS.getString("ftd_message_id"));
			}

		} finally {

			if (logger.isDebugEnabled()) {
				logger.debug("Exiting getMessageDetailNewFTD");
			}

		}

		return messageVO;

	}

	/**
	 * Retrieve the mercury status.
	 * 
	 * @param orderDetailId
	 * @param message
	 *            type
	 * @param comp
	 *            order
	 * @return CachedResultSet
	 * @throws java.lang.Exception
	 * 
	 */
	public CachedResultSet getMercuryOrderMessageStatus(String orderDetailId,
			String messageType, String compOrder) throws Exception {
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("GET_MERCURY_STATUS");
		dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
		dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
		dataRequest.addInputParam("IN_COMP_ORDER", compOrder);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		CachedResultSet searchResults = (CachedResultSet) dataAccessUtil
				.execute(dataRequest);

		return searchResults;

	}

	private double getDouble(Object obj) {
		double value = 0.00;
		if (obj != null) {
			value = Double.parseDouble(obj.toString());
		}

		return value;
	}

	/*
	 * Converts a database timestamp to a java.util.Date.
	 */
	private java.util.Date getUtilDate(Object time) {
		java.util.Date theDate = null;
		boolean test = false;
		if (time != null) {
			if (test || time instanceof Timestamp) {
				Timestamp timestamp = (Timestamp) time;
				theDate = new java.util.Date(timestamp.getTime());
			} else {
				theDate = (java.util.Date) time;
			}
		}

		return theDate;
	}

	/**
	 * This method obtains the order disposition code for an order
	 * 
	 * @param orderDetailId
	 *            - order detail id
	 * @return String - order disposition code
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SQLException
	 * @throws Exception
	 */
	public String getOrderDispCode(String orderDetailId) throws SAXException,
			ParserConfigurationException, IOException, SQLException, Exception {
		String orderDispCode = null;

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		dataRequest.setStatementID("CLEAN_GET_ORDER_DETAILS_II");
		dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		CachedResultSet rs = (CachedResultSet) dataAccessUtil
				.execute(dataRequest);

		if (rs.next()) {
			orderDispCode = rs.getString("order_disp_code");
		} else {
			throw new Exception("No order was found for order detail id:"
					+ orderDetailId);
		}

		return orderDispCode;
	}

	/**
	 * This method will record that changes that were made to order in a table.
	 * 
	 * @param origOrderDetailId
	 * @param origExternalOrderNumber
	 * @param newOrderDetailId
	 * @param origOrderDetailid
	 * @param csrId
	 * @param changesMap
	 */
	public void insertChanges(String origOrderDetailId,
			String origExternalOrderNumber, String newOrderDetailId,
			String newExternalOrderNumber, String csrId, Map changesMap)
			throws Exception {

		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(this.connection);
		dataRequest.setStatementID("INSERT_MO_XREF");
		dataRequest.addInputParam("IN_ORIG_ORDER_DETAIL_ID", origOrderDetailId);
		dataRequest.addInputParam("IN_NEW_ORDER_DETAIL_ID", newOrderDetailId);
		dataRequest.addInputParam("IN_ORIG_EXTERNAL_ORDER_NUMBER",
				origExternalOrderNumber);
		dataRequest.addInputParam("IN_NEW_EXTERNAL_ORDER_NUMBER",
				newExternalOrderNumber);
		dataRequest.addInputParam("IN_RECIP_NAME",
				getChangeValue((String) changesMap.get("recip_name")));
		dataRequest.addInputParam("IN_RECIP_BUSINESS",
				getChangeValue((String) changesMap.get("recip_business")));
		dataRequest.addInputParam("IN_RECIP_ADDRESS",
				getChangeValue((String) changesMap.get("recip_address")));
		dataRequest.addInputParam("IN_RECIP_DELIVERY_LOCATION",
				getChangeValue((String) changesMap
						.get("recip_delivery_location")));
		dataRequest.addInputParam("IN_RECIP_CITY",
				getChangeValue((String) changesMap.get("recip_city")));
		dataRequest.addInputParam("IN_RECIP_STATE",
				getChangeValue((String) changesMap.get("recip_state")));
		dataRequest.addInputParam("IN_RECIP_ZIPCODE",
				getChangeValue((String) changesMap.get("recip_zipcode")));
		dataRequest.addInputParam("IN_RECIP_COUNTRY",
				getChangeValue((String) changesMap.get("recip_country")));
		dataRequest.addInputParam("IN_RECIP_PHONE",
				getChangeValue((String) changesMap.get("recip_phone")));
		dataRequest.addInputParam("IN_DELIVERY_DATE",
				getChangeValue((String) changesMap.get("delivery_date")));
		dataRequest.addInputParam("IN_CARD_MESSAGE",
				getChangeValue((String) changesMap.get("card_message")));
		dataRequest.addInputParam("IN_SIGNATURE",
				getChangeValue((String) changesMap.get("signature")));
		dataRequest
				.addInputParam("IN_SPECIAL_INSTRUCTIONS",
						getChangeValue((String) changesMap
								.get("special_instructions")));
		dataRequest.addInputParam("IN_ALT_CONTACT_INFO",
				getChangeValue((String) changesMap.get("alt_contact_info")));
		dataRequest.addInputParam("IN_PRODUCT",
				getChangeValue((String) changesMap.get("product")));
		dataRequest.addInputParam("IN_PRODUCT_PRICE",
				getChangeValue((String) changesMap.get("product_price")));
		dataRequest.addInputParam("IN_SHIP_TYPE_CHANGED",
				getChangeValue((String) changesMap.get("ship_type_changed")));
		dataRequest.addInputParam("IN_COLOR_FIRST_CHOICE",
				getChangeValue((String) changesMap.get("color_first_choice")));
		dataRequest.addInputParam("IN_COLOR_SECOND_CHOICE",
				getChangeValue((String) changesMap.get("color_second_choice")));
		dataRequest.addInputParam("IN_SUBSTITUTION_INDICATOR",
				getChangeValue((String) changesMap
						.get("substitution_indicator")));
		dataRequest.addInputParam("IN_SHIP_METHOD",
				getChangeValue((String) changesMap.get("ship_method")));
		dataRequest.addInputParam("IN_SOURCE_CODE",
				getChangeValue((String) changesMap.get("source_code")));
		dataRequest.addInputParam("IN_MEMBERSHIP_NAME",
				getChangeValue((String) changesMap.get("membership_name")));
		dataRequest.addInputParam("IN_MEMBERSHIP_NUMBER",
				getChangeValue((String) changesMap.get("membership_number")));
		dataRequest.addInputParam("IN_ADD_ONS",
				getChangeValue((String) changesMap.get("add_ons")));
		dataRequest.addInputParam("IN_VENDOR_TO_FLORIST",
				getChangeValue((String) changesMap.get("vendor_to_florist")));
		dataRequest.addInputParam("IN_FLORIST_TO_VENDOR",
				getChangeValue((String) changesMap.get("florist_to_vendor")));
		dataRequest.addInputParam("IN_CREATED_BY",
				getChangeValue((String) changesMap.get("created_by")));
		dataRequest
				.addInputParam("IN_PERSONAL_GREETING_ID",
						getChangeValue((String) changesMap
								.get("personal_greeting_id")));

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		Map outputs = (Map) dataAccessUtil.execute(dataRequest);

		String status = (String) outputs.get(UpdateOrderConstants.STATUS_PARAM);
		if (StringUtils.equals(status, "N")) {
			String message = (String) outputs
					.get(UpdateOrderConstants.MESSAGE_PARAM);
			throw new Exception(message);
		}

	}

	private String getChangeValue(String value) {
		return value == null ? "N" : value;
	}

}
