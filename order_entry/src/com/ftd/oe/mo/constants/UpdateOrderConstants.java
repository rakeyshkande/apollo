/**
 * 
 */
package com.ftd.oe.mo.constants;

/**
 * @author bsurimen
 * 
 */
public class UpdateOrderConstants {

	// Refund Display Codes
	public static final String A40 = "A40";
	public static final String B40 = "B40";
	
	public static final String USER_REFUND_STATUS = "Unbilled";
	public static final String SDS_STATUS_PRINTED = "Printed";
	public static final String SDS_STATUS_SHIPPED = "Shipped";
	public static final String NEWLINE_CHAR_CODE = "\n";
	public static final String ORDER_COMMENT_TYPE = "Order";
	public static final String MERCURY_MESSAGE = "Mercury";
	public static final String VENUS_MESSAGE = "Venus";

	// Below two constants copied from COM in
	// "customer_order_management_config.xml" file.
	public static final String MODIFY_ORDER_CANCEL_REASON = "UPD";
	public static final String MODIFY_ORDER_ADJ_REASON = "10";

	public static final String MERCURY_ORDER_STATUS = "MO";

	public static final String MESSAGE_DATE_FORMAT = "yyyy-MM-dd";// 2004-10-11
	public static final String MESSAGE_END_DELIVERY_DATE_FORMAT = "MMM dd yyyy";

	/* Order disposition/status */
	public static String ORDER_DISP_PRINTED = "Printed";
	public static String ORDER_DISP_SHIPPED = "Shipped";

	public static final String FTD_MESSAGE = "FTD";

	/* Venus message */
	public static final String STATUS_VERIFIED = "VERIFIED";

	public static final String MERCURY_MESSAGE_MANUAL = "MANUAL";

	public static final String INBOUND_MESSAGE = "INBOUND";
	public static final String OUTBOUND_MESSAGE = "OUTBOUND";

	public static final String STATUS_PARAM = "OUT_STATUS";
	public static final String MESSAGE_PARAM = "OUT_MESSAGE";

	/* Shipping system */
	public static String SHIPPING_SYSTEM_SDS = "SDS";
   public static String SHIPPING_SYSTEM_FTD_WEST = "FTD WEST"; 
   
	/* Shipping Method */
	public static final String SD = "SD";

	public static final String UPDATE_ORDER_COMMENTS_ORIGIN = "CS Call";
	public static final String UPDATE_ORD_CMNTS_FOR_NEW_ORDER = "New order created due to updates on original order";

	public static final String CANCEL_MSG_COMMENT = "Please cancel this order.  Thank you.";
	public static final String ADJ_MSG_COMMENT = "Adjustment created.";

}
