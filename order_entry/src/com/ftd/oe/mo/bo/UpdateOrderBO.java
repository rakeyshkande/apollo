/**
 * 
 */
package com.ftd.oe.mo.bo;

import java.sql.Connection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import com.ftd.customerordermanagement.bo.RefundAPIBO;
import com.ftd.customerordermanagement.vo.AddOnVO;
import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.oe.mo.constants.UpdateOrderConstants;
import com.ftd.oe.mo.dao.UpdateOrderDAO;
import com.ftd.oe.mo.vo.MessageVO;
import com.ftd.oe.mo.vo.UpdateOrderVO;
import com.ftd.oe.util.MessagingServiceLocator;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author bsurimen
 * 
 */
public class UpdateOrderBO {

	private final static Logger logger = new Logger(
			"com.ftd.oe.mo.bo.UpdateOrderBO");

	private Connection connection;
	protected UpdateOrderDAO uoDAO = null;

	/*
	 * Constructor.
	 */
	public UpdateOrderBO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @param originalOrderVO
	 * @param updatedOrderVO
	 * @throws Exception
	 */
	public void processUpdateOrder(UpdateOrderVO originalOrderVO,
			UpdateOrderVO updatedOrderVO) throws Exception {

		String shipMethod = null;
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		String refundDisposition;
		
		if (this.uoDAO == null) {
			uoDAO = new UpdateOrderDAO(this.connection);
		}

		// check to see if this order has already been modified
		// if it hasn't process as normal
		String externalOrderNumber = originalOrderVO.getExternalOrderNumber();
		if (hasOrderBeenModified(externalOrderNumber)) {
			throw new Exception(
					"This Order Item already been updated. You cannot udpate. (External Order Number :"
							+ externalOrderNumber + ")");
		}

		//If Delivery date is not passed the current date issue a B40 refund
		Date orgDeliveryDate = originalOrderVO.getDeliveryDate();

		if (orgDeliveryDate != null && today.getTime().after(orgDeliveryDate)) {
			refundDisposition = UpdateOrderConstants.B40;
		}
		// If delivery date has not passed record an A40 refund
		else {

			refundDisposition = UpdateOrderConstants.A40;
		}
		originalOrderVO.setRefundDispCode(refundDisposition);
		logger.info("refundDisposition : " + refundDisposition);

		shipMethod = originalOrderVO.getShipMethod();
		originalOrderVO.setFloralFlag(isFloralOrder(shipMethod));
		logger.info("Orginal Order  shipMethod " + shipMethod);

		shipMethod = updatedOrderVO.getShipMethod();
		updatedOrderVO.setFloralFlag(isFloralOrder(shipMethod));
		logger.info("Updated Order shipMethod " + shipMethod);

		// Insert into clean.MO_XREF
		insertIntoMOXREF(originalOrderVO, updatedOrderVO);

		// Insert Refund here
		refundOriginalOrder(originalOrderVO);

		// Send CAN message to Florist/Vendor
		boolean canMessageSent = processCANMessage(originalOrderVO);
		logger.info("canMessageSent: " + canMessageSent);
		// Original Order Comments
		String commentsText = null;
		if(canMessageSent){
			commentsText = "CAN message issued due to Update order updates. "
				+ UpdateOrderConstants.NEWLINE_CHAR_CODE
				+ originalOrderVO.getRefundDispCode()
				+ " refund issued due to Update order updates."
				+ UpdateOrderConstants.NEWLINE_CHAR_CODE
				+ "New order "
				+ updatedOrderVO.getExternalOrderNumber()
				+ " created due to Update order updates.";
		}
		else {
			commentsText = originalOrderVO.getRefundDispCode()
					+ " refund issued due to Update order updates."
					+ UpdateOrderConstants.NEWLINE_CHAR_CODE
					+ "New order "
					+ updatedOrderVO.getExternalOrderNumber()
					+ " created due to Update order updates.";	
		}
		createComments(originalOrderVO, commentsText);

		
		// New Order Comments will be added to 
		//table : JOE.Order_details column : order_cmnt 
	
	}

	/**
	 * Insert into clean.MO_XREF to map original order and updated order
	 * 
	 * @param originalOrderVO
	 * @param updatedOrderVO
	 * @throws Exception
	 */
	private void insertIntoMOXREF(UpdateOrderVO originalOrderVO,
			UpdateOrderVO updatedOrderVO) throws Exception {
		HashMap changesMap = new HashMap();//checkForChanges(originalOrderVO, updatedOrderVO);

		uoDAO.insertChanges(originalOrderVO.getOrderDetailId(),
				originalOrderVO.getExternalOrderNumber(),
				updatedOrderVO.getOrderDetailId(),
				updatedOrderVO.getExternalOrderNumber(),
				updatedOrderVO.getCsrID(), changesMap);
	}

	

	/**
	 * Refunding the Original Order
	 * 
	 * @param originalOrderVO
	 * @throws Exception
	 */
	private void refundOriginalOrder(UpdateOrderVO originalOrderVO)
			throws Exception {

		RefundAPIBO refundApi = new RefundAPIBO();

		try {

			refundApi.postFullRemainingRefund(
					originalOrderVO.getOrderDetailId(),
					originalOrderVO.getRefundDispCode(),
					originalOrderVO.getResponsibleParty(),
					UpdateOrderConstants.USER_REFUND_STATUS,
					originalOrderVO.getCsrID(), connection);

		} catch (Exception e) {
			logger.error("postFullRemainingRefund returned with unsuccessful status: "
					+ e.getMessage());
			throw new Exception(
					"Couldn't refund the ammount for ExternalOrderNumber :"
							+ originalOrderVO.getExternalOrderNumber());
		}
	}

	/**
	 * Sending CAN message to the Mercury/Venus systems
	 * 
	 * @param originalOrderVO
	 * @throws Exception
	 */
	private boolean processCANMessage(UpdateOrderVO originalOrderVO)
			throws Exception {

		ResultTO result = null;
		String orderDetailId = originalOrderVO.getOrderDetailId();
		// check if a live mercury exists on the order
		CachedResultSet mercuryOrderMessageStatusInfo = uoDAO
				.getMercuryOrderMessageStatus(orderDetailId, null, "Y");
		String rejectSent = "N";
		String cancelSent = "N";
		String attemptedFTD = "N";
		String hasLiveFTD = "N";
		String ftdStatus = "N";
		boolean canMessageSent = false;
		if (mercuryOrderMessageStatusInfo.next()) {
			if (mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
				rejectSent = mercuryOrderMessageStatusInfo.getString(
						"reject_sent").trim();
			if (mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
				cancelSent = mercuryOrderMessageStatusInfo.getString(
						"cancel_sent").trim();
			if (mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
				attemptedFTD = mercuryOrderMessageStatusInfo.getString(
						"attempted_ftd").trim();
			if (mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
				hasLiveFTD = mercuryOrderMessageStatusInfo.getString(
						"has_live_ftd").trim();
			if (mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
				ftdStatus = mercuryOrderMessageStatusInfo.getString(
						"ftd_status").trim();
		} else {
			logger.debug("No prior message on order.  Order detail id="
					+ orderDetailId);
		}

		// only send CAN for orders that have an attempted FTD associated with
		// them
		// and if a CAN or REJ was not sent on the order
		if ((attemptedFTD != null && attemptedFTD.equalsIgnoreCase("Y"))
				&& (rejectSent == null || rejectSent.equalsIgnoreCase("N"))
				&& (cancelSent == null || cancelSent.equalsIgnoreCase("N"))
				&& ( ftdStatus != null && ( ftdStatus.equalsIgnoreCase("MC") || ftdStatus.equalsIgnoreCase("N")) )
				) {

			logger.debug("Attempted, no rejects or cancels");
			canMessageSent = true;
			// check if ADJ needs to be sent based on delivery date
			boolean adjustmentNeed = false;
			if (checkDeliveryDate(originalOrderVO
					.getDeliveryDate()) == true) {
				adjustmentNeed = true;
				logger.debug("Based on date, ADJ needed.  Note:  This status may be changed for SDS orders");
			} else {
				logger.debug("Based on date, ADJ not needed");
			}

			// check what type of order this orig one is
			if (originalOrderVO.getFloralFlag().equals("Y")) {
				// get the mercury message associated with the FTD
				MessageVO messageVO = uoDAO.getMessageDetailLastFTD(
						orderDetailId, UpdateOrderConstants.MERCURY_MESSAGE);

				// build mercury cancel TO
				CANMessageTO canTO = new CANMessageTO();
				canTO.setMercuryId(messageVO.getMessageId());
				canTO.setComments(UpdateOrderConstants.CANCEL_MSG_COMMENT);
				canTO.setOperator(originalOrderVO.getCsrID());
				canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
				canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
				canTO.setFillingFlorist(messageVO.getFillingFloristCode());
				canTO.setSendingFlorist(messageVO.getSendingFloristCode());

				if (adjustmentNeed) {

					/*ConfigurationUtil configUtil = ConfigurationUtil
							.getInstance();*/
					String adjReasonCode = UpdateOrderConstants.MODIFY_ORDER_ADJ_REASON;

					// combinded report number = month + "-1"
					int month =	originalOrderVO.getDeliveryDate().getMonth() + 1;
					String sMonth = String.valueOf(month);
					// make the month 2 digits
					if (sMonth.length() == 1) {
						sMonth = '0' + sMonth;
					}
					String combindedReportNumber = sMonth + "-1";

					ADJMessageTO adjTO = new ADJMessageTO();
					adjTO.setMercuryId(messageVO.getMessageId());
					adjTO.setComments(UpdateOrderConstants.ADJ_MSG_COMMENT);
					adjTO.setCombinedReportNumber(combindedReportNumber);
					adjTO.setOperator(originalOrderVO.getCsrID());
					adjTO.setMercuryStatus(UpdateOrderConstants.MERCURY_ORDER_STATUS);
					adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
					adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
					adjTO.setOverUnderCharge(new Double("0.00"));
					adjTO.setAdjustmentReasonCode(adjReasonCode);
					adjTO.setSendingFlorist(messageVO.getSendingFloristCode());
					adjTO.setFillingFlorist(messageVO.getFillingFloristCode());

					result = MessagingServiceLocator.getInstance()
							.getMercuryAPI().sendADJMessage(adjTO, connection);

					// check for errors
					if (!result.isSuccess()) {
						throw new Exception(
								"Could not create Mercury ADJ message (order detail id="
										+ orderDetailId + "). ERROR: "
										+ result.getErrorString());
					} else {
						logger.debug("ADJ sent - florist");
					}

				}

				// send mercury cancel
				result = MessagingServiceLocator.getInstance().getMercuryAPI()
						.sendCANMessage(canTO, connection);

				// check for errors
				if (!result.isSuccess()) {
					throw new Exception(
							"Could not create Mercury CAN message (order detail id="
									+ orderDetailId + "). ERROR: "
									+ result.getErrorString());
				}

			} else {
				// venus order

				// get the mercury message associated with the FTD
				MessageVO messageVO = uoDAO.getMessageDetailLastFTD(
						orderDetailId, UpdateOrderConstants.VENUS_MESSAGE);

				// get flag to determine if JMS should be sent out. (used while
				// testing)
				//ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
				String cancelReason = UpdateOrderConstants.MODIFY_ORDER_CANCEL_REASON;

				// build venus cancel TO
				CancelOrderTO canTO = new CancelOrderTO();
				canTO.setVenusId(messageVO.getMessageId());
				canTO.setComments(UpdateOrderConstants.CANCEL_MSG_COMMENT);

				canTO.setCsr(originalOrderVO.getCsrID());
				canTO.setCancelReasonCode(cancelReason);

				// Test to see if an adjustment is required for SDS orders.
				// MessageUtil msgUtil = new MessageUtil(connection);
				boolean SDSShippingSystem = isShippingSystemSDS(messageVO);

				// If shipping system is SDS
				if (SDSShippingSystem) {
					// If SDS CAN requires an ADJ
					if (requiresSDSAutoADJ(messageVO)) {
						adjustmentNeed = true;
						canTO.setVenusStatus(UpdateOrderConstants.STATUS_VERIFIED);

						if (logger.isDebugEnabled())
							logger.debug("Based on ship date, ADJ needed for SDS order");
					}
					/*
					 * If shipping system is SDS negate the ESCALATE logic run
					 * above if an auto ADJ is not required
					 */
					else {
						adjustmentNeed = false;
						if (logger.isDebugEnabled())
							logger.debug("Based on ship date, ADJ not needed for SDS order");
					}
				}

				// if going to do an adjustment then do not transmit the cancel
				if (adjustmentNeed) {
					canTO.setTransmitCancel(false);
				}

				// send venus cancel
				result = MessagingServiceLocator.getInstance().getVenusAPI()
						.sendCancel(canTO, connection);

				// check for errors
				if (!result.isSuccess()) {
					throw new Exception(
							"Could not create Venus CAN message (order detail id="
									+ orderDetailId + "). ERROR: "
									+ result.getErrorString());
				}

				// if adjustment needed
				if (adjustmentNeed) {
					AdjustmentMessageTO adjTO = new AdjustmentMessageTO();
					adjTO.setComments(UpdateOrderConstants.ADJ_MSG_COMMENT);
					adjTO.setOperator(originalOrderVO.getCsrID());
					adjTO.setPrice(new Double(messageVO.getCurrentPrice()));
					adjTO.setMessageType("ADJ");
					adjTO.setOrderDetailId(orderDetailId);
					adjTO.setVenusOrderNumber(messageVO.getMessageOrderId());
					adjTO.setFillingVendor(messageVO.getFillingFloristCode());
					adjTO.setOverUnderCharge(new Double("0.00"));
					adjTO.setCancelReasonCode(canTO.getCancelReasonCode());

					result = MessagingServiceLocator.getInstance()
							.getVenusAPI().sendAdjustment(adjTO, connection);

					// check for errors
					if (!result.isSuccess()) {
						throw new Exception(
								"Could not create ADJ message (order detail id="
										+ orderDetailId + "). ERROR: "
										+ result.getErrorString());
					} else {
						logger.debug("ADJ sent - vendor");
					}

				}

			}

		}
		return canMessageSent;

	}

	/**
	 * Check if the current date is greater than the last day of the delivery
	 * date month
	 * 
	 * @param deliveryDate
	 *            - Date
	 * 
	 * @return boolean - true if the current date is after the adjustment date
	 *         (calculated based off of Delivery Date)
	 * 
	 * @throws Exception
	 */
	private boolean checkDeliveryDate(Date deliveryDate) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering checkDeliveryDate");
			logger.debug("deliveryDate: " + deliveryDate);
		}

		boolean dateComparision = false;

		try {
			Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
			Date currentDate = calCurrent.getTime();

			if (currentDate.after(deliveryDate)) {
				Calendar calDelivery = Calendar.getInstance(TimeZone
						.getDefault());
				calDelivery.setTime(deliveryDate);

				int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

				Calendar calAdjustment = Calendar.getInstance(TimeZone
						.getDefault());
				calAdjustment.setTime(deliveryDate);
				calAdjustment.set(calDelivery.get(Calendar.YEAR),
						calDelivery.get(Calendar.MONTH), day);

				Date adjustmentDate = calAdjustment.getTime();

				if (currentDate.after(adjustmentDate)) {
					dateComparision = true;
				}
			}
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting checkDeliveryDate");
			}
		}

		return dateComparision;
	}

	/**
	 * Returns true if order is in printed or shipped status and is on or after
	 * the ship date but before the delivery date
	 * 
	 * @param shipDate
	 * @param deliveryDate
	 * @return boolean
	 * @throws Exception
	 */
	public boolean isOrderInTransit(Date shipDate, Date deliveryDate)
			throws Exception {
		// if ship date is null, assume it's a florist order and return false
		if (shipDate == null) {
			return false;
		}

		Date today = new Date();

		if (today.after(shipDate) && today.before(deliveryDate)) {
			return true;
		}
		return false;
	}

	/**
	 * This method is used to enter comments on the original/Updated order.
	 * 
	 * @param newExternalOrderNumber
	 * @param orderDetailId
	 * @param orderGuid
	 * @param csrId
	 * @throws java.lang.Exception
	 */
	private void createComments(UpdateOrderVO updateOrderVO, String commentsText)
			throws Exception {

		CommentsVO commentsVO = new CommentsVO();

		commentsVO.setComment(commentsText);
		commentsVO.setCommentOrigin(updateOrderVO.getOrigin());
		commentsVO.setCommentType(UpdateOrderConstants.ORDER_COMMENT_TYPE);
		commentsVO.setCreatedBy(updateOrderVO.getCsrID());
		commentsVO.setUpdatedBy(updateOrderVO.getCsrID());
		commentsVO.setOrderDetailId(updateOrderVO.getOrderDetailId());
		commentsVO.setOrderGuid(updateOrderVO.getOrderGuid());
		commentsVO.setCustomerId(updateOrderVO.getCustomerId());

		uoDAO.insertComment(commentsVO);
	}

	/**
	 * Determines if the message shipping system is SDS/FTD West
	 * 
	 * @param msgVO
	 *            - message to be evaluated
	 * @return boolean - true if shipping system is SDS or FTD West
	 * @throws Exception
	 */
	public boolean isShippingSystemSDS(MessageVO msgVO) throws Exception {
		if (msgVO == null)
			throw new Exception(
					"Unable to obtain FTD message shipping system.  Message is null.");

		if (msgVO.getShippingSystem().equalsIgnoreCase(UpdateOrderConstants.SHIPPING_SYSTEM_SDS) ||
		      msgVO.getShippingSystem().equalsIgnoreCase(UpdateOrderConstants.SHIPPING_SYSTEM_FTD_WEST))
			return true;
		else
			return false;
	}

	/**
	 * Is an auto ADJ required. This functionality only evaluates SDS and FTD West orders.
	 * The Venus functionality has been left untouched. The Venus functionality
	 * for sending auto ADJ with a CAN currently exists within RefundBO,
	 * SendMessageBO, and CreateNewOrderBO. There is a document detailing a
	 * possible refactoring of all the dropship SendCancel and SendCancel with
	 * an auto ADJ code. The document has been attached to a future enhancement
	 * defect.
	 * 
	 * Logic: If shipping system is SDS or FTD West and order status is shipped or order
	 * status is printed and ship date is on or after the current date an auto
	 * ADJ is required.
	 * 
	 * @param msgVO
	 *            - message to be evaluated
	 * @return boolean - true if an auto ADJ is required
	 * @throws Exception
	 */
	public boolean requiresSDSAutoADJ(MessageVO msgVO) throws Exception {
      if(msgVO.getShippingSystem().equalsIgnoreCase(UpdateOrderConstants.SHIPPING_SYSTEM_SDS) ||
            msgVO.getShippingSystem().equalsIgnoreCase(UpdateOrderConstants.SHIPPING_SYSTEM_FTD_WEST))
      {
			// Obtain order disposition (aka order status)
			String orderDisp = uoDAO.getOrderDispCode(msgVO.getOrderDetailId());

			/*
			 * Logic: If shipping system is SDS or FTD West and order status is shipped or
			 * order status is printed and ship date is on or after the current
			 * date an auto ADJ is required.
			 */
			if (orderDisp
					.equalsIgnoreCase(UpdateOrderConstants.ORDER_DISP_SHIPPED)
					|| (orderDisp
							.equalsIgnoreCase(UpdateOrderConstants.ORDER_DISP_PRINTED) && checkShipDate(msgVO))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the current date is greater than or equal to the ship date.
	 * 
	 * @param msgVO
	 *            - message to be evaluated
	 * @return boolean - true if current date is greater than or equal to today
	 * @throws ParseException
	 */
	private boolean checkShipDate(MessageVO msgVO) throws ParseException {

		if (logger.isDebugEnabled()) {
			logger.debug("Entering checkShipDate");
		}

		Date shipDate = msgVO.getShipDate();

		if (logger.isDebugEnabled()) {
			logger.debug("shipDate: " + shipDate);
		}

		boolean dateComparison = false;

		try {
			Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
			Date currentDate = calCurrent.getTime();

			if (!currentDate.before(shipDate)) {
				dateComparison = true;
			}

		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting checkShipDate");
			}
		}

		return dateComparison;
	}

	
	
	private String isFloralOrder(String shipMethod) {
		String isFloralOrder = "N";
		if (shipMethod == null || shipMethod.equals(UpdateOrderConstants.SD)) {
			isFloralOrder = "Y";

		}

		return isFloralOrder;
	}

	/**
	 * This method checks to see if there is already a record in the mo_xref
	 * table for this external order number. Return true if there is, false if
	 * there is not.
	 */
	private boolean hasOrderBeenModified(String externalOrderNumber)
			throws Exception {

		String orderHasBeenModified = uoDAO
				.hasOrderBeenModified(externalOrderNumber);
		boolean orderAlreadyModified = orderHasBeenModified
				.equalsIgnoreCase("Y") ? true : false;

		return orderAlreadyModified;
	}
}
