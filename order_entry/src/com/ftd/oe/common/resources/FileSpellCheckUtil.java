package com.ftd.oe.common.resources;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import java.net.URL;

import java.util.HashSet;
import java.util.TreeSet;


public class FileSpellCheckUtil extends SpellCheckUtilBase {
    private static final String dict_file = "dev-dictionary.txt";
    
    public FileSpellCheckUtil() {
        super();
        super.logger = new Logger("com.ftd.oe.common.resources.FileSpellCheckUtil");
        logger.debug("Starting FileSpellCheckUtil...");
        
        this.dictionary = new TreeSet();
        maxSuggestions = 10;

        loadDictionary();

        htmlTags = new HashSet();
        htmlTags.add( "<br>" );
    }

    protected void loadDictionary() {
        logger.debug("Loading dictionary :: "+dict_file);
        URL url = this.getClass().getClassLoader().getResource( dict_file );

        logger.debug("File location :: " + url.getFile());
        
        File file =  null;
        RandomAccessFile raf = null;
        
        try 
        {
            file = new File(url.getFile());
            raf = new RandomAccessFile(file, "r");
            logger.debug("Done reading file...");

           String str;
            while ((str = raf.readLine()) != null) {
                dictionary.add( str.trim().toLowerCase() );
            }

        } catch (Throwable t) {
            logger.error("Failed to load dictionary file "+dict_file+".  Spell checking will not be available.",t);
        } finally {
            logger.debug("Closing dictionary");
            if (raf != null) 
            {
                try {
                raf.close();
                    } catch (IOException ioe) {
                        logger.warn("Error while closing "+dict_file,ioe);
                    }
            }
        }
    }

    protected boolean isTimeToUpdate() {
        return false;
    }
}
