package com.ftd.oe.common.resources;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SpellCheckHandler;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;


public class CachedSpellCheckUtil extends SpellCheckUtilBase {
    private SpellCheckHandler sch;
    private Date lastUpdate = new Date();

/**
  * Default constructor, pre-load the small dictionary file into memory, throw exception
  * to the caller
  */
    public CachedSpellCheckUtil()
    {
        super();
        super.logger = new Logger("com.ftd.oe.common.resources.SpellCheckUtil");
        logger.debug("Starting CachedSpellCheckUtil...");
        
        sch = (SpellCheckHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SPELL_CHECK");
        
        this.dictionary = new TreeSet();
        maxSuggestions = 10;

        loadDictionary();

        htmlTags = new HashSet();
        htmlTags.add( "<br>" );
    }

    /**
      * load the words from the database <B>dictionary</B>
      */
    protected void loadDictionary()
    {
        synchronized (mutex) {
            if ( isTimeToUpdate() || this.dictionary==null || this.dictionary.size()==0 ) {
                logger.debug("Loading dictionary...");
                List dictionaryList = null;
                try {
                    dictionaryList = sch.getDictionaryList();
                    logger.debug("dictionary list returned from cache is: "+String.valueOf(dictionaryList.size()));
                    if( dictionaryList==null ) {
                        logger.error("A null dictionary list was returned from the cache.  Spell checking will not be available.");
                        return;
                    }
                } catch (Throwable t) {
                    logger.error("Error while retrieving the dictionary list from the cache handler.  Spell checking will not be available.",t);
                    return;
                }
                
                //Loop through cached values
                try {
                    List list = new ArrayList();
                    Iterator it = dictionaryList.iterator();
                    while( it.hasNext() ) {
                        String str = (String)it.next();
                        if( StringUtils.isNotBlank(str) ) {
                            list.add( str.trim().toLowerCase() );
                        }
                    }
                    
                    this.dictionary = new TreeSet(list);
                    this.lastUpdate = sch.getLastUpdate();                    
                    logger.debug("Dictionary is a size of "+String.valueOf(this.dictionary.size()));
                    
                    logger.debug("Spell check dictionary is ready.");
                } catch (Throwable t) {
                    logger.error("Error while initializing the spell checker.  Spell checking will not be available.",t);
                } 
           }
        }
    }

    protected boolean isTimeToUpdate() {
        if( this.lastUpdate==null || sch==null ) 
            return false;
        else
            return this.lastUpdate.getTime() < sch.getLastUpdate().getTime();
    }
}
