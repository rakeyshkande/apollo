package com.ftd.oe.common.resources;

import com.ftd.oe.common.OrderEntryConstants;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;

import java.util.Properties;

import javax.mail.Session;


public class TestResourceProviderImpl implements IResourceProvider {

    private static final Logger logger = new Logger("com.ftd.oe.common.resources.TestResourceProviderImpl");
    
    public TestResourceProviderImpl() {
    }
    
    public Connection getDatabaseConnection(String datasource) throws Exception {
        logger.debug("datasource: " + datasource);

        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String DB_DRIVER_CLASS = config.getProperty(OrderEntryConstants.TEST_CONFIG_FILE,datasource+".db.driver.class");
        String DB_USER_NAME = config.getProperty(OrderEntryConstants.TEST_CONFIG_FILE,datasource+".db.user.name");
        String DB_USER_CREDENTIALS = config.getProperty(OrderEntryConstants.TEST_CONFIG_FILE,datasource+".db.user.credentials");
        String DB_JDBC_URL = config.getProperty(OrderEntryConstants.TEST_CONFIG_FILE,datasource+".db.jdbc.url");

        logger.debug(DB_DRIVER_CLASS + " " + DB_USER_NAME + " " + DB_JDBC_URL);
        Connection conn = null;;

        try {
            DriverManager.registerDriver((Driver)(Class.forName(DB_DRIVER_CLASS).newInstance()));
            conn = DriverManager.getConnection(DB_JDBC_URL, DB_USER_NAME, DB_USER_CREDENTIALS);

        } catch (SQLException caughtException ) {
            // Test to see if the error is due to an invalid username/password
            if ( caughtException.getErrorCode() == 1017) {
                logger.error("\nInvalid user/password");
                logger.error("No Connection Established");
                return null;
            } else  {
                logger.error("\nOracle error: "+caughtException.getErrorCode());
                logger.error(caughtException);
                logger.error("Connection Terminated");
                return null;
            }
        } catch (Exception caughtException) {
            logger.error("Exception: " + caughtException);
            return null;
        }
        
        return conn;
    }


    public Session getEmailSession() throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String mailHost;
        try {
            mailHost = config.getProperty(OrderEntryConstants.TEST_CONFIG_FILE,"email.server");    
        } catch (Exception e) {
            mailHost = "mail4.ftdi.com";
        }
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol","smtp");
        properties.setProperty("mail.host",mailHost);
        Session session = Session.getInstance(properties);
        return session;
    }
}
