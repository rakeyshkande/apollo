package com.ftd.oe.common.resources;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


public abstract class SpellCheckUtilBase {
    protected TreeSet dictionary;
    protected int maxSuggestions;
    protected HashSet htmlTags;
    protected Object mutex = new Object();
    protected Logger logger;
    
    public SpellCheckUtilBase() {
    }
    /**
     * Set the maximum suggestions for a given mis-spelled word.
     */
    public void setMaximumSuggestion ( int maxSuggestions )
    {
        this.maxSuggestions = maxSuggestions;
    }

    /**
      * load the words from the database <B>dictionary</B>
      */
    protected abstract void loadDictionary();

/**
  * It completely ignores words with anything except letters, dashes, and apostrophes
  * @param word to be checked
  * @return String the processed word
  */
    private String prepForSpellCheck( String word )
    {
        if ( word == null )
            return "";

        StringBuffer sb = new StringBuffer();
        String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'-";

        for ( int i=0; i<word.length(); i++ )
        {
            char c = word.charAt( i );
            if ( validChars.indexOf( (int) c) != -1 )
                sb.append( c );
        }
        return sb.toString();
    }

    /**
    * Check to see if the word in in the dictionary
    * return false if failed again
    *
    * @param word a word to spell check
    * @return true if the word is in dictionary, false if not
    */
    public boolean spellCheck(String word) {
        logger.debug("Checking word \""+word+"\"");
        if ( word == null )
            return false;

        if ( htmlTags.contains( word ) )
            return true;
        
        if ( isTimeToUpdate() || this.dictionary==null || this.dictionary.size()==0 )
            loadDictionary();

        word = prepForSpellCheck( word );

        word = word.trim().toLowerCase();

        if ( word.length() == 0 )
            return true;

        return this.dictionary.contains( word );
    }

/**
  * The first letter of the soundex code is the first letter of the word (line 91).
  * The rest of the soundex code consists of numbers.
  * For each letter of the word after the first letter, append a number to the code
  * based on the rules. Note that vowels are ignored and several letters result in the same number.
  * Only append the number if it's not a repeat of the last number added to the soundex code
  *
  * @param word
  * @return String the soundex of word
  */
    private String soundex( String word )
    {
        if ( word == null )
            return null;
        word = word.toUpperCase();

        String result = "";

        if ( word.length() > 0 )
            result += word.charAt( 0 );

        char c;
        String code = "";

        for (int i = 1; i < word.length(); i++) {
            c = word.charAt( i );

            if( c == 'B' || c == 'P' )
                code = "1";
            else if( c == 'F' || c == 'V')
                code = "2";
            else if( c == 'C' || c == 'K' || c == 'S')
                code = "3";
            else if( c == 'J' || c == 'G')
                code = "4";
            else if( c == 'Q' || c == 'X' || c =='Z')
                code = "5";
            else if( c == 'D' || c == 'T')
                code = "6";
            else if( c == 'L')
                code = "7";
            else if( c == 'M' || c == 'N' )
                code = "8";
            else if( c == 'R' )
                code = "9";
            else
                code = "";

            if( !result.substring( result.length() -1 ).equals( code ) )
              result += code;
        }
        return result;
    }

/**
  * Calculate the word similarity index
  * @param strWord
  * @param similarWord
  * @return Float the value to evaluate the similarity
  */
    private float wordSimilarity(String strWord, String similarWord)
    {
        strWord = strWord.toLowerCase();
        similarWord = similarWord.toLowerCase();

        int wordLen = strWord.length();
        int similarWordLen = similarWord.length();
        int maxBonus = 3;
        int perfectValue = wordLen + wordLen + maxBonus;

        int similarityValue;

        similarityValue = maxBonus - Math.abs(wordLen - similarWordLen);

        int minLength = Math.min( wordLen, similarWordLen );

        for (int i = 0; i < minLength; i++)
        {
            if ( strWord.charAt( i ) == similarWord.charAt( i ) )
                similarityValue += 1;    // forward match

            if ( strWord.charAt( strWord.length() - i -1 ) == similarWord.charAt( similarWord.length() - i -1 ) )
                similarityValue += 1;   // backward match

        }

        return ((float)similarityValue / (float)perfectValue);
    }

/** Find the suggested word set
  * @param strWord original word
  * @return Set a set of suggestion words
  */

    public List suggest ( String strWord )
    {
        
        if ( strWord == null || strWord.length() == 0 )
            return null;
        
        if ( isTimeToUpdate() || this.dictionary==null || this.dictionary.size()==0 )
            loadDictionary();

        TreeSet result = new TreeSet();
        String soundexWord = this.soundex( strWord );

        strWord = this.prepForSpellCheck( strWord ).toLowerCase();

        if ( strWord == null || strWord.length() == 0 )
            return null;

        char startChar = strWord.charAt( 0 );
        char endChar = (char) (startChar + 1);
        SortedSet searchRange = this.dictionary.subSet( (new Character(startChar)).toString(), (new Character(endChar)).toString() );

        Iterator iter = searchRange.iterator();
        while (iter.hasNext() )
        {
            String tempWord = (String)iter.next();
            if ( soundex(tempWord).equals( soundexWord ) )
            {
                result.add( new SuggestWord( tempWord, wordSimilarity( strWord, tempWord ) ) );
            }
        }

        if ( result.isEmpty() )
            return null;

        int suggestionNumber = Math.min( result.size(), this.maxSuggestions );
        int index = 0;

        ArrayList suggested = new ArrayList();
        Iterator iter1 = result.iterator();

        while ( iter1.hasNext() )
        {
            suggested.add( ((SuggestWord)iter1.next()).word );
            if ( ++index >= suggestionNumber )
                break;
        }

        return suggested;
    }

    protected abstract boolean isTimeToUpdate();

    class SuggestWord implements Comparable
    {
        public float similarity;
        public String word;

        public SuggestWord( String word, float similarity )
        {
            this.word = word;
            this.similarity = similarity;
        }

        public int compareTo( Object other )
        {
            if ( other instanceof SuggestWord )
            {
                SuggestWord second = (SuggestWord)other;
                if ( this.similarity == second.similarity )
                    return this.word.compareTo( second.word );
                else
                    return (this.similarity > second.similarity) ? -1 : 1;
            }

            return this.toString().compareTo( other.toString() );
        }

        public boolean equals( Object other )
        {
            if ( other instanceof SuggestWord)
            {
                SuggestWord otherWord = (SuggestWord)other;
                return ( this.word.equals(otherWord.word) &&
                         this.similarity == otherWord.similarity );
            }

            return false;
        }
    }
}
