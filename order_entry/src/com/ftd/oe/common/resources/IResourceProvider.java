package com.ftd.oe.common.resources;

import com.ftd.osp.utilities.dataaccess.IDatabaseResourceProvider;

import java.sql.Connection;

import javax.mail.Session;


public interface IResourceProvider extends IDatabaseResourceProvider
{
    
    public Session getEmailSession() 
        throws Exception;
}
