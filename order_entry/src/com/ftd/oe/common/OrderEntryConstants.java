package com.ftd.oe.common;

public class OrderEntryConstants {


    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT    = "context";
    public static final String SEC_TOKEN  = "securitytoken";
    public static final String CONS_MAIN_MENU_URL   = "oe.main.menu";
    public static final String ADMIN_ACTION = "adminAction";
    //public static final String SECURITY_IS_ON  = "SECURITY_IS_ON";
    public static final String SECURE_CONFIG_CONTEXT = "ORDER-ENTRY";
    public static final String TEST_CONFIG_FILE = "order_entry_test_config.xml";
    public static final String DATASOURCE = "WOESDS";
    public static final String MILES_POINTS_PRICE_CODE = "ZZ";
    public static final String JOE_CONFIG_FILE = "oe_config.xml";

    public static final String CACHE_INTRO_DATA_HANDLER = "CACHE_NAME_INTRO_DATA";
    public static final String CACHE_ELEMENT_CONFIG_HANDLER = "CACHE_NAME_ELEMENT_CONFIG_DATA";

    public static final String JOE_CONTEXT = "OE_CONFIG";
    public static final String FTD_APPS_CONTEXT = "FTDAPPS_PARMS";
    public static final String ORDER_PROCESSING_CONTEXT = "ORDER_PROCESSING_CONFIG";
    public static final String PRICE_CALCULATION_CONTEXT = "PRICE_CALCULATION";
    public static final String NOVATOR_CONFIG = "NOVATOR_CONFIG";
    public static final String DELIVERY_DROPDOWN_DAYS = "DELIVERY_DATE_DROPDOWN_DAYS";
    public static final String FACILITY_SEARCH_RESULTS = "FACILITY_SEARCH_TOTAL_RESULTS";
    public static final String PRODUCT_SEARCH_RESULTS_TOTAL = "PRODUCT_SEARCH_RESULTS_TOTAL";
    public static final String IMAGE_SERVER_URL = "IMAGE_SERVER_URL";
    public static final String ORDER_GATHERER_URL = "ORDER_GATHERER_URL";
    public static final String ORDER_SEND_RETRY_MAX = "ORDER_SEND_RETRY_MAX";
    public static final String FRESHCUT_TRIGGER = "FRESHCUTS_SVC_CHARGE_TRIGGER";
    public static final String FRESHCUT_SERVICE_CHARGE = "FRESHCUTS_SVC_CHARGE";
    public static final String FRESHCUT_SATURDAY_CHARGE = "FRESHCUTS_SAT_CHARGE";
    public static final String ALASKA_HAWAII_CHARGE = "SPECIAL_SVC_CHARGE";
    public static final String SDG_GLOBAL_SERVICE_FEE = "SDG_GLOBAL_SERVICE_FEE";
    public static final String NOVATOR_PRODUCTION_FEED_IP = "production_feed_ip";
    public static final String NOVATOR_PRODUCTION_FEED_PORT = "production_feed_port";
    public static final String NOVATOR_CONTENT_FEED_IP = "content_feed_ip";
    public static final String NOVATOR_CONTENT_FEED_PORT = "content_feed_port";
    public static final String NOVATOR_TEST_FEED_IP = "test_feed_ip";
    public static final String NOVATOR_TEST_FEED_PORT = "test_feed_port";
    public static final String NOVATOR_UAT_FEED_IP = "uat_feed_ip";
    public static final String NOVATOR_UAT_FEED_PORT = "uat_feed_port";
    public static final String NOVATOR_FEED_TIMEOUT = "feed_timeout";
    public static final String NOVATOR_CONTENT_FEED_ALLOWED  = "content_feed_allowed";
    public static final String NOVATOR_PRODUCTION_FEED_ALLOWED  = "production_feed_allowed";
    public static final String NOVATOR_TEST_FEED_ALLOWED  = "test_feed_allowed";
    public static final String NOVATOR_UAT_FEED_ALLOWED  = "uat_feed_allowed";
    public static final String NOVATOR_CONTENT_FEED_CHECKED  = "content_feed_checked";
    public static final String NOVATOR_PRODUCTION_FEED_CHECKED  = "production_feed_checked";
    public static final String NOVATOR_TEST_FEED_CHECKED  = "test_feed_checked";
    public static final String NOVATOR_UAT_FEED_CHECKED  = "uat_feed_checked";
    public static final String IOTW_PAGESIZE = "IOTW_PAGESIZE";
    public static final String VIEW = "View";
    public static final String ITEM_OF_THE_WEEK_READ_ONLY = "Item Of The Week Read Only";
    public static final String APE_IOTW_Maintenance = "APE IOTW Maintenance";
    public static final String UPDATE = "Update";

    public static final String CCAS_CONTEXT = "CCAS_CONFIG";
    public static final String CCAS_AUTH_ACTIVE = "ccas.auth.client.active";
    public static final String VALIDATION_CONTEXT = "VALIDATION_SERVICE";
    public static final String MAX_ITEM_TOTAL = "MAX_ITEM_TOTAL";
    public static final String MAX_LINE_ITEMS = "MAX_LINE_ITEMS";
    public static final String MAX_ITEM_ADDON_TOTAL = "MAX_ITEM_ADDON_TOTAL";
    public static final String MAX_ORDER_TOTAL = "MAX_ORDER_TOTAL";
    public static final String LUX_MAX_ITEM_TOTAL = "LUX_MAX_ITEM_TOTAL";
    public static final String LUX_MAX_LINE_ITEMS = "LUX_MAX_LINE_ITEMS";
    public static final String LUX_MAX_ITEM_ADDON_TOTAL = "LUX_MAX_ITEM_ADDON_TOTAL";
    public static final String LUX_MAX_ORDER_TOTAL = "LUX_MAX_ORDER_TOTAL";
    public static final String INGRIAN_CONTEXT = "Ingrian";
    public static final String CURRENT_KEY = "Current Key";

    public static final String TAG_RESULT = "result";
    public static final String TAG_STATUS = "status";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_REQUEST = "request";
    public static final String TAG_TYPE = "type";
    public static final String TAG_PARAM = "param";
    public static final String TAG_NAME = "name";
    public static final String TAG_ECHO = "echo";
    public static final String TAG_ECHO_NODE = "echoNode";
    public static final String TAG_RS = "rs";
    public static final String TAG_RECORD = "record";
    public static final String TAG_ROW = "row";
    public static final String TAG_NUM = "num";
    public static final String TAG_REASON = "reason";
    public static final String TAG_VALIDATION = "validation";
    public static final String TAG_VALIDATE = "validate";
    public static final String TAG_SHIP_METHODS = "ship-methods";

    public static final String REQUEST_XML = "AJAX_XML";
    public static final String REQUEST_TYPE = "AJAX_REQUEST_TYPE";
    public static final String REQUEST_GET_INTRO_DATA = "AJAX_REQUEST_GET_INTRO_DATA";
    public static final String REQUEST_GET_CUSTOMER_BY_PHONE_NUMBER = "AJAX_REQUEST_PHONE_SEARCH";
    public static final String REQUEST_VALIDATE_DNIS = "AJAX_REQUEST_VALIDATE_DNIS";
    public static final String REQUEST_VALIDATE_SOURCE_CODE = "AJAX_REQUEST_VALIDATE_SOURCE_CODE";
    public static final String REQUEST_VALIDATE_ZIP_CODE = "AJAX_REQUEST_VALIDATE_ZIP_CODE";
    public static final String REQUEST_VALIDATE_PRODUCT = "AJAX_REQUEST_VALIDATE_PRODUCT";
    public static final String REQUEST_VALIDATE_PRODUCT_AVAILABILITY = "AJAX_REQUEST_VALIDATE_PRODUCT_AVAILABILITY";
    public static final String REQUEST_VALIDATE_FLORIST = "AJAX_REQUEST_VALIDATE_FLORIST_ID";
    public static final String REQUEST_VALIDATE_AVS_ADDRESS = "AJAX_REQUEST_VALIDATE_AVS";
    public static final String REQUEST_VALIDATE_GIFT_CERTIFICATE = "AJAX_REQUEST_VALIDATE_GIFT_CERTIFICATE";
    public static final String REQUEST_CALCULATE_ORDER_TOTALS = "AJAX_REQUEST_CALCULATE_ORDER_TOTALS";
    public static final String REQUEST_RECORD_CALL_TIME = "AJAX_REQUEST_RECORD_CALL_TIME";
    public static final String REQUEST_GET_POPULAR_PRODUCTS = "AJAX_REQUEST_GET_TOP_PRODUCTS";
    public static final String REQUEST_GET_CROSS_SELL_PRODUCTS = "AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS";
    public static final String REQUEST_SOURCE_CODE_SEARCH = "AJAX_REQUEST_SOURCE_CODE_SEARCH";
    public static final String REQUEST_PRODUCT_SEARCH = "AJAX_REQUEST_PRODUCT_SEARCH";
    public static final String REQUEST_FLORIST_SEARCH = "AJAX_REQUEST_FLORIST_SEARCH";
    public static final String REQUEST_FACILITY_SEARCH = "AJAX_REQUEST_FACILITY_SEARCH";
    public static final String REQUEST_ZIP_SEARCH = "AJAX_REQUEST_ZIP_SEARCH";
    public static final String REQUEST_SAVE_CART = "AJAX_REQUEST_SAVE_CART";
    public static final String REQUEST_GET_ELEMENT_DATA = "AJAX_REQUEST_GET_ELEMENT_DATA";
    public static final String REQUEST_XSELL_GET_COMPANIES = "AJAX_REQUEST_XSELL_GET_COMPANIES";
    public static final String REQUEST_XSELL_GET_MASTER_RECORDS = "AJAX_REQUEST_XSELL_GET_MASTER_RECORDS";
    public static final String REQUEST_XSELL_UPDATE = "AJAX_REQUEST_XSELL_UPDATE";
    public static final String REQUEST_XSELL_REMOVE_XSELLS = "AJAX_REQUEST_XSELL_REMOVE_XSELLS";
    public static final String REQUEST_XSELL_GET_XSELL = "AJAX_REQUEST_XSELL_GET_XSELL";
    public static final String REQUEST_NOVATOR_UPDATE_SETUP = "AJAX_REQUEST_NOVATOR_UPDATE_SETUP";
    public static final String REQUEST_GET_CARD_MESSAGES = "AJAX_REQUEST_GET_CARD_MESSAGES";
    public static final String REQUEST_SAVE_CARD_MESSAGES = "AJAX_REQUEST_SAVE_CARD_MESSAGES";
    public static final String REQUEST_FAV_GET_FAVORITES = "AJAX_REQUEST_FAV_GET_FAVORITES";
    public static final String REQUEST_FAV_UPDATE_FAVORITES = "AJAX_REQUEST_FAV_UPDATE_FAVORITES";
    public static final String REQUEST_VALIDATE_STATE_BY_ZIP_CITY = "AJAX_REQUEST_VALIDATE_STATE_BY_ZIP_CITY";
    public static final String REQUEST_GET_DELIVERY_DATES = "AJAX_REQUEST_GET_DELIVERY_DATES";
    public static final String REQUEST_GET_UPDATE_ORDER_INFO = "AJAX_REQUEST_GET_UPDATE_ORDER_INFO";
    public static final String REQUEST_MEMBERSHIP_ID_SEARCH = "AJAX_REQUEST_MEMBERSHIP_ID_SEARCH";
    public static final String REQUEST_GET_SYMPATHY_LOCATION_CHECK = "AJAX_REQUEST_GET_SYMPATHY_LOCATION_CHECK";

    public static final String RESULTS_SUCCESS = "Request was successful.";
    public static final String RESULTS_FAILURE = "Request failed!";
    public static final String RESULTS_EMPTY = "Database query returned no results.";

    public static final String PARAMETER_DNIS_ID = "DNIS_ID";
    public static final String PARAMETER_SOURCE_CODE = "SOURCE_CODE";
    public static final String PARAMETER_ZIP_CODE = "ZIP_CODE";
    public static final String PARAMETER_PRODUCT_ID = "PRODUCT_ID";
    public static final String PARAMETER_UPDATED_AFTER = "UPDATED_AFTER";
    public static final String PARAMETER_ORDER_XML = "ORDER_XML";
    public static final String PARAMETER_PHONE_NUMBER = "PHONE_NUMBER";
    public static final String PARAMETER_DELIVERY_DATE = "DELIVERY_DATE";
    public static final String PARAMETER_DELIVERY_TIME = "DELIVERY_TIME";
    public static final String PARAMETER_DELIVERY_METHOD = "PRODUCT_DELIVERY_METHOD";
    public static final String PARAMETER_CITY = "CITY";
    public static final String PARAMETER_STATE = "STATE";
    public static final String PARAMETER_ADDRESS_TYPE = "ADDRESS_TYPE";
    public static final String PARAMETER_LEAD_TIME_CHECK_FLAG = "LEAD_TIME_CHECK_FLAG";
    public static final String PARAMETER_KEYWORDS = "KEYWORD";
    public static final String PARAMETER_SEARCH_TYPE = "SEARCH_TYPE";
    public static final String PARAMETER_COUNTRY_ID = "COUNTRY_ID";
    public static final String PARAMETER_FLORIST_ID = "FLORIST_ID";
    public static final String PARAMETER_ADDRESS = "ADDRESS";
    public static final String PARAMETER_FIRM_NAME = "FIRM_NAME";
    public static final String PARAMETER_GIFT_CERTIFICATE_ID = "CERTIFICATE_ID";
    public static final String PARAMETER_START_TIME = "START_TIME";
    public static final String PARAMETER_CSR_ID = "CSR_ID";
    public static final String PARAMETER_EXTERNAL_ORDER_NUMBER = "EXTERNAL_ORDER_NUMBER";
    public static final String PARAMETER_MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
    public static final String PARAMETER_COMPANY_ID = "COMPANY_ID";
    public static final String PARAMETER_DATE_RANGE_FLAG = "DATE_RANGE_FLAG";
    public static final String PARAMETER_PRODUCT_CROSS_SELL_ID = "PRODUCT_CROSS_SELL_ID";
    public static final String PARAMETER_CROSS_SELL_PRODUCT_ID = "CROSS_SELL_PRODUCT_ID";
    public static final String PARAMETER_NOVATOR_UPDATE_LIVE = "NOVATOR_UPDATE_LIVE";
    public static final String PARAMETER_NOVATOR_UPDATE_TEST = "NOVATOR_UPDATE_TEST";
    public static final String PARAMETER_NOVATOR_UPDATE_CONTENT = "NOVATOR_UPDATE_CONTENT";
    public static final String PARAMETER_NOVATOR_UPDATE_UAT = "NOVATOR_UPDATE_UAT";
    public static final String PARAMETER_XSELL_PRODUCT = "XSELL_PRODUCT";
    public static final String PARAMETER_CARD_MESSAGES_XML = "CARD_MESSAGES_XML";
    public static final String PARAMETER_DISPLAY_SEQUENCE = "DISPLAY_SEQUENCE";
    public static final String PARAMETER_IOTW_SOURCE_CODE = "IOTW_SOURCE_CODE";
    public static final String PARAMETER_PRICE_HEADER_ID = "PRICE_HEADER_ID";
    public static final String PARAMETER_SEC_TOKEN = "SEC_TOKEN";
    public static final Object PARAMETER_ORDER_TYPE = "ORDER_TYPE";
    public static final Object PARAMETER_ADDONS = "ADDONS";

    public static final String XML_TAG_ORDER = "order";
    public static final String XML_TAG_HEADER = "header";
    public static final String XML_TAG_DNIS_ID = "dnis-id";
    public static final String XML_TAG_SOURCE_CODE = "source-code";
    public static final String XML_TAG_MASTER_ORDER_NUMBER = "master-order-number";
    public static final String XML_TAG_ORIGIN = "origin";
    public static final String XML_TAG_ORDER_DATE = "transaction-date";
    public static final String XML_TAG_ORDER_COUNT = "order-count";
    public static final String XML_TAG_ORDER_AMOUNT = "order-amount";
    public static final String XML_TAG_JOE_FLAG = "joe-indicator";
    public static final String XML_TAG_CREATED_BY = "order-created-by-id";
    public static final String XML_TAG_COMPANY_ID = "company";
    public static final String XML_TAG_BUYER_FIRST_NAME = "buyer-first-name";
    public static final String XML_TAG_BUYER_LAST_NAME = "buyer-last-name";
    public static final String XML_TAG_BUYER_ADDRESS1 = "buyer-address1";
    public static final String XML_TAG_BUYER_CITY = "buyer-city";
    public static final String XML_TAG_BUYER_STATE = "buyer-state";
    public static final String XML_TAG_BUYER_ZIP_CODE = "buyer-postal-code";
    public static final String XML_TAG_BUYER_COUNTRY = "buyer-country";
    public static final String XML_TAG_BUYER_PRIMARY_PHONE = "buyer-primary-phone";
    public static final String XML_TAG_BUYER_PRIMARY_EXT = "buyer-primary-phone-ext";
    public static final String XML_TAG_BUYER_SECONDARY_PHONE = "buyer-secondary-phone";
    public static final String XML_TAG_BUYER_SECONDARY_EXT = "buyer-secondary-phone-ext";
    public static final String XML_TAG_BUYER_EMAIL_ADDRESS = "buyer-email-address";
    public static final String XML_TAG_BUYER_SIGNED_IN = "buyer-signed-in";
    public static final String XML_TAG_NEWSLETTER_FLAG = "news-letter-flag";
    public static final String XML_TAG_FRAUD_FLAG = "fraud-flag";
    public static final String XML_TAG_FRAUD_ID = "fraud-id";
    public static final String XML_TAG_FRAUD_COMMENTS = "fraud-comments";
	public static final String XML_TAG_LANGUAGE_ID = "language-id";
	public static final String XML_TAG_BUYER_HAS_FREE_SHIPPING = "buyer-has-free-shipping";

    public static final String XML_TAG_CC_NUMBER = "cc-number";
    public static final String XML_TAG_CC_TYPE = "cc-type";
    public static final String XML_TAG_CC_EXPIRATION = "cc-exp-date";
    public static final String XML_TAG_CC_AMOUNT = "cc-approval-amt";
    public static final String XML_TAG_CC_APPROVAL_CODE = "cc-approval-code";
    public static final String XML_TAG_CC_APPROVAL_VERBIAGE = "cc-approval-verbage";
    public static final String XML_TAG_CC_APPROVAL_ACTION_CODE = "cc-approval-action-code";
    public static final String XML_TAG_CC_AVS_RESULT = "cc-avs-result";
    public static final String XML_TAG_CC_ACQ_DATA = "cc-acq-data";
    public static final String XML_TAG_AAFES_TICKET_NUMBER = "aafes-ticket-number";
    public static final String XML_TAG_CC_BYPASS_AUTH_FLAG = "cc-bypass-auth-flag";
    public static final String XML_TAG_CC_CSC = "csc-value";
    public static final String XML_TAG_CC_CSC_OVERRIDE = "csc-override-flag";
    public static final String XML_TAG_CC_CSC_FAILED_COUNT = "csc-failure-count";
    public static final String XML_TAG_CC_CSC_RESPONSE_CODE = "csc-response-code";
    public static final String XML_TAG_CC_CSC_VALIDATED_FLAG = "csc-validated-flag";

    public static final String XML_TAG_CO_BRANDS = "co-brands";
    public static final String XML_TAG_CO_BRAND = "co-brand";
    public static final String XML_TAG_DATA = "data";
    public static final String XML_TAG_NAME = "name";
    public static final String XML_TAG_FNAME = "FNAME";
    public static final String XML_TAG_LNAME = "LNAME";

    public static final String XML_TAG_GIFT_CERTIFICATES = "gift-certificates";
    public static final String XML_TAG_GIFT_CERTIFICATE = "gift-certificate";
    public static final String XML_TAG_NUMBER = "number";
    public static final String XML_TAG_AMOUNT = "amount";

    public static final String XML_TAG_NO_CHARGE = "no-charge";
    public static final String XML_TAG_NC_APPROVAL_ID = "approval-id";
    public static final String XML_TAG_NC_APPROVAL_PASSWORD = "approval-password";
    public static final String XML_TAG_NC_TYPE = "type";
    public static final String XML_TAG_NC_REASON = "reason";
    public static final String XML_TAG_NC_ORDER_REFERENCE = "order-reference";
    public static final String XML_TAG_NC_AMOUNT = "amount";

    public static final String XML_TAG_ITEM = "item";
    public static final String XML_TAG_ITEMS = "items";
    public static final String XML_TAG_EXTERNAL_ORDER_NUMBER = "order-number";
    public static final String XML_TAG_ITEM_SOURCE_CODE = "item-source-code";
    public static final String XML_TAG_PRODUCT_ID = "productid";
    public static final String XML_TAG_PRODUCT_SUBCODE_ID = "product-subcode-id";
    public static final String XML_TAG_DELIVERY_DATE = "delivery-date";
    public static final String XML_TAG_SECOND_DELIVERY_DATE = "second-delivery-date";
    public static final String XML_TAG_RECIP_FIRST_NAME = "recip-first-name";
    public static final String XML_TAG_RECIP_LAST_NAME = "recip-last-name";
    public static final String XML_TAG_RECIP_ADDRESS1 = "recip-address1";
    public static final String XML_TAG_RECIP_CITY = "recip-city";
    public static final String XML_TAG_RECIP_STATE = "recip-state";
    public static final String XML_TAG_RECIP_ZIP_CODE = "recip-postal-code";
    public static final String XML_TAG_RECIP_COUNTRY = "recip-country";
    public static final String XML_TAG_RECIP_PHONE = "recip-phone";
    public static final String XML_TAG_RECIP_PHONE_EXT = "recip-phone-ext";
    public static final String XML_TAG_QMS_ADDRESS1 = "qms-address1";
    public static final String XML_TAG_QMS_CITY = "qms-city";
    public static final String XML_TAG_QMS_STATE = "qms-state";
    public static final String XML_TAG_QMS_ZIP_CODE = "qms-postal-code";
    public static final String XML_TAG_QMS_RESULT_CODE = "qms-result-code";
    public static final String XML_TAG_CUSTOMER_INSISTED = "recip-address-customer-insisted";
    public static final String XML_TAG_IOTW_FLAG = "item-of-the-week-flag";
    public static final String XML_TAG_OCCASION = "occassion";
    public static final String XML_TAG_CARD_MESSAGE = "card-message";
    public static final String XML_TAG_SPECIAL_INSTRUCTIONS = "special-instructions";
    public static final String XML_TAG_ORDER_COMMENTS = "order-comments";
    public static final String XML_TAG_SHIP_METHOD = "shipping-method";
    public static final String XML_TAG_SIZE_INDICATOR = "size-indicator";
    public static final String XML_TAG_PRODUCT_PRICE = "product-price";
    public static final String XML_TAG_RETAIL_VARIABLE_PRICE = "retail-variable-price";
    public static final String XML_TAG_DISCOUNTED_PRODUCT_PRICE = "discounted-product-price";
    public static final String XML_TAG_DISCOUNT_AMOUNT = "discount-amount";
    public static final String XML_TAG_SERVICE_SHIPPING_FEE = "drop-ship-charges";
    public static final String XML_TAG_SERVICE_FEE = "service-fee";
    public static final String XML_TAG_LATE_CUTOFF_FEE = "late-cutoff-fee";
    public static final String XML_TAG_MONDAY_UPCHARGE = "vendor-mon-upcharge";
    public static final String XML_TAG_SUNDAY_UPCHARGE = "vendor-sun-upcharge";
    public static final String XML_TAG_TAX_AMOUNT = "tax-amount";
    public static final String XML_TAG_TAXES = "taxes";
    public static final String XML_TAG_TAXES_TAX = "tax";
    public static final String XML_TAG_TAXES_TAX_TYPE = "type";
    public static final String XML_TAG_TAXES_TAX_DESCRIPTION = "description";
    public static final String XML_TAG_TAXES_TAX_RATE = "rate";
    public static final String XML_TAG_TAXES_TAX_AMOUNT = "amount";
    public static final String XML_TAG_TAXES_TOTAL_TAX = "total_tax";
    public static final String XML_TAG_TAXES_TAX_SERVICE_PERFORMED = "tax-service-performed";
    public static final String XML_TAG_ADDON_AMOUNT = "add-on-amount";
    public static final String XML_TAG_ITEM_TOTAL = "order-total";
    public static final String XML_TAG_SHIP_TO_TYPE = "ship-to-type";
    public static final String XML_TAG_SHIP_TO_NAME = "ship-to-type-name";
    public static final String XML_TAG_SHIP_TO_INFO = "ship-to-info";
    public static final String XML_TAG_RECIP_ROOM = "recip-room";
    public static final String XML_TAG_RECIP_HOURS_START = "recip-hours-start";
    public static final String XML_TAG_RECIP_HOURS_END = "recip-hours-end";
    public static final String XML_TAG_TIME_OF_SERVICE_FLAG = "time-of-service-flag";
    public static final String XML_TAG_TIME_OF_SERVICE = "time-of-service";
    public static final String XML_TAG_FLORIST_ID = "florist";
    public static final String XML_TAG_FIRST_COLOR_CHOICE = "first-color-choice";
    public static final String XML_TAG_SECOND_COLOR_CHOICE = "second-color-choice";
    public static final String XML_TAG_CART_INDEX = "cart-index";
    public static final String XML_TAG_MILES_POINTS = "miles-points";
    public static final String XML_TAG_PERSONAL_GREETING_ID = "personal-greeting-id";
    public static final String XML_TAG_BIN_SOURCE_CHANGED_FLAG = "bin-source-changed";
    public static final String XML_TAG_ORIGINAL_ORDER_HAS_SDU = "original-order-has-sdu";

    public static final String PERSONAL_GREETING_PHONE_NUMBER_TOKEN = "~pgphone~";
    public static final String PERSONAL_GREETING_CONTEXT  = "PERSONAL_GREETINGS";
    public static final String PERSONAL_GREETING_PHONE_NUMBER_FILTER = "RECORDING_PHONE";
    public static final String PERSONAL_GREETING_INSTRUCTIONS_PHONE = "INSTRUCTIONS_PHONE";
    public static final String PERSONAL_GREETING_IDENTIFIER  = "IDENTIFIER";

    public static final String XML_TAG_ADDONS = "add-ons";
    public static final String XML_TAG_ADDON = "add-on";
    public static final String XML_TAG_ID = "id";
    public static final String XML_TAG_QUANTITY = "quantity";
    public static final String XML_TAG_FUNERAL_BANNER = "text";



    /* COM parameters */
    public static final String SEARCH_ACTION = "action";
    public static final String RECIPIENT_FLAG = "recipient_flag";
    public static final String ORDER_NUMBER = "in_order_number";

    // JOE does not allow products with the Over 21 flag set to "Y"
    public static final boolean INCLUDE_OVER_21_PRODUCTS = false;

    public static final String SHIP_METHOD_NEXT_DAY =  "ND";
    public static final String SHIP_METHOD_TWO_DAY =   "2F";
    public static final String SHIP_METHOD_SATURDAY =  "SA";
    public static final String SHIP_METHOD_GROUND =    "GR";
    public static final String SHIP_METHOD_SUNDAY =    "SU";

    // Free Shipping
    public static final String FREE_SHIPPING_NAME = "FREESHIP";
	public static final String RESPONSE_CAMS_UNREACHABLE = "System cannot determine {programName} eligibility at this time. Ask if customer has an {programName}";

	//Defect#16-BAMS Integration : Additional auth response elements
	public static final String XML_TAG_TRANSMISSION_DATE_TIME = "transmission-date-time";
	public static final String XML_TAG_CC_SYSTEM_TRACE_AUDIT_NUMBER = "cc-system-trace-audit-number";
	public static final String XML_TAG_CC_REFERENCE_NUMBER = "cc-ref-num";
	public static final String XML_TAG_CARDHOLDER_ACTIVATED_TERMINAL = "cardholder-activated-terminal";
	public static final String XML_TAG_VISA_POS_CONDITION_CODE = "visa-pos-condition-code";
	public static final String XML_TAG_CC_TRANS_CRNCY = "cc-trans-crncy";
	public static final String XML_TAG_CC_RESPONSE_CODE = "cc-response-code";
	public static final String XML_TAG_CARD_SPECIFIC_GROUP = "card-specific-group";
	public static final String XML_TAG_CARD_SPECIFIC_DETAIL = "card-specific-detail";
	public static final String XML_TAG_CC_AUTH_PROVIDER = "cc-auth-provider";

	//Original Order Details tags
	public static final String XML_TAG_ORIG_ORDER_GUID = "orig_order_guid";
	public static final String XML_TAG_ORIG_EXTERNAL_ORDER_NUMBER="orig_external_order_number";
	public static final String XML_TAG_ORIG_ORDER_DETAIL_ID="orig_order_detail_id";
	public static final String XML_TAG_ORIG_DELIVERY_DATE="orig_delivery_date";
	public static final String XML_TAG_ORIG_RECIPIENT_ID="orig_recipient_id";
	public static final String XML_TAG_ORIG_SHIP_METHOD="orig_ship_method";
	public static final String XML_TAG_ORIG_HAS_SDU="orig_has_sdu";
	public static final String XML_TAG_ORIG_PRODUCT_ID="orig_product_id";
	public static final String XML_TAG_WALLET_INDICATOR="wallet-indicator";

	public static final String XML_TAG_UPDATE_ORDER_FLAG = "update-order-flag";
	public static final String XML_TAG_PI_MODIFY_ORDER_FLAG = "pi-modify-order-flag";
	public static final String XML_TAG_PI_PARTNER_ID = "pi-partner-id";
	public static final String XML_TAG_PI_MODIFY_ORDER_THRESHOLD = "pi-modify-order-threshold";
	public static final String XML_TAG_PI_ORIGINAL_MERC_TOTAL = "pi-original-merc-total";
	public static final String XML_TAG_PI_ORIGINAL_PDB_PRICE = "pi-original-pdb-price";
	public static final String XML_TAG_PI_ORIGINAL_FLORIST_ID = "pi-original-florist-id";
	
	//Added for sp-87 for jccas validations based on BAMS on or off.
	public static final String AUTH_CONFIG = "AUTH_CONFIG";
	public static final String AUTH_PROVIDER = "cc.auth.provider";
	public static final String BAMS_CC_LIST = "bams.cc.list";
	public static final String BAMS_AUTH_PROVIDER="BAMS";

}
