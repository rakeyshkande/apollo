package com.ftd.oe.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.gson.Gson;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.SympathyControls;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.interfaces.LookupBO;
import com.ftd.oe.util.IntroDataHandler;
import com.ftd.oe.util.ProductUtil;
import com.ftd.oe.vo.FloristLookupVO;
import com.ftd.oe.vo.ProductVO;
import com.ftd.oe.vo.fresh.PromotionsVO;
import com.ftd.oe.vo.fresh.PartnerVO;
import com.ftd.oe.vo.fresh.IotwProductsVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.ElementConfigHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.vo.SympathyItemsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.visitor.OrderEntryElementVO;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pas.core.domain.ProductAvailVO;
import com.ftd.textsearch.client.FacilitySearchClient;
import com.ftd.textsearch.client.IOTWSearchClient;
import com.ftd.textsearch.client.ProductSearchClient;
import com.ftd.textsearch.client.SourceCodeSearchClient;
import com.ftd.textsearch.vo.FacilitySearchVO;
import com.ftd.textsearch.vo.IOTWSearchVO;
import com.ftd.textsearch.vo.ProductSearchVO;
import com.ftd.textsearch.vo.SourceCodeSearchVO;


public class FTDLookupBO implements LookupBO {

    private static Logger logger = new Logger("com.ftd.oe.bo.FTDLookupBO");
    private static IResourceProvider resourceProvider;
    private static OrderEntryDAO orderEntryDAO;
    private static String FRESH_SERVICE_PROMO_PULL = "Fresh Pull";

    public FTDLookupBO() {
    }

    public String getIntroData() throws Exception {
        String data = null;
        logger.debug("getIntroData()");
        
        try {

            IntroDataHandler idh = (IntroDataHandler)CacheManager.getInstance().getHandler(OrderEntryConstants.CACHE_INTRO_DATA_HANDLER);
            data = idh.getIntroDataJSON();

        } catch (Throwable t) {
            logger.error("Error in getIntroData()",t);
            throw new Exception("Could not retrieve data.");
        }
        
        return data;
    }
    
    @SuppressWarnings("rawtypes")
	public String getSympathyLocationCheck(String sourceCode, String companyId,
			String state, String deliveryDate,String deliveryTime,String addressType,String deliveryMethod,boolean leadTimeCheckFlag) throws Exception {
        String data = "";
        Integer timeOfService = -1;
        Connection qConn = null;
        logger.debug("getSympathyLocationCheck()");
        
        try {
        	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        	Date date = formatter.parse(deliveryDate);
        	String dataSource = OrderEntryConstants.DATASOURCE;
        	SympathyControls sympathyControls = new SympathyControls(qConn);
            qConn = resourceProvider.getDatabaseConnection(dataSource);   	
            
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            if(deliveryTime!=null && !deliveryTime.equals("")){
            	
            		timeOfService = Integer.valueOf(deliveryTime);
            	
            	
            }
            if(addressType.contains("FUNERAL") || addressType.contains("CEMETERY") || addressType.contains("HOSPITAL")){
            	SympathyItemsVO itemVO = new SympathyItemsVO();
            	itemVO.setSourceCode(sourceCode);
            	itemVO.setDeliveryDate(date);
            	itemVO.setDeliveryTime(timeOfService);
            	itemVO.setShipState(state);
            	itemVO.setDeliveryLocation(addressType);
            	itemVO.setShipMethod(deliveryMethod);
            	itemVO.setLeadTimeCheckFlag(leadTimeCheckFlag);
            	itemVO.setModule("JOE");
            	Map map = sympathyControls.performSympathyControls(qConn, itemVO);
            	if(map.get("status").equals('N')){
                	data = String.valueOf(map.get("message"));
                    logger.error(String.valueOf(map.get("message")));
                } 
            }           
            
        } catch (Throwable t) {
            logger.error("Error in validating Sympathy Controls",t);
            throw new Exception("Could not validate Sympathy controls.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return data;
    }

    
    public String getElementData() throws Exception {
        String data = null;
        logger.debug("getElementData()");
        
        try {
            ElementConfigHandler ech = (ElementConfigHandler)CacheManager.getInstance().getHandler(OrderEntryConstants.CACHE_ELEMENT_CONFIG_HANDLER);
            data = ech.getElementsJSON("JOE");
        } catch (Throwable t) {
            logger.error("Error in getElementData()",t);
            throw new Exception("Could not retrieve element config data.");
        } 
        
        return data;
    }

    public Document getElementData(String appCode) throws Exception {
        Document doc = null;
        
        if(logger.isDebugEnabled())
            logger.debug("getElementData(String appcode(" + appCode + ")");
        
        try {
            ElementConfigHandler ech = (ElementConfigHandler)CacheManager.getInstance().getHandler(OrderEntryConstants.CACHE_ELEMENT_CONFIG_HANDLER);
            doc = ech.getElementsXml(appCode);
            
        } catch (Throwable t) {
            logger.error("Error in getElementData(String appcode(" + appCode + ")",t);
            throw new Exception("Could not retrieve element config data for appcode " + appCode + ".");
        } 
        
        return doc;
    }
  
    public Document getCustomerByPhoneNumber(String phoneNumber, String companyId, String sourceCode) throws Exception {
        Connection qConn = null;
        Document doc = null;
        logger.debug("getCustomerByPhoneNumber(" + phoneNumber + ", " + companyId + ", " + sourceCode + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            phoneNumber = phoneNumber.replaceAll( "\\D", "" );
            if (phoneNumber == null) {
                doc = OEActionBase.createErrorResultSet("customer", "Invalid phone number");
            } else if (phoneNumber.equals("0000000000") || phoneNumber.equals("9999999999")) {
                doc = OEActionBase.createErrorResultSet("customer", "Search not performed for phone number.");
            } else {
                if (companyId == null) companyId = "FTD";
                doc = orderEntryDAO.getCustomerByPhoneNumberAjax(qConn, phoneNumber, companyId, sourceCode);
            }
        } catch (Throwable t) {
            logger.error("Error in getCustomerByPhoneNumber()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    public Document getZipsByCityState(String city, String state) throws Exception {
        Connection qConn = null;
        Document doc = null;
        logger.debug("getZipsByCityState(" + city + ", " + state + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            doc = orderEntryDAO.getZipsByCityStateAjax(qConn, city, state);
        } catch (Throwable t) {
            logger.error("Error in getZipsByCityState()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    @SuppressWarnings("rawtypes")
	public Document getPopularProducts(String zipCode, String deliveryDate, String countryId,
        String sourceCode, String dateRangeEnd) throws Exception {

        logger.debug("getPopularProducts(" + zipCode + ", " + deliveryDate + ", " + countryId + ", " + sourceCode + ")");

        Connection qConn = null;
        Document doc = null;

        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            int results = 99;
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            results = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                OrderEntryConstants.PRODUCT_SEARCH_RESULTS_TOTAL));

            CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
            cr.next();
            
            String priceHeaderId = cr.getString("price_header_id");
            String sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
            String companyId = cr.getString("company_id");

            //ProductAvailabilityBO pas = new ProductAvailabilityBO();
            ProductAvailVO[] paVOs = null;
            List<ProductAvailVO> paVOListFiltered = new ArrayList<ProductAvailVO>();
            SourceProductUtility spUtil = new SourceProductUtility();
            String productError = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Date delDate = null;
            
            if (dateRangeEnd == null || dateRangeEnd.equals("")) {

                if (deliveryDate != null && !deliveryDate.equals("")) {
                    delDate = sdf.parse(deliveryDate);
                }
                logger.info("Calling pas.getMostPopularProducts(" + deliveryDate + ", " + zipCode + ")");
                if (deliveryDate != null && deliveryDate.length() > 0 && zipCode != null && zipCode.length() > 0) {
                    paVOs = PASServiceUtil.getMostPopularProducts(delDate, zipCode, companyId, results);
                } else {
                	paVOs = PASServiceUtil.getMostPopularProducts(null, null, companyId, results);
                }
                logger.debug("size: " + paVOs.length);

            } else {

                try {

                    for (long i=Long.parseLong(deliveryDate); i<=Long.parseLong(dateRangeEnd); i++) {
                        String thisDeliveryDate = Long.toString(i);
                        logger.debug("Checking date range: " + thisDeliveryDate);
                        delDate = sdf.parse(thisDeliveryDate);
                        logger.debug("Calling pas.getMostPopularProducts(" + thisDeliveryDate + ", " + zipCode + ")");
                        paVOs = PASServiceUtil.getMostPopularProducts(delDate, zipCode, companyId, results);
                        logger.debug("size: " + paVOs.length);
                        if (paVOs.length > 0) break;
                    }
                    
                } catch (Exception e) {
                    logger.error("Date range error: " + e);
                }
            }

            if (paVOs.length <= 0) {
                doc = OEActionBase.createErrorResultSet("product-search", "No results found for the search criteria.");
            } 
            else
            {
              boolean domesticFlag = true;
              CachedResultSet crs = orderEntryDAO.getCountryMasterById(qConn, countryId);
              if (crs.next()) {
                  String countryType = crs.getString("oe_country_type");
                  if (countryType == null || countryType.equalsIgnoreCase("I")) {
                      domesticFlag = false;
                  }
              }

              //iterate thru and build a new paVOListFiltered by filtering thru the utility. 
              for (int i=0; i<paVOs.length; i++) 
              {
                ProductAvailVO paVO = paVOs[i];
                //If the Utility throws an Exception, still show the product to the user.  
                //If the user selects this product, FTDValidationBO will stop the user from adding it to the order
                productError = null;
                try
                {
                  productError = spUtil.getProductRestrictionMsg(qConn, sourceCode, paVO.getProductId(), true, domesticFlag?"D":"I");
                }
                catch(Exception e)
                {
                  logger.info("getPopularProducts - SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + paVO.getProductId() + ".  Error = " + e.toString()  );
                  productError = null;
                }
                if(productError == null || productError.length() == 0) 
                  paVOListFiltered.add(paVO);
                else
                  logger.info("getPopularProducts - SourceProductUtility filtered out sourceCode = " + sourceCode + " and productId = " + paVO.getProductId() + ".  Reason = " + productError);
              }
            
              if (paVOListFiltered.size() <= 0)
              {
                doc = OEActionBase.createErrorResultSet("product-search", "No results found for the search criteria for this source code.");
              } else {
                  doc = JAXPUtil.createDocument();
                  Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                  doc.appendChild(root);
                  JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                  JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");
  
                  Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                  rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product-search");
                  root.appendChild(rowset);
                  
                  Map iotwProducts = ProductUtil.getIOTWProducts(qConn, sourceCode);
  
                  for (int i=0; i<paVOListFiltered.size(); i++) {
                      ProductAvailVO thisVO = paVOListFiltered.get(i);
                      String productId = thisVO.getProductId();
                      logger.debug("productId: " + productId);
  
                      String[] result = (String[]) iotwProducts.get(productId);
                      if (result != null) {
  
                          String newSourceCode = result[0];
                          String newPriceHeaderId = result[1];
                          String productPageMessage = result[2];
                          String newSourceDiscountAllowedFlag = result[3];
                          String idddCount = result[4];
                          logger.debug("Found IOTW for " + productId + ": " + newSourceCode + " " + newPriceHeaderId + " " + productPageMessage);
                          if (idddCount != null) {
                              int deliveryCount = 0;
  
                              try {
                                  deliveryCount = Integer.parseInt(idddCount);
                              } catch (Exception e) {
                                  logger.error("Error converting IOTW delivery date count: " + idddCount);
                              }
  
                              if (deliveryCount > 0) {
                                  //If an IOTW has delivery date restrictions, don't return discount information
                                  logger.debug("IOTW has delivery date restrictions");
                                  newSourceCode = sourceCode;
                                  newSourceDiscountAllowedFlag = sourceDiscountAllowedFlag;
                                  newPriceHeaderId = priceHeaderId;
                              }
                          }
  
                          ProductUtil.createProductXML(qConn, doc, productId, newSourceCode,
                              newSourceDiscountAllowedFlag, newPriceHeaderId, i, null, productPageMessage);
  
                      } else {
  
                          ProductUtil.createProductXML(qConn, doc, productId, sourceCode,
                              sourceDiscountAllowedFlag, priceHeaderId, i, null, null);
  
                      }
                  }
              }
            }
            //logger.info(JAXPUtil.toString(doc));

        } catch (Throwable t) {
            logger.error("Error in getPopularProducts()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }
    
    public Document getCrossSellProducts(String productId, String sourceCode, String zipCode,
        String deliveryDate, String countryId, String dateRangeEnd) throws Exception {

        logger.debug("getCrossSellProducts(" + productId + ")");
        Document doc = null;
        Connection qConn = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
            doc.appendChild(root);
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

            if (sourceCode == null || sourceCode.equals("")) {
                doc = OEActionBase.createErrorResultSet("cross_sell_products", "Source code required.");
                return doc;
            }
            CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
            if (cr.next()) {
            
                String priceHeaderId = cr.getString("price_header_id");
                String sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                SourceProductUtility spUtil = new SourceProductUtility();
                String productError = null;

                boolean domesticFlag = true;
                CachedResultSet crs = orderEntryDAO.getCountryMasterById(qConn, countryId);
                if (crs.next()) {
                    String countryType = crs.getString("oe_country_type");
                    if (countryType == null || countryType.equalsIgnoreCase("I")) {
                        domesticFlag = false;
                    }
                }

                Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                rowset.setAttribute(OrderEntryConstants.TAG_NAME, "cross_sell_products");
                root.appendChild(rowset);

                int i = 0;
                cr = orderEntryDAO.getProductCrossSellAjax(qConn, productId);
                while (cr.next()) {
            
                    String crossSellProductId = cr.getString("cross_sell_product_id");
                    logger.debug("crossSellProductId: " + crossSellProductId);
                    
                    if (!OrderEntryConstants.INCLUDE_OVER_21_PRODUCTS) {
                        String crossSellOver21 = cr.getString("over_21");
                        if (crossSellOver21 == null || crossSellOver21.equalsIgnoreCase("Y")) {
                            logger.debug(crossSellProductId + " is flagged as Over 21. Removing");
                            crossSellProductId = null;
                        }
                    }

                    //If the Utility throws an Exception, still show the product to the user.  
                    //If the user selects this product, FTDValidationBO will stop the user from adding it to the order
                    if (crossSellProductId != null)
                    {
                      productError = null;
                      try
                      {
                        productError = spUtil.getProductRestrictionMsg(qConn, sourceCode, crossSellProductId, true, domesticFlag?"D":"I");
                      }
                      catch(Exception e)
                      {
                        logger.info("getCrossSellProducts - SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and crossSellProductId = " + crossSellProductId + ".  Error = " + e.toString()  );
                        productError = null;
                      }
                      if(productError != null && productError.length() > 0) 
                      {
                        logger.info("getCrossSellProducts - SourceProductUtility filtered out sourceCode = " + sourceCode + " and crossSellProductId = "  + crossSellProductId + ".  Reason = " + productError);
                        crossSellProductId = null;
                      }
                    }


                    if (zipCode != null && !zipCode.equals("") && deliveryDate != null && !deliveryDate.equals("")) {
                        Date delDate = sdf.parse(deliveryDate);
                        //ProductAvailabilityBO pas = new ProductAvailabilityBO();

                        if (crossSellProductId != null) {
                            ProductAvailVO paVO = PASServiceUtil.getProductAvailability(crossSellProductId, delDate, zipCode, countryId, null, sourceCode, false, true);
                            		//qConn, crossSellProductId, delDate, zipCode,null,sourceCode);
                            boolean isAvailable = paVO.isIsAvailable();
                            logger.info("crossSellProductId != null is Avail: " + isAvailable);
                            if (isAvailable) {
                                ProductUtil.createProductXML(qConn, doc, crossSellProductId, sourceCode,
                                    sourceDiscountAllowedFlag, priceHeaderId, i, null, null);
                                i += 1;
                            }
                        }
                    } else {
                        if (crossSellProductId != null) {
                            ProductUtil.createProductXML(qConn, doc, crossSellProductId, sourceCode,
                                sourceDiscountAllowedFlag, priceHeaderId, i, null, null);
                            i +=1;
                        }
                    }
                }
            } else {
                doc = OEActionBase.createErrorResultSet("cross_sell_products", "Invalid source code.");
            }
            //logger.info(JAXPUtil.toString(doc));

        } catch (Throwable t) {
            logger.error("Error in getCrossSellProducts()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }
    
    public Document sourceCodeSearch(String searchType, String keywords, String iotwSourceCode,
        String sourceCode, String productId, String priceHeaderId, String companyId) throws Exception {

        Document doc = null;

        if (searchType.equalsIgnoreCase("SOURCE_CODE")) {
            if (keywords == null || keywords.equals("")) {
                doc = OEActionBase.createErrorResultSet("search", "Keyword is required.");
            } else {
                SourceCodeSearchClient client = new SourceCodeSearchClient();
                List<SourceCodeSearchVO> voList = client.searchSourceCodes(keywords, companyId);
                if (voList.size() <= 0) {
                    doc = OEActionBase.createErrorResultSet("source_master", "No results found for the search criteria.");
                } else {
                    doc = JAXPUtil.createDocument();
                    Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                    doc.appendChild(root);
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                    Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                    rowset.setAttribute(OrderEntryConstants.TAG_NAME, "source_master");
                    root.appendChild(rowset);

                    for (int i=0; i<voList.size(); i++) {
                        SourceCodeSearchVO sVO = voList.get(i);
                        Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                        row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
                        rowset.appendChild(row);

                        Element value = doc.createElement("source_code");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getSourceCode()));

                        value = doc.createElement("description");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getDescription()));

                        value = doc.createElement("source_type");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getSourceType()));
                    }
                }
            }
        } else if (searchType.equalsIgnoreCase("IOTW")) {
            if ( (keywords == null || keywords.equals("")) &&
                (iotwSourceCode == null || iotwSourceCode.equals("")) &&
                (sourceCode == null || sourceCode.equals("")) &&
                (productId == null || productId.equals("")) &&
                (priceHeaderId == null || priceHeaderId.equals("")) ) {
                doc = OEActionBase.createErrorResultSet("search", "At least one data element is required.");
            } else {
                IOTWSearchClient client = new IOTWSearchClient();
                List<IOTWSearchVO> voList = client.searchIOTW(keywords, companyId, iotwSourceCode, sourceCode, productId, priceHeaderId);
                if (voList.size() <= 0) {
                    doc = OEActionBase.createErrorResultSet("iotw", "No results found for the search criteria.");
                } else {
                    doc = JAXPUtil.createDocument();
                    Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                    doc.appendChild(root);
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                    Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                    rowset.setAttribute(OrderEntryConstants.TAG_NAME, "iotw");
                    root.appendChild(rowset);

                    for (int i=0; i<voList.size(); i++) {
                        IOTWSearchVO sVO = voList.get(i);
                        Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                        row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
                        rowset.appendChild(row);

                        Element value = doc.createElement("product_id");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getProductId()));

                        value = doc.createElement("product_name");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getProductName()));

                        value = doc.createElement("product_description");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getProductLongDescription()));

                        value = doc.createElement("standard_price");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getStandardPrice()));

                        value = doc.createElement("source_code");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getSourceCode()));

                        value = doc.createElement("source_description");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getSourceCodeDescription()));

                        value = doc.createElement("source_type");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getSourceType()));

                        value = doc.createElement("iotw_source_code");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getIotwSourceCode()));

                        value = doc.createElement("iotw_source_description");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getIotwSourceCodeDescription()));

                        value = doc.createElement("iotw_source_type");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getIotwSourceType()));

                        value = doc.createElement("product_page_message");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getProductPageMessage()));

                        value = doc.createElement("discount");
                        row.appendChild(value);
                        value.appendChild(doc.createTextNode(sVO.getIotwSourceCodePriceHeaderId()));
                    }
                }
            }
        } else {
            doc = OEActionBase.createErrorResultSet("search", "Invalid search type");
        }

        return doc;
    }

    @SuppressWarnings("rawtypes")
	public Document productSearch(String keywords, String zipCode, String deliveryDate,
        String countryId, String sourceCode, String dateRangeEnd) throws Exception {

        logger.debug("productSearch(" + keywords + ", " + zipCode + ", " + deliveryDate + 
            ", " + countryId + ", " + sourceCode + ", " + dateRangeEnd + ")" );

        SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SOURCE_MASTER);
        SourceMasterVO scEnteredVO = handler.getSourceCodeById(sourceCode);
        logger.debug("For source code = " + sourceCode + ", related source code = " + scEnteredVO.getRelatedSourceCode() + 
                     " and order source = " + scEnteredVO.getOrderSource() + " and limit index flag = " + scEnteredVO.getSourceCodeHasLimitIndex());

        SourceMasterVO scRelatedVO = null;
        if (scEnteredVO.getRelatedSourceCode() != null && !scEnteredVO.getRelatedSourceCode().equals("") && scEnteredVO.getOrderSource().equalsIgnoreCase("P"))
        {
          scRelatedVO = handler.getSourceCodeById(scEnteredVO.getRelatedSourceCode());
          logger.debug("For source code = " + scRelatedVO.getSourceCode() + ", limit index flag = " + scRelatedVO.getSourceCodeHasLimitIndex());
        }


        Document doc = null;
        Connection qConn = null;
        
        if (deliveryDate != null && deliveryDate.equals("")) deliveryDate = null;
        if (zipCode != null && zipCode.equals("")) zipCode = null;
        if (keywords != null && keywords.equals("")) keywords = null;

        try {

            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            boolean domesticFlag = true;
            CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
            if (cr.next()) {
                String countryType = cr.getString("oe_country_type");
                if (countryType == null || countryType.equalsIgnoreCase("I")) {
                    domesticFlag = false;
                }
            }
        
            if (domesticFlag && deliveryDate != null && zipCode == null) {
                doc = OEActionBase.createErrorResultSet("product-search", "Zip code is required.");
            } else if (keywords == null && domesticFlag) {
                doc = getPopularProducts(zipCode, deliveryDate, countryId, sourceCode, dateRangeEnd);
            } else {

                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                int results = 99;
                results = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                OrderEntryConstants.PRODUCT_SEARCH_RESULTS_TOTAL));

                cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                cr.next();
                
                String companyId = cr.getString("company_id");
                String priceHeaderId = cr.getString("price_header_id");
                String sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");

                boolean filterAvailability = false;
                if (deliveryDate != null) filterAvailability = true;
                logger.debug("filterAvailability: " + filterAvailability);

                if(keywords != null) {
                    char chr[] = null;
                    chr = keywords.toCharArray();
                    StringBuffer sb = new StringBuffer();

                    for(int i=0; i<chr.length; i++)
                    {
                        if ((chr[i] >= '0' && chr[i] <= '9') || (chr[i] >= 'A' && chr[i] <= 'Z') || (chr[i] >= 'a' && chr[i] <= 'z')) {
                            sb.append(chr[i]);
                        } else {
                            sb.append(" ");
                        }
                    }
                    keywords = sb.toString();
                }
                logger.debug("keywords: " + keywords);

                Date formattedDate = null;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                ProductSearchClient client = new ProductSearchClient();
                List<ProductSearchVO> voList = null;

                String sourceCodeTemp = sourceCode;
                boolean bSourceCodeHasLimitIndex = false;
                String defaultDomain = null;
                
                if (scRelatedVO != null)
                {
                  sourceCodeTemp = scRelatedVO.getSourceCode(); 
                  if (scRelatedVO.getSourceCodeHasLimitIndex() != null && 
                      !scRelatedVO.getSourceCodeHasLimitIndex().equals("") && 
                      scRelatedVO. getSourceCodeHasLimitIndex().equalsIgnoreCase("Y")
                     )
                    bSourceCodeHasLimitIndex = true;
                }
                else 
                {
                  if (scEnteredVO.getSourceCodeHasLimitIndex() != null && 
                      !scEnteredVO.getSourceCodeHasLimitIndex().equals("") && 
                      scEnteredVO.getSourceCodeHasLimitIndex().equalsIgnoreCase("Y")
                     )
                    bSourceCodeHasLimitIndex = true;
                }
                logger.debug("ProductSearchClient.searchProducts - sourceCode = " + sourceCodeTemp + " and limit flag = " + bSourceCodeHasLimitIndex);
                
                // If the source code does not have a limit index or search is for international, 
                // retrieve the default domain to be used as OR source code in text search.
                if(!bSourceCodeHasLimitIndex || !domesticFlag) {
                    CacheUtil cacheUtil = CacheUtil.getInstance();
                    defaultDomain = cacheUtil.getSourceCodeDefaultDomain(sourceCodeTemp);
                }
                
                logger.debug("sourceDefaultDomain = " + defaultDomain);
                
                // Construct the visitable element passed to search.
                String deliveryType = domesticFlag?"D":"I";
                OrderEntryElementVO elemVO = new OrderEntryElementVO();
                elemVO.setSourceCode(sourceCode);
                elemVO.setSearchById(true);
                elemVO.setDeliveryType(deliveryType);
                elemVO.setConnection(qConn);

                if (dateRangeEnd == null || dateRangeEnd.equals("")) {

                    if (deliveryDate != null && !deliveryDate.equals("")) {
                        formattedDate = sdf.parse(deliveryDate);
                    }
                    voList = client.searchProducts(keywords, results, formattedDate,
                        zipCode, countryId, companyId, filterAvailability, 
                        OrderEntryConstants.INCLUDE_OVER_21_PRODUCTS, sourceCodeTemp, defaultDomain, elemVO);
                    logger.debug("size:" + voList.size());

                } else {

                    try {

                        for (long i=Long.parseLong(deliveryDate); i<=Long.parseLong(dateRangeEnd); i++) {
                            logger.debug("Checking date range: " + Long.toString(i));
                            formattedDate = sdf.parse(Long.toString(i));
                            voList = client.searchProducts(keywords, results, formattedDate,
                                zipCode, countryId, companyId, filterAvailability,
                                OrderEntryConstants.INCLUDE_OVER_21_PRODUCTS, sourceCodeTemp, defaultDomain, elemVO);
                            logger.debug("size:" + voList.size());
                            if (voList.size() > 0) break;
                        }
                        
                    } catch (Exception e) {
                        logger.error("Date range error: " + e);
                    }
                }

                if (voList.size() <= 0) {
                      doc = OEActionBase.createErrorResultSet("product-search", "No results found for the search criteria.");
                } else {
                      doc = JAXPUtil.createDocument();
                      Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                      doc.appendChild(root);
                      JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                      JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                      Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                      rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product-search");
                      root.appendChild(rowset);

                      String url = cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                          OrderEntryConstants.IMAGE_SERVER_URL);

                      Map iotwProducts = ProductUtil.getIOTWProducts(qConn, sourceCode);

                      for (int i=0; i<voList.size(); i++) {
                          ProductSearchVO sVO = voList.get(i);
                          ProductVO productVO = new ProductVO();

                          String productId = sVO.getProductId();
                          productVO.setProductId(productId);
                          productVO.setProductName(sVO.getProductName());
                          productVO.setNovatorName(sVO.getNovatorName());
                          productVO.setDiscountAllowedFlag(sVO.getDiscountAllowedFlag());
                          productVO.setStandardPrice(new BigDecimal(sVO.getStandardPrice()));
                          productVO.setDeluxePrice(new BigDecimal(sVO.getDeluxePrice()));
                          productVO.setPremiumPrice(new BigDecimal(sVO.getPremiumPrice()));
                          productVO.setShipMethodFlorist(sVO.getShipMethodFlorist());
                          productVO.setShipMethodCarrier(sVO.getShipMethodCarrier());
                          productVO.setShortDescription(sVO.getShortDescription());
                          productVO.setLongDescription(sVO.getLongDescription());
                          productVO.setSmallImage(url + "/" + sVO.getNovatorId() + "_a.jpg");
                          productVO.setLargeImage(url + "/" + sVO.getNovatorId() + "_c.jpg");
                          productVO.setPopularityOrderCount(sVO.getPopularity());
                          productVO.setAllowFreeShippingFlag(sVO.getAllowFreeShippingFlag());

                          String[] result = (String[]) iotwProducts.get(productId);
                          boolean foundIOTW = false;
                          String newSourceCode = null;
                          String newPriceHeaderId = null;
                          String productPageMessage = null;
                          String newSourceDiscountAllowedFlag = null;

                          if (result != null) {
                              foundIOTW = true;
                              newSourceCode = result[0];
                              newPriceHeaderId = result[1];
                              productPageMessage = result[2];
                              newSourceDiscountAllowedFlag = result[3];
                              String idddCount = result[4];
                              if (idddCount != null) {
                                  int deliveryCount = 0;

                                  try {
                                      deliveryCount = Integer.parseInt(idddCount);
                                  } catch (Exception e) {
                                      logger.error("Error converting IOTW delivery date count: " + idddCount);
                                  }

                                  if (deliveryCount > 0) {
                                      logger.debug("IOTW has delivery date restrictions");
                                      foundIOTW = false;
                                      if (deliveryDate != null) {
                                          String iotwId = result[5];
                                          cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                          boolean foundDate = true;
                                          formattedDate = sdf.parse(deliveryDate);

                                          while (cr.next()) {
                                              Date discountDate = cr.getDate("discount_date");
                                              if (discountDate.equals(formattedDate)) {
                                                  foundDate = true;
                                                  break;
                                              }
                                              foundDate = false;
                                          }

                                          if (foundDate) {
                                              logger.debug("Found IOTW delivery date");
                                              foundIOTW = true;
                                          }
                                      }
                                  }
                              }
                          }

                          if (foundIOTW) {
                              logger.debug("Found IOTW for " + productId + ": " + newSourceCode + " " + newPriceHeaderId + " " + productPageMessage);
                          } else {
                              newSourceCode = sourceCode;
                              newPriceHeaderId = priceHeaderId;
                              newSourceDiscountAllowedFlag = sourceDiscountAllowedFlag;
                          }

                          ProductUtil.createProductXML(qConn, doc, productId, newSourceCode,
                              newSourceDiscountAllowedFlag, newPriceHeaderId, i, productVO, productPageMessage);

                      }

                    }
                
            }
            //logger.info(JAXPUtil.toString(doc));

        } catch (Exception e) {
            logger.error(e);
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        return doc;
    }

    public Document floristSearch(String productId, String zipCode, String deliveryDate,
        String dateRangeEnd, String sourceCode) throws Exception {

        Document doc = null;
        Connection qConn = null;

        try {

            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

        	//ProductAvailabilityBO pas = new ProductAvailabilityBO();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Date delDate = null;
            String[] florists = null;

            if (dateRangeEnd == null || dateRangeEnd.equals("")) {

                if (deliveryDate != null && !deliveryDate.equals("")) {
                    delDate = sdf.parse(deliveryDate);
                }
                logger.debug("Calling pas.getFlorists(" + deliveryDate + ", " + zipCode + ")");
                florists = PASServiceUtil.getAvailableFlorists(delDate, zipCode);
                logger.debug("size:" + florists.length);

            } else {

                try {

                    for (long i=Long.parseLong(deliveryDate); i<=Long.parseLong(dateRangeEnd); i++) {
                        String thisDeliveryDate = Long.toString(i);
                        logger.debug("Checking date range: " + thisDeliveryDate);
                        delDate = sdf.parse(thisDeliveryDate);
                        logger.debug("Calling pas.getFlorists(" + thisDeliveryDate + ", " + zipCode + ")");
                        florists = PASServiceUtil.getAvailableFlorists(delDate, zipCode);
                        logger.debug("size:" + florists.length);
                        if (florists.length > 0) break;
                    }
                    
                } catch (Exception e) {
                    logger.error("Date range error: " + e);
                }
            }

            if (florists.length <= 0) {
                doc = OEActionBase.createErrorResultSet("product-search", "No results found for the search criteria.");
            } else {

                doc = JAXPUtil.createDocument();
                Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                doc.appendChild(root);
                JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                rowset.setAttribute(OrderEntryConstants.TAG_NAME, "florist-search");
                root.appendChild(rowset);

                ArrayList <FloristLookupVO>  floristSearchResult = orderEntryDAO.getFloristInfo(qConn, florists, sourceCode);
                for (int i = 0; i < floristSearchResult.size(); i++)
                {
                    FloristLookupVO floristLookupVO = floristSearchResult.get(i);
                    
                    Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                    row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
                    rowset.appendChild(row);

                    Element value = doc.createElement("florist_id");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getFloristId()));

                    value = doc.createElement("florist_name");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getFloristName()));

                    value = doc.createElement("address");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getFloristAddress()));

                    value = doc.createElement("phone_number");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getPhoneNumber()));

                    value = doc.createElement("mercury_flag");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getMercuryFlag()));

                    value = doc.createElement("florist_weight");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getFloristWeight()));

                    value = doc.createElement("goto_flag");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getGotoFlag()));

                    value = doc.createElement("sunday_delivery_flag");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getSundayFlag()));
                    
                    value = doc.createElement("priority");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(floristLookupVO.getPriority()));
                }
            }

        } catch (Throwable t) {
            logger.error("Error in floristSearch()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        return doc;
    }
    
    public Document institutionSearch(String city, String state, String addressType, String keyword) throws Exception {
        Document doc = null;

        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        int results = 99;
        try {
            results = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
            OrderEntryConstants.FACILITY_SEARCH_RESULTS));
        } catch (Exception e) {
            logger.error(e);
        }
        
        String newAddressType = "";
        if (addressType.equalsIgnoreCase("HOSPITAL")) {
            newAddressType = "H";
        } else if (addressType.equalsIgnoreCase("FUNERAL HOME")) {
            newAddressType = "F";
        } else if (addressType.equalsIgnoreCase("NURSING HOME")) {
            newAddressType = "N";
        }

        FacilitySearchClient client = new FacilitySearchClient();
        List<FacilitySearchVO> voList = client.searchFacilities(keyword, results, city, state, newAddressType);
        if (voList.size() <= 0) {
            doc = OEActionBase.createErrorResultSet("facilities", "No results found for the search criteria.");
        } else {
            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
            doc.appendChild(root);
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "facilities");
            root.appendChild(rowset);

            for (int i=0; i<voList.size(); i++) {
                FacilitySearchVO sVO = voList.get(i);
                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
                rowset.appendChild(row);

                Element value = doc.createElement("id");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getBusinessId()));

                value = doc.createElement("name");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getBusinessName()));

                value = doc.createElement("address");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getAddress()));

                value = doc.createElement("city");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getCity()));

                value = doc.createElement("state");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getState()));

                value = doc.createElement("zip_code");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getZipCode()));

                value = doc.createElement("phone_number");
                row.appendChild(value);
                value.appendChild(doc.createTextNode(sVO.getPhoneNumber()));
            }
        }
        return doc;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Document getDeliveryDates(String productId, String zipCode, String countryId, String addons,String source_code) throws Exception {
    	Document doc = JAXPUtil.createDocument();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Connection qConn = null;
        logger.info("getDeliveryDates(" + productId + ", " + zipCode + ", " + countryId + ", " + addons + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            Element resultNode = doc.createElement(OrderEntryConstants.TAG_RESULT);
        	resultNode.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
        	resultNode.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
        	
        	doc.appendChild(resultNode);
        	Element rs = doc.createElement(OrderEntryConstants.TAG_RS);
            rs.setAttribute(OrderEntryConstants.TAG_NAME, "product_delivery_dates");
            resultNode.appendChild(rs);

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            int days = 99;
            days = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
            OrderEntryConstants.DELIVERY_DROPDOWN_DAYS));
            
            List<String> addonList = null;
            if (addons != null && addons.length() > 0) {
                String[] addonString = addons.split(",");
                addonList = new ArrayList<String>(Arrays.asList(addonString));
            }

            Map deliveryDates = new HashMap();
            //ProductAvailabilityBO pas = new ProductAvailabilityBO();
            ProductAvailVO[] paVOs = null;

            String countryType = null;
            CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
            if (cr.next()) {
                countryType = cr.getString("oe_country_type");
            }
            if (countryType != null && countryType.equalsIgnoreCase("D")) {
            	List<ProductAvailVO> productAvailVOs = PASServiceUtil.getProductAvailableDates(productId, addonList, zipCode, countryId, source_code, days);
                if(productAvailVOs != null && productAvailVOs.size() > 0) {
                	paVOs = productAvailVOs.toArray(new ProductAvailVO[productAvailVOs.size()]);
    			}
            } else {
                paVOs = PASServiceUtil.getInternationalProductAvailableDates(productId, countryId, days);              
            }
            
            if(paVOs != null && paVOs.length > 0) {
	            for (ProductAvailVO paVO : paVOs) {
	            	Date pasDeliveryDate = paVO.getDeliveryDate();
	            	logger.debug("available: " + paVO.isIsAvailable() + " " + sdf.format(pasDeliveryDate.getTime()));            	
	            	
	            	if (paVO.getFloristCutoffDate() != null) {
	            	    logger.debug("florist: " + sdf.format(paVO.getFloristCutoffDate()) + " " +
	            	    		paVO.getFloristCutoffTime());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, true);
	            	}            	
	            	
	            	if (paVO.getShipDateND() != null) {
	            	    logger.debug("ND: " + sdf.format(paVO.getShipDateND()) + " " +
	            	    		paVO.getShipDateNDCutoff());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, false);
	            	}
	            	
	            	if (paVO.getShipDate2D() != null) {
	            	    logger.debug("2D: " + sdf.format(paVO.getShipDate2D()) + " " +
	            	    		paVO.getShipDate2DCutoff());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, false);
	            	}
	            	
	            	if (paVO.getShipDateGR() != null) {
	            	    logger.debug("GR: " + sdf.format(paVO.getShipDateGR()) + " " +
	            	    		paVO.getShipDateGRCutoff());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, false);
	            	}            	
	            	
	            	if (paVO.getShipDateSA() != null) {
	            	    logger.debug("SA: " + sdf.format(paVO.getShipDateSA()) + " " +
	            	    		paVO.getShipDateSACutoff());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, false);
	            	}            	
	            	
	            	if (paVO.getShipDateSU() != null) {
	            	    logger.debug("SU: " + sdf.format(paVO.getShipDateSU()) + " " +
	            	    		paVO.getShipDateSUCutoff());
	            	    addToDateMap(deliveryDates, pasDeliveryDate, false);
	            	}
	            }
            }
            
            Map sortedMap = new TreeMap(deliveryDates);
            for (Object key: sortedMap.keySet()) {
                String name = (String)key;
                String value = (String)deliveryDates.get(key);
                
                Element deliveryDate = doc.createElement("delivery_date");
                rs.appendChild(deliveryDate);
                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(name));
                deliveryDate.appendChild(date);
                Element displayValue = doc.createElement("displayDate");
                displayValue.appendChild(doc.createTextNode(value));
                deliveryDate.appendChild(displayValue);
            }
            rs.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
            rs.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
        	
            logger.debug(JAXPUtil.toString(doc));
            
        } catch (Exception e) {
        	logger.error(e);
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        return doc;
    }
    
    public Document getUpdateOrderInfo(String externalOrderNumber) throws Exception {
    	Document doc = JAXPUtil.createDocument();
        //SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Connection qConn = null;
        logger.info("getUpdateOrderInfo(" + externalOrderNumber + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            doc = orderEntryDAO.getUpdateOrderInfo(qConn, externalOrderNumber);
            
            Document tempDoc = JAXPUtil.createDocument();
            Node newNode = tempDoc.importNode(doc.getFirstChild(), true);
            tempDoc.appendChild(newNode);
            Element element = (Element) tempDoc.getElementsByTagName("cc_number").item(0);
            if (element != null) element.getParentNode().removeChild(element);
            logger.info(JAXPUtil.toString(tempDoc));
            
        } catch (Exception e) {
        	logger.error(e);
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        return doc;
    }

    public Document getMembershipIdInfo(String zipCode) throws Exception {
    	Document doc = JAXPUtil.createDocument();
      
        Connection qConn = null;
        logger.info("getMembershipIdInfo(" + zipCode + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            doc = orderEntryDAO.getMembershipIdInfo(qConn, zipCode);
            
        } catch (Exception e) {
        	logger.error(e);
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        return doc;
    }

   public void setResourceProvider(IResourceProvider resourceProvider) {
        FTDLookupBO.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public void setOrderEntryDAO(OrderEntryDAO orderEntryDAO) {
        FTDLookupBO.orderEntryDAO = orderEntryDAO;
    }

    public OrderEntryDAO getOrderEntryDAO() {
        return orderEntryDAO;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private static void addToDateMap(Map dateMap, Date deliveryDate, boolean isFlorist) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
        String tempDate = sdf2.format(deliveryDate);

        if (dateMap.get(tempDate) == null) {
    		String value = sdf.format(deliveryDate);
    		if (!isFlorist) {
    			value += " (DS only)";
    		}
    		dateMap.put(tempDate, value);
    		logger.debug(tempDate + " " + value + " added to map");
    	}
    	
    }


    public String getPromotionInfo(String sourcecode) throws Exception {
         String json = null;
         CachedResultSet pinfo = null;
         CachedResultSet iotw  = null;
         Connection qConn = null;
         PromotionsVO promoVo = new PromotionsVO();
         PartnerVO partnerVo  = new PartnerVO();
         IotwProductsVO iotwVo = null;
         List<IotwProductsVO> iotwList = null;
         logger.info("getPromotionInfo(" + sourcecode + ")");
         long startTime;
         
         try {
             startTime = (new Date()).getTime();
             String dataSource = OrderEntryConstants.DATASOURCE;
             qConn = resourceProvider.getDatabaseConnection(dataSource);
             if (qConn == null) {
                 throw new Exception("Unable to connect to database");
             }
             pinfo = orderEntryDAO.getPromotionsInfoAjax(qConn, sourcecode);
             iotw = orderEntryDAO.getPromotionsIotwAjax(qConn, sourcecode);
             
             if (iotw != null) {
                iotwList = new ArrayList();
                while (iotw.next()) {
                   iotwVo = new IotwProductsVO();
                   iotwVo.setFtdProduct(iotw.getString("novator_id"));
                   iotwVo.setDiscountAmount(iotw.getString("discount_amt"));
                   iotwVo.setDiscountType(iotw.getString("discount_type"));
                   iotwVo.setStartDate(iotw.getString("start_date"));
                   iotwVo.setEndDate(iotw.getString("end_date"));
                   iotwVo.setIotwAssociatedCode(iotw.getString("iotw_source_code"));
                   iotwVo.setDiscountDescription(iotw.getString("discount_description"));
                   String ddates = iotw.getString("discount_dates");
                   if (ddates != null && !ddates.isEmpty()) {
                      iotwVo.setDiscountDeliveryDates(Arrays.asList(ddates.split("\\s*,\\s*")));
                   }
                   iotwList.add(iotwVo);
                }                
             }
             if (pinfo != null) {
                while (pinfo.next()) {
                   promoVo.setCode(sourcecode);
                   promoVo.setDiscountDescription(pinfo.getString("discount_description"));
                   promoVo.setDescription(pinfo.getString("description"));
                   promoVo.setSourceType(pinfo.getString("source_type"));
                   promoVo.setDiscountAmount(pinfo.getString("discount_amt"));
                   promoVo.setDiscountType(pinfo.getString("discount_type"));
                   promoVo.setStatus(pinfo.getString("available"));   // ??? TODO
                   promoVo.setStartDate(pinfo.getString("start_date"));
                   promoVo.setEndDate(pinfo.getString("end_date"));
                   promoVo.setIotwFlag(pinfo.getString("iotw_flag"));
                   promoVo.setIotwProducts(iotwList);
                   
                   partnerVo.setName(pinfo.getString("partner_name"));
                   partnerVo.setType(pinfo.getString("program_name"));
                   // ??? TODO 
                   promoVo.setPartner(partnerVo);
                   
                   if (iotwList != null) {
                      promoVo.setIotwProducts(iotwList);
                   }
                }
             }
             Gson gson = new Gson();
             json = gson.toJson(promoVo);

             // Log to service table
             logServiceResponseTracking(qConn, sourcecode, "PromoInfo", startTime);
                          
         } catch (Exception e) {
          logger.error(e);
         } finally {
             if( qConn!=null ) {
                 try {
                     if( !qConn.isClosed() ) {
                         qConn.close();
                     }
                 } catch (SQLException sqle) {
                     logger.warn("Error closing database connection",sqle);
                 }
             }
         }
         return json;
     }

    
    private void logServiceResponseTracking(Connection con, String requestId, String serviceMethod, long startTime) {      
       try {
          long responseTime = System.currentTimeMillis() - startTime;
          ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
          srtVO.setServiceName(FRESH_SERVICE_PROMO_PULL);
          srtVO.setServiceMethod(serviceMethod);
          srtVO.setTransactionId(requestId);
          srtVO.setResponseTime(responseTime);
          srtVO.setCreatedOn(new Date());
          ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
          srtUtil.insert(con, srtVO);
       } catch (Exception e) {
          logger.error("Error in logServiceResponseTracking " + e.getMessage());
       }
     }
    
}
