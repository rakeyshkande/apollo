package com.ftd.oe.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.avs.webservice.Address;
import com.ftd.avs.webservice.AddressVerificationService;
import com.ftd.avs.webservice.DevaluationReason;
import com.ftd.avs.webservice.VerificationRequest;
import com.ftd.avs.webservice.VerificationResponse;
import com.ftd.avs.webservice.VerifiedAddress;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.interfaces.ValidationBO;
import com.ftd.oe.util.MembershipUtil;
import com.ftd.oe.util.OrderUtil;
import com.ftd.oe.util.ProductUtil;
import com.ftd.oe.util.ValidationUtil;
import com.ftd.oe.util.WebServiceClientFactory;
import com.ftd.oe.vo.fresh.CategoriesVO;
import com.ftd.oe.vo.fresh.FreshProdInfoVO;
import com.ftd.oe.vo.fresh.FreshProductVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.ProductVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.pas.core.domain.ProductAvailVO;
import com.ftd.security.SecurityManager;


/**
 * This class validateses data for the Order Entry application and returns it as
 * an XML formatted document.
 *
 * @author Tim Schmig -
 */

public class FTDValidationBO implements ValidationBO
{
    private static Logger logger = new Logger("com.ftd.oe.bo.FTDValidationBO");
    private static IResourceProvider resourceProvider;
    private static OrderEntryDAO orderEntryDAO;
    private static String CERTIFICATE_VALUE = "CERTIFICATE#";
    private static String DROPDOWN = "DROPDOWN";
    private static String STATIC = "STATIC";
    private static String DEFAULT_FRESH_SOURCECODE = "352";   // ??? TODO - Should not be hard-coded!
    private static int FRESH_CATEGORY_MAX_DEPTH = 5;
    private static String FRESH_PRODUCT_LOOKUP_ERROR   = "ProdInfoUnavail";
    private static String FRESH_PRODUCT_LOOKUP_SUCCESS = "Y";
    private static String FRESH_SERVICE_PRODUCT_PULL = "Fresh Pull";

    /**
     * constructor
     */
    public FTDValidationBO()
    {
    }

    public Document validateDnis(String dnisId, String secToken) throws Exception {
        Connection qConn = null;
        Document doc = null;;
        logger.debug("validateDnis(" + dnisId + ")");
        
        if (!ValidationUtil.fieldValidation(dnisId, true, false, null)) {
            doc = OEActionBase.createErrorResultSet("dnis", "DNIS is invalid. Please verify your phone display!");
        } else {
            try {
                String dataSource = OrderEntryConstants.DATASOURCE;
                qConn = resourceProvider.getDatabaseConnection(dataSource);
                if (qConn == null) {
                    throw new Exception("Unable to connect to database");
                }
                doc = orderEntryDAO.getDnisAjax(qConn, dnisId);

                NodeList nl = doc.getElementsByTagName("default_source_code");
                if (nl.getLength() > 0) {
                    Element sourceCode = (Element) nl.item(0);
                    if (sourceCode.hasChildNodes()) {
                        Node n = sourceCode.getFirstChild();
                        String value = n.getNodeValue();
                        getBillingInfo(qConn, value, doc);
                        getPartnerData(qConn, secToken, doc);
                    }
                }

                // add a timestamp node to the result document
                NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                if (node.getLength() > 0) {
                    Element resultNode = (Element) node.item(0);
                    Element timeNode = doc.createElement("order-timestamp");
                    Date timestamp = new Date();
                    long newTime = timestamp.getTime();
                    timeNode.appendChild(doc.createTextNode(String.valueOf(newTime)));
                    resultNode.appendChild(timeNode);
                }
            } catch (Throwable t) {
                logger.error("Error in validateDnis()",t);
                throw new Exception("Could not validate DNIS.");
            } finally {
                if( qConn!=null ) {
                    try {
                        if( !qConn.isClosed() ) {
                            qConn.close();
                        }
                    } catch (SQLException sqle) {
                        logger.warn("Error closing database connection",sqle);
                    }
                }
            }
        }
    
        return doc;
    }

    public Document validateSourceCode(String sourceCode, String dnisId, String secToken) throws Exception {
        Connection qConn = null;
        Document doc = null;
        if (sourceCode != null) sourceCode = sourceCode.toUpperCase();
        logger.debug("validateSourceCode(" + sourceCode + ", " + dnisId + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            doc = orderEntryDAO.getSourceCodeAjax(qConn, sourceCode, dnisId);
            if (ValidationUtil.validateIOTWSourceCode(doc)) {
                doc = OEActionBase.createErrorResultSet("source_master", "Cannot use an IOTW source code.");
            }
            getBillingInfo(qConn, sourceCode, doc);
            getPartnerData(qConn, secToken, doc);
            
        } catch (Throwable t) {
            logger.error("Error in validateSourceCode()",t);
            throw new Exception("Could not validate source code.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }
    
    public Document validateStateByZipcodeAndCity(String zipCode, String city) throws Exception {
    	Connection qConn = null;
        Document doc = null;
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            if(city == null || city.trim().equals("")){
            	city = "";
            }
            doc = orderEntryDAO.getStateByZipcodeAndCityAjax(qConn, zipCode, city);
        } catch (Throwable t) {
            logger.error("Error in validateStateByZipcodeAndCity()",t);
            throw new Exception("Could not validate state code.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    	
    }
    
    public Document validateZipCode(String zipCode) throws Exception {
        Connection qConn = null;
        Document doc = null;
        if ( zipCode != null ) zipCode = zipCode.toUpperCase();
        logger.debug("validateZipCode(" + zipCode + ")");

        // Only use first 3 characters for Canadian Zips
        if ( zipCode != null && zipCode.length() > 3 &&
            (Character.isLetter(zipCode.charAt(0))
                && Character.isDigit(zipCode.charAt(1))
                && Character.isLetter(zipCode.charAt(2)))) {

            zipCode = zipCode.substring(0,3);
            logger.debug("Changing Canadian zip code to: " + zipCode);
        } else {
            if ( zipCode != null && zipCode.length() > 5 ) {
                zipCode = zipCode.substring(0,5);
                logger.debug("Changing zip code to: " + zipCode);
            }
        }
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            doc = orderEntryDAO.getZipCodeAjax(qConn, zipCode);
        } catch (Throwable t) {
            logger.error("Error in validateZipCode()",t);
            throw new Exception("Could not validate zip code.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Document validateProduct(String productId, String sourceCode, String countryId) throws Exception {
        Connection qConn = null;
        Document doc = null;
        boolean isUpsellMaster = false;
        logger.info("validateProduct(" + productId + ", " + sourceCode + ", " + countryId + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            doc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode);
            if (doc != null) logger.debug(JAXPUtil.toString(doc));

            ProductVO productVO = ProductUtil.parseProductXML(doc);
            if (productVO != null && productId != null && !(productId.equalsIgnoreCase(productVO.getProductId())) &&
                productId.equalsIgnoreCase(productVO.getNovatorId())) {
                logger.debug("productId " + productId + " changed to " + productVO.getProductId());
                productId = productVO.getProductId();
            }

            boolean domesticFlag = true;
            boolean stillValid = true;
            String status = productVO.getStatus();
            logger.debug("status: " + status);
            logger.debug("hasSubcodes: " + productVO.isHasSubcodes());
            logger.debug("hasUpsells: " + productVO.isHasUpsells());

            if (status == null) {
                doc = OEActionBase.createErrorResultSet("product_details", "Product not on file");
                stillValid = false;
            } else if (status != null && !status.equalsIgnoreCase("A")) {
                doc = OEActionBase.createErrorResultSet("product_details", "This product is unavailable");
                Element rowset = JAXPUtil.selectSingleNode(doc,"/result/rs[@name=\"product_details\"]");
                rowset.setAttribute("product_status","U");
                stillValid = false;
            } else {
                String weboeBlocked = productVO.getWeboeBlocked();
                if (weboeBlocked != null && weboeBlocked.equalsIgnoreCase("Y")) {
                    doc = OEActionBase.createErrorResultSet("product_details", "This product is not available for phone orders.");
                    stillValid = false;
                } else {
                    CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
                    if (cr.next()) {
                        String countryType = cr.getString("oe_country_type");
                        if (countryType == null || countryType.equalsIgnoreCase("I")) {
                            domesticFlag = false;
                        }
                        String productType = productVO.getDeliveryType();
                        if (countryType == null || productType == null) {
                            doc = OEActionBase.createErrorResultSet("product_details", "Invalid country/product data");
                            stillValid = false;
                        } else {
                            boolean countryFailed=false;
                            if (countryType.equalsIgnoreCase("D") && !productType.equalsIgnoreCase("D")) {
                                doc = OEActionBase.createErrorResultSet("product_details", 
                                    "International products cannot be delivered to a domestic country.");
                                stillValid = false;
                                countryFailed = true;
                            } else {
                                if (countryType.equalsIgnoreCase("I") && !productType.equalsIgnoreCase("I")) {
                                    doc = OEActionBase.createErrorResultSet("product_details", 
                                        "Domestic products cannot be delivered to an international country.");
                                    stillValid = false;
                                    countryFailed = true;
                                }
                            }
                            
                            if( countryFailed ) {
                                Element rowset = JAXPUtil.selectSingleNode(doc, "/result/rs[@name=\"product_details\"]");
                                rowset.setAttribute("country_failed", "Y");
                            }
                        }
                    } else {
                        doc = OEActionBase.createErrorResultSet("product_details", "Bad country: " + countryId);
                        stillValid = false;
                    }
                }
            }

            //Validate product / upsell detail id attributes
            SourceProductUtility spUtil = new SourceProductUtility();

            if ( (productVO.getProductId() == null || productVO.getProductId().equalsIgnoreCase("")) && productVO.isHasUpsells())
              isUpsellMaster = true;

            logger.debug("product passed in = " + productId + " and productVO.getProductId = " + productVO.getProductId() + " and productVO.isHasUpsells = " + productVO.isHasUpsells() + " and isUpsellMaster = " + isUpsellMaster);

            if (productVO.isHasUpsells())
            {
              //Prior to 7229, JOE worked as follows:
                  //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                  //if productVO.isHasUpsells() and there exists at least one valid upsellDetailId
                      //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, upsellDetailId, sourceCode) at least once - GUI needs product (in this case = upsell detail id) details
              //Changes implemented with 7229:
                  //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                  //if productVO.isHasUpsells()
                      //create a list of valid and invalid upsellDetailIds by calling SourceProductUtility
                      //if there is at least one valid upsellDetailIds
                          //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, validUpsellDetailId, sourceCode, commaSeparatedValidUpsellDetailIds)
                            //Note --> validUpsellDetailId = validUpsellList.get(0)
              List upsellList = productVO.getUpsellDetailIds();
              List validUpsellList = new ArrayList(); 
              List invalidUpsellList = new ArrayList(); 
              String upsellDetailId = null;
              String upsellProductError = null;
              String productError = null; 

              for (int i=0; i<upsellList.size(); i++) 
              {
                upsellProductError = null; 
                upsellDetailId = (String) upsellList.get(i);
                //If the Utility throws an Exception, still show the product to the user. 
                try
                {
                  upsellProductError = spUtil.getProductRestrictionMsg(qConn, sourceCode, upsellDetailId, true, domesticFlag?"D":"I");
                  logger.debug("Upsells - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  upsellDetailId + ") returned -- " + upsellProductError);
                }
                catch(Exception e)
                {
                  logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + upsellDetailId + ".  Error = " + e.toString()  );
                  upsellProductError = null; 
                }
                //if utility returns an error, this upsell is considered invalid
                if (upsellProductError != null && upsellProductError.length() > 0)
                {
                  if (upsellDetailId.equalsIgnoreCase(productId))
                    productError = upsellProductError;
                  invalidUpsellList.add(upsellDetailId);
                }
                else
                {
                  validUpsellList.add(upsellDetailId);
                }
              }
              logger.debug("Upsells - validUpsellList.size = " + validUpsellList.size());
              if (productError != null && productError.length() > 0)
              {
                doc = OEActionBase.createErrorResultSet("product_details", productError);
                stillValid = false;
              }
              else if (validUpsellList.size()>0)
              {
                StringBuffer commaSeparatedValidUpsellDetailIds = new StringBuffer(); 
                for (int i = 0; i < validUpsellList.size(); i++)
                {
                  commaSeparatedValidUpsellDetailIds.append("'");
                  commaSeparatedValidUpsellDetailIds.append((String)validUpsellList.get(i));
                  commaSeparatedValidUpsellDetailIds.append("'");
                  commaSeparatedValidUpsellDetailIds.append(",");
                }
                
                // Remove the last comma
                if(commaSeparatedValidUpsellDetailIds.length() > 0)
                {
                  commaSeparatedValidUpsellDetailIds.deleteCharAt(commaSeparatedValidUpsellDetailIds.length()-1);
                }

                if (isUpsellMaster)
                  doc = orderEntryDAO.getProductDetailsAjax(qConn, (String)validUpsellList.get(0), sourceCode, commaSeparatedValidUpsellDetailIds.toString());
                else
                  doc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode, commaSeparatedValidUpsellDetailIds.toString());
              }
              else 
              {
                doc = OEActionBase.createErrorResultSet("product_details", "Product not available for this source code");
                stillValid = false;
              }
            }
            else 
            {
              if (stillValid)
              {
                //If the Utility throws an Exception, do not let the user select this product
                String productError = null; 
                try
                {
                  productError = spUtil.getProductRestrictionMsg(qConn, sourceCode, productId, true, domesticFlag?"D":"I");
                  logger.debug("ProductId - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  productId + ") returned -- " + productError);
                }
                catch(Exception e)
                {
                  logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + productId + ".  Error = " + e.toString()  );
                  productError = "Product not available for this source code"; 
                }
                if(productError != null && productError.length() > 0) {
                    doc = OEActionBase.createErrorResultSet("product_details", productError);
                    stillValid = false;
                }
              }
            }
            
            if (doc != null) logger.debug(JAXPUtil.toString(doc));
            
            boolean foundSourceCode = false;
            String priceHeaderId = "";
            String sourceDiscountAllowedFlag = "";
            String iotwFlag = "";

            CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
            
            if (cr.next()) {
                priceHeaderId = cr.getString("price_header_id");
                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                iotwFlag = cr.getString("iotw_flag");
                foundSourceCode = true;
            }

            if (stillValid) {

                Map iotwProducts = ProductUtil.getIOTWProducts(qConn, sourceCode);

                if (foundSourceCode) {
                    String iotwId = null;
                    String idddCount = null;

                    String originalSourceCode = sourceCode;
                    String originalPriceHeaderId = priceHeaderId;
                    String originalSourceDiscountAllowedFlag = sourceDiscountAllowedFlag;

                    String[] result = (String[]) iotwProducts.get(productId);
                    if (result != null) {
                        sourceCode = result[0];
                        productVO.setProductPageMessage(result[2]);
                        idddCount = result[4];
                        iotwId = result[5];
                        logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + productVO.getProductPageMessage());
                        cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                        if (cr.next()) {
                            priceHeaderId = cr.getString("price_header_id");
                            sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                            iotwFlag = cr.getString("iotw_flag");
                        }
                    }

                    String discountAllowedFlag = productVO.getDiscountAllowedFlag();
                    if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                        discountAllowedFlag = "Y";
                    }

                    if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                        OrderDetailVO odVO = new OrderDetailVO();
                        odVO.setItemSourceCode(sourceCode);
                        odVO.setDeliveryDate(new Date());
                        
                        MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                    } else {
                        ProductUtil.calcDiscount(qConn, priceHeaderId, productVO, discountAllowedFlag, true);
                    }

                    Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                    rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product_discounts");
                    rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                    rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                    
                    Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                    row.setAttribute(OrderEntryConstants.TAG_ROW, "1");
                    rowset.appendChild(row);
                    
                    Element value = doc.createElement("iotw-flag");
                    value.setAttribute("flag", iotwFlag);
                    row.appendChild(value);
                    //value.appendChild(doc.createTextNode(iotwFlag));

                    if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {

                        Element iotwValue = doc.createElement("product-page-message");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createCDATASection(productVO.getProductPageMessage()));

                        iotwValue = doc.createElement("item-source-code");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(sourceCode));

                        iotwValue = doc.createElement("master-source-code");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                        iotwValue = doc.createElement("discount-type");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                        iotwValue = doc.createElement("standard-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                        iotwValue = doc.createElement("deluxe-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                        iotwValue = doc.createElement("premium-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                        iotwValue = doc.createElement("delivery-dates");
                        value.appendChild(iotwValue);

                        if (idddCount != null) {
                            int deliveryCount = 0;

                            try {
                                deliveryCount = Integer.parseInt(idddCount);
                            } catch (Exception e) {
                                logger.error("Error converting IOTW delivery date count: " + idddCount);
                            }

                            if (deliveryCount > 0) {
                                logger.debug("IOTW has delivery date restrictions");

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                int cnt=0;
                                while (cr.next()) {
                                    Date discountDate = cr.getDate("discount_date");
                                    cnt = cnt + 1;
                                    Element dateValue = doc.createElement("date");
                                    dateValue.setAttribute("row", Integer.toString(cnt));
                                    iotwValue.appendChild(dateValue);
                                    dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                }

                            }
                        }

                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                            OrderDetailVO odVO = new OrderDetailVO();
                            odVO.setItemSourceCode(originalSourceCode);
                            odVO.setDeliveryDate(new Date());
                            
                            MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                        } else {
                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                discountAllowedFlag = "Y";
                            }
                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, productVO, discountAllowedFlag, true);
                        }

                    }

                    value = doc.createElement("discount");
                    row.appendChild(value);

                    Element discountValue = doc.createElement("standard-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                    discountValue = doc.createElement("deluxe-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                    discountValue = doc.createElement("premium-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                    discountValue = doc.createElement("discount-type");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                    NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element) node.item(0);
                        resultNode.appendChild(rowset);
                    }

                    if (productVO.isHasSubcodes() || productVO.isHasUpsells()) {
                        
                        NodeList nl = doc.getElementsByTagName(OrderEntryConstants.TAG_RS);
                        for (int i=0; i<nl.getLength(); i++) {
                            Element rsElement = (Element) nl.item(i);
                            String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);
                            if (nodeName != null && nodeName.equalsIgnoreCase("product_subcodes") && rsElement.hasChildNodes()) {
                                NodeList nlRS = rsElement.getChildNodes();
                                for (int j=0; j<nlRS.getLength(); j++) {
                                    Element record = (Element) nlRS.item(j);
                                    NodeList nlRecord = record.getChildNodes();
                                    String subcodeProductId = null;
                                    String subcodePrice = null;
                                    for (int k=0; k<nlRecord.getLength(); k++) {
                                        Element subcodeDetails = (Element) nlRecord.item(k);
                                        String name = subcodeDetails.getNodeName();
                                        String elementValue = "";
                                        if (subcodeDetails.hasChildNodes()) {
                                            Node n = subcodeDetails.getFirstChild();
                                            elementValue = n.getNodeValue();
                                        }
                                        if (name.equalsIgnoreCase("product_id")) {
                                            subcodeProductId = elementValue;
                                        } else if (name.equalsIgnoreCase("subcode_price")) {
                                            subcodePrice = elementValue;
                                       }
                                    }
                                    if (subcodeProductId != null && subcodePrice != null) {
                                        ProductVO subcodeVO = new ProductVO();
                                        subcodeVO.setProductId(subcodeProductId);
                                        subcodeVO.setStandardPrice(new BigDecimal(subcodePrice));
                                        subcodeVO.setDeluxePrice(new BigDecimal(0));
                                        subcodeVO.setPremiumPrice(new BigDecimal(0));

                                        sourceCode = originalSourceCode;
                                        priceHeaderId = originalPriceHeaderId;
                                        sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                        iotwFlag = "N";
                                        idddCount = null;

                                        result = (String[]) iotwProducts.get(subcodeProductId);
                                        if (result != null) {
                                            sourceCode = result[0];
                                            subcodeVO.setProductPageMessage(result[2]);
                                            idddCount = result[4];
                                            iotwId = result[5];
                                            logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + subcodeVO.getProductPageMessage());
                                            cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                            if (cr.next()) {
                                                priceHeaderId = cr.getString("price_header_id");
                                                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                iotwFlag = cr.getString("iotw_flag");
                                            }
                                        }

                                        discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                        if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                            discountAllowedFlag = "Y";
                                        }

                                        if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(sourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                        } else {
                                            ProductUtil.calcDiscount(qConn, priceHeaderId, subcodeVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("iotw-flag");
                                        value.setAttribute("flag", iotwFlag);
                                        record.appendChild(value);

                                        if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                            
                                            Element iotwValue = doc.createElement("product-page-message");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createCDATASection(subcodeVO.getProductPageMessage()));

                                            iotwValue = doc.createElement("item-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(sourceCode));

                                            iotwValue = doc.createElement("master-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                            iotwValue = doc.createElement("discount-type");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                            iotwValue = doc.createElement("discount-price");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                            iotwValue = doc.createElement("delivery-dates");
                                            value.appendChild(iotwValue);

                                            if (idddCount != null) {
                                                int deliveryCount = 0;

                                                try {
                                                    deliveryCount = Integer.parseInt(idddCount);
                                                } catch (Exception e) {
                                                    logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                }

                                                if (deliveryCount > 0) {
                                                    logger.debug("IOTW has delivery date restrictions");

                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                    cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                    int cnt=0;
                                                    while (cr.next()) {
                                                        Date discountDate = cr.getDate("discount_date");
                                                        cnt = cnt + 1;
                                                        Element dateValue = doc.createElement("date");
                                                        dateValue.setAttribute("row", Integer.toString(cnt));
                                                        iotwValue.appendChild(dateValue);
                                                        dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                    }

                                                }
                                            }
                                        }

                                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(originalSourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                        } else {
                                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                discountAllowedFlag = "Y";
                                            }
                                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, subcodeVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("discount");
                                        record.appendChild(value);

                                        discountValue = doc.createElement("discount-type");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                        discountValue = doc.createElement("discount-price");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                    }
                                }
                            } else if (nodeName != null && nodeName.equalsIgnoreCase("product_upsells") && rsElement.hasChildNodes()) {
                                NodeList nlRS = rsElement.getChildNodes();
                                for (int j=0; j<nlRS.getLength(); j++) {
                                    Element record = (Element) nlRS.item(j);
                                    NodeList nlRecord = record.getChildNodes();
                                    String upsellProductId = null;
                                    String upsellPrice = null;
                                    for (int k=0; k<nlRecord.getLength(); k++) {
                                        Element upsellDetails = (Element) nlRecord.item(k);
                                        String name = upsellDetails.getNodeName();
                                        String elementValue = "";
                                        if (upsellDetails.hasChildNodes()) {
                                            Node n = upsellDetails.getFirstChild();
                                            elementValue = n.getNodeValue();
                                        }
                                        if (name.equalsIgnoreCase("upsell_detail_id")) {
                                            upsellProductId = elementValue;
                                        } else if (name.equalsIgnoreCase("upsell_price")) {
                                            upsellPrice = elementValue;
                                       }
                                    }
                                    if (upsellProductId != null && upsellPrice != null) {
                                        ProductVO upsellVO = new ProductVO();
                                        upsellVO.setProductId(upsellProductId);
                                        upsellVO.setStandardPrice(new BigDecimal(upsellPrice));
                                        upsellVO.setDeluxePrice(new BigDecimal(0));
                                        upsellVO.setPremiumPrice(new BigDecimal(0));

                                        sourceCode = originalSourceCode;
                                        priceHeaderId = originalPriceHeaderId;
                                        sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                        iotwFlag = "N";
                                        idddCount = null;

                                        result = (String[]) iotwProducts.get(upsellProductId);
                                        if (result != null) {
                                            sourceCode = result[0];
                                            upsellVO.setProductPageMessage(result[2]);
                                            idddCount = result[4];
                                            iotwId = result[5];
                                            logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + upsellVO.getProductPageMessage());
                                            cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                            if (cr.next()) {
                                                priceHeaderId = cr.getString("price_header_id");
                                                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                iotwFlag = cr.getString("iotw_flag");
                                            }
                                        }

                                        discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                        if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                            discountAllowedFlag = "Y";
                                        }

                                        if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(sourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                        } else {
                                            ProductUtil.calcDiscount(qConn, priceHeaderId, upsellVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("iotw-flag");
                                        value.setAttribute("flag", iotwFlag);
                                        record.appendChild(value);

                                        if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                            
                                            Element iotwValue = doc.createElement("product-page-message");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createCDATASection(upsellVO.getProductPageMessage()));

                                            iotwValue = doc.createElement("item-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(sourceCode));

                                            iotwValue = doc.createElement("master-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                            iotwValue = doc.createElement("discount-type");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                            iotwValue = doc.createElement("discount-price");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));

                                            iotwValue = doc.createElement("delivery-dates");
                                            value.appendChild(iotwValue);

                                            if (idddCount != null) {
                                                int deliveryCount = 0;

                                                try {
                                                    deliveryCount = Integer.parseInt(idddCount);
                                                } catch (Exception e) {
                                                    logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                }

                                                if (deliveryCount > 0) {
                                                    logger.debug("IOTW has delivery date restrictions");

                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                    cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                    int cnt=0;
                                                    while (cr.next()) {
                                                        Date discountDate = cr.getDate("discount_date");
                                                        cnt = cnt + 1;
                                                        Element dateValue = doc.createElement("date");
                                                        dateValue.setAttribute("row", Integer.toString(cnt));
                                                        iotwValue.appendChild(dateValue);
                                                        dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                    }

                                                }
                                            }
                                        }

                                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(originalSourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                        } else {
                                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                discountAllowedFlag = "Y";
                                            }
                                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, upsellVO, discountAllowedFlag, true);
                                        }
                                        
                                        value = doc.createElement("discount");
                                        record.appendChild(value);

                                        discountValue = doc.createElement("discount-type");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                        discountValue = doc.createElement("discount-price");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));

                                    }
                                }
                            }

                        }

                    }
                }
                
                // get all available delivery dates by ship method
                /*
                if (productVO.getShipMethodCarrier() != null && productVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {
                    ProductUtil.getAvailableShipDatesXML(doc, productId, null);
                }
                */

            }

        } catch (Throwable t) {
            logger.error("Error in validateProduct()",t);
            throw new Exception("Could not retrieve product details.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    
    

    
   /**
    * This is a duplicate of validateProduct method.  It was copied and modified specifically for use with Groupon.
    * It should be considered a temporary method and should be replaced with a more permanent solution in the future.
    * 
    * @param productId
    * @param sourceCode
    * @param countryId
    * @return
    * @throws Exception
    */
    @SuppressWarnings({ "rawtypes", "unchecked" })
   public Document validateProductGroupon(String productId, String inSourcecode, String countryId) throws Exception {
        Connection qConn = null;
        Document doc = null;
        String sourceCode = inSourcecode;
        boolean isUpsellMaster = false;
        logger.info("validateProductGroupon(" + productId + ", " + sourceCode + ", " + countryId + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            // First get Groupon (limit_products) sourcecodes for the product along with their discounted prices.
            // Later in this method we'll add to document.  We call now since we need to grab a source code.
            //
            CachedResultSet gcr = orderEntryDAO.getGrouponSourcecodesAjax(qConn, productId);

            // If passed-in sourcecode parameter was not specified, then default to Groupon sourcecode from above query.
            // If there were multiple source codes for the product, we just arbitrarily pick one (i.e., the first one).
            //
            if (sourceCode == null || sourceCode.isEmpty()) { 
               if (gcr != null) {
                  while (gcr.next()) {
                     sourceCode = gcr.getString("source_code");
                     break;
                  }
                  gcr.reset();  // Reset since we will use gcr again later     
                  logger.info("Defaulting to Groupon sourcecode: " + sourceCode); 
               }
            }
            
            doc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode);
            if (doc != null) logger.debug(JAXPUtil.toString(doc));

            ProductVO productVO = ProductUtil.parseProductXML(doc);
            if (productVO != null && productId != null && !(productId.equalsIgnoreCase(productVO.getProductId())) &&
                productId.equalsIgnoreCase(productVO.getNovatorId())) {
                logger.debug("productId " + productId + " changed to " + productVO.getProductId());
                productId = productVO.getProductId();
            }

            boolean domesticFlag = true;
            boolean stillValid = true;
            String status = productVO.getStatus();
            logger.debug("status: " + status);
            logger.debug("hasSubcodes: " + productVO.isHasSubcodes());
            logger.debug("hasUpsells: " + productVO.isHasUpsells());

            if (status == null) {
                doc = OEActionBase.createErrorResultSet("product_details", "Product not on file");
                stillValid = false;
            } else if (status != null && !status.equalsIgnoreCase("A")) {
                doc = OEActionBase.createErrorResultSet("product_details", "This product is unavailable");
                Element rowset = JAXPUtil.selectSingleNode(doc,"/result/rs[@name=\"product_details\"]");
                rowset.setAttribute("product_status","U");
                stillValid = false;
            } else {
                String weboeBlocked = productVO.getWeboeBlocked();
                if (weboeBlocked != null && weboeBlocked.equalsIgnoreCase("Y")) {
                    doc = OEActionBase.createErrorResultSet("product_details", "This product is not available for phone orders.");
                    stillValid = false;
                } else {
                    CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
                    if (cr.next()) {
                        String countryType = cr.getString("oe_country_type");
                        if (countryType == null || countryType.equalsIgnoreCase("I")) {
                            domesticFlag = false;
                        }
                        String productType = productVO.getDeliveryType();
                        if (countryType == null || productType == null) {
                            doc = OEActionBase.createErrorResultSet("product_details", "Invalid country/product data");
                            stillValid = false;
                        } else {
                            boolean countryFailed=false;
                            if (countryType.equalsIgnoreCase("D") && !productType.equalsIgnoreCase("D")) {
                                doc = OEActionBase.createErrorResultSet("product_details", 
                                    "International products cannot be delivered to a domestic country.");
                                stillValid = false;
                                countryFailed = true;
                            } else {
                                if (countryType.equalsIgnoreCase("I") && !productType.equalsIgnoreCase("I")) {
                                    doc = OEActionBase.createErrorResultSet("product_details", 
                                        "Domestic products cannot be delivered to an international country.");
                                    stillValid = false;
                                    countryFailed = true;
                                }
                            }
                            
                            if( countryFailed ) {
                                Element rowset = JAXPUtil.selectSingleNode(doc, "/result/rs[@name=\"product_details\"]");
                                rowset.setAttribute("country_failed", "Y");
                            }
                        }
                    } else {
                        doc = OEActionBase.createErrorResultSet("product_details", "Bad country: " + countryId);
                        stillValid = false;
                    }
                }
            }

            //Validate product / upsell detail id attributes
            SourceProductUtility spUtil = new SourceProductUtility();

            if ( (productVO.getProductId() == null || productVO.getProductId().equalsIgnoreCase("")) && productVO.isHasUpsells())
              isUpsellMaster = true;

            logger.debug("product passed in = " + productId + " and productVO.getProductId = " + productVO.getProductId() + " and productVO.isHasUpsells = " + productVO.isHasUpsells() + " and isUpsellMaster = " + isUpsellMaster);

            if (productVO.isHasUpsells())
            {
              //Prior to 7229, JOE worked as follows:
                  //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                  //if productVO.isHasUpsells() and there exists at least one valid upsellDetailId
                      //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, upsellDetailId, sourceCode) at least once - GUI needs product (in this case = upsell detail id) details
              //Changes implemented with 7229:
                  //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                  //if productVO.isHasUpsells()
                      //create a list of valid and invalid upsellDetailIds by calling SourceProductUtility
                      //if there is at least one valid upsellDetailIds
                          //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, validUpsellDetailId, sourceCode, commaSeparatedValidUpsellDetailIds)
                            //Note --> validUpsellDetailId = validUpsellList.get(0)
              List upsellList = productVO.getUpsellDetailIds();
              List validUpsellList = new ArrayList(); 
              List invalidUpsellList = new ArrayList(); 
              String upsellDetailId = null;
              String upsellProductError = null;
              String productError = null; 

              for (int i=0; i<upsellList.size(); i++) 
              {
                upsellProductError = null; 
                upsellDetailId = (String) upsellList.get(i);
                //If the Utility throws an Exception, still show the product to the user. 
                try
                {
                  upsellProductError = spUtil.getProductRestrictionMsg(qConn, sourceCode, upsellDetailId, true, domesticFlag?"D":"I");
                  logger.debug("Upsells - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  upsellDetailId + ") returned -- " + upsellProductError);
                }
                catch(Exception e)
                {
                  logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + upsellDetailId + ".  Error = " + e.toString()  );
                  upsellProductError = null; 
                }
                //if utility returns an error, this upsell is considered invalid
                if (upsellProductError != null && upsellProductError.length() > 0)
                {
                  if (upsellDetailId.equalsIgnoreCase(productId))
                    productError = upsellProductError;
                  invalidUpsellList.add(upsellDetailId);
                }
                else
                {
                  validUpsellList.add(upsellDetailId);
                }
              }
              logger.debug("Upsells - validUpsellList.size = " + validUpsellList.size());
              if (productError != null && productError.length() > 0)
              {
                doc = OEActionBase.createErrorResultSet("product_details", productError);
                stillValid = false;
              }
              else if (validUpsellList.size()>0)
              {
                StringBuffer commaSeparatedValidUpsellDetailIds = new StringBuffer(); 
                for (int i = 0; i < validUpsellList.size(); i++)
                {
                  commaSeparatedValidUpsellDetailIds.append("'");
                  commaSeparatedValidUpsellDetailIds.append((String)validUpsellList.get(i));
                  commaSeparatedValidUpsellDetailIds.append("'");
                  commaSeparatedValidUpsellDetailIds.append(",");
                }
                
                // Remove the last comma
                if(commaSeparatedValidUpsellDetailIds.length() > 0)
                {
                  commaSeparatedValidUpsellDetailIds.deleteCharAt(commaSeparatedValidUpsellDetailIds.length()-1);
                }

                if (isUpsellMaster)
                  doc = orderEntryDAO.getProductDetailsAjax(qConn, (String)validUpsellList.get(0), sourceCode, commaSeparatedValidUpsellDetailIds.toString());
                else
                  doc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode, commaSeparatedValidUpsellDetailIds.toString());
              }
              else 
              {
                doc = OEActionBase.createErrorResultSet("product_details", "Product not available for this source code");
                stillValid = false;
              }
            }
            else 
            {
              if (stillValid)
              {
                //If the Utility throws an Exception, do not let the user select this product
                String productError = null; 
                try
                {
                  productError = spUtil.getProductRestrictionMsg(qConn, sourceCode, productId, true, domesticFlag?"D":"I");
                  logger.debug("ProductId - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  productId + ") returned -- " + productError);
                }
                catch(Exception e)
                {
                  logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + productId + ".  Error = " + e.toString()  );
                  productError = "Product not available for this source code"; 
                }
                if(productError != null && productError.length() > 0) {
                    doc = OEActionBase.createErrorResultSet("product_details", productError);
                    stillValid = false;
                }
              }
            }
            
            if (doc != null) logger.debug(JAXPUtil.toString(doc));
            
            boolean foundSourceCode = false;
            String priceHeaderId = "";
            String sourceDiscountAllowedFlag = "";
            String iotwFlag = "";

            CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
            
            if (cr.next()) {
                priceHeaderId = cr.getString("price_header_id");
                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                iotwFlag = cr.getString("iotw_flag");
                foundSourceCode = true;
            }

            if (stillValid) {

                Map iotwProducts = ProductUtil.getIOTWProducts(qConn, sourceCode);

                if (foundSourceCode) {
                    String iotwId = null;
                    String idddCount = null;

                    String originalSourceCode = sourceCode;
                    String originalPriceHeaderId = priceHeaderId;
                    String originalSourceDiscountAllowedFlag = sourceDiscountAllowedFlag;

                    String[] result = (String[]) iotwProducts.get(productId);
                    if (result != null) {
                        sourceCode = result[0];
                        productVO.setProductPageMessage(result[2]);
                        idddCount = result[4];
                        iotwId = result[5];
                        logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + productVO.getProductPageMessage());
                        cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                        if (cr.next()) {
                            priceHeaderId = cr.getString("price_header_id");
                            sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                            iotwFlag = cr.getString("iotw_flag");
                        }
                    }

                    String discountAllowedFlag = productVO.getDiscountAllowedFlag();
                    if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                        discountAllowedFlag = "Y";
                    }

                    if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                        OrderDetailVO odVO = new OrderDetailVO();
                        odVO.setItemSourceCode(sourceCode);
                        odVO.setDeliveryDate(new Date());
                        
                        MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                    } else {
                        ProductUtil.calcDiscount(qConn, priceHeaderId, productVO, discountAllowedFlag, true);
                    }

                    Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                    rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product_discounts");
                    rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                    rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                    
                    Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                    row.setAttribute(OrderEntryConstants.TAG_ROW, "1");
                    rowset.appendChild(row);
                    
                    Element value = doc.createElement("iotw-flag");
                    value.setAttribute("flag", iotwFlag);
                    row.appendChild(value);
                    //value.appendChild(doc.createTextNode(iotwFlag));

                    if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {

                        Element iotwValue = doc.createElement("product-page-message");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createCDATASection(productVO.getProductPageMessage()));

                        iotwValue = doc.createElement("item-source-code");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(sourceCode));

                        iotwValue = doc.createElement("master-source-code");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                        iotwValue = doc.createElement("discount-type");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                        iotwValue = doc.createElement("standard-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                        iotwValue = doc.createElement("deluxe-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                        iotwValue = doc.createElement("premium-price");
                        value.appendChild(iotwValue);
                        iotwValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                        iotwValue = doc.createElement("delivery-dates");
                        value.appendChild(iotwValue);

                        if (idddCount != null) {
                            int deliveryCount = 0;

                            try {
                                deliveryCount = Integer.parseInt(idddCount);
                            } catch (Exception e) {
                                logger.error("Error converting IOTW delivery date count: " + idddCount);
                            }

                            if (deliveryCount > 0) {
                                logger.debug("IOTW has delivery date restrictions");

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                int cnt=0;
                                while (cr.next()) {
                                    Date discountDate = cr.getDate("discount_date");
                                    cnt = cnt + 1;
                                    Element dateValue = doc.createElement("date");
                                    dateValue.setAttribute("row", Integer.toString(cnt));
                                    iotwValue.appendChild(dateValue);
                                    dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                }

                            }
                        }

                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                            OrderDetailVO odVO = new OrderDetailVO();
                            odVO.setItemSourceCode(originalSourceCode);
                            odVO.setDeliveryDate(new Date());
                            
                            MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                        } else {
                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                discountAllowedFlag = "Y";
                            }
                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, productVO, discountAllowedFlag, true);
                        }

                    }

                    value = doc.createElement("discount");
                    row.appendChild(value);

                    Element discountValue = doc.createElement("standard-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                    discountValue = doc.createElement("deluxe-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                    discountValue = doc.createElement("premium-price");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                    discountValue = doc.createElement("discount-type");
                    value.appendChild(discountValue);
                    discountValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                    NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element) node.item(0);
                        resultNode.appendChild(rowset);
                    }

                    if (productVO.isHasSubcodes() || productVO.isHasUpsells()) {
                        
                        NodeList nl = doc.getElementsByTagName(OrderEntryConstants.TAG_RS);
                        for (int i=0; i<nl.getLength(); i++) {
                            Element rsElement = (Element) nl.item(i);
                            String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);
                            if (nodeName != null && nodeName.equalsIgnoreCase("product_subcodes") && rsElement.hasChildNodes()) {
                                NodeList nlRS = rsElement.getChildNodes();
                                for (int j=0; j<nlRS.getLength(); j++) {
                                    Element record = (Element) nlRS.item(j);
                                    NodeList nlRecord = record.getChildNodes();
                                    String subcodeProductId = null;
                                    String subcodePrice = null;
                                    for (int k=0; k<nlRecord.getLength(); k++) {
                                        Element subcodeDetails = (Element) nlRecord.item(k);
                                        String name = subcodeDetails.getNodeName();
                                        String elementValue = "";
                                        if (subcodeDetails.hasChildNodes()) {
                                            Node n = subcodeDetails.getFirstChild();
                                            elementValue = n.getNodeValue();
                                        }
                                        if (name.equalsIgnoreCase("product_id")) {
                                            subcodeProductId = elementValue;
                                        } else if (name.equalsIgnoreCase("subcode_price")) {
                                            subcodePrice = elementValue;
                                       }
                                    }
                                    if (subcodeProductId != null && subcodePrice != null) {
                                        ProductVO subcodeVO = new ProductVO();
                                        subcodeVO.setProductId(subcodeProductId);
                                        subcodeVO.setStandardPrice(new BigDecimal(subcodePrice));
                                        subcodeVO.setDeluxePrice(new BigDecimal(0));
                                        subcodeVO.setPremiumPrice(new BigDecimal(0));

                                        sourceCode = originalSourceCode;
                                        priceHeaderId = originalPriceHeaderId;
                                        sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                        iotwFlag = "N";
                                        idddCount = null;

                                        result = (String[]) iotwProducts.get(subcodeProductId);
                                        if (result != null) {
                                            sourceCode = result[0];
                                            subcodeVO.setProductPageMessage(result[2]);
                                            idddCount = result[4];
                                            iotwId = result[5];
                                            logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + subcodeVO.getProductPageMessage());
                                            cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                            if (cr.next()) {
                                                priceHeaderId = cr.getString("price_header_id");
                                                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                iotwFlag = cr.getString("iotw_flag");
                                            }
                                        }

                                        discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                        if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                            discountAllowedFlag = "Y";
                                        }

                                        if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(sourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                        } else {
                                            ProductUtil.calcDiscount(qConn, priceHeaderId, subcodeVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("iotw-flag");
                                        value.setAttribute("flag", iotwFlag);
                                        record.appendChild(value);

                                        if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                            
                                            Element iotwValue = doc.createElement("product-page-message");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createCDATASection(subcodeVO.getProductPageMessage()));

                                            iotwValue = doc.createElement("item-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(sourceCode));

                                            iotwValue = doc.createElement("master-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                            iotwValue = doc.createElement("discount-type");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                            iotwValue = doc.createElement("discount-price");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                            iotwValue = doc.createElement("delivery-dates");
                                            value.appendChild(iotwValue);

                                            if (idddCount != null) {
                                                int deliveryCount = 0;

                                                try {
                                                    deliveryCount = Integer.parseInt(idddCount);
                                                } catch (Exception e) {
                                                    logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                }

                                                if (deliveryCount > 0) {
                                                    logger.debug("IOTW has delivery date restrictions");

                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                    cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                    int cnt=0;
                                                    while (cr.next()) {
                                                        Date discountDate = cr.getDate("discount_date");
                                                        cnt = cnt + 1;
                                                        Element dateValue = doc.createElement("date");
                                                        dateValue.setAttribute("row", Integer.toString(cnt));
                                                        iotwValue.appendChild(dateValue);
                                                        dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                    }

                                                }
                                            }
                                        }

                                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(originalSourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                        } else {
                                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                discountAllowedFlag = "Y";
                                            }
                                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, subcodeVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("discount");
                                        record.appendChild(value);

                                        discountValue = doc.createElement("discount-type");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                        discountValue = doc.createElement("discount-price");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                    }
                                }
                            } else if (nodeName != null && nodeName.equalsIgnoreCase("product_upsells") && rsElement.hasChildNodes()) {
                                NodeList nlRS = rsElement.getChildNodes();
                                for (int j=0; j<nlRS.getLength(); j++) {
                                    Element record = (Element) nlRS.item(j);
                                    NodeList nlRecord = record.getChildNodes();
                                    String upsellProductId = null;
                                    String upsellPrice = null;
                                    for (int k=0; k<nlRecord.getLength(); k++) {
                                        Element upsellDetails = (Element) nlRecord.item(k);
                                        String name = upsellDetails.getNodeName();
                                        String elementValue = "";
                                        if (upsellDetails.hasChildNodes()) {
                                            Node n = upsellDetails.getFirstChild();
                                            elementValue = n.getNodeValue();
                                        }
                                        if (name.equalsIgnoreCase("upsell_detail_id")) {
                                            upsellProductId = elementValue;
                                        } else if (name.equalsIgnoreCase("upsell_price")) {
                                            upsellPrice = elementValue;
                                       }
                                    }
                                    if (upsellProductId != null && upsellPrice != null) {
                                        ProductVO upsellVO = new ProductVO();
                                        upsellVO.setProductId(upsellProductId);
                                        upsellVO.setStandardPrice(new BigDecimal(upsellPrice));
                                        upsellVO.setDeluxePrice(new BigDecimal(0));
                                        upsellVO.setPremiumPrice(new BigDecimal(0));

                                        sourceCode = originalSourceCode;
                                        priceHeaderId = originalPriceHeaderId;
                                        sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                        iotwFlag = "N";
                                        idddCount = null;

                                        result = (String[]) iotwProducts.get(upsellProductId);
                                        if (result != null) {
                                            sourceCode = result[0];
                                            upsellVO.setProductPageMessage(result[2]);
                                            idddCount = result[4];
                                            iotwId = result[5];
                                            logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + upsellVO.getProductPageMessage());
                                            cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                            if (cr.next()) {
                                                priceHeaderId = cr.getString("price_header_id");
                                                sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                iotwFlag = cr.getString("iotw_flag");
                                            }
                                        }

                                        discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                        if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                            discountAllowedFlag = "Y";
                                        }

                                        if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(sourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                        } else {
                                            ProductUtil.calcDiscount(qConn, priceHeaderId, upsellVO, discountAllowedFlag, true);
                                        }

                                        value = doc.createElement("iotw-flag");
                                        value.setAttribute("flag", iotwFlag);
                                        record.appendChild(value);

                                        if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                            
                                            Element iotwValue = doc.createElement("product-page-message");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createCDATASection(upsellVO.getProductPageMessage()));

                                            iotwValue = doc.createElement("item-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(sourceCode));

                                            iotwValue = doc.createElement("master-source-code");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                            iotwValue = doc.createElement("discount-type");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                            iotwValue = doc.createElement("discount-price");
                                            value.appendChild(iotwValue);
                                            iotwValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));

                                            iotwValue = doc.createElement("delivery-dates");
                                            value.appendChild(iotwValue);

                                            if (idddCount != null) {
                                                int deliveryCount = 0;

                                                try {
                                                    deliveryCount = Integer.parseInt(idddCount);
                                                } catch (Exception e) {
                                                    logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                }

                                                if (deliveryCount > 0) {
                                                    logger.debug("IOTW has delivery date restrictions");

                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                    cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                    int cnt=0;
                                                    while (cr.next()) {
                                                        Date discountDate = cr.getDate("discount_date");
                                                        cnt = cnt + 1;
                                                        Element dateValue = doc.createElement("date");
                                                        dateValue.setAttribute("row", Integer.toString(cnt));
                                                        iotwValue.appendChild(dateValue);
                                                        dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                    }

                                                }
                                            }
                                        }

                                        if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                            OrderDetailVO odVO = new OrderDetailVO();
                                            odVO.setItemSourceCode(originalSourceCode);
                                            odVO.setDeliveryDate(new Date());
                                            
                                            MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                        } else {
                                            discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                            if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                discountAllowedFlag = "Y";
                                            }
                                            ProductUtil.calcDiscount(qConn, originalPriceHeaderId, upsellVO, discountAllowedFlag, true);
                                        }
                                        
                                        value = doc.createElement("discount");
                                        record.appendChild(value);

                                        discountValue = doc.createElement("discount-type");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                        discountValue = doc.createElement("discount-price");
                                        value.appendChild(discountValue);
                                        discountValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));

                                    }
                                }
                            }

                        }

                    }
                }
                
                // get all available delivery dates by ship method
                /*
                if (productVO.getShipMethodCarrier() != null && productVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {
                    ProductUtil.getAvailableShipDatesXML(doc, productId, null);
                }
                */

                
                // Special node just for Groupon listing all (limit_products) sourcecodes for the product along with their discounted prices.
                //
                Element scMainNode = null; 
                String scode = null;
                if (gcr != null) {   // We already obtained gcr way at top of this nasty little method
                   scMainNode = doc.createElement("source_codes");
                   Element scNode = null;
                   Element scCode = null;
                   Element scPrice = null;
                   while (gcr.next()) {
                      scode = gcr.getString("source_code");
                      
                      String discountType  = gcr.getString("discount_type");
                      String discountAmt   = gcr.getString("discount_amt");
                      String standardPrice = gcr.getString("standard_price");
                      String discountedPrice = ProductUtil.calcDiscountGroupon(discountType, discountAmt, standardPrice);  

                      scNode = doc.createElement("source_code");
                      scMainNode.appendChild(scNode);
                      scCode  = doc.createElement("code");
                      scPrice = doc.createElement("discountedprice");
                      scNode.appendChild(scCode);                         
                      scNode.appendChild(scPrice);                         
                      scCode.appendChild(doc.createTextNode(scode));                         
                      scPrice.appendChild(doc.createTextNode(discountedPrice));                         
                   }
                   //if (cr.getRowCount() == 1) {
                   // Always add a dummy source code.
                   // We do this since if only one sourcecode found, the upstream conversion to JSON 
                   // will result in an object, but the consumer of the JSON expects it to be a list.  
                   // Having more than one element ensures a list is generated.
                   scNode = doc.createElement("source_code");
                   scMainNode.appendChild(scNode);
                   scCode  = doc.createElement("code");
                   scPrice = doc.createElement("discountedprice");
                   scNode.appendChild(scCode);                         
                   scNode.appendChild(scPrice);                         
                   scCode.appendChild(doc.createTextNode("dummy"));                         
                   scPrice.appendChild(doc.createTextNode("0.00")); 
                   //}
                }
                if (scMainNode != null) {
                   NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                   if (node.getLength() > 0) {
                       Element resultNode = (Element) node.item(0);
                       resultNode.appendChild(scMainNode);
                   }
                }
                
            }

        } catch (Throwable t) {
            logger.error("Error in validateProduct()",t);
            throw new Exception("Could not retrieve product details.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }
    
    
    
    /**
     * This too is a duplicate of validateProduct method.  It was copied and modified specifically for use with Project Fresh.
     * It should be considered a temporary method and should be replaced with a more permanent solution in the future.
     * Same was done for Groupon, but this is an on-going work-in-progress for Fresh and most likely will result in a better solution.
     * At that point the Groupon specifics may be able to be incorporated (and a better home found for this logic).
     * 
     * @param productId
     * @param sourceCode
     * @param countryId
     * @return
     * @throws Exception
     */
     @SuppressWarnings({ "rawtypes", "unchecked" })
    public FreshProdInfoVO validateProductFresh(String productId, String inSourcecode, String countryId) throws Exception {
         Connection qConn = null;
         Document doc = null;
         String sourceCode = null;
         boolean isUpsellMaster = false;
         boolean isProdStatusA = false;
         FreshProdInfoVO freshProdInfoVo = null;
         long startTime;
         
         try {
             startTime = (new Date()).getTime();
             if (inSourcecode == null || inSourcecode.isEmpty()) {
                sourceCode = DEFAULT_FRESH_SOURCECODE;    // ??? TODO - Should not be hard-coded
             } else {
                sourceCode = inSourcecode;
             }
             logger.info("validateProductFresh(" + productId + ", " + sourceCode + ", " + countryId + ")");

             String dataSource = OrderEntryConstants.DATASOURCE;
             qConn = resourceProvider.getDatabaseConnection(dataSource);
             if (qConn == null) {
                 throw new Exception("Unable to connect to database");
             }
             
             /* ??? TODO 
              * 
              * OLD WAY
             doc = orderEntryDAO.getProductDetailsFresh(qConn, productId, sourceCode);
             //if (doc != null) logger.debug(JAXPUtil.toString(doc));

             ProductVO productVO = ProductUtil.parseProductXML(doc);
             if (productVO != null && productId != null && !(productId.equalsIgnoreCase(productVO.getProductId())) &&
                 productId.equalsIgnoreCase(productVO.getNovatorId())) {
                 logger.debug("productId " + productId + " changed to " + productVO.getProductId());
                 productId = productVO.getProductId();
             }
             */
             
             
             // Get product details
             //
             doc = orderEntryDAO.getProductDetailsFresh(qConn, productId, sourceCode);
                        
             // Populate POJOs
             //
             freshProdInfoVo = ProductUtil.parseFreshProductXML(doc);
             freshProdInfoVo.setStatus(FRESH_PRODUCT_LOOKUP_SUCCESS);
             FreshProductVO fpvo = freshProdInfoVo.getProduct_details();
             String apid = fpvo.getProduct_id();
             String status = fpvo.getStatus();
             boolean stillValid = true;
             if (status == null) {
                freshProdInfoVo = createFreshErrorResultSet("Product not on file");
                stillValid = false;
             } else {
                if (!status.equalsIgnoreCase("A")) {
                   isProdStatusA = false;
               } else {
                   isProdStatusA = true;
               }               
             }

             // Get product categories
             //
             if (isProdStatusA && stillValid) {
                String upsellMaster = fpvo.getUpsell_master_prod_id();
                if (upsellMaster != null && !upsellMaster.isEmpty()) {
                   // If product was upsell master, then use it to retrieve category info
                   apid = upsellMaster;
                }
                CachedResultSet fcr = orderEntryDAO.getProductCategoriesFresh(qConn, apid, null);
                if (fcr != null) {
                   List<CategoriesVO> csList = ProductUtil.parseFreshCategories(fcr);
                   freshProdInfoVo.setCategories(csList);
                }
             }
              
             /*
             boolean domesticFlag = true;
             boolean stillValid = true;
             String status = productVO.getStatus();
             logger.info("status: " + status);
             logger.debug("hasSubcodes: " + productVO.isHasSubcodes());
             logger.debug("hasUpsells: " + productVO.isHasUpsells());

             if (status == null) {
                 freshProdInfoVo = createFreshErrorResultSet("Product not on file");
                 stillValid = false;
             } else {
                 //String weboeBlocked = productVO.getWeboeBlocked();
                 //if (weboeBlocked != null && weboeBlocked.equalsIgnoreCase("Y")) {
                 //    doc = createFreshErrorResultSet("This product is not available for phone orders.");
                 //    stillValid = false;
                 if (!status.equalsIgnoreCase("A")) {
                     //doc = createFreshErrorResultSet("This product is unavailable");
                     //stillValid = false;
                     isProdStatusA = false;
                 } else {
                     isProdStatusA = true;
                     CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
                     if (cr.next()) {
                         String countryType = cr.getString("oe_country_type");
                         if (countryType == null || countryType.equalsIgnoreCase("I")) {
                             domesticFlag = false;
                         }
                         String productType = productVO.getDeliveryType();
                         if (countryType == null || productType == null) {
                             freshProdInfoVo = createFreshErrorResultSet("Invalid country/product data");
                             stillValid = false;
                         } else {
                             if (countryType.equalsIgnoreCase("D") && !productType.equalsIgnoreCase("D")) {
                                freshProdInfoVo = createFreshErrorResultSet("International products cannot be delivered to a domestic country.");
                             } else {
                                 if (countryType.equalsIgnoreCase("I") && !productType.equalsIgnoreCase("I")) {
                                     freshProdInfoVo = createFreshErrorResultSet("Domestic products cannot be delivered to an international country.");
                                     stillValid = false;
                                 }
                             }                             
                         }
                     } else {
                         freshProdInfoVo = createFreshErrorResultSet("Bad country: " + countryId);
                         stillValid = false;
                     }
                 }
             }

             //Validate product / upsell detail id attributes
             SourceProductUtility spUtil = new SourceProductUtility();

             if ( (productVO.getProductId() == null || productVO.getProductId().equalsIgnoreCase("")) && productVO.isHasUpsells())
               isUpsellMaster = true;

             logger.debug("product passed in = " + productId + " and productVO.getProductId = " + productVO.getProductId() + " and productVO.isHasUpsells = " + productVO.isHasUpsells() + " and isUpsellMaster = " + isUpsellMaster);

             if (productVO.isHasUpsells())
             {
               //Prior to 7229, JOE worked as follows:
                   //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                   //if productVO.isHasUpsells() and there exists at least one valid upsellDetailId
                       //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, upsellDetailId, sourceCode) at least once - GUI needs product (in this case = upsell detail id) details
               //Changes implemented with 7229:
                   //create doc by calling orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode)
                   //if productVO.isHasUpsells()
                       //create a list of valid and invalid upsellDetailIds by calling SourceProductUtility
                       //if there is at least one valid upsellDetailIds
                           //recreate doc by calling orderEntryDAO.getProductDetailsAjax(qConn, validUpsellDetailId, sourceCode, commaSeparatedValidUpsellDetailIds)
                             //Note --> validUpsellDetailId = validUpsellList.get(0)
               List upsellList = productVO.getUpsellDetailIds();
               List validUpsellList = new ArrayList(); 
               List invalidUpsellList = new ArrayList(); 
               String upsellDetailId = null;
               String upsellProductError = null;
               String productError = null; 

               for (int i=0; i<upsellList.size(); i++) 
               {
                 upsellProductError = null; 
                 upsellDetailId = (String) upsellList.get(i);
                 //If the Utility throws an Exception, still show the product to the user. 
                 try
                 {
                   upsellProductError = spUtil.getProductRestrictionMsg(qConn, sourceCode, upsellDetailId, true, domesticFlag?"D":"I");
                   logger.debug("Upsells - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  upsellDetailId + ") returned -- " + upsellProductError);
                 }
                 catch(Exception e)
                 {
                   logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + upsellDetailId + ".  Error = " + e.toString()  );
                   upsellProductError = null; 
                 }
                 //if utility returns an error, this upsell is considered invalid
                 if (upsellProductError != null && upsellProductError.length() > 0)
                 {
                   if (upsellDetailId.equalsIgnoreCase(productId))
                     productError = upsellProductError;
                   invalidUpsellList.add(upsellDetailId);
                 }
                 else
                 {
                   validUpsellList.add(upsellDetailId);
                 }
               }
               logger.debug("Upsells - validUpsellList.size = " + validUpsellList.size());
               if (productError != null && productError.length() > 0)
               {
                 freshProdInfoVo = createFreshErrorResultSet(productError);
                 stillValid = false;
               }
               else if (validUpsellList.size()>0)
               {
                 StringBuffer commaSeparatedValidUpsellDetailIds = new StringBuffer(); 
                 for (int i = 0; i < validUpsellList.size(); i++)
                 {
                   commaSeparatedValidUpsellDetailIds.append("'");
                   commaSeparatedValidUpsellDetailIds.append((String)validUpsellList.get(i));
                   commaSeparatedValidUpsellDetailIds.append("'");
                   commaSeparatedValidUpsellDetailIds.append(",");
                 }
                 
                 // Remove the last comma
                 if(commaSeparatedValidUpsellDetailIds.length() > 0)
                 {
                   commaSeparatedValidUpsellDetailIds.deleteCharAt(commaSeparatedValidUpsellDetailIds.length()-1);
                 }

                 // ==================  GET PRODUCT DETAILS FOR FRESH =============================================================== ??? TODO
                 
                 //if (isUpsellMaster)
                   //doc = orderEntryDAO.getProductDetailsFresh(qConn, (String)validUpsellList.get(0), sourceCode, commaSeparatedValidUpsellDetailIds.toString());
                 //else
                   doc = orderEntryDAO.getProductDetailsFresh(qConn, productId, sourceCode, commaSeparatedValidUpsellDetailIds.toString());

                 // ==================  GET PRODUCT DETAILS FOR FRESH =============================================================== ??? TODO
                 
               }
               else 
               {
                 freshProdInfoVo = createFreshErrorResultSet("Product not available for this source code");
                 stillValid = false;
               }
             }
             else 
             {
               if (stillValid)
               {
                 //If the Utility throws an Exception, do not let the user select this product
                 String productError = null; 
                 try
                 {
                   productError = spUtil.getProductRestrictionMsg(qConn, sourceCode, productId, true, domesticFlag?"D":"I");
                   logger.debug("ProductId - SourceProductUtility.getProductRestrictionMsg(" + sourceCode + "," +  productId + ") returned -- " + productError);
                 }
                 catch(Exception e)
                 {
                   logger.info("SourceProductUtility raised an exception for sourceCode = " + sourceCode + " and productId = " + productId + ".  Error = " + e.toString()  );
                   productError = "Product not available for this source code"; 
                 }
                 if(productError != null && productError.length() > 0) {
                     freshProdInfoVo = createFreshErrorResultSet(productError);
                     stillValid = false;
                 }
               }
             }
             
             //if (doc != null) logger.debug(JAXPUtil.toString(doc));
             
             boolean foundSourceCode = false;
             String priceHeaderId = "";
             String sourceDiscountAllowedFlag = "";
             String iotwFlag = "";

             CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
             
             if (cr.next()) {
                 priceHeaderId = cr.getString("price_header_id");
                 sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                 iotwFlag = cr.getString("iotw_flag");
                 foundSourceCode = true;
             }
             */

             //if (stillValid) {
                
                 /*              
                 // IOTW not needed for Fresh since in promotions
                 //
                 Map iotwProducts = ProductUtil.getIOTWProducts(qConn, sourceCode);

                 if (foundSourceCode) {
                     String iotwId = null;
                     String idddCount = null;

                     String originalSourceCode = sourceCode;
                     String originalPriceHeaderId = priceHeaderId;
                     String originalSourceDiscountAllowedFlag = sourceDiscountAllowedFlag;

                     String[] result = (String[]) iotwProducts.get(productId);
                     if (result != null) {
                         sourceCode = result[0];
                         productVO.setProductPageMessage(result[2]);
                         idddCount = result[4];
                         iotwId = result[5];
                         logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + productVO.getProductPageMessage());
                         cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                         if (cr.next()) {
                             priceHeaderId = cr.getString("price_header_id");
                             sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                             iotwFlag = cr.getString("iotw_flag");
                         }
                     }

                     String discountAllowedFlag = productVO.getDiscountAllowedFlag();
                     if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                         discountAllowedFlag = "Y";
                     }

                     if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                         OrderDetailVO odVO = new OrderDetailVO();
                         odVO.setItemSourceCode(sourceCode);
                         odVO.setDeliveryDate(new Date());
                         
                         MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                     } else {
                         ProductUtil.calcDiscount(qConn, priceHeaderId, productVO, discountAllowedFlag, true);
                     }

                     // product_discounts not needed for Fresh since will be part of Promotions  ??? TODO 
                     //
                     Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                     rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product_discounts");
                     rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                     rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                     
                     Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                     row.setAttribute(OrderEntryConstants.TAG_ROW, "1");
                     rowset.appendChild(row);
                     
                     Element value = doc.createElement("iotw-flag");
                     value.setAttribute("flag", iotwFlag);
                     row.appendChild(value);
                     //value.appendChild(doc.createTextNode(iotwFlag));

                     if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {

                         Element iotwValue = doc.createElement("product-page-message");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createCDATASection(productVO.getProductPageMessage()));

                         iotwValue = doc.createElement("item-source-code");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(sourceCode));

                         iotwValue = doc.createElement("master-source-code");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                         iotwValue = doc.createElement("discount-type");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                         iotwValue = doc.createElement("standard-price");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                         iotwValue = doc.createElement("deluxe-price");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                         iotwValue = doc.createElement("premium-price");
                         value.appendChild(iotwValue);
                         iotwValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                         iotwValue = doc.createElement("delivery-dates");
                         value.appendChild(iotwValue);

                         if (idddCount != null) {
                             int deliveryCount = 0;

                             try {
                                 deliveryCount = Integer.parseInt(idddCount);
                             } catch (Exception e) {
                                 logger.error("Error converting IOTW delivery date count: " + idddCount);
                             }

                             if (deliveryCount > 0) {
                                 logger.debug("IOTW has delivery date restrictions");

                                 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                 cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                 int cnt=0;
                                 while (cr.next()) {
                                     Date discountDate = cr.getDate("discount_date");
                                     cnt = cnt + 1;
                                     Element dateValue = doc.createElement("date");
                                     dateValue.setAttribute("row", Integer.toString(cnt));
                                     iotwValue.appendChild(dateValue);
                                     dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                 }

                             }
                         }

                         if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                             OrderDetailVO odVO = new OrderDetailVO();
                             odVO.setItemSourceCode(originalSourceCode);
                             odVO.setDeliveryDate(new Date());
                             
                             MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
                         } else {
                             discountAllowedFlag = productVO.getDiscountAllowedFlag();
                             if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                 discountAllowedFlag = "Y";
                             }
                             ProductUtil.calcDiscount(qConn, originalPriceHeaderId, productVO, discountAllowedFlag, true);
                         }

                     }

                     value = doc.createElement("discount");
                     row.appendChild(value);

                     Element discountValue = doc.createElement("standard-price");
                     value.appendChild(discountValue);
                     discountValue.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

                     discountValue = doc.createElement("deluxe-price");
                     value.appendChild(discountValue);
                     discountValue.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

                     discountValue = doc.createElement("premium-price");
                     value.appendChild(discountValue);
                     discountValue.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));

                     discountValue = doc.createElement("discount-type");
                     value.appendChild(discountValue);
                     discountValue.appendChild(doc.createTextNode(productVO.getRewardType()));

                     NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                     if (node.getLength() > 0) {
                         Element resultNode = (Element) node.item(0);
                         resultNode.appendChild(rowset);
                     }
                     */

                     // isHasSubcodes || isHasUpsells
                     //
                     /*
                     if (productVO.isHasSubcodes() || productVO.isHasUpsells()) {
                         
                         NodeList nl = doc.getElementsByTagName(OrderEntryConstants.TAG_RS);
                         for (int i=0; i<nl.getLength(); i++) {
                             Element rsElement = (Element) nl.item(i);
                             String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);

                             /*
                             // Start of product subcodes (which is no longer used - so can probably be removed)   ??? TODO
                             //
                             if (nodeName != null && nodeName.equalsIgnoreCase("product_subcodes") && rsElement.hasChildNodes()) {
                                 NodeList nlRS = rsElement.getChildNodes();
                                 for (int j=0; j<nlRS.getLength(); j++) {
                                     Element record = (Element) nlRS.item(j);
                                     NodeList nlRecord = record.getChildNodes();
                                     String subcodeProductId = null;
                                     String subcodePrice = null;
                                     for (int k=0; k<nlRecord.getLength(); k++) {
                                         Element subcodeDetails = (Element) nlRecord.item(k);
                                         String name = subcodeDetails.getNodeName();
                                         String elementValue = "";
                                         if (subcodeDetails.hasChildNodes()) {
                                             Node n = subcodeDetails.getFirstChild();
                                             elementValue = n.getNodeValue();
                                         }
                                         if (name.equalsIgnoreCase("product_id")) {
                                             subcodeProductId = elementValue;
                                         } else if (name.equalsIgnoreCase("subcode_price")) {
                                             subcodePrice = elementValue;
                                        }
                                     }
                                     if (subcodeProductId != null && subcodePrice != null) {
                                         ProductVO subcodeVO = new ProductVO();
                                         subcodeVO.setProductId(subcodeProductId);
                                         subcodeVO.setStandardPrice(new BigDecimal(subcodePrice));
                                         subcodeVO.setDeluxePrice(new BigDecimal(0));
                                         subcodeVO.setPremiumPrice(new BigDecimal(0));

                                         sourceCode = originalSourceCode;
                                         priceHeaderId = originalPriceHeaderId;
                                         sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                         iotwFlag = "N";
                                         idddCount = null;

                                         result = (String[]) iotwProducts.get(subcodeProductId);
                                         if (result != null) {
                                             sourceCode = result[0];
                                             subcodeVO.setProductPageMessage(result[2]);
                                             idddCount = result[4];
                                             iotwId = result[5];
                                             logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + subcodeVO.getProductPageMessage());
                                             cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                             if (cr.next()) {
                                                 priceHeaderId = cr.getString("price_header_id");
                                                 sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                 iotwFlag = cr.getString("iotw_flag");
                                             }
                                         }

                                         discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                         if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                             discountAllowedFlag = "Y";
                                         }

                                         if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                             OrderDetailVO odVO = new OrderDetailVO();
                                             odVO.setItemSourceCode(sourceCode);
                                             odVO.setDeliveryDate(new Date());
                                             
                                             MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                         } else {
                                             ProductUtil.calcDiscount(qConn, priceHeaderId, subcodeVO, discountAllowedFlag, true);
                                         }

                                         value = doc.createElement("iotw-flag");
                                         value.setAttribute("flag", iotwFlag);
                                         record.appendChild(value);

                                         if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                             
                                             Element iotwValue = doc.createElement("product-page-message");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createCDATASection(subcodeVO.getProductPageMessage()));

                                             iotwValue = doc.createElement("item-source-code");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(sourceCode));

                                             iotwValue = doc.createElement("master-source-code");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                             iotwValue = doc.createElement("discount-type");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                             iotwValue = doc.createElement("discount-price");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                             iotwValue = doc.createElement("delivery-dates");
                                             value.appendChild(iotwValue);

                                             if (idddCount != null) {
                                                 int deliveryCount = 0;

                                                 try {
                                                     deliveryCount = Integer.parseInt(idddCount);
                                                 } catch (Exception e) {
                                                     logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                 }

                                                 if (deliveryCount > 0) {
                                                     logger.debug("IOTW has delivery date restrictions");

                                                     SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                     cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                     int cnt=0;
                                                     while (cr.next()) {
                                                         Date discountDate = cr.getDate("discount_date");
                                                         cnt = cnt + 1;
                                                         Element dateValue = doc.createElement("date");
                                                         dateValue.setAttribute("row", Integer.toString(cnt));
                                                         iotwValue.appendChild(dateValue);
                                                         dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                     }

                                                 }
                                             }
                                         }

                                         if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                             OrderDetailVO odVO = new OrderDetailVO();
                                             odVO.setItemSourceCode(originalSourceCode);
                                             odVO.setDeliveryDate(new Date());
                                             
                                             MembershipUtil.calcMilesPoints(qConn, odVO, subcodeVO);
                                         } else {
                                             discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                             if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                 discountAllowedFlag = "Y";
                                             }
                                             ProductUtil.calcDiscount(qConn, originalPriceHeaderId, subcodeVO, discountAllowedFlag, true);
                                         }

                                         value = doc.createElement("discount");
                                         record.appendChild(value);

                                         discountValue = doc.createElement("discount-type");
                                         value.appendChild(discountValue);
                                         discountValue.appendChild(doc.createTextNode(subcodeVO.getRewardType()));

                                         discountValue = doc.createElement("discount-price");
                                         value.appendChild(discountValue);
                                         discountValue.appendChild(doc.createTextNode(subcodeVO.getNetStandardPrice().toString()));

                                     }
                                 }
                                 //
                                 // End of product subcodes 
                              
                             
                             // Pricing stuff for product_upsells - not needed for Fresh
                             //                                 
                             } else if (nodeName != null && nodeName.equalsIgnoreCase("product_upsells") && rsElement.hasChildNodes()) {
                                 NodeList nlRS = rsElement.getChildNodes();
                                 for (int j=0; j<nlRS.getLength(); j++) {
                                     Element record = (Element) nlRS.item(j);
                                     NodeList nlRecord = record.getChildNodes();
                                     String upsellProductId = null;
                                     String upsellPrice = null;
                                     for (int k=0; k<nlRecord.getLength(); k++) {
                                         Element upsellDetails = (Element) nlRecord.item(k);
                                         String name = upsellDetails.getNodeName();
                                         String elementValue = "";
                                         if (upsellDetails.hasChildNodes()) {
                                             Node n = upsellDetails.getFirstChild();
                                             elementValue = n.getNodeValue();
                                         }
                                         if (name.equalsIgnoreCase("upsell_detail_id")) {
                                             upsellProductId = elementValue;
                                         } else if (name.equalsIgnoreCase("upsell_price")) {
                                             upsellPrice = elementValue;
                                        }
                                     }
                                     if (upsellProductId != null && upsellPrice != null) {
                                         ProductVO upsellVO = new ProductVO();
                                         upsellVO.setProductId(upsellProductId);
                                         upsellVO.setStandardPrice(new BigDecimal(upsellPrice));
                                         upsellVO.setDeluxePrice(new BigDecimal(0));
                                         upsellVO.setPremiumPrice(new BigDecimal(0));

                                         sourceCode = originalSourceCode;
                                         priceHeaderId = originalPriceHeaderId;
                                         sourceDiscountAllowedFlag = originalSourceDiscountAllowedFlag;
                                         iotwFlag = "N";
                                         idddCount = null;

                                         result = (String[]) iotwProducts.get(upsellProductId);
                                         if (result != null) {
                                             sourceCode = result[0];
                                             upsellVO.setProductPageMessage(result[2]);
                                             idddCount = result[4];
                                             iotwId = result[5];
                                             logger.debug("Found IOTW for " + productId + ": " + sourceCode + " " + upsellVO.getProductPageMessage());
                                             cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                             if (cr.next()) {
                                                 priceHeaderId = cr.getString("price_header_id");
                                                 sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
                                                 iotwFlag = cr.getString("iotw_flag");
                                             }
                                         }

                                         discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                         if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                             discountAllowedFlag = "Y";
                                         }

                                         if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                             OrderDetailVO odVO = new OrderDetailVO();
                                             odVO.setItemSourceCode(sourceCode);
                                             odVO.setDeliveryDate(new Date());
                                             
                                             MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                         } else {
                                             ProductUtil.calcDiscount(qConn, priceHeaderId, upsellVO, discountAllowedFlag, true);
                                         }
                                         
                                         // IOTW (in product_upsells node) - not needed for Fresh since will be part of Promotions  ??? TODO 
                                         //
                                         value = doc.createElement("iotw-flag");
                                         value.setAttribute("flag", iotwFlag);
                                         record.appendChild(value);

                                         if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                                             
                                             Element iotwValue = doc.createElement("product-page-message");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createCDATASection(upsellVO.getProductPageMessage()));

                                             iotwValue = doc.createElement("item-source-code");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(sourceCode));

                                             iotwValue = doc.createElement("master-source-code");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(originalSourceCode));

                                             iotwValue = doc.createElement("discount-type");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                             iotwValue = doc.createElement("discount-price");
                                             value.appendChild(iotwValue);
                                             iotwValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));

                                             iotwValue = doc.createElement("delivery-dates");
                                             value.appendChild(iotwValue);

                                             if (idddCount != null) {
                                                 int deliveryCount = 0;

                                                 try {
                                                     deliveryCount = Integer.parseInt(idddCount);
                                                 } catch (Exception e) {
                                                     logger.error("Error converting IOTW delivery date count: " + idddCount);
                                                 }

                                                 if (deliveryCount > 0) {
                                                     logger.debug("IOTW has delivery date restrictions");

                                                     SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                                     cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
                                                     int cnt=0;
                                                     while (cr.next()) {
                                                         Date discountDate = cr.getDate("discount_date");
                                                         cnt = cnt + 1;
                                                         Element dateValue = doc.createElement("date");
                                                         dateValue.setAttribute("row", Integer.toString(cnt));
                                                         iotwValue.appendChild(dateValue);
                                                         dateValue.appendChild(doc.createTextNode(sdf.format(discountDate)));
                                                     }

                                                 }
                                             }
                                         }

                                         if (originalPriceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                                             OrderDetailVO odVO = new OrderDetailVO();
                                             odVO.setItemSourceCode(originalSourceCode);
                                             odVO.setDeliveryDate(new Date());
                                             
                                             MembershipUtil.calcMilesPoints(qConn, odVO, upsellVO);
                                         } else {
                                             discountAllowedFlag = productVO.getDiscountAllowedFlag();
                                             if (originalSourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                                                 discountAllowedFlag = "Y";
                                             }
                                             ProductUtil.calcDiscount(qConn, originalPriceHeaderId, upsellVO, discountAllowedFlag, true);
                                         }
                                         
                                         
                                         // Pricing no longer needed for Fresh since it will be calculated from pricing in promotions data
                                         //
                                         value = doc.createElement("discount");
                                         record.appendChild(value);

                                         discountValue = doc.createElement("discount-type");
                                         value.appendChild(discountValue);
                                         discountValue.appendChild(doc.createTextNode(upsellVO.getRewardType()));

                                         discountValue = doc.createElement("discount-price");
                                         value.appendChild(discountValue);
                                         discountValue.appendChild(doc.createTextNode(upsellVO.getNetStandardPrice().toString()));
                                     }
                                 }
                             }
                             // END Pricing stuff for product_upsells - not needed for Fresh

                         }

                     }
                     // isHasSubcodes || isHasUpsells
                 }
                 */
                 
                 // get all available delivery dates by ship method
                 /*
                 if (productVO.getShipMethodCarrier() != null && productVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {
                     ProductUtil.getAvailableShipDatesXML(doc, productId, null);
                 }
                 */
                 
                 /*
                 // Special node just for product categories
                 //
                 CachedResultSet fcr = null;
                 if (isProdStatusA) {
                    fcr = orderEntryDAO.getProductCategoriesFresh(qConn, productId, null);
                 }
                 
                 // Populate POJOs
                 //
                 freshProdInfoVo = ProductUtil.parseFreshProductXML(doc, fcr);
                 freshProdInfoVo.setStatus(FRESH_PRODUCT_LOOKUP_SUCCESS);
                 */
                
                 /*
                 // ??? TODO - TEMPORARY TEST CODE START ==========================
                 Gson gsonTest = new Gson();
                 String jsonTest = gsonTest.toJson(freshProdInfoVo);  
                 logger.info("GPS test Gson start ==========");
                 logger.info(jsonTest);
                 logger.info("GPS test Gson fin   ==========");
                 //fcr.reset();
                 // ??? TODO - TEMPORARY TEST CODE END   ==========================
                 */
                 
                 /*                 
                 List<Element> categoriesNodeList = new ArrayList<Element>();
                 Element categoriesNode = null; 
                 Element categoryNode = null;
                 String scode = null;
                 if (fcr != null) { 
                    Element cLevel = null;
                    Element cCat   = null;
                    Element cDisp  = null;
                    while (fcr.next()) {
                       categoriesNode = doc.createElement("categories");
                       int level = 0;
                       for (int x=FRESH_CATEGORY_MAX_DEPTH; x > 0; x--) {
                          categoryNode = doc.createElement("category");
                          String indexFile = fcr.getString("index_file_" + x);
                          String indexName = fcr.getString("index_name_" + x);
                          if (indexName != null && !indexName.isEmpty()) {
                             level++;
                             cLevel = doc.createElement("level");
                             cLevel.appendChild(doc.createTextNode(Integer.toString(level)));
                             cCat = doc.createElement("categoryName");
                             cCat.appendChild(doc.createTextNode(indexFile));
                             cDisp = doc.createElement("displayName");
                             cDisp.appendChild(doc.createTextNode(indexName));
                             categoryNode.appendChild(cLevel);
                             categoryNode.appendChild(cCat);
                             categoryNode.appendChild(cDisp);
                          }
                          categoriesNode.appendChild(categoryNode);
                       }
                       categoriesNodeList.add(categoriesNode);
                    }
                 }
                 if (categoriesNodeList.size() > 0) {
                    NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                       Element resultNode = (Element) node.item(0);
                       for (Element em : categoriesNodeList) {
                          resultNode.appendChild(em);                          
                       }
                    }
                 }
                 */
                 
             //}    // End if (stillValid)...
             
             // Log to service table
             logServiceResponseTracking(qConn, productId, "ProductInfo", startTime);

         } catch (Throwable t) {
             logger.error("Error in validateProduct()",t);
             throw new Exception("Could not retrieve product details.");
         } finally {
             if( qConn!=null ) {
                 try {
                     if( !qConn.isClosed() ) {
                         qConn.close();
                     }
                 } catch (SQLException sqle) {
                     logger.warn("Error closing database connection",sqle);
                 }
             }
         }
         
         return freshProdInfoVo;
     }  // End validateProductFresh
    
    
     private FreshProdInfoVO createFreshErrorResultSet(String errorMessage) throws Exception {
        FreshProdInfoVO fpvo = new FreshProdInfoVO();
        logger.info("Project Fresh note: " + errorMessage);
        fpvo.setMessage(errorMessage);
        fpvo.setStatus(FRESH_PRODUCT_LOOKUP_ERROR);
        return fpvo;
    }
     
   public void triggerProjectFreshUpdates(String dateStr) throws Exception {
      Connection qConn = null;
      String PROD_TRIGGER_DATE_REGEX = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}";
      
      // Confirm the date is in appropiate format
      Pattern pattern = Pattern.compile(PROD_TRIGGER_DATE_REGEX);
      Matcher m = pattern.matcher(dateStr);
      if (!m.matches()) {
          throw new Exception("Invalid date format passed to triggerProjectFreshUpdates");
      }
      
      try {
          String dataSource = OrderEntryConstants.DATASOURCE;
          qConn = resourceProvider.getDatabaseConnection(dataSource);
          if (qConn == null) {
              throw new Exception("Unable to connect to database");
          }
          String payload = "FRESH_MULTI_MSG_PRODUCER|product|joe_all|" + dateStr;
          orderEntryDAO.postAMessage(qConn, "OJMS.PDB", null, payload, Long.valueOf(15));
          payload = "FRESH_MULTI_MSG_PRODUCER|sourcecode|joe_all|" + dateStr;
          orderEntryDAO.postAMessage(qConn, "OJMS.PDB", null, payload, Long.valueOf(30));
      } catch (Throwable t) {
         logger.error("Error in triggerProductFreshUpdates()",t);
         throw new Exception("Could not trigger Product Fresh product update.");
      } finally {
         if( qConn!=null ) {
             try {
                 if( !qConn.isClosed() ) {
                     qConn.close();
                 }
             } catch (SQLException sqle) {
                 logger.warn("Error closing database connection",sqle);
             }
         }
      }
      
   }
    
    
    /* Validate product availability
     * @param productId - product being ordered
     * @param zipCode - zip code of delivery
     * @param deliveryDate - date being delivered
     * @param countryId - country of delivery
     * @param sourceCode - used for getProductDetails() if product is not available
     * @param dateRangeEnd - Date Range functionality is no longer supported in JOE
     * @param orderDoc - used for getAvailableDeliveryDatesAndChargesXML()
     * @param getChargesXML - set to true for Ajax calls, set to false if called from submitCart
     * @return doc - Document of PAS results and shipping charges
     * */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Document validateProductAvailability(String productId, String zipCode, String deliveryDate,
        String countryId, String sourceCode, String dateRangeEnd,Document orderDoc,
        boolean getChargesXML, String addons) throws Exception {
      
        //As of the tiered service fee project, this method also returns the charges associated with the shipping methods

        Connection qConn = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat sdfDay = new SimpleDateFormat("EEE");
        Date delDate = null;

        logger.info("validateProductAvailability(" + productId + ", " + deliveryDate + ", " +
            zipCode + ", " + countryId + ", " + sourceCode + ", " + addons + ")");

        String validationStatus = "Y";
        Map errorList = new HashMap();
        String state = null;
        boolean floristAvailable = false;
        boolean shipNDAvailable = false;
        boolean ship2FAvailable = false;
        boolean shipGRAvailable = false;
        boolean shipSAAvailable = false;
        boolean shipSUAvailable = false;
        //boolean frontEndFlag = true;
        boolean isAvailableForState = true; //Flag to check availability for AK/HI and PR/VI
        boolean isExtendedFloristAvailable = false;

        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

        if (productId == null || productId.equals("")) {
            validationStatus = "N";
            errorList.put("product_availability", "Product Id is required for delivery date validation.");
        } else if (countryId == null || countryId.equals("")) {
            validationStatus = "N";
            errorList.put("product_availability", "Country Id is required for delivery date validation.");
        } else if (deliveryDate == null || deliveryDate.equals("")) {
            validationStatus = "N";
            errorList.put("product_availability", "Delivery Date is required for delivery date validation.");
        } else if (orderDoc == null || orderDoc.equals("")) {
            validationStatus = "N";
            errorList.put("product_availability", "Order XML information is required for delivery date validation.");
        } else {

            try {
                String dataSource = OrderEntryConstants.DATASOURCE;
                qConn = resourceProvider.getDatabaseConnection(dataSource);
                if (qConn == null) {
                    throw new Exception("Unable to connect to database");
                }
              
                OrderVO orderVO = OrderUtil.parseOrderXML(orderDoc);

                List<String> addonList = null;
                if (addons != null && addons.length() > 0) {
                    String[] addonString = addons.split(",");
                    addonList = new ArrayList<String>(Arrays.asList(addonString));
                }
                boolean isAddonAvailable = true;

                String countryType = null;
                CachedResultSet cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
                if (cr.next()) {
                    countryType = cr.getString("oe_country_type");
                }
                if (countryType != null && countryType.equalsIgnoreCase("D")) {
                    if (zipCode == null || zipCode.equals("")) {
                        validationStatus = "N";
                        errorList.put("product_availability", "Zip Code is required for delivery date validation.");
                    } else {
                        //ProductAvailabilityBO pas = new ProductAvailabilityBO();
                        ProductAvailVO paVO = null;
                        boolean isAvailable = false;

                        if (dateRangeEnd == null || dateRangeEnd.equals("")) {

                            if (deliveryDate != null && !deliveryDate.equals("")) {
                                delDate = sdf.parse(deliveryDate);
                            }
                            logger.debug("Calling pas.getProductAvailability(" + productId + ", " + deliveryDate + ", " + zipCode + ")");
                            paVO = PASServiceUtil.getProductAvailability(productId, delDate, zipCode, countryId, addonList, sourceCode, false, true); //(qConn, productId, addonList, delDate, zipCode, ,sourceCode;
                            if (paVO == null) {
                            	isAvailable = false;
                            	isAddonAvailable = false;
                            } else {
                                isAvailable = paVO.isIsAvailable();
                                isAddonAvailable = paVO.isIsAddOnAvailable();
                                if(paVO.getErrorMessage() != null && paVO.getErrorMessage().contains("EFA_FLORIST_AVAILABLE")) {
                                	isExtendedFloristAvailable = true;
                                }
                            }
                            
                            logger.info("isAvailable: " + isAvailable + " isAddonAvailable: " + isAddonAvailable);

                        } else {

                            try {

                                for (long i=Long.parseLong(deliveryDate); i<=Long.parseLong(dateRangeEnd); i++) {
                                    String thisDeliveryDate = Long.toString(i);
                                    logger.debug("Checking date range: " + thisDeliveryDate);
                                    delDate = sdf.parse(thisDeliveryDate);
                                    logger.debug("Calling pas.getProductAvailability(" + productId + ", " + thisDeliveryDate + ", " + zipCode + ")");
                                    paVO = PASServiceUtil.getProductAvailability(productId, delDate, zipCode, countryId, addonList, sourceCode, false, true); //(qConn, productId, addonList, delDate, zipCode, ,sourceCode);
                                    if (paVO == null) {
                                    	isAvailable = false;
                                    	isAddonAvailable = false;
                                    } else {
                                        isAvailable = paVO.isIsAvailable();
                                        isAddonAvailable = paVO.isIsAddOnAvailable();
                                    }
                                    logger.debug("isAvailable: " + isAvailable);
                                    if (isAvailable) break;
                                }
                                
                            } catch (Exception e) {
                                logger.error("Date range error: " + e);
                            }
                        }
                        
                        Document productDoc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode);
                        ProductVO productVO = ProductUtil.parseProductXML(productDoc);
                        Document stateDoc = orderEntryDAO.getZipCodeAjax(qConn,zipCode);
                                                
                        
                        NodeList nl = stateDoc.getElementsByTagName(OrderEntryConstants.TAG_RS);
                        if(nl.getLength()> 0) {
                            Element rsElement = (Element) nl.item(0);
                            String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);
                            if (nodeName != null && nodeName.equalsIgnoreCase("zip_code") && rsElement.hasChildNodes()) {
                                NodeList nlRS = rsElement.getChildNodes();
                                Element record = (Element) nlRS.item(0);
                                NodeList nlRecord = record.getChildNodes();
                                for(int j=0; j<nlRecord.getLength(); j++) {
                                    Element stateDetails = (Element) nlRecord.item(j);
                                    String name = stateDetails.getNodeName();
                                    String value = "";
                                    if (stateDetails.hasChildNodes()) {
                                        Node n = stateDetails.getFirstChild();
                                        value = n.getNodeValue();
                                    }
                                    if (name.equalsIgnoreCase("state_id")) {
                                         state = value;
                                         break;
                                    }
                                }
                            }
                        }
                        
                        logger.info("The shipping system : "+productVO.getShippingSystem()+" and the Product type : "+productVO.getProductType()+" with State : "+state);
                        if(GeneralConstants.FTDW_SHIPPING_KEY.equals(productVO.getShippingSystem())){
                        	// FTD West fullfilled products are not allowed for delivery in PR/VI
                        	if((state.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) || state.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS))){
                        		validationStatus = "N";
                                errorList.put("product_availability", "This product is unavailable for this zip code.");
                                isAvailableForState = false;
                        	}else if(GeneralConstants.OE_PRODUCT_TYPE_FRECUT.equals(productVO.getProductType()) 
                        			&& (state.equals(GeneralConstants.STATE_CODE_ALASKA) || state.equals(GeneralConstants.STATE_CODE_HAWAII))){
                        		// FTD West fullfilled Freshcut's are not allowed for delivery in AK/HI
                        		validationStatus = "N";
                                errorList.put("product_availability", "This product is unavailable for this zip code.");
                                isAvailableForState = false;
                        	}
                        }
                      if(isAvailableForState){//Product unavailable for AK/Hi or PR/VI
                    	  
                        if (isAvailable) {                     

                            Date today = new Date();
                            String todayString = sdf3.format(today);
                            logger.debug("today: " + todayString);
                            
                            String cutoffTime = paVO.getFloristCutoffTime(); 
                            if (paVO.getFloristCutoffDate() != null) { 
                                String cutoffString = sdf.format(paVO.getFloristCutoffDate()) + cutoffTime;
                                logger.debug("florist cutoff: " + cutoffString);
                                //Checking for a cutoff is the correct way to determine if a product is florist delivered
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    floristAvailable = true;
                                }
                            }
                        
                            String cutoffGR = paVO.getShipDateGRCutoff(); 
                            if (paVO.getShipDateGR() != null) { 
                                String cutoffString = sdf.format(paVO.getShipDateGR()) + cutoffGR;
                                logger.debug("GR cutoff: " + cutoffString);
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    shipGRAvailable = true;
                                }
                            }

                            String cutoff2F = paVO.getShipDate2DCutoff(); 
                            if (paVO.getShipDate2D() != null) { 
                                String cutoffString = sdf.format(paVO.getShipDate2D()) + cutoff2F;
                                logger.debug("2F cutoff: " + cutoffString);
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    ship2FAvailable = true;
                                }
                            }

                            String cutoffND = paVO.getShipDateNDCutoff(); 
                            if (paVO.getShipDateND() != null) { 
                                String cutoffString = sdf.format(paVO.getShipDateND()) + cutoffND;
                                logger.debug("ND cutoff: " + cutoffString);
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    shipNDAvailable = true;
                                }
                            }
                        
                            String cutoffSA = paVO.getShipDateSACutoff(); 
                            if (paVO.getShipDateSA() != null) { 
                                String cutoffString = sdf.format(paVO.getShipDateSA()) + cutoffSA;
                                logger.debug("SA cutoff: " + cutoffString);
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    shipSAAvailable = true;
                                }
                            }
                            
                            String cutoffSU = paVO.getShipDateSUCutoff(); 
                            if (paVO.getShipDateSU() != null) { 
                                String cutoffString = sdf.format(paVO.getShipDateSU()) + cutoffSU;
                                logger.debug("SU cutoff: " + cutoffString);
                                if (todayString.compareTo(cutoffString) <= 0) {
                                    shipSUAvailable = true;
                                }
                            }
                            
                        } else {
                            
                            
                            String dayOfWeek = sdfDay.format(delDate).toUpperCase();
                            boolean isFloral = false;
                            boolean isDropShip = false;
                            if (productVO.getShipMethodFlorist() != null && productVO.getShipMethodFlorist().equalsIgnoreCase("Y")) {
                                isFloral = true;
                            }
                            if (productVO.getShipMethodCarrier() != null && productVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {
                                isDropShip = true;
                            }
                            
                            
                            if (!isFloral && isDropShip && dayOfWeek.equalsIgnoreCase("SUN")) {
                                validationStatus = "N";
                                errorList.put("product_delivery", "This product is not available for delivery on Sunday.");
                            } else {

                                logger.debug("Calling pas.getNextAvailableDeliveryDate(" + productId + ", " + zipCode + ")");
                                Date nextAvailableDate = PASServiceUtil.getNextAvailableDeliveryDate(productId, addonList, zipCode, countryId, sourceCode);

                                if (nextAvailableDate != null) {
                                    validationStatus = "N";
                                    errorList.put("product_delivery", "The selected delivery date is unavailable. " +
                                        "The next available delivery date for this product is " + sdf2.format(nextAvailableDate));
                                } else {

                                    boolean isAnyAvailable = false;
                                    boolean isAnyFloristAvailable = true;

                                    if (dateRangeEnd == null || dateRangeEnd.equals("")) {

                                        logger.debug("Calling pas.isAnyProductAvailable(" + deliveryDate + ", " + zipCode + ")");
                                        isAnyAvailable = PASServiceUtil.isAnyProductAvailable(delDate, zipCode);
                                        logger.debug("isAnyAvailable: " + isAnyAvailable);

                                        if (isAnyAvailable) {
                                            logger.debug("Calling pas.getFlorists(" + deliveryDate + ", " + zipCode + ")");
                                            String[] florists = PASServiceUtil.getAvailableFlorists(delDate, zipCode);
                                            logger.debug("size:" + florists.length);
                                            if (florists.length <= 0) {
                                                isAnyFloristAvailable = false;
                                            }
                                        }

                                    } else {

                                        try {

                                            for (long i=Long.parseLong(deliveryDate); i<=Long.parseLong(dateRangeEnd); i++) {
                                                String thisDeliveryDate = Long.toString(i);
                                                logger.debug("Checking date range: " + thisDeliveryDate);
                                                delDate = sdf.parse(thisDeliveryDate);
                                                logger.debug("Calling pas.isAnyProductAvailable(" + thisDeliveryDate + ", " + zipCode + ")");
                                                isAnyAvailable = PASServiceUtil.isAnyProductAvailable(delDate, zipCode);
                                                logger.debug("isAnyAvailable: " + isAnyAvailable);
                                                if (isAnyAvailable) {

                                                    logger.debug("Calling pas.getFlorists(" + thisDeliveryDate + ", " + zipCode + ")");
                                                    String[] florists = PASServiceUtil.getAvailableFlorists(delDate, zipCode);
                                                    logger.debug("size:" + florists.length);
                                                    if (florists.length <= 0) {
                                                        isAnyFloristAvailable = false;
                                                    }

                                                    break;
                                                }
                                            }
                                        
                                        } catch (Exception e) {
                                            logger.error("Date range error: " + e);
                                        }
                                    }


                                    if (!isAnyAvailable) {
                                        validationStatus = "N";
                                        errorList.put("product_availability",
                                            "No products are available to this zip code for the selected delivery date");
                                    } else {
                                        logger.debug("Checking product exception dates");
                                        String exceptionCode = productVO.getExceptionCode();
                                        logger.debug("exceptionCode: " + exceptionCode);
                                        if (exceptionCode != null) {
                                            Date startDate = productVO.getExceptionStartDate();
                                            Date endDate = productVO.getExceptionEndDate();
                                            if (exceptionCode.equalsIgnoreCase("A")) {
                                                if (delDate.before(startDate) || delDate.after(endDate)) {
                                                    validationStatus = "N";
                                                    errorList.put("product_availability", productVO.getExceptionMessage());
                                                }
                                            } else {
                                                if (delDate.after(startDate) || delDate.equals(startDate) ||
                                                        delDate.after(endDate) || delDate.equals(endDate)) {
                                                    validationStatus = "N";
                                                    errorList.put("product_availability", productVO.getExceptionMessage());
                                                }
                                            }
                                        } 
                                        if (validationStatus != null && validationStatus.equalsIgnoreCase("Y")) {

                                            dayOfWeek = sdfDay.format(delDate).toUpperCase();
                                            logger.debug("Checking PRODUCT_EXCLUDED_STATES for " + dayOfWeek);

                                            cr = orderEntryDAO.getProductExcludedStatesByZip(qConn, productId, zipCode);
                                            cr.next();

                                            String dbValue = cr.getString(dayOfWeek);
                                            logger.debug("dbValue: " + dbValue);
                                            if (dbValue != null && dbValue.equalsIgnoreCase("Y")) {
                                                validationStatus = "N";
                                                errorList.put("product_availability", "This product is not deliverable in this state.");
                                            }
                                        }
                                        if (validationStatus != null && validationStatus.equalsIgnoreCase("Y")) {
                                            if (!isAnyFloristAvailable) {
                                                validationStatus = "N";
                                                errorList.put("product_availability", "Only drop ship items are available in this zip code.");
                                            } else {
                                                validationStatus = "N";
                                                errorList.put("product_availability", "This product is unavailable for this zip code.");
                                            }
                                        }

                                    }
                                }
                            }
                        } // not isAvailable
                      }// Product unavailable for AK/Hi or PR/VI
                    }
                } else {
                	//ProductAvailabilityBO pas = new ProductAvailabilityBO();
                    delDate = sdf.parse(deliveryDate);
                    logger.debug("Calling pas.isInternationalProductAvailable(" + productId + ", " + deliveryDate + ", " + countryId + ")");
                    boolean isProductAvailable = PASServiceUtil.isInternationalProductAvailable(productId, delDate, countryId);
                    if (isProductAvailable) {
                        floristAvailable = true;
                    } else {
                        floristAvailable= false;
                        validationStatus = "N";
                        errorList.put("product_delivery", "This product is not available on this delivery date.");
                    }
                }

                doc = OEActionBase.createValidationResultSet(doc, validationStatus, errorList);
                
                if (validationStatus != null && validationStatus.equalsIgnoreCase("Y")) {
                    NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element) node.item(0);

                        Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                        rowset.setAttribute(OrderEntryConstants.TAG_NAME, "ship-methods");
                        rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                        rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");

                        Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                        row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(1));
                        rowset.appendChild(row);

                        Element value = doc.createElement("florist");
                        row.appendChild(value);
                        logger.info("floristAvailable: " + floristAvailable);
                        if(isExtendedFloristAvailable){
                        	floristAvailable = true;
                    	}
                        if (floristAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }

                        value = doc.createElement("carrierND");
                        row.appendChild(value);
                        if (shipNDAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }

                        value = doc.createElement("carrier2F");
                        row.appendChild(value);
                        if (ship2FAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }

                        value = doc.createElement("carrierGR");
                        row.appendChild(value);
                        if (shipGRAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }

                        value = doc.createElement("carrierSA");
                        row.appendChild(value);
                        if (shipSAAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }
                        
                        value = doc.createElement("carrierSU");
                        row.appendChild(value);
                        if (shipSUAvailable) {
                            value.appendChild(doc.createTextNode("Y"));
                        } else {
                            value.appendChild(doc.createTextNode("N"));
                        }

                        resultNode.appendChild(rowset);

                        logger.info("getChargesXML: " + getChargesXML);
                        if (getChargesXML) {
                            ProductUtil.getAvailableDeliveryDatesAndChargesXML(doc, productId, zipCode,sourceCode, orderVO, qConn);
                        }

                        rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                        rowset.setAttribute(OrderEntryConstants.TAG_NAME, "product-availability");
                        if (isAddonAvailable) {
                            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                        } else {
                            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "N");
                            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "One or more of the previously selected Addons are no longer available.");
                        }
                        resultNode.appendChild(rowset);
                    
                    }
                }
                logger.info(JAXPUtil.toString(doc));

            } catch (Throwable t) {
                logger.error("Error in validateProductAvailability()",t);
                throw new Exception("Could not validate product availability.");
            } finally {
                if( qConn!=null ) {
                    try {
                        if( !qConn.isClosed() ) {
                            qConn.close();
                        }
                    } catch (SQLException sqle) {
                        logger.warn("Error closing database connection",sqle);
                    }
                }
            }
        }

        return doc;
    }

    public Document validateFlorist(String floristId, String productId, String zipCode, String deliveryDateString) throws Exception
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date deliveryDate;
        try
        {
            if (deliveryDateString != null)
            {
                deliveryDate = sdf.parse(deliveryDateString);
            }
            else
            {
                deliveryDate = new Date();
            }
        }
        catch (ParseException pe)
        {
            deliveryDate = new Date();
        }
        return validateFlorist(floristId, productId, zipCode, deliveryDate);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Document validateFlorist(String floristId, String productId, String zipCode, Date deliveryDate) throws Exception {
        Connection qConn = null;

        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

        logger.info("validateFlorist(" + floristId + ", " + productId + ", " + zipCode + ", " + deliveryDate + ")");

        if ( floristId != null ) floristId = floristId.toUpperCase();
        if ( productId != null ) productId = productId.toUpperCase();
        if ( zipCode != null ) zipCode = zipCode.toUpperCase();

        // Only use first 3 characters for Canadian Zips
        if ( zipCode != null && zipCode.length() > 3 &&
            (Character.isLetter(zipCode.charAt(0))
                && Character.isDigit(zipCode.charAt(1))
                && Character.isLetter(zipCode.charAt(2)))) {

            zipCode = zipCode.substring(0,3);
            logger.debug("Changing Canadian zip code to: " + zipCode);
        } else {
            if ( zipCode != null && zipCode.length() > 5 ) {
                zipCode = zipCode.substring(0,5);
                logger.debug("Changing zip code to: " + zipCode);
            }
        }
        
        try {

            Map errorList = new HashMap();
            String status = "Y";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String todayString = sdf.format(new Date());

            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            Map output = orderEntryDAO.getFloristValidationInfo(qConn, floristId, productId, zipCode, deliveryDate);
            
            CachedResultSet crFloristMaster = (CachedResultSet) output.get("OUT_CUR_FM");
            if (crFloristMaster.next()) {
                String floristStatus = crFloristMaster.getString("status");
                String gotoFlag = crFloristMaster.getString("super_florist_flag");
                if (floristStatus != null && ( floristStatus.equalsIgnoreCase("Active") || 
                                               floristStatus.equalsIgnoreCase("Blocked") || 
                    ( floristStatus.equalsIgnoreCase("Mercury Suspend") && gotoFlag != null &&
                    gotoFlag.equalsIgnoreCase("Y") ) )) {

                    CachedResultSet crFloristZips = (CachedResultSet) output.get("OUT_CUR_FZ");
                    if (crFloristZips.next()) {
                        Date blockStartDate = crFloristZips.getDate("block_start_date");
                        //Date blockEndDate = crFloristZips.getDate("block_end_date");
                        if (blockStartDate != null) {
                            String blockStartString = sdf.format(blockStartDate);
                            if (blockStartString != null && (Integer.parseInt(blockStartString) <= Integer.parseInt(todayString))) {
                                errorList.put("florist-id", "Florist is blocked for this zip");
                                status = "N";
                            }
                        }
                    } else {
                        errorList.put("zip-code", "Florist is not signed up for zip code");
                        status = "N";
                    }

                    // Check for blocks
                    CachedResultSet crFloristBlocks = (CachedResultSet) output.get("OUT_CUR_FB");

                    boolean  blocked = false;
                    while (crFloristBlocks.next())
                    {
                        String blockType = crFloristBlocks.getString("block_type");
                        Date blockStartDate = crFloristBlocks.getDate("block_start_date");
                        Date blockEndDate = crFloristBlocks.getDate("block_end_date");

                        if (blockType != null && blockStartDate != null)
                        {
                            // If start < delivery date
                            if (blockType.equals("O"))
                            {
                                if (blockStartDate.compareTo(deliveryDate) <= 0)
                                {
                                    blocked = true;
                                }
                            }
                            else if (blockEndDate != null)
                            {
                                if ((blockStartDate.compareTo(deliveryDate) <= 0) &&
                                    (blockEndDate.compareTo(deliveryDate) >=0) )
                                {
                                    blocked = true;
                                }
                            }

                        }
                        if (blocked)
                        {
                            errorList.put("florist-id", "Florist is blocked for that delivery date dd/sd/ed = " +
                                    deliveryDate + " " + blockStartDate + " " + blockEndDate);
                            status = "N";
                        }
                    }


                    CachedResultSet crFloristCodification = (CachedResultSet) output.get("OUT_CUR_FC");
                    if (crFloristCodification.next()) {
                        String productCodified = crFloristCodification.getString("codified_flag");
                        String floristCodified = crFloristCodification.getString("florist_codified");
                        if (productCodified != null && productCodified.equalsIgnoreCase("Y")) {
                            if (floristCodified != null && floristCodified.equalsIgnoreCase("Y")) {
                                Date blockStartDate = crFloristCodification.getDate("block_start_date");
                                //Date blockEndDate = crFloristCodification.getDate("block_end_date");
                                if (blockStartDate != null) {
                                    String blockStartString = sdf.format(blockStartDate);
                                    if (blockStartString != null && (Integer.parseInt(blockStartString) <= Integer.parseInt(todayString))) {
                                        errorList.put("product-id", "Florist is blocked for this product");
                                        status = "N";
                                    }
                                }
                            } else {
                                errorList.put("product-id", "Florist is not codified for product");
                                status = "N";
                            }
                        }
                    } else {
                        errorList.put("product-id", "Florist is not codified for product");
                        status = "N";
                    }
                    
                                  
                    // Check if florist is open on delivery date
                    CachedResultSet crFloristOpenOnDelivery = (CachedResultSet) output.get("OUT_CUR_FDS");
                    boolean checkFloristClosureStatus = true;
                    boolean  openOnDeliveryDate = true;
                    while (crFloristOpenOnDelivery.next())
                    {
                        String floristOpenOnDD = crFloristOpenOnDelivery.getString("open_on_delivery");
                        String floristClosedOnDDMsg = crFloristOpenOnDelivery.getString("florist_closed_on_dd_msg");
                        
                        if (floristOpenOnDD != null && floristOpenOnDD.equals("N"))
                        {
                        	openOnDeliveryDate = false;
                           	checkFloristClosureStatus = false;
                        }
                        if (!openOnDeliveryDate)
                        {
                            errorList.put("florist-id", floristClosedOnDDMsg);
                            status = "N";
                        }
                    }
                    //only check if florist is currently in closure if the florist isn't closed on delivery date
                    if(checkFloristClosureStatus)
                    {
                    	// Check florist's current closure status 
	                    CachedResultSet crFloristClosureStatus = (CachedResultSet) output.get("OUT_CUR_FCS");
	
	                    boolean  floristCurrentlyOpen = true;
	                    while (crFloristClosureStatus.next())
	                    {
	                    	String floristClosureStatus = crFloristClosureStatus.getString("current_closure_status");
	                        String floristCurrentlyClosedMsg = crFloristClosureStatus.getString("florist_currently_closed_msg");
	                      	
	                        if (floristClosureStatus != null && floristClosureStatus.equals("N"))
	                        {
	                        	floristCurrentlyOpen = false;
	                        }
	                        if (!floristCurrentlyOpen)
	                        {
	                            errorList.put("florist-id", floristCurrentlyClosedMsg);
	                            status = "N";
	                        }
	                    }
                    }
                    
                } else {
                    errorList.put("florist-id", "Florist is not active.");
                    status = "N";
                }
            } else {
                errorList.put("florist-id", "Florist not on file");
                status = "N";
            }

            doc = OEActionBase.createValidationResultSet(doc, status, errorList);

            if (logger.isDebugEnabled() && status.equals("N"))
            {
                StringBuilder sb = new StringBuilder();
                sb.append("Florist Validation failed: \n");
                for (Object entry : errorList.entrySet())
                {
                    Map.Entry mapEntry = (Map.Entry) entry;
                    sb.append(mapEntry.getKey());
                    sb.append(":");
                    sb.append(mapEntry.getValue());
                }
                logger.debug(sb.toString());
            }

        } catch (Throwable t) {
            logger.error("Error in validateFlorist()",t);
            throw new Exception("Could not retrieve florist validation info.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        return doc;
    }
    
    /**
     * 
     * 
     * returns multiple XML elements that look like this:
     * 
     *  		<address-verification>
                        <recip-avs-performed>Yes/No</recip-avs-performed>
                        <recip-address-verification-result>Pass/Fail/NA</recip-address-verification-result>
                        <recip-address-verification-override-flag>Yes/No</recip-address-verification-override-flag>
                        <avs-street-address><![CDATA[123 second street]]></avs-street-address>
                        <avs-city><![CDATA[Downers Grove]]></avs-city>
                        <avs-state><![CDATA[IL]]></avs-state>
                        <avs-postal-code><![CDATA[60515]]></avs-postal-code>
                        <avs-country>US</avs-country>
						<avs-latitude></avs-latitude>
						<avs-longitude></avs-longitude>
						<avs-entity-type>ADDRESS/OTHER</avs-entity-type>
                        <avs-address-scores>
                              <threshold-value>50</threshold-value>
                              <high-confidence-value>10</high-confidence-value>
                              <medium-confidence-value>10</medium-confidence-value>
                              <low-confidence-value>10</low-confidence-value>
                              <addressline-different-value>10</addressline-different-value>
                              <city-different-value>10</city-different-value>
                              <state-different-value>10</state-different-value>
                              <zip-different-value>10</zip-different-value>
                              <zip4-different-value>10</zip4-different-value>
                              <entity-type-address>10</entity-type-address>
                              <entity-type-others>10</entity-type-others>
                        </avs-address-scores>
                  </address-verification>

     */
    public Document validateAVSAddress(String firmName, String address, String city,
        String state, String zipCode, String countryId, String orderType) throws Exception {
    	
    	logger.info("Validating address: " + address + ", " + city + ", " + state + " " + zipCode + " " + countryId + ".  ordertype="+orderType);

        Document doc = null;
        
        //AVS global flags.  skip AVS if they are turned off.
        GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
        String avsEnabledStr = "on";
        if (orderType.equals("D")) {
        	avsEnabledStr = gph.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_DROPSHIP_ORDERS);
        } else if (orderType.equals("F")) {
        	avsEnabledStr = gph.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_FLORIST_ORDERS);
        } else {
        	logger.error("unexpected orderType detected: "+ orderType);
        }
        Boolean avsEnabled = avsEnabledStr.equals("on")? true : false;
        
        if (!avsEnabled) {
        	doc = createAVSOffResponse("the Address Verification Service is disbled for this type of order");
        } else {

	        try {
	        	
	        	VerificationRequest vr = new VerificationRequest();
	        	Address a = new Address();
	        	
	        	a.setStreet1(address);
	        	a.setCity(city);
	        	a.setState(state);
	        	a.setZip(zipCode);
	        	a.setCountry(countryId);
	        	vr.setOrderType(orderType);
	        	
	        	vr.setAddress(a);
	        	vr.setClientIdentifier("JOE");
	
	        	
	        	//get the AVS web service reference and call it
	        	AddressVerificationService avs = WebServiceClientFactory.getAddressVerificationService();
	        	VerificationResponse resp = avs.verifyAddress(vr);
	        	logger.info("found "+ resp.getVerifiedAddresses().size() + " results");
	        	//now that we've got the response, do some logic to alert, replace, or show choices
	        	//based on how many 'valid' addresses we've received
	        	
	        	List<VerifiedAddress> joeAddresses = getJOEAddressesFromAVSResponse(resp);
	
	        	doc = DOMUtil.getDefaultDocument();
	            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
	            doc.appendChild(root);
	            
	            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
	            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");
	
	            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
	            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "avs");
	            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
	            root.appendChild(rowset);
	            
	
	            for (int i=0; i< joeAddresses.size(); i++) {
	            	VerifiedAddress va = joeAddresses.get(i);
	            	//create the row element
	                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
	                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
	                rowset.appendChild(row);
	            	//create the rows for the xml
	                appendSubElement(doc,row, "recip-avs-performed", GeneralConstants.AVS_YES);
	                appendSubElement(doc,row, "recip-address-verification-result", va.getStatus());   //this will get populated based on user input
	                appendSubElement(doc,row, "recip-address-verification-override-flag", "");  //populated based on user input
	                appendSubElement(doc,row, "avs-street-address", va.getStreet1());
	                appendSubElement(doc,row, "avs-city", va.getCity());
	                appendSubElement(doc,row, "avs-state", va.getState());
	                appendSubElement(doc,row, "avs-postal-code", va.getZip());
	                appendSubElement(doc,row, "avs-country", va.getCountry());
	                appendSubElement(doc,row, "avs-latitude", va.getLatitude());
	                appendSubElement(doc,row, "avs-longitude", va.getLongitutde());
	                appendSubElement(doc,row, "avs-entity-type", va.getEntityType());
	                appendSubElement(doc,row, "threshold-value", va.getConfidenceThreshold().toString());
	                
	                //scores section
	                Element scores = doc.createElement("avs-address-scores");
	                for (DevaluationReason score : va.getDevaluationReasons()) {
	                	appendSubElement(doc,scores, score.getReasonKey().toLowerCase(), score.getReasonScore().toString());
	                }
	
	                row.appendChild(scores);
	            }
	            
	            //DOMUtil.print(doc, System.out);
	            
	        } catch (Exception e) {
	        	logger.error("Failed to verify address");
	            logger.error(e);
	            doc = createAVSOffResponse("the Address Verification Service has experienced an error");
	        } finally {}
        }
	        return doc;
    }

    private Document createAVSOffResponse(String reason) throws Exception {
    	Document doc = DOMUtil.getDefaultDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        root.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
        root.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
        doc.appendChild(root);

        Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
        rowset.setAttribute(OrderEntryConstants.TAG_NAME, "avs");
        rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "N");
        rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, reason);
        root.appendChild(rowset);
        
        Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
        row.setAttribute(OrderEntryConstants.TAG_ROW, "1");
        rowset.appendChild(row);
    	//create the rows for the xml
        appendSubElement(doc,row, "recip-avs-performed", GeneralConstants.AVS_NO);
        appendSubElement(doc,row, "recip-address-verification-result", "PASS");   //this will get populated based on user input
        appendSubElement(doc,row, "recip-address-verification-override-flag", GeneralConstants.AVS_NO);  //populated based on user input
		return doc;
	}

	/**
     * 
     * 
     * @param parentElement
     * @param subElementName
     * @param subElementValue
     * @throws ParserConfigurationException 
     * @throws DOMException 
     */
    private void appendSubElement(Document doc, Element parentElement, String subElementName, String subElementValue) throws DOMException, ParserConfigurationException {
    	
    	Element value = doc.createElement(subElementName);
        parentElement.appendChild(value);
        value.appendChild(doc.createTextNode(subElementValue));
		
	}

	/**
     * Create a list of addresses based on the AVS response.
     * Order is important.
     * If there are no addresses, return an empty list
     * If at least one is valid, return the top 0-3 valid addresses, ordered highest to lowest
     * If there are zero valid adddresses, and 1 to many invalid addresses, return the top invalid addresses
     * @param resp
     * @return
     */
	private List<VerifiedAddress> getJOEAddressesFromAVSResponse(VerificationResponse resp) {
		List<VerifiedAddress> results = new ArrayList<VerifiedAddress>();
		
		if (resp.getVerifiedAddresses().size() >0) {
			//put the verified addresses in the arrayList.
			for (VerifiedAddress va : resp.getVerifiedAddresses()) {
				results.add(va);
			}
			
			//sort them by score.  This sort method uses an annonymous comparator which
			// that determines the sort order (based on score)
			Collections.sort(results, new Comparator<VerifiedAddress>() {
				public int compare(VerifiedAddress a1, VerifiedAddress a2) {
					
					/**
					 * return 1, 0, or -1
					 * 
					 * 1 means a1 has the higher score
					 * 0 means that a1 and a2 have the same score
					 * -1 means that a1 has the lower score
					 */
					if (a1.getFTDConfidenceScore() > a2.getFTDConfidenceScore()){
						return 1;
					} else if (a2.getFTDConfidenceScore() == a2.getFTDConfidenceScore() ) {
						return 0;
					} else {
						return -1;
					}
				}
			});
			
			//now that the results are sorted, make sure that there aren't more than 3 pass results
			// or if there are no passing results, keep 1 fail result
			int passResults = 0;
			int failResults = 0;
			Iterator<VerifiedAddress> it = results.iterator();
			while (it.hasNext()) {
				VerifiedAddress va = it.next();
				
				if (va.getStatus() != null && va.getStatus().equals("PASS")) {
					if (passResults >=3) {
						//remove this result from the list.  We only want 3 pass ones
						it.remove();
					} else {
						passResults ++;  //this is pass result and we are keeping it.  YAY!
					}
				} else if (va.getStatus() != null && va.getStatus().equals("FAIL")) {
					if (passResults >0) {
						//we don't need any fail results if we have at least one pass one.
						it.remove();
					} else if (failResults >0) {
						//we only need one fail result so remove this one
						it.remove();
					} else {
						//there are zero pass results and zero fail results.  keep this fail one!
						failResults ++;
					}
				}
			}
			logger.info("kept "+passResults+ " passing results and " +failResults + " failing results ");
			
		} else {
			//We have a zero sized results list.  This is ok.
		}
		
		return results;
	}

    public Document validateGiftCertificate(String certificateId) throws Exception {
        Document doc = null;
        logger.debug("validateGiftCertificate(" + certificateId + ")");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;

            RetrieveGiftCodeResponse retrieveGiftCodeResponse = null;
            retrieveGiftCodeResponse = GiftCodeUtil.getGiftCodeById(certificateId);
            if(retrieveGiftCodeResponse != null){
            	String couponStatus = retrieveGiftCodeResponse.getStatus();
                logger.info("couponStatus: " + couponStatus);      
                if (couponStatus != null && couponStatus.equalsIgnoreCase("Redeemed")) {
                    doc = OEActionBase.createErrorResultSet("gift_certificate", "Gift certificate has previously been redeemed.");
                } else if (couponStatus != null && couponStatus.equalsIgnoreCase("Issued Expired")) {
                    doc = OEActionBase.createErrorResultSet("gift_certificate", "Gift certificate has been expired.");
                } else if ( couponStatus != null && ( couponStatus.equalsIgnoreCase("Refund") || couponStatus.equalsIgnoreCase("Cancelled") )){
                    doc = OEActionBase.createErrorResultSet("gift_certificate", "Invalid Gift Certificate.");
                }else {
                    doc = JAXPUtil.createDocument();
                    Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                    doc.appendChild(root);
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                    JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                    Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
                    rowset.setAttribute(OrderEntryConstants.TAG_NAME, "gift_certificate");
                    root.appendChild(rowset);
                    JAXPUtil.addAttribute(rowset, OrderEntryConstants.TAG_STATUS, "Y");
                    JAXPUtil.addAttribute(rowset, OrderEntryConstants.TAG_MESSAGE, "");

                    Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                    row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(1));
                    rowset.appendChild(row);

                    Element value = doc.createElement("status");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(couponStatus));
                    
                    String amount = String.valueOf(retrieveGiftCodeResponse.getRemainingAmount());
                    value = doc.createElement("certificate-amount");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(amount));
                }
            } else {
            	doc = OEActionBase.createErrorResultSet("gift_certificate", "Gift Code Service is currently unavailable.  Please obtain a different payment type for the order or redeem manually after order placement.");
            }

        }catch(IllegalStateException ex) {
        	doc = OEActionBase.createErrorResultSet("gift_certificate", "Error : " + ex.getMessage());
        }catch (Throwable t) {
            logger.error("Error in validateGiftCertificate()",t);
            throw new Exception("Invalid Gift Certificate.");
        } finally {
        }


        return doc;

    }
    
    private void getBillingInfo(Connection conn, String sourceCode, Document doc) {

        NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
        if (node.getLength() > 0) {
            Element resultNode = (Element) node.item(0);

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "billing_info");
            String status = "N";
            String message = OrderEntryConstants.RESULTS_EMPTY;

            try {
                CachedResultSet cr = orderEntryDAO.getBillingInfo(conn, sourceCode);
                int i=0;
                while (cr.next()) {

                    status = "Y";
                    message = "";

                    Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                    i += 1;
                    row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i));
                    rowset.appendChild(row);
                    
                    String billingInfoSequence = cr.getString("billing_info_sequence");
                    String infoDescription = cr.getString("info_description");
                    String infoDisplay = cr.getString("info_display");
                    if (infoDisplay == null) infoDisplay = infoDescription;
                    String infoFormat = cr.getString("info_format");
                    String promptType = cr.getString("prompt_type");
                    
                    Element value = doc.createElement("sequence");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(billingInfoSequence));

                    value = doc.createElement("gift-certificate");
                    row.appendChild(value);
                    if (infoDisplay.equalsIgnoreCase(CERTIFICATE_VALUE)) {
                        value.appendChild(doc.createTextNode("Y"));
                    } else {
                        value.appendChild(doc.createTextNode("N"));
                    }

                    value = doc.createElement("display-prompt");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(infoDisplay));

                    value = doc.createElement("prompt-type");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(promptType));

                    value = doc.createElement("info-format");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(infoFormat));

                    value = doc.createElement("info-description");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(infoDescription));

                    value = doc.createElement("validation-regex");
                    row.appendChild(value);
                    value.appendChild(doc.createTextNode(cr.getString("validation_regex")));

                    if (promptType != null && (promptType.equalsIgnoreCase(DROPDOWN) || 
                        promptType.equalsIgnoreCase(STATIC))) {

                        Element options = doc.createElement("options");
                        row.appendChild(options);

                        CachedResultSet cr2 = orderEntryDAO.getBillingInfoOptions(conn, sourceCode, infoDescription);
                        int j=0;
                        while (cr2.next()) {
                            String optionName = cr2.getString("option_name");
                            String optionValue = cr2.getString("option_value");
                            String sequence = cr2.getString("option_sequence");

                            Element option = doc.createElement("option");
                            j += 1;
                            option.setAttribute(OrderEntryConstants.TAG_NUM, Integer.toString(j));
                            options.appendChild(option);
                            
                            value = doc.createElement("name");
                            option.appendChild(value);
                            value.appendChild(doc.createTextNode(optionName));

                            value = doc.createElement("value");
                            option.appendChild(value);
                            value.appendChild(doc.createTextNode(optionValue));

                            value = doc.createElement("sequence");
                            option.appendChild(value);
                            value.appendChild(doc.createTextNode(sequence));
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }

            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, status);
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, message);

            resultNode.appendChild(rowset);
        }
    }

    private void getPartnerData(Connection conn, String secToken, Document doc) {

        logger.debug("getPartnerData()");

        NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
        if (node.getLength() > 0) {

            Element resultNode = (Element) node.item(0);

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "partner_permission");
            String status = "Y";
            String message = "";
            String partnerName = "";
            boolean isPreferredPartner = false;
        
            try {

                String resourceId = "";
                NodeList nl = doc.getElementsByTagName("resource_id");
                if (nl.getLength() > 0) {
                    Element iotw = (Element) nl.item(0);
                    if (iotw.hasChildNodes()) {
                        Node n = iotw.getFirstChild();
                        String value = n.getNodeValue();
                        if (value != null && !value.equals("")) {
                            isPreferredPartner = true;
                            resourceId = value;
                            status = "N";
                            boolean csrPermission = SecurityManager.getInstance().assertPermission("Order Proc", secToken, resourceId, "View");
                            logger.debug("csrPermission: " + csrPermission);
                            if (csrPermission) {
                                status = "Y";
                            } else {
                                status = "N";
                                String transferNumber = orderEntryDAO.getContentWithFilter(conn, "PREFERRED_PARTNER", "TRANSFER_EXTENSION", resourceId, null);
                                message = transferNumber;
                            }
                        }
                    }
                }

                if (isPreferredPartner) {
                    nl = doc.getElementsByTagName("partner_name");
                    if (nl.getLength() > 0) {
                        Element iotw = (Element) nl.item(0);
                        if (iotw.hasChildNodes()) {
                            Node n = iotw.getFirstChild();
                            String value = n.getNodeValue();
                            if (value != null && !value.equals("")) {
                                partnerName = value.toUpperCase(); 
                                CachedResultSet cr = orderEntryDAO.getPreferredPartnerInfoByPartner(conn, partnerName);
                                Element rowsetForPartnerScript = doc.createElement(OrderEntryConstants.TAG_RS);
                                rowsetForPartnerScript.setAttribute(OrderEntryConstants.TAG_NAME, "partner_script");
                                resultNode.appendChild(rowsetForPartnerScript); 
                                int i = 0;
                                while (cr.next()){
                                  Element record = doc.createElement(OrderEntryConstants.TAG_RECORD);
                                  record.setAttribute(OrderEntryConstants.TAG_RECORD, Integer.toString(i));
                                  rowsetForPartnerScript.appendChild(record);
                                 
                                  Element scriptID = doc.createElement("script_id");
                                  scriptID.appendChild(doc.createTextNode(cr.getString("filter_2_value")));
                                  record.appendChild(scriptID);   
                                  
                                  Element scriptValue = doc.createElement("script_value");
                                  scriptValue.appendChild(doc.createTextNode(cr.getString("content_txt")));
                                  record.appendChild(scriptValue);
                                  i++;
                              }      
                            } else {
                                partnerName = "Preferred Partner";
                            }
                        }
                    }
                }

            } catch (Exception e) {
                logger.error(e);
            }

            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, status);
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, message);
            rowset.setAttribute(OrderEntryConstants.TAG_TYPE, partnerName);
            resultNode.appendChild(rowset);
            
        }
    }
    
    
    private void logServiceResponseTracking(Connection con, String requestId, String serviceMethod, long startTime) {      
       try {
          long responseTime = System.currentTimeMillis() - startTime;
          ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
          srtVO.setServiceName(FRESH_SERVICE_PRODUCT_PULL);
          srtVO.setServiceMethod(serviceMethod);
          srtVO.setTransactionId(requestId);
          srtVO.setResponseTime(responseTime);
          srtVO.setCreatedOn(new Date());
          ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
          srtUtil.insert(con, srtVO);
       } catch (Exception e) {
          logger.error("Error in logServiceResponseTracking " + e.getMessage());
       }
     }

    public void setResourceProvider(IResourceProvider resourceProvider) {
        FTDValidationBO.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public void setOrderEntryDAO(OrderEntryDAO orderEntryDAO) {
    	FTDValidationBO.orderEntryDAO = orderEntryDAO;
    }

    public OrderEntryDAO getOrderEntryDAO() {
        return orderEntryDAO;
    }
}
