package com.ftd.oe.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.customerordermanagement.vo.CommentsVO;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.J2EEResourceProvider;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.interfaces.OrderBO;
import com.ftd.oe.mo.bo.UpdateOrderBO;
import com.ftd.oe.mo.constants.UpdateOrderConstants;
import com.ftd.oe.mo.dao.UpdateOrderDAO;
import com.ftd.oe.mo.vo.FTDMessageVO;
import com.ftd.oe.mo.vo.MessageVO;
import com.ftd.oe.mo.vo.UpdateOrderVO;
import com.ftd.oe.util.CalculateOrderTotalsUtil;
import com.ftd.oe.util.CreditCardUtil;
import com.ftd.oe.util.MessagingServiceLocator;
import com.ftd.oe.util.OrderUtil;
import com.ftd.oe.util.ValidationUtil;
import com.ftd.oe.vo.AddonVO;
import com.ftd.oe.vo.CoBrandVO;
import com.ftd.oe.vo.CreditCardVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.PersonalGreetingVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.AddOnVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.SecondChoiceVO;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.OccasionHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourcePartnerBinHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.xml.JAXPUtil;

public class FTDOrderBO implements OrderBO {


	private static Logger logger = new Logger("com.ftd.oe.bo.FTDOrderBO");
    //private static IResourceProvider resourceProvider;
    J2EEResourceProvider resourceProvider = new J2EEResourceProvider();
    private static OrderEntryDAO orderEntryDAO = new OrderEntryDAO();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat sdfDOW = new SimpleDateFormat("EEE");
    private static String MERCURY_OPEN = "MO";
    private static String MERCURY_MANUAL = "MM";
    private static int LINE_LENGTH = 54;
    
    public FTDOrderBO() {
    }

    @SuppressWarnings("unchecked")
	public Document submitCart(Document orderDoc, String csrId) throws Exception {
        Connection qConn = null;
        Document doc;
        logger.debug("submitOrder()");
        
        Document tempDoc = JAXPUtil.createDocument();
        Node newNode = tempDoc.importNode(orderDoc.getFirstChild(), true);
        tempDoc.appendChild(newNode);
        Element element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_NUMBER).item(0);
        if (element != null) element.getParentNode().removeChild(element);
        element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_NC_APPROVAL_PASSWORD).item(0);
        if (element != null) element.getParentNode().removeChild(element);
        logger.info(JAXPUtil.toString(tempDoc));
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            qConn.setAutoCommit(false);

            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
            doc.appendChild(root);
            JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

            String masterOrderNumber = null;
            List externalOrderNumberList = new ArrayList();
            List personalGreetingList = new ArrayList();
            
            if (orderDoc != null) {

                OrderVO orderVO = OrderUtil.parseOrderXML(orderDoc);

                new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, true);

                String validationStatus = "Y";
                HashMap errorList = ValidationUtil.validateOrder(qConn, orderVO);
                HashMap tempErrorList = new HashMap();
                boolean sourceCodeChanged = false;
                String partnerName = null;
                BigDecimal totalProductPrice = new BigDecimal("0");
                BigDecimal totalAddonAmount = new BigDecimal("0");
                BigDecimal newMercTotal = new BigDecimal("0");
                String piValidationOverride = "N";
                boolean updatePartnerProduct = false;

                if (errorList == null || errorList.size() <= 0) {
                    String productId = null;
                    String zipCode = null;
                    String deliveryDate = null;
                    String state = null;
                    String countryId = null;
                    String sourceCode = null;
                    String shipMethod = null;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    CustomerVO custVO = null;
                    FTDValidationBO valBO = new FTDValidationBO();
                    Document availDoc = null;

                    List detailList = orderVO.getOdVO();
                    for (int i=0; i<detailList.size(); i++) {
                        OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                        productId = odVO.getProductId();
                        deliveryDate = sdf.format(odVO.getDeliveryDate());
                        custVO = odVO.getRecipientVO();
                        zipCode = custVO.getZipCode();
                        state = custVO.getState();
                        countryId = custVO.getCountry();
                        if (odVO.getItemSourceCode() != null) {
                        	sourceCode = odVO.getItemSourceCode();
                        } else {
                        	sourceCode = orderVO.getSourceCode();
                        }
                        shipMethod = odVO.getShipMethod();
                        if (shipMethod == null) shipMethod = "SD";
                        String addons = null;
                        if (odVO.getAddonVO() != null) {
                        	for (int j=0; j<odVO.getAddonVO().size(); j++) {
                        		AddonVO addonVO = (AddonVO) odVO.getAddonVO().get(j);
                        		if (addons == null) {
                        			addons = addonVO.getAddonId();
                        		} else {
                        			addons = addons + "," + addonVO.getAddonId();
                        		}
                        	}
                        }
                        // We don't need the detailed shipping charges at this point
                        boolean getChargesXML = false;
                        logger.info("Calling validateProductAvailability(" + productId + ", " + 
                            zipCode + ", " + deliveryDate + ", " + countryId + ", " + sourceCode +
                            ", " + addons + ")");
                        availDoc = valBO.validateProductAvailability(productId, zipCode, deliveryDate,
                        		countryId, sourceCode, null, orderDoc, getChargesXML, addons);
                        logger.info(JAXPUtil.toString(availDoc));
                        NodeList node = availDoc.getElementsByTagName(OrderEntryConstants.TAG_RS);
                        for (int j=0; j<node.getLength(); j++) {
                            Element temp = (Element)node.item(j);
                            String rsName = temp.getAttribute(OrderEntryConstants.TAG_NAME);
                            String rsStatus = temp.getAttribute(OrderEntryConstants.TAG_STATUS);
                            if (rsName != null && rsName.equalsIgnoreCase(OrderEntryConstants.TAG_VALIDATION)) {
                                if (rsStatus != null && rsStatus.equalsIgnoreCase("N")) {
                                    NodeList nl1 = temp.getElementsByTagName("record");
                                    NodeList nl2 = nl1.item(0).getChildNodes();
                                    Element e = (Element)nl2.item(0);
                                    if (e.getNodeName().equalsIgnoreCase("validate")) {
                                        String message = e.getAttribute("message");
                                        errorList.put("Item#" + (i+1), message);
                                        logger.error("PAS error for Item#" + (i+1) + " : " + message);
                                    }
                                }
                            }
                            if (rsName != null && rsName.equalsIgnoreCase(OrderEntryConstants.TAG_SHIP_METHODS)) {
                                logger.info("shipMethod: " + shipMethod);
                                boolean shipMethodFound = false;
                                NodeList nl1 = temp.getElementsByTagName("record");
                                NodeList nl2 = nl1.item(0).getChildNodes();
                             	for (int k=0; k<nl2.getLength(); k++) {
                              		Element smElement = (Element) nl2.item(k);
                                    String name = smElement.getNodeName();
                                    String value = "";
                                    if (smElement.hasChildNodes()) {
                                        Node n = smElement.getFirstChild();
                                        value = n.getNodeValue();
                                        if (name == null) name = "";
                                        if (value == null) value = "";
                                  		if (name.equalsIgnoreCase("florist")) {
                                  			if (shipMethod.equalsIgnoreCase("SD") && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		} else if (name.equalsIgnoreCase("carrierND")) {
                                  			if ((shipMethod.equalsIgnoreCase("ND")) && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		} else if (name.equalsIgnoreCase("carrier2F")) {
                                  			if ((shipMethod.equalsIgnoreCase("2F")) && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		} else if (name.equalsIgnoreCase("carrierGR")) {
                                  			if ((shipMethod.equalsIgnoreCase("GR")) && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		} else if (name.equalsIgnoreCase("carrierSA")) {
                                  			if ((shipMethod.equalsIgnoreCase("SA")) && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		}else if (name.equalsIgnoreCase("carrierSU")) {
                                  			if ((shipMethod.equalsIgnoreCase("SU")) && value.equalsIgnoreCase("Y")) {
                                  				shipMethodFound = true;
                                  				break;
                                  			}
                                  		}
                                    }
                               	}
                             	if (!shipMethodFound) {
                             		String message = "Product is no longer available for the selected delivery method. " +
                             		    "Return to Tab#1 and reselect your options.";
                             		errorList.put("Item#" + (i+1), message);
                             		logger.error(message);
                             	}
                            }
                            if (rsName != null && rsName.equalsIgnoreCase("product-availability")) {
                                if (rsStatus != null && rsStatus.equalsIgnoreCase("N")) {
                                    String rsMessage = temp.getAttribute(OrderEntryConstants.TAG_MESSAGE);
                                    errorList.put("Item#" + (i+1), rsMessage);
                                    logger.error("PAS error for Item#" + (i+1) + " : " + rsMessage);
                                }
                            }
                        }
                        
                        totalProductPrice = totalProductPrice.add(odVO.getProductPrice());
                        totalAddonAmount = totalAddonAmount.add(odVO.getAddonAmount());

                    }
                }
                
                if (errorList == null || errorList.size() <= 0) {

                    CreditCardVO ccVO = orderVO.getCcVO();
                    
                    
                    
                    
                    String sourceCode = orderVO.getSourceCode();
                    logger.debug("Starting Bin Processing: " + sourceCode);
                    if (sourceCode != null) {
                        SourcePartnerBinHandler spbHandler = (SourcePartnerBinHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_PARTNER_BIN");
                        partnerName = spbHandler.getSourceBinPartner(sourceCode, orderVO.getCcVO().getCreditCardNumber());
                        logger.debug("partnerName: " + partnerName);
                        if (partnerName != null) {
                        	
                        	boolean ignoreBinProcessing = "USAA".equalsIgnoreCase(partnerName) 
                							&& hasFSMembershipSKUItemOrWithFSApplied(qConn, orderVO);
                        	if(ignoreBinProcessing)
                        	{
                        		logger.debug("Skipping Bin Processing because Order is placed using USAA BIN CreditCard and has item with FSM SKU or FS Applied.");
                        	}
                        	else
                        	{
                        		logger.debug("Comparing source codes");
                                CachedResultSet cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                                if (cr.next()) {
                                    String oldOrderSource = cr.getString("order_source");
                                    PartnerVO partnerVO = FTDCommonUtils.getPartnerByName(partnerName, qConn);
                                    logger.debug("partnerVO: " + partnerVO.getPartnerName() + " " + partnerVO.getDefaultWebSourceCode() + " " + partnerVO.getDefaultPhoneSourceCode());
                                    String newSourceCode = partnerVO.getDefaultWebSourceCode();
                                    if (oldOrderSource != null && oldOrderSource.equalsIgnoreCase("P")) {
                                        newSourceCode = partnerVO.getDefaultPhoneSourceCode();
                                    }
                                    logger.debug("oldSourceCode: " + sourceCode + " " + oldOrderSource + " " + newSourceCode);
                                    BigDecimal oldTotal = orderVO.getOrderAmount();
                                    logger.debug("old total: " + oldTotal);
                                    BigDecimal newTotal = new BigDecimal("0");

                                    List detailList = orderVO.getOdVO();
                                    for (int i=0; i<detailList.size(); i++) {
                                        OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                                      
                                        OrderVO myClone = new OrderVO();
                                        
                                        OrderDetailVO newItem = (OrderDetailVO) odVO.clone();
                                        newItem.setItemSourceCode(newSourceCode);
                                        myClone.setOdVO(newItem);
                                        myClone.setSourceCode(newSourceCode);
                                        
                                        myClone.setBuyerVO(orderVO.getBuyerVO());;
                                        myClone.setCompany(orderVO.getCompany());
                                        myClone.setOrigin(orderVO.getOrigin());
                                        myClone.setOrderDate(orderVO.getOrderDate());
                                        
                                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, myClone, orderEntryDAO, true);
                                        logger.debug("old item total: " + odVO.getLineItemTotal());
                                        logger.debug("new item total: " + myClone.getOrderAmount());
                                        BigDecimal newVal = myClone.getOrderAmount();
                                        newTotal = newTotal.add(newVal);
                                    }
                                    logger.debug("newtotal: " + newTotal);
                                    if (newTotal.compareTo(oldTotal) <= 0) {
                                        logger.debug("new total is less");
                                        for (int i=0; i<detailList.size(); i++) {
                                            OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                                            odVO.setItemSourceCode(newSourceCode);
                                            odVO.setBinSourceChangedFlag("Y");
                                        }
                                        orderVO.setSourceCode(newSourceCode);
                                        CustomerVO buyerVO = orderVO.getBuyerVO();
                                        buyerVO.setMembershipType(null);
                                        buyerVO.setMembershipId(null);
                                        buyerVO.setMembershipFirstName(null);
                                        buyerVO.setMembershipLastName(null);
                                        sourceCodeChanged = true;
                                    }

                                    if (sourceCodeChanged) {
                                        logger.debug("Source Code was changed, recalcing");
                                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, true);

                                        // Rebuild CreditCardVO amounts
                                        String creditCardType = ccVO.getCreditCardType();
                                        if (creditCardType != null && !creditCardType.equalsIgnoreCase("IN")) {
                                            logger.debug("CC Approval amount: " + ccVO.getApprovalAmount());
                                            BigDecimal newApprovalAmount = new BigDecimal(0);
                                            BigDecimal orderAmount = orderVO.getOrderAmount();
                                            logger.debug("orderAmount: " + orderAmount);
                                            BigDecimal ncAmount = ccVO.getNoChargeAmount();
                                            logger.debug("ncAmount: " + ncAmount);
                                            BigDecimal gcAmount = ccVO.getGiftCertificateAmount();
                                            logger.debug("gcAmount: " + gcAmount);
                                            if (ncAmount != null && ncAmount.intValue() > 0) {
                                                orderAmount = orderAmount.subtract(ncAmount);
                                                logger.debug("nc new orderAmount: " + orderAmount);
                                            }
                                            if (gcAmount != null && gcAmount.intValue() > 0) {
                                                orderAmount = orderAmount.subtract(gcAmount);
                                                logger.debug("gc new orderAmount: " + orderAmount);
                                            }
                                            if (orderAmount.doubleValue() > 0) {
                                                newApprovalAmount = orderAmount;
                                                logger.debug("newApprovalAmount: " + newApprovalAmount);
                                            }
                                            if (ccVO.getApprovalAmount() == null ||
                                                !ccVO.getApprovalAmount().equals(newApprovalAmount.toString())) {

                                                ccVO.setApprovalAmount(newApprovalAmount.toString());
                                                logger.debug("CC Approval amount changed to: " + ccVO.getApprovalAmount());
                                            }
                                        }

                                    }
                                }
                        	}
                            
                        }

                    }

                    ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    String maxItemTotal = "0";
                    String maxLineItems = "0";
                    String maxItemAddonTotal = "0";
                    String maxOrderTotal = "0";
                    String preferredPartner = null;

                    // Determine if any Preferred Partner (e.g., USAA) items
                    List odList = orderVO.getOdVO();
                    for (int i=0; i<odList.size(); i++) {
                        OrderDetailVO odVO = (OrderDetailVO) odList.get(i);
                        CachedResultSet cr = orderEntryDAO.getPreferredPartnerBySourceCode(qConn, odVO.getItemSourceCode());
                        if (cr.next()) {
                            preferredPartner = cr.getString("partner_name");
                            break;
                        }
                    }

                    // If the order is associated with a Preferred Partner, obtain the appropriate thresholds
                    if (preferredPartner != null) {
                        logger.debug("Using 'max' thresholds for Preferred Partner: " + preferredPartner);
                        maxItemTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            preferredPartner + "_MAX_ITEM_TOTAL");
                        maxItemAddonTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            preferredPartner + "_MAX_ITEM_ADDON_TOTAL");
                        maxOrderTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            preferredPartner + "_MAX_ORDER_TOTAL");                    
                        if(orderVO.isHasLuxuryItem()) {
                            maxLineItems = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                                OrderEntryConstants.LUX_MAX_LINE_ITEMS);
                        } else {
                            maxLineItems = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                                OrderEntryConstants.MAX_LINE_ITEMS);
                        }
                    // Check against the lux threshold if there is a luxury item in cart.
                    } else if(orderVO.isHasLuxuryItem()) {
                        maxItemTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.LUX_MAX_ITEM_TOTAL);
                        maxLineItems = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.LUX_MAX_LINE_ITEMS);
                        maxItemAddonTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.LUX_MAX_ITEM_ADDON_TOTAL);
                        maxOrderTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.LUX_MAX_ORDER_TOTAL);
                    } else {
                        maxItemTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.MAX_ITEM_TOTAL);
                        maxLineItems = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.MAX_LINE_ITEMS);
                        maxItemAddonTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.MAX_ITEM_ADDON_TOTAL);
                        maxOrderTotal = cu.getFrpGlobalParm(OrderEntryConstants.VALIDATION_CONTEXT,
                            OrderEntryConstants.MAX_ORDER_TOTAL);
                    }
                    
                    logger.debug("maxItemTotal: " + maxItemTotal);
                    logger.debug("maxLineItems: " + maxLineItems);
                    logger.debug("maxItemAddonTotal: " + maxItemAddonTotal);
                    logger.debug("maxOrderTotal: " + maxOrderTotal);

                    if (orderVO.getFraudFlag() != null && orderVO.getFraudFlag().equalsIgnoreCase("on")) {
                        orderVO.setFraudFlag("Y");
                        orderVO.setFraudId("OE");
                        orderVO.setFraudComments(getFraudComments(qConn, orderVO.getFraudId()));
                    } else if (maxOrderTotal != null && (orderVO.getOrderAmount().compareTo(new BigDecimal(maxOrderTotal)) > 0)) {
                        orderVO.setFraudFlag("Y");
                        orderVO.setFraudId("HD");
                        orderVO.setFraudComments(getFraudComments(qConn, orderVO.getFraudId()));
                    } else {
                        List detailList = orderVO.getOdVO();
                        if (maxLineItems != null && (detailList.size() >= Integer.parseInt(maxLineItems))) {
                            orderVO.setFraudFlag("Y");
                            orderVO.setFraudId("LI");
                            orderVO.setFraudComments(getFraudComments(qConn, orderVO.getFraudId()));
                        } else {
                            for (int i=0; i<detailList.size(); i++) {
                                OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                                if (maxItemTotal != null && (odVO.getLineItemTotal().compareTo(new BigDecimal(maxItemTotal)) > 0)) {
                                    orderVO.setFraudFlag("Y");
                                    orderVO.setFraudId("HD");
                                    orderVO.setFraudComments(getFraudComments(qConn, orderVO.getFraudId()));
                                    break;
                                } else if (maxItemAddonTotal != null && (odVO.getAddonAmount().compareTo(new BigDecimal(maxItemAddonTotal)) > 0)) {
                                    orderVO.setFraudFlag("Y");
                                    orderVO.setFraudId("HA");
                                    orderVO.setFraudComments(getFraudComments(qConn, orderVO.getFraudId()));
                                    break;
                                }
                            }
                        }
                    }
                    
                    if (ccVO.isAuthorizationRequired()) {
                        logger.debug("cc amount: " + ccVO.getApprovalAmount());

                        String authActive = cu.getFrpGlobalParm(OrderEntryConstants.CCAS_CONTEXT,
                            OrderEntryConstants.CCAS_AUTH_ACTIVE);

                        if (authActive != null && authActive.equalsIgnoreCase("Y")) {

                            // perform cc auth
                            //CreditCardUtil.authorize(ccVO, qConn);
                            CreditCardUtil.authorize(ccVO, orderVO.getBuyerVO(), qConn);
                            String status = ccVO.getStatus();
                            String ccValidationStatus = ccVO.getValidationStatus();
                            String cscResponseCode = ccVO.getCscResponseCode();
                            String authResult = ccVO.getApprovalVerbiage();
                            boolean isUnderMin = ccVO.isUnderMinAuthAmt();

                            logger.debug("status: " + status);
                            logger.debug("ccValidationStatus: " + ccValidationStatus);
                            logger.debug("auth result: " + authResult);
                            logger.debug("under min auth: " + isUnderMin);

                            if (ccValidationStatus == null || ccValidationStatus.equalsIgnoreCase("E")) {
                                errorList.put("credit-card", "Credit card processing error.");
                                logger.error("Credit card processing error - status: " + status + " authResult: " + authResult);
                                validationStatus = "N";
                            } else if (ccValidationStatus.equalsIgnoreCase("D")) {
                                errorList.put("credit-card", "Credit card declined.");
                                logger.error("Credit card declined - status: " + status + " authResult: " + authResult);
                                validationStatus = "N";
                            } else if (isUnderMin) {
                                ccVO.setNoChargeType("AUTH");
                                ccVO.setNoChargeReason("AUTH UNDER MIN AMOUNT");
                                ccVO.setNoChargeAmount(new BigDecimal(ccVO.getApprovalAmount()));
                                ccVO.setCreditCardType(null);
                                ccVO.setCscValue(null);
                                ccVO.setCreditCardNumber(null);
                                ccVO.setCreditCardExpiration(null);
                                ccVO.setApprovalAmount(null);
                            }

                        }
                    }
                    
                    if (orderVO.isPiModifyOrderFlag()) {
                    	logger.info("Checking PI thresholds");
                    	logger.info("partnerId: " + orderVO.getPiPartnerId());
                    	logger.info("threshold: " + orderVO.getPiModifyOrderThreshold());
                    	logger.info("originalMercTotal: " + orderVO.getPiOriginalMercTotal());
                    	newMercTotal = totalProductPrice.add(totalAddonAmount);
                    	logger.info("newMercTotal: " + newMercTotal);
                    	BigDecimal totalDiff = newMercTotal.subtract(orderVO.getPiOriginalMercTotal());
                    	logger.info("totalDiff: " + totalDiff);
                    	int thresholdDiff = totalDiff.compareTo(orderVO.getPiModifyOrderThreshold());
                    	logger.info("thresholdDiff: " + thresholdDiff);
                    	if (thresholdDiff > 0) {
                            String contentMsg = orderEntryDAO.getContentWithFilter(qConn, "PARTNER_INTEGRATION", "THRESHOLD_MESSAGE_GREATER_THAN", orderVO.getPiPartnerId(), null);
                            String tempToken = String.format("%.2f", orderVO.getPiModifyOrderThreshold());
                            contentMsg = contentMsg.replaceAll("~threshold~", tempToken);
                    		errorList.put("threshold greater", contentMsg);
                    		validationStatus = "N";
                    	} else if (totalDiff.add(orderVO.getPiModifyOrderThreshold()).compareTo(new BigDecimal("0")) < 0) {
                            String contentMsg = orderEntryDAO.getContentWithFilter(qConn, "PARTNER_INTEGRATION", "THRESHOLD_MESSAGE_LESS_THAN", orderVO.getPiPartnerId(), null);
                    		tempErrorList.put("threshold less", contentMsg);
                    		piValidationOverride = "Y";
                    		validationStatus = "N";
                    	}
                    }

                    if (errorList == null || errorList.size() <= 0) {

                        // Order passes validation, persist to database
                    
                        InitialContext context = null;
                        UserTransaction userTransaction = null;
                        Connection utConn = null;
                        UpdateOrderVO updateOrderVO = null;
                    
                        try {
                            int transactionTimeout = Integer.parseInt(ConfigurationUtil.getInstance().getProperty("oe_config.xml", "transaction_timeout_in_seconds"));
                            String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty("oe_config.xml", "jndi_usertransaction_entry");
                        
                            context = new InitialContext();
                            userTransaction = (UserTransaction) context.lookup(jndi_usertransaction_entry);
                        
                            userTransaction.setTransactionTimeout(transactionTimeout);
                        
                            userTransaction.begin();

                            utConn = resourceProvider.getDatabaseConnection(dataSource);
                            if (utConn == null) {
                                throw new Exception("Unable to connect to database");
                            }

                            boolean success = true;
                            
                            if (orderVO.isPiModifyOrderFlag()) {
                            	OrderDetailVO odVO = (OrderDetailVO) orderVO.getOdVO().get(0);
                        		ResultTO result = null;
                        		String orderDetailId = odVO.getOriginalOrderDetailId();

                        		// check if a live mercury exists on the order
                        		UpdateOrderDAO uoDAO = new UpdateOrderDAO(utConn);
                        		CachedResultSet mercuryOrderMessageStatusInfo = uoDAO.getMercuryOrderMessageStatus(orderDetailId, null, "Y");
                        		String rejectSent = "N";
                        		String cancelSent = "N";
                        		String attemptedFTD = "N";
                        		String hasLiveFTD = "N";
                        		String ftdStatus = "N";
                        		if (mercuryOrderMessageStatusInfo.next()) {
                        			if (mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
                        				rejectSent = mercuryOrderMessageStatusInfo.getString("reject_sent").trim();
                        			if (mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
                        				cancelSent = mercuryOrderMessageStatusInfo.getString("cancel_sent").trim();
                        			if (mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
                        				attemptedFTD = mercuryOrderMessageStatusInfo.getString("attempted_ftd").trim();
                        			if (mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
                        				hasLiveFTD = mercuryOrderMessageStatusInfo.getString("has_live_ftd").trim();
                        			if (mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
                        				ftdStatus = mercuryOrderMessageStatusInfo.getString("ftd_status").trim();
                        		} else {
                        			logger.debug("No prior message on order.  Order detail id = " + orderDetailId);
                        		}
                                
                        		// only send CAN for orders that have an attempted FTD associated with
                        		// them
                        		// and if a CAN or REJ was not sent on the order
                        		
                        		if ((attemptedFTD != null && attemptedFTD.equalsIgnoreCase("Y"))
                        				&& (rejectSent == null || rejectSent.equalsIgnoreCase("N"))
                        				&& (cancelSent == null || cancelSent.equalsIgnoreCase("N"))) {

                        			logger.info("Attempted FTD, no rejects or cancels. Creating CAN.");

                        			// check if ADJ needs to be sent based on delivery date
                        			boolean adjustmentNeed = false;
                        			if (checkDeliveryDate(odVO.getOriginalDeliveryDate()) == true) {
                        				adjustmentNeed = true;
                        				logger.debug("Based on date, ADJ needed.");
                        			} else {
                        				logger.debug("Based on date, ADJ not needed");
                        			}

                        			// check what type of order this orig one is
                        			String originalShipMethod = odVO.getOriginalShipMethod();
                        			logger.info("originalShipMethod: " + originalShipMethod);
                        			if (originalShipMethod == null || originalShipMethod.equals("") ||
                        					originalShipMethod.equalsIgnoreCase("SD")) {
                        				// get the mercury message associated with the FTD
                        				MessageVO messageVO = uoDAO.getMessageDetailLastFTD(
                        						orderDetailId, UpdateOrderConstants.MERCURY_MESSAGE);
                        				logger.info("messageId: " + messageVO.getMessageId());

                        				// build mercury cancel TO
                        				CANMessageTO canTO = new CANMessageTO();
                        				canTO.setMercuryId(messageVO.getMessageId());
                        				canTO.setComments(UpdateOrderConstants.CANCEL_MSG_COMMENT);
                        				canTO.setOperator(csrId);
                        				canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
                        				canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                        				canTO.setFillingFlorist(messageVO.getFillingFloristCode());
                        				canTO.setSendingFlorist(messageVO.getSendingFloristCode());

                        				if (adjustmentNeed) {

                        					/*ConfigurationUtil configUtil = ConfigurationUtil
                        							.getInstance();*/
                        					String adjReasonCode = UpdateOrderConstants.MODIFY_ORDER_ADJ_REASON;

                        					// combined report number = month + "-1"
                        					int month =	odVO.getOriginalDeliveryDate().getMonth() + 1;
                        					String sMonth = String.valueOf(month);
                        					// make the month 2 digits
                        					if (sMonth.length() == 1) {
                        						sMonth = '0' + sMonth;
                        					}
                        					String combindedReportNumber = sMonth + "-1";

                        					ADJMessageTO adjTO = new ADJMessageTO();
                        					adjTO.setMercuryId(messageVO.getMessageId());
                        					adjTO.setComments(UpdateOrderConstants.ADJ_MSG_COMMENT);
                        					adjTO.setCombinedReportNumber(combindedReportNumber);
                        					adjTO.setOperator(csrId);
                        					adjTO.setMercuryStatus(UpdateOrderConstants.MERCURY_ORDER_STATUS);
                        					adjTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
                        					adjTO.setOldPrice(new Double(messageVO.getCurrentPrice()));
                        					adjTO.setOverUnderCharge(new Double("0.00"));
                        					adjTO.setAdjustmentReasonCode(adjReasonCode);
                        					adjTO.setSendingFlorist(messageVO.getSendingFloristCode());
                        					adjTO.setFillingFlorist(messageVO.getFillingFloristCode());

                        					result = MessagingServiceLocator.getInstance().getMercuryAPI().sendADJMessage(adjTO, utConn);

                        					// check for errors
                        					if (!result.isSuccess()) {
                        						throw new Exception(
                        								"Could not create Mercury ADJ message (order detail id="
                        										+ orderDetailId + "). ERROR: "
                        										+ result.getErrorString());
                        					} else {
                        						logger.debug("ADJ sent - florist");
                        					}

                        				}

                        				// send mercury cancel
                        				result = MessagingServiceLocator.getInstance().getMercuryAPI().sendCANMessage(canTO, utConn);

                        				// check for errors
                        				if (!result.isSuccess()) {
                        					throw new Exception(
                        							"Could not create Mercury CAN message (order detail id="
                        									+ orderDetailId + "). ERROR: "
                        									+ result.getErrorString());
                        				}
                        			}
                        		}
                        		
    							try {
    								logger.info(orderDetailId + ": Attempting RWD");
    								/* select florist */
    								RWDTO rwdTO=new RWDTO();
    								
    								CustomerVO recipVO = odVO.getRecipientVO();
    					            rwdTO.setOrderDetailId(orderDetailId);
    					            rwdTO.setDeliveryCity(recipVO.getCity());
    					            rwdTO.setDeliveryState(recipVO.getState());
    					            rwdTO.setZipCode(recipVO.getZipCode().toUpperCase());
    					            rwdTO.setReturnAll(true);
    					            BigDecimal productTotal = odVO.getProductPrice();
    					            rwdTO.setOrderValue(productTotal.doubleValue());
    					            rwdTO.setProduct(odVO.getProductId());
    					            rwdTO.setDeliveryDate(odVO.getDeliveryDate());
    					            //rwdTO.setDeliveryDateEnd(deliveryDateEnd);
    					            rwdTO.setSourceCode(sourceCode);
    					            rwdTO.setRWDFlagOverride(Boolean.TRUE);
    					            logger.info("city: " + rwdTO.getDeliveryCity());
    					            logger.info("state: " + rwdTO.getDeliveryState());
    					            logger.info("zip code: " + rwdTO.getZipCode());
    					            logger.info("product: " + rwdTO.getProduct());
    					            logger.info("delivery date: " + sdf.format(rwdTO.getDeliveryDate()));

    					            String fillingFlorist = null;
    					            String selectionData = null;
    					            String floristSelectionLogId = null;
    					            OrderDAO dao = new OrderDAO(utConn);
									com.ftd.op.order.vo.ProductVO product = dao.getProduct(rwdTO.getProduct());
									logger.info(rwdTO.getProduct() + " " + product.getCodifiedFlag());

									CachedResultSet cr = orderEntryDAO.getOrderFloristUsed(utConn, orderDetailId);
    					            RWDFloristTOList floristTO = MessagingServiceLocator.getInstance().getOrderAPI().getFillingFloristList(rwdTO, utConn);
									List floristList = floristTO.getFloristList();
								    for (int i=0; i< floristList.size(); i++) {
										RWDFloristTO fTO = (RWDFloristTO) floristList.get(i);
								    	logger.info(fTO.getFloristId() + " " + fTO.getCutoffTime() + " cod: " + fTO.getCodification() + " cod block: " +
								    			fTO.isCodificationBlockFlag() + " Sun: " + fTO.getSundayDeliveryFlag() +
								    			" Suspended: " + fTO.getFloristSuspended());
								    }
									while (cr.next()) {
										if (fillingFlorist == null) {
										    String floristId = cr.getString("florist_id");
										    logger.info("Checking order florist used for " + floristId);
										    for (int i=0; i< floristList.size(); i++) {
    											RWDFloristTO fTO = (RWDFloristTO) floristList.get(i);
	    										if (fTO.getFloristId().equals(floristId)) {
	    											if (fTO.getFloristSuspended() == null || fTO.getFloristSuspended().equalsIgnoreCase("N")) {
	    												if (checkFloristCutoff(fTO.getCutoffTime(), rwdTO.getDeliveryDate())) {
	    													if (product.getCodifiedFlag() == null) {
	    														fillingFlorist = floristId;
	    														selectionData = "FEMOE";
	    														logger.info("Using previous florist " + floristId);
	    														break;
	    													} else if (fTO.getCodification() != null && !fTO.isCodificationBlockFlag()) {
	    														fillingFlorist = floristId;
	    														selectionData = "FEMOE";
	    														logger.info("Using previous codified florist " + floristId);
	    														break;
	    													}
	    												}
	    											} else {
	    												logger.info("Skipping suspended florist");
	    											}
											    }
										    }
										}
									}
									
									if (fillingFlorist == null) {
    					                OrderService os = new OrderService(utConn);
    					                rwdTO.setReturnAll(false);
    					                RWDFloristVO florist = os.selectFlorist(rwdTO);
    					                if (florist != null) {
        					                logger.info("order service florist = " + florist.getFloristId() + " " + florist.getCodification() + " " +
    					                            florist.getZipCityFlag() + " " + florist.getFloristSelectionLogId());
    					                	fillingFlorist = florist.getFloristId();
    					                	selectionData = florist.getZipCityFlag();
    					                	floristSelectionLogId = florist.getFloristSelectionLogId();
    					                }
									}
									logger.info("fillingFlorist: " + fillingFlorist);
									odVO.setFloristId(fillingFlorist);

    								if (fillingFlorist != null) {
    									SecondChoiceVO secondChoiceVO = dao.getSecondChoice(product.getSecondChoiceCode());
    									SourceVO sourceVO = dao.getSource(odVO.getItemSourceCode());

                                        FloristVO floristVO = dao.getFlorist(fillingFlorist);
    						            String allowMessageForwarding = floristVO.getAllowMessageForwardingFlag();
    						            // default to N if not defined
    						            if (allowMessageForwarding == null) allowMessageForwarding = "N";
    						            
    						            FTDMessageVO ftdVO = uoDAO.getMessageDetailLastFTD(orderDetailId, "Mercury", "Y");
    						            if (ftdVO == null || ftdVO.getSendingFloristCode() == null ||
    						            		ftdVO.getSendingFloristCode().length() <= 0) {
    						            	logger.info("No previous FTD,get default sending florist");
    						            	ftdVO = new FTDMessageVO();
        						            String sendingFloristNumber = dao.retrieveDefaultFloristId(orderDetailId);
        						            ftdVO.setSendingFloristCode(sendingFloristNumber);
    						            }
    						            logger.info("sending florist: " + ftdVO.getSendingFloristCode());
    						            
    						            List addOnVOList = new ArrayList();
    						            for (int i=0; i< odVO.getAddonVO().size(); i++) {
    						            	AddonVO addonVO = (AddonVO) odVO.getAddonVO().get(i);
    						            	CachedResultSet cra = orderEntryDAO.getAddonById(utConn, addonVO.getAddonId());
    						            	String addonDescription = "N/A";
    						            	if (cra.next()) {
    						            		addonDescription = cra.getString("description");
    						            		if (addonVO.getAddonPrice() == null) {
    						            			addonVO.setAddonPrice(cra.getBigDecimal("price"));
    						            		}
    						            	}
    						            	logger.info(addonVO.getAddonId() + " " + addonDescription + " " +
    						            			addonVO.getAddonQuantity() + " " + addonVO.getAddonPrice());
    						            	AddOnVO tempAddon = new AddOnVO();
    						            	tempAddon.setAddOnCode(addonVO.getAddonId());
    						            	tempAddon.setAddOnQuantity(addonVO.getAddonQuantity());
    						            	tempAddon.setPrice(addonVO.getAddonPrice().doubleValue());
    						            	tempAddon.setDesciption(addonDescription);
    						            	addOnVOList.add(tempAddon);
    						            }

    						            FTDMessageTO ftdTO = new FTDMessageTO();

   						                java.sql.Date sqlDeliveryDate = new java.sql.Date(rwdTO.getDeliveryDate().getTime());
   						                ftdTO.setDeliveryDate(sqlDeliveryDate);
    						            SimpleDateFormat sdfm = new SimpleDateFormat("MMM dd");
    						            ftdTO.setDeliveryDateText(sdfm.format(rwdTO.getDeliveryDate()));
    						            java.sql.Date timeNow = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
    						            ftdTO.setOrderDate(timeNow);
    						            ftdTO.setRecipient(recipVO.getFirstName() + " " + recipVO.getLastName());
    						            String address = recipVO.getAddress();
    						            if (recipVO.getBusinessName() != null) {
    						            	address = address.concat("\n" + recipVO.getBusinessName());
    						            }
    						            ftdTO.setAddress(address);
    						            ftdTO.setCityStateZip(this.buildCityStateZipString(recipVO, dao));
    						            String phoneNumber = recipVO.getDayPhone();
    						            if (phoneNumber == null) {
    						            	phoneNumber = recipVO.getEveningPhone();
    						            }
    						            if (phoneNumber == null) {
    						            	phoneNumber = "000/000-0000";
    						            } else {
    						            	if (recipVO.getCountry() != null && recipVO.getCountry().equalsIgnoreCase("US") &&
    						            			phoneNumber.length() == 10) {
    						            		phoneNumber = phoneNumber.substring(0, 3) + "/" +
    						            		phoneNumber.substring(3, 6) + "-" +
    						            		phoneNumber.substring(6, 10);
    						            	}
    						            }
    						            ftdTO.setPhoneNumber(phoneNumber);
    						            ftdTO.setMessageType("FTD");
    						            ftdTO.setDirection("OUTBOUND");
    						            ftdTO.setCardMessage(breakString(odVO.getCardMessage(), LINE_LENGTH));
    						            String firstChoice = this.buildFirstChoiceString(odVO, product, addOnVOList, utConn);
    						            ftdTO.setFirstChoice(firstChoice);
    						            String secondChoice = this.buildSecondChoiceString(odVO, product, secondChoiceVO, addOnVOList, utConn);
    						            ftdTO.setSecondChoice(secondChoice);
    						            String lookupId = (odVO.getSizeIndicator() == null ? odVO.getProductId() : odVO.getProductId() + odVO.getSizeIndicator());
    						            String mercPriceOverride = cu.getFrpGlobalParm("ORDER_PROCESSING", "MERC-PRICE-OVERRIDE-" + lookupId);
    						            if (mercPriceOverride != null ) {
    						                try {
    						                	logger.info("Found MERC-PRICE-OVERRIDE: " + lookupId + " " + mercPriceOverride);
    						                	BigDecimal mercPriceBD = new BigDecimal(mercPriceOverride);
    						                	newMercTotal = mercPriceBD.add(totalAddonAmount);
    						                } catch (Exception e) {
    						                	logger.error("Could not convert MERC-PRICE-OVERRIDE, using pdb price");
    						                }
    						            }
    						            ftdTO.setPrice(newMercTotal.doubleValue());
    						            ftdTO.setOldPrice(null);
    						            ftdTO.setRequestConfirmation(sourceVO.getRequiresDeliveryConfirmation());
    						            ftdTO.setFillingFlorist(fillingFlorist);
    						            ftdTO.setOccasion(this.buildOccasionString(odVO.getOccasion(), recipVO, utConn));
    						            String specialInstructions = this.buildSpecialInstructionString(odVO, sourceVO, recipVO, product);
    						            ftdTO.setSpecialInstructions(specialInstructions);
    						            ftdTO.setOperator("FEMOE");
    						            ftdTO.setOrderDetailId(orderDetailId);
    						            ftdTO.setProductId(odVO.getProductId());
    						            ftdTO.setSendingFlorist(ftdVO.getSendingFloristCode());
    						            ftdTO.setZipCode(recipVO.getZipCode());

    						            // Set Mercury Status //
    						            if (floristVO.getMercuryFlag() != null) {
    						                if (floristVO.getMercuryFlag().equalsIgnoreCase("Y")) {
    						                    ftdTO.setMercuryStatus(MERCURY_OPEN);
    						                } else {
    						                    ftdTO.setMercuryStatus(MERCURY_MANUAL);
    						                }
    						            } else {
    						                ftdTO.setMercuryStatus(MERCURY_MANUAL);
    						            }

    						            //create Mercury FTD message TO.
    						            result = MessagingServiceLocator.getInstance().getMercuryAPI().sendFTDMessage(ftdTO, utConn);

    						            //check for errors
    						            if (!result.isSuccess())
    						            {
    						              logger.error("Cannot create Mercury FTD message: " + result.getErrorString());
    						              throw new Exception("Cannot create Mercury FTD message");
    						            }
    						            
    						            orderEntryDAO.insertOrderFloristUsed(utConn, orderDetailId, fillingFlorist, 
    						            		selectionData);
    						            
    						            logger.info("Original product id: " + odVO.getOriginalProductId());
    						            logger.info("New product id: " + odVO.getProductId());
    						            logger.info("Original delivery date: " + sdf.format(odVO.getOriginalDeliveryDate()));
    						            logger.info("New delivery date: " + sdf.format(odVO.getDeliveryDate()));
    						            String originalFloristId = orderVO.getPiOriginalFloristId();
    						            logger.info("Original florist id: " + originalFloristId);
    						            logger.info("New florist id: " + odVO.getFloristId());
    						            if (!odVO.getOriginalProductId().equals(odVO.getProductId()) ||
    						            		odVO.getOriginalDeliveryDate().compareTo(odVO.getDeliveryDate()) != 0 ||
    						            		(originalFloristId == null || !originalFloristId.equals(odVO.getFloristId()))) {
    						            	logger.info("Update order details with new info: " + odVO.getOriginalOrderDetailId());
    						            	HashMap outputMap = orderEntryDAO.updateOrderDetails(utConn, odVO, csrId);
    			                            String status = (String) outputMap.get("OUT_STATUS");
    	                                    String message = (String) outputMap.get("OUT_MESSAGE");
    	                                    logger.info("status: " + status + " - " + message);
    	        
    						            }
    						            
    						            String origOccasion = "";
    						            String origName = "";
    						            String origAddress = "";
    						            String origCity = "";
    						            String origState = "";
    						            String origZipCode = "";
    						            String origCountry = "";
    						            String origPhoneNumber = "";
    						            String origCardMessage = "";
    						            String origAddressType = "";
    						            String origBusinessName = "";
    						            String updateComment = null;
    						            String displayString = null;
    						            HashMap outMap = orderEntryDAO.getOriginalOrderInfo(utConn, odVO.getOriginalExternalOrderNumber());
    						            cr = (CachedResultSet) outMap.get("OUT_CUR_ORDER");
    						            if (cr.next()) {
    						            	origOccasion = cr.getString("occasion");
    						            	origName = cr.getString("recip_first_name") + " " + cr.getString("recip_last_name");
    						            	origAddress = cr.getString("recip_address_1");
    						            	origCity = cr.getString("recip_city");
    						            	origState = cr.getString("recip_state");
    						            	origZipCode = cr.getString("recip_zip_code");
    						            	origCountry = cr.getString("recip_country");
    						            	origPhoneNumber = cr.getString("recip_phone_number");
    						            	if (cr.getString("recip_phone_ext") != null && !cr.getString("recip_phone_ext").equals("")) {
    						            		origPhoneNumber = origPhoneNumber + " " + cr.getString("recip_phone_ext");
    						            	}
    						            	origCardMessage = cr.getString("card_message");
    						            	origAddressType = cr.getString("recip_address_type");
    						            	origBusinessName = cr.getString("recip_business_name");
    						            	if (origBusinessName == null) {
    						            		origBusinessName = "N/A";
    						            	}
    						            }
    						            HashMap addonMap = new HashMap();
    						            cr = (CachedResultSet) outMap.get("OUT_CUR_ADDON");
    						            while (cr.next()) {
    						            	addonMap.put(cr.getString("add_on_code"), cr.getString("add_on_quantity"));
    						            }
    						            List addonList = odVO.getAddonVO();
    						            if (addonList != null) {
    						                for (int a=0; a<addonList.size(); a++) {
    						                	AddonVO addonVO = (AddonVO) addonList.get(a);
    						            	    String mapQty = (String) addonMap.get(addonVO.getAddonId());
    						            	    if (mapQty == null) {
    						            	    	cr = orderEntryDAO.getAddonById(utConn, addonVO.getAddonId());
    						            	    	String addonDesc = addonVO.getAddonId();
    						            	    	if (cr.next()) {
    						            	    		addonDesc = cr.getString("description");
    						            	    	}
    	    						            	displayString = addonVO.getAddonQuantity() + " " + addonDesc + " added to order";
    	    						            	logger.debug(displayString);
    	    						            	if (updateComment == null) {
    	    						            	    updateComment = displayString;
    	    						            	} else {
    	    						            	    updateComment += "\n" + displayString;
    	    						            	}
    	    			                    		updatePartnerProduct = true;
    						            	    } else {
    						            	    	String addonQty = Integer.toString(addonVO.getAddonQuantity());
    						            	    	if (!mapQty.equals(addonQty)) {
        						            	    	cr = orderEntryDAO.getAddonById(utConn, addonVO.getAddonId());
        						            	    	String addonDesc = addonVO.getAddonId();
        						            	    	if (cr.next()) {
        						            	    		addonDesc = cr.getString("description");
        						            	    	}
    		    						            	displayString = addonDesc + " qty changed from " + mapQty + " to " + addonQty;
    		    						            	logger.debug(displayString);
    		    						            	if (updateComment == null) {
    		    						            	    updateComment = displayString;
    		    						            	} else {
    		    						            	    updateComment += "\n" + displayString;
    		    						            	}
    		    			                    		updatePartnerProduct = true;
    						            	    	}
    						            	    	addonMap.remove(addonVO.getAddonId());
    						            	    }
    						                }
    						            }
    						            if (addonMap != null && addonMap.size() > 0) {
    						            	for (Object key: addonMap.keySet()) {
    						            		String addonId = (String) key;
    						            		String mapQty = (String) addonMap.get(key);
						            	    	cr = orderEntryDAO.getAddonById(utConn, addonId);
						            	    	String addonDesc = addonId;
						            	    	if (cr.next()) {
						            	    		addonDesc = cr.getString("description");
						            	    	}
        						            	displayString = mapQty + " " + addonDesc + " removed from order";
        						            	logger.debug(displayString);
        						            	if (updateComment == null) {
        						            	    updateComment = displayString;
        						            	} else {
        						            	    updateComment += "\n" + displayString;
        						            	}
        			                    		updatePartnerProduct = true;
    						            	}
    						            }
    						            if (!odVO.getOriginalProductId().equals(odVO.getProductId())) {
    						            	displayString = "Product changed from " + odVO.getOriginalProductId() + " to " + odVO.getProductId();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            	updatePartnerProduct = true;
    						            } else {
    				                    	logger.info("original pdb price: " + orderVO.getPiOriginalPdbPrice());
    				                    	logger.info("new product price: " + totalProductPrice);
    				                    	int thresholdDiff = totalProductPrice.compareTo(orderVO.getPiOriginalPdbPrice());
    				                    	if (thresholdDiff != 0) {
        						            	displayString = "Price point changed from " + orderVO.getPiOriginalPdbPrice() + " to " + totalProductPrice;
        						            	logger.debug(displayString);
        						            	if (updateComment == null) {
        						            	    updateComment = displayString;
        						            	} else {
        						            	    updateComment += "\n" + displayString;
        						            	}
   				                    		    updatePartnerProduct = true;
    				                    	}
    						            }
    						            if (odVO.getOriginalDeliveryDate().compareTo(odVO.getDeliveryDate()) != 0) {
    						            	displayString = "Delivery date changed from " + sdf.format(odVO.getOriginalDeliveryDate()) + " to " +
    						            			sdf.format(odVO.getDeliveryDate());
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origOccasion.equalsIgnoreCase(odVO.getOccasion())) {
    						            	OccasionHandler occHandler = (OccasionHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_OCCASION);
    						            	String desc1 = occHandler.getOccasionDescriptionById(origOccasion);
    						            	String desc2 = occHandler.getOccasionDescriptionById(odVO.getOccasion());
    						            	displayString = "Occasion changed from " + desc1 + " to " + desc2;
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            String tempString = recipVO.getFirstName() + " " + recipVO.getLastName();
    						            if (!origName.equalsIgnoreCase(tempString)) {
    						            	displayString = "Recipient name changed from " + origName + " to " + tempString;
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origAddress.equalsIgnoreCase(recipVO.getAddress())) {
    						            	displayString = "Address changed from " + origAddress + " to " + recipVO.getAddress();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origCity.equalsIgnoreCase(recipVO.getCity())) {
    						            	displayString = "City changed from " + origCity + " to " + recipVO.getCity();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origState.equalsIgnoreCase(recipVO.getState())) {
    						            	displayString = "State changed from " + origState + " to " + recipVO.getState();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origZipCode.equalsIgnoreCase(recipVO.getZipCode())) {
    						            	displayString = "Zip code changed from " + origZipCode + " to " + recipVO.getZipCode();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origCountry.equalsIgnoreCase(recipVO.getCountry())) {
    						            	displayString = "Country changed from " + origCountry + " to " + recipVO.getCountry();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            tempString = recipVO.getDayPhone();
    						            if (recipVO.getDayPhoneExt() != null && !recipVO.getDayPhoneExt().equals("")) {
    						            	tempString = tempString + " " + recipVO.getDayPhoneExt();
    						            }
    						            if (!origPhoneNumber.equalsIgnoreCase(tempString)) {
    						            	displayString = "Phone number changed from " + origPhoneNumber + " to " + tempString;
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origCardMessage.equalsIgnoreCase(odVO.getCardMessage())) {
    						            	displayString = "Card message was changed";
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origAddressType.equalsIgnoreCase(recipVO.getAddressType())) {
    						            	displayString = "Address Type changed from " + origAddressType + " to " + recipVO.getAddressType();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (!origBusinessName.equalsIgnoreCase(recipVO.getBusinessName()) &&
    						            		recipVO.getAddressType() != null && !recipVO.getAddressType().equalsIgnoreCase("HOME")) {
    						            	displayString = "Business Name changed from " + origBusinessName + " to " + recipVO.getBusinessName();
    						            	logger.debug(displayString);
    						            	if (updateComment == null) {
    						            	    updateComment = displayString;
    						            	} else {
    						            	    updateComment += "\n" + displayString;
    						            	}
    						            }
    						            if (updateComment != null) {
    						            	updateComment = "Information changed due to FEMOE updates.\n" + updateComment;
    						            	CommentsVO commentVO = new CommentsVO();
    						            	commentVO.setComment(updateComment);
    						            	commentVO.setCreatedBy(csrId);
    						            	commentVO.setUpdatedBy(csrId);
    						            	commentVO.setOrderDetailId(odVO.getOriginalOrderDetailId());
    						            	commentVO.setOrderGuid(orderVO.getOriginalOrderGuid());
    						            	commentVO.setCommentOrigin("CS Call");
    						            	commentVO.setCommentType("Order");
    						            	uoDAO.insertComment(commentVO);
    						            	logger.info("Comment added: " + updateComment);
    						            }
    						            if (odVO.getOrderComments() != null) {
    						            	CommentsVO commentVO = new CommentsVO();
    						            	commentVO.setComment(odVO.getOrderComments());
    						            	commentVO.setCreatedBy(csrId);
    						            	commentVO.setUpdatedBy(csrId);
    						            	commentVO.setOrderDetailId(odVO.getOriginalOrderDetailId());
    						            	commentVO.setOrderGuid(orderVO.getOriginalOrderGuid());
    						            	commentVO.setCommentOrigin("CS Call");
    						            	commentVO.setCommentType("Order");
    						            	uoDAO.insertComment(commentVO);
    						            	logger.info("Order comment added");
    						            }
    						            if (updatePartnerProduct) {
    						            	outMap = orderEntryDAO.insertPartnerProductUpdate(utConn, odVO.getOriginalOrderDetailId(), odVO.getDeliveryDate(), 
    						            			odVO.getOriginalProductId(), odVO.getProductId(), orderVO.getPiOriginalMercTotal(), newMercTotal, csrId);
    	                                    String status = (String) outMap.get("OUT_STATUS");
    	                                    String message = (String) outMap.get("OUT_MESSAGE");
    	                                    logger.info("insertPartnerProductUpdate() status: " + status + " - " + message);
    	                                    if (status == null || !status.equalsIgnoreCase("Y")) {
    	                                        logger.error("Error in insertPartnerProductUpdate()");
    	                                    }
    						            }

    						            logger.info("Committing transaction");
    	                        		userTransaction.commit();

    								} else {
    									logger.error("Rolling back transaction");
                                        rollback(userTransaction);
                                        errorList.put("Filling Florist", "No florist is available to fulfill this order");
                                        validationStatus = "N";
                                		piValidationOverride = "N";
                                		tempErrorList = null;
    								}

    							} catch (Exception e) {
    								logger.error(orderDetailId + ": Error found in updating order");
    								logger.error(e);
    							}

                            } else {

                                String temp = orderEntryDAO.getMasterSequence(utConn);
                                if (temp == null || temp.equals("")) {
                                    logger.error("Could not retrieve Master Order Sequence");
                                    success = false;
                                } else {
                                    masterOrderNumber = "M" + temp;
                                    orderVO.setMasterOrderNumber(masterOrderNumber);
                                    logger.debug("Master Order Number: " + masterOrderNumber);
                                }

                                orderVO.setOrderDate(new Date());

                                CustomerVO buyerVO = orderVO.getBuyerVO();
                                //CreditCardVO ccVO = orderVO.getCcVO();
                                String buyerId = null;
                                String paymentId = null;
                        
                                if (success) {
                                    if (buyerVO.getNewsletterFlag() == null) {
                                        buyerVO.setNewsletterFlag("N");
                                    }
                                    HashMap outputMap = orderEntryDAO.persistCustomer(utConn, buyerVO);
                                    buyerId = (String) outputMap.get("OUT_CUSTOMER_ID");
                                    String status = (String) outputMap.get("OUT_STATUS");
                                    String message = (String) outputMap.get("OUT_MESSAGE");
                                    logger.debug("status: " + status + " - " + message);
                                    logger.debug("buyerId: " + buyerId);
                                    if (status == null || !status.equalsIgnoreCase("Y")) {
                                        success = false;
                                    } else if (buyerId != null) {
                                        orderVO.setBuyerId(buyerId);
                                    }
                                }

                                if (success) {
                                    HashMap outputMap = orderEntryDAO.persistOrders(utConn, orderVO, masterOrderNumber);
                                    String status = (String) outputMap.get("OUT_STATUS");
                                    String message = (String) outputMap.get("OUT_MESSAGE");
                                    logger.debug("status: " + status + " - " + message);
                                    if (status == null || !status.equalsIgnoreCase("Y")) {
                                        success = false;
                                    }
                                }

                                if (success) {
                                    String keyName = cu.getFrpGlobalParm(OrderEntryConstants.INGRIAN_CONTEXT,
                                        OrderEntryConstants.CURRENT_KEY);
                                    ccVO.setKeyName(keyName);
                                    /* If order amount < gift certificate amount then set gift cert amount to order amount */
                                    if (ccVO.getGiftCertificateAmount() != null) {
	                            	    if(orderVO.getOrderAmount().compareTo(ccVO.getGiftCertificateAmount()) < 0){
	                            		    ccVO.setGiftCertificateAmount(orderVO.getOrderAmount());
    	                            	}
                                    }
                                    
                                    HashMap outputMap = orderEntryDAO.persistPayments(utConn, ccVO, masterOrderNumber);
                                    paymentId = (String) outputMap.get("OUT_PAYMENT_ID");
                                    String status = (String) outputMap.get("OUT_STATUS");
                                    String message = (String) outputMap.get("OUT_MESSAGE");
                                    logger.debug("status: " + status + " - " + message);
                                    logger.debug("paymentId: " + paymentId);
                                    if (status == null || !status.equalsIgnoreCase("Y")) {
                                        success = false;
                                    }
                                }
                        
                                List coBrandList = orderVO.getCoBrandVO();
                                for (int j=0; j<coBrandList.size(); j++) {
                                    if (success) {
                                        CoBrandVO coBrandVO = (CoBrandVO) coBrandList.get(j);
                                        HashMap outputMap = orderEntryDAO.persistBillingInfo(utConn, coBrandVO, masterOrderNumber);
                                        String status = (String) outputMap.get("OUT_STATUS");
                                        String message = (String) outputMap.get("OUT_MESSAGE");
                                        logger.debug("status: " + status + " - " + message);
                                        if (status == null || !status.equalsIgnoreCase("Y")) {
                                            success = false;
                                        }
                                    }
                                }

                                if (success) {
                        	
                                	OrderDetailVO odVO=null;
                        	        CustomerVO recipVO=null;
                                	List addonList=null;
                                	AVSAddressVO avsAddressVO=null;
                                	String modifyOrderComments=null;
                                    List detailList = orderVO.getOdVO();
                                    for (int i=0; i<detailList.size(); i++) {
                                        odVO = (OrderDetailVO) detailList.get(i);
                                        String externalOrderNumber = null;

                                        if (success) {
                                            temp = orderEntryDAO.getOrderDetailSequence(utConn);
                                            if (temp == null || temp.equals("")) {
                                                logger.error("Could not retrieve Order Detail Sequence");
                                                success = false;
                                            } else {
                                                externalOrderNumber = "C" + temp;
                                                logger.debug("External Order Number: " + externalOrderNumber);
                                                odVO.setExternalOrderNumber(externalOrderNumber);
                                                externalOrderNumberList.add(externalOrderNumber);

                                                PersonalGreetingVO pgVO = odVO.getPersonalGreetingVO();
                                                if (pgVO != null) {
                                                    personalGreetingList.add(pgVO);
                                                }
                                        
                                            }
                                        }
                                
                                        recipVO = odVO.getRecipientVO();
                                        if (success) {
                                            if (recipVO.getNewsletterFlag() == null) {
                                                recipVO.setNewsletterFlag("N");
                                            }
                                            HashMap outputMap = orderEntryDAO.persistCustomer(utConn, recipVO);
                                            String recipientId = (String) outputMap.get("OUT_CUSTOMER_ID");
                                            String status = (String) outputMap.get("OUT_STATUS");
                                            String message = (String) outputMap.get("OUT_MESSAGE");
                                            logger.debug("status: " + status + " - " + message);
                                            logger.debug("recipientId: " + recipientId);
                                            if (status == null || !status.equalsIgnoreCase("Y")) {
                                                success = false;
                                            } else if (recipientId != null) {
                                                odVO.setRecipientId(recipientId);;
                                            }
                                        }

                                        if (success) {
                                            if (odVO.getIotwFlag() == null) {
                                                odVO.setIotwFlag("N");
                                            }
                                            if (odVO.getItemSourceCode() == null) {
                                                odVO.setItemSourceCode(orderVO.getSourceCode());
                                            }
                                            if (odVO.getShipMethod() == null) {
                                                odVO.setShipMethod("SD");  
                                            }
                                            //If Modify Order then
                                            if (orderVO.getOriginalOrderGuid() != null && !orderVO.getOriginalOrderGuid().equals("")) {
                                    
										        //If the Order is being created due to modification of other order
        										//generated comments will be added prior to the actual order comments 
                                            	modifyOrderComments = UpdateOrderConstants.UPDATE_ORD_CMNTS_FOR_NEW_ORDER
                                        				+" "+odVO.getOriginalExternalOrderNumber()+ ". ";
                                            	if(odVO.getOrderComments()!=null){	
                                            		odVO.setOrderComments(modifyOrderComments+odVO.getOrderComments().trim());
                                    	        }
                                            	else{
                                            		odVO.setOrderComments(modifyOrderComments);
                                            	}
                                            }
                                    
                                            HashMap outputMap = orderEntryDAO.persistOrderDetails(utConn, odVO, masterOrderNumber);
                                            String status = (String) outputMap.get("OUT_STATUS");
                                            String message = (String) outputMap.get("OUT_MESSAGE");
                                            logger.debug("status: " + status + " - " + message);
                                            if (status == null || !status.equalsIgnoreCase("Y")) {
                                                success = false;
                                            }
                                        }
                                
                                        addonList = odVO.getAddonVO();
                                        for (int j=0; j<addonList.size(); j++) {
                                            if (success) {
                                                AddonVO addonVO = (AddonVO) addonList.get(j);
                                                HashMap outputMap = orderEntryDAO.persistAddons(utConn, addonVO, externalOrderNumber);
                                                String status = (String) outputMap.get("OUT_STATUS");
                                                String message = (String) outputMap.get("OUT_MESSAGE");
                                                logger.debug("status: " + status + " - " + message);
                                                if (status == null || !status.equalsIgnoreCase("Y")) {
                                                    success = false;
                                                }
                                            }
                                        }
                                
                                        //Persisting AVS Address info
                                        avsAddressVO = odVO.getAvsAddressVO();
                                        if(avsAddressVO != null && success){
                                        	orderEntryDAO.persistAVSAddress(utConn, avsAddressVO, new Long(odVO.getRecipientId()));
                                        	orderEntryDAO.persistAvsAddressScores(utConn, avsAddressVO);
                                	
                                        }
                                    }
                            
                                    //Loading Original Order Details for Update Order
                                    UpdateOrderVO originalOrderVO = new UpdateOrderVO();
                                    if (orderVO.getOriginalOrderGuid() != null && !orderVO.getOriginalOrderGuid().equals("")) {
                            	
                                    	logger.info("JOE Update Order Process Starts for OrgExtOrderNum :"+odVO.getOriginalExternalOrderNumber());
                            	        originalOrderVO.setOrderGuid(orderVO.getOriginalOrderGuid());
                                    	originalOrderVO.setExternalOrderNumber(odVO.getOriginalExternalOrderNumber());
                                    	originalOrderVO.setOrderDetailId(odVO.getOriginalOrderDetailId());
                                    	originalOrderVO.setDeliveryDate(odVO.getOriginalDeliveryDate());
                                    	originalOrderVO.setShipMethod(odVO.getOriginalShipMethod());
                            	        originalOrderVO.setCustomerId(odVO.getOriginalRecipientId());

        	                            if(success)
	                                    {
	                                    	try
	                                    	{
	                                    		originalOrderVO.setOrigin(UpdateOrderConstants.UPDATE_ORDER_COMMENTS_ORIGIN);
	                            		        originalOrderVO.setCsrID(csrId);
        	                            		originalOrderVO.setResponsibleParty(csrId);
		                                    	updateOrderVO = loadUdpateOrderVO(odVO,orderVO.getOrigin(),csrId);
		                                    	new UpdateOrderBO(utConn).processUpdateOrder(originalOrderVO, updateOrderVO);
	                                    	}catch(Exception e)
	                                    	{
	                            		        logger.error("Error while Updating the Order: ",e);
	                            		        success=false;
	                            	        }
	                                    }
                                        logger.info("JOE Update Order Process Ends Successfully");
                                    }
                                }

                                if (success) {

                                    try {
                                        // post JMS message to JOE_ORDER_DISPATCHER
                                        MessageToken token = null;
                                        token = new MessageToken();
                                        token.setStatus("JOE Order Dispatcher");
                                        token.setMessage(masterOrderNumber);

                                        OrderUtil.dispatchJMSMessage(context, token);

                                    } catch (Exception e) {

                                        // Add to scrub.order_exceptions
                                        String JOEcontext = "JOE/Joe Order Dispatcher";
                                        String errorMessage = "Error sending order to JOE Order Dispatcher";
                                        HashMap outputMap = orderEntryDAO.insertOrderExceptions(utConn, masterOrderNumber, JOEcontext, errorMessage);
                                        String status = (String) outputMap.get("OUT_STATUS");
                                        String message = (String) outputMap.get("OUT_MESSAGE");
                                        logger.debug("status: " + status + " - " + message);

                                    } finally {

                                        userTransaction.commit();
                                        logger.debug("commit finished");
                            
                                    }

                                } else {

                                    rollback(userTransaction);
                                    errorList.put("persist-order", "Could not write order information to the database");
                                    validationStatus = "N";

                                }
                            }

                        } catch (Throwable t) {
                            logger.error(t.toString());
                            rollback(userTransaction);
                            errorList.put("persist-order", "Could not write order information to the database");
                            validationStatus = "N";

                        } finally {
                            if( utConn!=null ) {
                                try {
                                    if( !utConn.isClosed() ) {
                                        utConn.close();
                                    }
                                } catch (SQLException sqle) {
                                    logger.warn("Error closing database connection",sqle);
                                }
                            }
                        }
                        
                        // move tempErrorList from PI threshold check into errorList
                        if (tempErrorList != null && tempErrorList.size() > 0) {
                        	errorList = tempErrorList;
                        }
                    }

                } else {
                    validationStatus = "N";
                }

                logger.debug("validationStatus: " + validationStatus + " " + errorList.size());
                doc = OEActionBase.createValidationResultSet(doc, validationStatus, errorList);

                if ((validationStatus != null && validationStatus.equalsIgnoreCase("Y")) ||
                		(piValidationOverride != null && piValidationOverride.equalsIgnoreCase("Y"))) {
                    NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element) node.item(0);

                        Element order = doc.createElement("order");
                        Element master = doc.createElement("master-order-number");
                        order.appendChild(master);
                        master.appendChild(doc.createTextNode(masterOrderNumber));

                        Element orderTotal = doc.createElement("order-total");
                        order.appendChild(orderTotal);
                        orderTotal.appendChild(doc.createTextNode(orderVO.getOrderAmount().toString()));

                        Element discountTotal = doc.createElement("discount-total");
                        order.appendChild(discountTotal);
                        discountTotal.appendChild(doc.createTextNode(orderVO.getDiscountAmount().toString()));

                        Element milesTotal = doc.createElement("miles-points-total");
                        order.appendChild(milesTotal);
                        milesTotal.appendChild(doc.createTextNode(orderVO.getMilesPoints().toString()));

                        BigDecimal freeShippingSavings = orderVO.getServiceFeeTotalSavings().add(orderVO.getShippingFeeTotalSavings());
                        if (freeShippingSavings.compareTo(new BigDecimal(0)) > 0) {

                            String freeShippingDisplayName = "";
                          
                            AccountProgramMasterVO apmVO = CacheUtil.getInstance().getFSAccountProgramDetailsFromContent();
                            if (apmVO != null) {
                                freeShippingDisplayName = apmVO.getDisplayName();
                            } else {
                                freeShippingDisplayName = orderEntryDAO.getProgramDisplayName(qConn, OrderEntryConstants.FREE_SHIPPING_NAME);
                            }

                            String contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_OC_FS_APPLIED_ORDER_SAVINGS_LABEL", null);
                            contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingDisplayName);
                            Element value = doc.createElement("fs-applied-order-savings-label");
                            order.appendChild(value);
                            value.appendChild(doc.createTextNode(contentMsg));
                            
                            DecimalFormat myFormatter = new DecimalFormat("#,###,##0.00");
                            value=doc.createElement("free-shipping-savings");
                            order.appendChild(value);
                            value.appendChild(doc.createTextNode(myFormatter.format(freeShippingSavings)));

                        }

                        Element payment = doc.createElement("payment");
                        order.appendChild(payment);

                        CreditCardVO ccVO = orderVO.getCcVO();
                        Element ccAmount = doc.createElement("credit-card");
                        payment.appendChild(ccAmount);
                        if (ccVO.getApprovalAmount() != null) {
                            ccAmount.appendChild(doc.createTextNode(ccVO.getApprovalAmount()));
                        }

                        Element gcAmount = doc.createElement("gift-certificate");
                        payment.appendChild(gcAmount);
                        if (ccVO.getGiftCertificateAmount() != null) {
                            gcAmount.appendChild(doc.createTextNode(ccVO.getGiftCertificateAmount().toString()));
                        }

                        Element ncAmount = doc.createElement("no-charge");
                        payment.appendChild(ncAmount);
                        if (ccVO.getNoChargeAmount() != null) {
                            ncAmount.appendChild(doc.createTextNode(ccVO.getNoChargeAmount().toString()));
                        }
                        
                        Element items = doc.createElement("items");
                        order.appendChild(items);
                        for (int i=0; i<externalOrderNumberList.size(); i++) {
                            String number = (String) externalOrderNumberList.get(i);
                            Element external = doc.createElement("external-order-number");
                            items.appendChild(external);
                            external.appendChild(doc.createTextNode(number));
                        }

                        Element personalGreetings = doc.createElement("personal-greetings");
                        order.appendChild(personalGreetings);
                        if (personalGreetingList.size() > 0) {
                            for (int i=0; i<personalGreetingList.size(); i++) {
                                PersonalGreetingVO pgVO = (PersonalGreetingVO) personalGreetingList.get(i);
                                Element personal = doc.createElement("personal-greeting");
                                personalGreetings.appendChild(personal);
                                Element temp = doc.createElement("recipient-name");
                                personal.appendChild(temp);
                                temp.appendChild(doc.createTextNode(pgVO.getRecipientName()));
                                temp = doc.createElement("product-name");
                                personal.appendChild(temp);
                                temp.appendChild(doc.createTextNode(pgVO.getProductName()));
                                temp = doc.createElement("personal-greeting-id");
                                personal.appendChild(temp);
                                temp.appendChild(doc.createTextNode(pgVO.getPersonalGreetingId()));
                            }
                            Element instructions = doc.createElement("instructions");
                            personalGreetings.appendChild(instructions);

                            //get the text instructions and replace the phone number
                            String personalGreetingPhoneToken = OrderEntryConstants.PERSONAL_GREETING_PHONE_NUMBER_TOKEN;
                            ConfigurationUtil cu = ConfigurationUtil.getInstance();
                            String phoneNumber = cu.getContent(qConn, OrderEntryConstants.PERSONAL_GREETING_CONTEXT, OrderEntryConstants.PERSONAL_GREETING_PHONE_NUMBER_FILTER);
                            String personalGreetingInstructions = cu.getContent(qConn, OrderEntryConstants.PERSONAL_GREETING_CONTEXT, OrderEntryConstants.PERSONAL_GREETING_INSTRUCTIONS_PHONE);
                            personalGreetingInstructions = personalGreetingInstructions.replaceAll(personalGreetingPhoneToken,phoneNumber);   

                            instructions.appendChild(doc.createTextNode(personalGreetingInstructions));
                            
                            Element identifier = doc.createElement("identifier");
                            personalGreetings.appendChild(identifier);
                            String identifierValue = cu.getContent(qConn, OrderEntryConstants.PERSONAL_GREETING_CONTEXT, OrderEntryConstants.PERSONAL_GREETING_IDENTIFIER);
                            identifierValue = identifierValue.replace(":", "");
                            identifier.appendChild(doc.createTextNode(identifierValue));
                        }
                        
                        String confirmationMessageText = "";
                        if (sourceCodeChanged) {
                            confirmationMessageText = orderEntryDAO.getContentWithFilter(qConn, "PREFERRED_PARTNER", "JOE_BIN_CHANGED_TEXT", partnerName, null);
                        }
                        Element confirmationMessage = doc.createElement("confirmation-message");
                        order.appendChild(confirmationMessage);
                        confirmationMessage.appendChild(doc.createTextNode(confirmationMessageText));

                        resultNode.appendChild(order);
                    }
                }

            }

        } catch (Throwable t) {
            logger.error("Error in submitOrder()",t);
            throw new Exception("Could not submit order.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
    
        return doc;
    }
    
  

	private UpdateOrderVO loadDummyOriginalOrderVO() {
		
		UpdateOrderVO originalOrderVO= new UpdateOrderVO();
		try{
			originalOrderVO.setOrderDetailId("2268991");
			originalOrderVO.setOrderGuid("ORD200367");
			originalOrderVO.setExternalOrderNumber("C1000909347");
			originalOrderVO.setShipMethod("SD");
			originalOrderVO.setDeliveryDate(sdf.parse("2014/09/14"));
			originalOrderVO.setCsrID("bsurimen");
			//originalOrderVO.setOrigin("CS Call");
		}catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return originalOrderVO;
	}

	private static boolean hasFSMembershipSKUItemOrWithFSApplied(Connection qConn, OrderVO orderVO)
    {
    	String freeShipping = orderVO.getBuyerHasFreeShippingFlag();
    	logger.info("BuyerHasFreeShippingFlag:"+freeShipping);
    	boolean isOrderWithFSApplied = "Y".equalsIgnoreCase(freeShipping);
    	if(isOrderWithFSApplied){
    		return isOrderWithFSApplied;
    	}
    	logger.info("Order doesn't have any item with FS Applied.");
    	List detailList = orderVO.getOdVO();
        for (int i=0; i<detailList.size(); i++) 
        {
            OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
            boolean isOrderForFSMembershipSKU = isFSMembershipSKUItem(qConn, odVO);
            if(isOrderForFSMembershipSKU)
            {
            	return true;
            }
        }
        logger.info("Order doesn't have FS Membership SKU.");
        return false;
    }   

	private static boolean isFSMembershipSKUItem(Connection qConn, OrderDetailVO item) 
	{
		logger.info("ProductId:"+item.getProductId());
		RecalculateOrderDAO orderDAO = new RecalculateOrderDAO();
		ProductVO product = orderDAO.getProduct(qConn, item.getProductId());
        String productType = product.getProductType();
        String productSubType = product.getProductSubType();
        logger.info("productType:"+productType);
        logger.info("productSubType:"+productSubType);
        return ("SERVICES".equalsIgnoreCase(productType) && "FREESHIP".equalsIgnoreCase(productSubType));
	}

	public TreeMap calculateTaxTotals(List odvos, String company){    	
    	TreeMap taxValues = new TreeMap();
    	CalculateTaxUtil calcTax = new CalculateTaxUtil();
    	boolean isSplitAdded = false;
    	
    	for(int i=0; i<odvos.size(); i++) {
    		isSplitAdded = false; // every item it is defaulted to false.
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		List<TaxVO> itemTaxList = null; 
    		if (odvo.getItemTaxVO() != null && odvo.getItemTaxVO().getTaxSplit() != null && odvo.getItemTaxVO().getTaxSplit().size() >= 0) {                               	        	        	
    			itemTaxList = odvo.getItemTaxVO().getTaxSplit();
    		}
    		Boolean put_flag = true;
    		String recip_country = odvo.getRecipientVO().getCountry();
    		
        	for (int t=0; t<itemTaxList.size(); t++) {
            	TaxVO itvo = (TaxVO)itemTaxList.get(t);
            	String desc = itvo.getDescription();
            	BigDecimal amt = itvo.getAmount();
            	int order = itvo.getDisplayOrder();
            	if(company.equalsIgnoreCase("FTDCA") && recip_country.equalsIgnoreCase("CA") && amt.compareTo(BigDecimal.ZERO) == 0) {put_flag = false;}
            	if (order > 0) {
	            	if(put_flag) {
	            		isSplitAdded = true;
	            		if(odvo.isTaxServicePerformed()) {
	            			if(taxValues.containsKey(desc)){
	            				BigDecimal value = amt;
	            				value = value.add((BigDecimal) taxValues.get(desc));
	           					taxValues.put(desc, value);
	            			}else{
	            				taxValues.put(desc, amt);
            				}
            			}  else {
            					taxValues.put(desc, new BigDecimal("0.00"));
            				}
            			}
            		}
            	}
        
    	
        	String taxLabel = odvo.getItemTaxVO().getTotalTaxDescription();
        	if(StringUtils.isEmpty(taxLabel)) {
        		taxLabel = calcTax.getDefaultTaxLabel(recip_country, company);
        	}
        	
        	if(taxValues.isEmpty() || !isSplitAdded) {
        		if (odvo.getItemTaxVO() != null) {
	        		BigDecimal totalTax = new BigDecimal(odvo.getItemTaxVO().getTaxAmount());
	        		if(taxValues!=null && taxValues.containsKey(taxLabel)) {
	        			totalTax =  totalTax.add((BigDecimal)taxValues.get(taxLabel));//        			
	        		}
	            	taxValues.put(taxLabel, totalTax);
	        	} else {
        			taxValues.put(taxLabel, new BigDecimal("0.00"));
	        	}
        	}
    	}
    	return taxValues;
    }
    
    public Map sortTaxes(Map orderTaxes){
    	Map sortedMap = new TreeMap(orderTaxes);
    	return sortedMap;
    }
    
    public BigDecimal calculateSameDayFeeTotals(List odvos){		
    	BigDecimal totalSDU = new BigDecimal("0");
    	for(int i=0; i<odvos.size(); i++){
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		totalSDU = totalSDU.add(odvo.getSameDayUpcharge());
    	}    
    	return totalSDU;    	
    }
    
    public BigDecimal calculateLateCutOffFeeTotals(List odvos){		
    	BigDecimal totalLCO = new BigDecimal("0");
    	for(int i=0; i<odvos.size(); i++){
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		totalLCO = totalLCO.add(odvo.getLateCutOffUpcharge());
    		
    		
    	}    
    	return totalLCO;    	
    }
    
    public BigDecimal calculateMondayUpchargeTotals(List odvos){		
    	BigDecimal totalMUC = new BigDecimal("0");
    	for(int i=0; i<odvos.size(); i++){
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		totalMUC = totalMUC.add(odvo.getMondayUpcharge());
    		
    	}    
    	return totalMUC;    	
    }
    
    public BigDecimal calculateSundayUpchargeTotals(List odvos){		
    	BigDecimal totalSUC = new BigDecimal("0");
    	for(int i=0; i<odvos.size(); i++){
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		totalSUC = totalSUC.add(odvo.getSundayUpcharge());
    		
    	}    
    	return totalSUC;    	
    }
    
    // #SGC -7 Adding International fee in item subtotal and cart totals
    public BigDecimal calculateInternationalFeeTotals(List odvos){		
    	BigDecimal totalINTL = new BigDecimal("0");
    	for(int i=0; i<odvos.size(); i++){
    		OrderDetailVO odvo = (OrderDetailVO)odvos.get(i);
    		totalINTL = totalINTL.add(odvo.getInternationalFee());
    	}
    	return totalINTL;
    }
    public Document calculateOrderTotals(Document orderDoc, boolean frontEndFlag) throws Exception {

        logger.debug("calculateOrderTotals()");

        Connection qConn = null;
        Document doc = null;
       
        if (orderDoc != null) {

            try {

                String dataSource = OrderEntryConstants.DATASOURCE;
                qConn = resourceProvider.getDatabaseConnection(dataSource);
                if (qConn == null) {
                    throw new Exception("Unable to connect to database");
                }

                doc = JAXPUtil.createDocument();
                Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
                doc.appendChild(root);
                JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
                JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

                Element rs = doc.createElement(OrderEntryConstants.TAG_RS);
                rs.setAttribute(OrderEntryConstants.TAG_STATUS,"Y");
                rs.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                root.appendChild(rs);

                Document tempDoc = JAXPUtil.createDocument();
                Node newNode = tempDoc.importNode(orderDoc.getFirstChild(), true);
                tempDoc.appendChild(newNode);
                Element element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_NUMBER).item(0);
                if (element != null) element.getParentNode().removeChild(element);
                element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_NC_APPROVAL_PASSWORD).item(0);
                if (element != null) element.getParentNode().removeChild(element);
                element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_CSC).item(0);
                if (element != null) element.getParentNode().removeChild(element);
                logger.debug(JAXPUtil.toString(tempDoc));

                OrderVO orderVO = OrderUtil.parseOrderXML(orderDoc);

                new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, true);
               
                populateAjaxResponseDoc(qConn,orderVO,doc);

            } catch (Throwable t) {
                logger.error("Error in calculateOrderTotals()",t);
                throw new Exception("Could not calculate order totals.");
            } finally {
                if( qConn!=null ) {
                    try {
                        if( !qConn.isClosed() ) {
                            qConn.close();
                        }
                    } catch (SQLException sqle) {
                        logger.warn("Error closing database connection",sqle);
                    }
                }
            }
        } else {
            doc = OEActionBase.createErrorResultSet("order", "Order XML was null");
        }

        logger.debug("finished with calculateOrderTotals()");
        return doc;
    }

    public void populateAjaxResponseDoc(Connection qConn, OrderVO orderVO, Document doc) throws Exception {
    	
    	 boolean seperateLineSameDayFee = false;
         CalculateTaxUtil calcTax = new CalculateTaxUtil();
         
         NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RS);
         Element resultNode = (Element) node.item(0);
         
         
         Element order = doc.createElement("order");
         resultNode.appendChild(order);
    	
    	 
         
         try{
        	 
               	 
        Element value = doc.createElement("order-total-display");
        order.appendChild(value);
    	
    	 List odvoslist = orderVO.getOdVO();
         BigDecimal orderTotal = orderVO.getOrderAmount();
         for (int i =0; i<odvoslist.size();i++) { 
         	OrderDetailVO odvo = (OrderDetailVO)odvoslist.get(i);
         	if(!odvo.isTaxServicePerformed()){
             	orderTotal = orderTotal.subtract(new BigDecimal(odvo.getTaxAmount()));
             }
         }
         value.appendChild(doc.createTextNode(orderTotal.toString()));
         

         value = doc.createElement("order-total");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getOrderAmount().toString()));

         value = doc.createElement("total-product-price");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getProductAmount().toString()));

         value = doc.createElement("total-discounts");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getDiscountAmount().toString()));

         value = doc.createElement("total-points");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getMilesPoints().toString()));
         
         SourceMasterHandler handler = (SourceMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_SOURCE_MASTER);
         SourceMasterVO smVO = handler.getSourceCodeById(orderVO.getSourceCode());
       
         
         
         if(smVO.getDisplaySameDayUpcharge().equalsIgnoreCase("Y")){
         	seperateLineSameDayFee = true;
         }
         if(smVO.getDisplaySameDayUpcharge().equalsIgnoreCase("D")){
         	String gpSdu = null;
           	GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
               if(parmHandler != null)
               {
               	gpSdu = parmHandler.getGlobalParm("FTDAPPS_PARMS", "SAME_DAY_UPCHARGE_DISPLAY");                      	
               }
               if(gpSdu.equalsIgnoreCase("Y")){
             	  seperateLineSameDayFee = true;
               }
         }
         
         value = doc.createElement("total-addons");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getAddonAmount().toString()));
         
         BigDecimal totalLCO = calculateLateCutOffFeeTotals(orderVO.getOdVO());
         if(totalLCO.compareTo(BigDecimal.ZERO)> 0){
             value = doc.createElement("total-late-cutoff");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalLCO.toString()));
             
             
             value = doc.createElement("total-late-cutoff-fee-mo");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalLCO.toString()));
          }
         
         BigDecimal totalMUC = calculateMondayUpchargeTotals(orderVO.getOdVO());
         if(totalMUC.compareTo(BigDecimal.ZERO)> 0){
             value = doc.createElement("total-monday-upcharge");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalMUC.toString()));
             
             value = doc.createElement("total-monday-upcharge-mo");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalMUC.toString()));
          }
         
         BigDecimal totalSUC = calculateSundayUpchargeTotals(orderVO.getOdVO());
         if(totalSUC.compareTo(BigDecimal.ZERO)> 0){
             value = doc.createElement("total-sunday-upcharge");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalSUC.toString()));
             
             value = doc.createElement("total-sunday-upcharge-mo");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalSUC.toString()));
          }
         
 
         
         BigDecimal totalINTL = calculateInternationalFeeTotals(orderVO.getOdVO());
         if(totalINTL.compareTo(BigDecimal.ZERO)> 0){
             value = doc.createElement("total-international-fee");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalINTL.toString()));
             
             value = doc.createElement("total-international-fee-mo");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalINTL.toString()));
          }
         
         
         BigDecimal totalSduFee = calculateSameDayFeeTotals(orderVO.getOdVO());
         BigDecimal totalSrvcFee = orderVO.getServiceFeeAmount();
         BigDecimal totalSrvcFeeMO = totalSrvcFee;
         
         if(totalSduFee.compareTo(BigDecimal.ZERO)> 0){
        	 
        	 if(seperateLineSameDayFee){
        		 value = doc.createElement("total-same-day-fee");
        		 order.appendChild(value);
        		 value.appendChild(doc.createTextNode(totalSduFee.toString()));
        		 
        		 totalSrvcFee = totalSrvcFee.subtract(totalSduFee);
        	}
        	 
        	 totalSrvcFeeMO = totalSrvcFeeMO.subtract(totalSduFee);
        	 
             value = doc.createElement("total-same-day-fee-mo");                                                      
             order.appendChild(value);
             value.appendChild(doc.createTextNode(totalSduFee.toString()));
             
         }
        
         
         value = doc.createElement("total-shipping-fee");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getShippingFeeAmount().toString()));
         value = doc.createElement("apply-surcharge-code");
    	 order.appendChild(value);
    	 value.appendChild(doc.createTextNode(orderVO.getApplySurchargeCode()));

    	 value = doc.createElement("total-surcharge-fee");
    	 order.appendChild(value);
    	 value.appendChild(doc.createTextNode(orderVO.getSurchargeFee().toString()));

    	 value = doc.createElement("surcharge-description");
    	 order.appendChild(value);
    	 value.appendChild(doc.createTextNode(orderVO.getSurchargeDescription()));

    	 logger.debug("total surcharge-fee is" + orderVO.getSurchargeFee());
    	 logger.debug("surcharge-description is" + orderVO.getSurchargeDescription());
         /* Total Taxes */
         TreeMap totalTaxesMap = calculateTaxTotals(odvoslist, orderVO.getCompany());
                        
         value = doc.createElement("totaltaxlist-size");
         order.appendChild(value);
         int totalSize = totalTaxesMap.size();
     	value.appendChild(doc.createTextNode(Integer.toString(totalSize)));
     	int tcnt=1;
         for (Object key: totalTaxesMap.keySet()) 
         {                                            
         	if(totalTaxesMap.containsKey(key)){
         	value = doc.createElement("total-tax"+tcnt+"-desc");
         	order.appendChild(value);
         	value.appendChild(doc.createTextNode(key.toString()));
         	value = doc.createElement("total-tax"+tcnt+"-amt");
         	order.appendChild(value);
         	value.appendChild(doc.createTextNode(totalTaxesMap.get(key).toString()));                	
         	}
         	tcnt++;
         }                                                
         /* End Total Taxes */

         value = doc.createElement("total-items");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(Integer.toString(orderVO.getOrderCount())));
         
         BigDecimal freeShippingSavings = orderVO.getServiceFeeTotalSavings().add(orderVO.getShippingFeeTotalSavings());
         DecimalFormat myFormatter = new DecimalFormat("#,###,##0.00");
         String fsSavings = myFormatter.format(freeShippingSavings);
         logger.info("fsSavings: " + fsSavings);
         value=doc.createElement("free-shipping-savings");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(fsSavings));

         value = doc.createElement("buyer-has-free-shipping");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getBuyerHasFreeShippingFlag()));
         
         value = doc.createElement("buyer-email-address");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(orderVO.getBuyerVO().getEmailAddress()));

         value = doc.createElement("buyer-cams-verified");
         order.appendChild(value);
         String buyer_cams_verified = "";

         if (orderVO.getBuyerCamsVerified() == true){
         	buyer_cams_verified = "Y"; }
         else {
         	buyer_cams_verified = "N"; }
         
         value.appendChild(doc.createTextNode(buyer_cams_verified));
         
         String freeShippingProgramName = "";

         AccountProgramMasterVO apmVO = CacheUtil.getInstance().getFSAccountProgramDetailsFromContent();
         if (apmVO != null) {
             freeShippingProgramName = apmVO.getDisplayName();
         } else {
             freeShippingProgramName = orderEntryDAO.getProgramDisplayName(qConn, OrderEntryConstants.FREE_SHIPPING_NAME);
         }

         String contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_APPLIED_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-applied-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_NOT_APPLIED_SC_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-not-applied-sc-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_NOT_APPLIED_SC_UPDATE_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-not-applied-sc-update-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_NOT_APPLIED_EMAIL_NONE_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-not-applied-email-none-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_NOT_APPLIED_EMAIL_UPDATE_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-not-applied-email-update-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_FS_NOT_APPLIED_PRODUCT_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         value = doc.createElement("fs-not-applied-product-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SUBMIT_FS_APPLIED_MSG", null);
         contentMsg = contentMsg.replaceAll("~fsProgramName~", freeShippingProgramName);
         contentMsg = contentMsg.replaceAll("~freeShippingSavingsToken~", fsSavings);
         value = doc.createElement("submit-fs-applied-msg");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));

         //SN: UC24094
         contentMsg = FieldUtils.replaceAll(OrderEntryConstants.RESPONSE_CAMS_UNREACHABLE, "{programName}",freeShippingProgramName);
         value = doc.createElement("response-cams-unreachable");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(contentMsg));
         
         //premier circle shade message
         if(orderVO.getBuyerHasPCMembershipFlag()!=null && orderVO.getBuyerHasPCMembershipFlag().equalsIgnoreCase("Y")){
             contentMsg = orderEntryDAO.getContentWithFilter(qConn, "SERVICE_CONFIG", "JOE_MSG", "JOE_SHADE_PREMIER_CIRCLE_MSG", null);
             value = doc.createElement("premier-circle-msg");
             order.appendChild(value);
             value.appendChild(doc.createTextNode(contentMsg));
         }
         
         List detailList = orderVO.getOdVO();
         boolean isTaxPerformed = false;
         BigDecimal totalExclFee = BigDecimal.ZERO;
         for (int i=0; i<detailList.size(); i++) {
             OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);

             Element item = doc.createElement("item");
             order.appendChild(item);

             value = doc.createElement("cart-index");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getCartIndex()));

             String productId = odVO.getProductId();
             value = doc.createElement("product-id");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(productId));

             String productPrice = "";
             if (odVO.getProductPrice() != null) {
                 productPrice = odVO.getProductPrice().toString();
             }
             value = doc.createElement("product-price");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(productPrice));

             value = doc.createElement("discounted-price");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getRetailVariablePrice().toString()));

             value = doc.createElement("discount-amount");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getDiscountAmount().toString()));

             value = doc.createElement("addons");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getAddonAmount().toString()));
             
             
             BigDecimal excludeFee = BigDecimal.ZERO ;
             BigDecimal serviceFee = odVO.getServiceFee();
             BigDecimal serviceFeeMO = serviceFee;
            
             BigDecimal lateCutoffFee = odVO.getLateCutOffUpcharge();
             
             logger.info("Late Cutoff Fee : "+lateCutoffFee);
             if(lateCutoffFee.compareTo(BigDecimal.ZERO)> 0){
                 value = doc.createElement("late-cutoff-fee");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(lateCutoffFee.toString()));
                 
                 
                 value = doc.createElement("late-cutoff-fee-mo");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(lateCutoffFee.toString()));
                 if(serviceFee.compareTo(BigDecimal.ZERO)> 0){
                	 excludeFee = excludeFee.add(lateCutoffFee);
                	 totalExclFee = totalExclFee.add(lateCutoffFee);
                 }
             }
             
             BigDecimal mondayUpcharge = odVO.getMondayUpcharge();
            
             logger.info("Monday Upcharge : "+mondayUpcharge);
             if(mondayUpcharge.compareTo(BigDecimal.ZERO)> 0){
                 value = doc.createElement("vendor-mon-upcharge");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(mondayUpcharge.toString()));
                 
                 value = doc.createElement("monday-upcharge-mo");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(mondayUpcharge.toString()));
                 if(serviceFee.compareTo(BigDecimal.ZERO)> 0){
                	 excludeFee = excludeFee.add(mondayUpcharge);
                	 totalExclFee = totalExclFee.add(mondayUpcharge);
                 }
             }
             
             BigDecimal sundayUpcharge = odVO.getSundayUpcharge();
             
             logger.info("Sunday Upcharge : "+sundayUpcharge);
             if(sundayUpcharge.compareTo(BigDecimal.ZERO)> 0){
                 value = doc.createElement("vendor-sun-upcharge");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(sundayUpcharge.toString()));
                 
                 value = doc.createElement("sunday-upcharge-mo");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(sundayUpcharge.toString()));
                 if(serviceFee.compareTo(BigDecimal.ZERO)> 0){
                	 excludeFee = excludeFee.add(sundayUpcharge);
                	 totalExclFee = totalExclFee.add(sundayUpcharge);
                 }
             }
             
             
             BigDecimal internationalFee = odVO.getInternationalFee();
             
             logger.info("International Fee : "+internationalFee);
             if(internationalFee.compareTo(BigDecimal.ZERO)> 0){
                 value = doc.createElement("international-fee");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(internationalFee.toString()));
                 
                 value = doc.createElement("international-fee-mo");
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(internationalFee.toString()));
                 excludeFee = excludeFee.add(internationalFee);
                 totalExclFee = totalExclFee.add(internationalFee);
                 
             }
             
             BigDecimal sameDayFee = odVO.getSameDayUpcharge();
             BigDecimal serviceFeeBackUp = serviceFee;
             logger.info("Same Day Upcharge: "+sameDayFee);
             if(sameDayFee.compareTo(BigDecimal.ZERO)> 0){
            	 
            	 if(seperateLineSameDayFee){
            		 value = doc.createElement("same-day-fee");
            		 item.appendChild(value);
            		 value.appendChild(doc.createTextNode(sameDayFee.toString()));
            		 
            		 serviceFee = serviceFee.subtract(sameDayFee);
            	}
            	 
            	 serviceFeeMO = serviceFeeMO.subtract(sameDayFee);
            	 
                 value = doc.createElement("same-day-fee-mo");                                                      
                 item.appendChild(value);
                 value.appendChild(doc.createTextNode(sameDayFee.toString()));
                 
             }
             
             value = doc.createElement("surcharge-fee");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getSurchargeAmount().toString()));
             
             value = doc.createElement("shipping-fee");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getShippingFee().toString()));
             
             
             
             BigDecimal diffServiceFee = serviceFeeBackUp.subtract(odVO.getOriginalServiceFeeAmount());
             
             if(serviceFee.compareTo(BigDecimal.ZERO)> 0){
            	 
            	 
                 if(diffServiceFee.compareTo(odVO.getSurchargeAmount()) == 0){
        			 excludeFee = excludeFee.add(odVO.getSurchargeAmount());
        			 totalExclFee = totalExclFee.add(odVO.getSurchargeAmount());
        		 }
            	 
            	 
            	 value = doc.createElement("service-fee");
            	 item.appendChild(value);
            	 value.appendChild(doc.createTextNode(serviceFee.toString()));
             
            	 serviceFeeMO = serviceFeeMO.subtract(excludeFee);
                
            	 value = doc.createElement("service-fee-mo");
            	 item.appendChild(value);
            	 value.appendChild(doc.createTextNode(serviceFeeMO.toString()));
             }
             logger.info("***************ITEM :: "+ i+1 +"**********************");
             logger.info("*************surcharge"+odVO.getSurchargeAmount());
             logger.info("*************serviceFeeDifference"+diffServiceFee);
             logger.info("*************initial service-fee"+serviceFee);
             logger.info("*************exclude fee"+excludeFee);
             logger.info("*************final service-fee"+serviceFeeMO);
             logger.info("*************exclude fee total"+totalExclFee);
             logger.info("**************************************************");

             /*value = doc.createElement("sales-tax");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getTaxAmount().toString()));*/
             /* Item Taxes */                                       
             List<TaxVO> itemTaxList = null; 
     		if (odVO.getItemTaxVO() != null && odVO.getItemTaxVO().getTaxSplit() != null && odVO.getItemTaxVO().getTaxSplit().size() >= 0) {                               	        	        	
     			itemTaxList = odVO.getItemTaxVO().getTaxSplit();
     		}
     		if(!isTaxPerformed && odVO.isTaxServicePerformed()) {
 				isTaxPerformed = true;
 			}
             
             value = doc.createElement("taxlist-size");
         	item.appendChild(value);
         	Map taxValues = new HashMap(); 
         	for (int t=0; t<itemTaxList.size(); t++) { 
         		TaxVO itvo = itemTaxList.get(t);    
         		if(itvo.getDisplayOrder() > 0) {
         			if(odVO.isTaxServicePerformed()) {
         				taxValues.put(itvo.getDescription(), itvo.getAmount());
             		} else {
             			taxValues.put(odVO.getItemTaxVO().getTotalTaxDescription(), new BigDecimal(0));
             		}
         		}
             }
         	String defaultLabel = calcTax.getDefaultTaxLabel(odVO.getRecipientVO().getCountry(), orderVO.getCompany());
         	if((taxValues != null && taxValues.size() ==0)) {
         		if (odVO.getItemTaxVO() != null) {
             		if(odVO.isTaxServicePerformed()) {
             			taxValues.put(odVO.getItemTaxVO().getTotalTaxDescription(), odVO.getItemTaxVO().getTaxAmount());
             		} else {
             			taxValues.put((odVO.getItemTaxVO().getTotalTaxDescription() != null) ? odVO.getItemTaxVO().getTotalTaxDescription() : defaultLabel, new BigDecimal("0.00"));
             		}
             	} else {
             		taxValues.put(defaultLabel, new BigDecimal("0.00")); 
             	}
         	}
         	
         	value.appendChild(doc.createTextNode(Integer.toString(taxValues.size())));
         	int icnt=1;
         	Map sortedItemMap = sortTaxes(taxValues);
             for (Object key: sortedItemMap.keySet()){                                            
             	if(taxValues.containsKey(key)){                    		                    	
             	value = doc.createElement("tax"+icnt+"-desc");
             	item.appendChild(value);
             	value.appendChild(doc.createTextNode(key.toString()));
             	
             	value = doc.createElement("tax"+icnt+"-amt");
             	item.appendChild(value);
             	value.appendChild(doc.createTextNode(taxValues.get(key).toString()));
             	}                    	
             	icnt++;
             }
             
             value = doc.createElement("item-tax");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getItemTaxVO().getTaxAmount()!=null?odVO.getItemTaxVO().getTaxAmount():BigDecimal.ZERO.toString()));
             /* End Item Taxes */

             value = doc.createElement("partner-points");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getMilesPoints().toString()));

             value = doc.createElement("reward-type");
             item.appendChild(value);
             value.appendChild(doc.createTextNode(odVO.getRewardType()));

             value = doc.createElement("item-total");
             item.appendChild(value);
             BigDecimal itemTotal = odVO.getLineItemTotal();
             if(!odVO.isTaxServicePerformed()){
             	itemTotal = odVO.getLineItemTotal().subtract(new BigDecimal(odVO.getItemTaxVO().getTaxAmount()));
             }
             value.appendChild(doc.createTextNode(itemTotal.toString()));
          
             
             value = doc.createElement("alaska-hawaii-fee");
             item.appendChild(value);
             if (odVO.getAlaskaHawaiiFee() != null ) {
                 value.appendChild(doc.createTextNode(odVO.getAlaskaHawaiiFee().toString()));
             }
         }
         
         if(totalSrvcFee.compareTo(BigDecimal.ZERO)> 0){
        	 value = doc.createElement("total-service-fee");
        	 order.appendChild(value);
        	 value.appendChild(doc.createTextNode(totalSrvcFee.toString()));
          	
        	 totalSrvcFeeMO = totalSrvcFeeMO.subtract(totalExclFee);
          	
        	 value = doc.createElement("total-service-fee-mo");
        	 order.appendChild(value);
        	 value.appendChild(doc.createTextNode(totalSrvcFeeMO.toString()));
         }
         
         logger.info("*********************TOTALS***********************");
         logger.info("*************total exclude fee"+totalExclFee);
         logger.info("*************total initial service-fee"+totalSrvcFee);
         logger.info("*************total exclude fee"+totalExclFee);
         logger.info("*************total final service-fee"+totalSrvcFeeMO);
         logger.info("*************exclude fee total"+totalExclFee);
         logger.info("**************************************************");
         
         value = doc.createElement("taxservice-performed");
         order.appendChild(value);
         value.appendChild(doc.createTextNode(isTaxPerformed ? "Y" : "N"));
       
         
         } catch (Throwable t) {
             logger.error("Error in calculateOrderTotals()",t);
             throw new Exception("Could not calculate order totals.");
         } 
		
	}

	public Document recordCallTime(String startTime, String csrId, String dnisId,
        String masterOrderNumber) throws Exception {

        Document doc = null;
        Connection qConn = null;

        String validationStatus = "Y";
        HashMap errorList = new HashMap();

        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

        try {

            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }

            Date startDate = new Date(Long.parseLong(startTime));
            Date endDate = new Date();

            HashMap outputMap = orderEntryDAO.persistCallTimeHistory(qConn, dnisId, csrId, masterOrderNumber, startDate, endDate);
            String status = (String) outputMap.get("OUT_STATUS");
            String message = (String) outputMap.get("OUT_MESSAGE");
            logger.debug("status: " + status + " - " + message);
            if (status == null || !status.equalsIgnoreCase("Y")) {
                validationStatus = "N";
                errorList.put("record-call-time", message);
            }

        } catch (Exception e) {
            logger.error(e);
            validationStatus = "N";
            errorList.put("record-call-time", e.getMessage());
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        logger.debug("validationStatus: " + validationStatus + " " + errorList.size());
        doc = OEActionBase.createValidationResultSet(doc, validationStatus, errorList);

        return doc;
    }

    private void rollback(UserTransaction userTransaction)
    {
        try  
        {
            if (userTransaction != null)  
            {
              // rollback the user transaction
              userTransaction.rollback();

              logger.debug("User transaction rolled back");
            }
        } 
        catch (Exception ex)  
        {
            logger.error("Exception received" + ex.getMessage(), ex);
        } 
    }

/*
    public void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public void setOrderEntryDAO(OrderEntryDAO orderEntryDAO) {
        this.orderEntryDAO = orderEntryDAO;
    }

    public OrderEntryDAO getOrderEntryDAO() {
        return orderEntryDAO;
    }
*/

    private static String getFraudComments(Connection qConn, String fraudId) {

        logger.debug("getFraudDescription(" + fraudId + ")");
        String fraudDescription = null;
        
        try {
            CachedResultSet cr = orderEntryDAO.getFraudOrderDisposition(qConn, fraudId);
            if (cr.next()) {
                fraudDescription = cr.getString("fraud_description");
                logger.debug("fraud description: " + fraudDescription);
            }
        } catch (Exception e) {
        	logger.error("Exception received" + e.getMessage(), e);
        }

        return fraudDescription;

    }
    
	private UpdateOrderVO loadUdpateOrderVO(OrderDetailVO odVO, String origin,
			String csrId) throws Exception {

		UpdateOrderVO updateOrderVO = new UpdateOrderVO();
		updateOrderVO.setOrigin(origin);

		// Load UpdateOrderVO with Order Derails VO
		updateOrderVO.setOrderDetailId(odVO.getOrderDetailId());
		updateOrderVO.setExternalOrderNumber(odVO.getExternalOrderNumber());
		updateOrderVO.setShipMethod(odVO.getShipMethod());
		updateOrderVO.setDeliveryDate(odVO.getDeliveryDate());
		updateOrderVO.setCsrID(csrId);

		return updateOrderVO;
	}
   
	/**
	 * Check if the current date is greater than the last day of the delivery
	 * date month
	 * 
	 * @param deliveryDate
	 *            - Date
	 * 
	 * @return boolean - true if the current date is after the adjustment date
	 *         (calculated based off of Delivery Date)
	 * 
	 * @throws Exception
	 */
	private boolean checkDeliveryDate(Date deliveryDate) throws Exception {
		logger.info("Entering checkDeliveryDate()");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		logger.info("deliveryDate: " + sdf.format(deliveryDate));

		boolean dateComparision = false;

		try {
			Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
			Date currentDate = calCurrent.getTime();
			currentDate = sdf.parse(sdf.format(currentDate));

			if (currentDate.after(deliveryDate)) {
				Calendar calDelivery = Calendar.getInstance(TimeZone.getDefault());
				calDelivery.setTime(deliveryDate);

				int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

				Calendar calAdjustment = Calendar.getInstance(TimeZone.getDefault());
				calAdjustment.setTime(deliveryDate);
				calAdjustment.set(calDelivery.get(Calendar.YEAR), 
						calDelivery.get(Calendar.MONTH), day);

				Date adjustmentDate = calAdjustment.getTime();

				if (currentDate.after(adjustmentDate)) {
					dateComparision = true;
				}
			}
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting checkDeliveryDate");
			}
		}

		return dateComparision;
	}
	
	private boolean checkFloristCutoff(String cutoffTime, Date deliveryDate) throws Exception {
		logger.info("checkFloristCutoff(" + cutoffTime + ")");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmm");
        boolean beforeCutoff = false;
        
        Date today = new Date();
        String todayString = sdf2.format(today);
        
        String cutoffString = sdf1.format(deliveryDate) + cutoffTime;
        if (todayString.compareTo(cutoffString) <= 0) {
            beforeCutoff = true;
        }

        return beforeCutoff;

	}

    private String breakString(String inString, int lineLength) 
    {
      if(inString == null) return null;
      inString = inString.replaceAll("\n", " ");
      String outputString = new String();
      if(inString.length() == 0) return inString;
      
      // append a blank space so the substring method can always find a space at the end
      String remainingString = inString;
   
      try {   
        while(lineLength < remainingString.length()) {
          outputString += remainingString.substring(0, remainingString.indexOf(" ", lineLength)) + "\n";
          remainingString = remainingString.substring(remainingString.indexOf(" ", lineLength) + 1, remainingString.length()).trim();
        }
      } catch(Exception e) 
      {
        // fall out of loop on exception
      }
      return outputString + remainingString;
    }

    private String buildCityStateZipString(CustomerVO customerVO,
        OrderDAO orderDAO) throws Exception {
        String cityStateZip = "";
        String country = customerVO.getCountry();
        String city = customerVO.getCity();

        String state = customerVO.getState();
        String zipCode = customerVO.getZipCode();

        if (country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA") ||
                country.equalsIgnoreCase("")) {
            cityStateZip = city + ", " + state + " " + zipCode;
        } else if (country.equalsIgnoreCase("CA")) {
            cityStateZip = city + ", " + state + " " + zipCode;
        } else {
            cityStateZip = city + ", ";

            if (state == null) {
                cityStateZip = cityStateZip.concat("NA ");
            } else {
                cityStateZip = cityStateZip.concat(state + " ");
            }

            if ((zipCode != null) && (!zipCode.equalsIgnoreCase("99999"))) {
                cityStateZip = cityStateZip.concat(zipCode);
            }

            String fullCountryName = orderDAO.getActiveCountryName(country);

            if (fullCountryName != null) {
                cityStateZip = cityStateZip.concat(" " + fullCountryName);
            }
        }

        return cityStateZip;
    }

    public String buildFirstChoiceString(OrderDetailVO orderDetailVO, com.ftd.op.order.vo.ProductVO productVO,
    		List addOnVOList, Connection conn) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);

        ConfigurationUtil cu = ConfigurationUtil.getInstance();
        String deluxeSuffix = cu.getFrpGlobalParm("ORDER_PROCESSING", "FLORIST_REFERENCE_NUMBER_SUFFIX_DELUXE");
        String premiumSuffix = cu.getFrpGlobalParm("ORDER_PROCESSING", "FLORIST_REFERENCE_NUMBER_SUFFIX_PREMIUM");

        String firstChoiceString = productVO.getFloristReferenceNumber();

        if (orderDetailVO.getFirstColorChoice() != null && orderDetailVO.getFirstColorChoice().length() > 0) {
            firstChoiceString = firstChoiceString + " " + dao.getColorById(orderDetailVO.getFirstColorChoice());
        }

        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        if (orderDetailVO.getSizeIndicator() != null && productVO.getDeliveryType().equals("D")) {
            if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A")) { //Standard size
                firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) { // Deluxe size //
                if (productVO.getSendDeluxeRecipe() != null && productVO.getSendDeluxeRecipe().equalsIgnoreCase("Y")) {
                    firstChoiceString = firstChoiceString + deluxeSuffix;
                    firstChoiceString = firstChoiceString + " " + productVO.getProductName() + " Deluxe\n";
                    firstChoiceString = firstChoiceString + "See Special Instructions for Deluxe recipe\n";
                } else {
                    firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
                }
                firstChoiceString = firstChoiceString + "Include $" +
                    format.format(productVO.getDeluxePrice() - productVO.getStandardPrice()) +
                    " for DELUXE for 1st and 2nd choice\n";
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) { // Premium size //
                if (productVO.getSendPremiumRecipe() != null && productVO.getSendPremiumRecipe().equalsIgnoreCase("Y")) {
                    firstChoiceString = firstChoiceString + premiumSuffix;
                    firstChoiceString = firstChoiceString + " " + productVO.getProductName() + " Premium\n";
                    firstChoiceString = firstChoiceString + "See Special Instructions for Premium recipe\n";
                } else {
                    firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
                }
                firstChoiceString = firstChoiceString + "Include $" +
                    format.format(productVO.getPremiumPrice() - productVO.getStandardPrice()) +
                    " for PREMIUM for 1st and 2nd choice\n";
            }
        } else {
            firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
        }

        // add comments field in product_master
        if (productVO.getItemComments() != null) {
            firstChoiceString = firstChoiceString +
                productVO.getItemComments() + "\n";
        }

        // Need to Add Add-Ons//
        for (int i = 0; i < addOnVOList.size(); i++) {
            AddOnVO addOn = (AddOnVO) addOnVOList.get(i);
            
            // modification for Ren cards
            String description = null;
            if(addOn.getAddOnCode().startsWith("RC")) 
            {
              description = addOn.getAddOnCode() + " - " + addOn.getDesciption();
            } else 
            {
              description = addOn.getDesciption();
            }
            
            firstChoiceString = firstChoiceString + "PRICE INCLUDES " +
                addOn.getAddOnQuantity() + " " + description + "($" +
                format.format(addOn.getAddOnQuantity() * addOn.getPrice()
                                                              .doubleValue()) +
                ")" + (((addOnVOList.size() - 1) == i) ? "" : "\n");
        }

        return firstChoiceString;
    }

    public String buildSecondChoiceString(OrderDetailVO orderDetailVO, com.ftd.op.order.vo.ProductVO productVO,
    		SecondChoiceVO secondChoiceVO, List addOnVOList, Connection conn) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);

        String secondChoiceString = "";
        String substitutionIndicator = "Y";

        // if there are no substitutions, return "NONE"
        if ((substitutionIndicator != null) &&
                (substitutionIndicator.equalsIgnoreCase("N") &&
                (orderDetailVO.getSecondColorChoice() == null))) {
            secondChoiceString = "NONE. PLEASE FILL BY ITEM# TO FULL VALUE";
        } else {
            /*
             * if there is no second choice defined on the order and
             * the second choice on the product is either undefined,
             * or it's defined as None, but allows second choice color
             * to be found, then use the second choice color if it
             * exists on the order
             */
            if (((secondChoiceVO.getProductSecondChoiceId() == null) ||
                    (secondChoiceVO.getProductSecondChoiceId().equals("0") &&
                    productVO.getColorSizeFlag().equalsIgnoreCase("C")))) {
                if (orderDetailVO.getSecondColorChoice() == null && orderDetailVO.getSecondColorChoice().length() > 0) {
                    secondChoiceString = "NONE. PLEASE FILL BY ITEM# TO FULL VALUE";
                } else {
                    secondChoiceString = productVO.getFloristReferenceNumber() +
                        " " + dao.getColorById(orderDetailVO.getSecondColorChoice()) +
                        "-" + productVO.getMercuryDescription() + "\n";
                }
            } else {
                secondChoiceString = secondChoiceVO.getMercuryDescription1() +
                    " " +
                    ((secondChoiceVO.getMercuryDescription2() == null) ? ""
                                                                       : secondChoiceVO.getMercuryDescription2()) +
                    "\n";
            }
        }

        // add product comments
        if (productVO.getItemComments() != null) {
            secondChoiceString += (productVO.getItemComments() + "\n");
        }

        // add addons
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        for (int i = 0; i < addOnVOList.size(); i++) {
            AddOnVO addOn = (AddOnVO) addOnVOList.get(i);

            if (addOn.getAddOnCode().startsWith("RC")) {
                secondChoiceString += ("SIMILAR GREETING CARD APPROPRIATE FOR THE OCCASION" +
                (((addOnVOList.size() - 1) == i) ? "" : "\n"));
            } else {
                secondChoiceString = secondChoiceString + "PRICE INCLUDES " +
                    addOn.getAddOnQuantity() + " " + addOn.getDesciption() +
                    "($" +
                    format.format(addOn.getAddOnQuantity() * addOn.getPrice()
                                                                  .doubleValue()) +
                    ")" + (((addOnVOList.size() - 1) == i) ? "" : "\n");
            }
        }

        return secondChoiceString;
    }

    private String buildOccasionString(String occasionId, CustomerVO customerVO, Connection conn) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        String occasionString = "";

        if ((occasionId == null) ||
        		occasionId.equals("8") ||
                (new Integer(occasionId).intValue() <= 0)) {
            if ((customerVO.getAddressType() != null) &&
                (customerVO.getAddressType().trim().equalsIgnoreCase("FUNERAL HOME") ||
                customerVO.getAddressType().trim().equalsIgnoreCase("CEMETERY")
                )) {
                occasionString = new String("1"); // FUNERAL/SYMPATHY
            } else {
                occasionString = new String("8"); // OTHER
            }
        } else {
            int occasionInt = new Integer(occasionId).intValue();

            if (occasionInt > 13) {
                occasionString = new String("5"); // HOLIDAY
            } else if ((occasionInt <= 13) && (occasionInt >= 8)) {
                occasionString = new String("8"); // OTHER
            } else if (occasionInt < 8) {
                occasionString = new Integer(occasionInt).toString(); // HOLIDAY
            }
        }

        return dao.getBoxXOccasion(occasionString);
    }

    public String buildSpecialInstructionString(OrderDetailVO orderDetailVO,
            SourceVO sourceVO, CustomerVO customer,
            com.ftd.op.order.vo.ProductVO productVO) throws Exception {

        String specialInstructionString = "";

        if (customer.getDayPhoneExt() != null && customer.getDayPhoneExt().length() > 0) {
            specialInstructionString += "  Recipient Phone Extension: " + customer.getDayPhoneExt();
        }

        if (orderDetailVO.getSizeIndicator() != null && productVO.getDeliveryType().equals("D")) {
            if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A")) {
                if (productVO.getSendStandardRecipe() != null && productVO.getSendStandardRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getRecipe() != null ? productVO.getRecipe() : "");
                }
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) {
                if (productVO.getSendDeluxeRecipe() != null && productVO.getSendDeluxeRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getDeluxeRecipe() != null ? productVO.getDeluxeRecipe() : "");
                }
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) {
                if (productVO.getSendPremiumRecipe() != null && productVO.getSendPremiumRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getPremiumRecipe() != null ? productVO.getPremiumRecipe() : "");
                }
            }
        }
        if(orderDetailVO.getSpecialInstructions() != null) {
        	if(orderDetailVO.getSpecialInstructions().trim().length() > 0) {
   				specialInstructionString = orderDetailVO.getSpecialInstructions() + " " + specialInstructionString;
           	}
        }

        if(specialInstructionString == null || specialInstructionString.trim().length() == 0) { 
            // always default to None, because special Instructions is a required field
            specialInstructionString = "None";
        }
            
        return breakString(specialInstructionString, LINE_LENGTH);
    }

}
