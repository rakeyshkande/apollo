package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.FavoritesDAO;
import com.ftd.oe.vo.CrossSellVO;
import com.ftd.oe.vo.FavoriteVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FavoritesBO 
{

    private static final Logger logger = new Logger("com.ftd.oe.bo.FavoritesBO");
    IResourceProvider resourceProvider;
    FavoritesDAO favoritesDAO;
    
    public FavoritesBO() 
    {
    }

    public void setResourceProvider(IResourceProvider newresourceProvider) 
    {
        this.resourceProvider = newresourceProvider;
    }

    public IResourceProvider getResourceProvider() 
    {
        return resourceProvider;
    }

    public void setFavoritesDAO(FavoritesDAO newfavoritesDAO) 
    {
        this.favoritesDAO = newfavoritesDAO;
    }

    public FavoritesDAO getFavoritesDAO() 
    {
        return favoritesDAO;
    }

    public Document getFavoritesAjax() throws Exception
    {
        Connection qConn = null;
        Document doc = null;
        logger.debug("getFavoritesAjax()");
        
        try 
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            doc = favoritesDAO.getFavorites(qConn);
        } 
        catch (Throwable t) 
        {
            logger.error("Error in getCompaniesAjax()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Validate the list of favorites.
     *    There must be at least one favorite
     *    There can be no duplicate favorites
     * @param vos
     * @return
     */
    public Document validate(List vos) throws Exception
    {
        Connection conn = null;
        HashMap errors = new HashMap();
        Document doc = null;

        try
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            conn = resourceProvider.getDatabaseConnection(dataSource);
            if (conn == null)
            {
                throw new Exception("Unable to connect to database");
            }

            // Validate there is at least one favorite
            if (vos.size() < 1)
            {
                errors.put("NO PRODUCT","Favorites must have a product."); 
            }

            // Loop through each of the products, checking dups and product validity
            for (int i=0; i < vos.size(); i++)
            {
                FavoriteVO vo = (FavoriteVO) vos.get(i);
                
                // Validate the product id's exist, changing them to normal product Ids
                if (vo.getProductId() != null && !vo.getProductId().equals(""))
                {
                    String productId = favoritesDAO.getProductId(conn,vo.getProductId());
                    if (productId == null || productId.equals(""))
                    {
                        errors.put("INVALID PRODUCT","Product is not a valid product Id or Novator Id: " + vo.getProductId()); 
                    }
                    vo.setProductId(productId);
    
                    for (int j=0; j < vos.size(); j++)
                    {
                        FavoriteVO voDup = (FavoriteVO) vos.get(j);
                
                        // Validate there is no duplicate favorite ids
                        if (vo.getProductId() != null && 
                            voDup.getProductId() != null &&
                            vo.getProductId().equals(voDup.getProductId()) &&
                            i != j)
                        {
                            errors.put("DUP PRODUCT","Cannot have the same product in favorites twice:  "+ vo.getProductId()); 
                        }
                    }
                }
            }

            if (errors.size() > 0)
            {
                doc = createValidationDocument(errors);
            }
        }
        catch (Throwable t)
        {
            logger.error("Error in validate()", t);
            throw new Exception("Could not retrieve data.");
        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    if (!conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (SQLException sqle)
                {
                    logger.warn("Error closing database connection", sqle);
                }
            }
        }


        return doc;
    }

    public Document update(List vos) throws Exception
    {
        logger.debug("update");
        Connection qConn = null;
        Document doc = null;
        
        try 
        {
            // remove empty spaces
            Collections.sort(vos);
            String currentDisplaySeq = "0";
            List<FavoriteVO> blanks = new ArrayList<FavoriteVO>();
            List<FavoriteVO> orderedVOs = new ArrayList<FavoriteVO>();
            for (int i=0; i < vos.size(); i++)
            {
                FavoriteVO vo = (FavoriteVO) vos.get(i);
                if (vo.getProductId() == null || vo.getProductId().equals(""))
                {
                    blanks.add(vo);
                }
                else
                {
                    orderedVOs.add(vo);
                }
            }
            for (int i=0; i < blanks.size();i++)
            {
                orderedVOs.add(blanks.get(i));
            }
            for (int i=0; i < orderedVOs.size();i++)
            {
                orderedVOs.get(i).setDisplaySequence(Integer.toString(i+1));
            }
            
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            
            for (int i=0; i < vos.size(); i++)
            {
                FavoriteVO vo = (FavoriteVO) vos.get(i);
                if (vo.getProductId() == null || vo.getProductId().equals(""))
                {
                    doc = favoritesDAO.deleteFavorite(qConn,  vo.getDisplaySequence());
                }
                else
                {
                    doc = favoritesDAO.updateFavorite(qConn,  vo);
                }
            }
        } 
        catch (Throwable t) 
        {
            logger.error("Error in update()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }
    
    protected Document createValidationDocument(HashMap errors) throws Exception
    {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, "Validation Failed");
        
        OEActionBase.createValidationResultSet(doc,"N",errors);
        
        return doc;

    }
    
    
}
