package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.dao.IOTWDAO;
import com.ftd.oe.vo.IOTWDeliveryDateVO;
import com.ftd.oe.vo.IOTWVO;
import com.ftd.oe.vo.NovatorUpdateVO;
import com.ftd.oe.vo.PriceHeaderVO;
import com.ftd.oe.vo.SourceCodeVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class IOTWFeedBO {

    public static String CREATE_FEED = "create";
    public static String UPDATE_FEED = "update";
    public static String REMOVE_FEED = "remove";
    public static String CONTENT_ENV = "CONTENT";
    public static String TEST_ENV = "TEST";
    public static String UAT_ENV = "UAT";
    public static String LIVE_ENV = "LIVE";
    private static Logger logger = new Logger("com.ftd.oe.bo.IOTWFeedBO");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat displayFormatter = new SimpleDateFormat("MM/dd/yyyy");
    private ConfigurationUtil configurationUtil;
    private IOTWDAO iotwDAO;
    private FeedBO feedBO;


    public IOTWFeedBO() {
    }

    /**
     * Send IOTW program updates
     * @param conn Database connection
     * @param novatorUpdateVO Novator environment information
     * @param iotwList List of IOTW programs to send
     * @param action Update action
     * @throws Exception
     */
    public HashMap send(Connection conn, NovatorUpdateVO novatorUpdateVO, List<IOTWVO> iotwList, String action) throws Exception
    {
        boolean content, test, uat, live;
        HashMap sourceCodeMap = new HashMap();
        SourceCodeVO sourceCodeVO = null;
        IOTWVO iotwVO = null;
        String response = null;

        HashMap errors = null;
        String error = null;
        String environment = null;
        StringBuffer msg = new StringBuffer();

        int i = 0;
        for(i = 0; i < iotwList.size(); i++){
            iotwVO = iotwList.get(i);

            // Reset values
            content = false;
            test = false;
            uat = false;
            live = false;
            
            // Obtain source code information
            if(sourceCodeMap.containsKey(iotwVO.getIotwSourceCode())){
                sourceCodeVO = (SourceCodeVO)sourceCodeMap.get(iotwVO.getIotwSourceCode());
            } else{
                sourceCodeVO = iotwDAO.getSourceCode(conn, iotwVO.getIotwSourceCode()); 
                
                // If source code no longer exists create a blank SourceCodeVO
                if(sourceCodeVO == null)
                    sourceCodeVO = new SourceCodeVO();
                sourceCodeMap.put(iotwVO.getIotwSourceCode(), sourceCodeVO);
            }

            // Convert IOTW program data into XML
            Document doc = generateXML(conn, iotwVO, sourceCodeVO, action);
            
            String xmlString = JAXPUtil.toStringNoFormat(doc);
            
            if(logger.isDebugEnabled())
                logger.debug(xmlString);
            
            try {
                // Send IOTW program information to Novator
                if (novatorUpdateVO.isUpdateContent()) {
                    try{
                        response = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_CONTENT_FEED_IP, OrderEntryConstants.NOVATOR_CONTENT_FEED_PORT);
                        feedBO.parseResponse(response);
                        content = true;
                    } catch (Throwable t){
                        environment = CONTENT_ENV; 
                        throw t;
                    }
                }
                if (novatorUpdateVO.isUpdateTest()) {
                    try{
                        response = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_TEST_FEED_IP, OrderEntryConstants.NOVATOR_TEST_FEED_PORT);
                        feedBO.parseResponse(response);
                        test = true;
                    } catch (Throwable t){
                        environment = TEST_ENV;        
                        throw t;
                    }
                }
                if (novatorUpdateVO.isUpdateUAT()) {
                    try{
                        response = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_UAT_FEED_IP, OrderEntryConstants.NOVATOR_UAT_FEED_PORT);
                        feedBO.parseResponse(response);
                        uat = true;
                    } catch (Throwable t){
                        environment = UAT_ENV;        
                        throw t;
                    }
                }
                if (novatorUpdateVO.isUpdateLive()) {
                    try{
                        response = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_PRODUCTION_FEED_IP, OrderEntryConstants.NOVATOR_PRODUCTION_FEED_PORT);
                        feedBO.parseResponse(response);
                        live = true;
                    } catch (Throwable t){
                        environment = LIVE_ENV;        
                        throw t;
                    }
                }
            } catch(Throwable t) {
                error = t.getMessage() + " (" + environment + ")";
            }
            
            if(error != null){
                errors = new HashMap();
                String blank = " ";
                int errorMsgCnt = 0;
                
                // Display the error
                errors.put(errorMsgCnt++, error);
                
                // Display which program encountered an error and which
                // Novator environments the program was or wasn't processed by
                errors.put(errorMsgCnt++, blank);
                msg.setLength(0);
                if(content || test || uat || live){
                    msg.append("The following IOTW program encountered an error and was processed by ");
                    if(content) {msg.append(" "); msg.append(CONTENT_ENV);}
                    if(test) {msg.append(" "); msg.append(TEST_ENV);}
                    if(uat) {msg.append(" "); msg.append(UAT_ENV);}
                    if(live) {msg.append(" "); msg.append(LIVE_ENV);}
                    msg.append(" and not processed by ");
                } else{
                    msg.append("The following IOTW program encountered an error and was not processed by ");
                }
                if(novatorUpdateVO.isUpdateContent() && !content) {msg.append(" "); msg.append(CONTENT_ENV);}
                if(novatorUpdateVO.isUpdateTest() && !test) {msg.append(" "); msg.append(TEST_ENV);}
                if(novatorUpdateVO.isUpdateUAT() && !uat) {msg.append(" "); msg.append(UAT_ENV);}
                if(novatorUpdateVO.isUpdateLive() && !live) {msg.append(" "); msg.append(LIVE_ENV);}
                msg.append(".");
                errors.put(errorMsgCnt++, msg.toString());
                
                iotwVO = iotwList.get(i);
                msg.setLength(0);
                msg.append(iotwVO.getSourceCode());
                msg.append(" ");
                msg.append(iotwVO.getProductId());
                msg.append(" ");
                msg.append(iotwVO.getIotwSourceCode());
                msg.append(" ");
                msg.append(displayFormatter.format(iotwVO.getStartDate()));
                msg.append(" ");
                if(iotwVO.getEndDate() != null) msg.append(displayFormatter.format(iotwVO.getEndDate()));
                errors.put(errorMsgCnt++, msg.toString());

                // Display which programs were processed by Novator
                errors.put(errorMsgCnt++, blank);
                errors.put(errorMsgCnt++, "The following IOTW programs were processed by Novator");
                for(int j = 0; j < i; j++){
                    iotwVO = iotwList.get(j);
                    msg.setLength(0);
                    msg.append(iotwVO.getSourceCode());
                    msg.append(" ");
                    msg.append(iotwVO.getProductId());
                    msg.append(" ");
                    msg.append(iotwVO.getIotwSourceCode());
                    msg.append(" ");
                    msg.append(displayFormatter.format(iotwVO.getStartDate()));
                    msg.append(" ");
                    if(iotwVO.getEndDate() != null) msg.append(displayFormatter.format(iotwVO.getEndDate()));
                    errors.put(errorMsgCnt++, msg.toString());
                }                

                // Display which programs were not processed by Novator
                errors.put(errorMsgCnt++, blank);
                errors.put(errorMsgCnt++, "The following IOTW programs were not processed by Novator");
                for(int j = (i + 1); j < iotwList.size(); j++){
                    iotwVO = iotwList.get(j);
                    msg.setLength(0);
                    msg.append(iotwVO.getSourceCode());
                    msg.append(" ");
                    msg.append(iotwVO.getProductId());
                    msg.append(" ");
                    msg.append(iotwVO.getIotwSourceCode());
                    msg.append(" ");
                    msg.append(displayFormatter.format(iotwVO.getStartDate()));
                    msg.append(" ");
                    if(iotwVO.getEndDate() != null) msg.append(displayFormatter.format(iotwVO.getEndDate()));
                    errors.put(errorMsgCnt++, msg.toString());
                }
                return errors;
            }
        }
        return null;
    }

    
    /**
     * Generate IOTW program XML expected by Novator
     * @param iotwVO IOTW program
     * @param sourceCodeVO Source code information
     * @param action Update action
     * @return IOTW program XML
     * @throws Exception
     */
    private Document generateXML(Connection conn, IOTWVO iotwVO, SourceCodeVO sourceCodeVO, String action) throws Exception {
        Document doc = null;
        
        // Convert the VO to XML
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement("iotw");
        
        // If original data exists this is an update. Novator expects the new and existing data.  
        if(StringUtils.isNotBlank(iotwVO.getOriginalSourceCode())){
            JAXPUtil.addAttribute(root, "action", UPDATE_FEED);
            doc.appendChild(root);

            addNode(doc, root, "source_code", iotwVO.getOriginalSourceCode());
            addNode(doc, root, "product_id", iotwDAO.getProductId(conn, iotwVO.getOriginalProductId()).getNormalizedNovatorId());
            addNode(doc, root, "start_date", sdf.format(iotwVO.getOriginalStartDate()));
            addNode(doc, root, "end_date", iotwVO.getOriginalEndDate() != null?sdf.format(iotwVO.getOriginalEndDate()):"");
            addNode(doc, root, "new_source_code", iotwVO.getSourceCode());
            addNode(doc, root, "new_product_id", iotwDAO.getProductId(conn, iotwVO.getProductId()).getNormalizedNovatorId());
            addNode(doc, root, "new_start_date", sdf.format(iotwVO.getStartDate()));
            addNode(doc, root, "new_end_date", iotwVO.getEndDate() != null?sdf.format(iotwVO.getEndDate()):"");
        } else{
            JAXPUtil.addAttribute(root, "action", action);
            doc.appendChild(root);

            addNode(doc, root, "source_code", iotwVO.getSourceCode());
            addNode(doc, root, "product_id", iotwDAO.getProductId(conn, iotwVO.getProductId()).getNormalizedNovatorId());
            addNode(doc, root, "start_date", sdf.format(iotwVO.getStartDate()));
            addNode(doc, root, "end_date", iotwVO.getEndDate() != null?sdf.format(iotwVO.getEndDate()):"");
            addNode(doc, root, "new_source_code", "");
            addNode(doc, root, "new_product_id", "");
            addNode(doc, root, "new_start_date", "");
            addNode(doc, root, "new_end_date", "");                        
        }
        
        // Add delivery discount dates
        Element delDiscDateNode = doc.createElement("delivery_discount_dates");
        root.appendChild(delDiscDateNode);
        for(Iterator<IOTWDeliveryDateVO> it = iotwVO.getDeliveryDiscountDates().iterator(); it.hasNext();){
            IOTWDeliveryDateVO IOTWDelDateVO = it.next();
            addNode(doc, delDiscDateNode, "delivery_discount_date", sdf.format(IOTWDelDateVO.getDiscountDate()));
        }

        addCDATANode(doc, root, "product_page_messaging", iotwVO.getProductPageMessage());
        addCDATANode(doc, root, "calendar_messaging", iotwVO.getCalendarMessage());
        addNode(doc, root, "iotw_source_code", iotwVO.getIotwSourceCode());
        addNode(doc, root, "special_offer_flag", iotwVO.isSpecialOffer()?"Y":"N");
        addCDATANode(doc, root, "wording_color", iotwVO.getWordingColor());
        addNode(doc, root, "program_name", sourceCodeVO.getProgramReward().getProgramName());

        /* Add price header information.
         * Novator cannot accept multiple price headers.  Based on feedback from 
         * WebOps the smallest discount amount will be sent.  Also, discounts
         * with an amount of 0 will be omitted from the feed.
         */ 
        Element priceHeadersNode = doc.createElement("discounts");
        root.appendChild(priceHeadersNode);
        PriceHeaderVO priceHeaderVO = null;
        PriceHeaderVO smallestPriceHeaderVO = null;
        BigDecimal zeroAmt = new BigDecimal(0);
        BigDecimal currentDiscAmt = null;
        BigDecimal smallestDiscAmt = null;
        // Obtain the smallest discount amount
        for(Iterator<PriceHeaderVO> phIt = sourceCodeVO.getPriceHeaderList().iterator(); phIt.hasNext();){
            priceHeaderVO = phIt.next();
            currentDiscAmt = new BigDecimal(priceHeaderVO.getDiscountAmt());

            // If a smallest discount exists compare against the current discount
            if(smallestPriceHeaderVO != null){
                smallestDiscAmt = new BigDecimal(smallestPriceHeaderVO.getDiscountAmt());
                if(currentDiscAmt.compareTo(smallestDiscAmt) < 0){
                    smallestPriceHeaderVO = priceHeaderVO;        
                }
            } // If the current discount amount is greater than zero set it as the smallest discount
            else if(currentDiscAmt.compareTo(zeroAmt) > 0){
                smallestPriceHeaderVO = priceHeaderVO;        
            }
        }
        
        // If a valid discount exists create a discount node
        if(smallestPriceHeaderVO != null){
            Element priceHeaderNode = doc.createElement("discount");
            addNode(doc, priceHeaderNode, "discount_type", smallestPriceHeaderVO.getDiscountType());
            addNode(doc, priceHeaderNode, "discount_amt", smallestPriceHeaderVO.getDiscountAmt());
            addNode(doc, priceHeaderNode, "min_dollar_amt", smallestPriceHeaderVO.getMinDollarAmt());
            addNode(doc, priceHeaderNode, "max_dollar_amt", smallestPriceHeaderVO.getMaxDollarAmt());
            priceHeadersNode.appendChild(priceHeaderNode);
        }

        return doc;
    }
    
    /**
     * Add a node
     * @param doc Document
     * @param parent Parent node
     * @param nodeName Node name
     * @param nodeValue Node value
     * @return Node
     */
    private Element addNode(Document doc, Element parent, String nodeName, String nodeValue)
    {
        Element node = doc.createElement(nodeName);
        node.setTextContent(nodeValue);
        parent.appendChild(node);
        return node;
    }
    
    /**
     * Add a CDATA section within a node
     * @param doc Document
     * @param parent Parent node
     * @param nodeName Node name
     * @param nodeValue Node value
     * @return Node
     */
    private Element addCDATANode(Document doc, Element parent, String nodeName, String nodeValue)
    {
        Element node = doc.createElement(nodeName);
        CDATASection  cdata = doc.createCDATASection(nodeValue);
        node.appendChild(cdata);
        parent.appendChild(node);
        return node;
    }

    /**
     * Send information to Novator
     * @param xmlString Information
     * @param ipParamName IP configuration parameter
     * @param portParamName Port configuratin parameter
     * @return Novator response
     * @throws Exception
     */
    private String sendNovatorXML(String xmlString, String ipParamName, String portParamName) throws Exception
    {
        ConfigurationUtil cu = getConfigurationUtil();
        String server = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,ipParamName);
        String port = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,portParamName);
        Integer portInt = Integer.parseInt(port);
        String timeout = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,OrderEntryConstants.NOVATOR_FEED_TIMEOUT);
        Integer timeoutInt = Integer.parseInt(timeout);
        String response = SendUpdateFeed.send(server, portInt, timeoutInt, xmlString);
        
        return response;
    }

    /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param List List of objects to for whom messages need to be sent
     * @return HashMap collection of errors occured if any 
     */
      
    public HashMap<String,java.lang.Throwable> sendMyBuys(List<IOTWVO> iotwList) 
    {
            HashMap<String,java.lang.Throwable> errors = new HashMap<String, java.lang.Throwable>();
            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
               IOTWVO iotwVO = it.next();
               MessageToken messageToken = new MessageToken();
               messageToken.setMessage(iotwVO.getProductId());
               messageToken.setStatus("MY BUYS");
               try {
               Dispatcher dispatcher = Dispatcher.getInstance();
               dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
               } catch (Exception e)
               {
                 errors.put(iotwVO.getProductId(), e);
               }
 
            }
        return errors;
        
    }
    
    public ConfigurationUtil getConfigurationUtil() throws Exception{
        if (configurationUtil == null)
        {
            configurationUtil = ConfigurationUtil.getInstance();
        }
        return configurationUtil;
    }

    public void setIotwDAO(IOTWDAO newiotwDAO) {
        this.iotwDAO = newiotwDAO;
    }

    public IOTWDAO getIotwDAO() {
        return iotwDAO;
    }

    public void setFeedBO(FeedBO newfeedBO) {
        this.feedBO = newfeedBO;
    }

    public FeedBO getFeedBO() {
        return feedBO;
    }
}
