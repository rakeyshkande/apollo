package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.CrossSellDAO;
import com.ftd.oe.vo.CrossSellVO;
import com.ftd.oe.vo.CrossSellDetailVO;
import com.ftd.oe.vo.NovatorUpdateVO;
import com.ftd.oe.vo.ProductCheckVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.dataaccess.TransactionUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.StringTokenizer;

import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CrossSellBO
{
    private static Logger logger = new Logger("com.ftd.oe.bo.CrossSellBO");
    private CrossSellDAO crossSellDAO;
    private FeedBO feedBO;
    private IResourceProvider resourceProvider;
    private ConfigurationUtil configUtil;
    private TransactionUtil   transactionUtil;
    
    private static String RS_SUCCESS = "Y";
    private static String RS_FAILED = "N";
    
    public CrossSellBO()
    {
    }

    public void setCrossSellDAO(CrossSellDAO param)
    {
        this.crossSellDAO = param;
    }

    public CrossSellDAO getCrossSellDAO()
    {
        return crossSellDAO;
    }

    public void setResourceProvider(IResourceProvider param)
    {
        this.resourceProvider = param;
    }

    public IResourceProvider getResourceProvider()
    {
        return resourceProvider;
    }

    /**
     * Get the list of companies for use in the cross sell maintenance.
     * @return
     */
    public Document getCompaniesAjax() throws Exception
    {
        Connection qConn = null;
        Document doc = null;
        logger.debug("getCompaniesAjax()");
        
        try 
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            doc = crossSellDAO.getCompaniesAjax(qConn);
        } 
        catch (Throwable t) 
        {
            logger.error("Error in getCompaniesAjax()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Get all cross sells where the master product is a product for this company.
     * @param companyId
     * @return
     * @throws Exception
     */
    public List<CrossSellVO> getCrossSellByCompany(String companyId) throws Exception
    {
        logger.debug("getCrossSellByCompany(" + companyId + ")");
        Connection qConn = null;
        List<CrossSellVO> vos = new ArrayList<CrossSellVO>();
        
        try 
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            vos = crossSellDAO.getCrossSellByCompany(qConn, companyId);
        } 
        catch (Throwable t) 
        {
            logger.error("Error in getCrossSell()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return vos;
    }

    /**
     * Get the cross sell products where this product is the master.
     * @param productId
     * @return
     * @throws Exception
     */
    public Document getCrossSellProductAjax(String productId) throws Exception
    {
        logger.debug("getCrossSellProductAjax");
        Connection qConn = null;
        Document doc = null;
        
        try 
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            doc = crossSellDAO.getCrossSellProductAjax(qConn,  productId);
        } 
        catch (Throwable t) 
        {
            logger.error("Error in getCrossSellProductAjax()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    public Document getMasterRecordsAjax(String productId) throws Exception
    {
        logger.debug("getMasterRecordsAjax");
        Connection qConn = null;
        Document doc = null;
        
        try 
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) 
            {
                throw new Exception("Unable to connect to database");
            }
            ProductCheckVO productCheckVO = crossSellDAO.getProductId(qConn,productId);
            doc = crossSellDAO.getMasterRecordsAjax(qConn,  productCheckVO.getNormalizedId());
        } 
        catch (Throwable t) 
        {
            logger.error("Error in getMasterRecordsAjax()",t);
            throw new Exception("Could not retrieve data.");
        } finally 
        {
            if( qConn!=null ) 
            {
                try 
                {
                    if( !qConn.isClosed() ) 
                    {
                        qConn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Update the cross sell product.
     * @param vo
     * @return
     * @throws Exception
     */
    public Document update(CrossSellVO vo, NovatorUpdateVO updateVO) throws Exception
    {
        logger.debug("update");
        Connection conn = null;
        Document doc = null;
        UserTransaction ut = null;
        
        try 
        {
            ut = transactionUtil.createUserTransaction(OrderEntryConstants.JOE_CONFIG_FILE,"transaction_timeout_in_seconds","jndi_usertransaction_entry");
            ut.begin();
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);

            // If the first product is null, then this is a delete.  Otherwise it is an update
            if (vo.getDetailVO().size() == 0)
            {
                doc = crossSellDAO.deleteCrossSell(conn,  vo);
            }
            else
            {
                doc = crossSellDAO.updateCrossSell(conn,  vo);
            }
            
            // Send the update to Novator
            List<String> messages = new ArrayList<String>();
            boolean success = send(updateVO,vo, messages);
            if (success)
            {
                OEActionBase.createValidationResultSet(doc, RS_SUCCESS, messages);    
            }
            else
            {
                OEActionBase.createValidationResultSet(doc, RS_FAILED, messages);    
            }
            
            if (success)
            {
                ut.commit();
            }
            else
            {
                transactionUtil.rollback(ut);
            }

        } 
        catch (Throwable t) 
        {
            logger.error("Error in update()",t);
            transactionUtil.rollback(ut);
            throw new Exception("Could not retrieve data.");
        } 
        finally 
        {
            if( conn!=null ) 
            {
                try 
                {
                    if( !conn.isClosed() ) 
                    {
                        conn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Delete the product from all cross sell products it is on.
     * @param productId
     * @param masterCrossSellProductIdString
     * @return
     * @throws Exception
     */
    public Document deleteCrossSellProduct(String productId, String masterCrossSellProductIdString, String csrId, NovatorUpdateVO updateVO) throws Exception
    {
        logger.debug("deleteCrossSellProduct");
        logger.debug(productId + " " + masterCrossSellProductIdString);
        Connection conn = null;
        Document doc = null;
        UserTransaction ut = null;
        boolean successful = true;
        List<String> masterProductList = new ArrayList<String>();
        List<String> messages = new ArrayList<String>();
        
        try 
        {
            doc = createBlankDocument();
            
            ut = transactionUtil.createUserTransaction(OrderEntryConstants.JOE_CONFIG_FILE,"transaction_timeout_in_seconds","jndi_usertransaction_entry");
            ut.begin();
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);

            StringTokenizer tokenizer = new StringTokenizer(masterCrossSellProductIdString," ");
            while (tokenizer.hasMoreTokens())
            {
                masterProductList.add(tokenizer.nextToken());
            }
            
            int productIndex = 0;
            for (;productIndex < masterProductList.size(); productIndex++)
            {
                String crossSellProductId = masterProductList.get(productIndex);
                // Get the XC
                CrossSellVO vo = crossSellDAO.getCrossSellProduct(conn,crossSellProductId);
                
                vo.setUpdatedBy(csrId);
                if (vo != null)
                {
                    // Remove the product
                    removeProduct(vo,productId);
                    
                    // Update
                    if (vo.getDetailVO().size() == 0)
                    {
                        // Remove the cross sell
                        crossSellDAO.deleteCrossSell(conn,vo);
                    }
                    else
                    {
                        // Save the cross sell
                        doc = crossSellDAO.updateCrossSell(conn,vo);
                    }
                    // Send the update to Novator
                    successful = send(updateVO,vo, messages);
                    
                    if (!successful)
                    {
                        break;
                    }
                }
                else
                {
                    logger.debug("Could not find master product " + crossSellProductId);
                }
            }

            if (successful)
            {
                OEActionBase.createValidationResultSet(doc, RS_SUCCESS, messages);    
                ut.commit();
            }
            else
            {
                OEActionBase.createValidationResultSet(doc, RS_FAILED, messages);    
                transactionUtil.rollback(ut);

                // Figure out which worked and which didn't based on the value
                // of productIndex and put the info in the document
                HashMap<String,String> errors = new HashMap<String,String>();
                for (int i=0; i < productIndex; i++)
                {
                    errors.put("PRODUCT_SEND","Product " + masterProductList.get(i) + " sent to Novator ");
                }
                
                OEActionBase.createValidationResultSet(doc,"N",errors);

            }
        } 
        catch (Throwable t) 
        {
            logger.error("Error in deleteCrossSellProduct()",t);
            transactionUtil.rollback(ut);
            throw new Exception("Could not retrieve data.");
        } 
        finally 
        {
            if( conn!=null ) 
            {
                try 
                {
                    if( !conn.isClosed() ) 
                    {
                        conn.close();
                    }
                } 
                catch (SQLException sqle) 
                {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Validate the CrossSellVO.
     * @param vo
     * @return
     */
    public Document validate(CrossSellVO vo) throws Exception
    {
        Connection conn = null;
        HashMap errors = new HashMap();
        Document doc = null;

        try
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            conn = resourceProvider.getDatabaseConnection(dataSource);
            if (conn == null)
            {
                throw new Exception("Unable to connect to database");
            }

            // Validate there is a product Id
            if (vo.getProductId() == null || vo.getProductId().equals(""))
            {
                errors.put("NO PRODUCT","Cross Sell must have a product."); 
            }

            // Validate the product id's exist, changing them to normal product Ids
            ProductCheckVO productCheckVO = crossSellDAO.getProductId(conn,vo.getProductId());
            String productId = productCheckVO.getNormalizedId();
            if (productId == null || productId.equals(""))
            {
                errors.put("INVALID PRODUCT","Product is not a valid product Id, Novator Id or Subcode."); 
            }
            CrossSellVO tempVO = crossSellDAO.getCrossSellProduct(conn, productId);
            logger.debug(tempVO.getCrossSellMasterId() + " " + tempVO.getProductId() + " " + tempVO.getUpsellMasterId());
            vo.setCrossSellMasterId(tempVO.getCrossSellMasterId());
            vo.setProductId(productCheckVO.getProductId());
            vo.setUpsellMasterId(productCheckVO.getUpsellMasterId());
            logger.debug(vo.getProductId() + " " + vo.getUpsellMasterId());

            for (int i=0; i<vo.getDetailVO().size(); i++) {
                CrossSellDetailVO csd = (CrossSellDetailVO) vo.getDetailVO().get(i);
                productCheckVO = crossSellDAO.getProductId(conn, csd.getDetailProductId());
                String detailId = productCheckVO.getNormalizedId();
                if (detailId == null || detailId.equals("")) {
                    String msg = "Cross Sell Product " + (i+1) + " is not a valid product Id, Novator Id or Subcode.";
                    logger.error(msg);
                    errors.put("INVALID PRODUCT CS" + (i+1), msg); 
                } else if (detailId.equalsIgnoreCase(productId)) {
                    String msg = "Cross Sell " + (i+1) + " Product cannot match the Product.";
                    logger.error(msg);
                    errors.put("DUP PRODUCT CS" + (i+1), msg); 
                } else {
                    for (int j=(i+1); j<vo.getDetailVO().size(); j++) {
                        CrossSellDetailVO tempcsd = (CrossSellDetailVO) vo.getDetailVO().get(j);
                        if ( (tempcsd.getDetailProductId() != null && tempcsd.getDetailProductId().equalsIgnoreCase(detailId) ) || 
                                (tempcsd.getDetailUpsellId() != null && tempcsd.getDetailUpsellId().equalsIgnoreCase(detailId)) ) {

                            String msg = "Cross Sell " + (i+1) + " Product must be different than Cross Sell " + (j+1) + " Product.";
                            logger.error(msg);
                            errors.put("DUP PRODUCT CS" + (i+1), msg);
                        }
                    }
                }
                csd.setDetailProductId(productCheckVO.getProductId());
                csd.setDetailUpsellId(productCheckVO.getUpsellMasterId());
            }

/*
            // Validate there is at least one cross sell 
            if (vo.getCrossSellProductId1() == null || vo.getCrossSellProductId1().equals(""))
            {
                if (vo.getCrossSellProductId2() == null || vo.getCrossSellProductId2().equals(""))
                {
                    if (vo.getCrossSellProductId3() == null || vo.getCrossSellProductId3().equals(""))
                    {
                        // Deleting the cross sell, ignore this
                        //errors.put("NO CROSS SELL PRODUCT","Cross Sell must have a cross sell product."); 
                    }
                }
            }
*/
            
            if (errors.size() > 0)
            {
                doc = createValidationDocument(errors,RS_FAILED);
            }
        }
        catch (Throwable t)
        {
            logger.error("Error in validate()", t);
            throw new Exception("Could not retrieve data.");
        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    if (!conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (SQLException sqle)
                {
                    logger.warn("Error closing database connection", sqle);
                }
            }
        }


        return doc;
    }
    
    /**
     * Send the appropriate updates to the appropriate Novator system.
     * @param novatorUpdateVO
     * @param crossSellVO 
     * @return results
     */
    public boolean send(NovatorUpdateVO novatorUpdateVO, CrossSellVO crossSellVO, List<String> messages) throws Exception
    {
        Connection conn = null;
        boolean successful = true;
        
        try
        {
            String dataSource = OrderEntryConstants.DATASOURCE;
            conn = resourceProvider.getDatabaseConnection(dataSource);
            if (conn == null)
            {
                throw new Exception("Unable to connect to database");
            }

            Document doc = createCSDocument(conn, crossSellVO);
            
            String xmlString = JAXPUtil.toStringNoFormat(doc);
            logger.debug(xmlString);
            logger.debug("Updating Novator " + novatorUpdateVO.isUpdateContent() + " " + novatorUpdateVO.isUpdateUAT() + " " + novatorUpdateVO.isUpdateTest() + " " + novatorUpdateVO.isUpdateLive());
            
            if (novatorUpdateVO.isUpdateTest() && successful)
            {
                try 
                {
                    String result = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_TEST_FEED_IP, OrderEntryConstants.NOVATOR_TEST_FEED_PORT);
                    feedBO.parseResponse(result);
                    messages.add(crossSellVO.getProductId()  + " Novator Feed Successful (TEST)");    
                }
                catch(Throwable t) 
                {
                    successful = false;
                    messages.add(crossSellVO.getProductId()  + " " + t.getMessage() + "  (TEST)");    
                }
            }
            if (novatorUpdateVO.isUpdateUAT() && successful)
            {
                try 
                {
                    String result = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_UAT_FEED_IP, OrderEntryConstants.NOVATOR_UAT_FEED_PORT);
                    feedBO.parseResponse(result);
                    messages.add(crossSellVO.getProductId()  + " Novator Feed Successful (UAT)");    
                }
                catch(Throwable t) 
                {
                    successful = false;
                    messages.add(crossSellVO.getProductId()  + " " + t.getMessage() + "  (UAT)");    
                }
            }
            if (novatorUpdateVO.isUpdateLive() && successful)
            {
                try 
                {
                    String result = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_PRODUCTION_FEED_IP, OrderEntryConstants.NOVATOR_PRODUCTION_FEED_PORT);
                    feedBO.parseResponse(result);
                    messages.add(crossSellVO.getProductId()  + " Novator Feed Successful (LIVE)");    
                }
                catch(Throwable t) 
                {
                    successful = false;
                    messages.add(crossSellVO.getProductId()  + " " + t.getMessage() + "  (LIVE)");    
                }
            }
            if (novatorUpdateVO.isUpdateContent() && successful)
            {
                try 
                {
                    String result = sendNovatorXML(xmlString, OrderEntryConstants.NOVATOR_CONTENT_FEED_IP, OrderEntryConstants.NOVATOR_CONTENT_FEED_PORT);
                    feedBO.parseResponse(result);
                    messages.add(crossSellVO.getProductId()  + " Novator Feed Successful (CONTENT)");    
                }
                catch(Throwable t) 
                {
                    successful = false;
                    messages.add(crossSellVO.getProductId()  + " " + t.getMessage() + "  (CONTENT)");    
                }
            }
            
        }
        catch (Exception e)
        {
            logger.error("Error in send()", e);
            throw e;
        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    if (!conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (SQLException sqle)
                {
                    logger.warn("Error closing database connection", sqle);
                }
            }
        }
        
        return successful;
    }
    
    protected String sendNovatorXML(String xmlString, String ipParamName, String portParamName) throws Exception
    {
        ConfigurationUtil cu = getConfigurationUtil();
        String server = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,ipParamName);
        String port = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,portParamName);
        Integer portInt = Integer.parseInt(port);
        String timeout = cu.getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,OrderEntryConstants.NOVATOR_FEED_TIMEOUT);
        Integer timeoutInt = Integer.parseInt(timeout);
        String response = SendUpdateFeed.send(server, portInt, timeoutInt, xmlString);
        
        return response;
    }
    
    protected Document createCSDocument(Connection conn, CrossSellVO crossSellVO) throws Exception
    {
        CrossSellDAO dao = new CrossSellDAO();

        // Convert the VO to XML
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement("cross_sell");
        doc.appendChild(root);
        
        String novatorId = dao.getNovatorId(conn, crossSellVO.getNormalizedProduct());
        // Upsells don't have a novatorId, just use the productId
        if (StringUtils.isBlank(novatorId))
        {
            novatorId = crossSellVO.getNormalizedProduct();
        }
        if(StringUtils.isBlank(novatorId)) 
        {
            throw new Exception("A Novator Id does not exist for product id " + crossSellVO.getProductId() + " " + crossSellVO.getUpsellMasterId());    
        }
        
        addNode(doc, root, "product_id", novatorId);

        Element crossNode = doc.createElement("cross_sell_products");
        root.appendChild(crossNode);

        for (int i=0; i<crossSellVO.getDetailVO().size(); i++) {
            CrossSellDetailVO csd = (CrossSellDetailVO) crossSellVO.getDetailVO().get(i);
            addCrossSellNode(conn, doc, crossNode, csd.getNormalizedProduct(), Integer.toString(i+1));
        }

        return doc;
    }

    protected void addCrossSellNode(Connection conn, Document doc, Element parent, String productId, String sequence) throws Exception
    {
        if (productId != null && !productId.equals(""))
        {
            CrossSellDAO dao = new CrossSellDAO();
            String novatorId = dao.getNovatorId(conn, productId);
            if (StringUtils.isBlank(novatorId))
            {
                novatorId = productId;
            }
            if(StringUtils.isBlank(novatorId)) 
            {
                throw new Exception("A Novator Id does not exist for product id " + productId);    
            }

            Element crossNode = doc.createElement("cross_sell_product");
            parent.appendChild(crossNode);

            addNode(doc, crossNode, "product_id",novatorId);
            addNode(doc, crossNode, "display_sequence",sequence);
        }
    }
    
    protected Element addNode(Document doc, Element parent, String nodeName, String nodeValue)
    {
        Element node = doc.createElement(nodeName);
        node.setTextContent(nodeValue);
        parent.appendChild(node);
        return node;
    }
    
    
    protected Document createValidationDocument(HashMap errors, String rsStatus) throws Exception
    {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, "Validation Failed");
        
        OEActionBase.createValidationResultSet(doc,rsStatus,errors);
        
        return doc;

    }

    protected Document createBlankDocument() throws Exception
    {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, "");
        
        
        return doc;

    }
    
    /**
     * Remove the product from the cross Sell products.  Adjust the product
     * order if needed.
     * @param vo
     * @param productId
     */
    protected void removeProduct(CrossSellVO vo, String productId)
    {
        logger.debug("removeProduct(" + productId + ")");

        for (int i=0; i<vo.getDetailVO().size(); i++) {
            CrossSellDetailVO temp = (CrossSellDetailVO) vo.getDetailVO().get(i);
            String detailId = temp.getDetailProductId();
            String upsellId = temp.getDetailUpsellId();

            if (detailId != null && detailId.equalsIgnoreCase(productId) ||
                upsellId != null && upsellId.equalsIgnoreCase(productId)) {
                
                logger.debug("found VO to remove: " + i);
                vo.getDetailVO().remove(i);
                break;
            }
            
        }

    }

    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (configUtil == null)
        {
            configUtil = ConfigurationUtil.getInstance();
        }
        return configUtil;
    }

    public void setFeedBO(FeedBO newfeedBO) {
        this.feedBO = newfeedBO;
    }

    public FeedBO getFeedBO() {
        return feedBO;
    }

    public void setTransactionUtil(TransactionUtil transactionUtil)
    {
        this.transactionUtil = transactionUtil;
    }

    public TransactionUtil getTransactionUtil()
    {
        return transactionUtil;
    }
    
}
