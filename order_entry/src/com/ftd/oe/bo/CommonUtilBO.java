package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CommonUtilBO
{
    private static Logger logger = new Logger("com.ftd.oe.bo.CommonUtilBO");

    private ConfigurationUtil configUtil = null;
    
    
    public CommonUtilBO()
    {
    }
    
    
    public Document getNovatorUpdateSetup() throws Exception
    {
        Document doc = null;
        
        try 
        {
            doc = JAXPUtil.createDocument();
            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
            doc.appendChild(root);
            JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_STATUS,"Y");
            JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, "");

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "novator_update_setup");
            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
            root.appendChild(rowset);

            Element record = doc.createElement(OrderEntryConstants.TAG_RECORD);
            rowset.setAttribute(OrderEntryConstants.TAG_ROW, "1");
            rowset.appendChild(record);

            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_CONTENT_FEED_ALLOWED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_TEST_FEED_ALLOWED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_PRODUCTION_FEED_ALLOWED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_UAT_FEED_ALLOWED );

            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_CONTENT_FEED_CHECKED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_TEST_FEED_CHECKED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_PRODUCTION_FEED_CHECKED );
            addNovatorUpdateAllowedNode(doc, record,OrderEntryConstants.NOVATOR_UAT_FEED_CHECKED );
        } 
        catch (Throwable t) 
        {
            logger.error("Error in update()",t);
            throw new Exception("Could not retrieve data.");
        }
        
        return doc;
    }

    protected void addNovatorUpdateAllowedNode(Document doc, Element parent, String name) throws Exception
    {
        String updateAllowed = getConfigurationUtil().getFrpGlobalParm(OrderEntryConstants.NOVATOR_CONFIG,name);
        addNode(doc, parent, name, updateAllowed);            
    }
    
    protected Element addNode(Document doc, Element parent, String nodeName, String nodeValue)
    {
        Element node = doc.createElement(nodeName);
        node.setTextContent(nodeValue);
        parent.appendChild(node);
        return node;
    }
    
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (configUtil == null)
        {
            configUtil = ConfigurationUtil.getInstance();
        }
        return configUtil;
    }

}
