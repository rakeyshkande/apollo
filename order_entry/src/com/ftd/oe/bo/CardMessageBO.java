package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.CardMessageDAO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Business object for the JOE.CARD_MESSAGES table and JOE Maint Screens
 */
public class CardMessageBO {

    private static final Logger logger = 
        new Logger("com.ftd.oe.bo.CardMessageBO");
    IResourceProvider resourceProvider;
    CardMessageDAO cardMessageDAO;
    
    public CardMessageBO() {
    }

    /**
     * Get all the records from JOE.CARD_MESSAGES table
     * @return XML result set
     * @throws Exception
     */
    public Document getCardMessages() throws Exception {
        Connection qConn = null;
        Document doc = null;
        logger.debug("getCardMessages()");
        
        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            
            doc = cardMessageDAO.getCardMessages(qConn);
        } catch (Throwable t) {
            logger.error("Error in getCardMessages()",t);
            throw new Exception("Could not retrieve data.");
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }
        
        return doc;
    }

    /**
     * Within a single transaction, 
     * remove all records from the JOE.CARD_MESSAGES table and replace
     * with the new records that are in the passed document
     * @param doc containing the records to insert
     * @return Result Set document
     * @throws Exception
     */
    public Document saveCardMessages(Document doc) throws Exception {
        Connection qConn = null;
        logger.debug("saveCardMessages()");

        try {
            String dataSource = OrderEntryConstants.DATASOURCE;
            qConn = resourceProvider.getDatabaseConnection(dataSource);
            if (qConn == null) {
                throw new Exception("Unable to connect to database");
            }
            
            HashMap errorList = new HashMap();
            InitialContext context = null;
            UserTransaction userTransaction = null;
            
            try {
                int transactionTimeout = Integer.parseInt(ConfigurationUtil.getInstance().getProperty("oe_config.xml", "transaction_timeout_in_seconds"));
                String jndi_usertransaction_entry = ConfigurationUtil.getInstance().getProperty("oe_config.xml", "jndi_usertransaction_entry");
                
                context = new InitialContext();
                userTransaction = (UserTransaction) context.lookup(jndi_usertransaction_entry);
                
                userTransaction.setTransactionTimeout(transactionTimeout);
                userTransaction.begin();
                
                //Remove all the existing records
                try {
                    cardMessageDAO.removeAllCardMessages(qConn);
                } catch (Throwable t) {
                    logger.error("Error while trying to remove all card messages.");
                     errorList.put("card-messages", 
                                   "Error while trying to remove all card messages.+\r\n"+t.getMessage());
                    throw t;
                }
                
                
                //Insert the new ones
                try {
                    String userId = JAXPUtil.selectSingleNodeText(doc,"/card_messages/updated-by-id/text()","","");
                    logger.debug("UserId: "+userId);
                    //2.  loop through and save the new records
                    NodeList nodes = JAXPUtil.selectNodes(doc,"/card_messages/records/card_message");
                    for( int idx=0; idx<nodes.getLength(); idx++ ) {
                        Element el = (Element)nodes.item(idx);
                        long displaySeq = Long.parseLong(el.getAttribute("display_seq"));
                        String activeFlag = el.getAttribute("active_flag");
                        String message = URLDecoder.decode(el.getTextContent(), "UTF-8");
                        logger.debug("Sequence: "+String.valueOf(displaySeq)+"     active: "+activeFlag+"     message: "+message);

                        cardMessageDAO.insertCardMessage(qConn,userId,message,displaySeq,activeFlag);
                        
                    }
                } catch (Throwable t) {
                    logger.error("Error while tring to save a card message record.");
                    errorList.put("card-messages", 
                                  "Error while tring to save a card message record.+\r\n"+t.getMessage());
                    throw t;
                }

                doc = JAXPUtil.createDocument();
                Element root = 
                    doc.createElement(OrderEntryConstants.TAG_RESULT);
                doc.appendChild(root);
                JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
                JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");
                
                userTransaction.commit();
                logger.debug("commit finished");
    
            } catch (Throwable t) {
                logger.error(t.toString());
                try {
                    userTransaction.rollback();
                } catch (Throwable t2) {
                    logger.warn("Error while trying to rollback transaction in saveCardMessages()",t2);
                }
                
                throw t;
            }
            
        } catch (Throwable t) {
            logger.error("Error in saveCardMessages()", t);
            throw new Exception("Error while attempting to save the card message data.");
        } finally {
            if (qConn != null) {
                try {
                    if (!qConn.isClosed()) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection", sqle);
                }
            }
        }

        return doc;
    }

    public void setResourceProvider(IResourceProvider newresourceProvider) {
        this.resourceProvider = newresourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public void setCardMessageDAO(CardMessageDAO newcardMessageDAO) {
        this.cardMessageDAO = newcardMessageDAO;
    }

    public CardMessageDAO getCardMessageDAO() {
        return cardMessageDAO;
    }
}
