package com.ftd.oe.bo;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.dao.IOTWDAO;
import com.ftd.oe.vo.IOTWDeliveryDateVO;
import com.ftd.oe.vo.IOTWVO;
import com.ftd.oe.vo.NovatorUpdateVO;
import com.ftd.oe.vo.PriceHeaderVO;
import com.ftd.oe.vo.ProductCheckVO;
import com.ftd.oe.vo.SourceCodeVO;
import com.ftd.oe.web.OEActionBase;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.TransactionUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.vo.MessageToken;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.naming.InitialContext;

import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;


/**
 * Handles business logic associated with the Item Of The Week Maintenance screen.
 */
public class IOTWBO {

    private static Logger logger = new Logger("com.ftd.oe.bo.IOTWBO");
    private IOTWFeedBO iotwFeedBO;
    private IOTWDAO iotwDAO;
    private IResourceProvider resourceProvider;
    private TransactionUtil transactionUtil;

    public IOTWBO() {
    }

    /**
     * Obtains IOTW program data based on IOTW Id.  If the IOTW Id is null all
     * programs are returned.
     * @param iotwId IOTW program Id
     * @return List of IOTW programs 
     * @throws Exception
     */
    public List<IOTWVO> getIOTW(String iotwId, String productId, String sourceCode, String iotwSourceCode, boolean expiredOnly) throws Exception {

        Connection conn = null;

        // List of IOTW programs obtained
        List<IOTWVO> iotwList = null;

        try{
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
            
            // Obtain the requested IOTW programs
            iotwList = iotwDAO.getIOTW(conn, iotwId, productId, sourceCode, iotwSourceCode, expiredOnly);
            
            /*COMMENTED OUT WHEN THE STATUS COLUMN WAS REMOVED.
             * I'M LEAVING IT AROUND IN CASE THE BUSINESS CHANGES THEIR MIND
            // Determine if the IOTW program is active
            IOTWVO iotwVO = null;
            Date currentDate = sdf.parse(sdf.format(new Date()));
            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                iotwVO = it.next();
                
                if(logger.isDebugEnabled()){
                    logger.debug("current date:" + currentDate);
                    logger.debug("start date:" + iotwVO.getStartDate());
                    logger.debug("end date:" + iotwVO.getEndDate());
                    logger.debug("Start date comparison:" + iotwVO.getStartDate().compareTo(currentDate));
                    if(iotwVO.getEndDate() != null){
                        logger.debug("End date comparison:" + iotwVO.getEndDate().compareTo(currentDate));
                    }
                }

                // If the current date is within the program start and end date set the program status to active (true)
                if(iotwVO.getStartDate().compareTo(currentDate) < 1 && (iotwVO.getEndDate() == null || iotwVO.getEndDate().compareTo(currentDate) > -1)){
                    iotwVO.setStatus(true);                   
                }
            }
            *COMMENTED OUT WHEN THE STATUS COLUMN WAS REMOVED.
            * I'M LEAVING IT AROUND IN CASE THE BUSINESS CHANGES THEIR MIND
            */
        } catch (Exception e) {
            logger.error(e);
        } finally{
            transactionUtil.closeConnection(conn);
        }
        return iotwList;
    }    

    /**
     * Obtains IOTW information requested as an AJAX call and returned as XML.
     * If the IOTW Id is null all programs are returned.
     * @param iotwId IOTW program id
     * @return XML Document containing IOTW program information
     * @throws Exception
     */
    public Document getIOTWAjax(String iotwId) throws Exception {
        Connection conn = null;
        Document doc = null;
        try{
                conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
                
                // Obtain IOTW program data
                doc = iotwDAO.getIOTWAJAX(conn, iotwId);
        } finally{
            transactionUtil.closeConnection(conn);
        }
        return doc;
    }    

    /**
     * Validate and insert/save IOTW program data.
     * @param iotwList List of IOTW programs to validate and save
     * @param existingIOTWVO Existing IOTW program.  Only exists when editing an existing IOTW program
     * @param novatorUpdateVO Novator feed information
     * @return Validation document if a validation error occurred
     * @throws Exception
     */
    public Document insert(List<IOTWVO> iotwList, IOTWVO existingIOTWVO, NovatorUpdateVO novatorUpdateVO) throws Exception {
        Document doc = null;
        Connection conn = null;
        UserTransaction ut = null;
        boolean existingIOTWExists = false;
        IOTWVO iotwVO = null;
        String iotwId = null;
        HashMap errors = null;
        String myBuysFeedEnabled = "N";
     
        try {
            /* In the case of an edit one of the IOTW programs may match the
             * existing IOTW program.  If the IOTW program matches the existing
             * IOTW program set the the IOTW original values equal to the
             * existing ITOW values and set a flag to signify the edited IOTW
             * program exists within the list of IOTW programs to be inserted/saved.
             */ 
             
           GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
           if (gph != null)
            {
              myBuysFeedEnabled  = gph.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
            }
            
                  
            for(Iterator<IOTWVO> it = iotwList.iterator(); !existingIOTWExists && it.hasNext();){
                iotwVO = it.next();
                if(iotwVO.getProductId().equals(existingIOTWVO.getProductId()) 
                   && iotwVO.getSourceCode().equals(existingIOTWVO.getSourceCode())) {
                    iotwVO.setIotwId(existingIOTWVO.getIotwId());
                    iotwVO.setOriginalSourceCode(existingIOTWVO.getSourceCode());
                    iotwVO.setOriginalProductId(existingIOTWVO.getProductId());
                    iotwVO.setOriginalStartDate(existingIOTWVO.getStartDate());
                    iotwVO.setOriginalEndDate(existingIOTWVO.getEndDate());
                    existingIOTWExists = true;
                }
            }
            
            // Validate product id and source code data
            doc = validateData(iotwList);
            if(doc != null){
                return doc;           
            }

            // Validate that no overlapping programs already exist
            doc = validateProgram(iotwList);
            if(doc != null){
                return doc;           
            }
            
            // Obtain connection and create transaction
            ut = transactionUtil.createUserTransaction("oe_config.xml","transaction_timeout_in_seconds","jndi_usertransaction_entry");
            ut.begin();
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
            
            // Delete the existing program if it does not exist within the list of IOTW programs to insert/save
            if(existingIOTWVO != null && StringUtils.isNotBlank(existingIOTWVO.getIotwId()) && !existingIOTWExists){
                iotwDAO.delete(conn, existingIOTWVO);
            }

            // Insert/save each IOTW program
            IOTWDeliveryDateVO  iotwDeliveryDateVO = null;
            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                iotwVO = it.next();
                
                // Insert/save the IOTW program
                iotwId = iotwDAO.insert(conn, iotwVO);
                
                // If this was an existing program remove the delivery discount dates previous to saving new ones
                if(StringUtils.isNotBlank(iotwVO.getIotwId())){
                    iotwDAO.deleteDeliveryDiscountDates(conn, iotwVO);        
                }
                
                // Insert the delivery discount dates
                if(iotwVO.getDeliveryDiscountDates() != null){
                    for(Iterator<IOTWDeliveryDateVO> it2 = iotwVO.getDeliveryDiscountDates().iterator(); it2.hasNext();){
                        iotwDeliveryDateVO = it2.next();
                        iotwDeliveryDateVO.setIotwId(iotwId);
                        iotwDAO.insertDeliveryDiscountDate(conn, iotwDeliveryDateVO);
                    }
                }
            }
            
            // Send information to Novator as a feed
            if(novatorUpdateVO.isUpdateContent()
                || novatorUpdateVO.isUpdateLive()
                || novatorUpdateVO.isUpdateTest()
                || novatorUpdateVO.isUpdateUAT()){
                
                // Remove the existing program if it does not exist within the list of IOTW programs to insert/save
                if(existingIOTWVO != null  && StringUtils.isNotBlank(existingIOTWVO.getIotwId()) && !existingIOTWExists){
                    List<IOTWVO> tmpList = new ArrayList<IOTWVO>();
                    tmpList.add(existingIOTWVO);
                    errors = iotwFeedBO.send(conn, novatorUpdateVO, tmpList, IOTWFeedBO.REMOVE_FEED);
                    // If errors exist as a result of the feed create a validation document to display the errors
                    if (errors != null) {
                        transactionUtil.rollback(ut);
                        doc = createValidationDocument(errors);
                        return doc;
                    }
                }

                errors = iotwFeedBO.send(conn, novatorUpdateVO, iotwList, IOTWFeedBO.CREATE_FEED);
                // If errors exist as a result of the feed create a validation document to display the errors
                if (errors != null) {
                    transactionUtil.rollback(ut);
                    doc = createValidationDocument(errors);
                    return doc;
                }
                // send MY BUYS feed 
                if  (StringUtils.equalsIgnoreCase(myBuysFeedEnabled,"Y"))
                {
                  // Send myBuys feed for existing iotw which was deleted earlier 
                  if(existingIOTWVO != null && StringUtils.isNotBlank(existingIOTWVO.getIotwId()) && !existingIOTWExists){
                      List <IOTWVO> existingIOTWList = new ArrayList();
                      existingIOTWList.add(existingIOTWVO);
                      sendMyBuysFeed(existingIOTWList);    
                  }                  

                  sendMyBuysFeed(iotwList);    
                  
                }
            }

            // Commit transaction
            ut.commit();            
        } catch (Throwable t) {
            transactionUtil.rollback(ut);
            throw new Exception("Error inserting IOTW.  " + t.getMessage(), t);
        } finally {
            transactionUtil.closeConnection(conn);
        }

        return doc;
    }    

    
    /**
     * Delete IOTW programs.
     * @param iotwList List of IOTW programs to delete
     * @param novatorUpdateVO Novator feed information
     * @throws Exception
     */
    public Document delete(List<IOTWVO> iotwList, NovatorUpdateVO novatorUpdateVO) throws Exception {
        Connection conn = null;
        UserTransaction ut = null;
        boolean sendFeed = false;
        ArrayList<IOTWVO> iotwListData = new ArrayList<IOTWVO>();
        String myBuysFeedEnabled = "N";
        boolean sendMyBuysFeedFlag = false;
        
        try {
            // Obtain connection and start transaction
            ut = transactionUtil.createUserTransaction(OrderEntryConstants.JOE_CONFIG_FILE,"transaction_timeout_in_seconds","jndi_usertransaction_entry");
            ut.begin();
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
            GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
            
             if (gph != null)
             {
                myBuysFeedEnabled  = gph.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
             }
  
              if  (StringUtils.equalsIgnoreCase(myBuysFeedEnabled,"Y"))
              {
                sendMyBuysFeedFlag = true;
              }
            

            // Determine if the Novator feed should be sent
            if(novatorUpdateVO.isUpdateContent()
                || novatorUpdateVO.isUpdateLive()
                || novatorUpdateVO.isUpdateTest()
                || novatorUpdateVO.isUpdateUAT()){
                sendFeed = true;
            }

            // Delete IOTW programs
            IOTWVO iotwVO = null;
            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                iotwVO = it.next();

                /* The current IOTW list only contains IOTW Ids.  If a Novator
                 * feed is required obtain the rest of the program data.
                 */ 
                if(sendFeed || sendMyBuysFeedFlag){
                    iotwListData.add(iotwDAO.getIOTW(conn, iotwVO.getIotwId(), null, null, null, false).get(0));
                }
                
                // Delete the IOTW program
                iotwDAO.delete(conn, iotwVO);
            }

            // Send information to Novator as a feed
            if(sendFeed){
                HashMap errors = iotwFeedBO.send(conn, novatorUpdateVO, iotwListData, IOTWFeedBO.REMOVE_FEED);
                // If errors exist as a result of the feed create a validation document to display the errors
                if (errors != null) {
                    transactionUtil.rollback(ut);
                    Document doc = createValidationDocument(errors);
                    return doc;
                }
            }
            if(sendMyBuysFeedFlag)
            {
                sendMyBuysFeed(iotwListData);
            }

            // Commit transaction
            ut.commit();
            return null;
        } catch (Throwable t) {
            transactionUtil.rollback(ut);
            throw new Exception("Error removing IOTW.  " + t.getMessage(), t);
        } finally {
            transactionUtil.closeConnection(conn);
        }
    }    

    /**
     * Send all IOTW program data to Novator as a feed.
     * @param novatorUpdateVO Novator feed information
     * @throws Exception
     */
    public Document sendAll(NovatorUpdateVO novatorUpdateVO) throws Exception {
        Connection conn = null;
        IOTWVO iotwVO = null;
        logger.debug("sendAll()");
        
        try {
            // Determine if the Novator feed should be sent
            if(novatorUpdateVO.isUpdateContent()
                || novatorUpdateVO.isUpdateLive()
                || novatorUpdateVO.isUpdateTest()
               || novatorUpdateVO.isUpdateUAT()){
                conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
                
                // Obtain all IOTW programs
                List<IOTWVO> iotwList = iotwDAO.getIOTW(conn, null, null, null, null, false);
                
                // Novator expects the original values for an update feed.
                for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                    iotwVO = it.next();
                    iotwVO.setOriginalSourceCode(iotwVO.getSourceCode());
                    iotwVO.setOriginalProductId(iotwVO.getProductId());
                    iotwVO.setOriginalStartDate(iotwVO.getStartDate());
                    iotwVO.setOriginalEndDate(iotwVO.getEndDate());
                }
                
                // Send feed
                HashMap errors = iotwFeedBO.send(conn, novatorUpdateVO, iotwList, IOTWFeedBO.UPDATE_FEED);
                // If errors exist as a result of the feed create a validation document to display the errors
                if (errors != null) {
                    Document doc = createValidationDocument(errors);
                    return doc;
                }
            }
            return null;
        } finally {
            transactionUtil.closeConnection(conn);
        }
    }    

    /**
     * Validate IOTW program
     * @param iotwList List of IOTW programs to validate
     * @return Document Validation document
     * @throws Exception
     */
    private Document validateProgram(List<IOTWVO> iotwList) throws Exception
    {
        Connection conn = null;
        HashMap errors = new HashMap();
        Document doc = null;
        IOTWVO iotwVO = null;
        IOTWVO currentIOTWVO = null;
        List<IOTWVO> currentIOTWList = null;
        boolean overlap;
        SourceCodeVO iotwSourceCode = null;
        
        Date startDate, endDate, currentStartDate, currentEndDate;

        try
        {
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);
            
            // Obtain IOTW Source Code information
            iotwSourceCode = iotwDAO.getSourceCode(conn, iotwList.get(0).getIotwSourceCode());
            if(iotwSourceCode == null)
                throw new Exception("IOTW Source Code " + iotwList.get(0).getIotwSourceCode() + " does not exist.");

            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                iotwVO = it.next();
                startDate = iotwVO.getStartDate();
                endDate = iotwVO.getEndDate();
                
                /* If the product does not allow a discount and a discount
                 * exists create an error message
                */ 
                if(iotwVO.getProductCheckVO().isProduct() && !iotwDAO.getProductDiscountFlag(conn, iotwVO.getProductCheckVO().getProductId())) {
                    PriceHeaderVO phVO = null;
                    BigDecimal discountAmount;
                    BigDecimal zeroAmount = new BigDecimal(0);
                    for(Iterator<PriceHeaderVO> itPh = iotwSourceCode.getPriceHeaderList().iterator(); itPh.hasNext();){
                        phVO = itPh.next();
                        discountAmount = new BigDecimal(phVO.getDiscountAmt());
                        if(discountAmount.compareTo(zeroAmount) > 0){
                            errors.put("discount_not_allowed", "Product " + iotwVO.getProductCheckVO().getProductId() + " does not allow discounts and the item of the week source code contains a discount."); 
                        }
                    }
                }
                
                // Validate the IOTW program does not overlap with an existing IOTW program based on Master Source Code and Product Id
                currentIOTWList = iotwDAO.getIOTWByProductAndSourceCode(conn, iotwVO.getSourceCode(), iotwVO.getProductCheckVO().getProductId(), iotwVO.getProductCheckVO().getUpsellMasterId());
                for(Iterator<IOTWVO> itCurrent = currentIOTWList.iterator(); itCurrent.hasNext();){
                    currentIOTWVO = itCurrent.next();

                    // In the case of an edit the existing program will have an IOTW Id.  Do not validate against the existing program.
                    if(StringUtils.isBlank(iotwVO.getIotwId()) || !currentIOTWVO.getIotwId().equals(iotwVO.getIotwId())){
                        currentStartDate = currentIOTWVO.getStartDate();
                        currentEndDate = currentIOTWVO.getEndDate();
                        overlap = false;

                        // If no current end date exists
                        if(currentEndDate == null){
                            // If end date does not exist
                            if(endDate == null){
                                overlap = true;
                            }
                            // If end date is equal to or larger than the current start date
                            else if (endDate.compareTo(currentStartDate) > -1){
                                overlap = true; 
                            }
                        }
                        // If end date is null and start date is previous to or equal to the current end date
                        else if(endDate == null && startDate.compareTo(currentEndDate) < 1){
                            overlap = true;
                        }
                        // If start date is equal to or previous to the current end date and end date is equal to or after the current start date 
                        else if (startDate.compareTo(currentEndDate) < 1 && endDate.compareTo(currentStartDate) > -1){
                            overlap = true;
                        }
                        
                        // If there is an overlapping program create an error message
                        if(overlap){
                            errors.put("overlapping_program_dates", "The start and end date overlap with an existing program for source code " + iotwVO.getSourceCode() + " and product id " + iotwVO.getProductCheckVO().getNormalizedId() + "."); 
                        }
                    }
                }
            }
            
            // If errors exist create a validation document
            if (errors.size() > 0) {
                doc = createValidationDocument(errors);
            }
        }
        catch (Throwable t) {
            logger.error("Error in validateProgram()", t);
            throw new Exception("Validation failed.  " + t.getMessage(), t);
        } finally {
            transactionUtil.closeConnection(conn);    
        }

        return doc;
    }

    /**
     * Validate the IOTW source code and product ids
     * @param iotwList List of IOTW programs
     * @return Validation Document
     * @throws Exception
     */
    private Document validateData(List<IOTWVO> iotwList) throws Exception
    {
        Connection conn = null;
        HashMap errors = new HashMap();
        Document doc = null;
        IOTWVO iotwVO = null;
        HashMap sourceCodes = new HashMap();
        HashMap products = new HashMap();
        ProductCheckVO productCheckVO = null;
        

        try
        {
            conn = transactionUtil.getConnection(resourceProvider,OrderEntryConstants.DATASOURCE);

            iotwVO = iotwList.get(0);

            /* Product Id is required.
             * Technically there can be multiple product ids but if the first
             * object contains a product id the rest of the IOTWVO objects
             * will as well unless a bug exists.
             */ 
            if (StringUtils.isBlank(iotwVO.getProductId())) {
                errors.put("product_id", "Product Id is required."); 
            } 

            /* Source Code is required.
             * Technically there can be multiple source codes but if the first
             * object contains a product id the rest of the IOTWVO objects
             * will as well unless a bug exists.
             */ 
            if (StringUtils.isBlank(iotwVO.getSourceCode())) {
                errors.put("source_code", "Source Code is required."); 
            } 

            // Start date is required
            if (iotwVO.getStartDate() == null) {
                errors.put("start_date", "Program Start Date is required."); 
            } 
            // Start date cannot be greater than end date
            else if(iotwVO.getEndDate() != null && iotwVO.getStartDate().compareTo(iotwVO.getEndDate()) > 0){
                errors.put("start_date_vs_end_date", "The start date is greater than the end date.");
            }
            
            // Product Page Message cannot be longer than 255 characters
            if (StringUtils.isNotBlank(iotwVO.getProductPageMessage()) && iotwVO.getProductPageMessage().length() > 255) {
                errors.put("product_page_message", "Product Page Message cannot be longer than 255 characters."); 
            } 

            // Calendar Message cannot be longer than 255 characters
                if (StringUtils.isNotBlank(iotwVO.getCalendarMessage()) && iotwVO.getCalendarMessage().length() > 255) {
                errors.put("calendar_message", "Calendar Message cannot be longer than 255 characters."); 
            } 

            // Item of the week source code is required
            if (StringUtils.isBlank(iotwVO.getIotwSourceCode())) {
                errors.put("iotw_source_code", "IOTW Source Code is required."); 
            // Item of the week source code must be a valid source code and must be designated as an IOTW Source Code
            } else {
                SourceCodeVO sourceCodeVO = iotwDAO.getSourceCode(conn, iotwVO.getIotwSourceCode());
                if(sourceCodeVO == null)
                    errors.put("iotw_source_code", "IOTW Source Code entered does not exist."); 
                else if(!sourceCodeVO.isIotwFlag())
                    errors.put("iotw_source_code", "IOTW Source Code entered is not an IOTW Source Code."); 
            }
            
            // Validate the source code and product id for each IOTW program
            for(Iterator<IOTWVO> it = iotwList.iterator(); it.hasNext();){
                iotwVO = it.next();
                
                // Validate product id and convert Novator or Upsell Master Product Ids
                if(StringUtils.isNotBlank(iotwVO.getProductId())){
                    // If the product id was already found and converted
                    if(products.containsKey(iotwVO.getProductId())) {
                        productCheckVO = (ProductCheckVO)products.get(iotwVO.getProductId());
                        iotwVO.setProductCheckVO(productCheckVO);
                        iotwVO.setProductId(productCheckVO.getNormalizedId());
                    } else {
                        productCheckVO = iotwDAO.getProductId(conn, iotwVO.getProductId());
                        if(productCheckVO.isProduct() || productCheckVO.isUpsell()){
                            iotwVO.setProductCheckVO(productCheckVO);
                            iotwVO.setProductId(productCheckVO.getNormalizedId());
                            
                            // Store the converted product id
                            products.put(iotwVO.getProductId(), productCheckVO);
                        }else{
                            errors.put("product_id_" + iotwVO.getProductId(), "Product Id " + iotwVO.getProductId() + " does not exist."); 
                        }
                    }
                }

                // Validate source code
                if(StringUtils.isNotBlank(iotwVO.getSourceCode())){
                    if(!sourceCodes.containsKey(iotwVO.getSourceCode())){
                        if(iotwDAO.getSourceCode(conn, iotwVO.getSourceCode()) == null){
                            errors.put("source_code_" + iotwVO.getSourceCode(), "Source Code " + iotwVO.getSourceCode() + " does not exist."); 
                        }
                        
                        sourceCodes.put(iotwVO.getSourceCode(), "");
                    }
                }
            }

            // If errors exist create a validation document
            if (errors.size() > 0) {
                doc = createValidationDocument(errors);
            }
        }
        catch (Throwable t) {
            logger.error("Error in validateData()", t);
            throw new Exception("Validation failed.  " + t.getMessage(), t);
        } finally {
            transactionUtil.closeConnection(conn);
        }

        return doc;
    }

    
    /**
     * Creates a validation document
     * @param errors Validation errors
     * @return Validation Document
     */
    protected Document createValidationDocument(HashMap errors) throws Exception
    {
        Document doc = OEActionBase.createValidResponseDocument();
        TreeMap sortedErrors = new TreeMap(errors);
        OEActionBase.createValidationResultSet(doc, "N", sortedErrors);
        
        return doc;
    }

    public void sendMyBuysFeed(List<IOTWVO> iotwList) 
    {
      logger.debug("sending MyBuys feed for Item of the week ");
      HashMap<String, java.lang.Throwable> errorsforMyBuys = iotwFeedBO.sendMyBuys(iotwList);
  
      // If errors exist as a result of the feed create a validation document to display the errors
      if (errorsforMyBuys != null && errorsforMyBuys.size() > 0)
      {
  
        logger.info("Number of errors in list from calling sendMyBuys is:" + errorsforMyBuys.size());
        Set<Map.Entry<String, java.lang.Throwable>> set = errorsforMyBuys.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext())
        {
          Map.Entry entry = (Map.Entry) it.next();
          logger.error("Error in sending MyBuys message() for product id: " + entry.getKey(), 
                       (java.lang.Throwable) entry.getValue());
        }
      }
    }
    
    //This method is added for the new resource created for IOTW View Mode.
    public static boolean assertPermission(String sessionId,
            String resource, String permission)
            throws Exception {
            try {
                return com.ftd.security.SecurityManager.getInstance().assertPermission("Order Proc",
                    sessionId, resource, permission);
            } catch (Exception e) {
                logger.error(e);
                throw new Exception("assertPermission: Failed.", e);
            }
        }

    public void setIotwDAO(IOTWDAO newiotwDAO) {
        this.iotwDAO = newiotwDAO;
    }

    public IOTWDAO getIotwDAO() {
        return iotwDAO;
    }

    public void setResourceProvider(IResourceProvider newresourceProvider) {
        this.resourceProvider = newresourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public void setIotwFeedBO(IOTWFeedBO newiotwFeedBO) {
        this.iotwFeedBO = newiotwFeedBO;
    }

    public IOTWFeedBO getIotwFeedBO() {
        return iotwFeedBO;
    }

    public void setTransactionUtil(TransactionUtil transactionUtil)
    {
        this.transactionUtil = transactionUtil;
    }

    public TransactionUtil getTransactionUtil()
    {
        return transactionUtil;
    }
}
