package com.ftd.oe.interfaces;

import org.w3c.dom.Document;
import com.ftd.oe.vo.fresh.FreshProdInfoVO;

public interface ValidationBO {

    public Document validateDnis(String dnisId, String secToken) throws Exception;
    
    public Document validateSourceCode(String sourceCode, String dnisId, String secToken) throws Exception;
    
    public Document validateZipCode(String zipCode) throws Exception;
    
    public Document validateStateByZipcodeAndCity(String zipCode, String city) throws Exception;
    
    public Document validateProduct(String productId, String sourceCode, String countryId) throws Exception;

    public Document validateProductGroupon(String productId, String sourceCode, String countryId) throws Exception;
    public FreshProdInfoVO validateProductFresh(String productId, String sourceCode, String countryId) throws Exception;
    public void triggerProjectFreshUpdates(String dateStr) throws Exception;


    public Document validateProductAvailability(String productId, String zipCode, String deliveryDate,
        String countryId, String sourceCode, String dateRangeEnd, Document orderDoc,
        boolean getChargesXML, String addons) throws Exception;
    
    public Document validateFlorist(String floristId, String productId, String zipCode, String deliveryDate) throws Exception;
    
    public Document validateAVSAddress(String firmname, String address, String city,
        String state, String zipCode, String countryId, String orderType) throws Exception;
    
    public Document validateGiftCertificate(String certificateId) throws Exception;
    
}
