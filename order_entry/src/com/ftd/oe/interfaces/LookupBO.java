package com.ftd.oe.interfaces;

import org.w3c.dom.Document;

public interface LookupBO {

    public String getIntroData() throws Exception;
    
    public String getElementData() throws Exception;
    
    public Document getElementData(String appCode) throws Exception;

    public Document getCustomerByPhoneNumber(String phoneNumber, String companyId, String sourceCode) throws Exception;
    
    public Document getZipsByCityState(String city, String state) throws Exception;
    
    public Document getPopularProducts(String zipCode, String deliveryDate, String countryId,
        String sourceCode, String dateRangeEnd) throws Exception;
    
    public Document getCrossSellProducts(String productId, String sourceCode, String zipCode,
        String deliveryDate, String countryId, String dateRangeEnd) throws Exception;
    
    public Document sourceCodeSearch(String searchType, String keywords, String iotwSourceCode,
        String sourceCode, String productId, String priceHeaderId, String companyId) throws Exception;
    
    public Document productSearch(String keywords, String zipCode, String deliveryDate,
        String countryId, String sourceCode, String dateRangeEnd) throws Exception;

    public Document floristSearch(String productId, String zipCode, String deliveryDate,
        String dateRangeEnd, String sourceCode) throws Exception;
    
    public Document institutionSearch(String city, String state, String addressType, String keywords) throws Exception;
    
    public Document getDeliveryDates(String productId, String zipCode, String countryId, String addons,String source_code) throws Exception;
    
    public Document getUpdateOrderInfo(String externalOrderNumber) throws Exception;
    
    public Document getMembershipIdInfo(String zipCode) throws Exception;
    
    public String getSympathyLocationCheck(String sourceCode, String companyId,
			String state, String deliveryDate,String deliveryTime,String addressType, String deliveryMethod,boolean leadTimeCheckFlag) throws Exception;
    
    public String getPromotionInfo(String sourcecode) throws Exception;
}
