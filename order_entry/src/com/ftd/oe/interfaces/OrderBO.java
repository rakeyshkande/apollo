package com.ftd.oe.interfaces;

import org.w3c.dom.Document;

public interface OrderBO {

    public Document submitCart(Document orderDoc, String csrId) throws Exception;

    public Document calculateOrderTotals(Document orderDoc, boolean frontEndFlag) throws Exception;
 
    public Document recordCallTime(String startTime, String csrId, String dnisId,
        String externalOrderNumber) throws Exception;

}
