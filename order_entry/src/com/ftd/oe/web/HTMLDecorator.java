package com.ftd.oe.web;

import com.ftd.osp.utilities.plugins.Logger;

import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.properties.MediaTypeEnum;


/**
 * Encodes columns containing HTML so the HTML is not rendered.  Be aware
 * the encoding is limited to "<" and ">".
 */
public class HTMLDecorator implements DisplaytagColumnDecorator {
    private static final Logger logger = new Logger("com.ftd.oe.web.HTMLDecorator");

    public java.lang.Object decorate(java.lang.Object columnValue, javax.servlet.jsp.PageContext pageContext, MediaTypeEnum media){
        if(columnValue != null){
            String tmp = (String)columnValue;
            tmp = tmp.replaceAll("<", "&lt;");
            tmp = tmp.replaceAll(">", "&gt;");
            return tmp;
        }
        return columnValue;
    }
}
