package com.ftd.oe.web;

import com.ftd.oe.bo.CardMessageBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;


/**
 * Struts action class responsible for all AJAX requests to and from 
 * the "Card Message Phrase Maintenance" screen.
 */
public class CardMessageAjaxAction extends OEActionBase {

    private static final Logger logger = 
        new Logger("com.ftd.oe.web.CardMessageAjaxAction");
    private CardMessageBO cardMessageBO;

    /**
     * Processes AJAX requests for gathering and saving data
     * @param mapping struts action mapping object
     * @param form struts action form object
     * @param request request object
     * @param response response object
     * @return null
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        HashMap paramMap = null;
        try {
            String requestXML = 
                request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) {
                String errorMessage = 
                    OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, 
                               createErrorResponseDocument(errorMessage), 
                               null);
                return null;
            }
            Document requestDoc = JAXPUtil.parseDocument(requestXML);

            paramMap = parseRequestDoc(requestDoc);
            String requestType = 
                (String)paramMap.get(OrderEntryConstants.REQUEST_TYPE);
            logger.debug("requestType: " + requestType);

            Document responseDoc = null;
            if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_CARD_MESSAGES)) {
                responseDoc = cardMessageBO.getCardMessages();
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_SAVE_CARD_MESSAGES)) {
                String strXml = 
                    (String)paramMap.get(OrderEntryConstants.PARAMETER_CARD_MESSAGES_XML);
                Document doc = JAXPUtil.parseDocument(strXml);
                responseDoc = cardMessageBO.saveCardMessages(doc);
            } else {
                responseDoc = 
                        createErrorResponseDocument("Invalid request type: " + 
                                                    requestType);
            }

            createResponse(response, responseDoc, paramMap);

        } catch (Throwable t) {
            logger.error(t);
            createResponse(response, 
                           createErrorResponseDocument(t.getMessage()), 
                           paramMap);
        }

        return null;
    }

    public void setCardMessageBO(CardMessageBO newcardMessageBO) {
        this.cardMessageBO = newcardMessageBO;
    }

    public CardMessageBO getCardMessageBO() {
        return cardMessageBO;
    }
}
