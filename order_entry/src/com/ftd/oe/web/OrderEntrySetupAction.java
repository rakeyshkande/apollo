package com.ftd.oe.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.SecurityManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class OrderEntrySetupAction extends Action {

    private static final Logger logger = new Logger("com.ftd.oe.web.OrderEntrySetupAction");

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        String sessionId = request.getParameter("securitytoken");
        String userId = null;
        String firstName = null;
        String lastName = null;

        try {
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            userId = userInfo.getUserID();
            firstName = userInfo.getFirstName();
            lastName = userInfo.getLastName();
        } catch (Exception e) {
            logger.error("getUserInfo: User session ID verification failed: " +
                sessionId, e);
        }
        
        String secureURL = 
            ConfigurationUtil.getInstance().getFrpGlobalParm("OE_CONFIG", "SECURE_URL_PREFIX");

        String comSearchURL = 
            ConfigurationUtil.getInstance().getFrpGlobalParm("OE_CONFIG","COM_SEARCH_URL");
        
        request.setAttribute("identity",userId);
        request.setAttribute("first-name",firstName);
        request.setAttribute("last-name",lastName);
        request.setAttribute("secure-url",secureURL);
        request.setAttribute("com-return-url", comSearchURL);
        
        return mapping.findForward("success");
    }
}
