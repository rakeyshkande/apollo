package com.ftd.oe.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ftd.oe.bo.IOTWBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.vo.IOTWVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


/**
 * Obtains information required for the initial invocation of the Item
 * Of The Week Maintenance screen
 */
public class IOTWSetupAction extends OEActionBase {
    private static final Logger logger = new Logger("com.ftd.oe.web.IOTWSetupAction");
    private IOTWBO iotwBO;

    /**
     * Obtains and sets user, secure url, and all IOTW program data.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {
       
        // Obtain session information
        String sessionId = request.getParameter("securitytoken");
        String userId = null;

        // Obtain user information
        try {
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            userId = userInfo.getUserID();
        } catch (Exception e) {
            logger.error("getUserInfo: User session ID verification failed: " + sessionId, e);
        }

        try {
            
            // Set user and secure url information
            request.setAttribute("identity", userId);
            //request.setAttribute("secure-url", secureURL);

            // Obtain all IOTW program data
            String productId = request.getParameter("productId");
            if (productId == null) {
                productId = "";
            } else {
                productId = productId.toUpperCase();
            }
            String sourceCode = request.getParameter("sourceCode");
            if (sourceCode == null) {
                sourceCode = "";
            } else {
                sourceCode = sourceCode.toUpperCase();
            }
            String iotwSourceCode = request.getParameter("IOTWSourceCode");
            if (iotwSourceCode == null) {
                iotwSourceCode = "";
            } else {
                iotwSourceCode = iotwSourceCode.toUpperCase();
            }
            boolean expiredOnly = Boolean.parseBoolean(request.getParameter("expiredOnly"));
            logger.debug("productId: " + productId + " sourceCode: " + sourceCode + " iotwSourceCode: " + iotwSourceCode + " expiredOnly: " + expiredOnly);
            List<IOTWVO> iotwList = null;
            request.setAttribute("isBlankSearch", "Y");
            if(!StringUtils.isEmpty(productId) || !StringUtils.isEmpty(sourceCode) || !StringUtils.isEmpty(iotwSourceCode)){
            	iotwList = iotwBO.getIOTW(null, productId, sourceCode, iotwSourceCode, expiredOnly);
            	request.setAttribute("isBlankSearch", "N");
            }
            request.setAttribute("iotwList", iotwList);

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String pagesize = cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT, OrderEntryConstants.IOTW_PAGESIZE);
            if (pagesize == null || pagesize.equals("")) pagesize = "10";
            request.setAttribute("iotwPagesize", pagesize);

            Boolean IOTWReadOnly = IOTWBO.assertPermission(sessionId, OrderEntryConstants.ITEM_OF_THE_WEEK_READ_ONLY, OrderEntryConstants.VIEW);
            logger.debug("IOTWReadOnly: " + IOTWReadOnly);
            if (IOTWReadOnly) {
                request.setAttribute("searchIOTWReadOnly", "Y");
            } else {
                request.setAttribute("searchIOTWReadOnly", "N");
            }
            request.setAttribute("searchSourceCode", sourceCode);
            request.setAttribute("searchProductId", productId);
            request.setAttribute("searchIOTWSourceCode", iotwSourceCode);
            if (expiredOnly) {
                request.setAttribute("searchExpiredOnly", "Y");
            } else {
                request.setAttribute("searchExpiredOnly", "N");
            }
            
            Boolean APEenabledIOTWUpdate = IOTWBO.assertPermission(sessionId, OrderEntryConstants.APE_IOTW_Maintenance, OrderEntryConstants.UPDATE);
            logger.debug("APEenabledIOTWEditable: " + APEenabledIOTWUpdate);
            if (APEenabledIOTWUpdate) {
                request.setAttribute("APEenabledIOTWEditable", "Y");
            } else {
                request.setAttribute("APEenabledIOTWEditable", "N");
            }
        
        } catch (Exception e) {
            logger.error(e);
        }
        
        return mapping.findForward("success");
    }

    public void setIotwBO(IOTWBO newiotwBO) {
        this.iotwBO = newiotwBO;
    }

    public IOTWBO getIotwBO() {
        return iotwBO;
    }
}
