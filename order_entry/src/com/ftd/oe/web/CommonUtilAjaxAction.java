package com.ftd.oe.web;

import com.ftd.oe.bo.CommonUtilBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

public class CommonUtilAjaxAction extends OEActionBase 
{
    private static final Logger logger = new Logger("com.ftd.oe.web.CommonUtilAction");
    private CommonUtilBO commonUtilBO;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception 
    {
        HashMap paramMap = null;
        try 
        {
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) 
            {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocument(errorMessage), null);
                return null;
            }
            
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            
            if(logger.isDebugEnabled())
                logger.debug(JAXPUtil.toString(requestDoc));
            
            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);

            if(logger.isDebugEnabled())
                logger.debug("requestType: " + requestType);
            
            Document responseDoc = null;
            
            if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_NOVATOR_UPDATE_SETUP)) 
            {
                responseDoc = commonUtilBO.getNovatorUpdateSetup();
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            
        }
        catch(Throwable t)
        {
            logger.error(t);
            //System.out.println(t);
        }
        return null;

    }
    
    public void setCommonUtilBO(CommonUtilBO param)
    {
        this.commonUtilBO = param;
    }

    public CommonUtilBO getCommonUtilBO()
    {
        return commonUtilBO;
    }


}
