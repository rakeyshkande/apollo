package com.ftd.oe.web;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Struts action class responsible for security
 */
public class SecurityAction extends Action {

    private static final Logger logger = new Logger("com.ftd.oe.web.SecurityAction");

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {

        logger.debug("execute()");
        
        response.setContentType("text/xml");
        try {
            Document doc = JAXPUtil.createDocument();
            Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
            doc.appendChild(root);
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
            JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");

            String responseXML = JAXPUtil.toString(doc);
            logger.debug(responseXML);
            response.getWriter().write(responseXML);
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }
}
