package com.ftd.oe.web;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.CachedSpellCheckUtil;
import com.ftd.oe.common.resources.SpellCheckUtilBase;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.IOException;
import java.net.URLDecoder;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SpellCheckAjaxAction extends OEActionBase {
    private SpellCheckUtilBase spellCheckUtil;
    private static final Logger logger = new Logger("com.ftd.oe.web.SpellCheckServlet");

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        HashMap paramMap = null;
        try {
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocument(errorMessage), null);
                return null;
            }
            
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            
            logger.debug(JAXPUtil.toString(requestDoc));
            //System.out.println(JAXPUtil.toString(requestDoc));
            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);
            logger.debug("requestType: " + requestType);
            
            Document responseDoc = null;
            if( StringUtils.equals(requestType,"AJAX_REQUEST_SPELL_CHECK") ) {
                String str = (String) paramMap.get("TEXT");
                String corrections = processCheckSpell(str);
                
                responseDoc = JAXPUtil.createDocument();
                Element rootEl = responseDoc.createElement("result");
                rootEl.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
                rootEl.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
                responseDoc.appendChild(rootEl);
                
                Element rowset = responseDoc.createElement(OrderEntryConstants.TAG_RS);
                rowset.setAttribute(OrderEntryConstants.TAG_NAME, "spellcheck");
                rowset.setAttribute(OrderEntryConstants.TAG_STATUS, StringUtils.equals(corrections,"[]")?"Y":"N");
                rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, StringUtils.equals(corrections,"[]")?"":corrections);
                rootEl.appendChild(rowset);
            } else {
                responseDoc = createErrorResponseDocument("Invalid request type: " + requestType);
            }

            createResponse(response, responseDoc, paramMap);

        } catch (Throwable t) {
            logger.error(t);
            createResponse(response, createErrorResponseDocument(t.getMessage()), paramMap);
        }

        return null;
    }

/**
  * check the word to see if it is valid. If not, return suggestions
  * The returned suggestion format must comply with the javaScript rules.
  * @param word String to be checked
  * @param end the absolute position in the original string
  * @return String suggestions
  */
    private String getSuggestions( String word, int end ) throws IOException
    {
        if ( word == null || word.length() == 0 )
            return null;
        if ( spellCheckUtil.spellCheck( word ) )
            return null;
        List suggest = spellCheckUtil.suggest( word );
        int start = end - word.length();
        StringBuffer result = new StringBuffer();
        result.append( "{start:" );
        result.append( start );
        result.append( ",end:" );
        result.append( end );
        result.append( ",suggestions:[" );
        if ( suggest != null )
        {
            for ( int i=0; i<suggest.size(); i++ )
            {
                result.append( "\"" + (String)suggest.get(i) + "\"");
                if ( i < suggest.size() -1 )
                    result.append( "," );
            }
        }
        result.append( "]}" );
        return result.toString();
    }

//    private void processCheckSpell( /*HttpServletRequest request, HttpServletResponse response*/)
    private String processCheckSpell( String original ) throws IOException
    {
        //String original = request.getParameter( "content" );

        if ( original == null )
            return "";

        // decode string coming in assuming it has been encoded client side to handle special characters
        original = URLDecoder.decode(original, "UTF-8");
        
        original = parseValue( original );

        String temp = original + " " ;

        ArrayList suggestions = new ArrayList();

        StringBuffer sb = new StringBuffer();
        String suggest = null;
        for (int location=0; location<temp.length(); location++)
        {
            char c = temp.charAt( location );
            if ( charOmitted( c ) ) {
                if ( !spellCheckUtil.spellCheck( sb.toString() ) ) {
                    suggest = getSuggestions( sb.toString(), location );

                    if ( suggest != null ) {
                        suggestions.add( suggest );
					}
                }
                // reset
                sb = new StringBuffer();

            }
            else {
                sb.append( c );
            }
        }

        String corrections = "[";
        for (int i=0; i<suggestions.size(); i++) {
            corrections += (String)suggestions.get(i);
            if ( i < suggestions.size() - 1 )
                corrections += ",";
        }
        corrections += "]";

        StringBuffer sbOriginal = new StringBuffer();

        for (int i=0; i<original.length(); i++)
        {
            char c = original.charAt( i );
            if ( c == '"' )
                sbOriginal.append( "\\" );
            sbOriginal.append( c );
        }

//        request.setAttribute( "original", sbOriginal.toString() );
//        request.setAttribute( "text", sbOriginal.toString() );
//        request.setAttribute( "corrections", corrections );
//        request.setAttribute( "callerName", request.getParameter("callerName"));
//        request.getRequestDispatcher("/response.jsp").forward( request, response );
        
        return corrections;
    }

/**
     * Omit the char in the string to be spellCheckUtil checked
     * All the character that do not want to be checked should go here
     * @param c the char to be checked.
     * @return true if the passed character should be omitted.
     */
    private static boolean charOmitted ( char c )
    {
        return ( Character.isWhitespace( c ) || (c == ' ') || (c == '"') || ( c =='.') || ( c == ':' )
                 || ( c == ';' ) || ( c == '$') || ( c == '!' ) || ( c == '?' ) || ( c == ',') );
    }
/**
  * Change end of line character to html <br> tag.
  * @param value value to be changed
  * @return the  new formatted String
  */
    private String parseValue( String value )
    {
        StringBuffer sb = new StringBuffer();
		logger.debug("parseValue(): value :: " + value);
        for (int i=0; i<value.length(); i++)
        {
            char c = value.charAt( i );
            if ( c == '\r' )
                sb.append( "" );
            else if ( c == '\n' )
                sb.append( " <br> " );
            else
                sb.append( c );
        }
        return sb.toString();
    }

    public void setSpellCheckUtil(SpellCheckUtilBase param) {
        this.spellCheckUtil = param;
    }

    public SpellCheckUtilBase getSpellCheckUtil() {
        return spellCheckUtil;
    }
}
