package com.ftd.oe.web;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ComSearchAction extends Action {

    private static Logger logger = 
        new Logger("com.ftd.oe.web.ComSearchAction");

    /******************************************************************************
     * execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
     *******************************************************************************
     *
     * This is the main action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, 
                                                                      ServletException, 
                                                                      Exception {

        logger.debug("ComSearchAction.execute");
        String mainMenuUrl = "";
        HashMap requestParms = new HashMap();

        //Get info passed in the request object
        requestParms = getRequestInfo(request);

        String comURL = ConfigurationUtil.getInstance().getFrpGlobalParm("OE_CONFIG","COM_SEARCH_URL");
        
        if ( comURL != null) {
            //append adminAction
            comURL += "?adminAction=";
            comURL += (String)requestParms.get("adminAction");

            //append security to the main menu url
            comURL += "&context=";
            comURL += (String)requestParms.get("context");
            comURL += "&securitytoken=";
            comURL += (String)requestParms.get("securityToken");
            comURL += "&action=";
            comURL += (String)requestParms.get("action");
            comURL += "&recipient_flag=";
            comURL += (String)requestParms.get("recipient_flag");
            comURL += "&in_order_number=";
            comURL += (String)requestParms.get("in_order_number");

            response.sendRedirect(comURL);
        }

        return null;


    }

    /**
     * getRequestInfo(HttpServletRequest request)
     * 
     * Retrieve the info from the request object, and set class level variables
     * @param request
     * @return none 
     */
    private HashMap getRequestInfo(HttpServletRequest request) {
        HashMap requestParms = new HashMap();

        /**********************retreive the security info**********************/
        //retrieve the context
        String strValue = request.getParameter(OrderEntryConstants.ADMIN_ACTION);
        if (strValue != null)
            requestParms.put("adminAction", strValue);

        //retrieve the context
        strValue = request.getParameter(OrderEntryConstants.CONTEXT);
        if (strValue != null)        
        requestParms.put("context", strValue);

        //retrieve the security token
        strValue = request.getParameter(OrderEntryConstants.SEC_TOKEN);
        if (strValue != null)        
            requestParms.put("securityToken", strValue);

        //retrieve the search action
        strValue = request.getParameter(OrderEntryConstants.SEARCH_ACTION);
        if (strValue != null)        
            requestParms.put("action", strValue);

        //retrieve the search action
        strValue = request.getParameter(OrderEntryConstants.RECIPIENT_FLAG);
        if (strValue != null)        
            requestParms.put("recipient_flag", strValue);

        //retrieve the search action
        strValue = request.getParameter(OrderEntryConstants.ORDER_NUMBER);
        if (strValue != null)        
            requestParms.put("in_order_number", strValue);


        return requestParms;
    }
}
