package com.ftd.oe.web;

import com.ftd.oe.bo.CrossSellBO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class CrossSellSetupAction extends OEActionBase 
{
    private static final Logger logger = new Logger("com.ftd.oe.web.CrossSellSetupAction");
    private CrossSellBO crossSellBO;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception 
    {
        logger.debug("Called");
        String sessionId = request.getParameter("securitytoken");
        String userId = null;

        try {
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            userId = userInfo.getUserID();
            
        } catch (Exception e) {
            logger.error("getUserInfo: User session ID verification failed: " + sessionId, e);
        }

        // Check for the company info to do a search
        String companyId = request.getParameter("xs_select");
        
        if (companyId != null)
        {
            List vos = crossSellBO.getCrossSellByCompany(companyId);
            request.setAttribute("crossSells", vos);
        }

        String secureURL = ConfigurationUtil.getInstance().getFrpGlobalParm("OE_CONFIG", "SECURE_URL_PREFIX");

        request.setAttribute("identity", userId);
        request.setAttribute("secure-url", secureURL);
        
        return mapping.findForward("success");
    }

    public void setCrossSellBO(CrossSellBO param)
    {
        this.crossSellBO = param;
    }

    public CrossSellBO getCrossSellBO()
    {
        return crossSellBO;
    }

}
