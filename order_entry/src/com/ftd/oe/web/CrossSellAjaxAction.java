package com.ftd.oe.web;

import com.ftd.oe.bo.CrossSellBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.vo.CrossSellDetailVO;
import com.ftd.oe.vo.CrossSellVO;
import com.ftd.oe.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

public class CrossSellAjaxAction extends OEActionBase 
{
    private static final Logger logger = new Logger("com.ftd.oe.web.CrossSellAjaxAction");
    private CrossSellBO crossSellBO;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception 
    {
        HashMap paramMap = null;
        try 
        {
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) 
            {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocument(errorMessage), null);
                return null;
            }
            
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            
            if(logger.isDebugEnabled())
                logger.debug(JAXPUtil.toString(requestDoc));
            
            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);

            if(logger.isDebugEnabled())
                logger.debug("requestType: " + requestType);
            
            Document responseDoc = null;
            
            if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_XSELL_GET_COMPANIES)) 
            {
                responseDoc = crossSellBO.getCompaniesAjax();
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_XSELL_GET_MASTER_RECORDS)) 
            {
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                productId = productId.toUpperCase();
                responseDoc = crossSellBO.getMasterRecordsAjax(productId);
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_XSELL_UPDATE)) 
            {
                CrossSellVO vo = buildCrossSellVO(paramMap);                
                NovatorUpdateVO updateVO = buildNovatorUpdateVO(paramMap);

                responseDoc = crossSellBO.validate(vo);
                logger.debug(vo.getProductId() + " " + vo.getUpsellMasterId());
                if (responseDoc == null)
                {
                    responseDoc = crossSellBO.update(vo, updateVO);
                    createValidationResultSet(responseDoc,"Y",new HashMap());
                }
                createResponse(response, responseDoc, paramMap);
            }
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_XSELL_REMOVE_XSELLS)) 
            {
                // The product to remove
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                productId = productId.toUpperCase();
                NovatorUpdateVO updateVO = buildNovatorUpdateVO(paramMap);
                // Get the list of cross sell product masters to remove it from
                String crossSellProductList = (String) paramMap.get(OrderEntryConstants.PARAMETER_XSELL_PRODUCT);
                String csrId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CSR_ID);
                responseDoc = crossSellBO.deleteCrossSellProduct(productId, crossSellProductList, csrId, updateVO);
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_XSELL_GET_XSELL)) 
            {
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                productId = productId.toUpperCase();
                logger.debug("productId = " + productId);
                responseDoc = crossSellBO.getCrossSellProductAjax(productId);
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            
        }
        catch(Throwable t)
        {
            logger.error(t);
            createResponse(response, createErrorResponseDocumentNoSupportMsg(t.getMessage()), null);
        }
        return null;

    }

    public void setCrossSellBO(CrossSellBO param)
    {
        this.crossSellBO = param;
    }

    public CrossSellBO getCrossSellBO()
    {
        return crossSellBO;
    }
    
    private NovatorUpdateVO buildNovatorUpdateVO(HashMap paramMap)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("on"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("on"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("on"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_UAT);
        if (updateUAT != null && updateUAT.equals("on"))
        {
            vo.setUpdateUAT(true);
        }
        
        return vo;
    }
    
    
    private CrossSellVO buildCrossSellVO(HashMap paramMap)
    {
        String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
        productId = productId.toUpperCase();
        String csrId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CSR_ID);
        String crossSellId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CROSS_SELL_PRODUCT_ID);
        crossSellId = crossSellId.toUpperCase();
        logger.debug("product id: " + productId);
        CrossSellVO vo = new CrossSellVO();
        vo.setProductId(productId);
        vo.setUpdatedBy(csrId);

        if (crossSellId != null && !crossSellId.equals("")) {
            logger.debug("crossSellId: '" + crossSellId + "'");
            String[] detailIds = crossSellId.split(" ");
            for (int i=0; i<detailIds.length; i++) {
                logger.debug("detail id: " + detailIds[i]);
                CrossSellDetailVO csdVO = new CrossSellDetailVO();
                csdVO.setDetailProductId(detailIds[i]);
                vo.setDetailVO(csdVO);
            }
        }

        return vo;
    }

}
