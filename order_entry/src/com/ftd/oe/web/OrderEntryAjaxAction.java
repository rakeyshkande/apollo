package com.ftd.oe.web;

import com.ftd.oe.interfaces.LookupBO;
import com.ftd.oe.interfaces.ValidationBO;
import com.ftd.oe.interfaces.OrderBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.vo.fresh.FreshProdInfoVO;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OrderEntryAjaxAction extends OEActionBase {

    private static final Logger logger = new Logger("com.ftd.oe.web.OrderEntryAjaxAction");
    private LookupBO lookupBO;
    private ValidationBO validationBO;
    private OrderBO orderBO;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        HashMap paramMap = null;
        String jsonStr = null;
        String freshProdJson = null;
        FreshProdInfoVO freshProdInfoVo = null;
        try {
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocument(errorMessage), null);
                return null;
            }
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            logger.debug(JAXPUtil.toString(requestDoc));

            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);
            logger.debug("requestType: " + requestType);
            
            Document responseDoc = null;
            String responseData = null;
            if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_INTRO_DATA)) {
                responseData = lookupBO.getIntroData();
            }else if(requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_SYMPATHY_LOCATION_CHECK)){
            	logger.debug(JAXPUtil.toString(requestDoc));
            	boolean leadTimeFlag;
            	String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
            	String companyId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COMPANY_ID);
            	String state = (String) paramMap.get(OrderEntryConstants.PARAMETER_STATE);
            	String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
            	String deliveryTime = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_TIME);
            	String deliveryMethod = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_METHOD);
            	String addressType = (String) paramMap.get(OrderEntryConstants.PARAMETER_ADDRESS_TYPE);
            	String leadTimeCheckFlag = (String) paramMap.get(OrderEntryConstants.PARAMETER_LEAD_TIME_CHECK_FLAG);
            	leadTimeFlag = leadTimeCheckFlag.equals("Y") ? true : false;
            	responseData = lookupBO.getSympathyLocationCheck(sourceCode, companyId, state,deliveryDate,deliveryTime,addressType,deliveryMethod,leadTimeFlag);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_CUSTOMER_BY_PHONE_NUMBER)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String phoneNumber = (String) paramMap.get(OrderEntryConstants.PARAMETER_PHONE_NUMBER);
                String companyId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COMPANY_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                responseDoc = lookupBO.getCustomerByPhoneNumber(phoneNumber, companyId, sourceCode);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_ZIP_SEARCH)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String city = (String) paramMap.get(OrderEntryConstants.PARAMETER_CITY);
                String state = (String) paramMap.get(OrderEntryConstants.PARAMETER_STATE);
                //String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE;
                responseDoc = lookupBO.getZipsByCityState(city, state);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_POPULAR_PRODUCTS)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                String dateRangeEnd = (String) paramMap.get(OrderEntryConstants.PARAMETER_DATE_RANGE_FLAG);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                responseDoc = lookupBO.getPopularProducts(zipCode, deliveryDate, countryId, sourceCode, dateRangeEnd);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_CROSS_SELL_PRODUCTS)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                String dateRangeEnd = (String) paramMap.get(OrderEntryConstants.PARAMETER_DATE_RANGE_FLAG);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                responseDoc = lookupBO.getCrossSellProducts(productId, sourceCode, zipCode, deliveryDate, countryId, dateRangeEnd);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_SOURCE_CODE_SEARCH)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String searchType = (String) paramMap.get(OrderEntryConstants.PARAMETER_SEARCH_TYPE);
                String keywords = (String) paramMap.get(OrderEntryConstants.PARAMETER_KEYWORDS);
                String iotwSourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_IOTW_SOURCE_CODE);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String priceHeaderId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRICE_HEADER_ID);
                String companyId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COMPANY_ID);
                responseDoc = lookupBO.sourceCodeSearch(searchType, keywords, iotwSourceCode, sourceCode, productId, priceHeaderId, companyId);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_PRODUCT_SEARCH)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String keywords = (String) paramMap.get(OrderEntryConstants.PARAMETER_KEYWORDS);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                String dateRangeEnd = (String) paramMap.get(OrderEntryConstants.PARAMETER_DATE_RANGE_FLAG);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                responseDoc = lookupBO.productSearch(keywords, zipCode, deliveryDate, countryId, sourceCode, dateRangeEnd);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FLORIST_SEARCH)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                String dateRangeEnd = (String) paramMap.get(OrderEntryConstants.PARAMETER_DATE_RANGE_FLAG);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                responseDoc = lookupBO.floristSearch(productId, zipCode, deliveryDate, dateRangeEnd, sourceCode);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FACILITY_SEARCH)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String city = (String) paramMap.get(OrderEntryConstants.PARAMETER_CITY);
                String state = (String) paramMap.get(OrderEntryConstants.PARAMETER_STATE);
                String addressType = (String) paramMap.get(OrderEntryConstants.PARAMETER_ADDRESS_TYPE);
                String keywords = (String) paramMap.get(OrderEntryConstants.PARAMETER_KEYWORDS);
                responseDoc = lookupBO.institutionSearch(city, state, addressType, keywords);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_DNIS)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String secToken = (String) paramMap.get(OrderEntryConstants.PARAMETER_SEC_TOKEN);
                String dnisId = (String) paramMap.get(OrderEntryConstants.PARAMETER_DNIS_ID);
                responseDoc = validationBO.validateDnis(dnisId, secToken);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_SOURCE_CODE)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String secToken = (String) paramMap.get(OrderEntryConstants.PARAMETER_SEC_TOKEN);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                String dnisId = (String) paramMap.get(OrderEntryConstants.PARAMETER_DNIS_ID);
                responseDoc = validationBO.validateSourceCode(sourceCode, dnisId, secToken);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_ZIP_CODE)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                responseDoc = validationBO.validateZipCode(zipCode);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_STATE_BY_ZIP_CITY)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String city = (String) paramMap.get(OrderEntryConstants.PARAMETER_CITY);
                responseDoc = validationBO.validateStateByZipcodeAndCity(zipCode, city);
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_VALIDATE_PROD_GROUPON")) {
               logger.debug(JAXPUtil.toString(requestDoc));
               String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
               String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
               String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
               responseDoc = validationBO.validateProductGroupon(productId, sourceCode, countryId);
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_VALIDATE_PROD_FRESH")) {
               logger.debug(JAXPUtil.toString(requestDoc));
               String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
               String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
               String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
               freshProdInfoVo = validationBO.validateProductFresh(productId, sourceCode, countryId); 
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_TRIGGER_PROD_FRESH")) {
               logger.debug(JAXPUtil.toString(requestDoc));
               String updatedAfterDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_UPDATED_AFTER);
               validationBO.triggerProjectFreshUpdates(updatedAfterDate); 
               jsonStr = "{\"Success\": \"Triggers should occur for Apollo products updated after " + updatedAfterDate + "\"}";
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_PRODUCT)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                responseDoc = validationBO.validateProduct(productId, sourceCode, countryId);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_PRODUCT_AVAILABILITY)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                String dateRangeEnd = (String) paramMap.get(OrderEntryConstants.PARAMETER_DATE_RANGE_FLAG);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                String orderXML = (String) paramMap.get(OrderEntryConstants.PARAMETER_ORDER_XML);
                Document orderDoc = JAXPUtil.parseDocument(orderXML);
                String addons = (String) paramMap.get(OrderEntryConstants.PARAMETER_ADDONS);
                // Return the list of shipping charges by delivery date and ship method
                boolean getChargesXML = true;
                responseDoc = validationBO.validateProductAvailability(productId, zipCode, deliveryDate,
                		countryId, sourceCode, dateRangeEnd, orderDoc, getChargesXML, addons);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_FLORIST)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String floristId = (String) paramMap.get(OrderEntryConstants.PARAMETER_FLORIST_ID);
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String deliveryDate = (String) paramMap.get(OrderEntryConstants.PARAMETER_DELIVERY_DATE);
                responseDoc = validationBO.validateFlorist(floristId, productId, zipCode, deliveryDate);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_AVS_ADDRESS)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String firmName = (String) paramMap.get(OrderEntryConstants.PARAMETER_FIRM_NAME);
                String address = (String) paramMap.get(OrderEntryConstants.PARAMETER_ADDRESS);
                String city = (String) paramMap.get(OrderEntryConstants.PARAMETER_CITY);
                String state = (String) paramMap.get(OrderEntryConstants.PARAMETER_STATE);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                String orderType = (String) paramMap.get(OrderEntryConstants.PARAMETER_ORDER_TYPE);
                responseDoc = validationBO.validateAVSAddress(firmName, address, city, state, zipCode, countryId, orderType);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_VALIDATE_GIFT_CERTIFICATE)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String certificateId = (String) paramMap.get(OrderEntryConstants.PARAMETER_GIFT_CERTIFICATE_ID);
                responseDoc = validationBO.validateGiftCertificate(certificateId);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_CALCULATE_ORDER_TOTALS)) {
                String orderXML = (String) paramMap.get(OrderEntryConstants.PARAMETER_ORDER_XML);
                Document orderDoc = JAXPUtil.parseDocument(orderXML);
                responseDoc = orderBO.calculateOrderTotals(orderDoc, true);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_ELEMENT_DATA)) {
            	  responseData = lookupBO.getElementData();
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_SAVE_CART)) {
                String orderXML = (String) paramMap.get(OrderEntryConstants.PARAMETER_ORDER_XML);
                String csrId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CSR_ID);
                orderXML.replace("\n", "");
                Document orderDoc = JAXPUtil.parseDocument(orderXML);
                responseDoc = orderBO.submitCart(orderDoc,csrId);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_RECORD_CALL_TIME)) {
                logger.debug(JAXPUtil.toString(requestDoc));
                String startTime = (String) paramMap.get(OrderEntryConstants.PARAMETER_START_TIME);
                String csrId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CSR_ID);
                String dnisId = (String) paramMap.get(OrderEntryConstants.PARAMETER_DNIS_ID);
                String masterOrderNumber = (String) paramMap.get(OrderEntryConstants.PARAMETER_MASTER_ORDER_NUMBER);
                responseDoc = orderBO.recordCallTime(startTime, csrId, dnisId, masterOrderNumber);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_DELIVERY_DATES)) {
            	logger.info(JAXPUtil.toString(requestDoc));
                String productId = (String) paramMap.get(OrderEntryConstants.PARAMETER_PRODUCT_ID);
                String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
                String countryId = (String) paramMap.get(OrderEntryConstants.PARAMETER_COUNTRY_ID);
                String addons = (String) paramMap.get(OrderEntryConstants.PARAMETER_ADDONS);
                String source_code = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
                responseDoc = lookupBO.getDeliveryDates(productId, zipCode, countryId, addons,source_code);
            } else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_UPDATE_ORDER_INFO)) {
            	logger.debug(JAXPUtil.toString(requestDoc));
            	String externalOrderNumber = (String) paramMap.get(OrderEntryConstants.PARAMETER_EXTERNAL_ORDER_NUMBER);
            	responseDoc = lookupBO.getUpdateOrderInfo(externalOrderNumber);
            }	else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_MEMBERSHIP_ID_SEARCH)) {
            	logger.debug(JAXPUtil.toString(requestDoc));
            	String zipCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_ZIP_CODE);
            	responseDoc = lookupBO.getMembershipIdInfo(zipCode); 
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_PROMOTION_INFO")) {
               logger.debug(JAXPUtil.toString(requestDoc));
               String sourceCode = (String) paramMap.get(OrderEntryConstants.PARAMETER_SOURCE_CODE);
               jsonStr = lookupBO.getPromotionInfo(sourceCode);
            
            } else {
                responseDoc = createErrorResponseDocument("Invalid request type: " + requestType);
            }
            if(requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_POPULAR_PRODUCTS) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_PRODUCT_SEARCH) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_CROSS_SELL_PRODUCTS) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_CUSTOMER_BY_PHONE_NUMBER) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FLORIST_SEARCH) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FACILITY_SEARCH) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_ZIP_SEARCH) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_SOURCE_CODE_SEARCH) ||
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_MEMBERSHIP_ID_SEARCH)) {
                
                createResponseJSON(response, responseDoc, paramMap);

            } else if(requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_ELEMENT_DATA) || 
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_INTRO_DATA) || 
                    requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_SYMPATHY_LOCATION_CHECK)){
                createResponseNoEcho(response, responseData);
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_PROMOTION_INFO") ||
                       requestType.equalsIgnoreCase("AJAX_REQUEST_TRIGGER_PROD_FRESH")) {
               createResponseJsonStr(response, jsonStr);
            } else if (requestType.equalsIgnoreCase("AJAX_REQUEST_VALIDATE_PROD_FRESH")) {
               //GsonBuilder gbuild = new GsonBuilder();
               //gbuild.serializeNulls();
               //Gson gson = gbuild.create();
               Gson gson = new Gson();
               freshProdJson = gson.toJson(freshProdInfoVo);  
               createResponseJsonStr(response, freshProdJson);
            } else{
                createResponse(response, responseDoc, paramMap);
            }

        } catch (Throwable t) {
            logger.error(t);
            createResponse(response, createErrorResponseDocument(t.getMessage()), paramMap);
        }

        return null;
    }
    
    public void setLookupBO(LookupBO lookupBO) {
        this.lookupBO = lookupBO;
    }

    public LookupBO getLookupBO() {
        return lookupBO;
    }

    public void setValidationBO(ValidationBO validationBO) {
        this.validationBO = validationBO;
    }

    public ValidationBO getValidationBO() {
        return validationBO;
    }

    public void setOrderBO(OrderBO orderBO) {
        this.orderBO = orderBO;
    }

    public OrderBO getOrderBO() {
        return orderBO;
    }
}
