package com.ftd.oe.web;

import com.ftd.oe.bo.FavoritesBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.common.resources.IResourceProvider;
import com.ftd.oe.vo.CrossSellVO;
import com.ftd.oe.vo.FavoriteVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.xml.JAXPUtil;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

public class FavoritesAjaxAction extends OEActionBase {

    private static final Logger logger = 
        new Logger("com.ftd.oe.web.FavoritesAjaxAction");
    private FavoritesBO favoritesBO;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {

        HashMap paramMap = null;
        try 
        {
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) 
            {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocument(errorMessage), null);
                return null;
            }
            
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            
            if(logger.isDebugEnabled())
                logger.debug(JAXPUtil.toString(requestDoc));
            
            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);

            if(logger.isDebugEnabled())
                logger.debug("requestType: " + requestType);
            
            Document responseDoc = null;
            
            if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FAV_GET_FAVORITES)) 
            {
                responseDoc = favoritesBO.getFavoritesAjax();
                //((XMLDocument)responseDoc).print(System.out);
                createResponse(response, responseDoc, paramMap);
            }
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_FAV_UPDATE_FAVORITES)) 
            {
                List vos = buildFavoritesVO(paramMap);                

                responseDoc = favoritesBO.validate(vos);
                if (responseDoc == null)
                {
                    responseDoc = favoritesBO.update(vos);
                    createValidationResultSet(responseDoc,"Y",new HashMap());
                }
                createResponse(response, responseDoc, paramMap);
            }
        }
        catch(Throwable t)
        {
            logger.error(t);
            //System.out.println(t);
        }
        return null;
    }

    public void setFavoritesBO(FavoritesBO newfavoritesBO) {
        this.favoritesBO = newfavoritesBO;
    }

    public FavoritesBO getFavoritesBO() {
        return favoritesBO;
    }
    
    protected List buildFavoritesVO(Map paramMap) throws Exception
    {
        List favoritesList = new ArrayList();
        String csrId = (String) paramMap.get(OrderEntryConstants.PARAMETER_CSR_ID);
        
        // Loop through the keys looking for anything that starts with PRODUCT_ID
        Iterator keyIterator = paramMap.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String key = (String) keyIterator.next();
            if (key.startsWith(OrderEntryConstants.PARAMETER_PRODUCT_ID))
            {
                FavoriteVO vo = new FavoriteVO();
                String productId = (String) paramMap.get(key);
                productId = productId.toUpperCase();
                vo.setProductId(productId);
                vo.setUpdatedBy(csrId);
                int lastIndex = key.lastIndexOf("_");
                String displaySeqString = key.substring(lastIndex+1);
                vo.setDisplaySequence(displaySeqString);
                favoritesList.add(vo);
                
                logger.debug(" vo = [" + vo.getProductId() + "] [" + vo.getDisplaySequence() + "] [" + vo.getUpdatedBy() + "]");
                
            }
        }
        
        return favoritesList;
    }
}
