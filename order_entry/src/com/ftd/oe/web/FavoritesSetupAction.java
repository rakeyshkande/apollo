package com.ftd.oe.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class FavoritesSetupAction extends Action {

    private static final Logger logger = 
        new Logger("com.ftd.oe.web.FavoritesSetupAction");

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception 
    {
        String sessionId = request.getParameter("securitytoken");
        String userId = null;

        try {
            UserInfo userInfo = SecurityManager.getInstance().getUserInfo(sessionId);
            userId = userInfo.getUserID();
        } catch (Exception e) {
            logger.error("getUserInfo: User session ID verification failed: " + sessionId, e);
        }

        String secureURL = 
            ConfigurationUtil.getInstance().getFrpGlobalParm("OE_CONFIG", "SECURE_URL_PREFIX");

        request.setAttribute("identity", userId);
        request.setAttribute("secure-url", secureURL);
        
        return mapping.findForward("success");
    }
}
