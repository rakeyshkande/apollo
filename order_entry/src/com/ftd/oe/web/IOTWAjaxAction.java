package com.ftd.oe.web;

import com.ftd.oe.bo.IOTWBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.interfaces.LookupBO;
import com.ftd.oe.vo.IOTWDeliveryDateVO;
import com.ftd.oe.vo.IOTWVO;
import com.ftd.oe.vo.NovatorUpdateVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Handles AJAX calls for the Item Of The Week Maintenance screen
 */
public class IOTWAjaxAction extends OEActionBase {
    private static final Logger logger = new Logger("com.ftd.oe.web.IOTWAjaxAction");
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private LookupBO lookupBO;
    private IOTWBO iotwBO;

    /**
     * Handles the following AJAX Item Of The Week Maintenance calls:
     * 1.  Obtains Element Config data
     * 2.  Obtains IOTW data for a given IOTW Id
     * 3.  Saves IOTW program data
     * 4.  Removes IOTW data given IOTW Id(s)
     * 5.  Sends all IOTW program data to Novator as a feed
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {
       
        // Map of incoming parameters
        HashMap paramMap = null;
        
        // AJAX response document
        Document responseDoc = null;
        try {
            // Obtain AJAX request information
            String requestXML = request.getParameter(OrderEntryConstants.REQUEST_XML);
            if (requestXML == null) {
                String errorMessage = OrderEntryConstants.REQUEST_XML + " object is null.";
                logger.error(errorMessage);
                createResponse(response, createErrorResponseDocumentNoSupportMsg(errorMessage), null);
                return null;
            }
    
            Document requestDoc = JAXPUtil.parseDocument(requestXML);
            
            if(logger.isDebugEnabled())
                logger.debug(JAXPUtil.toString(requestDoc));
            
            paramMap = parseRequestDoc(requestDoc);
            String requestType = (String) paramMap.get(OrderEntryConstants.REQUEST_TYPE);
    
            // Obtain IOTW program information given an IOTW Id
            if (requestType.equalsIgnoreCase("AJAX_REQUEST_GET_IOTW")) {
    
                String iotwId = (String)paramMap.get("IOTW_ID");
                responseDoc = iotwBO.getIOTWAjax(iotwId);
                createResponse(response, responseDoc, paramMap);
    
            } // Obtain Element Config data
            else if (requestType.equalsIgnoreCase(OrderEntryConstants.REQUEST_GET_ELEMENT_DATA)) {
    
                responseDoc = lookupBO.getElementData("IOTW");
                createResponse(response, responseDoc, paramMap);
    
            } // Validate and save IOTW program information 
            else if (requestType.equalsIgnoreCase("AJAX_REQUEST_SAVE_IOTW")) {
    
                IOTWVO existingIOTWVO = null;
                
                // Build a list of IOTW programs to be saved
                List<IOTWVO> iotwList = buildIOTW((String)paramMap.get("IOTWS"), paramMap);
                
                /* 
                 * If an existing program is edited it will be passed in as the 
                 * existing IOTW program.  Build the existing IOTW program information
                 * if it exists.
                */ 
                if(paramMap.get("EXISTING_IOTW") != null){
                    List<IOTWVO> existingIOTWList = buildIOTW((String)paramMap.get("EXISTING_IOTW"), paramMap);
                    existingIOTWVO = existingIOTWList.get(0);
                } else{
                    existingIOTWVO = new IOTWVO();
                }
    
                // Determine and store which Novator environments should be updated
                NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(paramMap);
                
                // Validate and save the IOTW program data
                responseDoc = iotwBO.insert(iotwList, existingIOTWVO, novatorUpdateVO);
                
                // If a validation document isn't returned create a valid response document
                if(responseDoc == null){
                    responseDoc = createValidResponseDocument();
                    responseDoc = createValidationResultSet(responseDoc, "Y", new HashMap());
                }
                
                createResponse(response, responseDoc, paramMap);
    
            } // Remove IOTW program data 
            else if (requestType.equalsIgnoreCase("AJAX_REQUEST_REMOVE_IOTW")) {
    
                // Obtain a list of IOTW programs to remove
                List<IOTWVO> iotwList = buildIOTW((String)paramMap.get("IOTWS"), paramMap);
    
                // Determine and store which Novator environments should be updated
                NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(paramMap);
                
                // Remove the IOTW programs
                responseDoc = iotwBO.delete(iotwList, novatorUpdateVO);
                
                // If a validation document isn't returned create a valid response document
                if(responseDoc == null){
                    // Create a valid response document
                    responseDoc = createValidResponseDocument();
                    responseDoc = createValidationResultSet(responseDoc, "Y", new HashMap());
                }
                
                createResponse(response, responseDoc, paramMap);
    
            } // Send all IOTW programs to Novator as a feed 
            else if(requestType.equalsIgnoreCase("AJAX_REQUEST_SEND_FEED_ALL")) {
    
                // Determine and store which Novator environments should be updated
                NovatorUpdateVO novatorUpdateVO = buildNovatorUpdateVO(paramMap);
                
                // Send all IOTW programs to Novator as a feed
                responseDoc = iotwBO.sendAll(novatorUpdateVO);
                
                // If a validation document isn't returned create a valid response document
                if(responseDoc == null){
                    // Create a valid response document
                    responseDoc = createValidResponseDocument();
                    responseDoc = createValidationResultSet(responseDoc, "Y", new HashMap());
                }

                createResponse(response, responseDoc, paramMap);
    
            }
        }
        catch(Throwable t){
            logger.error(t);
            createResponse(response, createErrorResponseDocumentNoSupportMsg(t.getMessage()), null);
        }
        return null;
    }

    /**
     * Build a list of IOTW program objects from XML
     * @param iotwData XML string of IOTW program data
     * @param paramMap Map of incoming parameter data
     * @return List of IOTW program objects
     * @throws Exception
     */
    private List<IOTWVO> buildIOTW(String iotwData, HashMap paramMap) throws Exception {
        StringTokenizer st = null;
        
        // Map of source codes
        HashMap sourceCodeMap = null;
        
        // Map product ids
        HashMap productIdMap = null;
        
        // Working IOTWVO object
        IOTWVO iotwVO = null;
        
        // List of IOTW program objects
        ArrayList<IOTWVO> iotwList = new ArrayList();
        
        // IOTW node
        Node iotwNode = null;
        
        // IOTW child nodes
        NodeList iotwDataNL = null;
        
        // IOTW child data node
        Node iotwDataNode = null;
        
        // IOTW child data node name
        String iotwDataNodeName = null;
        
        // IOTW child data node value
        String iotwDataNodeValue = null;
        
        // Delivery discount data node list
        NodeList delDateNL = null;

        
        // Parse IOTW data
        Document iotwDoc = JAXPUtil.parseDocument(iotwData);
        
        // List of IOTW nodes
        NodeList iotwNL = iotwDoc.getElementsByTagName("IOTW");
        // Iterate through IOTW nodes
        for(int i = 0; i < iotwNL.getLength(); i++) {
            // Reset working data values
            sourceCodeMap = new HashMap();
            productIdMap = new HashMap();
            iotwDataNode = null;
            iotwDataNodeName = null;
            iotwDataNodeValue = null;
            delDateNL = null;
            iotwVO = new IOTWVO();

            // Obtain next IOTW node
            iotwNode = iotwNL.item(i);
            // Obtain IOTW child nodes
            iotwDataNL = iotwNode.getChildNodes();
    
            // Set csr id updated by field
            iotwVO.setUpdatedBy((String)paramMap.get("CSR_ID"));
    
            // Iterate through IOTW data
            for(int j = 0; j < iotwDataNL.getLength(); j++) {
                // Obtain IOTW child node name and value
                iotwDataNode = iotwDataNL.item(j);
                iotwDataNodeName = iotwDataNode.getNodeName();
                iotwDataNodeValue = null;
                if (iotwDataNode.hasChildNodes()) {
                    iotwDataNodeValue = iotwDataNode.getFirstChild().getNodeValue();
                }
                
                // If a value exists for the node set the appropriate value within the IOTW program object
                if(iotwDataNodeValue != null) {
                    if(iotwDataNodeName.equals("IOTW_ID")) {
                        iotwVO.setIotwId(iotwDataNodeValue.toUpperCase());
                    } else if(iotwDataNodeName.equals("SOURCE_CODE")) {
                        iotwVO.setSourceCode(iotwDataNodeValue);
                    } else if(iotwDataNodeName.equals("PRODUCT_ID")) {
                        iotwVO.setProductId(iotwDataNodeValue.toUpperCase());
                    } else if(iotwDataNodeName.equals("SOURCE_CODES")) {
                        // Add all source codes to a HashMap to alleviate duplicates
                        st = new StringTokenizer(iotwDataNodeValue);
                        while(st.hasMoreTokens()) {
                            sourceCodeMap.put(st.nextToken(), "");
                        }
                    } else if(iotwDataNodeName.equals("PRODUCT_IDS")) {
                        // Add all product ids to a HashMap to alleviate duplicates
                        st = new StringTokenizer(iotwDataNodeValue);
                        while(st.hasMoreTokens()) {
                            productIdMap.put(st.nextToken(), "");
                        }
                    } else if(iotwDataNodeName.equals("IOTW_SOURCE_CODE")) {
                        iotwVO.setIotwSourceCode(iotwDataNodeValue);              
                    } else if(iotwDataNodeName.equals("START_DATE")) {
                        iotwVO.setStartDate(sdf.parse(iotwDataNodeValue));
                    } else if(iotwDataNodeName.equals("END_DATE")) {
                        iotwVO.setEndDate(sdf.parse(iotwDataNodeValue));              
                    } else if(iotwDataNodeName.equals("SPECIAL_OFFER")) {
                        iotwVO.setSpecialOffer(Boolean.parseBoolean(iotwDataNodeValue));              
                    } else if(iotwDataNodeName.equals("WORDING_COLOR")) {
                        iotwVO.setWordingColor(iotwDataNodeValue);              
                    } else if(iotwDataNodeName.equals("PRODUCT_PAGE_MESSAGE")) {
                        iotwVO.setProductPageMessage(URLDecoder.decode(iotwDataNodeValue, "UTF-8"));              
                    } else if(iotwDataNodeName.equals("CALENDAR_MESSAGE")) {
                        iotwVO.setCalendarMessage(URLDecoder.decode(iotwDataNodeValue, "UTF-8"));  
                    } 
                } // If no value exists and the node name is for delivery discount dates 
                else if(iotwDataNodeName.equals("DELIVERY_DATES")) {
                    delDateNL = iotwDataNode.getChildNodes();
                    // Iterate through delivery date information and create a 
                    // Delivery Discount Date object for each delivery discount date
                    for(int k = 0; k < delDateNL.getLength(); k++) {
                        IOTWDeliveryDateVO iotwDelDateVO = new IOTWDeliveryDateVO();
                        iotwDelDateVO.setUpdatedBy((String)paramMap.get("CSR_ID"));
                        Node delDateNode = delDateNL.item(k);
                        String delDateNodeName = delDateNode.getNodeName();
                        if(delDateNodeName.equals("DELIVERY_DATE")){
                            if (delDateNode.hasChildNodes()) {
                                iotwDelDateVO.setDiscountDate(sdf.parse(delDateNode.getFirstChild().getNodeValue()));
                            }
                            iotwVO.getDeliveryDiscountDates().add(iotwDelDateVO);
                        }
                    }
                }
            }
    
            /* If multiple source codes and product ids exist create an IOTWVO for 
             * each source code and product id combination.
             */ 
            if(sourceCodeMap.size() > 0 && productIdMap.size() > 0){
                String sourceCode = null;
                IOTWVO newIOTWVO = null;
                for(Iterator<String> it = sourceCodeMap.keySet().iterator(); it.hasNext();) {
                    sourceCode = it.next().toUpperCase();
                    for(Iterator<String> it2 = productIdMap.keySet().iterator(); it2.hasNext();) {
                        newIOTWVO = new IOTWVO();
                        newIOTWVO.setIotwSourceCode(iotwVO.getIotwSourceCode());
                        newIOTWVO.setStartDate(iotwVO.getStartDate());
                        newIOTWVO.setEndDate(iotwVO.getEndDate());
                        newIOTWVO.setSpecialOffer(iotwVO.isSpecialOffer());
                        newIOTWVO.setWordingColor(iotwVO.getWordingColor());
                        newIOTWVO.setProductPageMessage(iotwVO.getProductPageMessage());
                        newIOTWVO.setCalendarMessage(iotwVO.getCalendarMessage());
                        newIOTWVO.setSourceCode(sourceCode);
                        newIOTWVO.setProductId(it2.next().toUpperCase());
                        newIOTWVO.setDeliveryDiscountDates(iotwVO.getDeliveryDiscountDates());
                        newIOTWVO.setUpdatedBy(iotwVO.getUpdatedBy());
                        iotwList.add(newIOTWVO);
                    }                    
                }
            } else {
                iotwList.add(iotwVO);
            }
        }
    
        return iotwList;
    }

    /**
     * Build a list of IOTW program objects from XML
     * @param paramMap Map of incoming parameter data
     * @return NovatorUpdateVO containing Novator feed information
     */
    private NovatorUpdateVO buildNovatorUpdateVO(HashMap paramMap)
    {
        NovatorUpdateVO vo = new NovatorUpdateVO();
        String updateContent = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_CONTENT);
        if (updateContent != null && updateContent.equals("on"))
        {
            vo.setUpdateContent(true);
        }
        String updateLive = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_LIVE);
        if (updateLive != null && updateLive.equals("on"))
        {
            vo.setUpdateLive(true);
        }
        String updateTest = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_TEST);
        if (updateTest != null && updateTest.equals("on"))
        {
            vo.setUpdateTest(true);
        }
        String updateUAT = (String) paramMap.get(OrderEntryConstants.PARAMETER_NOVATOR_UPDATE_UAT);
        if (updateUAT != null && updateUAT.equals("on"))
        {
            vo.setUpdateUAT(true);
        }
        
        return vo;
    }

    public void setLookupBO(LookupBO lookupBO){
    this.lookupBO = lookupBO;
    }
    
    public LookupBO getLookupBO(){
    return lookupBO;
    }

    public void setIotwBO(IOTWBO newiotwBO) {
        this.iotwBO = newiotwBO;
    }

    public IOTWBO getIotwBO() {
        return iotwBO;
    }
}
