package com.ftd.oe.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.osp.utilities.json.JSONUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

public abstract class OEActionBase extends Action {

    private static final Logger logger = new Logger("com.ftd.oe.web.OEActionBase");

    public abstract ActionForward execute(ActionMapping mapping, ActionForm form, 
                                          HttpServletRequest request, 
                                          HttpServletResponse response) throws Exception;

    public static Document createValidResponseDocument() throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "Y");
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_MESSAGE, "");
        return doc;
    }

    protected Document createErrorResponseDocumentNoSupportMsg(String errorMessage) throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "N");
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, errorMessage);
        return doc;
    }

    protected Document createErrorResponseDocument(String errorMessage) throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        doc.appendChild(root);
        JAXPUtil.addAttribute(root, OrderEntryConstants.TAG_STATUS, "N");
        JAXPUtil.addAttribute(root,OrderEntryConstants.TAG_MESSAGE, errorMessage + "  Contact support immediately.");
        return doc;
    }

    protected HashMap parseRequestDoc(Document requestDoc) {
        HashMap paramMap = new HashMap();
        if (requestDoc != null) {
            NodeList nl = requestDoc.getElementsByTagName(OrderEntryConstants.TAG_REQUEST);
            if (nl.getLength() > 0) {
                Element temp = (Element)nl.item(0);
                String requestType = temp.getAttribute(OrderEntryConstants.TAG_TYPE);
                paramMap.put(OrderEntryConstants.REQUEST_TYPE, requestType);
                NodeList nl2 = temp.getChildNodes();
                for (int i = 0; i < nl2.getLength(); i++) {
                    Element e = (Element)nl2.item(i);
                    if (e.getNodeName().equalsIgnoreCase(OrderEntryConstants.TAG_PARAM)) {
                        String param = e.getAttribute(OrderEntryConstants.TAG_NAME).toUpperCase();
                        String value = "";
                        if (e.hasChildNodes()) {
                            Node n = e.getFirstChild();
                            value = n.getNodeValue();
                        }
                        paramMap.put(param, value);
                    } else if (e.getNodeName().equalsIgnoreCase(OrderEntryConstants.TAG_ECHO)) {
                        paramMap.put(OrderEntryConstants.TAG_ECHO_NODE, e);
                    }
                }
            }
        }

        return paramMap;
    }

    protected void createResponse(HttpServletResponse response, Document responseDoc, HashMap paramMap) throws Exception {
        response.setContentType("text/xml");
        try {
            if (paramMap != null) {
                Node echoNode = (Node) paramMap.get(OrderEntryConstants.TAG_ECHO_NODE);
                if (echoNode != null) {
                    NodeList node = responseDoc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element)node.item(0);
                        Node newNode = responseDoc.importNode(echoNode, true);
                        resultNode.appendChild(newNode);
                    }
                }
            }
            String responseXML = JAXPUtil.toString(responseDoc);
            logger.debug(responseXML);
            response.getWriter().write(responseXML);
        } catch (Exception e) {
            logger.error(e);
            response.getWriter().write(JAXPUtil.toString(createErrorResponseDocument(e.getMessage())));
        }
    }

    protected void createResponseJSON(HttpServletResponse response, Document responseDoc, HashMap paramMap) throws Exception {
        response.setContentType("text/xml");
        try {
            if (paramMap != null) {
                Node echoNode = (Node) paramMap.get(OrderEntryConstants.TAG_ECHO_NODE);
                if (echoNode != null) {
                    NodeList node = responseDoc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
                    if (node.getLength() > 0) {
                        Element resultNode = (Element)node.item(0);
                        Node newNode = responseDoc.importNode(echoNode, true);
                        resultNode.appendChild(newNode);
                    }
                }
            }
            String responseJSON = JSONUtil.xmlToJSON(responseDoc);
            logger.debug(responseJSON);
            
            response.getWriter().write(responseJSON);
        } catch (Exception e) {
            logger.error(e);
            response.getWriter().write(JAXPUtil.toString(createErrorResponseDocument(e.getMessage())));
        }
    }

    
    protected void createResponseJsonStr(HttpServletResponse response, String json) throws Exception {
       response.setContentType("application/json");
       try {
           logger.debug(json);
           
           response.getWriter().write(json);
       } catch (Exception e) {
           logger.error(e);
           response.getWriter().write(JAXPUtil.toString(createErrorResponseDocument(e.getMessage())));
       }
   }    
    
    protected void createResponseNoEcho(HttpServletResponse response, String responseData) throws Exception {
        response.setContentType("text/xml");
        try {
            logger.debug(responseData);
            
            response.getWriter().write(responseData);
        } catch (Exception e) {
            logger.error(e);
            response.getWriter().write(JAXPUtil.toString(createErrorResponseDocument(e.getMessage())));
        }
    }

    public static Document createErrorResultSet(String tagName, String errorMessage) throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(OrderEntryConstants.TAG_RESULT);
        root.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
        root.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
        doc.appendChild(root);

        Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
        rowset.setAttribute(OrderEntryConstants.TAG_NAME, tagName);
        rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "N");
        rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, errorMessage);
        root.appendChild(rowset);

        return doc;
    }

    public static Document createValidationResultSet(Document doc, String status, Map validationErrors) throws Exception {

        NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
        if (node.getLength() > 0) {
            Element resultNode = (Element)node.item(0);

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "validation");
            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, status);
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
            if (status.equalsIgnoreCase("N")) {

                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(1));
                rowset.appendChild(row);

                Set keys = validationErrors.keySet();
                Iterator keyIter = keys.iterator();
                while (keyIter.hasNext()) {
                    Object key = keyIter.next();
                    String name = (String)key;
                    String value = (String)validationErrors.get(key);
                    Element validate = doc.createElement("validate");
                    if (name != null && name.equalsIgnoreCase("product_delivery")) {
                        validate.setAttribute(OrderEntryConstants.TAG_NAME, "product_availability");
                        validate.setAttribute(OrderEntryConstants.TAG_REASON, "delivery-date");
                    } else {
                        validate.setAttribute(OrderEntryConstants.TAG_NAME, name);
                    }
                    validate.setAttribute(OrderEntryConstants.TAG_STATUS, "N");
                    validate.setAttribute(OrderEntryConstants.TAG_MESSAGE, value);
                    row.appendChild(validate);
                }
            }
            resultNode.appendChild(rowset);
        }

        return doc;

    }

    public static Document createValidationResultSet(Document doc, String status, List validationErrors) throws Exception {
        TreeMap map = new TreeMap();
        for (int i=0; i < validationErrors.size();i++)
        {
            map.put(i,validationErrors.get(i));  
        }
        
        return createValidationResultSet(doc,status,map);
    }

    public static Document createValidationResultSet(Document doc, String status, TreeMap validationErrors) throws Exception {

        NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
        if (node.getLength() > 0) {
            Element resultNode = (Element)node.item(0);

            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "validation");
            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, status);
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");
            if (status.equalsIgnoreCase("N")) {

                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(1));
                rowset.appendChild(row);

                Set keys = validationErrors.keySet();
                Iterator keyIter = keys.iterator();
                while (keyIter.hasNext()) {
                    Object key = keyIter.next();
                    String name = String.valueOf(key);
                    String value = (String)validationErrors.get(key);
                    Element validate = doc.createElement("validate");
                    validate.setAttribute(OrderEntryConstants.TAG_NAME, name);
                    validate.setAttribute(OrderEntryConstants.TAG_STATUS, "N");
                    validate.setAttribute(OrderEntryConstants.TAG_MESSAGE, value);
                    if (name != null && name.equalsIgnoreCase("product_delivery")) {
                        validate.setAttribute(OrderEntryConstants.TAG_REASON, "delivery-date");
                    }
                    row.appendChild(validate);
                }
            }
            resultNode.appendChild(rowset);
        }

        return doc;

    }
}
