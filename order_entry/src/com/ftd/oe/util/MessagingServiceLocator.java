package com.ftd.oe.util;

import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.op.order.bo.OrderAPIBO;
import com.ftd.op.venus.bo.VenusAPIBO;
import com.ftd.osp.utilities.plugins.Logger;

/**
* This locator class provides access to external or dependent services.
* The class is a singleton.
*/
public class MessagingServiceLocator {

    /**
    * Singleton Instance of this class. 
    */
    private static MessagingServiceLocator messagingServiceLocator;

    /**
    * Logger instance
    */
    private Logger logger = new Logger("com.ftd.oe.util.MessagingServiceLocator");
    
    /**
    * Singleton instance to the OrderAPIBO
    */
    private OrderAPIBO orderAPIBO;
    private MercuryAPIBO mercuryAPIBO;  
    private VenusAPIBO venusAPIBO;  

    /**
    * Private constructor to enforce singleton. 
    * 
    * @see #getInstance()
    */
    private MessagingServiceLocator() throws Exception
    {
      orderAPIBO = new OrderAPIBO();
      mercuryAPIBO = new MercuryAPIBO();
      venusAPIBO = new VenusAPIBO();
    }

    /**
    * Initializes and returns a reference to the Locator singleton instance.
    */
    public static MessagingServiceLocator getInstance() throws Exception
    {
        if(messagingServiceLocator==null)
        {
            messagingServiceLocator=new MessagingServiceLocator();
        }
        return messagingServiceLocator;
    }


    /**
    * @return Reference to the Order API
    */
    public  OrderAPIBO getOrderAPI() throws Exception
    {
        return orderAPIBO;
    }

    /**
     * @return Reference to the Mercury API
     */
     public  MercuryAPIBO getMercuryAPI() throws Exception
     {
         return mercuryAPIBO;

     }
     
      /**
     * @return Reference to the Venus API
     */
      public VenusAPIBO  getVenusAPI() throws Exception
     {
         return venusAPIBO;
     }

}