package com.ftd.oe.util;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.DeliveryDateVO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.json.JSONUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.math.BigDecimal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class IntroDataHandler extends CacheHandlerBase {

    private static Logger logger = new Logger("com.ftd.oe.util.IntroDataHandler");
    private static String introData;

    public IntroDataHandler() {
    }

    public Object load(Connection qConn) throws CacheException {

        Document doc = null;
        String data = null;

        try {

            logger.debug("Loading Intro Data XML");

            OrderEntryDAO orderEntryDAO = new OrderEntryDAO();
            doc = orderEntryDAO.getIntroDataAjax(qConn);

            SimpleDateFormat sdf1 = new SimpleDateFormat("EEE MMM dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy");

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            int dropdownDays = 7;
            try {
                dropdownDays = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                OrderEntryConstants.DELIVERY_DROPDOWN_DAYS));
            } catch (Exception e) {
                logger.error(e);
            }

            GregorianCalendar calendar = new GregorianCalendar();
            Map hm = new HashMap();
            for (int i=1; i <= dropdownDays; i++ ) {
                Date d = calendar.getTime();
                DeliveryDateVO vo = new DeliveryDateVO();
                vo.setStartDate(d);
                vo.setEndDate(d);
                vo.setDateRange(false);
                vo.setValue(sdf2.format(d));
                vo.setDisplay(sdf1.format(d));
                hm.put(vo.getValue(), vo);
                calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
            }
            
            Element rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "vendor-delivery-dates");
            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");

            Map shm = new TreeMap(hm);
            Set set= shm.keySet (  ) ; 
            Iterator iter = set.iterator (  ) ; 
            int i=1;
            while ( iter.hasNext (  )  )  {
                String key = (String) iter.next();
                DeliveryDateVO vo = (DeliveryDateVO)shm.get(key);
                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i));
                i += 1;
                rowset.appendChild(row);
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"startDate",sdf2.format(vo.getStartDate())));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"endDate",sdf2.format(vo.getEndDate())));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"dateRange",vo.isDateRange()));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"display",vo.getDisplay()));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"value",vo.getValue()));
            }  

            NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
            if (node.getLength() > 0) {
                Element resultNode = (Element) node.item(0);
                resultNode.appendChild(rowset);
            }

            CachedResultSet cr = orderEntryDAO.getDeliveryDateRanges(qConn);
            while (cr.next()) {
                Date startDate = sdf3.parse(cr.getString("startDate"));
                Date endDate = sdf3.parse(cr.getString("endDate"));
            
                Date todayDate = new Date();

                long diff = getDateDiff(startDate, endDate);
                calendar.setTime(startDate);
                String dateRange = null;
                for (long j=0; j<=diff; j++) {
                    Date d = calendar.getTime();
                    if (!( sdf2.format(d).compareTo(sdf2.format(todayDate)) < 0)) {
                        if (dateRange == null) {
                            dateRange = sdf1.format(d);
                        } else {
                            dateRange = dateRange + " or " + sdf1.format(d);
                        }
                    }
                    if (hm.containsKey(sdf2.format(d))) {
                        hm.remove(sdf2.format(d));
                    }

                    DeliveryDateVO vo = new DeliveryDateVO();
                    vo.setStartDate(startDate);
                    vo.setEndDate(endDate);
                    vo.setDateRange(true);
                    vo.setValue(sdf2.format(startDate));
                    vo.setDisplay(dateRange);
                    hm.put(vo.getValue(), vo);

                    calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
                }

            }

            rowset = doc.createElement(OrderEntryConstants.TAG_RS);
            rowset.setAttribute(OrderEntryConstants.TAG_NAME, "delivery-dates");
            rowset.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
            rowset.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");

            shm = new TreeMap(hm);
            set= shm.keySet (  ) ; 
            iter = set.iterator (  ) ; 
            i=1;
            while ( iter.hasNext (  )  )  {
                String key = (String) iter.next();
                DeliveryDateVO vo = (DeliveryDateVO)shm.get(key);
                Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
                row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i));
                i += 1;
                rowset.appendChild(row);
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"startDate",sdf2.format(vo.getStartDate())));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"endDate",sdf2.format(vo.getEndDate())));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"dateRange",vo.isDateRange()));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"display",vo.getDisplay()));
                row.appendChild(JAXPUtil.buildSimpleXmlNode(doc,"value",vo.getValue()));
            }  

            node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
            if (node.getLength() > 0) {
                Element resultNode = (Element) node.item(0);
                resultNode.appendChild(rowset);
            }
            
            logger.debug(JAXPUtil.toString(doc));

            // Convert to JSON
            data = JSONUtil.xmlToJSON(doc);

            logger.debug("Finished loading Intro Data ");
        } catch (Throwable t) {
            logger.error("Error in getIntroData()",t);
        }
        
        return data;
    }

    /**
    * Set the cached object in the cache handler. The cache handler is then 
    * responsible for fulfilling all application level API calls, to access the
    * data in the cached object.
    * 
    * @param cachedObject
    * @throws com.ftd.osp.utilities.cacheMgr.exception.CacheException
    */    
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        try 
        {
            this.introData = (String)cachedObject;  
        } catch (Exception ex) 
        {
            super.logger.error(ex);
            throw new CacheException("Could not set the cached object.");
        } 
    }

    public String getIntroDataJSON() {

        return this.introData;

    }

    /**
    *
    * @param inFromDate first date to compare
    * @param inToDate second date to compare
    * @return Returns a long value showing the number of days between the 2 dates
    */
    public static long getDateDiff (Date inFromDate, Date inToDate)
    {
        long diff = 0;

        // convert dates to same time of day (00:00:00) for better comparison as
        // number of milliseconds between dates can be less than 24 hours
        // which causes function to return 0 as difference
        // ex. 11/06/2002 23:59 & 11/07/2002 23:58 returns 0 as less than 24
        // hours as elapsed
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String fromDate = sdf.format(inFromDate);
            String toDate = sdf.format(inToDate);

            Date date1 = sdf.parse(fromDate);
            Date date2 = sdf.parse(toDate);

            diff = date2.getTime() - date1.getTime();
        }
        catch (Exception e)
        {
            diff = inToDate.getTime() - inFromDate.getTime();
        }
        float tmpFlt = ((float)diff / (float)(1000 * 60 * 60 * 24));
        String tmpStr = String.valueOf(tmpFlt);
        BigDecimal bd = new BigDecimal(tmpStr);
        int ret = bd.setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
        return ret;
    }

}
