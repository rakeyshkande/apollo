package com.ftd.oe.util;


import java.math.BigDecimal;
import java.sql.Connection;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.ProductVO;

import com.ftd.oe.vo.fresh.FreshProdInfoVO;
import com.ftd.oe.vo.fresh.FreshProductVO;
import com.ftd.oe.vo.fresh.FreshMarkersVO;
import com.ftd.oe.vo.fresh.FreshAddonsVO;
import com.ftd.oe.vo.fresh.CategoriesVO;
import com.ftd.oe.vo.fresh.CategoryVO;
import com.ftd.oe.vo.fresh.UpsellsVO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.SurchargeVO;
import com.ftd.pas.core.domain.PasShipDatesAvailVO;

public class ProductUtil {

    private static Logger logger = new Logger("com.ftd.oe.util.ProductUtil");
    private static String DISCOUNT_TYPE_DOLLARS = "D";
    private static String DISCOUNT_TYPE_PERCENT = "P";
    private static OrderEntryDAO orderEntryDAO = new OrderEntryDAO();
    private static int AMOUNT_SCALE = 2;
    private static final String ZERO_PRICE = "0.00";
    private static int FRESH_CATEGORY_MAX_DEPTH = 5;

    public static ProductVO parseProductXML(Document productXML) {
    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        ProductVO productVO = new ProductVO();
        productVO.setHasSubcodes(false);
        productVO.setHasUpsells(false);

        try {

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String url = cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                OrderEntryConstants.IMAGE_SERVER_URL);

            NodeList nl = productXML.getElementsByTagName(OrderEntryConstants.TAG_RS);
            for (int i=0; i<nl.getLength(); i++) {
                Element rsElement = (Element) nl.item(i);
                String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);
                if (nodeName != null && nodeName.equalsIgnoreCase("product_details") && rsElement.hasChildNodes()) {
                    NodeList nlRS = rsElement.getChildNodes();
                    Element record = (Element) nlRS.item(0);
                    NodeList nlRecord = record.getChildNodes();
                    for (int j=0; j<nlRecord.getLength(); j++) {
                        Element productDetails = (Element) nlRecord.item(j);
                        String name = productDetails.getNodeName();
                        String value = "";
                        if (productDetails.hasChildNodes()) {
                            Node n = productDetails.getFirstChild();
                            value = n.getNodeValue();
                        }
                        if (name.equalsIgnoreCase("product_id") || name.equalsIgnoreCase("apollo_product_id")) {
                            productVO.setProductId(value);
                        } else if (name.equalsIgnoreCase("novator_id") || name.equalsIgnoreCase("ftd_product_id")) {
                            productVO.setNovatorId(value);
                            productVO.setSmallImage(url + "/" + value + "_a.jpg");
                            productVO.setLargeImage(url + "/" + value + "_c.jpg");
                        } else if (name.equalsIgnoreCase("status")) {
                            productVO.setStatus(value);
                        } else if (name.equalsIgnoreCase("delivery_type")) {
                            productVO.setDeliveryType(value);
                        } else if (name.equalsIgnoreCase("product_name") || name.equalsIgnoreCase("apollo_product_name")) {
                            productVO.setProductName(value);
                        } else if (name.equalsIgnoreCase("novator_name") || name.equalsIgnoreCase("ftd_product_name")) {
                            productVO.setNovatorName(value);
                        } else if (name.equalsIgnoreCase("product_type")) {
                            productVO.setProductType(value);
                        }else if (name.equalsIgnoreCase("shipping_system")) {
                            productVO.setShippingSystem(value);
                        } else if (name.equalsIgnoreCase("color_size_flag")) {
                            productVO.setColorSizeFlag(value);
                        } else if (name.equalsIgnoreCase("short_description")) {
                            productVO.setShortDescription(value);
                        } else if (name.equalsIgnoreCase("long_description")) {
                            productVO.setLongDescription(value);
                        } else if (name.equalsIgnoreCase("add_on_cards_flag")) {
                            productVO.setAddonCardFlag(value);
                        } else if (name.equalsIgnoreCase("add_on_funeral_flag")) {
                            productVO.setAddonFuneralFlag(value);
                        } else if (name.equalsIgnoreCase("standard_price")) {
                            if (StringUtils.isBlank(value)) {
                                productVO.setStandardPrice(new BigDecimal(0));
                            } else {
                                productVO.setStandardPrice(new BigDecimal(value));
                            }
                        } else if (name.equalsIgnoreCase("premium_price")) {
                            if (StringUtils.isBlank(value)) {
                                productVO.setPremiumPrice(new BigDecimal(0));
                            } else {
                                productVO.setPremiumPrice(new BigDecimal(value));
                            }
                        } else if (name.equalsIgnoreCase("deluxe_price")) {
                            if (StringUtils.isBlank(value)) {
                                productVO.setDeluxePrice(new BigDecimal(0));
                            } else {
                                productVO.setDeluxePrice(new BigDecimal(value));
                            }
                        } else if (name.equalsIgnoreCase("weboe_blocked")) {
                            productVO.setWeboeBlocked(value);
                        } else if (name.equalsIgnoreCase("ship_method_florist")) {
                            productVO.setShipMethodFlorist(value);
                        } else if (name.equalsIgnoreCase("ship_method_carrier")) {
                            productVO.setShipMethodCarrier(value);
                        } else if (name.equalsIgnoreCase("no_tax_flag")) {
                            productVO.setNoTaxFlag(value);
                        } else if (name.equalsIgnoreCase("shipping_key")) {
                            productVO.setShippingKey(value);
                        } else if (name.equalsIgnoreCase("discount_allowed_flag")) {
                            productVO.setDiscountAllowedFlag(value);
                        } else if (name.equalsIgnoreCase("custom_flag")) {
                            productVO.setCustomFlag(value);
                        } else if (name.equalsIgnoreCase("popularity_order_cnt")) {
                            productVO.setPopularityOrderCount(value);
                        } else if (name.equalsIgnoreCase("exception_code")) {
                            productVO.setExceptionCode(value);
                        } else if (name.equalsIgnoreCase("exception_start_date")) {
                            if (StringUtils.isNotBlank(value)) {
                                productVO.setExceptionStartDate(sdf.parse(value));
                            }
                        } else if (name.equalsIgnoreCase("exception_end_date")) {
                            if (StringUtils.isNotBlank(value)) {
                                productVO.setExceptionEndDate(sdf.parse(value));
                            }
                        } else if (name.equalsIgnoreCase("exception_message")) {
                            productVO.setExceptionMessage(value);
                        } else if (name.equalsIgnoreCase("over_21")) {
                            productVO.setOver21Flag(value);
                        } else if (name.equalsIgnoreCase("personal_greeting_flag")) {
                            productVO.setPersonalGreetingFlag(value);
                        } else if (name.equalsIgnoreCase("premier_collection_flag")) {
                            productVO.setPremierCollectionFlag(value);
                        } else if (name.equalsIgnoreCase("allow_free_shipping_flag")) {
                            productVO.setAllowFreeShippingFlag(value);
                        }
                    }
                } else if (nodeName != null && nodeName.equalsIgnoreCase("product_subcodes")) {
                    String status = rsElement.getAttribute(OrderEntryConstants.TAG_STATUS);
                    if (status != null && status.equalsIgnoreCase("Y")) {
                        productVO.setHasSubcodes(true);
                    }
                } else if (nodeName != null && nodeName.equalsIgnoreCase("product_upsells")) {
                    String status = rsElement.getAttribute(OrderEntryConstants.TAG_STATUS);
                    if (status != null && status.equalsIgnoreCase("Y")) {
                        productVO.setHasUpsells(true);
                    }
                    NodeList nlRS = rsElement.getChildNodes();
                    for (int k=0; k<nlRS.getLength(); k++) {
                        Element record = (Element) nlRS.item(k);
                        NodeList nlRecord = record.getChildNodes();
                        for (int j=0; j<nlRecord.getLength(); j++) {
                            Element upsellDetails = (Element) nlRecord.item(j);
                            String name = upsellDetails.getNodeName();
                            String value = "";
                            if (upsellDetails.hasChildNodes()) {
                                Node n = upsellDetails.getFirstChild();
                                value = n.getNodeValue();
                            }
                            if (name.equalsIgnoreCase("upsell_detail_id")) {
                                productVO.setUpsellDetailIds(value);
                            }
                        }
                    }
                } else if (nodeName != null && (nodeName.equalsIgnoreCase("product_addons") || nodeName.equalsIgnoreCase("product_vases"))) {
                    NodeList nlRS = rsElement.getChildNodes();
                    for (int k=0; k<nlRS.getLength(); k++) {
                        Element record = (Element) nlRS.item(k);
                        NodeList nlRecord = record.getChildNodes();
                        for (int j=0; j<nlRecord.getLength(); j++) {
                            Element addons = (Element) nlRecord.item(j);
                            String name = addons.getNodeName();
                            String value = "";
                            if (addons.hasChildNodes()) {
                                Node n = addons.getFirstChild();
                                value = n.getNodeValue();
                            }
                            if (name.equalsIgnoreCase("addon_id")) {
                                productVO.setAddonIds(value);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        
        return productVO;
    }

    public static void createProductXML(Connection qConn, Document doc, String productId,
        String sourceCode, String sourceDiscountAllowedFlag, String priceHeaderId, int i,
        ProductVO productVO, String productPageMessage) {

        try {

            NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RS);
            if (node.getLength() <= 0) {
                return;
            }

            Element resultNode = (Element) node.item(0);

            if (productVO == null) {
                Document productDoc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode);
                productVO = ProductUtil.parseProductXML(productDoc);
            }

            if (productPageMessage != null) productVO.setProductPageMessage(productPageMessage);
            
            Element row = doc.createElement(OrderEntryConstants.TAG_RECORD);
            row.setAttribute(OrderEntryConstants.TAG_ROW, Integer.toString(i+1));
            resultNode.appendChild(row);

            Element value = doc.createElement("product_id");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getProductId()));

            value = doc.createElement("product_name");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getProductName()));

            value = doc.createElement("novator_name");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getNovatorName()));

            value = doc.createElement("standard_price");
            row.appendChild(value);
            if (productVO.getStandardPrice() != null) {
                value.appendChild(doc.createTextNode(productVO.getStandardPrice().toString()));
            }

            value = doc.createElement("deluxe_price");
            row.appendChild(value);
            if (productVO.getDeluxePrice() != null) {
                value.appendChild(doc.createTextNode(productVO.getDeluxePrice().toString()));
            }

            value = doc.createElement("premium_price");
            row.appendChild(value);
            if (productVO.getPremiumPrice() != null) {
                value.appendChild(doc.createTextNode(productVO.getPremiumPrice().toString()));
            }

            value = doc.createElement("florist_delivered");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getShipMethodFlorist()));

            value = doc.createElement("carrier_delivered");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getShipMethodCarrier()));

            value = doc.createElement("short_description");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getShortDescription()));

            value = doc.createElement("long_description");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getLongDescription()));

            value = doc.createElement("smallimage");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getSmallImage()));

            value = doc.createElement("largeimage");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getLargeImage()));

            value = doc.createElement("popularity_order_count");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getPopularityOrderCount()));

            String discountAllowedFlag = productVO.getDiscountAllowedFlag();
            if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
                discountAllowedFlag = "Y";
            }

            if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
                OrderDetailVO odVO = new OrderDetailVO();
                odVO.setItemSourceCode(sourceCode);
                odVO.setDeliveryDate(new Date());
                
                MembershipUtil.calcMilesPoints(qConn, odVO, productVO);
            } else {
                ProductUtil.calcDiscount(qConn, priceHeaderId, productVO, discountAllowedFlag, true);
            }

            value = doc.createElement("discount_type");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getRewardType()));

            value = doc.createElement("standard_price_discounted");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getNetStandardPrice().toString()));

            value = doc.createElement("deluxe_price_discounted");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getNetDeluxePrice().toString()));

            value = doc.createElement("premium_price_discounted");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getNetPremiumPrice().toString()));
            
            value = doc.createElement("product_page_message");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getProductPageMessage()));

            value = doc.createElement("allow_free_shipping_flag");
            row.appendChild(value);
            value.appendChild(doc.createTextNode(productVO.getAllowFreeShippingFlag()));

        } catch (Exception e) {
            logger.error(e);
        }
    }

    public static void calcDiscount(Connection conn, String priceHeaderId, ProductVO productVO,
        String discountFlag, boolean calculatePricePoints) {

        BigDecimal productPrice = productVO.getProductPrice();

        //zero out discount amount
        if (calculatePricePoints) {
            productVO.setNetStandardPrice(new BigDecimal(0));
            productVO.setNetDeluxePrice(new BigDecimal(0));
            productVO.setNetPremiumPrice(new BigDecimal(0));
            productVO.setRewardType(null);
        } else {
            productVO.setDiscountAmount(new BigDecimal("0"));
            productVO.setDiscountedPrice(productPrice);
        }

        logger.debug("productPrice: " + productPrice + " pricingCode: " + priceHeaderId);

        //if no price code, then no discount
        if (priceHeaderId == null)
        {
            return;
        }
        
        if(discountFlag != null && discountFlag.equalsIgnoreCase("N"))
        {
            return;
        }

        try {

            //Check if the PRICE_HEADER table should be used for discount information
            if (!priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {

                if (calculatePricePoints) {

                    productPrice = productVO.getStandardPrice();
                    logger.debug("standard price: " + productPrice);
                    if (productPrice.floatValue() > 0) {
                        BigDecimal discountAmount = getDiscountDetails(conn, priceHeaderId, productPrice);
                        BigDecimal discountedPrice = productPrice.subtract(discountAmount);
                        if (discountedPrice.floatValue() < 0) {
                            discountedPrice = new BigDecimal(0);
                            discountAmount = productPrice;
                        }
                        productVO.setNetStandardPrice(discountedPrice);
                        if (discountAmount.floatValue() > 0) {
                            productVO.setRewardType("Discount");
                        }
                        logger.debug("discount: " + discountAmount);
                        logger.debug("discounted price: " + discountedPrice);
                    }
                    
                    productPrice = productVO.getDeluxePrice();
                    logger.debug("deluxe price: " + productPrice);
                    if (productPrice.floatValue() > 0) {
                        BigDecimal discountAmount = getDiscountDetails(conn, priceHeaderId, productPrice);
                        BigDecimal discountedPrice = productPrice.subtract(discountAmount);
                        if (discountedPrice.floatValue() < 0) {
                            discountedPrice = new BigDecimal(0);
                            discountAmount = productPrice;
                        }
                        productVO.setNetDeluxePrice(discountedPrice);
                        if (discountAmount.floatValue() > 0) {
                            productVO.setRewardType("Discount");
                        }
                        logger.debug("discount: " + discountAmount);
                        logger.debug("discounted price: " + discountedPrice);
                    }
                    
                    productPrice = productVO.getPremiumPrice();
                    logger.debug("premium price: " + productPrice);
                    if (productPrice.floatValue() > 0) {
                        BigDecimal discountAmount = getDiscountDetails(conn, priceHeaderId, productPrice);
                        BigDecimal discountedPrice = productPrice.subtract(discountAmount);
                        if (discountedPrice.floatValue() < 0) {
                            discountedPrice = new BigDecimal(0);
                            discountAmount = productPrice;
                        }
                        productVO.setNetPremiumPrice(discountedPrice);
                        if (discountAmount.floatValue() > 0) {
                            productVO.setRewardType("Discount");
                        }
                        logger.debug("discount: " + discountAmount);
                        logger.debug("discounted price: " + discountedPrice);
                    }
                    
                } else {
                    BigDecimal discountAmount = getDiscountDetails(conn, priceHeaderId, productPrice);
                    BigDecimal discountedPrice = productVO.getProductPrice().subtract(discountAmount);
                    if (discountedPrice.floatValue() < 0) {
                        discountedPrice = new BigDecimal(0);
                        discountAmount = productPrice;
                    }
                    productVO.setDiscountAmount(discountAmount);
                    productVO.setDiscountedPrice(discountedPrice.setScale(2,5));

                    logger.debug("discount: " + productVO.getDiscountAmount());
                    logger.debug("discountedPrice: "  + discountedPrice);
                }
                
            }//end if using price header table

        } catch (Exception e) {
            logger.error(e);
        }
    }
    

    /**
     * Method specific to calculate discounts for Groupon.
     * This was modeled after getDiscountDetails.  It was copied and modified specifically for use with Groupon.
     * It should be considered a temporary method and should be replaced with a more permanent solution in the future.
     * 
     * @param _discountType
     * @param _discountValue
     * @param _productPrice
     * @return 
     */
    public static String calcDiscountGroupon(String _discountType, String _discountValue, String _productPrice) {
       BigDecimal discountAmount  = new BigDecimal(0);
       BigDecimal discountedPrice = new BigDecimal(0);
       BigDecimal hundreths = new BigDecimal(0.01);
       
       logger.debug("Discount type: " + _discountType + " Discount value: " + _discountValue);
       if (StringUtils.isNotBlank(_discountType) && StringUtils.isNotBlank(_discountValue)) {
          try {
             BigDecimal discountValue = new BigDecimal(_discountValue);
             BigDecimal productPrice = new BigDecimal(_productPrice);
   
             //what kind of discount do we have?
             if(_discountType.equalsIgnoreCase(DISCOUNT_TYPE_DOLLARS))
             {
                 discountAmount = discountValue.setScale(AMOUNT_SCALE , BigDecimal.ROUND_DOWN);
             }
             else if(_discountType.equals(DISCOUNT_TYPE_PERCENT))
             {
                 //!! Note discounts are rounded down
                 BigDecimal amountOff = productPrice.multiply(discountValue).multiply(hundreths);
                 amountOff = amountOff.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
                 discountAmount = amountOff;
             }

             discountedPrice = productPrice.subtract(discountAmount);
             if (discountedPrice.floatValue() < 0) {
                 discountedPrice = new BigDecimal(0);
             }
          } catch(Exception e) {
             logger.error("Uh oh, can't calculate Groupon discount.  Continuing anyhow. " + e);
          }
          logger.info("Groupon discount: " + discountAmount + " Discounted price: " + discountedPrice + " Original price: " + _productPrice);
       }       
       return discountedPrice.toString();
    }

    
    private static BigDecimal getDiscountDetails(Connection conn, String priceHeaderId, BigDecimal productPrice) {
        
        BigDecimal returnDiscountAmount = new BigDecimal(0);
        BigDecimal hundreths = new BigDecimal(0.01);

        try {

            //retrieve price header information
            CachedResultSet rs = orderEntryDAO.getPriceHeaderDetails(conn, priceHeaderId, productPrice);       
            if (rs.next()) {
                String discountType = rs.getString("discount_type");
                BigDecimal discountAmount = new BigDecimal(rs.getString("discount_amt"));
                logger.debug("discountType: " + discountType + " discountAmount: " + discountAmount);

                //what kind of discount do we have?
                if(discountType.equalsIgnoreCase(DISCOUNT_TYPE_DOLLARS))
                {
                    returnDiscountAmount = discountAmount.setScale(AMOUNT_SCALE , BigDecimal.ROUND_DOWN);
                }
                else if(discountType.equals(DISCOUNT_TYPE_PERCENT))
                {
                    //!! Note discounts are rounded down
                    BigDecimal amountOff = productPrice.multiply(discountAmount).multiply(hundreths);
                    amountOff = amountOff.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
                    returnDiscountAmount = amountOff;
                }
            }
            
        } catch (Exception e) {
                logger.error(e);
        }
        
        return returnDiscountAmount;

    }

    @SuppressWarnings("rawtypes")
	public static void getAvailableDeliveryDatesAndChargesXML(Document doc, String productId, String zipCode,String sourceCode, 
                                                              OrderVO orderVO, Connection qConn) throws Exception { 

    
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);
      
        try {
        	
            NodeList node = doc.getElementsByTagName(OrderEntryConstants.TAG_RESULT);
            if (node.getLength() <= 0) {
                return;
            }

            Element resultNode = (Element) node.item(0);
            List detailList = orderVO.getOdVO();
            OrderDetailVO odVO = new OrderDetailVO();
            if (detailList.size() == 0)
            {
                logger.error("getAvailableDeliveryDatesAndChargesXML called without orderDetail record information");
                throw(new Exception("getAvailableDeliveryDatesAndChargesXML called without orderDetail record information"));
            }       
            //The first order detail should always be the one the information is gotten from
            //as joe has been modified to only send detail when making this call
            odVO = (OrderDetailVO) detailList.get(0);
            
            RecalculateOrderBO recalcBo=new RecalculateOrderBO();
            
            Date orderDate = orderVO.getOrderDate();
            if (orderDate == null)
            {
                orderDate = new Date();
            }
            
            boolean isFSmember=recalcBo.getFreeMemberShip(orderVO.getBuyerVO().getEmailAddress(), orderDate, qConn, productId, sourceCode);
            boolean isWestSpegft=isFTDWestProduct(qConn,productId,odVO.getRecipientVO().getState());
            //RecalculateOrderGlobalParmsVO rcalcOgPrams=new RecalculateOrderDAO().getGlobalRecalculationParameters(qConn);
            
            OEParameters globalParms = null;
            OEDeliveryDateParm parms = new OEDeliveryDateParm();                    
            DeliveryDateUTIL.getGlobalParameters(parms, qConn);
            globalParms = parms.getGlobalParms();
            String alaskaHawaiiCharge = globalParms.getSpecialSrvcCharge().toString();///12.99 for ak/hi charges 

            
            logger.info("orderDate::"+orderDate+" isFSmember::"+isFSmember);
            
            Element rs = doc.createElement(OrderEntryConstants.TAG_RS);
            rs.setAttribute(OrderEntryConstants.TAG_NAME, "carrier_delivery_dates");
            resultNode.appendChild(rs);

            Element row = doc.createElement(OrderEntryConstants.TAG_ROW);
            row.setAttribute(OrderEntryConstants.TAG_ROW, "1");

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            int days = 99;
            days = Integer.parseInt(cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
            OrderEntryConstants.DELIVERY_DROPDOWN_DAYS));

            String status = "N";
           // String message = "No delivery dates are available for this product";

            logger.info("Calling pas.getAvailableShipDates(qConn, " + productId + ", " + days + ", " + zipCode + ",addOns)");
            //ProductAvailabilityBO paBO = new ProductAvailabilityBO(); 
            PasShipDatesAvailVO asdVO = PASServiceUtil.getAvailableShipDates(productId, null, zipCode, sourceCode, days); 
            SurchargeVO surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(null,sourceCode,qConn );
            BigDecimal fuelSurcharge = new BigDecimal(0);
            BigDecimal AlaskaHawaiiFee = new BigDecimal(0);
            
            if(isWestSpegft)
            {
            	AlaskaHawaiiFee= alaskaHawaiiCharge!=null ? new BigDecimal(alaskaHawaiiCharge):AlaskaHawaiiFee;
            }
            logger.info("AlaskaHawaiiFee : "+AlaskaHawaiiFee);
            if (surchargeVO.isApplySurchargeFlag() && !isFSmember)
            {
               fuelSurcharge = new BigDecimal(String.valueOf(surchargeVO.getSurchargeAmount()));
            }
            else{
            	logger.info("Fuel surcharge not being applied because of surcharge Flag : "+surchargeVO.isApplySurchargeFlag()+" or the FS membership : "+isFSmember);
            }
            
            logger.info("The surcharge amount is : "+fuelSurcharge);
            
            Date[] groundDates = asdVO.getGroundDeliveryDates();
            BigDecimal[] groundCharges = asdVO.getGroundCharges();
            if (groundDates.length > 0) {
                if (status.equalsIgnoreCase("N")) {
                    status = "Y";
                    //message = "";
                    
                    rs.appendChild(row);
                }
                
                Element ground = doc.createElement("ground");
                row.appendChild(ground);
                
                for (int i=0; i< groundDates.length; i++) {
                    Date thisDate = groundDates[i];
                    Element value = doc.createElement("delivery-info" + (i+1));
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(sdf.format(thisDate)));
                    value.appendChild(date);
                        
                    odVO.setShipMethod(OrderEntryConstants.SHIP_METHOD_GROUND);
                    odVO.setDeliveryDate(thisDate);    
                    
                    BigDecimal totalCharges;
                    if (groundCharges != null && groundCharges.length > 0) {
                    	if(isFSmember)
                    		totalCharges = new BigDecimal("0") ;
                    	else
                    		totalCharges = groundCharges[i];
                        
                    	if(!isFSmember){
                    		if(fuelSurcharge.compareTo(BigDecimal.ZERO) > 0){
                    			totalCharges = totalCharges.add(fuelSurcharge);
                    		}
                    		if(isWestSpegft){
                    			totalCharges = totalCharges.add(AlaskaHawaiiFee);
                    		}
                    	}
                    
                    } else {
                        logger.debug("Calculating ground costs for date " + sdf.format(odVO.getDeliveryDate()) + 
                                 " and method " + odVO.getShipMethod() );
                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, false);
                        totalCharges = orderVO.getServiceFeeAmount().add(orderVO.getShippingFeeAmount());
                    }

                    Element charge = doc.createElement("charge");
                    charge.appendChild(doc.createTextNode(currency.format(totalCharges.doubleValue())));
                    value.appendChild(charge);
                    ground.appendChild(value);
                }
            }

            Date[] twoDayDates = asdVO.getTwoDayDeliveryDates();
            BigDecimal[] twoDayCharges = asdVO.getTwoDayCharges();
            if (twoDayDates.length > 0) {
                if (status.equalsIgnoreCase("N")) {
                    status = "Y";
                    //message = "";
                    
                    rs.appendChild(row);
                }
                
                Element twoDay = doc.createElement("two-day");
                row.appendChild(twoDay);
                
                for (int i=0; i< twoDayDates.length; i++) {
                    Date thisDate = twoDayDates[i];
                    Element value = doc.createElement("delivery-info" + (i+1));
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(sdf.format(thisDate)));
                    value.appendChild(date);
                                     
                    odVO.setShipMethod(OrderEntryConstants.SHIP_METHOD_TWO_DAY);
                    odVO.setDeliveryDate(thisDate);        
                    
                    BigDecimal totalCharges;
                    if (twoDayCharges != null && twoDayCharges.length > 0) {
                    	if(isFSmember)
                    		totalCharges = new BigDecimal("0") ;
                    	else
                    		totalCharges = twoDayCharges[i];
                    	
                    	if(!isFSmember){
                    		if(fuelSurcharge.compareTo(BigDecimal.ZERO) > 0){
                    			totalCharges = totalCharges.add(fuelSurcharge);
                    		}
                    		if(isWestSpegft){
                    			totalCharges = totalCharges.add(AlaskaHawaiiFee);
                    		}
                    	}
                    } else {
                        logger.debug("Calculating ground costs for date " + sdf.format(odVO.getDeliveryDate()) + 
                                 " and method " + odVO.getShipMethod() );
                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, false);
                        totalCharges = orderVO.getServiceFeeAmount().add(orderVO.getShippingFeeAmount());
                    }
                    
                    Element charge = doc.createElement("charge");
                    charge.appendChild(doc.createTextNode(currency.format(totalCharges.doubleValue())));
                    value.appendChild(charge);
                    twoDay.appendChild(value);
                }
            }

            Date[] nextDayDates = asdVO.getNextDayDeliveryDates();
            BigDecimal[] nextDayCharges = asdVO.getNextDayCharges();
            if (nextDayDates.length > 0) {
                if (status.equalsIgnoreCase("N")) {
                    status = "Y";
                    //message = "";
                    
                    rs.appendChild(row);
                }
                
                Element nextDay = doc.createElement("next-day");
                row.appendChild(nextDay);
                
                for (int i=0; i< nextDayDates.length; i++) {
                    Date thisDate = nextDayDates[i];
                    Element value = doc.createElement("delivery-info" + (i+1));
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(sdf.format(thisDate)));
                    value.appendChild(date);           
                  
                    odVO.setShipMethod(OrderEntryConstants.SHIP_METHOD_NEXT_DAY);
                    odVO.setDeliveryDate(thisDate);  
                    
                    BigDecimal totalCharges;
                    if (nextDayCharges != null && nextDayCharges.length > 0) {
                    	if(isFSmember)
                    		totalCharges = new BigDecimal("0") ;
                    	else
                    		totalCharges = nextDayCharges[i];
                    	
                    	if(!isFSmember){
                    		if(fuelSurcharge.compareTo(BigDecimal.ZERO) > 0){
                    			totalCharges = totalCharges.add(fuelSurcharge);
                    		}
                    		if(isWestSpegft){
                    			totalCharges = totalCharges.add(AlaskaHawaiiFee);
                    		}
                    	}
                    } else {
                        logger.debug("Calculating ground costs for date " + sdf.format(odVO.getDeliveryDate()) + 
                                 " and method " + odVO.getShipMethod() );
                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, false);
                        totalCharges = orderVO.getServiceFeeAmount().add(orderVO.getShippingFeeAmount());
                    }
  
                    Element charge = doc.createElement("charge");
                    charge.appendChild(doc.createTextNode(currency.format(totalCharges.doubleValue())));
                    value.appendChild(charge);
                    nextDay.appendChild(value);
                }
            }

            Date[] saturdayDates = asdVO.getSaturdayDeliveryDates();
            BigDecimal[] saturdayCharges = asdVO.getSaturdayCharges();
            if (saturdayDates.length > 0) {
                if (status.equalsIgnoreCase("N")) {
                    status = "Y";
                    //message = "";
                    
                    rs.appendChild(row);
                }
                
                Element saturday = doc.createElement("saturday");
                row.appendChild(saturday);
                
                for (int i=0; i< saturdayDates.length; i++) {
                    Date thisDate = saturdayDates[i];
                    Element value = doc.createElement("delivery-info" + (i+1));
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(sdf.format(thisDate)));
                    value.appendChild(date);
                             
                    odVO.setShipMethod(OrderEntryConstants.SHIP_METHOD_SATURDAY);
                    odVO.setDeliveryDate(thisDate);              
                    
                    BigDecimal totalCharges;
                    if (saturdayCharges != null && saturdayCharges.length > 0) {
                    	if(isFSmember)
                    		totalCharges = new BigDecimal("0") ;
                    	else
                    		totalCharges = saturdayCharges[i];
                    	
                    	if(!isFSmember){
                    		if(fuelSurcharge.compareTo(BigDecimal.ZERO) > 0){
                    			totalCharges = totalCharges.add(fuelSurcharge);
                    		}
                    		if(isWestSpegft){
                    			totalCharges = totalCharges.add(AlaskaHawaiiFee);
                    		}
                    	}
                    } else {
                        logger.debug("Calculating ground costs for date " + sdf.format(odVO.getDeliveryDate()) + 
                                 " and method " + odVO.getShipMethod() );
                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, false);
                        totalCharges = orderVO.getServiceFeeAmount().add(orderVO.getShippingFeeAmount());
                    }

                    Element charge = doc.createElement("charge");
                    charge.appendChild(doc.createTextNode(currency.format(totalCharges.doubleValue())));
                    value.appendChild(charge);
                    saturday.appendChild(value);
                }
            }
            
            
            
            
            Date[] sundayDates = asdVO.getSundayDeliveryDates();
            BigDecimal[] sundayCharges = asdVO.getSundayCharges();
            if(isFSmember)
            	sundayCharges=asdVO.getSundayUpCharges();
            if (sundayDates != null && sundayDates.length > 0) {
                if (status.equalsIgnoreCase("N")) {
                    status = "Y";
                    //message = "";
                    
                    rs.appendChild(row);
                }
                
                Element sunday = doc.createElement("sunday");
                row.appendChild(sunday);
                
                for (int i=0; i< sundayDates.length; i++) {
                    Date thisDate = sundayDates[i];
                    Element value = doc.createElement("delivery-info" + (i+1));
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(sdf.format(thisDate)));
                    value.appendChild(date);
                             
                    odVO.setShipMethod(OrderEntryConstants.SHIP_METHOD_SUNDAY);
                    odVO.setDeliveryDate(thisDate);              
                    
                    BigDecimal totalCharges;
                    if (sundayCharges != null && sundayCharges.length > 0) {
                    	totalCharges = sundayCharges[i];
                    	
                    	if(!isFSmember){
                    		if(fuelSurcharge.compareTo(BigDecimal.ZERO) > 0){
                    			totalCharges = totalCharges.add(fuelSurcharge);
                    		}
                    		if(isWestSpegft){
                    			totalCharges = totalCharges.add(AlaskaHawaiiFee);
                    		}
                    	}
                    } else {
                        logger.debug("Calculating ground costs for date " + sdf.format(odVO.getDeliveryDate()) + 
                                 " and method " + odVO.getShipMethod() );
                        new CalculateOrderTotalsUtil().calculateOrderTotals(qConn, orderVO, orderEntryDAO, false);
                        totalCharges = orderVO.getServiceFeeAmount().add(orderVO.getShippingFeeAmount());
                    }

                    Element charge = doc.createElement("charge");
                    charge.appendChild(doc.createTextNode(currency.format(totalCharges.doubleValue())));
                    value.appendChild(charge);
                    sunday.appendChild(value);
                }
            }

            

            rs.setAttribute(OrderEntryConstants.TAG_STATUS, "Y");
            rs.setAttribute(OrderEntryConstants.TAG_MESSAGE, "");

        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }
    
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getIOTWProducts(Connection conn, String sourceCode) {
        
        Map hm = new HashMap();

        try {
            
            CachedResultSet cr = orderEntryDAO.getIOTWProducts(conn, sourceCode);
            while (cr.next()) {
                String productId = cr.getString("product_id");
                String iotwSourceCode = cr.getString("iotw_source_code");
                String priceHeaderId = cr.getString("price_header_id");
                String productPageMessaging = cr.getString("product_page_messaging_txt");
                String sourceDiscountAllowed = cr.getString("discount_allowed_flag");
                String idddCount = cr.getString("iddd_cnt");
                String iotwId = cr.getString("iotw_id");
                logger.debug("getIOTWProducts: " + productId + " " + iotwSourceCode + " " + priceHeaderId
                    + " " + productPageMessaging + " " + idddCount + " " + iotwId);

                String[] values = new String[6];
                values[0] = iotwSourceCode;
                values[1] = priceHeaderId;
                values[2] = productPageMessaging;
                values[3] = sourceDiscountAllowed;
                values[4] = idddCount;
                values[5] = iotwId;
                hm.put(productId, values);
            }

        } catch (Exception e) {
            logger.error(e);
        }
        
        return hm;

    }
    private static  boolean isFTDWestProduct(Connection conn,String productId,String state)throws Exception
    {
    	 com.ftd.osp.utilities.vo.ProductVO productVO =new RecalculateOrderDAO().getProduct(conn, productId);
         boolean isSPEGFT=(productVO.getShippingSystem().equalsIgnoreCase(GeneralConstants.FTDW_SHIPPING_KEY) && productVO.getProductType().equalsIgnoreCase(GeneralConstants.OE_PRODUCT_TYPE_SPEGFT) && (state.equalsIgnoreCase("AK")||state.equalsIgnoreCase("HI"))) ? true: false;
         return isSPEGFT;
    	
    }
    
    
    
/* ??? TODO */   
    public static FreshProdInfoVO parseFreshProductXML(Document productXML) {
         ProductVO productVO = new ProductVO();    // ??? TODO - how to return this
         FreshProdInfoVO finfvo = new FreshProdInfoVO();
         FreshProductVO fpvo = new FreshProductVO();
         finfvo.setProduct_details(fpvo);
         productVO.setHasSubcodes(false);  //???
         productVO.setHasUpsells(false);   //???

         try {

             ConfigurationUtil cu = ConfigurationUtil.getInstance();

             NodeList nl = productXML.getElementsByTagName(OrderEntryConstants.TAG_RS);
             for (int i=0; i<nl.getLength(); i++) {
                 Element rsElement = (Element) nl.item(i);
                 String nodeName = rsElement.getAttribute(OrderEntryConstants.TAG_NAME);
                 if (nodeName != null && nodeName.equalsIgnoreCase("product_details") && rsElement.hasChildNodes()) {
                     NodeList nlRS = rsElement.getChildNodes();
                     Element record = (Element) nlRS.item(0);
                     NodeList nlRecord = record.getChildNodes();
                     for (int j=0; j<nlRecord.getLength(); j++) {
                         Element productDetails = (Element) nlRecord.item(j);
                         String name = productDetails.getNodeName();
                         String value = "";
                         if (productDetails.hasChildNodes()) {
                             Node n = productDetails.getFirstChild();
                             value = n.getNodeValue();
                         }
                         if (name.equalsIgnoreCase("product_id") || name.equalsIgnoreCase("apollo_product_id")) {
                             fpvo.setProduct_id(value);
                             fpvo.setApollo_product_id(value);
                         } else if(name.equalsIgnoreCase("monday_delivery_freshcut")){
                        	 fpvo.setMondayDeliveryFreshcut(value);
                         }  else if(name.equalsIgnoreCase("morning_delivery_flag") ){
                        	 fpvo.setMorningDeliveryFlag(value);
                         }else if(name.equalsIgnoreCase("express_shipping_only") ){
                        	 fpvo.setExpressShippingOnly(value);
                         }else if(name.equalsIgnoreCase("next_day_upgrade_flag") ){
                        	 fpvo.setNextDayUpgradeFlag(value);
                         }else if(name.equalsIgnoreCase("addon_id")){
                        	 fpvo.setAddonId(value);
                         } else if (name.equalsIgnoreCase("novator_id") || name.equalsIgnoreCase("ftd_product_id")) {
                             fpvo.setFtd_product_id(value);
                         } else if (name.equalsIgnoreCase("largeimage")) {
                            fpvo.setLargeimage(value);
                         } else if (name.equalsIgnoreCase("smallimage")) {
                            fpvo.setSmallimage(value);
                         } else if (name.equalsIgnoreCase("status")) {
                             fpvo.setStatus(value);
                         } else if (name.equalsIgnoreCase("delivery_type")) {
                             fpvo.setDelivery_type(value);
                         } else if (name.equalsIgnoreCase("product_name") || name.equalsIgnoreCase("apollo_product_name")) {
                             fpvo.setApollo_product_name(value);
                         } else if (name.equalsIgnoreCase("novator_name") || name.equalsIgnoreCase("ftd_product_name")) {
                             fpvo.setFtd_product_name(value);
                         } else if (name.equalsIgnoreCase("product_type")) {
                             fpvo.setProduct_type(value);
                         } else if (name.equalsIgnoreCase("pquad_product_id")) {
                            fpvo.setPquad_product_id(value);
                         } else if (name.equalsIgnoreCase("product_sub_type")) {
                            fpvo.setProduct_sub_type(value);
                         } else if (name.equalsIgnoreCase("shipping_system")) {
                             fpvo.setShipping_system(value);
                         } else if (name.equalsIgnoreCase("shipping_methods")) {
                            fpvo.setShipping_methods(value);
                         } else if (name.equalsIgnoreCase("color_size_flag")) {
                             fpvo.setColor_size_flag(value);
                         } else if (name.equalsIgnoreCase("short_description")) {
                             fpvo.setShort_description(value);
                         } else if (name.equalsIgnoreCase("long_description")) {
                             fpvo.setLong_description(value);
                         } else if (name.equalsIgnoreCase("bullet_description")) {
                            fpvo.setBullet_description(value);
                         } else if (name.equalsIgnoreCase("add_on_cards_flag")) {
                             fpvo.setAdd_on_cards_flag(value);
                         } else if (name.equalsIgnoreCase("add_on_funeral_flag")) {
                             fpvo.setAdd_on_funeral_flag(value);
                         } else if (name.equalsIgnoreCase("addon_text")) {
                            fpvo.setAddon_text(value);
                         } else if (name.equalsIgnoreCase("pdb_category")) {
                            fpvo.setPdb_category(value);
                         } else if (name.equalsIgnoreCase("standard_price")) {
                             if (StringUtils.isBlank(value)) {
                                 fpvo.setStandard_price(ZERO_PRICE);
                             } else {
                                 fpvo.setStandard_price(value);
                             }
                         } else if (name.equalsIgnoreCase("premium_price")) {
                             if (StringUtils.isBlank(value)) {
                                 fpvo.setPremium_price(ZERO_PRICE);
                             } else {
                                 fpvo.setPremium_price(value);
                             }
                         } else if (name.equalsIgnoreCase("deluxe_price")) {
                             if (StringUtils.isBlank(value)) {
                                 fpvo.setDeluxe_price(ZERO_PRICE);
                             } else {
                                 fpvo.setDeluxe_price(value);
                             }
                         } else if (name.equalsIgnoreCase("weboe_blocked")) {
                             fpvo.setWeboe_blocked(value);
                         } else if (name.equalsIgnoreCase("ship_method_florist")) {
                             fpvo.setShip_method_florist(value);
                         } else if (name.equalsIgnoreCase("ship_method_carrier")) {
                             fpvo.setShip_method_carrier(value);
                         } else if (name.equalsIgnoreCase("no_tax_flag")) {
                             fpvo.setNo_tax_flag(value);
                         } else if (name.equalsIgnoreCase("shipping_key")) {
                             fpvo.setShipping_key(value);
                         } else if (name.equalsIgnoreCase("discount_allowed_flag")) {
                             fpvo.setDiscount_allowed_flag(value);
                         } else if (name.equalsIgnoreCase("custom_flag")) {
                             fpvo.setCustom_flag(value);
                         } else if (name.equalsIgnoreCase("popularity_order_cnt")) {
                             fpvo.setPopularity_order_cnt(value);
                         //} else if (name.equalsIgnoreCase("exception_code")) {
                         //    productVO.setExceptionCode(value);
                         //} else if (name.equalsIgnoreCase("exception_start_date")) {
                         //    if (StringUtils.isNotBlank(value)) {
                         //        productVO.setExceptionStartDate(sdf.parse(value));
                         //    }
                         //} else if (name.equalsIgnoreCase("exception_end_date")) {
                         //    if (StringUtils.isNotBlank(value)) {
                         //        productVO.setExceptionEndDate(sdf.parse(value));
                         //    }
                         //} else if (name.equalsIgnoreCase("exception_message")) {
                         //    productVO.setExceptionMessage(value);
                         } else if (name.equalsIgnoreCase("over_21")) {
                             fpvo.setOver_21(value);
                         } else if (name.equalsIgnoreCase("personal_greeting_flag")) {
                             fpvo.setPersonal_greeting_flag(value);
                         } else if (name.equalsIgnoreCase("premier_collection_flag")) {
                             fpvo.setPremier_collection_flag(value);
                         } else if (name.equalsIgnoreCase("allow_free_shipping_flag")) {
                             fpvo.setAllow_free_shipping_flag(value);
                         } else if (name.equalsIgnoreCase("is_addon_product")) {
                            fpvo.setProduct_is_addon_flag(value);
                         } else if (name.equalsIgnoreCase("upsell_master_name")) {
                            fpvo.setUpsell_master_name(value);
                         } else if (name.equalsIgnoreCase("upsell_master_prod_id")) {
                            fpvo.setUpsell_master_prod_id(value);
                         } else if (name.equalsIgnoreCase("companies")) {
                            fpvo.setCompanies(value);
                         } else if (name.equalsIgnoreCase("upsell_master_companies")) {
                            fpvo.setUpsellMasterCompanies(value);
                         }
                     }
                 } else if (nodeName != null && nodeName.equalsIgnoreCase("product_upsells")) {
                     String status = rsElement.getAttribute(OrderEntryConstants.TAG_STATUS);
                     if (status != null && status.equalsIgnoreCase("Y")) {
                        productVO.setHasUpsells(true);  // ??? TODO
                     }
                     List<UpsellsVO> upsellsList = new ArrayList<UpsellsVO>();
                     NodeList nlRS = rsElement.getChildNodes();
                     for (int k=0; k<nlRS.getLength(); k++) {
                         Element record = (Element) nlRS.item(k);
                         NodeList nlRecord = record.getChildNodes();
                         UpsellsVO fupvo = new UpsellsVO();
                         for (int j=0; j<nlRecord.getLength(); j++) {
                             Element upsellDetails = (Element) nlRecord.item(j);
                             String name = upsellDetails.getNodeName();
                             String value = "";
                             if (upsellDetails.hasChildNodes()) {
                                 Node n = upsellDetails.getFirstChild();
                                 value = n.getNodeValue();
                             }
                             if (name.equalsIgnoreCase("upsell_detail_id")) {
                                 fupvo.setUpsell_detail_id(value);
                             } else if (name.equalsIgnoreCase("upsell_detail_sequence")) {
                                 fupvo.setUpsell_detail_sequence(value);
                             } else if (name.equalsIgnoreCase("upsell_master_id")) {
                                 fupvo.setUpsell_master_id(value);
                             } else if (name.equalsIgnoreCase("default_sku_flag")) {
                                 fupvo.setDefault_sku_flag(value);
                             } else if (name.equalsIgnoreCase("upsell_detail_name")) {
                                 fupvo.setUpsell_detail_name(value);
                             } else if (name.equalsIgnoreCase("ftd_product_id")) {
                                 fupvo.setFtd_product_id(value);
                             } else if (name.equalsIgnoreCase("upsell_price")) {
                                 fupvo.setUpsell_price(value);
                             } else if (name.equalsIgnoreCase("smallimage")) {
                                 fupvo.setSmallimage(value);
                             }
                         }
                         upsellsList.add(fupvo);
                     }
                     finfvo.setProduct_upsells(upsellsList);
                     
                 } else if (nodeName != null && (nodeName.equalsIgnoreCase("product_addons") || nodeName.equalsIgnoreCase("product_vases"))) {
                     List<FreshAddonsVO> addonsList = new ArrayList<FreshAddonsVO>();
                     NodeList nlRS = rsElement.getChildNodes();
                     for (int k=0; k<nlRS.getLength(); k++) {
                         Element record = (Element) nlRS.item(k);
                         NodeList nlRecord = record.getChildNodes();
                         FreshAddonsVO favo = new FreshAddonsVO();
                         for (int j=0; j<nlRecord.getLength(); j++) {
                             Element addons = (Element) nlRecord.item(j);
                             String name = addons.getNodeName();
                             String value = "";
                             if (addons.hasChildNodes()) {
                                 Node n = addons.getFirstChild();
                                 value = n.getNodeValue();
                             }
                             if (name.equalsIgnoreCase("addon_id")) {
                                 favo.setAddon_id(value);
                             } else if (name.equalsIgnoreCase("addon_type")) {
                                 favo.setAddon_type(value);
                             } else if (name.equalsIgnoreCase("addon_text")) {
                                favo.setAddon_text(value);
                             } else if (name.equalsIgnoreCase("apollo_product_id")) {
                                 favo.setApollo_product_id(value);
                             } else if (name.equalsIgnoreCase("description")) {
                                 favo.setDescription(value);
                             } else if (name.equalsIgnoreCase("display_seq_num")) {
                                 favo.setDisplay_seq_num(value);
                             } else if (name.equalsIgnoreCase("florist_addon_flag")) {
                                 favo.setFlorist_addon_flag(value);
                             } else if (name.equalsIgnoreCase("ftd_product_id")) {
                                 favo.setFtd_product_id(value);
                             } else if (name.equalsIgnoreCase("image")) {
                                 favo.setImage(value);
                             } else if (name.equalsIgnoreCase("max_qty")) {
                                 favo.setMax_qty(value);
                             } else if (name.equalsIgnoreCase("price")) {
                                 favo.setPrice(value);
                             } else if (name.equalsIgnoreCase("pquad_accessory_id")) {
                                favo.setPquad_accessory_id(value);
                             } else if (name.equalsIgnoreCase("vendor_addon_flag")) {
                                 favo.setVendor_addon_flag(value);
                             }
                         }
                         addonsList.add(favo);
                     }
                     if (nodeName.equalsIgnoreCase("product_addons")) {
                        finfvo.setProduct_addons(addonsList);
                     } else {
                        finfvo.setProduct_vases(addonsList);
                     }

                 // Product Markers (e.g., colors)
                 //
                 } else if (nodeName != null && (nodeName.equalsIgnoreCase("product_colors"))) {
                    List<FreshMarkersVO> mrkrList = new ArrayList<FreshMarkersVO>();                    
                    NodeList nlRS = rsElement.getChildNodes();
                    for (int k=0; k<nlRS.getLength(); k++) {
                        Element record = (Element) nlRS.item(k);
                        NodeList nlRecord = record.getChildNodes();
                        FreshMarkersVO mrkrVo = new FreshMarkersVO();
                        for (int j=0; j<nlRecord.getLength(); j++) {
                            Element markers = (Element) nlRecord.item(j);
                            String name = markers.getNodeName();
                            String value = "";
                            if (markers.hasChildNodes()) {
                                Node n = markers.getFirstChild();
                                value = n.getNodeValue();
                            }
                            if (name.equalsIgnoreCase("product_color")) {
                                mrkrVo.setMarkerId(value);
                            } else if (name.equalsIgnoreCase("description")) {
                                mrkrVo.setDescription(value);
                            } else if (name.equalsIgnoreCase("marker_type")) {
                                mrkrVo.setMarkerType(value);
                            } else if (name.equalsIgnoreCase("order_by")) {
                               mrkrVo.setOrderBy(value);
                            }
                        }
                        mrkrList.add(mrkrVo);
                    }
                    finfvo.setProduct_markers(mrkrList);
                    
                 // Website restrictions (e.g., products that are available only on certain sourcecodes)
                 //
                 } else if (nodeName != null && 
                           (nodeName.equalsIgnoreCase("product_restrictions") || nodeName.equalsIgnoreCase("upsell_restrictions"))) {
                    List<String> restrictList = new ArrayList<String>();
                    NodeList nlRS = rsElement.getChildNodes();
                    for (int k=0; k<nlRS.getLength(); k++) {
                        Element record = (Element) nlRS.item(k);
                        NodeList nlRecord = record.getChildNodes();
                        for (int j=0; j<nlRecord.getLength(); j++) {
                            Element restrict = (Element) nlRecord.item(j);
                            String name = restrict.getNodeName();
                            String value = "";
                            if (restrict.hasChildNodes()) {
                                Node n = restrict.getFirstChild();
                                value = n.getNodeValue();
                            }
                            if (name.equalsIgnoreCase("source_code")) {
                                restrictList.add(value);
                            }
                        }
                    }
                    if (nodeName.equalsIgnoreCase("product_restrictions")) {
                       finfvo.setProduct_restrictions(restrictList);                       
                    } else {
                       finfvo.setUpsell_restrictions(restrictList);
                    }
                 }
              }
              
             /* ??? TODO - MOVED TO NEW METHOD
              * 
              // Product Categories
              //
              if (categoriesSet != null) {
                 CategoryVO cat = null;
                 List<CategoryVO> catList = null;
                 CategoriesVO categories = null;
                 List<CategoriesVO> categoriesList = new ArrayList<CategoriesVO>();
                 while (categoriesSet.next()) {
                    catList = new ArrayList<CategoryVO>();
                    categories = new CategoriesVO();
                    int level = 0;
                    for (int x=FRESH_CATEGORY_MAX_DEPTH; x > 0; x--) {
                       String indexFile = categoriesSet.getString("index_file_" + x);
                       String indexName = categoriesSet.getString("index_name_" + x);
                       if (indexName != null && !indexName.isEmpty()) {
                          level++;
                          cat = new CategoryVO();
                          cat.setLevel(Integer.toString(level));
                          cat.setCategoryName(indexFile);
                          cat.setDisplayName(indexName);
                          catList.add(cat);
                       }
                    }
                    categories.setCategory(catList);
                    categoriesList.add(categories);
                 }
                 finfvo.setCategories(categoriesList);
              }
              */

                 
         } catch (Exception e) {
             logger.error(e);
         }
         
         return finfvo;
     }
    
    
    public static List<CategoriesVO> parseFreshCategories(CachedResultSet categoriesSet) {
       List<CategoriesVO> categoriesList = null;
       if (categoriesSet != null) {
          CategoryVO cat = null;
          List<CategoryVO> catList = null;
          CategoriesVO categories = null;
          categoriesList = new ArrayList<CategoriesVO>();
          while (categoriesSet.next()) {
             catList = new ArrayList<CategoryVO>();
             categories = new CategoriesVO();
             int level = 0;
             for (int x=FRESH_CATEGORY_MAX_DEPTH; x > 0; x--) {
                String indexFile = categoriesSet.getString("index_file_" + x);
                String indexName = categoriesSet.getString("index_name_" + x);
                if (indexName != null && !indexName.isEmpty()) {
                   level++;
                   cat = new CategoryVO();
                   cat.setLevel(Integer.toString(level));
                   cat.setCategoryName(indexFile);
                   cat.setDisplayName(indexName);
                   catList.add(cat);
                }
             }
             categories.setCategory(catList);
             categoriesList.add(categories);
          }
       }
       return categoriesList;
    }
    
}