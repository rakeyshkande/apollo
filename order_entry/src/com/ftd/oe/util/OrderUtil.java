package com.ftd.oe.util;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.AddonVO;
import com.ftd.oe.vo.CoBrandVO;
import com.ftd.oe.vo.CreditCardVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.PersonalGreetingVO;
import com.ftd.op.common.JccasUtils;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.TaxVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.JAXPUtil;


public class OrderUtil {
	
	public static String TAX_SERVICE_CONTEXT = "TAX_CONFIG";
	public static String DEFAULT_TAX_RATE_PARAM = "DEFAULT_TAX_RATE";

    private static Logger logger = new Logger("com.ftd.oe.util.OrderUtil");
    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private static String customerInsisted = "Customer insisted on address.";

    public static OrderVO parseOrderXML(Document doc) {

        logger.debug("Begin parsing XML doc");
        OrderVO orderVO = new OrderVO();
        ItemTaxVO itemTax = new ItemTaxVO();
        try {
            NodeList nl = doc.getElementsByTagName(OrderEntryConstants.XML_TAG_ORDER);
            if (nl.getLength() > 0) {
                Element rsElement = (Element) nl.item(0);
                NodeList nlOrder = rsElement.getChildNodes();
                for (int i=0; i<nlOrder.getLength(); i++) {
                    Element rec = (Element) nlOrder.item(i);
                    String nodeName = rec.getNodeName();
                    if (nodeName != null && nodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_HEADER)) {
                    	String ccType = null;
                        CustomerVO buyerVO = new CustomerVO();
                        buyerVO.setBuyerRecipientIndicator("B");
                        CreditCardVO ccVO = new CreditCardVO();
                        NodeList nlHeader = rec.getChildNodes();
                        for (int j=0; j<nlHeader.getLength(); j++) {
                            Element headerDetails = (Element) nlHeader.item(j);
                            String name = headerDetails.getNodeName();
                            String value = "";
                            if (headerDetails.hasChildNodes()) {
                                Node n = headerDetails.getFirstChild();
                                value = n.getNodeValue();
                            }
                            if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_DNIS_ID)) {
                                orderVO.setDnisId(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SOURCE_CODE)) {
                                orderVO.setSourceCode(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_LANGUAGE_ID)) {
								orderVO.setLanguageId(value);
							} else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_MASTER_ORDER_NUMBER)) {
                                orderVO.setMasterOrderNumber(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIGIN)) {
                                orderVO.setOrigin(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_HAS_FREE_SHIPPING)) {
                                orderVO.setBuyerHasFreeShippingFlag(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CREATED_BY)) {
                                orderVO.setOrderCreatedBy(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_UPDATE_ORDER_FLAG)) {
                                 orderVO.setUpdateOrderFlag("Y".equals(value) ? true : false);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_MODIFY_ORDER_FLAG)) {
                                orderVO.setPiModifyOrderFlag("Y".equals(value) ? true : false);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_PARTNER_ID)) {
                            	orderVO.setPiPartnerId(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_MODIFY_ORDER_THRESHOLD)) {
                            	if (value != null && !value.equals("")) {
                                    orderVO.setPiModifyOrderThreshold(new BigDecimal(value));
                            	} else {
                            		orderVO.setPiModifyOrderThreshold(new BigDecimal("0"));
                            	}
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_ORIGINAL_MERC_TOTAL)) {
                            	if (value != null && !value.equals("")) {
                                    orderVO.setPiOriginalMercTotal(new BigDecimal(value));
                            	} else {
                            		orderVO.setPiOriginalMercTotal(new BigDecimal("0"));
                            	}
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_ORIGINAL_PDB_PRICE)) {
                            	if (value != null && !value.equals("")) {
                                    orderVO.setPiOriginalPdbPrice(new BigDecimal(value));
                            	} else {
                            		orderVO.setPiOriginalPdbPrice(new BigDecimal("0"));
                            	}
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PI_ORIGINAL_FLORIST_ID)) {
                            	orderVO.setPiOriginalFloristId(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORDER_COUNT)) {
                                orderVO.setOrderCount(Integer.parseInt(value));
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_COMPANY_ID)) {
                                orderVO.setCompany(value);
                                ccVO.setCreditCardDnisType(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_FIRST_NAME)) {
                                buyerVO.setFirstName(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_LAST_NAME)) {
                                buyerVO.setLastName(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_ADDRESS1)) {
                                value = value.replaceAll("\n", " ");
                                value = value.replaceAll("\r", " ");
                                buyerVO.setAddress(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_CITY)) {
                                buyerVO.setCity(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_STATE)) {
                            	if (value != null && value.equalsIgnoreCase("null")) {
                            		value = null;
                            	}
                                buyerVO.setState(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_ZIP_CODE)) {
                                buyerVO.setZipCode(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_COUNTRY)) {
                                buyerVO.setCountry(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_PRIMARY_PHONE)) {
                                buyerVO.setDayPhone(value);
                            }
                            else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_PRIMARY_EXT)) {
                                buyerVO.setDayPhoneExt(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_SECONDARY_PHONE)) {
                                buyerVO.setEveningPhone(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_SECONDARY_EXT)) {
                                buyerVO.setEveningPhoneExt(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_BUYER_EMAIL_ADDRESS)) {
                                buyerVO.setEmailAddress(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NEWSLETTER_FLAG)) {
                                if (value != null && value.equalsIgnoreCase("on")) {
                                    buyerVO.setNewsletterFlag("Y");
                                } else {
                                    buyerVO.setNewsletterFlag("N");
                                }
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_NUMBER)) {
                                ccVO.setCreditCardNumber(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_TYPE)) {
                            	ccType = value;
                                ccVO.setCreditCardType(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_EXPIRATION)) {
                                ccVO.setCreditCardExpiration(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_AMOUNT)) {
                                ccVO.setApprovalAmount(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_CSC)) {
                                ccVO.setCscValue(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_CSC_OVERRIDE)) {
                                ccVO.setCscOverrideFlag(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_CSC_FAILED_COUNT)) {
                                try {
                                    ccVO.setCscFailedCount(Integer.parseInt(value));
                                } catch (Exception e) {
                                    logger.error("Invalid CSC failure count: " + value);
                                }
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_APPROVAL_CODE)) {
                                ccVO.setApprovalCode(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_APPROVAL_VERBIAGE)) {
                                ccVO.setApprovalVerbiage(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_APPROVAL_ACTION_CODE)) {
                                ccVO.setApprovalActionCode(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_AVS_RESULT)) {
                                ccVO.setAvsResultCode(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_ACQ_DATA)) {
                                ccVO.setAcqReferenceData(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CC_BYPASS_AUTH_FLAG)) {
                                orderVO.setBypassCCAuthFlag("Y".equals(value) ? true : false);
                                /*
                                 * if authorizations is erred out and csr opt to take the order with cc_bypass_auth_flag as Y.
                                 * Stamp payment record with cc auth provider.
                                 */
                                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                          	  	String bamsCCList = cu.getFrpGlobalParm(OrderProcessingConstants.AUTH_CONFIG, "bams.cc.list");
                                if("Y".equalsIgnoreCase(value) && ccType != null && JccasUtils.checkValidBamsCC(ccType, bamsCCList)){
                                	String ccAuthProvider = cu.getFrpGlobalParm("AUTH_CONFIG", "cc.auth.provider");
                                	ccVO.setCcAuthProvider(ccAuthProvider);
                                }
                                
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FRAUD_FLAG)) {
                                orderVO.setFraudFlag(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FRAUD_ID)) {
                                orderVO.setFraudId(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FRAUD_COMMENTS)) {
                                orderVO.setFraudComments(value);
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CO_BRANDS)) {
                                NodeList nlCoBrands = headerDetails.getChildNodes();
                                for (int a=0; a<nlCoBrands.getLength(); a++) {
                                    Element cobrand = (Element) nlCoBrands.item(a);
                                    NodeList thisCoBrand = cobrand.getChildNodes();
                                    String cbName = null;
                                    String cbData = null;
                                    for (int b=0; b<thisCoBrand.getLength(); b++) {
                                        Element coBrandDetails = (Element) thisCoBrand.item(b);
                                        String coBrandName = coBrandDetails.getNodeName();
                                        String coBrandValue = "";
                                        if (coBrandDetails.hasChildNodes()) {
                                            Node nn = coBrandDetails.getFirstChild();
                                            coBrandValue = nn.getNodeValue();
                                        }
                                        if (coBrandName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NAME)) {
                                            cbName = coBrandValue;
                                        } else if (coBrandName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_DATA)) {
                                            cbData = coBrandValue;
                                        }
                                    }
                                    CoBrandVO coBrandVO = new CoBrandVO();
                                    coBrandVO.setCoBrandName(cbName.toUpperCase());
                                    coBrandVO.setCoBrandData(cbData);
                                    orderVO.setCoBrandVO(coBrandVO);
                               }
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_GIFT_CERTIFICATES)) {
                                NodeList nlGift = headerDetails.getChildNodes();
                                for (int a=0; a<nlGift.getLength(); a++) {
                                    Element gift = (Element) nlGift.item(a);
                                    NodeList thisGift = gift.getChildNodes();
                                    for (int b=0; b<thisGift.getLength(); b++) {
                                        Element giftDetails = (Element) thisGift.item(b);
                                        String giftName = giftDetails.getNodeName();
                                        String giftValue = "";
                                        if (giftDetails.hasChildNodes()) {
                                            Node nn = giftDetails.getFirstChild();
                                            giftValue = nn.getNodeValue();
                                        }
                                        if (giftName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NUMBER)) {
                                            ccVO.setGiftCertificateId(giftValue);
                                        } else if (giftName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_AMOUNT)) {
                                            if (giftValue != null) {
                                                try {
                                                    ccVO.setGiftCertificateAmount(new BigDecimal(giftValue));
                                                } catch (Exception e) {
                                                    logger.error("Invalid gift certificate amount: " + giftValue);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NO_CHARGE)) {
                                NodeList nlNoCharge = headerDetails.getChildNodes();
                                for (int b=0; b<nlNoCharge.getLength(); b++) {
                                    Element ncDetails = (Element) nlNoCharge.item(b);
                                    String ncName = ncDetails.getNodeName();
                                    String ncValue = "";
                                    if (ncDetails.hasChildNodes()) {
                                        Node nn = ncDetails.getFirstChild();
                                        ncValue = nn.getNodeValue();
                                    }
                                    if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_APPROVAL_ID)) {
                                        ccVO.setNoChargeApprovalId(ncValue);
                                    } else if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_APPROVAL_PASSWORD)) {
                                        ccVO.setNoChargeApprovalPassword(ncValue);
                                    } else if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_TYPE)) {
                                        ccVO.setNoChargeType(ncValue);
                                    } else if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_REASON)) {
                                        ccVO.setNoChargeReason(ncValue);
                                    } else if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_ORDER_REFERENCE)) {
                                        ccVO.setNoChargeOrderReference(ncValue);
                                    } else if (ncName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_NC_AMOUNT)) {
                                        if (ncValue != null) {
                                            try {
                                                ccVO.setNoChargeAmount(new BigDecimal(ncValue));
                                            } catch (Exception e) {
                                                logger.error("Invalid no-charge amount: " + ncValue);
                                            }
                                        }
                                    }
                                }
                            } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_ORDER_GUID)) {
                            	orderVO.setOriginalOrderGuid(value);
                            } 
                           //#1051 - added Wallet-indicator in ccVO
                            else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_WALLET_INDICATOR)) {                                
                            	ccVO.setWalletIndicator(value);
                            } else {
                                logger.error("Unknown element: " + name + " / " + value);
                            }
                        }
                        orderVO.setBuyerVO(buyerVO);
                        orderVO.setCcVO(ccVO);
                    } else if (nodeName != null && nodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ITEMS)) {
                        NodeList nlItems = rec.getChildNodes();
                        for (int j=0; j<nlItems.getLength(); j++) {
                            OrderDetailVO odVO = new OrderDetailVO();
                            CustomerVO recipientVO = new CustomerVO();
                            recipientVO.setBuyerRecipientIndicator("R");
                            Element item = (Element) nlItems.item(j);
                            NodeList thisItem = item.getChildNodes();
                            for (int k=0; k< thisItem.getLength(); k++) {
                                Element itemDetails = (Element) thisItem.item(k);
                                String name = itemDetails.getNodeName();
                                String value = "";
                                if (itemDetails.hasChildNodes()) {
                                    Node n = itemDetails.getFirstChild();
                                    value = n.getNodeValue();
                                }
                                if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PRODUCT_ID)) {
                                    odVO.setProductId(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CART_INDEX)) {
                                    odVO.setCartIndex(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SHIP_METHOD)) {
                                    odVO.setShipMethod(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SIZE_INDICATOR)) {
                                    odVO.setSizeIndicator(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_EXTERNAL_ORDER_NUMBER)) {
                                    odVO.setExternalOrderNumber(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ITEM_SOURCE_CODE)) {
                                    odVO.setItemSourceCode(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_IOTW_FLAG)) {
                                    odVO.setIotwFlag(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_OCCASION)) {
                                    odVO.setOccasion(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_DELIVERY_DATE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setDeliveryDate(sdf.parse(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid delivery date: " + value);
                                            odVO.setDeliveryDate(null);
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SECOND_DELIVERY_DATE)) {
                                    if (value != null) {
                                        try {
                                            Date delDateRangeEnd = sdf.parse(value);
                                            if (delDateRangeEnd.after(odVO.getDeliveryDate())) {
                                                odVO.setDeliveryDateRangeEnd(delDateRangeEnd);
                                            }
                                        } catch (Exception e) {
                                            logger.error("Invalid delivery date range end: " + value);
                                            odVO.setDeliveryDateRangeEnd(null);
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CARD_MESSAGE)) {
                                    odVO.setCardMessage(URLDecoder.decode(value, "UTF-8"));
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SPECIAL_INSTRUCTIONS)) {
                                    odVO.setSpecialInstructions(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORDER_COMMENTS)) {
                                    if (value != null && !value.equals("")) {
                                        if (odVO.getOrderComments() == null || odVO.getOrderComments().equals("")) {
                                            odVO.setOrderComments(URLDecoder.decode(value, "UTF-8"));
                                        } else {
                                            odVO.setOrderComments(odVO.getOrderComments() + " " + URLDecoder.decode(value, "UTF-8"));
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FLORIST_ID)) {
                                    odVO.setFloristId(value);
                                /*
                                 * COLOR CODES NO LONGER USED FOR ORIGINAL PURPOSE - SO NEXT LINES 
                                 * WERE COMMENTED OUT MARCH 2018. COLOR TABLES HAVE BEEN REPURPOSED AS 
                                 * PRODUCT MARKERS FOR PROJECT FRESH.     
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FIRST_COLOR_CHOICE)) {
                                    odVO.setFirstColorChoice(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SECOND_COLOR_CHOICE)) {
                                    odVO.setSecondColorChoice(value);
                                */
                               } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_PRODUCT_PRICE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setProductPrice(new BigDecimal(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid product price");
                                            odVO.setProductPrice(new BigDecimal(0));
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RETAIL_VARIABLE_PRICE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setRetailVariablePrice(new BigDecimal(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid retail variable price");
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SERVICE_SHIPPING_FEE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setServiceFee(new BigDecimal(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid service/shipping fee");
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_LATE_CUTOFF_FEE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setLateCutOffUpcharge((new BigDecimal(value)));
                                        } catch (Exception e) {
                                            logger.error("Invalid Late Cutoff fee");
                                        }
                                    }
                                }else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_MONDAY_UPCHARGE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setMondayUpcharge((new BigDecimal(value)));
                                        } catch (Exception e) {
                                            logger.error("Invalid Monday Upcharge");
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SUNDAY_UPCHARGE)) {
                                    if (value != null) {
                                        try {
                                            odVO.setSundayUpcharge((new BigDecimal(value)));
                                        } catch (Exception e) {
                                            logger.error("Invalid Sunday Upcharge");
                                        }
                                    }
                                }  else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAX_AMOUNT)) {
                                    if (value != null) {
                                        try {
                                            odVO.setTaxAmount(value);
                                        } catch (Exception e) {
                                            logger.error("Invalid tax amount");
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES)) { //##Start Parsing Taxes##
                                    NodeList nlTaxes = itemDetails.getChildNodes();                                    
                                    for (int a=0; a<nlTaxes.getLength(); a++) {
                                        TaxVO taxVO = new TaxVO();
                                        List<TaxVO> taxvolist = new ArrayList<TaxVO>();
                                        Element taxes = (Element) nlTaxes.item(a);
                                        String taxesName = taxes.getNodeName();
                                        String taxesValue = taxes.getNodeValue();
                                        if(taxesName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX)) {
                                        NodeList thisTax = taxes.getChildNodes();
                                        for (int b=0; b<thisTax.getLength(); b++) {
                                            Element taxDetails = (Element) thisTax.item(b);
                                            String taxNodeName = taxDetails.getNodeName();
                                            String taxValue = "";
                                            if (taxDetails.hasChildNodes()) {
                                                Node nn = taxDetails.getFirstChild();
                                                taxValue = nn.getNodeValue();
                                            }
                                            if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_TYPE)) {
                                            	taxVO.setName(taxValue);
                                            } else if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_DESCRIPTION)) {                                                
                                                taxVO.setDescription(taxValue);                                                    
                                            } else if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_RATE)) {                                                
                                                taxVO.setRate(new BigDecimal(taxValue));                                                    
                                            } else if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_AMOUNT)) {                                                
                                                taxVO.setAmount(new BigDecimal(taxValue)); 
                                            }
                                            taxvolist.add(taxVO);
                                        }
                                        itemTax.setTaxSplit(taxvolist);
                                        } 
                                        if(taxesName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TOTAL_TAX)) {
                                        	NodeList thisTax = taxes.getChildNodes();
                                            for (int b=0; b<thisTax.getLength(); b++) {
                                                Element taxDetails = (Element) thisTax.item(b);
                                                String taxNodeName = taxDetails.getNodeName();
                                                String taxValue = "";
                                                if (taxDetails.hasChildNodes()) {
                                                    Node nn = taxDetails.getFirstChild();
                                                    taxValue = nn.getNodeValue();
                                                }
                                                if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_DESCRIPTION)) {                                                
                                                	itemTax.setTotalTaxDescription(taxValue);                                                    
                                                } else if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_RATE)) {                                                
                                                	itemTax.setTotalTaxrate(new BigDecimal(taxValue));                                                    
                                                } else if (taxNodeName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_AMOUNT)) {                                                
                                                	itemTax.setTaxAmount(taxValue); 
                                                }
                                                
                                        }
                                        } else if (taxesName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TAXES_TAX_SERVICE_PERFORMED)) {                                        	
                                        	boolean taxServicePerformed = (("Y".equalsIgnoreCase(taxesValue) ? true : false));
                                        	odVO.setTaxServicePerformed(taxServicePerformed);
                                        }
                                    }
                                    //##End Parsing Taxes##
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ITEM_TOTAL)) {
                                    if (value != null) {
                                        try {
                                            odVO.setLineItemTotal(new BigDecimal(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid line item total");
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_FIRST_NAME)) {
                                    recipientVO.setFirstName(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_LAST_NAME)) {
                                    recipientVO.setLastName(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_ADDRESS1)) {
                                    value = value.replaceAll("\n", " ");
                                    value = value.replaceAll("\r", " ");
                                    recipientVO.setAddress(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_CITY)) {
                                    recipientVO.setCity(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_STATE)) {
                                    recipientVO.setState(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_ZIP_CODE)) {
                                    recipientVO.setZipCode(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_COUNTRY)) {
                                    recipientVO.setCountry(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_PHONE)) {
                                    recipientVO.setDayPhone(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_PHONE_EXT)) {
                                    recipientVO.setDayPhoneExt(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QMS_ADDRESS1)) {
                                    recipientVO.setQMSAddress(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QMS_CITY)) {
                                    recipientVO.setQMSCity(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QMS_STATE)) {
                                    recipientVO.setQMSState(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QMS_ZIP_CODE)) {
                                    recipientVO.setQMSZipCode(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QMS_RESULT_CODE)) {
                                    recipientVO.setQMSResultCode(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_CUSTOMER_INSISTED)) {
                                    if (value != null && value.equalsIgnoreCase("true") ) {
                                        if (odVO.getOrderComments() == null || odVO.getOrderComments().equals("")) {
                                            odVO.setOrderComments(customerInsisted);
                                        } else {
                                            odVO.setOrderComments(odVO.getOrderComments() + " " + customerInsisted);
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SHIP_TO_TYPE)) {
                                    recipientVO.setAddressType(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SHIP_TO_NAME)) {
                                    recipientVO.setBusinessName(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_SHIP_TO_INFO)) {
                                    recipientVO.setBusinessInfo(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_ROOM)) {
                                    recipientVO.setRecipientRoom(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_HOURS_START)) {
                                    recipientVO.setHoursStart(value);
                                    //odVO.setTimeOfService(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_RECIP_HOURS_END)) {
                                    recipientVO.setHoursEnd(value);
                                }else if(name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_TIME_OF_SERVICE_FLAG)){
                                	logger.info("The value of Time of service Flag is : "+value);
                                	if("Y".equalsIgnoreCase(value)){
                                		
                                		recipientVO.setTimeOfServiceFlag(true);
                                	}
                                	
                                }
                                else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ADDONS)) {
                                    NodeList nlAddons = itemDetails.getChildNodes();
                                    for (int a=0; a<nlAddons.getLength(); a++) {
                                        AddonVO addonVO = new AddonVO();
                                        Element addon = (Element) nlAddons.item(a);
                                        NodeList thisAddon = addon.getChildNodes();
                                        for (int b=0; b<thisAddon.getLength(); b++) {
                                            Element addonDetails = (Element) thisAddon.item(b);
                                            String addonName = addonDetails.getNodeName();
                                            String addonValue = "";
                                            if (addonDetails.hasChildNodes()) {
                                                Node nn = addonDetails.getFirstChild();
                                                addonValue = nn.getNodeValue();
                                            }
                                            if (addonName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ID)) {
                                                addonVO.setAddonId(addonValue);
                                            } else if (addonName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_QUANTITY)) {
                                                if (addonValue != null) {
                                                    try {
                                                        addonVO.setAddonQuantity(Integer.parseInt(addonValue));
                                                    } catch (Exception e) {
                                                        logger.error("Invalid add-on quantity: " + addonValue + ". Defaulting to 1");
                                                        addonVO.setAddonQuantity(1);;
                                                    }
                                                }
                                            } else if (addonName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FUNERAL_BANNER)) {
                                                addonVO.setFuneralBannerText(addonValue);
                                            }
                                        }
                                        odVO.setAddonVO(addonVO);
                                    }
                                //Address Verifiaction     
                                } else if (name.equalsIgnoreCase(GeneralConstants.XML_TAG_ADDRESS_VERIFICATION)){
                                	NodeList avsNodes = itemDetails.getChildNodes();
                                	AVSAddressVO avsAddrVO = new AVSAddressVO();
                                	List<AVSAddressScoreVO> scores = new ArrayList<AVSAddressScoreVO>();
                                	for (int avsIndex=0; avsIndex<avsNodes.getLength(); avsIndex++){
                                		Object avsObj = (Object)avsNodes.item(avsIndex);
                        				Element avsElement = null;
                        				if(avsObj instanceof Element){
                        					avsElement = (Element)avsObj;
                        				}
                        				String avsName = "";
                                		if(avsElement != null){
                                			avsName = avsElement.getNodeName();
                                		}
                                		String avsValue = "";
                                		if (avsElement != null && avsObj instanceof Element && avsElement.hasChildNodes()) {
                                            Node n = avsElement.getFirstChild();
                                            avsValue = n.getNodeValue();
                                        }
                                		if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFIED)){
                                			avsAddrVO.setAvsPerformed(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_RESULT)){
                                			avsAddrVO.setResult(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_BYPASSED)){
                                			avsAddrVO.setOverrideFlag(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_STREET_ADDRESS)){
                                			//This is for not missing Address line 2 after AVS check
                                			String recipAddress[] = recipientVO.getAddress().split(",");
                                			if(recipAddress[0].contains(avsValue)){
                                			
                                				avsAddrVO.setAddress(recipientVO.getAddress());
                                			}else{
                                				avsAddrVO.setAddress(avsValue);
                                			} 
                                			
                                			
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_CITY)){
                                			avsAddrVO.setCity(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_STATE)){
                                			avsAddrVO.setStateProvince(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_POSTAL_CODE)){
                                			avsAddrVO.setPostalCode(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_COUNTRY)){
                                			avsAddrVO.setCountry(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_LONGITUDE)){
                                			avsAddrVO.setLongitude(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_LATITUDE)){
                                			avsAddrVO.setLatitude(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_ENTITY_TYPE)){
                                			avsAddrVO.setEntityType(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_THRESHOLD_VALUE)){
                                			avsAddrVO.setThreshold(avsValue);
                                		}
                                		else if(avsName.equalsIgnoreCase(GeneralConstants.XML_TAG_AVS_ADDRESS_SCORES)){
                                			NodeList scoreNodes = avsElement.getChildNodes();
                                			for(int scoresIdx=0; scoresIdx< scoreNodes.getLength(); scoresIdx++){
                                				Object scoreObj = (Object)scoreNodes.item(scoresIdx);
                                				Element scoreElement = null;
                                				if(scoreObj instanceof Element){
                                					scoreElement = (Element)scoreObj;
                                				}
                                				if(scoreElement != null){
	                                				String scoreName = scoreElement.getNodeName();
	                                				String scoreValue = "";
	                                				if (scoreElement.hasChildNodes()) {
	                                                    Node n = scoreElement.getFirstChild();
	                                                    scoreValue = n.getNodeValue();
	                                                }
	                                				AVSAddressScoreVO score;
	                                				if(scoreName != null && scoreName.trim().length() > 0){
	                                					score = new AVSAddressScoreVO();
	                                					score.setReason(scoreName);
	                                					score.setScore(scoreValue);
	                                					scores.add(score);
	                                				}
                                				}
                                				
                                			}
                                			avsAddrVO.setScores(scores);
                                			
                                		}
                                		
                                	}
                                	odVO.setAvsAddressVO(avsAddrVO);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_EXTERNAL_ORDER_NUMBER)) {
                                	odVO.setOriginalExternalOrderNumber(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_ORDER_DETAIL_ID)) {
                                	odVO.setOriginalOrderDetailId(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_DELIVERY_DATE)) {
                                    if (value != null && !value.equals("")) {
                                        try {
                                        	odVO.setOriginalDeliveryDate(sdf.parse(value));
                                        } catch (Exception e) {
                                            logger.error("Invalid delivery date: " + value);
                                            odVO.setOriginalDeliveryDate(new Date());
                                        }
                                    }
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_SHIP_METHOD)) {
                                	odVO.setOriginalShipMethod(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_RECIPIENT_ID)) {
                                	odVO.setOriginalRecipientId(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_HAS_SDU)) {
                                	odVO.setOriginalHasSDU(value);
                                } else if (name.equalsIgnoreCase(OrderEntryConstants.XML_TAG_ORIG_PRODUCT_ID)) {
                                	odVO.setOriginalProductId(value);
                                }else {
                                    logger.error("Unknown element: " + name + " / " + value);
                                }
                            }
                            odVO.setItemTaxVO(itemTax);
                            odVO.setRecipientVO(recipientVO);
                            orderVO.setOdVO(odVO);
                            orderVO.setOrderCount(orderVO.getOrderCount() + 1);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }

        logger.debug("Done parsing XML doc");
        return orderVO;
    }

    public static Document createOrderXML(OrderVO orderVO) {

    	Document doc = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat sdfLong = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

        try {

            doc = JAXPUtil.createDocument();

            Element order = doc.createElement(OrderEntryConstants.XML_TAG_ORDER);
            doc.appendChild(order);
            
            Element header = doc.createElement(OrderEntryConstants.XML_TAG_HEADER);
            order.appendChild(header);
            
            Element value = doc.createElement(OrderEntryConstants.XML_TAG_MASTER_ORDER_NUMBER);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getMasterOrderNumber()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_DNIS_ID);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getDnisId()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_SOURCE_CODE);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getSourceCode()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_ORIGIN);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getOrigin()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_ORDER_DATE);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(sdfLong.format(orderVO.getOrderDate())));

            value = doc.createElement(OrderEntryConstants.XML_TAG_ORDER_COUNT);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(Integer.toString(orderVO.getOrderCount())));

            value = doc.createElement(OrderEntryConstants.XML_TAG_ORDER_AMOUNT);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getOrderAmount().toString()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_CREATED_BY);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(orderVO.getOrderCreatedBy()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_FRAUD_FLAG);
            header.appendChild(value);
            if (orderVO.getFraudFlag() != null) {
                value.appendChild(doc.createTextNode(orderVO.getFraudFlag()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_FRAUD_ID);
            header.appendChild(value);
            if (orderVO.getFraudId() != null) {
                value.appendChild(doc.createTextNode(orderVO.getFraudId()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_FRAUD_COMMENTS);
            header.appendChild(value);
            if (orderVO.getFraudComments() != null) {
                value.appendChild(doc.createTextNode(orderVO.getFraudComments()));
            }

			value = doc.createElement(OrderEntryConstants.XML_TAG_LANGUAGE_ID);
			header.appendChild(value);
			if(orderVO.getLanguageId() != null) {
				value.appendChild(doc.createTextNode(orderVO.getLanguageId()));
			}
			
			value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_HAS_FREE_SHIPPING);
			header.appendChild(value);
			if(orderVO.getBuyerHasFreeShippingFlag() != null) {
				value.appendChild(doc.createTextNode(orderVO.getBuyerHasFreeShippingFlag()));
			}
			
            CustomerVO buyerVO = orderVO.getBuyerVO();

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_FIRST_NAME);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(buyerVO.getFirstName()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_LAST_NAME);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(buyerVO.getLastName()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_ADDRESS1);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(buyerVO.getAddress()));

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_CITY);
            header.appendChild(value);
            if (buyerVO.getCity() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getCity()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_STATE);
            header.appendChild(value);
            if (buyerVO.getState() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getState()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_ZIP_CODE);
            header.appendChild(value);
            if (buyerVO.getZipCode() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getZipCode()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_COUNTRY);
            header.appendChild(value);
            if (buyerVO.getCountry() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getCountry()));
            }
            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_PRIMARY_PHONE);
            header.appendChild(value);
            if (buyerVO.getDayPhone() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getDayPhone()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_PRIMARY_EXT);
            header.appendChild(value);
            if (buyerVO.getDayPhoneExt() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getDayPhoneExt()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_SECONDARY_PHONE);
            header.appendChild(value);
            if (buyerVO.getEveningPhone() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getEveningPhone()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_SECONDARY_EXT);
            header.appendChild(value);
            if (buyerVO.getEveningPhoneExt() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getEveningPhoneExt()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_SIGNED_IN);
            header.appendChild(value);
            if (buyerVO.getEmailAddress() != null && buyerVO.getEmailAddress().length() > 0) {
                // Buyer is signed in if there is a non-empty email address
                value.appendChild(doc.createTextNode("Y"));
            }
            
            value = doc.createElement(OrderEntryConstants.XML_TAG_BUYER_EMAIL_ADDRESS);
            header.appendChild(value);
            if (buyerVO.getEmailAddress() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getEmailAddress()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_NEWSLETTER_FLAG);
            header.appendChild(value);
            if (buyerVO.getNewsletterFlag() != null) {
                value.appendChild(doc.createTextNode(buyerVO.getNewsletterFlag()));
            }

            CreditCardVO ccVO = orderVO.getCcVO();

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_NUMBER);
            header.appendChild(value);
            if (ccVO.getCreditCardNumber() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCreditCardNumber()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_TYPE);
            header.appendChild(value);
            if (ccVO.getCreditCardType() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCreditCardType()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_EXPIRATION);
            header.appendChild(value);
            if (ccVO.getCreditCardExpiration() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCreditCardExpiration()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_AMOUNT);
            header.appendChild(value);
            if (ccVO.getApprovalAmount() != null && Double.parseDouble(ccVO.getApprovalAmount()) > 0) {
                value.appendChild(doc.createTextNode(ccVO.getApprovalAmount()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_APPROVAL_CODE);
            header.appendChild(value);
            if (ccVO.getApprovalCode() != null) {
                value.appendChild(doc.createTextNode(ccVO.getApprovalCode()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_APPROVAL_VERBIAGE);
            header.appendChild(value);
            if (ccVO.getApprovalVerbiage() != null) {
                value.appendChild(doc.createTextNode(ccVO.getApprovalVerbiage()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_APPROVAL_ACTION_CODE);
            header.appendChild(value);
            if (ccVO.getApprovalActionCode() != null) {
                value.appendChild(doc.createTextNode(ccVO.getApprovalActionCode()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_AVS_RESULT);
            header.appendChild(value);
            if (ccVO.getAvsResultCode() != null) {
                value.appendChild(doc.createTextNode(ccVO.getAvsResultCode()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_ACQ_DATA);
            header.appendChild(value);
            if (ccVO.getAcqReferenceData() != null) {
                value.appendChild(doc.createTextNode(ccVO.getAcqReferenceData()));
            }

            value = doc.createElement(OrderEntryConstants.XML_TAG_AAFES_TICKET_NUMBER);
            header.appendChild(value);
            if (ccVO.getAafesTicketNumber() != null) {
                value.appendChild(doc.createTextNode(ccVO.getAafesTicketNumber()));
            }
            
            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_CSC_VALIDATED_FLAG);
            header.appendChild(value);
            if (ccVO.getCscValidateFlag() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCscValidateFlag()));
            }
            
            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_CSC_RESPONSE_CODE);
            header.appendChild(value);
            if (ccVO.getCscResponseCode() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCscResponseCode()));
            }
            
            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_CSC_FAILED_COUNT);
            header.appendChild(value);
            value.appendChild(doc.createTextNode(String.valueOf(ccVO.getCscFailedCount())));
            
            String membershipId = buyerVO.getMembershipId();
            if (membershipId != null) {
                Element cobrands = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRANDS);
                header.appendChild(cobrands);
                
                Element cobrandFname = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRAND);
                cobrands.appendChild(cobrandFname);
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_NAME);
                cobrandFname.appendChild(value);
                value.appendChild(doc.createTextNode(OrderEntryConstants.XML_TAG_FNAME));

                value = doc.createElement(OrderEntryConstants.XML_TAG_DATA);
                cobrandFname.appendChild(value);
                if (buyerVO.getMembershipFirstName() != null) {
                    value.appendChild(doc.createTextNode(buyerVO.getMembershipFirstName()));
                }

                Element cobrandLname = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRAND);
                cobrands.appendChild(cobrandLname);
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_NAME);
                cobrandLname.appendChild(value);
                value.appendChild(doc.createTextNode(OrderEntryConstants.XML_TAG_LNAME));

                value = doc.createElement(OrderEntryConstants.XML_TAG_DATA);
                cobrandLname.appendChild(value);
                if (buyerVO.getMembershipLastName() != null) {
                    value.appendChild(doc.createTextNode(buyerVO.getMembershipLastName()));
                }

                Element cobrandId = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRAND);
                cobrands.appendChild(cobrandId);
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_NAME);
                cobrandId.appendChild(value);
                if (buyerVO.getMembershipType() != null) {
                    value.appendChild(doc.createTextNode(buyerVO.getMembershipType()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_DATA);
                cobrandId.appendChild(value);
                value.appendChild(doc.createTextNode(membershipId));
            }

            String gcNumber = ccVO.getGiftCertificateId();
            if (gcNumber != null) {
                Element giftCerts = doc.createElement(OrderEntryConstants.XML_TAG_GIFT_CERTIFICATES);
                header.appendChild(giftCerts);
                
                Element giftCert = doc.createElement(OrderEntryConstants.XML_TAG_GIFT_CERTIFICATE);
                giftCerts.appendChild(giftCert);
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_NUMBER);
                giftCert.appendChild(value);
                value.appendChild(doc.createTextNode(gcNumber));

                value = doc.createElement(OrderEntryConstants.XML_TAG_AMOUNT);
                giftCert.appendChild(value);
                value.appendChild(doc.createTextNode(ccVO.getGiftCertificateAmount().toString()));
            }

            String ncReason = ccVO.getNoChargeReason();
            if (ncReason != null) {
                Element noCharge = doc.createElement(OrderEntryConstants.XML_TAG_NO_CHARGE);
                header.appendChild(noCharge);
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_NC_APPROVAL_ID);
                noCharge.appendChild(value);
                if (ccVO.getNoChargeApprovalId() != null) {
                    value.appendChild(doc.createTextNode(ccVO.getNoChargeApprovalId()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_NC_AMOUNT);
                noCharge.appendChild(value);
                value.appendChild(doc.createTextNode(ccVO.getNoChargeAmount().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_NC_TYPE);
                noCharge.appendChild(value);
                if (ccVO.getNoChargeType() != null) {
                    value.appendChild(doc.createTextNode(ccVO.getNoChargeType()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_NC_REASON);
                noCharge.appendChild(value);
                value.appendChild(doc.createTextNode(ncReason));

                value = doc.createElement(OrderEntryConstants.XML_TAG_NC_ORDER_REFERENCE);
                noCharge.appendChild(value);
                if (ccVO.getNoChargeOrderDetailId() != null) {
                    value.appendChild(doc.createTextNode(ccVO.getNoChargeOrderDetailId()));
                }
            }

            List coBrandList = orderVO.getCoBrandVO();
            if (coBrandList.size() > 0) {
                Element cobrands = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRANDS);
                header.appendChild(cobrands);

                for (int j=0; j<coBrandList.size(); j++) {
                    CoBrandVO coBrandVO = (CoBrandVO) coBrandList.get(j);

                    Element cobrand = doc.createElement(OrderEntryConstants.XML_TAG_CO_BRAND);
                    cobrands.appendChild(cobrand);

                    value = doc.createElement(OrderEntryConstants.XML_TAG_NAME);
                    cobrand.appendChild(value);
                    if (coBrandVO.getCoBrandName() != null) {
                        value.appendChild(doc.createTextNode(coBrandVO.getCoBrandName()));
                    }

                    value = doc.createElement(OrderEntryConstants.XML_TAG_DATA);
                    cobrand.appendChild(value);
                    if (coBrandVO.getCoBrandData() != null) {
                        value.appendChild(doc.createTextNode(coBrandVO.getCoBrandData()));
                    }
                }
            }
            
            //Defect#16-BAMS Integration : Additional auth response elements
            
            value = doc.createElement(OrderEntryConstants.XML_TAG_CC_AUTH_PROVIDER);
            header.appendChild(value);
            if (ccVO.getCcAuthProvider() != null) {
                value.appendChild(doc.createTextNode(ccVO.getCcAuthProvider()));
            }

            Map<String,Object> paymentExtMap = ccVO.getPaymentExtMap();
            if(paymentExtMap != null && paymentExtMap.size()>0){
            	String paymentProperty;
                value = doc.createElement(OrderEntryConstants.XML_TAG_TRANSMISSION_DATE_TIME);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.TRANSMISSION_DATE_TIME);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CC_SYSTEM_TRACE_AUDIT_NUMBER);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.STAN);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CC_REFERENCE_NUMBER);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.REFERENCE_NUMBER);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CARDHOLDER_ACTIVATED_TERMINAL);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.TERMINAL_CATEGORY_CODE);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_VISA_POS_CONDITION_CODE);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.POS_CONDITION_CODE);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CC_TRANS_CRNCY);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.TRANSACTION_CURRENCY);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CC_RESPONSE_CODE);
                header.appendChild(value);
                paymentProperty = (String)paymentExtMap.get(PaymentExtensionConstants.RESPONSE_CODE);
                if (paymentProperty != null) {
                    value.appendChild(doc.createTextNode(paymentProperty));
                }
                
                @SuppressWarnings("unchecked")
				Map<String,String> cardSpecificDetailsMap = (Map<String, String>) paymentExtMap.get(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
                if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
                	Element cardSpecificGroup = doc.createElement(OrderEntryConstants.XML_TAG_CARD_SPECIFIC_GROUP);
                	cardSpecificGroup.setAttribute("type", ccVO.getCreditCardType());
                	for(Map.Entry<String, String> entry : cardSpecificDetailsMap.entrySet()){
                		Element cardSpecificDetail = doc.createElement(OrderEntryConstants.XML_TAG_CARD_SPECIFIC_DETAIL);
                		cardSpecificDetail.setAttribute("name", entry.getKey());
                		cardSpecificDetail.setTextContent(entry.getValue());
                		cardSpecificGroup.appendChild(cardSpecificDetail);
                	}
                	header.appendChild(cardSpecificGroup);
                }  
             
            }  
          //#1051 - added Wallet-indicator in xml
            value = doc.createElement(OrderEntryConstants.XML_TAG_WALLET_INDICATOR);
            header.appendChild(value);
            if (ccVO.getWalletIndicator() != null) {
                value.appendChild(doc.createTextNode(ccVO.getWalletIndicator()));
            }
            
            logger.info("Create Order Xml : Wallet Indicator" + ccVO.getWalletIndicator());
            
            Element items = doc.createElement(OrderEntryConstants.XML_TAG_ITEMS);
            order.appendChild(items);
            List detailList = orderVO.getOdVO();
            
            for (int i=0; i<detailList.size(); i++) {
                Element item = doc.createElement(OrderEntryConstants.XML_TAG_ITEM);
                items.appendChild(item);

                OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                String productId = odVO.getProductId();
                logger.debug("createOrderXML: " + productId);

                value = doc.createElement(OrderEntryConstants.XML_TAG_EXTERNAL_ORDER_NUMBER);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getExternalOrderNumber()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_ITEM_SOURCE_CODE);
                item.appendChild(value);
                if (odVO.getItemSourceCode() != null) {
                    value.appendChild(doc.createTextNode(odVO.getItemSourceCode()));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_DELIVERY_DATE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(sdf.format(odVO.getDeliveryDate())));

                value = doc.createElement(OrderEntryConstants.XML_TAG_SECOND_DELIVERY_DATE);
                item.appendChild(value);
                if (odVO.getDeliveryDateRangeEnd() != null) {
                    value.appendChild(doc.createTextNode(sdf.format(odVO.getDeliveryDateRangeEnd())));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_PRODUCT_ID);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(productId));

                value = doc.createElement(OrderEntryConstants.XML_TAG_PRODUCT_SUBCODE_ID);
                item.appendChild(value);
                if (odVO.getProductSubcodeId() != null) {
                    value.appendChild(doc.createTextNode(odVO.getProductSubcodeId()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_FIRST_COLOR_CHOICE);
                item.appendChild(value);
                if (odVO.getFirstColorChoice() != null) {
                    value.appendChild(doc.createTextNode(odVO.getFirstColorChoice()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SECOND_COLOR_CHOICE);
                item.appendChild(value);
                if (odVO.getSecondColorChoice() != null) {
                    value.appendChild(doc.createTextNode(odVO.getSecondColorChoice()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_IOTW_FLAG);
                item.appendChild(value);
                if (odVO.getIotwFlag() != null) {
                    value.appendChild(doc.createTextNode(odVO.getIotwFlag()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_OCCASION);
                item.appendChild(value);
                if (odVO.getOccasion() != null) {
                    value.appendChild(doc.createTextNode(odVO.getOccasion()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SHIP_METHOD);
                item.appendChild(value);
                if (odVO.getShipMethod() != null) {
                    value.appendChild(doc.createTextNode(odVO.getShipMethod()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SIZE_INDICATOR);
                item.appendChild(value);
                if (odVO.getSizeIndicator() != null) {
                    value.appendChild(doc.createTextNode(odVO.getSizeIndicator()));
                }

                //BigDecimal productPrice = odVO.getRetailVariablePrice().add(odVO.getDiscountAmount());
                value = doc.createElement(OrderEntryConstants.XML_TAG_PRODUCT_PRICE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getProductPrice().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_RETAIL_VARIABLE_PRICE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getRetailVariablePrice().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_DISCOUNTED_PRODUCT_PRICE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getRetailVariablePrice().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_DISCOUNT_AMOUNT);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getDiscountAmount().toString()));

/*
                BigDecimal serviceShippingFee = odVO.getServiceFee().add(odVO.getShippingFee());
                value = doc.createElement(OrderEntryConstants.XML_TAG_SERVICE_SHIPPING_FEE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(serviceShippingFee.toString()));
*/
                value = doc.createElement(OrderEntryConstants.XML_TAG_SERVICE_FEE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getServiceFee().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_SERVICE_SHIPPING_FEE);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getShippingFee().toString()));
                
                if(odVO.getLateCutOffUpcharge()!=null){
                	value = doc.createElement(OrderEntryConstants.XML_TAG_LATE_CUTOFF_FEE);
                	item.appendChild(value);
                	value.appendChild(doc.createTextNode(odVO.getLateCutOffUpcharge().toString()));
                }
                
                if(odVO.getMondayUpcharge()!=null){
	                value = doc.createElement(OrderEntryConstants.XML_TAG_MONDAY_UPCHARGE);
	                item.appendChild(value);
	                value.appendChild(doc.createTextNode(odVO.getMondayUpcharge().toString()));
                }
                
                if(odVO.getSundayUpcharge()!=null){
                	value = doc.createElement(OrderEntryConstants.XML_TAG_SUNDAY_UPCHARGE);
                	item.appendChild(value);
                	value.appendChild(doc.createTextNode(odVO.getSundayUpcharge().toString()));
                }
                

                value = doc.createElement(OrderEntryConstants.XML_TAG_TAX_AMOUNT);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getTaxAmount()));
                
                /* Order XML Tax nodes */
                Element taxes = doc.createElement(OrderEntryConstants.XML_TAG_TAXES);
                item.appendChild(taxes);
                if (!StringUtils.isEmpty(odVO.getTaxAmount()) && odVO.getTaxAmount().compareTo("0") > 0) {//For a non taxable product - No tax tags
	                if (odVO.getItemTaxVO() != null) {
		                List taxList = odVO.getItemTaxVO().getTaxSplit();                                  
		                if (!"Surcharge*".equalsIgnoreCase(odVO.getItemTaxVO().getTotalTaxDescription())) {
			                if (taxList.size() > 0) {
			                    for (int j=0; j<taxList.size(); j++) {
			                    	TaxVO itvo = (TaxVO) taxList.get(j);
			                    	if(itvo.getAmount().compareTo(BigDecimal.ZERO) > 0){
			                        Element tax = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX);
			                        taxes.appendChild(tax);
			
			                        value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_TYPE);
			                        tax.appendChild(value);                        
			                        value.appendChild(doc.createTextNode(itvo.getName()));
			                        
			                        value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_DESCRIPTION);
			                        tax.appendChild(value);                        
			                        value.appendChild(doc.createTextNode(itvo.getDescription()));
			                        
			                        value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_RATE);
			                        tax.appendChild(value);                        
			                        value.appendChild(doc.createTextNode(String.valueOf(itvo.getRate())));
			                        
			                        value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_AMOUNT);
			                        tax.appendChild(value);
			                        value.appendChild(doc.createTextNode(String.valueOf(itvo.getAmount())));
			                    	}
			                    }
			                }
		                }
	                }
	                Element totalTaxes = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TOTAL_TAX);
	                taxes.appendChild(totalTaxes);
	                
	                value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_RATE);
	                totalTaxes.appendChild(value);
	                String defaultTaxRate = ConfigurationUtil.getInstance().getFrpGlobalParm(TAX_SERVICE_CONTEXT, DEFAULT_TAX_RATE_PARAM);
	                value.appendChild(doc.createTextNode(String.valueOf(odVO.getItemTaxVO() != null ? odVO.getItemTaxVO().getTotalTaxrate() : defaultTaxRate)));
	                
	                value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_AMOUNT);
	                totalTaxes.appendChild(value);
	                value.appendChild(doc.createTextNode((odVO.getTaxAmount())));
	                
	                value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_DESCRIPTION);
	                totalTaxes.appendChild(value);
	                value.appendChild(doc.createTextNode(odVO.getItemTaxVO().getTotalTaxDescription() != null ? odVO.getItemTaxVO().getTotalTaxDescription() : "Taxes"));
	                
	                value = doc.createElement(OrderEntryConstants.XML_TAG_TAXES_TAX_SERVICE_PERFORMED);
	                taxes.appendChild(value);
	                boolean isTaxServicePerformed = odVO.isTaxServicePerformed();
	                value.appendChild(doc.createTextNode(isTaxServicePerformed ? "Y" : "N"));
                }
                /*End Order XML Tax nodes */
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_ADDON_AMOUNT);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getAddonAmount().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_ITEM_TOTAL);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(odVO.getLineItemTotal().toString()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_MILES_POINTS);
                item.appendChild(value);
                if (odVO.getMilesPoints() != null) {
                    value.appendChild(doc.createTextNode(odVO.getMilesPoints().toString()));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_PERSONAL_GREETING_ID);
                item.appendChild(value);
                if (odVO.getPersonalGreetingVO() != null && odVO.getPersonalGreetingVO().getPersonalGreetingId() != null) {
                    value.appendChild(doc.createTextNode(odVO.getPersonalGreetingVO().getPersonalGreetingId()));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_BIN_SOURCE_CHANGED_FLAG);
                item.appendChild(value);
                if (odVO.getBinSourceChangedFlag() != null && odVO.getBinSourceChangedFlag().equalsIgnoreCase("Y")) {
                    value.appendChild(doc.createTextNode(odVO.getBinSourceChangedFlag()));
                }

                CustomerVO recipVO = odVO.getRecipientVO();

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_FIRST_NAME);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(recipVO.getFirstName()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_LAST_NAME);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(recipVO.getLastName()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_ADDRESS1);
                item.appendChild(value);
                value.appendChild(doc.createTextNode(recipVO.getAddress()));

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_CITY);
                item.appendChild(value);
                if (recipVO.getCity() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getCity()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_STATE);
                item.appendChild(value);
                if (recipVO.getState() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getState()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_ZIP_CODE);
                item.appendChild(value);
                if (recipVO.getZipCode() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getZipCode()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_COUNTRY);
                item.appendChild(value);
                if (recipVO.getCountry() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getCountry()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_PHONE);
                item.appendChild(value);
                if (recipVO.getDayPhone() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getDayPhone()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_RECIP_PHONE_EXT);
                item.appendChild(value);
                if (recipVO.getDayPhoneExt() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getDayPhoneExt()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_QMS_RESULT_CODE);
                item.appendChild(value);
                if (recipVO.getQMSResultCode() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getQMSResultCode()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SHIP_TO_TYPE);
                item.appendChild(value);
                if (recipVO.getAddressType() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getAddressType()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SHIP_TO_NAME);
                item.appendChild(value);
                if (recipVO.getBusinessName() != null) {
                    value.appendChild(doc.createTextNode(recipVO.getBusinessName()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_TIME_OF_SERVICE);
                item.appendChild(value);
                if (odVO.getTimeOfService() != null) {
                    value.appendChild(doc.createTextNode(odVO.getTimeOfService()));
                }
                
                value = doc.createElement(OrderEntryConstants.XML_TAG_CARD_MESSAGE);
                item.appendChild(value);
                if (odVO.getCardMessage() != null) {
                    value.appendChild(doc.createTextNode(odVO.getCardMessage()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_SPECIAL_INSTRUCTIONS);
                item.appendChild(value);
                if (odVO.getSpecialInstructions() != null) {
                    value.appendChild(doc.createTextNode(odVO.getSpecialInstructions()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_ORDER_COMMENTS);
                item.appendChild(value);
                if (odVO.getOrderComments() != null) {
                    value.appendChild(doc.createTextNode(odVO.getOrderComments()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_FLORIST_ID);
                item.appendChild(value);
                if (odVO.getFloristId() != null) {
                    value.appendChild(doc.createTextNode(odVO.getFloristId()));
                }

                value = doc.createElement(OrderEntryConstants.XML_TAG_ORIGINAL_ORDER_HAS_SDU);
                item.appendChild(value);
                if (odVO.getOriginalHasSDU() != null) {
                    value.appendChild(doc.createTextNode(odVO.getOriginalHasSDU()));
                }

                List addonList = odVO.getAddonVO();
                if (addonList.size() > 0) {
                    Element addons = doc.createElement(OrderEntryConstants.XML_TAG_ADDONS);
                    item.appendChild(addons);

                    for (int j=0; j<addonList.size(); j++) {
                        AddonVO addonVO = (AddonVO) addonList.get(j);

                        Element addon = doc.createElement(OrderEntryConstants.XML_TAG_ADDON);
                        addons.appendChild(addon);

                        value = doc.createElement(OrderEntryConstants.XML_TAG_ID);
                        addon.appendChild(value);
                        if (addonVO.getAddonId() != null) {
                            value.appendChild(doc.createTextNode(addonVO.getAddonId()));
                        }

                        value = doc.createElement(OrderEntryConstants.XML_TAG_QUANTITY);
                        addon.appendChild(value);
                        value.appendChild(doc.createTextNode(Integer.toString(addonVO.getAddonQuantity())));

                        value = doc.createElement(OrderEntryConstants.XML_TAG_FUNERAL_BANNER);
                        addon.appendChild(value);
                        if (addonVO.getFuneralBannerText() != null) {
                            value.appendChild(doc.createTextNode(addonVO.getFuneralBannerText()));
                        }
                    }
                }
                
                //Create AVS xml part
                AVSAddressVO avsAddressVO = odVO.getAvsAddressVO();
                if(avsAddressVO != null){
                	List<AVSAddressScoreVO> avsScores = avsAddressVO.getScores();
                	Element avs_tag = doc.createElement(GeneralConstants.XML_TAG_ADDRESS_VERIFICATION);
                	item.appendChild(avs_tag);
                	
                	Element avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFIED);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getAvsPerformed() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getAvsPerformed()));
                	}
                	
                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_RESULT);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getResult() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getResult()));	
                	}
                	
                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_RECIP_ADDRESS_VERIFICATION_BYPASSED);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getOverrideflag() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getOverrideflag()));
                	}
                	            	
                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_STREET_ADDRESS);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getAddress() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getAddress()));	
                	}

                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_CITY);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getCity() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getCity()));	
                	}
                	
                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_STATE);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getStateProvince() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getStateProvince()));	
                	}

                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_POSTAL_CODE);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getPostalCode() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getPostalCode()));	
                	}

                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_COUNTRY);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getCountry() != null) {
                    	avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getCountry()));                		
                	}

                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_LONGITUDE);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getLongitude() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getLongitude()));	
                	}
                	
                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_LATITUDE);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getLatitude() != null) {
                		avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getLatitude()));	
                	}

                	avs_addr_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_ENTITY_TYPE);
                	avs_tag.appendChild(avs_addr_tag);
                	if (avsAddressVO.getEntityType() != null) {
                    	avs_addr_tag.appendChild(doc.createTextNode(avsAddressVO.getEntityType()));                		
                	}
                	
                	Element scores_tag;
                	Element score_tag;
                	scores_tag = doc.createElement(GeneralConstants.XML_TAG_AVS_ADDRESS_SCORES);
                	avs_tag.appendChild(scores_tag);
                	for(AVSAddressScoreVO avsScore:avsScores){
                		
                		
                		score_tag = doc.createElement(avsScore.getReason());
                		scores_tag.appendChild(score_tag);
                		score_tag.appendChild(doc.createTextNode(avsScore.getScore()));
                	}
                	
                	
                }
                
            }
            

        } catch (Exception e) {
            logger.error(e);
        }
        
        return doc;
    }

    public static OrderVO getOrderFromDB(Connection qConn, String masterOrderNumber) {
        logger.debug("getOrderFromDB(" + masterOrderNumber + ")");

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        OrderVO orderVO = new OrderVO();
        CachedResultSet cr = null;
        OrderEntryDAO orderEntryDAO = new OrderEntryDAO();

        try {

            cr = orderEntryDAO.getJoeOrders(qConn, masterOrderNumber);
            if (cr.next()) {
                orderVO.setBuyerId(cr.getString("buyer_id"));
                orderVO.setDnisId(cr.getString("dnis_id"));
                orderVO.setSourceCode(cr.getString("source_code"));
                orderVO.setOrigin(cr.getString("origin_id"));
                String orderDate = cr.getString("order_date");
                try {
                    orderVO.setOrderDate(sdf.parse(orderDate));
                } catch (Exception e) {
                    logger.error("Invalid order date: " + orderDate);
                    orderVO.setOrderDate(new Date());
                }
                orderVO.setOrderCreatedBy(cr.getString("order_taken_by_identity_id"));
                orderVO.setOrderCount(cr.getInt("item_cnt"));
                orderVO.setOrderAmount(cr.getBigDecimal("order_amt"));
                orderVO.setFraudId(cr.getString("fraud_id"));
                orderVO.setFraudComments(cr.getString("fraud_cmnt"));
                orderVO.setCompany(cr.getString("company_id"));
				orderVO.setLanguageId(cr.getString("language_id"));
                if (orderVO.getFraudId() == null) {
                    orderVO.setFraudFlag("N");
                } else {
                    orderVO.setFraudFlag("Y");
                }
                orderVO.setBuyerHasFreeShippingFlag(cr.getString("buyer_has_free_shipping"));
                
                Map paymentMap = orderEntryDAO.getJoeOrderPayments(qConn, masterOrderNumber);
                CreditCardVO ccVO = new CreditCardVO();
                cr = (CachedResultSet) paymentMap.get("OUT_CUR");
                if (cr.next()) {                    
                    ccVO.setCreditCardType(cr.getString("payment_method_id"));
                    ccVO.setCreditCardNumber(cr.getString("cc_number"));
                    ccVO.setCreditCardExpiration(cr.getString("cc_expiration"));
                    ccVO.setCscResponseCode(cr.getString("csc_response_code"));
                    ccVO.setCscFailedCount(cr.getInt("csc_failure_cnt"));
                    ccVO.setCscValidateFlag(cr.getString("csc_validated_flag"));
                    ccVO.setApprovalAmount(cr.getString("auth_amt"));
                    ccVO.setApprovalCode(cr.getString("approval_code"));
                    ccVO.setApprovalVerbiage(cr.getString("approval_verbiage"));
                    ccVO.setApprovalActionCode(cr.getString("approval_action_code"));
                    ccVO.setAcqReferenceData(cr.getString("acq_reference_number"));
                    ccVO.setAvsResultCode(cr.getString("avs_result_code"));
                    ccVO.setGiftCertificateId(cr.getString("gc_coupon_number"));
                    ccVO.setGiftCertificateAmount(cr.getBigDecimal("gc_amt"));
                    ccVO.setAafesTicketNumber(cr.getString("aafes_ticket_number"));
                    ccVO.setNoChargeApprovalId(cr.getString("nc_approval_identity_id"));
                    ccVO.setNoChargeType(cr.getString("nc_type_code"));
                    ccVO.setNoChargeReason(cr.getString("nc_reason_code"));
                    ccVO.setNoChargeOrderDetailId(cr.getString("nc_order_detail_id"));
                    ccVO.setNoChargeAmount(cr.getBigDecimal("nc_amt"));
                    ccVO.setCcAuthProvider(cr.getString("cc_auth_provider"));
                    ccVO.setWalletIndicator(cr.getString("wallet_indicator"));
                    
                    orderVO.setCcVO(ccVO);
                }
                
                cr = (CachedResultSet) paymentMap.get("PAYMENT_EXT_CUR");
                Map<String,Object> paymentExtMap = new HashMap<String,Object>();
                while(cr.next()){
                	paymentExtMap.put(cr.getString("auth_property_name"), cr.getString("auth_property_value"));
                }
                if(paymentExtMap!=null && paymentExtMap.size()>0){
                	String cardSpecificGroup = (String)paymentExtMap.remove(PaymentExtensionConstants.CARD_SPECIFIC_GROUP);
                	if(cardSpecificGroup!=null && !cardSpecificGroup.isEmpty()){
                		 Map<String,Object> cardSpecificDetailsMap = FieldUtils.getMapFromDelimitedString(cardSpecificGroup,":::","///");
                		 paymentExtMap.put(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL, cardSpecificDetailsMap);
                	}
                	ccVO.getPaymentExtMap().putAll(paymentExtMap);
                }
                
                cr = orderEntryDAO.getJoeOrderBillingInfo(qConn, masterOrderNumber);
                while (cr.next()) {
                    CoBrandVO cbVO = new CoBrandVO();
                    cbVO.setCoBrandName(cr.getString("billing_info_name"));
                    cbVO.setCoBrandData(cr.getString("billing_info_data"));
                    orderVO.setCoBrandVO(cbVO);
                }
                
                cr = orderEntryDAO.getJoeOrderCustomer(qConn, orderVO.getBuyerId());
                if (cr.next()) {
                    CustomerVO buyerVO = new CustomerVO();
                    buyerVO.setFirstName(cr.getString("first_name"));
                    buyerVO.setLastName(cr.getString("last_name"));
                    buyerVO.setAddress(cr.getString("address"));
                    buyerVO.setCity(cr.getString("city"));
                    buyerVO.setState(cr.getString("state"));
                    buyerVO.setZipCode(cr.getString("zip_code"));
                    buyerVO.setCountry(cr.getString("country"));
                    buyerVO.setBuyerRecipientIndicator(cr.getString("buyer_recipient_ind"));
                    buyerVO.setDayPhone(cr.getString("daytime_phone"));
                    buyerVO.setDayPhoneExt(cr.getString("daytime_phone_ext"));
                    buyerVO.setEveningPhone(cr.getString("evening_phone"));
                    buyerVO.setEveningPhoneExt(cr.getString("evening_phone_ext"));
                    buyerVO.setEmailAddress(cr.getString("email_address"));
                    buyerVO.setNewsletterFlag(cr.getString("newsletter_flag"));
                    buyerVO.setMembershipType(cr.getString("program_name"));
                    buyerVO.setMembershipId(cr.getString("membership_id"));
                    buyerVO.setMembershipFirstName(cr.getString("membership_first_name"));
                    buyerVO.setMembershipLastName(cr.getString("membership_last_name"));
                    
                    orderVO.setBuyerVO(buyerVO);
                }
                
                cr = orderEntryDAO.getJoeOrderDetails(qConn, masterOrderNumber);                                
                while (cr.next()) {
                    String externalOrderNumber = cr.getString("external_order_number");
                    String recipientId = cr.getString("recipient_id");
                    logger.debug("externalOrderNumber: " + externalOrderNumber);
                    
                    OrderDetailVO odVO = new OrderDetailVO();                    
                    odVO.setExternalOrderNumber(externalOrderNumber);
                    odVO.setItemSourceCode(cr.getString("source_code"));
                    odVO.setDeliveryDate(cr.getDate("delivery_date"));
                    odVO.setRecipientId(recipientId);
                    odVO.setProductId(cr.getString("product_id"));
                    odVO.setProductSubcodeId(cr.getString("product_subcode_id"));
                    /*
                     * COLOR CODES NO LONGER USED FOR ORIGINAL PURPOSE - SO NEXT TWO LINES 
                     * WERE COMMENTED OUT MARCH 2018. COLOR TABLES HAVE BEEN REPURPOSED AS 
                     * PRODUCT MARKERS FOR PROJECT FRESH. 
                    odVO.setFirstColorChoice(cr.getString("first_color_choice"));
                    odVO.setSecondColorChoice(cr.getString("second_color_choice"));
                    */
                    odVO.setIotwFlag(cr.getString("iotw_flag"));
                    odVO.setOccasion(cr.getString("occasion_id"));
                    odVO.setCardMessage(cr.getString("card_message_signature"));
                    odVO.setOrderComments(cr.getString("order_cmnts"));
                    odVO.setSpecialInstructions(cr.getString("florist_cmnts"));
                    odVO.setFloristId(cr.getString("florist_id"));
                    odVO.setShipMethod(cr.getString("ship_method_id"));
                    odVO.setDeliveryDateRangeEnd(cr.getDate("delivery_date_range_end"));
                    odVO.setSizeIndicator(cr.getString("size_ind"));
                    odVO.setProductPrice(cr.getBigDecimal("product_amt"));
                    odVO.setAddonAmount(cr.getBigDecimal("add_on_amt"));
                    odVO.setShippingFee(cr.getBigDecimal("shipping_fee_amt"));
                    odVO.setServiceFee(cr.getBigDecimal("service_fee_amt"));
                    odVO.setDiscountAmount(cr.getBigDecimal("discount_amt"));
                    odVO.setTaxAmount(cr.getString("tax_amt"));
                    odVO.setLineItemTotal(cr.getBigDecimal("item_total_amt"));
                    odVO.setMilesPoints(cr.getBigDecimal("miles_points_qty"));
                    PersonalGreetingVO pgVO = new PersonalGreetingVO();
                    pgVO.setPersonalGreetingId(cr.getString("personal_greeting_id"));
                    odVO.setPersonalGreetingVO(pgVO);
                    odVO.setRetailVariablePrice(odVO.getProductPrice());
                    String timeOfService = cr.getString("time_of_service");
                    odVO.setTimeOfService(timeOfService);
                    logger.info("Time of service: " + timeOfService);
                    
                    //setting latecutoff and upcharge fee
                    odVO.setLateCutOffUpcharge(cr.getBigDecimal("late_cutoff_fee"));
                    odVO.setMondayUpcharge(cr.getBigDecimal("vendor_mon_upcharge"));
                    odVO.setSundayUpcharge(cr.getBigDecimal("vendor_sun_upcharge"));
                    logger.info("Sunday Upcharge: " + cr.getBigDecimal("vendor_sun_upcharge"));
                    
                    if (odVO.getDiscountAmount() != null) {
                        odVO.setProductPrice(odVO.getProductPrice().add(odVO.getDiscountAmount()));
                    }
                    odVO.setBinSourceChangedFlag(cr.getString("bin_source_changed_flag"));
                    /*Retrieving Taxes*/
                    List taxvoslist = new ArrayList();
                    TaxVO taxVO;
                    //BigDecimal totalRate = new BigDecimal(0);
                    String t1n = cr.getString("tax1_name");
                    String t2n = cr.getString("tax2_name");
                    String t3n = cr.getString("tax3_name");
                    String t4n = cr.getString("tax4_name");
                    String t5n = cr.getString("tax5_name");
                    if(t1n != null){
                    	taxVO = new TaxVO();
                    	taxVO.setName(t1n);
                    	taxVO.setDescription(cr.getString("tax1_description"));
                    	taxVO.setRate(cr.getBigDecimal("tax1_rate"));
                        taxVO.setAmount(cr.getBigDecimal("tax1_amount"));
                        //totalRate = totalRate.add(cr.getBigDecimal("tax1_rate"));
                    	taxvoslist.add(taxVO);
                    }
                    if(t2n != null){
                    	taxVO = new TaxVO();
                    	taxVO.setName(t2n);
                    	taxVO.setDescription(cr.getString("tax2_description"));
                    	taxVO.setRate(cr.getBigDecimal("tax2_rate"));
                        taxVO.setAmount(cr.getBigDecimal("tax2_amount"));
                        //totalRate = totalRate.add(cr.getBigDecimal("tax2_rate"));
                    	taxvoslist.add(taxVO);
                    }
                    if(t3n != null){
                    	taxVO = new TaxVO();
                    	taxVO.setName(t3n);
                    	taxVO.setDescription(cr.getString("tax3_description"));
                    	taxVO.setRate(cr.getBigDecimal("tax3_rate"));
                        taxVO.setAmount(cr.getBigDecimal("tax3_amount"));
                       // totalRate = totalRate.add(cr.getBigDecimal("tax3_rate"));
                    	taxvoslist.add(taxVO);
                    }
                    if(t4n != null){
                    	taxVO = new TaxVO();
                    	taxVO.setName(t4n);
                    	taxVO.setDescription(cr.getString("tax4_description"));
                    	taxVO.setRate(cr.getBigDecimal("tax4_rate"));
                        taxVO.setAmount(cr.getBigDecimal("tax4_amount"));
                      //  totalRate = totalRate.add(cr.getBigDecimal("tax4_rate"));
                    	taxvoslist.add(taxVO);
                    }
                    if(t5n != null){
                    	taxVO = new TaxVO();
                    	taxVO.setName(t5n);
                    	taxVO.setDescription(cr.getString("tax5_description"));
                    	taxVO.setRate(cr.getBigDecimal("tax5_rate"));
                        taxVO.setAmount(cr.getBigDecimal("tax5_amount"));
                       // totalRate = totalRate.add(cr.getBigDecimal("tax5_rate"));
                    	taxvoslist.add(taxVO);
                    }
                    
                    ItemTaxVO itemTaxVO = new ItemTaxVO();
                    itemTaxVO.setTaxAmount(cr.getString("tax_amt"));
                    itemTaxVO.setTotalTaxrate(cr.getBigDecimal("total_tax_rate"));
                    itemTaxVO.setTaxSplit(taxvoslist);
                    itemTaxVO.setTotalTaxDescription(cr.getString("tax_total_description"));
                    odVO.setItemTaxVO(itemTaxVO);
                    boolean taxServicePerformed = ("Y".equalsIgnoreCase(cr.getString("tax_service_performed")) ? true : false );
                    odVO.setTaxServicePerformed(taxServicePerformed);
                    /*!Retrieving Taxes*/
                    odVO.setOriginalHasSDU(cr.getString("original_order_has_sdu"));
                    CachedResultSet recipCr = orderEntryDAO.getJoeOrderCustomer(qConn, recipientId);
                    if (recipCr.next()) {
                        CustomerVO recipVO = new CustomerVO();
                        recipVO.setFirstName(recipCr.getString("first_name"));
                        recipVO.setLastName(recipCr.getString("last_name"));
                        recipVO.setAddress(recipCr.getString("address"));
                        recipVO.setCity(recipCr.getString("city"));
                        recipVO.setState(recipCr.getString("state"));
                        recipVO.setZipCode(recipCr.getString("zip_code"));
                        recipVO.setCountry(recipCr.getString("country"));
                        recipVO.setBuyerRecipientIndicator(recipCr.getString("buyer_recipient_ind"));
                        recipVO.setDayPhone(recipCr.getString("daytime_phone"));
                        recipVO.setDayPhoneExt(recipCr.getString("daytime_phone_ext"));
                        recipVO.setEveningPhone(recipCr.getString("evening_phone"));
                        recipVO.setEveningPhoneExt(recipCr.getString("evening_phone_ext"));
                        recipVO.setQMSResultCode(recipCr.getString("qms_response_code"));
                        recipVO.setAddressType(recipCr.getString("address_type"));
                        recipVO.setBusinessName(recipCr.getString("business_name"));
                        recipVO.setBusinessInfo(recipCr.getString("business_info"));
                        
                        odVO.setRecipientVO(recipVO);
                    }
                    
                    CachedResultSet addonCr = orderEntryDAO.getJoeOrderAddons(qConn, externalOrderNumber);
                    while (addonCr.next()) {
                        AddonVO addonVO = new AddonVO();
                        addonVO.setAddonId(addonCr.getString("add_on_code"));
                        addonVO.setAddonQuantity(addonCr.getInt("add_on_qty"));
                        addonVO.setFuneralBannerText(addonCr.getString("funeral_banner_txt"));
                        
                        odVO.setAddonVO(addonVO);
                    }
                    
                    //get avs address details
                    CachedResultSet avsAddressCr = orderEntryDAO.getAvsAddress(qConn, externalOrderNumber);
                    AVSAddressVO avsAddress = null;
                    while (avsAddressCr.next()){
                    	avsAddress = new AVSAddressVO();
                    	avsAddress.setAddress(avsAddressCr.getString("address"));
                    	avsAddress.setAvsPerformed(avsAddressCr.getString("avs_performed"));
                    	avsAddress.setCity(avsAddressCr.getString("city"));
                    	avsAddress.setCountry(avsAddressCr.getString("country"));
                    	avsAddress.setEntityType(avsAddressCr.getString("entity_type"));
                    	avsAddress.setLatitude(avsAddressCr.getString("latitude"));
                    	avsAddress.setLongitude(avsAddressCr.getString("longitude"));
                    	avsAddress.setOverrideFlag(avsAddressCr.getString("override_flag"));
                    	avsAddress.setPostalCode(avsAddressCr.getString("zip"));
                    	avsAddress.setResult(avsAddressCr.getString("avs_result"));
                    	avsAddress.setStateProvince(avsAddressCr.getString("state"));
                    	
                    }
                    
                    //get avs address scores
                    CachedResultSet avsAddressScoreCr = orderEntryDAO.getAvsAddressScores(qConn, externalOrderNumber);
                    List<AVSAddressScoreVO> avsScores = new ArrayList<AVSAddressScoreVO>();
                    AVSAddressScoreVO avsScore;
                    while(avsAddressScoreCr.next()){
                    	avsScore = new AVSAddressScoreVO();
                    	avsScore.setReason(avsAddressScoreCr.getString("score_reason"));
                    	avsScore.setScore(avsAddressScoreCr.getString("score"));
                    	avsScores.add(avsScore);
                    	
                    }
                    if(avsAddress != null)
                    	avsAddress.setScores(avsScores);
                    
                    odVO.setAvsAddressVO(avsAddress);
                    orderVO.setOdVO(odVO);                                        
                }

                orderVO.setMasterOrderNumber(masterOrderNumber);
                
            } else {
                logger.error("Could not retrieve order from database");
            }

        } catch (Exception e) {
            logger.error(e);
        }

        return orderVO;
    }

    public static boolean sendOrderToOrderGatherer(Document doc) {
        logger.debug("sendOrderToOrderGatherer()");

        boolean success = false;

        try {

            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String orderGathererURL = cu.getFrpGlobalParm(OrderEntryConstants.JOE_CONTEXT,
                OrderEntryConstants.ORDER_GATHERER_URL);
            logger.debug("orderGathererURL: " + orderGathererURL);

            Document tempDoc = JAXPUtil.createDocument();
            Node newNode = tempDoc.importNode(doc.getFirstChild(), true);
            tempDoc.appendChild(newNode);
            Element element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_NUMBER).item(0);
            if (element != null) element.getParentNode().removeChild(element);
            element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_NC_APPROVAL_PASSWORD).item(0);
            if (element != null) element.getParentNode().removeChild(element);
            element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_EXPIRATION).item(0);
            if (element != null) element.getParentNode().removeChild(element);
            element = (Element) tempDoc.getElementsByTagName(OrderEntryConstants.XML_TAG_CC_CSC).item(0);
            if (element != null) element.getParentNode().removeChild(element);
            logger.debug(JAXPUtil.toString(tempDoc));

            String strXml = "";

            if (doc != null) {
                //Convert the document to a string
                Source source = new DOMSource(doc);
                StringWriter out = new StringWriter();
                Result result = new StreamResult(out);
                TransformerFactory tFactory = TransformerFactory.newInstance();
                Transformer transformer = tFactory.newTransformer();
                Properties props = new Properties();
                props.setProperty(OutputKeys.INDENT, "yes");
                props.setProperty(OutputKeys.ENCODING, "ISO-8859-1");
                props.setProperty(OutputKeys.MEDIA_TYPE, "text/xml");
                props.setProperty(OutputKeys.METHOD, "xml");
                props.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                props.setProperty(OutputKeys.CDATA_SECTION_ELEMENTS,
                    OrderEntryConstants.XML_TAG_BUYER_FIRST_NAME + " " +
                    OrderEntryConstants.XML_TAG_BUYER_LAST_NAME + " " +
                    OrderEntryConstants.XML_TAG_BUYER_ADDRESS1 + " " +
                    OrderEntryConstants.XML_TAG_BUYER_CITY + " " +
                    OrderEntryConstants.XML_TAG_BUYER_EMAIL_ADDRESS + " " +
                    OrderEntryConstants.XML_TAG_DATA + " " +
                    OrderEntryConstants.XML_TAG_NAME + " " +
                    OrderEntryConstants.XML_TAG_RECIP_FIRST_NAME + " " +
                    OrderEntryConstants.XML_TAG_RECIP_LAST_NAME + " " +
                    OrderEntryConstants.XML_TAG_RECIP_ADDRESS1 + " " +
                    OrderEntryConstants.XML_TAG_RECIP_CITY + " " +
                    OrderEntryConstants.XML_TAG_SHIP_TO_NAME + " " +
                    OrderEntryConstants.XML_TAG_CARD_MESSAGE + " " +
                    OrderEntryConstants.XML_TAG_SPECIAL_INSTRUCTIONS + " " +
                    OrderEntryConstants.XML_TAG_ORDER_COMMENTS + " " +
                    OrderEntryConstants.XML_TAG_FRAUD_COMMENTS);
                transformer.setOutputProperties(props);
                transformer.transform(source, result);
                strXml = out.toString();
            }

			
            String response;
            int result;
            
            PostMethod post = new PostMethod(orderGathererURL);
            NameValuePair nvPair = null;
            NameValuePair[] nvPairArray = new NameValuePair[1];
            String name, value;
            
            name = "Order";
            value = strXml;
            nvPair = new NameValuePair(name, value);
            nvPairArray[0] = nvPair;

            post.setRequestBody(nvPairArray);
            
            HttpClient httpclient = new HttpClient();
            // Execute request
            try {
                result = httpclient.executeMethod(post);
                response = post.getResponseBodyAsString();
            } finally {
                // Release current connection to the connection pool once you are done
                post.releaseConnection();
            }
            String message = "Http Response Code is " + result;
            logger.debug(message);
            
            if (result == 200) {
                success = true;
            }

        } catch (Exception e) {
            logger.error(e);
            success = false;
        }

        return success;

    }

    public static void dispatchJMSMessage(InitialContext context, MessageToken token) throws Exception
    {
         // post JMS message to JOE_ORDER_DISPATCHER

         logger.debug("MESSAGE : " + (String)token.getMessage());
         logger.debug("STATUS : " + (String)token.getStatus());
    
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(context, token);

         logger.debug("Message sent");
     }

    public static void dispatchJMSMessageWithProperty(InitialContext context, MessageToken token,
        String propertyName, String propertyValue) throws Exception
    {
         // post JMS message to JOE_ORDER_DISPATCHER

         logger.debug("MESSAGE : " + (String)token.getMessage());
         logger.debug("STATUS : " + (String)token.getStatus());
    
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessageWithProperty(context, token, propertyName, propertyValue);

         logger.debug("Message sent");
     }

//    public static void calculateOrderTotals(Connection qConn, OrderVO orderVO, OrderEntryDAO orderEntryDAO, boolean frontEndFlag) throws Exception {
//
//        logger.debug("calculateOrderTotals()");
//
//        try {
//
//            BigDecimal orderTotal = new BigDecimal(0);
//            BigDecimal totalProductPrice = new BigDecimal(0);
//            BigDecimal totalDiscounts = new BigDecimal(0);
//            BigDecimal totalMilesPoints = new BigDecimal(0);
//            BigDecimal totalAddons = new BigDecimal(0);
//            BigDecimal totalServiceFee = new BigDecimal(0);
//            BigDecimal totalShippingFee = new BigDecimal(0);
//            BigDecimal totalSalesTax = new BigDecimal(0);
//            BigDecimal totalFuelSurchargeFee =  new BigDecimal(0);
//            //String fuelSurchargeDescription = "";
//          
//            int totalItems = 0;
//        
//            int SCALE = 2;
//            int ROUNDING_DOWN = BigDecimal.ROUND_DOWN;
//            int ROUNDING_UP = BigDecimal.ROUND_HALF_UP;
//
//            String sourceCode = orderVO.getSourceCode();
//
//            Calendar cal = null;
//            if (orderVO.getOrderDate() != null)
//            {
//              cal = Calendar.getInstance();
//              cal.setTime(orderVO.getOrderDate());
//            }
//
//            SurchargeVO surchargeVO = FTDFuelSurchargeUtilities.getSurchargeVO(cal, sourceCode, qConn);
//            logger.debug("sourceCode being passed to fuelSurchargeUtility : " + sourceCode);
//           
//            List detailList = orderVO.getOdVO();
//            for (int i=0; i<detailList.size(); i++) {
//                OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
//
//                if (odVO.getProductPrice() == null) {
//                    odVO.setProductPrice(new BigDecimal(0));
//                }
//                BigDecimal itemDiscountPrice = odVO.getProductPrice();
//                BigDecimal itemTotal = new BigDecimal(0);
//                BigDecimal itemDiscountAmount = new BigDecimal(0);
//                BigDecimal itemMilesPoints = new BigDecimal(0);
//                BigDecimal itemAddons = new BigDecimal(0);
//                BigDecimal itemServiceFee = new BigDecimal(0);
//                BigDecimal itemShippingFee = new BigDecimal(0);
//                BigDecimal itemSalesTax = new BigDecimal(0);
//
//                totalItems += 1;
//
//                String productId = odVO.getProductId();
//
//                String itemSourceCode = odVO.getItemSourceCode();
//                if (itemSourceCode == null || itemSourceCode.equals("")) {
//                    itemSourceCode = sourceCode;
//                }
//
//                Date deliveryDate = odVO.getDeliveryDate();
//                //If delivery date hasn't been entered yet, use today's date
//                if (deliveryDate == null) {
//                    deliveryDate = new Date();
//                }
//
//                //Check for IOTW promotion
//                odVO.setIotwFlag("N");
//                CachedResultSet cr = orderEntryDAO.getJoeIOTW(qConn, itemSourceCode, productId);
//                if (cr.next()) {
//                    String iotwId = cr.getString("iotw_id");
//                    String iotwSourceCode = cr.getString("iotw_source_code");
//                    
//                    cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
//                    boolean foundDate = true;
//                    while (cr.next()) {
//                        Date discountDate = cr.getDate("discount_date");
//                        if (discountDate.equals(deliveryDate)) {
//                            foundDate = true;
//                            break;
//                        }
//                        foundDate = false;
//                    }
//                    
//                    if (foundDate) {
//                        logger.debug("Found IOTW promotion: " + iotwSourceCode);
//                        odVO.setItemSourceCode(iotwSourceCode);
//                        odVO.setIotwFlag("Y");
//                        itemSourceCode = iotwSourceCode;
//                    }
//                }
//
//                cr = orderEntryDAO.getSourceCodeById(qConn, itemSourceCode);
//                String priceHeaderId = null;
//                String snhId = null;
//                String sourceDiscountAllowedFlag = null;
//                String iotwFlag = null;
//                String partnerId = null;
//                if (cr.next()) {
//                    priceHeaderId = cr.getString("price_header_id");
//                    snhId = cr.getString("snh_id");
//                    sourceDiscountAllowedFlag = cr.getString("discount_allowed_flag");
//                    iotwFlag = cr.getString("iotw_flag");
//                    partnerId = cr.getString("partner_id");
//
//                    Document productDoc = orderEntryDAO.getProductDetailsAjax(qConn, productId, itemSourceCode);
//                    ProductVO prodVO = ProductUtil.parseProductXML(productDoc);
//                    prodVO.setProductPrice(odVO.getProductPrice());
//
//                    String productType = prodVO.getProductType();
//                    String shipMethod = odVO.getShipMethod();
//                    if (productType.equalsIgnoreCase("SDG") && (shipMethod == null || shipMethod.equals(""))) {
//                        shipMethod = "SD";
//                    }
//                    logger.debug("shipMethod: " + shipMethod);
//                    logger.debug("productType: " + productType);
//                    
//                    BigDecimal fuelSurcharge = BigDecimal.ZERO ;
//                    fuelSurcharge = new BigDecimal(String.valueOf(surchargeVO.getSurchargeAmount()));// converting to string first becuase BigDecimal doesn't scale double directly and gives a big number
//                    odVO.setSurchargeAmount(fuelSurcharge);
//
//                    if (surchargeVO.isApplySurchargeFlag())
//                    {
//                      totalFuelSurchargeFee = totalFuelSurchargeFee.add(fuelSurcharge);
//                    }
//                    logger.debug("fuelSurcharge: " + fuelSurcharge.toString());
//                    logger.debug("totalFuelSurchargeFee: " + totalFuelSurchargeFee.toString());
//                    
//                    
//                    FTDServiceChargeUtility ftdServiceChargeUtility = new FTDServiceChargeUtility();
//                    //We pass in current date as the order date to the getServiceChargeData method
//                    ServiceChargeVO serviceChargeVO = ftdServiceChargeUtility.getServiceChargeData(new Date(), itemSourceCode, deliveryDate, qConn);
//
//                    CustomerVO recipVO = odVO.getRecipientVO();
//                    String recipientState = recipVO.getState();
//                    if (shipMethod == null || shipMethod.equals("")) {
//                        logger.debug("We are in florist delivered logic to determine charges");
//                        //This will grab all florist delivered items except the SDG ones that had a shipMethod set above
//                        String countryId = recipVO.getCountry();
//                        String countryType = null;
//                        cr = orderEntryDAO.getCountryMasterById(qConn, countryId);
//                        if (cr.next()) {
//                            countryType = cr.getString("oe_country_type");
//                        }
//                        logger.debug("countryType: " + countryType);
//
//                        cr = orderEntryDAO.getSNHById(qConn, snhId, sdf.format(deliveryDate));
//                        if (cr.next()) {
//                            if (countryType == null || countryType.equalsIgnoreCase("D")) {
//                                itemServiceFee = serviceChargeVO.getDomesticCharge();
//                                logger.debug("domestic service fee: " + itemServiceFee.toString());
//                            } else {
//                                itemServiceFee = serviceChargeVO.getInternationalCharge();
//                                logger.debug("international service fee: " + itemServiceFee.toString());
//                            }
//                            if (frontEndFlag) {
//                                itemServiceFee = itemServiceFee.add(fuelSurcharge);
//                            } else {
//                                itemShippingFee = itemShippingFee.add(fuelSurcharge);
//                            }
//                        }
//                    } else {
//                        logger.debug("Order is Vendor delivered or is a Florist Delivered SDG");
//                        // Order is Vendor delivered or is a Florist Delivered SDG
//                        if (productType != null && (productType.equalsIgnoreCase("FRECUT") || productType.equalsIgnoreCase("SDFC"))) {
//                            logger.debug("Order is Vendor delivered FRECUT or SDFC");
//                            BigDecimal freshCutServiceFee = serviceChargeVO.getVendorCharge();
//
//                            itemServiceFee = freshCutServiceFee;
//                            logger.debug("itemServiceFee: " + itemServiceFee.toString());
//                    
//                            cal = Calendar.getInstance();
//                            cal.setTime(deliveryDate);
//                            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
//                                  BigDecimal saturdayFee = serviceChargeVO.getVendorSatUpcharge();
//                                  logger.debug("Saturday fresh cut fee: " + saturdayFee.toString());
//                                  itemShippingFee = itemShippingFee.add(saturdayFee);
//                            }
//                            
//                            if (frontEndFlag) {
//                                itemServiceFee = itemServiceFee.add(fuelSurcharge);
//                            } else {
//                                if (shipMethod.equalsIgnoreCase("SD")) {
//                                    //We again suspect this is never the case since this is in the vendor logic
//                                    itemShippingFee = itemShippingFee.add(fuelSurcharge);
//                                } else {
//                                    itemServiceFee = itemServiceFee.add(fuelSurcharge);
//                                }
//                            }
//
//                        } else {
//                            String shippingKey = prodVO.getShippingKey();
//                            double itemPrice = odVO.getProductPrice().doubleValue();
//
//                            cr = orderEntryDAO.getShippingCost(qConn, shippingKey, shipMethod, itemPrice);
//                            if (cr.next()) {
//                                itemShippingFee = new BigDecimal(cr.getDouble("shipping_cost")).setScale(SCALE, ROUNDING_UP);
//                                logger.debug("itemShippingFee: " + itemShippingFee.toString());
//                            }
//                            
//                            if (productType != null && productType.equalsIgnoreCase("SDG") && shipMethod != null && shipMethod.equalsIgnoreCase("SD") && !frontEndFlag) {
//                                ConfigurationUtil cu = ConfigurationUtil.getInstance();
//                                String tempGlobalParm = cu.getFrpGlobalParm(OrderEntryConstants.PRICE_CALCULATION_CONTEXT, OrderEntryConstants.SDG_GLOBAL_SERVICE_FEE);
//                                if (tempGlobalParm != null && !tempGlobalParm.equals("")) {
//                                    try {
//                                        Double serviceFee = Double.parseDouble(tempGlobalParm);
//                                        logger.debug("SDG service fee: " + serviceFee.toString());
//                                        itemServiceFee = new BigDecimal(serviceFee);
//                                        itemShippingFee = itemShippingFee.subtract(new BigDecimal(serviceFee));
//                                        if (itemShippingFee.floatValue() < 0) {
//                                            itemShippingFee = new BigDecimal(0);
//                                        }
//                                    } catch (Exception e) {
//                                        logger.error("Invalid SDG service fee: " + tempGlobalParm);
//                                    }
//                                }
//                            }
//
//                            itemShippingFee = itemShippingFee.add(fuelSurcharge);
//
//                        }
//
//                        if (recipientState != null && (recipientState.equalsIgnoreCase("AK") || recipientState.equalsIgnoreCase("HI"))
//                            && shipMethod != null && !shipMethod.equalsIgnoreCase("SD")) {
//
//                            ConfigurationUtil cu = ConfigurationUtil.getInstance();
//                            String tempGlobalParm = cu.getFrpGlobalParm(OrderEntryConstants.FTD_APPS_CONTEXT, OrderEntryConstants.ALASKA_HAWAII_CHARGE);
//                            if (tempGlobalParm != null && !tempGlobalParm.equals("")) {
//                                try {
//                                    Double alaskaHawaiiFee = Double.parseDouble(tempGlobalParm);
//                                    itemShippingFee = itemShippingFee.add(new BigDecimal(alaskaHawaiiFee));
//                                    logger.debug("Alaska/Hawaii charge: " + alaskaHawaiiFee.toString());
//                                    odVO.setAlaskaHawaiiFee(new BigDecimal(alaskaHawaiiFee).setScale(SCALE, ROUNDING_UP));
//                                } catch (Exception e) {
//                                    logger.error("Invalid Alaska/Hawaii fee: " + tempGlobalParm);
//                                }
//                            }
//                            
//                        }
//                    }
//
//                    if (sourceDiscountAllowedFlag == null) sourceDiscountAllowedFlag = "N";
//                    String discountAllowedFlag = prodVO.getDiscountAllowedFlag();
//                    if (sourceDiscountAllowedFlag.equalsIgnoreCase("Y") && discountAllowedFlag.equalsIgnoreCase("N")) {
//                        discountAllowedFlag = "Y";
//                    }
//
//                    if (!priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
//                        ProductUtil.calcDiscount(qConn, priceHeaderId, prodVO, discountAllowedFlag, false);
//                        itemDiscountPrice = prodVO.getDiscountedPrice();
//                        itemDiscountAmount = prodVO.getDiscountAmount();
//                        odVO.setMilesPoints(new BigDecimal(0));
//                    }
//
//                    List addonList = odVO.getAddonVO();
//                    for (int j=0; j<addonList.size(); j++) {
//                        AddonVO addonVO = (AddonVO) addonList.get(j);
//                        String addonId = addonVO.getAddonId();
//                        int quantity = addonVO.getAddonQuantity();
//
//                        cr = orderEntryDAO.getAddonById(qConn, addonId);
//                        cr.next();
//
//                        BigDecimal addonPrice = new BigDecimal(cr.getDouble("price"));
//
//                        itemAddons = itemAddons.add(addonPrice.multiply(new BigDecimal(quantity)));
//                    }
//
//                    itemDiscountPrice = itemDiscountPrice.setScale(SCALE, ROUNDING_UP);
//                    odVO.setRetailVariablePrice(itemDiscountPrice);
//                    itemDiscountAmount = itemDiscountAmount.setScale(SCALE, ROUNDING_DOWN);
//                    odVO.setDiscountAmount(itemDiscountAmount);
//                    itemAddons = itemAddons.setScale(SCALE, ROUNDING_UP);
//                    odVO.setAddonAmount(itemAddons);
//                    itemServiceFee = itemServiceFee.setScale(SCALE, ROUNDING_UP);
//                    odVO.setServiceFee(itemServiceFee);
//                    itemShippingFee = itemShippingFee.setScale(SCALE, ROUNDING_UP);
//                    odVO.setShippingFee(itemShippingFee);
//
//                    itemTotal = itemTotal.add(itemDiscountPrice);
//                    itemTotal = itemTotal.add(itemAddons);
//                    itemTotal = itemTotal.add(itemServiceFee);
//                    itemTotal = itemTotal.add(itemShippingFee);
//
//                    String noTaxFlag = prodVO.getNoTaxFlag();
//                    if (noTaxFlag == null || noTaxFlag.equalsIgnoreCase("N")) {
//
//                        cr = orderEntryDAO.getStateDetails(qConn, recipientState);
//                        cr.next();
//
//                        BigDecimal taxRate = new BigDecimal(cr.getDouble("taxRate"));
//                        if (taxRate != null && taxRate.intValue() > 0) {
//                            logger.debug("taxRate: " + taxRate.toString());
//                            itemSalesTax = itemTotal.multiply(taxRate);
//                            itemSalesTax = itemSalesTax.divide(new BigDecimal(100), SCALE, ROUNDING_DOWN);
//                            itemTotal = itemTotal.add(itemSalesTax);
//                        }
//                    }
//                    itemTotal = itemTotal.setScale(SCALE, ROUNDING_UP);
//
//                    odVO.setTaxAmount(itemSalesTax);
//                    odVO.setLineItemTotal(itemTotal);
//
//                    if (priceHeaderId.equalsIgnoreCase(OrderEntryConstants.MILES_POINTS_PRICE_CODE)) {
//                        MembershipUtil.calcMilesPoints(qConn, odVO, null);
//                        itemMilesPoints = odVO.getMilesPoints();
//                    }
//
//                    totalProductPrice = totalProductPrice.add(itemDiscountPrice);
//                    totalDiscounts = totalDiscounts.add(itemDiscountAmount);
//                    totalAddons = totalAddons.add(itemAddons);
//                    totalServiceFee = totalServiceFee.add(itemServiceFee);
//                    totalShippingFee = totalShippingFee.add(itemShippingFee);
//                    totalSalesTax = totalSalesTax.add(itemSalesTax);
//                    totalMilesPoints = totalMilesPoints.add(itemMilesPoints);
//                    orderTotal = orderTotal.add(itemTotal);
//
//                } else {
//                    logger.error("Invalid source code: " + itemSourceCode);
//                }
//
//            }
//
//            orderVO.setProductAmount(totalProductPrice);
//            orderVO.setOrderAmount(orderTotal);
//            orderVO.setDiscountAmount(totalDiscounts);
//            orderVO.setMilesPoints(totalMilesPoints);
//            orderVO.setAddonAmount(totalAddons);
//            orderVO.setServiceFeeAmount(totalServiceFee);
//            orderVO.setShippingFeeAmount(totalShippingFee);
//            orderVO.setSalesTaxAmount(totalSalesTax);
//            orderVO.setOrderCount(totalItems);
//            orderVO.setSurchargeFee(totalFuelSurchargeFee);
//            orderVO.setApplySurchargeCode(surchargeVO.getApplySurchargeCode());
//            orderVO.setSurchargeDescription(surchargeVO.getSurchargeDescription());
//            logger.debug("fuelSurchargeDescription: " + surchargeVO.getSurchargeDescription());
//            
//        } catch (Throwable t) {
//            logger.error("Error in calculateOrderTotals()",t);
//            throw new Exception("Could not calculate order totals.");
//        }
//
//    }
 
    /**
   * Formats a string dollar amount to the #,###,##0.99 format.
   * @param amount
   * @return String
   * @throws java.lang.Exception
   */
  public static String formatAmount(String amount) throws Exception
  {
      DecimalFormat myFormatter = new DecimalFormat("#,###,##0.00");
      double doubleAmount = 0;
      String output = "0.00";
      
      try 
      {
          doubleAmount = Double.valueOf(amount.trim()).doubleValue();
          output = myFormatter.format(doubleAmount);
         
      } catch (NumberFormatException nfe) {
         
          // ignore. return initialized value.
          logger.error(nfe);
      } catch (IllegalArgumentException iae) {
          // ignore. return initialized value.
          logger.error(iae);
      }
      return output;
  }    
}
