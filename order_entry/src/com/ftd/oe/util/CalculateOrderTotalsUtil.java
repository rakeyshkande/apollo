package com.ftd.oe.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.AddonVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.ItemTaxVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This utility Performs Calculations on Order Totals.
 * It utilizes the Recalculation Logic in the common utilities module.
 */
public class CalculateOrderTotalsUtil
{

	private static Logger logger = new Logger(CalculateOrderTotalsUtil.class.getName());

	
  public CalculateOrderTotalsUtil()
  {
  }

  /**
   * Invokes RecalculateOrderBO to perform Order Totals Calculations
   * @param qConn
   * @param orderVO
   * @param orderEntryDAO
   */
  public void calculateOrderTotals(Connection qConn, OrderVO orderVO, OrderEntryDAO orderEntryDAO, boolean calculateTaxFlag)
    throws Exception
  {
	  boolean camsVerified;
    try
    {
      logger.debug("calculateOrderTotals");    
      Date currentDate = new Date(); // used if Delivery Date is not entered.
      
      // Prepare the Order Fields/Information
      prepareOrderForCalculation(qConn, orderVO, orderEntryDAO, currentDate);

      // Convert to the Utilities Value Objects
      com.ftd.osp.utilities.order.vo.OrderVO utilitiesOrderVO = convertToUtilitiesOrderVO(orderVO, currentDate);
      
      // Invoke Order Calculation.
      RecalculateOrderBO recalculateOrderBO = new RecalculateOrderBO();
      recalculateOrderBO.setFrontEndExceptionMode(true);
      recalculateOrderBO.setCalculateTaxFlag(calculateTaxFlag);
      
      try
      {
        recalculateOrderBO.recalculate(qConn, utilitiesOrderVO, false);
        camsVerified = utilitiesOrderVO.getBuyerCamsVerified();
      } catch (Exception e)
      {
        logger.error("Error during recalculation, not all items in cart may have been calculated: " + e.getMessage(), e);
      }
      
      // Copy the Results Back
      retrieveCalculatedResults(orderVO, utilitiesOrderVO);

      orderVO.setBuyerCamsVerified(utilitiesOrderVO.getBuyerCamsVerified());
      logger.info("75: utilitiesOrderVO.getBuyerCamsVerified()" + utilitiesOrderVO.getBuyerCamsVerified());
      logger.info("75: orderVO.getBuyerCamsVerified()" + orderVO.getBuyerCamsVerified());      
      
      logger.debug("completed calculateOrderTotals");

    }
    catch (Throwable t)
    {
      logger.error("Failed to perform Order Calculation: " + t.getMessage(), t);
      throw new Exception("Failed to perform Order Calculation: " + t.getMessage(), t);
    }
  }


  /**
   * Prepares the Values in the Order and its Details. Fills in any missing values, checks for IOTW
   * and sets up the appropriate IOTW source codes.
   * @param qConn
   * @param orderVO
   * @param orderEntryDAO
   */
  private void prepareOrderForCalculation(Connection qConn, OrderVO orderVO, OrderEntryDAO orderEntryDAO, Date currentDate)
  {
    try
    {
      String sourceCode = orderVO.getSourceCode();

      List detailList = orderVO.getOdVO();
      for (int i = 0; i < detailList.size(); i++)
      {
        OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);

        if (odVO.getProductPrice() == null)
        {
          odVO.setProductPrice(new BigDecimal(0));
        }

        String itemSourceCode = odVO.getItemSourceCode();
        if (itemSourceCode == null || itemSourceCode.equals(""))
        {
          odVO.setItemSourceCode(sourceCode);
        }

        Date deliveryDate = odVO.getDeliveryDate();
        //If delivery date hasn't been entered yet, use today's date
        if (deliveryDate == null)
        {
          deliveryDate = currentDate;
        }

        checkOrderDetailIOTW(qConn, odVO, orderEntryDAO, deliveryDate);
      }
    }
    catch (Throwable t)
    {
      logger.error("Error preparing Order for Calculation", t);
      throw new RuntimeException("\"Error preparing Order for Calculation", t);
    }
  }


  private void checkOrderDetailIOTW(Connection qConn, OrderDetailVO odVO, OrderEntryDAO orderEntryDAO, Date deliveryDate)
    throws Exception
  {
    //Check for IOTW promotion
    odVO.setIotwFlag("N");
    CachedResultSet cr = orderEntryDAO.getJoeIOTW(qConn, odVO.getItemSourceCode(), odVO.getProductId());
    if (cr.next())
    {
      String iotwId = cr.getString("iotw_id");
      String iotwSourceCode = cr.getString("iotw_source_code");

      cr = orderEntryDAO.getIOTWDeliveryDiscountDates(qConn, iotwId);
      boolean foundDate = true;
      while (cr.next())
      {
        Date discountDate = cr.getDate("discount_date");
        if (discountDate.equals(deliveryDate))
        {
          foundDate = true;
          break;
        }
        foundDate = false;
      }

      if (foundDate)
      {
        logger.debug("Found IOTW promotion: " + iotwSourceCode);
        odVO.setItemSourceCode(iotwSourceCode);
        odVO.setIotwFlag("Y");
      }
    }
  }

  /**
   * Converts the JOE OrderVO to the Utilities Order VO
   * @param orderVO
   * @return
   */
  private com.ftd.osp.utilities.order.vo.OrderVO convertToUtilitiesOrderVO(OrderVO orderVO, Date currentDate)
  {
    com.ftd.osp.utilities.order.vo.OrderVO retVal = new com.ftd.osp.utilities.order.vo.OrderVO();

    Date orderDate = orderVO.getOrderDate();
    if (orderDate == null)
    {
      orderDate = currentDate;
    }
    retVal.setOrderDate(orderDate);
    retVal.setOrderOrigin(orderVO.getOrigin());
    retVal.setMpRedemptionRateAmt(null);

    List detailList = orderVO.getOdVO();

    retVal.setOrderDetail(new ArrayList());

    for (int i = 0; i < detailList.size(); i++)
    {
      OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
      OrderDetailsVO utilOrderDetailsVO = convertToUtilitiesOrderDetailsVO(odVO, currentDate);

      retVal.getOrderDetail().add(utilOrderDetailsVO);
    }
    
    retVal.setCompanyId(orderVO.getCompany());
    retVal.setBuyerSignedIn(true);
    retVal.setBuyerEmailAddress(orderVO.getBuyerVO().getEmailAddress());
 
    retVal.setBuyerCamsVerified(orderVO.getBuyerCamsVerified());
    
    logger.info("390: orderVO.getBuyerCamsVerified(): " + orderVO.getBuyerCamsVerified());
    
    return retVal;
  }

  /**
   * Converts the JOE OrderDetailVO to the Utilities OrderDetailsVO
   * @param p0
   * @return
   */
  private OrderDetailsVO convertToUtilitiesOrderDetailsVO(OrderDetailVO odVO, Date currentDate)
  {
    OrderDetailsVO retVal = new OrderDetailsVO();

    retVal.setSourceCode(odVO.getItemSourceCode());
    retVal.setProductId(odVO.getProductId());
    retVal.setShipMethod(odVO.getShipMethod());
    retVal.setLineNumber(odVO.getCartIndex());

    if(retVal.getShipMethod() == null || retVal.getShipMethod().length() == 0)
    {
      logger.info("Defaulting Null/Empty Ship Method Type to: SD");
      retVal.setShipMethod("SD"); // Default to SD.
    }

    Date deliveryDate = odVO.getDeliveryDate();
    //If delivery date hasn't been entered yet, use today's date
    if (deliveryDate == null)
    {
      deliveryDate = currentDate;
    }

    retVal.setDeliveryDate(deliveryDate);
    
    Date deliveryDateRangeEnd = odVO.getDeliveryDateRangeEnd();
    if (deliveryDateRangeEnd == null){retVal.setDeliveryDateRangeEnd("");}
    else {retVal.setDeliveryDateRangeEnd(deliveryDateRangeEnd);}
    
    if (odVO.getDiscountAmount() != null)
    {
      retVal.setDiscountAmount(odVO.getDiscountAmount().toPlainString());
    }
    else
    {
      retVal.setDiscountAmount("0");
    }

    if (odVO.getProductPrice() != null)
    {
      retVal.setProductsAmount(odVO.getProductPrice().toPlainString());
    }
    else
    {
      retVal.setProductsAmount("0");
    }

    retVal.setSizeChoice(odVO.getSizeIndicator());
    if (retVal.getSizeChoice() == null)
    {
      logger.warn("Size Was Null, defaulting to A");
      retVal.setSizeChoice("A"); // Default to A
    }
    else
    {
      logger.info("Size Choice=" + retVal.getSizeChoice());
    }

    retVal.setRecipients(new ArrayList());
    retVal.getRecipients().add(convertToUtilitiesRecipientsVO(odVO.getRecipientVO()));

    // TBD, shouldn't Recalculate use no Tax from the Product Level?
    // retVal.setNoTaxFlag(odVO);

    if (odVO.getAddonVO() != null)
    {
      List addOnsList = new ArrayList();
      retVal.setAddOns(addOnsList);
      for (int i = 0; i < odVO.getAddonVO().size(); i++)
      {
        addOnsList.add(convertToUtilitiesAddonVO((AddonVO) odVO.getAddonVO().get(i)));
      }
    }
    retVal.setOriginalOrderHasSDU(odVO.getOriginalHasSDU());
    logger.info("originalHasSDU: " + retVal.getOriginalOrderHasSDU());


    return retVal;
  }


  /**
   * Converts the JOE CustomerVO to a Utilities RecipientsVO
   * @param recipient
   * @return
   */
  private RecipientsVO convertToUtilitiesRecipientsVO(CustomerVO recipient)
  {
    RecipientsVO retVal = new RecipientsVO();
    retVal.setRecipientAddresses(new ArrayList());

    // We only care about the Address
    RecipientAddressesVO customerAddress = new RecipientAddressesVO();
    customerAddress.setCity(recipient.getCity());
    customerAddress.setCountry(recipient.getCountry());
    customerAddress.setStateProvince(recipient.getState());
    customerAddress.setPostalCode(recipient.getZipCode());
    customerAddress.setAddressTypeCode(recipient.getAddressType());
    retVal.getRecipientAddresses().add(customerAddress);

    return retVal;
  }


  /**
   * Converts the JOE Addon VO into the Utilities Addon VO
   * @param addonVO
   * @return
   */
  private com.ftd.osp.utilities.order.vo.AddOnsVO convertToUtilitiesAddonVO(AddonVO addonVO)
  {
    com.ftd.osp.utilities.order.vo.AddOnsVO retVal = new com.ftd.osp.utilities.order.vo.AddOnsVO();

    retVal.setAddOnCode(addonVO.getAddonId());
    retVal.setAddOnQuantity(String.valueOf(addonVO.getAddonQuantity()));

    if (addonVO.getAddonPrice() != null)
    {
      retVal.setPrice(addonVO.getAddonPrice().toPlainString());
    }

    return retVal;
  }

  /**
   * This is a helper Method to convert from String to BigDecimal.
   * Null or Blank Strings are returned as the default value.
   * If the default value is not null, the returned value has at least the same number of decimal points as the 
   * default value.
   * 
   * @param value
   * @param defaultValue The default value to return if the value is null. If this is null, null is returned
   *
   * @return String as Decimal or the defaultValue
   */
  private BigDecimal stringToBigDecimal(String value, String defaultValue)
  {
    BigDecimal defaultValueDecimal = null;
    int minimumScale = 0;
    
    if(defaultValue != null)
    {
      defaultValueDecimal = new BigDecimal(defaultValue);
      minimumScale = defaultValueDecimal.scale();      
    }
  
    if (value == null || value.trim().length() == 0)
    {
      return defaultValueDecimal;
    }

    // Make sure we have at least the minimum scale
    BigDecimal retVal = new BigDecimal(value.trim());
    if(retVal.scale() < minimumScale)
    {
      // logger.info("Original Value: " + retVal.toPlainString());
      // Use a scale of minimumScale, this should not result in rounding as we are just adding decimals
      // use Round_Half_Down as that is the mode recalculate uses
      retVal = retVal.setScale(minimumScale, BigDecimal.ROUND_HALF_DOWN);      
      // logger.info("Scaled Value: " + retVal.toPlainString());
    }
    
    return retVal;
  }


  /**
   * Retrieves the calculated results from the Utilities VOs into the JOE VOs
   * @param to
   * @param from
   * @throws IllegalArgumentException If the Details List does not match
   */
  private void retrieveCalculatedResults(OrderVO to, com.ftd.osp.utilities.order.vo.OrderVO from)
  {
    // Retrieve Order Values
    to.setProductAmount(from.getTotalProductAmount());
    to.setOrderAmount(stringToBigDecimal(from.getOrderTotal(), "0.00"));
    to.setDiscountAmount(stringToBigDecimal(from.getDiscountTotal(), "0.00"));
    to.setMilesPoints(from.getTotalPoints());
    to.setAddonAmount(stringToBigDecimal(from.getAddOnAmountTotal(), "0.00"));
    to.setServiceFeeAmount(stringToBigDecimal(from.getServiceFeeTotal(), "0.00"));
    to.setShippingFeeAmount(stringToBigDecimal(from.getShippingFeeTotal(), "0.00"));
    to.setSalesTaxAmount(stringToBigDecimal(from.getTaxTotal(), "0.00"));
    to.setOrderCount(from.getOrderDetail().size());
    to.setApplySurchargeCode(from.getApplySurchargeCode());
    to.setSurchargeFee(stringToBigDecimal(from.getFuelSurchargeFee(), "0.00"));
    to.setSurchargeDescription(from.getFuelSurchargeDescription());
    to.setBuyerHasFreeShippingFlag(from.getBuyerHasFreeShippingFlag());
    to.setServiceFeeTotalSavings(stringToBigDecimal(from.getServiceFeeTotalSavings(), "0.00"));
    to.setShippingFeeTotalSavings(stringToBigDecimal(from.getShippingFeeTotalSavings(), "0.00"));
    to.setBuyerHasPCMembershipFlag(from.getBuyerHasPCMembershipFlag());
    // Retrieve Order Details Values

    // Assumption, the Order Detail Items in to and from are in the same order, i.e. Recalculate is not sorting, which it shouldn't.
    // But will test to ensure
    if (to.getOdVO().size() != from.getOrderDetail().size())
    {
      // If we get here, there is a programming error in that Recalculate is changing/altering the list of OrderDetails that were passed to it.
      throw new IllegalArgumentException("Order Details VO from JOE and from Recalculate Do not have the same detail items");
    }

    for (int i = 0; i < to.getOdVO().size(); i++)
    {
      OrderDetailVO toDetail = (OrderDetailVO) to.getOdVO().get(i);
      OrderDetailsVO fromDetail = (OrderDetailsVO) from.getOrderDetail().get(i);

      retrieveCalculatedResults(toDetail, fromDetail);
    }
  }


  /**
   * Returns null if the passed in amount is 0 or null
   * @param value
   * @return
   */
  private BigDecimal getNullAmountForZero(BigDecimal value)
  {
    if (value == null)
    {
      return value;
    }

    if (value.multiply(new BigDecimal(100)).intValue() == 0)
    {
      return null;
    }

    return value;
  }

  /**
   * Retrieves calculated results from
   * @param toDetail
   * @param fromDetail
   */
  private void retrieveCalculatedResults(OrderDetailVO toDetail, OrderDetailsVO fromDetail)
  {
    toDetail.setAddonAmount(stringToBigDecimal(fromDetail.getAddOnAmount(), "0.00"));
    // Set 0 to Null. Otherwise an Alert is shown to the customer      
    toDetail.setAlaskaHawaiiFee(getNullAmountForZero(stringToBigDecimal(fromDetail.getAlaskaHawaiiSurcharge(), null)));
    toDetail.setDiscountAmount(stringToBigDecimal(fromDetail.getDiscountAmount(), "0.00"));
    toDetail.setLineItemTotal(stringToBigDecimal(fromDetail.getExternalOrderTotal(), "0.00"));
    toDetail.setMilesPoints(stringToBigDecimal(fromDetail.getMilesPoints(), "0"));
    toDetail.setRewardType(fromDetail.getRewardType());
    toDetail.setProductPrice(stringToBigDecimal(fromDetail.getProductsAmount(), "0.00"));
    toDetail.setServiceFee(stringToBigDecimal(fromDetail.getServiceFeeAmount(), "0.00"));
    toDetail.setShippingFee(stringToBigDecimal(fromDetail.getShippingFeeAmount(), "0.00"));
    toDetail.setTaxAmount(fromDetail.getTaxAmount());
    toDetail.setRetailVariablePrice(stringToBigDecimal(fromDetail.getDiscountedProductPrice(), "0.00"));
    toDetail.setSurchargeAmount(stringToBigDecimal(fromDetail.getFuelSurcharge(), "0.00"));   
    toDetail.setItemTaxVO(fromDetail.getItemTaxVO());
    toDetail.setSameDayUpcharge(stringToBigDecimal(fromDetail.getSameDayUpcharge(), "0.00"));
    toDetail.setLateCutOffUpcharge(stringToBigDecimal(fromDetail.getLatecutoffCharge(), "0.00"));
    toDetail.setMondayUpcharge(stringToBigDecimal(fromDetail.getMondayUpcharge(), "0.00"));
    toDetail.setSundayUpcharge(stringToBigDecimal(fromDetail.getSundayUpcharge(), "0.00"));
    
    // #SGC-7 Adding InternationalFee in item subtotal and cart total
    toDetail.setInternationalFee((stringToBigDecimal(fromDetail.getInternationalFloristServiceCharge(), "0.00")));
    toDetail.setOriginalServiceFeeAmount((stringToBigDecimal(fromDetail.getOriginalServiceFeeAmount(), "0.00")));
    
    toDetail.setTaxServicePerformed(fromDetail.isTaxServicePerformed());    
    if(fromDetail.isRecalculateAppliedFreeShipping())
    {
      // Set to null so the Alaska/Hawaii Surcharge Alert is not displayed
      toDetail.setAlaskaHawaiiFee(null); 
    }
  }
}
