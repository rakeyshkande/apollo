package com.ftd.oe.util;

import com.ftd.oe.bo.FTDValidationBO;
import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.AddonVO;
import com.ftd.oe.vo.CoBrandVO;
import com.ftd.oe.vo.CreditCardVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.OrderVO;
import com.ftd.oe.vo.PersonalGreetingVO;
import com.ftd.oe.vo.ProductVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.vo.SympathyItemsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ftdutilities.SympathyConstants;
import com.ftd.ftdutilities.SympathyControls;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.rbs.client.service.impl.RecordableBouquetServiceRemoteImpl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class ValidationUtil {

    private static Logger logger = new Logger("com.ftd.oe.util.ValidationUtil");
    private static OrderEntryDAO orderEntryDAO = new OrderEntryDAO();
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
    private static SimpleDateFormat sdfExpiration = new SimpleDateFormat("MM/yyyy");
    
    public static HashMap validateOrder(Connection qConn, OrderVO orderVO) {

        logger.debug("validateOrder()");
        HashMap errorList = new HashMap();
        boolean foundPersonalGreeting = false;
        CachedResultSet cr = null;

        try {
            
            String paymentMethodId = null;
            String partnerId = null;
            String partnerName = null;
            String iotwFlag = null;
            String membershipDataRequired = "N";
            orderVO.setHasLuxuryItem(false);

            String sourceCode = orderVO.getSourceCode();
            if (sourceCode != null) {
                cr = orderEntryDAO.getSourceCodeById(qConn, sourceCode);
                if (cr.next()) {

                    Date startDate = cr.getDate("start_date");
                    Date endDate = cr.getDate("end_date");
                    Date today = new Date();
                    if (startDate.after(today) || (endDate != null && endDate.before(today))) {
                        errorList.put("source-code", "Source code is not active.");
                        logger.error("Source code is not active.");
                    }

                    paymentMethodId = cr.getString("payment_method_id");
                    partnerId = cr.getString("partner_id");
                    partnerName = cr.getString("partner_name");
                    membershipDataRequired = cr.getString("membership_data_required");
                    iotwFlag = cr.getString("iotw_flag");
                    if (iotwFlag != null && iotwFlag.equalsIgnoreCase("Y")) {
                        errorList.put("source-code", "Cannot use an IOTW source code.");
                        logger.error("Cannot use an IOTW source code: " + sourceCode);
                    }

                } else {
                    errorList.put("source-code", "Source code is not on file.");
                    logger.error("Source code is not on file: " + sourceCode);
                }
            } else {
                errorList.put("source-code", "Source code is required.");
                logger.error("Source code is required.");
            }

            if (errorList.size() > 0) {
                logger.error("Order contains errors, not checking line items.");
                return errorList;
            }

            CustomerVO buyerVO = orderVO.getBuyerVO();
            
            // If this was an Update/Modify order, we want to skip all buyer info validation 
            // since those fields can't be edited (per requirements). Otherwise CSR would be
            // stuck (since no way to correct the non-editable fields).  
            //
            boolean isUpdateOrder = orderVO.isUpdateOrderFlag();
            if (!isUpdateOrder) { 

                String buyerFirstName = buyerVO.getFirstName();
                if (buyerFirstName == null || buyerFirstName.equals("")) {
                    errorList.put("buyer-first-name", "Buyer first name required.");
                    logger.error("Buyer first name required.");
                } else {
                    if (buyerFirstName.length() > 25) {
                        errorList.put("buyer-first-name", "Buyer first name too long.");
                        logger.error("Buyer first name too long.");
                    }
    //                if (!fieldValidation(buyerFirstName, true, true, null)) {
    //                    errorList.put("buyer-first-name", "Invalid characters.");
    //                    logger.error("Invalid characters.");
    //                }
                }
    
                String buyerLastName = buyerVO.getLastName();
                if (buyerLastName == null || buyerLastName.equals("")) {
                    errorList.put("buyer-last-name", "Buyer last name required.");
                    logger.error("Buyer last name required.");
                } else {
                    if (buyerLastName.length() > 25) {
                        errorList.put("buyer-last-name", "Buyer last name too long.");
                        logger.error("Buyer last name too long.");
                    }
    //                if (!fieldValidation(buyerLastName, true, true, ".' ")) {
    //                    errorList.put("buyer-last-name", "Invalid characters.");
    //                    logger.error("Invalid characters.");
    //                }
                }
    
                String buyerAddress = buyerVO.getAddress();
                if (buyerAddress == null || buyerAddress.equals("")) {
                    errorList.put("buyer-address", "Buyer address required.");
                    logger.error("Buyer address required.");
                } else {
                    if (buyerAddress.length() > 90) {
                        errorList.put("buyer-address", "Buyer address too long.");
                        logger.error("Buyer address name too long.");
                    }
    //                if (!fieldValidation(buyerAddress, true, true, ". ")) {
    //                    errorList.put("buyer-address", "Invalid characters.");
    //                    logger.error("Invalid characters.");
    //                }
                }
    
                String buyerCity = buyerVO.getCity();
                if (buyerCity == null || buyerCity.equals("")) {
                    errorList.put("buyer-city", "Buyer city required.");
                    logger.error("Buyer city required.");
                } else {
                    if (buyerCity.length() > 30) {
                        errorList.put("buyer-city", "Buyer city too long.");
                        logger.error("Buyer city too long.");
                    }
                }
    
                String buyerCountry = buyerVO.getCountry();
                boolean isBuyerDomestic = false;
                if (buyerCountry == null || buyerCountry.equals("")) {
                    errorList.put("buyer-country", "Buyer country required.");
                    logger.error("Buyer country required.");
                } else {
                    cr = orderEntryDAO.getCountryMasterById(qConn, buyerCountry);
                    if (cr.next()) {
                        String status = cr.getString("status");
                        if (status == null || !status.equalsIgnoreCase("active")) {
                            errorList.put("buyer-country", "Invalid buyer country.");
                            logger.error("Invalid buyer country.");
                        }
                        String domesticFlag = cr.getString("oe_country_type");
                        if (domesticFlag != null && domesticFlag.equalsIgnoreCase("D")) {
                            isBuyerDomestic = true;
                        }
                    } else {
                        errorList.put("buyer-country", "Invalid buyer country.");
                        logger.error("Invalid buyer country.");
                    }
                }
                
                String buyerState = buyerVO.getState();
                String buyerZipCode = buyerVO.getZipCode();
                if (isBuyerDomestic) {
                    if (buyerState == null || buyerState.equals("")) {
                        errorList.put("buyer-state", "Buyer state required.");
                        logger.error("Buyer state required.");
                    } else {
                        cr = orderEntryDAO.getStateDetails(qConn, buyerState);
                        if (!cr.next()) {
                            errorList.put("buyer-state", "Invalid buyer state.");
                            logger.error("Invalid buyer state.");
                        }
                    }
    
                    if (buyerZipCode == null || buyerZipCode.equals("")) {
                        errorList.put("buyer-zip-code", "Buyer zip code required.");
                        logger.error("Buyer zip code required.");
                    } else {
                        if (buyerZipCode.length() > 6) {
                            errorList.put("buyer-zip-code", "Buyer zip code too long.");
                            logger.error("Buyer zip code too long.");
                        }
                        if (!fieldValidation(buyerZipCode, true, true, null)) {
                            errorList.put("buyer-zip-code", "Invalid characters.");
                            logger.error("Invalid characters.");
                        }
                    }
                } else {
                    if (buyerZipCode != null && !buyerZipCode.equals("")) {
                        errorList.put("buyer-zip-code", "Buyer zip code should be blank.");
                        logger.error("Buyer zip code should be blank.");
                    }
                }
    
                String buyerPhone = buyerVO.getDayPhone();
                if (buyerPhone == null || buyerPhone.equals("")) {
                    errorList.put("buyer-phone", "Buyer phone required.");
                    logger.error("Buyer phone required.");
                } else {
                    if (isBuyerDomestic) {
                        String phoneNumber = buyerPhone.replaceAll( "\\D", "" );
                        if (phoneNumber.length() != 10) {
                            errorList.put("buyer-phone", "Buyer phone must be 10 digits.");
                            logger.error("Buyer phone must be 10 digits.");
                        }
                    } else if (buyerPhone.length() > 20) {
                        errorList.put("buyer-phone", "Buyer phone too long.");
                        logger.error("Buyer phone too long.");
                    }
                    if (!fieldValidation(buyerPhone, true, false, "-/() ")) {
                        errorList.put("buyer-phone", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }
    
                String buyerPhoneExt = buyerVO.getDayPhoneExt();
                if (buyerPhoneExt != null) {
                    if (buyerPhoneExt.length() > 10) {
                        errorList.put("buyer-phone-ext", "Buyer phone extension too long.");
                        logger.error("Buyer phone extension too long.");
                    }
                    if (!fieldValidation(buyerPhoneExt, true, false, "-/()")) {
                        errorList.put("buyer-phone-ext", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }
    
                String buyerEveningPhone = buyerVO.getEveningPhone();
                if (buyerEveningPhone != null && !buyerEveningPhone.equals("")) {
                    if (isBuyerDomestic) {
                        String phoneNumber = buyerEveningPhone.replaceAll( "\\D", "" );
                        if (phoneNumber.length() != 10) {
                            errorList.put("buyer-secondary-phone", "Buyer evening phone must be 10 digits.");
                            logger.error("Buyer evening phone must be 10 digits.");
                        }
                    } else if (buyerEveningPhone.length() > 20) {
                        errorList.put("buyer-secondary-phone", "Buyer evening phone too long.");
                        logger.error("Buyer evening phone too long.");
                    }
                    if (!fieldValidation(buyerEveningPhone, true, false, "-/() ")) {
                        errorList.put("buyer-secondary-phone", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }
    
                String buyerEveningPhoneExt = buyerVO.getEveningPhoneExt();
                if (buyerEveningPhoneExt != null) {
                    if (buyerEveningPhoneExt.length() > 10) {
                        errorList.put("buyer-secondary-phone-ext", "Buyer evening phone extension too long.");
                        logger.error("Buyer evening phone extension too long.");
                    }
                    if (!fieldValidation(buyerEveningPhoneExt, true, false, "-/()")) {
                        errorList.put("buyer-secondary-phone-ext", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }
    
                String buyerEmailAddress = buyerVO.getEmailAddress();
                if (buyerEmailAddress != null && !buyerEmailAddress.equals("")) {
                   if (buyerEmailAddress.length() > 40) {
                        errorList.put("buyer-email-address", "Buyer email address too long.");
                        logger.error("Buyer email address too long.");
                    }
                    if (!fieldValidation(buyerEmailAddress, true, true, "!#@$%&*+-/?^{}|.~'_")) {
                        errorList.put("buyer-email-address", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                    int cnt = StringUtils.countMatches(buyerEmailAddress, "@");
                    if (cnt != 1) {
                        errorList.put("buyer-email-address", "Email address must contain one '@'.");
                        logger.error("Email address must contain one '@'.");
                    }
                } else {
                    String newsletterFlag = buyerVO.getNewsletterFlag();
                    if (newsletterFlag != null && newsletterFlag.equalsIgnoreCase("Y")) {
                        errorList.put("buyer-email-address", "Email address required for opt-in.");
                        logger.error("Email address required for opt-in.");
                    }
                }
                
            } // End !isUpdateOrder
            
            	//Following Membership code placed outside of !isUpdateOrder condition since it can be editable while
            	//JOE Modify Order 
                List cbList = orderVO.getCoBrandVO();
                if (cbList.size() > 0) {
    
                    boolean isMembershipData = false;
                    // This will break if we ever need to validate more than 99 values
                    String[] validationString = new String[99];
    
                    HashMap cbMap = new HashMap();
                    cr = orderEntryDAO.getBillingInfo(qConn, orderVO.getSourceCode());
                    while (cr.next()) {
                        String infoDescription = cr.getString("info_description");
                        String sequence = cr.getString("billing_info_sequence");
                        String validationRegex = cr.getString("validation_regex");
                        String[] values = new String[2];
                        values[0] = validationRegex;
                        values[1] = sequence;
                        cbMap.put(infoDescription.toUpperCase(), values);
                        logger.debug("billingInfo: " + infoDescription + " - " + validationRegex + " - " + sequence);
                    }
                    
                    for (int j=0; j<cbList.size(); j++) {
                        CoBrandVO cbVO = (CoBrandVO) cbList.get(j);
                        String cbName = cbVO.getCoBrandName();
                        String cbData = cbVO.getCoBrandData();
                        logger.debug("co-brand name: " + cbName + " - " + cbData);
    
                        if (cbName != null && cbData != null) {
                            String[] result = (String[]) cbMap.get(cbName);
                            if (result != null) {
                                String validationRegex = result[0];
                                String sequence = result[1];
                                logger.debug("validationRegex: " + validationRegex);
                                logger.debug("sequence: " + sequence);
                                if (validationRegex != null) {
                                    Pattern p = Pattern.compile(validationRegex);
                                    Matcher m = p.matcher(cbData);
                                    logger.debug("matches: " + m.matches());
                                    if (!m.matches()) {
                                        errorList.put("billing-info", "Invalid " + cbName + " value.");
                                        logger.error("Invalid " + cbName + " value.");
                                    }
                                }
                                int thisSeq = Integer.parseInt(sequence);
                                validationString[thisSeq] = cbData;
                            } else {
                                isMembershipData = true;
                                if (cbName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_FNAME)) {
                                    if (cbData == null || cbData.equals("")) cbData = buyerVO.getFirstName();
                                    buyerVO.setMembershipFirstName(cbData);
                                } else if (cbName.equalsIgnoreCase(OrderEntryConstants.XML_TAG_LNAME)) {
                                    if (cbData == null || cbData.equals("")) cbData = buyerVO.getLastName();
                                    buyerVO.setMembershipLastName(cbData);
                                } else {
                                    buyerVO.setMembershipType(cbName);
                                    buyerVO.setMembershipId(cbData);
                                }
                            }
                        }
                    }
    
                    if (isMembershipData) {
                        String membershipId = buyerVO.getMembershipId();
    
                        logger.debug("partnerId: " + partnerId);
                        logger.debug("partnerName: " + partnerName);
                        logger.debug("membershipDataRequired: " + membershipDataRequired);
                        logger.debug("membershipId: " + membershipId);
    
                        if (partnerId != null && membershipDataRequired.equalsIgnoreCase("Y")) {
                            if (membershipId != null && !membershipId.equals("")) {
                                if (!validateMembershipId(qConn, membershipId, partnerId, partnerName)) {
                                    errorList.put("membership-id", "Invalid membership id");
                                    logger.error("Invalid membership id");
                                }
                            }
                        }
                        logger.debug("membershipType: " + buyerVO.getMembershipType());
                        logger.debug("id: " + buyerVO.getMembershipId());
                        logger.debug("first name: " + buyerVO.getMembershipFirstName());
                        logger.debug("last name: " + buyerVO.getMembershipLastName());
                        
                        orderVO.setCoBrandVO(null);
                    } else {
                        if (partnerId != null && (partnerId.equalsIgnoreCase("TARGET") || partnerId.equalsIgnoreCase("ADVO"))) {
                            logger.debug("Validating Cost Center");
                            String newString = "";
                            for (int i=0; i<validationString.length; i++) {
                                if (validationString[i] != null) {
                                    newString = newString.concat(validationString[i]);
                                }
                            }
                            logger.debug("Validation string: <" + newString + ">");
                            
                            if (partnerId.equalsIgnoreCase("TARGET")) {
                                cr = orderEntryDAO.getCostCenterById(qConn, newString, partnerId, sourceCode);
                            } else {
                                cr = orderEntryDAO.getCostCenterById(qConn, newString, partnerId, null);
                            }
    
                            if (!cr.next()) {
                                errorList.put("cost-center", "Invalid cost center information");
                                logger.error("Invalid cost center information");
                            }
                        }
                    }
                }
           

            CreditCardVO ccVO = orderVO.getCcVO();
            String creditCardType = ccVO.getCreditCardType();
            boolean hasNC = false;
            boolean hasGC = false;

            if (ccVO.getGiftCertificateId() != null) {
                hasGC = true;
                String giftCertificateId = ccVO.getGiftCertificateId();
                FTDValidationBO vbo = new FTDValidationBO();
                Document validateGC = vbo.validateGiftCertificate(giftCertificateId);
                Element rowset = JAXPUtil.selectSingleNode(validateGC,"/result/rs[@name=\"gift_certificate\"]");
                String status = rowset.getAttribute("status");
                logger.debug("gift certificate status: " + status);
                if (status == null || !status.equalsIgnoreCase("Y")) {
                    String message = rowset.getAttribute("message");
                    errorList.put("gift_certificate", message);
                    logger.error(message + ": " + giftCertificateId);
                }
            }

            if (ccVO.getNoChargeReason() != null || ccVO.getNoChargeType() != null) {
                hasNC = true;
                if (ccVO.getGiftCertificateAmount() == null) {
                    ccVO.setNoChargeAmount(orderVO.getOrderAmount());
                } else {
                    ccVO.setNoChargeAmount(orderVO.getOrderAmount().subtract(ccVO.getGiftCertificateAmount()));
                }
                logger.debug("No charge amount set to " + ccVO.getNoChargeAmount());
            }

            if (creditCardType != null && !creditCardType.equalsIgnoreCase("IN")) {
                logger.debug("CC Approval amount: " + ccVO.getApprovalAmount());
                BigDecimal newApprovalAmount = new BigDecimal(0);
                BigDecimal orderAmount = orderVO.getOrderAmount();
                logger.debug("orderAmount: " + orderAmount);
                BigDecimal ncAmount = ccVO.getNoChargeAmount();
                logger.debug("ncAmount: " + ncAmount);
                BigDecimal gcAmount = ccVO.getGiftCertificateAmount();
                logger.debug("gcAmount: " + gcAmount);
                if (ncAmount != null && ncAmount.intValue() > 0) {
                    orderAmount = orderAmount.subtract(ncAmount);
                    logger.debug("nc new orderAmount: " + orderAmount);
                }
                if (gcAmount != null && gcAmount.intValue() > 0) {
                    orderAmount = orderAmount.subtract(gcAmount);
                    logger.debug("gc new orderAmount: " + orderAmount);
                }
                if (orderAmount.doubleValue() > 0) {
                    newApprovalAmount = orderAmount;
                    logger.debug("newApprovalAmount: " + newApprovalAmount);
                }
                if (ccVO.getApprovalAmount() == null ||
                    !ccVO.getApprovalAmount().equals(newApprovalAmount.toString())) {

                    ccVO.setApprovalAmount(newApprovalAmount.toString());
                    logger.debug("CC Approval amount changed to: " + ccVO.getApprovalAmount());
                }
            }

            String creditCardNumber = ccVO.getCreditCardNumber();
            String expirationDate = ccVO.getCreditCardExpiration();
            String cscValue = ccVO.getCscValue();
            String cscOverrideFlag = ccVO.getCscOverrideFlag();
            ccVO.setAuthorizationRequired(false);

            if (hasNC) {

                String managerId = ccVO.getNoChargeApprovalId();
                String managerPassword = ccVO.getNoChargeApprovalPassword();
                String ncType = ccVO.getNoChargeType();
                String ncReason = ccVO.getNoChargeReason();
                String ncOrderRef = ccVO.getNoChargeOrderReference();
                
                if (managerId == null || managerId.equals("")) {
                    errorList.put("credit-card", "Manager Id is required");
                    logger.error("Manager Id is required");
                } else if (managerPassword == null || managerPassword.equals("")) {
                    errorList.put("credit-card", "Manager password is required");
                    logger.error("Manager password is required");
                } else {
                    cr = orderEntryDAO.authenticateIdentity(qConn, managerId, managerPassword);
                    cr.next();
                    String status = cr.getString("status");
                    if (status == null || !status.equalsIgnoreCase("Y")) {
                        errorList.put("credit-card", "Approval manager information is not valid. Please re-enter.");
                        logger.error("Approval manager information is not valid. Please re-enter.");
                    }
                }
                if (ncType == null || ncType.equals("")) {
                    errorList.put("credit-card", "No-charge type is required");
                    logger.error("No-charge type is required");
                }
                if (ncOrderRef == null || ncOrderRef.equals("")) {
                    if (ncType.equalsIgnoreCase("Resend")) {
                        errorList.put("order-reference", "No-charge order reference is required");
                        logger.error("No-charge order reference is required");
                    }
                } else {
                    HashMap outputMap = orderEntryDAO.validateExternalOrderNumber(qConn, ncOrderRef);
                    String orderDetailId = (String) outputMap.get("OUT_ORDER_DETAIL_ID");
                    logger.debug("orderDetailId: " + orderDetailId);
                    if (orderDetailId == null || orderDetailId.equals("")) {
                        errorList.put("order-reference", "Order reference not found.");
                        logger.error("Order reference not found.");
                    } else {
                        ccVO.setNoChargeOrderDetailId(orderDetailId);
                    }
                }
                if (ncReason == null || ncReason.equals("")) {
                    errorList.put("credit-card", "No-charge reason is required");
                    logger.error("No-charge reason is required");
                }
                
                if (creditCardType == null) creditCardType = "NC";

            } else {
                if (hasGC) {
                    if (creditCardType == null) creditCardType = "GC";
                } else {
                    if (creditCardType == null) {
                        errorList.put("credit-card", "Payment method required");
                        logger.error("Payment method required");
                    } else {
                        if (paymentMethodId != null) {
                            if (creditCardType.equalsIgnoreCase("IN") && paymentMethodId.equalsIgnoreCase("PC")) {
                                creditCardType = "PC";
                                ccVO.setCreditCardType("PC");
                            }
                            if (!creditCardType.equalsIgnoreCase(paymentMethodId)) {
                                errorList.put("credit-card", "Payment method must be " + paymentMethodId);
                                logger.error("Payment method must be " + paymentMethodId);
                            }
                        }
                    }
                }
                
                cr = orderEntryDAO.getPaymentMethodById(qConn, creditCardType);
                if (cr.next()) {
                    String paymentType = cr.getString("payment_type");
                    String hasExpirationDate = cr.getString("has_expiration_date");
                    String cscRequired = cr.getString("csc_required_flag");
                    if (creditCardType.equalsIgnoreCase("NC")) {
                        paymentType = "N";
                    }
                    if (paymentType != null && paymentType.equalsIgnoreCase("C")) {

                        // mark as approved if manual approval code entered
                        if (ccVO.getApprovalCode() != null && !ccVO.getApprovalCode().equals("")) {
                            logger.debug("Manual auth, setting to AP");
                            ccVO.setApprovalVerbiage("AP");
                        } else if (!orderVO.isBypassCCAuthFlag()){
                            ccVO.setAuthorizationRequired(true);
                        }

                        if (creditCardNumber == null || creditCardNumber.equals("")) {
                            errorList.put("cc-number", "Credit card number is required");
                            logger.error("Credit card number is required");
                        } else {
                            if (!CreditCardUtil.checkCreditCard(creditCardType, creditCardNumber)) {
                                errorList.put("cc-number", "Invalid credit card number.");
                                logger.error("Invalid credit card number.");
                            }
                        }
                        
                        if (cscValue != null && !cscValue.equals("")) {
                            if (cscValue.length() < 3 ||
                                  cscValue.length() > 4 ||
                                  !fieldValidation(cscValue,true,false,"")) {
                                errorList.put("csc-value", "Invalid card security code. Please confirm card security code and re-enter");
                                logger.error("Invalid card security code. Please confirm card security code and re-enter");
                            }
                        } else {
                            if ((cscRequired != null && "Y".equalsIgnoreCase(cscRequired)) && 
                                  (cscOverrideFlag == null || !"Y".equalsIgnoreCase(cscOverrideFlag))) {
                                errorList.put("csc-value", "CSC is required");
                                logger.error("CSC is required");
                            }
                        }
                        
                        if (expirationDate != null && !expirationDate.equals("")) {
                            try {
                                Date expire = sdfExpiration.parse(expirationDate);
                                String expireString = sdf.format(expire);
                                String today = sdf.format(new Date());
                                logger.debug("expire: " + expireString);
                                logger.debug("today: " + today);
                                if (expireString.compareTo(today) < 0) {
                                    errorList.put("cc-exp-date", "Invalid expiration date.");
                                    logger.error("Invalid expiration date.");
                                }
                            } catch (Exception e) {
                                errorList.put("cc-exp-date", "Invalid expiration date.");
                                logger.error("Invalid expiration date: " + e);
                            }
                        } else {
                            if (hasExpirationDate != null && hasExpirationDate.equalsIgnoreCase("Y")) {
                                errorList.put("cc-exp-date", "Expiration date is required");
                                logger.error("Expiration date is required");
                            }
                        }
                    }
                } else {
                    errorList.put("credit-card", "Invalid payment method");
                    logger.error("Invalid payment method");
                }
                
            }

            List detailList = orderVO.getOdVO();
            for (int i=0; i<detailList.size(); i++) {
                OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                ProductVO productVO = null;
                String personalGreetingData = null;
                
                String productId = odVO.getProductId();
                if (productId != null) {
                    Document productDoc = orderEntryDAO.getProductDetailsAjax(qConn, productId, sourceCode);
                    productVO = ProductUtil.parseProductXML(productDoc);
                    
                    // If one product is of Premier Collection (Luxury Item) then flag the cart as hasLuxuryItem.
                    if (productVO.getPremierCollectionFlag() != null && productVO.getPremierCollectionFlag().equals("Y")) {
                        orderVO.setHasLuxuryItem(true);
                    }
                    
                    if (productVO.getStatus() == null || productVO.getStatus().equalsIgnoreCase("U")) {
                        errorList.put("product", "Product is not available.");
                        logger.error("Product is not available: " + productId);
                    }
                    if (productId != null && !productId.equalsIgnoreCase(productVO.getProductId()) && productVO.isHasSubcodes()) {
                        logger.debug("different productId: " + productId + " " + productVO.getProductId());
                        odVO.setProductId(productVO.getProductId());
                        odVO.setProductSubcodeId(productId);
                    }
                    
                    String customFlag = productVO.getCustomFlag();
                    if (customFlag != null && customFlag.equalsIgnoreCase("Y")) {
                        if (odVO.getFloristId() == null) {
                            errorList.put("florist-id", "Florist Id is required for custom products.");
                            logger.error("Florist Id is required for custom products.");
                        }
                        if (odVO.getSpecialInstructions() == null) {
                            errorList.put("florist-comments", "Florist comments are required for custom products.");
                            logger.error("Florist comments are required for custom products.");
                        }
                    }
                    
                    String colorSizeFlag = productVO.getColorSizeFlag();
                    if (colorSizeFlag != null && colorSizeFlag.equalsIgnoreCase("C")) {
                        if (odVO.getFirstColorChoice() == null) {
                            errorList.put("first-color-choice", "A first color choice is required.");
                            logger.error("A first color choice is required.");
                        }
                        if (odVO.getSecondColorChoice() == null) {
                            errorList.put("second-color-choice", "A second color choice is required.");
                            logger.error("A second color choice is required.");
                        }
                    }
                    String personalGreetingFlag = productVO.getPersonalGreetingFlag();
                    if (personalGreetingFlag != null && personalGreetingFlag.equalsIgnoreCase("Y")) {
                        personalGreetingData = productVO.getProductName();
                    }
                } else {
                    errorList.put("product", "Product Id is required.");
                    logger.error("Product Id is required");
                }

                Date deliveryDate = odVO.getDeliveryDate();
                if (deliveryDate == null) {
                    errorList.put("delivery-date", "Delivery date is required.");
                    logger.error("Delivery date is required");
                }
                
                AddOnUtility aou = new AddOnUtility();
                ArrayList<String> addonArray = new ArrayList<String>();

                List addonList = odVO.getAddonVO();
                for (int j=0; j<addonList.size(); j++) {
                    AddonVO addonVO = (AddonVO) addonList.get(j);
                    String addonId = addonVO.getAddonId();
                    
                    if (addonId != null) {
                        if (addonId.equalsIgnoreCase("F")) {
                            if (productVO.getAddonFuneralFlag() == null || !productVO.getAddonFuneralFlag().equalsIgnoreCase("Y")) {
                                errorList.put("add-ons", "Product not flagged for funeral add-on");
                                logger.error("Product not flagged for funeral add-on");
                            }
                            String bannerText = addonVO.getFuneralBannerText();
                            if (bannerText == null || bannerText.equals("")) {
                                errorList.put("add-ons", "Funeral banner text required.");
                                logger.error("Funeral banner text required.");
                            } else {
                                if (bannerText.length() > 30) {
                                    errorList.put("add-ons", "Banner text too long.");
                                    logger.error("Banner text too long.");
                                }
                                if (!fieldValidation(bannerText, true, true, "& ")) {
                                    errorList.put("add-on", "This field cannot contain special characters.");
                                    logger.error("This field cannot contain special characters.");
                                }
                            }
                        } else if (addonId.startsWith("RC")) {
                            if (productVO.getAddonCardFlag() == null || !productVO.getAddonCardFlag().equalsIgnoreCase("Y")) {
                                errorList.put("add-ons", "Product not flagged for card add-on");
                                logger.error("Product not flagged for card add-on");
                            }
                        } else {
                            List productAddons = productVO.getAddonIds();
                            if (!productAddons.contains(addonId)) {
                                String errorMsg = "Vase and/or one or more add-ons are no longer available for this product. " + 
                                                  "Please make a different selection.";
                                errorList.put("add-ons", errorMsg);
                                logger.error(errorMsg + " - " + addonId);
                            } else {
                                addonArray.add(addonId);
                            }
                        }
                    }
                } // add-ons
                
                String cardMessage = odVO.getCardMessage();
                if (cardMessage == null) {
                    errorList.put("card-message", "Card Message required.");
                    logger.error("Card Message required.");
                } else if (cardMessage.length() > 240) {
                    errorList.put("card-message", "Card Message too long.");
                    logger.error("Card Message too long.");
                }

                String floristComments = odVO.getSpecialInstructions();
                if (floristComments != null) {
                    if (floristComments.length() > 160) {
                        errorList.put("florist-comments", "Florist Comments text too long.");
                        logger.error("Florist Comments text too long.");
                    }
                }
                
                String orderComments = odVO.getOrderComments();
                if (orderComments != null) {
                    if (orderComments.length() > 160) {
                        errorList.put("order-comments", "Order Comments text too long.");
                        logger.error("Order Comments text too long.");
                    }
                }
                
                CustomerVO recipientVO = odVO.getRecipientVO();
                String shipState = recipientVO.getState();
                String shipMethod = odVO.getShipMethod();

                if (shipMethod == null || shipMethod.equals("")) {
                    //odVO.setShipMethod("SD");
                    //logger.debug("Defaulting Ship Method to SD");
                } else if (!shipMethod.equalsIgnoreCase("SD")) { //AK/HI will now allow any ship method
                    /*if (!shipMethod.equalsIgnoreCase("2F") && shipState != null && 
                        (shipState.equalsIgnoreCase("AK") || shipState.equalsIgnoreCase("HI"))) {
                    
                        errorList.put("ship-method", "Orders to Alaska and Hawaii must be 2F");
                        logger.error("Orders to Alaska and Hawaii must be 2F");
                    }*/
                    if (addonArray.size() > 0) {
                        logger.debug("Checking for vendors using AddOnUtility");
                        ArrayList addonVendors = aou.getVendorListByAddonListAndProduct(productId, addonArray, qConn);
                        if (addonVendors == null || addonVendors.size() <= 0) {
                            String errorMessage = "There is not a vendor that has the combination of vase and/or add-ons selected. " + 
                                              "Please make a different selection.";
                            errorList.put("add-ons", errorMessage);
                            logger.error(errorMessage);
                        }
                    }

                }
                
                String recipientFirstName = recipientVO.getFirstName();
                if (recipientFirstName == null || recipientFirstName.equals("")) {
                    errorList.put("recipient-first-name", "Recipient first name required.");
                    logger.error("Recipient first name required.");
                } else {
                    if (recipientFirstName.length() > 25) {
                        errorList.put("recipient-first-name", "Recipient first name too long.");
                        logger.error("Recipient first name too long.");
                    }
//                    if (!fieldValidation(recipientFirstName, true, true, null)) {
//                        errorList.put("recipient-first-name", "Invalid characters.");
//                        logger.error("Invalid characters.");
//                    }
                }

                String recipientLastName = recipientVO.getLastName();
                if (recipientLastName == null || recipientLastName.equals("")) {
                    errorList.put("recipient-last-name", "Recipient last name required.");
                    logger.error("Recipient last name required.");
                } else {
                    if (recipientLastName.length() > 25) {
                        errorList.put("recipient-last-name", "Recipient last name too long.");
                        logger.error("Recipient last name too long.");
                    }
//                    if (!fieldValidation(recipientLastName, true, true, ".'- ")) {
//                        errorList.put("recipient-last-name", "Invalid characters.");
//                        logger.error("Invalid characters.");
//                    }
                }
                
                if (personalGreetingData != null) {
                    PersonalGreetingVO pgVO = new PersonalGreetingVO();
                    pgVO.setProductId(odVO.getProductId());
                    pgVO.setProductName(personalGreetingData);
                    pgVO.setRecipientName(recipientFirstName + " " + recipientLastName);
                    odVO.setPersonalGreetingVO(pgVO);
                    foundPersonalGreeting = true;
                    logger.debug("added pgVO: " + pgVO.getProductName() + " " + pgVO.getRecipientName());
                }

                String recipientAddress = recipientVO.getAddress();
                if (recipientAddress == null || recipientAddress.equals("")) {
                    errorList.put("recipient-last-name", "Recipient address required.");
                    logger.error("Recipient address required.");
                } else {
                    if (recipientAddress.length() > 90) {
                        errorList.put("recipient-address", "Recipient address too long.");
                        logger.error("Recipient address name too long.");
                    }
//                    if (!fieldValidation(recipientAddress, true, true, ". ")) {
//                        errorList.put("recipient-address", "Invalid characters.");
//                        logger.error("Invalid characters.");
//                    }
                    boolean isPO_FPO_APO = false;
                    recipientAddress = recipientAddress.toLowerCase();
                    if (recipientAddress.indexOf("p.o.") > -1) {
                        isPO_FPO_APO = true;
                    } else {
                        int pos = recipientAddress.indexOf("po");
                        if (pos > -1) {
                            isPO_FPO_APO = true;
                            if (pos > 0) {
                                String before = recipientAddress.substring(pos-1, pos);
                                if (fieldValidation(before, true, true, "")) {
                                    isPO_FPO_APO = false;
                                }
                            }
                            if (isPO_FPO_APO) {
                                int len = recipientAddress.length();
                                if (pos < (len - 2)) {
                                    isPO_FPO_APO = true;
                                    String after = recipientAddress.substring(pos+2, pos+3);
                                    if (fieldValidation(after, true, true, "")) {
                                        isPO_FPO_APO = false;
                                    }
                                }
                            }
                        }
                        pos = recipientAddress.indexOf("fpo");
                        if (pos > -1) {
                            isPO_FPO_APO = true;
                            if (pos > 0) {
                                String before = recipientAddress.substring(pos-1, pos);
                                if (fieldValidation(before, true, true, "")) {
                                    isPO_FPO_APO = false;
                                }
                            }
                            if (isPO_FPO_APO) {
                                int len = recipientAddress.length();
                                if (pos < (len - 3)) {
                                    isPO_FPO_APO = true;
                                    String after = recipientAddress.substring(pos+3, pos+4);
                                    if (fieldValidation(after, true, true, "")) {
                                        isPO_FPO_APO = false;
                                    }
                                }
                            }
                        }
                        pos = recipientAddress.indexOf("apo");
                        if (pos > -1) {
                            isPO_FPO_APO = true;
                            if (pos > 0) {
                                String before = recipientAddress.substring(pos-1, pos);
                                if (fieldValidation(before, true, true, "")) {
                                    isPO_FPO_APO = false;
                                }
                            }
                            if (isPO_FPO_APO) {
                                int len = recipientAddress.length();
                                if (pos < (len - 3)) {
                                    isPO_FPO_APO = true;
                                    String after = recipientAddress.substring(pos+3, pos+4);
                                    if (fieldValidation(after, true, true, "")) {
                                        isPO_FPO_APO = false;
                                    }
                                }
                            }
                        }
                    }
                    if (isPO_FPO_APO) {
                        errorList.put("recipient-address", "Deliveries to PO, FPO, and APO boxes are not permitted.");
                        logger.error("Deliveries to PO, FPO, and APO boxes are not permitted.");
                    }
                }

                String recipientCity = recipientVO.getCity();
                if (recipientCity == null || recipientCity.equals("")) {
                    errorList.put("recipient-city", "Recipient city required.");
                    logger.error("Recipient city required.");
                } else {
                    if (recipientCity.length() > 30) {
                        errorList.put("recipient-city", "Recipient city too long.");
                        logger.error("Recipient city too long.");
                    }
                }

                String recipientCountry = recipientVO.getCountry();
                boolean isRecipientDomestic = false;
                if (recipientCountry == null || recipientCountry.equals("")) {
                    errorList.put("recipient-country", "Recipient country required.");
                    logger.error("Recipient country required.");
                } else {
                    cr = orderEntryDAO.getCountryMasterById(qConn, recipientCountry);
                    if (cr.next()) {
                        String status = cr.getString("status");
                        if (status == null || !status.equalsIgnoreCase("active")) {
                            errorList.put("recipient-country", "Invalid recipient country.");
                            logger.error("Invalid recipient country.");
                        }
                        String domesticFlag = cr.getString("oe_country_type");
                        if (domesticFlag != null && domesticFlag.equalsIgnoreCase("D")) {
                            isRecipientDomestic = true;
                        }
                    } else {
                        errorList.put("recipient-country", "Invalid recipient country.");
                        logger.error("Invalid recipient country.");
                    }
                }
                
                String recipientState = recipientVO.getState();
                String recipientZipCode = recipientVO.getZipCode();
                if (isRecipientDomestic) {
                    if (recipientState == null || recipientState.equals("")) {
                        errorList.put("recipient-state", "Recipient state required.");
                        logger.error("Recipient state required.");
                    } else {
                        cr = orderEntryDAO.getStateDetails(qConn, recipientState);
                        if (!cr.next()) {
                            errorList.put("recipient-state", "Invalid recipient state.");
                            logger.error("Invalid recipient state.");
                        }
                    }

                    if (recipientZipCode == null || recipientZipCode.equals("")) {
                        errorList.put("recipient-zip-code", "Recipient zip code required.");
                        logger.error("Recipient zip code required.");
                    } else {
                        if (recipientZipCode.length() > 6) {
                            errorList.put("recipient-zip-code", "Recipient zip code too long.");
                            logger.error("Recipient zip code too long.");
                        }
                        if (!fieldValidation(recipientZipCode, true, true, null)) {
                            errorList.put("recipient-zip-code", "Invalid characters.");
                            logger.error("Invalid characters.");
                        }
                    }
                } else {
                    if (recipientZipCode != null && !recipientZipCode.equals("")) {
                        errorList.put("recipient-zip-code", "Recipient zip code should be blank.");
                        logger.error("Recipient zip code should be blank.");
                    }
                }

                String recipientPhone = recipientVO.getDayPhone();
                if (recipientPhone == null || recipientPhone.equals("")) {
                    errorList.put("recipient-phone", "Recipient phone required.");
                    logger.error("Recipient phone required.");
                } else {
                    if (isRecipientDomestic) {
                        String phoneNumber = recipientPhone.replaceAll( "\\D", "" );
                        if (phoneNumber.length() != 10) {
                            errorList.put("recipient-phone", "Recipient phone must be 10 digits.");
                            logger.error("Recipient phone must be 10 digits.");
                        }
                    } else if (recipientPhone.length() > 20) {
                        errorList.put("recipient-phone", "Recipient phone too long.");
                        logger.error("Recipient phone too long.");
                    }
                    if (!fieldValidation(recipientPhone, true, false, "-/() ")) {
                        errorList.put("recipient-phone", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }

                String recipientPhoneExt = recipientVO.getDayPhoneExt();
                if (recipientPhoneExt != null) {
                    if (recipientPhoneExt.length() > 10) {
                        errorList.put("recipient-phone-ext", "Recipient phone extension too long.");
                        logger.error("Recipient phone extension too long.");
                    }
                    if (!fieldValidation(recipientPhoneExt, true, false, "-/()")) {
                        errorList.put("recipient-phone-ext", "Invalid characters.");
                        logger.error("Invalid characters.");
                    }
                }

                String shipToType = recipientVO.getAddressType();
                String shipToTypeName = recipientVO.getBusinessName();
                String recipientRoom = recipientVO.getRecipientRoom();
                String hoursStart = recipientVO.getHoursStart();
                String hoursEnd = recipientVO.getHoursEnd();
                boolean timeOfServiceFlag = recipientVO.getTimeOfServiceFlag();
                
                //added for Sympathy Controls start
                String sympathyShipMethod = "florist";
                if(shipMethod!=null && (shipMethod.equals("ND") || shipMethod.equals("2F") || shipMethod.equals("GR") || shipMethod.equals("SA"))){
                	sympathyShipMethod = "dropship";
                }
                
                if(shipToType.contains(SympathyConstants.SYMPATHY_LOCATION_FUNERAL) || shipToType.contains(SympathyConstants.SYMPATHY_LOCATION_CEMETERY) || shipToType.contains(SympathyConstants.SYMPATHY_LOCATION_HOSPITAL)){
                	SympathyControls sympathyControls = new SympathyControls(qConn);
                	SympathyItemsVO itemVO = new SympathyItemsVO();
                	itemVO.setSourceCode(sourceCode);
                	itemVO.setDeliveryDate(deliveryDate);
                	itemVO.setDeliveryTime(-100);//passing negative value to skip the lead time check
                	itemVO.setShipState(shipState);
                	itemVO.setDeliveryLocation(shipToType);
                	itemVO.setShipMethod(sympathyShipMethod);
                	itemVO.setLeadTimeCheckFlag(false);
                	itemVO.setModule("JOE");
                	Map map = sympathyControls.performSympathyControls(qConn, itemVO);
                	if(map.get("status").equals('N')){
                    	errorList.put("Sympathy-Controls", String.valueOf(map.get("message")));
                        logger.error(String.valueOf(map.get("message")));
                    }
                }
                
                
                //added for Sympathy Controls end

                if (shipToType == null || shipToType.equals("")) {
                    errorList.put("ship-to-type", "Recipient ship-to type required.");
                    logger.error("Recipient ship-to type required.");
                } else {

                    cr = orderEntryDAO.getAddressTypeByCode(qConn, shipToType);
                    if (!cr.next()) {
                        errorList.put("ship-to-type", "Invalid Recipient ship-to type.");
                        logger.error("Invalid Recipient ship-to type.");
                    } else {
                        String promptForBusiness = cr.getString("prompt_for_business_flag");
                        String businessLabel = cr.getString("business_label_txt");
                        String promptForRoom = cr.getString("prompt_for_room_flag");
                        String roomLabel = cr.getString("room_label_txt");
                        String promptForHours = cr.getString("prompt_for_hours_flag");
                        String hoursLabel = cr.getString("hours_label_txt");

                        if (promptForBusiness != null && promptForBusiness.equalsIgnoreCase("Y")) {
                            if (shipToTypeName == null || shipToTypeName.equals("")) {
                                errorList.put("ship-to-type-name", "Recipient " + businessLabel + " required.");
                                logger.error("Recipient " + businessLabel + " required.");
                            }
                        }
                        if (promptForRoom != null && promptForRoom.equalsIgnoreCase("Y")) {
                            if (recipientRoom != null && !recipientRoom.equals("")) {
                                String roomString = roomLabel + ": " + recipientRoom;
                                odVO.setSpecialInstructions(odVO.getSpecialInstructions() + "  " + roomString);
                            }
                        }
                        if (promptForHours != null && promptForHours.equalsIgnoreCase("Y")) {
                            if ( (hoursStart != null && !hoursStart.equals("")) || (hoursEnd != null && !hoursEnd.equals("")) ) {
                                if (hoursStart == null || hoursStart.equals("")) {
                                    errorList.put("recip-hours-start", "Recipient " + hoursLabel + " required.");
                                    logger.error("Recipient " + hoursLabel + " required.");
                                }
                                if (hoursEnd == null || hoursEnd.equals("")) {
                                    errorList.put("recip-hours-end", "Recipient " + hoursLabel + " required.");
                                    logger.error("Recipient " + hoursLabel + " required.");
                                } else {
                                    try {
                                    	
                                    	
                                    	SimpleDateFormat sdfHours = new SimpleDateFormat("HH");
                                        
                                        //Changing the values for formatting the dates
                                        Date hoursStartDate = sdfHours.parse(Integer.toString(Integer.valueOf(hoursStart)/100));
                                        Date hoursEndDate = sdfHours.parse(Integer.toString(Integer.valueOf(hoursEnd)/100));
                                        sdfHours = new SimpleDateFormat("ha");
                                        String startString = sdfHours.format(hoursStartDate);
                                        String endString = sdfHours.format(hoursEndDate);
                                        String hoursString = hoursLabel + ": " + startString + " - " + endString;
                                        
                                        //If the Order doesn't have Time of service. 
                                        if(timeOfServiceFlag==false)
                                        {	 
                                        	odVO.setSpecialInstructions(odVO.getSpecialInstructions() + "  " + hoursString);
                                        }
                                        odVO.setTimeOfService(hoursStart);
                                        
                                    } catch (Exception e) {
                                        errorList.put("recip-hours-start", "Invalid recipient " + hoursLabel);
                                        logger.error("Invalid recipient " + hoursLabel);
                                    }
                                }
                            }
                        }
                    }
                }
                
                String floristId = odVO.getFloristId();
                if (floristId != null && !floristId.equals("")) {
                    FTDValidationBO vbo = new FTDValidationBO();
                    Document validateFlorist = vbo.validateFlorist(floristId, productId, recipientZipCode, deliveryDate);
                    Element rowset = JAXPUtil.selectSingleNode(validateFlorist,"/result/rs[@name=\"validation\"]");
                    String status = rowset.getAttribute("status");
                    logger.debug("florist validation status: " + status);
                    if (status == null || !status.equalsIgnoreCase("Y")) {
                        errorList.put("florist-id", "Invalid florist id");
                        logger.error("Invalid florist id: " + floristId);
                    }
                }

            }
            
            // Get Personal Greeting Id if necessary
            if ( (errorList == null || errorList.size() <= 0) && foundPersonalGreeting) {
                detailList = orderVO.getOdVO();
                for (int i=0; i<detailList.size(); i++) {
                    OrderDetailVO odVO = (OrderDetailVO) detailList.get(i);
                    PersonalGreetingVO pgVO = odVO.getPersonalGreetingVO();
                    if (pgVO != null) {
                        try {
                            String pgId = null;
                            RecordableBouquetServiceRemoteImpl rbsri = new RecordableBouquetServiceRemoteImpl();
                            pgId = rbsri.generateAndTransmitPersonalGreetingId(odVO.getDeliveryDate(), null);
                            pgVO.setPersonalGreetingId(pgId);
                            logger.debug("pg id assigned: " + pgVO.getProductName() + " " + pgVO.getRecipientName() + " " + pgVO.getPersonalGreetingId());
                        } catch (Exception e) {
                            logger.error(e);
                            errorList.put("product-id", "Orders cannot be placed for personal greeting products at this time. Please choose another product");
                        }
                        
                    }
                }
            }
            
        } catch (Exception e){
            errorList.put("order", e.getMessage());
            logger.error(e);
        }

        return errorList;

    }

    public static boolean fieldValidation(String value, boolean numeric, boolean alpha, String specialCharacters) {
        
        char[] chars = value.toCharArray();
        for (int x = 0; x < chars.length; x++) {      
            char c = chars[x];
            if (numeric) {
                if ((c >= '0') && (c <= '9')) continue;
            }
            if (alpha) {
                if ((c >= 'a') && (c <= 'z')) continue; // lowercase
                if ((c >= 'A') && (c <= 'Z')) continue; // uppercase
            }
            if (specialCharacters != null) {
                if ( specialCharacters.indexOf(c) != -1 ) continue;
            }
            return false;
        }  
        return true;

    }

    public static boolean validateIOTWSourceCode(Document doc) {

        boolean iotwFlag = false;

        NodeList nl = doc.getElementsByTagName("iotw_flag");
        if (nl.getLength() > 0) {
            Element iotw = (Element) nl.item(0);
            if (iotw.hasChildNodes()) {
                Node n = iotw.getFirstChild();
                String value = n.getNodeValue();
                if (value != null && value.equalsIgnoreCase("Y")) {
                    iotwFlag = true;
                }
            }
        }
        
        return iotwFlag;
        
    }

    public static boolean validateMembershipId(Connection qConn, String membershipId, String partnerId, String partnerName) throws Exception {
    
        boolean validMembershipId = true;
        validMembershipId = new ValidateMembership().validateMembershipById(membershipId, partnerId, partnerName, qConn);

        return validMembershipId;

    }

}