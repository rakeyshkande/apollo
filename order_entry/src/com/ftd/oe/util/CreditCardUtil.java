package com.ftd.oe.util;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ftd.oe.common.OrderEntryConstants;
import com.ftd.oe.vo.CreditCardVO;
import com.ftd.oe.vo.CustomerVO;
import com.ftd.op.order.to.CreditCardTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
* Utility class providing methods for working with Credit Cards.
*/
public class CreditCardUtil {

    private static Logger logger = new Logger("com.ftd.oe.util.CreditCardUtil");
    
    /**
    * Authorizes the passed in Credit Card Transaction.
    * 
    * @param vvCO Credit Card Transaction to be authorized. The result of the authorization is stored into the fields of this parameter.
    * @param conn Database connection for working with the OrderAPI which provided credit card validation functionality.
    */
    public static void authorize(CreditCardVO ccVO, CustomerVO custVO, Connection conn) {

        SimpleDateFormat sdf1 = new SimpleDateFormat("MM/yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyMM");

        CreditCardTO ccTO = new CreditCardTO();
        
        /*joe amex changes*/logger.debug("In CreditCardUtil Authorize ");              
        if(custVO == null)
        {
          logger.debug("custVO null ");
        }        
        else
        {
        ccTO.setName(custVO.getFirstName());        
        ccTO.setCity(custVO.getCity());
        ccTO.setCountry(custVO.getCountry());
        ccTO.setZipCode(custVO.getZipCode());        
        ccTO.setAddressLine(custVO.getAddress());
        }
        /*end of joe amex changes*/
        ccTO.setAmount(ccVO.getApprovalAmount());
        ccTO.setCreditCardType(ccVO.getCreditCardType());
        ccTO.setCreditCardNumber(ccVO.getCreditCardNumber());
        ccTO.setCscValue(ccVO.getCscValue());
        ccTO.setCreditCardDnsType(ccVO.getCreditCardDnisType());
        String expirationDate = ccVO.getCreditCardExpiration();
        try {
            ccTO.setExpirationDate(sdf2.format(sdf1.parse(expirationDate)));
        } catch (ParseException pe) {
            logger.error("Invalid expiration date");
            ccTO.setExpirationDate(sdf2.format(new Date()));
        }
        
        ccTO.setEci("");
        ccTO.setCavv("");
        ccTO.setXid("");
        ccTO.setUcaf("");
        
        try {
            ccTO = MessagingServiceLocator.getInstance().getOrderAPI().validateCreditCard(ccTO, conn);

            ccVO.setStatus(ccTO.getStatus());
            ccVO.setValidationStatus(ccTO.getValidationStatus());
            ccVO.setApprovalCode(ccTO.getApprovalCode());
            ccVO.setApprovalVerbiage(ccTO.getVerbiage());
            ccVO.setApprovalActionCode(ccTO.getActionCode());
            ccVO.setAvsResultCode(ccTO.getAVSIndicator());
            ccVO.setCscResponseCode(ccTO.getCscResponseCode());
            if (ccVO.getCreditCardType().equalsIgnoreCase("MS")) {
                ccVO.setAafesTicketNumber(ccTO.getAcquirerReferenceData());
            } else {
                ccVO.setAcqReferenceData(ccTO.getAcquirerReferenceData());
            }
            if (ccVO.getCscValue()!=null && !"".equals(ccVO.getCscValue())) {
                ccVO.setCscValidateFlag("Y");
            } else {
                ccVO.setCscValidateFlag("N");
            }
            ccVO.setUnderMinAuthAmt(ccTO.isUnderMinAuthAmt());
            ccVO.setCcAuthProvider(ccTO.getCcAuthProvider());
            ccVO.getPaymentExtMap().putAll(ccTO.getPaymentExtMap());

        } catch (Exception e) {
            logger.error(e);
        }
    }

    /**
    * Credit Card validation
    *
    */
    public static boolean checkCreditCard(String ccType, String id) {
        int idBegin = 0;
        int ccLength = 0;
        int first4 = 0;
        int first5 = 0;
        int first6 = 0;
        int digit = 0;
        int counter = 0;
        int total = 0;
        int second = 0;
        int multTotal = 0;
        int first = 0;
        int checkDigit = 0;
        int checkDigitLength = 0;
        String lastDigit = "";
        String digitStr = "";
        int first7 =0;
      
        //Remove the spaces
        id = id.trim();
        ccType = ccType.trim();

        //Validate the id is numeric
        try{
            Double.parseDouble(id);
        } catch (NumberFormatException e){
            return false;
        }

        //Credit card type is required
        if (ccType.length() == 0)
            return false;

        //Carte Blanche has the same check digit routine as Diners
        if (ccType.equals("CB"))
            ccType = "DC";

        idBegin = StringToInt(id.substring(0,2));
        //Determine the credit card type for gift certificates
        if (ccType.equals("GC"))
        {        

            if ((idBegin == 30) | (idBegin == 36) | (idBegin == 38))
                ccType = "DC";

            if ((idBegin == 34) | (idBegin == 37))
                ccType = "AX";

            if ((idBegin >=40) & (idBegin <=49))
                ccType = "VI";

            if ((idBegin >=50) & (idBegin <=59))
                ccType = "MC";

            if ((idBegin >=20) & (idBegin <=29))
               ccType = "MC";

            if ((idBegin >=60) & (idBegin <=69))
                ccType = "DI";
        }

        //Set the length variable based on the credit card type
        if (ccType.equals("VI"))
        {
            if(id.length() == 16)
                ccLength = 16;
            if(id.length() == 13)
                ccLength = 13;
        }

        else if((ccType.equals("MC")) | (ccType.equals("DI")))
            ccLength = 16;
        else if(ccType.equals("AX"))
            ccLength = 15;
        else if ((ccType.equals("DC")) | (ccType.equals("CB")))
            ccLength = 14;
        else if (ccType.equals("JP"))
            ccLength = 11;
        else if(ccType.equals("MS"))  // AAFES
        {
            ccLength = 16;
        }
        else
            ccLength = 16;

        //Compare the variable to the length of the id
        if (ccLength != id.length())
            return false;

        //Get the first 5 digits of the id
        first4 = StringToInt(id.substring(0,4));
        first5 = StringToInt(id.substring(0,5));
        first6 = StringToInt(id.substring(0,6));
        first7 = StringToInt(id.substring(0,7));
        boolean authProviderBAMS=false;
      //Getting BAMS global parameters.
        if(ccType.equals("DI") || ccType.equals("DC")){
        	authProviderBAMS = isAuthProviderBAMS(ccType);
        }
        
        //VISA
        if (ccType.equals("VI"))
            {
            if ((first5 < 40000) | (first6 > 499999))
                return false;
            }
        //MASTER CARD
        else if (ccType.equals("MC"))
            {
            if (((first5 < 50000) || (first5 > 59999)) && ((first5 < 20000) || (first5 > 29999)))               
                return false;
            }
        //DISCOVER CARD
        else if (ccType.equals("DI") )
            {
        	    logger.info("Order validation for Discover Card started.");
        	    //If authProvider is BAMS and BAMS accepting Discover card.
        	    if(authProviderBAMS){ 
        	    	if(!isValidDiscover(id,idBegin,first7))
        	    		return false;
        	    }else{ //If authProvider is not BAMS or BAMS not accepting Discover card then by default CCAS is authProvider.
        	    	if ((first5 < 60000) | (first5 > 69999))
        	    		return false;
        	    }
            }
        //AMERICAN EXPRESS
        else if (ccType.equals("AX"))
            {
            if ((first5 < 34000) | (first5 > 37999))
                return false;
            else if ((first5 > 34999) & (first5 < 37000))
                return false;
            }
        //DINERS CLUB
        else if (ccType.equals("DC")) 
            {
        	if(authProviderBAMS){ //Validations as in the jccas. 
        		if(!isValidDinersCard(id,idBegin,first7))
        			return false;
        	}else{
	        		if ((first5 < 30000) | (first5 > 38999))
	        			return false;
	        		if ((first5 > 30999) & (first5 < 36000))
	        			return false;
	        		if ((first5 > 36999) & (first5 < 38000))
	        			return false;
	        	}
            }
        // AAFES - Military Star
            else if(ccType.equals("MS"))
            {
                if(first4 != 6019)
                {
                    return false;   
                }
            }
        //ANY OTHER TYPE
        else {
            return false;
        }

        if (!ccType.equals("MS"))
        {
            checkDigitLength = id.length();

            //Validate the number
            for(int i = checkDigitLength-1; i >= 1; i--)
            {
                digit = Character.getNumericValue(((id.substring(i-1, i))).charAt(0));
                counter++;

                if ((counter % 2) != 0)
                {
                    digit *= 2;

                    //Add into the total
                    if (digit > 9)
                    {
                        //get the second number of the digit variable and hold it in an int
                        digitStr = (digit + "");
                        second = Character.getNumericValue(digitStr.charAt(1));
                        total += second;
                    }
                }

                digitStr = (digit + "");
                first = Character.getNumericValue(digitStr.charAt(0));
                total += first;
            }

            //if the remainder of total divided by 10 is not 0 then ....
            if ((total % 10) != 0)
                multTotal = ((total/10) + 1)*10;
            else
                multTotal = total;

            //set the checkDigit
            checkDigit = (multTotal - total);

            //Compare the checkDigit to the last digit of the id
            lastDigit = id.substring(checkDigitLength-1,checkDigitLength);

            if (checkDigit != Character.getNumericValue(lastDigit.charAt(0)))
                return false;
        }
        
        return true;

    }

    private static int StringToInt(String inString)
    {
        try{
           return (int)Double.parseDouble(inString);
        } catch(NumberFormatException e) {
           return -1;
        }
    }
    /**
     * This method is to validate discover card as in jccas.
     * @param id
     * @param idBegin
     * @param first7
     * @return
     */
    private static boolean isValidDiscover(String id, int idBegin,int first7){
    	 logger.info("Payment validation for Discover Card.");
		if((idBegin >= 60 & idBegin <= 69) && (id.length() == 16)){
			if(first7 >= 6011000 && first7 <= 6011999)
				return true;						
			if(first7 >= 6221260 && first7 <= 6229259)
				return true;
			if(first7 >= 6240000 && first7 <= 6269999)
				return true;
			if(first7 >= 6282000 && first7 <= 6288999)
				return true;			
			if(first7 >= 6440000 && first7 <= 6599999)
				return true;
		}
		//Here we are validating diners card validations also.
		return isValidCard(id, idBegin, first7);
	}
    /**
     * This method is to validate Diners Card as in jccas.
     * @param id
     * @param idBegin
     * @param first7
     * @return
     */
    private static boolean isValidDinersCard(String id, int idBegin,int first7){
    	 logger.info("Payment validation for Diners Card.");
		return isValidCard(id, idBegin, first7);
	}
    /**
     * This method is to validate Card details for both Discover card and Diners Card.
     * @param id
     * @param idBegin
     * @param first7
     * @return
     */
    private static boolean isValidCard(String id, int idBegin, int first7) {
    	if(((idBegin == 30 || idBegin == 35 || idBegin == 38) && id.length() == 16 )|| (idBegin == 36 && id.length() == 14)){

    		if(first7 >= 3000000 && first7 <= 3059999)
    			return true;
    		if(first7 >= 3095000 && first7 <= 3095999)
    			return true;
    		if(first7 >= 3528000 && first7 <= 3589999)
    			return true;
    		if(first7 >= 3600000 && first7 <= 3699999)
    			return true;
    		if(first7 >= 3800000 && first7 <= 3999999)
    			return true;
    	}
    	return false;
    }
   /**
    * This method is to check card authentication provider,Whether it is BAMS Or CCAS.If it is BAMS Return True otherwise false.
    * @param ccType
    * @return
    */
	private static boolean isAuthProviderBAMS(String ccType){
		boolean authProviderBAMS=false;
		try{
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String bamsCCList = cu.getFrpGlobalParm(OrderEntryConstants.AUTH_CONFIG, OrderEntryConstants.BAMS_CC_LIST);//It will get what are the cards accepted by BAMS.
			String authProvider = cu.getFrpGlobalParm(OrderEntryConstants.AUTH_CONFIG, OrderEntryConstants.AUTH_PROVIDER);
			if(OrderEntryConstants.BAMS_AUTH_PROVIDER.equals(authProvider) && checkValidBamsCC(ccType, bamsCCList) ){ 
				authProviderBAMS=true;
			}
		}catch(Exception e){
			logger.error("Error Occured while loading BAMS global parameters.");
			return false;
		}
		return authProviderBAMS;
	}
	/**
	 * Checks the given ccType present in comma separated list of credit cards. Return true if present other wise false
	 * 
	 * @param ccType
	 * @param csCcList
	 * @return
	 */
	public static boolean checkValidBamsCC(String ccType, String csCcList) {
		String[] ccList = csCcList.split(",");
		for(String cardType : ccList) {
			if(ccType.equals(cardType)) {
				return true;
			}
		}
		return false;
	}
}