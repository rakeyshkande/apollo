package com.ftd.oe.util;

import com.ftd.oe.dao.OrderEntryDAO;
import com.ftd.oe.vo.OrderDetailVO;
import com.ftd.oe.vo.ProductVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MembershipUtil {

    private static Logger logger = new Logger("com.ftd.oe.util.MembershipUtil");
    private static OrderEntryDAO orderEntryDAO = new OrderEntryDAO();

    /**
    * Calculates the partnership miles or points for a given product
    * This method is called from multiple classes. If the price point is already
    * known, the information is passed in via the OrderDetailVO object. This class
    * is also called from the product search and product details classes where a price
    * point is not selected so this class needs to calculate miles or points for all
    * three price points. In this case, the data is passed in via the ProductVO.
    * 
    * @param conn - Database connection
    * @param odVO - OrderDetailVO
    * @param productVO - ProductVO
    * @return nothing
    */
    public static void calcMilesPoints(Connection conn, OrderDetailVO odVO, ProductVO productVO) {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String sourceCode = odVO.getItemSourceCode();
        Date deliveryDate = odVO.getDeliveryDate();

        logger.debug("calcMilesPoints(" + sourceCode + ", " + (deliveryDate == null ? "null" : sdf.format(deliveryDate)) + ")");

        BigDecimal productPrice = new BigDecimal(0);
        BigDecimal itemTotal = new BigDecimal(0);
        BigDecimal basePoints = new BigDecimal(0);
        BigDecimal bonusPoints = new BigDecimal(0);
        BigDecimal standardPrice = new BigDecimal(0);
        BigDecimal deluxePrice = new BigDecimal(0);
        BigDecimal premiumPrice = new BigDecimal(0);
        BigDecimal standardPoints = new BigDecimal(0);
        BigDecimal standardBonus = new BigDecimal(0);
        BigDecimal deluxePoints = new BigDecimal(0);
        BigDecimal deluxeBonus = new BigDecimal(0);
        BigDecimal premiumPoints = new BigDecimal(0);
        BigDecimal premiumBonus = new BigDecimal(0);
        
        int scale = 0;

        if (productVO == null) {
            productPrice = odVO.getProductPrice();
            itemTotal = odVO.getLineItemTotal();
            odVO.setMilesPoints(new BigDecimal("0"));
        } else {
            standardPrice = productVO.getStandardPrice();
            deluxePrice = productVO.getDeluxePrice();
            premiumPrice = productVO.getPremiumPrice();
            logger.debug(standardPrice + " " + deluxePrice + " " + premiumPrice);
            productVO.setNetStandardPrice(new BigDecimal("0"));
            productVO.setNetDeluxePrice(new BigDecimal("0"));
            productVO.setNetPremiumPrice(new BigDecimal("0"));
            productPrice = standardPrice;
            itemTotal = standardPrice;
        }

        if (deliveryDate == null) {
            logger.error("Delivery date required for miles/points calculations");
            return;
        }

        try {

            CachedResultSet cr = orderEntryDAO.getPartnerProgramTerms(conn, sourceCode, deliveryDate);
            cr.next();

            String programType = cr.getString("program_type");
            logger.debug("programType: " + programType);

            if (programType != null && programType.equalsIgnoreCase("Default")) {

                String calcBasis = cr.getString("calculation_basis");
                BigDecimal points = cr.getBigDecimal("points");
                logger.debug("base: " + calcBasis + " " + points);

                if (points.scale() > 0) {
                    scale = 2;
                }

                String bonusBasis = cr.getString("bonus_calculation_basis");
                BigDecimal bonusPointsIn = cr.getBigDecimal("bonus_points");
                logger.debug("bonus: " + bonusBasis + " " + bonusPointsIn);

                if (productVO == null) {
                    basePoints = calculateReward(calcBasis , points, itemTotal, productPrice, scale);
                    bonusPoints = calculateReward(bonusBasis, bonusPointsIn, itemTotal, productPrice, scale);
                } else {
                    standardPoints = calculateReward(calcBasis , points, standardPrice, standardPrice, scale);
                    deluxePoints = calculateReward(calcBasis, points, deluxePrice, deluxePrice, scale);
                    premiumPoints = calculateReward(calcBasis, points, premiumPrice, premiumPrice, scale);
                    standardBonus = calculateReward(bonusBasis , bonusPointsIn, standardPrice, standardPrice, scale);
                    deluxeBonus = calculateReward(bonusBasis , bonusPointsIn, deluxePrice, deluxePrice, scale);
                    premiumBonus = calculateReward(bonusBasis , bonusPointsIn, premiumPrice, premiumPrice, scale);
                }

                // Next, calculate the minimum points (if any).
                String programName = cr.getString("program_name");
                logger.debug("calMilesPoints: sourceCode=" + sourceCode + " programName=" + programName + 
                    " deliveryDate=" + sdf.format(deliveryDate));

                CachedResultSet rewardRanges = orderEntryDAO.getProgramRewardRange(conn, programName);

                // Cycle through the resultset to determine the appropriate minimum points to apply.
                BigDecimal minPoints = new BigDecimal(0);

                while (rewardRanges.next()) {
                    BigDecimal rangeMinDollar = rewardRanges.getBigDecimal("min_dollar");
                    BigDecimal rangeMaxDollar = rewardRanges.getBigDecimal("max_dollar");
                    String rangeCalculationBasis = rewardRanges.getString("calculation_basis");
                    BigDecimal rangePointsFactor = rewardRanges.getBigDecimal("points");
                    logger.debug(rangeMinDollar + " " + rangeMaxDollar + " " + rangeCalculationBasis + " " + rangePointsFactor);

                    BigDecimal dollarAmount = new BigDecimal(0);

                    if (rangeCalculationBasis.equals("F")) {
                        dollarAmount = new BigDecimal(1);
                    } else if (rangeCalculationBasis.equals("M")) {
                        dollarAmount = productPrice;
                    } else if (rangeCalculationBasis.equals("T")) {
                        dollarAmount = itemTotal;
                    } else {
                        logger.error("Unrecognized range calculation basis: " + rangeCalculationBasis);
                        dollarAmount = new BigDecimal(0);
                    }

                    if ((rangeMinDollar.floatValue() <= dollarAmount.floatValue()) &&
                            (dollarAmount.floatValue() <= rangeMaxDollar.floatValue())) {
                        minPoints = rangePointsFactor.multiply(dollarAmount);
                        break;
                    }
                }

                String rewardType = cr.getString("reward_type");
                BigDecimal maxPoints = cr.getBigDecimal("maximum_points");
                logger.debug("minPoints: " + minPoints);
                logger.debug("maxPoints: " + maxPoints);

                if (productVO == null) {
                    BigDecimal totalPoints = basePoints.add(bonusPoints).setScale(scale);

                    logger.debug("basePoints: " + basePoints);
                    logger.debug("bonusPoints: " + bonusPoints);
                    logger.debug("totalPoints: " + totalPoints);

                    if (totalPoints.floatValue() < minPoints.floatValue()) {
                        totalPoints = minPoints;
                    } else if (maxPoints != null && (totalPoints.floatValue() < maxPoints.floatValue())) {
                        totalPoints = maxPoints;
                    }

                    odVO.setRewardType(rewardType);
                    odVO.setMilesPoints(totalPoints);
                } else {
                    BigDecimal totalPoints = standardPoints.add(standardBonus).setScale(scale);
                    if (totalPoints.floatValue() < minPoints.floatValue()) {
                        totalPoints = minPoints;
                    } else if (maxPoints != null && (totalPoints.floatValue() < maxPoints.floatValue())) {
                        totalPoints = maxPoints;
                    }
                    productVO.setNetStandardPrice(totalPoints);

                    totalPoints = deluxePoints.add(deluxeBonus).setScale(scale);
                    if (totalPoints.floatValue() < minPoints.floatValue()) {
                        totalPoints = minPoints;
                    } else if (maxPoints != null && (totalPoints.floatValue() < maxPoints.floatValue())) {
                        totalPoints = maxPoints;
                    }
                    productVO.setNetDeluxePrice(totalPoints);

                    totalPoints = premiumPoints.add(premiumBonus).setScale(scale);
                    if (totalPoints.floatValue() < minPoints.floatValue()) {
                        totalPoints = minPoints;
                    } else if (maxPoints != null && (totalPoints.floatValue() < maxPoints.floatValue())) {
                        totalPoints = maxPoints;
                    }
                    productVO.setNetPremiumPrice(totalPoints);
                    
                    productVO.setRewardType(rewardType);
                }

            }

        } catch (Exception e) {
            logger.error(e);
        }
    }

    private static BigDecimal calculateReward(String calculationBasis, BigDecimal pointsFactor, 
        BigDecimal dollarOrderTotal, BigDecimal dollarMerchandise, int rewardScale) {

        int rewardRoundMethod = BigDecimal.ROUND_HALF_UP;

        // Clean the inputs.
        pointsFactor = (pointsFactor == null) ? new BigDecimal(0) : pointsFactor;
        dollarOrderTotal = (dollarOrderTotal == null) ? new BigDecimal(0) : dollarOrderTotal;
        dollarMerchandise = (dollarMerchandise == null) ? new BigDecimal(0) : dollarMerchandise;

        logger.debug("calculateReward(" + calculationBasis + ", " + pointsFactor.toString() + ", " +
            dollarOrderTotal.toString() + ", " + dollarMerchandise.toString() + ")");

        BigDecimal points = new BigDecimal(0);

        if (pointsFactor.floatValue() > 0) {
            // Let's calculate the base points first.
            BigDecimal baseDollarAmount = new BigDecimal(0);

            if (calculationBasis.equalsIgnoreCase("F")) {
                baseDollarAmount = new BigDecimal(1);
            } else if (calculationBasis.equalsIgnoreCase("M")) {
                baseDollarAmount = dollarMerchandise;
            } else if (calculationBasis.equalsIgnoreCase("T")) {
                baseDollarAmount = dollarOrderTotal;
            } else {
                logger.error("Unrecognized calculation basis: " + calculationBasis);
                baseDollarAmount = new BigDecimal(0);
            }

            // This is the base points value.
            points = pointsFactor.multiply(baseDollarAmount);
        }

        return points.setScale(rewardScale, rewardRoundMethod);
    }


}