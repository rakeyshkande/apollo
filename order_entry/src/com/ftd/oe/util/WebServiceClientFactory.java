package com.ftd.oe.util;

import java.net.MalformedURLException;
import java.net.URL;

import com.ftd.avs.webservice.AddressVerificationService;
import com.ftd.avs.webservice.AddressVerificationServiceBingImplService;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This class should be used to fetch web service client instances
 * 
 * @author cjohnson
 *
 */
public class WebServiceClientFactory {
	
	private static final Logger log = new Logger(WebServiceClientFactory.class.getName());
	
	private static AddressVerificationService addressVerificationSerivce;
	private static String avsURL;
	
	
	public void setGeocodeServiceURL(String avsURL) {
		this.avsURL = avsURL;
	}
	
	
	public static AddressVerificationService getAddressVerificationService() throws CacheException, Exception {
		GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);

		if (addressVerificationSerivce == null || 
				(avsURL != null && !avsURL.equals(gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL")) )) {
			
			//this should be in global parms somewhere
			avsURL = gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL");
			//avsURL = "http://localhost/avs/services/AVS?wsdl";
			
			try {
				URL avsServiceURL = new URL(avsURL);
				AddressVerificationServiceBingImplService avs = new AddressVerificationServiceBingImplService(avsServiceURL);
				addressVerificationSerivce = avs.getAddressVerificationServiceBingImplPort();
			} catch (MalformedURLException e) {
				log.error("Error when getting bing web service: " + e.getMessage() );
				e.printStackTrace();
			}
		}
		
		return addressVerificationSerivce;
	}

}
