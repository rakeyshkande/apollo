package com.ftd.oe.test;

import com.ftd.oe.common.resources.IResourceProvider;

import com.ftd.oe.common.resources.TestResourceProviderImpl;

import java.sql.Connection;

import junit.framework.TestCase;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class OETestCase extends TestCase {
    Connection conn;
    ApplicationContext context;
    IResourceProvider resourceProvider = new TestResourceProviderImpl();
    
    public OETestCase(String testCase) {
        super(testCase);
    }

    /**
     * Override setup() to initialize variables
     **/
    protected void setUp() throws Exception {
        //Start up spring framework
        context = 
                new FileSystemXmlApplicationContext("C:/_RLSE_CURRENT_DEV/FTD/order_entry/web/WEB-INF/spring-oe-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection("WOESDS");
    }

    /**
     * Override tearDown() to release any permanent resources you allocated in setUp
     */
    public void tearDown() throws Exception {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }
}
