// <![CDATA[
var IOTW = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("IOTW requires the Prototype JavaScript framework >= 1.5");
      
IOTW.OBJECT = Class.create();

IOTW.OBJECT.prototype = {

    initialize: function() {
        this.reset();
        this.iotwListBody = 'listBody';
        this.iotwListRowPrefix = 'iotwRow';
        this.iotwListSourceCodePrefix = 'sourceCode';
        this.iotwListProductIdPrefix = 'productId';
        this.iotwListProductAvailablePrefix = 'productAvailable';
        this.iotwListStatusPrefix = 'status';
        this.iotwListDateRangePrefix = 'dateRange';
        this.iotwListDeliveryDateRestrictionPrefix = 'deliveryDateRestriction';
        this.iotwListProductPageMessagePrefix = 'productPageMessage';
        this.iotwListCalendarMessagePrefix = 'calendarMessage';
        this.iotwListPromotionDiscountPrefix = 'promotionDiscount';
        this.iotwListIotwSourceCodePrefix = 'iotwSourceCode';
        this.iotwListSpecialOfferPrefix = 'specialOffer';
        this.iotwListWordingColorPrefix = 'wordingColor';
        this.iotwListEditPrefix = 'edit';
        this.iotwListRemovePrefix = 'removeCheckbox';
    },
    
    reset: function() {
        this.iotwId='';
        this.sourceCodes='';
        this.sourceCode='';
        this.productIds='';
        this.productId='';
        this.productAvailable=false;
        this.iotwSourceCode='';
        this.startDate='';
        this.endDate='';
        this.status=false;
        this.dateRange='';
        this.deliveryDates=new Array();
        this.specialOffer=false;
        this.promotionDiscount='';
        this.wordingColor='';
        this.productPageMessage='';
        this.calendarMessage='';
    }
};