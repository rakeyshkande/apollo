//Begin element colors
var v_normalBgColor = "white";
var v_errorBgColor = "red";
var v_normalTextColor = "black";
var v_errorTextColor = "white";
//End element colors

// Uncomment if you want to see the progress bar during page load
/*
var initProcesses = 3;
var initProcessesCompleted = 0;
*/

var v_iotwFields = new Array();
var v_iotwListItems = new Array();
var startDateCal, endDateCal, deliveryDatesCal;
var v_removeFields = new Array();
var v_current_iotwObj;
var searchProductId = '';
var searchOverallSourceCode = '';
var searchIOTWSourceCode = '';
var searchExpiredOnly = '';
var v_modifyOrderFromCom = false;

// <![CDATA[
if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("iotw.js requires the Prototype JavaScript framework >= 1.5");
      
var v_iotwList = new Array();

function init() {
    // Uncomment if you want to see the progress bar during page load
    //IOTW.displayProcessing();

    IOTW.createCalendars();
    IOTW.getFieldData();
    IOTW.getNovatorUpdateSetup();

    Event.observe("exitButton","click",IOTW_EVENTS.onExitButton);
    Event.observe("backButton","click",IOTW_EVENTS.onBackButton);
    Event.observe("saveButton","click",IOTW_EVENTS.onSaveButton);
    Event.observe("removeSelectedButton","click",IOTW_EVENTS.onRemoveSelectedButton);
    Event.observe("removeButton","click",IOTW_EVENTS.onRemoveButton);
    Event.observe("resetButton","click",IOTW_EVENTS.onResetButton);
    Event.observe("addButton","click",IOTW_EVENTS.onAddButton);
    Event.observe("sendFeedAllButton","click",IOTW_EVENTS.onSendFeedAllButton);
    Event.observe("closeErrorButton","click",IOTW_EVENTS.onCloseErrorButton);
    Event.observe("viewErrorsButton","click",IOTW_EVENTS.onViewErrorButton);
    Event.observe("viewListErrorsButton","click",IOTW_EVENTS.onViewErrorButton);
    Event.observe("startDateCalButton","click",IOTW_EVENTS.onStartDateClick);
    Event.observe("endDateCalButton","click",IOTW_EVENTS.onEndDateClick);
    Event.observe("deliveryDatesCalButton","click",IOTW_EVENTS.onDeliveryDatesClick);
    Event.observe("searchButton","click",IOTW_EVENTS.onSearchButton);
    
    if (v_expiredOnly == 'Y') {
        document.getElementById('expiredOnlySearch').checked = true;
    }
}

IOTW.createCalendars = function() {
    startDateCal = new YAHOO.widget.Calendar("startDateCal","startDateCalContainer"); 
    startDateCal.selectEvent.subscribe(calendarDateSelectHandler, startDateCal, true);
    startDateCal.deselectEvent.subscribe(calendarDateSelectHandler, startDateCal, true);
    setGenericCalendarPropertiesNoMin(startDateCal);
    startDateCal.render(); 
    
    endDateCal = new YAHOO.widget.Calendar("endDateCal","endDateCalContainer"); 
    endDateCal.selectEvent.subscribe(calendarDateSelectHandler, endDateCal, true);
    endDateCal.deselectEvent.subscribe(calendarDateSelectHandler, endDateCal, true);
    setGenericCalendarPropertiesNoMin(endDateCal);
    endDateCal.render(); 

    deliveryDatesCal = new YAHOO.widget.Calendar("deliveryDatesCal","deliveryDatesCalContainer"); 
    deliveryDatesCal.selectEvent.subscribe(calendarDateSelectHandler, deliveryDatesCal, true);
    deliveryDatesCal.deselectEvent.subscribe(calendarDateSelectHandler, deliveryDatesCal, true);
    deliveryDatesCal.cfg.setProperty("MULTI_SELECT", true);
    setGenericCalendarPropertiesNoMin(deliveryDatesCal);
    deliveryDatesCal.render(); 
    
    startDateCal.hide();
    endDateCal.hide();
    deliveryDatesCal.hide();
    
    // Uncomment if you want to see the progress bar during page load
    //IOTW.initProcessingCompleted();
}

// Uncomment if you want to see the progress bar during page load
/*
IOTW.initProcessingCompleted = function(){
    initProcessesCompleted += + 1;
    if(initProcessesCompleted == initProcesses){
        IOTW.closeProcessing();
    }
}
*/

IOTW.validate = function() {
    var retval = true;
    var strHtml = '';
    var idx;
    var el;
    
    for( idx = 0; idx < v_iotwFields.length; idx++ ) {
        el = $(v_iotwFields[idx].elementId);
        if( v_iotwFields[idx].validate(false) == false ) {
            strHtml += "<a href=\"javascript:setFocus($('"+el.id+"'));\">";
            if( v_iotwFields[idx].labelText != null ) {
                strHtml += v_iotwFields[idx].labelText;
                strHtml += ": ";
            }
            strHtml += v_iotwFields[idx].errorText+"</a><br>";
            retval = false;
        }
        
        v_iotwFields[idx].setFieldStyle();
    }
    return retval;
}

IOTW.getFieldData = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_ELEMENT_DATA';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    newRequest = 
        new FTD_AJAX.Request('iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.setFieldDataCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

IOTW.setFieldDataCallback = function(transport) {
    xmlDoc = parseResponse(transport);
    if( xmlDoc!=null ) {
        records = XPath.selectNodes('/result/rs[@name="element_config"]/record[type="HEADER"]',xmlDoc);
        v_iotwFields = new Array();
        setElementConfig(records,v_iotwFields);
    }
    // Uncomment if you want to see the progress bar during page load
    //IOTW.initProcessingCompleted();
}

IOTW.getIOTW = function(iotwId) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_IOTW';
    root.setAttributeNode(attr);

    param = FTD_XML.createElementWithText(doc,'param',iotwId+'');
    attr = doc.createAttribute("name");
    attr.value = 'IOTW_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    newRequest = 
        new FTD_AJAX.Request('iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.getIOTWCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

IOTW.getIOTWCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    var iotwArray;
    if( xmlDoc != null ) {
        iotwArray = IOTW.parseIOTWXML(xmlDoc);
        new Effect.SlideUp("viewErrorButtonsDiv");
        IOTW.populateIotwEditPanel(iotwArray[0]);
        v_current_iotwObj = iotwArray[0];
        IOTW.openIotwEditPanel();
    }
}

IOTW.openIotwEditPanel = function() {
    new Effect.SlideUp("iotwListDiv");
    new Effect.SlideDown("iotwEditDiv");
}

IOTW.populateIotwEditPanel = function(iotwObj) {
    if(iotwObj != null) {
        $('sourceCodes').value = iotwObj.sourceCode;
        $('productIds').value = iotwObj.productId;
        $('iotwSourceCode').value = iotwObj.iotwSourceCode;
        $('specialOffer').checked = iotwObj.specialOffer;
        $('wordingColor').value = iotwObj.wordingColor;
        $('productPageMessage').value = iotwObj.productPageMessage;
        $('calendarMessage').value = iotwObj.calendarMessage;
        $('deliveryDates').options.length = 0;
        clearDates(startDateCal);
        clearDates(endDateCal);
        clearDates(deliveryDatesCal);
        resetDeselectMap();
        setDates(startDateCal, new Array(iotwObj.startDate));
        if(iotwObj.endDate != null && iotwObj.endDate != '')
            setDates(endDateCal, new Array(iotwObj.endDate));
        if(iotwObj.deliveryDates != null && iotwObj.deliveryDates.length > 0)
            setDates(deliveryDatesCal, iotwObj.deliveryDates);
    } else {
        IOTW.clearIotwEditPanel();
    }
}

IOTW.clearIotwEditPanel = function() {
    $('iotwDisplayed').value = -1;
    $('sourceCodes').value = '';
    $('productIds').value = '';
    $('iotwSourceCode').value = '';
    $('specialOffer').checked = false;
    $('wordingColor').value = '';
    $('productPageMessage').value = '';
    $('calendarMessage').value = '';
    $('deliveryDates').options.length = 0;
    clearDates(startDateCal);
    clearDates(endDateCal);
    clearDates(deliveryDatesCal);
}

IOTW.closeIotwEditPanel = function() {
    new Effect.SlideUp("iotwEditDiv");
    new Effect.SlideDown("iotwListDiv");
}

IOTW.save = function()
{
    IOTW.displayProcessing();

    var iotwObjArray = new Array();
    iotwObjArray[0] = IOTW.buildIOTWObject();
    var iotwDoc = IOTW.generateIOWTXML(iotwObjArray, false);
    var existingIOTWObjArray = new Array();

    var iotwObj;    
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SAVE_IOTW';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(iotwDoc));
    attr = doc.createAttribute("name");
    attr.value = 'IOTWS';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    IOTW.addEditFeedParameters(doc, root);
    IOTW.addIdentityParameters(doc, root);

    if(v_current_iotwObj != null) {
        existingIOTWObjArray[0] = v_current_iotwObj;    
        var existingIOTWDoc = IOTW.generateIOWTXML(existingIOTWObjArray, false);
        param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(existingIOTWDoc));
        attr = doc.createAttribute("name");
        attr.value = 'EXISTING_IOTW';
        param.setAttributeNode(attr);
        root.appendChild(param);
    }

    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.saveCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

IOTW.saveCallback = function(transport)
{
    $('saveButton').disabled = false;
    IOTW.closeProcessing();
    
    var xmlDoc = parseResponse(transport);
    if(xmlDoc != null) {
        // If no validation errors exist 
        if( IOTW.checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcdNotify') ){
            IOTW.redirectToIOTW();    
        } else {
            new Effect.SlideDown("viewErrorButtonsDiv");
        }
    }
}

IOTW.removeSelected = function() {
    IOTW.displayProcessing();
    if (typeof document.forms[0].removeCheckbox == 'object')
	{
    var cntr = 0;
    var iotwObjArray = new Array();
    var iotwObj = new IOTW.OBJECT();
    // When only one check box is checked, document.forms[0].removeCheckbox.length is undefined
    var removeCheckboxLength = document.forms[0].removeCheckbox.length;
	if (removeCheckboxLength == undefined)
	{
		removeCheckboxLength = 1;
	} 
    for (var i = 0; i < removeCheckboxLength ; i++) { 
		if(removeCheckboxLength == 1 && document.forms[0].removeCheckbox.checked == true) {	
			iotwObj = new IOTW.OBJECT();
            iotwObj.iotwId = document.forms[0].removeCheckbox.value;   
            iotwObjArray[cntr] = iotwObj;           
		} else if (removeCheckboxLength > 1 && document.forms[0].removeCheckbox[i].checked == true) { 
			iotwObj = new IOTW.OBJECT();
            iotwObj.iotwId = document.forms[0].removeCheckbox[i].value;  
            iotwObjArray[cntr] = iotwObj;
            cntr++;
        }
    }

    if(iotwObjArray.length != 0) {
        var iotwDoc = IOTW.generateIOWTXML(iotwObjArray, true);

        var doc = FTD_XML.createXmlDocument("request");
        var root = doc.documentElement;
        var attr = doc.createAttribute("type");
        attr.value = 'AJAX_REQUEST_REMOVE_IOTW';
        root.setAttributeNode(attr);
    
        var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(iotwDoc));
        attr = doc.createAttribute("name");
        attr.value = 'IOTWS';
        param.setAttributeNode(attr);
        root.appendChild(param);
        
        IOTW.addFeedParameters(doc, root);
        IOTW.addIdentityParameters(doc, root);
    
        //XMLHttpRequest
        var newRequest = 
            new FTD_AJAX.Request(v_serverURLPrefix+'iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.removeSelectedCallback,false,false);
        newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
        newRequest.send();
    } else {
        IOTW.closeProcessing();
        alert("You must choose an IOTW program to remove.");
    }
	}
    else {
        IOTW.closeProcessing();
        alert("There are no IOTW programs found.");
    }
}

IOTW.removeSelectedCallback = function(transport) {
    IOTW.closeProcessing();

    var xmlDoc = parseResponse(transport);
    if(xmlDoc != null){
        // If no validation errors exist 
        if( IOTW.checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcdNotify') ){
            IOTW.redirectToIOTW();
        } else {
            new Effect.SlideDown("viewListErrorButtonsDiv");
        }
    }
}

IOTW.remove = function() {
    if(v_current_iotwObj != null) {
        var result = confirm("The IOTW program you are editing will be deleted");
        if(result){        
            IOTW.displayProcessing();
            var iotwObjArray = new Array();
            iotwObjArray[0] = v_current_iotwObj;
            
            var iotwDoc = IOTW.generateIOWTXML(iotwObjArray, true);
    
            var doc = FTD_XML.createXmlDocument("request");
            var root = doc.documentElement;
            var attr = doc.createAttribute("type");
            attr.value = 'AJAX_REQUEST_REMOVE_IOTW';
            root.setAttributeNode(attr);
        
            var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(iotwDoc));
            attr = doc.createAttribute("name");
            attr.value = 'IOTWS';
            param.setAttributeNode(attr);
            root.appendChild(param);
            
            IOTW.addEditFeedParameters(doc, root);
            IOTW.addIdentityParameters(doc, root);
        
            //XMLHttpRequest
            var newRequest = 
                new FTD_AJAX.Request(v_serverURLPrefix+'iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.removeCallback,false,false);
            newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
            newRequest.send();
        }
    } else {
        alert("You must be editing a program to remove it.");
    }
}

IOTW.removeCallback = function(transport)
{
    IOTW.closeProcessing();
    var xmlDoc = parseResponse(transport);
    if(xmlDoc != null) {
        // If no validation errors exist 
        if( IOTW.checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcdNotify') ){
            IOTW.redirectToIOTW();    
        } else {
            new Effect.SlideDown("viewErrorButtonsDiv");
        }
    }
}

IOTW.sendFeedAll = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SEND_FEED_ALL';
    root.setAttributeNode(attr);

    var feedExists = IOTW.addFeedParameters(doc, root);

    if(feedExists){
        var result = confirm("All IOTW programs will be sent to Novator");
        if(result){
            $('sendFeedAllButton').disabled = true;
            IOTW.displayProcessing();
            //XMLHttpRequest
            var newRequest = 
                new FTD_AJAX.Request(v_serverURLPrefix+'iotwAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.sendFeedAllCallback,false,false);
            newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
            newRequest.send();
        }
    }else{
        alert("A Novator environment must be chosen to send a feed.");
    }
}

IOTW.sendFeedAllCallback = function(transport) {
    $('sendFeedAllButton').disabled = false;
    IOTW.closeProcessing();

    var xmlDoc = parseResponse(transport);
    if(xmlDoc != null){
        // If no validation errors exist 
        if( IOTW.checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcdNotify') ){
            new Effect.SlideUp("viewListErrorButtonsDiv");
            alert("All programs were sent to Novator.");
        } else {
            new Effect.SlideDown("viewListErrorButtonsDiv");
        }
    }
}

IOTW.buildIOTWObject = function()
{
    var iotwObj = new IOTW.OBJECT();
    iotwObj.sourceCodes = $('sourceCodes').value;
    iotwObj.productIds = $('productIds').value;
    iotwObj.iotwSourceCode = $('iotwSourceCode').value;
    iotwObj.startDate = $('startDate').value;
    iotwObj.endDate = $('endDate').value;
    iotwObj.specialOffer = $('specialOffer').checked + '';
    iotwObj.wordingColor = $('wordingColor').value;
    iotwObj.productPageMessage = $('productPageMessage').value;
    iotwObj.calendarMessage = $('calendarMessage').value;

    iotwObj.deliveryDates = new Array();
    var delDates = $('deliveryDates');
    for(var i = 0; i < delDates.options.length; i++)
    {
        iotwObj.deliveryDates[i] = delDates.options[i].value;
    }

    /*******Keep for debugging
    alert(iotwObj.sourceCodes + ':sourceCodes');
    alert(iotwObj.productIds + ':productIds');
    alert(iotwObj.iotwSourceCode + ':iotwSourceCode');
    alert(iotwObj.startDate + ':startDate');
    alert(iotwObj.endDate + ':endDate');
    for(var i = 0; i < iotwObj.deliveryDates.length; i++)
    {
        alert(iotwObj.deliveryDates[i]);
    }
    alert(iotwObj.specialOffer + ':specialOffer');
    alert(iotwObj.wordingColor + ':wordingColor');
    alert(iotwObj.productPageMessage + ':productPageMessage');
    alert(iotwObj.calendarMessage + ':calendarMessage');    
    *******Keep for debugging*/

    return iotwObj;
}

IOTW.generateIOWTXML = function(iotwObjArray, onlyKeyData) {
    var doc = FTD_XML.createXmlDocument("root");
    var root = doc.documentElement;
    for(var i = 0; i < iotwObjArray.length; i++)
    {
        iotwObj = iotwObjArray[i];
        
        var iotwElement = doc.createElement('IOTW');
        root.appendChild(iotwElement);
        
        var element = FTD_XML.createElementWithText(doc,'IOTW_ID',iotwObj.iotwId);
        iotwElement.appendChild(element);

        element = FTD_XML.createElementWithText(doc,'SOURCE_CODE',iotwObj.sourceCode);
        iotwElement.appendChild(element);
    
        element = FTD_XML.createElementWithText(doc,'PRODUCT_ID',iotwObj.productId);
        iotwElement.appendChild(element);
        
        if(!onlyKeyData) {
            element = FTD_XML.createElementWithText(doc,'SOURCE_CODES',iotwObj.sourceCodes);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'PRODUCT_IDS',iotwObj.productIds);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'IOTW_SOURCE_CODE',iotwObj.iotwSourceCode);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'START_DATE',iotwObj.startDate);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'END_DATE',iotwObj.endDate);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'SPECIAL_OFFER',iotwObj.specialOffer);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'WORDING_COLOR',iotwObj.wordingColor);
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'PRODUCT_PAGE_MESSAGE',encodeURIComponent(iotwObj.productPageMessage));
            iotwElement.appendChild(element);

            element = FTD_XML.createElementWithText(doc,'CALENDAR_MESSAGE',encodeURIComponent(iotwObj.calendarMessage));
            iotwElement.appendChild(element);
            
            delDatesElement = doc.createElement('DELIVERY_DATES');
            iotwElement.appendChild(delDatesElement);
            
            for(var j = 0; iotwObj.deliveryDates != null && j < iotwObj.deliveryDates.length; j++) {                
                var delDateElement = FTD_XML.createElementWithText(doc,'DELIVERY_DATE',iotwObj.deliveryDates[j]);
                delDatesElement.appendChild(delDateElement);
            }
        }
    }
    return doc;
}


IOTW.parseIOTWXML = function(xmlDoc) {
    var iotwArray = new Array();
    var iotwMap = new Object();
    var iotwObj;
    var i = 0;
    var value;
    var records = XPath.selectNodes('/result/rs[@name="IOTW_CUR"]/record',xmlDoc);
    for( i = 0; i < records.length; i++ ) {
        iotwObj = new IOTW.OBJECT;
        iotwObj.iotwId = FTD_XML.selectNodeText(records[i],'iotw_id');
        iotwObj.sourceCode = FTD_XML.selectNodeText(records[i],'source_code');
        iotwObj.productId = FTD_XML.selectNodeText(records[i],'product_id');
        iotwObj.startDate = FTD_XML.selectNodeText(records[i],'start_date_fmt');

        value = FTD_XML.selectNodeText(records[i],'end_date_fmt');
        if(value != null)
            iotwObj.endDate = value;
        value = FTD_XML.selectNodeText(records[i],'product_page_messaging_txt');
        if(value != null)
            iotwObj.productPageMessage = value;
        value = FTD_XML.selectNodeText(records[i],'calendar_messaging_txt');
        if(value != null)
            iotwObj.calendarMessage = value;
        value = FTD_XML.selectNodeText(records[i],'iotw_source_code');
        if(value != null)
           iotwObj.iotwSourceCode = value;
        value = FTD_XML.selectNodeText(records[i],'special_offer_flag');
        if(value != null)
         iotwObj.specialOffer = value == 'Y'?true:false;
        value = FTD_XML.selectNodeText(records[i],'wording_color_txt');
        if(value != null)
            iotwObj.wordingColor = value;
        value = FTD_XML.selectNodeText(records[i],'product_available');
        if(value != null)
            iotwObj.productAvailable = value;

        iotwArray[i] = iotwObj;
        iotwMap[iotwObj.iotwId] = iotwObj;
    }
    records = XPath.selectNodes('/result/rs[@name="DEL_DISC_DATE_CUR"]/record',xmlDoc);
    for( i = 0; i < records.length; i++ ) {
        id = FTD_XML.selectNodeText(records[i],'iotw_id');
        discount_date = FTD_XML.selectNodeText(records[i],'discount_date_fmt');
        var length = iotwMap[id].deliveryDates.length;
        iotwMap[id].deliveryDates[length] = discount_date;
    }
    return iotwArray;
}

IOTW.redirectToIOTW = function() {
    searchOverallSourceCode = document.forms[0].overallSourceSearch.value;
    searchIOTWSourceCode = document.forms[0].iotwSourceSearch.value;
    searchProductId = document.forms[0].productIdSearch.value;
    searchExpiredOnly = document.forms[0].expiredOnlySearch.checked;
    var url = v_serverURLPrefix + 'iotw.do' + getSecurityParams(true, '') + '&' + v_currentPage + '&' + v_currentSort;
    url = url + '&productId=' + searchProductId + '&sourceCode=' + searchOverallSourceCode + '&IOTWSourceCode=' + searchIOTWSourceCode;
    url = url + '&expiredOnly=' + searchExpiredOnly;
    window.location = url; 
}

IOTW.addFeedParameters = function(doc, root) {
    var toLive = $('liveFeed').getValue();
    var param = FTD_XML.createElementWithText(doc,'param',toLive);
    var attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_LIVE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toContent = $('contentFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toContent);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_CONTENT';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toTest = $('testFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toTest);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_TEST';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toUAT = $('uatFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toUAT);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_UAT';
    param.setAttributeNode(attr);
    root.appendChild(param);    
    
    if(toLive || toContent || toTest || toUAT){
        return true;
    } else{
        return false;
    }
}

IOTW.addEditFeedParameters = function(doc, root) {
    var toLive = $('liveEditFeed').getValue();
    var param = FTD_XML.createElementWithText(doc,'param',toLive);
    var attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_LIVE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toContent = $('contentEditFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toContent);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_CONTENT';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toTest = $('testEditFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toTest);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_TEST';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toUAT = $('uatEditFeed').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toUAT);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_UAT';
    param.setAttributeNode(attr);
    root.appendChild(param);    
}

IOTW.addIdentityParameters = function(doc, root) {
    var param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    var attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
}

IOTW.getNovatorUpdateSetup = function()
{
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_NOVATOR_UPDATE_SETUP';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('commonUtilAjax.do',FTD_AJAX_REQUEST_TYPE_POST,IOTW.getNovatorUpdateSetupCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}
 
IOTW.getNovatorUpdateSetupCallback = function(transport) {
    var contentFeedAllowed;
    var testFeedAllowed;
    var uatFeedAllowed;
    var productionFeedAllowed;
    var contentFeedChecked;
    var testFeedChecked;
    var uatFeedChecked;
    var productionFeedChecked;
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        var records = XPath.selectNodes('/result/rs[@name="novator_update_setup"]/record',xmlDoc);
        v_companies = new Array();
        var idx;
        
        for(idx=0; idx<records.length; idx++ ) 
        {
            contentFeedAllowed = FTD_XML.selectNodeText(records[idx],'content_feed_allowed');
            testFeedAllowed = FTD_XML.selectNodeText(records[idx],'test_feed_allowed');
            uatFeedAllowed = FTD_XML.selectNodeText(records[idx],'uat_feed_allowed');
            productionFeedAllowed = FTD_XML.selectNodeText(records[idx],'production_feed_allowed');
            contentFeedChecked = FTD_XML.selectNodeText(records[idx],'content_feed_checked');
            testFeedChecked = FTD_XML.selectNodeText(records[idx],'test_feed_checked');
            uatFeedChecked = FTD_XML.selectNodeText(records[idx],'uat_feed_checked');
            productionFeedChecked = FTD_XML.selectNodeText(records[idx],'production_feed_checked');
        }
        
        IOTW.setupCheckBox('contentEditFeed',contentFeedAllowed, contentFeedChecked);
        IOTW.setupCheckBox('testEditFeed',testFeedAllowed, testFeedChecked);
        IOTW.setupCheckBox('uatEditFeed',uatFeedAllowed, uatFeedChecked);
        IOTW.setupCheckBox('liveEditFeed',productionFeedAllowed, productionFeedChecked);
        IOTW.setupCheckBox('contentFeed',contentFeedAllowed, contentFeedChecked);
        IOTW.setupCheckBox('testFeed',testFeedAllowed, testFeedChecked);
        IOTW.setupCheckBox('uatFeed',uatFeedAllowed, uatFeedChecked);
        IOTW.setupCheckBox('liveFeed',productionFeedAllowed, productionFeedChecked);
    }    
    // Uncomment if you want to see the progress bar during page load
    //IOTW.initProcessingCompleted();
}
 
IOTW.setupCheckBox = function(idName, allowed, checked)
{
        var checkBox = $(idName);
        checkBox.checked = false;
 
        if (allowed != null && allowed == 'YES')
        {
            checkBox.disabled = false;
            if (checked != null && checked == 'YES'){
                checkBox.checked = true;
            }
        }
        else
        {
            checkBox.disabled = true;
        }
}

IOTW.checkResultSetStatus = function(xmlDoc,resultSetName,silent,popup,failOnNullStatus, notifyDivPrefix) {
    $(notifyDivPrefix + 'Area').innerHTML = '';
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = true;
    var errorMsg = '';
    
    if( resultSet==null ) {
        retval=false;
        errorMsg='The server did not return the required data for '+resultSetName+'.  Contact support';
    } else {
        var status = FTD_XML.getAttributeText(resultSet,'status');
        if( (status==null&&failOnNullStatus==false) || status=='Y' ) {
            retval=true;
        } else {
            retval=false;
            var records = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]/record/validate',xmlDoc);
            for(idx=0; idx < records.length; idx++ ) 
            {
                valMsg = FTD_XML.getAttributeText(records[idx],'message');
                errorMsg = errorMsg + '<BR/>' + valMsg;
            }
        }
    }
    
    if( retval==false && silent==false ) {
        if( popup==true ) {
            alert(errorMsg);
        } else {
            IOTW.notifyUser(errorMsg,false,false,notifyDivPrefix);
        }
    }
    
    return retval;
}

IOTW.notifyUser = function(message,appendFlag,startTimer, notifyDivPrefix) {
    if( (appendFlag && appendFlag==true) || $(notifyDivPrefix + 'DivId').visible() ) {
        oldMsg = $(notifyDivPrefix + 'Area').innerHTML;
        $(notifyDivPrefix + 'Area').innerHTML = oldMsg+'<br>'+message
    } else {
        $(notifyDivPrefix + 'Area').innerHTML = message;
    }
    
    if( !($(notifyDivPrefix + 'DivId').visible()) ) {
      new Effect.SlideDown(notifyDivPrefix + 'DivId');
    }
    
    if( startTimer ) {
        if( startTimer==true )
            v_notifyTimer.startTimer();
        else if( v_notifyTimer.isRunning() )
            v_notifyTimer.stopTimer();
    } 
}

IOTW.displayProcessing = function(event){
    $(actionButtonProcessing).style.visibility = "visible"; 

    // Keep around in case they want a popup window instead of the footer progress bar
    //popupWindow = window.open("","popupwindow","width=16px,height=200px");
    //popupWindow.document.write("<html><head><title>Processing Request...</title></head><body><img src=images/pleasewait.gif height=16px width=200px/></body></html>");
}

IOTW.closeProcessing = function(event){
    $(actionButtonProcessing).style.visibility = "hidden"; 

    // Keep around in case they want a popup window instead of the footer progress bar
    //popupWindow.close();
}

function unload() {
}

// ]]>