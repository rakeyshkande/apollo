// <![CDATA[
var IOTW_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("IOTW_EVENTS requires the Prototype JavaScript framework >= 1.5");
      
if(typeof FTD_DOM=='undefined')
      throw("IOTW_EVENTS requires FTD_DOM");
      
IOTW_EVENTS.onAddButton = function(event) {
    IOTW.openIotwEditPanel();
    IOTW.clearIotwEditPanel();
};

IOTW_EVENTS.onSendFeedAllButton = function(event) {
    IOTW.sendFeedAll();    
};

IOTW_EVENTS.onEditButton = function(iotwId) {
    IOTW.getIOTW(iotwId);
};
      
IOTW_EVENTS.onResetButton = function(event) {
    IOTW.populateIotwEditPanel(v_current_iotwObj);
};

IOTW_EVENTS.onRemoveSelectedButton = function(event) {
    var result = confirm("The IOTW programs you have selected will be deleted");
    if(result){
        IOTW.removeSelected();
    }
};

IOTW_EVENTS.onRemoveButton = function(event) {
    IOTW.remove();
};

IOTW_EVENTS.onBackButton = function(event) {
    IOTW.redirectToIOTW();    
};
      
IOTW_EVENTS.onSaveButton = function(event) {
    $('saveButton').disabled = true;
    if(IOTW.validate()) {
        IOTW.save();
    }else{
        $('saveButton').disabled = false;
    }
};

IOTW_EVENTS.onCloseErrorButton = function(event) {
    new Effect.SlideUp("xcdNotifyDivId");
}

IOTW_EVENTS.onViewErrorButton = function(event) {
    new Effect.SlideDown("xcdNotifyDivId");
}

IOTW_EVENTS.onStartDateClick = function(event) {
    manageCalendarDisplay($('startDateCalContainer'), startDateCal);
}

IOTW_EVENTS.onEndDateClick = function(event) {
    manageCalendarDisplay($('endDateCalContainer'), endDateCal);
}

IOTW_EVENTS.onDeliveryDatesClick = function(event) {
    manageCalendarDisplay($('deliveryDatesCalContainer'), deliveryDatesCal);
}

IOTW_EVENTS.onExitButton = function(event) {
    doMainMenuAction('');
};

IOTW_EVENTS.onSearchButton = function(event) {
    IOTW.redirectToIOTW();
};
// ]]>