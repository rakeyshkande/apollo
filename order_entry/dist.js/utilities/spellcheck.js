// <![CDATA[
var SPELL_CHECK = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1]),
  spellCheckObject: null
}

if((typeof Prototype=='undefined')) {
      throw("SPELL CHECK requires the Prototype JavaScript framework >= 1.5");
}

SPELL_CHECK.onChange = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.onChange(event);
    }
}

SPELL_CHECK.onChangeAll = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.onChangeAll(event);
    }
}
    
SPELL_CHECK.onIgnore = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.onIgnore(event);
    }
}

SPELL_CHECK.onIgnoreAll = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.onIgnoreAll(event);
    }
}


SPELL_CHECK.onChangeSuggestions = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.onChangeSuggestions(event);
    }
}

SPELL_CHECK.onKeyPressWord = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null && event.keyCode==13 ) {
        SPELL_CHECK.spellCheckObject.onKeyPressWord(event);
    }
}

SPELL_CHECK.onCompleted = function(event)
{
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject.completed();
    }
}
      
SPELL_CHECK.init = function(elementId,correctionOptions,options) {
    if( SPELL_CHECK.spellCheckObject!=null ) {
        SPELL_CHECK.spellCheckObject = null; //fix ie memory leak
    }
    
    SPELL_CHECK.spellCheckObject = new SPELL_CHECK.OBJECT(elementId,correctionOptions,options);
    SPELL_CHECK.spellCheckObject.openSpellCheck();
}

SPELL_CHECK.check = function(elementId,options) {
    if( elementId==null ) return;
    if( options==null ) options={};
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SPELL_CHECK';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',encodeURIComponent($(elementId).getValue()));
    attr = doc.createAttribute("name");
    attr.value = 'text';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'elementId',elementId));
    param.appendChild(FTD_XML.createElementWithText(doc,'options',Object.toJSON(options)));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'spellCheckAjax.do',FTD_AJAX_REQUEST_TYPE_POST,SPELL_CHECK.spellCheckCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

SPELL_CHECK.spellCheckCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var resultSet = XPath.selectNodes('/result/rs[@name="spellcheck"]',xmlDoc)[0];
        
        var options = FTD_XML.selectNodeText(echo,'options');
        try {
            options = options.evalJSON();
        } catch (err) {
            options = {};
        }
        
        var elementId = FTD_XML.selectNodeText(echo,'elementId');
        
        if( checkResultSetStatus(xmlDoc,"spellcheck",true,false) ) {
        
            if( options.successFunction ) {
                eval(options.successFunction+'(\''+elementId+'\')');
            } else {
                alert('No spelling errors found.');
            }
        } else {
            
            if( elementId!=null ) {
                var corrections;
                try { 
                    corrections = UTILS.convertToJavaScript(FTD_XML.getAttributeText(resultSet,'message'));
                } catch (err) {
                    corrections = [];
                }
                SPELL_CHECK.init(elementId,corrections,options);
            }
        }
    }
}
      
SPELL_CHECK.OBJECT = Class.create();

SPELL_CHECK.OBJECT.prototype = {
      
    initialize: function(elementId,correctionOptions,options) {
        this.elementId = elementId;
        this.text = $(elementId).getValue();
        this.original = this.text;
        this.corrections = correctionOptions;
        this.options = {};
        this.setOptions(options);
        this.current = 0;
        this.ignore = new Object();
        this.formatted = false;
    },

    setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
    },

    onChange: function(event)
    {
        if (this.current < this.corrections.size() ) 
        {
            this.changeWord(this.current);
            this.nextWord();
        }
    },

    onChangeAll: function(event)
    {
        if (this.current < this.corrections.size()) 
        {
            var currentWord = this.text.substring(this.corrections[this.current].start, this.corrections[this.current].end);
            this.changeWord(this.current);
            for (var i = this.current + 1; i < this.corrections.size(); i++) 
            {
                if (!this.ignore[i] && this.text.substring(this.corrections[i].start, this.corrections[i].end) == currentWord) 
                {
                    this.changeWord(i);
                    this.ignore[i] = true;
                }
            }
            this.nextWord();
        }
    },
    
    onIgnore: function(event)
    {
        if (this.current < this.corrections.size()) 
        {
            this.nextWord();
        }
    },

    onIgnoreAll: function(event)
    {
        if (this.current < this.corrections.size()) 
        {
            var currentWord = this.text.substring(this.corrections[this.current].start, this.corrections[this.current].end);
            for (var i = this.current + 1; i < this.corrections.size(); i++) 
            {
            if (!this.ignore[i] && this.text.substring(this.corrections[i].start, this.corrections[i].end) == currentWord) 
                {
                    this.ignore[i] = true;
                }
            }
            this.nextWord();
        }
    },

  
    onChangeSuggestions: function(event)
    {
        var suggestion = $('sc_suggestions').options[$('sc_suggestions').selectedIndex].text;
        if (suggestion != "no suggestions") 
        {
            $('sc_word').value = suggestion;
        }
    },

    onKeyPressWord: function(event)
    {
        if (event.keyCode == 13) 
        {
            this.onChange(event);
        }
    },

    nextWord: function()
    {
        while (this.current++ < this.corrections.size() && this.ignore[this.current]);
        this.update();
        if (this.current >= this.corrections.size()) 
        {
            $('sc_changeButton').disabled = true;
            $('sc_changeAllButton').disabled = true;
            $('sc_ignoreButton').disabled = true;
            $('sc_ignoreAllButton').disabled = true;
            this.completed();
        }
    },

    completed: function() 
    {
        alert("Spell check complete.");
        var parsedText = this.text.replace(/ <br> /g, "\n");
        $(this.elementId).setValue(parsedText);
        this.closeSpellCheck();
    },
    
    update: function()
    {
        var html = "";
        if (this.current < this.corrections.size()) 
        {
            html += this.text.substring(0, this.corrections[this.current].start);
            html += '<span id="highlight" style="font-weight:bold;color:red">';
            html += this.text.substring(this.corrections[this.current].start, this.corrections[this.current].end);
            html += '</span>';
            html += this.text.substring(this.corrections[this.current].end, this.text.length);
            $('sc_preview').innerHTML = html;
            
            $('sc_suggestions').options.length = 0;
            var n = this.corrections[this.current].suggestions.size();

            if (n == 0) 
            {
                $('sc_word').value = this.text.substring(this.corrections[this.current].start, this.corrections[this.current].end);
                $('sc_suggestions').options[0] = new Option("no suggestions");
            }
            else 
            {
                $('sc_word').value = this.corrections[this.current].suggestions[0];
                for (var i = 0; i < n; i++) 
                {
                    $('sc_suggestions').options[i] = new Option(this.corrections[this.current].suggestions[i]);
                }
                $('sc_suggestions').selectedIndex = 0;
            }
            
            $('sc_word').select();
        }
        else 
        {
            $('sc_preview').innerHTML = this.text;
            $('sc_word').value = "";
            $('sc_suggestions').options.length = 0;
            $('sc_suggestions').options[0] = new Option("no suggestions");
        }
    },

    changeWord: function(index)
    {
        var newText = "";
        newText += this.text.substring(0, this.corrections[index].start);
        newText += $('sc_word').value;
        newText += this.text.substring(this.corrections[index].end, this.text.length);
        this.adjustOffsets($('sc_word').value.length - this.text.substring(this.corrections[index].start, this.corrections[index].end).length, index + 1);
        this.text = newText;
    },

    adjustOffsets: function(delta, start)
    {
        for (i = start; i < this.corrections.size(); i++) 
        {
            this.corrections[i].start += delta;
            this.corrections[i].end += delta;
        }
    },
    
    openSpellCheck: function() {
        if( this.options.popup=='true' ) {
            $('sc_changeButton').disabled = false;
            $('sc_changeAllButton').disabled = false;
            $('sc_ignoreButton').disabled = false;
            $('sc_ignoreAllButton').disabled = false;
            
            if( this.options.openFunction ) {
                eval(this.options.openFunction+'()');
                
                this.update();
                
                if( this.corrections.size() == 0 ) {
                    this.completed();
                }
            }
        } 
        
        if( this.options.notifyFunction ) {
            eval(this.options.notifyFunction+'(\''+this.elementId+'\')');
        }
    },
    
    closeSpellCheck: function() {
        if( this.options.closeFunction ) {
            eval(this.options.closeFunction+'()');
        }
    }
}
// ]]>


