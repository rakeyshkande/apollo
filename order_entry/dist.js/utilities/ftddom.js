// <![CDATA[
/*
 *  Dependencies: prototype.js
 */

var FTD_DOM = {
  Version: '1.1',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || FTD_DOM.prototypeVersion < 1.5)
      throw("DOM Utility requires the Prototype JavaScript framework >= 1.5");

// This browser object will allow us to check the IE browser version
// use it like this: if (IEBrowser.Version() < 8) { do IE 6 stuff }
// in case it is not IE, the version # will always be 999
var IEBrowser = {
  Version: function() {
	  var version = 999; // we assume a sane browser
	  if (navigator.appVersion.indexOf("MSIE") != -1)
		  version = parseFloat(navigator.appVersion.split("MSIE")[1]);
	  return version;
  }
}


FTD_DOM.addEvent = function(elm, evType, fn, useCapture) {
    if( elm.addEventListener) {
        elm.addEventListener(evType, fn, useCapture);
        return true;
    } else if (elm.attachEvent) {
        var r = elm.attachEvent('on' + evType, fn);
        return r;
    } else {
        elm['on' + evType] = fn;
        return null;
    }
};

FTD_DOM.getStyle = function(element,styleProp)
{
    var value = element.style[styleProp.camelize()];
    if (!value) {
      if (document.defaultView && document.defaultView.getComputedStyle) {
        var css = document.defaultView.getComputedStyle(element, null);
        value = css ? css.getPropertyValue(styleProp) : null;
      } else if (element.currentStyle) {
        value = element.currentStyle[styleProp.camelize()];
      }
    }

    if (window.opera && ['left', 'top', 'right', 'bottom'].include(styleProp)) 
    {
        if (Element.getStyle(element, 'position') == 'static') { 
            value = 'auto';
        }
    }

    return value == 'auto' ? null : value;
};

FTD_DOM.findFirstInputFieldId = function(e) {
    if( e ) {
        var nodes = e.childNodes;
        for (var i = 0; i < nodes.length; i++) {
            if( (nodes[i].nodeName.toUpperCase() == 'INPUT' || 
                 nodes[i].nodeName.toUpperCase() == 'TEXTAREA' || 
                 nodes[i].nodeName.toUpperCase()=="SELECT") && 
                 nodes[i].id && !nodes[i].disabled ) {
                return nodes[i].id;
            }
            if( nodes[i].hasChildNodes() ) {
                var results = FTD_DOM.findFirstInputFieldId(nodes[i]);
                if( results && results.length > 0 ) {
                    return results;
                }
            }
        }
    }
    
    return undefined;
}

FTD_DOM.findFirstAncestorOfType = function(e,nodeType) {
    if( e && nodeType ) {
        var parent = e.parentNode;
        if( parent ) {
            var nodename = parent.nodeName;
            if( nodename == nodeType.toUpperCase() ) {
                return parent;
            } else {
                return FTD_DOM.findFirstAncestorOfType(parent,nodeType);
            }
        } else {
            return undefined;
        }
    }
    
    return undefined;
}

FTD_DOM.getAllDecendents = function(parent, childArray) {
    if( parent && childArray && parent.hasChildNodes() ) {
        var nodes = parent.childNodes;
        for (var i = 0; i < nodes.length; i++) {
            childArray.push(nodes[i]);
            FTD_DOM.getAllDecendents(nodes[i], childArray);
        }
    }
}

FTD_DOM.findDecendentsOfType = function(parent, cNodeName, cNodeType, childArray) {
    if( parent && cNodeName && childArray && parent.hasChildNodes() ) {
        var nodes = parent.childNodes;
        
        for (var i = 0; i < nodes.length; i++) {
            if( nodes[i].nodeName.toUpperCase() == cNodeName.toUpperCase() ) {
                if( cNodeType && cNodeType.length > 0 ) {
                    if( nodes[i].getAttribute('type') == cNodeType ) {
                        childArray.push(nodes[i]);
                    }
                } else {
                    childArray.push(nodes[i]);
                }
            }
            
            FTD_DOM.findDecendentsOfType(nodes[i], cNodeName, cNodeType, childArray);
        }
    }
}

FTD_DOM.removeAllTableRows = function(table) {
    var rows = table.rows;
    for( var i = rows.length-1; i>=0; i-- ) {
        table.deleteRow(i);
    }
}

FTD_DOM.clearSelect = function(selectElementId) {
    if( Object.isString(selectElementId) ) {
        var el = $(selectElementId);
    } else {
        el = selectElementId;
    }
    
    for( var i = el.length; i>=0; i-- ) {
        el.remove(i);
    }
}

FTD_DOM.insertOption = function(selectElementId,prompt,value,selected) {
    var x;
    if( Object.isString(selectElementId) ) 
        x=$(selectElementId);
    else 
        x=selectElementId;
        
    var newOption = new Option(prompt,value);
    if( selected==true )
        newOption.selected=true;
    else 
        newOption.selected=false;
        
    x.options[x.options.length] = newOption;
}

FTD_DOM.selectOptionByText = function(control,text) {
    if( typeof control == 'string' ) control = $(control);
    
    for( var i = control.options.length-1; i>=0; i-- ) {
        if(control.options[i].text == text) {
            control.options[i].selected = true;
            break;
        }
    }
}

FTD_DOM.selectOptionByValue = function(control,value) {
    if( typeof control == 'string' ) control = $(control);
    
    for( var i = control.options.length-1; i>=0; i-- ) {
        if(control.options[i].value == value) {
            control.options[i].selected = true;
            break;
        }
    }
}

FTD_DOM.findValueInSelect = function(control,value) {
    var retval = -1;
    
    for( var i = control.length-1; i>=0; i-- ) {
        if(control.options[i].value == value) {
            retval = i;
            break;
        }
    }
    
    return retval;
}

FTD_DOM.getSelectedRadioButton = function (groupName) {
    if(!groupName) {
        return null;
    }
    
    var buttons = document.getElementsByName(groupName);
    if( buttons==undefined ) {
        return null;
    }
    
    retval = null;
    
    for(i = 0; i < buttons.length; i++) {
        if( buttons[i].checked==true ) {
            retval = buttons[i];
            break;
        }
    }
    
    return retval;
}
    
FTD_DOM.getSelectedValue = function(el) {
    if( Object.isString(el) ) {
        el = $(el);
    }
    
    if( el==null || el.selectedIndex==-1 ) return "";
        
    return el.options[el.selectedIndex].value;
}
    
FTD_DOM.getSelectedText = function(el) {
    if( Object.isString(el) ) {
        el = $(el);
    }
    
    if( el==null || el.selectedIndex==-1 ) return "";
    
    if( el.selectedIndex>-1)
        return el.options[el.selectedIndex].text;
    else
        return "";
}

FTD_DOM.whichElement = function(e) {
    var targ;
    if (!e) 
        var e = window.event
    if( e.currentTarget )
        targ = e.currentTarget
    else if (e.target) 
        targ = e.target
    else if (e.srcElement) 
        targ = e.srcElement
    if (targ.nodeType == 3) // defeat Safari bug
        targ = targ.parentNode
        
    return targ;
}

FTD_DOM.oneOrNoCheckboxGroup = function(checkbox) {
  var checkboxGroup = checkbox.form[checkbox.name];
  for (var c = 0; c < checkboxGroup.length; c++)
    if (checkboxGroup[c] != checkbox)
      checkboxGroup[c].checked = false;
}

//FTD_DOM.getAncestorStyle = function(el,styleName) {
//    var parentNode = el.parentNode;
//    var style = null;
//    
//    while( parentNode!=null && UTILS.isEmpty(style) ) {
//        style = FTD_DOM.getStyle(parentNode,styleName);
//        //if( style=='transparent' && styleName=='backgroundColor') style='';
//    }
//    
//    alert('Returning '+style);
//    return style;
//}

FTD_DOM.FocusElement = Class.create();

FTD_DOM.FocusElement.prototype = {

    initialize: function(watchElementId, borderFocusColor, labelElementId, labelFocusColor) {
        this.watchElement           = $(watchElementId);
        this.borderFocusColor       = borderFocusColor;
        this.labelElementId         = labelElementId;
        this.borderSize = 1;
        this.currentBorderBlurColor = null;
        if( !this.watchElement ) return;
        if( !this.borderFocusColor ) this.borderFocusColor = Green;
        
        if ( this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA") {
          this.watchElement.style.borderWidth = this.borderSize;
          this.watchElement.style.borderColor = "InactiveCaption";
          this.watchElement.style.borderStyle = "solid";
        } else if ( this.watchElement.type == "checkbox" ) {
          this.watchElement.style.borderWidth = this.borderSize;
          //this.watchElement.style.borderColor = FTD_DOM.getAncestorStyle(this.watchElement,'backgroundColor');
          //this.watchElement.style.borderColor = '#eeeeee';
//          if( UTILS.isEmpty(this.watchElement.style.borderColor)==true ) {
//              this.watchElement.style.borderColor = 'white';
//          }
          this.watchElement.style.borderStyle = "solid";
        } 
        
        this._attachBehaviors();
        if( labelElementId && labelElementId!=null ) {
            new FTD_DOM.FocusLabel( watchElementId, labelElementId, labelFocusColor );
        }
    },
   
    _attachBehaviors: function() {
        FTD_DOM.addEvent(this.watchElement, "focus", this.elementHasFocus.bindAsEventListener(this),false);
        FTD_DOM.addEvent(this.watchElement, "blur", this.elementLostFocus.bindAsEventListener(this),false);
    },

    elementHasFocus: function(e) {
        this.currentBorderBlurColor = FTD_DOM.getStyle(this.watchElement,'borderColor');
        //this.currentBorderBlurColor = "Background";
        if ( this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA" || this.watchElement.type == "checkbox") {
            this.watchElement.style.borderWidth = this.borderSize;
            this.watchElement.style.borderColor = this.borderFocusColor;
            this.watchElement.style.borderStyle = "solid";
        } 
//        else if (this.watchElement.type == "checkbox" ) {
//            this.currentBorderBlurColor = FTD_DOM.getStyle(this.watchElement,'borderColor');
//            if( UTILS.isEmpty(this.currentBorderBlurColor)==true ) {
//                //this.currentBorderBlurColor = FTD_DOM.getAncestorStyle(this.watchElement,'backgroundColor');
//                this.currentBorderBlurColor = FTD_DOM.getStyle(this.watchElement,'backgroundColor');
//            }
//            this.watchElement.style.borderWidth = this.borderSize;
//            this.watchElement.style.borderColor = this.borderFocusColor;
//            this.watchElement.style.borderStyle = "solid";
//        } 
    },

    elementLostFocus: function(e) {
        if (this.watchElement.type == "text" || this.watchElement.type == "textarea" || this.watchElement.tagName == "TEXTAREA" || this.watchElement.type == "checkbox") {
            this.watchElement.style.borderWidth = this.borderSize;
            this.watchElement.style.borderColor = this.currentBorderBlurColor;
            this.watchElement.style.borderStyle = "solid";
        } 
//        else if (this.watchElement.type == "checkbox" ) {
//            this.watchElement.style.borderWidth = this.borderSize;
//            this.watchElement.style.borderColor = this.currentBorderBlurColor;
//            this.watchElement.style.borderStyle = "solid";
//        } 
    }
};
      
FTD_DOM.FocusLabel = Class.create();

FTD_DOM.FocusLabel.prototype = {

    initialize: function(watchElementId, labelElementId, labelFocusColor) {
        this.watchElement           = $(watchElementId);
        this.labelElement           = $(labelElementId);
        this.labelFocusColor        = labelFocusColor;
        this.currentLabelBlurColor  = null;
        if( !this.watchElement || !this.labelElement ) return;
        if( this.labelElement && !this.labelFocusColor ) this.labelFocusColor = Green;
        this._attachBehaviors();
    },
   
    _attachBehaviors: function() {
        FTD_DOM.addEvent(this.watchElement, "focus", this.elementHasFocus.bindAsEventListener(this),false);
        FTD_DOM.addEvent(this.watchElement, "blur", this.elementLostFocus.bindAsEventListener(this),false);
    },

    elementHasFocus: function(e) {
        this.currentLabelBlurColor = FTD_DOM.getStyle(this.labelElement,"color");
        this.labelElement.style.color = this.labelFocusColor;
    },

    elementLostFocus: function(e) {
        this.labelElement.style.color = this.currentLabelBlurColor;
    }
};

FTD_DOM.CharCountDown = Class.create();

FTD_DOM.CharCountDown.prototype = {

    initialize: function(textArea, countField, maxCount, label) {
      this.textArea             = $(textArea);
      this.countField           = $(countField);
      this.maxCount             = maxCount;
      this.label                = label;
      if( !label ) this.label   = 'Left: ';
      if( !textArea || !countField || !maxCount ) return;
      this._attachBehaviors();
   },
   
   _attachBehaviors: function() {
      this.textArea.onfocus     = this.textAreaHasFocus.bindAsEventListener(this);
      this.textArea.onblur      = this.textAreaLostFocus.bindAsEventListener(this);
      this.textArea.onkeyup     = this.textAreaHasChanged.bindAsEventListener(this);
   },

   textAreaHasFocus: function(e) {
        this.setCharCount();
        this.countField.style.visibility='visible';       
   },

   textAreaLostFocus: function(e) {
        this.countField.style.visibility='hidden'; 
   },

   textAreaHasChanged: function(e) {
        this.setCharCount();     
   },
   
   setCharCount: function() {
    var valueSize = this.textArea.value.length;
    this.countField.innerHTML= this.label+(this.maxCount-valueSize);
    
    if( this.maxCount-valueSize<0 ) {
        this.countField.style.color = v_labelFocusErrorColor;
    } else {
        this.countField.style.color = v_labelFocusColor;
    }
   }
};

FTD_DOM.InputElement = Class.create();

FTD_DOM.InputElement.prototype = {

   initialize: function(options) {
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var inputNode;

      //check for internet explorer 6
      if(IEBrowser.Version() < 8) {
        var nodeStr = '<input';
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            nodeStr += ' ';
            nodeStr += key;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        inputNode = document.createElement(nodeStr);
        
      } else {
        inputNode = document.createElement('input');
        
        for (var key in this.options) {
          var value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            inputNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return inputNode;
   }
};

FTD_DOM.SpanElement = Class.create();

FTD_DOM.SpanElement.prototype = {

   initialize: function(options) {
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var spanNode;

      //check for internet explorer
      if(IEBrowser.Version() < 8) {
        var nodeStr = '<span';
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            nodeStr += ' ';
            nodeStr += key;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        spanNode = document.createElement(nodeStr);
        
      } else {
        spanNode = document.createElement('span');
        
        for (var key in this.options) {
          var value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            spanNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return spanNode;
   }
};

FTD_DOM.LabelElement = Class.create();

FTD_DOM.LabelElement.prototype = {

   initialize: function(options) {
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var labelNode;

      //check for internet explorer
      if(IEBrowser.Version() < 8) {
        var nodeStr = '<label';
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            nodeStr += ' ';
            nodeStr += key;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        labelNode = document.createElement(nodeStr);
        
      } else {
        labelNode = document.createElement('label');
        
        for (var key in this.options) {
          var value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            labelNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return labelNode;
   }
};

FTD_DOM.SelectElement = Class.create();

FTD_DOM.SelectElement.prototype = {

   initialize: function(options) {
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var selectNode;

      //check for internet explorer
      if(IEBrowser.Version() < 8) {
        var nodeStr = '<select';
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            nodeStr += ' ';
            nodeStr += key;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        selectNode = document.createElement(nodeStr);
        
      } else {
        selectNode = document.createElement('select');
        
        for (var key in this.options) {
          var value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            selectNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return selectNode;
   }
};

FTD_DOM.DOMElement = Class.create();

FTD_DOM.DOMElement.prototype = {

   initialize: function(elementType, options) {
      this.elementType = elementType
      this.setOptions(options);
  },

   setOptions: function(options) {
      this.options = {};
      Object.extend(this.options, options || {});
   },
   
   createNode: function() {
      var newNode;

      //check for internet explorer
      if(IEBrowser.Version() < 8) {
        var nodeStr = '<'+this.elementType;
        
        for (var key in this.options) {
          var value = this.options[key];
          //alert('Key: '+key+"  Value: "+value);
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            var strKey;
            if( key=='className' ) strKey='class'; else strKey=key;
            nodeStr += ' ';
            nodeStr += strKey;
            nodeStr += '="';
            nodeStr += value;
            nodeStr += '"';
          }
        }
        nodeStr += '>';
        //alert(nodeStr);
        newNode = document.createElement(nodeStr);
        
      } else {
        newNode = document.createElement(this.elementType);
        
        for (var key in this.options) {
          value = this.options[key];
          if( (value || value==0) && key!='extend' ) {
            //alert('Key: '+key+"  Value: "+value);
            newNode.setAttribute(key,this.options[key]);
          }
        }
      }
      
      return newNode;
   }
};

FTD_DOM.unconvertHtmlSpecialChars = function(str) {
    if( str==undefined||str==null ) {
      return str;
    }
    
    retval = str;
    
    try {
        while( retval.match(/&copy;/)!=null ) { 
          retval = retval.replace(/&copy;/,"�");
        }
        
        while( retval.match(/&reg;/)!=null ) { 
          retval = retval.replace(/&reg;/,"�");
        }
        
        while( retval.match(/&nbsp;/)!=null ) { 
          retval = retval.replace(/&nbsp;/," ");
        }
        
        while( retval.match(/&#153;/)!=null ) { 
          retval = retval.replace(/&#153;/,"�");
        }
    } catch (err) {
        retval = str;
    }
    
    return retval;
}
// ]]>