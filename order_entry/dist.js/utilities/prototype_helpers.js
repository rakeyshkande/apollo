// <![CDATA[
var PH = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PH.prototypeVersion < 1.6)
      throw("Prototype Helpers (PH) requires the Prototype JavaScript framework >= 1.6");
      
PH.stringExistsInArray = function(searchArray,str){
    return searchArray.any(
        function(s){  
            return s==str;
        }
    );
}

PH.stringExistsInArrayIgnoreCase = function(searchArray,str){
    return searchArray.any(
        function(s){  
            var first=s.toLowerCase();
            var second=str.toLowerCase();
            return first==second;
        }
    );
}
// ]]>