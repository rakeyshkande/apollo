// <![CDATA[
var JOE_REQUIRED = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || JOE_REQUIRED.prototypeVersion < 1.6)
      throw("JOE_VALIDATE requires the Prototype JavaScript framework >= 1.6");
      
JOE_REQUIRED.addons = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    var addOnCode = elementId.charAt(5);
    var chkElement = 'addOn'+addOnCode+'Checkbox';
    
    return orderObj[chkElement];
}
      
JOE_REQUIRED.addonCard = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    return orderObj['addOnCardCheckbox'];
}

JOE_REQUIRED.isCarrierDelivery = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    if( orderObj.floristDelivered==false && orderObj.carrierDelivered==true ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.productColor = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    if( orderObj.productObject.colorArray.length>0 ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.productOptionVariablePrice = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    if( orderObj.productOption=='productOptionVariable' ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.floristData = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    if( orderObj.productObject.custom==true ) {
        return true;
    }
    
    return false;
} 

JOE_REQUIRED.productZipCode = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    var countryId = orderObj.productCountry;
    if( isCountryDomestic(countryId)==true ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.recipientBusiness = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    var selectedObject = v_locationTypes[orderObj.recipientType];
    
    if( selectedObject!=null ) return selectedObject.business;
    
    return false;
} 

JOE_REQUIRED.recipientZip = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    var countryId = orderObj.recipientCountry;
    if( isCountryDomestic(countryId)==true ) {
        return true;
    }
    
    return false;
} 

JOE_REQUIRED.recipientState = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return false;
    
    var countryId = orderObj.recipientCountry;
    if( isCountryDomestic(countryId)==true ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentCCManual = function(cartIdx) {
    if( JOE_REQUIRED.paymentCC(cartIdx) && $('paymentCCManAuthDiv').visible() ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentCC = function(cartIdx) {
    if( FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='C' || 
        FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'))=='CC' ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentCCExp = function(cartIdx) {
    if( (FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='C' || 
        FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'))=='CC') &&
        FTD_DOM.getSelectedValue($('paymentCCTypeCombo'))!='MS') {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentGC = function(cartIdx) {
    if( FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='G' ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentGCBalance = function(cartIdx) {
    if( JOE_REQUIRED.paymentGC() ) {
        try {
            var gcAmount = parseFloat($('paymentGCAmount').innerHTML);
        } catch (err) {
            notifyUser('Error while trying to apply gift certificate amount to the cart balance.  Contact BACOM immediately.');
            return true;
        }
        
        try {
            var total = parseFloat($('total').innerHTML);
        } catch (err) {
            notifyUser('Error while trying to determine order total at checkout.  Contact BACOM immediately.');
            return true;
        }

        if( gcAmount>=total ) return false;

        otherPaymentOption = FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'));
        if( otherPaymentOption=='NC' )
            var otherAmount = parseFloat($('paymentNCAmount').innerHTML);
        else if( otherPaymentOption=='CC' )
            otherAmount = parseFloat($('paymentCCAmount').innerHTML);
        else
            otherAmount = 0.00;
        if( (gcAmount+otherAmount).toFixed(2) >=total ) return false;
        
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentNC = function(elementId) {
    if( FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='N' || 
        FTD_DOM.getSelectedValue($('paymentGCBalanceCombo')).value=='NC' ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentNCOrderNumber = function(elementId) {
    if( JOE_REQUIRED.paymentNC(elementId) && FTD_DOM.getSelectedValue($('paymentNCType'))=='RESEND' ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.customerZip = function(elementId) {
    
    var countryId = $('customerCountryCombo').getValue();
    if( isCountryDomestic(countryId)==true ) {
        return true;
    }
    
    return false;
}

JOE_REQUIRED.customerState = function(elementId) {
    
    var countryId = $('customerCountryCombo').getValue();
    if( isCountryDomestic(countryId)==true ) {
        return true;
    }
    
    return false;
} 

JOE_REQUIRED.membershipId = function(elementId) {
	if (v_masterSourceCode.membershipDataRequired==true && 
        $('membershipSkip').checked==false && v_masterSourceCode.sourceType.indexOf("AAA") > -1)
		{
		return true;
		}
	
	else if ( v_masterSourceCode.membershipDataRequired==true && 
        $('membershipSkip').checked==false && 
        areNonIOTWItemsInCart()==true ) {
        return true;
    }
    
    return false;
} 

JOE_REQUIRED.customerEmailAddress = function(elementId) {
    return $('customerSubscribeCheckbox').checked==true;
}

JOE_REQUIRED.productSearchDeliveryDate = function(elementId) {
    if( isCountryDomestic($('productSearchCountryCombo').getValue()) ) {
        if( UTILS.isEmpty($('productSearchZip').value)==false || 
            UTILS.isEmpty($('productSearchDeliveryDate').getValue())==false  )
            return true;
    }
    
    return false;
}

JOE_REQUIRED.productSearchZip = function(elementId) {
    if( isCountryDomestic($('productSearchCountryCombo').getValue()) ) {
        if( UTILS.isEmpty($('productSearchZip').value)==false || 
            UTILS.isEmpty($('productSearchDeliveryDate').getValue())==false  )
            return true;
    }
    
    return false;
}

JOE_REQUIRED.paymentCSC = function(elementId) {
  if( !$('paymentCSCOverride').checked && 
	((FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='C' || 
        	FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'))=='CC') &&
        	FTD_DOM.getSelectedValue($('paymentCCTypeCombo'))!='MS')) {
        return true;
  }
  
  return false;
}

// ]]>
