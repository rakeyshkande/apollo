// <![CDATA[
var DELIVERY_DATE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || DELIVERY_DATE.prototypeVersion < 1.6)
      throw("DELIVERY_DATE requires the Prototype JavaScript framework >= 1.6");
      
DELIVERY_DATE.OBJECT = Class.create();

DELIVERY_DATE.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.startDate=null;
        this.endDate=null;
        this.isDateRange=false;
        this.displayString=null;
        this.valueString=null;
    },
    
    deepCopy: function() {
        var copyObj = new DELIVERY_DATE.OBJECT();
        
        copyObj.startDate=this.startDate;
        copyObj.endDate=this.endDate;
        copyObj.isDateRange=this.isDateRange;
        copyObj.displayString=this.displayString;
        copyObj.valueString=this.valueString;
        
        return copyObj;
    }
};
// ]]>