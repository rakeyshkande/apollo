// <![CDATA[
var PRODUCT = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PRODUCT.prototypeVersion < 1.6)
      throw("PRODUCT requires the Prototype JavaScript framework >= 1.6");
      
PRODUCT.OBJECT = Class.create();

PRODUCT.OBJECT.prototype = {

    initialize: function(productId) {
        this.reset();
        if( productId!=undefined && productId!=null ) {
            this.productId = productId;
            this.masterProductId = productId;
        }
    },
    
    reset: function() {
        this.readyFlag=false;
        this.productId=null;
        this.novatorId=null;
        this.masterProductId=null;
        this.name=''; //novator name
        this.productDescription='';  //long description
        this.subcodeDescription=null;
        this.standardPrice=null;
        this.premiumPrice=null;
        this.deluxePrice=null;
        this.variablePrice=null;
        this.standardDiscount=null;
        this.premiumDiscount=null;
        this.deluxeDiscount=null;
        this.discountType=null;
        this.smallImage="images/noproduct.gif";
        this.largeImage="images/noproduct.gif";
        this.productType=null;
        this.custom=false;
        this.secondChoice=null;
        this.floristProduct=false;
        this.vendorProduct=false;
        this.addOnArray = new Array();
        this.addOnMaxQtyArray = new Array();
        this.addOnDescriptionArray = new Array();
        this.addOnTypeArray = new Array();
        this.addOnPriceArray = new Array();
        this.addOnFloristVendorArray = new Array();
        this.vaseArray = new Array();
        this.vaseFloristVendorArray = new Array();
        this.addOnCard = false;
        this.subcodeArray = new Array();
        this.upsellArray = new Array();
        this.colorArray = new Array();
        this.matchOrder = null;
        this.popularOrder = null;
        this.index = null;
        this.available = null;  // YES NO based on status
        this.exceptionStartDate = null;  
        this.exceptionEndDate = null;
        this.over21 = null;
        this.shippingCostGR=null;
        this.shippingCost2F=null;
        this.shippingCostND=null;
        this.shippingCostSA=null;
        this.dropShipWarned=false;
        this.secondChoiceWarned=false;
        this.allowFreeShippingFlag=null;
        this.iotwObject=new IOTWOE.OBJECT();
    },
    
    copy: function() {
        var newProduct = new PRODUCT.OBJECT(this.productId);
        newProduct.readyFlag=this.readyFlag;
        newProduct.productId=this.productId;
        newProduct.novatorId=this.novatorId;
        newProduct.masterProductId=this.masterProductId;
        newProduct.name=this.name;
        newProduct.productDescription=this.productDescription;
        newProduct.subcodeDescription=this.subcodeDescription;
        newProduct.standardPrice=this.standardPrice;
        newProduct.premiumPrice=this.premiumPrice;
        newProduct.deluxePrice=this.deluxePrice;
        newProduct.variablePrice=this.variablePrice;
        newProduct.discountType=this.discountType;
        newProduct.standardDiscount=this.standardDiscount;
        newProduct.premiumDiscount=this.premiumDiscount;
        newProduct.deluxeDiscount=this.deluxeDiscount;
        newProduct.smallImage=this.smallImage;
        newProduct.largeImage=this.largeImage;
        newProduct.productType=this.productType;
        newProduct.custom=this.custom;
        newProduct.secondChoice=this.secondChoice;
        newProduct.floristProduct=this.floristProduct;
        newProduct.vendorProduct=this.vendorProduct;
        newProduct.addOnArray=this.addOnArray.clone();
        newProduct.addOnMaxQtyArray=this.addOnMaxQtyArray.clone();
        newProduct.addOnDescriptionArray=this.addOnDescriptionArray.clone();
        newProduct.addOnTypeArray=this.addOnTypeArray.clone();
        newProduct.addOnPriceArray=this.addOnPriceArray.clone();
        newProduct.addOnFloristVendorArray=this.addOnFloristVendorArray.clone();
        newProduct.vaseArray=this.vaseArray.clone();
        newProduct.vaseFloristVendorArray=this.vaseFloristVendorArray.clone();
        newProduct.addOnCard=this.addOnCard;
        newProduct.subcodeArray=this.subcodeArray.clone();
        newProduct.upsellArray=this.upsellArray.clone();
        newProduct.colorArray=this.colorArray.clone();
        newProduct.matchOrder=this.matchOrder;
        newProduct.popularOrder=this.popularOrder;
        newProduct.index=this.index;
        newProduct.available=this.available;
        newProduct.exceptionStartDate=this.exceptionStartDate;
        newProduct.exceptionEndDate=this.exceptionEndDate;
        newProduct.over21=this.over21;
        newProduct.shippingCostGR=this.shippingCostGR;
        newProduct.shippingCost2F=this.shippingCost2F;
        newProduct.shippingCostND=this.shippingCostND;
        newProduct.shippingCostSA=this.shippingCostSA;
        newProduct.allowFreeShippingFlag=this.allowFreeShippingFlag;
        //Never copy the following values
        //newProduct.dropShipWarned=this.dropShipWarned;
        //newProduct.secondChoiceWarned=this.secondChoiceWarned;
        //newProduct.iotwObject=this.iotwObject;
        return newProduct;
    }
};

// ]]>
