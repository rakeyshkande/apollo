// <![CDATA[
var ORDER = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || ORDER.prototypeVersion < 1.6)
      throw("ORDER requires the Prototype JavaScript framework >= 1.6");
      
ORDER.OBJECT = Class.create();

ORDER.OBJECT.prototype = {

    initialize: function(orderElement,cartListIndex) {
        this.reset();
        
        if (typeof orderElement == 'string') {
            this.orderElement = $(orderElement);
        } else {
            this.orderElement = orderElement;
        }
        
        this.cartListIndex = cartListIndex;
    },
    
    reset: function() {
        this.resetProduct(true);
        this.productOccasion=null;
        this.productDeliveryDate='';
        this.productZip=null;
        this.surchargeWarningGiven=false;
        this.productCountry='US';
		this.languageId=null;
        this.orderElement=null;
        this.cartListIndex=null;
        this.orderComments=null;
        this.cardMessage=null;
        this.recipientPhone=null;
        this.recipientPhoneExt=null;
        this.recipientPhoneChecked=false;
        this.recipientType=null;
        this.recipientBusiness=null;
        this.recipientRoom=null;
        this.recipientFirstName=null;
        this.recipientLastName=null;
        this.recipientAddress=null;
        this.recipientZip=null;
        this.recipientCity=null;
        this.recipientState=null;
        this.recipientCountry=null;
        this.recipientHoursFrom=null;
        this.recipientHoursTo=null;
        this.avSuggestedAddress_0=null;
        this.avSuggestedCity_0=null;
        this.avSuggestedState_0=null;
        this.avSuggestedZip_0=null;
        this.avSuggestedAddress_1=null;
        this.avSuggestedCity_1=null;
        this.avSuggestedState_1=null;
        this.avSuggestedZip_1=null;
        this.avSuggestedAddress_2=null;
        this.avSuggestedCity_2=null;
        this.avSuggestedState_2=null;
        this.avSuggestedZip_2=null;
        this.avResultCode=null;
        this.avNeedsResolution=false;
        this.avCustomerInsisted=false;
        this.avsPerformed=false;
        this.avDisabled=false;
        this.addons=new Array();
        this.addonsQty = new Array();
        this.serviceFees=null;
        this.shippingFees=null;
        this.salesTax=null;
        this.subTotal=null;
        this.orderFields=cloneOrderFields();
        this.lastAvailabilityValues=new Array();
//        this.lastCalculateValues=new Array();
        this.recipientEmailAddress=null;
        this.recipientOptIn=null;
        this.forceAvailabilityCheck=false;
        this.productDeliveryMethod=null;
        
        this.recipientTypeEnabled=false;
        this.deliveryMethodFloristEnabled=false;
        this.deliveryMethodDropshipEnabled=false;
        
        this.deliveryDatesList=new Array();
        this.addOnCheckedArray = new Array();
        this.addOnQtyArray = new Array();
        this.addOnIdArray = new Array();
        this.addOnMaxQtyArray = new Array();
        this.addOnDescriptionArray = new Array();
        this.addOnPriceArray = new Array();
        this.floristId=null;
        this.floristDesc=null;
        this.floristComments=null;
        
        //structure to hold results of address verification.  not backed up by any input fields
        this.avsAddresses = new Array();
        this.sizeIndicator = 'A'; 
    },
    
    resetProduct: function(resetAddons) {
        this.productObject=new PRODUCT.OBJECT();
        this.productGenerallyAvailable=false;
        this.productAvailable=false;
        this.productId=null;
        this.productPrice=null;
        this.productDiscount=null;
        if (this.productOption == null || this.productOption == 'undefined') {
            this.productOption='productOptionStandard';
        }
        this.productOptionVariablePrice=null;
        this.productOptionSubCode=null;
        this.productOptionUpsell=null;
        this.productOptionColor=null;
        this.productOptionColor2=null;
        this.productSourceCode=null;
        this.floristDelivered=false;
        this.carrierDelivered=false;
        this.carrierNextDayAvailable=false;
        this.carrierStandardAvailable=false;
        this.carrierTwoDayAvailable=false;
        this.carrierSaturdayDayAvailable=false;
        this.productIOTWDescription=null;
        this.discountType=null;
        
        if( resetAddons==true ) {
            this.addOnCardCheckbox=false;
            this.addOnCardSelect=null;
            this.productAddonVase=null;
            this.addOnVaseArray = new Array();
            this.addOnVaseDeliveryTypeArray = new Array();
            
            this.addOnCheckbox1=null;
            this.addOnQty1=null;
            this.addOnCheckbox2=null;
            this.addOnQty2=null;
            this.addOnCheckbox3=null;
            this.addOnQty3=null;
            this.addOnCheckbox4=null;
            this.addOnQty4=null;
            
            this.addOnFCheckbox=false;
            this.addOnFBannerText=null;

        }
        
        this.deliveryDaysND = new Array();
        this.deliveryDays2F = new Array();
        this.deliveryDaysGR = new Array();
        this.deliveryDaysSA = new Array();
        this.deliveryDaysSU = new Array();
        
        this.carrierDeliverOption = 'carrierDeliveryOptGR';
        this.carrierDeliveryDateSA = null;
        this.carrierDeliveryDateSU = null;
        this.carrierDeliveryDateND = null;
        this.carrierDeliveryDateGR = null;
        this.carrierDeliveryDate2F = null;
        
        this.sizeIndicator = 'A';
        
        //Loop through and reset all the field error flags
        for( var i in this.orderFields ) {
            if( Object.isFunction(this.orderFields[i])==false ) {
                this.orderFields[i].errorFlag=false;
            }
        }
    },
    
    populateDOM: function() {
      this.populateThumbNail();
      this.populateOrder();
    },
    
    populateOrder: function() {
        if( this.productObject!=null ) {
            //Reset all the fields and labels
            for( var i in this.orderFields ) {
                if( Object.isFunction(this.orderFields[i])==false ) {
                    this.orderFields[i].setFieldStyle();
                    var result = this.isRequired(this.orderFields[i].elementId);  //Set's the * on the element label
                }
            }
            
            this.setOrderProductValue('productId','');
            
            var productType = this.productObject.productType;
          // alert("The product type is " + productType);
          // #18729 - added the check for the product type 
          // If the product_type is SDFC then display flexfill icon 
            
            if (productType != null && productType == "SDFC")
            	{
            	 $('flexFill').src = 'images/flexfill.png';
                 $('flexFill').title ='SDFC Product';
                 $('flexFill').alt = 'images/flexfill.png';
            	}
            else 
            	{
            	 $('flexFill').src = 'images/blank.gif';
                 $('flexFill').title =' ';
                 $('flexFill').alt = 'images/blank.gif';
            	}
            var newValue = this.productObject.smallImage;
            if( newValue==null ) {
                newValue = 'images/noproduct.gif';
            }
            $('productImage').src = newValue;
            $('productImage').title = productVerbageCleanup(this.productObject.productDescription);
            $('productImage').alt = productVerbageCleanup(this.productObject.productDescription);
            
            this.setOrderProductValue('standardPrice','0.00','display','productOptionStandardRow');
            this.setOrderProductValue('premiumPrice','0.00','display','productOptionPremiumRow');
            this.setOrderProductValue('deluxePrice','0.00','display','productOptionDeluxeRow');
            
            var appliedDiscountType='';
            var appliedStandardDiscount;
            var appliedPremiumDiscount;
            var appliedDeluxeDiscount;
            
            this.productIOTWDescription = this.productObject.iotwObject.message;
            this.setOrderValue('productIOTWDescription','');
            var iotwApplies = this.productObject.iotwObject.doesIOTWPricingApply(this.productDeliveryDate);
            if( iotwApplies==true ) {
                appliedDiscountType = this.productObject.iotwObject.discountType;
                appliedStandardDiscount = this.productObject.iotwObject.standardDiscount;
                appliedPremiumDiscount = this.productObject.iotwObject.premiumDiscount;
                appliedDeluxeDiscount = this.productObject.iotwObject.deluxeDiscount;
                if (appliedStandardDiscount==null) appliedStandardDiscount=0;
                if (appliedPremiumDiscount==null) appliedPremiumDiscount=0;
                if (appliedDeluxeDiscount==null) appliedDeluxeDiscount=0;
            } else {
                appliedDiscountType = this.productObject.discountType;
                if( UTILS.isEmpty(appliedDiscountType)==false ) {
                    appliedStandardDiscount = this.productObject.standardDiscount;
                    appliedPremiumDiscount = this.productObject.premiumDiscount;
                    appliedDeluxeDiscount = this.productObject.deluxeDiscount;
                    if (appliedStandardDiscount==null) appliedStandardDiscount=0;
                    if (appliedPremiumDiscount==null) appliedPremiumDiscount=0;
                    if (appliedDeluxeDiscount==null) appliedDeluxeDiscount=0;
                }
            }
            
            if( this.productObject.standardPrice!=null ) {
                if (this.productObject.premiumPrice==null && this.productObject.deluxePrice==null && this.productObject.variablePrice==null)
                    $('productOptionStandard').disabled=true;
                else
                    $('productOptionStandard').disabled=false;
            }
            
            if( UTILS.isEmpty(appliedDiscountType)==true ) 
                this.discountType='';
            else 
                this.discountType=appliedDiscountType;
            this.setOrderValue('discountType','');
            
            $('standardLabel').innerHTML = v_standard_label;
            $('deluxeLabel').innerHTML = v_deluxe_label;
            $('premiumLabel').innerHTML = v_premium_label;
            
            if( appliedDiscountType=='Discount' ) {
                //Dollar discount
                $('standardPrice').innerHTML = UTILS.addCommas(appliedStandardDiscount);
                $('standardDiscount').innerHTML = UTILS.addCommas(this.productObject.standardPrice);
                $('standardDiscount').style.textDecoration = 'line-through';
                $('standardDiscountCell').style.display = 'inline';
                
                $('deluxePrice').innerHTML = UTILS.addCommas(appliedDeluxeDiscount);
                $('deluxeDiscount').innerHTML = UTILS.addCommas(this.productObject.deluxePrice);
                $('deluxeDiscount').style.textDecoration = 'line-through';
                $('deluxeDiscountCell').style.display = 'inline';
                
                $('premiumPrice').innerHTML = UTILS.addCommas(appliedPremiumDiscount);
                $('premiumDiscount').innerHTML = UTILS.addCommas(this.productObject.premiumPrice);
                $('premiumDiscount').style.textDecoration = 'line-through';
                $('premiumDiscountCell').style.display = 'inline';
            } else if( UTILS.isEmpty(appliedDiscountType)==false ) {
                //Points or miles
                $('standardPrice').innerHTML = UTILS.addCommas(this.productObject.standardPrice);
                $('standardDiscount').innerHTML = appliedStandardDiscount+'&nbsp;'+appliedDiscountType;
                $('standardDiscount').style.textDecoration = '';
                $('standardDiscountCell').style.display = 'inline';
                
                $('deluxePrice').innerHTML = UTILS.addCommas(this.productObject.deluxePrice);
                $('deluxeDiscount').innerHTML = appliedDeluxeDiscount+'&nbsp;'+appliedDiscountType;
                $('deluxeDiscount').style.textDecoration = '';
                $('deluxeDiscountCell').style.display = 'inline';
                
                $('premiumPrice').innerHTML = UTILS.addCommas(this.productObject.premiumPrice);
                $('premiumDiscount').innerHTML = appliedPremiumDiscount+'&nbsp;'+appliedDiscountType;
                $('premiumDiscount').style.textDecoration = '';
                $('premiumDiscountCell').style.display = 'inline';
            } else {
                //No discount
                $('standardPrice').innerHTML = UTILS.addCommas(this.productObject.standardPrice);
                $('standardDiscount').innerHTML = '';
                $('standardDiscount').style.textDecoration = '';
                $('standardDiscountCell').style.display = 'none';
                
                $('deluxePrice').innerHTML = UTILS.addCommas(this.productObject.deluxePrice);
                $('deluxeDiscount').innerHTML = '';
                $('deluxeDiscount').style.textDecoration = '';
                $('deluxeDiscountCell').style.display = 'none';
                
                $('premiumPrice').innerHTML = UTILS.addCommas(this.productObject.premiumPrice);
                $('premiumDiscount').innerHTML = '';
                $('premiumDiscount').style.textDecoration = '';
                $('premiumDiscountCell').style.display = 'none';
            }
            
            if( this.productObject.variablePrice==null ) {
                $('productOptionVariableRow').style.display = 'none';
                $('productOptionVariablePrice').title = '';
                $('productOptionVariable').title = '';
            } else {
                $('productOptionVariableRow').style.display = 'block';
                var povTitle = 'Price must be between $'+UTILS.addCommas(this.productObject.standardPrice)+' and  $'+UTILS.addCommas(this.productObject.variablePrice);
                $('productOptionVariable').title = povTitle;
                $('productOptionVariablePrice').title = povTitle;
                
                var fld = findFieldObject('productOptionVariable',this.cartListIndex);
                if( fld!=null ) {
                    fld.title=povTitle;
                    fld.errorText=povTitle+' in the format of 99,999.99';
                }
                fld = findFieldObject('productOptionVariablePrice',this.cartListIndex);
                if( fld!=null ) {
                    fld.title=povTitle;
                    fld.errorText=povTitle+' in the format of 99,999.99';
                }
            }
            
            $('productExceptionDates').innerHTML = '';
            if( this.productObject.exceptionStartDate != null ) {
                $('productExceptionDates').innerHTML = 'Product Availability Dates: ' + 
                    this.productObject.exceptionStartDate + ' - ' + this.productObject.exceptionEndDate;
            }
            
            this.populateSubCodeCombo();
            if( this.productObject.subcodeArray.length>0 ) {
                $('productOptionStandardRow').style.display = 'none'
                $('productOptionSubcodesRow').style.display = 'block';
            } else {
                $('productOptionSubcodesRow').style.display = 'none';
            }
            
            this.populateUpsellCombo();
            if( this.productObject.upsellArray.length>0 ) {
                $('productOptionUpsellRow').style.display = 'block';
            } else {
                $('productOptionUpsellRow').style.display = 'none';
            }
            
            this.populateColorCombo();
            if( this.productObject.colorArray.length>0 ) {
                $('productOptionColorRow').style.display = 'block';
            } else {
                $('productOptionColorRow').style.display = 'none';
            }

            if( this.productObject.productId==null ) {
                $('addOnDiv').style.display='none';
                $('productOptionsDiv').style.display='none';
                $('deliveryOptionsDiv').style.display='none';
                $('floristOptionsDiv').style.display='none';
            } else if (this.productGenerallyAvailable==true) {
                if (this.floristDelivered==true && this.productObject.floristProduct==true &&
                    this.carrierDelivered==true && this.productObject.vendorProduct==true) {
                    if (this.productDeliveryMethod=='productDeliveryMethodDropship') {
                        $('addOnDiv').style.display='block';
                        $('productOptionsDiv').style.display='block';
                        $('deliveryOptionsDiv').style.display='block';
                        if (v_modifyOrderFromCom == false || v_hasSpecialInstructions == false) {
                            $('floristOptionsDiv').style.display='none';
                        } else {
                            $('floristOptionsDiv').style.display='block';
                            $('floristIdDiv').style.display='none';
                            $('specialInstructionsLabel').style.display='none'
                        }
                    } else {
                        $('addOnDiv').style.display='block';
                        $('productOptionsDiv').style.display='block';
                        $('deliveryOptionsDiv').style.display='none';
                        $('floristOptionsDiv').style.display='block';
                        $('floristIdDiv').style.display='block';
                        $('specialInstructionsLabel').style.display='block';
                    }
                } else if( this.floristDelivered==true && this.productObject.floristProduct==true ) {
                    $('addOnDiv').style.display='block';
                    $('productOptionsDiv').style.display='block';
                    $('deliveryOptionsDiv').style.display='none';
                    $('floristOptionsDiv').style.display='block';
                    $('floristIdDiv').style.display='block';
                    $('specialInstructionsLabel').style.display='block';
                } else if( this.carrierDelivered==true && this.productObject.vendorProduct==true ) {
                    $('addOnDiv').style.display='block';
                    $('productOptionsDiv').style.display='block';
                    $('deliveryOptionsDiv').style.display='block';
                    if (v_modifyOrderFromCom == false || v_hasSpecialInstructions == false) {
                        $('floristOptionsDiv').style.display='none';
                    } else {
                        $('floristOptionsDiv').style.display='block';
                        $('floristIdDiv').style.display='none';
                        $('specialInstructionsLabel').style.display='none';
                    }
                    if (v_previousDeliveryType != 'vendor') {
                        //this logic will only be accessed if SDG or SDFC products are not available by florist
                        var ddObj = v_deliveryDates[FTD_DOM.getSelectedValue($('productDeliveryDate'))];
                        if (ddObj != null && ddObj.isDateRange) {
                            //Clear selected delivery date if switching from floral to vendor
                            //and previous delivery date was in a date range
                            changeProductDeliveryDate(null);
                            v_lastAvailabilityOrderValues[v_currentCartIdx] = null;
                        }
                        v_previousDeliveryType = 'vendor';
                        //populateDeliveryDateCombos();
                    }
                } else if( this.productObject.masterProductId==this.productObject.productId && 
                         ( this.productObject.upsellArray.length>0 || this.productObject.subcodeArray.length>0 ) ) {
                    //You are here where the productId entered is the subCode master product id 
                    //or the master upsell id.  The user needs to be able to pick the subcode
                    //or the upsell product before availability can be checked.
                    $('addOnDiv').style.display='none';
                    $('productOptionsDiv').style.display='block';
                    $('deliveryOptionsDiv').style.display='none';
                    $('floristOptionsDiv').style.display='none';
                }
            } else {
                if( this.productObject.upsellArray.length>0 || this.productObject.subcodeArray.length>0 ) {
                    $('productOptionsDiv').style.display='block';    
                } else {
                    $('productOptionsDiv').style.display='none';
                }
                $('addOnDiv').style.display='none';
                $('deliveryOptionsDiv').style.display='none';
                $('floristOptionsDiv').style.display='none';
            }
            
        }
        this.setOrderCombo('languageId');
        this.setOrderCombo('productOccasion');
        this.setOrderCombo('productCountry');
        this.setOrderCombo('productDeliveryDate');
        this.setOrderValue('productZip','');
        //Never set the following values
//        this.setOrderValue('surchargeWarningGiven','');
//        this.setOrderValue('productGenerallyAvailable','');
//        this.setOrderValue('productAvailable','');
        this.setOrderValue('productIOTWDescription','');
        this.setOrderValue('productSourceCode','');
        this.setOrderValue('recipientPhone','');
        this.setOrderValue('recipientPhoneExt','');
        this.setOrderValue('recipientPhoneChecked','');
        this.setOrderCombo('recipientType');
        ORDER_EVENTS.onRecipLocationTypeChanged(null);
        this.setOrderValue('recipientBusiness','');
        this.setOrderValue('recipientRoom','');
        this.setOrderValue('recipientPhone','');
        this.setOrderValue('recipientFirstName','');
        this.setOrderValue('recipientLastName','');
        this.setOrderValue('recipientAddress','');
        this.setOrderValue('recipientZip','');
        this.setOrderCombo('recipientCountry');
        this.setOrderValue('recipientCity','');
        this.setOrderValue('recipientEmailAddress','');
        this.setOrderValue('recipientOptIn','');
        this.setOrderCombo('recipientHoursFrom');
        this.setOrderCombo('recipientHoursTo');
        this.setOrderValue('avSuggestedAddress_0');
        this.setOrderValue('avSuggestedCity_0');
        this.setOrderValue('avSuggestedState_0');
        this.setOrderValue('avSuggestedZip_0');
        
        this.setOrderValue('avSuggestedAddress_1');
        this.setOrderValue('avSuggestedCity_1');
        this.setOrderValue('avSuggestedState_1');
        this.setOrderValue('avSuggestedZip_1');
        
        this.setOrderValue('avSuggestedAddress_2');
        this.setOrderValue('avSuggestedCity_2');
        this.setOrderValue('avSuggestedState_2');
        this.setOrderValue('avSuggestedZip_2');
        
        this.setOrderValue('avResultCode');
        this.setOrderValue('avsPerformed');
        this.setOrderValue('avNeedsResolution');
        this.setOrderValue('avCustomerInsisted');
        this.setOrderValue('avDisabled');
        this.setOrderRadioGroup('carrierDeliverOption');
        this.setOrderValue('orderComments','');
        this.setOrderValue('floristId','');
        this.setOrderValue('floristDesc','');
        this.setOrderValue('floristComments','');
        ORDER_EVENTS.onRecipientCountryChange(null);
        this.setOrderCombo('recipientState');
        this.setOrderValue('cardMessage','');
        this.setOrderRadioGroup('productOption');
        this.setOrderCombo('productOptionSubCode');
        this.setOrderCombo('productOptionUpsell');
        this.setOrderCombo('productOptionColor');

        var orderDeliveryDates = this['deliveryDatesList'];
        if (orderDeliveryDates.length > 0) {
            var sortArray = Object.keys(orderDeliveryDates);
            sortArray.sort();
            v_deliveryDates=orderDeliveryDates;
            var deliveryDateHold = this['productDeliveryDate'];
            populateDeliveryDateCombos();
            FTD_DOM.selectOptionByValue($('productDeliveryDate'), deliveryDateHold);
        }

        var newValue = this['productDeliveryMethod'];
        this.setOrderRadioGroup('productDeliveryMethod');
        if (newValue != null) {
        	//alert("Sympathy Testing  new value in order_objects "+newValue);
            if (this['deliveryMethodFloristEnabled']) {
                $('productDeliveryMethodFlorist').disabled=false;
            } else {
                $('productDeliveryMethodFlorist').disabled=true;
            }
            if (this['deliveryMethodDropshipEnabled']) {
                $('productDeliveryMethodDropship').disabled=false;
            } else {
                $('productDeliveryMethodDropship').disabled=true;
            }
            
            //Since Recipient type is enabled only if the PAS call is made in
            //the respective cart item.
            
            if (this['recipientTypeEnabled']) {
            	$('recipientType').disabled=false;
            }
            else {
            	$('recipientType').disabled=true;
            }

        } else {
            $('productDeliveryMethodFlorist').disabled=true;
            $('productDeliveryMethodFlorist').checked=false;
            $('productDeliveryMethodDropship').disabled=true;
            $('productDeliveryMethodDropship').checked=false;
            $('recipientType').disabled=true;
        }

        var enableDeliveryDate = false;
        if (isCountryDomestic($('productCountry').getValue())==false && UTILS.isEmpty($('productId').value)==false) {
            enableDeliveryDate = true;
        } else if ( UTILS.isEmpty($('productId').value)==false && UTILS.isEmpty($('productZip').value)==false) {
            enableDeliveryDate = true;
        }
        
        if (enableDeliveryDate) {
            v_displayDeliveryDate = true;
            $('productDeliveryDate').disabled=false;
            $('productCalendarButton').disabled=false;
        }
                
        this.configureAddons();
        this.setCarrierOptions();
        
        //Special configurations for field tabbing
        if( this.productObject.floristProduct==true && this.productObject.custom ) {
            fld = findFieldObject('floristId',v_currentCartIdx);
            if( fld!=null ) {
                $('floristId').tabIndex = fld.tabIndex;
            } 
            
            fld = findFieldObject('floristSearch',v_currentCartIdx);
            if( fld!=null ) {
                $('floristSearch').tabIndex = fld.tabIndex;
            } 
            
            fld = findFieldObject('floristComments',v_currentCartIdx);
            if( fld!=null ) {
                $('floristComments').tabIndex = fld.tabIndex;
            }
        } else {
            $('floristId').tabIndex="-1";
            $('floristSearch').tabIndex="-1";
            $('floristComments').tabIndex="-1";
        }
    },
    
    configureAddons: function() {
    	
        this.setOrderValue('addOnCardCheckbox',false);
        this.setAddons();
        
        //Configure Add-On display
        var idx;
        for( idx=0; idx<v_max_addons; idx++ ) {
//            alert(idx + ' ' + this.addOnIdArray[idx+1] + ' ' + this.addOnDescriptionArray[idx+1]);
            if( UTILS.isEmpty(this.addOnIdArray[idx+1])==false ) {
                $('addOnRow' + (idx+1)).style.display='inline';
            } else {
                $('addOnRow' + (idx+1)).style.display='none';
            }
            $('addOnCheckbox' + (idx+1)).checked = this.addOnCheckedArray[idx+1];
            $('addOnId' + (idx+1)).value = this.addOnIdArray[idx+1];
            FTD_DOM.clearSelect('addOnQty' + (idx+1));
            for( tempidx=0; tempidx<this.addOnMaxQtyArray[idx+1]; tempidx++) {
                FTD_DOM.insertOption('addOnQty' + (idx+1), tempidx+1, tempidx+1, false);
            }
            FTD_DOM.selectOptionByValue($('addOnQty' + (idx+1)), this.addOnQtyArray[idx+1])
            $('addOnCheckboxLabel' + (idx+1)).innerHTML = this.addOnDescriptionArray[idx+1];
            tempString = v_scriptFieldsHold['addOnCheckbox' + (idx+1)];
            if (tempString != null) {
                tempString = tempString.replace('~description~', this.addOnDescriptionArray[idx+1]);
                v_scriptFields['addOnCheckbox' + (idx+1)] = tempString;
            }
            $('addOnPrice' + (idx+1)).innerHTML = '$' + this.addOnPriceArray[idx+1];
            if (this.addOnCheckedArray[idx+1]) {
                $('addOnQty' + (idx+1)).disabled = false;
                fld = findFieldObject('addOnCheckbox' + (idx+1),v_currentCartIdx);
                if( fld!=null && fld.tabIndex!=null ) {
                    $('addOnCheckbox' + (idx+1)).tabIndex = fld.tabIndex;
                }
                fld = findFieldObject('addOnQty' + (idx+1),v_currentCartIdx);
                if( fld!=null && fld.tabIndex!=null ) {
                    $('addOnQty' + (idx+1)).tabIndex = fld.tabIndex;
                }
            } else {
                $('addOnQty' + (idx+1)).disabled = true;
                fld = findFieldObject('addOnCheckbox' + (idx+1),v_currentCartIdx);
                if( fld!=null && fld.tabIndexInitial!=null ) {
                    $('addOnCheckbox' + (idx+1)).tabIndex = fld.tabIndexInitial;
                }
                fld = findFieldObject('addOnQty' + (idx+1),v_currentCartIdx);
                if( fld!=null && fld.tabIndexInitial!=null ) {
                    $('addOnQty' + (idx+1)).tabIndex = fld.tabIndexInitial;
                }
            }
        }
    	
    	
        if( UTILS.isEmpty(this.productObject.addOnArray['F'])==false && this.floristDelivered == true ) {
            $('addOnFRow').style.display='inline';
            if(this.addOnFCheckbox){
            	$('addOnFCheckbox').checked = true;
            	$('addOnFBannerText').disabled = false;
            	$('addOnFBannerText').value = this.addOnFBannerText;
            }
            else{
            	$('addOnFCheckbox').checked = false;
            	$('addOnFBannerText').disabled = true;
            	$('addOnFBannerText').value = '';
            }
        } else {
            $('addOnFCheckbox').checked = false;
            $('addOnFRow').style.display='none';
        }

        FTD_DOM.clearSelect('productAddonVase');
        if (this.productObject.vaseArray.length < 1) {
            $('addOnVaseRow').style.display='none';
        } else {
            for( idx=0; idx<this.productObject.vaseArray.length; idx++) {
                if (this.addOnVaseArray[idx+1] != null && this.addOnVaseArray[idx+1].length > 0) {
                    var addon = findAddonById(this.addOnVaseArray[idx+1]);
                    FTD_DOM.insertOption($('productAddonVase'), UTILS.convertHtmlEntity(UTILS.convertHtmlEntity(addon.description)) + ' - $' + addon.price, addon.id, false);
                }
            }
            this.productAddonVase = this.productObject.productAddonVase;
            this.setOrderCombo('productAddonVase');
            $('addOnVaseRow').style.display='block';
        }

        FTD_DOM.clearSelect('addOnCardSelect');
        FTD_DOM.insertOption('addOnCardSelect','','',false);
        displayAddOnCards();
        if( this.productObject.addOnCard==true ) {
            var occasionId = $('productOccasion').getValue();
            for( idx=0; idx<v_addons.length; idx++) {
                if( v_addons[idx].type=='4' && PH.stringExistsInArray(v_addons[idx].occasions,occasionId) ) {
                    FTD_DOM.insertOption('addOnCardSelect', UTILS.convertHtmlEntity(UTILS.convertHtmlEntity(v_addons[idx].description)), v_addons[idx].id, false);
                }
            }
            if( $('addOnCardSelect').length==1 ) {  //Only the empty value exists
                this.addOnCardCheckbox=false;
                this.addOnCardSelect=0;
                $('addOnCardSelect').selectedIndex=0;
                $('addOnCardsTable').style.display='none';
            } else {
                var selectedRadio = FTD_DOM.getSelectedRadioButton('productDeliveryMethod');
                if (selectedRadio != null && selectedRadio.id=='productDeliveryMethodFlorist') {
                    $('addOnCardsTable').style.display='inline';
                } else {
                    this.addOnCardCheckbox=false;
                    this.addOnCardSelect=0;
                    $('addOnCardSelect').selectedIndex=0;
                    $('addOnCardsTable').style.display='none';
                }
            }
        } else {
            this.addOnCardCheckbox = false;
            this.addOnCardSelect=0;
            $('addOnCardSelect').selectedIndex=0;
            $('addOnCardsTable').style.display='none';
        }
        
        this.setOrderCombo('addOnCardSelect');
       
    },
    
    setAddons: function() {

        // Loop through add-ons and set checked and qty values if the order changed
        tempIdArray = this.addOnIdArray.clone();
        tempCheckedArray = this.addOnCheckedArray.clone();
        tempQtyArray = this.addOnQtyArray.clone();
        tempMaxQtyArray = this.addOnMaxQtyArray.clone();
        tempDescriptionArray = this.addOnDescriptionArray.clone();
        tempPriceArray = this.addOnPriceArray.clone();
        
        this.addOnIdArray = new Array();
        this.addOnCheckedArray = new Array();
        this.addOnQtyArray = new Array();
        this.addOnMaxQtyArray = new Array();
        this.addOnDescriptionArray = new Array();
        this.addOnPriceArray = new Array();
        tempAddonCount = 1;
        selectedRadio = FTD_DOM.getSelectedRadioButton('productDeliveryMethod');
        if (selectedRadio == null) {
            selectedDeliveryMethod = '';
        } else {
            selectedDeliveryMethod = selectedRadio.id;
        }

        for( temp1=0; temp1<this.productObject.addOnArray.length; temp1++) {
            if (this.productObject.addOnArray[temp1+1] != null && this.productObject.addOnArray[temp1+1].length > 0) {
                tempFloristVendorFlag = this.productObject.addOnFloristVendorArray[temp1+1];
                addonAvailable = false;
                if (selectedDeliveryMethod == 'productDeliveryMethodFlorist' && 
                        (tempFloristVendorFlag=='florist' || tempFloristVendorFlag=='both')) {
                    addonAvailable = true;
                }
                if (selectedDeliveryMethod == 'productDeliveryMethodDropship' &&
                        (tempFloristVendorFlag=='vendor' || tempFloristVendorFlag=='both')) {
                    addonAvailable = true;
                }

                if (addonAvailable) {
                    this.addOnIdArray[tempAddonCount] = this.productObject.addOnArray[temp1+1];
                    this.addOnCheckedArray[tempAddonCount] = false;
                    this.addOnQtyArray[tempAddonCount] = 0;
                    this.addOnMaxQtyArray[tempAddonCount] = this.productObject.addOnMaxQtyArray[temp1+1];
                    this.addOnDescriptionArray[tempAddonCount] = this.productObject.addOnDescriptionArray[temp1+1];
                    this.addOnPriceArray[tempAddonCount] = this.productObject.addOnPriceArray[temp1+1];
                    for( temp2=0; temp2<tempIdArray.length; temp2++) {
                        if (this.productObject.addOnArray[temp1+1] == tempIdArray[temp2+1]) {
                            this.addOnCheckedArray[tempAddonCount] = tempCheckedArray[temp2+1];
                            this.addOnQtyArray[tempAddonCount] = tempQtyArray[temp2+1];
                        }
                    }
                    if (this.productObject.addOnTypeArray[temp1+1] == '6') {
                        this.addOnCheckedArray[tempAddonCount] = true;
                        this.addOnQtyArray[tempAddonCount] = 1;
                    }
                    tempAddonCount = tempAddonCount + 1;
                    if (tempAddonCount > v_max_addons) {
                        break;
                    }
                }
            }
        }

        for (temp1=0; temp1<tempIdArray.length; temp1++) {
            if (tempIdArray[temp1+1] != null && tempIdArray[temp1+1].length > 0) {
                tempChecked = tempCheckedArray[temp1+1];
                if (tempChecked) {
                    newTempChecked=false;
                    for( temp2=0; temp2<this.addOnIdArray.length; temp2++) {
                        if (this.addOnIdArray[temp2+1] != null && this.addOnIdArray[temp2+1].length > 0 &&
                                this.addOnIdArray[temp2+1] == tempIdArray[temp1+1]) {
                            newTempChecked = this.addOnCheckedArray[temp2+1];
                            //alert('Found match: ' + this.addOnIdArray[temp2+1] + ' ' + newTempChecked);
                            break;
                        }
                    }
                    if (newTempChecked==false) {
                        //alert(tempIdArray[temp1+1] + ' / ' + tempDescriptionArray[temp1+1] + ' is checked ' + newTempChecked);
                        if (v_deliveryMethodMsgDisplayed == true) {
                            notifyUser('<br>One or more of the previously selected Addons are no longer available.',true,true);
                        } else {
                            notifyUser('One or more of the previously selected Addons are no longer available.',false,true);
                        }
                        v_deliveryMethodMsgDisplayed = false;
                        v_addOnMsgDisplayed = true;
                        break;
                    }
                }
            }
        }

        this.addOnVaseArray = new Array();
        this.addOnVaseDeliveryTypeArray = new Array();
        tempVaseCount = 1;
        for( temp1=0; temp1<this.productObject.vaseArray.length; temp1++) {
            if (this.productObject.vaseArray[temp1+1] != null && this.productObject.vaseArray[temp1+1].length > 0) {
                tempFloristVendorFlag = this.productObject.vaseFloristVendorArray[temp1+1];
                vaseAvailable = false;
                if (selectedDeliveryMethod == 'productDeliveryMethodFlorist' && 
                        (tempFloristVendorFlag=='florist' || tempFloristVendorFlag=='both')) {
                    vaseAvailable = true;
                }
                if (selectedDeliveryMethod == 'productDeliveryMethodDropship' &&
                        (tempFloristVendorFlag=='vendor' || tempFloristVendorFlag=='both')) {
                    vaseAvailable = true;
                }
//                alert(this.productObject.vaseArray[temp1+1] + ' ' + tempFloristVendorFlag + ' ' + vaseAvailable);
                if (vaseAvailable) {
                    this.addOnVaseArray[tempVaseCount] = this.productObject.vaseArray[temp1+1];
                    this.addOnVaseDeliveryTypeArray[tempVaseCount] = this.productObject.vaseFloristVendorArray[temp1+1];
                    tempVaseCount = tempVaseCount + 1;
                    if (tempVaseCount > v_max_vases) {
                        break;
                    }
                }
            }
        }

        this.productObject.productAddonVase = this.productAddonVase;
    },
    
    setCarrierOptions: function() {
        FTD_DOM.clearSelect('carrierDeliveryDateGR');
        for( var i in this.deliveryDaysGR ) {
            if( !Object.isFunction(this.deliveryDaysGR[i]) )
                FTD_DOM.insertOption('carrierDeliveryDateGR',this.deliveryDaysGR[i].displayString,this.deliveryDaysGR[i].valueString,false);
        }
        this.setOrderCombo('carrierDeliveryDateGR');
        $('carrierDeliveryOptGRRow').style.display = $('carrierDeliveryDateGR').options.length>0?'inline':'none';
        
        FTD_DOM.clearSelect('carrierDeliveryDate2F');
        for( var i in this.deliveryDays2F ) {
            if( !Object.isFunction(this.deliveryDays2F[i]) )
                FTD_DOM.insertOption('carrierDeliveryDate2F',this.deliveryDays2F[i].displayString,this.deliveryDays2F[i].valueString,false);
        }
        this.setOrderCombo('carrierDeliveryDate2F');
        $('carrierDeliveryOpt2FRow').style.display = $('carrierDeliveryDate2F').options.length>0?'inline':'none';
        
        FTD_DOM.clearSelect('carrierDeliveryDateND');
        for( var i in this.deliveryDaysND ) {
            if( !Object.isFunction(this.deliveryDaysND[i]) )
                FTD_DOM.insertOption('carrierDeliveryDateND',this.deliveryDaysND[i].displayString,this.deliveryDaysND[i].valueString,false);
        }
        this.setOrderCombo('carrierDeliveryDateND');
        $('carrierDeliveryOptNDRow').style.display = $('carrierDeliveryDateND').options.length>0?'inline':'none';
        
        FTD_DOM.clearSelect('carrierDeliveryDateSA');
        for( var i in this.deliveryDaysSA ) {
            if( !Object.isFunction(this.deliveryDaysSA[i]) )
                FTD_DOM.insertOption('carrierDeliveryDateSA',this.deliveryDaysSA[i].displayString,this.deliveryDaysSA[i].valueString,false);
        }
        this.setOrderCombo('carrierDeliveryDateSA');
        $('carrierDeliveryOptSARow').style.display = $('carrierDeliveryDateSA').options.length>0?'inline':'none';
        
        FTD_DOM.clearSelect('carrierDeliveryDateSU');
        for( var i in this.deliveryDaysSU ) {
            if( !Object.isFunction(this.deliveryDaysSU[i]) )
                FTD_DOM.insertOption('carrierDeliveryDateSU',this.deliveryDaysSU[i].displayString,this.deliveryDaysSU[i].valueString,false);
        }
        this.setOrderCombo('carrierDeliveryDateSU');
        $('carrierDeliveryOptSURow').style.display = $('carrierDeliveryDateSU').options.length>0?'inline':'none';
    },
    
    setOrderValue: function(propertyName,defaultValue,actionIfNull,actionElementId) {
        var newValue = this[propertyName];
        if( UTILS.isEmpty(newValue)==true ) newValue = defaultValue;
                
        var el = $(propertyName);
        
        if( el.nodeName.toUpperCase()=='DIV' || el.nodeName.toUpperCase()=='SPAN' ) {
            el.innerHTML = newValue;
        } else if( el.nodeName.toUpperCase()=='INPUT' ) {
            if( el.type.toUpperCase()=='TEXT' ) {
                el.value=newValue;
            } else if( el.type.toUpperCase()=='CHECKBOX' ) {
                el.checked = newValue;
            } else if( el.type.toUpperCase()=='HIDDEN' ) {
                el.checked = newValue;
            } 
        } else if( el.nodeName.toUpperCase()=='TEXTAREA' ) {
            el.value=newValue;
        } else if( el.nodeName.toUpperCase()=='SELECT' ) {
            if( el.type.toUpperCase()=='SELECT-ONE' ) {
                var selectedIndex = el.selectedIndex;
                if( selectedIndex == -1) {
                    this[el.id]=null;
        } else {
                    this[el.id]=el[el.selectedIndex].value;
                }
            }
        } else {
            alert('nodeName: '+el.nodeName);
        }
    },
    
    setOrderRadioGroup: function(propertyName) {
        if( propertyName!=null ) {
            var el = $(this[propertyName]);
        
            if( el!=null ) {
                el.checked = true;
                if( el.type=='checkbox') {
                    FTD_DOM.oneOrNoCheckboxGroup(el);
                }
            }
        }
    },
    
    setOrderCombo: function(propertyName) {
        if( propertyName!=null ) {
            var newValue = this[propertyName];
            if( newValue==null ) {
                if( $(propertyName).length>0 ) {
                    $(propertyName).options[0].selected = true;
                }
            } else {
                FTD_DOM.selectOptionByValue($(propertyName),newValue)
            }
        }
    },
    
    setOrderProductValue: function(propertyName,defaultValue,actionIfNull,actionElementId) {
        if( this.productObject!=null ) {
            var newValue = this.productObject[propertyName];
            if( UTILS.isEmpty(newValue)==true ) {
                newValue = defaultValue;
                //alert('actionIfNull: '+actionIfNull+'  actionElementId: '+actionElementId);
                if( typeof actionIfNull!=undefined &&actionIfNull!=null && typeof actionElementId!=undefined && actionElementId!=null ) {
                    if( actionIfNull.toUpperCase()=='DISPLAY' ) {
                        $(actionElementId).style.display = 'none';
                    } else if( actionIfNull.toUpperCase()=='VISIBILITY-COLLAPSE' ) {
                        $(actionElementId).style.visibility = 'collapse';
                    }
                }
            } else {
                if( typeof actionIfNull!=undefined && actionIfNull!=null && typeof actionElementId!=undefined && actionElementId!=null ) {
                    if( actionIfNull.toUpperCase()=='DISPLAY' ) {
                        if( $(actionElementId).nodeName.toUpperCase()=='DIV' ) {
                            $(actionElementId).style.display = 'block';
                        } else if( $(actionElementId).nodeName.toUpperCase()=='TR' ) {
                            $(actionElementId).style.display = 'table-row';
                        } else {
                            $(actionElementId).style.display = 'inline';
                        }
                    } else if( actionIfNull.toUpperCase()=='VISIBILITY-COLLAPSE' ) {
                        $(actionElementId).style.visibility = 'visible';
                    }
                }
            }
                
            var el = $(propertyName);
            
            if( el.nodeName.toUpperCase()=='DIV' || el.nodeName.toUpperCase()=='SPAN' ) {
                el.innerHTML = newValue;
            } else if( el.nodeName.toUpperCase()=='INPUT' ) {
                if( el.type.toUpperCase()=='TEXT' ) {
                    el.value=newValue;
                }
            }
        }
    },
    
    populateThumbNail: function() {
        if( this.productObject!=null ) {
            var newValue = this.productObject.smallImage;
            if( newValue==null ) {
                newValue = 'images/noproduct.gif';
            }
            
            $('ci_smallImage'+v_indexPrefix+this.cartListIndex).src = newValue;
            $('ci_smallImage'+v_indexPrefix+this.cartListIndex).title = productVerbageCleanup(this.productObject.name);
            $('ci_smallImage'+v_indexPrefix+this.cartListIndex).alt = productVerbageCleanup(this.productObject.name);

            if (this.productObject.allowFreeShippingFlag == null || this.productObject.allowFreeShippingFlag == 'Y') {
                $('ci_notFSEligible'+v_indexPrefix+this.cartListIndex).style.display = 'none';
            } else {
                $('ci_notFSEligible'+v_indexPrefix+this.cartListIndex).style.display = 'block';
            }
            var selectedRadio = FTD_DOM.getSelectedRadioButton('productDeliveryMethod');
            if (selectedRadio != null) {
                //alert(selectedRadio.id);
                if (selectedRadio.id=='productDeliveryMethodFlorist') {
                    $('ci_deliveryType___'+v_currentCartIdx).src = 'images/florist-delivery.png';
                    $('ci_deliveryType___'+v_currentCartIdx).alt = 'Florist Delivery';
                    $('ci_deliveryType___'+v_currentCartIdx).title = 'Florist Delivery';
                    $('ci_deliveryType___'+v_currentCartIdx).width = '30';
                    previousDeliveryMethod = 'florist';
                } else if (selectedRadio.id=='productDeliveryMethodDropship') {
                    $('ci_deliveryType___'+v_currentCartIdx).src = 'images/vendor-delivery.png';
                    $('ci_deliveryType___'+v_currentCartIdx).alt = 'Vendor Delivery';
                    $('ci_deliveryType___'+v_currentCartIdx).title = 'Vendor Delivery';
                    $('ci_deliveryType___'+v_currentCartIdx).width = '60';
                    previousDeliveryMethod = 'dropship';
                }
            }
        }
        
        this.setThumbNailValue('productId','');
        this.setThumbNailValue('recipientFirstName','');
        this.setThumbNailValue('recipientLastName','');
    },
    
    setThumbNailValue: function(propertyName,defaultValue) {
        var newValue = this[propertyName];
        if( UTILS.isEmpty(newValue)==true || newValue=='null' )
            newValue = defaultValue;
            
        $('ci_'+propertyName+v_indexPrefix+this.cartListIndex).innerHTML = newValue;
    },
    
    populateSubCodeCombo: function() {
        FTD_DOM.clearSelect('productOptionSubCode');
        FTD_DOM.insertOption('productOptionSubCode','','',false);
        
        if( this.productObject!=null ) {
            for( var idx=0; idx<this.productObject.subcodeArray.length; idx++) {
                var selectThis=false;
                if( this.productObject.subcodeArray[idx].productId==this.productId ) {
                    selectThis = true;
                    this.productOptionSubCode = this.productObject.subcodeArray[idx].productId;
                }
                var pricePrefix;
                var priceSuffix = ' - '+this.productObject.subcodeArray[idx].subcodeDescription+' ('+this.productObject.subcodeArray[idx].productId+')';
                
                var appliedDiscountType='';
                var appliedStandardDiscount;
                var appliedStandardPrice = this.productObject.subcodeArray[idx].standardPrice;
                
                var iotwApplies = this.productObject.subcodeArray[idx].iotwObject.doesIOTWPricingApply(this.productDeliveryDate);
                if( iotwApplies==true ) {
                    appliedDiscountType = this.productObject.subcodeArray[idx].iotwObject.discountType;
                    appliedStandardDiscount = this.productObject.subcodeArray[idx].iotwObject.standardDiscount;
                } else {
                    appliedDiscountType = this.productObject.subcodeArray[idx].discountType;
                    appliedStandardDiscount = this.productObject.subcodeArray[idx].standardDiscount;
                }
                
                if( UTILS.isEmpty(appliedDiscountType)==true ) {
                    pricePrefix = appliedStandardPrice;
                } else if( appliedDiscountType=='Discount' ) {
                    
                    pricePrefix = appliedStandardDiscount+
                    ' ('+
                    appliedStandardPrice+
                    ')';
                } else {
                    pricePrefix = appliedStandardPrice+
                    ' ('+
                    appliedStandardDiscount+
                    ' '+
                    appliedDiscountType+
                    ')';
                }
                FTD_DOM.insertOption(
                    'productOptionSubCode',
                    '$'+pricePrefix+priceSuffix,
                    this.productObject.subcodeArray[idx].productId,
                    selectThis);
            }
        }
    },
    
    populateUpsellCombo: function() {
        FTD_DOM.clearSelect('productOptionUpsell');
        FTD_DOM.insertOption('productOptionUpsell','','',false);
        
        if( this.productObject!=null ) {
            for( var idx=0; idx<this.productObject.upsellArray.length; idx++) {
                var selectThis=false;
                if( this.productObject.upsellArray[idx].productId==this.productId ) {
                    selectThis = true;
                    this.productOptionUpsell = this.productObject.upsellArray[idx].productId;
                }
                var pricePrefix;
                var priceSuffix = ' - '+this.productObject.upsellArray[idx].subcodeDescription+' ('+this.productObject.upsellArray[idx].productId+')';
                
                var appliedDiscountType='';
                var appliedStandardDiscount;
                var appliedStandardPrice = UTILS.addCommas(this.productObject.upsellArray[idx].standardPrice);
            
                var iotwApplies = this.productObject.upsellArray[idx].iotwObject.doesIOTWPricingApply(this.productDeliveryDate);
                if( iotwApplies==true ) {
                    appliedDiscountType = this.productObject.upsellArray[idx].iotwObject.discountType;
                    appliedStandardDiscount = UTILS.addCommas(this.productObject.upsellArray[idx].iotwObject.standardDiscount);
                } else {
                    appliedDiscountType = this.productObject.upsellArray[idx].discountType;
                    appliedStandardDiscount = UTILS.addCommas(this.productObject.upsellArray[idx].standardDiscount);
                }
                
                if( UTILS.isEmpty(appliedDiscountType)==true ) {
                    pricePrefix = appliedStandardPrice;
                } else if( appliedDiscountType=='Discount' ) {
                    
                    pricePrefix = appliedStandardDiscount+
                    ' ('+
                    appliedStandardPrice+
                    ')';
                } else {
                    pricePrefix = appliedStandardPrice+
                    ' ('+
                    appliedStandardDiscount+
                    ' '+
                    appliedDiscountType+
                    ')';
                }
                
                FTD_DOM.insertOption(
                    'productOptionUpsell',
                    '$'+pricePrefix+priceSuffix,
                    this.productObject.upsellArray[idx].productId,
                    selectThis);
            }
        }
    },
    
    populateColorCombo: function() {
        FTD_DOM.clearSelect('productOptionColor');
        FTD_DOM.clearSelect('productOptionColor2');
        FTD_DOM.insertOption('productOptionColor','','',true);
        FTD_DOM.insertOption('productOptionColor2','','',true);
        
        if( this.productObject!=null ) {
            for( var idx=0; idx<this.productObject.colorArray.length; idx++) {
                FTD_DOM.insertOption('productOptionColor',this.productObject.colorArray[idx].display,this.productObject.colorArray[idx].code,false);
                FTD_DOM.insertOption('productOptionColor2',this.productObject.colorArray[idx].display,this.productObject.colorArray[idx].code,false);
            }
        }
    },
    
    set: function() {
        var keys = Object.keys(this);
        for( var idx=0; idx<keys.length; idx++ ) {
            var obj = this[keys[idx]];
            
            if( typeof obj == 'function' ) continue;
//            if( obj=='lastAvailabilityValues' ) {
//                this.lastAvailabilityValues = v_lastAvailabilityOrderValues;
//                continue;
//            }
            
            var el = $(keys[idx]);
            if( el!=null ) {
                
                if( el.nodeName.toUpperCase()=='INPUT' ) {
                    if( el.type.toUpperCase()=='TEXT' || el.type.toUpperCase()=='HIDDEN' ) {
                        this[el.id]=el.value;
                        //alert(orderObj[el.id]);
                    } else if( el.type.toUpperCase()=='CHECKBOX' ) {
                        //this[el.id]=el.checked;
                        
                        //Is this a checkbox group
                        if( UTILS.isEmpty(el.name) ) {
                            this[el.id]=el.checked;
                        } else {
                            var checkboxes = document.getElementsByTagName(el.name);
                            if( checkboxes.length==1 ) {
                                this[el.id]=el.checked;
                            } else {
                                el = FTD_DOM.getSelectedRadioButton(el.name);
                                if( el!=null ) {
                                    this[el.name] = el.id;
                                }
                            }
                        }
                    } 
                } else if( el.nodeName.toUpperCase()=='TEXTAREA' ) {
                    this[el.id]=el.value;
                } else if( el.nodeName.toUpperCase()=='SELECT' ) {
                    if( el.type.toUpperCase()=='SELECT-ONE' ) {
                        var selectedIndex = el.selectedIndex;
                        if( selectedIndex == -1) {
                            this[el.id]=null;
                        } else {
                            this[el.id]=el[el.selectedIndex].value;
                        }
                    }
                } else if( el.nodeName.toUpperCase()=='DIV' || el.nodeName.toUpperCase()=='SPAN' ) {
                        this[el.id]=el.innerHTML;
                } else {
                    alert('nodeName: '+el.nodeName+'     type: '+el.type);
                }
            } 
            else {
                //Check for radio/checkbox button groups
                el = FTD_DOM.getSelectedRadioButton(keys[idx]);
                if( el!=null ) {
                    this[el.name]=el.id;
                } 
            }
        }
    },

    validate: function(checkOnly) {
        var retval = true;
        
        for( var i in this.orderFields ) {
            if( Object.isFunction(i)==true ) continue;
            if( validateField(this.orderFields[i],checkOnly )==false ) {
                retval = false;
            }
        }
        
        return retval;
    },
    
    validateField: function(elementId,checkOnly) {    
        var retval = true;
        var testValue='';
        var fldObj = this.orderFields[elementId];
        var el = $(elementId);
        
        if( !el || el==null || Object.isElement(el)==false ) {
            return true;
        }
        
        try {
            if(el.type == 'select' ) {
                testValue = UTILS.trim(FTD_DOM.getSelectedValue());
            } else if(el.type == 'text' || 
                      el.type == 'textarea' || 
                      el.type == 'select-one' || 
                      el.type == 'password') {
                testValue = this[fldObj.elementId];
            } else if(el.type == 'radio' || 
                      el.type == 'checkbox') {
                testValue = 'donottest'; 
            } else {
                //alert('Unsupported control of type '+el.type+' being validated for control '+el.id);
                return true;
            }
            
        } catch (err) {
            alert('Error getting value from '+el.id+' control: '+err.description);
            return true;
        }
        
        if( fldObj.requiredFlag==true || fldObj.validateFlag==true ) {
            
            var isrequired=this.isRequired(elementId);
            if( isrequired==true && UTILS.isEmpty(testValue)==true ) {
                retval = false;
                var x= this.updateRequest(fldObj);
            } else if( isrequired==false && UTILS.isEmpty(testValue)==true ) {
                retval = true;
            } else if( fldObj.validateFlag==true ) {
                var valFlag=false;
                if( fldObj.validateExpression==null || fldObj.validateExpression=='undefined' /*|| fldObj.validateExpression.length==0*/ ) {
                    valFlag = false;
                } else {
                    if( fldObj.validateExpressionType=='REGEX') {
                        valFlag=fldObj.validateExpression.test(testValue);
                    } else if(fldObj.validateExpressionType=='PASSED_FUNCTION') {
                        if( Object.isFunction(fldObj.validateExpression)==false )
                            fldObj.validateExpression = UTILS.convertToJavaScript(fldObj.validateExpression);
                        valFlag = fldObj.validateExpression(this.cartListIndex);
                    } else if(fldObj.validateExpressionType=='FUNCTION') {
                        if( Object.isFunction(fldObj.validateExpression)==false )
                            fldObj.validateExpression = UTILS.convertToJavaScript(fldObj.validateExpression);
                        valFlag = fldObj.validateExpression(this.cartListIndex,fldObj.elementId,fldObj);
                    }
                }
                
                if( valFlag==false ) {
                    retval = false;
                } 
            }
        }
        
        if( retval==true && el.type=='textarea' && isNaN(fldObj.maxLength)==false && UTILS.isEmpty(testValue)==false && testValue.length > parseInt(fldObj.maxLength) ) {
            retval = false;
            fldObj.errorText = 'Entry is too large.  Reduce to '+fldObj.maxLength+' characters or less.';
        } else {
            fldObj.errorText = fldObj.errorTextNormal;
        }
        
        if( checkOnly==false ) { 
            if( retval==false ) {
                fldObj.errorFlag = true;
            } else {
                fldObj.errorFlag = false;
            }
        }
        
        return retval;
    },
    
    isRequired: function(elementId) {
        var retval;
        var fldObj = this.orderFields[elementId];
        
        try {
            if( fldObj.requiredFlag==true ) {
                if( fldObj.requiredCondition==null || fldObj.requiredCondition=='undefined' /*|| fldObj.requiredCondition.length==0*/ ) {
                    retval = true;
                } else {
                    if(fldObj.requiredConditionType=='PASSED_FUNCTION') {
                        if( Object.isFunction(fldObj.requiredCondition)==false )
                            fldObj.requiredCondition = UTILS.convertToJavaScript(fldObj.requiredCondition);
                        retval = fldObj.requiredCondition(this.cartListIndex);
                    } else if(fldObj.requiredConditionType=='FUNCTION') {
                        if( Object.isFunction(fldObj.requiredCondition)==false )
                            fldObj.requiredCondition = UTILS.convertToJavaScript(fldObj.requiredCondition)
                        retval = fldObj.requiredCondition(this.cartListIndex,elementId);
                    }
                }
            } else {
                retval = false;
            }
        
            var labelEl = $(fldObj.labelElementId);
        
            if(labelEl) {
                var currentValue = labelEl.innerHTML;
                var testValue = currentValue.substring(0,v_fieldRequiredHtml.length);
                if( testValue==v_fieldRequiredHtml ) {
                    currentValue = currentValue.substring(v_fieldRequiredHtml.length);
                }
                fldObj.labelText = currentValue;
                if( retval==true ) {
                    labelEl.innerHTML=v_fieldRequiredHtml+currentValue;
                } else {
                    labelEl.innerHTML=currentValue;    
                }
            }
        } catch(err) {
            alert('Order function isRequired failed for field '+elementId);
            throw(err.description);
        }
        
        return retval;
    },
    updateRequest:function(fldObj){
    	if(fldObj.elementId=='cardMessage'){
    		fldObj.errorTextNormal="You must enter the card message";
    	}
    	return true;
    }
};

// ]]>
