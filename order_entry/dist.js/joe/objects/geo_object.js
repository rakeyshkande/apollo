// <![CDATA[
var GEO = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("GEO requires the Prototype JavaScript framework >= 1.5");
      
GEO.COUNTRY = Class.create();

GEO.COUNTRY.prototype = {

    initialize: function(comboPrompt,comboValue) {
        this.reset();
        if(comboPrompt) {
            this.comboPrompt=comboPrompt;
        }
        
        if(comboValue) {
            this.comboValue=comboValue;
        }
    },
    
    reset: function() {
        this.comboPrompt=null;
        this.comboValue=null;
        this.domestic=true;
        this.defaultValue=false;
    },
    
    populateDOM: function() {
    },
    
    populateObject: function() {
    }
};
GEO.STATE = Class.create();

GEO.STATE.prototype = {

    initialize: function(comboPrompt,comboValue) {
        this.reset();
        if(comboPrompt) {
            this.comboPrompt=comboPrompt;
        }
        
        if(comboValue) {
            this.comboValue=comboValue;
        }
    },
    
    reset: function() {
        this.comboPrompt=null;
        this.comboValue=null;
        this.countryId=null;
    },
    
    populateDOM: function() {
    },
    
    populateObject: function() {
    }
};

GEO.POSTAL = Class.create();

GEO.POSTAL.prototype = {

    initialize: function(postalCode,city,stateId) {
        this.reset();
        if(postalCode) {
            this.postalCode=postalCode;
        }
        
        if(city) {
            this.city=city;
        }
        
        if(stateId) {
            this.stateId=stateId;
        }
    },
    
    reset: function() {
        this.postalCode=null;
        this.city=null;
        this.stateId=null;
    },
    
    populateDOM: function() {
    },
    
    populateObject: function() {
    }
};

// ]]>