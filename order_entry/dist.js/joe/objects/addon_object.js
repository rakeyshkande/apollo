// <![CDATA[
var ADDON = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("ADDON requires the Prototype JavaScript framework >= 1.5");
      
ADDON.OBJECT = Class.create();

ADDON.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.id=null;
        this.type=null;
        this.description=null;
        this.price=null;
        this.image=null;
        this.insideText=null;
        this.image=v_defaultAddonProductImage;
        this.occasions=new Array();
    }
};
// ]]>
      