// <![CDATA[
var ADDRESS_TYPE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("ADDRESS_TYPE requires the Prototype JavaScript framework >= 1.5");
      
ADDRESS_TYPE.OBJECT = Class.create();

ADDRESS_TYPE.OBJECT.prototype = {

    initialize: function(comboPrompt,comboValue) {
        this.reset();
        if(comboPrompt) {
            this.comboPrompt=comboPrompt;
        }
        
        if(comboValue) {
            this.comboValue=comboValue;
        }
    },
    
    reset: function() {
        this.comboPrompt=null;
        this.comboValue=null;
        this.business=false;
        this.businessLabel='Business Name';
        this.lookup=false;
        this.lookupLabel='Search';
        this.room=false;
        this.roomLabel='Room';
        this.hours=false;
        this.hoursLabel='Hours';
        this.defaultValue=false;
    },
    
    populateDOM: function() {
    },
    
    populateObject: function() {
    }
};

// ]]>