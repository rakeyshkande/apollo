// <![CDATA[
var CROSS_SELL = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || CROSS_SELL.prototypeVersion < 1.6)
      throw("CROSS_SELL requires the Prototype JavaScript framework >= 1.6");
      
CROSS_SELL.OBJECT = Class.create();

CROSS_SELL.OBJECT.prototype = {

    initialize: function(productCrossSellId) {
        this.reset();
        if( productCrossSellId!=undefined && productCrossSellId!=null ) {
            this.product = new PRODUCT.OBJECT(productCrossSellId);
        }
    },
    
    reset: function() {

        this.productCrossSellId = null;
        
        this.productId = null;        
        
        this.detailProducts = new Array();
        
    }
};

// ]]>
