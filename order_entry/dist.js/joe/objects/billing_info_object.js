// <![CDATA[
var BILLING_INFO = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("BILLING_INFO requires the Prototype JavaScript framework >= 1.5");
      
BILLING_INFO.OBJECT = Class.create();

BILLING_INFO.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.sequence=null;            //INFO_SEQUENCE
        this.regex=null;               //INFO_FORMAT
        this.displayType=null;         //PROMPT_TYPE
        this.label=null;               //INFO_DISPLAY
        this.description=null;         //INFO_DESCRIPTION
        this.billingOptions = new Array();    //Array of BILLING_INFO_OPTION objects
    }
};

// ]]>