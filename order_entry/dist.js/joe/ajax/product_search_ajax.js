// <![CDATA[
var PRODUCT_SEARCH_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PRODUCT_SEARCH_AJAX.prototypeVersion < 1.6)
      throw("PRODUCT_SEARCH_AJAX requires the Prototype JavaScript framework >= 1.6");


PRODUCT_SEARCH_AJAX.start = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_PRODUCT_SEARCH';
    root.setAttributeNode(attr);
    
    var keyword = $('productSearchKeywords').value;
    param = FTD_XML.createElementWithText(doc,'param',keyword);
    attr = doc.createAttribute("name");
    attr.value = 'KEYWORD';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var countryId = FTD_DOM.getSelectedValue($('productSearchCountryCombo'));
    param = FTD_XML.createElementWithText(doc,'param',countryId);
    attr = doc.createAttribute("name");
    attr.value = 'COUNTRY_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //Only include the delivery date if it's a domestic product
    if( isCountryDomestic(countryId)==true ) {
        var ddObj = v_deliveryDates[FTD_DOM.getSelectedValue($('productSearchDeliveryDate'))];
        if( ddObj!=null && !Object.isString(ddObj) ) {
            var deliveryDate = ddObj.valueString;
            param = FTD_XML.createElementWithText(doc,'param',deliveryDate);
            attr = doc.createAttribute("name");
            attr.value = 'DELIVERY_DATE';
            param.setAttributeNode(attr);
            root.appendChild(param);
            
            if( ddObj.isDateRange==true ) {
                param = FTD_XML.createElementWithText(doc,'param',UTILS.formatToServerDate(ddObj.endDate));
                attr = doc.createAttribute("name");
                attr.value = 'DATE_RANGE_FLAG';
                param.setAttributeNode(attr);
                root.appendChild(param);
                
            }
            
            var zipCode = $('productSearchZip').getValue();
            param = FTD_XML.createElementWithText(doc,'param',zipCode+'');
            attr = doc.createAttribute("name");
            attr.value = 'ZIP_CODE';
            param.setAttributeNode(attr);
            root.appendChild(param);
        }
    }
    
    var sourceCode = $('callDataSourceCode').value;
    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
//    var companyId = $('callDataCompany').value;
//    var param = FTD_XML.createElementWithText(doc,'param',companyId);
//    attr = doc.createAttribute("name");
//    attr.value = 'COMPANY_ID';
//    param.setAttributeNode(attr);
//    root.appendChild(param);
    
    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'target','SEARCH'));
    v_productSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_productSearchId+''));
    root.appendChild(param);
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,PRODUCT_SEARCH_AJAX.productSearchCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
} 

PRODUCT_SEARCH_AJAX.productSearchCallback = function(transport) {
//    alert(transport.responseText);
    PRODUCT_SEARCH.stopSearch();
    
        logMessage("product search");

        var jsonRaw = transport.responseText;
        var jsonContent = eval("(" + jsonRaw + ")");

        var target = jsonContent.result.echo.target;
        var searchId = jsonContent.result.echo.search_id;
        var message = jsonContent.result.rs.message;

        if( searchId==v_productSearchId || searchId==null || searchId.length==0 ) {
            if( target=='SEARCH' ) {
                PRODUCT_SEARCH.searchProducts = new Array();
                if (jsonContent.result.rs.record == null) {
                    $('psNoResults').style.display = 'block';
                    if(message != null)
                    {
                      $('psTopNoResults').innerHTML = message;
                    }
                    $('psTopNoResults').style.display = 'block';
                    if ( $('topSellersDivLabel').innerHTML == 'Suggest Product Alternatives' ) {
                        new Effect.SlideUp('psTopSellersDivId');
                        PRODUCT_SEARCH_AJAX.getPopularProducts(true);
                    }
                    PRODUCT_SEARCH.openTopSellersPanel('Top Selling Products');
                } else {
                    var results = makeJSONArray(jsonContent.result.rs.record);
                    var recordsLength = results.length;
                    for(var idx = 0; idx < recordsLength; idx++) {
                        prodObj=PRODUCT_SEARCH.populateProductObjectFromSearchJSON(results[idx], idx);
                        prodObj.index=idx;
                        PRODUCT_SEARCH.searchProducts.push(prodObj);
                    }
                    
                    PRODUCT_SEARCH.changeSortOrder($('productSearchSortCombo').value);
                    PRODUCT_SEARCH.setPageNavigation(recordsLength);
                    PRODUCT_SEARCH.setCurrentSearchPage(0);
                }
            } else if( target=='TOP' ) {
                PRODUCT_SEARCH.topProducts = new Array();
                if (jsonContent.result.rs.record != null) {
                    var results = makeJSONArray(jsonContent.result.rs.record);
                    var recordsLength = results.length;
                    for(var idx = 0; idx < recordsLength; idx++) {
                        prodObj=PRODUCT_SEARCH.populateProductObjectFromSearchJSON(results[idx], idx);
                        prodObj.index=idx;
                        PRODUCT_SEARCH.topProducts.push(prodObj);
                    }
                    PRODUCT_SEARCH.setTopPageNavigation(recordsLength);
                    PRODUCT_SEARCH.setTopCurrentSearchPage(0);

                    var openResults = jsonContent.result.echo.open_results;
                    if( openResults=='true' ) {
                        FTD_DOM.selectOptionByValue('productSearchDeliveryDate',FTD_DOM.getSelectedValue('productDeliveryDate'));
                        FTD_DOM.selectOptionByValue('productSearchCountryCombo',FTD_DOM.getSelectedValue('productCountry'));
                        PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
                        $('productSearchZip').value = $('productZip').value;
                        $('productSearchKeywords').value='';
                        PRODUCT_SEARCH.openTopSellersPanel('Top Selling Products');
                    }
                }
            }
        }

        logMessage("product search");

}

PRODUCT_SEARCH_AJAX.getCrossSellProduct = function(productId,itemIdx) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //sourceCode = v_cartList[itemIdx].productSourceCode;
    var sourceCode = $('callDataSourceCode').value;
    if( sourceCode==null ) {
        sourceCode = $('callDataSourceCode').getValue();
        v_cartList[itemIdx].productSourceCode=sourceCode;
    }
    
    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',$('productCountry').getValue());
    attr = doc.createAttribute("name");
    attr.value = 'COUNTRY_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var zipCode = $('productZip').getValue();
    param = FTD_XML.createElementWithText(doc,'param',zipCode+'');
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var ddObj = FTD_DOM.getSelectedValue($('productDeliveryDate'));
    param = FTD_XML.createElementWithText(doc,'param',ddObj);
    attr = doc.createAttribute("name");
    attr.value = 'DELIVERY_DATE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',ddObj.isDateRange?'true':'false');
    attr = doc.createAttribute("name");
    attr.value = 'DATE_RANGE_FLAG';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'itemIdx',itemIdx+''));
    param.appendChild(FTD_XML.createElementWithText(doc,'product_id',productId));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,PRODUCT_SEARCH_AJAX.getCrossSellProductCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    //setProcessing(true);
    newRequest.send();
}

PRODUCT_SEARCH_AJAX.getCrossSellProductCallback = function(transport) {
    //alert(transport.responseText);
//    setProcessing(false);
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");

    PRODUCT_SEARCH.topProducts = new Array();
    if (jsonContent.result.rs.record == null) {
        //if no cross sells are available, then call
        PRODUCT_SEARCH_AJAX.getPopularProducts(true);
    } else {
        var results = makeJSONArray(jsonContent.result.rs.record);
        var recordsLength = results.length;
        for(var idx = 0; idx < recordsLength; idx++) {
            prodObj=PRODUCT_SEARCH.populateProductObjectFromSearchJSON(results[idx], idx);
            prodObj.index=idx;
            PRODUCT_SEARCH.topProducts.push(prodObj);
        }
        PRODUCT_SEARCH.setTopPageNavigation(recordsLength);
        PRODUCT_SEARCH.setTopCurrentSearchPage(0);

        //var openResults = jsonContent.result.echo.open_results;
        var openResults = 'true';
        
        if( openResults=='true' ) {
            FTD_DOM.selectOptionByValue('productSearchDeliveryDate',FTD_DOM.getSelectedValue('productDeliveryDate'));
            FTD_DOM.selectOptionByValue('productSearchCountryCombo',FTD_DOM.getSelectedValue('productCountry'));
            PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
            $('productSearchZip').value = $('productZip').value;
            $('productSearchKeywords').value='';
            $('psNoResults').style.display = 'none';
            $('psTopNoResults').style.display = 'none';
            PRODUCT_SEARCH.openTopSellersPanel('Suggest Product Alternatives');
        }
    }

/*
    PRODUCT_SEARCH.suggestProducts = new Array();
    if (jsonContent.result.rs.record == null) {
        //if no cross sells are available, then call
        PRODUCT_SEARCH_AJAX.getPopularProducts(true);
    } else {
        var results = makeJSONArray(jsonContent.result.rs.record);
        var recordsLength = results.length;
        for(var idx = 0; idx < recordsLength; idx++) {
            prodObj=PRODUCT_SEARCH.populateProductObjectFromSearchJSON(results[idx], idx);
            PRODUCT_SEARCH.suggestProducts.push(prodObj);
        }

        PRODUCT_SEARCH.populateSuggestProducts();
        PRODUCT_SEARCH.openSearch('CROSSSELL');
    }
*/

}

PRODUCT_SEARCH_AJAX.getPopularProducts = function(openOnResults) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_TOP_PRODUCTS';
    root.setAttributeNode(attr);
    
    var companyId = $('callDataCompany').value;
    var param = FTD_XML.createElementWithText(doc,'param',companyId);
    attr = doc.createAttribute("name");
    attr.value = 'COMPANY_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var zipCode = $('productZip').getValue();
    param = FTD_XML.createElementWithText(doc,'param',zipCode+'');
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var sourceCode = $('callDataSourceCode').value;
    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    // The availability check takes to long to process so don't send the delivery date
    var ddObj = v_deliveryDates[FTD_DOM.getSelectedValue($('productSearchDeliveryDate'))];
    if ( ddObj==null || ddObj=='' ) {
        ddObj = v_deliveryDates[FTD_DOM.getSelectedValue($('productDeliveryDate'))];
    }
    if( ddObj!=null && !Object.isString(ddObj) ) {
        var deliveryDate = ddObj.valueString;
        param = FTD_XML.createElementWithText(doc,'param',deliveryDate);
        attr = doc.createAttribute("name");
        attr.value = 'DELIVERY_DATE';
        param.setAttributeNode(attr);
        root.appendChild(param);
        
        if( ddObj.isDateRange==true ) {
            param = FTD_XML.createElementWithText(doc,'param',UTILS.formatToServerDate(ddObj.endDate));
            attr = doc.createAttribute("name");
            attr.value = 'DATE_RANGE_FLAG';
            param.setAttributeNode(attr);
            root.appendChild(param);
        }
    }

    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'target','TOP'));
    param.appendChild(FTD_XML.createElementWithText(doc,'open_results',openOnResults+''));
    root.appendChild(param);
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,PRODUCT_SEARCH_AJAX.productSearchCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

// ]]>