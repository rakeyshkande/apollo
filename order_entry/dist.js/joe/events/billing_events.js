// <![CDATA[
var BILLING_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("BILLING_EVENTS requires the Prototype JavaScript framework >= 1.5");
      
if(typeof FTD_DOM=='undefined')
      throw("BILLING_EVENTS requires FTD_DOM");

BILLING_EVENTS.onPayTypeChange = function(event) {
    BILLING_EVENTS.paymentTypeChanged();
}

BILLING_EVENTS.paymentTypeChanged = function() {
    
    var selectedValue = $('paymentTypeCombo').getValue();
    $('paymentGCNumber').value='';
    FTD_DOM.selectOptionByValue('paymentGCBalanceCombo','');
    $('paymentGCBalanceCombo').style.display='none';
    $('paymentGCBalanceComboLabel').style.display='none';
    if( selectedValue=='N' ) {
        $('paymentCCCaption').style.display = 'none';
        $('paymentCCDiv').style.display     = 'none';
        $('paymentCSCOverride').style.display       = 'none';
        $('paymentCSCOverrideLabel').style.display  = 'none';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentGCCaption').style.display = 'none';
        $('paymentGCDiv').style.display     = 'none';
        $('paymentINCaption').style.display = 'none';
        $('paymentINDiv').style.display     = 'none';
        $('paymentNCCaption').style.display = 'inline';
        $('paymentNCDiv').style.display     = 'inline';
        $('paymentCCAmount').innerHTML = '0.00';
        $('paymentGCAmount').innerHTML='0.00';
        $('paymentINAmount').innerHTML = '0.00';
        $('paymentNCAmount').innerHTML = '0.00';
    } else if( selectedValue=='G' ) {
        $('paymentCCCaption').style.display = 'none';
        $('paymentCCDiv').style.display     = 'none';
        $('paymentCSCOverride').style.display       = 'none';
        $('paymentCSCOverrideLabel').style.display  = 'none';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentGCCaption').style.display = 'inline';
        $('paymentGCDiv').style.display     = 'inline';
        $('paymentINCaption').style.display = 'none';
        $('paymentINDiv').style.display     = 'none';
        $('paymentNCCaption').style.display = 'none';
        $('paymentNCDiv').style.display     = 'none';
        $('paymentCCAmount').innerHTML = '0.00';
        $('paymentGCAmount').innerHTML = '0.00';
        $('paymentINAmount').innerHTML = '0.00';
        $('paymentNCAmount').innerHTML = '0.00';
    } else if( selectedValue=='C' ) {
        $('paymentCCCaption').style.display = 'inline';
        $('paymentCCDiv').style.display     = 'inline';
        $('paymentCSCOverride').style.display       = 'inline';
        $('paymentCSCOverrideLabel').style.display  = 'inline';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentGCCaption').style.display = 'none';
        $('paymentGCDiv').style.display     = 'none';
        $('paymentINCaption').style.display = 'none';
        $('paymentINDiv').style.display     = 'none';
        $('paymentNCCaption').style.display = 'none';
        $('paymentNCDiv').style.display     = 'none';
        $('paymentCCAmount').innerHTML = '0.00';
        $('paymentGCAmount').innerHTML='0.00';
        $('paymentINAmount').innerHTML = '0.00';
        $('paymentNCAmount').innerHTML = '0.00';
    } else if( selectedValue=='I' ) {
        $('paymentCCCaption').style.display = 'none';
        $('paymentCCDiv').style.display     = 'none';
        $('paymentCSCOverride').style.display       = 'none';
        $('paymentCSCOverrideLabel').style.display  = 'none';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentGCCaption').style.display = 'none';
        $('paymentGCDiv').style.display     = 'none';
        $('paymentINCaption').style.display = 'inline';
        $('paymentINDiv').style.display     = 'inline';
        $('paymentNCCaption').style.display = 'none';
        $('paymentNCDiv').style.display     = 'none';
        $('paymentCCAmount').innerHTML = '0.00';
        $('paymentGCAmount').innerHTML='0.00';
        $('paymentINAmount').innerHTML = '0.00';
        $('paymentNCAmount').innerHTML = '0.00';
    }
    setPaymentTypeAmounts();

    for( idx=0; idx<v_billingFields.length; idx++ ) {
        var fld = v_billingFields[idx];
        if( fld!=null && fld.elementId.substring(0,7)=='payment' ) {
            fld.isRequired();
        }
    }
}

BILLING_EVENTS.onGCBalanceChange = function(event) {
    
    var selectedValue = $('paymentGCBalanceCombo').getValue(); 
    
    if( selectedValue=='NC' ) {
        $('paymentCCCaption').style.display = 'none';
        $('paymentCCDiv').style.display     = 'none';
        $('paymentCSCOverride').style.display       = 'none';
        $('paymentCSCOverrideLabel').style.display  = 'none';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentNCCaption').style.display = 'inline';
        $('paymentNCDiv').style.display     = 'inline';
        $('paymentNCType').focus();
    } else if( selectedValue=='CC' ) {
        $('paymentCCCaption').style.display = 'inline';
        $('paymentCCDiv').style.display     = 'inline';
        $('paymentCSCOverride').style.display       = 'inline';
        $('paymentCSCOverrideLabel').style.display  = 'inline';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentNCCaption').style.display = 'none';
        $('paymentNCDiv').style.display     = 'none';
        $('paymentCCNumber').focus();
    } else {
        $('paymentCCCaption').style.display = 'none';
        $('paymentCCDiv').style.display     = 'none';
        $('paymentCSCOverride').style.display       = 'none';
        $('paymentCSCOverrideLabel').style.display  = 'none';
        $('paymentCCManAuthDiv').style.display = 'none';
        $('paymentNCCaption').style.display = 'none';
        $('paymentNCDiv').style.display     = 'none';
    }
    
    setPaymentTypeAmounts();
}

BILLING_EVENTS.onCustomerRecipCheckboxChange = function(event) {
    var checkEl = $('customerRecipSameCheckbox');
    var selectEl = $('customerRecipCombo');
    if( checkEl.checked==true ) {
        FTD_DOM.clearSelect(selectEl);
        FTD_DOM.insertOption(selectEl,'please select','',true);
        setOrderData();
        for( var idx=0; idx<v_cartList.length; idx++ ) {
            orderObj = v_cartList[idx];
            if( orderObj==null ) continue;
            
            FTD_DOM.insertOption(selectEl,UTILS.trim(orderObj.recipientFirstName+' '+orderObj.recipientLastName),idx,false);
        }
        
        if( CART_EVENTS.cartItemCount()==1 ) {
            BILLING_EVENTS.copyRecipientDataToCustomer(0);
            selectEl.selectedIdx=1;
            checkEl.checked=false;
            selectEl.disabled=true;
        } else {
            selectEl.disabled = false;
        }
    } else {
        selectEl.disabled = true;
    }
}

BILLING_EVENTS.onCustomerRecipComboChange = function(event) {
    var srcIdx = FTD_DOM.getSelectedValue($('customerRecipCombo'));
    BILLING_EVENTS.copyRecipientDataToCustomer(srcIdx);
    $('customerRecipSameCheckbox').checked=false;
    $('customerRecipCombo').disabled=true;
}  

BILLING_EVENTS.copyRecipientDataToCustomer = function(srcIdx) {
    var orderObj = v_cartList[srcIdx];
    if( orderObj!= null ) {
        $('customerBillingPhone').value = orderObj['recipientPhone'];
        $('customerBillingPhoneExt').value = orderObj['recipientPhoneExt'];
        $('customerEveningPhone').value = '';
        $('customerEveningPhoneExt').value = '';
        $('customerFirstName').value = orderObj['recipientFirstName'];
        $('customerLastName').value = orderObj['recipientLastName'];
        $('customerAddress').value = orderObj['recipientAddress'];
        $('customerZip').value = orderObj['recipientZip'];
        FTD_DOM.selectOptionByValue('customerCountryCombo',orderObj['recipientCountry']);
        setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
        FTD_DOM.selectOptionByValue('customerStateCombo',orderObj['recipientState']);
        $('customerCity').value = orderObj['recipientCity'];
        $('customerEmailAddress').value = orderObj['recipientEmailAddress'];
        
        var subscribe = (orderObj['recipientOptIn']=='true'?true:false);
        $('customerSubscribeCheckbox').checked = subscribe;
        if( subscribe==true ) {
            $('customerSubscribeCheckbox').style.display='none';
            $('customerSubscribeCheckboxLabel').style.display='none';
            //waitToSetFocus('customerEmailAddress');
        } else {
            $('customerSubscribeCheckbox').style.display='';
            $('customerSubscribeCheckboxLabel').style.display='';
            //waitToSetFocus('customerSubscribeCheckbox');
        }
        try {
        	$('customerBillingPhone').focus();
        } catch(err) {}
        
		CUSTOMER_EVENTS.customerEmailAddressChanged();
    }
}
      
BILLING_EVENTS.onSubmitButton = function(event) {
        $('billingButtonSubmit').disabled=true;
        $('leftDivId').disabled=true;
        $('businessDiv').disabled=true;
        $('actionButtonAbandon').disabled = true;
        $('onSubmitButtonFlagOn').value = 'Y';

    v_okToProcess = true;
    var securityRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'security.do' + getSecurityParams(true),FTD_AJAX_REQUEST_TYPE_POST,CART_EVENTS.securityCallback,true,false);
    securityRequest.send();

    setOrderData();
    if( validate()==true && JOE_VALIDATE.paymentGCBalance()==true && validateCustomerAndRecipientState() == true && v_okToProcess==true ) {
        
        v_checkAvailabilityTimer.stopTimer();
        v_recalcOrderTimer.stopTimer();
        
        ORDER_AJAX.calculateOrderTotals(true);
        
        //if( confirm('The total billed to your credit card will be $'+$('total').innerHTML+'.\r\nDo you want to place this order?')==true) {
        var strMsg = 'The total amount of your order is $'+$('totalOrderAmount').value + '.';
        if (v_freeShippingSavings > 0) {
            strMsg += '\n\n' + submitFreeShippingAppliedMsg;
        }
        strMsg += '\n\nIs there anything else I can help you with before I submit your order?';
        strMsg += '\r\n\r\nPress \"OK\" to submit the order or \"Cancel\" to return to the order.';
        if( confirm(strMsg)==true) {
            ORDER_AJAX.submitOrder();
            $('paymentNCAuthPwd').value='';
        } else {
            $('billingButtonSubmit').disabled=false;
            $('leftDivId').disabled=false;
            $('businessDiv').disabled=false;
            $('actionButtonAbandon').disabled = false;
            v_recalcOrderTimer.startTimer();
            v_checkAvailabilityTimer.startTimer();
        }
    } else {
        $('billingButtonSubmit').disabled=false;
        $('leftDivId').disabled=false;
        $('businessDiv').disabled=false;
        $('actionButtonAbandon').disabled = false;
    }
}     
      
BILLING_EVENTS.onCalculateButton = function(event) {
    if( v_recalcOrderTimer.isRunning() ) {
        v_recalcOrderTimer.stopTimer();
    } 
    $('onSubmitButtonFlagOn').value = 'N';
    ORDER_AJAX.calculateOrderTotals(false);
    
    notifyUser('You have forced the recalculation of the shopping cart.  All validations have been bypassed.  If you have not entered all the pertinent data, the results may not be accurate.<br>',false,true);
}

BILLING_EVENTS.onGCNumberChange = function(event) {
    $('paymentGCAmount').innerHTML='0.00';
    $('paymentCCAmount').innerHTML='0.00';
    $('paymentNCAmount').innerHTML='0.00';
    $('paymentGCBalanceCombo').style.display='none';
    $('paymentGCBalanceComboLabel').style.display='none';
    $('paymentCCCaption').style.display = 'none';
    $('paymentCCDiv').style.display = 'none';
    $('paymentCSCOverride').style.display       = 'none';
    $('paymentCSCOverrideLabel').style.display  = 'none';
    $('paymentCCManAuthDiv').style.display = 'none';
    $('paymentNCCaption').style.display = 'none';
    $('paymentNCDiv').style.display = 'none';
        
    JOE_VALIDATE.validateGiftCertificate();
}

BILLING_EVENTS.onCCNumberChange = function(event) {
	// #813
	// New Update Order will uncheck CSC Override on Billing section when credit card number is edited/change when performing an Update Order
	if (v_modifyOrderFromCom == true)
		{ 
			v_originalWalletIndicator = '';
		    $('paymentCSCOverride').checked=false;
		    $('paymentCSC').disabled = false;
		}
	
    var ccNumber=UTILS.trim($('paymentCCNumber').value);
    if( UTILS.isEmpty(ccNumber)==false ) {
        var ccType = UTILS.getCCTypeFromNumber(ccNumber);
        if( ccType.length>2 ) {
            notifyUser(ccType,false,true);
        } else {
            //There is no way do determine if a card is Diner's Club or
            //Carte Blanche, so, prompt to find out
            if( ccType=='DC' && !confirm('Is this a Diner\'s Club card? Click OK to continue.\nIs this a Carte Blanche card? Click Cancel to return to order. FTD no longer accepts Carte Blanche as a payment type.') ) {
                ccType='';
            }
            FTD_DOM.selectOptionByValue($('paymentCCTypeCombo'),ccType);
            BILLING_EVENTS.ccTypeChanged();
        }
    } 
    $('paymentCSC').value='';
}

BILLING_EVENTS.onPaymentCCTypeChange = function(event) {
	// #813
	// New Update Order will uncheck CSC Override on Billing section when credit card type is edited/change when performing an Update Order
	if (v_modifyOrderFromCom == true)
	{
		v_originalWalletIndicator = '';
	    $('paymentCSCOverride').checked=false;
	    $('paymentCSC').disabled = false;
	}
    var ccNumber=UTILS.trim($('paymentCCNumber').value);
    var enteredCCType = $('paymentCCTypeCombo').getValue();
    
    if( UTILS.isEmpty(ccNumber)==false ) {
        var ccType = UTILS.getCCTypeFromNumber(ccNumber);
        
        if( ccType!=enteredCCType ) {
            var fld = findFieldObject('paymentCCNumber',v_currentCartIdx);
            if( fld!=null ) {
                fld.errorFlag=true;
                fld.setFieldStyle();
            }
            notifyUser('Invalid credit card number.  The credit card number entered does not appear to be a '+enteredCCType+' card.');
        } else {
            BILLING_EVENTS.ccTypeChanged();       
        }
    }
}

BILLING_EVENTS.onPaymentNCTypeChange = function(event) {
    var fld = findFieldObject('paymentNCRefOrder', v_currentCartIdx);
    if( fld!=null ) fld.isRequired();
}

BILLING_EVENTS.ccTypeChanged = function() {              
    var ccType = $('paymentCCTypeCombo').getValue();
    if( ccType=='MS' ) {
        $('paymentCCExpYear').selectedIndex = 0;
        $('paymentCCExpMonthCombo').selectedIndex = 0;
        $('paymentCCExpMonthComboLabel').style.visibility = 'hidden';
        $('paymentCCExpMonthCombo').style.visibility = 'hidden';
        $('paymentCCExpYear').style.visibility = 'hidden';
        $('paymentCSCOverride').style.visibility       = 'hidden';
        $('paymentCSCOverrideLabel').style.visibility  = 'hidden';
        $('paymentCSCLabel').style.visibility  = 'hidden';
        $('paymentCSC').style.visibility  = 'hidden';
	$('paymentCSC').value='';
	var fld = findFieldObject('paymentCSC',v_currentCartIdx);
	if( fld!=null ) {
       		fld.errorFlag=false;
		fld.setFieldStyle();
        }
    } else {
        $('paymentCCExpMonthComboLabel').style.visibility = 'visible';
        $('paymentCCExpMonthCombo').style.visibility = 'visible';
        $('paymentCCExpYear').style.visibility = 'visible';
        $('paymentCSCOverride').style.visibility       = 'visible';
        $('paymentCSCOverrideLabel').style.visibility  = 'visible';
        $('paymentCSCLabel').style.visibility  = 'visible';
        $('paymentCSC').style.visibility  = 'visible';
    }
    
    // MD: add require marker
    for( idx=0; idx<v_billingFields.length; idx++ ) {
        var fld = v_billingFields[idx];
        if( fld!=null && fld.elementId.substring(0,7)=='payment' ) {
            fld.isRequired();
        }
    }
        
    var results = JOE_VALIDATE.validateCreditCardNumber();
    var fld = findFieldObject('paymentCCNumber',v_currentCartIdx);
    if( results!=true ) {
        if( fld!=null ) {
            fld.errorFlag=true;
        }
        notifyUser('Invalid credit card number.  Card number entered fails validation for a '+ccType+' card',false,true);
    } else {
        
        if( fld!=null ) {
            fld.errorFlag=false;
        }
    }
    
    if( fld!=null ) {
        fld.setFieldStyle();
    }
}

BILLING_EVENTS.onPaymentCCManualAuthChange = function(event) {
    var testStr = $('paymentCCManualAuth').getValue;
    
    if( UTILS.isEmpty(testStr)==true ) {
        $('paymentCCNumber').disabled=false;
        $('paymentCCTypeCombo').disabled=false;
        $('paymentCCExpMonthCombo').disabled=false;
        $('paymentCCExpYear').disabled=false;
        $('paymentCSCOverride').disabled=false;
        $('paymentCSC').disabled = false;
    } else {
        $('paymentCCNumber').disabled=true;
        $('paymentCCTypeCombo').disabled=true;
        $('paymentCCExpMonthCombo').disabled=true;
        $('paymentCCExpYear').disabled=true;
        $('paymentCSCOverride').disabled=true;
        $('paymentCSC').disabled = true;
	$('paymentCSC').value='';
	var fld = findFieldObject('paymentCSC',v_currentCartIdx);
	if( fld!=null ) {
       		fld.errorFlag=false;
		fld.setFieldStyle();
        }
    }
}

BILLING_EVENTS.onMembershipSkipClick = function(event) {
    var fld = findFieldObject('membershipId', v_currentCartIdx);
    if( fld!=null ) fld.isRequired();
}

BILLING_EVENTS.onCSCOverrideChange = function(event) {
    if ($('paymentCSCOverride').checked) {
        $('paymentCSC').value='';
	var fld = findFieldObject('paymentCSC',v_currentCartIdx);
	if( fld!=null ) {
       		fld.errorFlag=false;
		fld.setFieldStyle();
        }
        $('paymentCSC').disabled = true;
    } else {
        $('paymentCSC').disabled = false;
    }
}

BILLING_EVENTS.onCSCValueChange = function(event) {
    if( UTILS.isEmpty($('paymentCSC').value)==false ) {
         var fld = findFieldObject('paymentCSC',v_currentCartIdx);
         if( (FTD_DOM.getSelectedValue($('paymentTypeCombo'))=='C' || 
            FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'))=='CC') &&
            FTD_DOM.getSelectedValue($('paymentCCTypeCombo'))!='MS') {
            if( fld!=null ) {
                fld.errorFlag=true;
            }
            if ($('paymentCSC').value.length<3 && $('paymentCSC').value!='') {
                notifyUser('Please enter the 3 or 4 digit card security code found on the customer�s card.', false, true);
            } else if (!/^-?\d+$/.test($('paymentCSC').value)) {
                notifyUser('Invalid card security code. Please confirm card security code and re-enter.', false, true);
            } else {
                if( fld!=null ) {
                    fld.errorFlag=false;
                }
            }
        } else {
            if( fld!=null ) {
                fld.errorFlag=false;
            }
        }
        if( fld!=null ) {
            fld.setFieldStyle();
        }
    }
}
BILLING_EVENTS.onpaymentNCRefOrderChange = function(event) {
	if( UTILS.isEmpty($('paymentNCRefOrder').value)==false ){
	   $('paymentNCRefOrder').value = UTILS.trim($('paymentNCRefOrder').value);
	}
}


// ]]>