// <![CDATA[
var DNIS_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || DNIS_EVENTS.prototypeVersion < 1.6)
      throw("DNIS_EVENTS requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("DNIS_EVENTS requires FTD_DOM");

DNIS_EVENTS.onDnisChange = function(event) {
    //ORDER_EVENTS.startSaleProcess();
    var el = $('callDataDNIS');
    var newValue = UTILS.trim(el.getValue());
    el.value = newValue;
    if( newValue.length>0 ) {
        if( isNaN(newValue) ) {
            el.style.backgroundColor = v_errorBgColor;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
            notifyUser('DNIS is invalid.  A valid DNIS only contains digits.  (Hint:  check your phone display)',false,true);
        } else {
            validateDNIS();
        }
    } else {
        el.style.backgroundColor = v_errorBgColor;
        el.style.color = v_errorTextColor;
        el.style.fontWeight = 'bold';
        notifyUser('You must specify a DNIS.  (Hint:  check your phone display)',false,true);
    }
}

DNIS_EVENTS.onDnisBlur = function(event) {
    var el = $('callDataDNIS');
    var newValue = UTILS.trim(el.getValue());
    if( newValue.length==0 ) {
        el.style.backgroundColor = v_errorBgColor;
        el.style.color = v_errorTextColor;
        el.style.fontWeight = 'bold';
        notifyUser('You must specify a DNIS.  (Hint:  check your phone display)',false,true);
    } else if( isNaN(newValue) ) {
        el.style.backgroundColor = v_errorBgColor;
        el.style.color = v_errorTextColor;
        el.style.fontWeight = 'bold';
        notifyUser('DNIS is invalid.  A valid DNIS only contains digits.  (Hint:  check your phone display)',false,true);
    }
}

var v_sourceCodeRegEx = /^[A-Z0-9]{1,10}$/;
DNIS_EVENTS.onSourceCodeChange = function(event) {
    v_checkAvailabilityTimer.stopTimer();
    v_verifiedSourceCode = false;
    if( $('sourceCodePwdDiv').visible()==true ) {
        $('sourceCodePwdDiv').style.display='none';
        $('detailDiv').style.display='block';
    }
    
    var value = UTILS.trim($('callDataSourceCode').value).toUpperCase();
    $('callDataSourceCode').value = value;
    
    var fld = findFieldObject('callDataSourceCode',v_currentCartIdx);
    if( fld!=null ) {
//        if( fld.validate(true)==false ) {
//            notifyUser('Source codes can only contain up to six characters or numbers.  Special characters are not allowed.',false,true);
//            fld.setFieldStyle();
//            $('callDataSourceCode').focus();
//            $('callDataSourceCode').select();
//            Event.stop(event);
//            return;
//        }

        if( v_sourceCodeRegEx.test(value)==false ) { 
            notifyUser('Source codes can only contain up to ten characters or numbers.  Special characters are not allowed.',false,true);
            fld.errorFlag=true;
            fld.setFieldStyle();
            $('callDataSourceCode').focus();
            $('callDataSourceCode').select();
            Event.stop(event);
            return;
        }
    }
                    
    if( v_verifiedSourceCode==false ) {
        if( DNIS_EVENTS.checkSourceCodeChange()==true ) {
            changeSourceCode(value,true);
        } else {
            $('callDataSourceCode').value = v_masterSourceCode.id;
            $('callDataSourceCodeDesc').innerHTML = v_masterSourceCode.description;
            v_verifiedSourceCode = true;
            v_checkAvailabilityTimer.startTimer();
        }
        v_hasFocus='callDataSourceCodeSearch';
        waitToSetFocus('callDataSourceCodeSearch');
    } 
}

DNIS_EVENTS.checkSourceCodeChange = function() {
    var doValidation = true;
    for( var idx=0; idx<v_cartList.length; idx++ ) {
        if( v_cartList[idx]==null ) continue;
        if( v_cartList[idx].productObject!=null && 
            v_cartList[idx].productObject.iotwObject!=null &&
            v_cartList[idx].productObject.iotwObject.flag==true &&
            v_cartList[idx].productObject.iotwObject.doesIOTWPricingApply($('productDeliveryDate').value)) {
            if( !confirm('You have at least one \"Item of the Week\" product in the shopping cart.\r\nChanging the source code will cause these products to revert to standard pricing.\r\nDo you want to change the source code?')==true ) {
                $('callDataSourceCode').value=v_masterSourceCode;
                doValidation=false;
            }
            
            break;
        }
    }
    
    return doValidation;
}

DNIS_EVENTS.onSourceCodeLookup = function(event) {
    TEXT_SEARCH.openSourceCodeSearch(false);
}

DNIS_EVENTS.onValidateSourceCodePassword = function(event) {
    if(validateSourceCodePwd()) {
        v_verifiedSourceCode=true;
        
        //Force availability checks
        resetCartTotals();
        for( idx=0; idx<v_cartList.length; idx++ ) {
            if( v_cartList[idx]==null ) continue;
            resetThumbnailDetail(idx);
            v_cartList[idx].productGenerallyAvailable=false;
            ORDER_AJAX.getProductDetail(v_cartList[idx].productId,idx,'ORDER',false,true);
        }
        
        v_checkAvailabilityTimer.startTimer();
    }
}
// ]]>
