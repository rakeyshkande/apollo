// <![CDATA[
var TEXT_SEARCH_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("TEXT_SEARCH_EVENTS requires the Prototype JavaScript framework >= 1.5");
      
if(typeof FTD_DOM=='undefined')
      throw("TEXT_SEARCH_EVENTS requires FTD_DOM");
      

TEXT_SEARCH_EVENTS.onCloseButton = function(event) {
	try {
		TEXT_SEARCH.closeSearch();
	} catch(err) {}
	// finally.  It takes 1 second for the closing animation to complete, so wait 1.1 seconds then make sure the search panel is hidden
	// This code exists to work around a defect where the search panel would get stuck/frozen while closing.
	setTimeout(function() {
		$('rightDivId').show();
		$('textSearchDivId').hide();
		//the next two lines should undo any positioning where the content panel is in the middle of sliding down
		$('rightDivId').down().setStyle({bottom:''});
		$('rightDivId').down().setStyle({position:'static'});
		
	},1100);
	
    waitToSetFocus(v_textSearchTargetElementId);
}

TEXT_SEARCH_EVENTS.onTextSearchGoButton = function(event) {
    TEXT_SEARCH.startSearch('SEARCH');
}

TEXT_SEARCH_EVENTS.onTextSearchStopButton = function(event) {
    if( TEXT_SEARCH.CANCEL_DELAY_TIMER!=null )  {
        TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    }
    TEXT_SEARCH.CURRENT_SEARCH_ID = null;
    $('textSearchGoButton').style.display = 'inline';
    $('textSearchDisabledButton').style.display = 'none';
    $('textSearchStopButton').style.display = 'none';
    $('textSearchProcessing').style.visibility = 'hidden';
}

TEXT_SEARCH_EVENTS.onTextSearchSourceSearchChange = function(event) {
    var searchType = FTD_DOM.getSelectedValue('tssSearchType');
    
    if( searchType=='SOURCE_CODE' ) {
        FTD_DOM.removeAllTableRows($('tssSourceCodeResultsBody'));
        FTD_DOM.removeAllTableRows($('tssIOTWResultsBody'));
        $('tssSourceCodeResultsDiv').style.display = 'block';
        $('tssIOTWResultsDiv').style.display = 'none';
        $('tsIOTWSearchCriteriaDiv').style.display = 'none';
    } else {
        FTD_DOM.removeAllTableRows($('tssSourceCodeResultsBody'));
        FTD_DOM.removeAllTableRows($('tssIOTWResultsBody'));
        $('tssSourceCodeResultsDiv').style.display = 'none';
        $('tssIOTWResultsDiv').style.display = 'block';
        $('tsIOTWSearchCriteriaDiv').style.display = 'block';
    }
    $('tssKeyword').value = '';
}

TEXT_SEARCH_EVENTS.onTextSearchFloristZipButton = function(event) {
    TEXT_SEARCH.openZipSearch('tsflZip',false,true);
}
// ]]>