// <![CDATA[
var FRAMEWORK_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("FRAMEWORK_EVENTS requires the Prototype JavaScript framework >= 1.5");

FRAMEWORK_EVENTS.onNotifyClick = function(event) {
    manuallyCloseNotify();
}
// ]]>