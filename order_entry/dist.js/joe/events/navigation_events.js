// <![CDATA[
var NAV_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || NAV_EVENTS.prototypeVersion < 1.6)
      throw("NAV_EVENTS requires the Prototype JavaScript framework >= 1.6");

NAV_EVENTS.onBodyKeyDown = function(event) {
    var key = window.event ? event.keyCode : event.which;
    
    if( (key==67 || key==88) && event.ctrlKey==1 && /*event.metaKey==0 &&*/ event.shiftKey==0 ) { //ctrl c or x
        //Block ctrl-c or ctrl-x from a password field
        var el = FTD_DOM.whichElement(event);
        if( el.nodeName=='INPUT' && el.type=='password' ) {
            Event.stop(event);
        }
    } else if( key==123 && event.ctrlKey==0 && /*event.metaKey==0 &&*/ event.shiftKey==0 ) { //F12 key
        if( $('notifyDivId').visible() && v_notifyTimer.isRunning() ) {
            v_notifyTimer.stopTimer();
        }
        Effect.toggle($('notifyDivId'),'slide');
        Event.stop(event);
    } else if( key==123 && event.ctrlKey==1 && /*event.metaKey==0 &&*/ event.shiftKey==0 ) { // CTRL+F12 key
        Effect.toggle($('logDivId'),'slide');
        Event.stop(event);
    } else if( event.altKey==1 && event.ctrlKey==0 /*&& event.metaKey==0*/ ) {
        if( key==49 || key==97 ) { //1
            NAV_EVENTS.openProductTab();
            Event.stop(event);
        } else if( key==50 || key==98 ) { //2
            NAV_EVENTS.openCardTab();
            Event.stop(event);
        } else if( key==51 || key==99 ) { //3
            NAV_EVENTS.openRecipientTab();
            Event.stop(event);
        } else if( key==52 || key==100 ) { //4
            NAV_EVENTS.openBillingAccordion();
            Event.stop(event);
        } else if( key==67 ) { //c
            if( $('copyDivId').visible() ) {
                CART_EVENTS.selectCopyOrderButton(null);
            } else {
                new Effect.SlideDown('copyDivId',{afterFinish:CART_EVENTS.selectCopyOrderButton});    
            }
            Event.stop(event);
        } else if( key==82) { //r
            if( v_currentCartIdx > -1 ) {
                CART_EVENTS.removeOrder(v_currentCartIdx);
            }   
            Event.stop(event);
        }
        else if( key==70) { //f
            closePopups();
            if( $('floristOptionsDiv').visible()==true ) { 
                ORDER_EVENTS.doFloristSearch();
            } else {
                waitToSetFocus('productId');
            }
            Event.stop(event);
        }
        else if( key==77) { //m
            closePopups();
            if( $('floristOptionsDiv').visible()==true ) { 
                waitToSetFocus('floristComments');
            } else {
                waitToSetFocus('productId');
            }
            Event.stop(event);
        }
        else if( key==66) { //b
            closePopups();
            Event.stop(event);
        }
        else if( key==71) { //g
            closePopups();
            if( $('addOnCardsTable').style.display=='inline' ) { 
                waitToSetFocus('addOnCardCheckbox');
            } else {
                waitToSetFocus('cardMessage');
            }
            Event.stop(event);
        }
        else if( key==79) { //o
            closePopups();
            waitToSetFocus('orderComments');
            Event.stop(event);
        }
        else if( key==85) { //u
            closePopups();
            if( v_mainAccordion.selected.id=="upperContent" ) {
                //waitToSetFocus('billingButtonSubmit');
                BILLING_EVENTS.onSubmitButton();
            }
//            if( $('billingButtonSubmit').visible()==false )
//                BILLING_EVENTS.onSubmitButton();
//            else
//                waitToSetFocus('billingButtonSubmit');
            //BILLING_EVENTS.onSubmitButton();
            Event.stop(event);
        }
        else if( key==65) { //a
            closePopups();
            waitToSetFocus('actionButtonAbandon');
            //ACTION_EVENTS.onActionButtonAbandon();
            Event.stop(event);
        }
        else if( key==69) { //e
            if( $('detailDiv').visible()==true && $('productSearchDivId').visible()==false ) {
                if( $('textSearchDivId').visible()==true ) {
                    TEXT_SEARCH.closeSearch();
                }
                
                if( $('spellCheckDivId').visible()==true ) {
                    closeSpellCheck();
                }
                
                if( $('addressVerificationDivId').visible()==true ) {
                    ORDER_EVENTS.onRejectAVChanges(null);
                }
                
                PRODUCT_SEARCH.openSearch();
            }
            Event.stop(event);
        }
        else if( key==83 ) { //s 
            if( $('productSearchDivId').visible()==true ) {
                PRODUCT_SEARCH.closeSearch();
            } else if( $('textSearchDivId').visible()==true ) {
                TEXT_SEARCH.closeSearch();
            } else if( $('spellCheckDivId').visible()==true ) {
                closeSpellCheck();
            }
            
            var fld = findFieldObjectByAccessKey('s',v_currentCartIdx);
            
            if( fld!=null ) {
                waitToSetFocus(fld.elementId);
            }
        }
        else if( key==76) { //l
            if( $('textSearchDivId').visible()==true ) {
                if( v_textSearchType=='source' ) {
                    $('tssSearchType').focus();
                    Event.stop(event);
                    return false;
                } else {
                    TEXT_SEARCH.closeSearch();
                }
            } else if( $('productSearchDivId').visible()==true ) {
                PRODUCT_SEARCH.closeSearch();
            } else if( $('spellCheckDivId').visible()==true ) {
                closeSpellCheck();
            }
                
            TEXT_SEARCH.openSourceCodeSearch(false);
            Event.stop(event);
        }
    }
}
 
NAV_EVENTS.openBillingAccordion = function() {
    closePopups();
    waitToSetFocus('customerBillingPhone');
}
 
NAV_EVENTS.openProductTab = function() {
    closePopups();
    waitToSetFocus('productId');    
}
 
NAV_EVENTS.openCardTab = function() {
    closePopups();
    waitToSetFocus('cardTypeList');
}
 
NAV_EVENTS.openRecipientTab = function() {
    closePopups();
    waitToSetFocus('recipientPhone');
}

NAV_EVENTS.tabSelected = function(args) {
    if( args.index==0 ) {
        NAV_EVENTS.openProductTab();
    } else if( args.index==1 ) {
        NAV_EVENTS.openCardTab();
    } else if( args.index==2 ) {
        NAV_EVENTS.openRecipientTab();
    }
    return true;
}

NAV_EVENTS.onOrderBarClicked = function(event) {
    //Find the currently displayed tab
    var currentTabIdx = -1;
    for( var idx=0; idx<v_tabControl.tabs.length; idx++ ) {
        if( v_tabControl.tabs[idx].li.className=='tabberactive' ) {
            currentTabIdx = idx;
            break;
        }
    }
    
    switch( currentTabIdx ) {
        case 0:
          waitToSetFocus('productId');
          break;
        case 1:
          waitToSetFocus('cardTypeList');
          break;
        case 2:
          waitToSetFocus('recipientPhone');
          break;
    }
}

NAV_EVENTS.onBillingBarClicked = function(event) {
	if (v_modifyOrderFromCom) {
		// If we're doing a modify order, the billing phone is not editable, so set focus to editable payment field
	    waitToSetFocus('paymentTypeComboLabel');		
	} else {
	    waitToSetFocus('customerBillingPhone');
	}
}

NAV_EVENTS.accordionSelect = function(currentTab) {
    if( currentTab.titleBar.id=="upperHeader" ) {
        //Find the currently displayed tab
        var currentTabIdx = -1;
        for( var idx=0; idx<v_tabControl.tabs.length; idx++ ) {
            if( v_tabControl.tabs[idx].li.className=='tabberactive' ) {
                currentTabIdx = idx;
                break;
            }
        }
        
        switch( currentTabIdx ) {
            case 0:
              waitToSetFocus('productId');
              break;
            case 1:
              waitToSetFocus('cardTypeList');
              break;
            case 2:
              waitToSetFocus('recipientPhone');
              break;
        }
    } else if( currentTab.titleBar.id=="lowerHeader" ) {
        waitToSetFocus('customerBillingPhone');
    }
}

NAV_EVENTS.onNavigationFocus = function(event) {
    var el = FTD_DOM.whichElement(event);
    
    if( el.id=='pagetop' ) {
        NAV_EVENTS.openBillingAccordion();
    } else if( el.id=='tab1bottom' ) {
        NAV_EVENTS.openCardTab();
    }else if( el.id=='tab2top' ) {
        NAV_EVENTS.openProductTab();
    }else if( el.id=='tab2bottom' ) {
        NAV_EVENTS.openRecipientTab();
    }else if( el.id=='tab3top' ) {
        NAV_EVENTS.openCardTab();
    }else if( el.id=='tab3bottom' ) {
        NAV_EVENTS.openBillingAccordion();
    }else if( el.id=='billingtop' ) {
        NAV_EVENTS.openRecipientTab();
    }else if( el.id=='billingbottom' ) {
        NAV_EVENTS.openProductTab();
    }
}
 
// ]]>