// <![CDATA[
var CUSTOMER_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("CUSTOMER_EVENTS requires the Prototype JavaScript framework >= 1.5");


CUSTOMER_EVENTS.onCustomerZipSearchButton = function(event) {
    TEXT_SEARCH.openZipSearch('customerZip',false,true);
}

CUSTOMER_EVENTS.onCustomerCountryChange = function(event) {
    setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
}

CUSTOMER_EVENTS.onCustomerZipChange = function(event) {
    var countryId = null;
    var testZip = UTILS.trim($('customerZip').value).toUpperCase();
    $('customerZip').value=testZip;
    if( ORDER_EVENTS.REGEX_US_ZIP.test(testZip) ) {
        countryId = 'US';
    } else if( ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip) ) {
        countryId = 'CA';
    } else if( testZip!=null && testZip.length==3 && ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip.substr(0,3)) ) {
        countryId = 'CA';
    } else {
        countryId = FTD_DOM.getSelectedValue($('customerCountryCombo'));
    }
    
    if( countryId!=FTD_DOM.getSelectedValue($('customerCountryCombo')) ) {
        FTD_DOM.selectOptionByValue($('customerCountryCombo'),countryId);
    }
    
    var fld = findFieldObject('customerZip',v_currentCartIdx);
    if( fld!=null ) {
        var flag = fld.validate(false);
        fld.setFieldStyle();
        if( fld.errorFlag==true ) {
            if( countryId=='US' ) {
                notifyUser('Zip codes for the US must be in the format of \'99999\'',false,true);
            } else if( countryId=='CA' ) {
                notifyUser('Canadian postal codes must be in the format of \'A9A\' or \A9A9A9\'',false,true);    
            } else {
                notifyUser('Zip codes can only contain letters six letters or numbers',false,true);    
            }
            Event.stop(event);
            return false;
        } 
    }
    
    if( countryId=='US' ) {
        VALIDATE_AJAX.validateZipElement('customerZip');
    } else {
        CUSTOMER_EVENTS.changeCustomerZip($('customerZip').value,null,countryId);
    }
}

CUSTOMER_EVENTS.changeCustomerZip = function(zipCode,stateId,countryId) {
    zipCode = UTILS.trim(zipCode);
    
    if( zipCode!=null && zipCode.length>0 ) {
        $('customerZip').value = zipCode;
    }
    
    if( countryId!=null && countryId.length>0 ) {
        FTD_DOM.selectOptionByValue($('customerCountryCombo'),countryId);
        setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
    }
    
    if( stateId!=null && stateId.length>0 ) {
        FTD_DOM.selectOptionByValue($('customerStateCombo'),stateId);
    } else {
        $('customerStateCombo').selectedIndex=0;
    }
}

CUSTOMER_EVENTS.onCustomerPhoneChange = function(event) {
    var phoneNumber = UTILS.stripNonDigits($('customerBillingPhone').getValue());
    $('customerBillingPhone').value = phoneNumber;
    
    if( v_customerPhoneChecked==false ) {
        if( !JOE_VALIDATE.skipPhoneValidation(phoneNumber) ) {
            $('customerBillingPhone').setValue(phoneNumber);
            v_textSearchType = 'phone';
            v_textSearchTargetElementId = 'customerBillingPhone';
            TEXT_SEARCH.startSearch('VALIDATE');
            v_customerPhoneChecked = true;
            
            $('customerPhoneSearch').style.visibility = 'visible';
            //On ie6, the image gets lost with visibility set to hidden
            //so, need to recreate it
            var img = $('customerPhoneSearchImage');
            if( img ) {
                img.style.visibility='visible';
            } else {
                img = new FTD_DOM.DOMElement('img', {
                    id:'customerPhoneSearchImage',
                    alt:'Open zip search',
                    src:'images/magnify.jpg'
                }).createNode();
                $('customerPhoneSearch').appendChild(img);
            }
        }   
    }
}

CUSTOMER_EVENTS.onCustomerPhoneExtChange = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null ) {
        var value = UTILS.stripNonDigits(el.getValue());
        el.value = value;
    }
}

CUSTOMER_EVENTS.onCustomerPhoneSearchButton = function(event) {
    TEXT_SEARCH.openPhoneSearch('customerBillingPhone',false);
}

CUSTOMER_EVENTS.onCustomerFirstNameChange = function(event) {
    var newFirstName = $('customerFirstName').getValue();
    if( v_masterSourceCode.membershipDataRequired==true && UTILS.isEmpty(newFirstName)==false ) {
        var membershipId = $('membershipId').getValue();
        var membershipFirstName = $('membershipFirstName').getValue();
        if( UTILS.isEmpty(membershipFirstName)==true ) {
            $('membershipFirstName').value=newFirstName;
        }
    }
}

CUSTOMER_EVENTS.onCustomerLastNameChange = function(event) {
    var newLastName = $('customerLastName').getValue();
    if( v_masterSourceCode.membershipDataRequired==true && UTILS.isEmpty(newLastName)==false ) {
        var membershipId = $('membershipId').getValue();
        var membershipLastName = $('membershipLastName').getValue();
        if( UTILS.isEmpty(membershipLastName)==true ) {
            $('membershipLastName').value=newLastName;
        }
    }
}

CUSTOMER_EVENTS.onCustomerOptInChanged = function(event) {
    var fld = findFieldObject('customerEmailAddress', v_currentCartIdx);
    if( fld!=null ) fld.isRequired();
}

CUSTOMER_EVENTS.onCustomerEmailAddressChanged = function(event) {
  CUSTOMER_EVENTS.customerEmailAddressChanged(); 
}


CUSTOMER_EVENTS.customerEmailAddressChanged = function(event) {
    // Process a change in email address
    v_newCustomerEmailAddress = $('customerEmailAddress').getValue();    
    try {
      if( shouldRecalculateCart()==true ) {
    	ORDER_AJAX.calculateOrderTotals(false);
      } 
    } catch (err) {
      v_recalcOrderTimer.startTimer();
    }     
}

// ]]>