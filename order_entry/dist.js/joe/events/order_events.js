// <![CDATA[
var ORDER_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || ORDER_EVENTS.prototypeVersion < 1.6)
      throw("ORDER_EVENTS requires the Prototype JavaScript framework >= 1.6");

ORDER_EVENTS.REGEX_US_ZIP = /^\d{5}$/;
ORDER_EVENTS.REGEX_CA_POSTAL_CODE = /^([A-Za-z]\d[A-Za-z][-]?\d[A-Za-z]\d)/;
ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT = /^([A-Za-z]\d[A-Za-z])/;
//ORDER_EVENTS.REGEX_CA_POSTAL_CODE = /^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$/;
//Accurate Canadian postal code format verification. The format of a Canadian postal code is LDL DLD where L 
//are alpha characters and D are numeric digits. But there are some exceptions. The letters D, F, I, O, Q and U 
//never appear in a postal code because of their visual similarity to 0, E, 1, 0, 0, and V respectively. In 
//addition to avoiding the six &quot;forbidden&quot; letters W and Z also do not appear as the first letter of 
//a postal code (at least not at present).

ORDER_EVENTS.onProductSearch = function(event) {
    PRODUCT_SEARCH.openSearch();
}

ORDER_EVENTS.onProductDetailClose = function(event) {
    new Effect.SlideUp('psDetailDivId');
    new Effect.SlideDown('rightDivId');
}

//added Sympathy Controls

ORDER_EVENTS.onDeliveryLocationChange = function(event){
	var productId = $('productId').getValue();
	var productZip = $('productZip').getValue();
	var productDeliveryDate = $('productDeliveryDate').getValue(); 
	if(productId.length > 0 && productZip.length > 0 && productDeliveryDate.length > 0){
		ORDER_AJAX.getSympathyLocationCheck('N');
	}
}

ORDER_EVENTS.onServiceTimeChange = function(event){
	var hoursStart = $('recipientHoursFrom').getValue();
	var hoursEnd = $('recipientHoursTo').getValue();
	var productId = $('productId').getValue();
	var productZip = $('productZip').getValue();
	var productDeliveryDate = $('productDeliveryDate').getValue(); 
	
	$('timeOfService').value = hoursStart;
	$('timeOfServiceEnd').value = hoursEnd;
	
		
	if(hoursStart.length > 0 && hoursEnd.length > 0 && productId.length > 0 && productZip.length > 0 && productDeliveryDate.length > 0){
		ORDER_AJAX.getSympathyLocationCheck('Y');
	}
}

ORDER_EVENTS.onZipSearch = function(event) {
    TEXT_SEARCH.openZipSearch('productZip',false,true);
}

//Used to keep track of when to display drop ship warning
var v_lastLocationType='none'; 
ORDER_EVENTS.onRecipLocationTypeFocus = function(event) {
    v_lastLocationType = FTD_DOM.getSelectedValue('recipientType');
}

ORDER_EVENTS.onRecipLocationTypeChanged = function(event) {
	
	var selectedValue = FTD_DOM.getSelectedValue('recipientType');
    //$('orderComments').value='Last value: '+v_lastLocationType+'\tNew value: '+selectedValue;
    if( v_currentCartIdx>-1 &&                  //not on startup
        v_lastLocationType!=selectedValue )     //new value is different the last value
    {
        v_cartList[v_currentCartIdx].productObject.dropShipWarned=false;
    }
    
    var selectedObject = v_locationTypes[selectedValue];
    if( v_currentCartIdx>-1 )
        v_cartList[v_currentCartIdx].recipientType = selectedValue;
    
    if( selectedObject!=null ) {
         
        if( selectedObject.business==true ) {
           $('recipientBusinessLabel').innerHTML = selectedObject.businessLabel;
           $('recipientBusinessLabel').style.display = 'inline';
           $('recipientBusiness').style.display = 'inline';
           if( v_currentCartIdx>-1 )
              v_cartList[v_currentCartIdx].isRequired('recipientBusiness');
        } else {
           $('recipientBusinessLabel').style.display = 'none';
           $('recipientBusiness').style.display = 'none';
        }
         
        if( selectedObject.lookup==true ) {
           $('recipientFacilitySearch').style.display = 'inline';
           $('recipientFacilitySearch').title = selectedObject.lookupLabel;
        } else {
           $('recipientFacilitySearch').style.display = 'none';
        }
         
        if( selectedObject.room==true && (v_modifyOrderFromCom == false || v_hasSpecialInstructions == false) ) {
           $('recipientRoomLabel').innerHTML = selectedObject.roomLabel;
           $('recipientRoomLabel').style.display = 'inline';
           $('recipientRoom').style.display = 'inline';
        } else {
           $('recipientRoomLabel').style.display = 'none';
           $('recipientRoom').style.display = 'none';
        }
         
        if( selectedObject.hours==true && (v_modifyOrderFromCom == false || v_hasSpecialInstructions == false) ) {
           $('recipientHoursId').style.visibility = 'visible';
           $('recipientHoursFromLabel').style.visibility = 'visible';
           $('recipientHoursFromLabel').innerHTML = selectedObject.hoursLabel;
           $('recipientHoursToLabel').style.visibility = 'visible';
        } else {
           $('recipientHoursFromLabel').style.visibility = 'hidden';
           $('recipientHoursToLabel').style.visibility = 'hidden';
           $('recipientHoursId').style.visibility = 'hidden';
        }
         
        v_lastLocationType = selectedValue;
        JOE_ORDER.checkForRestrictedVendorDelivery(v_currentCartIdx,selectedValue);
    
    }
}
ORDER_EVENTS.onProductCountryChange = function(event) {
    var selectedValue = $('productCountry').getValue();
    setCountryFields('productCountry',null,'productZip','productZipSearch',null);
    
    FTD_DOM.selectOptionByValue($('recipientCountry'),selectedValue);
    setCountryFields('recipientCountry','recipientState','recipientZip','recipientZipSearch',null);
    
    FTD_DOM.selectOptionByValue($('productSearchCountryCombo'),selectedValue);
    PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
        
    //If the product details have not been retrieve, then try and get them now
    if( event!=null && v_currentCartIdx>-1 ) {
        var orderObj = v_cartList[v_currentCartIdx];
        var currentProductId = $('productId').value;
        if( UTILS.isEmpty(currentProductId)==false /*&& 
            orderObj!=null && 
            orderObj.productObject!=null && 
            UTILS.isEmpty(orderObj.productObject.name)==true*/  ) 
        {
            ORDER_AJAX.getProductDetail(currentProductId,v_currentCartIdx,'ORDER',true,false);
        }
    }
    ORDER_EVENTS.onProductIdOrZipCodeChange();
}

ORDER_EVENTS.onRecipientCountryChange = function(event) {
    var selectedValue = $('recipientCountry').getValue();
    setCountryFields('recipientCountry','recipientState','recipientZip','recipientZipSearch',null);
    
    FTD_DOM.selectOptionByValue($('productCountry'),selectedValue);
    setCountryFields('productCountry',null,'productZip','productZipSearch',null);
    
    FTD_DOM.selectOptionByValue($('productSearchCountryCombo'),selectedValue);
    PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
        
    //If the product details have not been retrieve, then try and get them now
    if( event!=null && v_currentCartIdx>-1 ) {
        var orderObj = v_cartList[v_currentCartIdx];
        var currentProductId = $('productId').value;
        ORDER_EVENTS.onProductIdOrZipCodeChange();
        if( UTILS.isEmpty(currentProductId)==false ) {
            ORDER_AJAX.getProductDetail(currentProductId,v_currentCartIdx,'ORDER',true,false);
        }
    }
}

ORDER_EVENTS.onLanguageIdChange = function(event) {
	var selectedValue = $('languageId').getValue();
	FTD_DOM.selectOptionByValue($('languageId'),selectedValue);
}

ORDER_EVENTS.onCardTypeButton = function(event) {
    var oldMsg = $('cardMessage').value;
    var cardType = FTD_DOM.getSelectedText($('cardTypeList'));
    $('cardMessage').value = oldMsg+cardType;
}

ORDER_EVENTS.onCardMessageChange = function(event) {
    spellCheck('cardMessage',false);
}

ORDER_EVENTS.onCardSpellCheckButton = function(event) {
    spellCheck('cardMessage',true);
}

ORDER_EVENTS.onCardAddonCheckbox = function(event) {
    displayAddOnCards();
}

ORDER_EVENTS.onCardAddonCheckboxBlur = function(event) {
    if( $('addOnCardCheckbox').checked==false ) {
        var fld = findFieldObject('addOnCardCheckbox',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnCardCheckbox').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onFloristIdChange = function(event) {
    $('floristDesc').innerHTML = '';
    var strVal = UTILS.trim($('floristId').value).toUpperCase();
    var el = $('floristId');
    el.value = strVal;
    
    var fld = v_cartList[v_currentCartIdx].orderFields['floristId'];
    if( fld!=null ) {
        var regex = fld.validateExpression;
        fld.errorFlag=!((new RegExp(regex)).test(strVal));
        if( fld.errorFlag==true ) {
            notifyUser('The florist id invalid.  It must be in the format of \"NN-NNNNAA\"',false,true);
            fld.setFieldStyle();
            Event.stop(event);
            return false;
        }
        fld.setFieldStyle();
    }
    
    if( UTILS.isEmpty(strVal)==false ) {
        VALIDATE_AJAX.validateFlorist();
    } else {
        el.style.backgroundColor = v_normalBgColor;
        el.style.color = v_normalTextColor;
        el.style.fontWeight = 'normal';
    }
}

ORDER_EVENTS.onFloristSearchButton = function(event) {
    ORDER_EVENTS.doFloristSearch();
}

ORDER_EVENTS.doFloristSearch = function() {
    var productId = $('productId').value;
    var productZip = $('productZip').value;
    var deliveryDate = FTD_DOM.getSelectedValue('productDeliveryDate');
    if( UTILS.isEmpty(productId)==false &&
        UTILS.isEmpty(productZip)==false &&
        UTILS.isEmpty(deliveryDate)==false ) {
        TEXT_SEARCH.openFloristSearch(false);
    } else {
        alert('You must enter a product, delivery date, and zip code before searching for a florist.');
    }
}

ORDER_EVENTS.onRecipPhoneSearchButton = function(event) {
    TEXT_SEARCH.openPhoneSearch('recipientPhone',false);
}

ORDER_EVENTS.onRecipientPhoneChange = function(event) {
    var phoneNumber = UTILS.stripNonDigits($('recipientPhone').getValue());
    var selectedValue = FTD_DOM.getSelectedValue('recipientType');

    $('recipientPhone').value = phoneNumber;
    
    if( $('recipientPhoneChecked').checked == false ) {
        if(JOE_VALIDATE.skipAutoPhoneSearch(selectedValue) ) 
        {
          $('recipientPhoneChecked').checked = true;
          $('recipientPhoneSearch').style.visibility = 'visible';

          //On ie6, the image gets lost with visibility set to hidden, so, need to recreate it
          var img = $('recipientPhoneSearchImage');
          if( img ) 
          {
            img.style.visibility='visible';
          } 
          else 
          {
            img = new FTD_DOM.DOMElement('img', {id:'recipientPhoneSearchImage',alt:'Open zip search',src:'images/magnify.jpg'}).createNode();
            $('recipientPhoneSearch').appendChild(img);
          }
        }   
        else
        {
          if( !JOE_VALIDATE.skipPhoneValidation(phoneNumber) ) {
              $('recipientPhone').setValue(phoneNumber);
              v_textSearchType = 'phone';
              v_textSearchTargetElementId = 'recipientPhone';
              TEXT_SEARCH.startSearch('VALIDATE');
              $('recipientPhoneChecked').checked = true;

              $('recipientPhoneSearch').style.visibility = 'visible';
              //On ie6, the image gets lost with visibility set to hidden
              //so, need to recreate it
              var img = $('recipientPhoneSearchImage');
              if( img ) {
                  img.style.visibility='visible';
              } else {
                  img = new FTD_DOM.DOMElement('img', {
                      id:'recipientPhoneSearchImage',
                      alt:'Open zip search',
                      src:'images/magnify.jpg'
                  }).createNode();
                  $('recipientPhoneSearch').appendChild(img);
              }
          }   
        }   
    }
}

ORDER_EVENTS.onRecipientPhoneExtChange = function(event) {
    var phoneNumber = UTILS.stripNonDigits($('recipientPhoneExt').getValue());
    $('recipientPhoneExt').value = phoneNumber;
}

ORDER_EVENTS.onRecipZipSearchButton = function(event) {
    FTD_DOM.removeAllTableRows($('tszResultsBody'));
    TEXT_SEARCH.openZipSearch('recipientZip',false,true);
}

ORDER_EVENTS.onMembershipIdSearchButton = function(event) {
    FTD_DOM.removeAllTableRows($('tsmResultsBody'));
    TEXT_SEARCH.openMembershipIdSearch('customerZip',true);
}

ORDER_EVENTS.onProductZipChange = function(event) {
    
    var testZip = UTILS.trim($('productZip').value).toUpperCase();
    $('productZip').value = testZip;
    if( ORDER_EVENTS.REGEX_US_ZIP.test(testZip) ) {
        countryId = 'US';
    } else if( ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip) ) {
        countryId = 'CA';
    } else if( testZip!=null && testZip.length==3 && ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip.substr(0,3)) ) {
        countryId = 'CA';
    } else {
        countryId = FTD_DOM.getSelectedValue($('productCountry'));
    }
    
    v_cartList[v_currentCartIdx].productZip=testZip;
    if( countryId!=FTD_DOM.getSelectedValue($('productCountry')) ) {
        FTD_DOM.selectOptionByValue($('productCountry'),countryId);
        v_cartList[v_currentCartIdx].productCountry=countryId;
    }
    
    var fld = v_cartList[v_currentCartIdx].orderFields['productZip'];
    if( fld!=null ) {
        var flag = v_cartList[v_currentCartIdx].validateField('productZip',false);
        fld.setFieldStyle();
        if( fld.errorFlag==true ) {
            if( countryId=='US' ) {
                notifyUser('Zip codes for the US must be in the format of \'99999\'',false,true);
            } else if( countryId=='CA' ) {
                notifyUser('Canadian postal codes must be in the format of \'A9A\' or \A9A9A9\'',false,true);    
            } else {
                notifyUser('Zip codes can only contain letters six letters or numbers',false,true);    
            }
            Event.stop(event);
            return false;
        }
    }
    
    if( countryId=='US' || countryId=='CA' ) {
        VALIDATE_AJAX.validateZipElement('productZip');
    } else {
        ORDER_EVENTS.changeProductZip($('productZip').value,countryId);
    }
    
    if( $('recipientZip').value != $('productZip').value ) {
        ORDER_EVENTS.changeRecipientZip($('productZip').value,null,countryId);
        fld = v_cartList[v_currentCartIdx].orderFields['recipientZip'];
        if( fld!=null ) {
            fld.errorFlag = false;
            fld.setFieldStyle();
        }
    }
}

ORDER_EVENTS.changeProductZip = function(zipCode,countryId) {
    zipCode = UTILS.trim(zipCode);
    if( zipCode!=null && zipCode.length>0 ) {
        $('productZip').value = zipCode;
        
        v_cartList[v_currentCartIdx].productZip = zipCode;
        
        if($('productSearchZip').value != $('productZip').value) {
            $('productSearchZip').value = $('productZip').value;
        }
    }
    
    if( countryId!=null && countryId.length>0 ) {
        FTD_DOM.selectOptionByValue($('productCountry'),countryId);
        v_cartList[v_currentCartIdx].productCountry = countryId;
    }
}

ORDER_EVENTS.changeProductCountry = function(countryId) {
    if( countryId!=null && countryId.length>0 ) {
        FTD_DOM.selectOptionByValue($('productCountry'),countryId);
        v_cartList[v_currentCartIdx].productCountry = countryId;

        var selectedValue = $('productCountry').getValue();
        setCountryFields('productCountry',null,'productZip','productZipSearch',null);

        FTD_DOM.selectOptionByValue($('recipientCountry'),selectedValue);
        setCountryFields('recipientCountry','recipientState','recipientZip','recipientZipSearch',null);

        FTD_DOM.selectOptionByValue($('productSearchCountryCombo'),selectedValue);
        PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
        
        //If the product details have not been retrieve, then try and get them now
        if( v_currentCartIdx>-1 ) {
            var orderObj = v_cartList[v_currentCartIdx];
            var currentProductId = $('productId').value;
            if( UTILS.isEmpty(currentProductId)==false /*&& 
                orderObj!=null && 
                orderObj.productObject!=null && 
                UTILS.isEmpty(orderObj.productObject.name)==true*/  ) 
            {
                ORDER_AJAX.getProductDetail(currentProductId,v_currentCartIdx,'ORDER',true,false);
            }
        }
    }
}

ORDER_EVENTS.onRecipientZipChange = function(event) {
    var countryId;
    var testZip = UTILS.trim($('recipientZip').value).toUpperCase();
    $('recipientZip').value=testZip;
    if( ORDER_EVENTS.REGEX_US_ZIP.test(testZip) ) {
        countryId = 'US';
    } else if( ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip) ) {
        countryId = 'CA';
    } else if( testZip!=null && testZip.length==3 && ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip.substr(0,3)) ) {
        countryId = 'CA';
    } else {
        countryId = FTD_DOM.getSelectedValue($('recipientCountry'));
    }
    
    v_cartList[v_currentCartIdx].recipientZip=testZip;
    if( countryId!=FTD_DOM.getSelectedValue($('recipientCountry')) ) {
        FTD_DOM.selectOptionByValue($('recipientCountry'),countryId);
        v_cartList[v_currentCartIdx].recipientCountry=countryId;
    }
    
    var fld = v_cartList[v_currentCartIdx].orderFields['recipientZip'];
    if( fld!=null ) {
        var flag = v_cartList[v_currentCartIdx].validateField('recipientZip',false);
        fld.setFieldStyle();
        if( fld.errorFlag==true ) {
            if( countryId=='US' ) {
                notifyUser('Zip codes for the US must be in the format of \'99999\'',false,true);
            } else if( countryId=='CA' ) {
                notifyUser('Canadian postal codes must be in the format of \'A9A\' or \A9A9A9\'',false,true);    
            } else {
                notifyUser('Zip codes can only contain letters six letters or numbers',false,true);    
            }
            Event.stop(event);
            return false;
        }
    }
    
    if( countryId=='US' || countryId=='CA' ) {
        VALIDATE_AJAX.validateZipElement('recipientZip');
    } else {
        ORDER_EVENTS.changeRecipientZip($('recipientZip').value,null,countryId);
    }
    
    if( $('productZip').value != $('recipientZip').value ) {
        ORDER_EVENTS.changeProductZip($('recipientZip').value,countryId);
        fld = v_cartList[v_currentCartIdx].orderFields['productZip'];
        if( fld!=null ) {
            fld.errorFlag = false;
            fld.setFieldStyle();
        }
        ORDER_EVENTS.onProductIdOrZipCodeChange();
    }
}

ORDER_EVENTS.changeRecipientZip = function(zipCode,stateId,countryId) {
    zipCode = UTILS.trim(zipCode);
    if( zipCode!=null && zipCode.length>0 ) {
        $('recipientZip').value = zipCode;
        
        v_cartList[v_currentCartIdx].recipientZip = zipCode;
    
        if($('productSearchZip').value != $('recipientZip').value) {
            $('productSearchZip').value = $('recipientZip').value;
        }
    }
    
    if( countryId!=null && countryId.length>0 ) {
        FTD_DOM.selectOptionByValue($('recipientCountry'),countryId);
        setCountryFields('recipientCountry','recipientState','recipientZip','recipientZipSearch',null);
        v_cartList[v_currentCartIdx].recipientCountry = countryId;        
    }
    
    if( stateId!=null && stateId.length>0 ) {
        FTD_DOM.selectOptionByValue($('recipientState'),stateId);
    } else {
        $('recipientState').selectedIndex=0;
    }
    v_cartList[v_currentCartIdx].recipientState = FTD_DOM.getSelectedValue($('recipientState'));
    v_cartList[v_currentCartIdx].recipientCity = $('recipientCity').value;
    
    toggleSurchargeExplanation();
}

ORDER_EVENTS.toUpperCase = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el.value.length>0 ) {
        el.value = el.value.toUpperCase();
    }
}

ORDER_EVENTS.onProductIdChanged = function(event) {
    var value = UTILS.trim($('productId').value).toUpperCase();
    $('productId').value = value;
    var fld = findFieldObject('productId',v_currentCartIdx);
    if( fld!=null ) {
        if( fld.validate(false)==false ) {
            notifyUser('Product Ids can only contain 1 to 10 digits, letters, or dashes.  All other values are invalid',false,true);
            fld.setFieldStyle();
            $('productId').focus();
            $('productId').select();
            Event.stop(event);
            return false;
        }
        
        fld.setFieldStyle();
    }
    
    productIdChanged(true);
}

ORDER_EVENTS.startSaleProcess = function() {
    if( !v_callTimer.isRunning() ) {
        //DNIS is locked
        $('callDataDNIS').style.borderColor = $('productZip').style.borderColor;
        $('callDataDNISLabel').style.color = $('productZipLabel').style.color;
        $('callDataDNIS').disabled = true;
        
        //Disable the exit button
        $('actionButtonExit').style.display = 'none';
        
        //Enable the abort cart button
        $('actionButtonAbandon').disabled = false;
        
        //Start the timers
        v_callTimer.startTimer(); 
        v_recalcOrderTimer.startTimer();
        v_checkAvailabilityTimer.startTimer();
        v_checkAddressTimer.startTimer();
    }
}

ORDER_EVENTS.onOrderObjectChange = function(event) {
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    return true;
}

ORDER_EVENTS.setOrderValue = function(el,newValue) {
    if( el!=null && newValue!=null ) {
        if( typeof el == 'string' ) {
            el = $(el);
            if( el==null ) return;
        }
        el.setValue(newValue);
        ORDER_EVENTS.setOrderObjectValue(el);
    }
}

ORDER_EVENTS.setOrderObjectValue = function(el) {
    var orderObj = v_cartList[v_currentCartIdx];
    
    if( el.nodeName.toUpperCase()=='INPUT' ) {
        
        if( el.type.toUpperCase()=='TEXT' || el.type.toUpperCase()=='HIDDEN' ) {
            orderObj[el.id]=el.value;
        } else if( el.type.toUpperCase()=='CHECKBOX' ) {
            //Is this a checkbox group
            if( UTILS.isEmpty(el.name) ) {
                orderObj[el.id]=el.checked;
            } else {
                var checkboxes = document.getElementsByTagName(el.name);
                if( checkboxes.length==1 ) {
                    orderObj[el.id]=el.checked;
                } else {
                    el = FTD_DOM.getSelectedRadioButton(el.name);
                    if( el!=null ) {
                        orderObj[el.name] = el.id;
                    }
                }
            }
        } else if( el.type.toUpperCase()=='RADIO' ) {
            el = FTD_DOM.getSelectedRadioButton(el.name);
            if( el!=null ) {
                orderObj[el.name]=el.id;
            } 
        }
    } else if( el.nodeName.toUpperCase()=='TEXTAREA' ) {
        orderObj[el.id]=el.value;
    } else if( el.nodeName.toUpperCase()=='SELECT' ) {
        if( el.type.toUpperCase()=='SELECT-ONE' ) {
            var selectedIndex = el.selectedIndex;
            if( selectedIndex==-1 )
                orderObj[el.id]=null;
            else
                orderObj[el.id]=el[selectedIndex].value;
        }
    } else if( el.nodeName.toUpperCase()=='DIV' || el.nodeName.toUpperCase()=='SPAN' ) {
        orderObj[el.id]=el.innerHTML;
    } else {
        alert('nodeName: '+el.nodeName);
    }
    
    if( el!=null ) {
        //Does the change cause the order to recalculate?
        if( inFieldArray(v_recalcFields, el.id ) ) {
            resetThumbnailDetail(v_currentCartIdx);
            resetCartTotals();
        }
        if (el.id=='productId' || el.id=='productZip') {
            ORDER_EVENTS.onProductIdOrZipCodeChange();
        }
    }
    
    orderObj.populateThumbNail();
    
    if( el!=null ) { 
        //Check for QMS
        if( inFieldArray(v_avsFields, el.id ) ) {
            $('avsPerformed').checked=false;
            orderObj.avsPerformed=false;
        }
    }
}

ORDER_EVENTS.onRecipientBusinessSearch = function(event) {
    TEXT_SEARCH.openFacilitySearch(false);
}

ORDER_EVENTS.onProductOccasionChanged = function(event) {
    var orderObj = v_cartList[v_currentCartIdx];
    orderObj.productOccasion = $('productOccasion').getValue();
    orderObj.configureAddons();
}

ORDER_EVENTS.onAddOnCardChanged = function(event) {
    var orderObj = v_cartList[v_currentCartIdx];
    orderObj.addOnCardSelect = $('addOnCardSelect').getValue();
    //get addon object
    var addon = findAddonById(orderObj.addOnCardSelect);
    if(addon!=null) {
        //populate the card elements
        $('addOnCardImage').src=addon.image;
        $('addOnCardPrice').innerHTML = '$'+addon.price;
        $('addOnCardFrontText').innerHTML = addon.description;
        $('addOnCardInsideText').innerHTML = addon.insideText;
        displayAddOnCards();
    } else {
        $('addOnCardImage').style.display = 'none';  
        $('addOnCardDetailRow').style.display = 'none';
    }
}

ORDER_EVENTS.onAddOnVaseChanged = function(event) {    
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].productAddonVase = $('productAddonVase').getValue();
}

ORDER_EVENTS.onProductUpsellChange = function(event) {
    $('productId').setValue($('productOptionUpsell').getValue());
    productIdChanged(true);
    ORDER_EVENTS.onProductIdOrZipCodeChange();
}

ORDER_EVENTS.onProductSubcodeChange = function(event) {
    $('productId').setValue($('productOptionSubCode').getValue());
    productIdChanged(true);
}

ORDER_EVENTS.onImageLoadError = function(event) {
    var el = FTD_DOM.whichElement(event);
    setDefaultImage(el);
}

ORDER_EVENTS.onFloristIdFocus = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null && el.id=='floristId' ) {
        //If this is a custom product, then the tab indexes are
        //set elsewhere 
        if( v_cartList[v_currentCartIdx].productObject.custom==true )
            return true;
    
        var fld = findFieldObject('floristId',v_currentCartIdx);
        if( fld!=null ) {
            if( fld.tabIndex==null )
                $('floristId').tabIndex="-1";
            else
                $('floristId').tabIndex = fld.tabIndex;
        } 
        
        fld = findFieldObject('floristSearch',v_currentCartIdx);
        if( fld!=null ) {
            if( fld.tabIndex==null )
                $('floristSearch').tabIndex="-1";
            else
                $('floristSearch').tabIndex = fld.tabIndex;
        }
    }
}

ORDER_EVENTS.onFloristSearchFocus = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null && el.id=='floristSearch' ) {
        //Don't screw around with the tab indexs on customer orders
        if( v_cartList[v_currentCartIdx].productObject.custom==true )
            return true;
            
        var fld = findFieldObject('floristId',v_currentCartIdx);
        if( fld!=null ) {
            if( fld.tabIndexInitial==null )
                $('floristId').tabIndex="-1";
            else
                $('floristId').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onFloristSearchBlur = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null && el.id=='floristSearch' ) {
        //Don't screw around with the tab indexs on custom orders
        if( v_cartList[v_currentCartIdx].productObject.custom==true )
            return true;
            
        var fld = findFieldObject('floristSearch',v_currentCartIdx);
        if( fld!=null ) {
            if( fld.tabIndexInitial==null )
                $('floristSearch').tabIndex="-1";
            else
                $('floristSearch').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onFloristCommentsFocus = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null && el.id=='floristComments' ) {
        //Don't screw around with the tab indexs on customer orders
        if( v_cartList[v_currentCartIdx].productObject.custom==true )
            return true;
            
        var fld = findFieldObject('floristComments',v_currentCartIdx);
        if( fld!=null ) {
            el.tabIndex = fld.tabIndex;
        }
    }
}

ORDER_EVENTS.onFloristCommentsBlur = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null && el.id=='floristComments' ) {
        //Don't screw around with the tab indexs on custom orders
        if( v_cartList[v_currentCartIdx].productObject.custom==true )
            return true;
            
        var fld = findFieldObject('floristComments',v_currentCartIdx);
        if( fld!=null ) {
            if( fld.tabIndexInitial==null )
                el.tabIndex="-1";
            else
                el.tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onProductDeliveryDateClick = function(event) {
    v_productDeliveryDateCal.deselectAll();
    v_productDeliveryDateCal.render();
    manageCalendarDisplay($('productDeliveryDateCalContainer'), v_productDeliveryDateCal);
    Event.stop(event);
}

ORDER_EVENTS.onProductOptionChanged = function(event) {
    var el = FTD_DOM.whichElement(event);
    var productPrice;
    var productDiscount;
//    var discountType;
    var orderObj = v_cartList[v_currentCartIdx];
    var prodObj = orderObj.productObject;
    
    if( el.checked==true ) {
        if( el.id=='productOptionVariable' ) {
            productPrice = UTILS.formatStringToPrice($('productOptionVariablePrice').value);
            productDiscount = productPrice;
            if( productPrice=='0.00' ) {
//                if( prodObj.standardDiscount==null ) 
                    productPrice=prodObj.standardPrice;
//                else 
//                    productPrice=prodObj.standardDiscount;
                productDiscount=prodObj.standardPrice;
            }
//            discountType = prodObj.discountType;
            $('productOptionVariablePrice').value = UTILS.addCommas(productPrice);
            $('productOptionVariablePrice').disabled = false;
            $('productOptionVariablePrice').focus();
            $('productOptionVariablePrice').select();
            v_size_indicator = "V";
            
        } else {
            $('productOptionVariablePrice').disabled = true;
            discountType = orderObj.discountType;
            if( discountType==null ) {
                discountType = prodObj.discountType;
            }
            if(el.id=='productOptionStandard') {
                productPrice=$('standardPrice').innerHTML;
                productDiscount=$('standardDiscount').innerHTML;
                v_size_indicator = "A";
            } else if(el.id=='productOptionPremium') {
                productPrice=$('premiumPrice').innerHTML;
                productDiscount=$('premiumDiscount').innerHTML;
                v_size_indicator = "C";
            } else if(el.id=='productOptionDeluxe') {
                productPrice=$('deluxePrice').innerHTML;
                productDiscount=$('deluxeDiscount').innerHTML;
                v_size_indicator = "B";
            }
            
            if( UTILS.isEmpty(productDiscount)==false && productDiscount.match(/(MILES|POINTS)/m) ) {
                productDiscount = parseInt(productDiscount)+'';
            }
        }
        
        FTD_DOM.oneOrNoCheckboxGroup(el);
//        orderObj.discountType = discountType;
        orderObj.productOption = el.id;
        orderObj.productPrice=productPrice;
        orderObj.productDiscount=productDiscount;
        orderObj.sizeIndicator=v_size_indicator;
        //orderObj.populateThumbNail();
        orderObj.populateDOM();
    }
}

ORDER_EVENTS.onProductVariablePriceChanged = function(event) {
    var orderObj = v_cartList[v_currentCartIdx];
    productPrice = UTILS.formatStringToPrice($('productOptionVariablePrice').value);
    productDiscount = orderObj.productObject.standardDiscount;
    if( productPrice!='0.00' ) {
        orderObj.productPrice=productPrice;
        orderObj.productDiscount=productPrice;
        if( orderObj.validateField('productOptionVariablePrice',true)==false ) {
            var fld = findFieldObject('productOptionVariablePrice',this.cartListIndex);
            notifyUser(fld.errorText,false,true);            
            closePopups();
            waitToSetFocus('productOptionVariablePrice');    
        }
        
    }
    $('productOptionVariablePrice').value = UTILS.addCommas(productPrice);
}

ORDER_EVENTS.onAcceptAVChanges = function(event) {
	//read the value associated with the avAddressRadio radio button and use that to get the right inputs
	var avIdx = $$('input:checked[type="radio"][name="avAddressRadioIdx"]')[0].value;
    $('recipientAddress').value = $('avSuggestedAddress_'+avIdx).innerHTML;
    $('recipientCity').value = $('avSuggestedCity_'+avIdx).innerHTML;
    FTD_DOM.selectOptionByValue($('recipientState'),$('avSuggestedState_'+avIdx).innerHTML);
    var oldValue=$('recipientZip').value;
    $('recipientZip').value = $('avSuggestedZip_'+avIdx).innerHTML;
    $('productZip').value = $('avSuggestedZip_'+avIdx).innerHTML;
    $('avCustomerInsisted').checked = false;
    $('avNeedsResolution').checked = false;
    
    //this index should also be the same as the orderObj.avsAddress array element that we want to keep
    //clear out the array keeping only the selected address
    var orderObj = v_cartList[v_currentCartIdx];
    var addressToKeep = orderObj.avsAddresses[avIdx];
    
    //update the addressToKeep with recip-address-verification-result = Fail
    //get the owner document so we can create a new element with it
    var doc = orderObj.avsAddresses[0].ownerDocument;
    var recip_address_verification_result = FTD_XML.createElementWithText(doc,'recip-address-verification-result','FAIL');
    addressToKeep.replaceChild(recip_address_verification_result, addressToKeep.getElementsByTagName("recip-address-verification-result")[0]);
    orderObj.avsAddresses = new Array(addressToKeep);
    
    
    JOE_ORDER.closeAddressVerificationPanel();
    waitToSetFocus('recipientState');
    if( v_checkAddressTimer.isRunning()==false ) v_checkAddressTimer.startTimer();
}

ORDER_EVENTS.onRejectAVChanges = function(event) {
    $('avCustomerInsisted').checked = true;
    $('avNeedsResolution').checked = false;
    JOE_ORDER.closeAddressVerificationPanel();
    waitToSetFocus('recipientState');
    if( v_checkAddressTimer.isRunning()==false ) v_checkAddressTimer.startTimer();
}

ORDER_EVENTS.onScriptElementFocus = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null ) {
        var script = v_scriptFields[el.id];
        if( script!=null ) {
            if(el.id=='callDataSourceCode') {
                var companyReplace = v_partnerScriptFields['Company'];
                var ppExtraReplace = v_partnerScriptFields['PreferredPartnerExtra'];
                if (!UTILS.isEmpty(companyReplace)) {
                    script = script.replace(/\(Company\)/,companyReplace);
		    $('confCompanyName').innerHTML = companyReplace;
                } else {
                    script = script.replace(/\(Company\)/,$('callDataCompanyName').value);
                }
                if (!UTILS.isEmpty(ppExtraReplace)) {
                    script = script.replace(/\(PreferredPartnerExtra\)/,ppExtraReplace);
                } else {
                    script = script.replace(/\(PreferredPartnerExtra\)/,'');
                }
                if( UTILS.isEmpty(v_repFirstName)==true ) 
                      v_repFirstName = '<i>(say your name)</i>';
                script = script.replace(/\(Rep Name\)/,v_repFirstName);   
            }
            $('scriptId').innerHTML = script;
        }
    }
}

ORDER_EVENTS.onCarrierDeliveryDateChange = function(event) {
    var changeIt = false;
    var el = FTD_DOM.whichElement(event);
    var selectedEl = FTD_DOM.getSelectedRadioButton('carrierDeliverOption');
    
    if( el.id=='carrierDeliveryDateGR' && selectedEl.id=='carrierDeliveryOptGR' ) {
        changeIt=true;
    } else if( el.id=='carrierDeliveryDate2F' && selectedEl.id=='carrierDeliveryOpt2F' ) {
        changeIt=true;
    } else if( el.id=='carrierDeliveryDateND' && selectedEl.id=='carrierDeliveryOptND' ) {
        changeIt=true;
    } else if( el.id=='carrierDeliveryDateSA' && selectedEl.id=='carrierDeliveryOptSA' ) {
        changeIt=true;
    }else if( el.id=='carrierDeliveryDateSU' && selectedEl.id=='carrierDeliveryOptSU' ) {
        changeIt=true;
    }
    
    if( changeIt ) {
        var newValue = FTD_DOM.getSelectedValue(el);
        FTD_DOM.selectOptionByValue($('productDeliveryDate'),newValue);
        FTD_DOM.selectOptionByValue($('productSearchDeliveryDate'),newValue);
        if(shouldRecalculateCart()==true){
        	ORDER_AJAX.calculateOrderTotals(true);
        }
    }
}

ORDER_EVENTS.onCarrierDeliveryOptChange = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( el.checked==true ) {
        FTD_DOM.oneOrNoCheckboxGroup(el);
        var selectEl=null;
        
        if( el.id=='carrierDeliveryOptGR' ) {
            selectEl=$('carrierDeliveryDateGR');
        } else if( el.id=='carrierDeliveryOpt2F' ) {
            selectEl=$('carrierDeliveryDate2F');
        } else if( el.id=='carrierDeliveryOptND' ) {
            selectEl=$('carrierDeliveryDateND');
        } else if( el.id=='carrierDeliveryOptSA' ) {
            selectEl=$('carrierDeliveryDateSA');
        }else if( el.id=='carrierDeliveryOptSU' ) {
            selectEl=$('carrierDeliveryDateSU');
        }
        
        if( selectEl!=null ) {
            var dateValue = FTD_DOM.getSelectedValue('productDeliveryDate');
            //If the current delivery date is in the list, select it
            FTD_DOM.selectOptionByValue(selectEl,dateValue);
            var newValue = FTD_DOM.getSelectedValue(selectEl);
            
            //Check to make sure the two dates are the same
            if( dateValue!=newValue ) {
                notifyUser('The selected ship method cannot be used for the requested delivery date of '+FTD_DOM.getSelectedText('productDeliveryDate')+'.\r\nChanging the delivery date to '+FTD_DOM.getSelectedText(selectEl)+'.',false,true);
                FTD_DOM.selectOptionByValue('productDeliveryDate',newValue);
                FTD_DOM.selectOptionByValue('productSearchDeliveryDate',newValue);
            }
            
            ORDER_EVENTS.setCarrierDeliveryTabIndicies();
        }
    }
}

ORDER_EVENTS.setCarrierDeliveryTabIndicies = function() {
    $('carrierDeliveryDateGR').tabIndex=-1;
    $('carrierDeliveryDate2F').tabIndex=-1;
    $('carrierDeliveryDateGR').tabIndex=-1;
    $('carrierDeliveryDateSA').tabIndex=-1;
    $('carrierDeliveryDateSU').tabIndex=-1;
    
    var radioEl = FTD_DOM.getSelectedRadioButton('carrierDeliverOption');
    var selectOpt = null;
    
    if( radioEl.id=='carrierDeliveryOptGR' ) {
        selectOpt=$('carrierDeliveryDateGR');
    } else if( radioEl.id=='carrierDeliveryOpt2F' ) {
        selectOpt=$('carrierDeliveryDate2F');
    } else if( radioEl.id=='carrierDeliveryOptND' ) {
        selectOpt=$('carrierDeliveryDateND');
    } else if( radioEl.id=='carrierDeliveryOptSA' ) {
        selectOpt=$('carrierDeliveryDateSA');
    }else if( radioEl.id=='carrierDeliveryOptSU' ) {
        selectOpt=$('carrierDeliveryDateSU');
    }
    
    if( selectOpt!=null ) {
        var fld = findFieldObject(selectOpt.id,v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            selectOpt.tabIndex = fld.tabIndex;
        }
    }
}

ORDER_EVENTS.onProductDeliveryDateChange = function(event) {
    changeProductDeliveryDate(FTD_DOM.getSelectedValue('productDeliveryDate'));
}

ORDER_EVENTS.onConfirmationNewOrderSameCustButton = function(event) {
    $('confNewOrderSameCustButton').disabled=true;
    $('confNewOrderButton').disabled=true;
    $('confExitButton').disabled=true;
    //ORDER_AJAX.recordCallTime();
    createCartForSameCustomer();
    $('confNewOrderSameCustButton').disabled=false;
    $('confNewOrderButton').disabled=false;
    $('confExitButton').disabled=false;
}

ORDER_EVENTS.onConfirmationNewOrderButton = function(event) {
    ORDER_EVENTS.startNewOrder();
}

ORDER_EVENTS.startNewOrder = function(event) {
    $('confNewOrderButton').disabled=true;
    $('confNewOrderSameCustButton').disabled=true;
    $('confExitButton').disabled=true;
    ORDER_AJAX.recordCallTime();
    createCartForNewCustomer();
    $('confNewOrderButton').disabled=false;
    $('confNewOrderSameCustButton').disabled=false;
    $('confExitButton').disabled=false;
}

ORDER_EVENTS.onConfirmationExitButton = function(event) {
    $('confExitButton').disabled=true;
    $('confNewOrderButton').disabled=true;
    $('confNewOrderSameCustButton').disabled=true;
    ORDER_AJAX.recordCallTime();
    doMainMenuAction('');
}

ORDER_EVENTS.onDoComSearch = function(orderNumber) {
	// If we came into JOE from COM to modify an order, return with all original COM data so context can be restored
	if (v_modifyOrderFromCom == true) {
		performReturnToComSearchAction(orderNumber);		
	// Otherwise it was a JOE order from scratch so just redirect to COM
	} else {
        var url = "ComSearchAction.do" + getSecurityParams(true);
        url+=     "&action=search";
        url+=     "&recipient_flag=y";
        url+=     "&in_order_number=";
        url+=     orderNumber;
        performAction(url);
	}
}

ORDER_EVENTS.onDoComSearchMO = function() {
	// If we came into JOE from COM to modify an order, return with all original COM data so context can be restored
	if (v_modifyOrderFromCom == true) {
		var ordnum = $('confMasterOrderNumber').innerHTML;
		performReturnToComSearchAction(ordnum);		
	// Otherwise it was a JOE order from scratch so just redirect to COM
	} else {
	    var url = "ComSearchAction.do" + getSecurityParams(true);
	    url+=     "&action=search";
	    url+=     "&recipient_flag=y";
	    url+=     "&in_order_number=";
	    url+=     $('confMasterOrderNumber').innerHTML;
	    performAction(url);
	}
}

ORDER_EVENTS.onDoComReturnToOriginal = function() {
	// Make sure we came into JOE from COM to modify an order, then return with all original COM data so context can be restored
	if (v_modifyOrderFromCom == true) {
		performReturnToComOriginalOrderAction();		
	} else {
		// We should never get here, if so there is a coding issue
		alert("Invalid configuration to return to COM. Please contact support. " + v_modifyOrderFromCom);
	}
}

ORDER_EVENTS.onAddonCheckbox1 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox1').checked==true ) {
        var fld = findFieldObject('addOnCheckbox1',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnCheckbox1').tabIndex = fld.tabIndex;
        }
        $('addOnQty1').disabled = false;
        $('addOnQty1').focus();
        $('addOnQty1').select();
    } else {
        $('addOnQty1').disabled = true;
    }
    
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) { 
        orderObj['addOnCheckbox1'] = $('addOnCheckbox1').checked;
        orderObj['addOnQty1'] = $('addOnQty1').value;
    }
    v_cartList[v_currentCartIdx].addOnCheckedArray[1] = $('addOnCheckbox1').checked;
    v_cartList[v_currentCartIdx].addOnQtyArray[1] = $('addOnQty1').value;
}

ORDER_EVENTS.onAddonCheckboxBlur1 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox1').checked==false ) {
        var fld = findFieldObject('addOnCheckbox1',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnCheckbox1').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onAddonCheckbox2 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox2').checked==true ) {
        var fld = findFieldObject('addOnCheckbox2',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnCheckbox2').tabIndex = fld.tabIndex;
        }
        $('addOnQty2').disabled = false;
        $('addOnQty2').focus();
        $('addOnQty2').select();
    } else {
        $('addOnQty2').disabled = true;
    }
    
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) { 
        orderObj['addOnCheckbox2'] = $('addOnCheckbox2').checked;
        orderObj['addOnQty2'] = $('addOnQty2').value;
    }
    v_cartList[v_currentCartIdx].addOnCheckedArray[2] = $('addOnCheckbox2').checked;
    v_cartList[v_currentCartIdx].addOnQtyArray[2] = $('addOnQty2').value;
}

ORDER_EVENTS.onAddonCheckboxBlur2 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox2').checked==false ) {
        var fld = findFieldObject('addOnCheckbox2',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnCheckbox2').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onAddonCheckbox3 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox3').checked==true ) {
        var fld = findFieldObject('addOnCheckbox3',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnCheckbox3').tabIndex = fld.tabIndex;
        }
        $('addOnQty3').disabled = false;
        $('addOnQty3').focus();
        $('addOnQty3').select();
    } else {
        $('addOnQty3').disabled = true;
    }
    
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) { 
        orderObj['addOnCheckbox3'] = $('addOnCheckbox3').checked;
        orderObj['addOnQty3'] = $('addOnQty3').value;
    }
    v_cartList[v_currentCartIdx].addOnCheckedArray[3] = $('addOnCheckbox3').checked;
    v_cartList[v_currentCartIdx].addOnQtyArray[3] = $('addOnQty3').value;
}

ORDER_EVENTS.onAddonCheckboxBlur3 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox3').checked==false ) {
        var fld = findFieldObject('addOnCheckbox3',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnCheckbox3').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onAddonCheckbox4 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox4').checked==true ) {
        var fld = findFieldObject('addOnCheckbox4',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnCheckbox4').tabIndex = fld.tabIndex;
        }
        $('addOnQty4').disabled = false;
        $('addOnQty4').focus();
        $('addOnQty4').select();
    } else {
        $('addOnQty4').disabled = true;
    }
    
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) { 
        orderObj['addOnCheckbox4'] = $('addOnCheckbox4').checked;
        orderObj['addOnQty4'] = $('addOnQty4').value;
    }
    v_cartList[v_currentCartIdx].addOnCheckedArray[4] = $('addOnCheckbox4').checked;
    v_cartList[v_currentCartIdx].addOnQtyArray[4] = $('addOnQty4').value;
}

ORDER_EVENTS.onAddonCheckboxBlur4 = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnCheckbox4').checked==false ) {
        var fld = findFieldObject('addOnCheckbox4',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnCheckbox4').tabIndex = fld.tabIndexInitial;
        }
    }
}



ORDER_EVENTS.onAddOnQty1Change = function(event) {
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].addOnQtyArray[1] = $('addOnQty1').selectedIndex + 1;
}

ORDER_EVENTS.onAddOnQty2Change = function(event) {
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].addOnQtyArray[2] = $('addOnQty2').selectedIndex + 1;
}

ORDER_EVENTS.onAddOnQty3Change = function(event) {
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].addOnQtyArray[3] = $('addOnQty3').selectedIndex + 1;
}

ORDER_EVENTS.onAddOnQty4Change = function(event) {
    ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].addOnQtyArray[4] = $('addOnQty4').selectedIndex + 1;
}

ORDER_EVENTS.onAddonFCheckbox = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnFCheckbox').checked==true ) {
        var fld = findFieldObject('addOnFCheckbox',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnFCheckbox').tabIndex = fld.tabIndex;
        }
        $('addOnFBannerText').disabled = false;
        $('addOnFBannerText').focus();
        
    } else {
        $('addOnFBannerText').disabled = true;
        $('addOnFBannerText').value = "";
    }
    
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) { 
        orderObj['addOnFCheckbox'] = $('addOnFCheckbox').checked;
        orderObj['addOnFBannerText'] = $('addOnFBannerText').value;
        orderObj.isRequired('addOnFBannerText');
    }
    
   v_cartList[v_currentCartIdx].addOnFCheckbox = $('addOnFCheckbox').checked;
   v_cartList[v_currentCartIdx].addOnFBannerText = $('addOnFBannerText').value;
}

ORDER_EVENTS.onAddonFCheckboxBlur = function(event) {
    var el = FTD_DOM.whichElement(event);
    if( $('addOnFCheckbox').checked==false ) {
        var fld = findFieldObject('addOnFCheckbox',v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null ) {
            $('addOnFCheckbox').tabIndex = fld.tabIndexInitial;
        }
    }
}

ORDER_EVENTS.onaddOnFBannerTextChange = function(event) {
	ORDER_EVENTS.setOrderObjectValue( FTD_DOM.whichElement(event) );
    v_cartList[v_currentCartIdx].addOnFBannerText = $('addOnFBannerText').value;
}

ORDER_EVENTS.onForceAvailabilityCheck = function(event) {
    var strMsg = 'If you force the checking of product availability,\r\n';
    strMsg +=    'all validation of required fields will be bypassed.\r\n';
    strMsg +=    'This action could cause unexpected and undesirable\r\n';
    strMsg +=    'results.\r\n\r\n';
    strMsg +=    'Are you sure you want to force the availability check?';
    if( confirm(strMsg) ) {
        ORDER_AJAX.checkProductAvailability(v_currentCartIdx,true);    
    }
}

ORDER_EVENTS.onProductIdOrZipCodeChange = function(event) {
    var enableDeliveryDate = false;
    if (isCountryDomestic($('productCountry').getValue())==false && UTILS.isEmpty($('productId').value)==false) {
        enableDeliveryDate = true;
    } else if ( UTILS.isEmpty($('productId').value)==false && UTILS.isEmpty($('productZip').value)==false) {
        enableDeliveryDate = true;
    }
        
    if (enableDeliveryDate) {
        v_displayDeliveryDate = true;
        $('productDeliveryDate').disabled=false;
        $('productCalendarButton').disabled=false;
        ORDER_AJAX.getDeliveryDates(v_currentCartIdx);
    }
}

ORDER_EVENTS.onProductDeliveryMethodChanged = function(event, tempIdx) {
	
    if (event == null) {
        orderIdx = tempIdx;
    } else {
        orderIdx = v_currentCartIdx;
    }

    var orderObj = v_cartList[orderIdx];

    if (event == null) {
        tempProductDeliveryMethod = orderObj.productDeliveryMethod;
    } else {
        var selectedRadio = FTD_DOM.getSelectedRadioButton('productDeliveryMethod');
        tempProductDeliveryMethod = selectedRadio.id;
    }

    //alert(orderIdx + ' delivery method: ' + tempProductDeliveryMethod + ' <' + previousDeliveryMethod + '>');

    if (previousDeliveryMethod != '' && event==null &&
        ((tempProductDeliveryMethod=='productDeliveryMethodFlorist' && previousDeliveryMethod != 'florist') ||
        (tempProductDeliveryMethod=='productDeliveryMethodDropship' && previousDeliveryMethod != 'dropship'))) {

        notifyUser('The delivery method has changed. Confirm with your customer.',false,true);
        v_deliveryMethodMsgDisplayed = true;
        // v_deliveryMethodMsgDisplayed changed to false in order_events.js.setAddons() 
        v_deliveryMethodChanged = v_deliveryMethodMsgDisplayed;

    }

    if (tempProductDeliveryMethod=='productDeliveryMethodFlorist') {
        $('ci_deliveryType___'+orderIdx).src = 'images/florist-delivery.png';
        $('ci_deliveryType___'+orderIdx).alt = 'Florist Delivery';
        $('ci_deliveryType___'+orderIdx).title = 'Florist Delivery';
        $('ci_deliveryType___'+orderIdx).width = '30';
        previousDeliveryMethod = 'florist';
        orderObj.carrierDelivered = false;
        orderObj.floristDelivered = true;
        if (orderIdx == v_currentCartIdx) {
            $('productOptionsDiv').style.display='block';
            $('deliveryOptionsDiv').style.display='none';
            $('floristOptionsDiv').style.display='block';
            $('floristIdDiv').style.display='block';
            $('specialInstructionsLabel').style.display='block';
        }
    } else if (tempProductDeliveryMethod=='productDeliveryMethodDropship') {
        $('ci_deliveryType___'+orderIdx).src = 'images/vendor-delivery.png';
        $('ci_deliveryType___'+orderIdx).alt = 'Vendor Delivery';
        $('ci_deliveryType___'+orderIdx).title = 'Vendor Delivery';
        $('ci_deliveryType___'+orderIdx).width = '60';
        previousDeliveryMethod = 'dropship';
        orderObj.carrierDelivered = true;
        orderObj.floristDelivered = false;
        if (orderIdx == v_currentCartIdx) {
            $('productOptionsDiv').style.display='block';
            $('deliveryOptionsDiv').style.display='block';
            if (v_modifyOrderFromCom == false || v_hasSpecialInstructions == false) {
                $('floristOptionsDiv').style.display='none';
            } else {
                $('floristOptionsDiv').style.display='block';
                $('floristIdDiv').style.display='none';
                $('specialInstructionsLabel').style.display='none'
            }
        }
    }

    if (event != null) {
        resetThumbnailDetail(orderIdx);
        //Calling Sympathy Location and Lead time check
        ORDER_EVENTS.onDeliveryLocationChange();
    }

    orderObj.productDeliveryMethod = tempProductDeliveryMethod;
    orderObj.configureAddons();
    
    
    

}

// ]]>
