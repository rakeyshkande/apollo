// <![CDATA[
var PRODUCT_SEARCH_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PRODUCT_SEARCH_EVENTS.prototypeVersion < 1.6)
      throw("PRODUCT_SEARCH_EVENTS requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("PRODUCT_SEARCH_EVENTS requires FTD_DOM");

PRODUCT_SEARCH_EVENTS.onProductSearchGoButton = function(event) { 

    var fldZip = findFieldObject('productSearchZip',v_currentCartIdx);
    var fldDate = findFieldObject('productSearchDeliveryDate',v_currentCartIdx);
    
    if( fldZip.validate(false)==false ) {
        notifyUser(fldZip.errorText,false,true);
    } else if( fldDate.validate(false)==false ) {
        notifyUser(fldDate.errorText,false,true);
    } else {
        PRODUCT_SEARCH.startSearch();
    }
    
    fldZip.setFieldStyle();
    fldDate.setFieldStyle();
}

PRODUCT_SEARCH_EVENTS.onProductSearchNavButton = function(event) { 
    var el = FTD_DOM.whichElement(event);
    
    if( el.id=='psNavBeginning' ) {
        PRODUCT_SEARCH.setCurrentSearchPage(0);
    } else if( el.id=='psNavPrevious' ) {
        if( PRODUCT_SEARCH.currentPageIndex==0 ) return;
        PRODUCT_SEARCH.setCurrentSearchPage(PRODUCT_SEARCH.currentPageIndex-1);
    } else if( el.id=='psNavNext' ) {
        if( PRODUCT_SEARCH.currentPageIndex+1>PRODUCT_SEARCH.maxPageIndex ) return;
        PRODUCT_SEARCH.setCurrentSearchPage(PRODUCT_SEARCH.currentPageIndex+1);
    } else if( el.id=='psNavEnd' ) {
        PRODUCT_SEARCH.setCurrentSearchPage(PRODUCT_SEARCH.maxPageIndex);
    }
}

PRODUCT_SEARCH_EVENTS.onProductSearchTopNavButton = function(event) { 
    var el = FTD_DOM.whichElement(event);
    
    if( el.id=='psTopNavBeginning' ) {
        PRODUCT_SEARCH.setTopCurrentSearchPage(0);
    } else if( el.id=='psTopNavPrevious' ) {
        if( PRODUCT_SEARCH.currentTopPageIndex==0 ) return;
        PRODUCT_SEARCH.setTopCurrentSearchPage(PRODUCT_SEARCH.currentTopPageIndex-1);
    } else if( el.id=='psTopNavNext' ) {
        if( PRODUCT_SEARCH.currentTopPageIndex+1>PRODUCT_SEARCH.maxTopPageIndex ) return;
        PRODUCT_SEARCH.setTopCurrentSearchPage(PRODUCT_SEARCH.currentTopPageIndex+1);
    } else if( el.id=='psTopNavEnd' ) {
        PRODUCT_SEARCH.setTopCurrentSearchPage(PRODUCT_SEARCH.maxTopPageIndex);
    }
}

PRODUCT_SEARCH_EVENTS.onProductSearchStopButton = function(event) {
    PRODUCT_SEARCH.stopSearch();
}

PRODUCT_SEARCH_EVENTS.onProductSearchClose = function(event) {
    try {
    	PRODUCT_SEARCH.closeSearch();
    } catch(err) {}
	// finally.  
    //It takes 1 second for the closing animation to complete, so wait 1.1 seconds then make sure the search panel is hidden
	// This code exists to work around a defect where the search panel would get stuck/frozen while closing.
	setTimeout(function() {
		$('rightDivId').show();
		$('productSearchDivId').hide();
		//the next two lines should undo any positioning where the content panel is in the middle of sliding down
		$('rightDivId').down().setStyle({bottom:''});
		$('rightDivId').down().setStyle({position:'static'});
	},1100);
    
    
    
    waitToSetFocus('productId');
}

PRODUCT_SEARCH_EVENTS.onTopSellerSelect = function(event) {
    var eventElement= FTD_DOM.whichElement(event);
    var pos = eventElement.id.lastIndexOf('_');
    if( pos>-1 ) {
        var idx = eventElement.id.substr(pos+1);
        PRODUCT_SEARCH.setOrderProduct(PRODUCT_SEARCH.topProducts[idx])
        PRODUCT_SEARCH.closeTopSellersPanel();
    }
}

PRODUCT_SEARCH_EVENTS.onTopSellerSelectDetail = function(idx) {
    PRODUCT_SEARCH.setProductDetail(PRODUCT_SEARCH.topProducts[idx]);
}

PRODUCT_SEARCH_EVENTS.onPromoSelect = function(event) {
    var eventElement= FTD_DOM.whichElement(event);
    var pos = eventElement.id.lastIndexOf('_');
    if( pos>-1 ) {
        var idx = eventElement.id.substr(pos+1);
        PRODUCT_SEARCH.setOrderProduct(PRODUCT_SEARCH.favoriteProducts[idx]);
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.onPromoSelectDetail = function(idx) {
    PRODUCT_SEARCH.setProductDetail(PRODUCT_SEARCH.favoriteProducts[idx]);
}

PRODUCT_SEARCH_EVENTS.onSuggestSelect = function(event) {
    var eventElement= FTD_DOM.whichElement(event);
    var pos = eventElement.id.lastIndexOf('_');
    if( pos>-1 ) {
        var idx = eventElement.id.substr(pos+1);
        PRODUCT_SEARCH.setOrderProduct(PRODUCT_SEARCH.suggestProducts[idx]);
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.onSuggestSelectDetail = function(idx) {
    PRODUCT_SEARCH.setProductDetail(PRODUCT_SEARCH.suggestProducts[idx]);
}

PRODUCT_SEARCH_EVENTS.onProductSearchCloseSuggest = function(event) {    
    if( $('psCrossSellDiv').visible() ) {
        new Effect.SlideUp('psCrossSellDiv',{afterFinish:PRODUCT_SEARCH.closeSearch});
    } else {
        PRODUCT_SEARCH.closeSearch(); 
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.onProductSelect = function(event) {
    var eventElement= FTD_DOM.whichElement(event);
    var pos = eventElement.id.lastIndexOf('_');
    if( pos>-1 ) {
        var idx = eventElement.id.substr(pos+1);
        PRODUCT_SEARCH.setOrderProduct(PRODUCT_SEARCH.searchProducts[PRODUCT_SEARCH.sortedSearchArray[idx].index]);
        ORDER_AJAX.getProductDetail($('productId').value,v_currentCartIdx,'ORDER',true,false);
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.onProductSelectDetail = function(idx) {
    PRODUCT_SEARCH.setProductDetail(PRODUCT_SEARCH.searchProducts[idx]);
}

PRODUCT_SEARCH_EVENTS.onProductSearchDetailClose = function(event) {
    PRODUCT_SEARCH.closeSearchDetails();
}

PRODUCT_SEARCH_EVENTS.onProductSearchDetailSelect = function(event) {
    if( v_currentProductDetailObject!=null ) {
        new Effect.SlideUp('psDetailDivId');
        PRODUCT_SEARCH.setOrderProduct(v_currentProductDetailObject);
        v_currentProductDetailObject = null;
        ORDER_AJAX.getProductDetail($('productId').value,v_currentCartIdx,'ORDER',true,false);
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.recentlyViewedLinkClicked = function(idx) {
    var prodObj = v_viewedProducts[idx];
    if( prodObj!=null ) {
        PRODUCT_SEARCH.closeRecentlyViewedPanel();
        PRODUCT_SEARCH.setOrderProduct(prodObj);
        v_currentProductDetailObject = null;
        waitToSetFocus('productOccasion');
    }
}

PRODUCT_SEARCH_EVENTS.onSearchDeliveryDateClick = function(event) {
    v_productSearchDeliveryDateCal.deselectAll();
    v_productSearchDeliveryDateCal.render();
    manageCalendarDisplay($('productSearchDeliveryDateCalContainer'), v_productSearchDeliveryDateCal);
    Event.stop(event);
}

PRODUCT_SEARCH_EVENTS.onChangeSortOrder = function(event) {
    PRODUCT_SEARCH.changeSortOrder($('productSearchSortCombo').value);
    PRODUCT_SEARCH.setCurrentSearchPage(0);
}

PRODUCT_SEARCH_EVENTS.onCloseTopSellers = function(event) {  
    PRODUCT_SEARCH.closeTopSellersPanel();
}

PRODUCT_SEARCH_EVENTS.onProductSearchZipSearch = function(event) {
    new Effect.SlideUp('productSearchDivId');
    TEXT_SEARCH.openZipSearch('productSearchZip',false,true);
}

PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange = function(event) {
    if( isCountryDomestic($('productSearchCountryCombo').getValue())==true ) {
        $('productSearchDeliveryDate').style.display='';
        $('productSearchDeliveryDateLabel').style.display='';
        $('productSearchCalendarButton').style.display='';
    } else {
        $('productSearchDeliveryDate').style.display='none';
        $('productSearchDeliveryDateLabel').style.display='none';
        $('productSearchCalendarButton').style.display='none';
    }
    setCountryFields('productSearchCountryCombo',null,'productSearchZip','productSearchZipSearch','productSearchKeywords');
}

PRODUCT_SEARCH_EVENTS.onCloseRecentlyViewed = function(event) {    
    PRODUCT_SEARCH.closeRecentlyViewedPanel();
}

PRODUCT_SEARCH_EVENTS.onProductSearchZipChange = function(event) {
    PRODUCT_SEARCH.setProductSearchFields();
}

PRODUCT_SEARCH_EVENTS.onProductSearchDeliveryDateChange = function(event) {
    PRODUCT_SEARCH.setProductSearchFields();
}

// ]]>
