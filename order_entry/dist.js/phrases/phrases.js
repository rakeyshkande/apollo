// <![CDATA[
var PHRASES = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}
if((typeof Prototype=='undefined') || PHRASES.prototypeVersion < 1.6)
      throw("phrases.js requires the Prototype JavaScript framework >= 1.6");
      
var v_testMode = false;        /********************************************/

function init() {
    Event.observe("exit_button","click",PHRASES_EVENTS.onExitButton);
    Event.observe("save_button","click",PHRASES_EVENTS.onSaveButton);
    Event.observe("add_button","click",PHRASES_EVENTS.onAddButton);
//    Event.observe("remove_button","click",PHRASES_EVENTS.onRemoveButton);

    if( v_testMode==true ) {
        v_serverURLPrefix='';
    }

    PHRASES_AJAX.getCardMessageData();
}

function unload() {
}

function addRow(cardMessage,active,lastRow) {
    var tbody = $('listBody');
    var rowCount = tbody.rows.length;
    
    var row = tbody.insertRow(rowCount);
    row.id='row___'+rowCount;
    var cell = row.insertCell(0);
    
    if( rowCount==0 ) {
        button = new FTD_DOM.DOMElement('button', {
            id:'upButton___'+rowCount,
            type:'button',
            disabled:'disabled'
        }).createNode();
    
        var img = new FTD_DOM.DOMElement('img', {
            id:'upImage___'+rowCount,
            src:'images/bullet_square_grey.png',
            alt:'Move up'
        }).createNode();
    } else {
        button = new FTD_DOM.DOMElement('button', {
            id:'upButton___'+rowCount,
            type:'button'
        }).createNode();
    
        img = new FTD_DOM.DOMElement('img', {
            id:'upImage___'+rowCount,
            src:'images/arrow_up_green.png',
            alt:'Move up'
        }).createNode();
    }
    button.appendChild(img);
    cell.appendChild(button);
    
    if( lastRow==true ) {
        button = new FTD_DOM.DOMElement('button', {
            id:'downButton___'+rowCount,
            type:'button',
            disabled:'disabled'
        }).createNode();
    
        img = new FTD_DOM.DOMElement('img', {
            id:'downImage___'+rowCount,
            src:'images/bullet_square_grey.png',
            alt:'Move down'
        }).createNode();
    } else {
        button = new FTD_DOM.DOMElement('button', {
            id:'downButton___'+rowCount,
            type:'button'
        }).createNode();
    
        img = new FTD_DOM.DOMElement('img', {
            id:'downImage___'+rowCount,
            src:'images/arrow_down_green.png',
            alt:'Move down'
        }).createNode();
    }
    button.appendChild(img);
    cell.appendChild(button);
    
    cell = row.insertCell(1);
    var input = new FTD_DOM.InputElement( {
        id:'description___'+rowCount,
        type:'text',
        size:'40',
        maxlength:'40',
        value:cardMessage
        } ).createNode();
    cell.appendChild(input);
    
    cell = row.insertCell(2);
    var div =  new FTD_DOM.DOMElement('div', {
        align:'center'
    }).createNode();
    cell.appendChild(div);
    
    if( active==true ) {
    input = new FTD_DOM.InputElement( {
        id:'active___'+rowCount,
        type:'checkbox',
        checked:'checked'
        } ).createNode();
    } else {
    input = new FTD_DOM.InputElement( {
        id:'active___'+rowCount,
        type:'checkbox'
        } ).createNode();
    }
    div.appendChild(input);
    
    cell = row.insertCell(3);
    div =  new FTD_DOM.DOMElement('div', {
        align:'center'
    }).createNode();
    cell.appendChild(div);
    input = new FTD_DOM.InputElement( {
        id:'remove___'+rowCount,
        type:'checkbox'
        } ).createNode();
    div.appendChild(input);
    
    Event.observe("upButton___"+rowCount,"click",PHRASES_EVENTS.onUpButton);
    Event.observe("downButton___"+rowCount,"click",PHRASES_EVENTS.onDownButton);
}

//function moveRow(targetRow,x){
//    if (targetRow) {
//        if (x=='up'&& targetRow.previousSibling)
//            targetRow.parentNode.insertBefore(targetRow,targetRow.previousSibling);
//        else if (x=='down'&& targetRow.nextSibling)
//            targetRow.parentNode.insertBefore(targetRow.nextSibling,targetRow);
////        else if (x=='first')
////            targetRow.parentNode.insertBefore(targetRow,targetRow.parentNode.firstChild);
////        else if (x=='last')
////            targetRow.parentNode.insertBefore(targetRow,null);
//    }
//}

function validate() {
    var tbody = $('listBody');
    var rowCount = tbody.rows.length;
    
    for( var idx=0; idx<rowCount; idx++ ) {
        if( $('remove___'+idx).checked==true ) continue;
        var desc = $('description___'+idx).value;
        if( UTILS.isEmpty(desc)==true ) {
            $('description___'+idx).focus();
            alert('Card message phrase cannot be blank');
            return false;
        }
    }
    
    return true;
}

function buildPhraseXml() {
    var doc = FTD_XML.createXmlDocument('card_messages');
    var root = doc.documentElement;
    
    root.appendChild(FTD_XML.createElementWithText(doc,'updated-by-id',v_repIdentity));
    
    var records = doc.createElement('records');
    root.appendChild(records);
    
    var tbody = $('listBody');
    var rowCount = tbody.rows.length;
    
    for( var idx=0; idx<rowCount; idx++ ) {
        if( $('remove___'+idx).checked==false ) {
            var record = FTD_XML.createElementWithText(doc,'card_message',encodeURIComponent($('description___'+idx).value));
            record.setAttribute('display_seq',($('row___'+idx).sectionRowIndex)+1);
            record.setAttribute('active_flag',$('active___'+idx).checked==true?'Y':'N');
            records.appendChild(record);
        }
    }
    
    return doc;
}