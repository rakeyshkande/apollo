// <![CDATA[
var FAVORITES_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || FAVORITES_AJAX.prototypeVersion < 1.6)
      throw("FAVORITES_AJAX requires the Prototype JavaScript framework >= 1.6");


FAVORITES_AJAX.getFavorites = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_FAV_GET_FAVORITES';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('favoritesAjax.do',FTD_AJAX_REQUEST_TYPE_POST,FAVORITES_AJAX.getFavoritesCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

FAVORITES_AJAX.getFavoritesCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        var records = XPath.selectNodes('/result/rs[@name="favorites"]/record',xmlDoc);
        var idx;
        
        for(idx=1; idx <= 3; idx++ ) 
        {
            $('product' + idx).value = '';        
        }

        for(idx=0; idx<records.length; idx++ ) 
        {
            var productId = FTD_XML.selectNodeText(records[idx],'product_id');
            var displaySeq = FTD_XML.selectNodeText(records[idx],'display_seq');
            
            $('product' + displaySeq).value = productId;        
        }
    
    }
 
}

FAVORITES_AJAX.saveChanges = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_FAV_UPDATE_FAVORITES';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var product1 = $('product1').getValue();
    param = FTD_XML.createElementWithText(doc,'param',product1);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID_1';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var product2 = $('product2').getValue();
    param = FTD_XML.createElementWithText(doc,'param',product2);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID_2';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var product3 = $('product3').getValue();
    param = FTD_XML.createElementWithText(doc,'param',product3);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID_3';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('favoritesAjax.do',FTD_AJAX_REQUEST_TYPE_POST,FAVORITES_AJAX.saveChangesCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
 
}

FAVORITES_AJAX.saveChangesCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        el = $('favNotifyArea');
        if( checkResultSetStatus(xmlDoc,"validation",false,false,false,'favNotify') ) 
        {
            message = 'Changes Successfully Saved';
            el.style.color = v_normalTextColor;
            el.style.fontWeight = 'normal';
            notifyUser(message,false,true,'favNotify');
        }
        else
        {
            message = 'Save Failed';
            oldMsg = $('favNotifyArea').innerHTML;
            $('favNotifyArea').innerHTML = oldMsg+'<br>'+message;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
        }
    }
    
    // Fire off a call to repopulate the data on the screen
    FAVORITES_AJAX.getFavorites();
}

