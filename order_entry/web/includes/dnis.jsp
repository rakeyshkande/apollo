<%
    String dnis = (String)request.getParameter("dnis");
    if( dnis==null ) dnis="";
    String identity = (String)request.getAttribute("identity");
    if( identity==null ) identity="testcsr1";
    String firstName = (String)request.getAttribute("first-name");
    if( firstName==null ) firstName="";
    String secureURL = (String)request.getAttribute("secure-url");
    if( secureURL==null ) 
        secureURL="";
    else
        secureURL+="/";
    String updateOrderId = (String)request.getParameter("orig_ext_order_num");
    if( updateOrderId==null ) updateOrderId="";
%>

<script type="text/javascript">
  var v_repIdentity = '<%=identity%>';
  var v_repFirstName = '<%=firstName%>';
  var v_serverURLPrefix = '<%=secureURL%>';
</script>

<table>
  <!--<thead/>-->
  <tbody>
    <tr>
        <td class="labelRight">
        <img id="pagetop" src="images/1px_image.gif" width="1px" height="1px" alt="">
        <label class="labelRight" id="callDataDNISLabel" for="callDataDNIS">DNIS</label>
        </td>
        <td>
          <input type="text" 
                 id="callDataDNIS"
                 size="4"
                 disabled="disabled"
                 value="<%=dnis%>">
            <input type="hidden" id="callDataOrigin" value="">
            <input type="hidden" id="callDataCompany" value="">
            <input type="hidden" id="callDataCompanyName" value="">
            <input type="hidden" id="partnerName" value="">
            <input type="hidden" id="updateOrderId" value="<%=updateOrderId%>">

        </td>
        <td width="35%">
            <span id="callDataDNISDesc" style="color:#888888"></span>
        </td>
        <td class="labelRight">
          <label class="labelRight" id="callDataSourceCodeLabel" for="callDataSourceCode">Source Code</label>
        </td>
        <td width="50px">
          <input type="text" 
                 id="callDataSourceCode"
                 size="11"
                 disabled="disabled">
        </td>
        <td align="left" width="30px">
          <button 
               type="button"
               id="callDataSourceCodeSearch"
               disabled="disabled">
            <img src="images/magnify.jpg"
               alt="Source code search">
          </button>
        </td>
        <td width="35%">
            <span id="callDataSourceCodeDesc" style="color:#888888"></span>
        </td>
      </tr>
    </tbody>
</table>

<div id="sourceCodePwdDiv" class="detailDiv" style="display:none">
    <div id="sourceCodeInnerPwdDiv" align="center">
        <table align="center">
            <tbody valign="middle">
                <tr>
                    <td align="center"><span>This source code requires a password.</span></td>
                </tr>
                <tr>
                    <td align="center"><span>Please enter the password below.</span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center"><input id="callDataSourceCodePwd" type="password" size="20"></td>
                </tr>
                <tr>
                    <td align="center"><button type="button" id="callDataSourceCodePwdButton">Validate Password</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="sourceCodePreferredPartnerDiv" class="detailDiv" style="display:none">
    <table align="center" width="50%" border="0">
        <tbody valign="middle">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="left"><span>Stop: this is a <label id="preferredPartnerName">XXXX</label> source code. Please</span></td>
            </tr>
            <tr>
                <td align="left"><span>advise the customer:</span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="left">
                    <span>"I apologize, I need to transfer your call to an</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <span>agent who can better assist you. Can you</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <span>please hold for one moment while I transfer</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <span>you? Thank you."</span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="left">
                    <span>Transfer the call to <label id="preferredPartnerNumber">XXXX</label></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
