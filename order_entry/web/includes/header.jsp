<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<script type="text/javascript" src="js/utilities/clock.js"></script>
<div id="headerDiv" class="headerDiv">
  <table width="100%" border="0" cellspacing="2" cellpadding="0">
    <tbody>
      <tr> 
        <td width="150px" align="left" style="vertical-align:middle;padding-top:.4em;"><img src="images/wwwftdcom_131x32.gif" alt="ftd.com" border="0" height="32"></td>
        <td align="center" class="header">
          <tiles:getAsString name="title"/>
        </td>
        <td width="150px" align="right" style="vertical-align:middle;padding-top:.4em;" class="clock">
            <span id="headerTime"> <script type="text/javascript">startClock();</script></span>
        </td>
      </tr>			
      <tr>
        <td colspan="3"><hr></td>
      </tr>
    </tbody>
  </table>
</div>