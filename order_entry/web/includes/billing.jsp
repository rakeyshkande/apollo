<img id="billingtop" src="images/1px_image.gif" width="1px" height="1px" alt="">
<table class="billing" border="1">
  <caption class="orderCaption">Customer</caption>
  
  <tbody>
    <tr>
      <td>
        <table width="100%" bgcolor="#eeeeee">
          <tbody>
            <tr>
              <td colspan="2">
                <input type="checkbox"
                      style="border-color:#eeeeee"
                      id="customerRecipSameCheckbox">
                &nbsp;
                <label id="customerRecipSameCheckboxLabel" 
                       class="radioLabel" 
                       for="customerRecipSameCheckbox">Customer is the same as recipient:</label>
                &nbsp;&nbsp;
                  <select size="1" disabled="disabled"
                          id="customerRecipCombo"
                          title="Select the recipient which is the customer">
                  </select>
              </td>
            </tr>
            <tr>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerBillingPhoneLabel"
                       for="customerBillingPhone">Phone 1</label>
              </td>
              <td>
                <input type="text" 
                       id="customerBillingPhone"
                       size="10">
                &nbsp;&nbsp;
                <label class="labelRight"
                       id="customerBillingPhoneExtLabel"
                       for="customerBillingPhoneExt">Ext</label>
                &nbsp;
                <input type="text"
                       id="customerBillingPhoneExt"
                       size="4">
                &nbsp;&nbsp;
                <button type="button"
                       id="customerPhoneSearch">
                  <img src="images/magnify.jpg" 
                       id="customerPhoneSearchImage"
                       alt="Customer phone search">
                </button>
              </td>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerEveningPhoneLabel"
                       for="customerEveningPhone">Phone 2</label>
              </td>
              <td>
                <input type="text" 
                       id="customerEveningPhone"
                       size="10">
                &nbsp;&nbsp;
                <label class="labelRight"
                       id="customerEveningPhoneExtLabel"
                       for="customerEveningPhoneExt">Ext</label>
                &nbsp;
                <input type="text"
                       id="customerEveningPhoneExt"
                       size="4">
              </td>
            </tr>
            <tr>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerFirstNameLabel"
                       for="customerFirstName">First Name</label>
              </td>
              <td>
                <input type="text" 
                       id="customerFirstName"
                       size="25">
              </td>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerLastNameLabel"
                       for="customerLastName">Last Name</label>
              </td>
              <td>
                <input type="text" 
                       id="customerLastName"
                       size="25">
              </td>
            </tr>
            <tr>
              <td class="labelRight" valign="top">
                <label class="labelRight"
                       id="customerAddressLabel"
                       for="customerAddress">Address</label>
              </td>
              <td valign="top" colspan="3" rowspan="2">
                <textarea cols="45" 
                          rows="2"
                          class="joe"
                          id="customerAddress"></textarea>
              </td>
            </tr>
            <tr>
                <td class="labelRight" valign="top">
                    <span id="customerAddressCount"></span>
                </td>
            </tr>
            <tr>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerZipLabel"
                       for="customerZip">Zip&nbsp;Code</label>
              </td>
              <td>
                <input id="customerZip"
                       type="text" 
                       size="6">
                &nbsp;&nbsp;
                <button type="button"
                       id="customerZipSearch">
                  <img src="images/magnify.jpg" 
                       id="customerZipSearchImage"
                       alt="Customer zip search">
                </button>
              </td>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerCountryComboLabel"
                       for="customerCountryCombo">Country</label>
              </td>
              <td>
                <select size="1"
                        name="customerCountryCombo"
                        id="customerCountryCombo">
                </select>
              </td>
            </tr>
            <tr>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerCityLabel"
                       for="customerCity">City</label>
              </td>
              <td>
                <input type="text" 
                       id="customerCity"
                       size="30">
              </td>
              <td class="labelRight">
                <label class="labelRight"
                       id="customerStateComboLabel" 
                       for="customerStateCombo">State</label>
              </td>
              <td>
                  <select size="1"
                          id="customerStateCombo">
                  </select>
              </td>
            </tr>
            <tr id="optInRow">
              <td class="labelRight">
                <label class="labelRight"
                       id="customerEmailAddressLabel"
                       for="customerEmailAddress">Email Address</label>
              </td>
              <td>
                <input type="text" 
                       id="customerEmailAddress"
                       size="40">
              </td>
              <td>&nbsp;</td>
              <td>
                <input type="checkbox" 
                       style="border-color:#eeeeee"
                       id="customerSubscribeCheckbox">
                &nbsp;
                <label id="customerSubscribeCheckboxLabel" 
                    class="radioLabel" 
                      for="customerSubscribeCheckbox">Subscription Opt In</label>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<table id="membership" class="billing" border="1">
    <caption class="orderCaption">Membership Information</caption>
    <tbody>
        <tr>
            <td>
                <table width="100%" bgcolor="#eeeeee">
                    
                    
                    <tbody>
                        <tr>
                            <td class="labelRight">
                                <label class="labelRight"
                                   id="membershipIdLabel"
                                   for="membershipId">Membership Info</label>
                                 
                            </td>
                            <td width="23%">
                                <input id="membershipId"
                                       type="text"
                                       size="25">
                                       &nbsp;&nbsp;								
							</td>
							<td>
							<button type="button" id="membershipIdSearch"  style="display:none;" >
									<img src="images/magnify.jpg" 
										alt="AAA Club search">
								</button>
							</td>
                            <td>
                                <input type="checkbox"
                                       id="membershipSkip"
                                       style="border-color:#eeeeee">
                                       &nbsp;
                                <label id="membershipSkipLabel" for="membershipSkip" class="radioLabel">Skip</label>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="labelRight">
                                <label class="labelRight"
                                   id="membershipFirstNameLabel"
                                   for="membershipFirstName">First Name</label>
                            </td>
                            <td>
                                <input id="membershipFirstName" 
                                       type="text"
                                       size="25">
                            </td>
                            <td class="labelRight">
                                <label class="labelRight"
                                   id="membershipLastNameLabel"
                                   for="membershipLastName">Last Name</label>
                            </td>
                            <td>
                                <input id="membershipLastName" 
                                       type="text"
                                       size="25">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table class="billing" border="1">
    <caption class="orderCaption">Payment</caption>
    
    <tbody>
        <tr>
            <td>
                <table width="100%" bgcolor="#eeeeee">
                    
                    
                    <tbody>
                        <tr>
                          <td class="labelRight">
                            <label class="labelRight"
                                   id="paymentTypeComboLabel"
                                   for="paymentTypeCombo">Type</label>
                          </td>
                          <td align="left">
                              <select size="1"
                                      id="paymentTypeCombo">
                              </select>
                          </td>
                          <td>
                            <input id="paymentFraud" type="checkbox" style="border-color:#eeeeee">&nbsp;<label id="paymentFraudLabel" for="paymentFraud" class="radioLabel">Fraud</label>
                          </td>
                          <td width="20%">
                            <input id="paymentCSCOverride" type="checkbox" style="border-color:#eeeeee">&nbsp;<label id="paymentCSCOverrideLabel" for="paymentCSCOverride" class="radioLabel">CSC override</label>
                          </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="payment">
                    
                    
                    <tbody>
                        <tr id="paymentGCCaption">
                          <td class="orderCaption" valign="top" colspan="3">Gift Certificate</td>
                        </tr>
                        <tr id="paymentGCDiv">
                          <td class="labelRight">
                            <label class="labelRight">Amount</label>
                          </td>
                          <td class="price">
                              <span id="paymentGCAmount">0.00</span>
                          </td>
                          <td width="100%">
                              <table>
                                
                                
                                <tbody>
                                  <tr>
                                    <td class="labelRight">
                                      <label class="labelRight">Certificate #</label>
                                    </td>
                                    <td>
                                      <input type="text"
                                           id="paymentGCNumber" 
                                           size="20">
                                    </td>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                             id="paymentGCBalanceComboLabel"
                                             for="paymentGCBalanceCombo">Balance</label>
                                    </td>
                                    <td>
                                      <select size="1" 
                                              id="paymentGCBalanceCombo">
                                        <option value="" selected="selected">
                                          -- Select One --
                                        </option>
                                        <option value="CC">
                                          Credit Card
                                        </option>
                                        <option value="NC">
                                          No Charge
                                        </option>
                                      </select>
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                          </td>
                        </tr>
                        <tr id="paymentCCCaption">
                          <td  class="orderCaption" valign="top" colspan="3">Credit Card</td>
                        </tr>
                        <tr id="paymentCCDiv">
                          <td width="70px">
                            <label class="labelRight">Amount</label>
                          </td>
                          <td class="price" align="left">
                            <span id="paymentCCAmount">0.00</span>
                            <input type="hidden" id="paymentCCApprovalVerbage" value="">
                            <input type="hidden" id="paymentCCApprovalActionCode" value="">
                            <input type="hidden" id="paymentCCAVSResult" value="">
                            <input type="hidden" id="paymentCCAcqData" value="">
                            <input type="hidden" id="paymentCCBypassAuthFlag" value="N">
                          </td>
                          <td width="100%">
                              <table>
                                
                                
                                <tbody>
                                  <tr>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                             id="paymentCCNumberLabel"
                                            for="paymentCCNumber">CC #</label>
                                    </td>
                                    <td>
                                      <input type="text"
                                             id="paymentCCNumber" 
                                           size="16">
                                    </td>
                                    <td class="labelRight">
                                        <label class="labelRight"
                                               id="paymentCCTypeComboLabel"
                                               for="paymentCCTypeCombo">Type</label>
                                    </td>
                                    <td>
                                        <select size="1"
                                                id="paymentCCTypeCombo">
                                          <option value="" selected="selected">&nbsp;</option>
                                            <option value="AX">American Express</option>
                                            <option value="CB">Carte Blanche</option>
                                            <option value="DC">Diner's Club</option>
                                            <option value="DI">Discover</option>
                                            <option value="MC">Master Card</option>
                                            <option value="VI">Visa</option>
                                        </select>
                                    </td>
                                    <td class="labelRight">&nbsp;&nbsp;<label class="labelRight"
                                                id="paymentCCExpMonthComboLabel"
                                                for="paymentCCExpMonthCombo">Exp&nbsp;Date</label>
                                    </td>
                                    <td>
                                        <select size="1"
                                                id="paymentCCExpMonthCombo">
                                          <option value="" selected="selected">
                                            month
                                          </option>
                                          <option value="01">
                                            January - 1
                                          </option>
                                          <option value="02">
                                            February - 2
                                          </option>
                                          <option value="03">
                                            March - 3
                                          </option>
                                          <option value="04">
                                            April - 4
                                          </option>
                                          <option value="05">
                                            May - 5
                                          </option>
                                          <option value="06">
                                            June - 6
                                          </option>
                                          <option value="07">
                                            July - 7
                                          </option>
                                          <option value="08">
                                            August - 8
                                          </option>
                                          <option value="09">
                                            September - 9
                                          </option>
                                          <option value="10">
                                            October - 10
                                          </option>
                                          <option value="11">
                                            November - 11
                                          </option>
                                          <option value="12">
                                            December - 12
                                          </option>
                                        </select>
                                      </td>
                                      <td>
                                        <select size="1"
                                                id="paymentCCExpYear">
                                        </select>
                                      </td>
                                      <td class="labelRight">&nbsp;&nbsp;<label class="labelRight"
                                                id="paymentCSCLabel"
                                                for="paymentCSC">CSC</label>
                                      </td>
                                      <td>
                                        <input type="text"
                                           id="paymentCSC" 
                                           size="5" maxlength="4">
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                          </td>
                        </tr>
                        <tr id="paymentCCManAuthDiv" style="display:none">
                            <td>
                                <label class="labelRight" id="paymentCCManualAuthLabel" for="paymentCCManualAuth">Manual&nbsp;Authorization</label>
                            </td>
                            <td colspan="2">
                                <input id="paymentCCManualAuth" type="text" size="10">
                            </td>
                        </tr>
                        <tr id="paymentINCaption">
                          <td  class="orderCaption" valign="top" colspan="3">Invoice</td>
                        </tr>
                        <tr id="paymentINDiv">
                          <td>
                            <label class="labelRight">Amount</label>
                          </td>
                          <td class="price">
                            <span id="paymentINAmount">0.00</span>
                          </td>
                          <td width="100%">
                              &nbsp;
                          </td>
                        </tr>
                        <tr id="paymentBillingInfo" style="display:none">
                            <td colspan="3">
                                <table width="100%" bgcolor="#eeeeee">
                                    <tbody id="billingInfoTbody">
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr id="paymentNCCaption">
                          <td  class="orderCaption" valign="top" colspan="3">No Charge</td>
                        </tr>
                        <tr id="paymentNCDiv">
                          <td valign="top">
                            <label class="labelRight">Amount</label>
                          </td>
                          <td class="price" valign="top">
                            <span id="paymentNCAmount">0.00</span>
                          </td>
                          <td width="100%" valign="top">
                              <table bgcolor="#eeeeee">
                                
                                
                                <tbody>
                                  <tr>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                             id="paymentNCTypeLabel"
                                             for="paymentNCType">Type</label>
                                    </td>
                                    <td>
                                        <select id="paymentNCType">
                                            <option value="" selected="selected">-- please select --</option>
                                            <option value="COMP">Complimentary</option>
                                            <option value="RESEND">Resend</option>
                                            <option value="FLOWERS_MONTHLY">Flowers Monthly</option>
                                        </select>
                                    </td>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                             id="paymentNCReasonLabel"
                                             for="paymentNCReason">Reason</label>
                                    </td>
                                    <td>
                                        <select id="paymentNCReason">
                                            <option value="" selected="selected">-- please select --</option>
                                            <option value="NONDELIVERY">Non-Delivery</option>
                                            <option value="NOSCAN">No Scan</option>
                                            <option value="LATE">Late</option>
                                            <option value="OTHER">Other</option>
                                            <option value="QUALITY">Quality</option>
                                            <option value="SUBSTITUTION">Substitution</option>
                                        </select>
                                    </td>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                           id="paymentNCRefOrderLabel"
                                           for="paymentNCRefOrder">Ref&nbsp;Order</label>
                                    </td>
                                    <td>
                                      <input type="text"
                                           id="paymentNCRefOrder" 
                                           size="10">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                           id="paymentNCAuthIdLabel"
                                           for="paymentNCAuthId">Approved By</label>
                                    </td>
                                    <td>
                                      <input type="text"
                                           id="paymentNCAuthId" 
                                           size="10">
                                    </td>
                                    <td class="labelRight">
                                      <label class="labelRight"
                                             id="paymentNCAuthPwdLabel"
                                             for="paymentNCAuthPwd">Password</label>
                                    </td>
                                    <td>
                                      <input type="password"
                                           id="paymentNCAuthPwd" 
                                           size="10">
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                          </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<br>

<div align="center"> 
        <button id="billingButtonCalculate" 
              type="button"
              >Calculate Totals</button>
        &nbsp;
        <button id="billingButtonSubmit" 
              type="button"
              >Submit Cart</button>
</div>
<img id="billingbottom" src="images/1px_image.gif" width="1px" height="1px" alt="">