<div id="spellCheckDiv" align="center" style="padding:2px">
    <table cellpadding="0" cellspacing="0" width="100%">
        <caption>Spell Check</caption>
        <tbody>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <div>
                        <table class="alerttable" align="center" cellSpacing="0" cellPadding="4" width="400" border="0">
                            <tr>
                                <td align="center" colspan="2">
                                    <table style="border: thin inset #ffffff">
                                        <tr>
                                            <td>
                                                <div id='sc_preview' style="width:400px;height:100px;overflow:auto;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Change to:<br>
                                    <input id="sc_word" style="width:20em" size="5"> 
                                </td>
                                <td valign="top">Suggestions:<br>
                                    <select id="sc_suggestions" style="width:20em" size="5">
                                    </select> 
                                </td>
                            </tr>
                            <tr class="bgd">
                                <td align="center" colspan="2">
                                    <input id="sc_completedButton" class="abutton" type="button" value="Done">
                                    <input id="sc_changeButton" class="abutton" type=button value="Change"> 
                                    <input id="sc_changeAllButton" class="fbutton" type=button value="Change All"> 
                                    <input id="sc_ignoreButton" class="fbutton" type=button value="Ignore"> 
                                    <input id="sc_ignoreAllButton" class="fbutton" type=button value="Ignore All"> 
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    Event.observe("sc_word","keypress",SPELL_CHECK.onKeyPressWord);
    Event.observe("sc_suggestions","change",SPELL_CHECK.onChangeSuggestions);
    Event.observe("sc_completedButton","click",SPELL_CHECK.onCompleted);
    Event.observe("sc_changeButton","click",SPELL_CHECK.onChange);
    Event.observe("sc_changeAllButton","click",SPELL_CHECK.onChangeAll);
    Event.observe("sc_ignoreButton","click",SPELL_CHECK.onIgnore);
    Event.observe("sc_ignoreAllButton","click",SPELL_CHECK.onIgnoreAll);
</script>