<!-- This JSP defines the form containing data that originated from COM (when COM enters JOE to do Modify Order).
     The data must be returned to COM in order to restore the proper COM context. 
     Logic below will set javascript boolean variable (v_modifyOrderFromCom) based on whether we came from COM (true) or not (false).
 -->
<%
   // Base URL for COM originated from global_parms (and was saved as attribute by Java Action class)
   //
   String comReturnUrl = (String)request.getAttribute("com-return-url");
   if (comReturnUrl == null) { 
      comReturnUrl = "";
   }

  // General Attributes
  //
  String g_securitytoken = request.getParameter("securitytoken");
  String g_context = request.getParameter("context");
  String g_adminAction = request.getParameter("adminAction");
  String g_applicationcontext = request.getParameter("applicationcontext");
  String g_ext_ordnum = request.getParameter("orig_ext_order_num");
  String g_orig_order_detail_id = request.getParameter("orig_order_detail_id");
  String g_call_cs_number = request.getParameter("call_cs_number");
  String g_call_type_flag = request.getParameter("call_type_flag");
  String g_call_brand_name = request.getParameter("call_brand_name");
  String g_call_dnis = request.getParameter("call_dnis");
  String g_start_origin = request.getParameter("start_origin");
  String g_customer_id = request.getParameter("customer_id");

  // If external_order_number was set then it's safe to assume we came from COM to do Modify Order, so set flag
  //
  boolean moFromCom = false;
  if (g_ext_ordnum != null && !g_ext_ordnum.isEmpty() && (g_ext_ordnum.indexOf("null") < 0)) {
      moFromCom = true;
  }

  // Updated Orders should use a default DNIS (e.g.,8888).  We just pass it from COM, so grab it and set variable.
  //
  String moDnis = request.getParameter("modify_order_dnis");
  
  // Call Log and Entity History Attributes
  //
  String cl_t_comment_origin_type = request.getParameter("t_comment_origin_type");
  String cl_t_entity_history_id = request.getParameter("t_entity_history_id");
  String cl_t_call_log_id = request.getParameter("t_call_log_id");

  // Search Attributes
  //
  String sc_cust_ind = request.getParameter("sc_cust_ind"); 
  String sc_tracking_number = request.getParameter("sc_tracking_number"); 
  String sc_cust_number = request.getParameter("sc_cust_number");
  String sc_phone_number = request.getParameter("sc_phone_number");
  String sc_rewards_number = request.getParameter("sc_rewards_number");
  String sc_pc_membership = request.getParameter("sc_pc_membership");
  String sc_recip_ind = request.getParameter("sc_recip_ind");
  String sc_order_number = request.getParameter("sc_order_number");
  String sc_email_address = request.getParameter("sc_email_address");
  String sc_zip_code = request.getParameter("sc_zip_code");
  String sc_last_name = request.getParameter("sc_last_name");
  
  // Queue Attributes
  //
  String q_rtq = request.getParameter("rtq");
  String q_rtq_queue_type = request.getParameter("rtq_queue_type");
  String q_rtq_queue_ind = request.getParameter("rtq_queue_ind");
  String q_rtq_attached = request.getParameter("rtq_attached");
  String q_rtq_tagged = request.getParameter("rtq_tagged");
  String q_rtq_user = request.getParameter("rtq_user");
  String q_rtq_group = request.getParameter("rtq_group");
  String q_rtq_sort_column = request.getParameter("rtq_sort_column");
  String q_rtq_max_records = request.getParameter("rtq_max_records");
  String q_rtq_current_sort_direction = request.getParameter("rtq_current_sort_direction");
  String q_rtq_position = request.getParameter("rtq_position");
  String q_rtq_sort_on_return = request.getParameter("rtq_sort_on_return");
  String q_rtq_received_date = request.getParameter("rtq_received_date");
  String q_rtq_delivery_date = request.getParameter("rtq_delivery_date");
  String q_rtq_tag_date = request.getParameter("rtq_tag_date");
  String q_rtq_last_touched = request.getParameter("rtq_last_touched");
  String q_rtq_type = request.getParameter("rtq_type");
  String q_rtq_sys = request.getParameter("rtq_sys");
  String q_rtq_timezone = request.getParameter("rtq_timezone");
  String q_rtq_sdg = request.getParameter("rtq_sdg");
  String q_rtq_disposition = request.getParameter("rtq_disposition");
  String q_rtq_priority = request.getParameter("rtq_priority");
  String q_rtq_new_item = request.getParameter("rtq_new_item");
  String q_rtq_occasion = request.getParameter("rtq_occasion");
  String q_rtq_location = request.getParameter("rtq_location");
  String q_rtq_member_num = request.getParameter("rtq_member_num");
  String q_rtq_zip = request.getParameter("rtq_zip");
  String q_rtq_tag_by = request.getParameter("rtq_tag_by");
  String q_rtq_filter = request.getParameter("rtq_filter");
  
  // Call Disposition & Decision Result Framework Attributes
  //
  String cdisp_autoOpenedFlag = request.getParameter("cdisp_autoOpenedFlag");
  String cdisp_reminderFlag = request.getParameter("cdisp_reminderFlag");
  String cdisp_cancelledOrder = request.getParameter("cdisp_cancelledOrder");
  String cdisp_enabledFlag = request.getParameter("cdisp_enabledFlag");
  String cdisp_resetFlag = request.getParameter("cdisp_resetFlag");
  String dr_availableEntities = request.getParameter("dr_availableEntities");
  String dr_currentLevel = request.getParameter("dr_currentLevel");        
  String dr_currentLevelParentValueId = request.getParameter("dr_currentLevelParentValueId");
  String dr_currentLevelComment = request.getParameter("dr_currentLevelComment");
  String dr_currentLevelSelectedValueId = request.getParameter("dr_currentLevelSelectedValueId");
  String dr_decisionConfigId = request.getParameter("dr_decisionConfigId");
  String dr_decisionStatus = request.getParameter("dr_decisionStatus");
  String dr_decisionTypeCode = request.getParameter("dr_decisionTypeCode");
  String dr_dispositionStatus = request.getParameter("dr_dispositionStatus");
  String dr_dnisId = request.getParameter("dr_dnisId");
  String dr_openFlag = request.getParameter("dr_openFlag");
  String dr_previousValueId = request.getParameter("dr_previousValueId");
  String dr_resetFlag = request.getParameter("dr_resetFlag");
  String dr_resultId = request.getParameter("dr_resultId");
  String dr_selectedEntities = request.getParameter("dr_selectedEntities");
  String dr_previousLevel = request.getParameter("dr_previousLevel"); 
  
  boolean callDispEnabled = false;
  if (cdisp_enabledFlag != null && "Y".equalsIgnoreCase(cdisp_enabledFlag)) {
      callDispEnabled = true;
  }  
%>

<form id="comForm" name="comForm" action="<%=comReturnUrl%>" method="post">

<input type="hidden" name="action" value="customer_account_search">
<input type="hidden" name="recipient_flag" value="y">
<input type="hidden" name="return_from_joe" value="Y">      <!-- This is so COM knows we came from JOE -->
<input type="hidden" name="modify_order_in_joe" value="N">  <!-- This will be set if modify order was completed in JOE, so COM will know -->

<input type="hidden" name="in_order_number" value="<%=g_ext_ordnum%>">
<input type="hidden" name="order_number" value="<%=g_ext_ordnum%>">
<input type="hidden" name="orig_order_detail_id" value="<%=g_orig_order_detail_id%>">
<input type="hidden" name="securitytoken" value="<%=g_securitytoken%>">
<input type="hidden" name="context" value="<%=g_context%>">
<input type="hidden" name="adminAction" value="<%=g_adminAction%>">        
<input type="hidden" name="applicationcontext" value="<%=g_applicationcontext%>">        
<input type="hidden" name="call_cs_number" value="<%=g_call_cs_number%>">
<input type="hidden" name="call_type_flag" value="<%=g_call_type_flag%>">
<input type="hidden" name="call_brand_name" value="<%=g_call_brand_name%>">
<input type="hidden" name="call_dnis" value="<%=g_call_dnis%>">
<input type="hidden" name="start_origin" value="<%=g_start_origin%>">
<input type="hidden" name="customer_id" value="<%=g_customer_id%>">

<input type="hidden" name="t_comment_origin_type" value="<%=cl_t_comment_origin_type%>">
<input type="hidden" name="t_entity_history_id" value="<%=cl_t_entity_history_id%>">
<input type="hidden" name="t_call_log_id" value="<%=cl_t_call_log_id%>">

<input type="hidden" name="sc_cust_ind" value="<%=sc_cust_ind%>">
<input type="hidden" name="sc_tracking_number" value="<%=sc_tracking_number%>">
<input type="hidden" name="sc_cust_number" value="<%=sc_cust_number%>">
<input type="hidden" name="sc_phone_number" value="<%=sc_phone_number%>">
<input type="hidden" name="sc_rewards_number" value="<%=sc_rewards_number%>">
<input type="hidden" name="sc_pc_membership" value="<%=sc_pc_membership%>">
<input type="hidden" name="sc_recip_ind" value="<%=sc_recip_ind%>">
<input type="hidden" name="sc_order_number" value="<%=sc_order_number%>">
<input type="hidden" name="sc_email_address" value="<%=sc_email_address%>">
<input type="hidden" name="sc_zip_code" value="<%=sc_zip_code%>">
<input type="hidden" name="sc_last_name" value="<%=sc_last_name%>">

<input type="hidden" name="rtq" value="<%=q_rtq%>">
<input type="hidden" name="rtq_queue_type" value="<%=q_rtq_queue_type%>">
<input type="hidden" name="rtq_queue_ind" value="<%=q_rtq_queue_ind%>">
<input type="hidden" name="rtq_attached" value="<%=q_rtq_attached%>">
<input type="hidden" name="rtq_tagged" value="<%=q_rtq_tagged%>">
<input type="hidden" name="rtq_user" value="<%=q_rtq_user%>">
<input type="hidden" name="rtq_group" value="<%=q_rtq_group%>">
<input type="hidden" name="rtq_sort_column" value="<%=q_rtq_sort_column%>">
<input type="hidden" name="rtq_max_records" value="<%=q_rtq_max_records%>">
<input type="hidden" name="rtq_current_sort_direction" value="<%=q_rtq_current_sort_direction%>">
<input type="hidden" name="rtq_position" value="<%=q_rtq_position%>">
<input type="hidden" name="rtq_sort_on_return" value="<%=q_rtq_sort_on_return%>">
<input type="hidden" name="rtq_received_date" value="<%=q_rtq_received_date%>">
<input type="hidden" name="rtq_delivery_date" value="<%=q_rtq_delivery_date%>">
<input type="hidden" name="rtq_tag_date" value="<%=q_rtq_tag_date%>">
<input type="hidden" name="rtq_last_touched" value="<%=q_rtq_last_touched%>">
<input type="hidden" name="rtq_type" value="<%=q_rtq_type%>">
<input type="hidden" name="rtq_sys" value="<%=q_rtq_sys%>">
<input type="hidden" name="rtq_timezone" value="<%=q_rtq_timezone%>">
<input type="hidden" name="rtq_sdg" value="<%=q_rtq_sdg%>">
<input type="hidden" name="rtq_disposition" value="<%=q_rtq_disposition%>">
<input type="hidden" name="rtq_priority" value="<%=q_rtq_priority%>">
<input type="hidden" name="rtq_new_item" value="<%=q_rtq_new_item%>">
<input type="hidden" name="rtq_occasion" value="<%=q_rtq_occasion%>">
<input type="hidden" name="rtq_location" value="<%=q_rtq_location%>">
<input type="hidden" name="rtq_member_num" value="<%=q_rtq_member_num%>">
<input type="hidden" name="rtq_zip" value="<%=q_rtq_zip%>">
<input type="hidden" name="rtq_tag_by" value="<%=q_rtq_tag_by%>">
<input type="hidden" name="rtq_filter" value="<%=q_rtq_filter%>">

<input type="hidden" name="cdisp_autoOpenedFlag" value="<%=cdisp_autoOpenedFlag%>">
<input type="hidden" name="cdisp_reminderFlag" value="<%=cdisp_reminderFlag%>">
<input type="hidden" name="cdisp_cancelledOrder" value="<%=cdisp_cancelledOrder%>">
<input type="hidden" name="cdisp_enabledFlag" value="<%=cdisp_enabledFlag%>">
<input type="hidden" name="cdisp_resetFlag" value="<%=cdisp_resetFlag%>">
<input type="hidden" name="dr_availableEntities" value="<%=dr_availableEntities%>">
<input type="hidden" name="dr_currentLevel" value="<%=dr_currentLevel%>">
<input type="hidden" name="dr_currentLevelParentValueId" value="<%=dr_currentLevelParentValueId%>">
<input type="hidden" name="dr_currentLevelComment" value="<%=dr_currentLevelComment%>">
<input type="hidden" name="dr_currentLevelSelectedValueId" value="<%=dr_currentLevelSelectedValueId%>">
<input type="hidden" name="dr_decisionConfigId" value="<%=dr_decisionConfigId%>">
<input type="hidden" name="dr_decisionStatus" value="<%=dr_decisionStatus%>">
<input type="hidden" name="dr_decisionTypeCode" value="<%=dr_decisionTypeCode%>">
<input type="hidden" name="dr_dispositionStatus" value="<%=dr_dispositionStatus%>">
<input type="hidden" name="dr_dnisId" value="<%=dr_dnisId%>">
<input type="hidden" name="dr_openFlag" value="<%=dr_openFlag%>">
<input type="hidden" name="dr_previousValueId" value="<%=dr_previousValueId%>">
<input type="hidden" name="dr_resetFlag" value="<%=dr_resetFlag%>">
<input type="hidden" name="dr_resultId" value="<%=dr_resultId%>">
<input type="hidden" name="dr_selectedEntities" value="<%=dr_selectedEntities%>">
<input type="hidden" name="dr_previousLevel" value="<%=dr_previousLevel%>">

</form>

<script type="text/javascript">
function clearCallDispo() {
	document.forms['comForm'].cdisp_autoOpenedFlag.value = "";
    document.forms['comForm'].cdisp_reminderFlag.value = "";
    document.forms['comForm'].cdisp_cancelledOrder.value = "";
    document.forms['comForm'].cdisp_enabledFlag.value = "";
    document.forms['comForm'].cdisp_resetFlag.value = "";
    document.forms['comForm'].dr_availableEntities.value = "";
    document.forms['comForm'].dr_currentLevel.value = "";
    document.forms['comForm'].dr_currentLevelParentValueId.value = "";
    document.forms['comForm'].dr_currentLevelComment.value = "";
    document.forms['comForm'].dr_currentLevelSelectedValueId.value = "";
    document.forms['comForm'].dr_decisionConfigId.value = "";
    document.forms['comForm'].dr_decisionStatus.value = "";
    document.forms['comForm'].dr_decisionTypeCode.value = "";
    document.forms['comForm'].dr_dispositionStatus.value = "";
    document.forms['comForm'].dr_openFlag.value = "";
    document.forms['comForm'].dr_previousValueId.value = "";
    document.forms['comForm'].dr_resetFlag.value = "";
    document.forms['comForm'].dr_resultId.value = "";
    document.forms['comForm'].dr_selectedEntities.value = "";
    document.forms['comForm'].dr_previousLevel.value = "";
}

var v_modify_order_in_joe = false;
var v_modifyOrderFromCom = <%=moFromCom%>;
var v_modify_order_dnis = '<%=moDnis%>';
var v_orig_external_order_number = '<%=g_ext_ordnum%>';
var v_call_disp_enabled = <%=callDispEnabled%>;
</script>

