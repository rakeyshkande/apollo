<script type="text/javascript">
var v_textSearchType="zip";
var v_textSearchTargetElementId="productZip";
var v_textSearchId = null;
</script>
<div align="center" style="background-color:#eeeeee;padding:2px;">
    <table width="100%" border="0" bgcolor="#ffffff">
        <caption><span id="textSearchMainCaption">Search</span></caption>
        <!--<thead/>-->
        <tbody>
            <tr>
                <td>
                    <div id="tsCriteraDiv" style="background-color:#ffffff;height:90px;">
                        <div id="tsZipSearchCriteriaDiv">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="5px">
                                <tbody>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tszCityLabel" for="tszCity" class="labelRight">City</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tszCity" size="30" maxlength="30">
                                        </td>
                                        <td class="labelRight">
                                            <label id="tszStateLabel" for="tszState" class="labelRight">State</label>
                                        </td>
                                        <td>
                                            <select id="tszState"></select>
                                            <select style="display:none" id="tszCountry"></select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tsMemberShipSearchCriteriaDiv">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="5px">
                                <tbody>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tsmZipLabel" for="tsmZip" class="labelRight">Zip Code</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tsmZip" size="30" maxlength="30">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tsSourceSearchCriteriaDiv">
                            <table border="0" cellpadding="2px" width="620">
                                <tbody>
                                    <tr>
                                        <td style="text-align:right;" width="95">
                                            <label id="tssSearchTypeLabel" for="tssSearchType" class="labelRight">Type</label>
                                        </td>
                                        <td width="135">
                                            <select id="tssSearchType">
                                                <option value="SOURCE_CODE" selected="selected">Source Code</option>
                                                <option value="IOTW">Item of the Week</option>
                                            </select>
                                        </td>
                                        <td style="text-align:right;" width="75">
                                            <label id="tssKeywordLabel" for="tssKeyword" class="labelRight">Keyword</label>
                                        </td>
                                        <td width="315">
                                            <input type="text" id="tssKeyword" size="20" maxlength="50">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tsIOTWSearchCriteriaDiv">
                            <table border="0" cellpadding="2px" width="620">
                                <tbody>
                                    <tr>
                                        <td width="150" style="text-align:right;">
                                            <label id="tssIotwSourceCodeLabel" class="labelRight">IOTW Source Code</label>
                                        </td>
                                        <td width="90">
                                            <input type="text" id="tssIotwSourceCode" size="10" maxlength="10">
                                        </td>
                                        <td width="80" style="text-align:right;">
                                            <label id="tssProductIdLabel" class="labelRight">Product ID</label>
                                        </td>
                                        <td width="300">
                                            <input type="text" id="tssProductId" size="10" maxlength="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;">
                                            <label id="tssMasterSourceCodeLabel" class="labelRight">Master Source Code</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tssMasterSourceCode" size="10" maxlength="10">
                                        </td>
                                        <td style="text-align:right;">
                                            <label id="tssDiscountLabel" class="labelRight">Discount</label>
                                        </td>
                                        <td>
                                            <select id="tssDiscount"></select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </div>
                        <div id="tsPhoneSearchCriteriaDiv">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="5px">
                                <tbody>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tspPhoneLabel" for="tspPhone" class="labelRight">Phone</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tspPhone" size="10" maxlength="20">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tsFacilitySearchCriteriaDiv">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="5px">
                                <tbody>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tsfSearchTypeLabel" for="tsfSearchType" class="labelRight">Type</label>
                                        </td>
                                        <td>
                                            <select id="tsfSearchType" size="1">
                                                <option value="FUNERAL HOME" selected="selected">Funeral Home / Cemetery</option>
                                                <option value="HOSPITAL">Hospital</option>
                                                <option value="NURSING HOME">Nursing Home</option>
                                            </select>
                                        </td>
                                        <td class="labelRight">
                                            <label id="tsfKeywordLabel" for="tsfKeyword" class="labelRight">Keyword</label>
                                        </td>
                                        <td colspan="3">
                                            <input type="text" id="tsfKeyword" size="20" maxlength="50">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tsfCityLabel" for="tsfCity" class="labelRight">City</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tsfCity" size="30" maxlength="30">
                                        </td>
                                        <td class="labelRight">
                                            <label id="tsfStateLabel" for="tsfState" class="labelRight">State</label>
                                        </td>
                                        <td>
                                            <select id="tsfState"></select>
                                        </td>
                                        <td class="labelRight">
                                            <!--
                                            <label id="tsfCountryLabel" for="tsfCountry" class="labelRight">Country</label>
                                            -->
                                            &nbsp;
                                        </td>
                                        <td>
                                            <select id="tsfCountry" style="display:none"></select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tsFloristSearchCriteriaDiv" align="left">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="2px">
                                <tbody>
                                    <tr>
                                        <td class="labelRight">
                                            <label id="tsflZipLabel" for="tsflZip" class="labelRight">Zip&nbsp;Code</label>
                                        </td>
                                        <td>
                                            <input type="text" id="tsflZip" size="6" maxlength="6">&nbsp;<button type="button"
                                                   id="tsflZipSearch">
                                              <img src="images/magnify.jpg" 
                                                   alt="Florist zip search"
                                                 width="16px"
                                                height="16px">
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  colspan="2">
                                          <img src="images/primaryFlorist.gif" align="left" width="18px" height="18px"/>&nbsp;=&nbsp;Primary&nbsp;Florist&nbsp;assigned&nbsp;to&nbsp;source&nbsp;code&nbsp;and&nbsp;zip&nbsp;code
                                        </td>         
                                    </tr>
                                    <tr>
                                        <td  colspan="2">
                                          <img src="images/backupFlorist.gif" align="left"  width="18px" height="18px"/>&nbsp=&nbsp;Backup&nbsp;Florist&nbsp;assigned&nbsp;to&nbsp;source&nbsp;code&nbsp;and&nbsp;zip&nbsp;code
                                        </td>         
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </td>
                <td valign="top" align="left" width="100%">
                    <div id="tsControlsDiv" align="left">
                            <!--table width="100%" border="0"-->
                            <table border="0" cellpadding="5px">
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                <!--
                                    <td width="33%">
                                    
                                    </td>
                                -->
                                    <td width="30px" align="center" valign="middle">
                                        <button type="button"
                                               name="textSearchGoButton"
                                               id="textSearchGoButton">
                                          <img src="images/bullet_square_green.png"
                                               width="16px"
                                               height="16px"
                                               alt="Start search">
                                        </button>
                                        <button type="button"
                                               name="textSearchDisabledButton"
                                               id="textSearchDisabledButton"
                                               disabled="disabled"
                                               style="display:none">
                                          <img src="images/bullet_square_grey.png" 
                                               alt="Unable to stop search at this time"
                                               id="textSearchDisabledButtonImage"
                                               width="16px" 
                                               height="16px">
                                        </button>
                                        <button type="button"
                                               name="textSearchStopButton"
                                               id="textSearchStopButton"
                                               style="display:none">
                                          <img src="images/stop.png" 
                                               alt="Stop search"
                                               id="textSearchStopButtonImage"
                                               width="16px" 
                                               height="16px">
                                        </button>
                                      </td>
                                      <td width="30px" align="left">
                                            <img src="images/animatedProgress.gif"
                                                   id="textSearchProcessing"
                                                   alt="Processing"
                                                   style="visibility:hidden;">
                                    </td>
                                    <td width="33%">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button" id="tsCloseButton" style="width:90px">Back to Order</button>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="background-color:#ffffff;border:none">
        <div id="tsNoResults" style="background-color:#cc0033;color:white;">
            <b>No results found for the search criteria</b>
        </div>
        <table style="width:100%">
            <caption>Search Results</caption>
            <tbody>
                <tr>
                    <td height="429px%" valign="top">
                        <div>
                            <div id="tszResultsDiv">
                                <table id='tszTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:15 table-page-number:tszpage table-page-count:tszpages">
                                    <thead>
                                        <tr>
                                            <th width="50px">
                                                Select
                                            </th>
                                            <th class="table-sortable:default">
                                                City
                                            </th>
                                            <th class="table-sortable:default">
                                                State
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Zip
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tszResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="2" style="text-align:center;">Page <span id="tszpage"></span>&nbsp;of <span id="tszpages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                            <div id="tsmResultsDiv">
                                <table id='tsmTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:15 table-page-number:tsmpage table-page-count:tsmpages">
                                    <thead>
                                        <tr>
                                            <th width="50px">
                                                Select
                                            </th>
                                            <th class="table-sortable:default" colspan="2">
                                                Club Name
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tsmResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td style="text-align:center;">Page <span id="tsmpage"></span>&nbsp;of <span id="tsmpages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        
                            <div id="tssSourceCodeResultsDiv">
                                <table id="tssTable" width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:15 table-page-number:tsspage table-page-count:tsspages">
                                    <thead>
                                        <tr>
                                            <th width="50px">
                                                Select
                                            </th>
                                            <th width="75px" class="table-sortable:default">
                                                Source Code
                                            </th>
                                            <th class="table-sortable:default">
                                                Description
                                            </th>
                                            <th width="50px" class="table-sortable:default">
                                                Type
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tssSourceCodeResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="2" style="text-align:center;">Page <span id="tsspage"></span>&nbsp;of <span id="tsspages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div id="tssIOTWResultsDiv">
                                <table id='tsiTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:10 table-page-number:tsipage table-page-count:tsipages">
                                    <thead>
                                        <tr>
                                            <th width="25px">
                                                Select
                                            </th>
                                            <th width="40px" class="table-sortable:default">
                                                Master Source Code
                                            </th>
                                            <th width="100px" class="table-sortable:default">
                                                Master Source Code Description
                                            </th>
                                            <th width="100px" class="table-sortable:default">
                                                IOTW Source Code Description
                                            </th>
                                            <th width="40px" class="table-sortable:default">
                                                Product ID
                                            </th>
                                            <th width="100px" class="table-sortable:default">
                                                Product Name
                                            </th>
                                            <th width="40px" class="table-sortable:default">
                                                Standard Price
                                            </th>
                                            <th width="40px" class="table-sortable:default">
                                                Discount
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tssIOTWResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="6" style="text-align:center;">Page <span id="tsipage"></span>&nbsp;of <span id="tsipages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div id="tspResultsDiv">
                                <table id='tspTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:12 table-page-number:tsppage table-page-count:tsppages">
                                    <thead>
                                        <tr>
                                            <th width="50px">
                                                Select
                                            </th>
                                            <th width="75px" class="table-sortable:default">
                                                Phone 1
                                            </th>
                                            <th width="75px" class="table-sortable:default">
                                                Phone 2
                                            </th>
                                            <th class="table-sortable:default">
                                                Name
                                            </th>
                                            <th class="table-sortable:default">
                                                Business Name
                                            </th>
                                            <th class="table-sortable:default">
                                                Address
                                            </th>
                                            <th class="table-sortable:default">
                                                City
                                            </th>
                                            <th width="50px" class="table-sortable:default">
                                                State
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tspResultsBody">
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="6" style="text-align:center;">Page <span id="tsppage"></span>&nbsp;of <span id="tsppages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div id="tsfResultsDiv">
                                <table id='tsfTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:11 table-page-number:tsfpage table-page-count:tsfpages">
                                    <thead>
                                        <tr>
                                            <th width="50px">
                                                Select
                                            </th>
                                            <th class="table-sortable:default">
                                                Institution
                                            </th>
                                            <th class="table-sortable:default">
                                                Address
                                            </th>
                                            <th class="table-sortable:default">
                                                City
                                            </th>
                                            <th width="50px" class="table-sortable:default">
                                                State
                                            </th>
                                            <th width="50px" class="table-sortable:default">
                                                Zip
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Phone
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tsfResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="5" style="text-align:center;">Page <span id="tsfpage"></span>&nbsp;of <span id="tsfpages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div id="tsflResultsDiv">
                                <table id='tsflTable' width="100%" class="sorttable sort01 table-autosort table-stripeclass:alternate table-autopage:13 table-page-number:tsflpage table-page-count:tsflpages">
                                    <thead>
                                        <tr>
                                            <th width="40px">
                                                Select
                                            </th>
                                            <th class="table-sortable:numeric" width="40px">
                                                Weight
                                            </th>
                                            <th width="20px">
                                                &nbsp;
                                            </th>
                                            <th class="table-sortable:default" width="60px">
                                                Florist ID
                                            </th>
                                            <th class="table-sortable:default">
                                                Name/Address
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Phone
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Goto Flag
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Sunday Delivery
                                            </th>
                                            <th class="table-sortable:default" width="50px">
                                                Mercury Flag
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tsflResultsBody">
                                        <tr><td></td></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="table-page:previous" style="cursor:pointer;"><div align="center"><img alt="Previous page of search results" src="images/navigate_left.png" width="16px" height="16px"></div></td>
                                            <td colspan="7" style="text-align:center;">Page <span id="tsflpage"></span>&nbsp;of <span id="tsflpages"></span></td>
                                            <td class="table-page:next" style="cursor:pointer;"><div align="center"><img alt="Next page of search results" src="images/navigate_right.png" width="16px" height="16px"></div></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="background-color:#ffffff;border:none" width="100%">
    	<tr>
	    	<td>
	    		<button type="button" id="tsBottomCloseButton">Back to Order</button>
	    	</td>
    	</tr>
    </table>
    
</div>