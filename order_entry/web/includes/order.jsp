<div id="tabDivId">
    <div id="orderTabberId" class="tabber">
        <div id="productTabId" class="tabbertab" title="#1 - Product">
            <table width="100%">
              <!--caption/-->
              <!--<thead/>-->
              <tbody>
                <tr>
                  <td class="labelRight" width="125"><label class="labelRight" 
                                                id="productIdLabel" 
                                                for="productId">Product ID</label></td>
                  <td>
                    <input type="text"
                           size="12"
                           maxlength="10"
                           id="productId">
                    &nbsp;&nbsp;
                    <button type="button"
                           id="productSearch">
                      <img src="images/magnify.jpg" 
                           alt="Product search" style="vertical-align: middle;">
                    </button>
                    &nbsp;&nbsp;
                                     
                     <img src="images/blank.gif"
                             alt="images/blank.gif"
                             title=" "
                             name="flexFill"
                             id="flexFill"
                             width="48px"
                             height="23px"
                             style="position: relative ; vertical-align: middle;"
                             >
                    <input type="hidden" id="productSourceCode" value="">
                    <input type="hidden" id="discountType" value="">
                    <!--
                    <input type="checkbox" id="productGenerallyAvailable" style="display:none">
                    <input type="checkbox" id="productAvailable" style="display:none">
                    <input type="checkbox" id="surchargeWarningGiven" style="display:none">
                    -->
                  </td>
                  <td class="labelRight" width="100">
                    <label class="labelRight" 
                           for="productOccasion"
                           id="productOccasionLabel">Occasion</label>
                  </td>
                  <td>
                    <select size="1"
                            id="productOccasion">
                    </select>
                  </td>
                  <td rowspan="4" valign="top" align="center">
                    <div align="center">
                        <img src="images/noproduct.gif"
                             alt="Please select a product"
                             title="Please select a product"
                             name="productImage"
                             id="productImage"
                             width="75px"
                             height="75px"
                             onerror="setDefaultImage(this);">
                    </div>
                    <div id="productIOTWDescription" style="text-align:center;font-weight:bold;color:#cc0033;"></div>
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight"
                           id="productZipLabel"
                           for="productZip">Zip&nbsp;Code</label>
                  </td>
                  <td>
                    <input type="text"
                           size="6"
                           id="productZip">
                    &nbsp;&nbsp;
                    <button type="button"
                           id="productZipSearch">
                      <img src="images/magnify.jpg" 
                           id="productZipSearchImage"
                           alt="Zip/Postal code search"
                           style="vertical-align:middle">
                    </button>
                  </td>
                  <td class="labelRight">
                    <label class="labelRight" 
                              id="productCountryLabel"
                              for="productCountry">Country</label>
                  </td>
                  <td>
                    <select size="1"
                            id="productCountry">
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight" 
                           id="productDeliveryDateLabel" 
                           for="productDeliveryDate">Delivery Date</label>
                  </td>
                  <td>
                    <select size="1"
                            id="productDeliveryDate">
                    </select>
                    &nbsp;&nbsp;
                    <button type="button"
                            id="productCalendarButton">
                      <img alt="Calendar"
                           src="images/calendar.gif" style="vertical-align:middle">
                    </button>
                    <div id="productDeliveryDateCalContainer" class="yui-skin-sam"></div>
                    <button type="button"
                           id="forceAvailabilityButton">
                      <img src="images/check.gif" 
                           alt="Force a product availability check" style="vertical-align:middle">
                    </button>
                  </td>
                    <td class="labelRight">
                    <label class="labelRight" 
                              id="languageIdLabel"
                              for="languageId">Language</label>
                  </td>
                  <td>
                    <select size="1"
                            id="languageId">
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight"
                           id="productDeliveryMethodLabel"
                           for="productDeliveryMethod">Delivery Method</label>
                  </td>
                  <td style="vertical-align:middle">
                    &nbsp;&nbsp;
                    <label>Florist</label>&nbsp;<input type="radio" name="productDeliveryMethod" id="productDeliveryMethodFlorist" value="florist" style="vertical-align:middle"/>
                    Dropship&nbsp;<input type="radio" name="productDeliveryMethod" id="productDeliveryMethodDropship" value="dropship" style="vertical-align:middle"/>
                  </td>
                    <td colspan="2">
                        <div id="productExceptionDates" style="text-align:left;font-weight:bold;color:#cc0033;"></div>
                    </td>
                </tr>
                <tr> 
	                  <td class="labelRight">
	                    <label class="labelRight"
	                           id="recipientTypeLabel"
	                           for="recipientType">Location Type</label></td>
	                  <td>
	                    <select size="1"
	                            id="recipientType">
	                    </select>
	                  </td>
	                  
	                  				  <td class="labelRight">
                    <label id="recipientHoursFromLabel" for="recipientHoursFrom" class="labelRight">Time of Service/Working Hours</label>
                  </td>
                  <td colspan="3">
                    <div id="recipientHoursId">
                      <table>
                        <!--caption/-->
                        <!--<thead/>-->
                        <tbody>
                            <tr>
                              <td>
                              <input type="hidden" id="timeOfService">
                              <input type="hidden" id="timeOfServiceFlag">
                                <select size="1" 
                                        id="recipientHoursFrom">
                                  <option value="" selected="selected">&nbsp;</option>
                                  <option value="900">
                                    9:00 AM
                                  </option>
                                  <option value="1000">
                                    10:00 AM
                                  </option>
                                  <option value="1100">
                                    11:00 AM
                                  </option>
                                  <option value="1200">
                                    Noon
                                  </option>
                                  <option value="1300">
                                    1:00 PM
                                  </option>
                                  <option value="1400">
                                    2:00 PM
                                  </option>
                                  <option value="1500">
                                    3:00 PM
                                  </option>
                                  <option value="1600">
                                    4:00 PM
                                  </option>
                                  <option value="1700">
                                    5:00 PM
                                  </option>
                                  <option value="1800">
                                    6:00 PM
                                  </option>
                                  <option value="1900">
                                    7:00 PM
                                  </option>
                                  <option value="2000">
                                    8:00 PM
                                  </option>
                                </select>
                                &nbsp;&nbsp;<label id="recipientHoursToLabel" for="recipientHoursTo" class="labelRight">to</label>&nbsp;&nbsp;
                                <input type="hidden" id="timeOfServiceEnd">
                                <select size="1" 
                                        id="recipientHoursTo">
                                  <option value="" selected="selected">&nbsp;</option>
                                  <option value="900">
                                    9:00 AM
                                  </option>
                                  <option value="1000">
                                    10:00 AM
                                  </option>
                                  <option value="1100">
                                    11:00 AM
                                  </option>
                                  <option value="1200" selected="selected">
                                    Noon
                                  </option>
                                  <option value="1300">
                                    1:00 PM
                                  </option>
                                  <option value="1400">
                                    2:00 PM
                                  </option>
                                  <option value="1500">
                                    3:00 PM
                                  </option>
                                  <option value="1600">
                                    4:00 PM
                                  </option>
                                  <option value="1700">
                                    5:00 PM
                                  </option>
                                  <option value="1800">
                                    6:00 PM
                                  </option>
                                  <option value="1900">
                                    7:00 PM
                                  </option>
                                  <option value="2000">
                                    8:00 PM
                                  </option>
                                </select>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                  </tr>
              </tbody>
            </table>
            <br>
            <div id="productOptionsDiv" style="display:none;">
            <table width="100%" border="1" bgcolor="#eeeeee">
              <caption class="orderCaption">Product Options</caption>
              <!--<thead/>-->
              <tbody>
                <tr>
                  <td width="250px" valign="top">
                    <div id="productOptionStandardRow">
                        <table bgcolor="#eeeeee">
                          <!--caption/-->
                          <!--<thead/>-->
                          <tbody>
                            <tr>
                              <td>
                                <input type="checkbox" name="productOption" id="productOptionStandard" class="greyElement">
                              </td>
                              <td class="radioLabel" style="white-space:nowrap;">
                                <label id="productOptionStandardLabel" for="productOptionStandard">$<span id="standardPrice"></span><span id="standardDiscountCell">&nbsp;(<span id="standardDiscount"></span>)&nbsp;</span>-&nbsp;<span id="standardLabel">Shown</span></label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionDeluxeRow">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                  <td>
                                    <input type="checkbox" name="productOption" id="productOptionDeluxe" class="greyElement">
                                  </td>
                                  <td class="radioLabel" style="white-space:nowrap;">
                                    <label id="productOptionDeluxeLabel" for="productOptionDeluxe">$<span id="deluxePrice"></span><span id="deluxeDiscountCell">&nbsp;(<span id="deluxeDiscount"></span>)&nbsp;</span>-&nbsp;<span id="deluxeLabel">Deluxe</span></label>
                                  </td>
                                </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionPremiumRow">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                  <td>
                                    <input type="checkbox" name="productOption" id="productOptionPremium" class="greyElement">
                                  </td>
                                  <td class="radioLabel" style="white-space:nowrap;">
                                    <label id="productOptionPremiumLabel" for="productOptionPremium">$<span id="premiumPrice"></span><span id="premiumDiscountCell">&nbsp;(<span id="premiumDiscount"></span>)&nbsp;</span>-&nbsp;<span id="premiumLabel">Premium</span></label>
                                  </td>
                                </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionVariableRow">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                  <td>
                                    <input type="checkbox" name="productOption" id="productOptionVariable" class="greyElement">
                                  </td>
                                  <td class="radioLabel">
                                    <label id="productOptionVariableLabel" for="productOptionVariable">Variable Price</label>
                                  </td>
                                  <td width="75px">
                                    <input type="text"
                                           size="9"
                                           id="productOptionVariablePrice"
                                           disabled="disabled">
                                  </td>
                                </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionSubcodesRow" style="display:none;">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                            <tr>
                              <td class="labelRight">
                                <label class="labelRight"
                                       id="productOptionSubCodeLabel"
                                       for="productOptionSubCode">Option</label>
                              </td>
                              <td colspan="2">
                                <select size="1"
                                          id="productOptionSubCode">
                                </select>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionUpsellRow" style="display:none;">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                            <tr>
                              <td class="labelRight">
                                <label class="labelRight"
                                       id="productOptionUpsellLabel"
                                       for="productOptionUpsell">Option</label>
                              </td>
                              <td colspan="2">
                                <select size="1"
                                          id="productOptionUpsell">
                                </select>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div id="productOptionColorRow" style="display:none;" >
                        <br>
                        <table>
                            <tbody>
                            <tr>
                              <td class="labelRight">
                                <label class="labelRight"
                                       id="productOptionColorLabel"
                                       for="productOptionColor">First&nbsp;Color&nbsp;Choice</label>
                              </td>
                              <td>
                                    <select size="1" id="productOptionColor"></select>
                              </td>
                            </tr>
                            <tr>
                              <td class="labelRight">
                                <label class="labelRight"
                                       id="productOptionColor2Label"
                                       for="productOptionColor2">Second&nbsp;Color&nbsp;Choice</label>
                              </td>
                              <td>
                                    <select size="1" id="productOptionColor2"></select>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </td>
                  <td valign="top">
                    <div id="addOnDiv" style="display:none;">
                    <table width="100%">
                      <!--caption/-->
                      <!--<thead/>-->
                      <tbody>
                       <tr id="addOnVaseRow" style="display:none;">
                         <td colspan="7" width="100%">
                           <select size="1" id="productAddonVase" style="width:100%">
                           </select>
                         </td>
                       </tr>
                       <tr id="addOnRow1" style="display:none;">
                         <td>
                           <input type="checkbox" id="addOnCheckbox1" class="greyElement">
                           <input type="hidden" id="addOnId1">
                         </td>
                         <td class="radioLabel"><label class="radioLabel" id="addOnCheckboxLabel1">Label 1</label></td>
                         <td width="5px">&nbsp;</td>
                         <td width="150px">&nbsp;</td>
                         <td class="price"><span id="addOnPrice1"></span></td>
                         <td width="5px"><label id="addOnQtyLabel1" for="addOnQty1"/></td>
                         <td><select size="1" id="addOnQty1" disabled="true"/></td>
                       </tr>
                       <tr id="addOnRow2" style="display:none;">
                         <td>
                             <input type="checkbox" id="addOnCheckbox2" class="greyElement">
                             <input type="hidden" id="addOnId2">
                         </td>
                         <td class="radioLabel"><label class="radioLabel" id="addOnCheckboxLabel2">Label 2</label></td>
                         <td width="5px">&nbsp;</td>
                         <td width="150px">&nbsp;</td>
                         <td class="price"><span id="addOnPrice2"></span></td>
                         <td width="5px"><label id="addOnQtyLabel2" for="addOnQty2"/></td>
                         <td><select size="1" id="addOnQty2" disabled="true"/></td>
                       </tr>
                       <tr id="addOnRow3" style="display:none;">
                         <td>
                             <input type="checkbox" id="addOnCheckbox3" class="greyElement">
                             <input type="hidden" id="addOnId3">
                         </td>
                         <td class="radioLabel"><label class="radioLabel" id="addOnCheckboxLabel3">Label 3</label></td>
                         <td width="5px">&nbsp;</td>
                         <td width="150px">&nbsp;</td>
                         <td class="price"><span id="addOnPrice3"></span></td>
                         <td width="5px"><label id="addOnQtyLabel3" for="addOnQty3"/></td>
                         <td><select size="1" id="addOnQty3" disabled="true"/></td>
                       </tr>
                       <tr id="addOnRow4" style="display:none;">
                         <td>
                             <input type="checkbox" id="addOnCheckbox4" class="greyElement">
                             <input type="hidden" id="addOnId4">
                         </td>
                         <td class="radioLabel"><label class="radioLabel" id="addOnCheckboxLabel4">Label 4</label></td>
                         <td width="5px">&nbsp;</td>
                         <td width="150px">&nbsp;</td>
                         <td class="price"><span id="addOnPrice4"></span></td>
                         <td width="5px"><label id="addOnQtyLabel4" for="addOnQty4"/></td>
                         <td><select size="1" id="addOnQty4" disabled="true"/></td>
                       </tr>
                       <tr id="addOnFRow">
                         <td>
                           <input type="checkbox"
                                  id="addOnFCheckbox"
                                  class="greyElement">
                         </td>
                         <td class="radioLabel">
                           <label class="radioLabel" 
                                    id="addOnFCheckboxLabel" 
                                    for="addOnFCheckbox">Addon F not configured</label>
                         </td>
                         <td width="5px">
                            <label id="addOnFBannerTextLabel" for="addOnFBannerText"></label>
                         </td>
                         <td>
                            <input type="text"
                                   id="addOnFBannerText"
                                   size="25"
                                   title="banner text - maximum length of 30 characters"
                                   disabled="disabled">
                         </td>
                         <td class="price"><span id="addOnFPrice">$.01</span></td>
                         <td width="5px"></td>
                         <td>&nbsp;</td>
                       </tr>
                      </tbody>
                    </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <br>
            <div id="deliveryOptionsDiv" style="display:none">
            <table width="100%" border="1" bgcolor="#eeeeee">
              <caption class="orderCaption">Delivery Options</caption>
              <!--<thead/>-->
              <tbody>
                <tr>
                  <td>
                    <table>
                      <!--caption/-->
                      <!--<thead/>-->
                      <tbody>
                         <tr id="carrierDeliveryOptGRRow">
                            <td width="25px">
                                <input type="checkbox" 
                                       name="carrierDeliverOption" 
                                       id="carrierDeliveryOptGR" 
                                       class="greyElement">
                            </td>
                            <td>
                                <label class="radioLabel"
                                       id="carrierDeliveryOptGRLabel"
                                       for="carrierDeliveryOptGR">Standard Delivery</label>
                            <td align="right"></td>
                            <td class="labelRight"> Arrives no later than &nbsp;</td>
                            <td><select id="carrierDeliveryDateGR">
                               </select>
                            </td>
                         </tr>
                         <tr id="carrierDeliveryOpt2FRow">
                            <td width="25px">
                              <input type="checkbox" 
                                     name="carrierDeliverOption" 
                                     id="carrierDeliveryOpt2F"
                                     class="greyElement">
                            </td>
                            <td>
                                <label class="radioLabel"
                                       id="carrierDeliveryOpt2FLabel"
                                       for="carrierDeliveryOpt2F">Two Day Delivery</label>
                            <td align="right"></td>
                            <td class="labelRight"> Arrives no later than &nbsp;</td>
                            <td><select id="carrierDeliveryDate2F">
                               </select>
                            </td>
                         </tr>
                        <tr id="carrierDeliveryOptNDRow">
                            <td width="25px">
                              <input type="checkbox"
                                     id="carrierDeliveryOptND"
                                     name="carrierDeliverOption" 
                                     class="greyElement">
                            </td>
                            <td>
                                <label class="radioLabel"
                                       id="carrierDeliveryOptNDLabel"
                                       for="carrierDeliveryOptND">Next Day Delivery</label>
                            </td>
                            <td align="right"></td>
                            <td class="labelRight"> Will be delivered on &nbsp;&nbsp;</td>
                            <td><select id="carrierDeliveryDateND">
                               </select>
                            </td>
                         </tr>
                         <tr id="carrierDeliveryOptSARow">
                            <td width="25px">
                              <input type="checkbox" 
                                     name="carrierDeliverOption" 
                                     id="carrierDeliveryOptSA"
                                     class="greyElement">
                            </td>
                            <td>
                                <label class="radioLabel"
                                        id="carrierDeliveryOptSALabel"
                                        for="carrierDeliveryOptSA">Saturday Delivery</label>
                            </td>
                            <td align="right"></td>
                            <td class="labelRight"> Will be delivered on &nbsp;&nbsp;</td>
                            <td>
                               <select id="carrierDeliveryDateSA">
                               </select>
                            </td>
                         </tr>
                         
                         <tr id="carrierDeliveryOptSURow">
                            <td width="25px">
                              <input type="checkbox" 
                                     name="carrierDeliverOption" 
                                     id="carrierDeliveryOptSU"
                                     class="greyElement">
                            </td>
                            <td>
                                <label class="radioLabel"
                                        id="carrierDeliveryOptSULabel"
                                        for="carrierDeliveryOptSU">Sunday Delivery</label>
                            </td>
                            <td align="right"></td>
                            <td class="labelRight"> Will be delivered on &nbsp;&nbsp;</td>
                            <td>
                               <select id="carrierDeliveryDateSU">
                               </select>
                            </td>
                         </tr>
                         
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
        
          </div>
            <div id="floristOptionsDiv" style="display:none">
              <table width="100%" border="1" bgcolor="#eeeeee">
                <caption class="orderCaption">Options</caption>
                  <!--<thead/>-->
                  <tbody>
                    <tr>
                      <td>
                        <table>
                           <tbody>
                             <tr>
                               <td class="labelRight" style="vertical-align:top" width="150">
                                 <label class="labelRight"
                                         id="floristCommentsLabel"
                                         for="floristComments">Special Instructions</label>
                               </td>
                               <td rowspan="3" width="300">
                                 <textarea id="floristComments" 
                                            cols="50" 
                                            rows="5"
                                            class="joe"
                                            title="These comments will be sent to the filling florist"
                                            accesskey="m"></textarea> 
                               </td>
                               <td width="300">
                                 <div id="floristIdDiv">
                                   <table width="100%">
                                     <tbody>
                                       <tr>
                                         <td style="vertical-align:middle;text-align:right;" width="100">
                                           <label class="labelRight"
                                               id="floristIdLabel"
                                               for="floristId">Florist ID</label>
                                         </td>
                                         <td style="vertical-align:middle" width="200">
                                           <input type="text" 
                                               size="9"
                                               id="floristId">
                                           &nbsp;&nbsp;
                                           <button type="button"
                                               id="floristSearch">
                                             <img src="images/magnify.jpg" 
                                               alt="Florist search" style="vertical-align:middle">
                                           </button>
                                         </td>
                                       </tr>
                                     </tbody>
                                   </table>
                                 </div>
                               </td>
                             </tr>
                             <tr>
                               <td>&nbsp;</td>
                               <td align="left" style="text-align:center">
                                 <span id="floristDesc" style="color:#888888"></span>
                               </td>
                             </tr>
                             <tr>
                               <td class="labelRight" valign="top">
                                 <span id="floristCommentsCount"></span>
                               </td>
                               <td>&nbsp;</td>
                             </tr>
                             <tr>
                                <td>&nbsp;</td>
                                <td colspan="3"><label id="specialInstructionsLabel">These comments will be sent to the filling florist</label></td>
                             </tr>
                            </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <img id="tab1bottom" src="images/1px_image.gif" width="1px" height="1px" alt="">
        </div>
       
        <div class="tabbertab" title="#2 - Card">
            <img id="tab2top" src="images/1px_image.gif" width="1px" height="1px" alt="">
            <table width="100%">
              <!--caption/-->
              <!--<thead/>-->
              <tbody>
                <tr>
                  <td class="labelRight" valign="top">
                    <label class="labelRight"
                           id="cardTypeListLabel"
                           for="cardTypeList"
                           title="Add a greeting to the card message">Greeting</label>
                  </td>
                  <td valign="top">
                    <select size="6" 
                            id="cardTypeList" 
                            title="Add a greeting to the card message">
                    </select>
                    &nbsp;&nbsp;
                    <button type="button" id="cardTypeButton">
                      <img id="cardTypeButtonImg"
                           src="images/arrow_down_green.png"
                           alt="Move text to the card message"
                           width="16"
                           height="16">
                    </button>
                  </td>
                </tr>
                <tr>
                  <td class="labelRight" valign="top">
                    <label class="labelRight"
                           id="cardMessageLabel"
                           for="cardMessage">Message</label>
                  </td>
                  <td valign="top" rowspan="2">
                    <textarea name="cardMessage"
                              cols="45" rows="5"
                              class="joe"
                              id="cardMessage"></textarea>
                    &nbsp;&nbsp;
                    <button type="button" id="cardSpellCheckButton">
                      <img id="cardSpellCheckButtonImg"
                           src="images/spellcheck.png"
                           alt="Spell check the card message"
                           width="16"
                           height="16">
                    </button>
                  </td>
                </tr>
                <tr>
                    <td class="labelRight" valign="top">
                        <span id="cardMessageCount"></span>
                    </td>
                </tr>
                <tr>
                 <td>&nbsp;</td>
                 <td>
                    <table id="addOnCardsTable">
                        <tbody>
                            <tr>
                                <td width="320px">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox"
                                                           id="addOnCardCheckbox"
                                                           class="whiteElement">
                                                </td>
                                                <td>
                                                    <label class="radioLabel" 
                                                          id="addOnCardCheckboxLabel" 
                                                          for="addOnCardCheckbox">Full-Size Greeting&nbsp;Card</label>
                                                </td>
                                                <td width="5px">
                                                    <label id="addOnCardSelectLabel" for="addOnCardSelect"></label>
                                                </td>
                                                <td>
                                                    <select size="1"
                                                            id="addOnCardSelect">
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td rowspan="2" valign="top">
                                    <img id="addOnCardImage" 
                                        src="images/npi_a.jpg" 
                                        alt="card image"
                                        style="width:250px;height:220px;"
                                        onerror="setDefaultImage(this);">
                                </td>
                            </tr>
                            <tr id="addOnCardDetailRow" valign="top">
                                <td valign="top" width="320px">
                                    <table border="1" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="labelRight" valign="top">
                                                    <label class="labelRight">Price</label>
                                                </td>
                                                <td>
                                                    <span id="addOnCardPrice"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="labelRight" valign="top">
                                                    <label class="labelRight">Front&nbsp;Text</label>
                                                </td>
                                                <td>
                                                    <span id="addOnCardFrontText"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="labelRight" valign="top">
                                                    <label class="labelRight">Inside&nbsp;Text</label>
                                                </td>
                                                <td>
                                                    <span id="addOnCardInsideText"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <img id="tab2bottom" src="images/1px_image.gif" width="1px" height="1px" alt="">
        </div>
    
        <div class="tabbertab" title="#3 - Recipient">  
            <img id="tab3top" src="images/1px_image.gif" width="1px" height="1px" alt="">
            <table width="100%">
              <!--caption/-->
              <!--<thead/>-->
              <tbody>
                <tr> 
                  <td class="labelRight">
                    &nbsp;</td>
                  <td>
                   &nbsp;
                  </td>
                  <td class="labelRight">
                    <label class="labelRight"
                            id="recipientPhoneLabel"
                            for="recipientPhone">Phone Number</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientPhone"
                           size="10"
                           title="Recipient phone">
                    &nbsp;&nbsp;
                    <label id="recipientPhoneExtLabel" 
                        class="labelRight" 
                          for="recipientPhoneExt">Ext</label>
                    &nbsp;
                    <input type="text"
                           id="recipientPhoneExt"
                           size="4"
                           title="Phone extension">
                    &nbsp;&nbsp;
                    <button type="button"
                           id="recipientPhoneSearch">
                      <img src="images/magnify.jpg" 
                           id="recipientPhoneSearchImage"
                           alt="Recipient phone search">
                    </button>
                    <input type="checkbox" id="recipientPhoneChecked" style="display:none">
                  </td>
                </tr>
                <tr>
                  <td height="25" class="labelRight">
                    <label id="recipientBusinessLabel" for="recipientBusiness" class="labelRight">Business/Institution</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientBusiness"
                           size="20">
                    &nbsp;&nbsp;
                    <button type="button"
                           id="recipientFacilitySearch"
                           title="Search">
                      <img src="images/magnify.jpg" 
                           alt="Search">
                    </button>
                  </td>
                  <td class="labelRight">
                    <label id="recipientRoomLabel" for="recipientRoom" class="labelRight">Room/Ward</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientRoom"
                           size="20">
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight"
                           id="recipientFirstNameLabel"
                           for="recipientFirstName">First Name</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientFirstName"
                           size="25">
                  </td>
                  <td class="labelRight">
                    <label class="labelRight"
                           id="recipientLastNameLabel"
                           for="recipientLastName">Last Name</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientLastName"
                           size="25">
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight" id="recipientAddressLabel" for="recipientAddress">Address</label>
                  </td>
                  <td valign="top" colspan="3" rowspan="2">
                    <textarea cols="45" 
                              class="joe"
                              rows="2"
                              id="recipientAddress"></textarea>
                  </td>
                </tr>
                <tr>
                    <td class="labelRight" valign="top">
                        <span id="recipientAddressCount"></span>
                    </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label id="recipientZipLabel" for="recipientZip" class="labelRight">Zip Code</label>
                  </td>
                  <td>
                    <input type="text" 
                           size="6"
                           id="recipientZip">
                    &nbsp;&nbsp;
                    <button type="button"
                           id="recipientZipSearch">
                      <img src="images/magnify.jpg" 
                            id="recipientZipSearchImage"
                           alt="Recipient zip search">
                    </button>
                  </td>
                  <td class="labelRight">
                    <label class="labelRight" id="recipientCountryLabel" for="recipientCountry">Country</label>
                  </td>
                  <td>
                    <select size="1"
                            id="recipientCountry">
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="labelRight">
                    <label class="labelRight" id="recipientCityLabel" for="recipientCity">City</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="recipientCity"
                           size="30">
                  </td>
                  <td class="labelRight">
                    <label id="recipientStateLabel" class="labelRight">State</label>
                  </td>
                  <td>
                      <select size="1" 
                              id="recipientState">
                      </select>
                  </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
                 <tr>
                    <td>
                        <input type="hidden" id="recipientEmailAddress" value="">
                        <input type="hidden" id="recipientOriginalAddress1" value="">
                        <input type="hidden" id="recipientAddress2" value="">
                        <input type="hidden" id="recipientOptIn" value="">
                    </td>
                 </tr>
                 <tr>
                     <td class="labelRight" valign="top">
                       <label id="orderCommentsLabel" for="orderComments" class="labelRight">Order&nbsp;Comments</label>
                     </td>
                     <td colspan="3" rowspan="2" valign="top">
                       <textarea id="orderComments" class="joe" cols="50" rows="5"></textarea> 
                     </td>
                 </tr>
                <tr>
                    <td class="labelRight" valign="top">
                        <span id="orderCommentsCount"></span>
                    </td>
                </tr>
              </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <div class="scriptDiv">
                <table width="100%">
                    <!--<thead/>-->
                    <tbody>
                      <tr>
                        <td width="100%" class="script">Ask the customer if they would like to order another product.</td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <img id="tab3bottom" src="images/1px_image.gif" width="1px" height="1px" alt="">
          </div>
    
        <div id="errorTabId" class="tabbertab" title="Validation Errors">
            <table style="border: thin inset #eeeeee;padding:5px;background-color:#cc0033;">
                <tr>
                    <td>
                        <div id='validationErrorDiv' style="width:715px;height:430px;overflow:auto;background-color:white;padding:5px;">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div id="addressVerificationDivId" style="display:none;padding:2px">
    <div id="innerAddressVerificationDivId">
        <table width="100%" align="center">
            <caption>Address Verification</caption>
            <!--<thead/>-->
            <tbody>
                <tr>
                    <td align="center">
                        <table border="1" class="addressVerificationChoice">
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                    <td valign="top">
                                    <div>
                                    	<div class="orderCaption">Entered&nbsp;Address</div>
                                        <table class="address" >
                                            
                                            <!--<thead/>-->
                                            <tbody>
                                                <tr>
                                                    <td><b>Address</b></td>
                                                    <td><span id="avEnteredAddress"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><b>City</b></td>
                                                    <td><span id="avEnteredCity"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><b>State</b></td>
                                                    <td><span id="avEnteredState"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Zip&nbsp;Code</b></td>
                                                    <td><span id="avEnteredZip"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </td>
                                    <!--
                                </tr>
                                <tr>
                                    -->
                                    <td valign="top">
                                    <!--  need to dynamically generate this table? -->
                                    	<div class="orderCaption">Suggested&nbsp;Address(es)</div>
                                    	<div id="addrDiv_0" style="display: none;">
                                    		<input type="radio" class="avRadio" name="avAddressRadioIdx" value="0" />
	                                        <table class="address" >
	                                            <tbody>
	                                                <tr>
	                                                    <td><b>Address</b></td>
	                                                    <td><span id="avSuggestedAddress_0"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>City</b></td>
	                                                    <td><span id="avSuggestedCity_0"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>State</b></td>
	                                                    <td><span id="avSuggestedState_0"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>Zip&nbsp;Code</b></td>
	                                                    <td><span id="avSuggestedZip_0"></span></td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
                                        </div>
                                        <div style="height:25px"></div>
                                        <div id="addrDiv_1" style="display: none;">
                                        	<input type="radio" class="avRadio" name="avAddressRadioIdx" value="1" />
	                                        <table class="address" >
	                                            
	                                            <tbody>
	                                                <tr>
	                                                    <td><b>Address</b></td>
	                                                    <td><span id="avSuggestedAddress_1"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>City</b></td>
	                                                    <td><span id="avSuggestedCity_1"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>State</b></td>
	                                                    <td><span id="avSuggestedState_1"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>Zip&nbsp;Code</b></td>
	                                                    <td><span id="avSuggestedZip_1"></span></td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
                                        </div>
                                        <div style="height:25px"></div>
                                        <div id="addrDiv_2" style="display: none;">
                                        	<input type="radio" class="avRadio" name="avAddressRadioIdx" value="2" />
	                                        <table class="address" >
	                                            <!--<thead/>-->
	                                            <tbody>
	                                                <tr>
	                                                    <td><b>Address</b></td>
	                                                    <td><span id="avSuggestedAddress_2"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>City</b></td>
	                                                    <td><span id="avSuggestedCity_2"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>State</b></td>
	                                                    <td><span id="avSuggestedState_2"></span></td>
	                                                </tr>
	                                                <tr>
	                                                    <td><b>Zip&nbsp;Code</b></td>
	                                                    <td><span id="avSuggestedZip_2"></span></td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div align="center">
                                            <button type="button" id="avIgnoreButton">Ignore Suggestion</button>
                                        </div>
                                    </td>
                                    <td align="center">
                                        <div align="center">
                                            <button type="button" id="avAcceptButton">Accept Suggestion</button>
                                        </div>
                                        <input type="hidden" id="avResultCode" value="" />
                                        <input type="checkbox" id="avsPerformed" style="display:none" />
                                        <input type="checkbox" id="avNeedsResolution" style="display:none"/>
                                        <input type="checkbox" id="avCustomerInsisted" style="display:none"/>
                                        <input type="checkbox" id="avDisabled" checked="checked" style="display:none;" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
