<div style="width: 100%">
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
  <!--caption/-->
  <!--<thead/>-->
  <tbody>
    <tr>
      <td class="cartCaption" align="right">
        Shopping&nbsp;Cart
        &nbsp;&nbsp;
        <img src="images/calculator.png"
               alt="Shopping Cart"
               class="imageicon"
               height="16px"
               width="16px">
      </td>
    </tr>
  </tbody>
</table>
</div>
<div id="cartDiv" style="overflow:auto; height:415px; padding:5px; position:relative">
    <div id="cartItemId" idx="-1" class="cartItemDiv" style="display:none">
        <div style="padding:2px">
            <table border="1" cellpadding="0" cellspacing="0" width="176px">
              <!--caption/-->
              <!--<thead/>-->
              <tbody>
                <tr>
                  <td>
                    <table style="background-color: #cccccc; width: 100%;">
                      <!--caption/-->
                      <!--<thead/>-->
                      <tbody>
                        <tr>
                          <td  align="left" id="addCart_Item_Button" width="20px">
                            <img src="images/add.png"
                               id="addCartItemButton"
                               alt="Add an order to the shopping cart"
                               class="imageicon"
                               height="16px"
                               width="16px">
                          </td>
                          <td width="100%"><div align="center" id="ci_productId___dummy"></div></td>
                          <td align="right" id="deleteCart_Item_Button" width="20px">
                            <img src="images/delete.png"
                               id="deleteCartItemButton"
                               alt="Delete this order"
                               class="imageicon"
                               height="16px"
                               width="16px">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <table>
                      <!--caption/-->
                      <!--<thead/>-->
                      <tbody>
                        <tr>
                          <td colspan="3"><span id="ci_recipientFirstName___dummy"></span>&nbsp;<span id="ci_recipientLastName___dummy"></span></td>
                        </tr>
                        <tr>
                          <td width="100%">Product</td>
                          <td><div id="ci_productPriceDiscounted___dummy" style="text-align:right;"></div></td>
                          <td><div id="ci_productPriceMilesPoints___dummy" style="text-align:right;text-decoration: line-through;"></div></td>
                        </tr>
                        <tr>
                          <td>Add Ons</td>
                          <td><div id="ci_addOns___dummy" style="text-align:right;"></div></td>
                          <td width="100%" rowspan="4" valign="top">
                            <div align="right">
                            <img src="images/noproduct.gif"
                               id="ci_smallImage___dummy"
                               alt="Please select a product"
                               title="Please select a product"
                               height="60px"
                               width="60px"
                               onerror="setDefaultImage(this);">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Shipping</td>
                          <td><div id="ci_shippingFees___dummy" style="text-align:right;"></div></td>
                        </tr>
                        <tr>
                          <td>Fees</td>
                          <td><div id="ci_serviceFees___dummy" style="text-align:right;"></div></td>
                        </tr>
                        <tr>
                          <td id="ci_same_day_fee___dummy" style="display:none;" >Same Day Fee</td>
                          <td><div id="ci_same_day_fee_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>    
                        <tr>
                          <td><div id="ci_late_cutoff_fee_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr> 
                        <tr>
                         <td><div id="ci_vendor_mon_upcharge_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr> 
                        <tr>
                          <td><div id="ci_vendor_sun_upcharge_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr> 
                        <tr>
                          <td><div id="ci_international_fee_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>
                        <tr>
                          <td><div id="ci_service_fee_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>                    
                        <tr>
                          <td id="ci_tax1_name___dummy">Taxes</td>
                          <td><div id="ci_tax1_amount___dummy" style="text-align:right;"></div></td>
                          <td width="100%" rowspan="5" valign="top">
                            <div align="center">
                            <img src="images/30_grey.gif"
                               id="ci_deliveryType___dummy"
                               alt="Delivery Type"
                               title="Delivery Type"
                               height="30px"
                               width="30px"
                               onerror="setDefaultImage(this);">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td id="ci_tax2_name___dummy" style="display:none;" >Tax2</td>
                          <td><div id="ci_tax2_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>
                        <tr>
                          <td id="ci_tax3_name___dummy" style="display:none;" >Tax3</td>
                          <td><div id="ci_tax3_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>
                        <tr>
                          <td id="ci_tax4_name___dummy" style="display:none;" >Tax4</td>
                          <td><div id="ci_tax4_amount___dummy" style="text-align:right;display:none;"></div></td>
                        </tr>
                        <tr>
                          <td id="ci_tax5_name___dummy" style="display:none;" >Tax5</td>
                          <td><div id="ci_tax5_amount___dummy" style="text-align:right;display:none;"></div></td>
              			</tr>
                        <tr>
                          <td>Subtotal</td>

<td  >

  <div id="ci_surchargeFees___dummy" style="text-align:center;display:none;"></div>
 <a class="itemtooltip" href="" style="text-decoration: none; cursor:default;"  onclick="return false" > 
   <div id="ci_subTotal___dummy" style="text-align:right; color:#6b79a5;" onmouseover = "javascript:displayFeeOnMouseOver(this);"></div>
   <span id="ci_spanItemMouseOver___dummy" style="font-weight:normal"></span>
  </a>
</td>
 


                          <td>
                            <div id="ci_notFSEligible___dummy" style="text-align:center;display:none;">
                            <img src="images/noFSicon.jpg"
                               alt="No Free Shipping"
                               title="No Free Shipping"
                               height="16px"
                               width="38px">
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>

<div  id="original_order_info" style="overflow:auto; height:160px; display:none;">
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
  <!--caption/-->
  <!--<thead/>-->
  <tbody>
    
      <tr>
        <td ><b>Current Customer is: </b>&nbsp;
        <span id="uo_customer_name" style="text-align:right;"></span>
        </td>
      </tr>
      <tr>
        <td><b>Order #:</b>&nbsp;
        <span id="uo_external_order_number" style="text-align:right;"></span>
        </td>
      </tr>
      <tr>
        <td> <span id="uo_delivery_date_text" style="font-weight: bold;"></span>
       <span id="uo_delivery_date" style="text-align:right;"></span>
       </td>
      </tr>
      <tr>
        <td><b>Item #:</b>&nbsp;
        <span id="uo_item" style="text-align:right;"></span><br/>
        <span id="uo_color1_description_text" style="font-weight: bold;"></span>
        <span id="uo_color1_description" style="text-align:right;"></span>
        <span id="uo_color2_description_text" style="font-weight: bold;"></span>
        <span id="uo_color2_description" style="text-align:right;"></span>
        </td>
     </tr>
     <tr>
        <td><b>Merch Price:</b>&nbsp;$&nbsp;
        <span id="uo_original_product_amount" style="text-align:right;color:red;text-decoration: line-through;margin-right: 2px" ></span>
        <span id="uo_original_discount_product_amount" style="text-align:right;" ></span>&nbsp;
        <span id="uo_original_miles_point" style="text-align:right;" ></span>
        </td>
      </tr>
      <tr>
        <td><span id="uo_ship_method_text" style="font-weight: bold;"></span>
        <span id="uo_ship_method" style="text-align:right;" ></span></td>
      </tr>
       <tr>
        <td><span id="uo_tax_text" style="font-weight: bold;"></span>
        <span id="uo_tax" style="text-align:right;" ></span></td>
      </tr>
     
       <tr id="uo_addon_row" style="display:none;">
        <td><span id="uo_addon_text" style="font-weight: bold;"></span>
        <span id="uo_addon_amount" style="text-align:right;color:red;text-decoration: line-through;margin-right: 2px" ></span>
        <span id="uo_addon_discount_amount" style="text-align:right;" ></span><br/>
        <span id="uo_addon_desc" style="text-align:right;" ></span>
        </td>
     </tr>
     
      <tr>
        <td id="sympathyDeliveryLocation"><b>Delivery Location:</b>&nbsp;
        <span id="uo_delivery_location" style="text-align:right;"></span>
        </td>
      </tr>
      
       <tr>
        <td id='timeOfService'><b>Time Of Service:</b>&nbsp;
        <span id="uo_timeOfService" style="text-align:right;"></span>
        </td>
      </tr>
      
   </tbody>
</table>
</div>
<div style="vertical-align:bottom;width:100%;background-color:#eeeeee;">
    <table width="100%">
    <caption>Total</caption>
    <!--<thead/>-->
    <tbody>
      <tr>
        <td>Orders in Cart</td>
        <td colspan="2"><div id="cartOrdersInCart" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td>Product</td>
        <td colspan="2"><div id="totalProduct" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td>Add Ons</td>
        <td colspan="2"><div id="totalAddons" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td>Shipping</td>
        <td colspan="2"><div id="totalShippingFees" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td>Fees</td>
        <td colspan="2"><div id="totalServiceFees" style="text-align:right;" ></div></td>
      </tr>
      <tr>
        <td id="total_same_day_fee" style="display:none;">Same Day Fee</td>
        <td colspan="2"><div id="total_same_day_fee_amount" style="text-align:right;display:none;"></div></td>
      </tr>
       <tr>
        <td colspan="2"><div id="total_late_cutoff_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td colspan="2"><div id="total_monday_upcharge_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td colspan="2"><div id="total_sunday_upcharge_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td colspan="2"><div id="total_international_fee_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td colspan="2"><div id="total_service_fee_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td id="total_tax1_name">Taxes</td>
        <td colspan="2"><div id="total_tax1_amount" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td id="total_tax2_name" style="display:none;">Tax2</td>
        <td colspan="2"><div id="total_tax2_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td id="total_tax3_name" style="display:none;">Tax3</td>
        <td colspan="2"><div id="total_tax3_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td id="total_tax4_name" style="display:none;">Tax4</td>
        <td colspan="2"><div id="total_tax4_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td id="total_tax5_name" style="display:none;">Tax5</td>
        <td colspan="2"><div id="total_tax5_amount" style="text-align:right;display:none;"></div></td>
      </tr>
      <tr>
        <td>Total</td>
          <td >
             <input type = "hidden" id = "applySurchargeCode" />
             <input type = "hidden" id = "totalSurchargeFee" />
             <input type = "hidden" id = "surchargeDescription" />
             <input type = "hidden" id = "sameDayUpchargeMouseOver" />
             <input type = "hidden" id = "lateCutOffMouseOver" />
             <input type = "hidden" id = "mondayUpchargeMouseOver" />
             <input type = "hidden" id = "sundayUpchargeMouseOver" />
             <input type = "hidden" id = "internationalFeeMouseOver" />
             <input type = "hidden" id = "serviceFeeMouseOver" />
             <input type = "hidden" id = "totalOrderAmount" />
             <input type = "hidden" id = "onSubmitButtonFlagOn" />
          </td>
          <td >
             <a class="tooltip2" href="" style="text-decoration: none; cursor:default; color: black;" onmouseover = "javascript:displayFeeTotalsOnMouseOver();" onclick="return false" > 
              <div id="total" style="text-align:right;background-color:#eeeeee;" ></div>  
         	    <span id="spanTotalsMouseOver" style= "margin-left : -20%; margin-top: -24%"></span>
            </a>
		       </td>
      </tr>
      <tr>
        <td colspan="3">
            <hr>
        </td>
      </tr>
      <tr>
        <td colspan="2">Total Discounts Applied</td>
        <td><div id="totalDiscounts" style="text-align:right;"></div></td>
      </tr>
      <tr>
        <td colspan="2">Total Miles/Points Earned</td>
        <td>
            <div id="totalPointMiles" style="text-align:right;"></div>
            <input type="hidden" id="masterOrderNumber" value="">
        </td>
      </tr>
    </tbody>
    </table>
</div>
  
<div id="copyDivId" class="copyDiv" style="display:none">
  <div id="innerCopyDivId" class="innerCopyDiv">
    <table>
      <!--caption/-->
      <!--<thead/>-->
      <tbody>
        <tr>
          <td>
            <input type="radio" name="copyRadio" id="copyRadioSameSame">
          </td>
          <td>
            <label for="copyRadioSameSame" id="copyRadioSameSameLabel">Same Product / Same Recipient</label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="copyRadio" id="copyRadioSameDiff">
          </td>
          <td>
            <label for="copyRadioSameDiff" id="copyRadioSameDiffLabel">Same Product / New Recipient</label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="copyRadio" id="copyRadioDiffSame">
          </td>
          <td>
            <label for="copyRadioDiffSame" id="copyRadioDiffSameLabel">New Product / Same Recipient</label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="copyRadio" id="copyRadioDiffDiff" checked="checked">
          </td>
          <td>
            <label for="copyRadioDiffDiff" id="copyRadioDiffDiffLabel">New Product / New Recipient</label>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <div align="center">
              <button id="copyOrderButton" type="button">Create Order</button>&nbsp;<button id="copyCancelButton" type="button">Cancel</button>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
