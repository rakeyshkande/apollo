<div align="center" style="background-color:#eeeeee;padding:2px">
<table width="772px" border="0" bgcolor="#ffffff">
  <caption>Product Search</caption>
  <!--<thead/>-->
  <tbody>
    <tr>
      <td rowspan="2" bgcolor="#eeeeee" valign="top" height="629px" width="110px">
        <div style="overflow: auto;">
          <table width="100%" align="center">
            <caption class="smallCaption">Favorites</caption>
            <!--<thead/>-->
            <tbody id="productSearchPromoBody" align="left">
                <tr><td></td></tr>
            </tbody>
          </table>
        </div>
      </td>
      <td width="665px" height="629px" valign="top">
        <table width="100%">
          <!--caption/-->
          <!--<thead/>-->
          <tbody>
            <tr>
              <td class="labelRight" >
                <label id="productSearchKeywordsLabel" for="productSearchKeywords" class="labelRight">Keywords</label>
              </td>
              <td align="center"
                       width="100px">
                <input type="text" 
                       id="productSearchKeywords"
                       name="productSearchKeywords"
                       size="20">
              </td>
              <td class="labelRight">
                <label id="productSearchDeliveryDateLabel" for="productSearchDeliveryDate" class="labelRight">Delivery&nbsp;Date</label>
              </td>
              <td>
                <select size="1"
                        name="productSearchDeliveryDate"
                        id="productSearchDeliveryDate">
                </select>
             </td>
             <td width="30px">
                <button type="button"
                        id="productSearchCalendarButton">
                  <img alt="Calendar"
                       src="images/calendar.gif">
                </button>
                <div id="productSearchDeliveryDateCalContainer" class="yui-skin-sam"></div>
              </td>
              <td width="30px" align="center" rowspan="2">
                    <img src="images/animatedProgress.gif"
                           id="searchProcessing"
                           alt="Processing"
                           style="visibility:hidden;">
              </td>
                <td rowspan="2" bgcolor="#eeeeee">
                    <div style="background-color:#eeeeee;padding-left:2px;padding-right:2px;text-align:center">
                    <a tabindex="-1" href="javascript:PRODUCT_SEARCH.toggleRecentlyViewedPanel();" id="psRecentlyViewedLink">Recently&nbsp;Viewed</a>
                    <br>
                    <a tabindex="-1" href="javascript:PRODUCT_SEARCH.toggleTopSellersPanel();" id="psTopSellersLink">Top&nbsp;Sellers</a>
                    </div>
                </td>
            </tr>
            <tr>
                  <td id="productSearchZipLabel" class="labelRight">
                    <label for="productSearchZip" class="labelRight">Zip&nbsp;Code</label>
                  </td>
                  <td>
                    <input type="text" 
                           id="productSearchZip"
                           size="6">
                    &nbsp;
                    <button type="button"
                           id="productSearchZipSearch">
                      <img src="images/magnify.jpg" 
                            id="productSearchZipSearchImage"
                           alt="Recipient zip search"
                         width="16px"
                        height="16px">
                    </button>
                  </td>
                  <td class="labelRight">
                    <label id="productSearchCountryComboLabel" 
                           class="labelRight" 
                           for="productSearchCountryCombo">Country</label>
                  </td>
                  <td>
                    <select size="1"
                            name="productSearchCountryCombo"
                            id="productSearchCountryCombo">
                    </select>
                  </td>
                  <td width="30px" align="left">
                    <button type="button"
                           name="productSearchGoButton"
                           id="productSearchGoButton">
                      <img src="images/bullet_square_green.png" 
                           width="16px"
                           height="16px"
                           alt="Start search">
                    </button>
                    <button type="button"
                           name="productSearchDisabledButton"
                           id="productSearchDisabledButton"
                           disabled="disabled">
                      <img src="images/bullet_square_grey.png" 
                           alt="Unable to stop search at this time"
                           id="productSearchDisabledButtonImage"
                           width="16px" 
                           height="16px">
                    </button>
                    <button type="button"
                           name="productSearchStopButton"
                           id="productSearchStopButton">
                      <img src="images/stop.png" 
                           alt="Stop search"
                           id="productSearchStopButtonImage" 
                           width="16px" 
                           height="16px">
                    </button>
                  </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</div>
<div id="psResultsDivId" class="psResultsDiv">
  <div id="innerPsResultsDivId" class="innerPsResultsDiv">   
    <div>
        <table width="100%">
          <!--caption/-->
          <!--<thead/>-->
          <tbody>
            <tr>
              <td>
                <div align="left">
                  <button type="button" id="productSearchCloseButton"  style="width: 90px;">Back to Order</button>
                </div>
              </td>
              <td width="340">&nbsp;</td>
              <td class="labelRight">
                <label id="productSearchSortComboLabel" for="productSearchSortCombo" class="labelRight">Sort</label>
              </td>
              <td>
                <select size="1"
                        name="productSearchSortCombo"
                        id="productSearchSortCombo">
                  <option value="MATCH">
                    Best Match
                  </option>
                  <option value="ASCENDING">
                    Lowest Price
                  </option>
                  <option value="DESCENDING">
                    Highest Price
                  </option>
                  <option value="POPULAR">
                    Popularity
                  </option>
                </select>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
    <div id="psNoResults" style="background-color:#cc0033;color:white;display:none;text-align:center">
        <b>No results found for the search criteria</b>
    </div>
    <table align="center">
      <!--caption/-->
      <!--<thead/>-->
      <tbody id="productSearchProductBody">
      </tbody>
    </table>
    <table align="center" bgcolor="#ffffff" width="100%">
      <!--caption/-->
      <!--<thead/>-->
      <tbody>
        <tr>
          <td align="right">
            <img  src="images/clock.png"
                  alt="Same day delivery"
                  title="Same day delivery"
                  width="16px"
                  height="16px">
            &nbsp;&nbsp;<label>Same day delivery</label>
          </td>
        </tr>
        <tr>
          <td width="100%" height="30px" valign="middle">
            <div id="psNavButtons">
              <div align="center" style="white-space: nowrap;">
                <span id="psLeftNavButtons">
                  <button type="button" 
                          id="psNavBeginning">
                      <img alt="Move to start of search results"
                           src="images/navigate_beginning.png"
                           class="productSearchPageNavImg" 
                           width="16px" 
                           height="16px">
                  </button>
                  <button type="button" 
                          id="psNavPrevious">
                      <img alt="Previous page of search results"
                           src="images/navigate_left.png"
                           width="16px" 
                           height="16px">
                  </button>
                    &nbsp;&nbsp;
                </span>
                    <span id="productSearchPage" class="productSearchPageNav"></span>
                <span id="psRightNavButtons">
                    &nbsp;&nbsp;
                  <button type="button" 
                          id="psNavNext">
                      <img alt="Next page of search results"
                           src="images/navigate_right.png"
                           width="16px" 
                           height="16px">
                  </button>
                  <button type="button" 
                          id="psNavEnd">
                      <img alt="Move to end of search results"
                           src="images/navigate_end.png" 
                           width="16px" 
                           height="16px">
                  </button>
                </span>
              </div>
            </div>
          </td>
        </tr>
    </table>
	<button type="button" id="productSearchBottomCloseButton"  style="width: 90px;">Back to Order</button>
  </div>
</div>
<div id="psDetailDivId" class="psDetailDiv">
  <div id="innerPsDetailDivId" class="innerPsDetailDiv">
    <table id="psDetailProduct">
      <caption id="psDetailProductName"></caption>
      <!--<thead/>-->
      <tbody>
        <tr>
            <td valign="top">
                <div align="left">
                    <button id="psDetailCloseButton" type="button">Back to Search</button>
                    <input type="hidden" id="psDetailProductId">
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color:#eeeeee;">
                <div style="height:380px;width:340px;padding-top:5px;">
                    <img src="images/npi_c.jpg"
                      id="psDetailProductImage"
                      alt=""
                      title=""
                      class="centeredDetailedProductImage"
                      onerror="setDefaultImage(this);">
                </div>
            </td>
            <td style="background-color:#eeeeee;font: bold 12px Verdana; vertical-align:top;">
                <span id="psDetailProductDesc" style="height:370px;padding:5px;"></span>
            </td>
        </tr>
        <tr><td colspan="2"><div id="psDetailIOTWDescription" style="font-size:large;text-align:center;"></div></td></tr>
        <tr>
        <td width="20px">
            <span style="float:left;">
                <img id="psDetailSameDay"
                      src="images/clock.png"
                      alt="Same day delivery"
                      title="Same day delivery"
                      width="16px"
                      height="16px">
            </span>
        </td>
          <td align="center">
            <div align="center">
              <button id="productDetailSelectButton" type="button">Order This Product</button>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <button id="psDetailBottomCloseButton" type="button" style="float:left;">Back to Search</button>
  </div>
</div>
<div id="psCrossSellDiv" class="psDetailDiv">
    <div id="innerPsCrossSellDiv" class="innerPsDetailDiv">
        <table width="100%" align="center">
            <caption>Suggest Product Alternatives</caption>
            <thead>
                <tr>
                    <td valign="top">
                        <div align="left">
                            <button id="psSuggestCloseButton" type="button" style="width: 90px;">Back to Order</button>
                        </div>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="50%">&nbsp;</td>
                    <td align="center">
                        <table align="center">
                            <tbody id="psDetailSuggestBody">
                                <tr><td></td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="50%">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="psTopSellersDivId" class="psDetailDiv">
    <div id="innerPsTopSellersDivId" class="innerPsDetailDiv">
        <div id="psTopNoResults" style="background-color:#cc0033;color:white;font-weight:bold;display:none;text-align:center">
            No results found for the search criteria<
        </div>
        <table>
            <caption><label id="topSellersDivLabel"/></caption>
            <thead>
                <tr>
                    <td valign="top">
                        <div align="left">
                            <button id="psTopCloseButton" type="button">Back to Search</button>
                        </div>
                    </td>
                </tr>
            </thead>
            <tbody id="psTopSellersBody">
                <tr><td></td></tr>
            </tbody>
        </table>
        <table align="center" bgcolor="#ffffff" width="100%">
          <!--caption/-->
          <!--<thead/>-->
          <tbody>
            <tr>
              <td align="right">
                <img  src="images/clock.png"
                      alt="Same day delivery"
                      title="Same day delivery"
                      width="16px"
                      height="16px">
                &nbsp;&nbsp;<label>Same day delivery</label>
              </td>
            </tr>
            <tr>
              <td width="100%" height="30px" valign="middle">
                <div id="psTopNavButtons">
               	  <button id="psBottomCloseButton" type="button" style="float:left;">Back to Search</button>
                  <div align="center" style="white-space: nowrap;">
                    <span id="psTopLeftNavButtons">
                      <button type="button" 
                              id="psTopNavBeginning">
                          <img alt="Move to start of search results"
                               src="images/navigate_beginning.png"
                               class="productSearchPageNavImg" 
                               width="16px" 
                               height="16px">
                      </button>
                      <button type="button" 
                              id="psTopNavPrevious">
                          <img alt="Previous page of search results"
                               src="images/navigate_left.png" 
                               width="16px" 
                               height="16px">
                      </button>
                        &nbsp;&nbsp;
                    </span>
                        <span id="psTopNavLinks" class="productSearchPageNav"></span>
                    <span id="psTopRightNavButtons">
                        &nbsp;&nbsp;
                      <button type="button" 
                              id="psTopNavNext">
                          <img alt="Next page of search results"
                               src="images/navigate_right.png"
                               width="16px" 
                               height="16px">
                      </button>
                      <button type="button" 
                              id="psTopNavEnd">
                          <img alt="Move to end of search results"
                               src="images/navigate_end.png"
                               width="16px" 
                               height="16px">
                      </button>
                    </span>
                  </div>
                </div>
              </td>
            </tr>
        </table>
    </div>
</div>

<div id="psViewedDivId">
    <div id="innerPsViewedDivId">
        <table width="100%">
            <thead>
                <tr>
                    <td width="20px">&nbsp;</td>
                    <td width="100%">
                        <!--
                        <span class="caption">Recently Viewed Products</span>
                        -->
                        <div style="color:#CED7EF;font-weight:bolder;vertical-align:middle;height:25px;text-align:center;">
                        Recently Viewed Products
                        </div>
                    </td>
                    <td align="right" width="20px">
                    <div align="right">
                    <img src="images/delete.png"
                       id="closeRecentlyViewedButton"
                       alt="Close window"
                       width="16px" 
                       height="16px">
                    </div>
                  </td>
                </tr>
            </thead>
            <tbody>
              <tr>
                  <td valign="top" colspan="3" bgcolor="#eeeeee">
                    <div style="overflow:auto;height:170px">
                        <ol id="psViewedList">
                        </ol>
                    </div>
                  </td>
              </tr>
            </tbody>
        </table>
    </div>
</div>