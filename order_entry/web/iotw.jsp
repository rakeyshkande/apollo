<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"  %>
<%@ page import="org.displaytag.tags.TableTagParameters"%>
<%@ page import="org.displaytag.util.ParamEncoder"%>
<link type="text/css" rel="stylesheet" href="css/calendar.css"/>
<script type="text/javascript" src="js/utilities/yahoo-dom-event.js"></script> 
<script type="text/javascript" src="js/utilities/calendar.js"></script> 
<script type="text/javascript" src="js/utilities/calendarUtil.js"></script> 
<script type="text/javascript" src="js/utilities/prototype.js"></script>
<script type="text/javascript" src="js/utilities/rico.js"></script>
<script type="text/javascript" src="js/utilities/ricoCommon.js"></script>
<script type="text/javascript" src="js/utilities/ricoEffects.js"></script>
<script type="text/javascript" src="js/utilities/ricoComponents.js"></script>
<script type="text/javascript" src="js/utilities/ricoCalendar.js"></script>
<script type="text/javascript" src="js/utilities/ricoBehaviors.js"></script>
<script type="text/javascript" src="js/utilities/prototype_helpers.js"></script>
<script type="text/javascript" src="js/utilities/ftdutils.js"></script>
<script type="text/javascript" src="js/utilities/global.js"></script>
<script type="text/javascript" src="js/objects/field_object.js"></script>
<script type="text/javascript" src="js/xml/javeline_xpath.js"></script>
<script type="text/javascript" src="js/xml/sarissa.js"></script>
<script type="text/javascript" src="js/utilities/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/utilities/ftddom.js"></script>
<script type="text/javascript" src="js/utilities/ftdxml.js"></script>
<script type="text/javascript" src="js/utilities/ftdajax.js"></script>
<script type="text/javascript" src="js/utilities/sorttable.js"></script>
<script type="text/javascript" src="js/iotw/iotw_object.js"></script>
<script type="text/javascript" src="js/iotw/iotw_events.js"></script>
<script type="text/javascript" src="js/iotw/iotw.js"></script>
<script type="text/javascript" src="js/oe.js"></script>
<%
    String identity = (String)request.getAttribute("identity");
    if( identity==null || identity.length()==0 ) identity="testcsr1";
    String secureURL = (String)request.getAttribute("secure-url");
    if( secureURL==null ) 
        secureURL="";
    else
        secureURL+="/";

    // Obtain current page and sort
    String currentPageParameter = new ParamEncoder("iotwVO").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
    String currentSortParameter = new ParamEncoder("iotwVO").encodeParameterName(TableTagParameters.PARAMETER_SORT);
    String currentPage = currentPageParameter + "=" + request.getParameter(currentPageParameter);
    String currentSort = currentSortParameter + "=" + request.getParameter(currentSortParameter);
    
    String inSourceCode = (String)request.getAttribute("searchSourceCode");
    String inProductId = (String)request.getAttribute("searchProductId");
    String inIOTWSourceCode = (String)request.getAttribute("searchIOTWSourceCode");
    String inExpiredOnly = (String)request.getAttribute("searchExpiredOnly");
    String pagesizeStr = (String)request.getAttribute("iotwPagesize");
    
    int pagesize = 10;
    try
    {
        pagesize = Integer.parseInt(pagesizeStr);
    } catch (Throwable t)
    {
        pagesize = 10;        
    }
    
%>
<script type="text/javascript">
  var v_repIdentity = '<%=identity%>';
  var v_serverURLPrefix = '<%=secureURL%>';
  var v_currentPage = '<%=currentPage%>';
  var v_currentSort = '<%=currentSort%>';
  var v_expiredOnly = '<%=inExpiredOnly%>';
</script>
<form action="/iotw.do">
    <jsp:include page="includes/security.jsp"/>
    
    <div id="iotwListDiv" class="iotwDiv">
        <div id="iotwListInnerDiv" class="iotwInnerDiv">
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td align="right"><b>Overall Source Code:</b></td>
                    <td><input type="text" name="overallSourceSearch" size="7" value="<%= inSourceCode %>"/>&nbsp;&nbsp;</td>
                    <td align="right"><b>Product Id:</b></td>
                    <td><input type="text" name="productIdSearch" size="15" value="<%= inProductId %>"/>&nbsp;&nbsp;</td>
                    <td align="right"><b>IOTW Source Code:</b></td>
                    <td><input type="text" name="iotwSourceSearch" size="7" value="<%= inIOTWSourceCode %>"/>&nbsp;&nbsp;</td>
                    <td align="right"><b>Expired Only:</b></td>
                    <td><input type="checkbox" name="expiredOnlySearch"/>&nbsp;&nbsp;</td>
                    <td><input type="button" name="searchButton" id="searchButton" value="Search"/></td>
                </tr>
            </table>
            <div class="iotwListDiv">
                    <display:table name="iotwList" id="iotwVO" requestURI="iotw.do" sort="list" defaultsort="2" pagesize="<%=pagesize%>" class="sorttable altstripe sort01">
                        <display:setProperty name="paging.banner.placement" value="both" />
                        <display:setProperty name="paging.banner.onepage" value=""/>
                        <display:setProperty name="paging.banner.all_items_found" value='<span class="pagebanner"> {0} {1} found.&nbsp;&nbsp;&nbsp; </span>'/>
                        <display:setProperty name="paging.banner.some_items_found" value='<span class="pagebanner"> {0} {1} found.&nbsp;&nbsp;&nbsp;</span>'/>
                        <display:setProperty name="paging.banner.full" value='<span class="pagelinks"> [&nbsp;<a href="{1}">First</a>&nbsp;/&nbsp;<a href="{2}">Prev</a>&nbsp;]&nbsp;&nbsp;{0}&nbsp;&nbsp;[&nbsp;<a href="{3}">Next</a>&nbsp;/&nbsp;<a href="{4}">Last</a>&nbsp;]</span>'/>
                        <display:setProperty name="paging.banner.first" value='<span class="pagelinks"> [&nbsp;First&nbsp;/&nbspPrev&nbsp;]&nbsp;&nbsp;{0}&nbsp;&nbsp;[&nbsp;<a href="{3}">Next</a>&nbsp;/&nbsp;<a href="{4}">Last</a>&nbsp;]  </span>'/>
                        <display:setProperty name="paging.banner.last" value='<span class="pagelinks"> [&nbsp;<a href="{1}">First</a>&nbsp;/&nbsp;<a href="{2}">Prev</a>&nbsp;]&nbsp;&nbsp;{0}&nbsp;&nbsp;[&nbsp;Next&nbsp;/&nbsp;Last&nbsp;] </span>'/>
                        <display:setProperty name="paging.banner.page.selected" value="<strong>&nbsp;{0}&nbsp;</strong>"/>
                        <display:setProperty name="paging.banner.page.link" value='<a href="{1}" title="Go to page {0}">&nbsp;{0}&nbsp;</a>'/>
                        <c:if test="${requestScope.isBlankSearch eq 'Y' }">
                        	<display:setProperty name="basic.msg.empty_list" value="<span>Please fill in search criteria and search</span>"/>
                        </c:if>
                        <display:caption>Program Details</display:caption>
                        <display:column sortable="false" title="" class="centerColumn">
                        	<c:choose>
                        	<c:when test="${iotwVO.createdBy eq 'APE'}">
                        		<c:choose>
                        			<c:when test="${APEenabledIOTWEditable eq 'Y'}">
                        				<button type="button" id="editButton" onclick="IOTW_EVENTS.onEditButton(${iotwVO.iotwId});">Edit</button>
                        			</c:when>
                        			<c:otherwise>
                        				APE Enabled
                        			</c:otherwise>
                        		</c:choose>
                        	</c:when>
                        	<c:otherwise>
                        		<button type="button" id="editButton" onclick="IOTW_EVENTS.onEditButton(${iotwVO.iotwId});">Edit</button>
                        	</c:otherwise>
                        </c:choose>                            
                        </display:column>
                        <display:column property="sourceCode" sortable="true" title="Overall Source Code" sortProperty="sortableSourceCode" class="centerColumn"/>
                        <display:column sortable="true" title="Product ID" class="centerColumn">
                            ${iotwVO.productId}(<c:choose><c:when test="${iotwVO.productAvailable}">Y</c:when><c:otherwise>N</c:otherwise></c:choose>)
                        </display:column>
                        <display:column sortable="true" title="Program Date Range" class="centerColumn">
                            <fmt:formatDate pattern="MM/dd/yyyy" value="${iotwVO.startDate}" /><br/>to<br/><fmt:formatDate pattern="MM/dd/yyyy" value="${iotwVO.endDate}" />
                        </display:column>
                        <display:column property="productPageMessage" maxLength="10" sortable="true" title="Product Page Messaging" decorator="com.ftd.oe.web.HTMLDecorator" />
                        <display:column property="calendarMessage" maxLength="10" sortable="true" title="Calendar Widget Messaging" decorator="com.ftd.oe.web.HTMLDecorator" />
                        <display:column sortable="true" title="Promotion / Discount">
                            <c:forEach var="discountOrProgram" items="${iotwVO.discountOrProgram}">
                                ${discountOrProgram}<br/>
                            </c:forEach>
                        </display:column>
                        <display:column property="iotwSourceCode" sortable="true" title="Source Code (IOTW)" class="centerColumn"/>
                        <display:column sortable="true" title="Special Offer Switch(hp)" class="centerColumn">
                            <c:choose><c:when test="${iotwVO.specialOffer}">On</c:when><c:otherwise>Off</c:otherwise></c:choose><input type="hidden" id="specialOffer_${iotwVO.iotwId}" value="${iotwVO.specialOffer}">
                        </display:column>
                        <display:column property="wordingColor" maxLength="7" sortable="true" title="Wording Color (product page)" class="centerColumn"/>
                        <display:column sortable="true" title="Delivery Discount Dates" class="centerColumn">    
                            <a title="${iotwVO.formattedDeliveryDiscountDates}"><c:choose><c:when test="${iotwVO.deliveryDiscountDatesExist}">Yes</c:when><c:otherwise>No</c:otherwise></c:choose></a>
                        </display:column>
                        <display:column sortable="true" title="Remove" class="centerColumn">
                        	<c:choose>
                        	<c:when test="${iotwVO.createdBy eq 'APE'}">
                        		APE Enabled
                        	</c:when>
                        	<c:otherwise>
                        		<input type="checkbox" name="removeCheckbox" value="${iotwVO.iotwId}"/>
                        	</c:otherwise>
                        </c:choose>                            
                        </display:column>
                        <%
                        /*COMMENTED OUT BUT LEAVE AROUND IN CASE THE BUSINESS CHANGES THEIR MIND
                        <display:column sortable="true" title="Product Available">
                            <c:choose><c:when test="${iotwVO.productAvailable}">Yes</c:when><c:otherwise>No</c:otherwise></c:choose>
                        </display:column>
                        <display:column sortable="true" title="Status">
                            <c:choose><c:when test="${iotwVO.status}">On</c:when><c:otherwise>Off</c:otherwise></c:choose>
                        </display:column>
                        * COMMENTED OUT BUT LEAVE AROUND IN CASE THE BUSINESS CHANGES THEIR MIND
                        */
                        %>
                    </display:table>
            </div>
            
            <div class="iotwNovatorDiv">
                <input type="checkbox"
                      disabled="true"
                      id="liveFeed"
                      title="Send updates to Novator production web site"/>
                <label class="radioLabel" id="liveFeedLabel" for="liveFeed">Live</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="contentFeed"
                      title="Send updates to Novator content web site"/>
                <label class="radioLabel" id="contentFeedLabel" for="contentFeed">Content</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="testFeed"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="testFeedLabel" for="testFeed">Test</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="uatFeed"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="uatFeedLabel" for="uatFeed">UAT</label>&nbsp;&nbsp;
            </div>
            
            <div id="viewListErrorButtonsDiv" style="display:none" class="viewListErrorButtonsDiv">
                <button type="button" id="viewListErrorsButton">View Errors</button>
            </div>

            <div class="iotwButtonsDiv">
            <c:choose>
			<c:when test="${searchIOTWReadOnly eq 'Y'}">
				<button type="button" id="removeSelectedButton" disabled="disabled">Remove Selected</button>&nbsp;&nbsp;
                <button type="button" id="addButton" disabled="disabled">Add</button>&nbsp;&nbsp;
                <button type="button" id="sendFeedAllButton" disabled="disabled">Send All</button>&nbsp;&nbsp;
			</c:when>
			<c:otherwise>
				<button type="button" id="removeSelectedButton">Remove Selected</button>&nbsp;&nbsp;
                <button type="button" id="addButton">Add</button>&nbsp;&nbsp;
                <button type="button" id="sendFeedAllButton">Send All</button>&nbsp;&nbsp;
			</c:otherwise>
			</c:choose>                    
                <button type="button" id="exitButton">Exit</button>
            </div>
        </div>
    </div>
    
    <div id="iotwEditDiv" class="iotwDiv" style="display:none;">
        <div id="iotwEditDiv" class="iotwInnerDiv">
            <div style="overflow:auto;height:500px">
                <input type="hidden" id="iotwDisplayed">
                <table width="100%" bgcolor="#eeeeee">
                    <caption>Program Details</caption>
                    <thead/>
                    <tbody>
                        <tr>
                            <td valign="top">
                                <div id="idAddDetailDiv">
                                    <table width="100%">
                                        <!--caption/-->
                                        <thead/>
                                        <tbody>
                                            <tr>
                                                <td class="labelRight" valign="top"><label id="sourceCodesLabel" for="sourceCodes" class="labelRight">Overall Source Codes</label></td>
                                                <td>
                                                    <textarea id="sourceCodes" 
                                                              cols="30" 
                                                              rows="3"></textarea>
                                                </td>
                                                <td class="labelRight" valign="top"><label id="productIdsLabel" for="productIds" class="labelRight">Product IDs</label></td>
                                                <td>
                                                    <textarea id="productIds" 
                                                              cols="30" 
                                                              rows="3"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="labelRight"><label id="iotwSourceCodeLabel" for="iotwSourceCode" class="labelRight">Source Code (IOTW)</label></td>
                                                <td colspan="3">
                                                    <input type="text" 
                                                           id="iotwSourceCode"
                                                           size="10"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table width="100%">
                                    <caption>Program Management</caption>
                                    <thead/>
                                    <tbody>
                                        <tr>
                                            <td valign="top" class="labelRight"><label id="startDateLabel" for="startDate" class="labelRight">Start Date</label></td>
                                            <td valign="top">
                                                <input id="startDate" type="text" size="10" readonly="readonly"/>
                                                &nbsp;
                                                <button type="button"
                                                        id="startDateCalButton">
                                                  <img alt="Calendar"
                                                       src="images/calendar.gif"/>
                                                </button>
                                                <div id="startDateCalContainer" class="yui-skin-sam"></div>
                                            </td>
                                            <td valign="top" class="labelRight"><label id="endDateLabel" for="endDate" class="labelRight">End Date</label></td>
                                            <td valign="top">
                                                <input id="endDate" type="text" size="10" readonly="readonly"/>
                                                &nbsp;
                                                <button type="button"
                                                        id="endDateCalButton">
                                                  <img alt="Calendar"
                                                       src="images/calendar.gif"/>
                                                </button>
                                                <div id="endDateCalContainer" class="yui-skin-sam"></div>
                                            </td>
                                            <td valign="top" class="labelRight">
                                                <label id="deliveryDatesLabel" for="deliveryDates" class="labelRight">Delivery Discount Dates</label>
                                            </td>
                                            <td valign="top" rowspan="3" width="75px">
                                                <select id="deliveryDates" size="10" multiple="multiple">
                                                    <option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                                </select>
                                            </td>
                                            <td valign="top" width="20px" align="center">
                                                <button type="button"
                                                        id="deliveryDatesCalButton">
                                                    <img alt="Calendar"
                                                       src="images/calendar.gif"/>
                                                </button>
                                                <div id="deliveryDatesCalContainer" class="yui-skin-sam"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="labelRight"><label id="specialOfferLabel" for="specialOffer" class="labelRight" title="homepage">Special Offer Switch (homepage)</label></td>
                                            <td valign="top"><input id="specialOffer" type="checkbox"/></td>
                                            <td valign="top" class="labelRight"><label id="wordingColorLabel" for="wordingColor" class="labelRight" title="product page">Wording Color <br/>(product page)</label></td>
                                            <td valign="top"><input id="wordingColor" type="text" size="10"/></td>
                                            <td valign="top" rowspan="2" height="100%">&nbsp;</td>
                                            <td valign="top" colspan="2" rowspan="2" height="100%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" height="100%">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table width="100%">
                                    <caption>Messaging</caption>
                                    <thead/>
                                    <tbody>
                                        <tr>
                                            <td class="labelRight" valign="top"><label id="productPageMessageLabel" for="productPageMessage" class="labelRight">Product Page <br/>Messaging</label></td>
                                            <td>
                                                <textarea id="productPageMessage" 
                                                          cols="30" 
                                                          rows="3"></textarea>
                                            </td>
                                            <td class="labelRight" valign="top"><label id="calendarMessageLabel" for="calendarMessage" class="labelRight">Calendar Widget Messaging</label></td>
                                            <td>
                                                <textarea id="calendarMessage" 
                                                          cols="30" 
                                                          rows="3"></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="viewErrorButtonsDiv" style="display:none" class="viewErrorButtonsDiv">
                <button type="button" id="viewErrorsButton">View Errors</button>
            </div>
            
            <div class="iotwNovatorDiv">
                <input type="checkbox"
                      disabled="true"
                      id="liveEditFeed"
                      title="Send updates to Novator production web site"/>
                <label class="radioLabel" id="liveEditFeedLabel" for="liveEditFeed">Live</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="contentEditFeed"
                      title="Send updates to Novator content web site"/>
                <label class="radioLabel" id="contentEditFeedLabel" for="contentEditFeed">Content</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="testEditFeed"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="testEditFeedLabel" for="testEditFeed">Test</label>&nbsp;&nbsp;
                <input type="checkbox"
                      disabled="true"
                      id="uatEditFeed"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="uatEditFeedLabel" for="uatEditFeed">UAT</label>&nbsp;&nbsp;
            </div>
    
            <div class="iotwButtonsDiv">
               <c:choose>
				<c:when test="${searchIOTWReadOnly eq 'Y'}">
					<button type="button" id="saveButton" disabled="disabled">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
                	<button type="button" id="resetButton" disabled="disabled">Reset</button>&nbsp;&nbsp;&nbsp;&nbsp;
                	<button type="button" id="removeButton" disabled="disabled">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;
				</c:when>
				<c:otherwise>
					<button type="button" id="saveButton">Save</button>&nbsp;&nbsp;&nbsp;&nbsp;
                	<button type="button" id="resetButton">Reset</button>&nbsp;&nbsp;&nbsp;&nbsp;
                	<button type="button" id="removeButton">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;
				</c:otherwise>
				</c:choose>
                <button type="button" id="backButton">Back</button>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
    </div>
    </div>

    <div id="xcdNotifyDivId" style="display:none" class="iotwDiv">
        <div id="xcdInnerNodifyDivId" class="xcdInnerNotifyDiv">
            <div align="center">
            <strong>Alert!</strong>
            </div>
            <div id="xcdNotifyArea" 
            class="xcdNotifyArea"
            align="left" 
            >
            </div>
            </div>
            <div align="center" class="iotwButtonsDiv">
                <button type="button" id="closeErrorButton">Close</button>
            </div>
        </div>
    </div>
</form>