<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page  contentType="text/html;charset=iso-8859-1" errorPage="/error.jsp"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <title><tiles:getAsString name="title"/></title>
    <!--[if lte IE 6]>
        <style  type="text/css">
            img { behavior: url("css/iepngfix.htc") }
        </style>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/oe.css" />
    <link type="text/css" rel="stylesheet" href="css/page.css"/>
    <link type="text/css" rel="stylesheet" href="css/sorttable.css"/>
  </head>
  <body bgcolor="#ffffff" onload="init();" onunload="unload();">
    <tiles:importAttribute name="title"/>
    <tiles:insert attribute="header">
      <tiles:put beanName="title" name="title" direct="true"></tiles:put>
    </tiles:insert>
    <tiles:insert attribute="pagecontent" />
    <tiles:insert attribute="footer" />
  </body>
</html>