<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=iso-8859-1" errorPage="/error.jsp"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <title><tiles:getAsString name="title"/></title>
    <!--[if lte IE 6]>
        <style  type="text/css">
            img { behavior: url("css/iepngfix.htc") }
        </style>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="css/tabber.css">
    <link type="text/css" rel="stylesheet" href="css/sorttable.css">
    <link type="text/css" rel="stylesheet" href="css/oe.css">
    <link type="text/css" rel="stylesheet" href="css/page.css">
    <link type="text/css" rel="stylesheet" href="css/oeframe.css">
    <link type="text/css" rel="stylesheet" href="css/calendar.css">
    <style type="text/css">

      a.itemtooltip{
          position:relative;
          }
      
      a.itemtooltip:hover{z-index:25; }
      
      a.itemtooltip span{display: none}
      
      a.itemtooltip:hover span{ 
          display:block;
          position:absolute;
          float: left; 
          top:-4.5em; width:0px; margin-left: -70px;
          border:0px;
          background-color: transparent;
          color:#000000;
          text-align: left;
          padding-right: 4px;
          padding-left: 0px;
          font-family: arial,helvetica,sans-serif;
          font-size: 8pt;
          text-decoration:none;
          filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
      }

      a.tooltip2{
          position:relative;
          z-index:24}
      
      a.tooltip2:hover{z-index:25; }
      
      a.tooltip2 span{display: none}
      
      a.tooltip2:hover span{ 
          display:block;
          position:absolute;
	      float: left; 
          top:1em; right:1em; width:0px;
          border:0px;
          background-color: transparent;
          color:#000000;
          text-align: left;
          padding-right: 4px;
          padding-left: 4px;
          font-family: arial,helvetica,sans-serif;
          font-size: 8pt;
          text-decoration:none;
          filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
      }
      </style>
      
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/yahoo-dom-event.js"></script> 
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/calendar.js"></script> 
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/calendarUtil.js"></script> 
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/prototype.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/rico.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ricoCommon.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ricoEffects.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ricoComponents.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ricoBehaviors.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/sorttable.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/timer.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ftddom.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ftdxml.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ftdajax.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/ftdutils.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/prototype_helpers.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/spellcheck.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/dnis_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/product_search_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/text_search_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/color_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/order_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/payment_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/payment_type_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/price_header_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/order_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/cart_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/billing_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/action_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/framework_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/customer_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/events/navigation_events.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/address_type_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/geo_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/product_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/iotwoe_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/objects/field_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/addon_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/sourcecode_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/delivery_date_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/billing_info_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/objects/billing_info_options_object.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/ajax/validate_ajax.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/ajax/product_search_ajax.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/ajax/text_search_ajax.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/ajax/order_ajax.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/xml/sarissa.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/xml/javeline_xpath.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/dnis.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/product_search.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/text_search.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/order.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/joe_validations.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/joe_required.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/oe.js"></script>
    <script type="text/javascript" charset="iso-8859-1" src="js/joe/joe.js"></script>
    <script type="text/javascript" charset="iso-8859-1">
        var tabberOptions = {manualStartup:true, 'onLoad':tabberLoad, 'onClick':NAV_EVENTS.tabSelected };
    </script>
    <script type="text/javascript" charset="iso-8859-1" src="js/utilities/tabber.js"></script>
  </head>
  <!--
    Firefox/Mozilla browsers call onload at the beginning and of the
    page load.  So for those browsers, we use the custom event onpageshow
    to be fired after the page displays completes.
  -->
  <body id="oeframeBodyId" onload="init();" onunload="unload();" onpageshow="setup();" >
  <form action="/oeAjax.do" method="POST" >
    <jsp:include page="includes/security.jsp"/>
  <div id="leftDivId">
    <tiles:insert attribute="leftPanel"/>
  </div>
  <div id="rightDivId">
   <div id="innerRightDivId">
    <div id="scriptDivId" class="scriptDiv">
      <tiles:insert attribute="scriptPanel"/>
    </div>
    <div id="businessDiv" class="businessDiv" style="display:none">
        <tiles:insert attribute="businessPanel"/>
    </div>
    <div id="testModeDiv" style="display:none;background-color:#cc0033;color:white;font-weight:bold;text-align:center;">
        JOE is in test mode.  AJAX calls will be made to localhost
    </div>
    <div id="detailDiv" class="detailDiv" style="display:none">
        <div id="detailInnerDiv">
            <div id="accordionDiv">
                <div id="upperPanel">
                    <div id="upperHeader" class="accordionTabTitleBar">
                        Order  
                    </div>
                    <div id="upperContent" class="accordionTabContentBox">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                    <td>
                                        <tiles:insert attribute="upperPanel"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- end orderContent -->
                </div> <!-- end orderPanel -->
                <div id="lowerPanel">
                    <div id="lowerHeader" class="accordionTabTitleBar">
                        Billing  
                    </div>
                    <div id="lowerContent" class="accordionTabContentBox">
                        <table>
                            <!--caption/-->
                            <!--<thead/>-->
                            <tbody>
                                <tr>
                                    <td>
                                        <tiles:insert attribute="lowerPanel"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- end lowerContent -->
                </div> <!-- end lowerPanel -->
            </div>
        </div>
    </div>
    <div id="actionDiv" align="center">
      <tiles:insert attribute="actionButtonPanel"/>
    </div>
  </div>
    </div> <!-- end inner right div -->
  
    
  <div id="footerDivId">
      <tiles:insert attribute="footer" />
  </div>
  
  <div id="notifyDivId" style="display:none" class="notifyDiv">
      <div id="innerNodifyDivId" class="innerNotifyDiv">
        <div align="center">
        Alert!
        </div>
        <div id="notifyArea" 
             align="left" 
             style="background-color:#eeeeee;color:black;overflow:auto;height:200px;padding-left:2px">
        </div>
      </div>
  </div>
  
  <div id="logDivId" style="display:none" class="notifyDiv">
      <div id="innerLogDivId" class="innerLogDiv">
        <div align="center">
        Log Console
        </div>
        <div id="logArea" 
             align="left" 
             style="background-color:#eeeeee;color:black;overflow:auto;height:100%;padding-left:2px">
        </div>
      </div>
  </div>
  
  <div id="productSearchDivId" class="productSearchDiv" style="display:none">
      <div id="innerProductSearchDivId" class="innerProductSearchDiv">
        <tiles:insert attribute="productSearchPanel" />
      </div>
  </div>
  
  <div id="textSearchDivId" class="productSearchDiv" style="display:none">
      <div id="innerTextSearchDivId" class="innerProductSearchDiv">
        <tiles:insert attribute="textSearchPanel" />
      </div>
  </div>
  
  <div id="spellCheckDivId" class="productSearchDiv" style="display:none">
      <div id="innerSpellCheckDivId" class="innerProductSearchDiv">
        <tiles:insert attribute="spellCheckPanel" />
      </div>
  </div>

<div id="confirmationDivId" class="productSearchDiv" style="display:none">
    <div id="innerConfirmationDivId" class="innerProductSearchDiv">    
        <tiles:insert attribute="confirmationPanel" />
    </div>
</div>

  </form>  

<div id="returnToComId" style="display:none">
   <tiles:insert attribute="returnToCom" />
</div>
  
  </body>
</html>