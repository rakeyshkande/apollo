<%
    String identity = (String)request.getAttribute("identity");
    if( identity==null || identity.length()==0 ) identity="testcsr1";
    String secureURL = (String)request.getAttribute("secure-url");
    if( secureURL==null ) 
        secureURL="";
    else
        secureURL+="/";
%>

<script type="text/javascript">
  var v_repIdentity = '<%=identity%>';
  var v_serverURLPrefix = '<%=secureURL%>';
</script>

<script type="text/javascript" src="js/utilities/prototype.js"></script>
<!--
<script type="text/javascript" src="js/utilities/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/utilities/sorttable.js"></script>
-->
<script type="text/javascript" src="js/utilities/ftddom.js"></script>
<script type="text/javascript" src="js/utilities/ftdxml.js"></script>
<script type="text/javascript" src="js/utilities/ftdajax.js"></script>
<script type="text/javascript" src="js/utilities/ftdutils.js"></script>
<script type="text/javascript" src="js/xml/sarissa.js"></script>
<script type="text/javascript" src="js/xml/javeline_xpath.js"></script>
<script type="text/javascript" src="js/oe.js"></script>
<script type="text/javascript" src="js/phrases/phrases_events.js"></script>
<script type="text/javascript" src="js/phrases/phrases_object.js"></script>
<script type="text/javascript" src="js/phrases/phrases_ajax.js"></script>
<script type="text/javascript" src="js/phrases/phrases.js"></script>

<form action="/phrases.do">
    <jsp:include page="includes/security.jsp"/>
    <div class="phraseDiv">
        <div class="phraseInnerDiv">
            <div style="overflow:auto;height:500px;">
                <table border="1" align="center">
                  <caption>Card Message Phrase Maintenance</caption>
                  <thead>
                    <tr>
                      <th>Order</th>
                      <th>Phrases</th>
                      <th>Active</th>
                      <th>Remove</th>
                    </tr>
                  </thead>
                  <tbody id="listBody"/>
                </table>
            </div>
            
            <div class="phraseButtonsDiv">
                <button type="button" id="save_button">Save</button>&nbsp;&nbsp;
                <button type="button" id="add_button">Add</button>&nbsp;&nbsp;
                <button type="button" id="exit_button">Exit</button>
            </div>
        </div>
    </div>
</form>