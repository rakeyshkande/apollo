<%
	String securitytoken = "FTD_GUID_-11250782090-52816797601856315780021031414320-191469760015051548070-3371621300-1267432292118325209450536682130-18878251771400708018212123390965164-567050528235-119148031210599592323759";
%>
<html>
<head>
<title>OE Test Page</title>

<script type="text/javascript" src="js/utilities/prototype.js"></script>
<script type="text/javascript" src="js/utilities/ftdxml.js"></script>
<script type="text/javascript" src="js/xml/sarissa.js"></script>
<script type="text/javascript">

function valueChange1() {
    requestType = document.testform.AJAX_REQUEST_TYPE.value;
    if (requestType == 'AJAX_REQUEST_VALIDATE_DNIS')
        document.testform.DNIS_ID.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_SOURCE_CODE')
        document.testform.SOURCE_CODE.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_ZIP_CODE')
        document.testform.ZIP_CODE.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_PRODUCT')
        document.testform.PRODUCT_ID.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_SOURCE_CODE_SEARCH')
        document.testform.SEARCH_TYPE.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_ZIP_SEARCH')
        document.testform.CITY.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_FACILITY_SEARCH')
        document.testform.CITY.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_PRODUCT_SEARCH')
        document.testform.KEYWORDS.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_GIFT_CERTIFICATE')
        document.testform.CERTIFICATE_ID.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_AVS')
        document.testform.ADDRESS.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_FLORIST_ID')
        document.testform.FLORIST_ID.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_FLORIST_SEARCH')
        document.testform.DELIVERY_DATE.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS')
        document.testform.PRODUCT_ID.value = document.testform.value1.value;
    else if (requestType == 'AJAX_REQUEST_PHONE_SEARCH')
        document.testform.PHONE_NUMBER.value = document.testform.value1.value;
    else if (requestType == 'sendTest')
        document.testform.PRODUCT_ID.value = document.testform.value1.value;
}

function valueChange2() {
    requestType = document.testform.AJAX_REQUEST_TYPE.value;
    if (requestType == 'AJAX_REQUEST_VALIDATE_PRODUCT')
        document.testform.SOURCE_CODE.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_SOURCE_CODE')
        document.testform.DNIS_ID.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_SOURCE_CODE_SEARCH')
        document.testform.KEYWORDS.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_PRODUCT_SEARCH')
        document.testform.SOURCE_CODE.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_ZIP_SEARCH')
        document.testform.STATE.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_FACILITY_SEARCH')
        document.testform.KEYWORDS.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_AVS')
        document.testform.CITY.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_VALIDATE_FLORIST_ID')
        document.testform.PRODUCT_ID.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_FLORIST_SEARCH')
        document.testform.ZIP_CODE.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_PHONE_SEARCH')
        document.testform.COMPANY_ID.value = document.testform.value2.value;
    else if (requestType == 'AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS')
        document.testform.SOURCE_CODE.value = document.testform.value2.value;
}

function valueChange3() {
    requestType = document.testform.AJAX_REQUEST_TYPE.value;
    if (requestType == 'AJAX_REQUEST_VALIDATE_FLORIST_ID')
        document.testform.ZIP_CODE.value = document.testform.value3.value;
    else if (requestType == 'AJAX_REQUEST_FLORIST_SEARCH')
        document.testform.PRODUCT_ID.value = document.testform.value3.value;
    else if (requestType == 'AJAX_REQUEST_PRODUCT_SEARCH') {
        document.testform.DELIVERY_DATE.value = document.testform.value3.value;
        document.testform.ZIP_CODE.value = '60515';
    }
}

function createOrder() {
    var doc = FTD_XML.createXmlDocument("order");
    var root = doc.documentElement;

    header = doc.createElement("header");
    root.appendChild(header);

    work = doc.createElement("source-code");
    header.appendChild(work);
    txtNode = doc.createTextNode("10577");
    work.appendChild(txtNode);

    work = doc.createElement("dnis-id");
    header.appendChild(work);
    txtNode = doc.createTextNode("3333");
    work.appendChild(txtNode);

    work = doc.createElement("order-created-by-id");
    header.appendChild(work);
    txtNode = doc.createTextNode("tschmig");
    work.appendChild(txtNode);

    work = doc.createElement("origin");
    header.appendChild(work);
    txtNode = doc.createTextNode("FTDP");
    work.appendChild(txtNode);

    temp=doc.createElement("buyer-first-name");
    header.appendChild(temp);
    txtNode = doc.createTextNode("Tim");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-last-name");
    header.appendChild(temp);
    txtNode = doc.createTextNode("Schmig");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-address1");
    header.appendChild(temp);
    txtNode = doc.createTextNode("3113 Woodcreek Dr");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-city");
    header.appendChild(temp);
    txtNode = doc.createTextNode("Downers Grove");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-state");
    header.appendChild(temp);
    txtNode = doc.createTextNode("IL");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-postal-code");
    header.appendChild(temp);
    txtNode = doc.createTextNode("60515");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-country");
    header.appendChild(temp);
    txtNode = doc.createTextNode("US");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-daytime-phone");
    header.appendChild(temp);
    txtNode = doc.createTextNode("6307192939");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-evening-phone");
    header.appendChild(temp);
    txtNode = doc.createTextNode("6302484978");
    temp.appendChild(txtNode);

    temp=doc.createElement("buyer-email-address");
    header.appendChild(temp);
    txtNode = doc.createTextNode("tschmig@ftdi.com");
    temp.appendChild(txtNode);

    temp=doc.createElement("cc-number");
    header.appendChild(temp);
    txtNode = doc.createTextNode("4444333322221111");
    temp.appendChild(txtNode);
    
    temp=doc.createElement("cc-type");
    header.appendChild(temp);
    txtNode = doc.createTextNode("VI");
    temp.appendChild(txtNode);
    
    temp=doc.createElement("cc-exp-date");
    header.appendChild(temp);
    txtNode = doc.createTextNode("10/2008");
    temp.appendChild(txtNode);

    work = doc.createElement("items");
    root.appendChild(work);
    
    itemNode = doc.createElement("item");
    work.appendChild(itemNode);
    
    temp=doc.createElement("productid");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("3072");
    temp.appendChild(txtNode);

    temp=doc.createElement("product-price");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("39.99");
    temp.appendChild(txtNode);

    temp=doc.createElement("delivery-date");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("12/21/2007");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-first-name");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("Jane");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-last-name");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("McGrath");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-address1");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("508 Meadow Wood Dr.");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-city");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("Joliet");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-state");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("IL");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-postal-code");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("60431");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-country");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("US");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-phone");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("8157294688");
    temp.appendChild(txtNode);

    temp=doc.createElement("size-indicator");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("B");
    temp.appendChild(txtNode);

    temp=doc.createElement("card-message");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("Happy Birthday!");
    temp.appendChild(txtNode);

    temp=doc.createElement("shipping-method");
    itemNode.appendChild(temp);
    txtNode = doc.createTextNode("");
    temp.appendChild(txtNode);

    addonsNode = doc.createElement("add-ons");
    itemNode.appendChild(addonsNode);
    addonNode = doc.createElement("add-on");
    addonsNode.appendChild(addonNode);
    temp=doc.createElement("id");
    addonNode.appendChild(temp);
    txtNode = doc.createTextNode("A");
    temp.appendChild(txtNode);
    temp=doc.createElement("quantity");
    addonNode.appendChild(temp);
    txtNode = doc.createTextNode("1");
    temp.appendChild(txtNode);

    addon2Node = doc.createElement("add-on");
    addonsNode.appendChild(addon2Node);
    temp=doc.createElement("id");
    addon2Node.appendChild(temp);
    txtNode = doc.createTextNode("RC66");
    temp.appendChild(txtNode);
    temp=doc.createElement("quantity");
    addon2Node.appendChild(temp);
    txtNode = doc.createTextNode("1");
    temp.appendChild(txtNode);

    item2Node = doc.createElement("item");
    work.appendChild(item2Node);
    
    temp=doc.createElement("productid");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("BB02");
    temp.appendChild(txtNode);

    temp=doc.createElement("product-price");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("46.99");
    temp.appendChild(txtNode);

    temp=doc.createElement("shipping-method");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("2F");
    temp.appendChild(txtNode);

    temp=doc.createElement("item-source-code");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("7545");
    temp.appendChild(txtNode);

    temp=doc.createElement("delivery-date");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("12/22/2007");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-first-name");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("Annie");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-last-name");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("Schmig");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-address1");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("508 Meadow Wood Dr.");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-city");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("Honolulu");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-state");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("HI");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-postal-code");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("96849");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-country");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("US");
    temp.appendChild(txtNode);

    temp=doc.createElement("recip-phone");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("8157294688");
    temp.appendChild(txtNode);

    temp=doc.createElement("card-message");
    item2Node.appendChild(temp);
    txtNode = doc.createTextNode("Hello World.");
    temp.appendChild(txtNode);

    //alert(FTD_XML.toString(doc));
    return doc;
}
    
function doSubmit() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = document.testform.AJAX_REQUEST_TYPE.value;
    root.setAttributeNode(attr);
    
    var p = doc.createElement('param');
    p.setAttribute('name', 'DNIS_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.DNIS_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'SOURCE_CODE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.SOURCE_CODE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'ZIP_CODE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.ZIP_CODE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'PRODUCT_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.PRODUCT_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'COUNTRY_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.COUNTRY_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'COMPANY_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.COMPANY_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'SEARCH_TYPE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.SEARCH_TYPE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'KEYWORD');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.KEYWORDS.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'CITY');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.CITY.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'STATE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.STATE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'ADDRESS_TYPE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.ADDRESS_TYPE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'DELIVERY_DATE');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.DELIVERY_DATE.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'CERTIFICATE_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.CERTIFICATE_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'ADDRESS');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.ADDRESS.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'FIRM_NAME');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.FIRM_NAME.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'PHONE_NUMBER');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.PHONE_NUMBER.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'FLORIST_ID');
    root.appendChild(p);
    txtNode = doc.createTextNode(document.testform.FLORIST_ID.value);
    p.appendChild(txtNode);

    var p = doc.createElement('param');
    p.setAttribute('name', 'ORDER_XML');
    root.appendChild(p);
    orderDoc = createOrder();
    cdataNode = doc.createCDATASection(FTD_XML.toString(orderDoc));
    p.appendChild(cdataNode);

    if (document.testform.AJAX_REQUEST_TYPE.value != "AJAX_REQUEST_GET_INTRO_DATA") {
        var p = doc.createElement('echo');
        p.setAttribute('timestamp', '08/07/2007');
        root.appendChild(p);
        txtNode = doc.createTextNode("Hello");
        p.appendChild(txtNode);
    }
    
    //alert(FTD_XML.toString(doc));
    document.testform.comments.value = '';
    new Ajax.Request('oeAjax.do', {parameters: 'AJAX_XML='+FTD_XML.toString(doc), onComplete: getDataCallback});
}

function getDataCallback(transport) {
    var xmlStr = transport.responseText.replace(/\s+$/, "");
    document.testform.comments.value = xmlStr;
}

</script>
</head>

<body>

<form name="testform" action="" method="post">

<input type="hidden" name="DNIS_ID" value="">
<input type="hidden" name="SOURCE_CODE" value="">
<input type="hidden" name="ZIP_CODE" value="">
<input type="hidden" name="PRODUCT_ID" value="">
<input type="hidden" name="COUNTRY_ID" value="US">
<input type="hidden" name="COMPANY_ID" value="FTD">
<input type="hidden" name="KEYWORDS" value="">
<input type="hidden" name="SEARCH_TYPE" value="">
<input type="hidden" name="CITY" value="">
<input type="hidden" name="STATE" value="IL">
<input type="hidden" name="ADDRESS_TYPE" value="F">
<input type="hidden" name="DELIVERY_DATE" value="">
<input type="hidden" name="CERTIFICATE_ID" value="">
<input type="hidden" name="ADDRESS" value="">
<input type="hidden" name="FIRM_NAME" value="">
<input type="hidden" name="FLORIST_ID" value="">
<input type="hidden" name="PHONE_NUMBER" value="">

<!-- Main Table -->
<table width="98%" border="0" cellpadding="2" cellspacing="2">
    <caption/>
    <thead/>
    <tbody>
        <tr>
            <td>Request Type:</td>
            <td>
                <select name="AJAX_REQUEST_TYPE">
                    <option value="AJAX_REQUEST_GET_INTRO_DATA">AJAX_REQUEST_GET_INTRO_DATA</option>
                    <option value="AJAX_REQUEST_VALIDATE_PRODUCT">AJAX_REQUEST_VALIDATE_PRODUCT</option>
                    <option value="AJAX_REQUEST_VALIDATE_DNIS">AJAX_REQUEST_VALIDATE_DNIS</option>
                    <option value="AJAX_REQUEST_VALIDATE_ZIP_CODE">AJAX_REQUEST_VALIDATE_ZIP_CODE</option>
                    <option value="AJAX_REQUEST_VALIDATE_SOURCE_CODE">AJAX_REQUEST_VALIDATE_SOURCE_CODE</option>
                    <option value="AJAX_REQUEST_SOURCE_CODE_SEARCH">AJAX_REQUEST_SOURCE_CODE_SEARCH</option>
                    <option value="AJAX_REQUEST_ZIP_SEARCH">AJAX_REQUEST_ZIP_SEARCH</option>
                    <option value="AJAX_REQUEST_FACILITY_SEARCH">AJAX_REQUEST_FACILITY_SEARCH</option>
                    <option value="AJAX_REQUEST_PRODUCT_SEARCH">AJAX_REQUEST_PRODUCT_SEARCH</option>
                    <option value="AJAX_REQUEST_FLORIST_SEARCH">AJAX_REQUEST_FLORIST_SEARCH</option>
                    <option value="AJAX_REQUEST_PHONE_SEARCH">AJAX_REQUEST_PHONE_SEARCH</option>
                    <option value="AJAX_REQUEST_VALIDATE_GIFT_CERTIFICATE">AJAX_REQUEST_VALIDATE_GIFT_CERTIFICATE</option>
                    <option value="AJAX_REQUEST_VALIDATE_AVS">AJAX_REQUEST_VALIDATE_AVS</option>
                    <option value="AJAX_REQUEST_VALIDATE_FLORIST_ID">AJAX_REQUEST_VALIDATE_FLORIST_ID</option>
                    <option value="AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS">AJAX_REQUEST_GET_CROSS_SELL_PRODUCTS</option>
                    <option value="AJAX_REQUEST_CALCULATE_ORDER_TOTALS">AJAX_REQUEST_CALCULATE_ORDER_TOTALS</option>
                    <option value="AJAX_REQUEST_SAVE_CART">AJAX_REQUEST_SAVE_CART</option>
                    <option value="sendTest">Send Test</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Value1:</td>
            <td><input type="text" size="15" name="value1" value="" onChange="valueChange1()"></td>
        </tr>
        <tr>
            <td>Value2:</td>
            <td><input type="text" size="15" name="value2" value="" onChange="valueChange2()"></td>
        </tr>
        <tr>
            <td>Value3:</td>
            <td><input type="text" size="15" name="value3" value="" onChange="valueChange3()"></td>
        </tr>
        <tr>
            <td><button onClick="javascript:doSubmit()">Submit</button></td>
            <td></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <TD width="15%" class="Header3"> Maintenance </td>
            <TD width="85%"> &nbsp; </td>
        </tr>
        <tr>
                                <td> &nbsp; </td>
            <td>
                <a href="oe.do?securitytoken=<%=securitytoken%>">
                    Order Entry
                </a>                     
            </td>
        </tr>                        
        <tr>
            <td> <hr> </td>
            <td> &nbsp; </td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea name="comments" cols=120 rows=25></textarea>
            </td>
        </tr>
    </tbody>
</table>

</form>
</body>
</html>
