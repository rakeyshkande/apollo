// <![CDATA[
var UTILS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || UTILS.prototypeVersion < 1.6)
      throw("UTILS requires the Prototype JavaScript framework >= 1.6");


UTILS.convertHtmlEntity = function(input) {
     var div = document.createElement('div');
     div.innerHTML = input;
     return div.innerHTML;
}




UTILS.stripNonDigits = function(n) {
    var s = String(n);
    var re = /\D/g;
    return s.replace(re,"");
}

UTILS.formatStringToPrice = function(price) {
    //alert('Before: '+price);
    price = price.replace(/$/g,'');
    price = price.replace(/,/g,'');
    if( isNaN(price) ) {
        var retval="0.00";
    } else {
        try {
            price = UTILS.trim(price);
            if( UTILS.isEmpty(price)==true ) {
                retval = "0.00";
            } else {
                var decimalIdx=price.indexOf(".");
                if( decimalIdx==-1 ) { //whole number
                    retval = price+".00";
                } else if( price.length-decimalIdx==3 ) { 
                    //already properly formatted
                    retval = price;
                } else {
                    var testFraction = price.substr(decimalIdx);
                    if( testFraction.length==1 ) {
                        retval = price+"00";    //decimal at the end of the string
                    } else if( testFraction.length==2 ) { //Only one decimal position showing
                        retval = price+"0";
                    } else if( testFraction.length>=3 ) { //More than three decimal positions
                        retval = price.substring(0,decimalIdx)+testFraction.substring(0,2);
                    }
                }
            }
        } catch (err) {
            //alert(err.description);
            retval = "0.00";
        }
    }
    
    return retval; 
}

/*
 * Formats a date object to a string in yyyymmdd format
 */
UTILS.formatDateToServerString = function(date) {
    var retval = '';
    try {
        retval += date.getFullYear();
        var str = (date.getMonth()+1)+'';
        if( str.length==1) str = '0'+str;
        retval += str;
        
        str = (date.getDate())+'';
        if( str.length==1 ) str = '0'+str;
        retval+=str;
    } catch (err) {
        return UTILS.formatDateToServerString(new Date());
    }
    
    return retval; 
}

UTILS.rtrim = function(sInString) {
    if( Object.isUndefined(sInString) || sInString == null ) {
        return '';
    }
    
    return sInString.replace( /\s+$/g, "" );// strip trailing
}

UTILS.ltrim = function(sInString) {
    if( Object.isUndefined(sInString) || sInString == null ) {
        return '';
    }
    
    return sInString.replace( /^\s+/g, "" );// strip leading
}

UTILS.trim = function(sInString) {
    return UTILS.ltrim(UTILS.rtrim(sInString));
}

UTILS.parseZip = function(sInZip, countryId) {
	var zipCode = sInZip;
	if( (countryId=='US' || countryId=='United States') && zipCode!=null && zipCode.length>5 ) {
        zipCode = zipCode.substr(0,5);
    } else if( (countryId=='CA' || countryId=='Canada') && zipCode!=null && zipCode.length>3 ) {
        zipCode = zipCode.substr(0,3);
    }
	return zipCode;
}

UTILS.convertToJavaScript = function(code) {
    var gs = this; // global scope reference
    var retval;
    
    if (window.execScript) {
        code = 'window.__newobj = '+code;
        window.execScript(code); // eval in global scope for IE
        retval = window.__newobj;
    } else {
        //gs.eval ? gs.eval(code) : eval(code);
        if( gs.eval ) {
            retval=gs.eval(code);
        } else {
            retval=eval(code);
        }
            
    }
    
    return retval;
}

UTILS.second = 1000;
UTILS.minute = UTILS.second*60;
UTILS.hour = UTILS.minute*60;
UTILS.day = UTILS.hour*24;

UTILS.addDay = function(srcDate,days) {
    srcDate.setTime(srcDate.getTime()+(UTILS.day*days));
    return srcDate;
}

UTILS.isEmpty = function(strValue) {
    if( strValue==null ) return true;
    if( Object.isUndefined(strValue)==true ) return true;
    if( Object.isString(strValue)==false ) return false;
    
    strValue = UTILS.trim(strValue);
    if( strValue.length==0 ) return true;
    
    return false;
}

UTILS.boldWrapper = function(strValue) {
    if( Object.isUndefined(strValue) || strValue==null || strValue.length==0 ) return '';
    
    return '<b>'+strValue+'</b>';
}

/*
 * Formats a date object to a string in DOW Mon d format
 */
UTILS.formatToDisplayDeliveryDate = function(sourceDate) {
    if( sourceDate==null ) {
        return '';
    }
    
    var dow = sourceDate.getDay();
    var date = sourceDate.getDate();
    var month = sourceDate.getMonth();
    
    var retval = '';
    
    switch(dow) {
        case 0:
            retval += 'Sun ';
            break;
        case 1:
            retval += 'Mon ';
            break;
        case 2:
            retval += 'Tue ';
            break;
        case 3:
            retval += 'Wed ';
            break;
        case 4:
            retval += 'Thu ';
            break;
        case 5:
            retval += 'Fri ';
            break;
        case 6:
            retval += 'Sat ';
            break;
    }
    
    switch(month) {
        case 0:
            retval += 'Jan ';
            break;
        case 1:
            retval += 'Feb ';
            break;
        case 2:
            retval += 'Mar ';
            break;
        case 3:
            retval += 'Apr ';
            break;
        case 4:
            retval += 'May ';
            break;
        case 5:
            retval += 'Jun ';
            break;
        case 6:
            retval += 'Jul ';
            break;
        case 7:
            retval += 'Aug ';
            break;
        case 8:
            retval += 'Sep ';
            break;
        case 9:
            retval += 'Oct ';
            break;
        case 10:
            retval += 'Nov ';
            break;
        case 11:
            retval += 'Dec ';
            break;
    }
    
    if( date<10 ) retval+=0;
    
    retval+=date;
    
    return retval;
}

UTILS.formatToServerDate = function(sourceDate) {
    if( sourceDate==null ) {
        sourceDate = new Date();
    }
    
    var day = sourceDate.getDate()+'';
    var month = (sourceDate.getMonth()+1)+'';
    var year = sourceDate.getFullYear()+'';
    
    var retval = year+'';
    if( month<10 ) retval+='0';
    retval+=month;
    
    if( day<10 ) retval+='0';
    retval+=day
    
    return retval;  
}

/*
 * Formats a date object to a string mm/dd/yyyy format
 */
UTILS.formatToMMDDYYYY = function(sourceDate,padZeros) {
    if( sourceDate==null ) {
        sourceDate = new Date();
    }
    
    var day = sourceDate.getDate()+'';
    var month = (sourceDate.getMonth()+1)+'';
    var year = sourceDate.getFullYear()+'';
    
    var retval = '';
    if( padZeros==true && month<10 ) retval+='0';
    retval+=month;
    retval+='/';
    
    if( padZeros==true && day<10 ) retval+='0';
    retval+=day
    retval+='/';
    retval+=year;
    
    return retval;  
}

UTILS.formatServerDateStrToMMDDYYY = function(dateString) {
    if( dateString==null || dateString.length==0 ) {
        return null;
    }
    
    var yearStr = dateString.substring(0,4);
    var monthStr = dateString.substring(4,6);
    var dayStr = dateString.substring(6);
    
    return monthStr+'/'+dayStr+'/'+yearStr;
}

UTILS.serverDateToDate = function(dateString) {
    if( dateString==null || dateString.length==0 ) {
        return null;
    }
    
    var yearStr = dateString.substring(0,4);
    var monthStr = dateString.substring(4,6)-1;
    var dayStr = dateString.substring(6);
    
    return new Date(yearStr,monthStr,dayStr);
}

UTILS.toString = function(value) {
    if( value==null ) return '';
    
    return value+'';
}

UTILS.mod10Check = function(creditCardNumber) {
    //convert to a string if the value passed is a number
    if( Object.isNumber(creditCardNumber) ) creditCardNumber+='';  
    creditCardNumber = UTILS.trim(creditCardNumber);
    
    if(creditCardNumber.length==0) return false;
    
    try {
        var testVal=0;
        var doubleVal=0;
        var count=0;
        for( var idx=creditCardNumber.length-1; idx>=0; idx-- ) {
            if( count%2==0 ) {
                testVal+=parseInt(creditCardNumber.charAt(idx));
            } else {
                doubleVal=(parseInt(creditCardNumber.charAt(idx))*2)+'';
                for( var idx2=0; idx2<doubleVal.length; idx2++ ) {
                    testVal+=parseInt(doubleVal.charAt(idx2));
                }
            }
            count++;
        }
        
        testVal+='';
        
        if( testVal.charAt(testVal.length-1)=='0' ) return true;
    } catch(err) {
        alert(err.description);
    }
    
    return false;
}

UTILS.arrayLength = function(associativeArray) {
    var ctr=0;
    for (ent in associativeArray)
        if( Object.isString(associativeArray[ent]) ) ctr++;
        
    return ctr;
}

UTILS.hasEntries = function(associativeArray) {
    for (ent in associativeArray)
        if( Object.isString(associativeArray[ent]) ) return true;
        
    return false;
}

UTILS.findValueInArray = function(array,searchValue) {
    if( Object.isArray(array)==true && UTILS.isEmpty(searchValue)==false ) {
        for( var idx=0; idx<array.length; idx++ ) {
            if(array[idx]==searchValue) return true;
        }
    }
    
    return false;
}

UTILS.getCCTypeFromNumber = function(creditCardNumber) {
    if( Object.isNumber(creditCardNumber) ) 
        creditCardNumber +='';
    else {   
        //Remove the spaces
        creditCardNumber = UTILS.trim(creditCardNumber);
        if(creditCardNumber.length==0) return 'Invalid credit card number.  An empty credit card number was entered.';
        
        //Validate that the card number is number
        try {
            parseInt(creditCardNumber);  
        } catch (err) {
            return 'Invalid credit card number.  Entered credit card number contains non-digit characters.  Try entering the credit card without dashes or spaces.';
        }   
    }
    
    var idBegin = parseInt( creditCardNumber.substring(0,2) );
    //Get the first 5 digits of the id
    var first4 = parseInt(creditCardNumber.substring(0,4));
    var first5 = parseInt(creditCardNumber.substring(0,5));
    var first6 = parseInt(creditCardNumber.substring(0,6));
    var ccType;

    if( idBegin==30 || idBegin==36 || idBegin == 38 ) {
        ccType = 'DC';
    } else if( idBegin==34 || idBegin==37 ) {
        ccType = 'AX';
    } else {
        if( idBegin>=40 && idBegin <=49 ) {
            ccType = 'VI';
        } else if( idBegin >=50 && idBegin <=59 ) {
            ccType = 'MC';
        } else if( idBegin >=20 && idBegin <=29 ) {
            ccType = 'MC';
        } else if(first4 == 6019) {
            ccType = 'MS';
        } else if( idBegin>=60 && idBegin <=69 ) {
            ccType = 'DI';
        } else {
            return 'Invalid credit card number.  Unable to determine credit card type.  Please confirm credit card number and re-enter.';
        }
    }
    
    //Set the length variable based on the credit card type
    var ccLength = 16;
    if (ccType=='VI'){
        if(creditCardNumber.length==16)
            ccLength = 16;
        if(creditCardNumber.length==13)
            ccLength = 13;
    } else if( ccType=='MC' || ccType=='DI' ) {
        ccLength = 16;
    } else if( ccType=='AX' ) {
        ccLength = 15;
    } else if( ccType=='DC' ) {
        ccLength = 14;
    } else if( ccType=='MS' ) {  // AAFES
        ccLength = 16;
    }

    //Compare the variable to the length of the id
    if( ccLength!=creditCardNumber.length ) {
        return 'Invalid credit card length for credit card type '+ccType;
    }
    
    //VISA
    if( ccType=='VI' ) {
        if( first5 < 40000 || first6 > 499999 ) {
            return false;
        }
    }
    //MASTER CARD
    else if( ccType=='MC' ) {
        if (((first5 < 50000) || (first5 > 59999)) && ((first5 < 20000) || (first5 > 29999))) {
            return false;
        }
    }
    //DISCOVER CARD
    else if( ccType=='DI' ) {
        if( first5 < 60000 || first5 > 69999 ) {
            return false;
        }
    }
    //AMERICAN EXPRESS
    else if( ccType=='AX' ) {
        if( first5 < 34000 || first5 > 37999 ) {
            return false;
        } else if( first5 > 34999 && first5 < 37000 ) {
            return false;
        }
    }
    //DINERS CLUB
    else if( ccType=='DC' ) { 
        if( first5 < 30000 || first5 > 38999 ) {
            return false;
        }
        if( first5 > 30999 && first5 < 36000 ) {
            return false;
        }
        if( first5 > 36999 && first5 < 38000 ) {
            return false;
        }
    }
    // AAFES - Military Star has no additional checks, so, assume error
    else if( ccType!='MS' ) {
        return 'Invalid number of digits for a credit card of type '+ccType+'.';
    }
    
    return ccType;
}

/*
 * Adds comma formatting to a number
 */
UTILS.addCommas = function(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

/*
 * Formats a date object to a string in hh:mm:ss format
 */
UTILS.formatDateToTimeString = function(date) {
    var retval = '';
    try {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var retval = hours + ':' + minutes + ':' + seconds;
    } catch (err) {
        return UTILS.formatDateToTimeString(new Date());
    }
    
    return retval; 
}

// ]]>
