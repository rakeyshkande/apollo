// <![CDATA[
var FTD_XML = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || FTD_XML.prototypeVersion < 1.6)
      throw("FTD XML Utility requires the Prototype JavaScript framework >= 1.6");
      
FTD_XML.parseXmlString = function(xmlString)
{
    var xmlDoc;
    // code for IE
    if (window.ActiveXObject)
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async=false;
        xmlDoc.loadXML(xmlString);
      }
    // code for Mozilla, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        var parser=new DOMParser();
        xmlDoc=parser.parseFromString(xmlString,"text/xml");
    }
    else
    {
        alert('Your browser cannot handle this script');
    }
    
    return xmlDoc;
}

FTD_XML.parseXmlFile = function(fileName)
{
    var xmlDoc;
    // code for IE
    if (window.ActiveXObject)
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async=false;
        xmlDoc.load(fileName);
      }
    // code for Mozilla, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        xmlDoc= document.implementation.createDocument("","",null);
        xmlDoc.load(fileName);
    }
    else
    {
        alert('Your browser cannot handle this script');
    }
    
    return xmlDoc;
}

FTD_XML.createXmlDocument = function(rootTagName, namespaceURL) { 
    if (!rootTagName) rootTagName = ""; 
    if (!namespaceURL) namespaceURL = ""; 
    if (document.implementation && document.implementation.createDocument) { 
        // This is the W3C standard way to do it 
        return document.implementation.createDocument(namespaceURL, rootTagName, null); 
    } 
    else 
    { 
        // This is the IE way to do it 
        // Create an empty document as an ActiveX object 
	// If there is no root element, this is all we have to do 
	var doc = new ActiveXObject("Microsoft.XMLDOM"); 
	// If there is a root tag, initialize the document 
	if (rootTagName) { 
            // Look for a namespace prefix 
            var prefix = ""; 
            var tagname = rootTagName; 
            var p = rootTagName.indexOf(':'); 
            if (p != -1) { 
                prefix = rootTagName.substring(0, p); 
                tagname = rootTagName.substring(p+1); 
            } 
            // If we have a namespace, we must have a namespace prefix 
            // If we don't have a namespace, we discard any prefix 
            if (namespaceURL) { 
                if (!prefix) prefix = "a0"; // What Firefox uses 
            } 
            else 
            {
                prefix = "";
            }
            // Create the root element (with optional namespace) as a 
            // string of text 
            var text = "<" + (prefix?(prefix+":"):"") +  tagname + 
            (namespaceURL ?(" xmlns:" + prefix + '="' + namespaceURL +'"') :"") + "/>"; 
            // And parse that text into the empty document 
            doc.loadXML(text); 
        }
        
        return doc; 
    }
}

FTD_XML.selectNodeText = function(xmlNode,tagName) {
    var retval = null;
    
    try {
        var node =  xmlNode.getElementsByTagName(tagName)[0];
        if( node==null ) {
            retval = null;
        } else {
            
            for( var idx=0; idx<node.childNodes.length; idx++ ) {
                var childNode = node.childNodes[idx];
                if( childNode==null ) continue;
                if( UTILS.isEmpty(childNode.nodeValue)==false ) {
                    retval = childNode.nodeValue;
                    break;
                }
            }
        }
    } catch (err) {
        retval = null;
    }
    
    return retval;
}

FTD_XML.getInnerHTMLText = function(node) {
    var retval = null;
    
    try {
        if( node==null ) {
            retval = null;
        } else {
            for( var idx=0; idx<node.childNodes.length; idx++ ) {
                var childNode = node.childNodes[idx];
                if( childNode==null ) continue;
                if( UTILS.isEmpty(childNode.nodeValue)==false ) {
                    retval = childNode.nodeValue;
                    break;
                }
            }
        }
    } catch (err) {
        retval = null;
    }
    
    return retval;
}

FTD_XML.getAttributeText = function(xmlNode,attributeName) {
    var retval = null;
    
    try {
        if( xmlNode!=null && attributeName!=null ) {
            var attrib = xmlNode.getAttributeNode(attributeName);
            
            if(attrib!=null) {
                retval = attrib.firstChild.nodeValue;
            }
        }
    } catch (err) {
        retval = null;
    }
    
    return retval;
}

FTD_XML.getChildElements = function(xmlNode) {
    var retval = new Array();
    
    for( var idx=0; idx< xmlNode.childNodes.length; idx++ ) {
        if( xmlNode.childNodes[idx].nodeType==1 ) {
            retval.push(xmlNode.childNodes[idx]);
        }
    }
    
    return retval;
}

FTD_XML.toString = function(xmlNode) {
    return new XMLSerializer().serializeToString(xmlNode);
}

FTD_XML.createElementWithText = function( xmlDocument,elementName,elementText ) {
    var el = xmlDocument.createElement(elementName);
    if( elementText!=null && elementText.length>0) {
        el.appendChild(xmlDocument.createTextNode(elementText+''));
    }
    
    return el;
}

FTD_XML.removeTextNodes = function(xmlDoc) {
	var reBlank = /^\s*$/;
	var walk = function(node) {
	    var child, next;
	    switch (node.nodeType) {
	        case 3: // Text node
	            if (reBlank.test(node.nodeValue)) {
	                node.parentNode.removeChild(node);
	            }
	            break;
	        case 1: // Element node
	        case 9: // Document node
	            child = node.firstChild;
	            while (child) {
	                next = child.nextSibling;
	                walk(child);
	                child = next;
	            }
	            break;
	    }
	}
	walk(xmlDoc); // Where xmlDoc is your XML document instance
}
// ]]>
