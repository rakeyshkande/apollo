var deselectMap = new Object();

/* Handles Yahoo calendar select events.
 * To use this handler the name of the calendar object must be based on the data 
 * field associated with the calendar.  Add 'Cal' to the data field name to come 
 * up with the calendar object name.  For example, if the data field is named 
 * startDate the calendar object will be named startDateCal.
 */
var calendarDateSelectHandler = function(type, args, obj) {
    var dataField;
    var formattedDate = '';

    // Obtain name of data field
    dataField = obj.id.substring(0, obj.id.indexOf("Cal"));
    
    /* If the calendar is MULTI_SELECT the data field must be a select.
     * Else it is a text field.
     */
    if(obj.cfg.getProperty("MULTI_SELECT")) {
        // Obtain selected dates
        var date;
        var dates = obj.getSelectedDates();
       
        $(dataField).options.length = 0;
        for(var i = 0; i < dates.size(); i++) {
            date = dates[i];
            formattedDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getYear(); 
            $(dataField).options[i] = new Option(formattedDate, formattedDate, false, false);
        }
    } else {
        /* Instead of making the textfield editable and implementing REGEX validation
         * the calendar will be used to deselect a chosen date.  The logic is fairly 
         * simple.
         */

        var fullDate = args[0] + '';
        // If a value is deselected set the deselected date
        if(type == 'deselect'){
            if($(dataField).value != ''){
                deselectMap[dataField] = fullDate;
            }
        } // If the value selected matches the deselected value the value was really deselected
        else if(type == 'select' && deselectMap[dataField] == fullDate){
            $(dataField).value = '';
            deselectMap[dataField] = '';
            clearDates(obj);
        } // If a value was selected format the value and set the textfield 
        else{
            var month, date, year;
            year = fullDate.substring(0, fullDate.indexOf(","));
            fullDate = fullDate.substring(fullDate.indexOf(",") + 1);
            month = fullDate.substring(0, fullDate.indexOf(","));
            fullDate = fullDate.substring(fullDate.indexOf(",") + 1);
            date = fullDate;
            formattedDate = month + "/" + date + "/" + year;

            deselectMap[dataField] = '';
            $(dataField).value = formattedDate;
        }
        
        // Hide the calendar
        obj.hide();
    }
}

// Sets generic calendar properties
function setGenericCalendarProperties(calObj) {
    calObj.cfg.setProperty("close", true);
    calObj.cfg.setProperty("mindate", new Date());
    calObj.addRenderer("12/25", calObj.renderBodyCellRestricted);
    calObj.addRenderer("7/4", calObj.renderBodyCellRestricted);
    calObj.addRenderer("1/1", calObj.renderBodyCellRestricted);
}

// Sets generic calendar properties
function setGenericCalendarPropertiesNoMin(calObj) {
    calObj.cfg.setProperty("close", true);
    calObj.addRenderer("12/25", calObj.renderBodyCellRestricted);
    calObj.addRenderer("7/4", calObj.renderBodyCellRestricted);
    calObj.addRenderer("1/1", calObj.renderBodyCellRestricted);
}

// Manages if the calendar is displayed
function manageCalendarDisplay(calContainer, calObj) {
    if(calContainer.style.display != '' && 
        calContainer.style.display != 'none') {
        calObj.hide();
    }
    else {
        calObj.show();
    }
}

function clearDates(calObj) {
    calObj.deselectAll();
    calObj.render();
}

function setDates(calObj, dates) {
    var datesSelected = '';
    for(var i = 0; i < dates.length; i++) {
        datesSelected = datesSelected + dates[i] + ",";
    }

    if(datesSelected != null && datesSelected.length != 0) {
        datesSelected = datesSelected.substring(0, datesSelected.length - 1);
        calObj.select(datesSelected);

        var firstDate = dates[0];
        var month = firstDate.substring(0, firstDate.indexOf("/"));
        var year = firstDate.substring(firstDate.lastIndexOf("/") + 1);
        var date = month + "/" + year;
        calObj.cfg.setProperty("pagedate", date)

        calObj.render();    
    }
}

function resetDeselectMap(){
    deselectMap = null;
    deselectMap = new Object();   
}