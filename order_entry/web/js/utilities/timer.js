// <![CDATA[
var TIMER = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("TIMER requires the Prototype JavaScript framework >= 1.5");

TIMER.Timer = Class.create();

TIMER.Timer.prototype = {

    initialize: function(delayInMilliseconds,callbackFunction,counterElement) {
        this.callTimer = null;
        this.timerCount = 0;
        this.hoursCount = 0;
        this.minutesCount = 0;
        this.secondsCount = 0;
        this.running = false;
        this.delay = delayInMilliseconds;
        this.callbackFunction = callbackFunction;
        
        if( counterElement!=undefined && counterElement!=null ) {
            if (typeof counterElement == 'string') {
                this.counterElement = $(counterElement);
            } else {
                this.counterElement = counterElement;
            }
        }
    },
    
    startTimer: function() {
        if( this.running==false ) {
            this.running = true;
            this.timerCount = -1;
            this.hoursCount = 0;
            this.minutesCount = 0;
            this.secondsCount = -1;
            this.setTimer();
        }
    },
    
    setTimer: function() {
        if( this.running ) {
            this.timerCount=this.timerCount+1;
            this.secondsCount=this.secondsCount+1;
            
            if( this.secondsCount==60 ) {
              this.minutesCount=this.minutesCount+1;
              this.secondsCount=0;
            }
              
            if( this.minutesCount==60 ) {
                this.hoursCount=this.hoursCount+1;
                this.minutesCount=0;
            }
            
            if( this.counterElement!=undefined && this.counterElement!=null ) {
                this.counterElement.innerHTML=this.timerCount;
            }
            this.callTimer=setTimeout(this.callbackFunction,this.delay);
        }
    },
    
    stopTimer: function() {
        if( this.callTimer!=null ) {
            clearTimeout(this.callTimer);
        }
        this.running = false;
    },
    
    isRunning: function() {
      return this.running;
    }
};
// ]]>

