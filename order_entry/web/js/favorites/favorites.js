// <![CDATA[
var FAVORITES = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || FAVORITES.prototypeVersion < 1.6)
      throw("favorites.js requires the Prototype JavaScript framework >= 1.6");

var v_notifyTimer;
var v_notifySeconds = 10;
//Begin element colors
var v_normalBgColor = "white";
var v_errorBgColor = "red";
var v_normalTextColor = "black";
var v_errorTextColor = "red";

function init() 
{
    //$('callDataTimer').style.display = 'none';
    Event.observe("exit_button","click",FAVORITES_EVENTS.onExitButton);
    Event.observe("save_button","click",FAVORITES_EVENTS.onSaveButton);
    
    FAVORITES_AJAX.getFavorites();

    v_notifyTimer = new TIMER.Timer(v_notifySeconds*1000,'notifyTimerCallback()',null);   

}


function unload() 
{
}

function checkResultSetStatus(xmlDoc,resultSetName,silent,popup,failOnNullStatus, notifyDivPrefix) {
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = true;
    var errorMsg = '';
    
    if( resultSet==null ) {
        retval=false;
        errorMsg='The server did not return the required data for '+resultSetName+'.  Contact BACOM support immediatedly';
    } else {
        var status = FTD_XML.getAttributeText(resultSet,'status');
        if( (status==null&&failOnNullStatus==false) || status=='Y' ) {
            retval=true;
        } else {
            retval=false;
            var records = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]/record/validate',xmlDoc);
            for(idx=0; idx < records.length; idx++ ) 
            {
                valMsg = FTD_XML.getAttributeText(records[idx],'message');
                errorMsg = errorMsg + '<BR/>' + valMsg;
            }
        }
    }
    
    if( retval==false && silent==false ) {
        if( popup==true ) {
            alert(errorMsg);
        } else {
            notifyUser(errorMsg,false,true, notifyDivPrefix);
        }
    }
    
    return retval;
}

function notifyUser(message,appendFlag,startTimer, notifyDivPrefix) {
    if( (appendFlag && appendFlag==true) || $(notifyDivPrefix + 'DivId').visible() ) {
        oldMsg = $(notifyDivPrefix + 'Area').innerHTML;
        $(notifyDivPrefix + 'Area').innerHTML = oldMsg+'<br>'+message
    } else {
        $(notifyDivPrefix + 'Area').innerHTML = message;
    }
    
    if( !($(notifyDivPrefix + 'DivId').visible()) ) {
      new Effect.SlideDown(notifyDivPrefix + 'DivId');
    }
    
    if( startTimer ) {
        if( startTimer==true )
            v_notifyTimer.startTimer();
        else if( v_notifyTimer.isRunning() )
            v_notifyTimer.stopTimer();
    } 
}

function notifyTimerCallback() {
  manuallyCloseNotify();
}
  
function manuallyCloseNotify() {
    if( $('favNotifyDivId').visible() ) {
        new Effect.SlideUp('favNotifyDivId');
    }
    
    if( v_notifyTimer.isRunning() ) {
        v_notifyTimer.stopTimer();
    }
}
