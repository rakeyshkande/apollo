// <![CDATA[
var FAVORITES_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("FAVORITES_EVENTS requires the Prototype JavaScript framework >= 1.5");
      
if(typeof FTD_DOM=='undefined')
      throw("FAVORITES_EVENTS requires FTD_DOM");
      
FAVORITES_EVENTS.onExitButton = function(event) {
    doMainMenuAction('');
};
      
FAVORITES_EVENTS.onSaveButton = function(event) {
    FAVORITES_AJAX.saveChanges();

};

// ]]>