// <![CDATA[
if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("crosssell_events.js requires the Prototype JavaScript framework >= 1.5");
      

var CROSSSELL_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

CROSSSELL_EVENTS.onCrossSellEdit = function(event) {
    CROSSSELL.getCrossSellProduct();
}

CROSSSELL_EVENTS.onExitButton = function(event) {
    doMainMenuAction('');
};

CROSSSELL_EVENTS.onCloseRemoveError = function(event) {
    new Effect.SlideUp('xcrNotifyDivId');
    new Effect.SlideDown('xcRemoveDiv');
};


CROSSSELL_EVENTS.showDetail = function() {
    if( !($('xcProductMaintDiv').visible()) ) {
        if( $('xcRemoveDiv').visible() ) {
            new Effect.SlideUp('xcRemoveDiv');
        }
        new Effect.SlideDown('xcEditDiv');
        new Effect.SlideDown('xcProductMaintDiv');
    }
    
    if( !($('xcProductMaintDiv').visible()) ) {
        $('xcEditDiv').style.display = "block";
        $('xcRemoveDiv').style.display = "none";
        new Effect.SlideDown('xcProductMaintDiv');
    }
    
    if( $('xcSearchDiv').visible() ) {
        new Effect.SlideUp('xcSearchDiv');
    }
    
}

CROSSSELL_EVENTS.onCrossSellRemove = function(event) {
    CROSSSELL.getCrossSellProductUse();
}

CROSSSELL_EVENTS.showRemove = function() {
    if( !($('xcProductMaintDiv').visible()) ) {
        if( $('xcEditDiv').visible() ) {
            new Effect.SlideUp('xcEditDiv');
        }
        new Effect.SlideDown('xcRemoveDiv');
        new Effect.SlideDown('xcProductMaintDiv');
    }
    
    if( $('xcSearchDiv').visible() ) {
        new Effect.SlideUp('xcSearchDiv');
    }
}

CROSSSELL_EVENTS.onCloseCrossSellEdit = function(event) {
    if( !($('xcSearchDiv').visible()) ) {
        new Effect.SlideDown('xcSearchDiv');
    }
    
    if( $('xcEditDiv').visible() ) {
        new Effect.SlideUp('xcEditDiv');
    }
    
    if( $('xcProductMaintDiv').visible() ) {
        new Effect.SlideUp('xcProductMaintDiv');
    }
}

CROSSSELL_EVENTS.onCloseCrossSellRemove = function(event) {
    if( !($('xcSearchDiv').visible()) ) {
        new Effect.SlideDown('xcSearchDiv');
    }
    
    if( $('xcRemoveDiv').visible() ) {
        new Effect.SlideUp('xcRemoveDiv');
    }
    
    if( $('xcProductMaintDiv').visible() ) {
        new Effect.SlideUp('xcProductMaintDiv');
    }
}

CROSSSELL_EVENTS.onCompanySearch = function(event) {
    CROSSSELL.getCrossSell();
}

CROSSSELL_EVENTS.onMoveRight = function(event) {
    var el = FTD_DOM.whichElement(event);
    pos = el.id.substring(8,el.id.length);

    work = CROSSSELL.detailProducts[pos];
    CROSSSELL.detailProducts[pos] = CROSSSELL.detailProducts[pos-1];
    CROSSSELL.detailProducts[pos-1] = work;

    CROSSSELL.populateCrossSellProductDetail();
}

CROSSSELL_EVENTS.onMoveLeft = function(event) {
    var el = FTD_DOM.whichElement(event);
    pos = el.id.substring(7,el.id.length);

    work = CROSSSELL.detailProducts[pos-1];
    CROSSSELL.detailProducts[pos-1] = CROSSSELL.detailProducts[pos-2];
    CROSSSELL.detailProducts[pos-2] = work;

    CROSSSELL.populateCrossSellProductDetail();
}

CROSSSELL_EVENTS.onRemove = function(event) {
    var el = FTD_DOM.whichElement(event);
    pos = el.id.substring(15,el.id.length);
    CROSSSELL.detailProducts.splice(pos-1,1);
    CROSSSELL.createBlankProduct();

    CROSSSELL.populateCrossSellProductDetail();
}

CROSSSELL_EVENTS.onRemoveAll = function(event) {
    CROSSSELL.detailProducts = new Array();
    for (i=0; i<CROSSSELL.maxDetailItems; i++) {
        CROSSSELL.createBlankProduct();
    }
    CROSSSELL.populateCrossSellProductDetail();
}

CROSSSELL_EVENTS.onLookup = function(event) {
    var el = FTD_DOM.whichElement(event);
    pos = el.id.substring(16,el.id.length);
    CROSSSELL.lookupProduct(pos);
}


CROSSSELL_EVENTS.onSubmit = function(event) {
    CROSSSELL.saveChanges();
}


CROSSSELL_EVENTS.onCrossSellRemoveSelected = function(event) {
    CROSSSELL.crossSellRemoveSelected();
}

CROSSSELL_EVENTS.onRemoveAllChecked = function(event) {
    CROSSSELL.crossSellRemoveAllChecked();
}
CROSSSELL_EVENTS.onSearchLinkClicked = function(event) 
{
    var prefix = event.target.id;
    var fullId = event.target.id + CROSSSELL.resultsLinkProductSuffix;
    var productElement = $(fullId);
    if (productElement != null)
    {
        var productId = productElement.getValue();
        showCrossSellProduct(productId);
    }
}
// ]]>