// <![CDATA[
var CROSSSELL_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || CROSSSELL_AJAX.prototypeVersion < 1.6)
      throw("CROSSSELL_AJAX requires the Prototype JavaScript framework >= 1.6");


CROSSSELL_AJAX.getCompanies = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_GET_COMPANIES';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.getCompaniesCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

CROSSSELL_AJAX.getCompaniesCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        var records = XPath.selectNodes('/result/rs[@name="company_master"]/record',xmlDoc);
        v_companies = new Array();
        var idx;
        
        for(idx=0; idx<records.length; idx++ ) 
        {
            var companyId = FTD_XML.selectNodeText(records[idx],'company_id');
            v_companies.push(companyId);
        }
    
        FTD_DOM.clearSelect('xs_select');
        
        FTD_DOM.insertOption('xs_select','ALL','',true);
        for( idx=0; idx < v_companies.length; idx++) 
        {
            FTD_DOM.insertOption('xs_select',v_companies[idx],v_companies[idx],false);
        }
    }
 
}

CROSSSELL_AJAX.getNovatorUpdateSetup = function()
{
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_NOVATOR_UPDATE_SETUP';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('commonUtilAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.getNovatorUpdateSetupCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

CROSSSELL_AJAX.getNovatorUpdateSetupCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        var records = XPath.selectNodes('/result/rs[@name="novator_update_setup"]/record',xmlDoc);
        v_companies = new Array();
        var idx;
        
        for(idx=0; idx<records.length; idx++ ) 
        {
            var contentFeedAllowed = FTD_XML.selectNodeText(records[idx],'content_feed_allowed');
            var testFeedAllowed = FTD_XML.selectNodeText(records[idx],'test_feed_allowed');
            var uatFeedAllowed = FTD_XML.selectNodeText(records[idx],'uat_feed_allowed');
            var liveFeedAllowed = FTD_XML.selectNodeText(records[idx],'production_feed_allowed');
            var contentFeedOn = FTD_XML.selectNodeText(records[idx],'content_feed_checked');
            var testFeedOn = FTD_XML.selectNodeText(records[idx],'test_feed_checked');
            var uatFeedOn = FTD_XML.selectNodeText(records[idx],'uat_feed_checked');
            var liveFeedOn = FTD_XML.selectNodeText(records[idx],'production_feed_checked');
        }
        
        setupCheckBox('xcrToContent',contentFeedAllowed,contentFeedOn);
        setupCheckBox('xcrToTest',testFeedAllowed,testFeedOn);
        setupCheckBox('xcrToUAT',uatFeedAllowed,uatFeedOn);
        setupCheckBox('xcrToLive',liveFeedAllowed,liveFeedOn);
        setupCheckBox('xcdToContent',contentFeedAllowed,contentFeedOn);
        setupCheckBox('xcdToTest',testFeedAllowed,testFeedOn);
        setupCheckBox('xcdToUAT',uatFeedAllowed,uatFeedOn);
        setupCheckBox('xcdToLive',liveFeedAllowed,liveFeedOn);
        
    }    
 
}

function setupCheckBox(idName, allowed, boxOn)
{
        var checkBox = $(idName);

        if (allowed != null && allowed == 'YES')
        {
            checkBox.checked = false;
            checkBox.disabled = false;
            if (boxOn != null && boxOn == 'YES')
            {
                checkBox.checked = true;
            }
        }
        else
        {
            checkBox.checked = false;
            checkBox.disabled = true;
        }
}

CROSSSELL_AJAX.getCrossSellProduct = function() {
    var productId = $('xcMainProductId').getValue();
    CROSSSELL_AJAX.getCrossSellProductWithProduct(productId);
}

CROSSSELL_AJAX.getCrossSellProductWithProduct = function(productId) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_GET_XSELL';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.getCrossSellProductCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

CROSSSELL_AJAX.getCrossSellProductCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        // Get the product details first
        var records = XPath.selectNodes('/result/rs[@name="product"]/record',xmlDoc);
        var idx = 0;
        var crossSell = null;
        
        // should be just one record
        if (records.length > 0)
        {
            var productId = FTD_XML.selectNodeText(records[idx],'product_id');
            crossSell = new CROSS_SELL.OBJECT(productId);
            crossSell.productCrossSellId = FTD_XML.selectNodeText(records[idx],'product_cross_sell_id');
            crossSell.product.novatorId = FTD_XML.selectNodeText(records[idx],'novator_id');
            crossSell.product.available = FTD_XML.selectNodeText(records[idx],'status');
            crossSell.product.exceptionStartDate = FTD_XML.selectNodeText(records[idx],'exception_start_date');
            crossSell.product.exceptionEndDate = FTD_XML.selectNodeText(records[idx],'exception_end_date');
            crossSell.product.over21 = FTD_XML.selectNodeText(records[idx],'over_21');
            crossSell.product.name=FTD_XML.selectNodeText(records[idx],'novator_name')
            crossSell.product.productType=FTD_XML.selectNodeText(records[idx],'product_type');
            crossSell.product.description=FTD_XML.selectNodeText(records[idx],'long_description');
            crossSell.product.standardPrice=FTD_XML.selectNodeText(records[idx],'standard_price');
            crossSell.product.smallImage=FTD_XML.selectNodeText(records[idx],'smallimage');
            crossSell.product.largeImage=FTD_XML.selectNodeText(records[idx],'largeimage');
            
            CROSSSELL.crossSellObject = crossSell;
            CROSSSELL.maxDetailItems = FTD_XML.selectNodeText(records[idx],'max_items');
        }

        // Get the cross sell product details
        var xc_records = XPath.selectNodes('/result/rs[@name="cross_sell"]/record',xmlDoc);
        
        CROSSSELL.detailProducts = new Array();
        for(idx=0; idx<xc_records.length; idx++ ) {
            cs_product_id = FTD_XML.selectNodeText(xc_records[idx],'product_id');
            if (cs_product_id == null || cs_product_id == '') {
                cs_product_id = FTD_XML.selectNodeText(xc_records[idx],'upsell_id');
            }
            crossSellProduct = new PRODUCT.OBJECT(cs_product_id);
            crossSellProduct.available = FTD_XML.selectNodeText(xc_records[idx],'status');
            crossSellProduct.name = FTD_XML.selectNodeText(xc_records[idx],'novator_name');
            crossSellProduct.standardPrice = FTD_XML.selectNodeText(xc_records[idx],'standard_price');
            crossSellProduct.exceptionStartDate = FTD_XML.selectNodeText(xc_records[idx],'exception_start_date');
            crossSellProduct.exceptionEndDate = FTD_XML.selectNodeText(xc_records[idx],'exception_end_date');
            crossSellProduct.smallImage = FTD_XML.selectNodeText(xc_records[idx],'small_image');
            crossSellProduct.largeImage = FTD_XML.selectNodeText(xc_records[idx],'large_image');
            crossSellProduct.description = FTD_XML.selectNodeText(xc_records[idx],'long_description');
            crossSellProduct.over21 = FTD_XML.selectNodeText(xc_records[idx],'over_21');
            crossSellProduct.productType = FTD_XML.selectNodeText(xc_records[idx],'product_type');

            CROSSSELL.detailProducts.push(crossSellProduct);
        }
        for(idx=xc_records.length; idx<CROSSSELL.maxDetailItems; idx++) {
            CROSSSELL.createBlankProduct();
        }

        if (records.length == 0)
        {
            // If nothing was found, let the user know
            alert("Product was not found");
        }
        else
        {
            CROSSSELL.populateCrossSellProductDetail();
            CROSSSELL_EVENTS.showDetail();
        }
        
    }
}

CROSSSELL_AJAX.saveChanges = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_UPDATE';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var productId = $('xcdProductId').innerHTML;
    param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var crossSellProduct = '';
    for (i=0; i<CROSSSELL.detailProducts.length; i++) {
        //alert(i + ' ' + CROSSSELL.detailProducts[i].productId + ' ' + $('xcdProductId'+(i+1)).value);
        if ($('xcdProductId'+(i+1)).value != '' ) {
            if (crossSellProduct != '') crossSellProduct = crossSellProduct + ' ';
            crossSellProduct = crossSellProduct + $('xcdProductId'+(i+1)).value;
        }
    }
    param = FTD_XML.createElementWithText(doc,'param',crossSellProduct);
    attr = doc.createAttribute("name");
    attr.value = 'CROSS_SELL_PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toLive = $('xcdToLive').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toLive);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_LIVE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toContent = $('xcdToContent').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toContent);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_CONTENT';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toTest = $('xcdToTest').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toTest);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_TEST';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toUAT = $('xcdToUAT').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toUAT);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_UAT';
    param.setAttributeNode(attr);
    root.appendChild(param);

    $(actionButtonProcessing).style.visibility = "visible";

    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.saveChangesCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
 
}

CROSSSELL_AJAX.saveChangesCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        el = $('xcdNotifyArea');
        if( checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcdNotify') ) 
        {
            message = 'Changes Successfully Saved';
            el.style.color = v_normalTextColor;
            el.style.fontWeight = 'normal';
            notifyUser(message,false,true,'xcdNotify');
        }
        else
        {
            message = 'Save Failed';
            oldMsg = $('xcdNotifyArea').innerHTML;
            $('xcdNotifyArea').innerHTML = oldMsg+'<br>'+message;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
        }
    }
    
    // Fire off a call to repopulate the data on the screen
    var productId = $('xcdProductId').innerHTML;
    CROSSSELL_AJAX.getCrossSellProductWithProduct(productId);

    $(actionButtonProcessing).style.visibility = "hidden";

}

CROSSSELL_AJAX.crossSellRemoveSelected = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_REMOVE_XSELLS';
    root.setAttributeNode(attr);

    param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var productId = $('xcdProductId').innerHTML;
    var param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    // Get a list of all the master products selected
    var numberChecks = $('xcrCounter').value;
    var productIdList = '';
    for (var idx =0; idx < numberChecks; idx++)
    {
        var useCheckBox = $(CROSSSELL.useCheckBoxPrefix + idx);
        if (useCheckBox.checked)
        {
            productIdList = productIdList + ' ' + useCheckBox.name;
        }
    }

    param = FTD_XML.createElementWithText(doc,'param',productIdList);
    attr = doc.createAttribute("name");
    attr.value = 'XSELL_PRODUCT';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toLive = $('xcrToLive').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toLive);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_LIVE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    var toContent = $('xcrToContent').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toContent);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_CONTENT';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toTest = $('xcrToTest').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toTest);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_TEST';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var toUAT = $('xcrToUAT').getValue();
    param = FTD_XML.createElementWithText(doc,'param',toUAT);
    attr = doc.createAttribute("name");
    attr.value = 'NOVATOR_UPDATE_UAT';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    $(actionButtonProcessing).style.visibility = "visible";
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.crossSellRemoveSelectedCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();

}

CROSSSELL_AJAX.crossSellRemoveSelectedCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        el = $('xcrNotifyArea');
        if( checkResultSetStatus(xmlDoc,"validation",false,false,false,'xcrNotify') ) 
        {
            message = 'Changes Successfully Saved';
            el.style.color = v_normalTextColor;
            el.style.fontWeight = 'normal';
            notifyUser(message,false,true,'xcrNotify');
        }
        else
        {
            message = 'Save Failed';
            oldMsg = $('xcrNotifyArea').innerHTML;
            $('xcrNotifyArea').innerHTML = oldMsg+'<br>'+message;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
        }
    }
    
    CROSSSELL_AJAX.getCrossSellProductUse();
    
    $(actionButtonProcessing).style.visibility = "hidden"; 


}

CROSSSELL_AJAX.getCrossSellProductUse = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_GET_MASTER_RECORDS';
    root.setAttributeNode(attr);
    
    var productId = $('xcProductId').getValue();
    var param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.getCrossSellProductUseCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

CROSSSELL_AJAX.getCrossSellProductUseCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        // Get the product details first
        var records = XPath.selectNodes('/result/rs[@name="product"]/record',xmlDoc);
        var idx = 0;
        var crossSell = null;
        v_cross_sell = new Array();
        
        // should be just one record
        if (records.length > 0)
        {
            var productId = FTD_XML.selectNodeText(records[idx],'product_id');
            crossSell = new CROSS_SELL.OBJECT(productId);
            crossSell.product.novatorId = FTD_XML.selectNodeText(records[idx],'novator_id');
            crossSell.product.available = FTD_XML.selectNodeText(records[idx],'status');
            crossSell.product.over21 = FTD_XML.selectNodeText(records[idx],'over_21');
            crossSell.product.name = FTD_XML.selectNodeText(records[idx],'novator_name')
            crossSell.product.productType = FTD_XML.selectNodeText(records[idx],'product_type');
            crossSell.product.description = FTD_XML.selectNodeText(records[idx],'long_description');
            crossSell.product.standardPrice = FTD_XML.selectNodeText(records[idx],'standard_price');
            crossSell.product.smallImage = FTD_XML.selectNodeText(records[idx],'smallimage');
            crossSell.product.largeImage = FTD_XML.selectNodeText(records[idx],'largeimage');
            
            CROSSSELL.crossSellObject = crossSell;
        }

        // Get the cross sell product details
        var xc_records = XPath.selectNodes('/result/rs[@name="cross_sell"]/record',xmlDoc);
        
        // should be just one record
        for(idx=0; idx < xc_records.length; idx++ ) 
        {
            cs_product_id = FTD_XML.selectNodeText(xc_records[idx],'product_id');

            crossSellUse = new PRODUCT.OBJECT(cs_product_id);

            crossSellUse.novatorId = FTD_XML.selectNodeText(xc_records[idx],'novator_id');
            crossSellUse.description = FTD_XML.selectNodeText(xc_records[idx],'long_description');
            crossSellUse.over21 = FTD_XML.selectNodeText(xc_records[idx],'over_21');
            crossSellUse.productType = FTD_XML.selectNodeText(xc_records[idx],'product_type');

            v_cross_sell.push(crossSellUse);
        }
        
        if (records.length == 0)
        {
            // If nothing was found, let the user know
            alert("Product was not found");
        }
        else
        {
            CROSSSELL.crossSellUseProducts = v_cross_sell;
            CROSSSELL.populateCrossSellProductUse();
            CROSSSELL_EVENTS.showRemove();
        }
        
    }
}

CROSSSELL_AJAX.lookupProduct = function(productSlot) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_XSELL_GET_XSELL';
    root.setAttributeNode(attr);

    var productId = $('xcdProductId' + productSlot).getValue();
    
    var param = FTD_XML.createElementWithText(doc,'param',productId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'slot_number',productSlot+''));
    root.appendChild(param);
    
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('crossSellAjax.do',FTD_AJAX_REQUEST_TYPE_POST,CROSSSELL_AJAX.lookupProductCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

CROSSSELL_AJAX.lookupProductCallback = function(transport) {
    //alert(transport.responseText);
    xmlDoc = parseResponse(transport);
        
    if( xmlDoc!=null ) 
    {
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var productSlot = FTD_XML.selectNodeText(echo,'slot_number');


        // Get the product details first
        var records = XPath.selectNodes('/result/rs[@name="product"]/record',xmlDoc);
        var idx = 0;
        
        // should be just one record
        if (records.length > 0)
        {
            var productId = FTD_XML.selectNodeText(records[idx],'product_id');
            var newProduct = new PRODUCT.OBJECT(productId);
            newProduct.novatorId = FTD_XML.selectNodeText(records[idx],'novator_id');
            newProduct.available = FTD_XML.selectNodeText(records[idx],'status');
            newProduct.over21 = FTD_XML.selectNodeText(records[idx],'over_21');
            newProduct.name=FTD_XML.selectNodeText(records[idx],'novator_name')
            newProduct.productType=FTD_XML.selectNodeText(records[idx],'product_type');
            newProduct.description=FTD_XML.selectNodeText(records[idx],'long_description');
            newProduct.standardPrice=FTD_XML.selectNodeText(records[idx],'standard_price');
            newProduct.smallImage=FTD_XML.selectNodeText(records[idx],'smallimage');
            newProduct.largeImage=FTD_XML.selectNodeText(records[idx],'largeimage');
            newProduct.exceptionStartDate = FTD_XML.selectNodeText(records[idx],'exception_start_date');
            newProduct.exceptionEndDate = FTD_XML.selectNodeText(records[idx],'exception_end_date');
            if (newProduct.available == 'YES')
            {
                newProduct.available = 'AVAILABLE';
            }
            else
            {
                newProduct.available = 'NOT AVAILABLE';
            }

            CROSSSELL.detailProducts[productSlot-1] = newProduct;
        }
    
        CROSSSELL.populateCrossSellProductDetail();
        
    }
}



