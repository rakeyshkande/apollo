// <![CDATA[
var CROSSSELL = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || CROSSSELL.prototypeVersion < 1.6)
      throw("crosssell.js requires the Prototype JavaScript framework >= 1.6");

CROSSSELL.useBodyPrefix = 'xcRemoveBody';
CROSSSELL.useBodyRowPrefix = 'xcRemoveBodyRow_';
CROSSSELL.useTablePrefix = 'xcRemoveTable';
CROSSSELL.useCheckBoxPrefix = 'xcrCheckbox_';
CROSSSELL.useProductIdPrefix = 'xcrProductId_';
CROSSSELL.useNovatorIdPrefix = 'xcrNovatorId_';
CROSSSELL.useDescPrefix = 'xcrDescription_';
CROSSSELL.useOver21Prefix = 'xcrOver21_';
CROSSSELL.useSameDayPrefix = 'xcrSameDay_';

//Begin element colors
var v_normalBgColor = "white";
var v_errorBgColor = "red";
var v_normalTextColor = "black";
var v_errorTextColor = "red";
//End element colors
var v_notifyTimer;
var v_notifySeconds = 30;
var v_notAvailLinkClass = "notAvail";

CROSSSELL.crossSellProducts = new Array();
CROSSSELL.crossSellUseProducts = new Array();
CROSSSELL.crossSellObject = null;
CROSSSELL.detailProducts = new Array();
CROSSSELL.maxDetailItems = 3;
      
function init() {
    Event.observe("xcEditButton","click",CROSSSELL_EVENTS.onCrossSellEdit);
    Event.observe("xcRemoveButton","click",CROSSSELL_EVENTS.onCrossSellRemove);
    Event.observe("xcCompanySearch","click",CROSSSELL_EVENTS.onCompanySearch);
    
    Event.observe("xcrAll","click",CROSSSELL_EVENTS.onRemoveAllChecked);
    Event.observe("xcrBackButton","click",CROSSSELL_EVENTS.onCloseCrossSellRemove);
    Event.observe("xcrRemoveButton","click",CROSSSELL_EVENTS.onCrossSellRemoveSelected);
    Event.observe("xcrCloseRemoveError","click",CROSSSELL_EVENTS.onCloseRemoveError);

    Event.observe("xcdRemoveButton","click",CROSSSELL_EVENTS.onRemoveAll);
    Event.observe("xcdSubmitButton","click",CROSSSELL_EVENTS.onSubmit);
    Event.observe("xcdBackButton","click",CROSSSELL_EVENTS.onCloseCrossSellEdit);

    Event.observe("exit_button","click",CROSSSELL_EVENTS.onExitButton);

    v_notifyTimer = new TIMER.Timer(v_notifySeconds*1000,'notifyTimerCallback()',null);   
    
    CROSSSELL_AJAX.getCompanies();
    CROSSSELL_AJAX.getNovatorUpdateSetup();

}

function unload() {
}

CROSSSELL.getCrossSell = function() {
    // Not doing with AJAX now.  Just submit the form
    //CROSSSELL_AJAX.getCrossSell();
    
    // Submit the form
    $('xcForm').submit();
}

CROSSSELL.getCrossSellProduct = function() {
    CROSSSELL_AJAX.getCrossSellProduct();
}

CROSSSELL.populateCrossSellProductDetail = function() {

    if (CROSSSELL.crossSellObject.product != null)
    {
        $('xcdProductId').innerHTML = CROSSSELL.crossSellObject.product.productId;
        $('xcdNovatorId').innerHTML = CROSSSELL.crossSellObject.product.novatorId;
        $('xcdProductTitle').innerHTML = CROSSSELL.crossSellObject.product.name;
        $('xcProductDesc').innerHTML = CROSSSELL.crossSellObject.product.description;
        $('xcdProductPrice').innerHTML = '$' + UTILS.addCommas(CROSSSELL.crossSellObject.product.standardPrice);
        $('xcdProductAvailable').innerHTML = CROSSSELL.crossSellObject.product.available;
        $('xcdProductImage').src = CROSSSELL.crossSellObject.product.largeImage;
        $('xcdProductImage').alt = CROSSSELL.crossSellObject.product.description;
        if (CROSSSELL.crossSellObject.product.over21 == 'Y')
            $('xcdOver21').style.visibility = 'visible';
        else
            $('xcdOver21').style.visibility = 'hidden';                    
        if (isSameDay(CROSSSELL.crossSellObject.product))
            $('xcdSameDay').style.visibility = 'visible';                    
        else
            $('xcdSameDay').style.visibility = 'hidden';                    
        $('xcdProductAvailableException').innerHTML = ' ' ;
        if (CROSSSELL.crossSellObject.product.exceptionStartDate != null)
        {
            $('xcdProductAvailableException').innerHTML = CROSSSELL.crossSellObject.product.exceptionStartDate + ' - ' 
                + CROSSSELL.crossSellObject.product.exceptionEndDate;
        }

        productCount = CROSSSELL.detailProducts.length;
        productsPerRow = parseInt('5');
        quotient = productCount / productsPerRow;
        rows = Math.ceil( quotient );
        if (rows == 0) rows = 1;
        start = 0;
        totalCount = 0;

        work = document.getElementById('xcEditRowDiv');
        while (work.childNodes[0]) {
            work.removeChild(work.childNodes[0]);
        }

        for (rowLoop=0; rowLoop<rows; rowLoop++) {

            var temp = $('xcEditRowSubDiv');
            var newCrossSell = temp.cloneNode(true);
            childArray = new Array();
            FTD_DOM.getAllDecendents(newCrossSell, childArray);
            newCrossSell.id = 'subDiv'+rowLoop;
            newCrossSell.style.display = "block";
            document.getElementById('xcEditRowDiv').appendChild(newCrossSell);

            for(idx=start; idx<(start+productsPerRow); idx++ ) {
                foundId = false;
                foundName = false;
                foundImage = false;
                foundException = false;
                foundPrice = false;
                foundAvailable = false;
                foundOver21 = false;
                foundSameDay = false;
                foundLookup = false;
                foundArrowDivLeft = false;
                foundArrowDivRight = false;
                foundRemove = false;
                foundSkuNumber = false;
                foundTD = false;
                for(i=0; i<childArray.length; i++) {
                    var e1 = childArray[i];
                    if (e1.id  == 'xcdProductId_dummy' && !foundId) {
                        e1.id = 'xcdProductId'+(idx+1);
                        e1.value = CROSSSELL.detailProducts[idx].productId;
                        foundId = true;
                    } else if (e1.id == 'xcdTD_dummy' && !foundTD) {
                        e1.id = 'xcdTD'+(idx+1);
                        e1.style.display = "block";
                        foundTD = true;
                    } else if (e1.id == 'skuNumber_dummy' && !foundSkuNumber) {
                        e1.id = 'skuNumber'+(idx+1);
                        e1.innerHTML = 'SKU ' + (idx+1);
                        foundSkuNumber = true;
                    } else if (e1.id == 'xcdProductName_dummy' && !foundName) {
                        e1.id = 'xcdProductName' + (idx+1);
                        e1.innerHTML = CROSSSELL.detailProducts[idx].name;
                        foundName = true;
                    } else if (e1.id == 'xcdImage_dummy' && !foundImage) {
  
                        if (null == CROSSSELL.detailProducts[idx].description) {
                            CROSSSELL.detailProducts[idx].description = 'No description found';
                        }
                        else 
                        {
                            CROSSSELL.detailProducts[idx].description = CROSSSELL.detailProducts[idx].description.replace(/<br>/gi," ");
                            CROSSSELL.detailProducts[idx].description = CROSSSELL.detailProducts[idx].description.replace(/<[^>]+>/g,"");   
                        }
 
                        e1.id = 'xcdImage'+(idx+1);
                        e1.src = CROSSSELL.detailProducts[idx].largeImage;
                        e1.alt = CROSSSELL.detailProducts[idx].description;
                        foundImage = true;
                    } else if (e1.id == 'xcdProductPrice_dummy' && !foundPrice) {
                        e1.id = 'xcdProductPrice'+(idx+1);
                        e1.innerHTML = '$' + UTILS.addCommas(CROSSSELL.detailProducts[idx].standardPrice);
                        foundPrice = true;
                    } else if (e1.id == 'xcdProductAvailable_dummy' && !foundAvailable) {
                        e1.id = 'xcdProductAvailable'+(idx+1);
                        e1.innerHTML = CROSSSELL.detailProducts[idx].available;
                        foundAvailable = true;
                    } else if (e1.id == 'xcdProductException_dummy' && !foundException) {
                        e1.id = 'xcdProductException'+(idx+1);
                        e1.innerHTML = ' ';
                        if (CROSSSELL.detailProducts[idx].exceptionStartDate != null) {
                            e1.innerHTML = CROSSSELL.detailProducts[idx].exceptionStartDate + ' - ' 
                                + CROSSSELL.detailProducts[idx].exceptionEndDate;
                        }
                        foundException = true;
                    } else if (e1.id == 'xcdOver21_dummy' && !foundOver21) {
                        e1.id = 'xcdOver21'+(idx+1);
                        if (CROSSSELL.detailProducts[idx].over21 == 'Y')
                            $('xcdOver21'+(idx+1)).style.visibility = 'visible';                    
                        else
                            $('xcdOver21'+(idx+1)).style.visibility = 'hidden';                    
                        foundOver21 = true;
                    } else if (e1.id == 'xcdSameDay_dummy' && !foundSameDay) {
                        e1.id = 'xcdSameDay'+(idx+1);
                        if (CROSSSELL.detailProducts[idx].productType == 'FLORAL' ||
                                CROSSSELL.detailProducts[idx].productType == 'SDFC' ||
                                CROSSSELL.detailProducts[idx].productType == 'SDG')
                            $('xcdSameDay'+(idx+1)).style.visibility = 'visible';                    
                        else
                            $('xcdSameDay'+(idx+1)).style.visibility = 'hidden';                    
                        foundSameDay = true;
                    } else if (e1.id == 'xcdProductLookup_dummy' && !foundLookup) {
                        e1.id = 'xcdProductLookup'+(idx+1);
                        e1.name = 'xcdProductLookup'+(idx+1);
                        Event.observe("xcdProductLookup"+(idx+1),"click",CROSSSELL_EVENTS.onLookup);
                        foundLookup = true;
                    } else if (e1.id == 'xcdLeft_dummy' && !foundArrowDivLeft) {
                        e1.id = 'xcdLeft'+(idx+1);
                        if (idx > 0) {
                            e1.style.visibility = 'visible';
                            Event.observe("xcdLeft"+(idx+1),"click",CROSSSELL_EVENTS.onMoveLeft);
                        }
                        foundArrowDivLeft = true;
                    } else if (e1.id == 'xcdRight_dummy' && !foundArrowDivRight) {
                        e1.id = 'xcdRight'+(idx+1);
                        if (idx < (productCount - 1)) {
                            e1.style.visibility = 'visible';
                            Event.observe("xcdRight"+(idx+1),"click",CROSSSELL_EVENTS.onMoveRight);
                        }
                        foundArrowDivRight = true;
                    } else if (e1.id == 'xcdRemoveButton_dummy' && !foundRemove) {
                        e1.id = 'xcdRemoveButton'+(idx+1);
                        Event.observe("xcdRemoveButton"+(idx+1),"click",CROSSSELL_EVENTS.onRemove);
                        foundRemove = true;
                    }
                }
                
                totalCount = totalCount + 1;
                if (totalCount == productCount) {
                    break;
                }
            }
            
            start = start + productsPerRow;
        
        }

    }

}

CROSSSELL.saveChanges = function() {
    if (CROSSSELL.detailProducts.length > CROSSSELL.maxDetailItems) {
        errorMsg = 'Number of Cross-sells (' + CROSSSELL.detailProducts.length + ') is greater than the maximum allowed (' + CROSSSELL.maxDetailItems + ')';
        el = $('xcdNotifyArea');
        el.style.color = v_errorTextColor;
        el.style.fontWeight = 'bold';
        notifyUser(errorMsg,false,true,'xcdNotify');
    } else {
        CROSSSELL_AJAX.saveChanges();
    }
}

CROSSSELL.crossSellRemoveSelected = function() {
    CROSSSELL_AJAX.crossSellRemoveSelected();
}

CROSSSELL.getCrossSellProductUse = function() {
    CROSSSELL_AJAX.getCrossSellProductUse();
}

CROSSSELL.populateCrossSellProductUse = function() {


    if (CROSSSELL.crossSellObject.product != null)
    {
        $('xcdProductId').innerHTML = CROSSSELL.crossSellObject.product.productId;
        $('xcdNovatorId').innerHTML = CROSSSELL.crossSellObject.product.novatorId;
        $('xcdProductTitle').innerHTML = CROSSSELL.crossSellObject.product.name;
        $('xcProductDesc').innerHTML = CROSSSELL.crossSellObject.product.description;
        $('xcdProductPrice').innerHTML = '$' + UTILS.addCommas(CROSSSELL.crossSellObject.product.standardPrice);
        $('xcdProductAvailable').innerHTML = CROSSSELL.crossSellObject.product.available;
        $('xcdProductImage').src = CROSSSELL.crossSellObject.product.largeImage;
        if (CROSSSELL.crossSellObject.product.over21 == 'Y')
            $('xcdOver21').style.visibility = 'visible';                    
        else
            $('xcdOver21').style.visibility = 'hidden';                    
        if (isSameDay(CROSSSELL.crossSellObject.product))
            $('xcdSameDay').style.visibility = 'visible';                    
        else
            $('xcdSameDay').style.visibility = 'hidden';                    


        var tableBody = $(CROSSSELL.useBodyPrefix);
        var dummyCrossSell = $(CROSSSELL.useBodyRowPrefix+'dummy');
        FTD_DOM.removeAllTableRows(tableBody);
        var childArray;
        var counterInput = $('xcrCounter');
        counterInput.value = CROSSSELL.crossSellUseProducts.length;
        for( var idx=0; idx < CROSSSELL.crossSellUseProducts.length; idx++ ) {
            
            var newCrossSell = dummyCrossSell.cloneNode(true);
            newCrossSell.id = CROSSSELL.useBodyRowPrefix+idx;
    
            childArray = new Array();
            FTD_DOM.getAllDecendents(newCrossSell,childArray);
            for( var i=0; i<childArray.length; i++ ) {
                var el = childArray[i];
                if( el.id == CROSSSELL.useTablePrefix+'dummy' ) {
                    el.className='xcRemoveTable';
                    el.id=CROSSSELL.useTablePrefix+idx;
                } else if( el.id == CROSSSELL.useProductIdPrefix+'dummy' ) {
                    el.id=CROSSSELL.useProductIdPrefix+idx;
                    el.innerHTML=CROSSSELL.crossSellUseProducts[idx].productId;
                } else if( el.id == CROSSSELL.useNovatorIdPrefix+'dummy' ) {
                    el.id=CROSSSELL.useNovatorIdPrefix+idx;
                    el.innerHTML=CROSSSELL.crossSellUseProducts[idx].novatorId;
                } else if( el.id == CROSSSELL.useDescPrefix+'dummy' ) {
                    el.id=CROSSSELL.useDescPrefix+idx;
                    el.innerHTML=CROSSSELL.crossSellUseProducts[idx].description;
                } else if( el.id == CROSSSELL.useCheckBoxPrefix+'dummy' ) {
                    el.id=CROSSSELL.useCheckBoxPrefix+idx;
                    el.name=CROSSSELL.crossSellUseProducts[idx].productId;
                } else if( el.id == CROSSSELL.useOver21Prefix+'dummy' ) {
                    el.id=CROSSSELL.useOver21Prefix+idx;
                    if (CROSSSELL.crossSellUseProducts[idx].over21 == 'Y')
                        el.style.visibility = 'visible';                    
                    else
                        el.style.visibility = 'hidden';                    
                } else if( el.id == CROSSSELL.useSameDayPrefix+'dummy' ) {
                    el.id=CROSSSELL.useSameDayPrefix+idx;
                    if (isSameDay(CROSSSELL.crossSellUseProducts[idx]))
                        el.style.visibility = 'visible';                    
                    else
                        el.style.visibility = 'hidden';                    
                }
            }
            
            tableBody.appendChild(newCrossSell);
        }
        Table.page($('xcRemoveTable'),0);
    }
}

CROSSSELL.lookupProduct = function(productSlotNumber) {
    CROSSSELL_AJAX.lookupProduct(productSlotNumber);
}

CROSSSELL.crossSellRemoveAllChecked = function() {
    var allCheckbox = $('xcrAll');
    CROSSSELL.modifyAllRemoveChecks(allCheckbox.checked);
}

CROSSSELL.modifyAllRemoveChecks = function(checkValue)
{
    var numberChecks = $('xcrCounter').value;
    for (var idx =0; idx < numberChecks; idx++)
    {
        $(CROSSSELL.useCheckBoxPrefix + idx).checked = checkValue;
    }
}

CROSSSELL.createBlankProduct = function() 
{
    crossSellProduct = new PRODUCT.OBJECT('');
    crossSellProduct.novatorId = '';
    crossSellProduct.available = '';
    crossSellProduct.over21 = '';
    crossSellProduct.name='';
    crossSellProduct.productType='';
    crossSellProduct.description='';
    crossSellProduct.standardPrice='';
    crossSellProduct.smallImage='images/noproduct.gif';
    crossSellProduct.largeImage='images/noproduct.gif';
    CROSSSELL.detailProducts.push(crossSellProduct);
    
    return crossSellProduct;
}

   
function checkResultSetStatus(xmlDoc,resultSetName,silent,popup,failOnNullStatus, notifyDivPrefix) {
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = true;
    var errorMsg = '';
    
    if( resultSet==null ) {
        retval=false;
        errorMsg='The server did not return the required data for '+resultSetName+'.  Contact BACOM support immediatedly';
    } else {
        var status = FTD_XML.getAttributeText(resultSet,'status');
        if( (status==null&&failOnNullStatus==false) || status=='Y' ) {
            retval=true;
        } else {
            retval=false;
            var records = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]/record/validate',xmlDoc);
            for(idx=0; idx < records.length; idx++ ) 
            {
                valMsg = FTD_XML.getAttributeText(records[idx],'message');
                errorMsg = errorMsg + '<BR/>' + valMsg;
            }
        }
    }
    
    if( retval==false && silent==false ) {
        if( popup==true ) {
            alert(errorMsg);
        } else {
            notifyUser(errorMsg,false,true, notifyDivPrefix);
        }
    }
    
    return retval;
}

function notifyUser(message,appendFlag,startTimer, notifyDivPrefix) {
    if( (appendFlag && appendFlag==true) || $(notifyDivPrefix + 'DivId').visible() ) {
        oldMsg = $(notifyDivPrefix + 'Area').innerHTML;
        $(notifyDivPrefix + 'Area').innerHTML = oldMsg+'<br>'+message
    } else {
        $(notifyDivPrefix + 'Area').innerHTML = message;
    }

    if( notifyDivPrefix == 'xcrNotify' ) {
        new Effect.SlideUp('xcRemoveDiv');
    }
    
    if( !($(notifyDivPrefix + 'DivId').visible()) ) {
      new Effect.SlideDown(notifyDivPrefix + 'DivId');
    }
    
    if( startTimer ) {
        if( startTimer==true )
            v_notifyTimer.startTimer();
        else if( v_notifyTimer.isRunning() )
            v_notifyTimer.stopTimer();
    } 
}

function notifyTimerCallback() {
  manuallyCloseNotify();
}
  
function manuallyCloseNotify() {
    if( $('xcdNotifyDivId').visible() ) {
        new Effect.SlideUp('xcdNotifyDivId');
    }
    
    if( v_notifyTimer.isRunning() ) {
        v_notifyTimer.stopTimer();
    }
}


function showCrossSellProduct(productCrossSellId)
{
    CROSSSELL_AJAX.getCrossSellProductWithProduct(productCrossSellId);
    CROSSSELL_EVENTS.showDetail();
    return false;
}

function isSameDay(product)
{
    if (product.productType == 'FLORAL' ||
        product.productType == 'SDFC' ||
        product.productType == 'SDG')
    {
        return true;
    }
    else
    {
        return false;
    }
}

     
// ]]>