// <![CDATA[
var DNIS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("DNIS requires the Prototype JavaScript framework >= 1.5");
      
DNIS.OBJECT = Class.create();

DNIS.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.id = null;
        this.description = null;
        this.defaultSourceCode = null;
    }
};
// ]]>