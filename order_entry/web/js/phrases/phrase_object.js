// <![CDATA[
var PHRASE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PHRASE.prototypeVersion < 1.6)
      throw("DNIS requires the Prototype JavaScript framework >= 1.6");
      
PHRASE.OBJECT = Class.create();

PHRASE.OBJECT.prototype = {

    initialize: function(message,sequence) {
        this.reset();
        this.message = message;
        this.sequence = sequence;
    },
    
    reset: function() {
        this.message = null;
        this.sequence = null;
    }
};
// ]]>