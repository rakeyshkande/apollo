// <![CDATA[
var PHRASES_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PHRASES_EVENTS.prototypeVersion < 1.6)
      throw("PHRASES_EVENTS requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("PHRASES_EVENTS requires FTD_DOM");
      
PHRASES_EVENTS.onExitButton = function(event) {
    doMainMenuAction('customerService');
};
      
PHRASES_EVENTS.onSaveButton = function(event) {
    if( validate() ) {
        PHRASES_AJAX.saveCardMessageData();
    }
};
      
PHRASES_EVENTS.onAddButton = function(event) {
    var tBody = $('listBody');
    var rowSize = tBody.rows.length;
    if( rowSize>0 ) {
        id = tBody.rows[rowSize-1].id;
        id = id.replace(/row___/,'downButton___');
        $(id).disabled=false;
        id = id.replace(/downButton___/,'downImage___');
        $(id).src='images/arrow_down_green.png';
    }
    
    addRow('',true,true);
    $('description___'+rowSize).focus();
};
      
PHRASES_EVENTS.onUpButton = function(event) {
    var button = FTD_DOM.whichElement(event);
    var selectedRow = button.parentNode.parentNode;
    var previousRow = selectedRow.previousSibling
    if( previousRow ) {
        if( previousRow.sectionRowIndex==0 ) {
            var id = previousRow.id;
            id = id.replace(/row___/,'upButton___');
            $(id).disabled=false;
            id = id.replace(/upButton___/,'upImage___');
            $(id).src='images/arrow_up_green.png';
            
            id = selectedRow.id;
            id = id.replace(/row___/,'upButton___');
            $(id).disabled=true;
            id = id.replace(/upButton___/,'upImage___');
            $(id).src='images/bullet_square_grey.png';
        } 
        
        if( selectedRow.sectionRowIndex==$('listBody').rows.length-1 ) {
            id = previousRow.id;
            id = id.replace(/row___/,'downButton___');
            $(id).disabled=true;
            id = id.replace(/downButton___/,'downImage___');
            $(id).src='images/bullet_square_grey.png';
            
            id = selectedRow.id;
            id = id.replace(/row___/,'downButton___');
            $(id).disabled=false;
            id = id.replace(/downButton___/,'downImage___');
            $(id).src='images/arrow_down_green.png';
        } 
        selectedRow.parentNode.insertBefore(selectedRow,previousRow);
    }
};
      
PHRASES_EVENTS.onDownButton = function(event) {
    
    var button = FTD_DOM.whichElement(event);
    var selectedRow = button.parentNode.parentNode;
    var nextRow = selectedRow.nextSibling;
    if( nextRow ) {
        if( nextRow.sectionRowIndex==$('listBody').rows.length-1 ) {
            var id = nextRow.id;
            id = id.replace(/row___/,'downButton___');
            $(id).disabled=false;
            id = id.replace(/downButton___/,'downImage___');
            $(id).src='images/arrow_down_green.png';
            
            id = selectedRow.id;
            id = id.replace(/row___/,'downButton___');
            $(id).disabled=true;
            id = id.replace(/downButton___/,'downImage___');
            $(id).src='images/bullet_square_grey.png';
        } 
        
        if( selectedRow.sectionRowIndex==0 ) {
            id = selectedRow.id;
            id = id.replace(/row___/,'upButton___');
            $(id).disabled=false;
            id = id.replace(/upButton___/,'upImage___');
            $(id).src='images/arrow_up_green.png';
            
            id = nextRow.id;
            id = id.replace(/row___/,'upButton___');
            $(id).disabled=true;
            id = id.replace(/upButton___/,'upImage___');
            $(id).src='images/bullet_square_grey.png';
        }
        selectedRow.parentNode.insertBefore(nextRow,selectedRow);
    }
};

// ]]>