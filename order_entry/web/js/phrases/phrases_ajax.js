// <![CDATA[
var PHRASES_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PHRASES_AJAX.prototypeVersion < 1.6)
      throw("PHRASES_EVENTS requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("PHRASES_AJAX requires FTD_DOM");

PHRASES_AJAX.getCardMessageData = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_CARD_MESSAGES';
    root.setAttributeNode(attr);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('phrasesAjax.do',FTD_AJAX_REQUEST_TYPE_POST,PHRASES_AJAX.getCardMessageDataCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

PHRASES_AJAX.getCardMessageDataCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="card_messages"]/record',xmlDoc);
        FTD_DOM.removeAllTableRows($('listBody'));
        
        for( idx=0; idx<records.length; idx++ ) {
            var message = FTD_XML.selectNodeText(records[idx],'message_txt');
            var active = (FTD_XML.selectNodeText(records[idx],'active_flag')=='Y');
            //var sequence = FTD_XML.selectNodeText(records[idx],'display_seq');
            addRow(message,active,idx==(records.length-1?true:false));
        }
    }
}

PHRASES_AJAX.saveCardMessageData = function() {
    $('save_button').disabled=true;
    $('add_button').disabled=true;
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SAVE_CARD_MESSAGES';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(buildPhraseXml()));
    attr = doc.createAttribute("name");
    attr.value = 'CARD_MESSAGES_XML';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request('phrasesAjax.do',FTD_AJAX_REQUEST_TYPE_POST,PHRASES_AJAX.saveCardMessageDataCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

PHRASES_AJAX.saveCardMessageDataCallback = function(transport) {
    $('save_button').disabled=false;
    $('add_button').disabled=false;
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        PHRASES_AJAX.getCardMessageData();
        alert('Card message changes have been saved.');
    }
}
// ]]>
