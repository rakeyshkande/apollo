// <![CDATA[
var JOE_VALIDATE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || JOE_VALIDATE.prototypeVersion < 1.6)
      throw("JOE_VALIDATE requires the Prototype JavaScript framework >= 1.6");
      
JOE_VALIDATE.carrierDeliveryOptions = function() {
    return true;
}

JOE_VALIDATE.skipPhoneValidation = function(phoneNumber) {
    
    phoneNumber = UTILS.trim(phoneNumber);
    if( phoneNumber==null || phoneNumber.length==0 ) {
        return true;
    }
    
    //Only look at digits
    if( v_phoneValidateRegex!=null ) {
        var testPhoneNumber = UTILS.stripNonDigits(phoneNumber);
        return v_phoneValidateRegex.test(testPhoneNumber);
    }
    
    return false;
}

JOE_VALIDATE.skipAutoPhoneSearch = function(recipientType) {
    
  recipientType = UTILS.trim(recipientType);
  if( recipientType =='FUNERAL HOME' || recipientType == 'HOSPITAL' || recipientType == 'NURSING HOME' || recipientType == 'CEMETERY' ) 
  {
    return true;
  }
  else
    return false;
}

JOE_VALIDATE.validateGiftCertificate = function() {   
    VALIDATE_AJAX.validateGiftCertificate($('paymentGCNumber').value);
}

JOE_VALIDATE.validateCreditCardNumber = function() {
    var retval = UTILS.mod10Check($('paymentCCNumber').value);
    return retval;
}

JOE_VALIDATE.carrierDeliveryOptions = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    //Being delivered by a florist, so item will not
    //be carrier delivered.
    if( orderObj.floristDelivered==true ) return true;
    
    if( orderObj.carrierDelivered==true ) {
        var optionsEl = orderObj.carrierDeliverOption;
        if( optionsEl==null ) return false;
    }
    
    return true;
}

JOE_VALIDATE.productOptions = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    //Not a valid option, so, return true
    //logMessage('JOE_VALIDATE.productOptions: orderObj.productObject= '+orderObj.productObject);
    if( elementId=="productOptionStandard" && orderObj.productObject["standardPrice"]==null) return true;
    if( elementId=="productOptionPremium" && orderObj.productObject["premiumPrice"]==null) return true;
    if( elementId=="productOptionDeluxe" && orderObj.productObject["deluxePrice"]==null) return true;
    if( elementId=="productOptionVariable" && orderObj.productObject["variablePrice"]==null) return true;
    
    var optionsEl = orderObj.productOption;
    if( optionsEl==null ) return false;
    
    return true;
}

JOE_VALIDATE.productOptionVariablePrice = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    if( orderObj.productOption!='productOptionVariable' ) 
        return true;
    
    //var variablePrice = parseFloat(orderObj.productOptionVariablePrice);
    var variablePrice = UTILS.formatStringToPrice(orderObj.productOptionVariablePrice);
    if( isNaN(variablePrice)==true ) return false;
    variablePrice = Math.round(variablePrice*100);
    
    var standardPrice = parseFloat(orderObj.productObject.standardPrice);
    if( isNaN(standardPrice)==true ) return false;
    standardPrice = Math.round(standardPrice*100);
    
    var maxPrice = parseFloat(orderObj.productObject.variablePrice);
    if( isNaN(maxPrice)==true ) return false;
    maxPrice = Math.round(maxPrice*100);
    
    if( variablePrice>=standardPrice && variablePrice<=maxPrice ) return true;

    return false;
}

JOE_VALIDATE.recipientPhoneNumber = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    var testValue = parseInt(orderObj.recipientPhone);
    
    if( isNaN(testValue) ) return false;
    
    testValue = orderObj.recipientPhone;
    var countryId = orderObj.recipientCountryorderObj;
    
    if( isCountryDomestic(countryId)==true && testValue.length!=10 )  return false;
    
    return true;
}

JOE_VALIDATE.customerPhoneNumber = function(elementId) {
    if( UTILS.isEmpty(elementId)==true ) 
        return false;
    
    var testValue = parseInt($(elementId).value);
    
    if( isNaN(testValue) ) return false;
    
    testValue = $(elementId).value;
    var countryId = $('customerCountryCombo').getValue();
    
    if( isCountryDomestic(countryId)==true && testValue.length!=10 )  return false;
    
    return true;
}

JOE_VALIDATE.productZipCode = function(cartIdx, elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    var testZip = orderObj.productZip;
    var countryId = orderObj.productCountry;
    
    if( isCountryDomestic(countryId)==false ) {
        return true;
    } else if( countryId=='US' ) {
        return ORDER_EVENTS.REGEX_US_ZIP.test(testZip);
    } else if( countryId=='CA' ) {
        if( testZip.length==3 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip);
        } else if( testZip.length==6 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip);
        } else {
            return false;
        }
    } else {
        var re = /^[\w]{0,6}$/;
        if( re.test(testZip==false) ) return false;
    }
    
    return true;
}

JOE_VALIDATE.recipientZip = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    var countryId = orderObj.recipientCountry;
    var testZip = orderObj.recipientZip;
    
    if( isCountryDomestic(countryId)==false ) {
        return true;
    } else if( countryId=='US' ) {
        return ORDER_EVENTS.REGEX_US_ZIP.test(testZip);
    } else if( countryId=='CA' ) {
        if( testZip.length==3 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip);
        } else if( testZip.length==6 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip);
        } else {
            return false;
        }
    } else {
        var re = /^[\w]{0,6}$/;
        if( re.test(testZip==false) ) return false;
    }
    
    return true;
}

JOE_VALIDATE.customerZip = function(elementId) {
    if( UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var testZip = $(elementId).value;
    var countryId = $('customerCountryCombo').getValue();
    
    if( isCountryDomestic(countryId)==false ) {
        return true;
    } else if( countryId=='US' ) {
        return ORDER_EVENTS.REGEX_US_ZIP.test(testZip);
    } else if( countryId=='CA' ) {
        if( testZip.length==3 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE_SHORT.test(testZip);
        } else if( testZip.length==6 ) {
            return ORDER_EVENTS.REGEX_CA_POSTAL_CODE.test(testZip);
        } else {
            return false;
        }
    } else {
        var re = /^[\w]{0,6}$/;
        if( re.test(testZip==false) ) return false;
    }
    
    return true;
}

JOE_VALIDATE.sourceCode = function(elementId) {
    if( UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var value = $(elementId).value;
    
    if( v_sourceCodeRegEx.test(value)==false || v_verifiedSourceCode==false ) {
        return false;
    }
    
    return true;
}

JOE_VALIDATE.recipientAddress = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    var elmtValue = orderObj['recipientAddress'];
    var s = String(elmtValue);
    
    //Get rid of everything bud numbers, digits, and white space
    var re = /[^\w ]/g;
    var testValue = s.replace(re,"").toUpperCase();
    
    //Look for "APO " at the beginning of the string
    re = /^APO\s/
    if( re.test(testValue)==true ) return false;
    
    //Look for " APO " anywhere in the string
    re = /\sAPO\s/
    if( re.test(testValue)==true ) return false;
    
//    //Look for "^APOBOX " at the beginning of the string
//    re = /^APOBOX\s/
//    if( re.test(testValue)==true ) return false;
//    
//    //Look for " APOBOX " anywhere in the string
//    re = /\sAPOBOX\s/
//    if( re.test(testValue)==true ) return false;
    
    
    //Look for "PO " at the beginning of the string
    re = /^PO\s/
    if( re.test(testValue)==true ) return false;
    
    //Look for " PO " anywhere in the string
    re = /\sPO\s/
    if( re.test(testValue)==true ) return false;
    
//    //Look for "^POBOX " at the beginning of the string
//    re = /^POBOX\s/
//    if( re.test(testValue)==true ) return false;
//    
//    //Look for " POBOX " anywhere in the string
//    re = /\sPOBOX\s/
//    if( re.test(testValue)==true ) return false;
    
    
    //Look for "FPO " at the beginning of the string
    re = /^FPO\s/
    if( re.test(testValue)==true ) return false;
    
    //Look for " FPO " anywhere in the string
    re = /\sFPO\s/
    if( re.test(testValue)==true ) return false;
    
//    //Look for "^FPOBOX " at the beginning of the string
//    re = /^FPOBOX\s/
//    if( re.test(testValue)==true ) return false;
//    
//    //Look for " FPOBOX " anywhere in the string
//    re = /\sFPOBOX\s/
//    if( re.test(testValue)==true ) return false;
    
    return true;
}

JOE_VALIDATE.addOnQuantity = function(cartIdx,elementId) {
    if( UTILS.isEmpty(cartIdx+'')==true || UTILS.isEmpty(elementId)==true ) 
        return false;
        
    var orderObj = v_cartList[cartIdx];
    if( orderObj==null ) return true;
    
    //Determine the appropriate addon checkbox
    var addOnCode = elementId.charAt(5);
    var chkElement = 'addOn'+addOnCode+'Checkbox';
    
    //If it's not checked, the no need to validate the quantity
    if( orderObj[chkElement]==false ) return true;
    
    //Validate the quantity
    var re = /^[123456789]$/
    return re.test($(elementId).value);
}

JOE_VALIDATE.paymentGCBalance = function() {
    if( JOE_REQUIRED.paymentGC() ) {
        try {
            var gcAmount = parseFloat($('paymentGCAmount').innerHTML);
        } catch (err) {
            notifyUser('Error while trying to apply gift certificate amount to the cart balance.  Contact BACOM immediately.');
            return false;
        }
        
        try {
            var total = parseFloat($('total').innerHTML);
        } catch (err) {
            notifyUser('Error while trying to determine order total at checkout.  Contact BACOM immediately.');
            return false;
        }

        if( gcAmount>=total ) return true;
        
        //var otherPaymentOption=$('paymentGCBalanceCombo').getValue;
        otherPaymentOption = FTD_DOM.getSelectedValue($('paymentGCBalanceCombo'));
        if( otherPaymentOption=='NC' )
            var otherAmount = parseFloat($('paymentNCAmount').innerHTML);
        else if( otherPaymentOption=='CC' )
            otherAmount = parseFloat($('paymentCCAmount').innerHTML);
        else
            otherAmount = 0.00;
        //alert('Tendered: '+(gcAmount+otherAmount)+'     Total: '+total);
        if( (gcAmount+otherAmount).toFixed(2) >=total ) return true;
        
        notifyUser('Unable to determine gift certificate payments applied.  Please enter a gift certificate number and add any additional monies which will need to be applied to the balance of the cart total.');
        return false;
    }
    
    return true;
}

//This method validates ccnumber in Billing fields 
JOE_VALIDATE.BillingFieldsValidation = function(elementId,fieldObj)
	{    	  
	  var elementValue = UTILS.trim($(elementId).value);	  
	  var result=true;		   
	  if(elementValue.match(/(^|[^\d])(\d{13,20})([^\d]|$)/g) )
	 	 {	  		   
		   fieldObj.errorTextNormal="STOP! There is an error within the "+fieldObj.labelText+". Clear the field and try again.";
		   result=false;
		 }
	  return result;
	}

JOE_VALIDATE.CardMessageValidation = function(cartListIndex,elementId,fieldObj)
	{ 	
	  fieldObj.labelText="Card Message";
	  return JOE_VALIDATE.BillingFieldsValidation(elementId,fieldObj);
	}  
  
JOE_VALIDATE.EmailValidation = function(elementId,fieldObj)
	{
	fieldObj.errorTextNormal="";
	fieldObj.errorText="";
	var elementValue = $(elementId).value;	
	  var result=false;
	  result =(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/).test(elementValue);
	  if(result)
		{
		  result = JOE_VALIDATE.BillingFieldsValidation(elementId,fieldObj);
		}
	  else{
		  fieldObj.errorTextNormal="A valid email address is required if the customer is opted in to receive promotional emails. If you do not see the the Subscription Opt In checkbox, it's because they have already opted in."
	  }
		  
	  return result;
	}
// ]]>