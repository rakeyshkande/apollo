// <![CDATA[
if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("dnis.js requires the Prototype JavaScript framework >= 1.5");

var v_verifiedSourceCode = false;

var v_partnerScriptFields = new Array();

var globalCCNumber='';
var display_credit_card = '';

function validateDNIS() {
    //alert('DNIS validation will happen now');
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_DNIS';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',$('callDataDNIS').value);
    attr = doc.createAttribute("name");
    attr.value = 'DNIS_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',document.getElementById("securitytoken").value);
    attr = doc.createAttribute("name");
    attr.value = 'SEC_TOKEN';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,validateDNISCallback,true,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    setProcessing(true);
    newRequest.send();
}

function validateDNISCallback(transport) {
    //alert(transport.responseText);
    $('callDataSourceCodePwd').setValue('');
    setProcessing(false);
    var xmlDoc = parseResponse(transport);

    if( xmlDoc!=null ) {
        var record;
        var strVal;
        var el = $('callDataDNIS');

        if( checkResultSetStatus(xmlDoc,"dnis",false,false,false) ) {
            record = XPath.selectNodes('/result/rs[@name="dnis"]/record',xmlDoc)[0];
            if( record==null ) {
                notifyUser('Server failed to retrieve a record for DNIS '+el.getValue(),false,true);
                el.style.backgroundColor = v_errorBgColor;
                el.style.color = v_errorTextColor;
                el.style.fontWeight = 'bold';
                el.disabled=false;
                el.focus();
                el.select();
            } else {
                el.style.backgroundColor = v_normalBgColor;
                el.style.color = v_normalTextColor;
                el.style.fontWeight = 'normal';
                $('callDataDNISDesc').innerHTML = FTD_XML.selectNodeText(record,'description');
                $('callDataOrigin').setValue(FTD_XML.selectNodeText(record,'origin'));
                $('callDataCompany').setValue(FTD_XML.selectNodeText(record,'company_id'));
                $('callDataCompanyName').setValue(FTD_XML.selectNodeText(record,'company_name'));
                $('confCompanyName').innerHTML = FTD_XML.selectNodeText(record,'company_name');
                $('callDataSourceCode').setValue(FTD_XML.selectNodeText(record,'default_source_code'));
                if (v_callTimestamp == null) {
                    v_callTimestamp = FTD_XML.selectNodeText(XPath.selectNodes('/result',xmlDoc)[0],'order-timestamp');
                }
                $('callDataSourceCodeSearch').disabled=false;
                $('callDataSourceCode').disabled=false;
                $('detailDiv').style.display='block';

                if($('callDataSourceCode').getValue().length>0) {
                    processValidatedSourceCode(xmlDoc);
                }

                $('callDataSourceCode').focus();
                $('callDataSourceCode').select();
                v_mainAccordion.openByIndex(0);
                $('orderTabberId').tabber.tabShow(0);
                ORDER_EVENTS.startSaleProcess();

                if( v_cartList.length==0 ) {
                    CART_EVENTS.createNewOrder();
                    v_cartList[0].populateDOM();
                }
    populateSCRecipientInfo();
    populateSCCustomerInfo();
            }
        } else {
            el.style.backgroundColor = v_errorBgColor;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
            el.disabled=false;
            el.focus();
            el.select();
        }
    }
}

function validateSourceCode(resetAddons) {
    $('callDataSourceCodePwd').setValue('');
    v_verifiedSourceCode = false;
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_SOURCE_CODE';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',$('callDataDNIS').value);
    attr = doc.createAttribute("name");
    attr.value = 'DNIS_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',$('callDataSourceCode').value);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param.appendChild(FTD_XML.createElementWithText(doc,'reset_addons',(resetAddons==false?"false":"true")));
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',document.getElementById("securitytoken").value);
    attr = doc.createAttribute("name");
    attr.value = 'SEC_TOKEN';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,validateSourceCodeCallback,true,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

function validateSourceCodeCallback(transport) {
    var xmlDoc = parseResponse(transport);

    if( xmlDoc!=null ) {
        processValidatedSourceCode(xmlDoc);
  populateSCRecipientInfo();
  populateSCCustomerInfo();
    }
}

function processValidatedSourceCode(xmlDoc) {
    if( xmlDoc!=null ) {
        var record;
        var strVal;
        var el = $('callDataSourceCode');
        var bSourceCodeChanged = false;
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var resetAddons=false;
        if( echo && echo!=null ) {
            resetAddons = (FTD_XML.selectNodeText(echo,'reset_addons')=='true'?true:false);
        }

        v_partnerScriptFields = new Array();
        if( checkResultSetStatus(xmlDoc,"partner_script",true,false,false)) {
          var resultSetRecords = XPath.selectNodes('/result/rs[@name="partner_script"]/record',xmlDoc);

          for (i=0;i<resultSetRecords.length;i++){
            var script_id = FTD_XML.selectNodeText(resultSetRecords[i],'script_id');
            var script_value = FTD_XML.selectNodeText(resultSetRecords[i],'script_value');
            v_partnerScriptFields[script_id] = script_value;
          }
        }

        if( checkResultSetStatus(xmlDoc,"source_master",false,false,true) ) {
            record = XPath.selectNodes('/result/rs[@name="source_master"]/record',xmlDoc)[0];
            if( record==null ) {
                notifyUser('Server failed to retrieve a record for source code '+el.getValue(),false,true);
                el.style.backgroundColor = v_errorBgColor;
                el.style.color = v_errorTextColor;
                el.style.fontWeight = 'bold';
                el.focus();
            } else {
                el.style.backgroundColor = v_normalBgColor;
                el.style.color = v_normalTextColor;
                el.style.fontWeight = 'normal';

                var sourceCode = FTD_XML.selectNodeText(record,'source_code');
//                if( v_masterSourceCode.id!=sourceCode) {
                    bSourceCodeChanged = true;
//                }

                var newSourceCode = new SOURCECODE.OBJECT();
                newSourceCode.id = sourceCode;
                newSourceCode.description = FTD_XML.selectNodeText(record,'description');
                newSourceCode.partnerId = FTD_XML.selectNodeText(record,'partner_id');
                newSourceCode.membershipDataRequired = (FTD_XML.selectNodeText(record,'membership_data_required')=='Y');
                newSourceCode.password = FTD_XML.selectNodeText(record,'invoice_password');
                newSourceCode.iotwFlag = (FTD_XML.selectNodeText(record,'iotw_flag')=='Y');
                newSourceCode.paymentMethod = FTD_XML.selectNodeText(record,'payment_method_id');
                newSourceCode.resourceId = FTD_XML.selectNodeText(record,'resource_id');
                $('partnerName').setValue(FTD_XML.selectNodeText(record,'partner_name'));
                newSourceCode.recpt_location_type = FTD_XML.selectNodeText(record,'recpt_location_type');
                newSourceCode.recpt_business_name = FTD_XML.selectNodeText(record,'recpt_business_name');
                newSourceCode.recpt_location_detail = FTD_XML.selectNodeText(record,'recpt_location_detail');
                newSourceCode.recpt_address = FTD_XML.selectNodeText(record,'recpt_address');
                newSourceCode.recpt_zip_code = FTD_XML.selectNodeText(record,'recpt_zip_code');
                newSourceCode.recpt_city = FTD_XML.selectNodeText(record,'recpt_city');
                newSourceCode.recpt_state_id = FTD_XML.selectNodeText(record,'recpt_state_id');
                newSourceCode.recpt_country_id = FTD_XML.selectNodeText(record,'recpt_country_id');
                newSourceCode.recpt_phone = FTD_XML.selectNodeText(record,'recpt_phone');
                newSourceCode.recpt_phone_ext = FTD_XML.selectNodeText(record,'recpt_phone_ext');
                newSourceCode.cust_first_name = FTD_XML.selectNodeText(record,'cust_first_name');
                newSourceCode.cust_last_name = FTD_XML.selectNodeText(record,'cust_last_name');
                newSourceCode.cust_daytime_phone = FTD_XML.selectNodeText(record,'cust_daytime_phone');
                newSourceCode.cust_daytime_phone_ext = FTD_XML.selectNodeText(record,'cust_daytime_phone_ext');
                newSourceCode.cust_evening_phone = FTD_XML.selectNodeText(record,'cust_evening_phone');
                newSourceCode.cust_evening_phone_ext = FTD_XML.selectNodeText(record,'cust_evening_phone_ext');
                newSourceCode.cust_address = FTD_XML.selectNodeText(record,'cust_address');
                newSourceCode.cust_zip_code = FTD_XML.selectNodeText(record,'cust_zip_code');
                newSourceCode.cust_city = FTD_XML.selectNodeText(record,'cust_city');
                newSourceCode.cust_state_id = FTD_XML.selectNodeText(record,'cust_state_id');
                newSourceCode.cust_country_id = FTD_XML.selectNodeText(record,'cust_country_id');
                newSourceCode.cust_email_address = FTD_XML.selectNodeText(record,'cust_email_address');
                newSourceCode.allowFreeShippingFlag = FTD_XML.selectNodeText(record, 'allow_free_shipping_flag');
                v_sourceCodeAllowsFreeShipping = FTD_XML.selectNodeText(record, 'allow_free_shipping_flag');

                newSourceCode.sourceType = FTD_XML.selectNodeText(record, 'source_type');

                if (newSourceCode.sourceType.indexOf("AAA") > -1)
                	{
                	document.getElementById("membershipSkip").disabled = true;
                	$('membershipIdSearch').style.display='block';
                	}


                //Get the billing info
                var biRecords = XPath.selectNodes('/result/rs[@name="billing_info"]/record',xmlDoc);
                var idx;
                if( biRecords!=null ) {
                    for( idx=0; idx<biRecords.length; idx++ ) {
                        var giftCertificate = (FTD_XML.selectNodeText(biRecords[idx],'gift-certificate')=='Y');
                        if( giftCertificate==true ) {
                            newSourceCode.allowGCPayment = true;
                            continue;
                        }
                        var displayType = FTD_XML.selectNodeText(biRecords[idx],'prompt-type');
                        if( displayType==null || displayType.length==0 ) {
                            displayType = 'TEXTBOX';
                        }

                        if( displayType!='TEXTBOX' && displayType!='STATIC' && displayType!='DROPDOWN' )
                            continue;

                        var biObject = new BILLING_INFO.OBJECT();
                        var sequence = FTD_XML.selectNodeText(biRecords[idx],'sequence');
                        biObject.sequence = sequence;
                        biObject.label = FTD_XML.selectNodeText(biRecords[idx],'display-prompt');
                        biObject.displayType = displayType;
                        biObject.regex = FTD_XML.selectNodeText(biRecords[idx],'validation-regex');
                        biObject.description = FTD_XML.selectNodeText(biRecords[idx],'info-description');

                        var biOptions = XPath.selectNodes('/result/rs[@name="billing_info"]/record[sequence="'+sequence+'"]/options/option',xmlDoc);
                        if( biOptions!=null ) {
                            for( var bIdx=0; bIdx<biOptions.length; bIdx++ ) {
                                var opt = new BILLING_INFO_OPTION.OBJECT();
                                opt.sequence=FTD_XML.selectNodeText(biOptions[bIdx],'sequence');
                                opt.optionName=FTD_XML.selectNodeText(biOptions[bIdx],'name');
                                opt.optionValue=FTD_XML.selectNodeText(biOptions[bIdx],'value');
                                biObject.billingOptions.push(opt);
                            }
                        }

                        newSourceCode.billingInfo.push(biObject);
                    }
                }

                if( UTILS.isEmpty(newSourceCode.resourceId)==false &&
                    checkResultSetStatus(xmlDoc,"partner_permission",true,false,false) == false ) {
                        // Preferred Partner (e.g., USAA) permissions?
                        $('detailDiv').style.display='none';
                        $('sourceCodePreferredPartnerDiv').style.display='block';
                        var resultSet = XPath.selectNodes('/result/rs[@name="partner_permission"]',xmlDoc)[0];
                        var partnerName = FTD_XML.getAttributeText(resultSet,'type');
                        var transferNumber = FTD_XML.getAttributeText(resultSet,'message');
                        $('preferredPartnerName').innerHTML = partnerName;
                        $('preferredPartnerNumber').innerHTML = transferNumber;
                        waitToSetFocus('callDataSourceCode');
                } else {

                if( UTILS.isEmpty(newSourceCode.password)==false ) {
                    v_pendingSourceCode = newSourceCode;
                    $('callDataSourceCodeDesc').innerHTML = v_pendingSourceCode.description;
                    v_verifiedSourceCode=false;
                    showSourceCodePwdPanel();
                } else {
                    $('sourceCodePwdDiv').style.display='none';
                    $('sourceCodePreferredPartnerDiv').style.display='none';
                    $('detailDiv').style.display='block';
                    if( bSourceCodeChanged ) {
                        v_masterSourceCode = newSourceCode;
                        $('callDataSourceCodeDesc').innerHTML = v_masterSourceCode.description;
                        createBillingInfoFields();
                        setCartPaymentMethods();

                        if( v_masterSourceCode.membershipDataRequired==true ) {
                            $('membership').style.display='block';
                        } else {
                            $('membership').style.display='none';
                        }

                        v_verifiedSourceCode = true;

                        //Force availability checks
                        resetCartTotals();
                        for( idx=0; idx<v_cartList.length; idx++ ) {
                            if( v_cartList[idx]==null ) continue;
                            resetThumbnailDetail(idx);
                            v_cartList[idx].productGenerallyAvailable=false;
                            ORDER_AJAX.getProductDetail(v_cartList[idx].productId,idx,'ORDER',resetAddons,true);
                        }
                    }

                    v_checkAvailabilityTimer.startTimer();
                }

            //Pull up the favorites
            for( idx=0; idx<PRODUCT_SEARCH.allFavoriteProducts.length; idx++ ) {
                //Now get the product detail
                if(PRODUCT_SEARCH.allFavoriteProducts[idx]==null) continue;
                ORDER_AJAX.getProductDetail(PRODUCT_SEARCH.allFavoriteProducts[idx].productId,idx,'FAVORITES',true,false);
            }

            //Get the top products
            PRODUCT_SEARCH_AJAX.getPopularProducts();

            }
            }

        } else {
            el.style.backgroundColor = v_errorBgColor;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
            el.focus();

            $('productSourceCode').value = v_masterSourceCode.id;
            for( idx=0; idx<v_cartList.length; idx++ ) {
                if( v_cartList[idx]!=null ) {
                    v_cartList[idx].productSourceCode = v_masterSourceCode.id;
                }
            }

        }
    }
}

function setMembershipRequired() {
    var bDisplayMembership=false;

    if( v_masterSourceCode.membershipDataRequired==true /*&& areNonIOTWItemsInCart()==true*/ ) {
        bDisplayMembership=true;
        $('membership').style.display = 'block';

        var fld = findFieldObject('membershipId', v_currentCartIdx);
        if( fld!=null ) fld.isRequired();
    } else {
        bDisplayMembership=false;
        $('membership').style.display = 'none';
        $('membershipSkip').checked=false;
    }

    return bDisplayMembership;
}

function changeSourceCode(newSourceCode, bValidate) {
    $('callDataSourceCode').value = newSourceCode;
    $('callDataSourceCodeDesc').innerHTML='';
    $('productSourceCode').value = newSourceCode;
    $('productIOTWDescription').innerHTML = '';
    for( idx=0; idx<v_cartList.length; idx++ ) {
        if( v_cartList[idx]!=null ) {
            v_cartList[idx].productSourceCode = newSourceCode;
            v_cartList[idx].productIOTWDescription = null;
        }
    }

    if( bValidate==true ) {
        validateSourceCode();
    } else {
        v_verifiedSourceCode=true;
    }
}

function setCartPaymentMethods() {
    $('paymentCCCaption').style.display = 'none';
    $('paymentCCDiv').style.display     = 'none';
    $('paymentCCManAuthDiv').style.display = 'none';
    $('paymentGCCaption').style.display = 'none';
    $('paymentGCDiv').style.display     = 'none';
    $('paymentINCaption').style.display = 'none';
    $('paymentINDiv').style.display     = 'none';
    $('paymentNCCaption').style.display = 'none';
    $('paymentNCDiv').style.display     = 'none';
    var scpm = v_masterSourceCode.paymentMethod;
    var pt;
    var pm;
    var oldCCPaymentMethod = $('paymentCCTypeCombo').getValue();
    FTD_DOM.clearSelect('paymentTypeCombo');

    if(scpm==null || scpm.length==0) {
        pt = v_paymentTypes['C'];
        FTD_DOM.clearSelect('paymentCCTypeCombo');
        FTD_DOM.insertOption('paymentCCTypeCombo','','',true);
        for( var key in v_paymentMethods ) {
            pm = v_paymentMethods[key];
            if( pm.type==pt.id ) {
                FTD_DOM.insertOption('paymentCCTypeCombo',pm.description,pm.id,(pm.id==oldCCPaymentMethod?true:false));
            }
        }
    } else {
        pm = v_paymentMethods[scpm];
        if( pm==null ) {
            alert('Unsupported payment method of '+scpm+' being presented to JOE.\r\nSource code has been configured incorrectly.\r\nContact BACOM immediatedly.');
            return;
        }

        pt = v_paymentTypes[pm.type];
        if( pt==null ) {
            alert('Unsupported payment type '+pm.type+' for payment method of '+scpm+' being presented to JOE.\r\nPayment method has been configured incorrectly.\r\nContact BACOM immediatedly.');
            return;
        }

        if( pt.id=='C' ) {
            FTD_DOM.clearSelect('paymentCCTypeCombo');
            FTD_DOM.insertOption('paymentCCTypeCombo',pm.description,pm.id,(pm.id==oldCCPaymentMethod?true:false));
        }
    }

    FTD_DOM.insertOption('paymentTypeCombo',pt.display,pt.id,true);

    //Case where gift certificates are allowed along with a nother payment type
    //Defect 4432
    if( v_masterSourceCode.allowGCPayment==true && pt.id!='G' ) {
        FTD_DOM.insertOption('paymentTypeCombo',v_paymentTypes['G'].display,v_paymentTypes['G'].id,false);
    }

    //Always add nocharge options except for gift certificates and the "nocharge" type.
    if( pt.id!='G' && pt.id!='N' ) {
        FTD_DOM.insertOption('paymentTypeCombo',v_paymentTypes['N'].display,v_paymentTypes['N'].id,false);
    }

    FTD_DOM.selectOptionByValue($('paymentTypeCombo'),pt.id);
    BILLING_EVENTS.paymentTypeChanged();
}

function showSourceCodePwdPanel() {
    $('detailDiv').style.display='none';
    $('sourceCodePwdDiv').style.display='block';
    //$('callDataSourceCodePwd').focus();
    waitToSetFocus('callDataSourceCodePwd');
}

function validateSourceCodePwd() {
    var retval = false;
    var sourceCode;
    if( v_pendingSourceCode!=null ) {
        sourceCode = v_pendingSourceCode;
    } else {
        sourceCode = v_masterSourceCode;
    }

    var checkPwd = sourceCode.password;
    if( UTILS.isEmpty(checkPwd) ) {
        retval = true;
        v_masterSourceCode = sourceCode;
        $('callDataSourceCodePwd').value = '';
    } else {
        var strPwd = $('callDataSourceCodePwd').getValue();
        if( checkPwd==strPwd ) {
            v_masterSourceCode = sourceCode;
            $('sourceCodePwdDiv').style.display='none';
            $('detailDiv').style.display='block';

            //Don't switch if the billing page is showing
            if( $('upperContent').visible())
                waitToSetFocus('productId');

            createBillingInfoFields();
            setCartPaymentMethods();
      populateSCRecipientInfo();
      populateSCCustomerInfo();
            if( v_masterSourceCode.membershipDataRequired==true ) {
                $('membership').style.display='block';
            } else {
                $('membership').style.display='none';
            }

            retval=true;
        } else {
            notifyUser('Invalid password',false,true);
            $('callDataSourceCodePwd').focus();
            $('callDataSourceCodePwd').select();
        }
    }

    return retval;
}

function createBillingInfoFields() {
    var tableBody = $('billingInfoTbody');
    FTD_DOM.removeAllTableRows(tableBody);
    v_billingInfoFields = new Array();

    var biObjects = v_masterSourceCode.billingInfo;
    var startTabIndex = $('paymentNCAuthPwd').tabIndex+1;
    var colCnt=0;
    for( var idx=0; idx<biObjects.length; idx++ ) {
        var newRow;
        var cell;
        var input;
        var label;
        if( idx%2==0 ) {
            newRow = tableBody.insertRow(tableBody.rows.length);
            colCnt=0;
        }

        cell = newRow.insertCell(colCnt++);
        label = new FTD_DOM.LabelElement( {
            id:'biField___'+idx+'Label',
            'class':'labelRight',
            'for':'biField___'+idx} ).createNode();
        label.innerHTML = biObjects[idx].label;
        cell.appendChild(label);
        cell.className='labelRight';

        var elementType = biObjects[idx].displayType;
        if( elementType=='TEXTBOX' ) {
            cell = newRow.insertCell(colCnt++);
            cell.align = "center";
            input = new FTD_DOM.InputElement( {
                id:'biField___'+idx,
                type:'text',
                size:'25',
                name:'biField',
                tabIndex:startTabIndex++} ).createNode();
            cell.appendChild(input);
        } else if( elementType=='STATIC') {
            try {
                var value = biObjects[idx].billingOptions[0].optionValue;
            } catch (err) {
                value='';
            }
            cell = newRow.insertCell(colCnt++);
            input = new FTD_DOM.InputElement( {
                id:'biField___'+idx,
                type:'text',
                size:'25',
                name:'biField',
                disabled:'disabled',
                value:value,
            tabIndex:startTabIndex++} ).createNode();
            cell.appendChild(input);
        } else if( elementType=='DROPDOWN') {
            cell = newRow.insertCell(colCnt++);
            var select = new FTD_DOM.SelectElement( {
                id:'biField___'+idx,
                name:'biField',
                size:'1',
                tabIndex:startTabIndex++} ).createNode();
            cell.appendChild(select);
            var opts = biObjects[idx].billingOptions;

            for( var oIdx=0; oIdx<opts.length; oIdx++ ) {
                FTD_DOM.insertOption('biField___'+idx,opts[oIdx].optionName,opts[oIdx].optionValue,false);
            }
        }

        input = new FTD_DOM.InputElement( {
            id:'biField___'+idx+'Name',
            type:'hidden',
            value:biObjects[idx].description
            } ).createNode();
        cell.appendChild(input);

        var fld = new FIELD.OBJECT('biField___'+idx,'biField___'+idx+'Label',null);
        fld.requiredFlag = true;
        fld.requiredCondition = null;
        fld.requiredConditionType = null;
        var str = biObjects[idx].regex;
        if( str==null || str.length==0 ) {
            fld.validateFlag = false;
            fld.validateExpression = null;
            fld.validateExpressionType = null;
        } else {
            fld.validateFlag = true;
            fld.validateExpression = str;
            fld.validateExpressionType = 'REGEX';
        }
        fld.errorText = 'Invalid value entered';
        fld.title = biObjects[idx].label;
        fld.maxLength = null;
        fld.tabIndex = startTabIndex-1;
        fld.tabIndexInitial = startTabIndex-1;
        fld.tabIndexExpression = null;
        fld.tabIndexExpressionType = null;
        fld.accordionCtrlIdx = 1;
        fld.tabCtrlIdx = null;
        fld.recalcOrderOnChange = false;
        fld.validateProductOnChange = false;
        fld.accessKey = null;
        fld.orderXmlNode = null;
        fld.calcXmlFlag = 'Y';
        v_billingInfoFields.push(fld);

        fld.setup();
    }

    if( tableBody.rows.length==0 )
        $('paymentBillingInfo').style.display = 'none';
    else
        $('paymentBillingInfo').style.display = 'block';
}

function populateSCCustomerInfo() {
    if ($('customerBillingPhone').value=='' &&
      $('customerBillingPhoneExt').value=='' &&
      $('customerEveningPhone').value=='' &&
      $('customerEveningPhoneExt').value=='' &&
      $('customerFirstName').value=='' &&
      $('customerLastName').value=='' &&
      $('customerAddress').value=='' &&
      $('customerZip').value=='' &&
      $('customerCity').value==''
      ) {
  if (v_masterSourceCode.cust_daytime_phone!=null) {
          v_customerPhoneChecked = true;
    $('customerBillingPhone').value=v_masterSourceCode.cust_daytime_phone;
    $('customerPhoneSearch').style.visibility = 'visible';
              //On ie6, the image gets lost with visibility set to hidden
              //so, need to recreate it
              var img = $('customerPhoneSearchImage');
              if( img ) {
                  img.style.visibility='visible';
             } else {
                  img = new FTD_DOM.DOMElement('img', {
                     id:'customerPhoneSearchImage',
                      alt:'Customer phone search',
                      src:'images/magnify.jpg'
                  }).createNode();
                  $('customerPhoneSearch').appendChild(img);
               }
  }
  if (v_masterSourceCode.cust_daytime_phone_ext)
        $('customerBillingPhoneExt').value=v_masterSourceCode.cust_daytime_phone_ext;
  if (v_masterSourceCode.cust_evening_phone!=null)
        $('customerEveningPhone').value=v_masterSourceCode.cust_evening_phone;
  if (v_masterSourceCode.cust_evening_phone_ext!=null)
        $('customerEveningPhoneExt').value=v_masterSourceCode.cust_evening_phone_ext;
  if (v_masterSourceCode.cust_first_name!=null)
        $('customerFirstName').value=v_masterSourceCode.cust_first_name;
  if (v_masterSourceCode.cust_last_name!=null)
        $('customerLastName').value=v_masterSourceCode.cust_last_name;
  if (v_masterSourceCode.cust_address!=null)
        $('customerAddress').setValue(v_masterSourceCode.cust_address);

  if (v_masterSourceCode.cust_country_id!=null && v_masterSourceCode.cust_zip_code!=null && v_masterSourceCode.cust_state_id!=null &&
    v_masterSourceCode.cust_country_id!='' && v_masterSourceCode.cust_zip_code!='' && v_masterSourceCode.cust_state_id!='') {
        CUSTOMER_EVENTS.changeCustomerZip(v_masterSourceCode.cust_zip_code,v_masterSourceCode.cust_state_id,v_masterSourceCode.cust_country_id);
  } else {
    if (v_masterSourceCode.cust_zip_code!=null)
          $('customerZip').value=v_masterSourceCode.cust_zip_code;
    if (v_masterSourceCode.cust_state_id && v_masterSourceCode.cust_state_id!='')
      FTD_DOM.selectOptionByValue('customerStateCombo',v_masterSourceCode.cust_state_id);
    if (v_masterSourceCode.cust_country_id!=null && v_masterSourceCode.cust_country_id!=''){
      FTD_DOM.selectOptionByValue('customerCountryCombo',v_masterSourceCode.cust_country_id);
      CUSTOMER_EVENTS.onCustomerCountryChange(null);
    }
  }
        if (v_masterSourceCode.cust_city)
        $('customerCity').value=v_masterSourceCode.cust_city;
  if (v_masterSourceCode.cust_email_address!=null)
    $('customerEmailAddress').value=v_masterSourceCode.cust_email_address;
  setOrderData();
      v_cartList[v_currentCartIdx].populateThumbNail();

    }
}
function populateSCRecipientInfo() {
   if (
  $('recipientBusiness').value=='' &&
  $('recipientRoom').value=='' &&
  $('recipientPhone').value=='' &&
  $('recipientPhoneExt').value=='' &&
  $('recipientCity').value=='' &&
  $('recipientAddress').value=='' &&
  $('recipientZip').value==''
        ) {

    if (v_masterSourceCode.recpt_country_id!=null && v_masterSourceCode.recpt_country_id!='') {
      ORDER_EVENTS.onProductCountryChange(null);
      ORDER_EVENTS.changeProductZip(v_masterSourceCode.recpt_zip_code,v_masterSourceCode.recpt_country_id);
      ORDER_EVENTS.changeRecipientZip(v_masterSourceCode.recpt_zip_code, v_masterSourceCode.recpt_state_id, v_masterSourceCode.recpt_country_id);
      ORDER_EVENTS.onRecipLocationTypeChanged(null);
    }
    if (v_masterSourceCode.recpt_phone!=null) {
      $('recipientPhoneChecked').checked = true;
      $('recipientPhone').value=v_masterSourceCode.recpt_phone;
      $('recipientPhoneSearch').style.visibility = 'visible';
                  //On ie6, the image gets lost with visibility set to hidden
                  //so, need to recreate it
                  var img = $('recipientPhoneSearchImage');
                  if( img ) {
                      img.style.visibility='visible';
                  } else {
                      img = new FTD_DOM.DOMElement('img', {
                            id:'recipientPhoneSearchImage',
                            alt:'Recipient phone search',
                            src:'images/magnify.jpg'
                      }).createNode();
                      $('recipientPhoneSearch').appendChild(img);
                  }
    }
    if (v_masterSourceCode.recpt_phone_ext!=null)
      $('recipientPhoneExt').value=v_masterSourceCode.recpt_phone_ext;
    if (v_masterSourceCode.recpt_city!=null)
      $('recipientCity').value=v_masterSourceCode.recpt_city;
    if (v_masterSourceCode.recpt_address!=null)
      $('recipientAddress').setValue(v_masterSourceCode.recpt_address);
    if (v_masterSourceCode.recpt_location_type!=null && v_masterSourceCode.recpt_location_type!='') {
      FTD_DOM.selectOptionByValue($('recipientType'),v_masterSourceCode.recpt_location_type);
      ORDER_EVENTS.onRecipLocationTypeChanged();
    }
    if (v_masterSourceCode.recpt_business_name!=null)
      $('recipientBusiness').value=v_masterSourceCode.recpt_business_name;
    if (v_masterSourceCode.recpt_location_detail!=null)
      $('recipientRoom').value=v_masterSourceCode.recpt_location_detail;
          setOrderData();
        v_cartList[v_currentCartIdx].populateThumbNail();
  }
}

function getUpdateOrderInfo(externalOrderNumber) {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_UPDATE_ORDER_INFO';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',externalOrderNumber);
    attr = doc.createAttribute("name");
    attr.value = 'EXTERNAL_ORDER_NUMBER';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,getUpdateOrderInfoCallback,true,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    setProcessing(true);
    newRequest.send();
}

function getUpdateOrderInfoCallback(transport) {
    //alert(transport.responseText);
    var xmlDoc = parseResponse(transport);

    if( xmlDoc!=null ) {
        if( checkResultSetStatus(xmlDoc,"order",false,false,false) ) {
            record = XPath.selectNodes('/result/rs[@name="order"]/record',xmlDoc)[0];
            if( record==null ) {
                notifyUser('Server failed to retrieve a record for order '+$('updateOrderId').value,false,true);
            } else {
                var orderObj = v_cartList[v_currentCartIdx];
                if(orderObj != null) {
                    $('callDataSourceCode').value = FTD_XML.selectNodeText(record,'source_code');
                    $('productSourceCode').value = $('callDataSourceCode').value;
                    orderObj.productSourceCode = $('callDataSourceCode').value;
                    $('productOccasion').value = FTD_XML.selectNodeText(record,'occasion');
                    orderObj.productOccasion = $('productOccasion').value;
                    $('productCountry').value = UTILS.trim(FTD_XML.selectNodeText(record,'recip_country'));
                    orderObj.productCountry = $('productCountry').value;
                    $('recipientCountry').value = $('productCountry').value;
                    orderObj.recipientCountry = $('recipientCountry').value;
                    uo_recipientZip = UTILS.trim(FTD_XML.selectNodeText(record,'recip_zip_code'));
                    if (uo_recipientZip != null && uo_recipientZip.length > 5) {
                        if ($('productCountry').value == 'US') {
                            uo_recipientZip = uo_recipientZip.substring(0,5);
                        } else if ($('productCountry').value == 'CA') {
                            uo_recipientZip = uo_recipientZip.substring(0,3);
                        }
                    }
                    $('productZip').value = uo_recipientZip;
                    orderObj.productZip = $('productZip').value;
                    $('recipientZip').value = $('productZip').value;
                    orderObj.recipientZip = $('productZip').value;
                    $('productDeliveryDate').value = FTD_XML.selectNodeText(record,'delivery_date');
                    orderObj.productDeliveryDate = $('productDeliveryDate').value;
                    $('languageId').value = FTD_XML.selectNodeText(record,'language_id');
                    orderObj.languageId = $('languageId').value;
                    $('cardMessage').value = UTILS.trim(FTD_XML.selectNodeText(record,'card_message'));
                    orderObj.cardMessage = $('cardMessage').value;
                    $('recipientPhone').value = FTD_XML.selectNodeText(record,'recip_phone_number');
                    orderObj.recipientPhone = $('recipientPhone').value;
                    $('recipientPhoneExt').value = UTILS.trim(FTD_XML.selectNodeText(record,'recip_phone_ext'));
                    orderObj.recipientPhoneExt = $('recipientPhoneExt').value;
                    $('recipientFirstName').value = FTD_XML.selectNodeText(record,'recip_first_name');
                    orderObj.recipientFirstName = $('recipientFirstName').value;
                    $('recipientLastName').value = FTD_XML.selectNodeText(record,'recip_last_name');
                    orderObj.recipientLastName = $('recipientLastName').value;
                    $('recipientAddress').value = FTD_XML.selectNodeText(record,'recip_address_1');
                    if(FTD_XML.selectNodeText(record,'recip_address_2')!=null && FTD_XML.selectNodeText(record,'recip_address_2')!=''){
                    	$('recipientAddress').value = $('recipientAddress').value+' '+FTD_XML.selectNodeText(record,'recip_address_2');
                    }
                    
                    $('recipientOriginalAddress1').value = FTD_XML.selectNodeText(record,'recip_address_1');
                    $('recipientAddress2').value = FTD_XML.selectNodeText(record,'recip_address_2');
                    orderObj.recipientAddress = $('recipientAddress').value;
                    $('recipientCity').value = FTD_XML.selectNodeText(record,'recip_city');
                    orderObj.recipientCity = $('recipientCity').value;
                    $('recipientState').value = UTILS.trim(FTD_XML.selectNodeText(record,'recip_state'));
                    orderObj.recipientState = $('recipientState').value;
                    $('recipientType').value = FTD_XML.selectNodeText(record,'recip_address_type');
                    orderObj.recipientType = $('recipientType').value;
                    $('recipientBusiness').value = UTILS.trim(FTD_XML.selectNodeText(record,'recip_business_name'));
                    orderObj.recipientBusiness = $('recipientBusiness').value;

                    prodObj = orderObj.productObject;
                    if (prodObj == null) {
                        alert('prodObject is null');
                    }
                    var sizeIndicator = FTD_XML.selectNodeText(record,'size_indicator');
                    prodObj.standardPrice = FTD_XML.selectNodeText(record,'product_amount');

                    $('customerBillingPhone').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_primary_phone'));
                    $('customerBillingPhoneExt').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_primary_phone_ext'));
                    $('customerEveningPhone').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_secondary_phone'));
                    $('customerEveningPhoneExt').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_secondary_phone_ext'));
                    $('customerFirstName').value = FTD_XML.selectNodeText(record,'buyer_first_name');
                    $('customerLastName').value = FTD_XML.selectNodeText(record,'buyer_last_name');
                    $('customerAddress').value = FTD_XML.selectNodeText(record,'buyer_address_1');
                    $('customerCity').value = FTD_XML.selectNodeText(record,'buyer_city');
                    $('customerStateCombo').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_state'));
                    $('customerCountryCombo').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_country'));
                    uo_customerZip = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_zip_code'));
                    if (uo_customerZip != null && uo_customerZip.length > 5) {
                        if ($('customerCountryCombo').value == 'US') {
                            uo_customerZip = uo_customerZip.substring(0,5);
                        } else if ($('customerCountryCombo').value == 'CA') {
                            uo_customerZip = uo_customerZip.substring(0,3);
                        }
                    }
                    $('customerZip').value = uo_customerZip;

                    var u_membershipId = UTILS.trim(FTD_XML.selectNodeText(record,'membership_number'));
                    $('membershipId').value =u_membershipId;
                    $('membershipFirstName').value = UTILS.trim(FTD_XML.selectNodeText(record,'membership_first_name'));
                    $('membershipLastName').value = UTILS.trim(FTD_XML.selectNodeText(record,'membership_last_name'));

                    $('customerEmailAddress').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_email_address'));

                    setCountryFields('productCountry', null, 'productZip', 'productZipSearch', null);
                    setCountryFields('recipientCountry','recipientState','recipientZip','recipientZipSearch',null);
                    $('recipientState').value = UTILS.trim(FTD_XML.selectNodeText(record,'recip_state'));
                    setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
                    $('customerStateCombo').value = UTILS.trim(FTD_XML.selectNodeText(record,'buyer_state'));
                    ORDER_EVENTS.onRecipLocationTypeChanged(null);

                    var uo_ccType = UTILS.trim(FTD_XML.selectNodeText(record,'cc_type'));
                    var uo_ccNumber = UTILS.trim(FTD_XML.selectNodeText(record,'cc_number'));

                    globalCCNumber = uo_ccNumber;
                    var uo_ccExpiration = UTILS.trim(FTD_XML.selectNodeText(record,'cc_expiration'));
                    var uo_paymentType = UTILS.trim(FTD_XML.selectNodeText(record,'payment_type'));
                    if (uo_paymentType == 'C') {
                    	$('paymentCCNumber').value = uo_ccNumber;
                        $('paymentCCTypeCombo').value = uo_ccType;
                        BILLING_EVENTS.ccTypeChanged();
                    	display_credit_card = "************" + uo_ccNumber.substr(uo_ccNumber.length - 4, 4);
                    	$('paymentCCNumber').value = display_credit_card;
                        if (uo_ccExpiration != null && uo_ccExpiration != '') {
                            var ccExpSplit = uo_ccExpiration.split('/');
                            if (ccExpSplit[1].length == 2) ccExpSplit[1] = '20' + ccExpSplit[1];
                            $('paymentCCExpMonthCombo').value = ccExpSplit[0];
                            $('paymentCCExpYear').value = ccExpSplit[1];
                        }
                    }
                    v_piModifyOrderFlag = UTILS.trim(FTD_XML.selectNodeText(record, 'pi_modify_order_flag'));
                    v_piPartnerId = UTILS.trim(FTD_XML.selectNodeText(record, 'pi_partner_id'));
                    v_piModifyOrderThreshold = UTILS.trim(FTD_XML.selectNodeText(record, 'pi_modify_order_threshold'));
                    v_originalMercTotal = UTILS.trim(FTD_XML.selectNodeText(record, 'pi_merc_total'));
                    v_originalPdbPrice = UTILS.trim(FTD_XML.selectNodeText(record, 'pi_pdb_price'));
                    v_originalFloristId = UTILS.trim(FTD_XML.selectNodeText(record, 'florist_id'));

                    makeUpdateOrderCustomerInfoReadOnly();

                    records = XPath.selectNodes('/result/rs[@name="order_addons"]/record',xmlDoc);
                    for(idx=0; idx<records.length; idx++ ) {
                        tempAddonId = FTD_XML.selectNodeText(records[idx],'add_on_code');
                        tempAddonQty = FTD_XML.selectNodeText(records[idx],'add_on_quantity');
                        tempAddonPrice = FTD_XML.selectNodeText(records[idx],'addon_price');
                        v_originalAddonName[idx] = FTD_XML.selectNodeText(records[idx],'add_on_description');
                        v_originalAddonQty[idx] = tempAddonQty;
                    }

                    v_originalFirstName = $('customerFirstName').value;
                    v_originalLastName = $('customerLastName').value;
                    v_originalDeliveryDate = FTD_XML.selectNodeText(record,'display_delivery_date');
                    v_originalDeliveryDateEnd = UTILS.trim(FTD_XML.selectNodeText(record,'delivery_date_range_end'));
                    v_originalProductId = FTD_XML.selectNodeText(record,'product_id');
                    v_originalProductName = FTD_XML.selectNodeText(record,'product_name');
                    v_originalProductAmount = FTD_XML.selectNodeText(record,'product_amount');
                    v_originalDiscountProductAmount = UTILS.trim(FTD_XML.selectNodeText(record,'discount_product_price'));
                    v_originalTaxAmount = FTD_XML.selectNodeText(record,'tax');
                    v_originalAddonTotal = FTD_XML.selectNodeText(record,'add_on_amount');
                    v_originalDiscountAddonAmount = UTILS.trim(FTD_XML.selectNodeText(record,'add_on_discount_amount'));
                    v_originalShipMethod = UTILS.trim(FTD_XML.selectNodeText(record,'ship_method'));
                    v_originalShipMethodDescription = UTILS.trim(FTD_XML.selectNodeText(record,'ship_method_description'));
                    v_originalColor1Description = UTILS.trim(FTD_XML.selectNodeText(record,'color1_description'));
                    v_originalColor2Description = UTILS.trim(FTD_XML.selectNodeText(record,'color2_description'));
                    v_originalMilesPoints = UTILS.trim(FTD_XML.selectNodeText(record,'miles_points'));
                    v_originalDiscountRewardType = UTILS.trim(FTD_XML.selectNodeText(record,'discount_reward_type'));
                    v_originalSpecialInstructions = UTILS.trim(FTD_XML.selectNodeText(record,'special_instructions'));
                    v_hasSpecialInstructions = false;
                    if (v_originalSpecialInstructions != null && v_originalSpecialInstructions != '') {
                        v_hasSpecialInstructions = true;
                        orderObj.floristComments = v_originalSpecialInstructions;
                        $('floristComments').value = v_originalSpecialInstructions;
                    }

                    v_originalOrderDetailId = FTD_XML.selectNodeText(record,'order_detail_id');
                    v_originalOrderGuid = FTD_XML.selectNodeText(record,'order_guid');
                    v_originalDeliveryDate = v_originalDeliveryDate;
                    v_originalRecipientId = FTD_XML.selectNodeText(record,'recipient_id');
                    v_originalShipMethod = v_originalShipMethod;
                    v_originalOrderHasSDU = UTILS.trim(FTD_XML.selectNodeText(record,'original_order_has_sdu'));
                    v_originalWalletIndicator = UTILS.trim(FTD_XML.selectNodeText(record,'wallet_indicator'));

                    var deliveryLocation = UTILS.trim(FTD_XML.selectNodeText(record,'recip_address_type'));
                    if(deliveryLocation.indexOf('FUNERAL')>-1){
                    		v_deliveryLocation = 'FUNERAL';
                    		$('timeOfService').innerHTML = "<b>Time of Service:</b>";
                    }
                    else{
                    	if(deliveryLocation.indexOf('HOME')==0){
                    		v_deliveryLocation = 'RESIDENTIAL';
                    	}
                    	else{
                    		v_deliveryLocation = deliveryLocation;
                    	}

                    	if(deliveryLocation.indexOf('CEMETERY')>-1){
                    		$('timeOfService').innerHTML = "<b>Time of Service:</b>";
                    	}
                    	else if(deliveryLocation.indexOf('BUSINESS')>-1){
                    		$('timeOfService').innerHTML = "<b>Work Hours:</b>";
                    	}
                    	else if(deliveryLocation.indexOf('HOSPITAL')>-1){
                    		$('timeOfService').innerHTML = "<b>Hours:</b>";
                    	}
                    }

                    v_timeOfService = getTimeOfService(UTILS.trim(FTD_XML.selectNodeText(record,'time_of_service')));
                    if(v_timeOfService==null){
                    	$('timeOfService').style.display='none';
                    }
                    else{
                    	$('timeOfService').value = UTILS.trim(FTD_XML.selectNodeText(record,'time_of_service'));
                    	$('timeOfServiceEnd').value = UTILS.trim(FTD_XML.selectNodeText(record,'time_of_service'));
                    	$('timeOfServiceFlag').value = "Y";

                    }

                    if(v_timeOfService!=null || v_hasSpecialInstructions==true){
                        	$('recipientHoursFromLabel').style.visibility = 'hidden';
                            $('recipientHoursToLabel').style.visibility = 'hidden';
                            $('recipientHoursId').style.visibility = 'hidden';
                        }




                    waitToSetFocus('productId');

                    //Explicitly setting the master source code
                    if(u_membershipId !=null && u_membershipId != '')
                    {
                    	$('membership').style.display='block';
                    	v_masterSourceCode.membershipDataRequired=true;
                    }

                } else {
                    alert('null order object ' + v_currentCartIdx);
                }
            }
        }
    }
}

function getTimeOfService(time){

	if(time==null || time==''){
		return null;
	}
	else if(time.toUpperCase() == 'UNKNOWN'){
		return time;
	}
	var hours = '';
    var minutes = '';
    var meridianValue = '';
    if(time.length<4){
      hours = time.substring(0,1);
      minutes = time.substring(1,time.length);
    }
    else{
      hours = time.substring(0,2);
      minutes = time.substring(2,time.length);
    }

    var intHours  = parseInt(hours);
    if(intHours>12){
      hours = intHours - 12;
      meridianValue = 'PM';
    }
    else if(intHours==12){
     meridianValue = 'PM';
    }
    else{
      meridianValue = 'AM';
    }
    return hours+':'+minutes+' '+meridianValue;
}


function makeUpdateOrderCustomerInfoReadOnly(){
	if (v_modifyOrderFromCom == true) {
		$('customerRecipSameCheckbox').disabled = true;
		$('customerBillingPhone').disabled = true;
		$('customerBillingPhoneExt').disabled = true;
		$('customerEveningPhone').disabled = true;
		$('customerEveningPhoneExt').disabled = true;
		$('customerFirstName').disabled = true;
		$('customerLastName').disabled = true;
		$('customerAddress').disabled = true;
		$('customerZip').disabled = true;
	    $('customerZipSearch').disabled = true;
		$('customerCountryCombo').disabled = true;
		$('customerCity').disabled = true;
		$('customerStateCombo').disabled = true;
		$('customerEmailAddress').disabled = true;
		$('customerSubscribeCheckbox').disabled = true;

		if (v_piModifyOrderFlag == 'Y') {
			$('paymentTypeCombo').disabled = true;
			$('paymentFraud').disabled = true;
			$('billingButtonCalculate').disabled = true;
			$('callDataSourceCode').disabled = true;
			$('callDataSourceCodeSearch').disabled = true;
		}
	}
}

function enableMembershipDetails()
{
	//Making the Membership Section available if the Orginal order has information.
    //alert($('membershipId').value);

}


// ]]>