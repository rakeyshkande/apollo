var v_logMessageMap = new Array();
var v_hashvalue = '_';
var v_mainAccordion;
var v_productDeliveryDateCal;
var v_productSearchDeliveryDateCal;
var v_cartList = new Array();
var v_currentCartIdx = -1;
var v_locationTypes;
var v_countries = new Array();
var v_languages = new Array();
var v_states = new Array();
var v_addons = new Array();
var v_paymentMethods = new Array();
var v_paymentTypes = new Array();
var v_deliveryDates = new Array();
var v_billingFields = new Array();
var v_orderFields = new Array();
var v_billingInfoFields = new Array();
var v_searchFields = new Array();
var v_availabilityFields = new Array();
var v_recalcFields = new Array();
var v_avsFields = new Array();
var v_scriptFields = new Array();
var v_viewedProducts = new Array();
var v_indexPrefix = '___';
var v_callTimestamp = null;
var v_callTimer;
var v_callTimerFirstWarnSeconds = 360;
var v_callTimerSecondWarnSeconds = 420;
var v_callTimerFirstMsg=null;
var v_callTimerSecondMsg=null;
var v_notifyTimer;
var v_notifySeconds = 10;
var v_recalcOrderTimer;
var v_recalcOrderSeconds = 5;
var v_checkAvailabilityTimer;
var v_checkAvailabilitySeconds = 5;
var v_checkAddressTimer;
var v_checkAddressSeconds = 5;
var v_phoneValidateRegex = null;
var v_displayDeliveryDaysCount=7;
var v_searchCancelDelaySeconds = 5;
var v_callbackTimerColorNormal;
var v_callbackTimerColorWarn = 'gold';
var v_callbackTimerColorError = '#ff6699';
var v_currentProductDetailObject = null;
var v_currentZip = null;
/*** DO NOT CHANGE THE FOLLOWING LINE.   IT ***/
/*** GETS REPLACED IN THE ANT BUILD SCRIPT. ***/
var v_testMode=true;
var v_masterSourceCode = new SOURCECODE.OBJECT();
var v_pendingSourceCode = null;
var v_customerPhoneChecked = false;
var v_validationTabIdx = 3;
var v_tabControl = null;
var v_hasFocus = null;

var v_currentRecalcCartValues = new Array();
var v_currentRecalcOrderValues = new Array();
var v_lastRecalcCartValues = new Array();
var v_lastRecalcOrderValues = new Array();

var v_currentAvailabilityOrderValues = new Array(1);
var v_lastAvailabilityOrderValues = new Array(1);

var v_standard_label = "Shown";
var v_deluxe_label = "Deluxe";
var v_premium_label = "Premium";
var v_size_indicator = "A";

//Begin element colors
var v_errorColor = "#cc0033";
var v_normalBgColor = "white";
var v_errorBgColor= v_errorColor;
var v_normalTextColor = "black";
var v_errorTextColor = "white";
var v_scPwdEntered = false;
var v_defaultSmallProductImage = 'images/npi_a.jpg';
var v_defaultLargeProductImage = 'images/npi_c.jpg';
var v_defaultAddonProductImage = 'images/npi_a.jpg';
var v_startupTimer = null;
var v_initDataFinished = false;
var v_elementConfigFinished = false;
var v_lastMasterOrderNumber = null;
var v_okToProcess = true;
var v_textSearchOpen = false;

//assign default delivery type to 'florist'
var v_previousDeliveryType = 'florist';

//assign maximum number of add-ons
var v_max_addons = 4;
var v_max_vases = 4;

var v_csc_failed_count = 0;

var v_lastCustomerEmailAddress = '';
var v_newCustomerEmailAddress = '';

var v_buyerHasFreeShipping = 'N';
var v_sourceCodeAllowsFreeShipping = 'N';
var v_displayed_buyerHasFreeShipping = 'N';
var v_displayed_sourceCode_FreeShippingNotAllowed = 'N';
var v_displayed_updatedSourceCode_FreeShippingNotAllowed = 'N';
var v_displayed_noFreeShippingProducts = 'N';
var v_freeShippingSavings = 0;
var submitFreeShippingAppliedMsg = '';
var addressVerified = 'Y';
var stateValidationError = '';
var v_displayDeliveryDate = false;
var previousDeliveryMethod = '';
var logTimePD = '';
var logTimePA = '';
var logTimeCOT = '';

// Update order original data variables
var v_originalFirstName = '';
var v_originalLastName = '';
var v_originalDeliveryDate = '';
var v_originalProductId = '';
var v_originalProductName = '';
var v_originalProductAmount = '';
var v_originalDiscountProductAmount = '';
var v_originalTaxAmount = '';
var v_originalShipMethod = '';
var v_originalShipMethodDescription = '';
var v_originalAddonTotal = '';
var v_originalDiscountAddonAmount = '';
var v_originalAddonName = new Array();
var v_originalAddonQty = new Array();
var v_originalDeliveryDateEnd = '';
var v_originalColor1Description = '';
var v_originalColor2Description = '';
var v_originalMilesPoints = '';
var v_originalDiscountRewardType = '';
var v_modifyOrderFromCom = false;
var v_originalOrderDetailId = '';
var v_originalOrderGuid = '';
var v_originalRecipientId = '';
var v_originalShipMethod = '';
var v_addOnMsgDisplayed = false;
var v_deliveryMethodMsgDisplayed = false;
var v_originalSpecialInstructions = '';
var v_hasSpecialInstructions = false;
var v_originalOrderHasSDU = false;
var v_originalWalletIndicator = '';
var v_deliveryLocation = null;
var v_timeOfService = null;
var v_piModifyOrderFlag = null;
var v_piPartnerId = null;
var v_piModifyOrderThreshold = null;
var v_originalMercTotal = null;
var v_originalPdbPrice = null;
var v_originalFloristId = null;
var v_deliveryMethodChanged=false;
//var onloads = new Array();
//onloads.push( accord ); function accord() {v_mainAccordion = new Rico.Accordion( 'accordionDiv', {panelHeight:500, onShowTab:NAV_EVENTS.accordionSelect} ); }

Rico.loadModule('Accordion');
Rico.onLoad( function() {
    v_mainAccordion = new Rico.Accordion( '.accordionTabTitleBar', '.accordionTabContentBox', {panelHeight:500} );

});

function init() {
    //Firefox/Mozilla browsers call onload at the beginning and end of the
    //page load.  So for those browsers, we use the custom event onpageshow
    //to be fired after the page displays completes.
    if( Prototype.Browser.Gecko ) {
        return;
    } else {
        setup();
    }
}

function setup() {
//    for ( var i = 0 ; i < onloads.length ; i++ ) {
//      onloads[i]();
//    }
    logMessage("preliminary setup");
    logMessage("misc");
    $('upperHeader').innerHTML = 'Order (Steps #1 - #3)'
    $('lowerHeader').innerHTML = 'Billing (Step #4)'
    tabberAutomatic(tabberOptions);
    v_tabControl.tabs[0].a.tabIndex = -1;
    v_tabControl.tabs[1].a.tabIndex = -1;
    v_tabControl.tabs[2].a.tabIndex = -1;
    v_tabControl.tabs[3].a.tabIndex = -1;

    if( v_testMode==true ) {
        $('testModeDiv').style.display='block';
        v_serverURLPrefix='';
    }
    //fixImages(document);
    $('cartItemId').style.display = 'none';
    $('productSearchStopButton').style.display = 'none';
    $('productSearchDisabledButton').style.display = 'none';

    $('copyDivId').style.display='none';
    $('notifyDivId').style.display='none';
    $('psViewedDivId').style.display='none';
    $('psDetailDivId').style.display='none';
    $('psTopSellersDivId').style.display='none';
    $('productSearchDivId').style.display='none';
    $('textSearchDivId').style.display='none';
    logMessage("misc");

    logMessage("calendar");
    v_productDeliveryDateCal = new YAHOO.widget.Calendar("productDeliveryDateCal","productDeliveryDateCalContainer");
    v_productDeliveryDateCal.selectEvent.subscribe(deliveryDateCalendarHandler, v_productDeliveryDateCal, true);
    setGenericCalendarProperties(v_productDeliveryDateCal);
    v_productDeliveryDateCal.render();
    v_productDeliveryDateCal.hide();

    v_productSearchDeliveryDateCal = new YAHOO.widget.Calendar("productSearchDeliveryDateCal","productSearchDeliveryDateCalContainer");
    v_productSearchDeliveryDateCal.selectEvent.subscribe(deliveryDateCalendarHandler, v_productSearchDeliveryDateCal, true);
    setGenericCalendarProperties(v_productSearchDeliveryDateCal);
    v_productSearchDeliveryDateCal.render();
    v_productSearchDeliveryDateCal.hide();

    $('productDeliveryDate').disabled=true;
    $('productCalendarButton').disabled=true;
    $('productDeliveryMethodFlorist').disabled=true;
    $('productDeliveryMethodDropship').disabled=true;
    $('productDeliveryMethodFlorist').checked=false;
    $('productDeliveryMethodDropship').checked=false;
    $('recipientType').disabled=true;
    v_displayDeliveryDate = false;

    logMessage("calendar");

    logMessage("events");
    var strVal = $('callDataDNIS').value;
    if( UTILS.isEmpty(strVal)==false ) $('callDataDNIS').disabled=true;

    //Add the event handling

    /** begin order object changing events **/
	Event.observe("languageId","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productId","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOccasion","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productCountry","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productDeliveryDate","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productZip","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("floristId","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientType","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientPhone","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientPhoneExt","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientBusiness","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientRoom","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientFirstName","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientLastName","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientAddress","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientZip","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientCountry","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientCity","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientState","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientHoursFrom","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("recipientHoursTo","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("orderComments","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("floristComments","change",ORDER_EVENTS.onOrderObjectChange);


    Event.observe("cardMessage","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnCardCheckbox","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnCardSelect","change",ORDER_EVENTS.onOrderObjectChange);

    Event.observe("productAddonVase","change",ORDER_EVENTS.onAddOnVaseChanged);
    Event.observe("addOnCheckbox1","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnCheckbox2","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnCheckbox3","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnCheckbox4","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnFCheckbox","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("addOnQty1","change",ORDER_EVENTS.onAddOnQty1Change);
    Event.observe("addOnQty2","change",ORDER_EVENTS.onAddOnQty2Change);
    Event.observe("addOnQty3","change",ORDER_EVENTS.onAddOnQty3Change);
    Event.observe("addOnQty4","change",ORDER_EVENTS.onAddOnQty4Change);
    Event.observe("addOnFBannerText","change",ORDER_EVENTS.onaddOnFBannerTextChange);

    Event.observe("productOptionStandard","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionPremium","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionDeluxe","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionVariable","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionVariablePrice","change",ORDER_EVENTS.onOrderObjectChange);

    Event.observe("carrierDeliveryOptGR","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("carrierDeliveryOpt2F","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("carrierDeliveryOptND","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("carrierDeliveryOptSA","click",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("carrierDeliveryOptSU","click",ORDER_EVENTS.onOrderObjectChange);

    Event.observe("productOptionSubCode","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionUpsell","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionColor","change",ORDER_EVENTS.onOrderObjectChange);
    Event.observe("productOptionColor2","change",ORDER_EVENTS.onOrderObjectChange);
    /** end order object changing events **/

    //Sympathy Controls
    Event.observe("recipientType","change",ORDER_EVENTS.onDeliveryLocationChange);
    Event.observe("recipientHoursFrom","change",ORDER_EVENTS.onServiceTimeChange);
    Event.observe("recipientHoursTo","change",ORDER_EVENTS.onServiceTimeChange);


    //Framework
    Event.observe("oeframeBodyId","keydown",NAV_EVENTS.onBodyKeyDown);

    //DNIS panel
    Event.observe("callDataDNIS","change",DNIS_EVENTS.onDnisChange);
    Event.observe("callDataDNIS","blur",DNIS_EVENTS.onDnisBlur);
    Event.observe("callDataSourceCode","change",DNIS_EVENTS.onSourceCodeChange);
    Event.observe("callDataSourceCodeSearch","click",DNIS_EVENTS.onSourceCodeLookup);
    Event.observe("callDataSourceCodePwdButton","click",DNIS_EVENTS.onValidateSourceCodePassword);

    //Product
    Event.observe("productId","change",ORDER_EVENTS.onProductIdChanged);
    Event.observe("productId","change",ORDER_EVENTS.toUpperCase);
    Event.observe("productZipSearch","click",ORDER_EVENTS.onZipSearch);
    Event.observe("productCountry","change",ORDER_EVENTS.onProductCountryChange);
    Event.observe("productCountry","focus",onElementFocus);
    Event.observe("productCountry","blur",onElementBlur);
    Event.observe("floristSearch","click",ORDER_EVENTS.onFloristSearchButton);
    Event.observe("productZip","change",ORDER_EVENTS.onProductZipChange);
    Event.observe("productOccasion","change",ORDER_EVENTS.onProductOccasionChanged);
    Event.observe("productOptionUpsell","change",ORDER_EVENTS.onProductUpsellChange);
    Event.observe("productOptionSubCode","change",ORDER_EVENTS.onProductSubcodeChange);
    Event.observe("productOptionStandard","click",ORDER_EVENTS.onProductOptionChanged);
    Event.observe("productOptionPremium","click",ORDER_EVENTS.onProductOptionChanged);
    Event.observe("productOptionDeluxe","click",ORDER_EVENTS.onProductOptionChanged);
    Event.observe("productOptionVariable","click",ORDER_EVENTS.onProductOptionChanged);
    Event.observe("productOptionVariablePrice","change",ORDER_EVENTS.onProductVariablePriceChanged);
    Event.observe("orderComments","focus",onElementFocus);
    Event.observe("orderComments","blur",onElementBlur);
    Event.observe("forceAvailabilityButton","click",ORDER_EVENTS.onForceAvailabilityCheck);
    Event.observe("forceAvailabilityButton","focus",onElementFocus);
    Event.observe("forceAvailabilityButton","blur",onElementBlur);
    Event.observe("productDeliveryMethodFlorist","click",ORDER_EVENTS.onProductDeliveryMethodChanged);
    Event.observe("productDeliveryMethodDropship","click",ORDER_EVENTS.onProductDeliveryMethodChanged);

    Event.observe("floristId","focus",ORDER_EVENTS.onFloristIdFocus);
    Event.observe("floristSearch","focus",ORDER_EVENTS.onFloristSearchFocus);
    Event.observe("floristSearch","blur",ORDER_EVENTS.onFloristSearchBlur);
    Event.observe("floristComments","focus",ORDER_EVENTS.onFloristCommentsFocus);
    Event.observe("floristComments","blur",ORDER_EVENTS.onFloristCommentsBlur);
    Event.observe("productCalendarButton","click",ORDER_EVENTS.onProductDeliveryDateClick);
    Event.observe("productSearchCalendarButton","click",PRODUCT_SEARCH_EVENTS.onSearchDeliveryDateClick);
    Event.observe("carrierDeliveryDateGR","change",ORDER_EVENTS.onCarrierDeliveryDateChange);
    Event.observe("carrierDeliveryDate2F","change",ORDER_EVENTS.onCarrierDeliveryDateChange);
    Event.observe("carrierDeliveryDateND","change",ORDER_EVENTS.onCarrierDeliveryDateChange);
    Event.observe("carrierDeliveryDateSA","change",ORDER_EVENTS.onCarrierDeliveryDateChange);
    Event.observe("carrierDeliveryDateSU","change",ORDER_EVENTS.onCarrierDeliveryDateChange);
    Event.observe("carrierDeliveryOptGR","click",ORDER_EVENTS.onCarrierDeliveryOptChange);
    Event.observe("carrierDeliveryOpt2F","click",ORDER_EVENTS.onCarrierDeliveryOptChange);
    Event.observe("carrierDeliveryOptND","click",ORDER_EVENTS.onCarrierDeliveryOptChange);
    Event.observe("carrierDeliveryOptSA","click",ORDER_EVENTS.onCarrierDeliveryOptChange);
    Event.observe("carrierDeliveryOptSU","click",ORDER_EVENTS.onCarrierDeliveryOptChange);
    Event.observe("productDeliveryDate","change",ORDER_EVENTS.onProductDeliveryDateChange);

    Event.observe("addOnCheckbox1","click",ORDER_EVENTS.onAddonCheckbox1);
    Event.observe("addOnCheckbox2","click",ORDER_EVENTS.onAddonCheckbox2);
    Event.observe("addOnCheckbox3","click",ORDER_EVENTS.onAddonCheckbox3);
    Event.observe("addOnCheckbox4","click",ORDER_EVENTS.onAddonCheckbox4);
    Event.observe("addOnFCheckbox","click",ORDER_EVENTS.onAddonFCheckbox);

    Event.observe("addOnCheckbox1","blur",ORDER_EVENTS.onAddonCheckboxBlur1);
    Event.observe("addOnCheckbox2","blur",ORDER_EVENTS.onAddonCheckboxBlur2);
    Event.observe("addOnCheckbox3","blur",ORDER_EVENTS.onAddonCheckboxBlur3);
    Event.observe("addOnCheckbox4","blur",ORDER_EVENTS.onAddonCheckboxBlur4);
    Event.observe("addOnFCheckbox","blur",ORDER_EVENTS.onAddonFCheckboxBlur);

    //Product Search
    Event.observe("productSearch","click",ORDER_EVENTS.onProductSearch);
    Event.observe("productSearchCloseButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchClose);
    Event.observe("productSearchBottomCloseButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchClose);
    Event.observe("productSearchGoButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchGoButton);
    Event.observe("productSearchStopButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchStopButton);
    Event.observe("productSearchZip","change",PRODUCT_SEARCH_EVENTS.onProductSearchZipChange);
    Event.observe("productSearchDeliveryDate","change",PRODUCT_SEARCH_EVENTS.onProductSearchDeliveryDateChange);
    Event.observe("psNavBeginning","click",PRODUCT_SEARCH_EVENTS.onProductSearchNavButton);
    Event.observe("psNavPrevious","click",PRODUCT_SEARCH_EVENTS.onProductSearchNavButton);
    Event.observe("psNavNext","click",PRODUCT_SEARCH_EVENTS.onProductSearchNavButton);
    Event.observe("psNavEnd","click",PRODUCT_SEARCH_EVENTS.onProductSearchNavButton);
    Event.observe("psTopNavBeginning","click",PRODUCT_SEARCH_EVENTS.onProductSearchTopNavButton);
    Event.observe("psTopNavPrevious","click",PRODUCT_SEARCH_EVENTS.onProductSearchTopNavButton);
    Event.observe("psTopNavNext","click",PRODUCT_SEARCH_EVENTS.onProductSearchTopNavButton);
    Event.observe("psTopNavEnd","click",PRODUCT_SEARCH_EVENTS.onProductSearchTopNavButton);
    Event.observe("productSearchZipSearch","click",PRODUCT_SEARCH_EVENTS.onProductSearchZipSearch);
    Event.observe("psViewedDivId","click",PRODUCT_SEARCH_EVENTS.onCloseRecentlyViewed);
    Event.observe("psTopCloseButton","click",PRODUCT_SEARCH_EVENTS.onCloseTopSellers);
    Event.observe("psBottomCloseButton","click",PRODUCT_SEARCH_EVENTS.onCloseTopSellers);
    Event.observe("productSearchSortCombo","change",PRODUCT_SEARCH_EVENTS.onChangeSortOrder);
    Event.observe("psSuggestCloseButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchCloseSuggest);
    Event.observe("productSearchCountryCombo","change",PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange);
    Event.observe("productSearchCountryCombo","focus",onElementFocus);
    Event.observe("productSearchCountryCombo","blur",onElementBlur);
    Event.observe("psRecentlyViewedLink","focus",onElementFocus);
    Event.observe("psRecentlyViewedLink","blur",onElementBlur);
    Event.observe("psTopSellersLink","focus",onElementFocus);
    Event.observe("psTopSellersLink","blur",onElementBlur);

    //Product Detail
    Event.observe("psDetailCloseButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchDetailClose);
    Event.observe("psDetailBottomCloseButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchDetailClose);
    Event.observe("productDetailSelectButton","click",PRODUCT_SEARCH_EVENTS.onProductSearchDetailSelect);

    //Card panel
    Event.observe("cardTypeButton","click",ORDER_EVENTS.onCardTypeButton);
    Event.observe("cardTypeList","dblclick",ORDER_EVENTS.onCardTypeButton);
    Event.observe("cardMessage","change",ORDER_EVENTS.onCardMessageChange);
    Event.observe("cardSpellCheckButton","click",ORDER_EVENTS.onCardSpellCheckButton);
    Event.observe("addOnCardCheckbox","click",ORDER_EVENTS.onCardAddonCheckbox);
    Event.observe("addOnCardCheckbox","blur",ORDER_EVENTS.onCardAddonCheckboxBlur);
    Event.observe("addOnCardImage","error",ORDER_EVENTS.onImageLoadError);
    Event.observe("addOnCardSelect","change",ORDER_EVENTS.onAddOnCardChanged);

    //Recipient Panel
	Event.observe("languageId","change",ORDER_EVENTS.onLanguageIdChange);
	Event.observe("languageId","focus",onElementFocus);
    Event.observe("languageId","blur",onElementBlur);
    Event.observe("recipientType","change",ORDER_EVENTS.onRecipLocationTypeChanged);
    Event.observe("recipientType","focus",ORDER_EVENTS.onRecipLocationTypeFocus);
    Event.observe("recipientCountry","change",ORDER_EVENTS.onRecipientCountryChange);
    Event.observe("recipientCountry","focus",onElementFocus);
    Event.observe("recipientCountry","blur",onElementBlur);
    Event.observe("recipientPhoneSearch","click",ORDER_EVENTS.onRecipPhoneSearchButton);
    Event.observe("recipientZipSearch","click",ORDER_EVENTS.onRecipZipSearchButton);
    Event.observe("membershipIdSearch","click",ORDER_EVENTS.onMembershipIdSearchButton);
    Event.observe("recipientZip","change",ORDER_EVENTS.onRecipientZipChange);
    Event.observe("recipientFacilitySearch","click",ORDER_EVENTS.onRecipientBusinessSearch);
    Event.observe("recipientPhone","change",ORDER_EVENTS.onRecipientPhoneChange);
    Event.observe("recipientPhoneExt","change",ORDER_EVENTS.onRecipientPhoneExtChange);
    Event.observe("recipientPhoneExt","focus",onElementFocus);
    Event.observe("recipientPhoneExt","blur",onElementBlur);
    Event.observe("floristId","change",ORDER_EVENTS.onFloristIdChange);

    //Customer Panel
    Event.observe("customerPhoneSearch","click",CUSTOMER_EVENTS.onCustomerPhoneSearchButton);
    Event.observe("customerZipSearch","click",CUSTOMER_EVENTS.onCustomerZipSearchButton);
    Event.observe("customerCountryCombo","change",CUSTOMER_EVENTS.onCustomerCountryChange);
    Event.observe("customerZip","change",CUSTOMER_EVENTS.onCustomerZipChange);
    Event.observe("customerBillingPhone","change",CUSTOMER_EVENTS.onCustomerPhoneChange);
    Event.observe("customerBillingPhoneExt","change",CUSTOMER_EVENTS.onCustomerPhoneExtChange);
    Event.observe("customerCountryCombo","focus",onElementFocus);
    Event.observe("customerCountryCombo","blur",onElementBlur);
    Event.observe("customerBillingPhoneExt","focus",onElementFocus);
    Event.observe("customerBillingPhoneExt","blur",onElementBlur);
    Event.observe("customerPhoneSearch","focus",onElementFocus);
    Event.observe("customerPhoneSearch","blur",onElementBlur);
    Event.observe("customerEveningPhone","focus",onElementFocus);
    Event.observe("customerEveningPhone","blur",onElementBlur);
    Event.observe("customerEveningPhoneExt","change",CUSTOMER_EVENTS.onCustomerPhoneExtChange);
    Event.observe("customerEveningPhoneExt","focus",onElementFocus);
    Event.observe("customerEveningPhoneExt","blur",onElementBlur);
    Event.observe("customerFirstName","change",CUSTOMER_EVENTS.onCustomerFirstNameChange);
    Event.observe("customerLastName","change",CUSTOMER_EVENTS.onCustomerLastNameChange);
    Event.observe("customerSubscribeCheckbox","click",CUSTOMER_EVENTS.onCustomerOptInChanged);
    Event.observe("customerEmailAddress","change",CUSTOMER_EVENTS.onCustomerEmailAddressChanged);

    //Billing Panel
    Event.observe("paymentTypeCombo","change",BILLING_EVENTS.onPayTypeChange);
    Event.observe("paymentGCBalanceCombo","change",BILLING_EVENTS.onGCBalanceChange);
    Event.observe("paymentGCNumber","change",BILLING_EVENTS.onGCNumberChange);
    Event.observe("customerRecipSameCheckbox","click",BILLING_EVENTS.onCustomerRecipCheckboxChange);
    Event.observe("customerRecipCombo","change",BILLING_EVENTS.onCustomerRecipComboChange);
    Event.observe("billingButtonSubmit","click",BILLING_EVENTS.onSubmitButton);
    Event.observe("billingButtonCalculate","click",BILLING_EVENTS.onCalculateButton);
    Event.observe("paymentCCNumber","change",BILLING_EVENTS.onCCNumberChange);
    Event.observe("paymentCCTypeCombo","change",BILLING_EVENTS.onPaymentCCTypeChange);
    Event.observe("paymentNCType","change",BILLING_EVENTS.onPaymentNCTypeChange);
    Event.observe("paymentNCRefOrder","blur",BILLING_EVENTS.onpaymentNCRefOrderChange);
    Event.observe("paymentNCRefOrder","change",BILLING_EVENTS.onpaymentNCRefOrderChange);
    Event.observe("confNewOrderSameCustButton","click",ORDER_EVENTS.onConfirmationNewOrderSameCustButton);
    Event.observe("confNewOrderButton","click",ORDER_EVENTS.onConfirmationNewOrderButton);
    Event.observe("confExitButton","click",ORDER_EVENTS.onConfirmationExitButton);
    Event.observe("paymentFraud","focus",onElementFocus);
    Event.observe("paymentFraud","blur",onElementBlur);
    Event.observe("membershipSkip","click",BILLING_EVENTS.onMembershipSkipClick);
    Event.observe("paymentCSCOverride","click",BILLING_EVENTS.onCSCOverrideChange);
    Event.observe("paymentCSC","change",BILLING_EVENTS.onCSCValueChange);

    //Address Verification Panel
    Event.observe("avAcceptButton","click",ORDER_EVENTS.onAcceptAVChanges);
    Event.observe("avIgnoreButton","click",ORDER_EVENTS.onRejectAVChanges);

    //Copy Type Panel
    Event.observe("copyOrderButton","click",CART_EVENTS.onCreateNewOrder);
    Event.observe("copyCancelButton","click",CART_EVENTS.onCancelNewOrder);

    //Button Panel
    Event.observe("actionButtonExit","click",ACTION_EVENTS.onActionButtonExit);
    Event.observe("actionButtonAbandon","click",ACTION_EVENTS.onActionButtonAbandon);

    //Text Search
    Event.observe("tsCloseButton","click",TEXT_SEARCH_EVENTS.onCloseButton);
    Event.observe("tsBottomCloseButton","click",TEXT_SEARCH_EVENTS.onCloseButton);
    Event.observe("textSearchGoButton","click",TEXT_SEARCH_EVENTS.onTextSearchGoButton);
    Event.observe("textSearchStopButton","click",TEXT_SEARCH_EVENTS.onTextSearchStopButton);
    Event.observe("tssSearchType","change",TEXT_SEARCH_EVENTS.onTextSearchSourceSearchChange);
    Event.observe("tsflZipSearch","click",TEXT_SEARCH_EVENTS.onTextSearchFloristZipButton);

    //Navigation
    Event.observe("pagetop","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("tab1bottom","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("tab2top","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("tab2bottom","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("tab3top","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("tab3bottom","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("billingtop","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("billingbottom","focus",NAV_EVENTS.onNavigationFocus);
    Event.observe("upperHeader","click",NAV_EVENTS.onOrderBarClicked);
    Event.observe("lowerHeader","click",NAV_EVENTS.onBillingBarClicked);

    BILLING_EVENTS.paymentTypeChanged();
    logMessage("events");

    logMessage("misc 2");
    var el = document.forms[0].elements;
    for( i=0; i<el.length; i++ ){
        var elId = el[i].id;
        if( UTILS.isEmpty(elId)==false ) {
            Event.observe(elId,"focus",setHasFocus);
        }
    }

    //Set the cc expiration date year
    FTD_DOM.clearSelect('paymentCCExpYear');
    FTD_DOM.insertOption('paymentCCExpYear','year','',true);
    var thisYear = new Date().getFullYear();
    for( idx=0; idx<15; idx++ ) {
        FTD_DOM.insertOption('paymentCCExpYear',thisYear+idx,thisYear+idx,false);
    }

    v_callTimer = new TIMER.Timer(1000,'callTimerCallback()',null);
    v_callbackTimerColorNormal = $('callDataTimer').style.color;

    //Need to determine what the innerHTML function will return
    //for the v_fieldRequiredHtml so we can query on the results
    var oldLabel = $('productIdLabel').innerHTML;
    $('productIdLabel').innerHTML = v_fieldRequiredHtml;
    v_fieldRequiredHtml = $('productIdLabel').innerHTML;
    $('productIdLabel').innerHTML = oldLabel;


    v_startupTimer = new TIMER.Timer(100,'startupTimerCallback()',null);
    v_startupTimer.startTimer();

    logMessage("misc 2");

    logMessage("preliminary setup");
    ORDER_AJAX.getElementConfig();
    ORDER_AJAX.getIntroData();
}

function tabberLoad(argsObj) {
    v_tabControl = argsObj.tabber;
    hideValidationTab();
}

function unload() {
}

function setProcessing(switchFlag) {
    $('actionButtonProcessing').style.visibility = (switchFlag?'visible':'hidden');
}

/* replace by JSON
function setOccasions(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="occasions"]/record',xmlDoc);
        if( records.length>0 ) {
            FTD_DOM.clearSelect('productOccasion');
            FTD_DOM.insertOption('productOccasion','','',true);
            var el = $('productOccasion');
            for( idx=0; idx<records.length; idx++ ) {
                var occasionId = FTD_XML.selectNodeText(records[idx],'occasion_id');
                var occasionName = FTD_XML.selectNodeText(records[idx],'description');
                FTD_DOM.insertOption(el,occasionName,occasionId);
            }
        }
    }
}
*/

function setOccasionsJSON(occasions) {
    if( occasions!=null ) {
        var results = makeJSONArray(occasions.record);
        var records = results.length;
        if( records>0 ) {
            FTD_DOM.clearSelect('productOccasion');
            FTD_DOM.insertOption('productOccasion','','',true);
            var el = $('productOccasion');
            for( idx=0; idx<records; idx++ ) {
                var occasionId = results[idx].occasion_id;
                var occasionName = results[idx].description;
                FTD_DOM.insertOption(el,occasionName,occasionId);
            }
        }
    }
}

function setSurchargeExplanationJSON(surcharge) {
	var sur = surcharge["surcharge-explanation"];
	$('sur_exp').innerHTML = sur;
}
/* replace by JSON
function setCountries(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="country_master"]/record',xmlDoc);
        v_countries = new Array();
        var idx;

        for(idx=0; idx<records.length; idx++ ) {
            var countryCode = FTD_XML.selectNodeText(records[idx],'country_id');
            var countryName = FTD_XML.selectNodeText(records[idx],'name');
            var domesticFlag = FTD_XML.selectNodeText(records[idx],'oe_country_type');

            country = new GEO.COUNTRY(countryName,countryCode);
            country.domestic = (domesticFlag=='D'?true:false);
            country.defaultValue = (idx==0?true:false);
            v_countries.push(country);
        }

        FTD_DOM.clearSelect('productCountry');

        for( idx=0; idx<v_countries.length; idx++) {
            FTD_DOM.insertOption('productCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
        }

        FTD_DOM.clearSelect('recipientCountry');

        for( idx=0; idx<v_countries.length; idx++) {
            FTD_DOM.insertOption('recipientCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
        }

        FTD_DOM.clearSelect('customerCountryCombo');

        for( idx=0; idx<v_countries.length; idx++) {
            FTD_DOM.insertOption('customerCountryCombo',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
        }

        FTD_DOM.clearSelect('productSearchCountryCombo');

        for( idx=0; idx<v_countries.length; idx++) {
            FTD_DOM.insertOption('productSearchCountryCombo',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
        }

        FTD_DOM.clearSelect('tszCountry');

        for( idx=0; idx<v_countries.length; idx++) {
            if( v_countries[idx].domestic ) {
                FTD_DOM.insertOption('tszCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            }
        }

        FTD_DOM.clearSelect('tsfCountry');

        for( idx=0; idx<v_countries.length; idx++) {
            if( v_countries[idx].domestic ) {
                FTD_DOM.insertOption('tsfCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            }
        }

        ORDER_EVENTS.onProductCountryChange(null);
        setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
        setCountryFields('tszCountry','tszState','tszStateLabel',null,null);
        setCountryFields('tsfCountry','tsfState','tsfStateLabel',null,null);
    }
}
*/

function setCountriesJSON(countries) {
    if( countries!=null ) {
        var results = makeJSONArray(countries.record);
        var records = results.length;
        v_countries = new Array();
        var idx;
        FTD_DOM.clearSelect('productCountry');
        FTD_DOM.clearSelect('recipientCountry');
        FTD_DOM.clearSelect('customerCountryCombo');
        FTD_DOM.clearSelect('productSearchCountryCombo');
        FTD_DOM.clearSelect('tszCountry');
        FTD_DOM.clearSelect('tsfCountry');

        for(idx=0; idx<records; idx++ ) {
            var countryCode = results[idx].country_id;
            var countryName = results[idx].name;
            var domesticFlag = results[idx].oe_country_type;

            country = new GEO.COUNTRY(countryName,countryCode);
            country.domestic = (domesticFlag=='D'?true:false);
            country.defaultValue = (idx==0?true:false);
            v_countries.push(country);

            FTD_DOM.insertOption('productCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            FTD_DOM.insertOption('recipientCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            FTD_DOM.insertOption('customerCountryCombo',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            FTD_DOM.insertOption('productSearchCountryCombo',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            if( country.domestic ) {
                FTD_DOM.insertOption('tszCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
                FTD_DOM.insertOption('tsfCountry',v_countries[idx].comboPrompt,v_countries[idx].comboValue,v_countries[idx].defaultValue);
            }

        }

        ORDER_EVENTS.onProductCountryChange(null);
        //setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
        //setCountryFields('tszCountry','tszState','tszStateLabel',null,null);
        //setCountryFields('tsfCountry','tsfState','tsfStateLabel',null,null);
    }
}

function setLanguagesJSON(languages) {
    if( languages!=null ) {
        var results = makeJSONArray(languages.record);
        var records = results.length;
        v_languages = new Array();
        var idx;
        for(idx=0; idx<records; idx++ ) {
            var languageId = results[idx].language_id;
            var description = results[idx].description;
            FTD_DOM.insertOption('languageId',description,languageId);
        }
    }
}

/* replace by JSON
function setStates(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="state_master"]/record',xmlDoc);
        v_states = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            stateId = FTD_XML.selectNodeText(records[idx],'state_master_id');
            stateName = FTD_XML.selectNodeText(records[idx],'state_name');
            countryId = FTD_XML.selectNodeText(records[idx],'country_id');

            state = new GEO.STATE(stateName,stateId);
            state.countryId = countryId;
            v_states.push(state);
        }
    }
}
*/

function setStatesJSON(states) {
    if( states!=null ) {
        var results = makeJSONArray(states.record);
        var records = results.length;
        v_states = new Array();
        for( idx=0; idx<records; idx++ ) {
            stateId = results[idx].state_master_id;
            stateName = results[idx].state_name;
            countryId = results[idx].country_id;

            state = new GEO.STATE(stateName,stateId);
            state.countryId = countryId;
            v_states.push(state);
        }
    }
}

/* replace by JSON
function setAddons(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="add-ons"]/record',xmlDoc);
        v_addons = new Array();
        v_possibleAddons = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            addonId = FTD_XML.selectNodeText(records[idx],'addon_id');
            addonType = FTD_XML.selectNodeText(records[idx],'addon_type');
            addonDescription = FTD_XML.selectNodeText(records[idx],'description');
            addonPrice = FTD_XML.selectNodeText(records[idx],'price');
            addonText = FTD_XML.selectNodeText(records[idx],'addon_text');
            addonImage = FTD_XML.selectNodeText(records[idx],'small_image_url');

            addon = new ADDON.OBJECT();
            addon.id=addonId;
            addon.type=addonType;
            addon.description=addonDescription;
            addon.price=UTILS.formatStringToPrice(addonPrice);
            addon.insideText=addonText;
            if( addonImage!=null )
                addon.image=addonImage;
            v_addons.push(addon);

            occasions = XPath.selectNodes('/result/rs[@name="occasion_addons"]/record[addon_id="'+addonId+'"]/occasion_id/text()',xmlDoc);
            for( oIdx=0; oIdx<occasions.length; oIdx++) {
                addon.occasions.push(occasions[oIdx].nodeValue);
            }

            if( addonType!='4' ) {
                v_possibleAddons.push(addonId);
                el = $("addOn"+addonId+"CheckboxLabel");
                if( el!=null ) {
                    el.innerHTML=addonDescription;
                    $("addOn"+addonId+"Price").innerHTML='$'+addon.price;
                    var fld = findFieldObject("addOn"+addonId+"Checkbox",v_currentCartIdx);
                    if( fld!=null && fld.tabIndex!=null ) {
                        fld.labelText=addonDescription;
                    }
                }
            }
        }
    }
}
*/

function setAddonsJSON(addOns, occasionAddonsMap) {
    if( addOns!=null ) {
        var results = makeJSONArray(addOns.record);
        var records = results.length;
        v_addons = new Array();
        for( idx=0; idx<records; idx++ ) {
            addonId = results[idx].addon_id;
            addonType = results[idx].addon_type;
            addonDescription = results[idx].description;
            addonPrice = results[idx].price;
            addonText = results[idx].addon_text;
            addonImage = results[idx].small_image_url;

            addon = new ADDON.OBJECT();
            addon.id=addonId;
            addon.type=addonType;
            addon.description=addonDescription;
            addon.price=UTILS.formatStringToPrice(addonPrice);
            addon.insideText=addonText;
            if( addonImage!=null )
                addon.image=addonImage;
            v_addons.push(addon);

            occasions = occasionAddonsMap[v_hashvalue + addonId]
            for( oIdx=0; occasions!=null && oIdx<occasions.length; oIdx++) {
                addon.occasions.push(occasions[oIdx]);
            }

            if( addonType!='4' && addonType!='6') {
                //v_possibleAddons.push(addonId);
                el = $("addOn"+addonId+"CheckboxLabel");
                if( el!=null ) {
                    el.innerHTML=addonDescription;
                    $("addOn"+addonId+"Price").innerHTML='$'+addon.price;
                    var fld = findFieldObject("addOn"+addonId+"Checkbox",v_currentCartIdx);
                    if( fld!=null && fld.tabIndex!=null ) {
                        fld.labelText=addonDescription;
                    }
                }
            }

        }
    }
}

function populatetOccasionAddonsMapJSON(occasionAddOns) {
    var v_occasionAddonsMap = new Array();
    if( occasionAddOns!=null ) {
        var addonId;
        var occasionId;
        var v_occasionAddons;

        var results = makeJSONArray(occasionAddOns.record);
        var records = results.length;
        for( idx=0; idx<records; idx++ ) {
            addonId = v_hashvalue + results[idx].addon_id;
            occasionId = results[idx].occasion_id;
            if(v_occasionAddonsMap[addonId]!=null){
                v_occasionAddonsMap[addonId].push(occasionId);
            } else {
                v_occasionAddons = new Array();
                v_occasionAddons.push(occasionId);
                v_occasionAddonsMap[addonId] = v_occasionAddons;
            }
        }
    }
    return v_occasionAddonsMap;
}

function setPaymentTypes() {
    v_paymentTypes = new Array();
    var pt = new PAYMENT_TYPE.OBJECT();
    pt.id = 'C';
    pt.display = 'Credit Card';
    v_paymentTypes['C']=pt;

    pt = new PAYMENT_TYPE.OBJECT();
    pt.id = 'N';
    pt.display = 'No Charge';
    v_paymentTypes['N']=pt;

    pt = new PAYMENT_TYPE.OBJECT();
    pt.id = 'I';
    pt.display = 'Invoice';
    v_paymentTypes['I']=pt;

    pt = new PAYMENT_TYPE.OBJECT();
    pt.id = 'G';
    pt.display = 'Gift Certificate';
    v_paymentTypes['G']=pt;
}

/* replace by JSON
function setPaymentMethods(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="payment_methods"]/record',xmlDoc);
        v_paymentMethods = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            var active = (FTD_XML.selectNodeText(records[idx],'active_flag')=='Y');
            var joe = (FTD_XML.selectNodeText(records[idx],'joe_flag')=='Y');

            if( active && joe ) {
                pm = new PAYMENT_METHOD.OBJECT();
                pm.id = FTD_XML.selectNodeText(records[idx],'payment_method_id');
                pm.description = FTD_XML.selectNodeText(records[idx],'description');
                if( pm.id=='NC' ) pm.type = 'N';
                else pm.type = FTD_XML.selectNodeText(records[idx],'payment_type');
                pm.cardId = FTD_XML.selectNodeText(records[idx],'card_id');
                pm.hasExpirationDate = (FTD_XML.selectNodeText(records[idx],'has_expiration_date')=='Y');
                pm.regexPattern = FTD_XML.selectNodeText(records[idx],'regex_pattern');

                v_paymentMethods[pm.id] = pm;
            }
        }
    }
}
*/

function setPaymentMethodsJSON(paymentMethods) {
    if( paymentMethods!=null ) {
        var results = makeJSONArray(paymentMethods.record);
        var records = results.length;
        v_paymentMethods = new Array();
        for( idx=0; idx<records; idx++ ) {
            var active = (results[idx].active_flag=='Y');
            var joe = (results[idx].joe_flag=='Y');

            if( active && joe ) {
                pm = new PAYMENT_METHOD.OBJECT();
                pm.id = results[idx].payment_method_id;
                pm.description = results[idx].description;
                if( pm.id=='NC' ) pm.type = 'N';
                else pm.type = results[idx].payment_type;
                pm.cardId = results[idx].card_id;
                pm.hasExpirationDate = (results[idx].has_expiration_date=='Y');
                pm.regexPattern = results[idx].regex_pattern;
                pm.cscRequired = (results[idx].csc_required_flag=='Y');

                v_paymentMethods[pm.id] = pm;
            }
        }
    }
}

/* replace by JSON
function setDeliveryDates(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="delivery-dates"]/record',xmlDoc);
        var idx;
        var ddObj;
        var ddNode;
        v_deliveryDates = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            ddObj = new DELIVERY_DATE.OBJECT();
            ddObj.startDate = UTILS.serverDateToDate(FTD_XML.selectNodeText(records[idx],'start-date'));
            ddObj.endDate = UTILS.serverDateToDate(FTD_XML.selectNodeText(records[idx],'end-date'));
            ddObj.isDateRange = (FTD_XML.selectNodeText(records[idx],'date-range')=='true'?true:false);
            ddObj.displayString = FTD_XML.selectNodeText(records[idx],'display');
            ddObj.valueString = FTD_XML.selectNodeText(records[idx],'value');
            v_deliveryDates[ddObj.valueString]=ddObj;
        }

        populateDeliveryDateCombos();
    }
}
*/

function setDeliveryDatesJSON(deliveryDates) {
    if( deliveryDates!=null ) {
        var results = makeJSONArray(deliveryDates.record);
        var records = results.length;
        var ddObj;
        var dispToday = '';
        var dateRange = '';
        var ddNode;
        v_deliveryDates = new Array();
        for( var idx=0; idx<records; idx++ ) {
            ddObj = new DELIVERY_DATE.OBJECT();

            ddObj.startDate = UTILS.serverDateToDate(results[idx].startDate);
            ddObj.endDate = UTILS.serverDateToDate(results[idx].endDate);
            ddObj.isDateRange = results[idx].dateRange=='true'?true:false;

            dateRange = results[idx].dateRange;
            dispToday = results[idx].display;

            if(dateRange=='false' && idx==0){
            ddObj.displayString = dispToday.replace(dispToday.substring(0,3),"Today");
        	}
            else{
            ddObj.displayString = results[idx].display;
            }

            ddObj.valueString = results[idx].value;
            v_deliveryDates[ddObj.valueString]=ddObj;
        }

        populateDeliveryDateCombos();
    }
}

function populateDeliveryDateCombos() {
    var productDelDateEl = $('productDeliveryDate');
    var searchDelDateEl = $('productSearchDeliveryDate');

    //Sort the keys
    var sortArray = Object.keys(v_deliveryDates);
    sortArray.sort();

    FTD_DOM.clearSelect('productDeliveryDate');
    FTD_DOM.clearSelect('productSearchDeliveryDate');
    FTD_DOM.insertOption('productDeliveryDate'," ","");
    FTD_DOM.insertOption('productSearchDeliveryDate'," ","");
    for (var i=0; i<sortArray.length; i++) {
        var obj = v_deliveryDates[sortArray[i]];
        if( Object.isFunction(obj) )
            continue;
        FTD_DOM.insertOption(productDelDateEl,obj.displayString,obj.valueString);
        FTD_DOM.insertOption(searchDelDateEl,obj.displayString,obj.valueString);
    }
}

/* replace by JSON
function setAddressTypes(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="address_types"]/record',xmlDoc);
        FTD_DOM.clearSelect('recipientType');
        v_locationTypes = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            var location = new ADDRESS_TYPE.OBJECT(FTD_XML.selectNodeText(records[idx],'description'),FTD_XML.selectNodeText(records[idx],'address_type_code'));
            location.business=(FTD_XML.selectNodeText(records[idx],'prompt_for_business_flag')=='Y');
            location.businessLabel=FTD_XML.selectNodeText(records[idx],'business_label_txt');
            location.lookup=(FTD_XML.selectNodeText(records[idx],'prompt_for_lookup_flag')=='Y');
            location.lookupLabel=FTD_XML.selectNodeText(records[idx],'lookup_label_txt');
            location.hours=(FTD_XML.selectNodeText(records[idx],'prompt_for_hours_flag')=='Y');
            location.hoursLabel=FTD_XML.selectNodeText(records[idx],'hours_label_txt');
            location.room=(FTD_XML.selectNodeText(records[idx],'prompt_for_room_flag')=='Y');
            location.roomLabel=FTD_XML.selectNodeText(records[idx],'room_label_txt');
            location.defaultValue=(FTD_XML.selectNodeText(records[idx],'default_flag')=='Y');
            v_locationTypes[location.comboValue]=location;
            FTD_DOM.insertOption('recipientType',location.comboPrompt,location.comboValue,location.defaultValue);
        }

        ORDER_EVENTS.onRecipLocationTypeChanged(null);
    }
}
*/

function setAddressTypesJSON(addressTypes) {
    if( addressTypes!=null ) {
        var results = makeJSONArray(addressTypes.record);
        var records = results.length;
        FTD_DOM.clearSelect('recipientType');
        v_locationTypes = new Array();
        for( idx=0; idx<records; idx++ ) {
            var location = new ADDRESS_TYPE.OBJECT(results[idx].description,results[idx].address_type_code);
            location.business=(results[idx].prompt_for_business_flag=='Y');
            location.businessLabel=results[idx].business_label_txt;
            location.lookup=(results[idx].prompt_for_lookup_flag=='Y');
            location.lookupLabel=results[idx].lookup_label_txt;
            location.hours=(results[idx].prompt_for_hours_flag=='Y');
            location.hoursLabel=results[idx].hours_label_txt;
            location.room=(results[idx].prompt_for_room_flag=='Y');
            location.roomLabel=results[idx].room_label_txt;
            location.defaultValue=(results[idx].default_flag=='Y');
            v_locationTypes[location.comboValue]=location;
            FTD_DOM.insertOption('recipientType',location.comboPrompt,location.comboValue,location.defaultValue);
        }

        ORDER_EVENTS.onRecipLocationTypeChanged(null);
    }
}

function setPriceHeaderJSON(priceTypes) {
    if( priceTypes!=null ) {
        var results = makeJSONArray(priceTypes.record);
        var records = results.length;
        FTD_DOM.clearSelect('tssDiscount');
        FTD_DOM.insertOption('tssDiscount', ' ', '');
        for( idx=0; idx<records; idx++ ) {
            var location = new PRICE_HEADER.OBJECT(results[idx].description,results[idx].price_header_id);
            FTD_DOM.insertOption('tssDiscount',location.comboPrompt,location.comboValue);
        }
    }
}

function populatetGlobalParmsMapJSON(globalParms) {
    var v_globalParmsMap = new Array();
    if( globalParms!=null ) {
        var key;
        var value;

        var results = makeJSONArray(globalParms.record);
        var records = results.length;
        for( idx=0; idx<records; idx++ ) {
            key = v_hashvalue + results[idx].name;
            value = results[idx].value;
            v_globalParmsMap[key] = value;
        }
    }
    return v_globalParmsMap;
}

/* replace by JSON
function setGlobalParms(xmlDoc) {
    if( xmlDoc!=null ) {
        var node;
        var strVal;

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CALL_CRITICAL_TIME"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_callTimerFirstWarnSeconds = parseInt(strVal);
            if( isNaN(v_callTimerFirstWarnSeconds)==true ) {
                throw("OE_CONFIG/CALL_CRITICAL_TIME is not a number.");
            }
        } catch (err) {
            alert('Global parameter OE_CONFIG/CALL_CRITICAL_TIME is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 360 seconds.');
            v_callTimerFirstWarnSeconds = 360;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CALL_CRITICAL_MESSAGE"]/value/text()',xmlDoc);
            v_callTimerFirstMsg = node[0].nodeValue;
            if( UTILS.isEmpty(v_callTimerFirstMsg)==true ) v_callTimerFirstMsg=null;
        } catch (err) {
            v_callTimerFirstMsg = null;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CALL_WARNING_TIME"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_callTimerSecondWarnSeconds = parseInt(strVal);
            if( isNaN(v_callTimerSecondWarnSeconds)==true ) {
                throw("OE_CONFIG/CALL_WARNING_TIME is not a number.");
            }
        } catch (err) {
            alert('Global parameter OE_CONFIG/CALL_WARNING_TIME is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 420 seconds.');
            v_callTimerSecondWarnSeconds = 420;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CALL_WARNING_MESSAGE"]/value/text()',xmlDoc);
            v_callTimerSecondMsg = node[0].nodeValue;
            if( UTILS.isEmpty(v_callTimerSecondMsg)==true ) v_callTimerSecondMsg=null;
        } catch (err) {
            v_callTimerSecondMsg = null;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="SKIP_PHONE_NUMBER_VAL_REGEX"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_phoneValidateRegex = new RegExp(strVal);
            "abcd".match(v_phoneValidateRegex);
        } catch (err) {
            alert('Global parameter OE_CONFIG/SKIP_PHONE_NUMBER_VAL_REGEX is not configured correctly.\r\n'+err.description+'\r\nRecipient and customer phone validation will be disabled.');
            v_phoneValidateRegex=null;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="DELIVERY_DAYS_OUT_MAX"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            var intVal = parseInt(strVal);
            if( isNaN(intVal) ) {
                throw("OE_CONFIG/DELIVERY_DAYS_OUT_MAX is not a number.");
            }
            v_productDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),intVal));
            v_productSearchDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),intVal));
        } catch(err) {
            alert('Global parameter OE_CONFIG/DELIVERY_DAYS_OUT_MAX is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 120 days.');
            v_productDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),120));
            v_productSearchDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),120));
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="DELIVERY_DATE_DROPDOWN_DAYS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_displayDeliveryDaysCount = parseInt(strVal);
            if( isNaN(v_displayDeliveryDaysCount) ) {
                throw("OE_CONFIG/DELIVERY_DATE_DROPDOWN_DAYS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/DELIVERY_DATE_DROPDOWN_DAYS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 7 days.');
            v_displayDeliveryDaysCount=7;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="PRODUCT_SEARCH_RESULT_ROWS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            PRODUCT_SEARCH.resultsRows = parseInt(strVal);
            if( isNaN(PRODUCT_SEARCH.resultsRows)) {
                throw("OE_CONFIG/PRODUCT_SEARCH_RESULT_ROWS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/PRODUCT_SEARCH_RESULT_ROWS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 2 rows.');
            PRODUCT_SEARCH.resultsRows = 2;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="PRODUCT_SEARCH_RESULT_COLUMNS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            PRODUCT_SEARCH.resultsColumns = parseInt(strVal);
            if( isNaN(PRODUCT_SEARCH.resultsColumns) ) {
                throw("OE_CONFIG/PRODUCT_SEARCH_RESULT_COLUMNS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/PRODUCT_SEARCH_RESULT_COLUMNS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 rows.');
            PRODUCT_SEARCH.resultsColumns = 5;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="SEARCH_CANCEL_DELAY_SECONDS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_searchCancelDelaySeconds = parseInt(strVal);
            if( isNaN(v_searchCancelDelaySeconds) ) {
                throw("OE_CONFIG/SEARCH_CANCEL_DELAY_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/SEARCH_CANCEL_DELAY_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_searchCancelDelaySeconds=5;
        }

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="NOTIFY_PANEL_HIDE_SECONDS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_notifySeconds = parseInt(strVal);
            if( isNaN(v_notifySeconds) ) {
                throw("OE_CONFIG/NOTIFY_PANEL_HIDE_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/NOTIFY_PANEL_HIDE_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 10 seconds.');
            v_notifySeconds=10;
        }
        v_notifyTimer = new TIMER.Timer(v_notifySeconds*1000,'notifyTimerCallback()',null);

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="RECALC_ORDER_SECONDS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_recalcOrderSeconds = parseInt(strVal);
            if( isNaN(v_recalcOrderSeconds) ) {
                throw("OE_CONFIG/RECALC_ORDER_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/RECALC_ORDER_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_recalcOrderSeconds=5;
        }
        v_recalcOrderTimer = new TIMER.Timer(v_recalcOrderSeconds*1000,'recalcOrderTimerCallback()',null);

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CHECK_AVAILABILITY_SECONDS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_checkAvailabilitySeconds = parseInt(strVal);
            if( isNaN(v_recalcOrderSeconds) ) {
                throw("OE_CONFIG/CHECK_AVAILABILITY_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/CHECK_AVAILABILITY_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_checkAvailabilitySeconds=5;
        }
        v_checkAvailabilityTimer = new TIMER.Timer(v_checkAvailabilitySeconds*1000,'checkAvailabilityTimerCallback()',null);

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="CHECK_ADDRESS_SECONDS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_checkAddressSeconds = parseInt(strVal);
            if( isNaN(v_checkAddressSeconds) ) {
                throw("OE_CONFIG/CHECK_ADDRESS_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/CHECK_ADDRESS_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 1 second.');
            v_checkAddressSeconds=1;
        }
        v_checkAddressTimer = new TIMER.Timer(v_checkAddressSeconds*1000,'checkAddressTimerCallback()',null);

        try {
            node = XPath.selectNodes('/result/rs[@name="global_parms"]/record[name="VALIDATE_ADDRESS"]/value/text()',xmlDoc);
            strVal = node[0].nodeValue;
            v_validateAddress = (strVal=='Y'?true:false);
        } catch(err) {
            alert('Global parameter OE_CONFIG/VALIDATE_ADDRESS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to Y.');
            v_validateAddress=true;
        }
    }
}
*/

function setGlobalParmsJSON(globalParmsMap) {
    if( globalParmsMap!=null ) {
        var node;
        var strVal;

        try {
            strVal = globalParmsMap[v_hashvalue + "CALL_CRITICAL_TIME"];
            if(strVal == null) throw "value is null";
            v_callTimerFirstWarnSeconds = parseInt(strVal);
            if( isNaN(v_callTimerFirstWarnSeconds)==true ) {
                throw("OE_CONFIG/CALL_CRITICAL_TIME is not a number.");
            }
        } catch (err) {
            alert('Global parameter OE_CONFIG/CALL_CRITICAL_TIME is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 360 seconds.');
            v_callTimerFirstWarnSeconds = 360;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "CALL_CRITICAL_MESSAGE"];
            if(strVal == null) throw "value is null";
            v_callTimerFirstMsg = strVal;
            if( UTILS.isEmpty(v_callTimerFirstMsg)==true ) v_callTimerFirstMsg=null;
        } catch (err) {
            v_callTimerFirstMsg = null;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "CALL_WARNING_TIME"];
            if(strVal == null) throw "value is null";
            v_callTimerSecondWarnSeconds = parseInt(strVal);
            if( isNaN(v_callTimerSecondWarnSeconds)==true ) {
                throw("OE_CONFIG/CALL_WARNING_TIME is not a number.");
            }
        } catch (err) {
            alert('Global parameter OE_CONFIG/CALL_WARNING_TIME is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 420 seconds.');
            v_callTimerSecondWarnSeconds = 420;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "CALL_WARNING_MESSAGE"];
            if(strVal == null) throw "value is null";
            v_callTimerSecondMsg = strVal;
            if( UTILS.isEmpty(v_callTimerSecondMsg)==true ) v_callTimerSecondMsg=null;
        } catch (err) {
            v_callTimerSecondMsg = null;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "SKIP_PHONE_NUMBER_VAL_REGEX"];
            if(strVal == null) throw "value is null";
            v_phoneValidateRegex = new RegExp(strVal);
            "abcd".match(v_phoneValidateRegex);
        } catch (err) {
            alert('Global parameter OE_CONFIG/SKIP_PHONE_NUMBER_VAL_REGEX is not configured correctly.\r\n'+err.description+'\r\nRecipient and customer phone validation will be disabled.');
            v_phoneValidateRegex=null;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "DELIVERY_DAYS_OUT_MAX"];
            if(strVal == null) throw "value is null";
            var intVal = parseInt(strVal);
            if( isNaN(intVal) ) {
                throw("OE_CONFIG/DELIVERY_DAYS_OUT_MAX is not a number.");
            }
            v_productDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),intVal));
            v_productSearchDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),intVal));
        } catch(err) {
            alert('Global parameter OE_CONFIG/DELIVERY_DAYS_OUT_MAX is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 120 days.');
            v_productDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),120));
            v_productSearchDeliveryDateCal.cfg.setProperty("maxdate", UTILS.addDay(new Date(),120));
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "DELIVERY_DATE_DROPDOWN_DAYS"];
            if(strVal == null) throw "value is null";
            v_displayDeliveryDaysCount = parseInt(strVal);
            if( isNaN(v_displayDeliveryDaysCount) ) {
                throw("OE_CONFIG/DELIVERY_DATE_DROPDOWN_DAYS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/DELIVERY_DATE_DROPDOWN_DAYS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 7 days.');
            v_displayDeliveryDaysCount=7;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "PRODUCT_SEARCH_RESULT_ROWS"];
            if(strVal == null) throw "value is null";
            PRODUCT_SEARCH.resultsRows = parseInt(strVal);
            if( isNaN(PRODUCT_SEARCH.resultsRows)) {
                throw("OE_CONFIG/PRODUCT_SEARCH_RESULT_ROWS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/PRODUCT_SEARCH_RESULT_ROWS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 2 rows.');
            PRODUCT_SEARCH.resultsRows = 2;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "PRODUCT_SEARCH_RESULT_COLUMNS"];
            if(strVal == null) throw "value is null";
            PRODUCT_SEARCH.resultsColumns = parseInt(strVal);
            if( isNaN(PRODUCT_SEARCH.resultsColumns) ) {
                throw("OE_CONFIG/PRODUCT_SEARCH_RESULT_COLUMNS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/PRODUCT_SEARCH_RESULT_COLUMNS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 rows.');
            PRODUCT_SEARCH.resultsColumns = 5;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "SEARCH_CANCEL_DELAY_SECONDS"];
            if(strVal == null) throw "value is null";
            v_searchCancelDelaySeconds = parseInt(strVal);
            if( isNaN(v_searchCancelDelaySeconds) ) {
                throw("OE_CONFIG/SEARCH_CANCEL_DELAY_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/SEARCH_CANCEL_DELAY_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_searchCancelDelaySeconds=5;
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "NOTIFY_PANEL_HIDE_SECONDS"];
            if(strVal == null) throw "value is null";
            v_notifySeconds = parseInt(strVal);
            if( isNaN(v_notifySeconds) ) {
                throw("OE_CONFIG/NOTIFY_PANEL_HIDE_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/NOTIFY_PANEL_HIDE_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 10 seconds.');
            v_notifySeconds=10;
        }
        v_notifyTimer = new TIMER.Timer(v_notifySeconds*1000,'notifyTimerCallback()',null);

        try {
            strVal = globalParmsMap[v_hashvalue + "RECALC_ORDER_SECONDS"];
            if(strVal == null) throw "value is null";
            v_recalcOrderSeconds = parseInt(strVal);
            if( isNaN(v_recalcOrderSeconds) ) {
                throw("OE_CONFIG/RECALC_ORDER_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/RECALC_ORDER_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_recalcOrderSeconds=5;
        }
        v_recalcOrderTimer = new TIMER.Timer(v_recalcOrderSeconds*1000,'recalcOrderTimerCallback()',null);

        try {
            strVal = globalParmsMap[v_hashvalue + "CHECK_AVAILABILITY_SECONDS"];
            if(strVal == null) throw "value is null";
            v_checkAvailabilitySeconds = parseInt(strVal);
            if( isNaN(v_recalcOrderSeconds) ) {
                throw("OE_CONFIG/CHECK_AVAILABILITY_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/CHECK_AVAILABILITY_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 5 seconds.');
            v_checkAvailabilitySeconds=5;
        }
        v_checkAvailabilityTimer = new TIMER.Timer(v_checkAvailabilitySeconds*1000,'checkAvailabilityTimerCallback()',null);

        try {
            strVal = globalParmsMap[v_hashvalue + "CHECK_ADDRESS_SECONDS"];
            if(strVal == null) throw "value is null";
            v_checkAddressSeconds = parseInt(strVal);
            if( isNaN(v_checkAddressSeconds) ) {
                throw("OE_CONFIG/CHECK_ADDRESS_SECONDS is not a number.");
            }
        } catch(err) {
            alert('Global parameter OE_CONFIG/CHECK_ADDRESS_SECONDS is not configured correctly.\r\n'+err.description+'\r\nDefaulting to 1 second.');
            v_checkAddressSeconds=1;
        }
        v_checkAddressTimer = new TIMER.Timer(v_checkAddressSeconds*1000,'checkAddressTimerCallback()',null);

        try {
            strVal = globalParmsMap[v_hashvalue + "FLORAL_LABEL_STANDARD"];
            v_standard_label = strVal;
            if( UTILS.isEmpty(v_standard_label)==true ) v_standard_label = 'Shown';
        } catch (err) {
            v_standard_label = 'Shown';
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "FLORAL_LABEL_DELUXE"];
            v_deluxe_label = strVal;
            if( UTILS.isEmpty(v_deluxe_label)==true ) v_deluxe_label = 'Deluxe';
        } catch (err) {
            v_deluxe_label = 'Deluxe';
        }

        try {
            strVal = globalParmsMap[v_hashvalue + "FLORAL_LABEL_PREMIUM"];
            v_premium_label = strVal;
            if( UTILS.isEmpty(v_premium_label)==true ) v_premium_label = 'Premium';
        } catch (err) {
            v_premium_label = 'Premium';
        }

    }
}

/* replace by JSON
function setFavorites(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="product_favorites"]/record',xmlDoc);
        PRODUCT_SEARCH.favoriteProducts = new Array(records.length);
        for( idx=0; idx<records.length; idx++ ) {
            var prodObj = new PRODUCT.OBJECT(FTD_XML.selectNodeText(records[idx],'product_id'));
            PRODUCT_SEARCH.favoriteProducts[idx]=prodObj;
        }
    }
}
*/

function setFavoritesJSON(favorites) {
    if( favorites!=null ) {
        var results = makeJSONArray(favorites.record);
        var records = results.length;
        PRODUCT_SEARCH.favoriteProducts = new Array(records);
        for( idx=0; idx<records; idx++ ) {
            var prodObj = new PRODUCT.OBJECT(results[idx].product_id);
            PRODUCT_SEARCH.favoriteProducts[idx]=prodObj;
            PRODUCT_SEARCH.allFavoriteProducts[idx]=prodObj;
        }
    }
}

/* replace by JSON
function setGreetings(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="card_messages"]/record',xmlDoc);
        FTD_DOM.clearSelect('cardTypeList');
        for( idx=0; idx<records.length; idx++ ) {
            FTD_DOM.insertOption('cardTypeList',FTD_XML.selectNodeText(records[idx],'message_txt'),FTD_XML.selectNodeText(records[idx],'display_seq'),false);
        }
    }
}
*/

function setGreetingsJSON(greetings) {
    if( greetings!=null ) {
        var results = makeJSONArray(greetings.record);
        var records = results.length;
        FTD_DOM.clearSelect('cardTypeList');
        for( idx=0; idx<records; idx++ ) {
            FTD_DOM.insertOption('cardTypeList',results[idx].message_txt,results[idx].display_seq,false);
        }
    }
}

/* replace by JSON
function setScripting(xmlDoc) {
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="oe_script_master"]/record',xmlDoc);
        v_scriptFields = new Array();
        for( idx=0; idx<records.length; idx++ ) {
            var elId = FTD_XML.selectNodeText(records[idx],'script_id');
            var text = FTD_XML.selectNodeText(records[idx],'script_txt');

            var el = $(elId);

            if( el!=null && UTILS.isEmpty(text)==false ) {
                v_scriptFields[elId]=text;
                Event.observe(elId,"focus",ORDER_EVENTS.onScriptElementFocus);
            }
        }
    }
}
*/

function setScriptingJSON(scripting) {
    if( scripting!=null ) {
        var results = makeJSONArray(scripting.record);
        var records = results.length;
        v_scriptFields = new Array();
        v_scriptFieldsHold = new Array();
        for( idx=0; idx<records; idx++ ) {
            var elId = results[idx].script_id;
            var text = results[idx].script_txt;

            var el = $(elId);

            if( el!=null && UTILS.isEmpty(text)==false ) {
                v_scriptFields[elId]=text;
                v_scriptFieldsHold[elId]=text;
                Event.observe(elId,"focus",ORDER_EVENTS.onScriptElementFocus);
            }
        }
    }
}

function checkResultSetStatus(xmlDoc,resultSetName,silent,popup,failOnNullStatus) {
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = true;
    var errorMsg = '';

    if( resultSet==null ) {
        retval=false;
        errorMsg='The server did not return the required data for '+resultSetName+'.  Contact BACOM support immediatedly';
    } else {
        var status = FTD_XML.getAttributeText(resultSet,'status');
        if( (status==null&&failOnNullStatus==false) || status=='Y' ) {
            retval=true;
        } else {
            retval=false;
            errorMsg=FTD_XML.getAttributeText(resultSet,'message');
        }
    }

    if( retval==false && silent==false ) {
        if( popup==true ) {
            alert(errorMsg);
        } else {
            notifyUser(errorMsg,false,true);
        }
    }

    return retval;
}

function resetForm() {
    var idx;
    for( idx=0; idx<v_cartList.length; idx++ ) {
        var orderObject = v_cartList[idx];
        if( orderObject!=null && orderObject.orderElement!=null ) {
            $('cartDiv').removeChild(orderObject.orderElement);
        }
    }
    v_currentCartIdx = -1;

    v_verifiedSourceCode = false;

    FTD_DOM.clearSelect('customerRecipCombo');

    v_cartList.clear();
    v_callTimer.stopTimer();
    hideValidationTab();
    $('callDataTimer').innerHTML = '00:00';
    $('paymentGCAmount').innerHTML = '0.00';
    $('paymentCCAmount').innerHTML = '0.00';
    $('paymentINAmount').innerHTML = '0.00';
    $('paymentNCAmount').innerHTML = '0.00';
    $('callDataTimer').style.color = v_callbackTimerColorNormal;
    $('rightDivId').style.display='block';
    $('confirmationDivId').style.display='none';
    $('confMasterOrderNumber').innerHTML='';
    $('actionButtonExit').style.display = '';
    $('actionButtonAbandon').disabled = true;
    $('billingButtonSubmit').disabled = false;
    $('leftDivId').disabled=false;
    $('businessDiv').disabled=false;
    $('cartItemId').style.display = 'none';
    $('productId').value='';
    $('addOnCardsTable').style.display = 'none';
    $('detailDiv').style.display='none';
    $('callDataSourceCode').disabled=true;
    $('callDataSourceCodeSearch').disabled=true;
    $('paymentCCManAuthDiv').style.display='none';
    $('paymentCCManualAuth').value='';
    $('recipientPhoneSearch').style.visibility = 'hidden';
    $('customerPhoneSearch').style.visibility = 'hidden';
    $('paymentCCNumber').disabled=false;
    $('paymentCCTypeCombo').disabled=false;
    $('paymentCCExpMonthCombo').disabled=false;
    $('paymentCCExpYear').disabled=false;
    $('paymentCCManAuthDiv').style.display='none';
    $('paymentCCManualAuth').value='';
    $('cardTypeList').selectedIndex=-1;
    v_iotwMembershipWarning = false;
    v_customerPhoneChecked = false;
    v_viewedProducts = new Array();
    v_currentRecalcCartValues = new Array();
    v_currentRecalcOrderValues = new Array();
    v_lastRecalcCartValues = new Array();
    v_lastRecalcOrderValues = new Array();
    v_currentAvailabilityOrderValues = new Array(1);
    v_lastAvailabilityOrderValues = new Array(1);
    $('psViewedList').innerHTML='';
    setOrderData();
    setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
    if (v_previousDeliveryType != 'florist') {
        v_previousDeliveryType = 'florist';
        populateDeliveryDateCombos();
    }
}

var v_firstWarningGiven=false;
var v_secondWarningGiven=false;
function callTimerCallback() {
    var displayText='';
    v_callTimer.setTimer();

    if( v_callTimer.hoursCount>0 ) {
      displayText = displayText + v_callTimer.hoursCount + ':';
    }

    if( v_callTimer.minutesCount < 10 ) {
        displayText = displayText + '0';
    }
    displayText = displayText + v_callTimer.minutesCount + ':';

    if( v_callTimer.secondsCount < 10 ) {
        displayText = displayText + '0';
    }
    displayText = displayText + v_callTimer.secondsCount;

    $('callDataTimer').innerHTML = displayText;

    if( v_callTimer.timerCount >= v_callTimerSecondWarnSeconds ) {
        if( v_secondWarningGiven==false ) {
            $('callDataTimer').style.color = v_callbackTimerColorError;
            v_secondWarningGiven=true;
            if( v_callTimerSecondMsg!=null ) notifyUser(v_callTimerSecondMsg,false,true);
        }
    } else if( v_callTimer.timerCount >= v_callTimerFirstWarnSeconds ) {
        if( v_firstWarningGiven==false ) {
            $('callDataTimer').style.color = v_callbackTimerColorWarn;
            v_firstWarningGiven=true;
            if( v_callTimerFirstMsg!=null ) notifyUser(v_callTimerFirstMsg,false,true);
        }
    }
}

function notifyUser(message,appendFlag,startTimer) {

    if( (appendFlag && appendFlag==true) || $('notifyDivId').visible() ) {

        var oldMsg = $('notifyArea').innerHTML;
        if(oldMsg.indexOf(message)<0){
        	$('notifyArea').innerHTML = oldMsg+'<br>'+ message;
        }
        else{
        	$('notifyArea').innerHTML = message;
        }

    } else {
        $('notifyArea').innerHTML = message;
    }

    if( !($('notifyDivId').visible()) ) {
      new Effect.SlideDown('notifyDivId');
    }

    if( startTimer ) {
        if( startTimer==true )
            v_notifyTimer.startTimer();
        else if( v_notifyTimer.isRunning() )
            v_notifyTimer.stopTimer();
    }
}

function logMessage(message) {
        try {
            var date = new Date();
            if(v_logMessageMap[message] == null){
                v_logMessageMap[message] = date;
                message = "Starting " + message;
                var oldMsg = $('logArea').innerHTML;
                $('logArea').innerHTML = oldMsg+'<br>'+message;
            }else{
                date = new Date();

                var startDate = v_logMessageMap[message].getTime();
                v_logMessageMap[message] = null;
                var endDate = date.getTime();
                var result = endDate - startDate;
                var seconds = result/1000;

                var oldMsg = $('logArea').innerHTML;
                message = "Ending " + message + " which took " + seconds + "seconds!";
                $('logArea').innerHTML = oldMsg+'<br>'+message;
            }
        } catch (err) {
            // Do nothing.  This code is only used for helper messaging.  If it blows up we don't want it to halt the application.
        }

}

function notifyTimerCallback() {
  manuallyCloseNotify();
}

function manuallyCloseNotify() {
    if( $('notifyDivId').visible() ) {
        new Effect.SlideUp('notifyDivId');
    }

    if( v_notifyTimer.isRunning() ) {
        v_notifyTimer.stopTimer();
    }
}

function productVerbageCleanup(str) {
    if( str==undefined||str==null ) {
      return str;
    }

    var retval = FTD_DOM.unconvertHtmlSpecialChars(str);

    while( retval.match(/<BR>/)!=null ) {
      retval = retval.replace(/<BR>/,"  ");
    }

    while( retval.match(/<br>/)!=null ) {
      retval = retval.replace(/<br>/,"  ");
    }

    return retval;
}

function productIdChanged(getProductDetails) {
    var orderObj = v_cartList[v_currentCartIdx];
    var newValue = UTILS.trim($('productId').value);

    if(Object.isUndefined(newValue) || newValue==null || newValue.length==0) {
        return;
    } else {
        newValue = newValue.toUpperCase();
        var productObj = orderObj.productObject;

        if( getProductDetails==true ) {
            if( productObj.productId!=newValue ) {
                orderObj.resetProduct(true); //Defect 4517
                productObj = new PRODUCT.OBJECT(newValue);
                orderObj.productObject = productObj;
                orderObj.productSourceCode = v_masterSourceCode.id;

                //Defect 4517
                orderObj.productId=newValue;
                //orderObj.populateDOM();
                //End changes for defect 4517
            }
            orderObj.productOption = null;
            ORDER_AJAX.getProductDetail(newValue,v_currentCartIdx,'ORDER',true);
        } else {
            PRODUCT_SEARCH.addProductToRecentlyViewed(productObj);
        }

        if( productObj.smallImage=='images/noproduct.gif' ) {
            productObj.smallImage='images/npi_a.jpg';
        }

        if( productObj.largeImage=='images/noproduct.gif' ) {
            productObj.largeImage='images/npi_c.jpg';
        }
    }
}

function populateProductObjectFromDetail(xmlDoc,productId,target) {
    var prodObj = new PRODUCT.OBJECT(null);
    if( xmlDoc!=null ) {
        var record = XPath.selectNodes('/result/rs[@name="product_details"]/record',xmlDoc)[0];

        if( checkResultSetStatus(xmlDoc,"product_details",false,false,false) && record!=null ) {
            var strValue = prodObj.productId=FTD_XML.selectNodeText(record,'novator_id');
            if( strValue==productId ) {
                productId=FTD_XML.selectNodeText(record,'product_id');
            }
            prodObj.productId=productId;
            prodObj.masterProductId=prodObj.productId;
            prodObj.name=FTD_XML.selectNodeText(record,'novator_name');
            prodObj.productDescription=FTD_XML.selectNodeText(record,'long_description');  //long description
            prodObj.subcodeDescription=null;

//            if( productId=='3052' ) {
//                var x = 1;
//            }

            strValue = FTD_XML.selectNodeText(record,'standard_price');
            if( strValue=="0" ) strValue=null;
            if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
            prodObj.standardPrice=strValue;

            strValue = FTD_XML.selectNodeText(record,'premium_price');
            if( strValue=="0" ) strValue=null;
            prodObj.premiumPrice=strValue;

            strValue = FTD_XML.selectNodeText(record,'deluxe_price');
            if( strValue=="0" ) strValue=null;
            if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
            prodObj.deluxePrice=strValue;

            strValue = FTD_XML.selectNodeText(record,'variable_price_max');
            if( strValue=="0" ) strValue=null;
            if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
            prodObj.variablePrice=strValue;

            prodObj.smallImage=FTD_XML.selectNodeText(record,'smallimage');
            prodObj.largeImage=FTD_XML.selectNodeText(record,'largeimage');
            prodObj.productType=FTD_XML.selectNodeText(record,'product_type');
            prodObj.custom=(FTD_XML.selectNodeText(record,'custom_flag')=='Y');
            prodObj.floristProduct=(FTD_XML.selectNodeText(record,'ship_method_florist')=='Y');
            prodObj.vendorProduct=(FTD_XML.selectNodeText(record,'ship_method_carrier')=='Y');
            prodObj.secondChoice=FTD_XML.selectNodeText(record,'second_choice');
            if(FTD_XML.selectNodeText(record,'add_on_funeral_flag')=='Y') prodObj.addOnArray['F']='F';
            prodObj.addOnCard = (FTD_XML.selectNodeText(record,'add_on_cards_flag')=='Y'?true:false);

            if(FTD_XML.selectNodeText(record,'exception_code')=='A') {
                prodObj.exceptionStartDate=FTD_XML.selectNodeText(record,'exception_start_date');
                prodObj.exceptionEndDate=FTD_XML.selectNodeText(record,'exception_end_date');
            }

            prodObj.allowFreeShippingFlag = FTD_XML.selectNodeText(record,'allow_free_shipping_flag');

            var idx;
            var records;

            //Discounts
            var discounts = XPath.selectNodes('/result/rs[@name="product_discounts"]/record[@row="1"]/discount',xmlDoc)[0];
            if(discounts!=null && Object.isUndefined(discounts)==false) {

                prodObj.discountType=FTD_XML.selectNodeText(discounts,'discount-type');
                if( UTILS.isEmpty(prodObj.discountType)==false ) {
                    strValue = FTD_XML.selectNodeText(discounts,'standard-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.standardDiscount=strValue;

                     strValue = FTD_XML.selectNodeText(discounts,'deluxe-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.deluxeDiscount=strValue;

                    strValue = FTD_XML.selectNodeText(discounts,'premium-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.premiumDiscount=strValue;
                }


                //IOTW
                var iotwNode = XPath.selectNodes('/result/rs[@name="product_discounts"]/record[@row="1"]/iotw-flag[@flag="Y"]',xmlDoc)[0];
                if(iotwNode!=null && Object.isUndefined(iotwNode)==false) {
                    prodObj.iotwObject.flag=true;
                    prodObj.iotwObject.iotwSourceCode=FTD_XML.selectNodeText(iotwNode,'item-source-code');
                    prodObj.iotwObject.sourceCode=FTD_XML.selectNodeText(iotwNode,'master-source-code');
                    prodObj.iotwObject.message=FTD_XML.selectNodeText(iotwNode,'product-page-message');
                    prodObj.iotwObject.discountType=FTD_XML.selectNodeText(iotwNode,'discount-type');

                    strValue = FTD_XML.selectNodeText(iotwNode,'standard-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.iotwObject.discountType=='Discount') strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.iotwObject.standardDiscount=strValue;

                    strValue = FTD_XML.selectNodeText(iotwNode,'premium-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.iotwObject.discountType=='Discount') strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.iotwObject.premiumDiscount=strValue;

                    strValue = FTD_XML.selectNodeText(iotwNode,'deluxe-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && prodObj.iotwObject.discountType=='Discount') strValue=UTILS.formatStringToPrice(strValue);
                    prodObj.iotwObject.deluxeDiscount=strValue;

                    var deliveryDates = XPath.selectNodes('/result/rs[@name="product_discounts"]/record[@row="1"]/iotw-flag[@flag="Y"]/delivery-dates/date',xmlDoc);
                    for( var ddidx=0; ddidx<deliveryDates.length; ddidx++ ) {
                        var ddNode = deliveryDates[ddidx];
                        if( ddNode!=null ) {
                            var strDate = ddNode.firstChild.nodeValue+'';
                            if( UTILS.isEmpty(strDate)==false )
                                prodObj.iotwObject.deliveryDates['dd'+strDate]=strDate;
                        }
                    }
                } else {
                    prodObj.iotwSourceCode=null;
                }
            }

            //Subcodes
            records = XPath.selectNodes('/result/rs[@name="product_subcodes"]/record',xmlDoc);
            for(idx=0; idx<records.length; idx++ ) {
                var subcode = new PRODUCT.OBJECT(FTD_XML.selectNodeText(records[idx],'product_subcode_id'));
                subcode.masterProductId = FTD_XML.selectNodeText(records[idx],'product_id');
                if(prodObj.masterProductId!=subcode.masterProductId) prodObj.masterProductId=subcode.masterProductId;
                subcode.name=FTD_XML.selectNodeText(records[idx],'subcode_description');
                subcode.subcodeDescription=subcode.name;
                subcode.productDescription=prodObj.productDescription;
                subcode.standardPrice=FTD_XML.selectNodeText(records[idx],'subcode_price');
                subcode.premiumPrice=null;
                subcode.deluxePrice=null;
                subcode.variablePrice=null;
                subcode.smallImage=prodObj.smallImage;
                subcode.largeImage=prodObj.largeImage;
                subcode.productType=prodObj.productType;
                subcode.floristProduct=prodObj.floristProduct;
                subcode.vendorProduct=prodObj.vendorProduct;

                var discount = records[idx].getElementsByTagName('discount')[0];
                if(discount!=null && Object.isUndefined(discount)==false) {
                    subcode.discountType = FTD_XML.selectNodeText(discount,'discount-type');
                    subcode.standardDiscount = FTD_XML.selectNodeText(discount,'discount-price');
                }

                iotwNode = records[idx].getElementsByTagName('iotw-flag')[0];
                if(iotwNode!=null && Object.isUndefined(iotwNode)==false && FTD_XML.getAttributeText(iotwNode,'flag')=='Y') {
                    subcode.iotwObject.flag=true;
                    subcode.iotwObject.iotwSourceCode=FTD_XML.selectNodeText(iotwNode,'item-source-code');
                    subcode.iotwObject.sourceCode=FTD_XML.selectNodeText(iotwNode,'master-source-code');
                    subcode.iotwObject.message=FTD_XML.selectNodeText(iotwNode,'product-page-message');
                    subcode.iotwObject.discountType=FTD_XML.selectNodeText(iotwNode,'discount-type');

                    strValue = FTD_XML.selectNodeText(iotwNode,'discount-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && subcode.iotwObject.discountType=='Discount') strValue=UTILS.formatStringToPrice(strValue);
                    subcode.iotwObject.standardDiscount=strValue;

                    deliveryDates = iotwNode.getElementsByTagName('delivery-dates')[0];
                    for( ddidx=0; ddidx<deliveryDates.childNodes.length; ddidx++ ) {
                        ddNode = deliveryDates.childNodes[ddidx];
                        if( ddNode!=null && ddNode.nodeType==1 ) {
                            strDate = FTD_XML.getInnerHTMLText(ddNode);
                            if( UTILS.isEmpty(strDate)==false )
                                subcode.iotwObject.deliveryDates['dd'+strDate]=strDate;
                        }
                    }
                }

                prodObj.subcodeArray.push(subcode);
            }

            //Upsells
            records = XPath.selectNodes('/result/rs[@name="product_upsells"]/record',xmlDoc);
            for(idx=0; idx<records.length; idx++ ) {
                var upsell = new PRODUCT.OBJECT(FTD_XML.selectNodeText(records[idx],'upsell_detail_id'));
                upsell.masterProductId = FTD_XML.selectNodeText(records[idx],'upsell_master_id');
                if(prodObj.masterProductId!=upsell.masterProductId) prodObj.masterProductId=upsell.masterProductId;
                var upsellName=FTD_XML.selectNodeText(records[idx],'upsell_detail_name');
                upsellName = upsellName.replace(/(<([^>]+)>)/ig,"");
                upsell.name = upsellName;
                upsell.subcodeDescription=upsell.name;
                upsell.productDescription=prodObj.productDescription;
                upsell.standardPrice=FTD_XML.selectNodeText(records[idx],'upsell_price');
                upsell.premiumPrice=null;
                upsell.deluxePrice=null;
                upsell.variablePrice=null;
                upsell.smallImage=prodObj.smallImage;
                upsell.largeImage=prodObj.largeImage;
                upsell.productType=prodObj.productType;
                upsell.floristProduct=prodObj.floristProduct;
                upsell.vendorProduct=prodObj.vendorProduct;

                discount = records[idx].getElementsByTagName('discount')[0];
                if(discount!=null && Object.isUndefined(discount)==false) {
                    upsell.discountType = FTD_XML.selectNodeText(discount,'discount-type');
                    upsell.standardDiscount = FTD_XML.selectNodeText(discount,'discount-price');
                }

                iotwNode = records[idx].getElementsByTagName('iotw-flag')[0];
                if(iotwNode!=null && Object.isUndefined(iotwNode)==false && FTD_XML.getAttributeText(iotwNode,'flag')=='Y') {
                    upsell.iotwObject.flag=true;
                    upsell.iotwObject.iotwSourceCode=FTD_XML.selectNodeText(iotwNode,'item-source-code');
                    upsell.iotwObject.sourceCode=FTD_XML.selectNodeText(iotwNode,'master-source-code');
                    upsell.iotwObject.message=FTD_XML.selectNodeText(iotwNode,'product-page-message');
                    upsell.iotwObject.discountType=FTD_XML.selectNodeText(iotwNode,'discount-type');

                    strValue = FTD_XML.selectNodeText(iotwNode,'discount-price');
                    if( strValue=="0" ) strValue=null;
                    if( strValue!=null && upsell.iotwObject.discountType=='Discount') strValue=UTILS.formatStringToPrice(strValue);
                    upsell.iotwObject.standardDiscount=strValue;

                    deliveryDates = iotwNode.getElementsByTagName('delivery-dates')[0];
                    for( ddidx=0; ddidx<deliveryDates.childNodes.length; ddidx++ ) {
                        ddNode = deliveryDates.childNodes[ddidx];
                        if( ddNode!=null && ddNode.nodeType==1 ) {
                            strDate = FTD_XML.getInnerHTMLText(ddNode);
                            if( UTILS.isEmpty(strDate)==false )
                                upsell.iotwObject.deliveryDates['dd'+strDate]=strDate;
                        }
                    }
                }

                prodObj.upsellArray.push(upsell);
            }

            //Colors
            //  COLORS BEING RE-PURPOSED FOR PROJECT FRESH - SO COMMENTED OUT MARCH 2018
            //records = XPath.selectNodes('/result/rs[@name="product_colors"]/record',xmlDoc);
            //for(idx=0; idx<records.length; idx++ ) {
            //    prodObj.colorArray.push(new COLOR.OBJECT(FTD_XML.selectNodeText(records[idx],'product_color'),FTD_XML.selectNodeText(records[idx],'description')));
            //}

            //Vases
            records = XPath.selectNodes('/result/rs[@name="product_vases"]/record',xmlDoc);
            if (records.length > 0) {
                prodObj.vaseArray = new Array();
                prodObj.vaseFloristVendorArray = new Array();
                for(idx=0; idx<records.length; idx++ ) {
                    thisAddonId = FTD_XML.selectNodeText(records[idx],'addon_id');
                    thisAddonDesc = FTD_XML.selectNodeText(records[idx],'description');
                    thisAddonPrice = FTD_XML.selectNodeText(records[idx],'price');
                    thisAddonFloristFlag = FTD_XML.selectNodeText(records[idx],'florist_addon_flag');
                    thisAddonVendorFlag = FTD_XML.selectNodeText(records[idx],'vendor_addon_flag');
                    thisAddonDeliveryType='';
                    if (thisAddonFloristFlag=='Y') {
                        if (thisAddonVendorFlag=='Y') {
                            thisAddonDeliveryType='both';
                        } else {
                            thisAddonDeliveryType='florist';
                        }
                    } else {
                        if (thisAddonVendorFlag=='Y') {
                            thisAddonDeliveryType='vendor';
                        }
                    }
                    prodObj.vaseArray[idx+1] = thisAddonId;
                    prodObj.vaseFloristVendorArray[idx+1] = thisAddonDeliveryType;
                }
                $('addOnVaseRow').style.display='block';
                var fld = findFieldObject('productAddonVase',v_currentCartIdx);
                if( fld!=null && fld.tabIndex!=null ) {
                    $('productAddonVase').tabIndex = fld.tabIndex;
                }
            } else {
                $('addOnVaseRow').style.display='none';
                fld = findFieldObject('productAddonVase',v_currentCartIdx);
                if( fld!=null && fld.tabIndexInitial!=null ) {
                    $('productAddonVase').tabIndex = fld.tabIndexInitial;
                }
            }

            //Add-ons
            records = XPath.selectNodes('/result/rs[@name="product_addons"]/record',xmlDoc);
            for(idx=records.length; idx<v_max_addons; idx++ ) {
                prodObj.addOnArray[idx+1] = '';
                prodObj.addOnDescriptionArray[idx+1] = '';
                prodObj.addOnMaxQtyArray[idx+1] = '';
                prodObj.addOnTypeArray[idx+1] = '';
                prodObj.addOnPriceArray[idx+1] = '';
            }
            for(idx=0; idx<records.length; idx++ ) {
                thisAddonId = FTD_XML.selectNodeText(records[idx],'addon_id');
                thisAddonDesc = FTD_XML.selectNodeText(records[idx],'description');
                thisAddonPrice = FTD_XML.selectNodeText(records[idx],'price');
                thisAddonMaxQty = FTD_XML.selectNodeText(records[idx],'max_qty');
                thisAddonType = FTD_XML.selectNodeText(records[idx],'addon_type');
                thisAddonFloristFlag = FTD_XML.selectNodeText(records[idx],'florist_addon_flag');
                thisAddonVendorFlag = FTD_XML.selectNodeText(records[idx],'vendor_addon_flag');
                thisAddonDeliveryType='';
                if (thisAddonFloristFlag=='Y') {
                    if (thisAddonVendorFlag=='Y') {
                        thisAddonDeliveryType='both';
                    } else {
                        thisAddonDeliveryType='florist';
                    }
                } else {
                    if (thisAddonVendorFlag=='Y') {
                        thisAddonDeliveryType='vendor';
                    }
                }
                prodObj.addOnArray[idx+1] = thisAddonId;
                prodObj.addOnMaxQtyArray[idx+1] = thisAddonMaxQty;
                prodObj.addOnDescriptionArray[idx+1] = thisAddonDesc;
                prodObj.addOnTypeArray[idx+1] = thisAddonType;
                prodObj.addOnPriceArray[idx+1] = UTILS.formatStringToPrice(thisAddonPrice);
                prodObj.addOnFloristVendorArray[idx+1] = thisAddonDeliveryType;
            }

//            if (prodObj.addOnCard) {
//                prodObj.addOnArray[4] = '';
//                prodObj.addOnDescriptionArray[4] = '';
//                prodObj.addOnMaxQtyArray[4] = '';
//                prodObj.addOnTypeArray[4] = '';
//                prodObj.addOnPriceArray[4] = '';
//            } else if (prodObj.addOnArray['F'] == 'F') {
//                //max is already 4 so we don't need to do anything
//            }

            prodObj.readyFlag=true;
        }
    }

    return prodObj;
}

function displayAddOnCards() {
    if( $('addOnCardCheckbox').checked ) {
        var fld = findFieldObject('addOnCardCheckbox',v_currentCartIdx);
        if( fld!=null && fld.tabIndex!=null ) {
            $('addOnCardCheckbox').tabIndex = fld.tabIndex;
        }
        $('addOnCardSelect').disabled = false;
        if( $('addOnCardSelect').selectedIndex>0 ) {
            $('addOnCardImage').style.display = 'inline';
            $('addOnCardDetailRow').style.display = 'inline';
        } else {
            $('addOnCardImage').style.display = 'none';
            $('addOnCardDetailRow').style.display = 'none';
        }
    } else {
        if( $('addOnCardSelect').length>0 ) {
            $('addOnCardSelect').selectedIndex = 0;
        }
        $('addOnCardSelect').disabled = true;
        $('addOnCardImage').style.display = 'none';
        $('addOnCardDetailRow').style.display = 'none';
    }

    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj!=null ) {
        orderObj['addOnCardCheckbox'] = $('addOnCardCheckbox').checked;
        orderObj.isRequired('addOnCardSelect');
    }
}

function findAddonById(addonID) {
    var retval = null;

    for( var idx=0; idx<v_addons.length; idx++ ) {
        if( v_addons[idx].id==addonID ) {
            retval = v_addons[idx];
        }
    }

    return retval;
}

function setDefaultImage(el) {
    if( el.nodeName.toUpperCase()=='IMG' ) {
        el.src='images/npi_a.jpg';
    }
}

function setCountryFields(countryElementId,stateElementId,zipElementId,zipLookupId,keywordsId) {

    var selectedCountryId = $(countryElementId).getValue();
    var stateElement = $(stateElementId);

    if( UTILS.isEmpty(stateElement)==false ) {
        FTD_DOM.clearSelect(stateElement);

        if( v_states.length > 0 ) {
            FTD_DOM.insertOption(stateElement,'','',false);

            for( var idx=0; idx<v_states.length; idx++ ) {
                if( v_states[idx].countryId==selectedCountryId ) {
                    FTD_DOM.insertOption(stateElement,v_states[idx].comboPrompt,v_states[idx].comboValue,false);
                }
            }
        }

        var stateFld = findFieldObject(stateElementId,v_currentCartIdx);
        if( stateFld!=null ) {
            if( stateElement.length>1 ) {
                stateElement.style.visibility='visible';
                if( UTILS.isEmpty(stateFld.labelElementId)==false ) {
                    $(stateFld.labelElementId).style.visibility='visible';
                }
            } else {
                stateElement.style.visibility='hidden';
                if( UTILS.isEmpty(stateFld.labelElementId)==false )
                    $(stateFld.labelElementId).style.visibility='hidden';
            }

            if( stateFld.type=='HEADER' ) {
                stateFld.isRequired();
            }
        }
    }

    if( UTILS.isEmpty(zipElementId)==false ) {
        var zipFld = findFieldObject(zipElementId, v_currentCartIdx);
        if( zipFld!=null ) {
            var zipElement = $(zipElementId);
            if( isCountryDomestic(selectedCountryId) ) {
                zipElement.style.visibility='visible';
                if( UTILS.isEmpty(zipFld.labelElementId)==false ) {
                    $(zipFld.labelElementId).style.visibility='visible';
                }
            } else {
                zipElement.value='';
                zipElement.style.visibility='hidden';
                if( UTILS.isEmpty(zipFld.labelElementId)==false )
                    $(zipFld.labelElementId).style.visibility='hidden';
            }

            if( zipFld.type=='DETAIL' && v_currentCartIdx>-1 ) {
                v_cartList[v_currentCartIdx][countryElementId] = selectedCountryId;
                v_cartList[v_currentCartIdx].isRequired(zipElementId);
            } else if( zipFld.type=='HEADER' ) {
                zipFld.isRequired();
            }
        }
    }

    if( UTILS.isEmpty(zipLookupId)==false ) {
        zipFld = findFieldObject(zipLookupId, v_currentCartIdx);
        if( zipFld!=null ) {
            var zipLookup = $(zipLookupId);
            if( isCountryDomestic(selectedCountryId) ) {
                zipLookup.style.visibility='visible';
                //On ie6, the image gets lost with visibility set to hidden
                //so, need to recreate it
                var img = $(zipLookupId+'Image');
                if( img ) {
                    img.style.visibility='visible';
                } else {
                    img = new FTD_DOM.DOMElement('img', {
                        id:zipLookupId+'Image',
                        alt:'Open zip search',
                        src:'images/magnify.jpg'
                    }).createNode();
                    zipLookup.appendChild(img);
                }
            } else {
                zipLookup.value='';
                zipLookup.style.visibility='hidden';
            }
        }
    }

    if( UTILS.isEmpty(keywordsId)==false ) {
        var keywordsFld = findFieldObject(keywordsId, v_currentCartIdx);
        if( keywordsFld!=null ) {
            var keywords = $(keywordsId);
            if( isCountryDomestic(selectedCountryId) ) {
                keywords.style.visibility='visible';
                if( UTILS.isEmpty(keywordsFld.labelElementId)==false )
                    $(keywordsFld.labelElementId).style.visibility='visible';
            } else {
                keywords.value='';
                keywords.style.visibility='hidden';
                if( UTILS.isEmpty(keywordsFld.labelElementId)==false )
                    $(keywordsFld.labelElementId).style.visibility='hidden';
            }
        }
    }
}

function setFieldRequired(el, itemIdx) {
    if( Object.isString(el)==true ) element = $(el);

    if( el==null ) return;

    var fld = findFieldObject(zipElementId, itemIdx);
        if( zipFld!=null ) {
            var zipElement = $(zipElementId);
            if( isCountryDomestic(selectedCountryId) ) {
                zipElement.style.visibility='visible';
                if( UTILS.isEmpty(zipFld.labelElementId)==false ) {
                    $(zipFld.labelElementId).style.visibility='visible';
                    zipFld.isRequired();
                }
            } else {
                zipElement.value='';
                zipElement.style.visibility='hidden';
                if( UTILS.isEmpty(zipFld.labelElementId)==false )
                    $(zipFld.labelElementId).style.visibility='hidden';
            }

            if( zipFld.type=='DETAIL' && v_currentCartIdx>-1 ) {
                v_cartList[v_currentCartIdx][countryElementId] = selectedCountryId;
                v_cartList[v_currentCartIdx].isRequired(zipElementId);
            } else if( zipFld.type=='HEADER' ) {
                zipFld.isRequired();
            }
        }
}


function validateCustomerAndRecipientState(){
    var retVal = false;
    stateValidationError = '';
    addressVerified = 'Y';
    for( var oIdx=0; oIdx<v_cartList.length; oIdx++ ) {
        if (v_cartList[oIdx]!= null)
            validateRecipientStateByZipcode(oIdx);
    }
    if (v_modifyOrderFromCom == false) {
        validateCustomerStateByZipcode();
    }

    if(addressVerified == 'N') {
        $('validationErrorDiv').innerHTML = stateValidationError;
        v_okToProcess = false;
        showValidationTab();
        v_tabControl.tabShow(v_validationTabIdx);
        v_mainAccordion.openByIndex(0);
        retVal = false;
    } else {
        stateValidationError = '';
        v_okToProcess = true;
        retVal = true;
    }
    return retVal;
}

function validateCustomerStateByZipcode() {
	var countryId = $('customerCountryCombo').value;
    if( countryId!='US' ) {
        return;
    }
    var city = $('customerCity').value;
    var zipCode = $('customerZip').value;
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_STATE_BY_ZIP_CITY';
    root.setAttributeNode(attr);


    param = FTD_XML.createElementWithText(doc,'param','');
    attr = doc.createAttribute("name");
    attr.value = 'CITY';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',zipCode);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //Syncronous Ajax Call
    var result = getSynchronousResponse(v_serverURLPrefix+'oeAjax.do', FTD_XML.toString(doc));
    validateCustomerStateCallback(result);

}


function validateRecipientStateByZipcode(itemIdx) {
	var orderObject = v_cartList[itemIdx];
	var countryId = orderObject.recipientCountry;
    if( countryId!='US' ) {
        return;
    }
    var city = orderObject.recipientCity;
    var zipCode = orderObject.recipientZip;
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_STATE_BY_ZIP_CITY';
    root.setAttributeNode(attr);


    param = FTD_XML.createElementWithText(doc,'param','');
    attr = doc.createAttribute("name");
    attr.value = 'CITY';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',zipCode);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'itemidx',itemIdx+''));
    root.appendChild(param);

    //Syncronous Ajax Call
    var result = getSynchronousResponse(v_serverURLPrefix+'oeAjax.do', FTD_XML.toString(doc));
    validateRecipientStateCallback(result);

}

function validateRecipientStateCallback(transport) {
var xmlDoc = parseResponse(transport);

    if( xmlDoc!=null ) {
    	var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var itemIdx = FTD_XML.selectNodeText(echo,'itemidx');
        var records = XPath.selectNodes('/result/rs[@name="state_id"]/record',xmlDoc);

        var state;
        var now = new Date();
        if( records!=null ) {
	        if( records.length==0 ) {
	            //not valid, open up zip search
	            //TEXT_SEARCH.openZipSearch(target,true,false);
	            notifyUser('Failed to recipient validate State.',false,true);
	            v_okToProcess = false;


	        } else{
	            //Populate target fields
	            state = FTD_XML.selectNodeText(records[0],'state_id');
	            var orderObject = v_cartList[itemIdx];
	            var enteredState = orderObject.recipientState;
	            var ordNum = itemIdx;
	            if( state != enteredState) {
	            	ordNum++;
	            	stateValidationError += UTILS.boldWrapper('Validating Reciepient State for order '+ ordNum)+' : <br>';
	        		stateValidationError += "<a href=\"javascript:waitToSetFocus($('recipientState'), "+itemIdx+"); var tmstmp="+now.getTime()+";\">";
	            	stateValidationError += "State: State must be valid for the zip code</a><br>";
	                addressVerified = 'N';
	                v_okToProcess = false;
	                var fld = findFieldObject('recipientState',itemIdx);
	                if( fld!=null ) {
	                	fld.errorFlag = true;
                        fld.setFieldStyle();
                    }

	            }

	        }
        }
    }
}

function validateCustomerStateCallback(transport) {
	var xmlDoc = parseResponse(transport);

	    if( xmlDoc!=null ) {
	    	/*var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
	        var itemIdx = FTD_XML.selectNodeText(echo,'itemidx');;*/
	        var records = XPath.selectNodes('/result/rs[@name="state_id"]/record',xmlDoc);

	        var state;
	        var now = new Date();
	        if( records!=null ) {
		        if( records.length==0 ) {
		            //not valid, open up zip search
		            //TEXT_SEARCH.openZipSearch(target,true,false);
		            notifyUser('Failed to customer validate State.',false,true);
		            v_okToProcess = false;


		        } else{
		            //Populate target fields
		            state = FTD_XML.selectNodeText(records[0],'state_id');

		            if( state != $('customerStateCombo').value) {
		            	stateValidationError += UTILS.boldWrapper('Validating Customer State ')+' : <br>';
		            	stateValidationError += "<a href=\"javascript:waitToSetFocus($('customerStateCombo')); var tmstmp="+now.getTime()+";\">";
		            	stateValidationError += "State: State must be valid for the zip code</a><br>";
		                addressVerified = 'N';
		                v_okToProcess = false;
		                var fld = findFieldObject('customerStateCombo');
		                if( fld!=null ) {
		                	fld.errorFlag = true;
                            fld.setFieldStyle();
	                    }

		            }

		        }
	        }
	    }
	}


var TAB_STR = '&nbsp;&nbsp;&nbsp;&nbsp;';
function validate() {
    //Set the current order data
    setOrderData();

    var retval = true;
    var strHtml = UTILS.boldWrapper('Validating customer and billing data:')+'<br>';
    var idx;
    var el;
    var strval;

    var now = new Date();
    for( idx=0; idx<v_billingFields.length; idx++ ) {
        el = $(v_billingFields[idx].elementId);
        if( v_billingFields[idx].validate(false)==false ) {
            strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('"+el.id+"')); var tmstmp="+now.getTime()+";\">";
            if( v_billingFields[idx].labelText!=null && UTILS.isEmpty(v_billingFields[idx].labelText)==false ) {
                strHtml += UTILS.boldWrapper(v_billingFields[idx].labelText);
                strHtml += ": ";
            }
            strHtml += v_billingFields[idx].errorText+"</a><br>";
            retval = false;
        }

        v_billingFields[idx].setFieldStyle();
    }

    //Cobrand data
    for( idx=0; idx<v_billingInfoFields.length; idx++ ) {
        el = $(v_billingInfoFields[idx].elementId);
        if( v_billingInfoFields[idx].validate(false)==false ) {
            strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('"+el.id+"')); var tmstmp="+now.getTime()+";\">";
            if( v_billingInfoFields[idx].labelText!=null && UTILS.isEmpty(v_billingInfoFields[idx].labelText)==false ) {
                strHtml += UTILS.boldWrapper(v_billingInfoFields[idx].labelText);
                strHtml += ": ";
            }
            strHtml += v_billingInfoFields[idx].errorText+"</a><br>";
            retval = false;
        }

        v_billingInfoFields[idx].setFieldStyle();
    }

    if(retval==true) {
        strHtml+=TAB_STR+'No errors detected in customer or billing data.<br>';
    }
    strHtml+='<br>';

    var cartItemCount=1;
    for( var oIdx=0; oIdx<v_cartList.length; oIdx++ ) {
        var orderObject = v_cartList[oIdx];
        var orderRetVal = true;
        if( orderObject!=null && orderObject.orderElement!=null ) {
            strHtml+=UTILS.boldWrapper('Validating order number '+(cartItemCount++))+':<br>';


            if( orderObject.recipientCountry=='US' && orderObject.avNeedsResolution==true ) {
                    strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('avAcceptButton'),"+oIdx+"); var tmstmp="+now.getTime()+";\">";
                    strHtml += UTILS.boldWrapper("Recipient Address:");
                    strHtml += " Verify address change</a><br>";
                    orderRetVal = false;
            }

            var badProductId=false;
            if( UTILS.isEmpty(orderObject['productId'])==false ) {
                if( orderObject.productGenerallyAvailable==false ) {
                    strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('productId'),"+oIdx+"); var tmstmp="+now.getTime()+";\">";
                    strHtml += UTILS.boldWrapper("Product ID:");
                    strHtml += " Product is not available.</a><br>";
                    orderRetVal=false;
                    badProductId=true;
                } else if( orderObject.productAvailable==false ) {
                    strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('productId'),"+oIdx+"); var tmstmp="+now.getTime()+";\">";
                    strHtml += UTILS.boldWrapper("Product ID:");
                    strHtml += " Product is not available or availability could not be determined.</a><br>";
                    orderRetVal=false;
                    badProductId=true;
                }
            }

            for( var i in orderObject.orderFields ) {
                if( Object.isFunction(orderObject.orderFields[i])==true ) continue;
                var elementId = orderObject.orderFields[i].elementId;
                if( Object.isUndefined(elementId) ) {
                    alert('Undefined elementId');
                    continue;
                }

                if( elementId=='productId' && badProductId==true ) {
                    orderObject.orderFields[i].errorFlag=true;
                    continue;
                }

                if( orderObject.validateField(elementId,false)==false ) {
                    strHtml += TAB_STR+"<a href=\"javascript:waitToSetFocus($('"+elementId+"'),"+oIdx+"); var tmstmp="+now.getTime()+";\">";
                    if( UTILS.isEmpty(orderObject.orderFields[i].labelText)==false ) {
                        strHtml += UTILS.boldWrapper(orderObject.orderFields[i].labelText);
                        strHtml += ": ";
                    }
                    strHtml += orderObject.orderFields[i].errorText+"</a><br>";
                    orderRetVal = false;
                }

                orderObject.orderFields[i].setFieldStyle();
            }

            if( orderRetVal==true ) {
                strHtml += TAB_STR+'No errors detected in order.<br>';
            } else {
                retval=false;
            }
        }
    }


    if( retval==true ) {
        $('validationErrorDiv').innerHTML = 'No errors detected at this time.';
        hideValidationTab();
        v_tabControl.tabShow(0);
    } else {
        $('validationErrorDiv').innerHTML = strHtml;
        showValidationTab();
        v_tabControl.tabShow(v_validationTabIdx);
        v_mainAccordion.openByIndex(0);
    }

    return retval;
}

function setFocus(el,cartIdx) {
    if( el && el!=null ) {
        var fldObj = findFieldObject(el.id,cartIdx);
        if( fldObj!=null ) {
            if( fldObj.accordionCtrlIdx!=null ) {
                v_mainAccordion.openByIndex(fldObj.accordionCtrlIdx);
            }

            if(cartIdx>-1) {
                CART_EVENTS.changeSelectedItem(cartIdx);

                if( fldObj.tabCtrlIdx!=null ) {
                    $('orderTabberId').tabber.tabShow(fldObj.tabCtrlIdx);
                }
                if( el.id.substring(0,2)=='av' )
                    JOE_ORDER.openAddressVerificationPanel();
                else {
                    JOE_ORDER.closeAddressVerificationPanel();
                }
            }

            try {
                el.focus();
            } catch (err) {
                //ignore it
            }
        }
    }
}

function findFieldObject(elId,cartIdx) {
    var idx;
    for( idx=0; idx<v_billingFields.length; idx++ ) {
        if( v_billingFields[idx].elementId == elId ) {
            return v_billingFields[idx];
        }
    }

    for( idx=0; idx<v_billingInfoFields.length; idx++ ) {
        if( v_billingInfoFields[idx].elementId == elId ) {
            return v_billingInfoFields[idx];
        }
    }

    for( idx=0; idx<v_searchFields.length; idx++ ) {
        if( v_searchFields[idx].elementId == elId ) {
            return v_searchFields[idx];
        }
    }

    if( cartIdx>-1 ) {
        var orderFields = v_cartList[cartIdx].orderFields;
        if( orderFields && orderFields!=null ) {
            for( var i in orderFields ) {
                if( Object.isFunction(orderFields[i])==true ) continue;

                if( orderFields[i].elementId==elId ) {
                    return orderFields[i];
                }
            }
        }
    } else {
        for( idx=0; idx<v_orderFields.length; idx++ ) {
            if( Object.isFunction(v_orderFields[idx])==true ) continue;

            if( v_orderFields[idx].elementId==elId ) {
                return v_orderFields[idx];
            }
        }
    }

    return null;
}

function cloneOrderFields() {
    var orderFields = new Array();

    for( var idx=0; idx<v_orderFields.length; idx++ ) {
        orderFields[v_orderFields[idx].elementId]=v_orderFields[idx].cloneNew();
    }

    return orderFields;
}

function checkAvailabilityTimerCallback() {
    if( v_cartList.length==0 ) return;

    if( v_checkAvailabilityTimer.isRunning() ) {
        v_checkAvailabilityTimer.stopTimer();
    } else {
        //Timer should always be running
        return;
    }

    try {
        v_currentAvailabilityOrderValues = new Array(v_cartList.length);
        for( var idx=0; idx<v_cartList.length; idx++ ) {
            if( v_cartList[idx]==null ) {
                v_currentAvailabilityOrderValues[idx]=null;
            } else {
                v_currentAvailabilityOrderValues[idx]=new Array();
                if( shouldAvailabilityBeChecked(idx)==true ) {
                    v_lastAvailabilityOrderValues[idx]=v_currentAvailabilityOrderValues[idx];
                    ORDER_AJAX.checkProductAvailability(idx,false);
                }
            }
        }
    } finally {
        v_checkAvailabilityTimer.startTimer();
    }
}

/*
 * Should availability be checked.
 * Only availability for the currently view order is checked
 */
function shouldAvailabilityBeChecked(itemIdx) {
    //If the timer is not running, then the check
    //is already in progress, so skip it
    if( v_checkAvailabilityTimer.isRunning()==true ) return false;

    //Never run both at once
    if( v_recalcOrderTimer.isRunning()==false ) return false;

    //A source code must be verified
    if( v_verifiedSourceCode==false ) return false;

    var fld;
    var idx;
    var elementId;
    var currentValue;
    var oldValue;
    var somethingChanged=false;
    var failedValidation=false;
    var strHtml='';

    //The current cart item may not have been saved to the
    //order object, so do it now
    if( itemIdx==v_currentCartIdx )
        setOrderData();

    var orderObj = v_cartList[itemIdx];
    if( orderObj==null ) return false;

    //If product is not generallyAvailable, then there is no need to check the
    //rest of the order
    if( orderObj.productGenerallyAvailable==false ) return false;

    //If the product is not generally available, then retrieval of product details
    //hasn't happened or it failed.  Don't attempt to check product availability
    if( orderObj.productObject==null ) return false;
    if( orderObj.productGenerallyAvailable==false ) return false;

    //if the displayed item is the product subcode productId or the upsell master id
    //then availability cannot be checked
    if( orderObj.productObject.masterProductId==orderObj.productObject.productId &&
      ( orderObj.productObject.upsellArray.length>0||orderObj.productObject.subcodeArray.length>0 ) ) {
        return false;
    }

    for( idx=0; idx<v_availabilityFields.length; idx++ ) {
        fld = v_availabilityFields[idx];
        elementId = fld.elementId;

        //Never do availability while the the field is in focus
        if( itemIdx==v_currentCartIdx && v_hasFocus!=null && v_hasFocus==elementId )  return false;

        if( fld.type=='HEADER' ) {
            //Get header data off of the screen
            currentValue = getValue($(elementId));
        } else if( fld.type=='DETAIL' ) {
            //Get detail data off of the order object
            if( Object.isFunction(orderObj.orderFields[elementId])==true ) continue;
            currentValue = orderObj[elementId];
        } else {
            continue;
        }

        if( Object.isString(currentValue) ) currentValue = currentValue.toUpperCase();
        v_currentAvailabilityOrderValues[itemIdx][elementId]=currentValue;

        if( Object.isUndefined(v_lastAvailabilityOrderValues[itemIdx]) ||
            v_lastAvailabilityOrderValues[itemIdx]==null )
        {
            somethingChanged=true;
        } else {
            oldValue = v_lastAvailabilityOrderValues[itemIdx][elementId];

            //If the value has not changed, then move on to the next field
            if( currentValue==oldValue ) {
                continue;
            } else {
                somethingChanged=true;
            }
        }

        //Only do the validation if something changed
        if( fld.type=='HEADER' ) {
            if( fld.validate(false)==false ) {
                failedValidation=true;
            }
            fld.setFieldStyle();
        } else {
            if( orderObj.validateField(elementId,false)==false ) {
                failedValidation=true;
            }

            orderObj.orderFields[elementId].setFieldStyle();
        }
    }

    if( failedValidation==true ) {
        return false;
    }

    if( orderObj.forceAvailabilityCheck==true) {
        somethingChanged=true;
        orderObj.forceAvailabilityCheck=false;
    }

    return somethingChanged;
}

function recalcOrderTimerCallback() {
    if( v_recalcOrderTimer.isRunning() ) {
        //$('orderComments').value = $('orderComments').value+'  recalc order timer is running.';
        v_recalcOrderTimer.stopTimer();
    } else {
        //Timer should always be running
        return;
    }

    try {
        if( shouldRecalculateCart()==true ) {
        	ORDER_AJAX.calculateOrderTotals(false);
        }  else {
            v_recalcOrderTimer.startTimer();
        }
    } catch (err) {
        //alert('Error caught will trying to determine if order should be recalculated:\r\n\r\n'+err.description+'\r\n\r\nContact BACOM support.');
        v_recalcOrderTimer.startTimer();
    }
}

function shouldRecalculateCart() {
    //If the timer is not running, then the recalculation
    //is already in progress, so skip it
    if( v_recalcOrderTimer.isRunning()==true ) return false;

    //Never run both at once
    if( v_checkAvailabilityTimer.isRunning()==false ) return false;

    //A source code must be verified
    if( v_verifiedSourceCode==false ) return false;

    //If any of the items in the card are not available,
    //then do not calculate the cart.
    for( var idx=0; idx<v_cartList.length; idx++ ) {
        if( v_cartList[idx]==null ) continue;
        if( v_cartList[idx].productObject==null ) return false;
        if( v_cartList[idx].productGenerallyAvailable==false ||
            v_cartList[idx].productAvailable==false) return false;
    }

    var fld;
    var elementId;
    var detailArray = new Array();
    var currentValue;
    var oldValue;
    var somethingChanged=false;
    var failedValidation=false;

    v_currentRecalcCartValues = new Array();
    v_currentRecalcOrderValues = new Array(v_cartList.length);

    //Check to see if the number of items in the cart has changed
    currentValue = v_cartList.length+'';
    v_currentRecalcCartValues['ITEMS_IN_CART'] = currentValue;
    oldValue = v_lastRecalcCartValues['ITEMS_IN_CART'];
    if( currentValue!=oldValue ) {
        somethingChanged=true;
    }

    //Check to see if the number of active items in the cart has changed
    currentValue = CART_EVENTS.cartItemCount();
    v_currentRecalcCartValues['ACTIVE_ITEMS_IN_CART'] = currentValue;
    oldValue = v_lastRecalcCartValues['ACTIVE_ITEMS_IN_CART'];
    if( currentValue!=oldValue ) {
        somethingChanged=true;
    }

    //Check at the cart level
    for( idx=0; idx<v_recalcFields.length; idx++ ) {
        fld = v_recalcFields[idx];
        if( fld.type=='HEADER' ) {
            //Did the values change since the last check?
            elementId = fld.elementId;

            var el = $(elementId);
            if( (el.nodeName.toUpperCase()=='INPUT' && (el.type.toLowerCase()=='radio' || el.type.toLowerCase()=='checkbox') )==false ) {
                //Never do recalc while the the field is in focus
                //regardless of the order you are checking
                if( v_hasFocus!=null && v_hasFocus==elementId )  return false;
            }

            currentValue = getValue($(elementId));
            if( Object.isString(currentValue) ) currentValue = currentValue.toUpperCase();
            v_currentRecalcCartValues[elementId] = currentValue;
            oldValue = v_lastRecalcCartValues[elementId];

            //If the value has not changed, then move on to the next field
            if( currentValue==oldValue  ) {
                continue;
            } else {
                somethingChanged=true;

                //Only do the validation if something changed
                if( fld.validate(false)==false ) {
                    failedValidation=true;
                }
                fld.setFieldStyle();
            }

        } else if( fld.type=='DETAIL' ) {
            detailArray.push(fld.elementId);
        }
    }

    //Now check each order in the cart
    for( idx=0; idx<v_cartList.length; idx++ ) {

        if( idx==v_currentCartIdx ) {
            //Current cart item may not have been saved to the order object
            setOrderData();
        }

        var orderObj = v_cartList[idx];
        if( orderObj==null ) continue;

        //If you don't have a product name, then retrieval of product details
        //hasn't happened or the product id entered is invalid.  Don't attempt
        //to recalculate the shopping cart
        if( orderObj.productObject==null ) return false;
        if( UTILS.isEmpty(orderObj.productObject.name)==true ) return false;

        if( orderObj.productAvailable==false ) return false;

        //if the displayed item is the product subcode productId or the upsell master id
        //then recalculation of the cart cannot happen
        if( orderObj.productObject.masterProductId==orderObj.productObject.productId &&
          ( orderObj.productObject.upsellArray.length>0||orderObj.productObject.subcodeArray.length>0 ) ) {
            return false;
        }

        v_currentRecalcOrderValues[idx]=new Array();
        for( var idx2=0; idx2<detailArray.length; idx2++ ) {
            elementId = detailArray[idx2];
            //if( Object.isFunction(orderObj.orderFields[elementId])==true ) continue;
            if( Object.isFunction(orderObj[elementId])==true ) continue;

            el = $(elementId);
            if( (el.nodeName=='INPUT' && (el.type=='radio' || el.type=='checkbox') )==false ) {
                //Never do recalc while the the field is in focus
                //regardless of the order you are checking
                if( v_hasFocus!=null && v_hasFocus==elementId )  return false;
            }

            var chkField;
            if( el.nodeName.toUpperCase()=='INPUT' && el.type.toLowerCase()=='radio' ) {
                chkField = el.name;
            } else if( el.nodeName.toUpperCase()=='INPUT' && el.type.toLowerCase()=='checkbox' ) {
                //Is this a checkbox group
                if( UTILS.isEmpty(el.name) ) {
                    chkField = elementId;
                } else {
                    var checkboxes = document.getElementsByTagName(el.name);
                    if( checkboxes.length==1 ) {
                        chkField = elementId;
                    } else {
                        el = FTD_DOM.getSelectedRadioButton(el.name);
                        if( el!=null ) {
                            chkField = el.name;
                        }
                    }
                }
            } else {
                chkField = elementId;
            }

            //Did the values change since the last check?
            currentValue = orderObj[chkField];
            if( Object.isString(currentValue) ) currentValue = currentValue.toUpperCase();
            v_currentRecalcOrderValues[idx][chkField]=currentValue;
            if( Object.isUndefined(v_lastRecalcOrderValues[idx])==true ||
                Object.isUndefined(v_lastRecalcOrderValues[idx][chkField])==true )
                oldValue='';
            else
                oldValue = v_lastRecalcOrderValues[idx][chkField];

            //If the value has not changed, then move on to the next field
            if( currentValue==oldValue ) {
                continue;
            } else {
                somethingChanged=true;

                //Only do the validation if something changed
                if( orderObj.validateField(elementId,false)==false ) {
                    failedValidation=true
                }
                orderObj.orderFields[elementId].setFieldStyle();
            }
        }

        if( failedValidation==true ) {
            return false;
        }
    }

    // has the email changed? Need to handle Empty and Non Empty
    // As moving from non-empty to empty should calculate with nopersonal account mode
    if(somethingChanged == false
       && v_lastCustomerEmailAddress != v_newCustomerEmailAddress ) {
      somethingChanged = true;
    }

    v_lastCustomerEmailAddress = v_newCustomerEmailAddress;

    return somethingChanged;
}

function checkAddressTimerCallback() {
    if( v_checkAddressTimer.isRunning() ) {
        v_checkAddressTimer.stopTimer();
    } else {
        //Timer should always be running
        return;
    }

    try {
        if( shouldValidateAddress(v_currentCartIdx)==true ) {
            VALIDATE_AJAX.validateAddress(v_currentCartIdx);
        }  else {
            v_checkAddressTimer.startTimer();
        }
    } catch (err) {
        //alert('Error caught will trying to determine if order should be recalculated:\r\n\r\n'+err.description+'\r\n\r\nContact BACOM support.');
        v_checkAddressTimer.startTimer();
    }
}

function shouldValidateAddress(itemIdx) {
    //If the timer is not running, then the check
    //is already in progress, so skip it
    if( v_checkAddressTimer.isRunning()==true ) return false;

    var idx;
    var elementId;
    var currentValue;
    var oldValue;

    v_currentAddressValues = new Array();

    //The current cart item may not have been saved to the
    //order object, so do it now
    if( itemIdx==v_currentCartIdx )
        setOrderData();

    var orderObj = v_cartList[itemIdx];
    if( orderObj==null ) return false;
    if( UTILS.isEmpty(orderObj.avResultCode)==false ) return false;
    //flag to track if we've succesfully run verification
    if (orderObj.avsPerformed === true) return false;
    //if address verification was disabled for this order, don't try again
    if (orderObj.avDisabled === true) return false;

    if( FTD_DOM.getSelectedValue($('recipientCountry'))!='US' ) return false;

    // Don't check while Text Search Screen is open
    if (v_textSearchOpen) return false;

    for( idx=0; idx<v_avsFields.length; idx++ ) {
        elementId = v_avsFields[idx].elementId;
        //Never do availability while the the field is in focus
        if( v_hasFocus!=null && v_hasFocus==elementId )  return false;
        if( Object.isFunction(orderObj.orderFields[elementId])==true ) continue;

        el = $(elementId);
        if( (el.nodeName.toUpperCase()=='INPUT' && (el.type.toLowerCase()=='radio' || el.type.toLowerCase()=='checkbox') )==false ) {
            //Never do check while the the field is in focus
            //regardless of the order you are checking
            if( v_hasFocus!=null && v_hasFocus==elementId )  return false;
        }

        //Only do the validation if something changed
        if( orderObj.validateField(elementId,true)==false ) {
            return false;
        }
    }

    return true;
}

function buildOrderXML(calcFieldsOnly, getAllCarts) {
    //if getAllCarts is true we get the data for all carts
    //if it is is false, just get the order data for the current cart
    setOrderData();
    var doc = FTD_XML.createXmlDocument("order");
    var root = doc.documentElement;
    var attr = doc.createAttribute("guid");
    root.setAttributeNode(attr);

    var header = doc.createElement("header");
    root.appendChild(header);



    //Loop through header info
    var idx;
    //var ncNode = null;
    for( idx=0; idx<v_billingFields.length; idx++ ) {
        //Pull in header data that is configured in the database
        if( UTILS.isEmpty(v_billingFields[idx].orderXmlNode)==false ) {

            if(calcFieldsOnly==false || (calcFieldsOnly==true && v_billingFields[idx].calcXmlFlag==true) ) {
                header.appendChild(FTD_XML.createElementWithText(doc,v_billingFields[idx].orderXmlNode,UTILS.trim(getValue(v_billingFields[idx].elementId)+'')));
            }
        }
    }
    header.appendChild(FTD_XML.createElementWithText(doc,'order-created-by-id',v_repIdentity));
    header.appendChild(FTD_XML.createElementWithText(doc,'update-order-flag', (v_modifyOrderFromCom?'Y':'N') ));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-modify-order-flag', v_piModifyOrderFlag));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-partner-id', v_piPartnerId));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-modify-order-threshold', v_piModifyOrderThreshold));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-original-merc-total', v_originalMercTotal));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-original-pdb-price', v_originalPdbPrice));
    header.appendChild(FTD_XML.createElementWithText(doc,'pi-original-florist-id', v_originalFloristId));

    //Membership data
    var cobrandsEl = null;
    if( v_masterSourceCode.membershipDataRequired==true ) {
    		cobrandsEl = doc.createElement('co-brands');
    		header.appendChild(cobrandsEl);

        cbEl = doc.createElement('co-brand');
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'name','FNAME'));
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'data',UTILS.trim($('membershipFirstName').value)));
        cobrandsEl.appendChild(cbEl);

        cbEl = doc.createElement('co-brand');
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'name','LNAME'));
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'data',UTILS.trim($('membershipLastName').value)));
        cobrandsEl.appendChild(cbEl);

        cbEl = doc.createElement('co-brand');
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'name',UTILS.trim(v_masterSourceCode.partnerId)));
        cbEl.appendChild(FTD_XML.createElementWithText(doc,'data',UTILS.trim($('membershipId').value)));
        cobrandsEl.appendChild(cbEl);
    }

    //Newsletter flag - don't send if already opted in (checkbox won't be visible)
    if($('customerSubscribeCheckbox').style.display!='none') {
        header.appendChild(FTD_XML.createElementWithText(doc,'news-letter-flag',($('customerSubscribeCheckbox').checked?'on':'off')));
    }

    if( calcFieldsOnly!=true ) {
        var paymentType = FTD_DOM.getSelectedValue('paymentTypeCombo');
        var gcAddtlPaymentType = FTD_DOM.getSelectedValue('paymentGCBalanceCombo');
        if( paymentType=='C' || (paymentType=='G' && gcAddtlPaymentType!='NC' && UTILS.isEmpty(gcAddtlPaymentType)==false ) ) {
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-approval-amt',$('paymentCCAmount').innerHTML));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-number',$('paymentCCNumber').value));
            var ccType = FTD_DOM.getSelectedValue('paymentCCTypeCombo');
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-type',ccType));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-approval-code',$('paymentCCManualAuth').value));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-approval-verbage',$('paymentCCApprovalVerbage').value));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-approval-action-code',$('paymentCCApprovalActionCode').value));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-avs-result',$('paymentCCAVSResult').value));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-acq-data',$('paymentCCAcqData').value));
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-bypass-auth-flag',$('paymentCCBypassAuthFlag').value));
            if( ccType=='MS' ) {
                header.appendChild(doc.createElement("cc-exp-date"));
                header.appendChild(doc.createElement("csc-value"));
                header.appendChild(doc.createElement("csc-override-flag"));
            } else {
                header.appendChild(FTD_XML.createElementWithText(doc,'cc-exp-date',$('paymentCCExpMonthCombo').getValue()+'/'+$('paymentCCExpYear').getValue()));
                header.appendChild(FTD_XML.createElementWithText(doc,'csc-value',$('paymentCSC').value));
                header.appendChild(FTD_XML.createElementWithText(doc,'csc-override-flag',$('paymentCSCOverride').checked ? 'Y' : 'N'));
            }
            header.appendChild(FTD_XML.createElementWithText(doc,'csc-failure-count',""+v_csc_failed_count));
        }

        if( paymentType=='G' ) {
            var gcNodes = doc.createElement('gift-certificates');
            header.appendChild(gcNodes);
            var gcNode = doc.createElement('gift-certificate');
            gcNodes.appendChild(gcNode);
            gcNode.appendChild(FTD_XML.createElementWithText(doc,'amount',$('paymentGCAmount').innerHTML));
            gcNode.appendChild(FTD_XML.createElementWithText(doc,'number',$('paymentGCNumber').value));
        }

        if( paymentType=='N'  || (paymentType=='G' && gcAddtlPaymentType=='NC') ) {
            var ncNode = doc.createElement('no-charge');
            header.appendChild(ncNode);
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'amount',$('paymentNCAmount').innerHTML));
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'type',FTD_DOM.getSelectedValue('paymentNCType')));
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'reason',FTD_DOM.getSelectedValue('paymentNCReason')));
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'order-reference',$('paymentNCRefOrder').value));
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'approval-id',$('paymentNCAuthId').value));
            ncNode.appendChild(FTD_XML.createElementWithText(doc,'approval-password',$('paymentNCAuthPwd').value));
        }

        if( paymentType=='I' ) {
            header.appendChild(FTD_XML.createElementWithText(doc,'cc-type','IN'));
        }

        //Billing info
        for( idx=0; idx<v_billingInfoFields.length; idx++ ) {
            if( cobrandsEl==null ) {
                cobrandsEl = doc.createElement('co-brands');
                header.appendChild(cobrandsEl);
            }

            var cbEl = doc.createElement('co-brand');
            cbEl.appendChild(FTD_XML.createElementWithText(doc,'name',getValue(v_billingInfoFields[idx].elementId+'Name')));
            cbEl.appendChild(FTD_XML.createElementWithText(doc,'data',getValue(v_billingInfoFields[idx].elementId)+''));
            cobrandsEl.appendChild(cbEl);
        }
        //Defect#1051
        //Adding Wallet Indicator for master Pass Order
        header.appendChild(FTD_XML.createElementWithText(doc,'wallet-indicator', v_originalWalletIndicator));
        // Update Order info
        header.appendChild(FTD_XML.createElementWithText(doc, 'orig_order_guid', v_originalOrderGuid));

    }

    var items = doc.createElement("items");
    root.appendChild(items);

    var oIdx = 0;
    var oIterations = v_cartList.length;
    if (getAllCarts == false)
    {
      oIdx = v_currentCartIdx;
      oIterations = oIdx +1;
    }
    for( ; oIdx<oIterations; oIdx++ ) {
        var orderObject = v_cartList[oIdx];
        if( orderObject==null || orderObject.orderElement==null ) continue;

        var prodObject = orderObject.productObject;
        if( prodObject!=null ) {
            var item = doc.createElement("item");
            items.appendChild(item);
            item.appendChild(item.appendChild(FTD_XML.createElementWithText(doc,'cart-index',UTILS.toString(oIdx))));

            //Pull in detail data that is configured in the database
            for( idx=0; idx<v_orderFields.length; idx++ ) {
                if( UTILS.isEmpty(v_orderFields[idx].orderXmlNode)==false ) {
                    if(calcFieldsOnly==false || (calcFieldsOnly==true && v_orderFields[idx].calcXmlFlag==true) ) {
                    	if( v_orderFields[idx].elementId=="cardMessage" || v_orderFields[idx].elementId=="orderComments" ) { // or orderXmlNode = "card-message"
                    		item.appendChild(FTD_XML.createElementWithText(doc,v_orderFields[idx].orderXmlNode, encodeURIComponent(UTILS.trim(orderObject[v_orderFields[idx].elementId]+''))));
                    	}
                    	else {
                    		item.appendChild(FTD_XML.createElementWithText(doc,v_orderFields[idx].orderXmlNode, UTILS.trim(orderObject[v_orderFields[idx].elementId]+'')));
                    	}
                    }
                }
            }

            //Recipient data
            var locationType = FTD_DOM.getSelectedValue('recipientType');
            var locationObj = v_locationTypes[locationType];
            if( locationObj!=null ) {
                if( locationObj.business==true ) {
                    item.appendChild(FTD_XML.createElementWithText(doc,'ship-to-type-name',orderObject.recipientBusiness));
                }

                if( locationObj.room==true ) {
                    item.appendChild(FTD_XML.createElementWithText(doc,'recip-room',orderObject.recipientRoom));
                }

                if( locationObj.hours==true ) {
                	item.appendChild(FTD_XML.createElementWithText(doc,'recip-hours-start',$('timeOfService').value));
                    item.appendChild(FTD_XML.createElementWithText(doc,'recip-hours-end',$('timeOfServiceEnd').value));
                    //Added for Time for service check for update order
                	item.appendChild(FTD_XML.createElementWithText(doc,'time-of-service-flag',$('timeOfServiceFlag').value));

                }

            }

            //address verification data
            if (orderObject.avsAddresses != null && orderObject.avsAddresses.length > 0) {
            	//put a 'address_verification' element on the order XML.  The contents should be those
            	//from the first avsAddresses element on the order
            	var avsEl = doc.createElement("address-verification");
            	for (var i=0;i<orderObject.avsAddresses[0].childNodes.length;i++) {
            			avsEl.appendChild(orderObject.avsAddresses[0].childNodes[i].cloneNode(true));
            	}
            	item.appendChild(avsEl);

            	//set the recip-avs-performed node
            	if (orderObject.avsPerformed === true && orderObject.avDisabled === false) {
            		var avs_performed = FTD_XML.createElementWithText(doc,'recip-avs-performed','Y');
            		item.getElementsByTagName("address-verification")[0].replaceChild(avs_performed, item.getElementsByTagName("recip-avs-performed")[0]);
            	}
            	//set the recip-address-verification-override node
            	var avs_override;
            	if (orderObject.avCustomerInsisted) {
            		avs_override = FTD_XML.createElementWithText(doc,'recip-address-verification-override-flag','Y');
            	} else {
            		avs_override = FTD_XML.createElementWithText(doc,'recip-address-verification-override-flag','N');
            	}
            	item.getElementsByTagName("address-verification")[0].replaceChild(avs_override, item.getElementsByTagName("recip-address-verification-override-flag")[0]);
            }

            var strValue;
            if( orderObject.floristDelivered==true ) {
                //Product price
                if( orderObject.productOption=='productOptionStandard' ) {
                    strValue = prodObject.standardPrice;
                } else if( orderObject.productOption=='productOptionDeluxe' ) {
                    strValue = prodObject.deluxePrice;
                } else if( orderObject.productOption=='productOptionPremium' ) {
                    strValue = prodObject.premiumPrice;
                } else if( orderObject.productOption=='productOptionVariable' ) {
                    strValue = orderObject.productOptionVariablePrice;
                }
                else {
                    alert('Cannot determine product price option.  Order object says option is '+orderObject.productOption.type);
                }
                strValue = strValue.replace(/,/g,'');

                item.appendChild(FTD_XML.createElementWithText(doc,'product-price',strValue));
                item.appendChild(FTD_XML.createElementWithText(doc,'size-indicator',orderObject.sizeIndicator));

                item.appendChild(FTD_XML.createElementWithText(doc,'delivery-date',UTILS.formatServerDateStrToMMDDYYY(orderObject.productDeliveryDate)));

                var ddObj = v_deliveryDates[orderObject.productDeliveryDate];
                if( ddObj!=null && !Object.isString(ddObj) && ddObj.endDate!=null ) {
                    item.appendChild(FTD_XML.createElementWithText(doc,'second-delivery-date',UTILS.formatToMMDDYYYY(ddObj.endDate,true)));
                }

                //color
                item.appendChild(FTD_XML.createElementWithText(doc,'first-color-choice',orderObject.productOptionColor));
                item.appendChild(FTD_XML.createElementWithText(doc,'second-color-choice',orderObject.productOptionColor2));

                //Florist id
                item.appendChild(FTD_XML.createElementWithText(doc,'florist',orderObject.floristId));

            } else if( orderObject.carrierDelivered==true ) {
                //Product price
                
            	if( orderObject.productOption=='productOptionStandard' ) {
                    strValue = prodObject.standardPrice;
                } else if( orderObject.productOption=='productOptionDeluxe' ) {
                    strValue = prodObject.deluxePrice;
                } else if( orderObject.productOption=='productOptionPremium' ) {
                    strValue = prodObject.premiumPrice;
                } else if( orderObject.productOption=='productOptionVariable' ) {
                    strValue = orderObject.productOptionVariablePrice;
                }
                else {
                    alert('Cannot determine product price option.  Order object says option is '+orderObject.productOption.type);
                }
                strValue = strValue.replace(/,/g,'');

                item.appendChild(FTD_XML.createElementWithText(doc,'product-price',strValue));
                item.appendChild(FTD_XML.createElementWithText(doc,'size-indicator',orderObject.sizeIndicator));

                //shipping-method
                strValue = orderObject.carrierDeliverOption;
                var shipMethod = strValue.substring(strValue.length-2);
                item.appendChild(FTD_XML.createElementWithText(doc,'shipping-method',shipMethod));

                //Delivery Date
                strValue = orderObject.productDeliveryDate;
                strValue = UTILS.formatServerDateStrToMMDDYYY(strValue);
                item.appendChild(FTD_XML.createElementWithText(doc,'delivery-date',strValue));
            }
            else
            {
                item.appendChild(FTD_XML.createElementWithText(doc,'product-price',prodObject.standardPrice));
            }

            //Florist comments
            item.appendChild(FTD_XML.createElementWithText(doc,'special-instructions',orderObject.floristComments));

            //Add ons
            var addOnsNode = doc.createElement('add-ons');
            item.appendChild(addOnsNode);
            //Mylar
            var addOnNode;

            if( orderObject.addOnCheckbox1==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.addOnIdArray[1]));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity',orderObject.addOnQty1));
            }
            if( orderObject.addOnCheckbox2==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.addOnIdArray[2]));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity',orderObject.addOnQty2));
            }
            if( orderObject.addOnCheckbox3==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.addOnIdArray[3]));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity',orderObject.addOnQty3));
            }
            if( orderObject.addOnCheckbox4==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.addOnIdArray[4]));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity',orderObject.addOnQty4));
            }
            //Funeral
            if( orderObject.addOnFCheckbox==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id','F'));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity', '1'));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'text',orderObject.addOnFBannerText));
            }
            //Vase
            if( orderObject.productAddonVase!=null) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.productAddonVase));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity', '1'));
            }
            //Greeting cards
            if( orderObject.addOnCardCheckbox==true ) {
                addOnNode = doc.createElement('add-on');
                addOnsNode.appendChild(addOnNode);
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'id',orderObject.addOnCardSelect));
                addOnNode.appendChild(FTD_XML.createElementWithText(doc,'quantity','1'));
            }

            // Update Order info
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_external_order_number', $('updateOrderId').value));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_order_detail_id', v_originalOrderDetailId));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_delivery_date', v_originalDeliveryDate));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_recipient_id', v_originalRecipientId));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_ship_method', v_originalShipMethod));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_has_sdu', v_originalOrderHasSDU));
            item.appendChild(FTD_XML.createElementWithText(doc, 'orig_product_id', v_originalProductId));
        }
    }

    header.appendChild(FTD_XML.createElementWithText(doc,'order-count',CART_EVENTS.cartItemCount()+''));

    return doc;
}

function getValue(elementId) {
    var retval = null;

    var el = $(elementId);

    if( el!=null ) {
        if(el.nodeName.toUpperCase()=='DIV'||el.nodeName.toUpperCase()=='SPAN' || el.nodeName.toUpperCase()=='A') {
            retval = el.innerHTML;
        } else {
            try {
            retval = el.getValue();
            } catch (err) {
                alert('function oe.js/getValue failed on a '+el.nodeName+' element for '+el.id);
            }
        }
    }

    return retval;
}

function inFieldArray(fieldArray,elementId) {
    if( Object.isUndefined(fieldArray) ||
        fieldArray==null ||
        Object.isUndefined(elementId) ||
        elementId==null )
    {
        return false;
    }

    for( var idx=0; idx<fieldArray.length; idx++ ) {
        if( fieldArray[idx].elementId==elementId ) {
            return true;
        }
    }

    return false;
}

spellCheck = function(elementId,popup) {
    if( elementId!=null ) {
        var checkText = $(elementId).getValue();
        if( checkText!=null && checkText.length>0 ) {
            if( popup )
                SPELL_CHECK.check(elementId,{popup:popup+'',openFunction:'openSpellCheck',closeFunction:'closeSpellCheck',notifyFunction:'notifySpellCheck'});
            else
                SPELL_CHECK.check(elementId,{popup:popup+'',openFunction:'openSpellCheck',closeFunction:'closeSpellCheck',notifyFunction:'notifySpellCheck',successFunction:'spellCheckSuccess'});
        } else if( popup==true ) {
            alert('Nothing to spell check');
        }
    }
}

openSpellCheck = function() {
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('spellCheckDivId');
}

closeSpellCheck = function() {
    new Effect.SlideUp('spellCheckDivId');
    new Effect.SlideDown('rightDivId');
}

notifySpellCheck = function(elementId) {
    if( elementId!=null ) new Effect.Pulsate(elementId);
    notifyUser('Spell check errors!',false,true);
}

spellCheckSuccess = function(elementId) {
    //Do nothing, run silently
}

findStateById = function(stateId) {
    var retval = null;
    for( var idx=0; idx<v_states.length; idx++ ) {
        if( v_states[idx].comboValue==stateId ) {
            retval = v_states[idx];
        }
    }

    return retval;
}

setOrderData = function() {
    if( v_currentCartIdx>-1 && v_cartList[v_currentCartIdx]!=null  ) {
        v_cartList[v_currentCartIdx].set();
    }
}

resetCartTotals = function() {
    $('totalProduct').innerHTML = '';
    $('totalAddons').innerHTML = '';
    $('totalShippingFees').innerHTML = '';
    document.getElementById('total_same_day_fee').style.display = 'none';
    document.getElementById('total_same_day_fee_amount').style.display = 'none';
    document.getElementById('total_late_cutoff_amount').style.display = 'none';
    document.getElementById('total_monday_upcharge_amount').style.display = 'none';
    document.getElementById('total_sunday_upcharge_amount').style.display = 'none';
    document.getElementById('totalServiceFees').style.display = 'none';
    document.getElementById('total_service_fee_amount').style.display = 'none';
    $('total_tax1_name').innerHTML = 'Taxes';
    $('total_tax1_amount').innerHTML = '';
    document.getElementById('total_tax2_name').style.display = 'none';
    document.getElementById('total_tax2_amount').style.display = 'none';
    document.getElementById('total_tax3_name').style.display = 'none';
    document.getElementById('total_tax3_amount').style.display = 'none';
    document.getElementById('total_tax4_name').style.display = 'none';
    document.getElementById('total_tax4_amount').style.display = 'none';
    document.getElementById('total_tax5_name').style.display = 'none';
    document.getElementById('total_tax5_amount').style.display = 'none';
    $('total').innerHTML = '';
    $('totalDiscounts').innerHTML = '';
    $('totalPointMiles').innerHTML = '';
}

resetThumbnailDetail = function(itemIdx) {
    if( v_cartList[itemIdx]!=null ) {
        $('ci_productPriceDiscounted___'+itemIdx).innerHTML = '';
        $('ci_productPriceMilesPoints___'+itemIdx).innerHTML='';
        $('ci_addOns___'+itemIdx).innerHTML='';
        $('ci_same_day_fee___'+itemIdx).style.display = 'none';
        $('ci_same_day_fee_amount___'+itemIdx).style.display = 'none';
        $('ci_late_cutoff_fee_amount___'+itemIdx).style.display = 'none';
        $('ci_vendor_mon_upcharge_amount___'+itemIdx).style.display = 'none';
        $('ci_vendor_sun_upcharge_amount___'+itemIdx).style.display = 'none';
        $('ci_international_fee_amount___'+itemIdx).style.display = 'none';
        $('ci_serviceFees___'+itemIdx).style.display = 'none';
        $('ci_service_fee_amount___'+itemIdx).style.display = 'none';
        $('ci_shippingFees___'+itemIdx).innerHTML='';
        $('ci_tax1_name___'+itemIdx).innerHTML= 'Taxes';
        $('ci_tax1_amount___'+itemIdx).innerHTML='';
        $('ci_tax2_name___'+itemIdx).style.display = 'none';
        $('ci_tax2_amount___'+itemIdx).style.display = 'none';
        $('ci_tax3_name___'+itemIdx).style.display = 'none';
        $('ci_tax3_amount___'+itemIdx).style.display = 'none';
        $('ci_tax4_name___'+itemIdx).style.display = 'none';
        $('ci_tax4_amount___'+itemIdx).style.display = 'none';
        $('ci_tax5_name___'+itemIdx).style.display = 'none';
        $('ci_tax5_amount___'+itemIdx).style.display = 'none';
        $('ci_subTotal___'+itemIdx).innerHTML='';
    }
}

deliveryDateCalendarHandler = function(type, args, obj) {
    // Hide the calendar
    obj.hide();

    var dataField, dates, date;
    var formattedDate = '';

    // Obtain name of data field
    dataField = obj.id.substring(0, obj.id.indexOf("Cal"));

    // Obtain selected dates
    dates = obj.getSelectedDates();

    var testVal = dates[0];
    formattedDate = UTILS.formatToServerDate(dates[0]);

    for( var idx=0; idx<$('productDeliveryDate').options.length; idx++ ) {

        var ddObj = v_deliveryDates[$('productDeliveryDate').options[idx].value];
        if( Object.isUndefined(ddObj) || ddObj==null ) continue;

        if( formattedDate == ddObj.startDate ) {
            if( dataField=='productDeliveryDate' ) {
                changeProductDeliveryDate(formattedDate);
            } else if( dataField=='productSearchDeliveryDate' ) {
                FTD_DOM.selectOptionByValue($('productSearchDeliveryDate'),formattedDate);
            }
            return;
        }
    }

    //Got here because the selected date is not in the combo
    ddObj = new DELIVERY_DATE.OBJECT();
    ddObj.startDate=formattedDate;
    ddObj.endDate=testVal;
    ddObj.isDateRange=false;
    ddObj.displayString=UTILS.formatToDisplayDeliveryDate(testVal);
    ddObj.valueString=formattedDate;
    v_deliveryDates[formattedDate]=ddObj;

    populateDeliveryDateCombos();

    if( dataField=='productDeliveryDate' ) {
        changeProductDeliveryDate(ddObj.valueString);
        //setCheckAvailability(v_cartList[v_currentCartIdx],dataField);
        //if( canValidateAvailability()==true ) ORDER_AJAX.checkProductAvailability();
    } else if( dataField=='productSearchDeliveryDate' ) {
        FTD_DOM.selectOptionByValue($('productSearchDeliveryDate'),ddObj.valueString);
    }
}

function showValidationTab() {
	v_tabControl.tabs[v_validationTabIdx].li.style.display = '';
}

function hideValidationTab() {
    v_tabControl.tabs[v_validationTabIdx].li.style.display = 'none';
}

function changeProductDeliveryDate(newValue) {
    if( FTD_DOM.getSelectedValue('productDeliveryDate')!=newValue ) {
        v_cartList[v_currentCartIdx].productDeliveryDate=newValue;
        FTD_DOM.selectOptionByValue('productDeliveryDate',newValue);
    }

    FTD_DOM.selectOptionByValue('carrierDeliveryDateGR',newValue);
    FTD_DOM.selectOptionByValue('carrierDeliveryDate2F',newValue);
    FTD_DOM.selectOptionByValue('carrierDeliveryDateND',newValue);
    FTD_DOM.selectOptionByValue('carrierDeliveryDateSA',newValue);
    FTD_DOM.selectOptionByValue('carrierDeliveryDateSU',newValue);

    var selectedRadio=null;
    var selectedDeliverOption = v_cartList[v_currentCartIdx].carrierDeliverOption;
    if (selectedDeliverOption == 'carrierDeliveryOpt2F' && $('carrierDeliveryOpt2F').checked == true) {
        selectedRadio=$('carrierDeliveryOpt2F');
    } else if (selectedDeliverOption == 'carrierDeliveryOptND' && $('carrierDeliveryOptND').checked == true) {
        selectedRadio=$('carrierDeliveryOptND');
    } else if( FTD_DOM.getSelectedValue('carrierDeliveryDateGR')==newValue ) {
        selectedRadio=$('carrierDeliveryOptGR');
    } else if( FTD_DOM.getSelectedValue('carrierDeliveryDate2F')==newValue ) {
        selectedRadio=$('carrierDeliveryOpt2F');
    } else if( FTD_DOM.getSelectedValue('carrierDeliveryDateND')==newValue ) {
        selectedRadio=$('carrierDeliveryOptND');
    } else if( FTD_DOM.getSelectedValue('carrierDeliveryDateSA')==newValue ) {
        selectedRadio=$('carrierDeliveryOptSA');
    }else if( FTD_DOM.getSelectedValue('carrierDeliveryDateSU')==newValue ) {
        selectedRadio=$('carrierDeliveryOptSU');
    }
    else {
        return;
    }

    selectedRadio.checked=true;
    FTD_DOM.oneOrNoCheckboxGroup(selectedRadio);
    v_cartList[v_currentCartIdx].carrierDeliverOption=selectedRadio.id;
    ORDER_EVENTS.setCarrierDeliveryTabIndicies();
}

function createCartForSameCustomer() {
    $('confirmationDivId').style.display='none';
    $('rightDivId').style.display='block';
    v_mainAccordion.openByIndex(0);
    $('orderTabberId').tabber.tabShow(0);

    //The function resetform() resets the state combo so
    //save the value and reselect once function returns
    var customerState = $('customerStateCombo').getValue();
    resetForm();

    // Source code has already been validated
    v_verifiedSourceCode = true;

    FTD_DOM.selectOptionByValue($('customerStateCombo'),customerState);

    $('paymentGCNumber').value='';
    FTD_DOM.selectOptionByValue($('paymentGCBalanceCombo'),'');
    $('paymentCCApprovalVerbage').value='';
    $('paymentCCApprovalActionCode').value='';
    $('paymentCCAVSResult').value='';
    $('paymentCCAcqData').value='';
    $('paymentCCBypassAuthFlag').value='N';
    $('paymentCCNumber').value='';
    $('paymentCCManualAuth').value='';
    $('paymentCSCOverride').checked=false;
    $('paymentCSC').value='';
    $('paymentCSC').disabled=false;
    v_csc_failed_count = 0;
    v_customerPhoneChecked = true;
    resetCartTotals();
    v_masterSourceCode = new SOURCECODE.OBJECT();
    $('productSourceCode').value='';

    v_buyerHasFreeShipping = 'N';
    v_displayed_buyerHasFreeShipping = 'N';
    v_displayed_sourceCode_FreeShippingNotAllowed = 'N';
    v_displayed_updatedSourceCode_FreeShippingNotAllowed = 'N';
    v_displayed_noFreeShippingProducts = 'N';
    v_freeShippingSavings = 0;
    $('productDeliveryDate').disabled=true;
    $('productCalendarButton').disabled=true;
    $('productDeliveryMethodFlorist').disabled=true;
    $('productDeliveryMethodDropship').disabled=true;
    $('productDeliveryMethodFlorist').checked=false;
    $('productDeliveryMethodDropship').checked=false;
    $('recipientType').disabled=true;

    v_displayDeliveryDate=false;
    previousDeliveryMethod='';

    validateDNIS();
}

function createCartForNewCustomer() {
    $('confirmationDivId').style.display='none';
    $('rightDivId').style.display='block';
    v_mainAccordion.openByIndex(0);
    $('orderTabberId').tabber.tabShow(0);
    $('cartItemId').style.display = 'none';
    $('productSearchStopButton').style.display = 'none';
    $('productSearchDisabledButton').style.display = 'none';
    $('copyDivId').style.display='none';
    $('notifyDivId').style.display='none';
    $('psViewedDivId').style.display='none';
    $('psDetailDivId').style.display='none';
    $('psTopSellersDivId').style.display='none';
    $('productSearchDivId').style.display='none';
    $('textSearchDivId').style.display='none';
    FTD_DOM.selectOptionByValue($('paymentTypeCombo'),'C');
    BILLING_EVENTS.paymentTypeChanged();
    resetForm();
    $('customerBillingPhone').value='';
    $('customerBillingPhoneExt').value='';
    $('customerEveningPhone').value='';
    $('customerEveningPhoneExt').value='';
    $('customerFirstName').value='';
    $('customerLastName').value='';
    $('customerAddress').value='';
    $('customerZip').value='';
    FTD_DOM.selectOptionByValue($('customerCountryCombo'),'US');
    setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
    $('customerCity').value='';
    $('customerEmailAddress').value='';
    $('recipientEmailAddress').value='';
    $('customerSubscribeCheckbox').checked=false;
    $('membershipId').value='';
    $('membershipSkip').checked=false;
    $('membershipFirstName').value='';
    $('membershipLastName').value='';
    $('paymentFraud').checked=false;
    $('paymentGCNumber').value='';
    FTD_DOM.selectOptionByValue($('paymentGCBalanceCombo'),'');
    $('paymentCCApprovalVerbage').value='';
    $('paymentCCApprovalActionCode').value='';
    $('paymentCCAVSResult').value='';
    $('paymentCCAcqData').value='';
    $('paymentCCBypassAuthFlag').value='N';
    $('paymentCCNumber').value='';
    $('paymentCCManualAuth').value='';
    $('paymentCSCOverride').checked=false;
    $('paymentCSC').value='';
    $('paymentCSC').disabled=false;
    v_csc_failed_count = 0;
    FTD_DOM.selectOptionByValue($('paymentCCTypeCombo'),'');
    FTD_DOM.selectOptionByValue($('paymentCCExpMonthCombo'),'');
    FTD_DOM.selectOptionByValue($('paymentCCExpYear'),'');
    $('paymentCCManualAuth').value='';
    FTD_DOM.removeAllTableRows($('billingInfoTbody'));
    FTD_DOM.selectOptionByValue($('paymentNCType'),'');
    FTD_DOM.selectOptionByValue($('paymentNCReason'),'');
    $('paymentNCRefOrder').value='';
    $('paymentNCAuthId').value='';
    $('paymentNCAuthPwd').value='';
    $('customerSubscribeCheckbox').style.display='';
    $('customerSubscribeCheckboxLabel').style.display='';
    resetCartTotals();
    $('callDataDNIS').value = '';
    $('callDataDNIS').disabled=false;
    $('callDataDNISDesc').innerHTML='';
    $('callDataSourceCode').value='';
    $('callDataSourceCodeDesc').innerHTML='';
    v_masterSourceCode = new SOURCECODE.OBJECT();
    $('actionButtonExit').style.display='';
    $('actionButtonAbandon').disable=true;
    //Reset IOTW data
    $('productSourceCode').value='';
    $('productIOTWDescription').value='';
    v_lastMasterOrderNumber=null;
    v_callTimestamp = null;

    for( idx=0; idx<v_billingFields.length; idx++ ) {
        v_billingFields[idx].errorFlag=false;
        v_billingFields[idx].setFieldStyle();
    }

    v_buyerHasFreeShipping = 'N';
    v_sourceCodeAllowsFreeShipping = 'N';
    v_displayed_buyerHasFreeShipping = 'N';
    v_displayed_sourceCode_FreeShippingNotAllowed = 'N';
    v_displayed_updatedSourceCode_FreeShippingNotAllowed = 'N';
    v_displayed_noFreeShippingProducts = 'N';
    v_freeShippingSavings = 0;
    $('productDeliveryDate').disabled=true;
    $('productCalendarButton').disabled=true;
    $('productDeliveryMethodFlorist').disabled=true;
    $('productDeliveryMethodDropship').disabled=true;
    $('productDeliveryMethodFlorist').checked=false;
    $('productDeliveryMethodDropship').checked=false;
    $('recipientType').disabled=true;
    v_displayDeliveryDate=false;
    previousDeliveryMethod='';

    waitToSetFocus('callDataDNIS');
}

function setHasFocus(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null ) {
        v_hasFocus = el.id;
    }
}

function onElementFocus(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null ) {

        var fld = findFieldObject(el.id,v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null && fld.tabIndex!=null && fld.tabIndex!=fld.tabIndexInitial ) {
            el.tabIndex = fld.tabIndex;
        }
    }
}

function onElementBlur(event) {
    var el = FTD_DOM.whichElement(event);
    if( el!=null ) {
        var fld = findFieldObject(el.id,v_currentCartIdx);
        if( fld!=null && fld.tabIndexInitial!=null && fld.tabIndex!=null && fld.tabIndex!=fld.tabIndexInitial ) {
            el.tabIndex = fld.tabIndexInitial;
        }
    }
}

var v_focusTimer = new TIMER.Timer(500,'waitToSetFocusCallback()',null);
var v_focusTimerElementId = null;
var v_focusTimerCartIdx = -1;

function waitToSetFocus(elementId,cartIdx) {
    if( v_focusTimer.isRunning() ) {
        v_focusTimer.stopTimer();
    }

    v_focusTimerElementId = elementId;
    if( cartIdx==null || Object.isUndefined(cartIdx) )
        v_focusTimerCartIdx = v_currentCartIdx;
    else
        v_focusTimerCartIdx = cartIdx;

    v_focusTimer.startTimer();
}

function waitToSetFocusCallback() {
    if( v_focusTimer.isRunning() ) {
        v_focusTimer.stopTimer();
    }

    var el = $(v_focusTimerElementId);

    if( el && el!=null ) {
        try {
            if( v_focusTimerCartIdx>-1 && v_focusTimerCartIdx!=v_currentCartIdx )
                CART_EVENTS.changeSelectedItem(v_focusTimerCartIdx);
            var fldObj = findFieldObject(el.id,v_currentCartIdx);
            if( fldObj!=null ) {
                if( fldObj.accordionCtrlIdx!=null ) {
                    v_mainAccordion.openByIndex(fldObj.accordionCtrlIdx);
                }

                if( fldObj.tabCtrlIdx!=null ) {
                    $('orderTabberId').tabber.tabShow(fldObj.tabCtrlIdx);
                }

                if( el.id.substring(0,2)=='av' )
                    JOE_ORDER.openAddressVerificationPanel();
                else
                    JOE_ORDER.closeAddressVerificationPanel();

                el.focus();
            }

            v_focusTimerElementId = null;
            v_focusTimerCartIdx = v_currentCartIdx;

        } catch (err) {
            v_focusTimer.startTimer();
        }
    }
}

function findFieldObjectByAccessKey(accessKey,cartIdx) {
    var idx;
    for( idx=0; idx<v_billingFields.length; idx++ ) {
        if( v_billingFields[idx].accessKey == accessKey ) {
            return v_billingFields[idx];
        }
    }

    for( idx=0; idx<v_billingInfoFields.length; idx++ ) {
        if( v_billingInfoFields[idx].accessKey == accessKey ) {
            return v_billingInfoFields[idx];
        }
    }

    if( cartIdx>-1 ) {
        var orderFields = v_cartList[cartIdx].orderFields;
        if( orderFields && orderFields!=null ) {
            for( var i in orderFields ) {
                if( Object.isFunction(orderFields[i])==true ) continue;

                if( orderFields[i].accessKey==accessKey ) {
                    return orderFields[i];
                }
            }
        }
    }

    return null;
}

function closePopups() {
    if( $('rightDivId').visible()==false ) {
        if( $('productSearchDivId').visible()==true ) {
            PRODUCT_SEARCH.closeSearch();
            Event.stop(event);
        } else if( $('textSearchDivId').visible()==true ) {
            TEXT_SEARCH.closeSearch();
            Event.stop(event);
        } else if( $('spellCheckDivId').visible()==true ) {
            closeSpellCheck();
        }
    } else {
        if( $('addressVerificationDivId').visible()==true ) {
            ORDER_EVENTS.onRejectAVChanges(null);
        }
    }
}

function isCountryDomestic(countryId) {
    for( var idx=0; idx<v_countries.length; idx++) {
        if( v_countries[idx].comboValue==countryId ) {
            return v_countries[idx].domestic;
        }
    }

    return false;
}

function setPaymentTypeAmounts() {
    var paymentType = FTD_DOM.getSelectedValue('paymentTypeCombo');
    if( paymentType=='G' ) {

        if( UTILS.isEmpty($('paymentGCNumber').value)==true ) return;

        var dGCAmount = Math.round(parseFloat($('paymentGCAmount').innerHTML)*100);

        if( isNaN(dGCAmount)==false && dGCAmount>=0 ) {
            var dTotal = Math.round(parseFloat($('total').innerHTML)*100);

            if( isNaN(dTotal)==false && dTotal>0 ) {
                var gcAddtlPaymentType = FTD_DOM.getSelectedValue('paymentGCBalanceCombo');

                if( dGCAmount<dTotal ) {
                    var underAmt = (dTotal-dGCAmount)/100;
                    underAmt = UTILS.formatStringToPrice(underAmt+'');
                    if( gcAddtlPaymentType=='NC' ) {
                        $('paymentCCAmount').innerHTML='0.00';
                        $('paymentNCAmount').innerHTML=underAmt;
                    } else if( gcAddtlPaymentType=='CC' ) {
                        $('paymentCCAmount').innerHTML=underAmt;
                        $('paymentNCAmount').innerHTML='0.00';
                    } else {
                        if( $('paymentGCBalanceCombo').visible()==false ) {
                            $('paymentGCBalanceCombo').style.display='';
                            $('paymentGCBalanceComboLabel').style.display='';
                        }

                        notifyUser('The amount of the gift certificate is less than the shopping cart total.  You will need to collect an additional '+underAmt+' on a credit card or process as a no-charge',false,true);
                    }
                } else {
                    $('paymentGCBalanceCombo').style.display='none';
                    $('paymentGCBalanceComboLabel').style.display='none';
                    $('paymentCCAmount').innerHTML='0.00';
                    $('paymentNCAmount').innerHTML='0.00';
                }
            }
        }
    } else if( paymentType=='N' ) {
        $('paymentNCAmount').innerHTML=$('total').innerHTML;
    } else if( paymentType=='C' ) {
        $('paymentCCAmount').innerHTML=$('total').innerHTML;
    } else if( paymentType=='I' ) {
        $('paymentINAmount').innerHTML=$('total').innerHTML;
    }

 }

 function iotwDescriptionWrapper(strValue) {
    if( Object.isUndefined(strValue) || strValue==null || strValue.length==0 ) return '';

    return '<span style="font-weight:bold;color:#cc0033;">'+strValue+'</span>';
 }

function areIOTWItemsInCart() {
    var retval = false;

    for( var idx=0; idx<v_cartList.length; idx++ ) {
        if( v_cartList[idx]==null ) continue;

        if( v_cartList[idx].productObject.iotwObject.flag==true ) {
            var deliveryDate = $('productDeliveryDate').getValue();
            var ddIOTW = v_cartList[idx].productObject.iotwObject.deliveryDates['dd'+deliveryDate];
            if( UTILS.isEmpty(ddIOTW)==false ) return true;
        }
    }

    return false;
}

function areNonIOTWItemsInCart() {
    var retval = false;

    for( var idx=0; idx<v_cartList.length; idx++ ) {
        if( v_cartList[idx]==null ) continue;

        if( v_cartList[idx].productObject.iotwObject.flag==true ) {
            var deliveryDate = $('productDeliveryDate').getValue();
            var ddIOTW = v_cartList[idx].productObject.iotwObject.deliveryDates['dd'+deliveryDate];
            if( UTILS.isEmpty(ddIOTW)==true ) return true;
        }
    }

    return false;
}

function startupTimerCallback() {
    if( v_startupTimer!=null && v_startupTimer.isRunning() ) {
        v_startupTimer.stopTimer();
    }

    if( v_initDataFinished==true && v_elementConfigFinished==true  ) {

        $('businessDiv').style.display = 'block';
        resetForm();

        var updateOrderId = $('updateOrderId').value;
        if( UTILS.isEmpty(updateOrderId)==false ) {
            $('callDataDNIS').value = v_modify_order_dnis;
            v_modifyOrderFromCom = true;
            doNotDisplayThisField();
            validateDNIS();
            getUpdateOrderInfo(updateOrderId);
            validateSourceCode();
            populateOriginalCustomerInfo();

        } else {

            var strVal = $('callDataDNIS').value;
            if( UTILS.isEmpty(strVal)==false ) {
                validateDNIS();
            } else {
                $('callDataDNIS').disabled=false;
                $('actionButtonExit').style.display='';
                waitToSetFocus('callDataDNIS');
            }
        }

        setProcessing(false);

    } else {
        v_startupTimer.startTimer();
    }
}

function makeJSONArray(record) {

    var recordLength = record.length ? record.length : '1';
    var jsonRecords = new Array();
    for(var idx = 0; idx < recordLength; idx++) {
        if (recordLength == 1) {
            thisRecord = record;
        } else {
            thisRecord = record[idx];
        }
        jsonRecords[idx] = thisRecord;
    }

    return jsonRecords;

}

function getSynchronousResponse(url, postData) {
	//alert('postData : '+ postData);
	var resText = null;
	var xmlhttp=null;
    if (window.XMLHttpRequest) {
        try {
          netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
        } catch (e) {
          //alert("Permission UniversalBrowserRead denied.  Configure your user.js file to include this host.");
        }
        xmlhttp=new XMLHttpRequest();	                //Mozilla, Firefox, Safari, etc
    }else if (typeof ActiveXObject != "undefined"){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");    //IE
    }
        // to be ensure non-cached version of response
    url = url + "?AJAX_XML="+postData+"&rnd=" + Math.random();

    xmlhttp.open("POST", url, false);//false means synchronous
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlhttp.send();
    return xmlhttp;
}

function populateOriginalCustomerInfo() {


	$('cartDiv').style.height = '200px';
	$('original_order_info').style.display = 'block';

	var customerName = v_originalFirstName + " " + v_originalLastName;
	var externalOrderNumber = $('updateOrderId').value;
	var originalItems = v_originalProductId + ' - ' + v_originalProductName;
	var deldate = v_originalDeliveryDate;
	var milespoint = '(' + v_originalMilesPoints + " " + v_originalDiscountRewardType + ')';
	var tax = '$ ' + parseFloat(v_originalTaxAmount).toFixed(2);

	var addonAmount = parseFloat(v_originalAddonTotal).toFixed(2);

	var addondesc = '';

	var i;
	for (i = 0; i < v_originalAddonName.length; i++) {
		addondesc += v_originalAddonName[i] + '(' + v_originalAddonQty[i] + ')'
				+ "<br>";
	}

	$('uo_customer_name').innerHTML = customerName;
	$('uo_external_order_number').innerHTML = externalOrderNumber;

	if (v_originalDeliveryDateEnd == null || v_originalDeliveryDateEnd == '') {
		$('uo_delivery_date_text').innerHTML = "Requested Delivery Date: ";
		$('uo_delivery_date').innerHTML = deldate;
	} else {
		$('uo_delivery_date_text').innerHTML = "Requested Delivery Date Range: ";
		$('uo_delivery_date').innerHTML = deldate + " to "
				+ v_originalDeliveryDateEnd;
	}

	$('uo_item').innerHTML = originalItems;

	if (v_originalColor1Description != null
			&& v_originalColor1Description != '') {
		$('uo_color1_description_text').innerHTML = "1st Color Choice: ";
		$('uo_color1_description').innerHTML = v_originalColor1Description + "<br>";
	}

	if (v_originalColor2Description != null
			&& v_originalColor2Description != '') {
		$('uo_color2_description_text').innerHTML = "2nd Color Choice: ";
		$('uo_color2_description').innerHTML = v_originalColor2Description;
	}

	if (v_originalDiscountProductAmount != null
			&& v_originalDiscountProductAmount != '' && v_originalDiscountProductAmount != v_originalProductAmount) {
		$('uo_original_product_amount').innerHTML = parseFloat(v_originalProductAmount).toFixed(2);
		$('uo_original_discount_product_amount').innerHTML = parseFloat(v_originalDiscountProductAmount).toFixed(2);

	} else {
		 $('uo_original_product_amount').style.display='none';
		$('uo_original_discount_product_amount').innerHTML = parseFloat(v_originalProductAmount).toFixed(2);
	}

	if (v_originalMilesPoints != null && v_originalMilesPoints != '' && v_originalMilesPoints > 0) {
		$('uo_original_miles_point').innerHTML = milespoint;
	}

	if (v_originalShipMethodDescription != null
			&& v_originalShipMethodDescription != '') {
		$('uo_ship_method_text').innerHTML = "Ship Method: ";
		$('uo_ship_method').innerHTML = v_originalShipMethodDescription;
	}

	if (v_originalTaxAmount != null && v_originalTaxAmount != '' && v_originalTaxAmount > 0 ) {
		$('uo_tax_text').innerHTML = "Taxes: ";
		$('uo_tax').innerHTML = tax;
	}

	if (v_originalAddonTotal != null && v_originalAddonTotal != '' && v_originalAddonTotal > 0 ) {
		$('uo_addon_row').style.display='block';
		$('uo_addon_text').innerHTML = "Add-Ons: $ ";
		if (v_originalDiscountAddonAmount != null && v_originalDiscountAddonAmount != ''
			&& v_originalDiscountAddonAmount != v_originalAddonTotal) {
			$('uo_addon_amount').innerHTML = addonAmount;
			$('uo_addon_discount_amount').innerHTML = parseFloat(v_originalDiscountAddonAmount).toFixed(2);
		} else {
			$('uo_addon_amount').style.display='none';
			$('uo_addon_discount_amount').innerHTML = addonAmount;
		}
		$('uo_addon_desc').innerHTML = addondesc;
	}

	if(v_timeOfService != null){
		var html = $('timeOfService').innerHTML;
		$('timeOfService').innerHTML = html+' '+v_timeOfService;

	}

	if(v_deliveryLocation != null){
		$('uo_delivery_location').innerHTML = v_deliveryLocation;
	}




}

function doNotDisplayThisField() {
    $('addCart_Item_Button').style.visibility = "hidden";
    $('deleteCart_Item_Button').style.visibility = "hidden";
    $('paymentCSCOverride').checked = true;
    $('paymentCSC').disabled = true;
    $('confNewOrderSameCustButton').style.visibility = "hidden";
    $('confNewOrderButton').style.visibility = "hidden";
    $('confExitButton').style.visibility = "hidden";

}
