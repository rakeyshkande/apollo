// <![CDATA[
var CART_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("CART_EVENTS requires the Prototype JavaScript framework >= 1.5");
      
if(typeof FTD_DOM=='undefined')
      throw("CART_EVENTS requires FTD_DOM");

CART_EVENTS.onCartItemChanged = function(event) {
    var el = FTD_DOM.whichElement(event);
    
    while(el!=null && el.idx==undefined) {
        el = el.parentNode;
    }
    
    if( el!=null ) {
        CART_EVENTS.changeSelectedItem(el.idx);
        
        if( v_cartList[v_currentCartIdx].avNeedsResolution==true ) {
            JOE_ORDER.openAddressVerificationPanel();
        } else {
            setFocus($('productId'),el.idx);
        }
    }
}

CART_EVENTS.changeSelectedItem = function(newItemIndex) {
    if( v_currentCartIdx!=newItemIndex ) {
        //Set the current order data
        setOrderData();
        
        if( v_cartList.length>0 && v_currentCartIdx>-1 && v_cartList[v_currentCartIdx]!=null ) {
            $('cartItemId'+v_indexPrefix+v_currentCartIdx).style.backgroundColor='#6B79A5';
            $('cartItemId'+v_indexPrefix+v_currentCartIdx).style.color='#ffffff';
            $('ci_productPriceMilesPoints'+v_indexPrefix+v_currentCartIdx).style.color='#ffffff';
            $('ci_subTotal'+v_indexPrefix+v_currentCartIdx).style.color='#ffffff';
        }
        $('cartItemId'+v_indexPrefix+newItemIndex).style.backgroundColor='#dde';
        $('cartItemId'+v_indexPrefix+newItemIndex).style.color='#6B79A5';
        $('ci_productPriceMilesPoints'+v_indexPrefix+newItemIndex).style.color='#6B79A5';
        $('ci_subTotal'+v_indexPrefix+newItemIndex).style.color='#6B79A5';
        v_currentCartIdx = newItemIndex;
        
        if( v_currentCartIdx!=null ) {
        	
        	v_cartList[v_currentCartIdx].populateOrder();
        }
    }
}

CART_EVENTS.selectNewOrderType = function(event) {
    new Effect.SlideDown('copyDivId',{afterFinish:CART_EVENTS.selectCopyOrderButton});  
}

CART_EVENTS.selectCopyOrderButton = function(obj) {
    try {
        $('copyOrderButton').focus();
    } catch (err) {
        //ignore it
    }
}

CART_EVENTS.onCreateNewOrder = function(event) {
    if( $('copyDivId').visible() ) {
        new Effect.SlideUp('copyDivId');
    }
    var selectedRadio = FTD_DOM.getSelectedRadioButton('copyRadio');
    CART_EVENTS.createNewOrder(selectedRadio);
}

CART_EVENTS.securityCallback = function(transport) {
    var xmlStr = transport.responseText.replace(/\s+$/, "");
    var xmlDoc = new DOMParser().parseFromString(xmlStr, "text/xml");
    var root = xmlDoc.documentElement;
    
    var status = root.getAttribute('status');
    var message = root.getAttribute('message');
    if( status != 'Y' ) {
        v_okToProcess = false;
        ORDER_AJAX.recordCallTime();
        doMainMenuAction('');
    }
}

CART_EVENTS.createNewOrder = function(selectedRadio) {
    //XMLHttpRequest
    var securityRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'security.do' + getSecurityParams(true),FTD_AJAX_REQUEST_TYPE_POST,CART_EVENTS.securityCallback,false,false);
    securityRequest.send();

    if( $('copyDivId').visible() ) {
        new Effect.SlideUp('copyDivId');
    }
    var newOrderHtml = $('cartItemId').cloneNode(true);
    var cartListIndex = v_cartList.length;
    newOrderHtml.id='cartItemId'+v_indexPrefix+cartListIndex;
    newOrderHtml.idx=cartListIndex;
    newOrderHtml.productSourceCode = $('callDataSourceCode').getValue();
    CART_EVENTS.changeCartElementIds(newOrderHtml,cartListIndex);
    var newOrderObject = new ORDER.OBJECT(newOrderHtml,cartListIndex);
    
    if( CART_EVENTS.cartItemCount()>0 && v_currentCartIdx>-1 ) { 
        if( UTILS.isEmpty(selectedRadio)==false ) {
            if(selectedRadio.id=='copyRadioSameSame' || selectedRadio.id=='copyRadioSameDiff') {
                //copy product
                newOrderObject.productId = v_cartList[v_currentCartIdx].productId;
                newOrderObject.productObject = v_cartList[v_currentCartIdx].productObject.copy();
                newOrderObject.productSourceCode = v_cartList[v_currentCartIdx].productSourceCode;
                //Always set availability to false!
                newOrderObject.productGenerallyAvailable=false;
                newOrderObject.productOccasion = v_cartList[v_currentCartIdx].productOccasion;
                newOrderObject.productPrice = v_cartList[v_currentCartIdx].productPrice;
                newOrderObject.productDiscount = v_cartList[v_currentCartIdx].productDiscount;
                newOrderObject.productOption = v_cartList[v_currentCartIdx].productOption;
                newOrderObject.productOptionVariablePrice = v_cartList[v_currentCartIdx].productOptionVariablePrice;
                newOrderObject.productOptionSubCode = v_cartList[v_currentCartIdx].productOptionSubCode;
                newOrderObject.productOptionUpsell = v_cartList[v_currentCartIdx].productOptionUpsell;
                newOrderObject.productOptionColor = v_cartList[v_currentCartIdx].productOptionColor;
                newOrderObject.productOptionColor2 = v_cartList[v_currentCartIdx].productOptionColor2;
                newOrderObject.addOnCardCheckbox = v_cartList[v_currentCartIdx].addOnCardCheckbox;
                newOrderObject.addOnCardSelect = v_cartList[v_currentCartIdx].addOnCardSelect;
                newOrderObject.addOnCheckedArray = v_cartList[v_currentCartIdx].addOnCheckedArray.clone();
                newOrderObject.addOnQtyArray = v_cartList[v_currentCartIdx].addOnQtyArray.clone();
                newOrderObject.addOnIdArray = v_cartList[v_currentCartIdx].addOnIdArray.clone();
                newOrderObject.addOnMaxQtyArray = v_cartList[v_currentCartIdx].addOnMaxQtyArray.clone();
                newOrderObject.addOnDescriptionArray = v_cartList[v_currentCartIdx].addOnDescriptionArray.clone();
                newOrderObject.addOnPriceArray = v_cartList[v_currentCartIdx].addOnPriceArray.clone();
                newOrderObject.addOnFCheckbox = v_cartList[v_currentCartIdx].addOnFCheckbox;
                newOrderObject.addOnFBannerText = v_cartList[v_currentCartIdx].addOnFBannerText;
                newOrderObject.productAddonVase = v_cartList[v_currentCartIdx].productAddonVase;
                newOrderObject.addOnVaseArray = v_cartList[v_currentCartIdx].addOnVaseArray;
                newOrderObject.addOnVaseDeliveryTypeArray = v_cartList[v_currentCartIdx].addOnVaseDeliveryTypeArray;
                newOrderObject.productDeliveryMethod = v_cartList[v_currentCartIdx].productDeliveryMethod;
                newOrderObject.productDeliveryDate = v_cartList[v_currentCartIdx].productDeliveryDate;
                newOrderObject.deliveryDatesList = v_cartList[v_currentCartIdx].deliveryDatesList;
            }
            
            if(selectedRadio.id=='copyRadioSameSame' || selectedRadio.id=='copyRadioDiffSame') {
                //copy recipient
                newOrderObject.recipientPhone = v_cartList[v_currentCartIdx].recipientPhone;
                newOrderObject.recipientPhoneExt = v_cartList[v_currentCartIdx].recipientPhoneExt;
                newOrderObject.recipientPhoneChecked = v_cartList[v_currentCartIdx].recipientPhoneChecked;
                newOrderObject.recipientType = v_cartList[v_currentCartIdx].recipientType;
                newOrderObject.recipientBusiness = v_cartList[v_currentCartIdx].recipientBusiness;
                newOrderObject.recipientRoom = v_cartList[v_currentCartIdx].recipientRoom;
                newOrderObject.recipientFirstName = v_cartList[v_currentCartIdx].recipientFirstName;
                newOrderObject.recipientLastName = v_cartList[v_currentCartIdx].recipientLastName;
                newOrderObject.recipientAddress = v_cartList[v_currentCartIdx].recipientAddress;
                newOrderObject.recipientZip = v_cartList[v_currentCartIdx].recipientZip;
                newOrderObject.productZip = v_cartList[v_currentCartIdx].recipientZip;
                newOrderObject.recipientCity = v_cartList[v_currentCartIdx].recipientCity;
                newOrderObject.recipientState = v_cartList[v_currentCartIdx].recipientState;
                newOrderObject.recipientCountry = v_cartList[v_currentCartIdx].recipientCountry;               
                newOrderObject.recipientHoursFrom = v_cartList[v_currentCartIdx].recipientHoursFrom;
                newOrderObject.recipientHoursTo = v_cartList[v_currentCartIdx].recipientHoursTo;
                newOrderObject.recipientEmailAddress = v_cartList[v_currentCartIdx].recipientEmailAddress;
                newOrderObject.recipientOptIn = v_cartList[v_currentCartIdx].recipientOptIn;
                newOrderObject.avSuggestedAddress_0 = v_cartList[v_currentCartIdx].avSuggestedAddress_0;
                newOrderObject.avSuggestedCity_0 = v_cartList[v_currentCartIdx].avSuggestedCity_0;
                newOrderObject.avSuggestedState_0 = v_cartList[v_currentCartIdx].avSuggestedState_0;
                newOrderObject.avSuggestedZip_0 = v_cartList[v_currentCartIdx].avSuggestedZip_0;
                newOrderObject.avSuggestedAddress_1 = v_cartList[v_currentCartIdx].avSuggestedAddress_1;
                newOrderObject.avSuggestedCity_1 = v_cartList[v_currentCartIdx].avSuggestedCity_1;
                newOrderObject.avSuggestedState_1 = v_cartList[v_currentCartIdx].avSuggestedState_1;
                newOrderObject.avSuggestedZip_1 = v_cartList[v_currentCartIdx].avSuggestedZip_1;
                newOrderObject.avSuggestedAddress_2 = v_cartList[v_currentCartIdx].avSuggestedAddress_2;
                newOrderObject.avSuggestedCity_2 = v_cartList[v_currentCartIdx].avSuggestedCity_2;
                newOrderObject.avSuggestedState_2 = v_cartList[v_currentCartIdx].avSuggestedState_2;
                newOrderObject.avSuggestedZip_2 = v_cartList[v_currentCartIdx].avSuggestedZip_2;
                newOrderObject.avResultCode = v_cartList[v_currentCartIdx].avResultCode;
                newOrderObject.avNeedsResolution = v_cartList[v_currentCartIdx].avNeedsResolution;
                newOrderObject.avCustomerInsisted = v_cartList[v_currentCartIdx].avCustomerInsisted;
                newOrderObject.avsPerformed = v_cartList[v_currentCartIdx].avsPerformed;
            }
            
            if(selectedRadio.id!='copyRadioDiffDiff' ) {
                newOrderObject.productOccasion = v_cartList[v_currentCartIdx].productOccasion;
                newOrderObject.productDeliveryDate = v_cartList[v_currentCartIdx].productDeliveryDate;
                newOrderObject.productCountry = v_cartList[v_currentCartIdx].productCountry;
            }
            
            if(selectedRadio.id=='copyRadioSameSame') {
                newOrderObject.productAvailable=v_cartList[v_currentCartIdx].productAvailable;
                newOrderObject.languageId = v_cartList[v_currentCartIdx].languageId;
            }

	    if(selectedRadio.id=='copyRadioDiffDiff' || selectedRadio.id=='copyRadioSameDiff') {
                //copy recipient
		if (v_masterSourceCode.recpt_phone!=null && v_masterSourceCode.recpt_phone!='')
                	newOrderObject.recipientPhone = v_masterSourceCode.recpt_phone;
                if (v_masterSourceCode.recpt_phone_ext!=null && v_masterSourceCode.recpt_phone_ext!='')
			newOrderObject.recipientPhoneExt = v_masterSourceCode.recpt_phone_ext;
                if (v_masterSourceCode.recpt_location_type!=null && v_masterSourceCode.recpt_location_type!='')
			newOrderObject.recipientType = v_masterSourceCode.recpt_location_type;
		if (v_masterSourceCode.recpt_business_name!=null && v_masterSourceCode.recpt_business_name!='')
                	newOrderObject.recipientBusiness = v_masterSourceCode.recpt_business_name;
		if (v_masterSourceCode.recpt_location_detail!=null && v_masterSourceCode.recpt_location_detail!='')
                	newOrderObject.recipientRoom = v_masterSourceCode.recpt_location_detail;
		if (v_masterSourceCode.recpt_address!=null && v_masterSourceCode.recpt_address!='')
                	newOrderObject.recipientAddress = v_masterSourceCode.recpt_address;
		if (v_masterSourceCode.recpt_zip_code!=null && v_masterSourceCode.recpt_zip_code!='') {
                	newOrderObject.recipientZip = v_masterSourceCode.recpt_zip_code;
                	newOrderObject.productZip = v_masterSourceCode.recpt_zip_code;
		}
		if (v_masterSourceCode.recpt_city!=null && v_masterSourceCode.recpt_city!='')
                	newOrderObject.recipientCity = v_masterSourceCode.recpt_city;
		if (v_masterSourceCode.recpt_state_id!=null && v_masterSourceCode.recpt_state_id!='')
                	newOrderObject.recipientState = v_masterSourceCode.recpt_state_id;
		if (v_masterSourceCode.recpt_country_id!=null && v_masterSourceCode.recpt_country_id!='')
                	newOrderObject.recipientCountry = v_masterSourceCode.recpt_country_id;
            }

            if(selectedRadio.id=='copyRadioDiffDiff' || selectedRadio.id=='copyRadioDiffSame') {
                $('productDeliveryDate').disabled=true;
                $('productCalendarButton').disabled=true;
                $('productDeliveryMethodFlorist').disabled=true;
                $('productDeliveryMethodDropship').disabled=true;
                $('productDeliveryMethodFlorist').checked=false;
                $('productDeliveryMethodDropship').checked=false;
                $('recipientType').disabled=true;
                v_displayDeliveryDate=false;
                previousDeliveryMethod='';
                newOrderObject.addOnFCheckbox = false;
                newOrderObject.addOnFBannerText = '';
            }	
            
            //Never copy over card message data
        }
    }
    
    
    
    $('cartDiv').appendChild(newOrderHtml);
    Event.observe('cartItemId'+v_indexPrefix+cartListIndex,'click',CART_EVENTS.onCartItemChanged);
    Event.observe('addCartItemButton'+v_indexPrefix+cartListIndex,'click',CART_EVENTS.selectNewOrderType);
    Event.observe('deleteCartItemButton'+v_indexPrefix+cartListIndex,'click',CART_EVENTS.onRemoveOrder);
    newOrderHtml.style.display = 'block';
    
    v_cartList.push(newOrderObject);
    
    CART_EVENTS.changeSelectedItem(cartListIndex);
    resetCartTotals();
    CART_EVENTS.setOrdersInCart();
    v_cartList[cartListIndex].populateThumbNail();
    
     if( CART_EVENTS.cartItemCount()>1 && v_currentCartIdx>-1 && UTILS.isEmpty(selectedRadio)==false ) {
        if(selectedRadio.id=='copyRadioSameSame') {
            waitToSetFocus('cardMessage');
            v_lastAvailabilityCartValues = new Array();
        } else if(selectedRadio.id=='copyRadioSameDiff') {
            waitToSetFocus('productOccasion');
        } else if(selectedRadio.id=='copyRadioDiffSame') {
            waitToSetFocus('productId');
        } else if(selectedRadio.id=='copyRadioDiffDiff') {
            waitToSetFocus('productId');
        }
    }
    
    if( v_cartList[cartListIndex].productGenerallyAvailable==false ) {
    	
    	ORDER_AJAX.getProductDetail(v_cartList[cartListIndex].productId,cartListIndex,'ORDER',false,false);
    }
    
    toggleSurchargeExplanation();
}

CART_EVENTS.onCancelNewOrder = function(event) {
  if( $('copyDivId').visible() ) {
    new Effect.SlideUp('copyDivId');
  }
}

CART_EVENTS.onRemoveOrder = function(event) {
    var el = FTD_DOM.whichElement(event);
    CART_EVENTS.removeOrder(el.idx);
}

CART_EVENTS.removeOrder = function(targetIdx) {
    if( confirm("Do you really want to remove this order from the cart?") ) {
        var sourceEl = $('cartItemId'+v_indexPrefix+targetIdx);
        $('cartDiv').removeChild(sourceEl);
        v_cartList[targetIdx]=null;
        v_lastRecalcOrderValues[targetIdx]=new Array();
        if( CART_EVENTS.cartItemCount()==0 ) {
            CART_EVENTS.createNewOrder(event);
        } else {
            //loop backwards and try to find a valid cart item
            var selectedItem = -1;
            var idx;
            for( idx=targetIdx; idx>=0; idx-- ) {
              if( v_cartList[idx]!=null ) {
                selectedItem = idx;
                break;
              }
            }
            
            //If no item was found, loop forward
            if( selectedItem==-1 ) {
                for( idx=targetIdx+1; idx<v_cartList.length; idx++ ) {
                    if( v_cartList[idx]!=null ) {
                        selectedItem = idx;
                        break;
                    }
                }   
            }
            
            if( selectedItem>-1 ) {
                CART_EVENTS.changeSelectedItem(selectedItem);
                setFocus($('productId'),selectedItem);
            }
        }
        
        
        resetCartTotals();
        CART_EVENTS.setOrdersInCart();
        setMembershipRequired();
        toggleSurchargeExplanation();
    }
    
}

function toggleSurchargeExplanation(){
	var display_flag = false;
    for( var idx=0; idx<v_cartList.length; idx++ ) {        	
        if( v_cartList[idx]!=null ) {
        	var recipCountry = v_cartList[idx].recipientCountry;
        	var company_id = $('callDataCompany').value;
        	if(company_id != 'FTDCA' && recipCountry == 'CA'){
        		display_flag = true;
        		document.getElementById('surcharge_exp').style.display = 'block';
        		document.getElementById('footerDiv').style.top = '750px';
        		//alert('in tse ## company_id: '+company_id+' ## recipCountry: '+recipCountry+' ## display_flag: '+display_flag);
        	}
        }
    }
    if(!display_flag){
    	document.getElementById('surcharge_exp').style.display = 'none';
		document.getElementById('footerDiv').style.top = '720px';
    }
}

CART_EVENTS.changeCartElementIds = function(parent, idIndex) {
    //alert('parent: '+parent+' idIndex: '+idIndex+' hasNodes: '+parent.hasChildNodes());
    if( parent && idIndex!=null && parent.hasChildNodes() ) {
        var nodes = parent.childNodes;
        for (var i = 0; i < nodes.length; i++) {
            var tempId = nodes[i].id;
            if( tempId ) {
              var endIdx = tempId.lastIndexOf(v_indexPrefix);
              var newId;
              if( endIdx == -1 ) {
                newId = tempId+v_indexPrefix+idIndex;
              } else {
                newId = tempId.substring(0,endIdx)+v_indexPrefix+idIndex;
              }
              //alert('Changing '+tempId+' to '+newId);
              nodes[i].id = newId;
              nodes[i].idx = idIndex;
            }
            
            CART_EVENTS.changeCartElementIds(nodes[i],idIndex);
        }
    }
}

CART_EVENTS.setOrdersInCart = function() {
    $('cartOrdersInCart').innerHTML=CART_EVENTS.cartItemCount();
    $('cartOrdersInCart').style.backgroundColor='#eeeeee';
}

CART_EVENTS.cartItemCount = function() {
    var ctr = 0;
    
    for( var idx=0; idx<v_cartList.length; idx++ ) {
      if( v_cartList[idx]!=null ) {
        ctr++;
      }
    }
    
    return ctr;
}

// ]]>
