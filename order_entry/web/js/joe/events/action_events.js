// <![CDATA[
var ACTION_EVENTS = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("ACTION_EVENTS requires the Prototype JavaScript framework >= 1.5");

ACTION_EVENTS.onActionButtonAbandon = function(event) {
	if (v_modifyOrderFromCom) {
		// We originally came from COM to do a Modify/Update Order, so we must return to COM on abandon
	    if( confirm("Are you sure you want to abandon cart and cancel update of order?") ) {
	        v_checkAvailabilityTimer.stopTimer();
	        v_recalcOrderTimer.stopTimer();
            ORDER_AJAX.recordCallTime();
            ORDER_EVENTS.onDoComReturnToOriginal();
	    }
	} else {
		// Normal JOE abandon
	    if( confirm("Are you sure you want to abandon cart and cancel order?") ) {	        
	        v_checkAvailabilityTimer.stopTimer();
	        v_recalcOrderTimer.stopTimer();
	        
	        if( confirm('Would you like to place another order or return to the menu?\r\nClick "OK" to place another order or "Cancel" to return to the menu') ) {
	            ORDER_EVENTS.startNewOrder();
	        } else {
	            ORDER_AJAX.recordCallTime();
	            doMainMenuAction('customerService');
	        }
	    }
	}
}

ACTION_EVENTS.onActionButtonExit = function(event) {
    if( confirm("Are you sure you want to exit this page?") ) {
        doMainMenuAction('customerService');
    }
}

// ]]>