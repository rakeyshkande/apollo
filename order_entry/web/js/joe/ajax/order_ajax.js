// <![CDATA[
var ORDER_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || ORDER_AJAX.prototypeVersion < 1.6)
      throw("ORDER_AJAX requires the Prototype JavaScript framework >= 1.6");



ORDER_AJAX.getIntroData = function() {
    try {
        var doc = FTD_XML.createXmlDocument("request");
        var root = doc.documentElement;
        var attr = doc.createAttribute("type");
        attr.value = 'AJAX_REQUEST_GET_INTRO_DATA';
        root.setAttributeNode(attr);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.getIntroDataCallback,false,false);
        newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
        setProcessing(true);
        newRequest.send();
    } catch (err) {
        setProcessing(false);
        throw err;
    }
}

ORDER_AJAX.getIntroDataCallback = function(transport) {
    try {
        logMessage("intro data");
        var raw = transport.responseText;
        var content = eval("(" + raw + ")");
        var results = makeJSONArray(content.result.rs);
        var recordsLength = results.length;
        var occasionAddonsMap;
        var globalParmsMap;

        // Build preliminary objects
        for(var i = 0; i < recordsLength; i++){
            if(results[i].name == 'occasion_addons'){
                occasionAddonsMap = populatetOccasionAddonsMapJSON(results[i]);
            }
            else if(results[i].name == 'global_parms'){
                globalParmsMap = populatetGlobalParmsMapJSON(results[i]);
            }
        }

        setPaymentTypes();
        setGlobalParmsJSON(globalParmsMap);

        for(var i = 0; i < recordsLength; i++){
            if(results[i].name == 'delivery-dates'){
                setDeliveryDatesJSON(results[i]);
            } else if(results[i].name == 'state_master'){
                setStatesJSON(results[i]);
            } else if(results[i].name == 'country_master'){
                setCountriesJSON(results[i]);
            } else if(results[i].name == 'occasions'){
                setOccasionsJSON(results[i]);
            } else if(results[i].name == 'address_types'){
                setAddressTypesJSON(results[i]);
            } else if(results[i].name == 'add-ons'){
                setAddonsJSON(results[i], occasionAddonsMap);
            } else if(results[i].name == 'card_messages'){
                setGreetingsJSON(results[i]);
            } else if(results[i].name == 'payment_methods'){
                setPaymentMethodsJSON(results[i]);
            } else if(results[i].name == 'product_favorites'){
                setFavoritesJSON(results[i]);
            } else if(results[i].name == 'oe_script_master'){
                setScriptingJSON(results[i]);
            } else if(results[i].name == 'price_header'){
                setPriceHeaderJSON(results[i]);
            } else if(results[i].name == 'languages'){
				setLanguagesJSON(results[i]);
			}
        }

        setCountryFields('customerCountryCombo','customerStateCombo','customerZip','customerZipSearch',null);
        setCountryFields('tszCountry','tszState','tszStateLabel',null,null);
        setCountryFields('tsfCountry','tsfState','tsfStateLabel',null,null);
        setSurchargeExplanationJSON(content.result);

        v_initDataFinished=true;
        logMessage("intro data");

        /* Original Code
        var xmlDoc = parseResponse(transport);

        if( xmlDoc!=null ) {
            setDeliveryDates(xmlDoc);
            setStates(xmlDoc);
            setCountries(xmlDoc);
            setOccasions(xmlDoc);
            setAddressTypes(xmlDoc);
            setAddons(xmlDoc);
            setGreetings(xmlDoc);
            setPaymentTypes();
            setPaymentMethods(xmlDoc);
            setGlobalParms(xmlDoc);
            setFavorites(xmlDoc);
            setScripting(xmlDoc);

            v_initDataFinished=true;
        }
        */
    } catch (err) {
        v_initDataFinished=true;
        throw err;
    }
}

//Added for Sympathy Controls

ORDER_AJAX.getSympathyLocationCheck = function(leadTimeCheckFlag){
	 var doc = FTD_XML.createXmlDocument("request");
	    var root = doc.documentElement;
	    var attr = doc.createAttribute("type");
	    attr.value = 'AJAX_REQUEST_GET_SYMPATHY_LOCATION_CHECK';
	    root.setAttributeNode(attr);

	    var sourceCode = $('callDataSourceCode').getValue();
	    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
	    attr = doc.createAttribute("name");
	    attr.value = 'SOURCE_CODE';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

	    var companyId = $('callDataCompany').getValue();
	    param = FTD_XML.createElementWithText(doc,'param',companyId);
	    attr = doc.createAttribute("name");
	    attr.value = 'COMPANY_ID';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

	    var state = $('recipientState').getValue();
	    param = FTD_XML.createElementWithText(doc,'param',state);
	    attr = doc.createAttribute("name");
	    attr.value = 'STATE';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

	    var deliveryDate = $('productDeliveryDate').getValue();
	    param = FTD_XML.createElementWithText(doc,'param',deliveryDate);
	    attr = doc.createAttribute("name");
	    attr.value = 'DELIVERY_DATE';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

	    var deliveryTime = $('timeOfService').value;
	    param = FTD_XML.createElementWithText(doc,'param',deliveryTime);
	    attr = doc.createAttribute("name");
	    attr.value = 'DELIVERY_TIME';
	    param.setAttributeNode(attr);
	    root.appendChild(param);



	    if($('productDeliveryMethodFlorist').checked == true){
	    	  var deliveryMethod = $('productDeliveryMethodFlorist').value;
	    }
	    else{
	    	var deliveryMethod = $('productDeliveryMethodDropship').value;
	    }


	    param = FTD_XML.createElementWithText(doc,'param',deliveryMethod);
	    attr = doc.createAttribute("name");
	    attr.value = 'PRODUCT_DELIVERY_METHOD';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

	    var addressType = $('recipientType').getValue();
	    param = FTD_XML.createElementWithText(doc,'param',addressType);
	    attr = doc.createAttribute("name");
	    attr.value = 'ADDRESS_TYPE';
	    param.setAttributeNode(attr);
	    root.appendChild(param);


	    param = FTD_XML.createElementWithText(doc,'param',leadTimeCheckFlag);
	    attr = doc.createAttribute("name");
	    attr.value = 'LEAD_TIME_CHECK_FLAG';
	    param.setAttributeNode(attr);
	    root.appendChild(param);


	    param.appendChild(FTD_XML.createElementWithText(doc,'source_code',sourceCode));
	    param.appendChild(FTD_XML.createElementWithText(doc,'company_id',companyId));
	    param.appendChild(FTD_XML.createElementWithText(doc,'state',state));
	    param.appendChild(FTD_XML.createElementWithText(doc,'delivery_date',deliveryDate));
	    param.appendChild(FTD_XML.createElementWithText(doc,'product_delivery_method',deliveryMethod));
	    param.appendChild(FTD_XML.createElementWithText(doc,'address_type',addressType));
	    param.appendChild(FTD_XML.createElementWithText(doc,'lead_time_check_flag',leadTimeCheckFlag));
	    root.appendChild(param);

	    //XMLHttpRequest
	    var newRequest =
	        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.getSympathyLocationCheckCallback,false,false);
	    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
	    newRequest.send();
}

ORDER_AJAX.getSympathyLocationCheckCallback = function(transport) {
	var jsonRaw = transport.responseText;
	if(jsonRaw.length > 0){

		notifyUser(jsonRaw,false,true);
	}


}

ORDER_AJAX.getElementConfig = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_GET_ELEMENT_DATA';
    root.setAttributeNode(attr);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.getElementConfigCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

ORDER_AJAX.getElementConfigCallback = function(transport) {
    try {
        logMessage("element config");
        var jsonRaw = transport.responseText;
        var jsonContent = eval("(" + jsonRaw + ")");

        var results = makeJSONArray(jsonContent.result.rs.record);
        var recordsLength = results.length;
        v_billingFields = new Array();
        v_orderFields = new Array();
        v_searchFields = new Array();

        v_availabilityFields = new Array();
        v_recalcFields = new Array();
        v_avsFields = new Array();
        for(var i = 0; i < recordsLength; i++){

            if(results[i].type == 'HEADER'){
                setElementConfigJSON(results[i],v_billingFields);
            }
            else if(results[i].type == 'DETAIL'){
                setElementConfigJSON(results[i],v_orderFields);
            }
            else if(results[i].type == 'SEARCH'){
                setElementConfigJSON(results[i],v_searchFields);
            }
        }

        /* Original code
        xmlDoc = parseResponse(transport);
        if( xmlDoc!=null ) {
            records = XPath.selectNodes('/result/rs[@name="element_config"]/record[type="HEADER"]',xmlDoc);
            v_billingFields = new Array();
            v_availabilityFields = new Array();
            v_recalcFields = new Array();
            v_avsFields = new Array();
            setElementConfig(records,v_billingFields);

            records = XPath.selectNodes('/result/rs[@name="element_config"]/record[type="DETAIL"]',xmlDoc);
            v_orderFields = new Array();
            setElementConfig(records,v_orderFields);

            records = XPath.selectNodes('/result/rs[@name="element_config"]/record[type="SEARCH"]',xmlDoc);
            v_searchFields = new Array();
            setElementConfig(records,v_searchFields);

            //Create a focus element on the variable price field
            var el = $('productOptionVariablePrice');
            if( el!=null ) {
                new FTD_DOM.FocusElement(el.id, v_borderFocusColor, null, v_labelFocusColor);
            }
        }
        */
        //Create a focus element on the variable price field
        var el = $('productOptionVariablePrice');
        if( el!=null ) {
            new FTD_DOM.FocusElement(el.id, v_borderFocusColor, null, v_labelFocusColor);
        }
        v_elementConfigFinished=true;
        logMessage("element config");
    } catch (err) {
        v_elementConfigFinished=true;
        throw err;
    }
}

ORDER_AJAX.checkProductAvailability = function(itemidx,forced) {
    try {
    	$('recipientType').disabled = true;
        logTimePA = UTILS.formatDateToTimeString(new Date());
        logMessage("product availability " + logTimePA);
        //Set the current order data
        if( v_currentCartIdx==itemidx ) setOrderData();

        var orderObj = v_cartList[itemidx];
        if(orderObj==null) return;

        var doc = FTD_XML.createXmlDocument("request");
        var root = doc.documentElement;
        var attr = doc.createAttribute("type");
        attr.value = 'AJAX_REQUEST_VALIDATE_PRODUCT_AVAILABILITY';
        root.setAttributeNode(attr);

        var productId = orderObj.productId;
        var param = FTD_XML.createElementWithText(doc,'param',productId);
        attr = doc.createAttribute("name");
        attr.value = 'PRODUCT_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = FTD_XML.createElementWithText(doc,'param',orderObj.productZip);
        attr = doc.createAttribute("name");
        attr.value = 'ZIP_CODE';
        param.setAttributeNode(attr);
        root.appendChild(param);
        
        var state = $('recipientState').getValue();
        param = FTD_XML.createElementWithText(doc,'param',state);
	    attr = doc.createAttribute("name");
	    attr.value = 'STATE';
	    param.setAttributeNode(attr);
	    root.appendChild(param);

        param = FTD_XML.createElementWithText(doc,'param',orderObj.productDeliveryDate);
        attr = doc.createAttribute("name");
        attr.value = 'DELIVERY_DATE';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var ddObj = orderObj.deliveryDatesList[orderObj.productDeliveryDate];
        if( ddObj!=null && !Object.isString(ddObj) ) {
            if( ddObj.isDateRange==true ) {
                param = FTD_XML.createElementWithText(doc,'param',UTILS.formatToServerDate(ddObj.endDate));
                attr = doc.createAttribute("name");
                attr.value = 'DATE_RANGE_FLAG';
                param.setAttributeNode(attr);
                root.appendChild(param);

            }
        }

        param = FTD_XML.createElementWithText(doc,'param',orderObj.productCountry);
        attr = doc.createAttribute("name");
        attr.value = 'COUNTRY_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var sourceCode = orderObj.productSourceCode;
        if( sourceCode==null || sourceCode.length==0 ) {
            sourceCode = v_masterSourceCode.id;
            orderObj.productSourceCode=v_masterSourceCode.id;
            if( v_currentCartIdx==itemidx )
                $('productSourceCode').value=v_masterSourceCode.id;
        }

        //Never check product availability without a source code
        if( UTILS.isEmpty(sourceCode)==true ) return;
        param = FTD_XML.createElementWithText(doc,'param',sourceCode);
        attr = doc.createAttribute("name");
        attr.value = 'SOURCE_CODE';
        param.setAttributeNode(attr);
        root.appendChild(param);

        //We include the orderXML now to product accurate fee calculations
        var orderXml = buildOrderXML(true,false);

        param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(orderXml));
        attr = doc.createAttribute("name");
        attr.value = 'order_xml';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var addonString = '';
        for( var idx=0; idx<orderObj.addOnIdArray.length; idx++) {
        	if (orderObj.addOnCheckedArray[idx]) {
            	if (addonString.length > 0) {
            		addonString = addonString + ',';
            	}
            	addonString = addonString + orderObj.addOnIdArray[idx];
        	}
        }
        if( orderObj.productAddonVase!=null) {
        	if (addonString.length > 0) {
        		addonString = addonString + ',';
        	}
        	addonString = addonString + orderObj.productAddonVase;
        }
        param = FTD_XML.createElementWithText(doc,'param',addonString);
        attr = doc.createAttribute("name");
        attr.value = 'ADDONS';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = doc.createElement('echo');
        param.appendChild(FTD_XML.createElementWithText(doc,'product_id',productId));
        param.appendChild(FTD_XML.createElementWithText(doc,'country_id',orderObj.productCountry));
        param.appendChild(FTD_XML.createElementWithText(doc,'itemidx',itemidx+''));
        param.appendChild(FTD_XML.createElementWithText(doc,'forced',(forced==true?'true':'false')));
        root.appendChild(param);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.checkProductAvailabilityCallback,false,false);
        newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
        setProcessing(true);
        newRequest.send();
    } catch (err) {
        setProcessing(false);
        throw err;
    }
}

ORDER_AJAX.checkProductAvailabilityCallback = function(transport) {
    try {
        var xmlDoc = parseResponse(transport);

        if( xmlDoc!=null ) {


            var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
            var productId = FTD_XML.selectNodeText(echo,'product_id');
            var countryId = FTD_XML.selectNodeText(echo,'country_id');
            var itemIdx = FTD_XML.selectNodeText(echo,'itemidx');
            var forced = (FTD_XML.selectNodeText(echo,'forced')=='true'?true:false);

            //Shut off the focus timer so user will not be unexpectedly
            //redirected to a field based on an old request.
            if( v_focusTimer.isRunning() ) {
                v_focusTimer.stopTimer();
            }

            //1.  Check the status flag off the validation result set
            var record = XPath.selectNodes('/result/rs[@name="validation"]',xmlDoc)[0];
            var status = FTD_XML.getAttributeText(record,'status');

            v_cartList[itemIdx].deliveryDaysND = new Array();
            v_cartList[itemIdx].deliveryDays2F = new Array();
            v_cartList[itemIdx].deliveryDaysGR = new Array();
            v_cartList[itemIdx].deliveryDaysSA = new Array();
            v_cartList[itemIdx].deliveryDaysSU = new Array();



            if( status=='Y' ) {
                //2.  If status==y, then get the ship methods and carrier delivery dates
                record = XPath.selectNodes('/result/rs[@name="ship-methods"]/record',xmlDoc)[0];
                itemIdx = parseInt(itemIdx);
                v_cartList[itemIdx].floristDelivered=(FTD_XML.selectNodeText(record,'florist')=='Y');
                v_cartList[itemIdx].carrierNextDayAvailable=(FTD_XML.selectNodeText(record,'carrierND')=='Y');
                v_cartList[itemIdx].carrierStandardAvailable=(FTD_XML.selectNodeText(record,'carrierGR')=='Y');
                v_cartList[itemIdx].carrierTwoDayAvailable=(FTD_XML.selectNodeText(record,'carrier2F')=='Y');
                v_cartList[itemIdx].carrierSaturdayDayAvailable=(FTD_XML.selectNodeText(record,'carrierSA')=='Y');
                v_cartList[itemIdx].carrierSundayDayAvailable=(FTD_XML.selectNodeText(record,'carrierSU')=='Y');
                previousDeliveryMethod = '';
                if (v_cartList[itemIdx].productDeliveryMethod == 'productDeliveryMethodFlorist') {
                    previousDeliveryMethod = 'florist';
                } else if (v_cartList[itemIdx].productDeliveryMethod == 'productDeliveryMethodDropship') {
                    previousDeliveryMethod = 'dropship';
                }
                //alert('previous: ' + previousDeliveryMethod);
                v_cartList[itemIdx].deliveryMethodFloristEnabled=false;
                v_cartList[itemIdx].deliveryMethodDropshipEnabled=false;
                v_cartList[itemIdx].recipientTypeEnabled=false;

                if (itemIdx == v_currentCartIdx) {
                    $('productDeliveryMethodFlorist').disabled=true;
                    $('productDeliveryMethodDropship').disabled=true;

                    $('recipientType').disabled = true;
                 }

                if (v_cartList[itemIdx].floristDelivered==true) {
                    if (itemIdx == v_currentCartIdx) {
                        $('productDeliveryMethodFlorist').disabled=false;
                        $('recipientType').disabled = false;
                    }
                    v_cartList[itemIdx].deliveryMethodFloristEnabled=true;
                    v_cartList[itemIdx].recipientTypeEnabled=true;

                    if (previousDeliveryMethod=='') {
                        if (itemIdx == v_currentCartIdx) {
                            $('productDeliveryMethodFlorist').checked=true;

                            $('recipientType').disabled = false;
                        }
                        v_cartList[itemIdx].productDeliveryMethod = 'productDeliveryMethodFlorist';
                        ORDER_EVENTS.onProductDeliveryMethodChanged(null, itemIdx);
                        previousDeliveryMethod = 'florist';
                    }


                } else {
                    if (previousDeliveryMethod=='florist') {
                        if (itemIdx == v_currentCartIdx) {
                            $('productDeliveryMethodFlorist').checked=false;

                            $('recipientType').disabled = true;
                        }
                        previousDeliveryMethod='unavailable';
                    }
                }

                if( v_cartList[itemIdx].carrierNextDayAvailable==true ||
                    v_cartList[itemIdx].carrierStandardAvailable==true ||
                    v_cartList[itemIdx].carrierTwoDayAvailable==true ||
                    v_cartList[itemIdx].carrierSaturdayDayAvailable==true ||
                    v_cartList[itemIdx].carrierSundayDayAvailable==true)
                {
                    v_cartList[itemIdx].carrierDelivered=true;
                    if (itemIdx == v_currentCartIdx) {
                        $('productDeliveryMethodDropship').disabled=false;
                        $('recipientType').disabled = false;
                    }
                    v_cartList[itemIdx].deliveryMethodDropshipEnabled=true;
                    v_cartList[itemIdx].recipientTypeEnabled=true;

                    if (previousDeliveryMethod=='' || previousDeliveryMethod=='unavailable') {
                        if (itemIdx == v_currentCartIdx) {
                            $('productDeliveryMethodDropship').checked=true;

                            $('recipientType').disabled = false;
                        }
                        v_cartList[itemIdx].productDeliveryMethod = 'productDeliveryMethodDropship';
                        ORDER_EVENTS.onProductDeliveryMethodChanged(null, itemIdx);
                        previousDeliveryMethod = 'dropship';
                    } else if (previousDeliveryMethod=='dropship') {
                        v_cartList[itemIdx].floristDelivered=false;
                    }
                    record = XPath.selectNodes('/result/rs[@name="carrier_delivery_dates"]/row[@row="1"]',xmlDoc)[0];

                    var dates;
                    var year;
                    var month;
                    var day;
                    var ddObj;
                    var dateObj;
                    var idx
                    var dateString;
                    var newDates;
                    if( Object.isUndefined(record)==false ) {
                        //dates = FTD_XML.getChildElements(record.getElementsByTagName('next-day')[0]);
                        newDates = record.getElementsByTagName('next-day');
                        if( newDates.length>0 ) {
                          dates=FTD_XML.getChildElements(newDates[0]);
                            for( idx=0; idx<dates.length; idx++ ) {
                                ddObj = new DELIVERY_DATE.OBJECT();
                                dateString = dates[idx].getElementsByTagName("date")[0].firstChild.nodeValue;
                                dateCharge = dates[idx].getElementsByTagName("charge")[0].firstChild.nodeValue;

                                year = dateString.substring(6);
                                month = dateString.substring(0,2)-1;
                                day = dateString.substring(3,5);
                                dateObj = new Date(year,month,day);

                                ddObj.startDate=dateObj;
                                ddObj.endDate=dateObj;
                                ddObj.isDateRange=false;    //No date ranges for drop ship
                                ddObj.displayString=UTILS.formatToDisplayDeliveryDate(ddObj.startDate) + " - " + dateCharge;
                                ddObj.valueString=UTILS.formatToServerDate(ddObj.startDate);
                                v_cartList[itemIdx].deliveryDaysND['VS' + ddObj.valueString]=ddObj;
                            }
                        }
                        newDates = record.getElementsByTagName('ground');
                        if( newDates.length>0 ) {
                            dates=FTD_XML.getChildElements(newDates[0]);
                            for( idx=0; idx<dates.length; idx++ ) {
                                ddObj = new DELIVERY_DATE.OBJECT();
                                dateString = dates[idx].getElementsByTagName("date")[0].firstChild.nodeValue;
                                dateCharge = dates[idx].getElementsByTagName("charge")[0].firstChild.nodeValue;

                                year = dateString.substring(6);
                                month = dateString.substring(0,2)-1;
                                day = dateString.substring(3,5);
                                dateObj = new Date(year,month,day);

                                ddObj.startDate=dateObj;
                                ddObj.endDate=dateObj;
                                ddObj.isDateRange=false;    //No date ranges for drop ship
                                ddObj.displayString=UTILS.formatToDisplayDeliveryDate(ddObj.startDate) + " - " + dateCharge;
                                ddObj.valueString=UTILS.formatToServerDate(ddObj.startDate);
                                v_cartList[itemIdx].deliveryDaysGR['VS' + ddObj.valueString]=ddObj;
                            }
                        }

                        newDates = record.getElementsByTagName('two-day');
                        if( newDates.length>0 ) {
                            dates=FTD_XML.getChildElements(newDates[0]);
                            for( idx=0; idx<dates.length; idx++ ) {
                                ddObj = new DELIVERY_DATE.OBJECT();
                                dateString = dates[idx].getElementsByTagName("date")[0].firstChild.nodeValue;
                                dateCharge = dates[idx].getElementsByTagName("charge")[0].firstChild.nodeValue;

                                year = dateString.substring(6);
                                month = dateString.substring(0,2)-1;
                                day = dateString.substring(3,5);
                                dateObj = new Date(year,month,day);

                                ddObj.startDate=dateObj;
                                ddObj.endDate=dateObj;
                                ddObj.isDateRange=false;    //No date ranges for drop ship
                                ddObj.displayString=UTILS.formatToDisplayDeliveryDate(ddObj.startDate) + " - " + dateCharge;
                                ddObj.valueString=UTILS.formatToServerDate(ddObj.startDate);
                                v_cartList[itemIdx].deliveryDays2F['VS' + ddObj.valueString]=ddObj;
                            }
                        }

                        newDates = record.getElementsByTagName('saturday');
                        if( newDates.length>0 ) {
                            dates=FTD_XML.getChildElements(newDates[0]);
                            for( idx=0; idx<dates.length; idx++ ) {
                                ddObj = new DELIVERY_DATE.OBJECT();
                                dateString = dates[idx].getElementsByTagName("date")[0].firstChild.nodeValue;
                                dateCharge = dates[idx].getElementsByTagName("charge")[0].firstChild.nodeValue;

                                year = dateString.substring(6);
                                month = dateString.substring(0,2)-1;
                                day = dateString.substring(3,5);
                                dateObj = new Date(year,month,day);

                                ddObj.startDate=dateObj;
                                ddObj.endDate=dateObj;
                                ddObj.isDateRange=false;    //No date ranges for drop ship
                                ddObj.displayString=UTILS.formatToDisplayDeliveryDate(ddObj.startDate) + " - " + dateCharge;
                                ddObj.valueString=UTILS.formatToServerDate(ddObj.startDate);
                                v_cartList[itemIdx].deliveryDaysSA['VS' + ddObj.valueString]=ddObj;
                            }
                        }

                        newDates = record.getElementsByTagName('sunday');
                        if( newDates.length>0 ) {
                            dates=FTD_XML.getChildElements(newDates[0]);
                            for( idx=0; idx<dates.length; idx++ ) {
                                ddObj = new DELIVERY_DATE.OBJECT();
                                dateString = dates[idx].getElementsByTagName("date")[0].firstChild.nodeValue;
                                dateCharge = dates[idx].getElementsByTagName("charge")[0].firstChild.nodeValue;

                                year = dateString.substring(6);
                                month = dateString.substring(0,2)-1;
                                day = dateString.substring(3,5);
                                dateObj = new Date(year,month,day);

                                ddObj.startDate=dateObj;
                                ddObj.endDate=dateObj;
                                ddObj.isDateRange=false;    //No date ranges for drop ship
                                ddObj.displayString=UTILS.formatToDisplayDeliveryDate(ddObj.startDate) + " - " + dateCharge;
                                ddObj.valueString=UTILS.formatToServerDate(ddObj.startDate);
                                v_cartList[itemIdx].deliveryDaysSU['VS' + ddObj.valueString]=ddObj;
                            }
                        }
                    }

                    //Now check to see if the productDeliveryDate is in the specific shipping arrays
                    var dateValue = v_cartList[itemIdx].productDeliveryDate;
                    if( UTILS.isEmpty(dateValue)==false ) {
                        ddObj = v_cartList[itemIdx].deliveryDatesList[dateValue];
                        var testDDObj;
                        var newDDObj;

                        dateValue = 'VS' + dateValue;
                        if( v_cartList[itemIdx].carrierNextDayAvailable==true )
                        {
                            testDDObj = v_cartList[itemIdx].deliveryDaysND[dateValue];
                            if( testDDObj==null ) {
                                if( ddObj.isDateRange==false ) {
                                    v_cartList[itemIdx].deliveryDaysND[dateValue]=ddObj;
                                }
                            }
                        }

                        if( v_cartList[itemIdx].carrierStandardAvailable==true )
                        {
                            testDDObj = v_cartList[itemIdx].deliveryDaysGR[dateValue];
                            if( testDDObj==null ) {
                                if( ddObj.isDateRange==false ) {
                                    v_cartList[itemIdx].deliveryDaysGR[dateValue]=ddObj;
                                }
                            }
                        }

                        if( v_cartList[itemIdx].carrierTwoDayAvailable==true )
                        {
                            testDDObj = v_cartList[itemIdx].deliveryDays2F[dateValue];
                            if( testDDObj==null ) {
                                if( ddObj.isDateRange==false ) {
                                    v_cartList[itemIdx].deliveryDays2F[dateValue]=ddObj;
                                }
                            }
                        }

                        if( v_cartList[itemIdx].carrierSaturdayDayAvailable==true )
                        {
                            testDDObj = v_cartList[itemIdx].deliveryDaysSA[dateValue];
                            if( testDDObj==null ) {
                                if( ddObj.isDateRange==false ) {
                                    v_cartList[itemIdx].deliveryDaysSA[dateValue]=ddObj;
                                }
                            }
                        }

                        if( v_cartList[itemIdx].carrierSundayDayAvailable==true )
                        {
                            testDDObj = v_cartList[itemIdx].deliveryDaysSU[dateValue];
                            if( testDDObj==null ) {
                                if( ddObj.isDateRange==false ) {
                                    v_cartList[itemIdx].deliveryDaysSU[dateValue]=ddObj;
                                }
                            }
                        }

                    }

                    record = XPath.selectNodes('/result/rs[@name="product-availability"]',xmlDoc)[0];
                    status = FTD_XML.getAttributeText(record,'status');
                    if (status == 'N') {
                    	if(!v_deliveryMethodChanged)
                    		{
                    		 var message = FTD_XML.getAttributeText(record,'message');						
    						 notifyUser(message,false,true);
                    		}
                    	v_deliveryMethodChanged=false;
				    }

                } else {
                    v_cartList[itemIdx].carrierDelivered=false;
                    if (previousDeliveryMethod=='dropship') {
                        if (itemIdx == v_currentCartIdx) {
                            $('productDeliveryMethodDropship').checked=false;

                            $('recipientType').disabled = true;
                        }
                        if (v_cartList[itemIdx].floristDelivered==true) {
                            if (itemIdx == v_currentCartIdx) {
                                $('productDeliveryMethodFlorist').checked=true;

                                $('recipientType').disabled = false;
                            }
                            v_cartList[itemIdx].productDeliveryMethod = 'productDeliveryMethodFlorist';
                            ORDER_EVENTS.onProductDeliveryMethodChanged(null, itemIdx);
                            previousDeliveryMethod = 'florist';
                        }
                    }
                }

                JOE_ORDER.checkForRestrictedVendorDelivery(itemIdx,v_cartList[itemIdx].recipientType);

                v_cartList[itemIdx].productAvailable=true;


            } else {
                //3.  If no, then get the first validation record with and error and report the error message.
            	v_cartList[itemIdx].productAvailable=false;
                record = XPath.selectNodes('/result/rs[@name="validation"]/record/validate[@name="product_availability"][@status="N"]',xmlDoc)[0];
                var message = FTD_XML.getAttributeText(record,'message');
                if( UTILS.isEmpty(message)==true ) {
                    if( forced==true )
                        message='An error has occurred while trying to check product availability.\r\nDid you enter a valid product id, zip code, and delivery date?';
                    else
                        message='An error has occurred while trying to check product availability.';
                }
                notifyUser(message,false,true);

                //Hide all product options by setting all delivery flags to false
                v_cartList[itemIdx].floristDelivered=false;
                v_cartList[itemIdx].carrierDelivered=false;
                v_cartList[itemIdx].carrierNextDayAvailable=false;
                v_cartList[itemIdx].carrierStandardAvailable=false;
                v_cartList[itemIdx].carrierTwoDayAvailable=false;
                v_cartList[itemIdx].carrierSaturdayDayAvailable=false;

                $('addOnDiv').style.display='none';
                $('deliveryOptionsDiv').style.display='none';
                $('floristOptionsDiv').style.display='none';

                if( forced==false && itemIdx==v_currentCartIdx ) {
                    var badDeliveryDate = XPath.selectNodes('/result/rs[@name="validation"]/record/validate[@name="product_availability"][@reason="delivery-date"]',xmlDoc)[0];
                    if( Object.isUndefined(badDeliveryDate)==true ) {
                        if( isCountryDomestic(countryId) )
                            PRODUCT_SEARCH_AJAX.getCrossSellProduct(productId,itemIdx);
                        else
                            PRODUCT_SEARCH.openSearch();
                    }
                }

            }

            if( itemIdx==v_currentCartIdx ) {
                v_cartList[itemIdx].populateDOM();
                changeProductDeliveryDate(FTD_DOM.getSelectedValue('productDeliveryDate'));
            }

            if( forced==true ) {
                v_checkAvailabilityTimer.startTimer();
            }
        }
        logMessage("product availability " + logTimePA);

    } finally {
        setProcessing(false);
        //$('recipientType').disabled = false;
        //Calling Sympathy Location and Lead time check
        ORDER_EVENTS.onDeliveryLocationChange();

    }
}


ORDER_AJAX.getProductDetail = function(productId,itemIdx,target,resetAddons,forced) {
    try {
        logTimePD = UTILS.formatDateToTimeString(new Date());
        logMessage("product details " + logTimePD);
        if( UTILS.isEmpty(productId)==true ) return;

        if( Object.isUndefined(itemIdx) || itemIdx==null ) {
            itemIdx=-1;
        }

        var doc = FTD_XML.createXmlDocument("request");
        var root = doc.documentElement;
        var attr = doc.createAttribute("type");
        attr.value = 'AJAX_REQUEST_VALIDATE_PRODUCT';
        root.setAttributeNode(attr);

        var param = FTD_XML.createElementWithText(doc,'param',productId);
        attr = doc.createAttribute("name");
        attr.value = 'PRODUCT_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var sourceCode;
        if( target=='ORDER' ) {
            sourceCode = v_cartList[itemIdx].productSourceCode;
            if( UTILS.isEmpty(sourceCode)==true ) {
                sourceCode = v_masterSourceCode.id;
                if( UTILS.isEmpty(sourceCode)==true ) {
                    sourceCode = $('callDataSourceCode').getValue();
                } else {
                    $('productSourceCode').value=v_masterSourceCode.id;
                }
                v_cartList[itemIdx].productSourceCode=sourceCode;
            }
        } else {
            sourceCode = $('callDataSourceCode').getValue();
        }

        param = FTD_XML.createElementWithText(doc,'param',sourceCode);
        attr = doc.createAttribute("name");
        attr.value = 'SOURCE_CODE';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var countryId = $('productCountry').getValue();
        param = FTD_XML.createElementWithText(doc,'param',countryId);
        attr = doc.createAttribute("name");
        attr.value = 'COUNTRY_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = doc.createElement('echo');
        param.appendChild(FTD_XML.createElementWithText(doc,'itemIdx',itemIdx+''));
        param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
        param.appendChild(FTD_XML.createElementWithText(doc,'product_id',productId));
        param.appendChild(FTD_XML.createElementWithText(doc,'country_id',countryId));
        param.appendChild(FTD_XML.createElementWithText(doc,'reset_addons',(resetAddons==false?"false":"true")));
        param.appendChild(FTD_XML.createElementWithText(doc,'forced',(forced==true?'true':'false')));
        root.appendChild(param);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.getProductDetailCallback,false,false);
        newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
        setProcessing(true);
        newRequest.send();
    } catch (err) {
        setProcessing(false);
        throw err;
    }
}

ORDER_AJAX.getProductDetailCallback = function(transport) {
    try {
        var xmlDoc = parseResponse(transport);

        if( xmlDoc!=null ) {
            var record = XPath.selectNodes('/result/rs[@name="product_details"]/record',xmlDoc)[0];
            var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
            var itemIdx = FTD_XML.selectNodeText(echo,'itemIdx');
                //itemIdx = parseInt(itemIdx);
            var target = FTD_XML.selectNodeText(echo,'target');
            var productId = FTD_XML.selectNodeText(echo,'product_id');
            var countryId = FTD_XML.selectNodeText(echo,'country_id');
            var resetAddons = (FTD_XML.selectNodeText(echo,'reset_addons')=='true'?true:false);
            var forced = (FTD_XML.selectNodeText(echo,'forced')=='true'?true:false);
            var prodObj;
            var getDetails = true;
            var getCrossSell = false;
            var rsStatus = checkResultSetStatus(xmlDoc,"product_details",(target=='FAVORITES'?true:false),false,false);

            if( target=='ORDER' ) var orderObj = v_cartList[itemIdx];

            if( Object.isUndefined(record)==true ) {
                getDetails = false;
            } else if( rsStatus==false ) {
                getDetails = false;
            }

            if( getDetails==true ) {
                if( target=='ORDER' ) {
                    if( orderObj != null ) {
                        v_addOnMsgDisplayed = false;
                        resetCartTotals();
                        var sameProduct = false;
                        var secondChoiceWarned;
                        var currentProductOption;
                        var currentDeliverOption;
                        if( orderObj.productId==productId ) {
                            secondChoiceWarned=orderObj.productObject.secondChoiceWarned;
                            currentProductOption=orderObj.productOption;
                            currentDeliverOption=orderObj.carrierDeliverOption;
                            sameProduct = true;
                        }
                        orderObj.resetProduct(resetAddons);
                        prodObj=populateProductObjectFromDetail(xmlDoc,productId,target);
                        if( sameProduct==true ) {
                            prodObj.secondChoiceWarned=secondChoiceWarned;
                            orderObj.carrierDeliverOption=currentProductOption;
                            orderObj.carrierDeliverOption=currentDeliverOption;
                        }
                        if( prodObj.iotwObject.flag==true ) {
                            orderObj.productSourceCode = prodObj.iotwObject.iotwSourceCode;
                        } else {
                            orderObj.productSourceCode = v_masterSourceCode.id;
                        }

                        orderObj.productObject = prodObj;
                        orderObj.productId = prodObj.productId;
                        if( /*prodObj.subcodeArray.length==0 &&*/ orderObj.productOption==null && prodObj.standardPrice!=null ) {
                            orderObj.productOption = 'productOptionStandard';
                        }

                        var strMsg = '';
                        if( itemIdx==v_currentCartIdx ) {
                            orderObj.populateDOM();
                            changeProductDeliveryDate(FTD_DOM.getSelectedValue('productDeliveryDate'));

                            if( v_iotwMembershipWarning==false &&
                                ((prodObj.iotwObject.flag==true && areNonIOTWItemsInCart()==true) ||
                                (prodObj.iotwObject.flag==false && areIOTWItemsInCart()==true)) ) {
                                strMsg += "Other discounts or miles/points promotions do not apply to item of the week products.<br><br>";
                                v_iotwMembershipWarning = true;
                            }
                        } else {
                            orderObj.setAddons();
                            orderObj.populateThumbNail();
                        }

                        if( UTILS.isEmpty(prodObj.secondChoice)==false && prodObj.secondChoiceWarned==false ) {
                            strMsg += 'This product has a second choice:<br><br>Should this item not be available, the florist will deliver \"'+FTD_DOM.unconvertHtmlSpecialChars(prodObj.secondChoice)+'\"<br><br>';
                            prodObj.secondChoiceWarned=true;
                        }

                        //If this is the productId on a subcode or the upsellmaster id,
                        //then show the product options and inform the user that they
                        //must select the product options before availability can be
                        //checked
                        if( prodObj.masterProductId==prodObj.productId ) {
                            var doIt = false;
                            if( prodObj.upsellArray.length>0 ) {
                                strMsg += 'The product id is an upsell master sku.  You must select a specific product option before availability can be checked.<br><br>';
                            } else if( prodObj.subcodeArray.length>0 ) {
                                strMsg += 'The product id is a subcode master sku.  You must select a specific product option before availability can be checked.<br><br>';
                            }
                        }

                        if( strMsg.length>0 ) {
                            if (v_addOnMsgDisplayed == true) {
                                notifyUser(strMsg,true,true);
                            } else {
                                notifyUser(strMsg,false,true);
                            }
                            v_addOnMsgDisplayed = false;
                        }

                        var fld = findFieldObject('productId',itemIdx);
                        if( fld!=null ) {
                            fld.errorFlag = false;
                            fld.setFieldStyle();
                        }

                        PRODUCT_SEARCH.addProductToRecentlyViewed(prodObj);
                        if( forced ) {
                            orderObj.forceAvailabilityCheck=true;
                        }
                        orderObj.productGenerallyAvailable=true;

                        if (prodObj.floristProduct == true) {
                            if (v_previousDeliveryType != 'florist') {
                                v_previousDeliveryType = 'florist';
                                populateDeliveryDateCombos();
                                orderObj.setOrderCombo('productDeliveryDate');
                            }
                        } else {
                            if (v_previousDeliveryType != 'vendor') {
                                var ddObj = v_deliveryDates[FTD_DOM.getSelectedValue($('productDeliveryDate'))];
                                if (ddObj != null && ddObj.isDateRange) {
                                    //Clear selected delivery date if switching from floral to vendor
                                    //and previous delivery date was in a date range
                                    changeProductDeliveryDate(null);
                                }
                                v_previousDeliveryType = 'vendor';
                                populateDeliveryDateCombos();
                                orderObj.setOrderCombo('productDeliveryDate');
                            }
                        }

                    }
                } else if( target=='FAVORITES' ) {
                    PRODUCT_SEARCH.favoriteProducts[itemIdx] = populateProductObjectFromDetail(xmlDoc,productId,target);
                }
            } else {
                if( target=='ORDER' ) {
                    //Is the product is marked unavailable in PDB or can't be
                    //delivered to the indicated country
                    var productStatus = XPath.selectNodes('/result/rs[@name="product_details" and @product_status="U"]',xmlDoc)[0];
                    if( Object.isUndefined(productStatus)==false ) {
                        if( orderObj != null ) orderObj.productGenerallyAvailable=false;

                        if( itemIdx==v_currentCartIdx ) {
                            if( isCountryDomestic(countryId)==true )
                                PRODUCT_SEARCH_AJAX.getCrossSellProduct(productId,itemIdx);
                            else
                                PRODUCT_SEARCH.openSearch();
                        }


                    } else {
                        fld = findFieldObject('productId',itemIdx);
                        if( fld!=null ) {
                            fld.errorFlag = true;
                            if( itemIdx==v_currentCartIdx ) {
                                fld.setFieldStyle();
                                //waitToSetFocus('productId');
                            }
                        }

                        var countryFailed = XPath.selectNodes('/result/rs[@name="product_details" and @country_failed="Y"]',xmlDoc)[0];
                        if( Object.isUndefined(countryFailed)==false && orderObj != null )
                            orderObj.productGenerallyAvailable=false;
                    }

                    if( orderObj != null && itemIdx==v_currentCartIdx ) {
                        orderObj.populateDOM();
                    }
                } else if( target=='FAVORITES' ) {
                    PRODUCT_SEARCH.favoriteProducts[itemIdx]=null;
                }
            }

            setMembershipRequired();
        }
        logMessage("product details " + logTimePD);
    } catch (err) {
        setProcessing(false);
        throw err;
    }
    setProcessing(false);
}

ORDER_AJAX.calculateOrderTotals = function(synchronousMode) {

	logTimeCOT = UTILS.formatDateToTimeString(new Date());
    logMessage("calculate totals " + logTimeCOT);

    if(v_cartList.length==0) {
        return;
    }
    //Set the current order data
    setOrderData();

    var orderXml = buildOrderXML(true, true);

    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_CALCULATE_ORDER_TOTALS';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(orderXml));
    attr = doc.createAttribute("name");
    attr.value = 'order_xml';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.calculateOrderTotalsCallback,synchronousMode,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

ORDER_AJAX.calculateOrderTotalsCallback = function(transport) {

	var xmlDoc = parseResponseQuietly(transport);

    if( xmlDoc!=null ) {
        var rs = XPath.selectNodes('/result/rs',xmlDoc)[0];
        var status = FTD_XML.getAttributeText(rs,'status');
        if( status=='Y' ) {

        	populateCalculateOrderTotals(xmlDoc,'/result/rs/order');
        }
    }
    if( v_recalcOrderTimer.isRunning()==false ) v_recalcOrderTimer.startTimer();
    logMessage("calculate totals " + logTimeCOT);
}

function populateCalculateOrderTotals(xmlDoc,path){

	//alert("populating order totals");
	if(xmlDoc!=null){


    var record = XPath.selectNodes(path,xmlDoc)[0];
    $('cartOrdersInCart').innerHTML=FTD_XML.selectNodeText(record,'total-items');
    $('totalProduct').innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(record,'total-product-price'));
    $('totalAddons').innerHTML=FTD_XML.selectNodeText(record,'total-addons');
    $('totalDiscounts').innerHTML=FTD_XML.selectNodeText(record,'total-discounts');
    $('totalPointMiles').innerHTML=FTD_XML.selectNodeText(record,'total-points');
    $('totalShippingFees').innerHTML=FTD_XML.selectNodeText(record,'total-shipping-fee');
    $('applySurchargeCode').value=FTD_XML.selectNodeText(record,'apply-surcharge-code');
    $('totalSurchargeFee').value=FTD_XML.selectNodeText(record,'total-surcharge-fee');
    $('surchargeDescription').value=FTD_XML.selectNodeText(record,'surcharge-description');
    $('totalServiceFees').innerHTML=FTD_XML.selectNodeText(record,'total-service-fee');
    $('total_service_fee_amount').innerHTML=FTD_XML.selectNodeText(record,'total-service-fee-mo');

    if(parseFloat($('totalShippingFees').innerHTML) == 0 || parseFloat($('totalSurchargeFee').value) == 0){
    	$('applySurchargeCode').value='OFF';
    }
    var totalSameDayFee = FTD_XML.selectNodeText(record,'total-same-day-fee');
    var tsdfn = 'total_same_day_fee';
	var tsdfa = 'total_same_day_fee_amount';

	var totalLateCutoff = FTD_XML.selectNodeText(record,'total-late-cutoff');
    var tlcfa = 'total_late_cutoff_amount';

	var totalMondayUpcharge = FTD_XML.selectNodeText(record,'total-monday-upcharge');
   	var tmuca = 'total_monday_upcharge_amount';

	var totalSundayUpcharge = FTD_XML.selectNodeText(record,'total-sunday-upcharge');
    var tsuca = 'total_sunday_upcharge_amount';
    
    var totalInternationalFee = FTD_XML.selectNodeText(record,'total-international-fee');
    var tifa = 'total_international_fee_amount';
    
    var totalServiceFee = FTD_XML.selectNodeText(record,'total-service-fee');
    var tsfa = 'totalServiceFees';

    if(totalSameDayFee != null && totalSameDayFee != ""){
    	document.getElementById(tsdfn).style.display = 'block';
		document.getElementById(tsdfa).style.display = 'block';
		$(tsdfa).innerHTML = totalSameDayFee;
    }
    else{
    	document.getElementById(tsdfn).style.display = 'none';
		document.getElementById(tsdfa).style.display = 'none';
    }

    if(totalLateCutoff != null && totalLateCutoff != ""){
    	document.getElementById(tlcfa).style.display = 'none';
		$(tlcfa).innerHTML = totalLateCutoff;
    }

    if(totalMondayUpcharge != null && totalMondayUpcharge != ""){
    	document.getElementById(tmuca).style.display = 'none';
		$(tmuca).innerHTML = totalMondayUpcharge;
    }

    if(totalSundayUpcharge != null && totalSundayUpcharge != ""){
    	document.getElementById(tsuca).style.display = 'none';
		$(tsuca).innerHTML = totalSundayUpcharge;
    }
    if(totalInternationalFee != null && totalInternationalFee != ""){
    	document.getElementById(tifa).style.display = 'none';
		$(tifa).innerHTML = totalInternationalFee;
    }
    
    if(totalServiceFee != null && totalServiceFee != ""){
    	document.getElementById(tsfa).style.display = 'block';
		$(tsfa).innerHTML = totalServiceFee;
    }
    else{
    	document.getElementById(tsfa).style.display = 'block';
    }
    

    var totalSameDayFeeMO = FTD_XML.selectNodeText(record,'total-same-day-fee-mo');
    if(totalSameDayFeeMO != null && totalSameDayFeeMO != ""){
		$(tsdfa).innerHTML = totalSameDayFeeMO;
		$('sameDayUpchargeMouseOver').value='ON';
    }
    else{
    	$(tsdfa).innerHTML = '0.00';
		$('sameDayUpchargeMouseOver').value='OFF';
    }

    var totalLateCutoffMO = FTD_XML.selectNodeText(record,'total-late-cutoff-fee-mo');
    if(totalLateCutoffMO != null && totalLateCutoffMO != ""){
		$(tlcfa).innerHTML = totalLateCutoffMO;
		$('lateCutOffMouseOver').value='ON';
    }
    else{
    	$(tlcfa).innerHTML = '0.00';
		$('lateCutOffMouseOver').value='OFF';
    }

    var totalMondayUpchargeMO = FTD_XML.selectNodeText(record,'total-monday-upcharge-mo');
    if(totalMondayUpchargeMO != null && totalMondayUpchargeMO != ""){
		$(tmuca).innerHTML = totalMondayUpchargeMO;
		$('mondayUpchargeMouseOver').value='ON';
    }
    else{
    	$(tmuca).innerHTML = '0.00';
		$('mondayUpchargeMouseOver').value='OFF';
    }

    var totalSundayUpchargeMO = FTD_XML.selectNodeText(record,'total-sunday-upcharge-mo');
    if(totalSundayUpchargeMO != null && totalSundayUpchargeMO != ""){
		$(tsuca).innerHTML = totalSundayUpchargeMO;
		$('sundayUpchargeMouseOver').value='ON';
    }
    else{
    	$(tsuca).innerHTML = '0.00';
		$('sundayUpchargeMouseOver').value='OFF';
    }
    
    var totalInternationalFeeMO = FTD_XML.selectNodeText(record,'total-international-fee-mo');
    if(totalInternationalFeeMO != null && totalInternationalFeeMO != ""){
		$(tifa).innerHTML = totalInternationalFeeMO;
		$('internationalFeeMouseOver').value='ON';
    }
    else{
    	$(tifa).innerHTML = '0.00';
		$('internationalFeeMouseOver').value='OFF';
    }
    
    var totalServiceFeeMO = FTD_XML.selectNodeText(record,'total-service-fee-mo');
    if(totalServiceFeeMO != null && totalServiceFeeMO != ""){
    	//$(tsfa).innerHTML = totalServiceFeeMO
		$('serviceFeeMouseOver').value='ON';
    }
    else{
    	$(tsfa).innerHTML = '0.00';
		$('serviceFeeMouseOver').value='OFF';
    }
    //$('totalSalesTax').innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(record,'total-sales-tax'));
    var totaltaxlistsize = UTILS.addCommas(FTD_XML.selectNodeText(record,'totaltaxlist-size'));
    var taxServicePerformed = UTILS.addCommas(FTD_XML.selectNodeText(record,'taxservice-performed'));
    if(parseInt(totaltaxlistsize)==0){
    	$('total_tax1_amount').innerHTML='0.00';
    }
    for(var t=1; t<=parseInt(totaltaxlistsize); t++)
    {
    	var ttn = 'total_tax'+t+'_name';
    	var tta = 'total_tax'+t+'_amount';
    	var ttdesc = 'total-tax'+t+'-desc';
    	var ttamt = 'total-tax'+t+'-amt';
    	var amt = UTILS.addCommas(FTD_XML.selectNodeText(record,ttamt));

    			document.getElementById(ttn).style.display = 'block';
    			$(ttn).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(record,ttdesc));
    			document.getElementById(tta).style.display = 'block';
    			if(parseFloat(amt) == 0 || taxServicePerformed =='N'){$(tta).innerHTML='0.00';}
    			else{$(tta).innerHTML=amt;}
       			if($('onSubmitButtonFlagOn').value == 'Y' && taxServicePerformed =='N') {
    				$(tta).innerHTML=amt;
    				if (parseFloat(amt) == 0) {$(tta).innerHTML ='0.00';}
    			}

    }
    //## end of total taxes ##
    $('total').innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(record,'order-total-display'));
    $('totalOrderAmount').value = UTILS.addCommas(FTD_XML.selectNodeText(record,'order-total'));

    var v_cartHasFreeShippingProduct = 'N';
    var oIterations = v_cartList.length;
    for(oIdx=0 ; oIdx<oIterations; oIdx++ ) {
        var orderObject = v_cartList[oIdx];
        if( orderObject==null || orderObject.orderElement==null ) continue;
        var prodObject = orderObject.productObject;
        if( prodObject!=null && prodObject.allowFreeShippingFlag == 'Y') {
            v_cartHasFreeShippingProduct = 'Y';
        }
    }

    v_freeShippingSavings = FTD_XML.selectNodeText(record,'free-shipping-savings');
    submitFreeShippingAppliedMsg = FTD_XML.selectNodeText(record, 'submit-fs-applied-msg');
    var buyerHasFreeShipping = FTD_XML.selectNodeText(record,'buyer-has-free-shipping');
    var emailAddress = UTILS.trim(FTD_XML.selectNodeText(record,'buyer-email-address'));
    var buyerCamsVerified = FTD_XML.selectNodeText(record,'buyer-cams-verified');
    var premierCircleMembershipMsg = FTD_XML.selectNodeText(record,'premier-circle-msg');
    var displayFSandPCMsg = 'N';
    if (buyerHasFreeShipping == 'Y' && v_cartHasFreeShippingProduct == 'Y') {
        if (v_sourceCodeAllowsFreeShipping == 'N') {
            if (v_displayed_buyerHasFreeShipping == 'Y' && v_displayed_updatedSourceCode_FreeShippingNotAllowed != 'Y') {
                shadeMsg = FTD_XML.selectNodeText(record,'fs-not-applied-sc-update-msg');
                notifyUser(shadeMsg, false, true);
            } else {
            if (v_displayed_sourceCode_FreeShippingNotAllowed != 'Y') {
                shadeMsg = FTD_XML.selectNodeText(record,'fs-not-applied-sc-msg');
                notifyUser(shadeMsg, false, true);
            }
            }
            v_displayed_sourceCode_FreeShippingNotAllowed = 'Y';
            v_displayed_updatedSourceCode_FreeShippingNotAllowed = 'Y';
            v_displayed_buyerHasFreeShipping = 'N';
        } else {
        	if (v_displayed_buyerHasFreeShipping != 'Y' || !UTILS.isEmpty(premierCircleMembershipMsg)) {
                shadeMsg = FTD_XML.selectNodeText(record,'fs-applied-msg');
                if (!UTILS.isEmpty(premierCircleMembershipMsg)){
                	displayFSandPCMsg = 'Y';
                }
                else{
                	notifyUser(shadeMsg, false, true);
                }
                v_displayed_buyerHasFreeShipping = 'Y';
                // Don't display the "No FS Products" message once the "Customer has FS" message
                v_displayed_noFreeShippingProducts = 'Y';
                //$('sameDayUpchargeMouseOver').value='OFF';
                $('applySurchargeCode').value='OFF';
            }
        }
    }

    if (buyerHasFreeShipping == 'Y' && v_sourceCodeAllowsFreeShipping == 'Y') {
        if (v_cartHasFreeShippingProduct != 'Y' && v_displayed_noFreeShippingProducts != 'Y') {
            shadeMsg = FTD_XML.selectNodeText(record,'fs-not-applied-product-msg');
            notifyUser(shadeMsg, false, true);
            v_displayed_noFreeShippingProducts = 'Y';
        }
    }

    if (buyerHasFreeShipping != 'Y' && v_displayed_buyerHasFreeShipping == 'Y' &&
                v_cartHasFreeShippingProduct == 'Y' && v_sourceCodeAllowsFreeShipping == 'Y' && buyerCamsVerified == 'Y') {
        if (!UTILS.isEmpty(emailAddress)) {
            shadeMsg = FTD_XML.selectNodeText(record,'fs-not-applied-email-update-msg');
            notifyUser(shadeMsg, false, true);
        } else {
        		shadeMsg = FTD_XML.selectNodeText(record,'fs-not-applied-email-none-msg');
                notifyUser(shadeMsg, false, true);
        }
     }


    if (!UTILS.isEmpty(emailAddress) && buyerCamsVerified != 'Y') {
    		shadeMsg = FTD_XML.selectNodeText(record,'response-cams-unreachable');
            notifyUser(shadeMsg, false, true);
    }

    if (!UTILS.isEmpty(premierCircleMembershipMsg)){
    	if(displayFSandPCMsg == 'Y'){
    		shadeMsg = 'Customer is a member of the following:'+
    					'<br/><br/>Premier Circle'+
    					'<br/><br/>FTD Gold Member'+
    					'<br/><br/>'+shadeMsg;
    	}
    	else{shadeMsg = '<br/>'+premierCircleMembershipMsg;}
    	notifyUser(shadeMsg, false, true);
    }

    v_buyerHasFreeShipping = buyerHasFreeShipping;

    var records = XPath.selectNodes('/result/rs/order/item',xmlDoc);
    for( var idx=0; idx<records.length; idx++ ) {
        var cartIdx = FTD_XML.selectNodeText(records[idx],'cart-index');
        $('ci_productId___'+cartIdx).innerHTML=FTD_XML.selectNodeText(records[idx],'product-id');
        var productPrice = FTD_XML.selectNodeText(records[idx],'product-price');
        var discountedPrice = FTD_XML.selectNodeText(records[idx],'discounted-price');
        if( productPrice==discountedPrice ) {
            $('ci_productPriceMilesPoints___'+cartIdx).style.textDecoration = '';
            $('ci_productPriceDiscounted___'+cartIdx).innerHTML=UTILS.addCommas(productPrice);
            var points = FTD_XML.selectNodeText(records[idx],'partner-points');
            if( points=='0' )
                $('ci_productPriceMilesPoints___'+cartIdx).innerHTML='';
            else {
                $('ci_productPriceMilesPoints___'+cartIdx).innerHTML=points+'&nbsp;'+FTD_XML.selectNodeText(records[idx],'reward-type');
            }
        } else {
            $('ci_productPriceDiscounted___'+cartIdx).innerHTML=UTILS.addCommas(discountedPrice);
            $('ci_productPriceMilesPoints___'+cartIdx).innerHTML=UTILS.addCommas(productPrice);
            $('ci_productPriceMilesPoints___'+cartIdx).style.textDecoration = 'line-through';
        }
        $('ci_addOns___'+cartIdx).innerHTML=FTD_XML.selectNodeText(records[idx],'addons');
        $('ci_serviceFees___'+cartIdx).innerHTML=FTD_XML.selectNodeText(records[idx],'service-fee');

        $('ci_spanItemMouseOver___' + cartIdx).style.width="0px";
  	    $('ci_spanItemMouseOver___' + cartIdx).style.border="0px";
  	    $('ci_spanItemMouseOver___' + cartIdx).style.background="transparent";
  	    $('ci_spanItemMouseOver___' + cartIdx).innerHTML='';

        var itemSameDayFee = FTD_XML.selectNodeText(records[idx],'same-day-fee');
        var sdfn = 'ci_same_day_fee___'+cartIdx;
    	var sdfa = 'ci_same_day_fee_amount___'+cartIdx;

    	var lateCutOff = FTD_XML.selectNodeText(records[idx],'late-cutoff-fee');
    	var lcfa = 'ci_late_cutoff_fee_amount___'+cartIdx;

    	var mondayUpcharge = FTD_XML.selectNodeText(records[idx],'vendor-mon-upcharge');
    	var muca = 'ci_vendor_mon_upcharge_amount___'+cartIdx;

    	var sundayUpcharge = FTD_XML.selectNodeText(records[idx],'vendor-sun-upcharge');
    	var suc = 'ci_vendor_sun_upcharge___'+cartIdx;
    	var suca = 'ci_vendor_sun_upcharge_amount___'+cartIdx;
    	
    	var internationalFee = FTD_XML.selectNodeText(records[idx],'international-fee');
    	var ifa = 'ci_international_fee_amount___'+cartIdx;
    	
    	var serviceFee = FTD_XML.selectNodeText(records[idx],'service-fee');
    	var sfa = 'ci_serviceFees___'+cartIdx;
    	var sfaMO = 'ci_service_fee_amount___'+cartIdx;
    	
    	$(sfaMO).innerHTML=FTD_XML.selectNodeText(records[idx],'service-fee-mo');

        if(itemSameDayFee != null && itemSameDayFee != ""){
        	document.getElementById(sdfn).style.display = 'block';
			document.getElementById(sdfa).style.display = 'block';
			$(sdfa).innerHTML = itemSameDayFee;
		}
        else{
        	document.getElementById(sdfn).style.display = 'none';
			document.getElementById(sdfa).style.display = 'none';
		}

        if(lateCutOff != null && lateCutOff != ""){
        	document.getElementById(lcfa).style.display = 'none';
			$(lcfa).innerHTML = lateCutOff;
		}

        if(mondayUpcharge != null && mondayUpcharge != ""){
        	document.getElementById(muca).style.display = 'none';
			$(muca).innerHTML = mondayUpcharge;
		}

        if(sundayUpcharge != null && sundayUpcharge != ""){
        	document.getElementById(suca).style.display = 'none';
			$(suca).innerHTML = sundayUpcharge;
		}

        if(internationalFee != null && internationalFee != ""){
        	document.getElementById(ifa).style.display = 'none';
			$(ifa).innerHTML = internationalFee;
		}
		
		if(serviceFee != null && serviceFee != ""){
        	document.getElementById(sfa).style.display = 'block';	
			$(sfa).innerHTML = serviceFee;
		}
		else{
			document.getElementById(sfa).style.display = 'block';
		}

        var itemSameDayFeeMO = FTD_XML.selectNodeText(records[idx],'same-day-fee-mo');
        if(itemSameDayFeeMO != null && itemSameDayFeeMO != ""){
			$(sdfa).innerHTML = itemSameDayFeeMO;
        }
        else{$(sdfa).innerHTML = '0.00';}

        var itemLateCutoffMO = FTD_XML.selectNodeText(records[idx],'late-cutoff-fee-mo');
		if(itemLateCutoffMO != null && itemLateCutoffMO != ""){
			$(lcfa).innerHTML = itemLateCutoffMO;
        }
        else{$(lcfa).innerHTML = '0.00';}

        var itemMondayUpchargeMO = FTD_XML.selectNodeText(records[idx],'monday-upcharge-mo');
        if(itemMondayUpchargeMO != null && itemMondayUpchargeMO != ""){
			$(muca).innerHTML = itemMondayUpchargeMO;
        }
        else{$(muca).innerHTML = '0.00';}

        var itemSundayUpchargeMO = FTD_XML.selectNodeText(records[idx],'sunday-upcharge-mo');
        if(itemSundayUpchargeMO != null && itemSundayUpchargeMO != ""){
			$(suca).innerHTML = itemSundayUpchargeMO;
        }
        else{$(suca).innerHTML = '0.00';}
        
        
        var itemInternationalFeeMO = FTD_XML.selectNodeText(records[idx],'international-fee-mo');
        if(itemInternationalFeeMO != null && itemInternationalFeeMO != ""){
			$(ifa).innerHTML = itemInternationalFeeMO;
        }
        else{$(ifa).innerHTML = '0.00';}
        
        var itemServiceFeeMO = FTD_XML.selectNodeText(records[idx],'service-fee-mo');
        if(itemServiceFeeMO != null && itemServiceFeeMO != ""){
        	//$(sfa).innerHTML = itemServiceFeeMO;
		}
        else{$(sfa).innerHTML = '0.00';}
        
        
        $('ci_shippingFees___'+cartIdx).innerHTML=FTD_XML.selectNodeText(records[idx],'shipping-fee');
        $('ci_surchargeFees___'+cartIdx).innerHTML=FTD_XML.selectNodeText(records[idx],'surcharge-fee');
        //$('ci_salesTax___'+cartIdx).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'sales-tax'));
        var taxlistsize = UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'taxlist-size'));
        if(parseInt(taxlistsize)==0){
        	$('ci_tax1_amount___'+cartIdx).innerHTML='0.00';
        }
        for(var t=1; t<=parseInt(taxlistsize); t++)
        {
        	var tn = 'ci_tax'+t+'_name___'+cartIdx;
        	var ta = 'ci_tax'+t+'_amount___'+cartIdx;
        	var tdesc = 'tax'+t+'-desc'; 	var tamt = 'tax'+t+'-amt'; var amt = UTILS.addCommas(FTD_XML.selectNodeText(records[idx],tamt));
        	var recipCountry = v_cartList[cartIdx].recipientCountry; var company_id = $('callDataCompany').value;
			document.getElementById(tn).style.display = 'block';
			$(tn).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],tdesc));
			document.getElementById(ta).style.display = 'block';
			if(parseFloat(amt) == 0 || taxServicePerformed =='N'){$(ta).innerHTML='0.00';}
			else{$(ta).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],tamt));}

        }


        var itemTaxAmount = parseFloat(UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'item-tax')));
        var itemTotalAmount = parseFloat(UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'item-total')));
        var finalItemAmount = (itemTotalAmount + itemTaxAmount).toFixed(2);
        //## end of Item taxes ##
        $('ci_subTotal___'+cartIdx).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'item-total'));
        if($('onSubmitButtonFlagOn').value == 'Y' && taxServicePerformed =='N') {
        	$('ci_tax1_amount___'+cartIdx).innerHTML=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'item-tax'));
        	if(parseFloat($('ci_tax1_amount___'+cartIdx).innerHTML)==0) {$('ci_tax1_amount___'+cartIdx).innerHTML = '0.00';}
        	$('ci_subTotal___'+cartIdx).innerHTML = finalItemAmount;
        	$('total').innerHTML = UTILS.addCommas(FTD_XML.selectNodeText(record,'order-total'));
        }

        v_cartList[cartIdx].addOns=FTD_XML.selectNodeText(records[idx],'addons');
        v_cartList[cartIdx].serviceFees=FTD_XML.selectNodeText(records[idx],'service-fee');
        v_cartList[cartIdx].shippingFees=FTD_XML.selectNodeText(records[idx],'shipping-fee');
        //v_cartList[cartIdx].salesTax=FTD_XML.selectNodeText(records[idx],'sales-tax');
        v_cartList[cartIdx].subTotal=UTILS.addCommas(FTD_XML.selectNodeText(records[idx],'item-total'));

        //Alaska/Hawaii surcharge
        if( v_cartList[cartIdx].surchargeWarningGiven==false ) {
            var surchargeNode = XPath.selectNodes('/result/rs/order/item/alaska-hawaii-fee/text()',xmlDoc);
            if( surchargeNode!=null && surchargeNode.length>0 ) {
                var strMsg = "A $";
                strMsg += surchargeNode[0].nodeValue;
                strMsg += " surcharge will be added to deliver this item to Alaska or Hawaii.<br>";
                notifyUser(strMsg,false,true);
                v_cartList[cartIdx].surchargeWarningGiven=true;
            }
        }

    }

    //Now reset the last calculated values
    v_lastRecalcCartValues=v_currentRecalcCartValues;
    v_lastRecalcOrderValues=new Array(v_cartList.length);
    for( var idx2=0; idx2<v_cartList.length; idx2++ ) {
        v_lastRecalcOrderValues[idx2]=v_currentRecalcOrderValues[idx2];
    }

    setPaymentTypeAmounts();

	}

}

ORDER_AJAX.submitOrder = function() {

    if (v_modifyOrderFromCom){
        var creditCardInfo = $('paymentCCNumber').value;
        if ( (creditCardInfo.indexOf("************") > -1 )  && ( creditCardInfo.indexOf(globalCCNumber.substr(globalCCNumber.length - 4, 4)) > -1 ) ){
            $('paymentCCNumber').value = globalCCNumber;
        }
    }

    //XMLHttpRequest
    var securityRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'security.do' + getSecurityParams(true),FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.securityCallback,false,false);
    securityRequest.send();

    var orderXml = buildOrderXML(false, true);

    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SAVE_CART';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',FTD_XML.toString(orderXml));
    attr = doc.createAttribute("name");
    attr.value = 'order_xml';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.submitOrderCallback,true,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    setProcessing(true);
    newRequest.send();
}

ORDER_AJAX.securityCallback = function(transport) {
    //Do nothing
}

ORDER_AJAX.submitOrderCallback = function(transport) {
    setProcessing(false);
    var xmlDoc = parseResponse(transport);

    if (v_modifyOrderFromCom){
        $('paymentCCNumber').value = display_credit_card;
    }

    if( xmlDoc!=null ) {
        //1.  Check the status flag off the validation result set
        var record = XPath.selectNodes('/result/rs[@name="validation"]',xmlDoc)[0];
        var status = FTD_XML.getAttributeText(record,'status');
        var idx;
        var bypassCCAuth = 'N';
        var errMsg;
        var errContext;
        var showNotifyUser = true;

        if( status=='N' ) {
            // Defect 5245. Check if failure is due to CC auth error.
            // If the only error is due to CCAS error and user decides to place order without auth, resubmit order.
            var errors = XPath.selectNodes('/result/rs[@name="validation"]/record/validate[@status="N"]',xmlDoc);
            if(errors.length == 1) {
                errContext = FTD_XML.getAttributeText(errors[0],'name');
                errMsg = FTD_XML.getAttributeText(errors[0],'message');

                if( errMsg=='Credit card processing error.' ) {
                    var messages = 'Credit card authorization service is not available at this time.  Would you like to proceed without authorization?';
                    messages += '\r\n\r\nPress \"OK\" to submit the order or \"Cancel\" to return to the order.';

                    if ( confirm(messages)==true ) {
                         bypassCCAuth = 'Y';
                    }
                }

                if( errMsg=='Credit card declined.' ) {
                    v_csc_failed_count++;
                    $('paymentCSC').value='';
                }
                if (errContext == 'threshold greater') {
                	alert('Agent Alert: ' + errMsg);
                	showNotifyUser = false;
                } else if (errContext == 'threshold less') {
                	alert('Agent Alert: ' + errMsg);
                	status = 'Y';
                }

            }
        }

        if (status!='N' && status!='Y') {
            $('paymentCSC').value='';
        }

        if (bypassCCAuth == 'Y') {
            $('paymentCCBypassAuthFlag').value='Y';
            $('paymentNCAuthPwd').value='';
            ORDER_AJAX.submitOrder();
        }
        else if( status=='Y' ) {
            $('paymentCSC').value='';
            //2.  If status==Y, then get the data and prepare the confirmation page
            record = XPath.selectNodes('/result/order',xmlDoc)[0];
            var masterOrderNumber = FTD_XML.selectNodeText(record,'master-order-number');
            //If this is a new order for the same customer, customer service
            //wants all time recorded against the first order.
            if( v_lastMasterOrderNumber==null )
                v_lastMasterOrderNumber = masterOrderNumber;
            $('confMasterOrderNumber').innerHTML = masterOrderNumber;
            var totalDiscounts = FTD_XML.selectNodeText(record,'discount-total');
            $('confTotalDiscount').innerHTML = totalDiscounts;
            var totalMilesPoints = FTD_XML.selectNodeText(record,'miles-points-total');
            $('confTotalRewards').innerHTML = totalMilesPoints;
            var totalOrder = FTD_XML.selectNodeText(record,'order-total');
            $('confTotalCost').innerHTML = UTILS.addCommas(totalOrder);
            var confirmationMessage = FTD_XML.selectNodeText(record,'confirmation-message');
            if (confirmationMessage == null) {
                $('confirmationMessageRow').style.display = 'none';
            } else {
                $('confirmationMessageRow').style.display = '';
                $('confirmationMessage').innerHTML = confirmationMessage;
            }

            var payment = XPath.selectNodes('/result/order/payment',xmlDoc)[0];
            var creditCardAmount = FTD_XML.selectNodeText(payment, 'credit-card');
            if (creditCardAmount == null) {
                $('creditCardRow').style.display='none';
            } else {
                $('creditCardRow').style.display='';
                $('creditCardAmount').innerHTML = UTILS.addCommas(creditCardAmount);
            }
            var giftCertificateAmount = FTD_XML.selectNodeText(payment, 'gift-certificate');
            if (giftCertificateAmount == null) {
                $('giftCertificateRow').style.display='none';
            } else {
                $('giftCertificateRow').style.display='';
                $('giftCertificateAmount').innerHTML = UTILS.addCommas(giftCertificateAmount);
            }
            var noChargeAmount = FTD_XML.selectNodeText(payment, 'no-charge');
            if (noChargeAmount == null) {
                $('noChargeRow').style.display='none';
            } else {
                $('noChargeRow').style.display='';
                $('noChargeAmount').innerHTML = UTILS.addCommas(noChargeAmount);
            }

            var freeShippingSavings = FTD_XML.selectNodeText(record,'free-shipping-savings');
            if (freeShippingSavings > 0) {
                $('freeShippingSavingsRow').style.display = '';
                $('confFreeShippingSavings').innerHTML = freeShippingSavings;
            } else {
                $('freeShippingSavingsRow').style.display = 'none';
            }

            var orders = XPath.selectNodes('/result/order/items/external-order-number/text()',xmlDoc);

            if( orders.length==1 ) {
                var externalOrderNumber = orders[0].nodeValue;
                $('confExternalOrderNumber').innerHTML = externalOrderNumber;
                $('confExtOrdNumAnchor').href = 'javascript:ORDER_AJAX.recordCallTime();ORDER_EVENTS.onDoComSearch(\''+externalOrderNumber+'\');';
                $('externalOrderNumberRow').style.display='';
                $('masterOrderNumberRow').style.display='none';
                $('confScriptOrderNumber').innerHTML = externalOrderNumber;
                $('externalOrderNumberLabel').innerHTML = 'Confirmation&nbsp;Number';
            } else {
                $('externalOrderNumberRow').style.display='none';
                $('confMasterOrdNumAnchor').href = 'javascript:ORDER_AJAX.recordCallTime();ORDER_EVENTS.onDoComSearchMO();';
                $('masterOrderNumberRow').style.display='';
                $('confScriptOrderNumber').innerHTML = masterOrderNumber;
                $('masterOrderNumberLabel').innerHTML = 'Confirmation&nbsp;Number';
            }

        	// If we originally came from COM to do a Modify/Update order, set appropriate values to diplay links back to COM
            //
            if (v_modifyOrderFromCom) {
            	v_modify_order_in_joe = true;
            	$('origOrderNumberRow').style.display='';
                $('masterOrderNumberLabel').innerHTML = 'New&nbsp;Order&nbsp;Confirmation&nbsp;Number';
                $('externalOrderNumberLabel').innerHTML = 'New&nbsp;Order&nbsp;Confirmation&nbsp;Number';
                $('origExternalOrderNumber').innerHTML = v_orig_external_order_number;
                $('origOrdNumAnchor').href = 'javascript:ORDER_AJAX.recordCallTime();ORDER_EVENTS.onDoComReturnToOriginal();';
                if (v_call_disp_enabled) {
                	// Call Disposition session was active for original order in COM, so we are only allowed to return it (not new order)
                	var alertMsg = 'Call Disposition was active on original order, you must return to original order first';
                    $('confExtOrdNumAnchor').href = 'javascript:alert(\'' + alertMsg + '\');';
                    $('confMasterOrdNumAnchor').href = 'javascript:alert(\'' + alertMsg + '\');';
                }
                if (v_piModifyOrderFlag == 'Y') {
                	$('externalOrderNumberRow').style.display='none';
                	$('masterOrderNumberRow').style.display='none';
                	$('confScriptOrderNumber').innerHTML = v_orig_external_order_number;
                }
            } else {
            	$('origOrderNumberRow').style.display='none';
            }

            var tbl = document.getElementById('pgTable');
            while (tbl.rows.length > 2) {
                tbl.deleteRow(2);
            }
            document.getElementById("personalGreetingDiv").style.display='none';

            var personalGreeting = XPath.selectNodes('/result/order/personal-greetings/personal-greeting',xmlDoc);
            if (personalGreeting.length > 0) {

                document.getElementById('personalGreetingDiv').style.display='';

	            var pgInstructions = XPath.selectNodes('/result/order/personal-greetings/instructions/text()',xmlDoc);
	            if (pgInstructions.length > 0) {
	                $('pgInstructions').innerHTML = pgInstructions[0].nodeValue;
	            }

	            var pgIdentifier = XPath.selectNodes('/result/order/personal-greetings/identifier/text()',xmlDoc);
	            if (pgIdentifier.length > 0) {
	                $('pgIdentifier').innerHTML = pgIdentifier[0].nodeValue;
	            }

	            for (idx=0; idx<personalGreeting.length; idx++) {
	                var pgId = FTD_XML.selectNodeText(personalGreeting[idx], 'personal-greeting-id');
	                var recipientName = FTD_XML.selectNodeText(personalGreeting[idx], 'recipient-name');
	                var productName = FTD_XML.selectNodeText(personalGreeting[idx], 'product-name');

	                lastRow = tbl.rows.length;
	                var row = tbl.insertRow(lastRow);

	                var cellLeft = row.insertCell(0);
	                var textNode = document.createTextNode(pgId);
	                cellLeft.appendChild(textNode);

	                var cellMiddle = row.insertCell(1);
	                textNode = document.createTextNode(productName);
	                cellMiddle.appendChild(textNode);

	                var cellRight = row.insertCell(2);
	                textNode = document.createTextNode(recipientName);
	                cellRight.appendChild(textNode);
	            }
            }

            //show the screen with the confirmation number
            // we're just hiding/showing divs instead of using a slider because it is much more reliable
            $('rightDivId').hide();
            $('confirmationDivId').show();

            waitToSetFocus('confNewOrderButton');

        } else {
            //3.  If no, then iterate throuth the errors and display then in the notify section
            var errors = XPath.selectNodes('/result/rs[@name="validation"]/record/validate[@status="N"]',xmlDoc);

            var message = UTILS.boldWrapper('Please correct the following error(s):')+'<br><br>';
            for( idx=0; idx<errors.length; idx++ ) {
                var errContext = FTD_XML.getAttributeText(errors[idx],'name');
                var errMsg = FTD_XML.getAttributeText(errors[idx],'message');
                //Per email from kslater on 02/01/2008, don't show the manual auth box
//                if( errMsg=='Credit card declined.' ) {
//                    //open up the manual auth field and put focus on it
//                    $('paymentCCManAuthDiv').style.display='inline';
//                    waitToSetFocus('paymentCCManualAuth');
//                }
                message += UTILS.boldWrapper(errContext)+':&nbsp;&nbsp;';
                message += errMsg+'<br><br>';
            }
            $('billingButtonSubmit').disabled=false;
            $('leftDivId').disabled=false;
            $('businessDiv').disabled=false;
            $('actionButtonAbandon').disabled = false;
            if (showNotifyUser == true) {
                notifyUser(message,false,true);
            }
            v_checkAvailabilityTimer.startTimer();
            v_recalcOrderTimer.startTimer();
        }
    } else {
        $('billingButtonSubmit').disabled=false;
        $('leftDivId').disabled=false;
        $('businessDiv').disabled=false;
        $('actionButtonAbandon').disabled = false;
        notifyUser(message,false,true);
        v_checkAvailabilityTimer.startTimer();
        v_recalcOrderTimer.startTimer();
    }
}

ORDER_AJAX.recordCallTime = function() {
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_RECORD_CALL_TIME';
    root.setAttributeNode(attr);

    var param = FTD_XML.createElementWithText(doc,'param',v_callTimestamp+'');
    attr = doc.createAttribute("name");
    attr.value = 'START_TIME';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',v_repIdentity);
    attr = doc.createAttribute("name");
    attr.value = 'CSR_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

    param = FTD_XML.createElementWithText(doc,'param',$('callDataDNIS').value);
    attr = doc.createAttribute("name");
    attr.value = 'DNIS_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);

//    param = FTD_XML.createElementWithText(doc,'param',$('confMasterOrderNumber').innerHTML);
    param = FTD_XML.createElementWithText(doc,'param',v_lastMasterOrderNumber);
    attr = doc.createAttribute("name");
    attr.value = 'MASTER_ORDER_NUMBER';
    param.setAttributeNode(attr);
    root.appendChild(param);
    //XMLHttpRequest
    var newRequest =
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.recordCallTimeCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

ORDER_AJAX.recordCallTimeCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    //Nothing else to do
}

ORDER_AJAX.getDeliveryDates = function(itemidx) {
    try {
        var orderObj = v_cartList[itemidx];
        if(orderObj==null) return;

        var doc = FTD_XML.createXmlDocument("request");
        var root = doc.documentElement;
        var attr = doc.createAttribute("type");
        attr.value = 'AJAX_REQUEST_GET_DELIVERY_DATES';
        root.setAttributeNode(attr);

        var productId = orderObj.productId.toUpperCase();
        var param = FTD_XML.createElementWithText(doc,'param',productId);
        attr = doc.createAttribute("name");
        attr.value = 'PRODUCT_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = FTD_XML.createElementWithText(doc,'param',orderObj.productZip);
        attr = doc.createAttribute("name");
        attr.value = 'ZIP_CODE';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = FTD_XML.createElementWithText(doc,'param',orderObj.productCountry);
        attr = doc.createAttribute("name");
        attr.value = 'COUNTRY_ID';
        param.setAttributeNode(attr);
        root.appendChild(param);
        
        param = FTD_XML.createElementWithText(doc,'param',$('callDataSourceCode').getValue());
        attr = doc.createAttribute("name");
        attr.value = 'SOURCE_CODE';
        param.setAttributeNode(attr);
        root.appendChild(param);

        var addonString = '';
        for( var idx=0; idx<orderObj.addOnIdArray.length; idx++) {
        	if (orderObj.addOnCheckedArray[idx]) {
            	if (addonString.length > 0) {
            		addonString = addonString + ',';
            	}
            	addonString = addonString + orderObj.addOnIdArray[idx];
        	}
        }
        if( orderObj.productAddonVase!=null) {
        	if (addonString.length > 0) {
        		addonString = addonString + ',';
        	}
        	addonString = addonString + orderObj.productAddonVase;
        }
        param = FTD_XML.createElementWithText(doc,'param',addonString);
        attr = doc.createAttribute("name");
        attr.value = 'ADDONS';
        param.setAttributeNode(attr);
        root.appendChild(param);

        param = doc.createElement('echo');
        param.appendChild(FTD_XML.createElementWithText(doc,'product_id',productId));
        param.appendChild(FTD_XML.createElementWithText(doc,'country_id',orderObj.productCountry));
        param.appendChild(FTD_XML.createElementWithText(doc,'itemidx',itemidx+''));
        root.appendChild(param);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,ORDER_AJAX.getDeliveryDatesCallback,true,false);
        newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
        setProcessing(true);
        newRequest.send();
    } catch (err) {
        setProcessing(false);
        throw err;
    }
}

ORDER_AJAX.getDeliveryDatesCallback = function(transport) {
    //alert(transport.responseText);
    try {
        var xmlDoc = parseResponse(transport);

        if( xmlDoc!=null ) {
            var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
            var productId = FTD_XML.selectNodeText(echo,'product_id');
            var countryId = FTD_XML.selectNodeText(echo,'country_id');
            var itemIdx = FTD_XML.selectNodeText(echo,'itemidx');

            //Shut off the focus timer so user will not be unexpectedly
            //redirected to a field based on an old request.
            if( v_focusTimer.isRunning() ) {
                v_focusTimer.stopTimer();
            }

            var records = XPath.selectNodes('/result/rs[@name="product_delivery_dates"]/delivery_date',xmlDoc);
            if (records.length > 0) {
                var foundPreviousDeliveryDate = true;
                var currentDeliveryDate = $('productDeliveryDate').getValue();
                if (currentDeliveryDate != null && currentDeliveryDate != '') {
                    foundPreviousDeliveryDate = false;
                    var currentObj = v_deliveryDates[currentDeliveryDate];
                }
                v_deliveryDates = new Array();
                for( var idx=0; idx<records.length; idx++ ) {
                    var dateString = FTD_XML.selectNodeText(records[idx],'date');
                    var displayDate = FTD_XML.selectNodeText(records[idx],'displayDate');
                    //alert(dateString + " " + displayDate);

                    ddObj = new DELIVERY_DATE.OBJECT();
                    ddObj.startDate = dateString ;
                    //ddObj.endDate = dateString ;
                    ddObj.isDateRange = false;
                    ddObj.displayString = displayDate;
                    ddObj.valueString = dateString ;

                    v_deliveryDates[ddObj.valueString]=ddObj;

                    if (foundPreviousDeliveryDate==false && dateString == currentObj.valueString) {
                        foundPreviousDeliveryDate = true;
                    }
                }
                if (foundPreviousDeliveryDate == false) {
                    ddObj = new DELIVERY_DATE.OBJECT();
                    ddObj.startDate = currentObj.valueString;
                    ddObj.isDateRange = false;
                    ddObj.displayString = currentObj.displayString;
                    ddObj.valueString = currentObj.valueString;
                    v_deliveryDates[ddObj.valueString]=ddObj;
                }
                var deliveryDateHold = $('productDeliveryDate').getValue();
                populateDeliveryDateCombos();
                FTD_DOM.selectOptionByValue($('productDeliveryDate'), deliveryDateHold);

                var orderObj = v_cartList[itemIdx];
                if(orderObj!=null) {
                    orderObj.deliveryDatesList = v_deliveryDates;
                }
            }
        }
    } finally {
        setProcessing(false);
    }
}

// ]]>
