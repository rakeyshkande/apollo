// <![CDATA[
var TEXT_SEARCH_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || TEXT_SEARCH_AJAX.prototypeVersion < 1.6)
      throw("TEXT_SEARCH_AJAX requires the Prototype JavaScript framework >= 1.6");
      
TEXT_SEARCH_AJAX.zipSearch = function(mode,target) {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tszResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_ZIP_SEARCH';
    root.setAttributeNode(attr);
    
    var searchCity = $('tszCity').getValue();
    if( UTILS.isEmpty(searchCity)==false ) {
        searchCity = searchCity.toUpperCase();
        $('tszCity').value=searchCity;
    }
    var param = FTD_XML.createElementWithText(doc,'param',searchCity);
    attr = doc.createAttribute("name");
    attr.value = 'CITY';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchState = FTD_DOM.getSelectedValue('tszState');
    param = FTD_XML.createElementWithText(doc,'param',searchState);
    attr = doc.createAttribute("name");
    attr.value = 'STATE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var continueSearch = true;
    var errMsg = 'Unable to perform zip search:<br><br>';
    if( UTILS.isEmpty(searchCity) ) {
        continueSearch = false;
        waitToSetFocus($('tszCity'));
        errMsg += 'City cannot be blank<br>';
    }
    if( UTILS.isEmpty(searchState) ) {
        if( continueSearch == true) waitToSetFocus($('tszState'));
        continueSearch = false;
        errMsg += 'A state must be selected<br>';
    }
    
    if( continueSearch==false ) {
        notifyUser(errMsg,false,true);
        TEXT_SEARCH.showDone();
        TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
        return;
    }
    
    param = doc.createElement('echo');
    
    //param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    param.appendChild(FTD_XML.createElementWithText(doc,'city',searchCity));
    param.appendChild(FTD_XML.createElementWithText(doc,'state',searchState));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.zipSearchCallbackJSON,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    //setProcessing(true);
    newRequest.send();
}

TEXT_SEARCH_AJAX.zipSearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var target = jsonContent.result.echo.target;
        var searchId = jsonContent.result.echo.search_id;
        //var city = jsonContent.result.echo.city;
        var state = jsonContent.result.echo.state;
        var countryId = findStateById(state).countryId;
        var zipCode;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tszResultsBody');
            
            if( jsonContent.result.rs.status != 'Y' ) {
                TEXT_SEARCH.openZipSearch(target,true,true);
            } else {
                if (jsonContent.result.rs.record == null) {
                    //notify user that there were no matching records
                    $('tsNoResults').style.display='block';
                } else {
                        
                    results = makeJSONArray(jsonContent.result.rs.record);
                    recordsLength = results.length;

                    if ( recordsLength==1 ) {
                        zipCode = UTILS.trim(results[0].zip_code_id);
                        city = UTILS.trim(results[0].city);
                        if( target=='productZip' ) {
                            ORDER_EVENTS.changeProductZip(zipCode,countryId);
                            ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                            $('recipientCity').value = city;
                            TEXT_SEARCH.closeSearch();
                            ORDER_EVENTS.onProductIdOrZipCodeChange();
                            resetThumbnailDetail(v_currentCartIdx);
                            waitToSetFocus('productDeliveryDate'); 
                        } else if( target=='recipientZip' ) {
                            ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                            $('recipientCity').value = city;
                            ORDER_EVENTS.changeProductZip(zipCode,countryId);
                            TEXT_SEARCH.closeSearch();
                            ORDER_EVENTS.onProductIdOrZipCodeChange();
                            resetThumbnailDetail(v_currentCartIdx);
                            if( isCountryDomestic(countryId) ) {
                                waitToSetFocus('recipientState');
                            } else {
                                waitToSetFocus('recipientCity');
                            }
                        } else if( target=='customerZip' ) {
                            CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
                            $('customerCity').value = city;
                            TEXT_SEARCH.closeSearch();
                            if( isCountryDomestic(countryId) ) {
                                waitToSetFocus('customerStateCombo');
                            } else {
                                waitToSetFocus('customerCity');
                            }
                        } else if( target=='productSearchZip' ) {
                            CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
                            $('productSearchZip').value = zipCode;
                            new Effect.SlideUp('textSearchDivId');
                            new Effect.SlideDown('productSearchDivId');
                        } else if( target=='tsflZip' ) {
                            $('tsflZip').value = zipCode;
                            $('tsZipSearchCriteriaDiv').style.display = 'none';
                            $('textSearchMainCaption').innerHTML = 'Florist Search';
                            v_textSearchType = 'florist';
                            $('tsFloristSearchCriteriaDiv').style.display = 'block';
                            $('tszResultsDiv').style.display = 'none';
                            $('tsflResultsDiv').style.display = 'block';
                            $('tsflZip').focus();
                        }
                    } else {
                        //Populate the rearch results
                        $('tsNoResults').style.display='none';
                        for(var idx = 0; idx < recordsLength; idx++) {
                            zipCode = UTILS.trim(results[idx].zip_code_id);
                            city = UTILS.trim(results[idx].city);

                            var newRow = tbody.insertRow(tbody.rows.length);
                            if( tbody.rows.length%2==0 ) {
                                newRow.className = 'alternate';
                            }
                    
                            var cell = newRow.insertCell(0);
                            cell.align = "center";
                            var input = new FTD_DOM.InputElement( {
                                id:'tszRadioGroup'+'___'+idx,
                                type:'checkbox', 
                                name:'tszRadioGroup',
                                onClick:'TEXT_SEARCH.zipSelected(\''+target+'\','+idx+');'} ).createNode();
                            cell.appendChild(input);
                        
                            cell = newRow.insertCell(1);
                            cell.id='tsZipCity___'+idx;
                            cell.innerHTML = city;
                        
                            cell = newRow.insertCell(2);
                            cell.id='tsZipState___'+idx;
                            cell.innerHTML = state;
                        
                            cell = newRow.insertCell(3);
                            cell.id='tsZipCode___'+idx;
                            cell.innerHTML = zipCode;
                        }
                    }
                }
            }
        }
        
        Table.page($('tszTable'),0);
    }
}

TEXT_SEARCH_AJAX.membershipIdSearch = function(mode,target) {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tsmResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_MEMBERSHIP_ID_SEARCH';
    root.setAttributeNode(attr);
    
    var searchValue;
    if( mode=='SEARCH' ) {
        searchValue = $('tsmZip').getValue();
    } else {
        //Validate mode
        searchValue = $(target).getValue();
    }

    var param = FTD_XML.createElementWithText(doc,'param',searchValue);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    
    param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    param.appendChild(FTD_XML.createElementWithText(doc,'zip_code',searchValue));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.MembershipIDSearchCallbackJSON,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    //setProcessing(true);
    newRequest.send();
}

TEXT_SEARCH_AJAX.MembershipIDSearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();

    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var target = jsonContent.result.echo.target;
        var searchId = jsonContent.result.echo.search_id;
        var mode = jsonContent.result.echo.mode;
        var searchZipCode = jsonContent.result.echo.zip_code;
        var clubName;
        var membershipId;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tsmResultsBody');
            FTD_DOM.removeAllTableRows(tbody);
            
            if( jsonContent.result.rs.status != 'Y' ) {
                if( mode!='VALIDATE' ) { 
                    $('tsmZip').value = zipCode;
                    TEXT_SEARCH.openMembershipIdSearch(target,true);
                }
            } else {
                if( mode=='SEARCH' || forceSearch==true ) {
                    if (jsonContent.result.rs.record == null) {
                        //notify user that there were no matching records
                        $('tsNoResults').style.display='block';
                    } else {
                        //Populate the rearch results
                        $('tsNoResults').style.display='none';
                        
                        results = makeJSONArray(jsonContent.result.rs.record);
                        recordsLength = results.length;

                        for(var idx = 0; idx < recordsLength; idx++) {
                            var colCnt=0;
                            thisRecord=results[idx];

                            //dayPhoneNumber = UTILS.trim(jsonContent.result.rs.record[idx].day_phone);
                            clubName = UTILS.trim(thisRecord.member_name);
                            membershipId = UTILS.trim(thisRecord.aaa_member_id);
                                
                            var newRow = tbody.insertRow(tbody.rows.length);
                            if( tbody.rows.length%2==0 ) {
                                newRow.className = 'alternate';
                            }
                    
                            var cell = newRow.insertCell(colCnt++);
                            cell.align = "center";
                            var input = new FTD_DOM.InputElement( {
                                id:'tszRadioGroup'+'___'+idx,
                                type:'checkbox', 
                                name:'tszRadioGroup',
                                onClick:'TEXT_SEARCH.membershipIdSelected(\''+target+'\','+idx+');'} ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsClubName___'+idx;
                            cell.colSpan = 2;
                            cell.innerHTML = clubName;
                            
                             input = new FTD_DOM.InputElement( {
                                id:'memberShipId___'+idx,
                                type:'hidden', 
                                value:membershipId
                                } ).createNode();
                            cell.appendChild(input);
                        } //End for loop
                    }
                } else { 
                    //Validate mode
                    if (jsonContent.result.rs.record == null) {
                        //don't do anything since nothing was found
                    }  else {
                        //Let the user choose to accept or reject
                        $('tsmZip').value = searchZipCode;
                        TEXT_SEARCH.openMembershipIdSearch(target,false);
                        TEXT_SEARCH_AJAX.MembershipIDSearchCallbackJSON(transport,true);
                    }
                }
            }
        }
        Table.page($('tsmTable'),0);
    }
}


/* replace by JSON
TEXT_SEARCH_AJAX.zipSearchCallback = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        
        var records = XPath.selectNodes('/result/rs[@name="zips"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        //var mode = FTD_XML.selectNodeText(echo,'mode');
        var target = FTD_XML.selectNodeText(echo,'target');
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        var city = FTD_XML.selectNodeText(echo,'city');
        var state = FTD_XML.selectNodeText(echo,'state');
        var countryId = findStateById(state).countryId;
        var zipCode;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tszResultsBody');
            
            if( !TEXT_SEARCH.checkResultSetStatus(xmlDoc,"zips",false) ) {
                TEXT_SEARCH.openZipSearch(target,true,true);
            } else if( records!=null ) {
                if( records.length==0 ) {
                    //notify user that there were no matching records
                    $('tsNoResults').style.display='block';
                } else if( records.length==1 ) { 
                    zipCode = UTILS.trim(FTD_XML.selectNodeText(records[0],'zip_code_id'));
                    if( target=='productZip' ) {
                        ORDER_EVENTS.changeProductZip(zipCode,countryId);
                        ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                        $('recipientCity').value = city;
                        TEXT_SEARCH.closeSearch();
                        waitToSetFocus('productDeliveryDate'); 
                    } else if( target=='recipientZip' ) {
                        ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                        $('recipientCity').value = city;
                        ORDER_EVENTS.changeProductZip(zipCode,countryId);
                        TEXT_SEARCH.closeSearch();
                        if( isCountryDomestic(countryId) ) {
                            waitToSetFocus('recipientState');
                        } else {
                            waitToSetFocus('recipientCity');
                        }
                    } else if( target=='customerZip' ) {
                        CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
                        $('customerCity').value = city;
                        TEXT_SEARCH.closeSearch();
                        if( isCountryDomestic(countryId) ) {
                            waitToSetFocus('customerStateCombo');
                        } else {
                            waitToSetFocus('customerCity');
                        }
                    } else if( target=='productSearchZip' ) {
                        CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
                        $('productSearchZip').value = zipCode;
                        new Effect.SlideUp('textSearchDivId');
                        new Effect.SlideDown('productSearchDivId');
                    } else if( target=='tsflZip' ) {
                        $('tsflZip').value = zipCode;
                        $('tsZipSearchCriteriaDiv').style.display = 'none';
                        $('textSearchMainCaption').innerHTML = 'Florist Search';
                        v_textSearchType = 'florist';
                        $('tsFloristSearchCriteriaDiv').style.display = 'block';
                        $('tszResultsDiv').style.display = 'none';
                        $('tsflResultsDiv').style.display = 'block';
                        $('tsflZip').focus();
                    }
                } else {
                    //Populate the rearch results
                    $('tsNoResults').style.display='none';
                    for( idx=0; idx<records.length; idx++ ) {
                        zipCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'zip_code_id'));
                        
                        var newRow = tbody.insertRow(tbody.rows.length);
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                    
                        var cell = newRow.insertCell(0);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tszRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tszRadioGroup',
                            onClick:'TEXT_SEARCH.zipSelected(\''+target+'\','+idx+');'} ).createNode();
                        cell.appendChild(input);
                        
                        cell = newRow.insertCell(1);
                        cell.id='tsZipCity___'+idx;
                        cell.innerHTML = city;
                        
                        cell = newRow.insertCell(2);
                        cell.id='tsZipState___'+idx;
                        cell.innerHTML = state;
                        
                        cell = newRow.insertCell(3);
                        cell.id='tsZipCode___'+idx;
                        cell.innerHTML = zipCode;
                    }
                }
            }
        }
        
        Table.page($('tszTable'),0);
    }
}
*/

TEXT_SEARCH_AJAX.phoneSearch = function(mode,target) {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tspResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_PHONE_SEARCH';
    root.setAttributeNode(attr);
    
    var searchValue;
    if( mode=='SEARCH' ) {
        searchValue = $('tspPhone').getValue();
    } else {
        //Validate mode
        searchValue = $(target).getValue();
    }
    var param = FTD_XML.createElementWithText(doc,'param',searchValue);
    attr = doc.createAttribute("name");
    attr.value = 'PHONE_NUMBER';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var sourceCode = $('callDataSourceCode').getValue();
    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    
    param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    param.appendChild(FTD_XML.createElementWithText(doc,'source_code',sourceCode));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    param.appendChild(FTD_XML.createElementWithText(doc,'phone_number',searchValue));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.phoneSearchCallbackJSON,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    //setProcessing(true);
    newRequest.send();
}

TEXT_SEARCH_AJAX.phoneSearchCallbackJSON = function(transport,forceSearch) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();

    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var target = jsonContent.result.echo.target;
        var searchId = jsonContent.result.echo.search_id;
        var mode = jsonContent.result.echo.mode;
        var searchPhone = jsonContent.result.echo.phone_number;
        var dayPhoneNumber;
        var dayExtension;
        var eveningPhoneNumber;
        var eveningExtension;
        var addressType;
        var businessName;
        var name;
        var firstName;
        var lastName;
        var address1;
        var address2;
        var city;
        var state;
        var countryId;
        var zipCode;
        var emailAddress;
        var subscribe;
        var membershipId;
        var membershipFirstName;
        var membershipLastName;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tspResultsBody');
            FTD_DOM.removeAllTableRows(tbody);
            
            if( jsonContent.result.rs.status != 'Y' ) {
                if( mode!='VALIDATE' ) { 
                    $('tspPhone').value = searchPhone;
                    TEXT_SEARCH.openPhoneSearch(target,true);
                }
            } else {
                if( mode=='SEARCH' || forceSearch==true ) {
                    if (jsonContent.result.rs.record == null) {
                        //notify user that there were no matching records
                        $('tsNoResults').style.display='block';
                    } else {
                        //Populate the rearch results
                        $('tsNoResults').style.display='none';
                        
                        results = makeJSONArray(jsonContent.result.rs.record);
                        recordsLength = results.length;

                        for(var idx = 0; idx < recordsLength; idx++) {
                            var colCnt=0;
                            thisRecord=results[idx];

                            //dayPhoneNumber = UTILS.trim(jsonContent.result.rs.record[idx].day_phone);
                            dayPhoneNumber = UTILS.trim(thisRecord.day_phone);
                            dayExtension = UTILS.trim(thisRecord.day_extension);
                            eveningPhoneNumber = UTILS.trim(thisRecord.evening_phone);
                            eveningExtension = UTILS.trim(thisRecord.evening_extension);
                            addressType = UTILS.trim(thisRecord.address_type);
                            businessName = UTILS.trim(thisRecord.business_name);
                            firstName = UTILS.trim(thisRecord.first_name);
                            lastName = UTILS.trim(thisRecord.last_name);
                            name="";
                            if( lastName!=null && lastName.length>0 ) {
                                name+=lastName;
                            }
                            if( firstName!=null && firstName.length>0 ) {
                                if( name.length>0 ) name+=', ';
                                name+=firstName;
                            }
                            address1 = UTILS.trim(thisRecord.address_1);
                            address2 = UTILS.trim(thisRecord.address_2);
                            if( address2!=null && address2.length>0 ) {
                                address1 = address1+address2;
                            }
                            city = UTILS.trim(thisRecord.city);
                            state = UTILS.trim(thisRecord.state);
                            zipCode = UTILS.trim(thisRecord.zip_code);
                            countryId = UTILS.trim(thisRecord.country);
                            emailAddress = UTILS.trim(thisRecord.email_address);
                            subscribe = (UTILS.trim(thisRecord.subscribe_status)=='Subscribe'?'true':'false');
                            membershipId = UTILS.trim(thisRecord.membership_id);
                            membershipFirstName = UTILS.trim(thisRecord.membership_first_name);
                            membershipLastName = UTILS.trim(thisRecord.membership_last_name);
    
                            if( countryId=='US' && zipCode!=null && zipCode.length>5 ) {
                                zipCode = zipCode.substr(0,5);
                            } else if( countryId=='CA' && zipCode!=null && zipCode.length>3 ) {
                                zipCode = zipCode.substr(0,3);
                            }
                            
                            var newRow = tbody.insertRow(tbody.rows.length);
                            if( tbody.rows.length%2==0 ) {
                                newRow.className = 'alternate';
                            }
                    
                            var cell = newRow.insertCell(colCnt++);
                            cell.align = "center";
                            var input = new FTD_DOM.InputElement( {
                                id:'tspRadioGroup'+'___'+idx,
                                type:'checkbox', 
                                name:'tspRadioGroup',
                                onClick:'TEXT_SEARCH.phoneSelected(\''+target+'\','+idx+');'} ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneNumber___'+idx;
                            cell.innerHTML = dayPhoneNumber;
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneEvening___'+idx;
                            cell.innerHTML = eveningPhoneNumber;
                            
                            cell = newRow.insertCell(colCnt++);
                            var span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneName___'+idx
                                } ).createNode();
                            span.innerHTML = name;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneFirstName___'+idx,
                                type:'hidden', 
                                value:firstName
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneLastName___'+idx,
                                type:'hidden', 
                                value:lastName
                                } ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneBusiness___'+idx;
                            cell.innerHTML = businessName;
                            
                            cell = newRow.insertCell(colCnt++);
                            span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneAddress___'+idx
                                } ).createNode();
                            span.innerHTML = address1;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneAddressType___'+idx,
                                type:'hidden', 
                                value:addressType
                                } ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneCity___'+idx;
                            cell.innerHTML = city;
                            
                            cell = newRow.insertCell(colCnt++);
                            span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneState___'+idx
                                } ).createNode();
                            span.innerHTML = state;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneZipCode___'+idx,
                                type:'hidden', 
                                value:zipCode
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneCountry___'+idx,
                                type:'hidden', 
                                value:countryId
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneExt___'+idx,
                                type:'hidden', 
                                value:dayExtension
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneEveningExt___'+idx,
                                type:'hidden', 
                                value:eveningExtension
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneEmail___'+idx,
                                type:'hidden', 
                                value:emailAddress
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneSubscribe___'+idx,
                                type:'hidden', 
                                value:subscribe
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipId___'+idx,
                                type:'hidden', 
                                value:membershipId
                                } ).createNode();
                            cell.appendChild(input);
                            
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipFirstName___'+idx,
                                type:'hidden', 
                                value:membershipFirstName
                                } ).createNode();
                            cell.appendChild(input);
                            
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipLastName___'+idx,
                                type:'hidden', 
                                value:membershipLastName
                                } ).createNode();
                            cell.appendChild(input);
                        } //End for loop
                    }
                } else { 
                    //Validate mode
                    if (jsonContent.result.rs.record == null) {
                        //don't do anything since nothing was found
                    }  else {
                        //Let the user choose to accept or reject
                        $('tspPhone').value = searchPhone;
                        TEXT_SEARCH.openPhoneSearch(target,false);
                        TEXT_SEARCH_AJAX.phoneSearchCallbackJSON(transport,true);
                    }
                }
            }
        }
        Table.page($('tspTable'),0);
    }
}

/* replace by JSON
TEXT_SEARCH_AJAX.phoneSearchCallback = function(transport,forceSearch) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        
        var records = XPath.selectNodes('/result/rs[@name="customers"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var mode = FTD_XML.selectNodeText(echo,'mode');
        var target = FTD_XML.selectNodeText(echo,'target');
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        var searchPhone = FTD_XML.selectNodeText(echo,'phone_number');
        var dayPhoneNumber;
        var dayExtension;
        var eveningPhoneNumber;
        var eveningExtension;
        var addressType;
        var businessName;
        var name;
        var firstName;
        var lastName;
        var address1;
        var address2;
        var city;
        var state;
        var countryId;
        var zipCode;
        var emailAddress;
        var subscribe;
        var membershipId;
        var membershipFirstName;
        var membershipLastName;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tspResultsBody');
            FTD_DOM.removeAllTableRows(tbody);
            
            if( !TEXT_SEARCH.checkResultSetStatus(xmlDoc,"customers",true) ) {
                if( mode!='VALIDATE' ) { 
                    $('tspPhone').value = searchPhone;
                    TEXT_SEARCH.openPhoneSearch(target,true);
                }
            } else if( records!=null ) {
                if( mode=='SEARCH' || forceSearch==true ) {
                    if( records.length==0 ) {
                        //notify user that there were no matching records
                        $('tsNoResults').style.display='block';
                    } else {
                        //Populate the rearch results
                        $('tsNoResults').style.display='none';
                        for( idx=0; idx<records.length; idx++ ) {
                            var colCnt=0;
                            dayPhoneNumber = UTILS.trim(FTD_XML.selectNodeText(records[idx],'day_phone'));
                            dayExtension = UTILS.trim(FTD_XML.selectNodeText(records[idx],'day_extension'));
                            eveningPhoneNumber = UTILS.trim(FTD_XML.selectNodeText(records[idx],'evening_phone'));
                            eveningExtension = UTILS.trim(FTD_XML.selectNodeText(records[idx],'evening_extension'));
                            addressType = UTILS.trim(FTD_XML.selectNodeText(records[idx],'address_type'));
                            businessName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'business_name'));
                            firstName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'first_name'));
                            lastName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'last_name'));
                            name="";
                            if( lastName!=null && lastName.length>0 ) {
                                name+=lastName;
                            }
                            if( firstName!=null && firstName.length>0 ) {
                                if( name.length>0 ) name+=', ';
                                name+=firstName;
                            }
                            address1 = UTILS.trim(FTD_XML.selectNodeText(records[idx],'address_1'));
                            address2 = UTILS.trim(FTD_XML.selectNodeText(records[idx],'address_2'));
                            if( address2!=null && address2.length>0 ) {
                                address1 = address1+address2;
                            }
                            city = UTILS.trim(FTD_XML.selectNodeText(records[idx],'city'));
                            state = UTILS.trim(FTD_XML.selectNodeText(records[idx],'state'));
                            zipCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'zip_code'));
                            countryId = UTILS.trim(FTD_XML.selectNodeText(records[idx],'country'));
                            emailAddress = UTILS.trim(FTD_XML.selectNodeText(records[idx],'email_address'));
                            subscribe = (UTILS.trim(FTD_XML.selectNodeText(records[idx],'subscribe_status'))=='Subscribe'?'true':'false');
                            membershipId = UTILS.trim(FTD_XML.selectNodeText(records[idx],'membership_id'));
                            membershipFirstName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'membership_first_name'));
                            membershipLastName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'membership_last_name'));
    
                            if( countryId=='US' && zipCode!=null && zipCode.length>5 ) {
                                zipCode = zipCode.substr(0,5);
                            } else if( countryId=='CA' && zipCode!=null && zipCode.length>3 ) {
                                zipCode = zipCode.substr(0,3);
                            }
                            
                            var newRow = tbody.insertRow(tbody.rows.length);
                            if( tbody.rows.length%2==0 ) {
                                newRow.className = 'alternate';
                            }
                    
                            var cell = newRow.insertCell(colCnt++);
                            cell.align = "center";
                            var input = new FTD_DOM.InputElement( {
                                id:'tspRadioGroup'+'___'+idx,
                                type:'checkbox', 
                                name:'tspRadioGroup',
                                onClick:'TEXT_SEARCH.phoneSelected(\''+target+'\','+idx+');'} ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneNumber___'+idx;
                            cell.innerHTML = dayPhoneNumber;
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneEvening___'+idx;
                            cell.innerHTML = eveningPhoneNumber;
                            
                            cell = newRow.insertCell(colCnt++);
                            var span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneName___'+idx
                                } ).createNode();
                            span.innerHTML = name;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneFirstName___'+idx,
                                type:'hidden', 
                                value:firstName
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneLastName___'+idx,
                                type:'hidden', 
                                value:lastName
                                } ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneBusiness___'+idx;
                            cell.innerHTML = businessName;
                            
                            cell = newRow.insertCell(colCnt++);
                            span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneAddress___'+idx
                                } ).createNode();
                            span.innerHTML = address1;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneAddressType___'+idx,
                                type:'hidden', 
                                value:addressType
                                } ).createNode();
                            cell.appendChild(input);
                            
                            cell = newRow.insertCell(colCnt++);
                            cell.id='tsPhoneCity___'+idx;
                            cell.innerHTML = city;
                            
                            cell = newRow.insertCell(colCnt++);
                            span = new FTD_DOM.SpanElement( {
                                id:'tsPhoneState___'+idx
                                } ).createNode();
                            span.innerHTML = state;
                            cell.appendChild(span);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneZipCode___'+idx,
                                type:'hidden', 
                                value:zipCode
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneCountry___'+idx,
                                type:'hidden', 
                                value:countryId
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneExt___'+idx,
                                type:'hidden', 
                                value:dayExtension
                                } ).createNode();
                            cell.appendChild(input);
//                            input = new FTD_DOM.InputElement( {
//                                id:'tsPhoneEvening___'+idx,
//                                type:'hidden', 
//                                value:eveningPhoneNumber
//                                } ).createNode();
//                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneEveningExt___'+idx,
                                type:'hidden', 
                                value:eveningExtension
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneEmail___'+idx,
                                type:'hidden', 
                                value:emailAddress
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneSubscribe___'+idx,
                                type:'hidden', 
                                value:subscribe
                                } ).createNode();
                            cell.appendChild(input);
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipId___'+idx,
                                type:'hidden', 
                                value:membershipId
                                } ).createNode();
                            cell.appendChild(input);
                            
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipFirstName___'+idx,
                                type:'hidden', 
                                value:membershipFirstName
                                } ).createNode();
                            cell.appendChild(input);
                            
                            input = new FTD_DOM.InputElement( {
                                id:'tsPhoneMembershipLastName___'+idx,
                                type:'hidden', 
                                value:membershipLastName
                                } ).createNode();
                            cell.appendChild(input);
                        } //End for loop
                    }
                } else { 
                    //Validate mode
                    if( records.length==0 ) {
                        //don't do anything since nothing was found
                    }  else {
                        //Let the user choose to accept or reject
                        $('tspPhone').value = searchPhone;
                        TEXT_SEARCH.openPhoneSearch(target,false);
                        TEXT_SEARCH_AJAX.phoneSearchCallback(transport,true);
                    }
                }
            }
        }
        Table.page($('tspTable'),0);
    }
}
*/

TEXT_SEARCH_AJAX.floristSearch = function(mode) {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tsflResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_FLORIST_SEARCH';
    root.setAttributeNode(attr);
    
    var searchProductId = $('productId').getValue();
    var searchZipCode = $('tsflZip').getValue();
    
    var searchDeliveryDate = $('productDeliveryDate').getValue();
    var ddObj = v_deliveryDates[searchDeliveryDate];
    if( ddObj!=null && !Object.isString(ddObj) ) {
        param = FTD_XML.createElementWithText(doc,'param',searchDeliveryDate);
        attr = doc.createAttribute("name");
        attr.value = 'DELIVERY_DATE';
        param.setAttributeNode(attr);
        root.appendChild(param);
        
        if( ddObj.isDateRange==true ) {
            param = FTD_XML.createElementWithText(doc,'param',UTILS.formatToServerDate(ddObj.endDate));
            attr = doc.createAttribute("name");
            attr.value = 'DATE_RANGE_FLAG';
            param.setAttributeNode(attr);
            root.appendChild(param);
            
        }
    
//        var searchDateRange = ddObj.isDateRange?'true':'false';
//        param = FTD_XML.createElementWithText(doc,'param',searchDateRange);
//        attr = doc.createAttribute("name");
//        attr.value = 'DATE_RANGE_FLAG';
//        param.setAttributeNode(attr);
//        root.appendChild(param);
    }
    
    var param = FTD_XML.createElementWithText(doc,'param',searchProductId);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',searchZipCode);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var sourceCode = $('callDataSourceCode').value;
    param = FTD_XML.createElementWithText(doc,'param',sourceCode);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //Never check product availability without a source code
    if( UTILS.isEmpty(sourceCode)==true ) return;
    
    param = doc.createElement('echo');
    
    //param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    //param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    param.appendChild(FTD_XML.createElementWithText(doc,'product_id',searchProductId));
    param.appendChild(FTD_XML.createElementWithText(doc,'zip_code',searchZipCode));
    param.appendChild(FTD_XML.createElementWithText(doc,'delivery_date',searchDeliveryDate));
    param.appendChild(FTD_XML.createElementWithText(doc,'date_range_flag',UTILS.formatToServerDate(ddObj.endDate)));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.floristSearchCallbackJSON,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

TEXT_SEARCH_AJAX.floristSearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var searchId = jsonContent.result.echo.search_id;
        var searchProductId = jsonContent.result.echo.phone_id;
        var searchZipCode = jsonContent.result.echo.zip_code;
        
        var weight;
        var priority;
        var membershipNumber;
        var name;
        var address;
        var phone;
        var gotoFlag;
        var sundayDelivery;
        var mercuryFlag;
        
        if( searchId==v_textSearchId ) {
            if( jsonContent.result.rs.record!=null ) {
                //Populate the rearch results
                var tbody = $('tsflResultsBody');
                results = makeJSONArray(jsonContent.result.rs.record);
                recordsLength = results.length;
                for(var idx = 0; idx < recordsLength; idx++) {
                    var colCnt=0;
                    weight = UTILS.trim(results[idx].florist_weight);
                    priority = UTILS.trim(results[idx].priority);
                    membershipNumber = UTILS.trim(results[idx].florist_id);
                    name = UTILS.trim(results[idx].florist_name);
                    address = UTILS.trim(results[idx].address);
                    phone = UTILS.trim(results[idx].phone_number);
                    gotoFlag = UTILS.trim(results[idx].goto_flag);
                    sundayDelivery = UTILS.trim(results[idx].sunday_delivery_flag);
                    mercuryFlag = UTILS.trim(results[idx].mercury_flag);
                  
                    var newRow = tbody.insertRow(tbody.rows.length);
                    if( tbody.rows.length%2==0 ) {
                        newRow.className = 'alternate';
                    }
                    var cell = newRow.insertCell(colCnt++);
                    cell.align = "center";
                    var input = new FTD_DOM.InputElement( {
                      id:'tsflRadioGroup'+'___'+idx,
                      type:'checkbox', 
                      name:'tflRadioGroup',
                      onClick:'TEXT_SEARCH.floristSelected('+idx+');'} ).createNode();
                    cell.appendChild(input);
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristWeight___'+idx;
                    cell.innerHTML = weight;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristPriority___'+idx;
                    
                    if (priority == 1)
                    {
                        cell.innerHTML = '<img src="images/primaryFlorist.gif" align="center" width="18px" height="18px"/>';
                    }
                    else if (priority == 2)
                    {
                        cell.innerHTML = '<img src="images/backupFlorist.gif" align="center" width="18px" height="18px"/>';
                    }
                    else
                    {
                        cell.innerHTML = '&nbsp;'
                    }
                                       
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristNumber___'+idx;
                    cell.innerHTML = membershipNumber;
                  
                    cell = newRow.insertCell(colCnt++);
                    var span = new FTD_DOM.SpanElement( {
                      id:'tsFloristAddress___'+idx
                      } ).createNode();
                    span.innerHTML = name+'\r\n'+address;
                    cell.appendChild(span);
                    input = new FTD_DOM.InputElement( {
                      id:'tsFloristName___'+idx,
                      type:'hidden', 
                      value:name
                      } ).createNode();
                      cell.appendChild(input);
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristPhone___'+idx;
                    cell.innerHTML = phone;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristGoTo___'+idx;
                    cell.innerHTML = gotoFlag;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristSunday___'+idx;
                    cell.innerHTML = sundayDelivery;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristMerc___'+idx;
                    cell.innerHTML = mercuryFlag;
                }
            }  
        }
        
        Table.page($('tsflTable'),0);
    }
}

/* replace by JSON
TEXT_SEARCH_AJAX.floristSearchCallback = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        
        var records = XPath.selectNodes('/result/rs[@name="florist-search"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        //var mode = FTD_XML.selectNodeText(echo,'mode');
        //var target = FTD_XML.selectNodeText(echo,'target');
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        var searchProductId = FTD_XML.selectNodeText(echo,'phone_id');
        var searchZipCode = FTD_XML.selectNodeText(echo,'zip_code');
        //var searchDeliveryDate = FTD_XML.selectNodeText(echo,'delivery_date');
        //var searchDateRange = FTD_XML.selectNodeText(echo,'date_range_flag');
        
        var weight;
        var membershipNumber;
        var name;
        var address;
        var phone;
        var gotoFlag;
        var sundayDelivery;
        var mercuryFlag;
        
        if( searchId==v_textSearchId ) {
            if( records!=null ) {
                //Populate the rearch results
                var tbody = $('tsflResultsBody');
                for( idx=0; idx<records.length; idx++ ) {
                    var colCnt=0;
                    weight = UTILS.trim(FTD_XML.selectNodeText(records[idx],'florist-weight'));
                    membershipNumber = UTILS.trim(FTD_XML.selectNodeText(records[idx],'florist-id'));
                    name = UTILS.trim(FTD_XML.selectNodeText(records[idx],'florist-name'));
                    address = UTILS.trim(FTD_XML.selectNodeText(records[idx],'address'));
                    phone = UTILS.trim(FTD_XML.selectNodeText(records[idx],'phone-number'));
                    gotoFlag = UTILS.trim(FTD_XML.selectNodeText(records[idx],'goto-flag'));
                    sundayDelivery = UTILS.trim(FTD_XML.selectNodeText(records[idx],'sunday-delivery-flag'));
                    mercuryFlag = UTILS.trim(FTD_XML.selectNodeText(records[idx],'mercury-flag'));
                  
                    var newRow = tbody.insertRow(tbody.rows.length);
                    if( tbody.rows.length%2==0 ) {
                        newRow.className = 'alternate';
                    }
                    var cell = newRow.insertCell(colCnt++);
                    cell.align = "center";
                    var input = new FTD_DOM.InputElement( {
                      id:'tsflRadioGroup'+'___'+idx,
                      type:'checkbox', 
                      name:'tflRadioGroup',
                      onClick:'TEXT_SEARCH.floristSelected('+idx+');'} ).createNode();
                    cell.appendChild(input);
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristWeight___'+idx;
                    cell.innerHTML = weight;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristNumber___'+idx;
                    cell.innerHTML = membershipNumber;
                  
                    cell = newRow.insertCell(colCnt++);
                    var span = new FTD_DOM.SpanElement( {
                      id:'tsFloristAddress___'+idx
                      } ).createNode();
                    span.innerHTML = name+'\r\n'+address;
                    cell.appendChild(span);
                    input = new FTD_DOM.InputElement( {
                      id:'tsFloristName___'+idx,
                      type:'hidden', 
                      value:name
                      } ).createNode();
                      cell.appendChild(input);
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristPhone___'+idx;
                    cell.innerHTML = phone;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristGoTo___'+idx;
                    cell.innerHTML = gotoFlag;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristSunday___'+idx;
                    cell.innerHTML = sundayDelivery;
                    
                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsFloristMerc___'+idx;
                    cell.innerHTML = mercuryFlag;
                }
            }  
        }
        
        Table.page($('tsflTable'),0);
    }
}
*/

TEXT_SEARCH_AJAX.facilitySearch = function() {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tsfResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_FACILITY_SEARCH';
    root.setAttributeNode(attr);
    
    var searchCity = $('tsfCity').getValue()
    var param = FTD_XML.createElementWithText(doc,'param',searchCity);
    attr = doc.createAttribute("name");
    attr.value = 'CITY';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchState = FTD_DOM.getSelectedValue('tsfState');
    param = FTD_XML.createElementWithText(doc,'param',searchState);
    attr = doc.createAttribute("name");
    attr.value = 'STATE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchAddressType = FTD_DOM.getSelectedValue('tsfSearchType');
    param = FTD_XML.createElementWithText(doc,'param',searchAddressType);
    attr = doc.createAttribute("name");
    attr.value = 'ADDRESS_TYPE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchKeywords = $('tsfKeyword').value;
    param = FTD_XML.createElementWithText(doc,'param',searchKeywords);
    attr = doc.createAttribute("name");
    attr.value = 'KEYWORD';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    
    //param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    //param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    param.appendChild(FTD_XML.createElementWithText(doc,'city',searchCity));
    param.appendChild(FTD_XML.createElementWithText(doc,'state',searchState));
    param.appendChild(FTD_XML.createElementWithText(doc,'address_type',searchAddressType));
    param.appendChild(FTD_XML.createElementWithText(doc,'keyword',searchKeywords));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.facilitySearchCallbackJSON,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    //setProcessing(true);
    newRequest.send();
}

TEXT_SEARCH_AJAX.facilitySearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var searchId = jsonContent.result.echo.search_id;
        var name;
        var address;
        var city;
        var state;
        var zipCode;
        var phoneNumber;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tsfResultsBody');
            
            if( jsonContent.result.rs.record==null ) {
                //TEXT_SEARCH.openFacilitySearch(true);
                $('tsNoResults').style.display = 'block';
            } else {
                    //Populate the rearch results
                    results = makeJSONArray(jsonContent.result.rs.record);
                    recordsLength = results.length;
                    for(var idx = 0; idx < recordsLength; idx++) {
                        var colCnt=0;
                        name = UTILS.trim(results[idx].name);
                        address = UTILS.trim(results[idx].address);
                        city = UTILS.trim(results[idx].city);
                        state = UTILS.trim(results[idx].state);
                        zipCode = UTILS.trim(results[idx].zip_code);
                        phoneNumber = UTILS.trim(results[idx].phone_number);
                        
                        var newRow = tbody.insertRow(tbody.rows.length);
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                    
                        var cell = newRow.insertCell(colCnt++);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tsfRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tsfRadioGroup',
                            onClick:'TEXT_SEARCH.facilitySelected('+idx+');'} ).createNode();
                        cell.appendChild(input);
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacName___'+idx;
                        cell.innerHTML = name;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacAddress___'+idx;
                        cell.innerHTML = address;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacCity___'+idx;
                        cell.innerHTML = city;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacState___'+idx;
                        cell.innerHTML = state;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacZipCode___'+idx;
                        cell.innerHTML = zipCode;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacPhone___'+idx;
                        cell.innerHTML = phoneNumber;
                }
            }
        }
        
        Table.page($('tsfTable'),0);
    }
    TEXT_SEARCH.showDone();
}

/* replace by JSON
TEXT_SEARCH_AJAX.facilitySearchCallback = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        
        var records = XPath.selectNodes('/result/rs[@name="facilities"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        //var mode = FTD_XML.selectNodeText(echo,'mode');
//        var target = FTD_XML.selectNodeText(echo,'target');
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        var name;
        var address;
        var city;
        var state;
        var zipCode;
        var phoneNumber;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tsfResultsBody');
            
            if( records==null ) {
                //TEXT_SEARCH.openFacilitySearch(true);
                $('tsNoResults').style.display = 'block';
            } else {
                if( records.length==0 ) {
                    //notify user that there were no matching records
                    $('tsNoResults').style.display='block';
                } else {
                    //Populate the rearch results
                    for( idx=0; idx<records.length; idx++ ) {
                        var colCnt=0;
                        name = UTILS.trim(FTD_XML.selectNodeText(records[idx],'name'));
                        address = UTILS.trim(FTD_XML.selectNodeText(records[idx],'address'));
                        city = UTILS.trim(FTD_XML.selectNodeText(records[idx],'city'));
                        state = UTILS.trim(FTD_XML.selectNodeText(records[idx],'state'));
                        zipCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'zip-code'));
                        phoneNumber = UTILS.trim(FTD_XML.selectNodeText(records[idx],'phone-number'));
                        
                        var newRow = tbody.insertRow(tbody.rows.length);
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                    
                        var cell = newRow.insertCell(colCnt++);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tsfRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tsfRadioGroup',
                            onClick:'TEXT_SEARCH.facilitySelected('+idx+');'} ).createNode();
                        cell.appendChild(input);
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacName___'+idx;
                        cell.innerHTML = name;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacAddress___'+idx;
                        cell.innerHTML = address;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacCity___'+idx;
                        cell.innerHTML = city;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacState___'+idx;
                        cell.innerHTML = state;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacZipCode___'+idx;
                        cell.innerHTML = zipCode;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsFacPhone___'+idx;
                        cell.innerHTML = phoneNumber;
                    }
                }
            }
        }
        
        Table.page($('tsfTable'),0);
    }
    TEXT_SEARCH.showDone();
}
*/

TEXT_SEARCH_AJAX.sourceCodeSearch = function() {
    TEXT_SEARCH.showStart();
    FTD_DOM.removeAllTableRows($('tssSourceCodeResultsBody'));
    FTD_DOM.removeAllTableRows($('tssIOTWResultsBody'));
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_SOURCE_CODE_SEARCH';
    root.setAttributeNode(attr);
    
    var searchType = FTD_DOM.getSelectedValue('tssSearchType');
    var param = FTD_XML.createElementWithText(doc,'param',searchType);
    attr = doc.createAttribute("name");
    attr.value = 'SEARCH_TYPE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchKeywords = $('tssKeyword').value;
    param = FTD_XML.createElementWithText(doc,'param',searchKeywords);
    attr = doc.createAttribute("name");
    attr.value = 'KEYWORD';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchParam = $('tssIotwSourceCode').value;
    param = FTD_XML.createElementWithText(doc,'param',searchParam);
    attr = doc.createAttribute("name");
    attr.value = 'IOTW_SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchParam = $('tssMasterSourceCode').value;
    param = FTD_XML.createElementWithText(doc,'param',searchParam);
    attr = doc.createAttribute("name");
    attr.value = 'SOURCE_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchParam = $('tssProductId').value;
    param = FTD_XML.createElementWithText(doc,'param',searchParam);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchParam = FTD_DOM.getSelectedValue('tssDiscount');
    param = FTD_XML.createElementWithText(doc,'param',searchParam);
    attr = doc.createAttribute("name");
    attr.value = 'PRICE_HEADER_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    var searchCompany = $('callDataCompany').value;
    param = FTD_XML.createElementWithText(doc,'param',searchCompany);
    attr = doc.createAttribute("name");
    attr.value = 'COMPANY_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    //param.appendChild(FTD_XML.createElementWithText(doc,'mode',mode));
    //param.appendChild(FTD_XML.createElementWithText(doc,'target',target));
    //param.appendChild(FTD_XML.createElementWithText(doc,'search_type',searchType));
    //param.appendChild(FTD_XML.createElementWithText(doc,'keyword',searchKeywords));
    //param.appendChild(FTD_XML.createElementWithText(doc,'company',searchCompany));
    v_textSearchId = new Date().getTime();
    param.appendChild(FTD_XML.createElementWithText(doc,'search_id',v_textSearchId+''));
    root.appendChild(param);
    
    //alert(FTD_XML.toString(doc));
    
    //XMLHttpRequest
    var newRequest
    if( searchType=='SOURCE_CODE' ) {
        newRequest = 
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.sourceCodeSearchCallbackJSON,false,false);
    }
    else {
        newRequest = 
            new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,TEXT_SEARCH_AJAX.iotwSearchCallbackJSON,false,false);
    }
        
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

TEXT_SEARCH_AJAX.sourceCodeSearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        
        var searchId = jsonContent.result.echo.search_id;
        var sourceCode;
        var description;
        var type;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tssSourceCodeResultsBody');
            
            if (jsonContent.result.rs.record == null) {
                $('tsNoResults').style.display = 'block';
            } else {
                //Populate the rearch results
                results = makeJSONArray(jsonContent.result.rs.record);
                recordsLength = results.length;
                for(var idx = 0; idx < recordsLength; idx++) {
                    var colCnt=0;
                    sourceCode = UTILS.trim(results[idx].source_code);
                    description = UTILS.trim(results[idx].description);
                    type = UTILS.trim(results[idx].source_type);

                    var newRow = tbody.insertRow(tbody.rows.length);
                    if( tbody.rows.length%2==0 ) {
                        newRow.className = 'alternate';
                    }

                    var cell = newRow.insertCell(colCnt++);
                    cell.align = "center";
                    var input = new FTD_DOM.InputElement( {
                        id:'tssRadioGroup'+'___'+idx,
                        type:'checkbox', 
                        name:'tssRadioGroup',
                        onClick:'TEXT_SEARCH.sourceCodeSelected('+idx+');'} ).createNode();
                    cell.appendChild(input);

                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsSourceCode___'+idx;
                    cell.innerHTML = sourceCode;

                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsSourceDesc___'+idx;
                    cell.innerHTML = description;

                    cell = newRow.insertCell(colCnt++);
                    cell.id='tsSourceDesc___'+idx;
                    cell.innerHTML = type;
                }
            }
        }
        
        Table.page($('tssTable'),0);
    }
}

TEXT_SEARCH_AJAX.iotwSearchCallbackJSON = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var jsonRaw = transport.responseText;
    var jsonContent = eval("(" + jsonRaw + ")");
    
    if( jsonContent!=null ) {
        var searchId = jsonContent.result.echo.search_id;
        //var searchType = FTD_XML.selectNodeText(echo,'searchType');
        var sourceCode;
        var sourceCodeDesc;
        var iotwSourceCode;
        var iotwSourceCodeDesc;
        var productId;
        var productName;
        var standardPrice;
        var discount;
        var productPageMessage;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tssIOTWResultsBody');
            
            if (jsonContent.result.rs.record == null) {
                $('tsNoResults').style.display = 'block';
            } else {
                //Populate the rearch results
                results = makeJSONArray(jsonContent.result.rs.record);
                recordsLength = results.length;
                for(var idx = 0; idx < recordsLength; idx++) {
                        var colCnt=0;
                        sourceCode = UTILS.trim(results[idx].source_code);
                        sourceCodeDesc = UTILS.trim(results[idx].source_description);
                        iotwSourceCode = UTILS.trim(results[idx].iotw_source_code);
                        iotwSourceCodeDesc = UTILS.trim(results[idx].iotw_source_description);
                        productId = UTILS.trim(results[idx].product_id);
                        productName = UTILS.trim(results[idx].product_name);
                        standardPrice = UTILS.trim(results[idx].standard_price);
                        discount = UTILS.trim(results[idx].discount);
                        productPageMessage = UTILS.trim(results[idx].product_page_message);
                        
                        var newRow = tbody.insertRow(tbody.rows.length);                        
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                        
                        var cell = newRow.insertCell(colCnt++);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tssIOTWRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tssITOWRadioGroup',
                            onClick:'TEXT_SEARCH.iotwSelected('+idx+');'} ).createNode();
                        cell.appendChild(input);

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWMasterSourceCode___'+idx;
                        cell.innerHTML = sourceCode;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWMasterSourceCodeDesc___'+idx;
                        cell.innerHTML = sourceCodeDesc;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWSourceCodeDesc___'+idx;
                        cell.innerHTML = iotwSourceCodeDesc;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWProductId___'+idx;
                        cell.innerHTML = productId;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWProductName___'+idx;
                        cell.innerHTML = productName;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWStandardPrice___'+idx;
                        cell.innerHTML = standardPrice;

                        cell = newRow.insertCell(colCnt++);
                        span = new FTD_DOM.SpanElement( {
                            id:'tsIOTWDiscount___'+idx
                            } ).createNode();
                        span.innerHTML = discount;
                        cell.appendChild(span);
                        input = new FTD_DOM.InputElement( {
                            id:'tsIOTWSourceCode___'+idx,
                            type:'hidden', 
                            value:iotwSourceCode
                            } ).createNode();
                        cell.appendChild(input);
                        input = new FTD_DOM.InputElement( {
                            id:'tsIOTWProductMsg___'+idx,
                            type:'hidden', 
                            value:productPageMessage
                            } ).createNode();
                        cell.appendChild(input);
                }
            }
        }
        
        Table.page($('tsiTable'),0);

    }
}

/* replace by JSON
TEXT_SEARCH_AJAX.sourceCodeSearchCallback = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="source_master"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        //var searchType = FTD_XML.selectNodeText(echo,'searchType');
        var sourceCode;
        var description;
        var type;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tssSourceCodeResultsBody');
            
            if( records==null ) {
                $('tsNoResults').style.display = 'block';
            } else {
                if( records.length==0 ) {
                    //notify user that there were no matching records
                    $('tsNoResults').style.display='block';
                } else {
                    //Populate the rearch results
                    for( idx=0; idx<records.length; idx++ ) {
                        var colCnt=0;
                        sourceCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'source-code'));
                        description = UTILS.trim(FTD_XML.selectNodeText(records[idx],'description'));
                        type = UTILS.trim(FTD_XML.selectNodeText(records[idx],'source-type'));
                        
                        var newRow = tbody.insertRow(tbody.rows.length);
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                    
                        var cell = newRow.insertCell(colCnt++);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tssRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tssRadioGroup',
                            onClick:'TEXT_SEARCH.sourceCodeSelected('+idx+');'} ).createNode();
                        cell.appendChild(input);
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsSourceCode___'+idx;
                        cell.innerHTML = sourceCode;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsSourceDesc___'+idx;
                        cell.innerHTML = description;
                        
                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsSourceDesc___'+idx;
                        cell.innerHTML = type;
                    }
                }
            }
        }
        
        Table.page($('tssTable'),0);
    }
}
*/

/* replace by JSON
TEXT_SEARCH_AJAX.iotwSearchCallback = function(transport) {
    //alert(transport.responseText);
    TEXT_SEARCH.showDone();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var records = XPath.selectNodes('/result/rs[@name="iotw"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var searchId = FTD_XML.selectNodeText(echo,'search_id');
        //var searchType = FTD_XML.selectNodeText(echo,'searchType');
        var sourceCode;
        var sourceCodeDesc;
        var iotwSourceCode;
        var iotwSourceCodeDesc;
        var productId;
        var productName;
        var standardPrice;
        var discount;
        var productPageMessage;
        
        if( searchId==v_textSearchId ) {
            var tbody = $('tssIOTWResultsBody');
            
            if( records==null ) {
                $('tsNoResults').style.display = 'block';
            } else {
                if( records.length==0 ) {
                    //notify user that there were no matching records
                    $('tsNoResults').style.display='block';
                } else {
                    //Populate the rearch results
                    for( idx=0; idx<records.length; idx++ ) {
                        var colCnt=0;
                        sourceCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'source-code'));
                        sourceCodeDesc = UTILS.trim(FTD_XML.selectNodeText(records[idx],'source-description'));
                        iotwSourceCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'iotw-source-code'));
                        iotwSourceCodeDesc = UTILS.trim(FTD_XML.selectNodeText(records[idx],'iotw-source-description'));
                        productId = UTILS.trim(FTD_XML.selectNodeText(records[idx],'product-id'));
                        productName = UTILS.trim(FTD_XML.selectNodeText(records[idx],'product-name'));
                        standardPrice = UTILS.trim(FTD_XML.selectNodeText(records[idx],'standard-price'));
                        discount = UTILS.trim(FTD_XML.selectNodeText(records[idx],'discount'));
                        productPageMessage = UTILS.trim(FTD_XML.selectNodeText(records[idx],'product-page-message'));
                        
                        var newRow = tbody.insertRow(tbody.rows.length);                        
                        if( tbody.rows.length%2==0 ) {
                            newRow.className = 'alternate';
                        }
                        
                        var cell = newRow.insertCell(colCnt++);
                        cell.align = "center";
                        var input = new FTD_DOM.InputElement( {
                            id:'tssIOTWRadioGroup'+'___'+idx,
                            type:'checkbox', 
                            name:'tssITOWRadioGroup',
                            onClick:'TEXT_SEARCH.iotwSelected('+idx+');'} ).createNode();
                        cell.appendChild(input);

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWMasterSourceCode___'+idx;
                        cell.innerHTML = sourceCode;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWMasterSourceCodeDesc___'+idx;
                        cell.innerHTML = sourceCodeDesc;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsIOTWSourceCodeDesc___'+idx;
                        cell.innerHTML = iotwSourceCodeDesc;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWProductId___'+idx;
                        cell.innerHTML = productId;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWProductName___'+idx;
                        cell.innerHTML = productName;

                        cell = newRow.insertCell(colCnt++);
                        cell.id='tsITOWStandardPrice___'+idx;
                        cell.innerHTML = standardPrice;

                        cell = newRow.insertCell(colCnt++);
                        span = new FTD_DOM.SpanElement( {
                            id:'tsIOTWDiscount___'+idx
                            } ).createNode();
                        span.innerHTML = discount;
                        cell.appendChild(span);
                        input = new FTD_DOM.InputElement( {
                            id:'tsIOTWSourceCode___'+idx,
                            type:'hidden', 
                            value:iotwSourceCode
                            } ).createNode();
                        cell.appendChild(input);
                        input = new FTD_DOM.InputElement( {
                            id:'tsIOTWProductMsg___'+idx,
                            type:'hidden', 
                            value:productPageMessage
                            } ).createNode();
                        cell.appendChild(input);
                    }
                }
            }
        }
    }
}
*/

// ]]>
