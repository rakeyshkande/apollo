// <![CDATA[
var VALIDATE_AJAX = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || VALIDATE_AJAX.prototypeVersion < 1.6)
      throw("VALIDATE_AJAX requires the Prototype JavaScript framework >= 1.6");

VALIDATE_AJAX.validateFlorist = function() {
    FTD_DOM.removeAllTableRows($('tsflResultsBody'));
      
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_FLORIST_ID';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',$('floristId').value);
    attr = doc.createAttribute("name");
    attr.value = 'FLORIST_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',$('productId').value);
    attr = doc.createAttribute("name");
    attr.value = 'PRODUCT_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',$('productZip').value);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',$('productDeliveryDate').value);
    attr = doc.createAttribute("name");
    attr.value = 'DELIVERY_DATE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,VALIDATE_AJAX.validateFloristCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

VALIDATE_AJAX.validateFloristCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var el = $('floristId');
        var record = XPath.selectNodes('/result/rs[@name="validation"]',xmlDoc)[0];
        if( FTD_XML.getAttributeText(record,"status")=='Y' ) {
            $('floristDesc').innerHTML = 'Selected florist can fulfill this order';
            el.style.backgroundColor = v_normalBgColor;
            el.style.color = v_normalTextColor;
            el.style.fontWeight = 'normal';
        } else {
            var errors = XPath.selectNodes('/result/rs[@name="validation"]/record/validate[@status="N"]',xmlDoc);
            var message = UTILS.boldWrapper('Florist validation error:')+'<br><br>';
            for( idx=0; idx<errors.length; idx++ ) {
                var errContext = FTD_XML.getAttributeText(errors[idx],'name');
                var errMsg = FTD_XML.getAttributeText(errors[idx],'message');
                message += UTILS.boldWrapper(errContext)+':&nbsp;&nbsp;';
                message += errMsg+'<br><br>';
            }
            $('floristDesc').innerHTML = '';
            el.style.backgroundColor = v_errorBgColor;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
            el.disabled=false;
            notifyUser(message,false,true);        }
    }
}

VALIDATE_AJAX.validateGiftCertificate = function(giftCertificateNumber) {
    //alert('Source code validation will happen now');
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_GIFT_CERTIFICATE';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',giftCertificateNumber);
    attr = doc.createAttribute("name");
    attr.value = 'CERTIFICATE_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,VALIDATE_AJAX.validateGiftCertificateCallback,true,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

VALIDATE_AJAX.validateGiftCertificateCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var el = $('paymentGCNumber');
        var record;
        var strVal;
        
        if( checkResultSetStatus(xmlDoc,"gift_certificate",false,false,false) ) {
            record = XPath.selectNodes('/result/rs[@name="gift_certificate"]/record',xmlDoc)[0];
            if( record==null ) {
                el.style.backgroundColor = v_errorBgColor;
                el.style.color = v_errorTextColor;
                el.style.fontWeight = 'bold';
                el.disabled=false;
                $('paymentGCAmount').innerHTML='0.00';
                $('paymentCCAmount').innerHTML='0.00';
                $('paymentNCAmount').innerHTML='0.00';
                //el.focus();
            } else {
                el.style.backgroundColor = v_normalBgColor;
                el.style.color = v_normalTextColor;
                el.style.fontWeight = 'normal';
                $('paymentGCAmount').innerHTML = UTILS.formatStringToPrice(FTD_XML.selectNodeText(record,'certificate-amount'));
                
                setPaymentTypeAmounts();
            }
        } else {
            el.style.backgroundColor = v_errorBgColor;
            el.style.color = v_errorTextColor;
            el.style.fontWeight = 'bold';
            $('paymentGCAmount').innerHTML='0.00';
            $('paymentCCAmount').innerHTML='0.00';
            $('paymentNCAmount').innerHTML='0.00';
        }
    }
} 

VALIDATE_AJAX.validateAddress = function(itemidx) {
	
	logMessage('verifying address');
    var countryId = $('recipientCountry').getValue();
    if( countryId!='US' ) {
        return;
    }
    
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_AVS';
    root.setAttributeNode(attr);
    
    var firmName = $('recipientBusiness').getValue();
    var address = $('recipientAddress').getValue();
    var city = $('recipientCity').getValue();
    var state = $('recipientState').getValue();
    var zipCode = $('recipientZip').getValue();
    
    
    var param = FTD_XML.createElementWithText(doc,'param',firmName);
    attr = doc.createAttribute("name");
    attr.value = 'FIRM_NAME';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',address);
    attr = doc.createAttribute("name");
    attr.value = 'ADDRESS';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',city);
    attr = doc.createAttribute("name");
    attr.value = 'CITY';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',state);
    attr = doc.createAttribute("name");
    attr.value = 'STATE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',zipCode);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = FTD_XML.createElementWithText(doc,'param',countryId);
    attr = doc.createAttribute("name");
    attr.value = 'COUNTRY_ID';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    //florist vs. dropship  F. for florist.  D. for Dropship.
    var orderObj = v_cartList[itemidx];
    var floristOrDropship;
    if (orderObj.carrierDelivered == true && orderObj.floristDelivered == false) {
    	floristOrDropship = "D";
    } else {
    	floristOrDropship = "F";
    }
    param = FTD_XML.createElementWithText(doc,'param',floristOrDropship);
    attr = doc.createAttribute("name");
    attr.value = 'ORDER_TYPE';
    param.setAttributeNode(attr);
    root.appendChild(param)
        
    param = doc.createElement('echo');
    param.appendChild(FTD_XML.createElementWithText(doc,'itemidx',itemidx+''));
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,VALIDATE_AJAX.validateAddressCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

/**
 * Receive a XML response with a list of 'valid' addresses
 */
VALIDATE_AJAX.validateAddressCallback = function (transport) {
	logMessage('verifying address');
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var itemIdx = FTD_XML.selectNodeText(echo,'itemidx');
        var result = XPath.selectNodes('/result/rs[@name="avs"]',xmlDoc)[0];
        var records = XPath.selectNodes('/result/rs[@name="avs"]/record',xmlDoc);
        //save the avsAddresses list to the cart so we can send one in the POST when we submit the cart.
        v_cartList[itemIdx].avsAddresses = records;
        if( FTD_XML.getAttributeText(result,"status")=='Y' ) {
	        //iterate through the records and update the recipient address based on results
	        //if no results, notify user 'unable to validate address'
	        //if 1 fail result, notify user 'unable to validate address'
	        //if 1 pass result, update the address for the user
	        //if >0 pass result, draw the UI for the user to choose the correct address
	        if (records.length ===0) {
	        	notifyUser('Unable to validate the recipient address.  Ask the customer to confirm the address.',false,true);
	        	$('avCustomerInsisted').checked=true;
	        } else if (records.length ===1) {
	        	var record = records[0];
	        	var verification_result = FTD_XML.selectNodeText(records[0],'recip-address-verification-result');
	        	if (verification_result === "FAIL") {
	        		//1 fail result scenario
	        		notifyUser('Unable to validate the recipient address.  Ask the customer to confirm the address.',false,true);
	        		$('avCustomerInsisted').checked=true;
	        	} else if (verification_result === "PASS") {
	        		//1 passing result scenario        		
	        		//populate the recipient address with our AVS result
	        		 $('avNeedsResolution').checked=false;
	                 $('avCustomerInsisted').checked=false;
	                 var zip = UTILS.parseZip(UTILS.trim(FTD_XML.selectNodeText(record,'avs-postal-code')), v_cartList[v_currentCartIdx].recipientCountry);
	                 if( v_currentCartIdx==itemIdx ) {
	                     $('recipientAddress').value = UTILS.trim(FTD_XML.selectNodeText(record,'avs-street-address'));
	                     if($('recipientAddress2').value != "" && $('recipientAddress2').value != null  && $('recipientAddress').value.indexOf($('recipientAddress2').value) < 0
	                    		 && $('recipientAddress').value.indexOf($('recipientOriginalAddress1').value) == 0){
	                    	 $('recipientAddress').value = $('recipientAddress').value +" "+ $('recipientAddress2').value;
	                    	 
	                     }
	                     $('recipientCity').value = UTILS.trim(FTD_XML.selectNodeText(record,'avs-city'));
	                     FTD_DOM.selectOptionByValue('recipientState',UTILS.trim(FTD_XML.selectNodeText(record,'avs-state')));
	                     $('recipientZip').value = zip;
	                 } else {
	                	 var orderObj = v_cartList[itemIdx];
		                 orderObj.recipientAddress = UTILS.trim(FTD_XML.selectNodeText(record,'avs-street-address'));
		                 if($('recipientAddress2').value != "" && $('recipientAddress2').value != null && orderObj.recipientAddress.indexOf($('recipientAddress2').value) < 0
		                		 && orderObj.recipientAddress.indexOf($('recipientOriginalAddress1').value) == 0){
		                	 orderObj.recipientAddress = orderObj.recipientAddress +" "+ $('recipientAddress2').value;
	                    	 
	                     }
		                 orderObj.recipientCity = UTILS.trim(FTD_XML.selectNodeText(record,'avs-city'));
		                 orderObj.recipientState = UTILS.trim(FTD_XML.selectNodeText(record,'avs-state'));
		                 orderObj.recipientZip = zip;
	                 }
	        	} else {
	        		//we got something other than PASS or FAIL.  not a supported response
	        		alert('recieved unexpected Address Verification result: ' + verification_result );
	        	}
	        } else {
	        	//there are multiple results which should mean that they are all valid.  show them to the user to pick which one
	        	if( v_currentCartIdx==itemIdx ) {
	        		$('avNeedsResolution').checked=true;
	        	    
	        		for (var i=0; i<records.length;i++) {
	        		    var record = records[i];
	        		    $('addrDiv_'+i).show();
	                    $('avSuggestedAddress_'+i).innerHTML = UTILS.trim(FTD_XML.selectNodeText(record,'avs-street-address'));
	                    $('avSuggestedCity_'+i).innerHTML = UTILS.trim(FTD_XML.selectNodeText(record,'avs-city'));
	                    $('avSuggestedState_'+i).innerHTML = UTILS.trim(FTD_XML.selectNodeText(record,'avs-state'));
	                    $('avSuggestedZip_'+i).innerHTML = UTILS.trim(FTD_XML.selectNodeText(record,'avs-postal-code'));
	                }
	        		JOE_ORDER.openAddressVerificationPanel();
	                
	                
	        	} else {
	                	//what is this for?
	                	alert('validated address on an item other than the current item');
	                    var orderObj = v_cartList[itemIdx];
	                    //orderObj.avResultCode = matchCode;
	            }
	        }
	        
	        //bind some event handler to accept button?
	        
	        //this will tell the 'shouldValidate' method to skip address verification (since we just finished it)
	        $('avsPerformed').checked = true;
	        
	    }  //end status=Y check
        else {
        	logMessage("avs is disabled for this item");
        	//status is not Y.  Something went wrong with the verification, or it is turned off.
        	//We are still going to keep the AVS data we got but we are not going to update the recipient address
        	
        	//setting the avDisabled flag will ensure that the proper xml values are set when we submit the order
        	$('avDisabled').checked = true;
        }
    }
    
    if( v_checkAddressTimer.isRunning()==false ) v_checkAddressTimer.startTimer();   
}

VALIDATE_AJAX.validateZipElement = function(zipEl) {
    if( Object.isString(zipEl) ) {
        zipEl = $(zipEl);
    }
    
    if( zipEl==null ) {
        return;
    }
    
    FTD_DOM.removeAllTableRows($('tszResultsBody'));
    
    var zipStr = zipEl.getValue();
    
    var doc = FTD_XML.createXmlDocument("request");
    var root = doc.documentElement;
    var attr = doc.createAttribute("type");
    attr.value = 'AJAX_REQUEST_VALIDATE_ZIP_CODE';
    root.setAttributeNode(attr);
    
    var param = FTD_XML.createElementWithText(doc,'param',zipStr);
    attr = doc.createAttribute("name");
    attr.value = 'ZIP_CODE';
    param.setAttributeNode(attr);
    root.appendChild(param);
    
    param = doc.createElement('echo');
    
    param.appendChild(FTD_XML.createElementWithText(doc,'target',zipEl.id));
    root.appendChild(param);
    
    //XMLHttpRequest
    var newRequest = 
        new FTD_AJAX.Request(v_serverURLPrefix+'oeAjax.do',FTD_AJAX_REQUEST_TYPE_POST,VALIDATE_AJAX.validateZipElementCallback,false,false);
    newRequest.addParam("AJAX_XML",FTD_XML.toString(doc));
    newRequest.send();
}

VALIDATE_AJAX.validateZipElementCallback = function(transport) {
    var xmlDoc = parseResponse(transport);
    
    if( xmlDoc!=null ) {
        
        var records = XPath.selectNodes('/result/rs[@name="zip_code"]/record',xmlDoc);
        var echo = XPath.selectNodes('/result/echo',xmlDoc)[0];
        var target = FTD_XML.selectNodeText(echo,'target'); 
        var zipCode;
        var city;
        var state;
        var countryId;
            
        if( !TEXT_SEARCH.checkResultSetStatus(xmlDoc,"zip_code",true) ) {
            notifyUser(TEXT_SEARCH.getResultSetStatusMessage(xmlDoc,"zip_code"),false,true);
        } else if( records!=null ) {
            if( records.length==0 ) {
                //not valid, open up zip search
                //TEXT_SEARCH.openZipSearch(target,true,false);
                notifyUser('Failed to validate zip.',false,true);
            } else if( records.length==1 ) {
                //Populate target fields
                zipCode = FTD_XML.selectNodeText(records[0],'zip_code_id');
                city = FTD_XML.selectNodeText(records[0],'city');
                state = FTD_XML.selectNodeText(records[0],'state_id');
                countryId = findStateById(state).countryId;
                if( target=='productZip' ) {
                    $('recipientCity').value = city;
                    ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                    ORDER_EVENTS.changeProductZip(zipCode,countryId);
                } else if( target=='recipientZip' ) {
                    $('recipientCity').value=city;
                    ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
                    ORDER_EVENTS.changeProductZip(zipCode,countryId);
                } else if( target=='customerZip' ) {
                    CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
                    $('customerCity').value=city;
                }
            } else {
                //More than one result returned, open up zip search and populate the fields
                var tbody = $('tszResultsBody');
                for( idx=0; idx<records.length; idx++ ) {
                    zipCode = UTILS.trim(FTD_XML.selectNodeText(records[idx],'zip_code_id'));
                    city = UTILS.trim(FTD_XML.selectNodeText(records[idx],'city'));
                    state = UTILS.trim(FTD_XML.selectNodeText(records[idx],'state_id'));
                    
                    var newRow = tbody.insertRow(tbody.rows.length);
                    if( tbody.rows.length%2==0 ) {
                        newRow.className = 'alternate';
                    }
                        
                    var cell = newRow.insertCell(0);
                    cell.align = "center";
                    var input = new FTD_DOM.InputElement( {
                        id:'tszRadioGroup'+'___'+idx,
                        type:'checkbox', 
                        name:'tszRadioGroup',
                        onClick:'TEXT_SEARCH.zipSelected(\''+target+'\','+idx+');'} ).createNode();
                    cell.appendChild(input);
                    
                    cell = newRow.insertCell(1);
                    cell.id='tsZipCity___'+idx;
                    cell.innerHTML = city;
                    
                    cell = newRow.insertCell(2);
                    cell.id='tsZipState___'+idx;
                    cell.innerHTML = state;
                    
                    cell = newRow.insertCell(3);
                    cell.id='tsZipCode___'+idx;
                    cell.innerHTML = zipCode;
                }
                TEXT_SEARCH.openZipSearch(target,false,false);
            }
        }
    }
}

// ]]>