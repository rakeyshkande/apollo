// <![CDATA[
var PAYMENT_TYPE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("PAYMENT_TYPE requires the Prototype JavaScript framework >= 1.5");
      
PAYMENT_TYPE.OBJECT = Class.create();

PAYMENT_TYPE.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.id=null;
        this.display=null;
//        this.allowedMethods=new Array();
    }
};
// ]]>