// <![CDATA[
var PRICE_HEADER = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("PRICE_HEADER requires the Prototype JavaScript framework >= 1.5");
      
PRICE_HEADER.OBJECT = Class.create();

PRICE_HEADER.OBJECT.prototype = {

    initialize: function(comboPrompt,comboValue) {
        this.reset();
        if(comboPrompt) {
            this.comboPrompt=comboValue + ' - ' + comboPrompt;
        }
        
        if(comboValue) {
            this.comboValue=comboValue;
        }
    },
    
    reset: function() {
        this.comboPrompt=null;
        this.comboValue=null;
    }
};
// ]]>