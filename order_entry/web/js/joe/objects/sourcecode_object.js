// <![CDATA[
var SOURCECODE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("SOURCECODE requires the Prototype JavaScript framework >= 1.5");
      
SOURCECODE.OBJECT = Class.create();

SOURCECODE.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.id = null;
        this.description = null;
        this.partnerId = null;
        this.membershipDataRequired = false;
        this.paymentMethod = null;
        this.password = null;
        this.iotwFlag = false;
        this.billingInfo = new Array();
        this.allowGCPayment =false; //added for Defect 4432
        this.resourceId = null;
        this.recpt_location_type = null;
        this.recpt_business_name = null;
        this.recpt_location_detail = null;
        this.recpt_address = null;
        this.recpt_zip_code = null;
        this.recpt_city = null;
        this.recpt_state_id = null;
        this.recpt_country_id = null;
        this.recpt_phone = null;
        this.recpt_phone_ext = null;
        this.cust_first_name = null;
        this.cust_last_name = null;
        this.cust_daytime_phone = null;
        this.cust_daytime_phone_ext = null;
        this.cust_evening_phone = null;
        this.cust_evening_phone_ext = null;
        this.cust_address = null;
        this.cust_zip_code = null;
        this.cust_city = null;
        this.cust_state_id = null;
        this.cust_country_id = null;
        this.cust_email_address = null;
        this.allowFreeShippingFlag = null;
        this.sourceType= null;
    }
};
// ]]>