// <![CDATA[
var IOTWOE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.6)
      throw("IOTWOE requires the Prototype JavaScript framework >= 1.6");
      
IOTWOE.OBJECT = Class.create();

IOTWOE.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.flag=false;
        this.iotwSourceCode=null;
        this.sourceCode=null;
        this.message=null;
        this.discountType=null;
        this.standardDiscount=null;
        this.premiumDiscount=null;
        this.deluxeDiscount=null;
        this.deliveryDates=new Array();
    },

    doesIOTWPricingApply: function(deliveryDate) {
        var retval = false;
    
        if( this.flag==true ) {
            if( UTILS.hasEntries(this.deliveryDates)==false ) {
                //No delivery date restrictions
                retval = true;
            } else {
                //if( UTILS.isEmpty(orderObject.productDeliveryDate)==false && productObject!=null ) {
                if( UTILS.isEmpty(deliveryDate)==false ) {
                    //var ddIOTW = 'dd'+orderObject.productDeliveryDate;
                    var ddIOTW = 'dd'+deliveryDate;
                    ddIOTW = this.deliveryDates[ddIOTW];
                    if( UTILS.isEmpty(ddIOTW)==false ) retval=true;
                }
            }
        }
        return retval;
    }
};
// ]]>