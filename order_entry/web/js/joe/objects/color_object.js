// <![CDATA[
var COLOR = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("COLOR requires the Prototype JavaScript framework >= 1.5");
      
COLOR.OBJECT = Class.create();

COLOR.OBJECT.prototype = {

    initialize: function(code,display) {
        this.code = code;
        this.display = display;
    }
};

// ]]>