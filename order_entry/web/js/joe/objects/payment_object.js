// <![CDATA[
var PAYMENT_METHOD = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("PAYMENT_METHOD requires the Prototype JavaScript framework >= 1.5");
      
PAYMENT_METHOD.OBJECT = Class.create();

PAYMENT_METHOD.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.id=null;
        this.description=null;
        this.type=null;
        this.cardId=null;
        this.hasExpirationDate=false;
        this.regexPattern=null;
        this.cscRequired=null;
    }
};
// ]]>