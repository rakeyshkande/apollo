// <![CDATA[
var BILLING_INFO_OPTION = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("BILLING_INFO_OPTION requires the Prototype JavaScript framework >= 1.5");
      
BILLING_INFO_OPTION.OBJECT = Class.create();

BILLING_INFO_OPTION.OBJECT.prototype = {

    initialize: function() {
        this.reset();
    },
    
    reset: function() {
        this.sequence=null;     //OPTION_SEQUENCE
        this.optionName=null;   //OPTION_NAME
        this.optionValue=null;  //OPTION_VALUE
    }
};

// ]]>