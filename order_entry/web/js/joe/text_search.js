// <![CDATA[
var TEXT_SEARCH = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || TEXT_SEARCH.prototypeVersion < 1.6)
      throw("TEXT_SEARCH requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("TEXT_SEARCH requires FTD_DOM");
      
TEXT_SEARCH.CURRENT_SEARCH_ID = null;
TEXT_SEARCH.CANCEL_DELAY_TIMER = null;
var v_textSearchId = null;

TEXT_SEARCH.hideSearchElements = function() {
    $('tsZipSearchCriteriaDiv').style.display = 'none';
    $('tsSourceSearchCriteriaDiv').style.display = 'none';
    $('tsPhoneSearchCriteriaDiv').style.display = 'none';
    $('tsFacilitySearchCriteriaDiv').style.display = 'none';
    $('tsFloristSearchCriteriaDiv').style.display = 'none';
    $('tsIOTWSearchCriteriaDiv').style.display = 'none';
    $('tsMemberShipSearchCriteriaDiv').style.display = 'none'
    
    $('tszResultsDiv').style.display = 'none';
    $('tssSourceCodeResultsDiv').style.display = 'none';
    $('tssIOTWResultsDiv').style.display = 'none';
    $('tspResultsDiv').style.display = 'none';
    $('tsfResultsDiv').style.display = 'none';
    $('tsflResultsDiv').style.display = 'none';
    $('tsmResultsDiv').style.display = 'none';
    
}

TEXT_SEARCH.closeSearch = function() {
    new Effect.SlideUp('textSearchDivId');
    if( v_textSearchTargetElementId=='productSearchZip' ) 
        new Effect.SlideDown('productSearchDivId');
    else
        new Effect.SlideDown('rightDivId');
    
    v_textSearchOpen = false;
}

TEXT_SEARCH.showStart = function() {
    $('textSearchStopButton').disabled = true;
    $('textSearchGoButton').style.display = 'none';
    $('textSearchDisabledButton').style.display = 'inline';
    $('textSearchStopButton').style.display = 'none';
    $('textSearchProcessing').style.visibility = 'visible';
}

TEXT_SEARCH.showDone = function() {
    $('textSearchDisabledButton').style.display = 'none';
    $('textSearchStopButton').style.display = 'none';
    $('textSearchGoButton').style.display = 'inline';
    $('textSearchProcessing').style.visibility = 'hidden';
}


TEXT_SEARCH.checkResultSetStatus = function(xmlDoc,resultSetName,errorOnBadStatus) {
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = true;
    
    if( resultSet==null ) {
        retval=false;
        alert('The server did not return the required data for '+resultSetName+'.  Contact BACOM support immediatedly');
    } else {
        if( FTD_XML.getAttributeText(resultSet,'status')=='Y' ) {
            retval=true;
        } else if( errorOnBadStatus==true ) {
            retval=false;
        }
    }
    
    return retval;
}

TEXT_SEARCH.getResultSetStatusMessage = function(xmlDoc,resultSetName) {
    var resultSet = XPath.selectNodes('/result/rs[@name="'+resultSetName+'"]',xmlDoc)[0];
    var retval = 'Unexpected results have been returned for result set '+resultSetName+'.  Contact BACOM support immediatedly';
    
    if( resultSet==null ) {
        retval=false;
        alert('The server did not return the required data for '+resultSetName+'.  Contact BACOM support immediatedly');
    } else {
        retval=FTD_XML.getAttributeText(resultSet,'message'); 
    }
    
    return retval;
}

TEXT_SEARCH.startSearch = function(mode) {
    if( TEXT_SEARCH.CANCEL_DELAY_TIMER==null )  {
        TEXT_SEARCH.CANCEL_DELAY_TIMER = new TIMER.Timer(v_searchCancelDelaySeconds*1000,'TEXT_SEARCH.cancelDelayTimerCallback()',null);
    }
    TEXT_SEARCH.CURRENT_SEARCH_ID = new Date().getTime();
    TEXT_SEARCH.CANCEL_DELAY_TIMER.startTimer();
    
    $('tsNoResults').style.display = 'none';
    if( v_textSearchType == 'zip' ) {
        FTD_DOM.removeAllTableRows($('tszResultsBody'));
        TEXT_SEARCH_AJAX.zipSearch(mode,v_textSearchTargetElementId);
    } else if( v_textSearchType == 'phone' ) {
        FTD_DOM.removeAllTableRows($('tspResultsBody'));
        TEXT_SEARCH_AJAX.phoneSearch(mode,v_textSearchTargetElementId);
    } else if( v_textSearchType == 'facility' ) {
        FTD_DOM.removeAllTableRows($('tsfResultsBody'));
        TEXT_SEARCH_AJAX.facilitySearch();
    } else if( v_textSearchType == 'source' ) {
        FTD_DOM.removeAllTableRows($('tssSourceCodeResultsBody'));
        FTD_DOM.removeAllTableRows($('tssIOTWResultsBody'));
        TEXT_SEARCH_AJAX.sourceCodeSearch();
    } else if( v_textSearchType == 'membershipId' ) {
        FTD_DOM.removeAllTableRows($('tsmResultsBody'));
        TEXT_SEARCH_AJAX.membershipIdSearch(mode,v_textSearchTargetElementId);
    } else if( v_textSearchType == 'florist' ) {
        FTD_DOM.removeAllTableRows($('tsflResultsBody'));
        TEXT_SEARCH_AJAX.floristSearch();
    } else {
        TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
        TEXT_SEARCH.showDone();
        $('tsNoResults').style.display = 'block';
    }
}

TEXT_SEARCH.cancelDelayTimerCallback = function() {
    TEXT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    $('textSearchStopButton').disabled = false;
    $('textSearchDisabledButton').style.display = 'none';
    $('textSearchStopButton').style.display = 'inline';
}


/*****************************************************************************/
/*                           ZIP SEARCH                                      */
/*****************************************************************************/
TEXT_SEARCH.openZipSearch = function(targetElementId,errorMode,displayCritera) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'zip';
    v_textSearchTargetElementId = targetElementId;
    $('textSearchMainCaption').innerHTML = 'Zip Search';
    $('tsCloseButton').innerHTML = 'Back to Order';
    
    if( targetElementId=='productZip' ) {
        $('tszCity').value = $('recipientCity').value;
        FTD_DOM.selectOptionByValue($('tszState'),FTD_DOM.getSelectedValue('recipientState'));
    } else if( targetElementId=='recipientZip' ) {
        $('tszCity').value = $('recipientCity').value;
        FTD_DOM.selectOptionByValue($('tszState'),FTD_DOM.getSelectedValue('recipientState'));
    } else if( targetElementId=='customerZip' ) {
        $('tszCity').value = $('customerCity').value;
        FTD_DOM.selectOptionByValue($('tszState'),FTD_DOM.getSelectedValue('customerStateCombo'));
    } else if( targetElementId=='productSearchZip' ) {
        $('tsCloseButton').innerHTML = 'Back to Search';
    } 
    
    $('tsZipSearchCriteriaDiv').style.display = displayCritera==true?'block':'none';
    $('textSearchGoButton').style.display = displayCritera==true?'inline':'none';
    $('tszResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
        
    if( targetElementId=='tsflZip') {
        $('tsFloristSearchCriteriaDiv').style.display = 'none';
        $('tsZipSearchCriteriaDiv').style.display = 'block';
        $('tsflResultsDiv').style.display = 'none';
        $('tszResultsDiv').style.display = 'block';
        $('tszCity').focus();
    } else {
        new Effect.SlideUp('rightDivId');
        new Effect.SlideDown('textSearchDivId',{afterFinish:TEXT_SEARCH.selectZipSearch});
    }
    v_textSearchOpen = true;
}

TEXT_SEARCH.zipSelected = function(targetElementId,rowIdx) {
    var zipCode = $('tsZipCode___'+rowIdx).innerHTML;
    var stateId = $('tsZipState___'+rowIdx).innerHTML;
    var city = $('tsZipCity___'+rowIdx).innerHTML;
    var countryId = findStateById(stateId).countryId;
    
    if( targetElementId=='productZip' ) {
        ORDER_EVENTS.changeProductZip(zipCode,countryId);
        ORDER_EVENTS.changeRecipientZip(zipCode,stateId,countryId);
        $('recipientCity').value = city;
        TEXT_SEARCH.closeSearch();
        ORDER_EVENTS.onProductIdOrZipCodeChange();
        //resetThumbnailDetail(v_currentCartIdx);
        waitToSetFocus('productDeliveryDate'); 
    } else if( targetElementId=='recipientZip' ) {
        ORDER_EVENTS.changeRecipientZip(zipCode,stateId,countryId);
        $('recipientCity').value = city;
        ORDER_EVENTS.changeProductZip(zipCode,countryId);
        TEXT_SEARCH.closeSearch();
        ORDER_EVENTS.onProductIdOrZipCodeChange();
        //resetThumbnailDetail(v_currentCartIdx);
        if( isCountryDomestic(countryId) ) {
            waitToSetFocus('recipientState');
        } else {
            waitToSetFocus('recipientCity');
        }
    } else if( targetElementId=='customerZip' ) {
        CUSTOMER_EVENTS.changeCustomerZip(zipCode,stateId,countryId);
        $('customerCity').value = city;
        TEXT_SEARCH.closeSearch();
        if( isCountryDomestic(countryId) ) {
            waitToSetFocus('customerStateCombo');
        } else {
            waitToSetFocus('customerCity');
        }
    } else if( targetElementId=='productSearchZip' ) {
        CUSTOMER_EVENTS.changeCustomerZip(zipCode,stateId,countryId);
        $('productSearchZip').value = zipCode;
        new Effect.SlideUp('textSearchDivId');
        new Effect.SlideDown('productSearchDivId');
    } else if( targetElementId=='tsflZip' ) {
        $('tsflZip').value = zipCode;
        $('tsZipSearchCriteriaDiv').style.display = 'none';
        $('textSearchMainCaption').innerHTML = 'Florist Search';
        v_textSearchType = 'florist';
        $('tsFloristSearchCriteriaDiv').style.display = 'block';
        $('tszResultsDiv').style.display = 'none';
        $('tsflResultsDiv').style.display = 'block';
        $('tsflZip').focus();
    }
    
}

TEXT_SEARCH.selectZipSearch = function(obj) {
    try {
        $('tszCity').focus();
    } catch (err) {
        //ignore it
    }
}

/*****************************************************************************/
/*                           MEMBERSHIP SEARCH                               */
/*****************************************************************************/
TEXT_SEARCH.openMembershipIdSearch = function(targetElementId,errorMode) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'membershipId';
    v_textSearchTargetElementId = targetElementId;
    $('textSearchMainCaption').innerHTML = 'AAA Club Search';
    
    //FTD_DOM.removeAllTableRows($('tsmResultsDiv'));
    $('tsmZip').setValue($(targetElementId).getValue());
    
    $('tsMemberShipSearchCriteriaDiv').style.display = 'block';
    $('tsmResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
      
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('textSearchDivId',{afterFinish:TEXT_SEARCH.selectMembershipIdSearch});
    v_textSearchOpen = true;
}

TEXT_SEARCH.membershipIdSelected = function(targetElementId,rowIdx) {
    var clubName = $('tsClubName___'+rowIdx).innerHTML;
    var membershipID = $('memberShipId___'+rowIdx).value;
        
    $('membershipId').value = membershipID;  
	if(UTILS.isEmpty($('membershipId').value)==false) {
        $('membershipId').disabled = true;
        $('membershipFirstName').value = $('customerFirstName').value;
        $('membershipLastName').value = $('customerLastName').value;
	}
    TEXT_SEARCH.closeSearch();
}

TEXT_SEARCH.selectMembershipIdSearch = function(obj) {
    try {
    	
        
    } catch (err) {
        //ignore it
    }
}

/*****************************************************************************/
/*                          PHONE SEARCH                                     */
/*****************************************************************************/
TEXT_SEARCH.openPhoneSearch = function(targetElementId,errorMode) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'phone';
    v_textSearchTargetElementId = targetElementId;
    $('textSearchMainCaption').innerHTML = 'Phone Search';
    
//    var newSearchValue = $(targetElementId).getValue();
//    var oldSearchValue = $('tspPhone').getValue();
//    if( newSearchValue!=oldSearchValue ) {
//        FTD_DOM.removeAllTableRows($('tspResultsBody'));
//        $('tspPhone').setValue(newSearchValue);
//    }
    
    FTD_DOM.removeAllTableRows($('tspResultsBody'));
    $('tspPhone').setValue($(targetElementId).getValue());
    
    $('tsPhoneSearchCriteriaDiv').style.display = 'block';
    $('tspResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
    
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('textSearchDivId',{afterFinish:TEXT_SEARCH.selectPhoneSearch});
    v_textSearchOpen = true;
}

TEXT_SEARCH.phoneSelected = function(targetElementId,rowIdx) {
    var dayPhoneNumber = $('tsPhoneNumber___'+rowIdx).innerHTML;
    var dayExtension = $('tsPhoneExt___'+rowIdx).value;
    var eveningPhoneNumber = $('tsPhoneEvening___'+rowIdx).innerHTML;
    var eveningExtension = $('tsPhoneEveningExt___'+rowIdx).value;
    var addressType = $('tsPhoneAddressType___'+rowIdx).value;
    var businessName = $('tsPhoneBusiness___'+rowIdx).innerHTML;
    var firstName = $('tsPhoneFirstName___'+rowIdx).value;
    var lastName = $('tsPhoneLastName___'+rowIdx).value;
    var address = $('tsPhoneAddress___'+rowIdx).innerHTML;
    var city = $('tsPhoneCity___'+rowIdx).innerHTML;
    var state = $('tsPhoneState___'+rowIdx).innerHTML;
    var zipCode = $('tsPhoneZipCode___'+rowIdx).value;
    var countryId = $('tsPhoneCountry___'+rowIdx).value;
    var emailAddress = $('tsPhoneEmail___'+rowIdx).value;
    var subscribe = $('tsPhoneSubscribe___'+rowIdx).value;
    var membershipId = $('tsPhoneMembershipId___'+rowIdx).value;
    var membershipFirstName = $('tsPhoneMembershipFirstName___'+rowIdx).value;
    var membershipLastName = $('tsPhoneMembershipLastName___'+rowIdx).value;
    
    if( targetElementId=='recipientPhone' ) {
        var oldProductZip = $('productZip').value;
        ORDER_EVENTS.changeRecipientZip(zipCode,state,countryId);
        ORDER_EVENTS.changeProductZip(zipCode,countryId);
        if (oldProductZip != zipCode) {
            ORDER_EVENTS.onProductIdOrZipCodeChange();
            //resetThumbnailDetail(v_currentCartIdx);
        }
        if( dayPhoneNumber==null || dayPhoneNumber.length==0 ) {
            $('recipientPhone').value=eveningPhoneNumber;
            $('recipientPhoneExt').value=eveningExtension; 
        } else {
            $('recipientPhone').value=dayPhoneNumber;
            $('recipientPhoneExt').value=dayExtension;
        }
        $('recipientCity').value=city;
        $('recipientAddress').value=address;
        FTD_DOM.selectOptionByValue($('recipientType'),addressType);
        $('recipientBusiness').value=businessName;
        if( addressType!='FUNERAL HOME' && addressType!='HOSPITAL' && addressType!='NURSING HOME' ) {
            $('recipientFirstName').value=firstName;
            $('recipientLastName').value=lastName;
        }
        // The same event is called above
        //ORDER_EVENTS.changeProductZip(zipCode,countryId);
        ORDER_EVENTS.onRecipLocationTypeChanged(null);
        
        //Calling Sympathy Location check
        ORDER_EVENTS.onDeliveryLocationChange();
        
        $('recipientEmailAddress').value=emailAddress;
        $('recipientOptIn').value=subscribe;
        
        if( addressType=='FUNERAL HOME' || addressType=='HOSPITAL' || addressType=='NURSING HOME' ) {
            waitToSetFocus('recipientFirstName');
        } else if( isCountryDomestic(countryId)==true ) {
            waitToSetFocus('recipientState');
        } else {
            waitToSetFocus('recipientCity');
        }
        
    } else if( targetElementId=='customerBillingPhone' ) {
        CUSTOMER_EVENTS.changeCustomerZip(zipCode,state,countryId);
        $('customerBillingPhone').value=dayPhoneNumber;
        $('customerBillingPhoneExt').value=dayExtension;
        $('customerEveningPhone').value=eveningPhoneNumber;
        $('customerEveningPhoneExt').value=eveningExtension;
        $('customerCity').value=city;
        //$('customerAddress').setValue(address);
        //FTD_DOM.selectOptionByValue($('customerAddressType'),addressType);
        //$('customerBusiness').value=businessName;
        $('customerFirstName').value=firstName;
        $('customerLastName').value=lastName;
        $('customerAddress').setValue(address);
        $('customerEmailAddress').value=emailAddress;
        $('customerRecipSameCheckbox').checked = false;
        
        if( UTILS.isEmpty(membershipId)==false ) {
            $('membershipId').value = membershipId;
            
            if( UTILS.isEmpty(membershipFirstName)==true || UTILS.isEmpty(membershipLastName)==true ) {
                $('membershipFirstName').value=firstName;
                $('membershipLastName').value=lastName;
            } else {
                $('membershipFirstName').value=membershipFirstName;
                $('membershipLastName').value=membershipLastName;
            }
        } else {
            $('membershipFirstName').value=firstName;
            $('membershipLastName').value=lastName;
        }
        
        $('customerSubscribeCheckbox').checked = (subscribe=='true'?true:false);
        if( subscribe=='true' ) {
            $('customerSubscribeCheckbox').style.display='none';
            $('customerSubscribeCheckboxLabel').style.display='none';
            waitToSetFocus('customerEmailAddress');
        } else {
            $('customerSubscribeCheckbox').style.display='';
            $('customerSubscribeCheckboxLabel').style.display='';
            waitToSetFocus('customerSubscribeCheckbox');
        }
        
        var fld = findFieldObject('customerEmailAddress', v_currentCartIdx);
        if( fld!=null ) fld.isRequired();
    }
    
    setOrderData();
    v_cartList[v_currentCartIdx].populateThumbNail();
    TEXT_SEARCH.closeSearch();
    
    if( targetElementId=='customerBillingPhone' ) {
      CUSTOMER_EVENTS.customerEmailAddressChanged();
    }
}

TEXT_SEARCH.selectPhoneSearch = function(obj) {
    try {
        $('tspPhone').focus();
    } catch (err) {
        //ignore it
    }
}

/*****************************************************************************/
/*                         FLORIST SEARCH                                    */
/*****************************************************************************/
TEXT_SEARCH.openFloristSearch = function(targetElementId,errorMode) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'florist';
    v_textSearchTargetElementId = 'floristId';
    $('textSearchMainCaption').innerHTML = 'Florist Search';
    $('tsflZip').value = $('productZip').value;
    FTD_DOM.removeAllTableRows($('tsflResultsBody'));
    
    TEXT_SEARCH.showDone();
    $('tsFloristSearchCriteriaDiv').style.display = 'block';
    $('tsflResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
    
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('textSearchDivId');
}

TEXT_SEARCH.floristSelected = function(rowIdx) {
    var floristId = $('tsFloristNumber___'+rowIdx).innerHTML;
    //var floristName = $('tsFloristName___'+rowIdx).value;
    
    $('floristId').value = floristId;
    //$('floristDesc').innerHTML = floristName;

    TEXT_SEARCH.closeSearch();
    waitToSetFocus('floristId');
}


/*****************************************************************************/
/*                         FACILITY SEARCH                                   */
/*****************************************************************************/
TEXT_SEARCH.openFacilitySearch = function(errorMode) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'facility';
    v_textSearchTargetElementId = 'recipientPhone';
    $('textSearchMainCaption').innerHTML = 'Facility Search';
    
    $('tsfCity').value = $('recipientCity').value;
    FTD_DOM.selectOptionByValue($('tsfState'),FTD_DOM.getSelectedValue('recipientState'));
    FTD_DOM.selectOptionByValue($('tsfSearchType'),FTD_DOM.getSelectedValue('recipientType'));
    
    $('tsfKeyword').value='';
    FTD_DOM.removeAllTableRows($('tsfResultsBody'));
    
    $('tsFacilitySearchCriteriaDiv').style.display = 'block';
    $('textSearchGoButton').style.display = 'inline';
    $('tsfResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
    
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('textSearchDivId',{afterFinish:TEXT_SEARCH.selectFacilitySearch});
}

TEXT_SEARCH.facilitySelected = function(rowIdx) {
    var name = $('tsFacName___'+rowIdx).innerHTML;
    var address = $('tsFacAddress___'+rowIdx).innerHTML;
    var city = $('tsFacCity___'+rowIdx).innerHTML;
    var stateId = $('tsFacState___'+rowIdx).innerHTML;
    var zipCode = $('tsFacZipCode___'+rowIdx).innerHTML;
    var countryId = findStateById(stateId).countryId;
    var phone = $('tsFacPhone___'+rowIdx).innerHTML;
    if( UTILS.isEmpty(phone)==false ) {
        //Remove the dashes
        phone = phone.replace(/-/g,'');
    }
    
    ORDER_EVENTS.changeProductZip(zipCode,countryId);
    ORDER_EVENTS.changeRecipientZip(zipCode,stateId,countryId);
    $('recipientBusiness').value = name;
    $('recipientAddress').value = address;
    $('recipientCity').value = city;
    $('recipientPhone').value = phone;
    FTD_DOM.selectOptionByValue($('recipientType'),FTD_DOM.getSelectedValue($('tsfSearchType')));
    ORDER_EVENTS.onRecipLocationTypeChanged(null);
    
    TEXT_SEARCH.closeSearch();
    waitToSetFocus('recipientFirstName');
}

TEXT_SEARCH.selectFacilitySearch = function(obj) {
    try {
        $('tsfSearchType').focus();
    } catch (err) {
        //ignore it
    }
}


/*****************************************************************************/
/*                        SOURCE CODE SEARCH                                 */
/*****************************************************************************/
TEXT_SEARCH.openSourceCodeSearch = function(errorMode) {
    TEXT_SEARCH.hideSearchElements();
    
    v_textSearchType = 'source';
    v_textSearchTargetElementId = 'callDataSourceCode';
    $('textSearchMainCaption').innerHTML = 'Source Code Search';
    
    $('tssKeyword').value='';
    FTD_DOM.removeAllTableRows($('tssSourceCodeResultsBody'));
    FTD_DOM.removeAllTableRows($('tssIOTWResultsBody'));
    FTD_DOM.selectOptionByValue($('tssSearchType'),'SOURCE_CODE');
    
    $('tsSourceSearchCriteriaDiv').style.display = 'block';
    $('textSearchGoButton').style.display = 'inline';
    $('tssSourceCodeResultsDiv').style.display = 'block';
    $('tsNoResults').style.display = errorMode==true?'block':'none';
    v_textSearchOpen = true;
    
    new Effect.SlideUp('rightDivId');
    new Effect.SlideDown('textSearchDivId',{afterFinish:TEXT_SEARCH.selectSourceCodeSearch});
}

TEXT_SEARCH.sourceCodeSelected = function(rowIdx) {
    var masterSourceCode = $('tsSourceCode___'+rowIdx).innerHTML;
    TEXT_SEARCH.closeSearch();
    waitToSetFocus('productId'); 
    
    if( DNIS_EVENTS.checkSourceCodeChange() ) {
        changeSourceCode(masterSourceCode,true);
    }
}

TEXT_SEARCH.iotwSelected = function(rowIdx) {
    var productId = $('tsITOWProductId___'+rowIdx).innerHTML;
    var masterSourceCode = $('tsIOTWMasterSourceCode___'+rowIdx).innerHTML;
    var iotwSourceCode = $('tsIOTWSourceCode___'+rowIdx).value;
    var productMsg =  $('tsIOTWProductMsg___'+rowIdx).value;
    
    if( v_masterSourceCode!=masterSourceCode && DNIS_EVENTS.checkSourceCodeChange() ) {
        //Change the product id
        $('productId').value=productId;
        v_cartList[v_currentCartIdx].productId=productId;
        changeSourceCode(masterSourceCode,false);
        
        //changeSourceCode changes the values below, so, set them now
        $('productSourceCode').value=iotwSourceCode;
        v_cartList[v_currentCartIdx].productSourceCode=iotwSourceCode;
        $('productIOTWDescription').innerHTML=productMsg;
        v_cartList[v_currentCartIdx].productIOTWDescription=productMsg;
        productIdChanged(false);
        validateSourceCode();
    }
    
    TEXT_SEARCH.closeSearch();
    waitToSetFocus('productId'); 
}

TEXT_SEARCH.selectSourceCodeSearch = function(obj) {
    try {
        $('tssSearchType').focus();
    } catch (err) {
        //ignore it
    }
}
// ]]>