// <![CDATA[
var PRODUCT_SEARCH = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || PRODUCT_SEARCH.prototypeVersion < 1.6)
      throw("PRODUCT_SEARCH requires the Prototype JavaScript framework >= 1.6");
      
if(typeof FTD_DOM=='undefined')
      throw("PRODUCT_SEARCH requires FTD_DOM");
      
PRODUCT_SEARCH.CURRENT_SEARCH_ID = null;
PRODUCT_SEARCH.CANCEL_DELAY_TIMER = null;
var v_productSearchId = null;

PRODUCT_SEARCH.promoPrefix = 'product_search_promo_';
PRODUCT_SEARCH.promoBodyPrefix = 'product_search_promo_body_';
PRODUCT_SEARCH.promoTablePrefix = 'product_search_promo_table_';
PRODUCT_SEARCH.promoProductPrefix = 'product_search_promo_product_id_';
PRODUCT_SEARCH.promoImageAnchorPrefix = 'product_search_promo_image_anchor_';
PRODUCT_SEARCH.promoImagePrefix = 'product_search_promo_image_';
PRODUCT_SEARCH.promoNamePrefix = 'product_search_promo_name_anchor_';
PRODUCT_SEARCH.promoPricePrefix = 'product_search_promo_price_anchor_';
PRODUCT_SEARCH.promoDiscountPriceCellPrefix = 'product_search_promo_discount_price_cell_';
PRODUCT_SEARCH.promoDiscountPricePrefix = 'product_search_promo_discount_price_anchor_';
PRODUCT_SEARCH.promoButtonPrefix = 'product_search_promo_button_';
PRODUCT_SEARCH.promoFlexfillPrefix = 'product_search_promo_flexfill_';
PRODUCT_SEARCH.promoClockPrefix = 'product_search_promo_clock_';
PRODUCT_SEARCH.promoIotwRowPrefix = 'product_search_promo_iotw_row_';
PRODUCT_SEARCH.promoIotwDescPrefix = 'product_search_promo_iotw_desc_';

PRODUCT_SEARCH.resultsPrefix = 'product_search_results_';
PRODUCT_SEARCH.resultsBodyPrefix = 'product_search_results_body_';
PRODUCT_SEARCH.resultsTablePrefix = 'product_search_results_table_';
PRODUCT_SEARCH.resultsProductPrefix = 'product_search_results_product_id_';
PRODUCT_SEARCH.resultsImageAnchorPrefix = 'product_search_results_image_anchor_';
PRODUCT_SEARCH.resultsImagePrefix = 'product_search_results_image_';
PRODUCT_SEARCH.resultsNamePrefix = 'product_search_results_name_anchor_';
PRODUCT_SEARCH.resultsPricePrefix = 'product_search_results_price_anchor_';
PRODUCT_SEARCH.resultsDiscountPriceCellPrefix = 'product_search_results_discount_price_cell_';
PRODUCT_SEARCH.resultsDiscountPricePrefix = 'product_search_results_discount_price_anchor_';
PRODUCT_SEARCH.resultsButtonPrefix = 'product_search_results_button_';
PRODUCT_SEARCH.resultsFlexfillPrefix = 'product_search_results_flexfill_';
PRODUCT_SEARCH.resultsClockPrefix = 'product_search_results_clock_';
PRODUCT_SEARCH.resultsIotwRowPrefix = 'product_search_results_iotw_row_';
PRODUCT_SEARCH.resultsIotwDescPrefix = 'product_search_results_iotw_desc_';

PRODUCT_SEARCH.suggestPrefix = 'product_search_suggest_';
PRODUCT_SEARCH.suggestBodyPrefix = 'product_search_suggest_body_';
PRODUCT_SEARCH.suggestTablePrefix = 'product_search_suggest_table_';
PRODUCT_SEARCH.suggestProductPrefix = 'product_search_suggest_product_id_';
PRODUCT_SEARCH.suggestImageAnchorPrefix = 'product_search_suggest_image_anchor_';
PRODUCT_SEARCH.suggestImagePrefix = 'product_search_suggest_image_';
PRODUCT_SEARCH.suggestNamePrefix = 'product_search_suggest_name_anchor_';
PRODUCT_SEARCH.suggestPricePrefix = 'product_search_suggest_price_anchor_';
PRODUCT_SEARCH.suggestDiscountPriceCellPrefix = 'product_search_suggest_discount_price_cell_';
PRODUCT_SEARCH.suggestDiscountPricePrefix = 'product_search_suggest_discount_price_anchor_';
PRODUCT_SEARCH.suggestButtonPrefix = 'product_search_suggest_button_';
PRODUCT_SEARCH.suggestFlexfillPrefix = 'product_search_suggest_flexfill_';
PRODUCT_SEARCH.suggestClockPrefix = 'product_search_suggest_clock_';
PRODUCT_SEARCH.suggestIotwRowPrefix = 'product_search_suggest_iotw_row_';
PRODUCT_SEARCH.suggestIotwDescPrefix = 'product_search_suggest_iotw_desc_';

PRODUCT_SEARCH.topPrefix = 'product_search_top_';
PRODUCT_SEARCH.topBodyPrefix = 'product_search_top_body_';
PRODUCT_SEARCH.topTablePrefix = 'product_search_top_table_';
PRODUCT_SEARCH.topProductPrefix = 'product_search_top_product_id_';
PRODUCT_SEARCH.topImageAnchorPrefix = 'product_search_top_image_anchor_';
PRODUCT_SEARCH.topImagePrefix = 'product_search_top_image_';
PRODUCT_SEARCH.topNamePrefix = 'product_search_top_name_anchor_';
PRODUCT_SEARCH.topPricePrefix = 'product_search_top_price_anchor_';
PRODUCT_SEARCH.topDiscountPriceCellPrefix = 'product_search_top_discount_price_cell_';
PRODUCT_SEARCH.topDiscountPricePrefix = 'product_search_top_discount_price_anchor_';
PRODUCT_SEARCH.topButtonPrefix = 'product_search_top_button_';
PRODUCT_SEARCH.topFlexfillPrefix = 'product_search_top_flexfill_';
PRODUCT_SEARCH.topClockPrefix = 'product_search_top_clock_';
PRODUCT_SEARCH.topIotwRowPrefix = 'product_search_top_iotw_row_';
PRODUCT_SEARCH.topIotwDescPrefix = 'product_search_top_iotw_desc_';

PRODUCT_SEARCH.resultsRows = 2;
PRODUCT_SEARCH.resultsColumns = 5;
PRODUCT_SEARCH.currentPageIndex = 0;
PRODUCT_SEARCH.maxPageIndex = 0;
PRODUCT_SEARCH.currentTopPageIndex = 0;
PRODUCT_SEARCH.maxTopPageIndex = 0;
PRODUCT_SEARCH.allFavoriteProducts = new Array();
PRODUCT_SEARCH.favoriteProducts = new Array();
PRODUCT_SEARCH.searchProducts = new Array();
PRODUCT_SEARCH.suggestProducts = new Array();
PRODUCT_SEARCH.topProducts = new Array();

PRODUCT_SEARCH.viewedProductsMax = 10;


PRODUCT_SEARCH.sortedSearchArray = new Array();
PRODUCT_SEARCH.sortedTopArray = new Array();


PRODUCT_SEARCH.startSearch = function() {
    $('psNoResults').style.display = 'none';
    $('psNavButtons').style.display = 'none';
    FTD_DOM.removeAllTableRows($('productSearchProductBody'));
    
    if( $('psViewedDivId').visible() ) {
        new Effect.SlideUp('psViewedDivId');
    }
    
    //PRODUCT_SEARCH_EVENTS.onCloseTopSellers();
    PRODUCT_SEARCH.closeTopSellersPanel();
    
    if( PRODUCT_SEARCH.CANCEL_DELAY_TIMER==null )  {
        PRODUCT_SEARCH.CANCEL_DELAY_TIMER = new TIMER.Timer(v_searchCancelDelaySeconds*1000,'PRODUCT_SEARCH.cancelDelayTimerCallback()',null);
    }
    PRODUCT_SEARCH.CURRENT_SEARCH_ID = new Date().getTime();
    PRODUCT_SEARCH.CANCEL_DELAY_TIMER.startTimer();
    $('productSearchStopButton').disabled = true;
    $('productSearchGoButton').style.display = 'none';
    $('productSearchDisabledButton').style.display = 'inline';
    $('productSearchStopButton').style.display = 'none';
    $('searchProcessing').style.visibility = 'visible';
    
    PRODUCT_SEARCH_AJAX.start();
}

PRODUCT_SEARCH.stopSearch = function() {
    if( PRODUCT_SEARCH.CANCEL_DELAY_TIMER!=null )  {
        PRODUCT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    }
    PRODUCT_SEARCH.CURRENT_SEARCH_ID = null;
    $('productSearchGoButton').style.display = 'inline';
    $('productSearchDisabledButton').style.display = 'none';
    $('productSearchStopButton').style.display = 'none';
    $('searchProcessing').style.visibility = 'hidden';
}

PRODUCT_SEARCH.closeSearch = function(obj) {
    //look to see if any of the parameters have changed
    var bDateChanged = false;
    var changedDeliveryDate = FTD_DOM.getSelectedValue('productSearchDeliveryDate');
    if( /*UTILS.isEmpty(changedDeliveryDate)==false &&*/ changedDeliveryDate != FTD_DOM.getSelectedValue('productDeliveryDate') ) {
        bDateChanged=true;
    }

    var bCountryChanged = false;
    var changedCountry = FTD_DOM.getSelectedValue($('productSearchCountryCombo'));
    //alert(changedCountry + ' ' + FTD_DOM.getSelectedValue($('productCountry')));
    if (changedCountry != FTD_DOM.getSelectedValue($('productCountry'))) {
        bCountryChanged = true;
    }

    var bZipChanged = false;
    var changedZip = $('productSearchZip').value;
    changedZip = changedZip.toUpperCase();
    if( /*UTILS.isEmpty(changedZip)==false &&*/ changedZip != $('productZip').value ) {
        bZipChanged=true;
    }
    
    var strMsg = 'While you were searching for a product, you changed';
    if( bDateChanged ) {
        strMsg+=' the delivery date';
    }

    if( bCountryChanged ) {
        if( bDateChanged ) strMsg+=' and';
        strMsg+=' the country';
    }

    if( bZipChanged ) {
        if( bDateChanged || bCountryChanged ) strMsg+=' and';
        strMsg+=' the zip/postal code';
    }
    
    if( bDateChanged==true || bZipChanged==true || bCountryChanged==true ) {
        strMsg+='.\r\nDid you want to make ';
        strMsg+=(bDateChanged==true&&bZipChanged==true?'these changes':'this change');
        strMsg+=' to the order also?';
        if( confirm(strMsg) ) {
            if( bDateChanged ) {
                changeProductDeliveryDate(changedDeliveryDate);
            }
            if( bCountryChanged ) {
                ORDER_EVENTS.changeProductCountry(changedCountry);
            }
            if( bZipChanged ) {
                ORDER_EVENTS.changeRecipientZip(changedZip,null,changedCountry);
                ORDER_EVENTS.changeProductZip(changedZip,changedCountry);
                VALIDATE_AJAX.validateZipElement('productZip');
                ORDER_EVENTS.onProductIdOrZipCodeChange();
            }
        }
    }
    
    $('productSearchStopButton').disabled = false;
        
    PRODUCT_SEARCH.stopSearch();
    if( $('psViewedDivId').visible() )
        $('psViewedDivId').style.display='none';
    
    if( $('psDetailDivId').visible() ) {
        $('productSearchDivId').style.display = 'none';
        new Effect.SlideUp('psDetailDivId');
    } else if( $('productSearchDivId').visible() ) {
        new Effect.SlideUp('productSearchDivId');
    }
    
    v_textSearchOpen = false;
    new Effect.SlideDown('rightDivId');
}

PRODUCT_SEARCH.openSearch = function(searchType) {
    FTD_DOM.removeAllTableRows($('productSearchProductBody'));
    FTD_DOM.selectOptionByValue('productSearchDeliveryDate',FTD_DOM.getSelectedValue('productDeliveryDate'));
    FTD_DOM.selectOptionByValue('productSearchCountryCombo',FTD_DOM.getSelectedValue('productCountry'));
    PRODUCT_SEARCH_EVENTS.onProductSearchCountryChange();
    $('productSearchZip').value = $('productZip').value;
    $('productSearchKeywords').value='';
    $('psNavButtons').style.display = 'none';
    FTD_DOM.selectOptionByValue($('productSearchSortCombo'),'MATCH');
    PRODUCT_SEARCH.populateFavoriteProducts();
    PRODUCT_SEARCH.setProductSearchFields();
    v_textSearchOpen = true;
    new Effect.SlideUp('rightDivId');
    
    if( searchType=='CROSSSELL' ) {
        new Effect.SlideDown('productSearchDivId',{afterFinish:PRODUCT_SEARCH.openCrossSell});
    }
    else {
        if( $('psResultsDivId').visible()==false ) {
            $('psResultsDivId').style.display='';
        }
        new Effect.SlideDown('productSearchDivId');
        
        var selectedCountryId = $('productSearchCountryCombo').getValue();
        if( isCountryDomestic(selectedCountryId) ) {
            $('productSearchDeliveryDate').style.display='';
            $('productSearchDeliveryDateLabel').style.display='';
            $('productSearchCalendarButton').style.display='';
            waitToSetFocus('productSearchKeywords');
        } else {
            $('productSearchDeliveryDate').style.display='none';
            $('productSearchDeliveryDateLabel').style.display='none';
            $('productSearchCalendarButton').style.display='none';
            waitToSetFocus('productSearchCountryCombo');
        }
    }
}

PRODUCT_SEARCH.openCrossSell = function(obj) {
    $('psResultsDivId').style.display = 'none';
    $('psCrossSellDiv').style.display = 'block';
}

PRODUCT_SEARCH.closeSearchDetails = function() {
    new Effect.SlideUp('psDetailDivId');
    new Effect.SlideDown('psResultsDivId');
}

PRODUCT_SEARCH.cancelDelayTimerCallback = function() {
    PRODUCT_SEARCH.CANCEL_DELAY_TIMER.stopTimer();
    $('productSearchStopButton').disabled = false;
    $('productSearchDisabledButton').style.display = 'none';
    $('productSearchStopButton').style.display = 'inline';
}

PRODUCT_SEARCH.setProductDetail = function(productObj) {
    $('psDetailProductId').value=productObj.productId;
    $('psDetailProductName').innerHTML = productObj.name;
    $('psDetailProductImage').src = productObj.largeImage;
    $('psDetailProductDesc').innerHTML=productObj.productDescription;
    if( productObj.iotwObject.flag==true ) {
        $('psDetailIOTWDescription').innerHTML = iotwDescriptionWrapper(productObj.iotwObject.message);
    } else {
        $('psDetailIOTWDescription').innerHTML = '';
    }
//    var msg = iotwDescriptionWrapper(productObj.iotwObject.message);
//    if( UTILS.isEmpty()==true ) 
//        $('psDetailIOTWDescription').innerHTML = '';
//    else 
//        $('psDetailIOTWDescription').innerHTML = iotwDescriptionWrapper(msg);
    v_currentProductDetailObject = productObj;
    
    if( $('psResultsDivId').visible() ) {
        new Effect.SlideUp('psResultsDivId');
    } else if( $('psTopSellersDivId').visible() ) {
        new Effect.SlideUp('psTopSellersDivId');
    } else if( $('psCrossSellDiv').visible() ) {
        new Effect.SlideUp('psCrossSellDiv');
    }
    
    PRODUCT_SEARCH.closeRecentlyViewedPanel();
    new Effect.SlideDown('psDetailDivId');
    PRODUCT_SEARCH.addProductToRecentlyViewed(productObj);
}

PRODUCT_SEARCH.setOrderProduct = function(productObj) {
    $('productOptionStandard').checked = true;
    var orderObj = v_cartList[v_currentCartIdx];
    orderObj.productId = productObj.productId;
    orderObj.productObject = productObj;
    orderObj.productPriceDiscounted = productObj.standardPrice;
    orderObj.productPriceMilesPoints = productObj.standardPrice;
    orderObj.addOns=null;
    orderObj.serviceFees=null;
    orderObj.shippingFees=null;
    orderObj.salesTax=null;
    orderObj.subTotal=null;
    //resetCartTotals();
    
    if( !productObj.addOnCard ) {
        orderObj['addOnCardCheckbox'] = false;
    }
    
    PRODUCT_SEARCH.closeSearch();
    orderObj.populateDOM();
    productIdChanged(true);
    waitToSetFocus('productOccasion');
    ORDER_EVENTS.onProductIdOrZipCodeChange();
}

PRODUCT_SEARCH.openRecentlyViewedPanel = function() {
    if( $('psViewedDivId').visible()==false ) {
        if( $('psTopSellersDivId').visible()  ) {
            new Effect.SlideUp('psTopSellersDivId');
        }
        
        if( $('psDetailDivId').visible()  ) {
            new Effect.SlideUp('psDetailDivId');
        }
        
        if( !($('psResultsDivId').visible()) ) {
            new Effect.SlideDown('psResultsDivId');
        }
        
        new Effect.SlideDown('psViewedDivId');
    }
}

PRODUCT_SEARCH.closeRecentlyViewedPanel = function() {    
    if( $('psViewedDivId').visible() ) {
        new Effect.SlideUp('psViewedDivId');
    }
}

PRODUCT_SEARCH.toggleRecentlyViewedPanel = function() {
    if( $('psViewedDivId').visible()==true ) {
        PRODUCT_SEARCH.closeRecentlyViewedPanel();
    } else {
        PRODUCT_SEARCH.openRecentlyViewedPanel();
    }
}

PRODUCT_SEARCH.openTopSellersPanel = function(topSellerLabel) {
    $('topSellersDivLabel').innerHTML = topSellerLabel;
        
    if( $('psTopSellersDivId').visible()==false ) {  
        if( $('rightDivId').visible() ) {
            new Effect.SlideUp('rightDivId');
        }
        
        if( $('psViewedDivId').visible() ) {
            new Effect.SlideUp('psViewedDivId');
        }
        
        if( $('psDetailDivId').visible()  ) {
            new Effect.SlideUp('psDetailDivId');
        }
        
        if( $('psResultsDivId').visible() ) {
            new Effect.SlideUp('psResultsDivId');
        }
        
        if( $('psCrossSellDiv').visible() ) {
            new Effect.SlideUp('psCrossSellDiv');
        }
        
        if( $('productSearchDivId').visible()==false ) {
            PRODUCT_SEARCH.setProductSearchFields();
            new Effect.SlideDown('productSearchDivId');
        }
        
        //Defect 4598 - always show the first page
        if( PRODUCT_SEARCH.currentTopPageIndex != 0 )
            PRODUCT_SEARCH.setTopCurrentSearchPage(0);
        new Effect.SlideDown('psTopSellersDivId');
    }
}

PRODUCT_SEARCH.closeTopSellersPanel = function() {
    if( $('psTopSellersDivId').visible()==true ) {
        if( $('psTopNoResults').visible() ) {
             $('psTopNoResults').style.display = 'none';
        }
        
        new Effect.SlideUp('psTopSellersDivId');
        
        if( !($('psResultsDivId').visible()) ) {
            new Effect.SlideDown('psResultsDivId');
        }
    }
}

PRODUCT_SEARCH.toggleTopSellersPanel = function() {
    if ( $('topSellersDivLabel').innerHTML == 'Suggest Product Alternatives' ) {
        new Effect.SlideUp('psTopSellersDivId');
        PRODUCT_SEARCH_AJAX.getPopularProducts(true);
    } else {
    if( $('psTopSellersDivId').visible()==true ) {  
        PRODUCT_SEARCH.closeTopSellersPanel();
    } else {
        PRODUCT_SEARCH.openTopSellersPanel( $('topSellersDivLabel').innerHTML );
    }
    }
}

PRODUCT_SEARCH.addProductToRecentlyViewed = function(prodObj) {
    var tempArray = new Array();
    tempArray.push(prodObj);
    var idx;
    for( idx=0; idx<v_viewedProducts.length; idx++ ) {
        if( tempArray.length>PRODUCT_SEARCH.viewedProductsMax ) {
            break;
        }
        
        if( v_viewedProducts[idx].productId==prodObj.productId ) {
            continue;
        }
        
        tempArray.push(v_viewedProducts[idx]);
    }
    
    v_viewedProducts = $A(tempArray);
    $('psViewedList').innerHTML='';
    for( idx=0; idx<v_viewedProducts.length; idx++ ) {
        prodObj = v_viewedProducts[idx];
        
        var strLink='('+prodObj.productId+') '+FTD_DOM.unconvertHtmlSpecialChars(prodObj.name);
        var linkNode = FTD_XML.createElementWithText(document,"a",strLink);
        var attr = document.createAttribute("href");
        attr.value = "javascript:PRODUCT_SEARCH_EVENTS.recentlyViewedLinkClicked("+idx+");";
        linkNode.setAttributeNode(attr);
        
        var liNode = document.createElement('li');
        liNode.appendChild(linkNode)
        $('psViewedList').appendChild(liNode);
    }
}
    
PRODUCT_SEARCH.populateFavoriteProducts = function() {
    var tableBody = $('productSearchPromoBody');
    FTD_DOM.removeAllTableRows(tableBody);
    
    var idx;
    for( idx=0; idx<PRODUCT_SEARCH.favoriteProducts.length; idx++ ) {
        if( PRODUCT_SEARCH.favoriteProducts[idx]==null ) continue;
        if( PRODUCT_SEARCH.favoriteProducts[idx].readyFlag==false ) continue;
        
        var row = tableBody.insertRow(tableBody.rows.length);
        var cell = row.insertCell(0);
    
        cell.appendChild(PRODUCT_SEARCH.appendProductSearchResult(PRODUCT_SEARCH.promoPrefix, idx));
        var discountType = PRODUCT_SEARCH.favoriteProducts[idx].discountType;
        var el = $(PRODUCT_SEARCH.promoTablePrefix+idx);
        el.className='productSearchPromoTable';
        
        el = $(PRODUCT_SEARCH.promoProductPrefix+idx);
        el.value=PRODUCT_SEARCH.favoriteProducts[idx].productId;
        
        el = $(PRODUCT_SEARCH.promoImageAnchorPrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onPromoSelectDetail('+idx+');';
        
        el = $(PRODUCT_SEARCH.promoImagePrefix+idx);
        el.src=PRODUCT_SEARCH.favoriteProducts[idx].smallImage;
        el.alt=productVerbageCleanup(PRODUCT_SEARCH.favoriteProducts[idx].name);
        el.title=productVerbageCleanup(PRODUCT_SEARCH.favoriteProducts[idx].productDescription);
        el.className='productSearchPromoImg';
                
        el = $(PRODUCT_SEARCH.promoNamePrefix+idx);
        el.innerHTML=PRODUCT_SEARCH.favoriteProducts[idx].productId+' - '+PRODUCT_SEARCH.favoriteProducts[idx].name;
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onPromoSelectDetail('+idx+');';
        
        el = $(PRODUCT_SEARCH.promoIotwRowPrefix+idx);
        if( PRODUCT_SEARCH.favoriteProducts[idx].iotwObject.flag==true ) {
            el.style.display='';
            el = $(PRODUCT_SEARCH.promoIotwDescPrefix+idx);
            el.innerHTML = iotwDescriptionWrapper(PRODUCT_SEARCH.favoriteProducts[idx].iotwObject.message);
        } else {
            el.style.display='none';    
        }
        
        el = $(PRODUCT_SEARCH.promoPricePrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onPromoSelectDetail('+idx+');';
        if( discountType=='Discount' ) {
            el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.favoriteProducts[idx].standardDiscount));
        } else { 
            el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.favoriteProducts[idx].standardPrice));
        } 
        
//        el = $(PRODUCT_SEARCH.promoDiscountPriceCellPrefix+idx);
//        if( UTILS.isEmpty(discountType)==true ) {
//            el.style.display = 'none';
//        } else {
//            el.style.display = 'inline';
//        }
        
        el = $(PRODUCT_SEARCH.promoDiscountPricePrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onPromoSelectDetail('+idx+');';
        if( UTILS.isEmpty(discountType)==false ) {
            el.style.display = '';
            if(discountType=='Discount') {
                el.style.textDecoration='line-through';
                el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.favoriteProducts[idx].standardPrice));
            } else {
                el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.favoriteProducts[idx].standardDiscount)+' '+discountType);
            }
        } else {
            el.style.display = 'none';
        }
        
       //#18729 - Added the check if the product is deliver by florist and carrier then display SDFC icon
        el = $(PRODUCT_SEARCH.promoFlexfillPrefix+idx);
    	if( PRODUCT_SEARCH.favoriteProducts[idx].floristProduct==true  && PRODUCT_SEARCH.favoriteProducts[idx].vendorProduct==true ) {
            el.style.display = '';
        } else {
            el.style.display = 'none';
        }
        
        el = $(PRODUCT_SEARCH.promoClockPrefix+idx);
        if( PRODUCT_SEARCH.favoriteProducts[idx].floristProduct==true ) {
            el.style.display = '';
        } else {
            el.style.display = 'none';
        }

        el = $('product_search_promo_noFS_'+idx);
        if (PRODUCT_SEARCH.favoriteProducts[idx].allowFreeShippingFlag == 'Y') {
            el.style.display = 'none';
        } else {
            el.style.display = '';
        }
        
        Event.observe(PRODUCT_SEARCH.promoButtonPrefix+idx,"click",PRODUCT_SEARCH_EVENTS.onPromoSelect);
        Event.observe(PRODUCT_SEARCH.promoImagePrefix+idx,"error",ORDER_EVENTS.onImageLoadError);
    }
}

PRODUCT_SEARCH.setPageNavigation = function(resultsCount) {
    PRODUCT_SEARCH.currentPageIndex = 0;
    $('productSearchPage').innerHTML='';
    var maxProductsPerPage = PRODUCT_SEARCH.resultsRows*PRODUCT_SEARCH.resultsColumns;
    PRODUCT_SEARCH.maxPageIndex = parseInt((resultsCount/maxProductsPerPage)+'')-1;
    if( resultsCount%maxProductsPerPage!=0 ) PRODUCT_SEARCH.maxPageIndex++; 
    for( var idx=0; idx<=PRODUCT_SEARCH.maxPageIndex; idx++ ) {
        //<a href="javascript:setCurrentSearchPage(0)" id="productSearchPageAnchor___">1</a>
        var anchor = document.createElement('a');
        var attr = document.createAttribute("href");
        attr.value = 'javascript:PRODUCT_SEARCH.setCurrentSearchPage('+idx+');';
        anchor.setAttributeNode(attr);
        attr = document.createAttribute("id");
        attr.value = 'productSearchPageAnchor___'+idx;
        anchor.setAttributeNode(attr);
        anchor.innerHTML = (idx+1)+'';
        $('productSearchPage').appendChild(anchor);
        $('productSearchPage').innerHTML = $('productSearchPage').innerHTML+'&nbsp;';
    }
    
    if( resultsCount==0 )
        $('psNavButtons').style.display = 'none';
    else
        $('psNavButtons').style.display = 'block';
}

PRODUCT_SEARCH.setCurrentSearchPage = function(newPageIdx) {
    if( Object.isNumber(newPageIdx)==true && newPageIdx>=0 && newPageIdx<=PRODUCT_SEARCH.maxPageIndex ) {
        $('productSearchPageAnchor___'+newPageIdx).style.fontSize = 'x-large';
        if( newPageIdx!=PRODUCT_SEARCH.currentPageIndex) {
            $('productSearchPageAnchor___'+PRODUCT_SEARCH.currentPageIndex).style.fontSize = 'large';
        }
        PRODUCT_SEARCH.currentPageIndex=newPageIdx;
        PRODUCT_SEARCH.populateSearchProducts();
    }
}

PRODUCT_SEARCH.populateSearchProducts = function() {
    var tableBody = $('productSearchProductBody');
    FTD_DOM.removeAllTableRows(tableBody);
    var maxProductsPerPage = PRODUCT_SEARCH.resultsRows*PRODUCT_SEARCH.resultsColumns;
    var startSearchIdx = PRODUCT_SEARCH.currentPageIndex*maxProductsPerPage;
    var endSearchIdx = (PRODUCT_SEARCH.currentPageIndex+1)*maxProductsPerPage;
    
    //Populate the page
    for( var idx=startSearchIdx; idx<PRODUCT_SEARCH.sortedSearchArray.length && idx<endSearchIdx; ) {
        
        for( var rowCount=0; rowCount<PRODUCT_SEARCH.resultsRows && idx<PRODUCT_SEARCH.sortedSearchArray.length && idx<endSearchIdx; rowCount++ ) {
        
            var newRow = tableBody.insertRow(rowCount);
            newRow.className='productSearchProductTable';
            
            for( var columnCount=0; columnCount<PRODUCT_SEARCH.resultsColumns && idx<PRODUCT_SEARCH.sortedSearchArray.length && idx<endSearchIdx; columnCount++ ) {
                var newColumn = newRow.insertCell(columnCount);
                newColumn.className='productSearchProductColumn';
                newColumn.appendChild(PRODUCT_SEARCH.appendProductSearchResult(PRODUCT_SEARCH.resultsPrefix, idx));
                var discountType = PRODUCT_SEARCH.sortedSearchArray[idx].discountType;
                
                var el = $(PRODUCT_SEARCH.resultsTablePrefix+idx);
                el.className='productSearchProductTable';
                
                el = $(PRODUCT_SEARCH.resultsProductPrefix+idx);
                el.value=PRODUCT_SEARCH.sortedSearchArray[idx].productId;
                
                el = $(PRODUCT_SEARCH.resultsImageAnchorPrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onProductSelectDetail('+PRODUCT_SEARCH.sortedSearchArray[idx].index+');';
                
                el = $(PRODUCT_SEARCH.resultsImagePrefix+idx);
                el.src=PRODUCT_SEARCH.sortedSearchArray[idx].smallImage;
                el.alt=productVerbageCleanup(PRODUCT_SEARCH.sortedSearchArray[idx].name);
                el.title=productVerbageCleanup(PRODUCT_SEARCH.sortedSearchArray[idx].productDescription);
                el.className='productSearchProductImg';
                el.name='product_search_results_image';
        
                el = $(PRODUCT_SEARCH.resultsIotwRowPrefix+idx);
                if( PRODUCT_SEARCH.sortedSearchArray[idx].iotwObject.flag==true ) {
                    el.style.display='';
                    el = $(PRODUCT_SEARCH.resultsIotwDescPrefix+idx);
                    el.innerHTML = iotwDescriptionWrapper(PRODUCT_SEARCH.sortedSearchArray[idx].iotwObject.message);
                } else {
                    el.style.display='none';    
                }
                
                el = $(PRODUCT_SEARCH.resultsNamePrefix+idx);
                el.innerHTML=PRODUCT_SEARCH.sortedSearchArray[idx].productId+' - '+PRODUCT_SEARCH.sortedSearchArray[idx].name;
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onProductSelectDetail('+PRODUCT_SEARCH.sortedSearchArray[idx].index+');';

                el = $(PRODUCT_SEARCH.resultsPricePrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onProductSelectDetail('+PRODUCT_SEARCH.sortedSearchArray[idx].index+');';
                if( discountType=='Discount' ) {
                    el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.sortedSearchArray[idx].standardDiscount));
                } else { 
                    el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.sortedSearchArray[idx].standardPrice));
                } 
                
//                el = $(PRODUCT_SEARCH.resultsDiscountPriceCellPrefix+idx);
//                if( UTILS.isEmpty(discountType)==true ) {
//                    el.style.display = 'none';
//                } else {
//                    el.style.display = 'inline';
//                }
                
                el = $(PRODUCT_SEARCH.resultsDiscountPricePrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onProductSelectDetail('+PRODUCT_SEARCH.sortedSearchArray[idx].index+');';
                if( UTILS.isEmpty(discountType)==false ) {
                    el.style.display = '';
                    if(discountType=='Discount') {
                        el.style.textDecoration='line-through';
                        el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.sortedSearchArray[idx].standardPrice));
                    } else {
                        el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.sortedSearchArray[idx].standardDiscount)+' '+discountType);
                    }
                } else {
                    el.style.display = 'none';
                }
                //#18729 - Added the check if the product is deliver by florist and carrier then display SDFC icon
                                              
                el = $(PRODUCT_SEARCH.resultsFlexfillPrefix+idx);
                if( PRODUCT_SEARCH.sortedSearchArray[idx].floristProduct==true && PRODUCT_SEARCH.sortedSearchArray[idx].vendorProduct==true  ) {
                    el.style.display = '';
                } else {
                    el.style.display = 'none';
                }
                
                el = $(PRODUCT_SEARCH.resultsClockPrefix+idx);
                if( PRODUCT_SEARCH.sortedSearchArray[idx].floristProduct==true ) {
                    el.style.display = '';
                } else {
                    el.style.display = 'none';
                }

                el = $('product_search_results_noFS_'+idx);
                if (PRODUCT_SEARCH.sortedSearchArray[idx].allowFreeShippingFlag == 'Y') {
                    el.style.display = 'none';
                } else {
                    el.style.display = '';
                }
                
                Event.observe(PRODUCT_SEARCH.resultsButtonPrefix+idx,"click",PRODUCT_SEARCH_EVENTS.onProductSelect);
                Event.observe(PRODUCT_SEARCH.resultsImagePrefix+idx,"error",ORDER_EVENTS.onImageLoadError);
                
                idx++;
            }
        }
    }
}

PRODUCT_SEARCH.populateSuggestProducts = function() {
    var tableBody = $('psDetailSuggestBody');
    FTD_DOM.removeAllTableRows(tableBody);
    var newRow = tableBody.insertRow(0);
    
    for( var idx=0; idx<PRODUCT_SEARCH.suggestProducts.length; idx++ ) {
        var newColumn = newRow.insertCell(idx);
        newColumn.className='productSearchSuggestColumn';
        newColumn.appendChild(PRODUCT_SEARCH.appendProductSearchResult(PRODUCT_SEARCH.suggestPrefix, idx));
        var discountType = PRODUCT_SEARCH.suggestProducts[idx].discountType;
        
        var el = $(PRODUCT_SEARCH.suggestTablePrefix+idx);
        el.className='productSearchSuggestTable';
        
        el = $(PRODUCT_SEARCH.suggestProductPrefix+idx);
        el.value=PRODUCT_SEARCH.suggestProducts[idx].productId;
        
        el = $(PRODUCT_SEARCH.suggestImageAnchorPrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onSuggestSelectDetail('+idx+');';
        
        el = $(PRODUCT_SEARCH.suggestImagePrefix+idx);
        el.src=PRODUCT_SEARCH.suggestProducts[idx].smallImage;
        el.alt=productVerbageCleanup(PRODUCT_SEARCH.suggestProducts[idx].name);
        el.title=productVerbageCleanup(PRODUCT_SEARCH.suggestProducts[idx].productDescription);
        //el.className='productSearchPromoImg';
        el.className='productSearchProductImg';
        
        el = $(PRODUCT_SEARCH.suggestNamePrefix+idx);
        el.innerHTML=PRODUCT_SEARCH.suggestProducts[idx].productId+' - '+PRODUCT_SEARCH.suggestProducts[idx].name;
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onSuggestSelectDetail('+idx+');';
        
        el = $(PRODUCT_SEARCH.suggestIotwRowPrefix+idx);
        if( PRODUCT_SEARCH.suggestProducts[idx].iotwObject.flag==true ) {
            el.style.display='';
            el = $(PRODUCT_SEARCH.suggestIotwDescPrefix+idx);
            el.innerHTML = iotwDescriptionWrapper(PRODUCT_SEARCH.suggestProducts[idx].iotwObject.message);
        } else {
            el.style.display='none';    
        }
        
        el = $(PRODUCT_SEARCH.suggestPricePrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onSuggestSelectDetail('+idx+');';
        if( discountType=='Discount' ) {
            el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.suggestProducts[idx].standardDiscount));
        } else { 
            el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.suggestProducts[idx].standardPrice));
        } 
        
//        el = $(PRODUCT_SEARCH.suggestDiscountPriceCellPrefix+idx);
//        if( UTILS.isEmpty(discountType)==true ) {
//            el.style.display = 'none';
//        } else {
//            el.style.display = 'inline';
//        }
        
        el = $(PRODUCT_SEARCH.suggestDiscountPricePrefix+idx);
        el.href='javascript:PRODUCT_SEARCH_EVENTS.onSuggestSelectDetail('+idx+');';
        if( UTILS.isEmpty(discountType)==false ) {
            el.style.display = '';
            if(discountType=='Discount') {
                el.style.textDecoration='line-through';
                el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.suggestProducts[idx].standardPrice));
            } else {
                el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.suggestProducts[idx].standardDiscount+' '+discountType));
            }
        } else {
            el.style.display = 'none';
        }
        //#18729 - Added the check if the product is deliver by florist and carrier then display SDFC icon
        el = $(PRODUCT_SEARCH.suggestFlexfillPrefix+idx);
        if( PRODUCT_SEARCH.suggestProducts[idx].floristProduct==true  != null && PRODUCT_SEARCH.suggestProducts[idx].vendorProduct==true) {
            el.style.display = '';
        } else {
            el.style.display = 'none';
        }
        
        el = $(PRODUCT_SEARCH.suggestClockPrefix+idx);
        if( PRODUCT_SEARCH.suggestProducts[idx].floristProduct==true ) {
            el.style.display = '';
        } else {
            el.style.display = 'none';
        }

        el = $('product_search_suggest_noFS_'+idx);
        if (PRODUCT_SEARCH.suggestProducts[idx].allowFreeShippingFlag == 'Y') {
            el.style.display = 'none';
        } else {
            el.style.display = '';
        }
        
        Event.observe(PRODUCT_SEARCH.suggestButtonPrefix+idx,"click",PRODUCT_SEARCH_EVENTS.onSuggestSelect);
        Event.observe(PRODUCT_SEARCH.suggestImagePrefix+idx,"error",ORDER_EVENTS.onImageLoadError);
    }
}

PRODUCT_SEARCH.setTopPageNavigation = function(resultsCount) {
    PRODUCT_SEARCH.currentTopPageIndex = 0;
    $('psTopNavLinks').innerHTML='';
    var maxProductsPerPage = PRODUCT_SEARCH.resultsRows*PRODUCT_SEARCH.resultsColumns;
    PRODUCT_SEARCH.maxTopPageIndex = parseInt((resultsCount/maxProductsPerPage)+'')-1;
    if( resultsCount%maxProductsPerPage!=0 ) PRODUCT_SEARCH.maxTopPageIndex++; 
    for( var idx=0; idx<=PRODUCT_SEARCH.maxTopPageIndex; idx++ ) {
        var anchor = document.createElement('a');
        var attr = document.createAttribute("href");
        attr.value = 'javascript:PRODUCT_SEARCH.setTopCurrentSearchPage('+idx+');';
        anchor.setAttributeNode(attr);
        attr = document.createAttribute("id");
        attr.value = 'psTopPageAnchor___'+idx;
        anchor.setAttributeNode(attr);
        anchor.innerHTML = (idx+1)+'';
        $('psTopNavLinks').appendChild(anchor);
        $('psTopNavLinks').innerHTML = $('psTopNavLinks').innerHTML+'&nbsp;';
    }
    
    if( resultsCount==0 )
        $('psTopNavButtons').style.display = 'none';
    else
        $('psTopNavButtons').style.display = 'block';
}

PRODUCT_SEARCH.setTopCurrentSearchPage = function(newPageIdx) {
    if( Object.isNumber(newPageIdx)==true && newPageIdx>=0 && newPageIdx<=PRODUCT_SEARCH.maxTopPageIndex ) {
        $('psTopPageAnchor___'+newPageIdx).style.fontSize = 'x-large';
        if( newPageIdx!=PRODUCT_SEARCH.currentTopPageIndex) {
            $('psTopPageAnchor___'+PRODUCT_SEARCH.currentTopPageIndex).style.fontSize = 'large';
        }
        PRODUCT_SEARCH.currentTopPageIndex=newPageIdx;
        PRODUCT_SEARCH.populateTopSellerProducts();
    }
}

PRODUCT_SEARCH.populateTopSellerProducts = function() {
    var tableBody = $('psTopSellersBody');
    FTD_DOM.removeAllTableRows(tableBody);
    var childArray;
    var maxProductsPerPage = PRODUCT_SEARCH.resultsRows*PRODUCT_SEARCH.resultsColumns;
    var startSearchIdx = PRODUCT_SEARCH.currentTopPageIndex*maxProductsPerPage;
    var endSearchIdx = (PRODUCT_SEARCH.currentTopPageIndex+1)*maxProductsPerPage;
    
    for( var idx=startSearchIdx; idx<PRODUCT_SEARCH.topProducts.length && idx<endSearchIdx; ) {
        
        for( var rowCount=0; rowCount<PRODUCT_SEARCH.resultsRows && idx<PRODUCT_SEARCH.topProducts.length && idx<endSearchIdx; rowCount++ ) {
            var newRow = tableBody.insertRow(rowCount);
            newRow.className='productSearchProductTable';
            
            for( var columnCount=0; columnCount<PRODUCT_SEARCH.resultsColumns && idx<PRODUCT_SEARCH.topProducts.length && idx<endSearchIdx; columnCount++ ) {
                var newColumn = newRow.insertCell(columnCount);
                newColumn.className='productSearchProductColumn';
                newColumn.appendChild(PRODUCT_SEARCH.appendProductSearchResult(PRODUCT_SEARCH.topPrefix, idx));
                var discountType = PRODUCT_SEARCH.topProducts[idx].discountType;
                
                var el = $(PRODUCT_SEARCH.topTablePrefix+idx);
                el.className='productSearchProductTable';
                
                el = $(PRODUCT_SEARCH.topProductPrefix+idx);
                el.value=PRODUCT_SEARCH.topProducts[idx].productId;
                
                el = $(PRODUCT_SEARCH.topImageAnchorPrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onTopSellerSelectDetail('+idx+');';
                
                el = $(PRODUCT_SEARCH.topImagePrefix+idx);
                el.src=PRODUCT_SEARCH.topProducts[idx].smallImage;
                el.alt=productVerbageCleanup(PRODUCT_SEARCH.topProducts[idx].name);
                el.title=productVerbageCleanup(PRODUCT_SEARCH.topProducts[idx].productDescription);
                el.className='productSearchProductImg';
                
                el = $(PRODUCT_SEARCH.topNamePrefix+idx);
                el.innerHTML=PRODUCT_SEARCH.topProducts[idx].productId+' - '+PRODUCT_SEARCH.topProducts[idx].name;
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onTopSellerSelectDetail('+idx+');';
        
                el = $(PRODUCT_SEARCH.topIotwRowPrefix+idx);
                if( PRODUCT_SEARCH.topProducts[idx].iotwObject.flag==true ) {
                    el.style.display='';
                    el = $(PRODUCT_SEARCH.topIotwDescPrefix+idx);
                    el.innerHTML = iotwDescriptionWrapper(PRODUCT_SEARCH.topProducts[idx].iotwObject.message);
                } else {
                    el.style.display='none';    
                }
                
                el = $(PRODUCT_SEARCH.topPricePrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onTopSellerSelectDetail('+idx+');';
                if( discountType=='Discount' ) {
                    el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.topProducts[idx].standardDiscount));
                } else { 
                    el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.topProducts[idx].standardPrice));
                } 
                
//                el = $(PRODUCT_SEARCH.topDiscountPriceCellPrefix+idx);
//                if( UTILS.isEmpty(discountType)==true ) {
//                    el.style.display = 'none';
//                } else {
//                    el.style.display = 'inline';
//                }
                
                el = $(PRODUCT_SEARCH.topDiscountPricePrefix+idx);
                el.href='javascript:PRODUCT_SEARCH_EVENTS.onTopSellerSelectDetail('+idx+');';
                if( UTILS.isEmpty(discountType)==false ) {
                    el.style.display = '';
                    if(discountType=='Discount') {
                        el.style.textDecoration='line-through';
                        el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.topProducts[idx].standardPrice));
                    } else {
                        el.innerHTML=UTILS.boldWrapper(UTILS.addCommas(PRODUCT_SEARCH.topProducts[idx].standardDiscount)+' '+discountType);
                    }
                } else {
                    el.style.display = 'none';
                }
                //#18729 - Added the check if the product is deliver by florist and carrier then display SDFC icon
                el = $(PRODUCT_SEARCH.topFlexfillPrefix+idx);
                if( PRODUCT_SEARCH.topProducts[idx].floristProduct==true && PRODUCT_SEARCH.topProducts[idx].vendorProduct==true ) {
                    el.style.display = '';
                } else {
                    el.style.display = 'none';
                }

                
                el = $(PRODUCT_SEARCH.topClockPrefix+idx);
                if( PRODUCT_SEARCH.topProducts[idx].floristProduct==true ) {
                    el.style.display = '';
                } else {
                    el.style.display = 'none';
                }

                el = $('product_search_top_noFS_'+idx);
                if (PRODUCT_SEARCH.topProducts[idx].allowFreeShippingFlag == 'Y') {
                    el.style.display = 'none';
                } else {
                    el.style.display = '';
                }
                
                Event.observe(PRODUCT_SEARCH.topButtonPrefix+idx,"click",PRODUCT_SEARCH_EVENTS.onTopSellerSelect);
                Event.observe(PRODUCT_SEARCH.topImagePrefix+idx,"error",ORDER_EVENTS.onImageLoadError);
                idx++;
            }
        }
    }
}

/* replace by JSON
PRODUCT_SEARCH.populateProductObjectFromSearch = function(record) {
    var prodObj = new PRODUCT.OBJECT(null);
    
    if( record!=null ) {
        prodObj.productId=FTD_XML.selectNodeText(record,'product-id');
        prodObj.masterProductId=prodObj.productId;
        //prodObj.name=FTD_XML.selectNodeText(record,'product-name'); 
        prodObj.name=FTD_XML.selectNodeText(record,'novator-name'); 
        prodObj.productDescription=FTD_XML.selectNodeText(record,'long-description');  //long description
        prodObj.subcodeDescription=null;
        
        var strValue = FTD_XML.selectNodeText(record,'standard-price');
        if( strValue=="0" ) strValue=null;
        if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.standardPrice=strValue;
        
        strValue = FTD_XML.selectNodeText(record,'premium-price');
        if( strValue=="0" ) strValue=null;
        if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.premiumPrice=strValue;
        
        strValue = FTD_XML.selectNodeText(record,'deluxe-price');
        if( strValue=="0" ) strValue=null;
        if( strValue!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.deluxePrice=strValue;
            
        prodObj.discountType=FTD_XML.selectNodeText(record,'discount-type'); //ie Discount
        
        strValue = FTD_XML.selectNodeText(record,'standard-price-discounted');
        if( strValue=="0" ) strValue=null; 
        if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.standardDiscount=strValue;
        
        strValue = FTD_XML.selectNodeText(record,'premium-price-discounted');
        if( strValue=="0" ) strValue=null;
        if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.premiumDiscount=strValue;
        
        strValue = FTD_XML.selectNodeText(record,'deluxe-price-discounted');
        if( strValue=="0" ) strValue=null;
        if( strValue!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.deluxeDiscount=strValue;
        
        prodObj.floristProduct=(FTD_XML.selectNodeText(record,'florist-delivered')=='Y');
        prodObj.vendorProduct=(FTD_XML.selectNodeText(record,'carrier-delivered')=='Y');
        
        strValue = FTD_XML.selectNodeText(record,'smallimage');
        if( strValue!=null && strValue.length>0 )
            prodObj.smallImage=strValue;
            
        strValue = FTD_XML.selectNodeText(record,'largeimage');
        if( strValue!=null && strValue.length>0 )
            prodObj.largeImage=strValue;
            
        strValue = FTD_XML.selectNodeText(record,'product-page-message');
        if( strValue!=null && strValue.length>0 ) {
            prodObj.iotwObject.flag=true;
            prodObj.iotwObject.message=strValue;
        }
        
        strValue = FTD_XML.selectNodeText(record,'popularity-order-count');
        //alert(strValue);
        if( UTILS.isEmpty(strValue)==true ) strValue="0";
        prodObj.popularOrder = strValue;
        
        prodObj.matchOrder = FTD_XML.getAttributeText(record,'row');
        prodObj.readyFlag=true;
    }
    
    return prodObj;
}
*/

PRODUCT_SEARCH.populateProductObjectFromSearchJSON = function(record, idx) {
    var prodObj = new PRODUCT.OBJECT(null);
    
    if( record!=null ) {
        prodObj.productId=record.product_id;
        prodObj.masterProductId=prodObj.productId;
        //prodObj.name=FTD_XML.selectNodeText(record,'product-name'); 
        prodObj.name=record.novator_name; 
        prodObj.productDescription=record.long_description;  //long description
        prodObj.subcodeDescription=null;
        
        var strValue = record.standard_price;
        if( strValue=="0" ) strValue=null;
        if( swapEmptyForNull(strValue)!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.standardPrice=strValue;
        
        strValue = record.premium_price;
        if( strValue=="0" ) strValue=null;
        if( swapEmptyForNull(strValue)!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.premiumPrice=strValue;
        
        strValue = record.deluxe_price;
        if( strValue=="0" ) strValue=null;
        if( swapEmptyForNull(strValue)!=null ) strValue=UTILS.formatStringToPrice(strValue);
        prodObj.deluxePrice=strValue;
            
        prodObj.discountType=record.discount_type; //ie Discount
        
        strValue = record.standard_price_discounted;
        if( strValue=="0" ) strValue=null; 
        if( swapEmptyForNull(strValue)!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.standardDiscount=strValue;
        
        strValue = record.premium_price_discounted;
        if( strValue=="0" ) strValue=null;
        if( swapEmptyForNull(strValue)!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.premiumDiscount=strValue;
        
        strValue = record.deluxe_price_discounted;
        if( strValue=="0" ) strValue=null;
        if( swapEmptyForNull(strValue)!=null && prodObj.discountType=='Discount')strValue=UTILS.formatStringToPrice(strValue);
        prodObj.deluxeDiscount=strValue;
        
        prodObj.floristProduct=(record.florist_delivered=='Y');
        prodObj.vendorProduct=(record.carrier_delivered=='Y');
        
        strValue = record.smallimage;
        if( swapEmptyForNull(strValue)!=null && strValue.length>0 )
            prodObj.smallImage=strValue;
            
        strValue = record.largeimage;
        if( swapEmptyForNull(strValue)!=null && strValue.length>0 )
            prodObj.largeImage=strValue;
            
        strValue = FTD_XML.selectNodeText(record,'product-page-message');
        if( swapEmptyForNull(strValue)!=null && strValue.length>0 ) {
            prodObj.iotwObject.flag=true;
            prodObj.iotwObject.message=strValue;
        }
        
        strValue = record.allow_free_shipping_flag;
        if( swapEmptyForNull(strValue)!=null && strValue.length>0 )
            prodObj.allowFreeShippingFlag=strValue;

        strValue = record.popularity_order_count;
        //alert(strValue);
        if( UTILS.isEmpty(strValue)==true ) strValue="0";
        prodObj.popularOrder = strValue;
        
        prodObj.matchOrder = idx;
        prodObj.readyFlag=true;
    }
    
    return prodObj;
}

PRODUCT_SEARCH.changeSortOrder = function(sortOrder) {
    var sortField;
    PRODUCT_SEARCH.sortedSearchArray = new Array();
    
    if(sortOrder=='ASCENDING') {
        sortField='standardPrice'    
    } else if(sortOrder=='DESCENDING') {
        sortField='standardPrice'
    } else if(sortOrder=='POPULAR') {
        sortField='popularOrder'
    } else {
        //if(sortOrder=='MATCH')
        sortField='matchOrder'
    }
    
    var workArray = new Array();
    var sortArray = new Array();
    var padStr = '0000000000';
    for( var idx=0; idx<PRODUCT_SEARCH.searchProducts.length; idx++ ) {
        var strValue1 = (padStr+PRODUCT_SEARCH.searchProducts[idx][sortField]);
        strValue1 = strValue1.slice(strValue1.length-padStr.length);
        var strValue2 = (padStr+PRODUCT_SEARCH.searchProducts[idx].productId);
        strValue2 = strValue2.slice(strValue2.length-padStr.length);
        workArray[strValue1+strValue2]=PRODUCT_SEARCH.searchProducts[idx]; 
        sortArray.push(strValue1+strValue2);
        //if(sortOrder=='POPULAR') {
        //    alert(strValue1+strValue2);
        //}
    }
    
    sortArray.sort();
    
    var obj;
    if( sortOrder=='DESCENDING' || sortOrder=='POPULAR') {        
        for( idx=sortArray.length-1; idx>=0; idx-- ) {
            obj = workArray[sortArray[idx]];
            if( Object.isFunction(obj)==false ) {
                PRODUCT_SEARCH.sortedSearchArray.push(obj);
            }
        }
    } else {
        for( idx=0; idx<sortArray.length; idx++ ) {
            obj = workArray[sortArray[idx]];
            if( Object.isFunction(obj)==false ) {
                PRODUCT_SEARCH.sortedSearchArray.push(obj);
            }
        }
    }
            
}

PRODUCT_SEARCH.appendProductSearchResult = function(idPrefix, idx) {
    var table = document.createElement("table");
    table.setAttribute("id",idPrefix+"table_"+idx);
    var tbody = document.createElement("tbody");
    table.appendChild(tbody);
    
    var rowCtr=0;
    
    //Row 1
    var row = tbody.insertRow(rowCtr++);
    var cell = row.insertCell(0);
    cell.width="90px";
    cell.align="center";
    cell.vAlign="top";
    cell.colSpan = "3";
    var input = new FTD_DOM.InputElement( {
        id:idPrefix+'product_id_'+idx,
        type:'hidden'
        } ).createNode();
    cell.appendChild(input);
    
    var anchor = new FTD_DOM.DOMElement('a', {
        id:idPrefix+'image_anchor_'+idx
    }).createNode();
    cell.appendChild(anchor);
    
    var div = new FTD_DOM.DOMElement('div', {
        align:'center'
    }).createNode();
    anchor.appendChild(div)
    
    var img = new FTD_DOM.DOMElement('img', {
        id:idPrefix+'image_'+idx,
        src:'images/npi_a.gif',
        'class':'productSearchPromoImg'
    }).createNode();
    div.appendChild(img);
    
    
    //Row 2
    row = tbody.insertRow(rowCtr++);
    cell = row.insertCell(0);
    cell.colSpan = "3";
    
    anchor = new FTD_DOM.DOMElement('a', {
        id:idPrefix+'name_anchor_'+idx
    }).createNode();
    cell.appendChild(anchor);
    
    
    //Row 3 (iotw promo msg)
    row = tbody.insertRow(rowCtr++);
    row.id = idPrefix+'iotw_row_'+idx;
    row.style.display = 'none';
    cell = row.insertCell(0);
    cell.id = idPrefix+'iotw_desc_'+idx;
    cell.colSpan = "3";
    
    
    //Row 4
    row = tbody.insertRow(rowCtr++);
    cell = row.insertCell(0);
    
    anchor = new FTD_DOM.DOMElement('a', {
        id:idPrefix+'price_anchor_'+idx
    }).createNode();
    cell.appendChild(anchor);
    
    cell = row.insertCell(1);
    cell.id=idPrefix+"discount_price_cell_"+idx;
    cell.colSpan = "2";
    
    anchor = new FTD_DOM.DOMElement('a', {
        id:idPrefix+'discount_price_anchor_'+idx
    }).createNode();
    cell.appendChild(anchor);
    
    
    //Row 5
    row = tbody.insertRow(rowCtr++);
    cell = row.insertCell(0);
    cell.align="left";
    
    var button = new FTD_DOM.DOMElement('button', {
        id:idPrefix+'button_'+idx,
        type:'button'
    }).createNode();
    button.innerHTML = "Select";
    cell.appendChild(button);

    //Row 6
    row = tbody.insertRow(rowCtr++);
    cell = row.insertCell(0);
    cell.align="center";
    var span = new FTD_DOM.SpanElement( {
        style:'float:right;'
        } ).createNode();
    cell.appendChild(span);

    img = new FTD_DOM.DOMElement('img', {
        id:idPrefix+'noFS_'+idx,
        src:'images/noFSicon.jpg',
        alt:'Not eligible for Free Shipping',
        title:'Not eligible for Free Shipping',
        width:'38px',
        height:'16px'
    }).createNode();
    span.appendChild(img);
    
    cell = row.insertCell(1);
    cell.align="center";
    span = new FTD_DOM.SpanElement( {
        style:'float:right;'
        } ).createNode();
    cell.appendChild(span);
    
    img = new FTD_DOM.DOMElement('img', {
        id:idPrefix+'flexfill_'+idx,
        src:'images/flexfill.png',
        alt:'Flexfill Product',
        title:'Flexfill Product',
        width:'40px',
        height:'21px'
    }).createNode();
    span.appendChild(img);
    
    cell = row.insertCell(2);
    cell.align="center";
    span = new FTD_DOM.SpanElement( {
        style:'float:right;'
        } ).createNode();
    cell.appendChild(span);
    

    img = new FTD_DOM.DOMElement('img', {
        id:idPrefix+'clock_'+idx,
        src:'images/clock.png',
        alt:'Same day delivery',
        title:'Same day delivery',
        width:'16px',
        height:'16px'
    }).createNode();
    span.appendChild(img);
    
    return table;
}

PRODUCT_SEARCH.setProductSearchFields = function() {
    var fld = findFieldObject('productSearchDeliveryDate',v_currentCartIdx);
    if( fld!=null ) fld.isRequired(false);
    
    fld = findFieldObject('productSearchZip',v_currentCartIdx);
    if( fld!=null ) fld.isRequired(false);
}

// ]]>
