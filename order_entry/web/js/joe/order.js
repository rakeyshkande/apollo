// <![CDATA[
var JOE_ORDER = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || Rico.prototypeVersion < 1.5)
      throw("ORDER requires the Prototype JavaScript framework >= 1.5");
      
JOE_ORDER.openAddressVerificationPanel = function() {
    var orderObj = v_cartList[v_currentCartIdx];
    if( orderObj==null ) return;
    
    $('avEnteredAddress').innerHTML = orderObj.recipientAddress;
    $('avEnteredCity').innerHTML = orderObj.recipientCity;
    $('avEnteredState').innerHTML = orderObj.recipientState;
    $('avEnteredZip').innerHTML = orderObj.recipientZip;
    
    if( $('addressVerificationDivId').visible()==false ) {
        $('tabDivId').style.display = 'none';
        new Effect.SlideDown('addressVerificationDivId');
    }
}

JOE_ORDER.closeAddressVerificationPanel = function() {
    if( $('addressVerificationDivId').visible()==true )
        $('addressVerificationDivId').style.display = 'none';
    if( $('tabDivId').visible()==false )
        new Effect.SlideDown('tabDivId');
}

JOE_ORDER.checkForRestrictedVendorDelivery = function(itemIdx,addressTypeId) {
    //Will the order be vendor delivered?
    if( itemIdx>-1 && 
        v_cartList[itemIdx].productObject.dropShipWarned==false && 
        v_cartList[itemIdx].floristDelivered==false && 
        v_cartList[itemIdx].carrierDelivered==true ) 
    {
        var selectedValue = FTD_DOM.getSelectedValue('recipientType');
        
        if( addressTypeId=='HOSPITAL' || addressTypeId=='FUNERAL HOME' ) {
            var selectedObject = v_locationTypes[selectedValue];
            //var strMsg = 'Florist delivery of product '+v_cartList[itemIdx].productId+' is unavailable.  Assembly may be required.  Timed delivery is not possible.  Do you really want to drop-ship this product to a '+ selectedObject.comboPrompt.toLowerCase()+'?';
            //notifyUser(strMsg,false,true);
            //notifyUser("This item is delivered via common carrier - assembly may be required.  Timed delivery is not possible.",false,true);
            v_cartList[itemIdx].productObject.dropShipWarned=true;
        } 
    }
}

// ]]>