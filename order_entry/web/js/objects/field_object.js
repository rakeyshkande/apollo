// <![CDATA[
var FIELD = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

if((typeof Prototype=='undefined') || FIELD.prototypeVersion < 1.6)
      throw("FIELD requires the Prototype JavaScript framework >= 1.6");
      
var v_fieldRequiredHtml = "<span style=\"color: red;\">*</span>";
var v_borderFocusColor = "GoldenRod";
var v_labelFocusColor = "DarkGoldenRod";
var v_selectedCaptionColor = 'PaleGoldenRod';
//var v_labelFocusErrorColor = "red";
var v_labelFocusErrorColor = "#cc0033";
    
FIELD.OBJECT = Class.create();

FIELD.OBJECT.prototype = {

    initialize: function(elementId,labelElementId,countElementId) {
        this.reset();
        
        if( $(elementId)!=null ) {
            this.elementId = elementId;
            
            if( $(labelElementId)!=null ) {
                this.labelElementId=labelElementId;
                this.labelText = $(this.labelElementId).innerHTML;
            }
            
            if( $(countElementId)!=null ) {
                this.countElementId = countElementId;
            }
        }
    },
    
    reset: function() {
        this.elementId = null;
        this.labelElementId = null;
        this.countElementId = null;
        this.type = null;
        this.labelText = null;
        this.title = null;
        this.script = null;
        this.maxLength = null;
        this.tabIndex = null;
        this.tabIndexInitial = null;
        this.tabIndexExpression = null;
        this.tabIndexExpressionType = null;
        this.recalcOrderOnChange = false;
        this.validateProductOnChange = false;
        this.errorFlag = false;
        
        //Validation fields
        this.requiredFlag = false;
        this.requiredConditionType = null;
        //this.requiredData = UTILS.convertToJavaScript('function() { alert("Got to the default conditional check");return true; }');
        this.requiredCondition = null;
        this.validateFlag = false;
        this.validateExpressionType = null;
        this.validateExpression = null;
        this.errorText = null;
        this.errorTextNormal = null;
        this.accordionCtrlIdx = null;
        this.tabCtrlIdx = null;
        this.accessKey=null;
        this.orderXmlNode=null;
        this.calcXmlFlag=false;
    },
    
    cloneNew: function() {
        var newObj = new FIELD.OBJECT();
        newObj.elementId = this.elementId;
        newObj.labelElementId = this.labelElementId;
        newObj.countElementId = this.countElementId;
        newObj.type = this.type;
        newObj.labelText = this.labelText;
        newObj.title = this.title;
        newObj.script = this.script;
        newObj.maxLength = this.maxLength;
        newObj.tabIndex = this.tabIndex;
        newObj.tabIndexInitial = this.tabIndexInitial;
        newObj.tabIndexExpression = this.tabIndexExpression;
        newObj.tabIndexExpressionType = this.tabIndexExpressionType;
        newObj.recalcOrderOnChange = this.recalcOrderOnChange;
        newObj.validateProductOnChange = this.validateProductOnChange;
        newObj.errorFlag = this.errorFlag;
        newObj.requiredFlag = this.requiredFlag;
        newObj.requiredConditionType = this.requiredConditionType;
        newObj.requiredCondition = this.requiredCondition;
        newObj.validateFlag = this.validateFlag;
        newObj.validateExpressionType = this.validateExpressionType;
        newObj.validateExpression = this.validateExpression;
        newObj.errorText = this.errorText;
        newObj.errorTextNormal = this.errorTextNormal;
        newObj.accordionCtrlIdx = this.accordionCtrlIdx;
        newObj.tabCtrlIdx = this.tabCtrlIdx;
        newObj.accessKey=this.accessKey;
        
        return newObj;
    },
    
    setup: function() {
        var el = $(this.elementId);
        var lblEl = $(this.labelElementId);
        var countEl = $(this.countElementId);
        
        if( !el || el==null ) {
            return;
        }
        
        if( this.requiredFlag==true && this.requiredCondition!=null ) {
            if(this.requiredConditionType=='PASSED_FUNCTION' || this.requiredConditionType=='FUNCTION') {
                if( this.type!='DETAIL' )
                    this.requiredCondition = UTILS.convertToJavaScript(this.requiredCondition);
            } else {
                alert('Required validation for field '+this.elementId +' is not set up correctly.  Contact support immediately.');
                return;
            }
        }
        
        if( this.validateFlag==true && this.validateExpression!=null ) {
            if( this.validateExpressionType=='REGEX' ) {
                this.validateExpression = new RegExp(this.validateExpression);
            } else if(this.validateExpressionType=='PASSED_FUNCTION' || this.validateExpressionType=='FUNCTION' ) {
                if( this.type!='DETAIL' )
                    this.validateExpression = UTILS.convertToJavaScript(this.validateExpression);
            } else {
                alert('Validation for field '+this.elementId +' is not set up correctly.  Contact support immediately.\r\nValidation Type: '+this.validateExpressionType+'\r\nType'+this.type);
                return;
            }
        }
        
        if(lblEl && this.requiredFlag == true && this.requiredConditionType == null) {
            var strLabel = this.labelText;
            if( UTILS.trim(strLabel).length>0 )
                lblEl.innerHTML=v_fieldRequiredHtml+this.labelText;
            else
                lblEl.innerHTML=v_fieldRequiredHtml;
        }
        
        if( lblEl ) {
            new FTD_DOM.FocusElement(el.id, v_borderFocusColor, lblEl.id, v_labelFocusColor);
        }
        
        if( this.maxLength!=null ) {
            if(  el.nodeName.toUpperCase() == "TEXTAREA" && countEl!=null ) {
                new FTD_DOM.CharCountDown(this.elementId, this.countElementId, this.maxLength);
                countEl.style.color=v_labelFocusColor;
            } else if( el.nodeName.toUpperCase() == "INPUT" && ( el.type.toLowerCase()=="text" || el.type.toLowerCase()=="password") ) {
                el.maxLength=this.maxLength;
            }
        }
        
        if( UTILS.isEmpty(this.title)==false ) {
            el.title = this.title;
            
            if( lblEl ) {
                lblEl.title = this.title;
            }
        }
        
        if( this.tabIndexInitial!=null ) {
            el.tabIndex=this.tabIndexInitial;
        } else {
            if( this.tabIndex==null )
                el.tabIndex="-1";
            else
                el.tabIndex=this.tabIndex;
        }
        
        if( this.accessKey==null ) {
            el.accessKey="";
        } else {
            el.accessKey=this.accessKey;
        }
    },
    
    isRequired: function() {
        //Order object will take care of this
        if(this.type=='DETAIL') return false;
        var retval;
        
        try {
            if( this.requiredFlag==true ) {
                if( this.requiredCondition==null || this.requiredCondition=='undefined' /*|| this.requiredCondition.length==0*/ ) {
                    retval = true;
                } else {
                    if(this.requiredConditionType=='PASSED_FUNCTION') {
                        retval = this.requiredCondition();
                    } else if(this.requiredConditionType=='FUNCTION') {
                        retval = this.requiredCondition(this.elementId);
                    }
                }
            } else {
                retval = false;
            }
        } catch(err) {
            alert('Function isRequired failed for field '+this.elementId);
            throw(err.description);
        }
            
        var labelEl = $(this.labelElementId);
    
        if(labelEl) {
            if( retval==true ) {
                labelEl.innerHTML=v_fieldRequiredHtml+this.labelText;
            } else {
                labelEl.innerHTML=this.labelText;    
            }
        }
        
        return retval;
    },
    
    validate: function(checkOnly) {
        //Order object takes care of detail validations
        if( this.type=='DETAIL' ) return true;
        
        var retval = true;
        var testValue='';
        
        var el = $(this.elementId);
        
        if( !el || el==null || Object.isElement(el)==false ) {
            return true;
        }
        
        try {
            if(el.type == 'select' ) {
                testValue = UTILS.trim(FTD_DOM.getSelectedValue());
            } else if( UTILS.isEmpty(el.type)==false && (
                      el.type.toLowerCase() == 'text' || 
                      el.type.toLowerCase() == 'textarea' || 
                      el.type.toLowerCase() == 'select-one' || 
                      el.type.toLowerCase() == 'password') ) {
                testValue = UTILS.trim(el.getValue());
            } else if(el.type == 'radio' || 
                      el.type == 'checkbox') {
                testValue = 'donottest';
            } else {
                //alert('Unsupported control of type '+el.type+' being validated for control '+el.id);
                return true;
            }
            
        } catch (err) {
            alert('Error getting value from '+el.id+' control: '+err.description);
            return true;
        }
        
        if( this.requiredFlag==true || this.validateFlag==true ) {
            
            //alert(this.elementId+' value: '+testValue);
            var isrequired = this.isRequired();
            if( isrequired==true && UTILS.isEmpty(testValue)==true ) {
                retval = false;
               var x= this.updateRequest();
            } else if( isrequired==false && UTILS.isEmpty(testValue)==true ) {
                retval = true;
            } else if( this.validateFlag==true ) {
                var valFlag=false;
                if( this.validateExpression==null || this.validateExpression=='undefined' /*|| this.validateExpression.length==0*/ ) {
                    valFlag = false;
                } else {
                    if( this.validateExpressionType=='REGEX') {
                        valFlag=this.validateExpression.test(testValue);
                    } else if(this.validateExpressionType=='PASSED_FUNCTION') {
                        valFlag = this.validateExpression();
                    } else if(this.validateExpressionType=='FUNCTION') {
                        valFlag = this.validateExpression(this.elementId,this);
                    }
                }
                
                if( valFlag==false ) {
                    retval = false;
                }
            } 
        }
        
        if( retval==true && el.type=='textarea' && isNaN(this.maxLength)==false && UTILS.isEmpty(testValue)==false && testValue.length > parseInt(this.maxLength) ) {
            retval = false;
            this.errorText = 'Entry is too large.  Reduce to '+this.maxLength+' characters or less.';
        } else {
            this.errorText = this.errorTextNormal;
        }
        
        if( checkOnly==false ) { 
            if( retval==false ) {
                this.errorFlag = true;
            } else {
                this.errorFlag = false;
            }
        }
        
        return retval;
    },

    setFieldStyle: function() {
        if( this.elementId!=null ) {
            try{
                var el = $(this.elementId);
                if( Object.isUndefined(el) ) {
                    alert('HTML element for id '+this.elementId+' not found in FIELD_OBJECT.setFieldStyle');
                    return;
                }
                if( el.nodeName.toUpperCase()=='LABEL' ) return;
                if( el.nodeName.toUpperCase()=='INPUT' && el.type.toLowerCase()=='hidden' ) return;
                
                if( this.errorFlag==true ) {
                    el.style.backgroundColor = v_errorBgColor;
                    el.style.color = v_errorTextColor;
                    el.style.fontWeight = 'bold';
                    el.setAttribute('title',this.errorText);
                } else {
                    if( el.nodeName.toUpperCase()=='INPUT' && el.type.toLowerCase()=='checkbox' )
                        el.style.backgroundColor = 'transparent';
                    else
                        el.style.backgroundColor = v_normalBgColor;
                    el.style.color = v_normalTextColor;
                    el.style.fontWeight = 'normal';
                    el.setAttribute('title',this.title);
                }
            } catch(err) {
                alert(err.description);
                throw (err);
            }
        }
    },
    updateRequest:function(){
    	if(this.elementId=='customerFirstName'|| this.elementId=='customerLastName' || this.elementId=='customerAddress' ||this.elementId=='customerCity'){    		
    		this.errorTextNormal="You must enter the customer's "+this.labelText;
    	}
    	return true;
    }
};
// ]]>