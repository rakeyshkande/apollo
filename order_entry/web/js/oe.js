// <![CDATA[
var OE = {
  Version: '1.0',
  prototypeVersion: parseFloat(Prototype.Version.split(".")[0] + "." + Prototype.Version.split(".")[1])
}

function parseResponse(transport) {
    var xmlStr = transport.responseText.replace(/\s+$/, "");
//    if( xmlStr.substring(0,5)!='<?xml' ) xmlStr = '<?xml version="1.0" encoding="ISO-8859-1"?>'+xmlStr;
    var xmlDoc = FTD_XML.parseXmlString(xmlStr);
    var root = xmlDoc.documentElement;
    
    if( root.getAttribute('status')!='Y' ) {
        alert(root.getAttribute('message'));
        return null;
    }
    
    return xmlDoc;
}

function parseResponseQuietly(transport) {
    var xmlStr = transport.responseText.replace(/\s+$/, "");
//    if( xmlStr.substring(0,5)!='<?xml' ) xmlStr = '<?xml version="1.0" encoding="ISO-8859-1"?>'+xmlStr;
    var xmlDoc = new DOMParser().parseFromString(xmlStr, "text/xml");
    var root = xmlDoc.documentElement;
    
    if( root.getAttribute('status')!='Y' ) {
        return null;
    }
    
    return xmlDoc;
}

function isModifyOrderCustomerInfo(fld) {
    if (v_modifyOrderFromCom == true && (
            fld.elementId=='customerAddress' || 
            fld.elementId=='customerBillingPhone' || 
            fld.elementId=='customerBillingPhoneExt' || 
            fld.elementId=='customerCity' ||
            fld.elementId=='customerCountryCombo' ||
            fld.elementId=='customerEmailAddress' ||
            fld.elementId=='customerEveningPhone' ||
            fld.elementId=='customerEveningPhoneExt' ||
            fld.elementId=='customerFirstName' ||
            fld.elementId=='customerLastName' ||
            fld.elementId=='customerStateCombo' ||
            fld.elementId=='customerZip')) 
    {
    	return true;
    } else {
    	return false;
    }	
}

function setElementConfig(records,targetArray) {
    for( idx=0; idx<records.length; idx++ ) {
        var elementId = FTD_XML.selectNodeText(records[idx],'element_id');
        var labelElementId = FTD_XML.selectNodeText(records[idx],'label_element_id');
        var countElementId = FTD_XML.selectNodeText(records[idx],'count_element_id');
        var fld = new FIELD.OBJECT(elementId,labelElementId,countElementId);
        fld.type = FTD_XML.selectNodeText(records[idx],'type');
        if (isModifyOrderCustomerInfo(fld) == true) {
            // For Modify Orders don't validate buyer info (since fields are not editable)
        	fld.requiredFlag = false;
            fld.validateFlag = false;  
        } else {
            fld.requiredFlag = (FTD_XML.selectNodeText(records[idx],'required_flag')=='Y');
            fld.requiredCondition = FTD_XML.selectNodeText(records[idx],'required_condition');
            fld.requiredConditionType = FTD_XML.selectNodeText(records[idx],'required_condition_type');
            fld.validateFlag = (FTD_XML.selectNodeText(records[idx],'validate_flag')=='Y');
            fld.validateExpression = FTD_XML.selectNodeText(records[idx],'validate_expression');
            fld.validateExpressionType = FTD_XML.selectNodeText(records[idx],'validate_expression_type');        	
        }
        fld.errorText = FTD_XML.selectNodeText(records[idx],'error_text');
        fld.errorTextNormal = fld.errorText;
        fld.title = FTD_XML.selectNodeText(records[idx],'title');
        fld.maxLength = FTD_XML.selectNodeText(records[idx],'max_size');
        fld.tabIndex = FTD_XML.selectNodeText(records[idx],'tab_idx');
        fld.tabIndexInitial = FTD_XML.selectNodeText(records[idx],'tab_idx_initial');
        fld.tabIndexExpression = FTD_XML.selectNodeText(records[idx],'tab_idx_expression');
        fld.tabIndexExpressionType = FTD_XML.selectNodeText(records[idx],'tab_idx_expression_type');
        fld.accordionCtrlIdx = FTD_XML.selectNodeText(records[idx],'accordion_ctrl_idx');
        fld.tabCtrlIdx = FTD_XML.selectNodeText(records[idx],'tab_ctrl_idx');
        fld.recalcOrderOnChange = (FTD_XML.selectNodeText(records[idx],'recalc_flag')=='Y');
        fld.validateProductOnChange = (FTD_XML.selectNodeText(records[idx],'product_validate_flag')=='Y');
        fld.accessKey = FTD_XML.selectNodeText(records[idx],'access_key');
        fld.orderXmlNode = FTD_XML.selectNodeText(records[idx],'order_xml_node');
        fld.calcXmlFlag = (FTD_XML.selectNodeText(records[idx],'calc_xml_flag')=='Y');
        targetArray.push(fld);
        
        if(fld.validateProductOnChange==true) {
            v_availabilityFields.push(fld);
        }
        
        if(fld.recalcOrderOnChange==true) {
            v_recalcFields.push(fld);
        }
        
        if( fld.elementId=='recipientAddress' || 
            fld.elementId=='recipientCity' || 
            fld.elementId=='recipientState' || 
            fld.elementId=='recipientZip' || 
            fld.elementId=='recipientCountry') {
                
            v_avsFields.push(fld);        
        }
        
        fld.setup();
    }
}

function setElementConfigJSON(record,targetArray) {
    var elementId = swapEmptyForNull(record.element_id);
    var labelElementId = swapEmptyForNull(record.label_element_id);
    var countElementId = swapEmptyForNull(record.count_element_id);
    var fld = new FIELD.OBJECT(elementId,labelElementId,countElementId);
    fld.type = swapEmptyForNull(record.type);
    if (isModifyOrderCustomerInfo(fld) == true) {
        // For Modify Orders don't validate buyer info (since fields are not editable)
        fld.validateFlag = false;  
        fld.requiredFlag = false;
    } else {
        fld.requiredFlag = (record.required_flag == 'Y');
        fld.requiredCondition = swapEmptyForNull(record.required_condition);
        fld.requiredConditionType = swapEmptyForNull(record.required_condition_type);
        fld.validateFlag = (record.validate_flag == 'Y');
        fld.validateExpression = swapEmptyForNull(record.validate_expression);
        fld.validateExpressionType = swapEmptyForNull(record.validate_expression_type);
    }
    fld.errorText = swapEmptyForNull(record.error_text);
    fld.errorTextNormal = swapEmptyForNull(fld.errorText);
    fld.title = swapEmptyForNull(record.title);
    fld.maxLength = swapEmptyForNull(record.max_size);
    fld.tabIndex = swapEmptyForNull(record.tab_idx);
    fld.tabIndexInitial = swapEmptyForNull(record.tab_idx_initial);
    fld.tabIndexExpression = swapEmptyForNull(record.tab_idx_expression);
    fld.tabIndexExpressionType = swapEmptyForNull(record.tab_idx_expression_type);
    fld.accordionCtrlIdx = swapEmptyForNull(record.accordion_ctrl_idx);
    fld.tabCtrlIdx = swapEmptyForNull(record.tab_ctrl_idx);
    fld.recalcOrderOnChange = (record.recalc_flag =='Y');
    fld.validateProductOnChange = (record.product_validate_flag =='Y');
    fld.accessKey = swapEmptyForNull(record.access_key);
    fld.orderXmlNode = swapEmptyForNull(record.order_xml_node);
    fld.calcXmlFlag = (record.calc_xml_flag =='Y');
    targetArray.push(fld);

    if(fld.validateProductOnChange==true) {
        v_availabilityFields.push(fld);
    }
    
    if(fld.recalcOrderOnChange==true) {
        v_recalcFields.push(fld);
    }
    
    if( fld.elementId=='recipientAddress' || 
        fld.elementId=='recipientCity' || 
        fld.elementId=='recipientState' || 
        fld.elementId=='recipientZip' || 
        fld.elementId=='recipientCountry') {
            
        v_avsFields.push(fld);        
    }
    
    fld.setup();
}

function swapEmptyForNull(value){
    if(UTILS.isEmpty(value))
        return null;
    else
        return value;
}

function doMainMenuAction(adminAction){
    var url = "MainMenuAction.do" + getSecurityParams(true,adminAction);
    performAction(url);
}

function performAction(url){
    document.forms[0].action = url;
    document.forms[0].submit();
}

// Function for returning to view new order in COM after a Modify Order
//
function performReturnToComSearchAction(order_number){
	// comForm contains all request data that originated from COM
    document.forms['comForm'].action.value = 'search';
    document.forms['comForm'].in_order_number.value = order_number;
    document.forms['comForm'].modify_order_in_joe.value = 'Y';  // Since we're here, Modify Order was performed
    clearCallDispo();  // We don't want any Call Disposition info lingering from original order
    // alert("Return with new in_order_number: " + document.forms['comForm'].in_order_number.value); 
    document.forms['comForm'].submit();
}

// Function for returning to original order in COM after a Modify Order (or abandoned Modify Order)
//
function performReturnToComOriginalOrderAction(){
	// comForm contains all request data that originated from COM
    document.forms['comForm'].action.value = 'customer_account_search';
    // Modify Order may or may not have been done - JS variable should reflect this
    if (v_modify_order_in_joe == true) {
       document.forms['comForm'].modify_order_in_joe.value = 'Y';
    } else {
       document.forms['comForm'].modify_order_in_joe.value = 'N';    	
    }
    // alert("Return to original with in_order_number: " + document.forms['comForm'].in_order_number.value); 
    document.forms['comForm'].submit();
}


function getSecurityParams(areOnlyParams,adminAction){
    var retval = ((areOnlyParams) ? "?" : "&") 
    retval += "securitytoken=" 
    retval += document.getElementById("securitytoken").value 
    retval += "&context=" 
    retval += document.getElementById("context").value
    retval += "&adminAction="
    
    if( UTILS.isEmpty(adminAction)==true )
        retval+=document.getElementById("adminAction").value;
    else
        retval+=adminAction;
    
    return retval;
}


function displayFeeOnMouseOver(object)
{

  var testId = object.id;
  var testIdx = object.idx;

  var mouseOverString = "" ;  
  document.getElementById("ci_spanItemMouseOver___" + testIdx).innerHTML = mouseOverString;
  
  var surchargeFees = document.getElementById("ci_surchargeFees___" + testIdx).innerHTML;
  var sameDayUpcharge = document.getElementById("ci_same_day_fee_amount___" + testIdx).innerHTML;
  
  var lateCutOff = document.getElementById("ci_late_cutoff_fee_amount___" + testIdx).innerHTML;
  var mondayUpcharge = document.getElementById("ci_vendor_mon_upcharge_amount___" + testIdx).innerHTML;
  var sundayUpcharge = document.getElementById("ci_vendor_sun_upcharge_amount___" + testIdx).innerHTML;
  var internationalFee = document.getElementById("ci_international_fee_amount___" + testIdx).innerHTML;
  var serviceFee = document.getElementById("ci_service_fee_amount___" + testIdx).innerHTML;
  
  
  
  var applySurchargeCode = document.getElementById("applySurchargeCode").value ;
  var sameDayUpchargeMouseOver = document.getElementById("sameDayUpchargeMouseOver").value;
  var lateCutOffMouseOver = document.getElementById("lateCutOffMouseOver").value;
  var mondayUpchargeMouseOver = document.getElementById("mondayUpchargeMouseOver").value;
  var sundayUpchargeMouseOver = document.getElementById("sundayUpchargeMouseOver").value;
  var internationalFeeMouseOver = document.getElementById("internationalFeeMouseOver").value;
  var serviceFeeMouseOver = document.getElementById("serviceFeeMouseOver").value;
  
   if (applySurchargeCode == "OFF"&& internationalFeeMouseOver == "OFF"&&sameDayUpchargeMouseOver == "OFF"&&serviceFeeMouseOver  == "OFF"&& lateCutOffMouseOver == "OFF" && mondayUpchargeMouseOver == "OFF" && sundayUpchargeMouseOver == "OFF")
   {    
	   document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="0px";
	   document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="0px";
	   document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="transparent";
	   document.getElementById("ci_spanItemMouseOver___" + testIdx).innerHTML = mouseOverString;
	   
   }
   else
   {
        var feesDescription = document.getElementById("surchargeDescription").value ;        
        mouseOverString = "Fee Contains:";
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(sameDayUpcharge) == 0 &&parseFloat(internationalFee) == 0 &&
        		parseFloat(lateCutOff) == 0 && parseFloat(mondayUpcharge) == 0 && parseFloat(sundayUpcharge) == 0){        	
           document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="0px";
     	   document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="0px";
     	   document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="transparent";
     	   document.getElementById("ci_spanItemMouseOver___" + testIdx).innerHTML = "";
        }
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(serviceFee) > 0){     
        	mouseOverString = mouseOverString+"<br>"+ "Service Fee:  $"+serviceFee;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
          
        if(parseFloat(surchargeFees) == 0 && parseFloat(sameDayUpcharge) > 0){        	
        	mouseOverString = mouseOverString+"<br>"+"Same Day Fee: $"+sameDayUpcharge;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(lateCutOff) > 0){        	
        	mouseOverString = mouseOverString+"<br>"+"Late Cutoff: $"+lateCutOff;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(mondayUpcharge) > 0){        	
        	mouseOverString = mouseOverString+"<br>"+"Monday Upcharge: $"+mondayUpcharge;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(sundayUpcharge) > 0){        	
	    	mouseOverString = mouseOverString+"<br>"+"Sunday Upcharge: $"+sundayUpcharge;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
        
        if(parseFloat(surchargeFees) == 0 && parseFloat(internationalFee) > 0){     
        	
        	mouseOverString = mouseOverString+"<br>"+ "International Fee:  $"+internationalFee;
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
        	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        }
        
        
        if(parseFloat(surchargeFees) > 0){
        	
        	if(parseFloat(surchargeFees) > 0 && parseFloat(serviceFee) > 0){ 
           	 
              	mouseOverString = mouseOverString+"<br>"+ "Service Fee:  $"+serviceFee;
              	
            }
        	
        	if(parseFloat(sameDayUpcharge) > 0){        	
            	mouseOverString = mouseOverString+"<br>"+"Same Day Fee:  $"+sameDayUpcharge;
            	
            }
        	
        	 if(parseFloat(lateCutOff) > 0){        	
             	mouseOverString = mouseOverString+"<br>"+"Late Cutoff: $"+lateCutOff;
             	
             }
             
             if(parseFloat(surchargeFees) > 0 && parseFloat(mondayUpcharge) > 0){        	
             	mouseOverString = mouseOverString+"<br>"+"Monday Upcharge: $"+mondayUpcharge;
             	
             }
             
             if(parseFloat(surchargeFees) > 0 && parseFloat(sundayUpcharge) > 0){        	
             	mouseOverString = mouseOverString+"<br>"+"Sunday Upcharge: $"+sundayUpcharge;
             	
             }
             
             
             mouseOverString = mouseOverString+"<br>"+feesDescription+": $"+surchargeFees;
         	
             
             if(parseFloat(surchargeFees) > 0 && parseFloat(internationalFee) > 0){ 
            	 
            	 mouseOverString = mouseOverString+"<br>"+ "International Fee: $"+internationalFee;
              	
              }
              
             document.getElementById("ci_spanItemMouseOver___" + testIdx).style.width="16em";
          	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.border="1px solid #3669AC";
          	document.getElementById("ci_spanItemMouseOver___" + testIdx).style.background="#CCEFFF";
        	
        }
       
        if(document.getElementById("original_order_info").style.display=='block')
              document.getElementById("cartDiv").style.overflow='hidden';
        
        document.getElementById("ci_spanItemMouseOver___" + testIdx).innerHTML = mouseOverString;    
   }
  
}

function displayFeeTotalsOnMouseOver()
{
  var mouseOverString = "" ;
  document.getElementById("spanTotalsMouseOver").innerHTML = mouseOverString;
  document.getElementById("spanTotalsMouseOver").style.width="0px";
  document.getElementById("spanTotalsMouseOver").style.border="0px";
  document.getElementById("spanTotalsMouseOver").style.background="transparent";   
  
  var applySurchargeCode = document.getElementById("applySurchargeCode").value ;
  var sameDayUpchargeMouseOver = document.getElementById("sameDayUpchargeMouseOver").value;
  var lateCutOffMouseOver = document.getElementById("lateCutOffMouseOver").value;
  var mondayUpchargeMouseOver = document.getElementById("mondayUpchargeMouseOver").value;
  var sundayUpchargeMouseOver = document.getElementById("sundayUpchargeMouseOver").value;
  var internationalFeeMouseOver = document.getElementById("internationalFeeMouseOver").value;
  var serviceFeeMouseOver = document.getElementById("serviceFeeMouseOver").value;
  
  var totalFees = document.getElementById("totalSurchargeFee").value ;
  var totalSameDayFees = document.getElementById("total_same_day_fee_amount").innerHTML ;
  var totalLateCutoff = document.getElementById("total_late_cutoff_amount").innerHTML ;
  var totalMondayUpcharge = document.getElementById("total_monday_upcharge_amount").innerHTML ;
  var totalSundayUpcharge = document.getElementById("total_sunday_upcharge_amount").innerHTML ;
  var totalInternationalFee = document.getElementById("total_international_fee_amount").innerHTML ;
  var totalServiceFee = document.getElementById("total_service_fee_amount").innerHTML ;
  
   if (applySurchargeCode == "OFF" && sameDayUpchargeMouseOver == "OFF" && internationalFeeMouseOver == "OFF" 
	   &&serviceFeeMouseOver  == "OFF"&&lateCutOffMouseOver == "OFF" && mondayUpchargeMouseOver == "OFF" && sundayUpchargeMouseOver == "OFF")
   {    
	   document.getElementById("spanTotalsMouseOver").style.width="0px";
	   document.getElementById("spanTotalsMouseOver").style.border="0px";
	   document.getElementById("spanTotalsMouseOver").style.background="transparent";   
   }
   else
   {
    var feesDescription = document.getElementById("surchargeDescription").value ;    
    mouseOverString = "Fee Contains:";
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalSameDayFees) == 0){    	
       document.getElementById("spanTotalsMouseOver").style.width="0px";
 	   document.getElementById("spanTotalsMouseOver").style.border="0px";
 	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalLateCutoff) == 0){    	
        document.getElementById("spanTotalsMouseOver").style.width="0px";
  	   document.getElementById("spanTotalsMouseOver").style.border="0px";
  	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
     }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalMondayUpcharge) == 0){    	
        document.getElementById("spanTotalsMouseOver").style.width="0px";
  	   document.getElementById("spanTotalsMouseOver").style.border="0px";
  	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
     }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalSundayUpcharge) == 0){    	
        document.getElementById("spanTotalsMouseOver").style.width="0px";
  	   document.getElementById("spanTotalsMouseOver").style.border="0px";
  	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
     }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalInternationalFee) == 0){   
    	
        document.getElementById("spanTotalsMouseOver").style.width="0px";
  	   document.getElementById("spanTotalsMouseOver").style.border="0px";
  	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
     }
     
    if(parseFloat(totalFees) == 0 && parseFloat(totalServiceFee) == 0){   
    	
        document.getElementById("spanTotalsMouseOver").style.width="0px";
  	   document.getElementById("spanTotalsMouseOver").style.border="0px";
  	   document.getElementById("spanTotalsMouseOver").style.background="transparent"; 	
     }
      
   if(parseFloat(totalFees) == 0 && parseFloat(totalServiceFee) > 0){  
    	
    	mouseOverString = mouseOverString+"<br>"+ "Service Fee: $"+totalServiceFee;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalSameDayFees) > 0){    	
    	mouseOverString = mouseOverString+"<br>"+  "Same Day Fee:  $"+totalSameDayFees;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalLateCutoff) > 0){    	
    	mouseOverString = mouseOverString+"<br>"+"Late Cutoff: $"+totalLateCutoff;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalMondayUpcharge) > 0){    	
    	mouseOverString = mouseOverString+"<br>"+"Monday Upcharge: $"+totalMondayUpcharge;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalSundayUpcharge) > 0){    	
    	mouseOverString = mouseOverString+"<br>"+"Sunday Upcharge: $"+totalSundayUpcharge;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
    if(parseFloat(totalFees) == 0 && parseFloat(totalInternationalFee) > 0){  
    	
    	mouseOverString = mouseOverString+"<br>"+ "International Fee: $"+totalInternationalFee;
    	document.getElementById("spanTotalsMouseOver").style.width="12em";
    	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
    	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    }
    
   
    
    if(parseFloat(totalFees) > 0){
    	
    		if(parseFloat(totalServiceFee) > 0){ 
	    	
    			mouseOverString = mouseOverString+"<br>"+ "Service Fee: $"+totalServiceFee;
	    	
    		}
    	
    	 	if(parseFloat(totalFees) > 0 && parseFloat(totalSameDayFees) > 0){    	
    	    	mouseOverString = mouseOverString+"<br>"+ "Same Day Fee: $"+totalSameDayFees;
    	    	
    	    }
    	
    	 	if(parseFloat(totalLateCutoff) > 0){    	
    	    	mouseOverString = mouseOverString+"<br>"+"Late Cutoff: $"+totalLateCutoff;
    	    	
    	    }
    	    
    	    if(parseFloat(totalMondayUpcharge) > 0){    	
    	    	mouseOverString = mouseOverString+"<br>"+"Monday Upcharge: $"+totalMondayUpcharge;
    	    	
    	    }
    	    
    	    if(parseFloat(totalSundayUpcharge) > 0){    	
    	    	mouseOverString = mouseOverString+"<br>"+"Sunday Upcharge: $"+totalSundayUpcharge;
    	    	
    	    }
    	    
    	    
    	   mouseOverString = mouseOverString+"<br>"+feesDescription+": $"+totalFees;
        	
    	    
    	    if(parseFloat(totalInternationalFee) > 0){ 
    	    	
    	    	mouseOverString = mouseOverString+"<br>"+ "International Fee: $"+totalInternationalFee;
    	    	
    	    }
    	    document.getElementById("spanTotalsMouseOver").style.width="12em";
        	document.getElementById("spanTotalsMouseOver").style.border="1px solid #3669AC";
        	document.getElementById("spanTotalsMouseOver").style.background="#CCEFFF";
    	    
    	
    	
    }
    
   
    document.getElementById("spanTotalsMouseOver").innerHTML = mouseOverString;

   }  
}
 
// ]]>

