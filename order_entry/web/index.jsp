<%
	String securitytoken = (String)request.getParameter("securitytoken");
%>
<!-- Main Table -->
<table width="98%" border="0" cellpadding="2" cellspacing="2">
    <!--caption/-->
    <thead/>
    <tbody>
        <tr>
            <TD width="15%" class="Header3"> Order Entry </td>
            <TD width="85%"> &nbsp; </td>
        </tr>
        <tr>
            <td> &nbsp; </td>
            <td>
                <a href="oe.do?securitytoken=<%=securitytoken%>">
                    Order Entry
                </a>                     
            </td>
        </tr>       
        <tr>
            <td> &nbsp; </td>
            <td>
                <a href="iotw.do?securitytoken=<%=securitytoken%>">
                    Item of the Week (IOTW)
                </a>                     
            </td>
        </tr>                
        <tr>
            <td> &nbsp; </td>
            <td>
                <a href="crossSell.do?securitytoken=<%=securitytoken%>">
                    Cross Sell
                </a>                     
            </td>
        </tr>                 
        <tr>
            <td> &nbsp; </td>
            <td>
                <a href="phrases.do?securitytoken=<%=securitytoken%>">
                    Card Message Phrase Maintenance
                </a>                     
            </td>
        </tr>               
        <tr>
            <td> &nbsp; </td>
            <td>
                <a href="favorites.do?securitytoken=<%=securitytoken%>">
                    Favorite Products Maintenance
                </a>                     
            </td>
        </tr>                        
        <tr>
            <td> <hr> </td>
            <td> &nbsp; </td>
        </tr>
    </tbody>
</table>