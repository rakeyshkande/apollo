<%
    String identity = (String)request.getAttribute("identity");
    if( identity==null || identity.length()==0 ) identity="testcsr1";
    String secureURL = (String)request.getAttribute("secure-url");
    if( secureURL==null ) 
        secureURL="";
    else
        secureURL+="/";
%>

<script type="text/javascript">
  var v_repIdentity = '<%=identity%>';
  var v_serverURLPrefix = '<%=secureURL%>';
</script>


<script type="text/javascript" src="js/utilities/prototype.js"></script>
<script type="text/javascript" src="js/utilities/rico.js"></script>
<script type="text/javascript" src="js/utilities/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/oe.js"></script>
<script type="text/javascript" src="js/utilities/timer.js"></script>
<script type="text/javascript" src="js/utilities/ftddom.js"></script>
<script type="text/javascript" src="js/utilities/ftdxml.js"></script>
<script type="text/javascript" src="js/utilities/ftdajax.js"></script>
<script type="text/javascript" src="js/utilities/ftdutils.js"></script>
<script type="text/javascript" src="js/utilities/sorttable.js"></script>
<script type="text/javascript" src="js/favorites/favorites.js"></script>
<script type="text/javascript" src="js/favorites/favorites_events.js"></script>
<script type="text/javascript" src="js/favorites/favorites_ajax.js"></script>
<script type="text/javascript" src="js/xml/javeline_xpath.js"></script>
<script type="text/javascript" src="js/xml/sarissa.js"></script>

<form action="/phrases.do">
    <jsp:include page="includes/security.jsp"/>
    <div class="iotwDiv">
        <div class="iotwInnerDiv">
            <div style="overflow:auto;height:500px">
              <table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
			    <tbody>
			      <tr> 
			        <td width="150px" align="left" style="vertical-align:middle;padding-top:.4em;">&nbsp;</td>
			        <td align="center" >
		                <table border="1" title="To remove a product, delete the product id." align="center">
		                  <caption>Favorite Products Maintenance</caption>
		                  <thead/>
		                  <tbody>
		                    <tr>
		                        <td class="labelRight"><label id="product1Label" for="product1" class="labelRight">Product 1</label></td>
		                        <td><input type="text" id="product1" size="15" maxlength="10"/></td>
		                    </tr>
		                    <tr>
		                        <td class="labelRight"><label id="product2Label" for="product2" class="labelRight">Product 2</label></td>
		                        <td><input type="text" id="product2" size="15" maxlength="10"/></td>
		                    </tr>
		                    <tr>
		                        <td class="labelRight"><label id="product3Label" for="product3" class="labelRight">Product 3</label></td>
		                        <td><input type="text" id="product3" size="15" maxlength="10"/></td>
		                    </tr>
		                  </tbody>
		                </table>
			        </td>
			        <td width="150px" align="right" style="vertical-align:middle;padding-top:.4em;">&nbsp;</td>
			      </tr>			
			      
			    </tbody>
  			</table>
            </div>
            <div id="favNotifyDivId" style="display:none" class="favNotifyDiv">
                <div id="favInnerNodifyDivId" class="favInnerNotifyDiv">
                    <div align="center">
                    Alert!
                    </div>
                    <div id="favNotifyArea" 
                         class="favNotifyArea"
                         align="left" 
                         >
                    </div>
                </div>
            </div>
            
            <div class="iotwButtonsDiv">
                <button type="button" id="save_button">Save</button>&nbsp;&nbsp;
                <button type="button" id="exit_button">Exit</button>
            </div>
        </div>
    </div>
</form>