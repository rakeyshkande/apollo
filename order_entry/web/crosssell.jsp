<%
    String identity = (String)request.getAttribute("identity");
    if( identity==null || identity.length()==0 ) identity="testcsr1";
    String secureURL = (String)request.getAttribute("secure-url");
    if( secureURL==null ) 
        secureURL="";
    else
        secureURL+="/";
%>

<script type="text/javascript">
  var v_repIdentity = '<%=identity%>';
  var v_serverURLPrefix = '<%=secureURL%>';
</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<script type="text/javascript" src="js/utilities/prototype.js"></script>
<script type="text/javascript" src="js/utilities/rico.js"></script>
<script type="text/javascript" src="js/utilities/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/oe.js"></script>
<script type="text/javascript" src="js/utilities/ftddom.js"></script>
<script type="text/javascript" src="js/utilities/ftdxml.js"></script>
<script type="text/javascript" src="js/utilities/ftdajax.js"></script>
<script type="text/javascript" src="js/utilities/ftdutils.js"></script>
<script type="text/javascript" src="js/utilities/sorttable.js"></script>
<script type="text/javascript" src="js/utilities/timer.js"></script>
<script type="text/javascript" src="js/cross_sell/crosssell.js"></script>
<script type="text/javascript" src="js/cross_sell/crosssell_events.js"></script>
<script type="text/javascript" src="js/cross_sell/crosssell_ajax.js"></script>
<script type="text/javascript" src="js/joe/objects/product_object.js"></script>
<script type="text/javascript" src="js/joe/objects/cross_sell_object.js"></script>
<script type="text/javascript" src="js/joe/objects/iotwoe_object.js"></script>
<script type="text/javascript" src="js/xml/javeline_xpath.js"></script>
<script type="text/javascript" src="js/xml/sarissa.js"></script>
<script type="text/javascript">Table.setFilterLabel("All");</script>
<div>
<form id="xcForm" action="crossSellMaint.do" method="post">
    <jsp:include page="includes/security.jsp"/>
    
    <div id="xcSearchDiv">
        <div id="xcSearchInnerDiv">    
            <div class="oeMaintSearchDiv">
                <table>
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table>
                                    <caption>Cross Sell Edit</caption>
                                    <thead/>
                                    <tbody>
                                        <tr>
                                          <td class="labelRight"><label id="xcMainProductIdLabel" for="xcMainProductId" class="labelRight">Product&nbsp;Id</label></td>
                                          <td><input type="text" id="xcMainProductId" size="15" maxlength="10"/></td>
                                          <td align="left"><button type="button" id="xcEditButton">Edit</button>
                                        </tr>
                                        <tr>
                                          <td class="labelRight"><label id="xcProductIdLabel" for="xcProductId" class="labelRight">Cross&nbsp;Sell&nbsp;Product&nbsp;Id</label></td>
                                          <td><input type="text" id="xcProductId" size="15" maxlength="10"/></td>
                                          <td align="left"><button type="button" id="xcRemoveButton">Edit</button>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <caption>Cross Sell Search</caption>
                                    <thead/>
                                    <tbody>
                                        <tr>
                                          <td class="labelRight"><label id="xcCompanyLabel" for="xcCompany" class="labelRight">Company</label></td>
                                          <td>
                                            <select id="xs_select" name="xs_select">
                                                <option value="ALL">ALL</option>
                                                <option value="FTD">FTD</option>
                                            </select>
                                          </td>
                                          <td align="left"><button type="button" id="xcCompanySearch">Submit</button>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="oeMaintListDiv">
                <div style="overflow:auto; height:400px; width:1024">
                    <display:table name="crossSells" id="crossSellVO" requestURI="crossSellMaint.do" class="sorttable altstripe sort01">
                        <display:caption>Search Results</display:caption>
                        <display:column property="productId" sortable="true" title="Product Id"/>
                        <display:column property="novatorId" sortable="true" title="Novator Id"/>
                        <display:column  sortable="true" title="Available">
                            ${crossSellVO.status} 
                            <c:if test="${crossSellVO.over21Flag == 'Y'}"><img src="images/over21.jpg" alt=""  /></c:if>
                            <c:if test="${crossSellVO.productSameDay}"><img src="images/clock.gif" alt="" /></c:if>
                        </display:column>
                        <display:column sortable="true" title="Description" >
                            <div class="xcProductLink${crossSellVO.status}">
                            <a  href="#" onclick="return showCrossSellProduct('${crossSellVO.productId}') " >${crossSellVO.novatorName}</a>
                            </div>
                        </display:column>
                        <c:forEach items="${crossSellVO.detailVO}" var = "detail" varStatus="status">
                            <display:column title="SKU ${status.count}" sortable="true">
                                ${detail.displayId}
                            </display:column>
                            <display:column title="Available" sortable="true">
                                ${detail.status}
                                <c:if test="${detail.over21Flag == 'Y'}"><img src="images/over21.jpg" alt=""  /></c:if>
                                <c:if test="${detail.productSameDay}"><img src="images/clock.gif" alt=""  /></c:if>
                            </display:column>
                        </c:forEach>
                        <display:footer>
                          <tr>
                            <td><img src="images/40.gif" border="0"/></td>
                            <td><img src="images/50.gif" border="0"/></td>
                            <td><img src="images/50.gif" border="0"/></td>
                            <td><img src="images/200.gif" border="0"/></td>
                            <c:forEach items="${crossSellVO.detailVO}" var="temp">
                                <td><img src="images/40.gif" border="0"/></td>
                                <td><img src="images/50.gif" border="0"/></td>
                            </c:forEach>
                          <tr>
                        </display:footer>
                    </display:table>
                </div>
                <div class="oeMaintButtonsDiv">
                    <button type="button" id="exit_button">Exit</button>
                </div>
            </div>
        </div>
    </div>
    
    <div id="xcProductMaintDiv" class="oeMaintDetailDiv" style="display:none;">
        <div id="xcProductMaintInnerDiv" class="xcMaintDetailInnerDiv">
            <table>
                <caption>Cross Sell Product Maintenance</caption>
                <thead/>
                <tbody>
                    <tr>
                        <td style="background-color:#eeeeee;">
                            <div style="height:200px;width:178px;padding:2px;">
                                <!--
                                <img
                                  src="images/npi_c.jpg"
                                  id="xcdProductImage"
                                  alt=""
                                  title=""
                                  height="240px"/>
                                  -->
                                <img
                                  src="http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/products/BB02_c.jpg"
                                  id="xcdProductImage"
                                  alt=""
                                  title=""
                                  height="200px"/>
                            </div>
                        </td>
                        <td style="width:100%;vertical-align:top;background-color:#eeeeee;">
                            <div style="padding:2px;">
                                <table width="100%" border="0">
                                    <!--caption/-->
                                    <thead/>
                                    <tbody>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Product Id</label></td>
                                            <td><span id="xcdProductId">BB02</span></td>
                                        </tr>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Novator Id</label></td>
                                            <td><span id="xcdNovatorId">BB02</span></td>
                                        </tr>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Title</label></td>
                                            <td><span id="xcdProductTitle">Build-A-Bear Workshop&reg; I'll Love You Fur Always Bear&#153;</span></td>
                                        </tr>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Description</label></td>
                                            <td><span id="xcProductDesc">Send a message of love with this charmer Stuffed with Hugs and Good Wishes&reg; by our friends at Build-A-Bear Workshop&reg;. This child safe and adult friendly bear is all dressed up in his best vest, bearing red velvet roses and ready to love.  Includes birth certificate for the bear.</span></td>
                                        </tr>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Price</label></td>
                                            <td><span id="xcdProductPrice">46.99</span></td>
                                        </tr>
                                        <tr>
                                            <td class="labelRight"><label class="labelRight">Availability</label></td>
                                            <td><span  id="xcdProductAvailable">Yes</span><div id="xcdOver21" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcdOver21Image" /></div><div id="xcdSameDay" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcdSameDayImage" /></div><span  id="xcdProductAvailableException">Yes</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div id="xcdNotifyDivId" style="display:none" class="xcdNotifyDiv">
                                                    <div id="xcdInnerNodifyDivId" class="xcdInnerNotifyDiv">
                                                        <div align="left">
                                                        Alert!
                                                        </div>
                                                        <div id="xcdNotifyArea" class="xcdNotifyArea" align="left">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="xcEditDiv" class="xcMaintEditDiv" style="display:none;">
        <div id="xcEditInnerDiv" class="xcMaintEditInnerDiv">
            <table width="100%">
                <caption>Cross Sell SKUs</caption>
                <thead/>
                <tbody>
                <tr><td>
    <div id="xcEditRowSubDiv" style="display:none">
    <table border="0" width="95%" align="center">
                    <tr>
                        <td valign="top">
                            <table align="center" bgcolor="#eeeeee" class="xcdTable" border="0">
                                <caption><div id="skuNumber_dummy">&nbsp;</div></caption>
                                <thead/>
                                <tbody id="xcdTD_dummy" style="display:none">
                                    <tr>
                                        <td valign="center" colspan="2">
                                            <div align="center">
                                                <input type="text" id="xcdProductId_dummy" size="15" maxlength="10"/>
                                                &nbsp;&nbsp;
                                                <button type="button" name="xcdProductLookup" id="xcdProductLookup_dummy">
                                                    <img src="images/magnify.jpg" width="16px" height="16px" alt="Search"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="xcdProductImage">
                                            <div align="center">
                                                <img src="images/noproduct.gif" id="xcdImage_dummy" width="90px" height="90px" alt="Product image"/>
                                            </div>
                                        </td>
                                        <td valign="center" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductName"><span id="xcdProductName_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductException"><span id="xcdProductException_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="xcdProductPrice_dummy">&nbsp;</span>&nbsp;
                                            <span id="xcdProductAvailable_dummy">&nbsp;</span>
                                            <div id="xcdOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcd1Over21Image" /></div>
                                            <div id="xcdSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcd1SameDayImage" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div align="center">
                                                <button type="button" name="xcdLeft" id="xcdLeft_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_left.jpg"  alt="Move to the left"/>
                                                </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" id="xcdRemoveButton_dummy">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" name="xcdRight" id="xcdRight_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_right.jpg"  alt="Move to the right"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--tr>
                                        <td colspan="2" align="center"><div align="center"><button type="button" id="xcdRemoveButton_dummy">Remove</button></div></td>
                                    </tr-->
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <table align="center" bgcolor="#eeeeee" class="xcdTable" border="0">
                                <caption><div id="skuNumber_dummy">&nbsp;</div></caption>
                                <thead/>
                                <tbody id="xcdTD_dummy" style="display:none">
                                    <tr>
                                        <td valign="center" colspan="2">
                                            <div align="center">
                                                <input type="text" id="xcdProductId_dummy" size="15" maxlength="10"/>
                                                &nbsp;&nbsp;
                                                <button type="button" name="xcdProductLookup" id="xcdProductLookup_dummy">
                                                    <img src="images/magnify.jpg" width="16px" height="16px" alt="Search"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="xcdProductImage">
                                            <div align="center">
                                                <img src="images/noproduct.gif" id="xcdImage_dummy" width="90px" height="90px" alt="Product image"/>
                                            </div>
                                        </td>
                                        <td valign="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductName"><span id="xcdProductName_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductException"><span id="xcdProductException_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="xcdProductPrice_dummy">&nbsp;</span>&nbsp;
                                            <span id="xcdProductAvailable_dummy">&nbsp;</span>
                                            <div id="xcdOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcd2Over21Image" /></div>
                                            <div id="xcdSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcd2SameDayImage" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div align="center">
                                                <button type="button" name="xcdLeft" id="xcdLeft_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_left.jpg"  alt="Move to the left"/>
                                                </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" id="xcdRemoveButton_dummy">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" name="xcdRight" id="xcdRight_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_right.jpg"  alt="Move to the right"/>
                                                </button>
                                            </div>
                                         </td>
                                    </tr>
                                    <!--tr>
                                        <td colspan="2" align="center"><div align="center"><button type="button" id="xcdRemoveButton_dummy">Remove</button></div></td>
                                    </tr-->
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <table align="center" bgcolor="#eeeeee" class="xcdTable" border="0">
                                <caption><div id="skuNumber_dummy">&nbsp;</div></caption>
                                <thead/>
                                <tbody id="xcdTD_dummy" style="display:none">
                                    <tr>
                                        <td valign="center" colspan="2">
                                            <div align="center">
                                                <input type="text" id="xcdProductId_dummy" size="15" maxlength="10"/>
                                                &nbsp;&nbsp;
                                                <button type="button" name="xcdProductLookup" id="xcdProductLookup_dummy">
                                                    <img src="images/magnify.jpg" width="16px" height="16px" alt="Search"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="xcdProductImage">
                                            <div align="center">
                                                <img src="images/noproduct.gif" id="xcdImage_dummy" width="90px" height="90px" alt="Product image"/>
                                            </div>
                                        </td>
                                         <td valign="center">
                                            &nbsp;
                                        </td>
                                   </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductName"><span id="xcdProductName_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductException"><span id="xcdProductException_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="xcdProductPrice_dummy">&nbsp;</span>&nbsp;
                                            <span id="xcdProductAvailable_dummy">&nbsp;</span>
                                            <div id="xcdOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcd3Over21Image" /></div>
                                            <div id="xcdSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcd3SameDayImage" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div align="center">
                                                <button type="button" name="xcdLeft" id="xcdLeft_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_left.jpg"  alt="Move to the left"/>
                                                </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" id="xcdRemoveButton_dummy">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" name="xcdRight" id="xcdRight_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_right.jpg"  alt="Move to the right"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--tr>
                                        <td colspan="2" align="center"><div align="center"><button type="button" id="xcdRemoveButton_dummy">Remove</button></div></td>
                                    </tr-->
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <table align="center" bgcolor="#eeeeee" class="xcdTable" border="0">
                                <caption><div id="skuNumber_dummy">&nbsp;</div></caption>
                                <thead/>
                                <tbody id="xcdTD_dummy" style="display:none">
                                    <tr>
                                        <td valign="center" colspan="2">
                                            <div align="center">
                                                <input type="text" id="xcdProductId_dummy" size="15" maxlength="10"/>
                                                &nbsp;&nbsp;
                                                <button type="button" name="xcdProductLookup" id="xcdProductLookup_dummy">
                                                    <img src="images/magnify.jpg" width="16px" height="16px" alt="Search"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="xcdProductImage">
                                            <div align="center">
                                                <img src="images/noproduct.gif" id="xcdImage_dummy" width="90px" height="90px" alt="Product image"/>
                                            </div>
                                        </td>
                                         <td valign="center">
                                            &nbsp;
                                        </td>
                                   </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductName"><span id="xcdProductName_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductException"><span id="xcdProductException_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="xcdProductPrice_dummy">&nbsp;</span>&nbsp;
                                            <span id="xcdProductAvailable_dummy">&nbsp;</span>
                                            <div id="xcdOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcd3Over21Image" /></div>
                                            <div id="xcdSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcd3SameDayImage" /></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div align="center">
                                                <button type="button" name="xcdLeft" id="xcdLeft_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_left.jpg"  alt="Move to the left"/>
                                                </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" id="xcdRemoveButton_dummy">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" name="xcdRight" id="xcdRight_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_right.jpg"  alt="Move to the right"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--tr>
                                        <td colspan="2" align="center"><div align="center"><button type="button" id="xcdRemoveButton_dummy">Remove</button></div></td>
                                    </tr-->
                                </tbody>
                            </table>
                        </td>
                        <td valign="top">
                            <table align="center" bgcolor="#eeeeee" class="xcdTable" border="0">
                                <caption><div id="skuNumber_dummy">&nbsp;</div></caption>
                                <thead/>
                                <tbody id="xcdTD_dummy" style="display:none">
                                    <tr>
                                        <td valign="center" colspan="2">
                                            <div align="center">
                                                <input type="text" id="xcdProductId_dummy" size="15" maxlength="10"/>
                                                &nbsp;&nbsp;
                                                <button type="button" name="xcdProductLookup" id="xcdProductLookup_dummy">
                                                    <img src="images/magnify.jpg" width="16px" height="16px" alt="Search"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="xcdProductImage">
                                            <div align="center">
                                                <img src="images/noproduct.gif" id="xcdImage_dummy" width="90px" height="90px" alt="Product image"/>
                                            </div>
                                        </td>
                                         <td valign="center">
                                            &nbsp;
                                        </td>
                                   </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductName"><span id="xcdProductName_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" class="xcdProductException"><span id="xcdProductException_dummy">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <span id="xcdProductPrice_dummy">&nbsp;</span>&nbsp;
                                            <span id="xcdProductAvailable_dummy">&nbsp;</span>
                                            <div id="xcdOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcd3Over21Image" /></div>
                                            <div id="xcdSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcd3SameDayImage" /></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div align="center">
                                                <button type="button" name="xcdLeft" id="xcdLeft_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_left.jpg"  alt="Move to the left"/>
                                                </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" id="xcdRemoveButton_dummy">Remove</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" name="xcdRight" id="xcdRight_dummy" style="visibility:hidden;">
                                                    <img src="images/navigate_right.jpg"  alt="Move to the right"/>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <!--tr>
                                        <td colspan="2" align="center"><div align="center"><button type="button" id="xcdRemoveButton_dummy">Remove</button></div></td>
                                    </tr-->
                                </tbody>
                            </table>
                        </td>
                    </tr>
    </table>
    </div>
<div id="xcEditRowDiv" class="xcMaintEditRowDiv">
</div>
</td></tr>
                </tbody>
            </table>
            
            <div class="xcMaintNovatorDiv">
                <input type="checkbox"
                      id="xcdToLive"
                      class="novatorToLive"
                      title="Send updates to Novator production web site"/>
                <label class="radioLabel" id="toLiveLabel" for="xcdToLive">Live</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcdToContent"
                      class="novatorToContent"
                      title="Send updates to Novator content web site"/>
                <label class="radioLabel" id="toContentLabel" for="xcdToContent">Content</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcdToTest"
                      class="novatorToTest"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="toTestLabel" for="xcdToTest">Test</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcdToUAT"
                      class="novatorToUAT"
                      title="Send updates to Novator UAT web site"/>
                <label class="radioLabel" id="toUATLabel" for="xcdToUAT">UAT</label>
            </div>
            <div class="xcMaintButtonsDiv">
                <button type="button" id="xcdSubmitButton">Submit</button>
                <button type="button" id="xcdRemoveButton">Remove All</button>
                <button type="button" id="xcdBackButton">Back</button>
            </div>
        </div>
    </div>
    
    <div id="xcrNotifyDivId" style="display:none" class="xcMaintEditDiv">
        <div id="xcrInnerNodifyDivId" class="xcrInnerNotifyDiv">
            <div align="center">
            Alert!
            </div>
            <div id="xcrNotifyArea" 
                align="left" 
                style="background-color:#eeeeee;color:black;overflow:auto;height:100%;padding-left:2px">
            </div>
            <div class="xcMaintButtonsDiv">
                <button type="button" id="xcrCloseRemoveError">Back</button>
            </div>
        </div>
    </div>

    <div id="xcRemoveDiv" class="xcMaintEditDiv" style="display:none;">
        <div id="xcRemoveInnerDiv" class="xcMaintEditInnerDiv">
        <input type="hidden" value="0" id="xcrCounter" name="xcrCounter" />
            <div id="xcRemoveTableDiv" class="xcRemoveTableDiv" >
            <table id="xcRemoveTable" width="100%" class="sorttable altstripe sort01 table-autostripe table-autofilter table-autosort:1 table-stripeclass:alternate2">
                <caption>Products that use the SKU for Cross Sell</caption>
                <thead>
                    <tr>
                        <th><input id="xcrAll" type="checkbox"/>All</th>
                        <th class="table-sortable:default">Product Id</th>
                        <th class="table-sortable:default">Novator Id</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody id="xcRemoveBodyDummy" style="display:none">
                    <tr id="xcRemoveBodyRow_dummy">
                        <td>
                            <input type="checkbox" id="xcrCheckbox_dummy"/>
                        </td>
                        <td>
                            <span id="xcrProductId_dummy"></span>
                            <div id="xcrOver21_dummy" class="xcOver21"><img src="images/over21.jpg" alt="" id="xcrOver21Image" /></div>
                            <div id="xcrSameDay_dummy" class="xcSameDay"><img src="images/clock.gif" alt="" id="xcrSameDayImage" /></div>
                        </td>
                        <td>
                            <span id="xcrNovatorId_dummy"></span>
                        </td>
                        <td>
                            <span id="xcrDescription_dummy"></span>
                        </td>
                    </tr>
                </tbody>
                <tbody id="xcRemoveBody">
                </tbody>
            </table>
            </div>
            <div class="xcMaintNovatorDiv">
                <input type="checkbox"
                      id="xcrToLive"
                      title="Send updates to Novator production web site"/>
                <label class="radioLabel" id="cxToLiveLabel" for="xcrToLive">Live</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcrToContent"
                      title="Send updates to Novator content web site"/>
                <label class="radioLabel" id="xcToContentLabel" for="xcrToContent">Content</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcrToTest"
                      title="Send updates to Novator test web site"/>
                <label class="radioLabel" id="xcToTestLabel" for="xcrToTest">Test</label>&nbsp;&nbsp;
                <input type="checkbox"
                      id="xcrToUAT"
                      title="Send updates to Novator UAT web site"/>
                <label class="radioLabel" id="xcToUATLabel" for="xcrToUAT">UAT</label>
            </div>
            <div class="xcMaintButtonsDiv">
                <button type="button" id="xcrRemoveButton">Remove</button>
                <button type="button" id="xcrBackButton">Back</button>
            </div>
        </div>
    </div>
</form>
</div>