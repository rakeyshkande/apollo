package com.ftd.monitortool;

import com.ftd.monitortool.client.alert.AlertJInternalFrame;
import com.ftd.monitortool.client.load.LoadJInternalFrame;
import com.ftd.monitortool.client.queue.QueueJInternalFrame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class MonitorJFrame extends JFrame
{
    private BorderLayout layoutMain = new BorderLayout();
    private JDesktopPane centerJDesktopPane = new JDesktopPane();
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menuFile = new JMenu();
    private JMenuItem menuFileExit = new JMenuItem();
    private JMenu menuMonitor = new JMenu();
    private JMenu menuMonitorQueue = new JMenu();
    private JMenuItem menuMonitorOrderQueue = new JMenuItem();
    private JMenuItem menuMonitorSpikeQueue = new JMenuItem();
    private JMenuItem menuMonitorLoad = new JMenu();
    private JMenuItem menuMonitorLoadApp = new JMenuItem();
    private JMenuItem menuMonitorLoadDb = new JMenuItem();
    private JMenuItem menuMonitorLoadOther = new JMenuItem();
    private JMenuItem menuMonitorLog = new JMenuItem();
    private JMenu menuHelp = new JMenu();
    private JMenuItem menuHelpAbout = new JMenuItem();
    private JLabel statusBar = new JLabel();
    
    private DatabaseProperties databaseProperties;
    
    public MonitorJFrame()
    {
        try
        {
            jbInit();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception
    {
        this.setJMenuBar( menuBar );
        this.getContentPane().setLayout( layoutMain );
        this.setSize( new Dimension(950, 600) );
        this.setTitle( "Monitor" );
        menuFile.setText( "File" );
        menuFileExit.setText( "Exit" );
        menuFileExit.addActionListener( new ActionListener() { public void actionPerformed( ActionEvent ae ) { fileExit_ActionPerformed( ae ); } } );
        menuMonitor.setText( "Monitor" );

        menuMonitorQueue.setText( "Queue" );
        menuMonitorOrderQueue.setText( "Order" );
        menuMonitorOrderQueue.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorOrderQueue_ActionPerformed( ae ); } } );
        menuMonitorSpikeQueue.setText( "Spike" );
        menuMonitorSpikeQueue.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorSpikeQueue_ActionPerformed( ae ); } } );

        menuMonitorLoad.setText( "Load" );
        menuMonitorLoadDb.setText( "Db" );
        menuMonitorLoadDb.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorLoadDb_ActionPerformed( ae ); } } );
        menuMonitorLoadApp.setText( "App" );
        menuMonitorLoadApp.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorLoadApp_ActionPerformed( ae ); } } );
        menuMonitorLoadOther.setText( "Other" );
        menuMonitorLoadOther.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorLoadOther_ActionPerformed( ae ); } } );
        
        menuMonitorLog.setText( "Log" );
        menuMonitorLog.addActionListener( new ActionListener() 
        { public void actionPerformed( ActionEvent ae ) { monitorLog_ActionPerformed( ae ); } } );

        menuHelp.setText( "Help" );
        menuHelpAbout.setText( "About" );
        menuHelpAbout.addActionListener( new ActionListener() { public void actionPerformed( ActionEvent ae ) { helpAbout_ActionPerformed( ae ); } } );
        statusBar.setText("");

        menuFile.add( menuFileExit );
        menuMonitorQueue.add( menuMonitorOrderQueue );
        menuMonitorQueue.add( menuMonitorSpikeQueue );
        menuMonitorLoad.add( menuMonitorLoadDb);
        menuMonitorLoad.add( menuMonitorLoadApp );
        menuMonitorLoad.add( menuMonitorLoadOther );
        menuMonitor.add( menuMonitorQueue );
        menuMonitor.add( menuMonitorLoad );
        menuMonitor.add( menuMonitorLog );
        menuBar.add( menuFile );
        menuBar.add( menuMonitor );
        menuHelp.add( menuHelpAbout );
        menuBar.add( menuHelp );
        this.getContentPane().add( statusBar, BorderLayout.SOUTH );
        this.getContentPane().add( centerJDesktopPane, BorderLayout.CENTER );
    }

    void fileExit_ActionPerformed(ActionEvent e)
    {
        System.exit(0);
    }

    void helpAbout_ActionPerformed(ActionEvent e)
    {
        JOptionPane.showMessageDialog(this, new AboutBoxJPanel(), "About", JOptionPane.PLAIN_MESSAGE);
    }
    
    void monitorSpikeQueue_ActionPerformed(ActionEvent e)
    {
        monitorQueue_ActionPerformed("spike");
    }
    void monitorOrderQueue_ActionPerformed(ActionEvent e)
    {
        monitorQueue_ActionPerformed("order");
    }
    void monitorQueue_ActionPerformed(String category)
    {
        if (databaseProperties == null)
        {
            retrieveDatabaseProperties();
        }
        QueueJInternalFrame queueJInternalFrame = new QueueJInternalFrame(databaseProperties,category);
        queueJInternalFrame.setVisible(true);
        centerJDesktopPane.add(queueJInternalFrame);
        
    }

    void monitorLoadDb_ActionPerformed(ActionEvent e)
    {
        monitorLoad_ActionPerformed("Db");
    }
    void monitorLoadApp_ActionPerformed(ActionEvent e)
    {
        monitorLoad_ActionPerformed("App");
    }
    void monitorLoadOther_ActionPerformed(ActionEvent e)
    {
        monitorLoad_ActionPerformed("Other");
    }
    void monitorLoad_ActionPerformed(String category)
    {
        LoadJInternalFrame loadJInternalFrame = new LoadJInternalFrame(category);
        loadJInternalFrame.setVisible(true);
        centerJDesktopPane.add(loadJInternalFrame);
    }

    void monitorLog_ActionPerformed(ActionEvent e)
    {
        AlertJInternalFrame logJInternalFrame = new AlertJInternalFrame(this);
        logJInternalFrame.setVisible(true);
        centerJDesktopPane.add(logJInternalFrame);
    }
    
    private void retrieveDatabaseProperties()
    {
        
    }

}
