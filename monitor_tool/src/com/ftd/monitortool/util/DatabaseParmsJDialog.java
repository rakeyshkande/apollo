package com.ftd.monitortool.util;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class DatabaseParmsJDialog extends JDialog
{
    JLabel usernameJLabel;
    JLabel passwordJLabel;
    JLabel hostJLabel;
    JLabel portJLabel;
    JLabel sidJLabel;
    
    JTextField usernameJTextField;
    JPasswordField passwordJPasswordField;
    JTextField hostJTextField;
    JTextField portJTextField;
    JTextField sidJTextField;
    
    JButton acceptJButton;
    
    public DatabaseParmsJDialog()
    {
        init();
    }
    
    private void init()
    {
        usernameJLabel = new JLabel("Username:");
        passwordJLabel = new JLabel("Password:");
        hostJLabel = new JLabel("Host:");
        portJLabel = new JLabel("Port:");
        sidJLabel = new JLabel("Sid:");
        usernameJTextField = new JTextField(20);
        passwordJPasswordField = new JPasswordField(20);
        hostJTextField = new JTextField(20);
        portJTextField = new JTextField(20);
        sidJTextField = new JTextField(20);
        
        hostJTextField.setText("tritium.ftdi.com");
        portJTextField.setText("1521");
        sidJTextField.setText("zeus4");

        acceptJButton = new JButton("Accept");
        acceptJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                acceptButtonPressed();
            }
        });
        getRootPane().setDefaultButton(acceptJButton);
        
        JPanel fieldJPanel = new JPanel();
        fieldJPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.WEST;
        
        fieldJPanel.add(usernameJLabel,gbc);
        gbc.gridx++;
        fieldJPanel.add(usernameJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        fieldJPanel.add(passwordJLabel,gbc);
        gbc.gridx++;
        fieldJPanel.add(passwordJPasswordField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        fieldJPanel.add(hostJLabel,gbc);
        gbc.gridx++;
        fieldJPanel.add(hostJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        fieldJPanel.add(portJLabel,gbc);
        gbc.gridx++;
        fieldJPanel.add(portJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        fieldJPanel.add(sidJLabel,gbc);
        gbc.gridx++;
        fieldJPanel.add(sidJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        fieldJPanel.add(acceptJButton,gbc);
        
        this.getContentPane().add(fieldJPanel);
    }
    
    public String getUsername()
    {
        return usernameJTextField.getText();
    }

    public String getPassword()
    {
        return passwordJPasswordField.getText();
    }
    public String getHost()
    {
        return hostJTextField.getText();
    }
    public String getPort()
    {
        return portJTextField.getText();
    }
    public String getSid()
    {
        return sidJTextField.getText();
    }
    
    private void acceptButtonPressed()
    {
        this.setVisible(false);
    }
}
