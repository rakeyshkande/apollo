package com.ftd.monitortool.util;

import java.sql.Connection;

import javax.swing.JDialog;

import oracle.jdbc.pool.OracleDataSource;

public class ConnectionManager
{
    private static ConnectionManager instance;
    private OracleDataSource ds;
    
    private String username;
    private String password;
    private String host;
    private String port;
    private String sid;
    
    private ConnectionManager()
    {
        getParameters();
        initDataSource();
    }
    
    public static ConnectionManager getInstance()
    {
        if (instance == null)
        {
            instance = new ConnectionManager();
        }
        return instance;
    }
    
    public Connection getConnection() throws Exception
    {
        Connection con = ds.getConnection(username,password);
        return con;
    }


    private void getParameters()
    {
       
        DatabaseParmsJDialog databaseParmsJDialog = new DatabaseParmsJDialog();
        databaseParmsJDialog.setModal(true);
        databaseParmsJDialog.setSize(280,200);
        databaseParmsJDialog.setLocation(400,400);
        databaseParmsJDialog.setVisible(true);
        
        
        username = databaseParmsJDialog.getUsername();
        password = databaseParmsJDialog.getPassword();
        host = databaseParmsJDialog.getHost();
        port = databaseParmsJDialog.getPort();
        sid = databaseParmsJDialog.getSid();
        
    }
    
    private void initDataSource()
    {
        try
        {
            ds = new oracle.jdbc.pool.OracleDataSource();
            String url = "jdbc:oracle:thin:@" + host + ":" + port + ":" + sid;
            ds.setURL(url);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
