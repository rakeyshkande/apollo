package com.ftd.monitortool.server.impl;

import com.ftd.monitortool.server.LoadAPI;
import com.ftd.monitortool.server.LoadListener;

import com.ftd.monitortool.server.LoadVO;

import com.ftd.monitortool.server.QueueListener;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LoadAPIImpl implements LoadAPI
{
    private Map<String, List> listenerMap;

    public LoadAPIImpl()
    {
        listenerMap = new HashMap();
        init();
    }

    public void subscribeLoad(String hostname, LoadListener listener)
    {
        List listenerList = listenerMap.get(hostname);
        if (listenerList == null)
        {
            listenerList = new ArrayList();
            listenerMap.put(hostname, listenerList);
        }
        listenerList.add(listener);
    }

    public void unsubscribeLoad(String hostname, LoadListener listener)
    {
        List listenerList = listenerMap.get(hostname);
        if (listenerList != null)
        {
            listenerList.remove(listener);
        }
    }

    public void fireLoadEvent(String serverName, String timestamp, LoadVO value)
    {
        List listenerList = listenerMap.get(serverName);
        for (int i=0; i < listenerList.size();i++)
        {
            LoadListener listener = (LoadListener) listenerList.get(i);
            listener.loadUpdate(serverName, timestamp, value);
        }
    }

    public String[] getServerNameListeners()
    {
        synchronized(listenerMap)
        {
            Set keySet = listenerMap.keySet();
            int size = keySet.size();
            String[] serverNames = new String[size];
            Iterator keyIterator = keySet.iterator();
            int i = 0;
            while (keyIterator.hasNext())
            {
                serverNames[i++] = (String) keyIterator.next();
            }
            return serverNames;
        }
    }

    private void init()
    {
        spawnLoadThread();
    }
    
    private void spawnLoadThread()
    {
        LoadLookupThread loadThread = new LoadLookupThread(this);
        loadThread.start();
    }
    


}
