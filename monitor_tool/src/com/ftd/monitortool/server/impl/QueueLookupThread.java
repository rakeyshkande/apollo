package com.ftd.monitortool.server.impl;

import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.ServerFactory;
import com.ftd.monitortool.util.ConnectionManager;

import java.sql.Connection;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.apache.log4j.Logger;

public class QueueLookupThread extends Thread
{
    private static Logger logger  = Logger.getLogger(QueueLookupThread.class);

    private QueueAPIImpl queueAPI;
    private int          refreshRate = 15000;
    private boolean      stop;
    private QueueDAO     dao;
    private SimpleDateFormat sdf;
    
    public QueueLookupThread(QueueAPIImpl queueAPI)
    {
        this.queueAPI = queueAPI;
    }
    
    public void run()
    {
        dao = new QueueDAO();
        Connection con = null;
        sdf = new SimpleDateFormat("mmss");
        AlertAPI alertAPI = ServerFactory.createAlertAPI();
        
        try
        {
            con = getConnection();

            while (!stop)
            {
                try
                {
                    String timestamp = sdf.format(new Date());
                    // Get a list of queueNames to process
                    String[] queues = queueAPI.getQueueNameListeners();
                    
                    // Process each queue
                    for (int i=0; i < queues.length;i++)
                    {
                        String queueName = queues[i];                
                        logger.debug("Looking up count for " + queueName);
                        int value = 0;
                        try
                        {
                            value = dao.getQueueData(con,queueName);
                            queueAPI.fireQueueEvent(queueName,timestamp, value);
                        }
                        catch (Exception e)
                        {
                            alertAPI.fireAlertEvent("Exception in getting queue Data:  " + e.getMessage(), AlertAPI.CRIT_LEVEL);
                            try
                            {
                                con.close();
                            }
                            catch (Exception e2)
                            {
                                e2.printStackTrace();
                            }
                            con = getConnection();
                        }
                    }
                    sleep(refreshRate);
                }
                catch (Exception e)
                {
                    alertAPI.fireAlertEvent("Exception in getting queue Data:  " + e.getMessage(), AlertAPI.CRIT_LEVEL);
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (con != null)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    public Connection getConnection() throws Exception
    {
        Connection con;
        con = ConnectionManager.getInstance().getConnection();
        return con;
        
    }
    
    public void stopThread()
    {
        stop = true;
    }
}
