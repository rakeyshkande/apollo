package com.ftd.monitortool.server.impl;

import com.ftd.monitortool.server.QueueAPI;
import com.ftd.monitortool.server.QueueListener;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QueueAPIImpl implements QueueAPI
{
    private Map<String,List> listenerMap;
    
    public QueueAPIImpl()
    {
        listenerMap = new HashMap();
        init();
    }

    public void subscribeQueueCount(String queueName, QueueListener listener)
    {
        synchronized(listenerMap)
        {
            List listenerList = listenerMap.get(queueName);
            if (listenerList == null)
            {
                listenerList = new ArrayList();
                listenerMap.put(queueName, listenerList);
            }
            listenerList.add(listener);
        }
    }

    public void unsubscribeQueueCount(String queueName, QueueListener listener)
    {
        synchronized(listenerMap)
        {
            List listenerList = listenerMap.get(queueName);
            if (listenerList != null)
            {
                listenerList.remove(listener);
            }
        }
    }

    public String[] getQueueNameListeners()
    {
        synchronized(listenerMap)
        {
            Set keySet = listenerMap.keySet();
            int size = keySet.size();
            String[] queueNames = new String[size];
            Iterator keyIterator = keySet.iterator();
            int i = 0;
            while (keyIterator.hasNext())
            {
                queueNames[i++] = (String) keyIterator.next();
            }
            return queueNames;
        }
    }
    
    private void init()
    {
        spawnQueueThread();
    }
    
    private void spawnQueueThread()
    {
        QueueLookupThread queueThread = new QueueLookupThread(this);
        queueThread.start();
    }
    
    public void fireQueueEvent(String queueName, String timestamp, int value)
    {
        List listenerList = listenerMap.get(queueName);
        for (int i=0; i < listenerList.size();i++)
        {
            QueueListener listener = (QueueListener) listenerList.get(i);
            listener.queueSizeUpdate(queueName, timestamp, value);
        }
    }
}
