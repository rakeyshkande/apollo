package com.ftd.monitortool.server.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QueueDAO
{
    public QueueDAO()
    {
    }
    
    
    public int getQueueData(Connection con, String queueName) throws SQLException
    {
        if (queueName.equals("SHIP_PROCESSING"))
        {
            String sqlQuery = "select count(*) from ojms." + queueName + " WHERE state = 0";
            return getQueueQuery(con,sqlQuery);
        }
        else
        {
            String sqlQuery = "select count(*) from ojms." + queueName;
            return getQueueQuery(con,sqlQuery);
        }
    }
    
    private int getQueueQuery(Connection con, String sqlQuery) throws SQLException
    { 
        int queueCount = -1;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = con.prepareStatement(sqlQuery);
            rs = stmt.executeQuery();
        
            if (rs.next())
            {
                queueCount = rs.getInt(1);        
            }
        }
        finally
        {
            try
            {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return queueCount;
    }

}
