package com.ftd.monitortool.server.impl;

import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.AlertListener;
import com.ftd.monitortool.server.QueueAPI;
import com.ftd.monitortool.server.QueueListener;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AlertAPIImpl implements AlertAPI
{
    private List             listenerList;

    public AlertAPIImpl()
    {
        listenerList = new ArrayList();
        init();
    }

    private void init()
    {
    }

    public void fireAlertEvent(String message, String alertLevel)
    {
        Date timestamp = new Date();
        for (int i = 0; i < listenerList.size(); i++)
        {
            AlertListener listener = (AlertListener)listenerList.get(i);
            listener.alert(message, timestamp, alertLevel);
        }
    }

    public void subscribeAlert(AlertListener listener)
    {
        synchronized (listenerList)
        {
            listenerList.add(listener);
        }
    }

    public void unsubscribeAlert(AlertListener listener)
    {
        synchronized (listenerList)
        {
            listenerList.remove(listener);
        }
    }
}
