package com.ftd.monitortool.server.impl;

import com.ftd.monitortool.server.LoadVO;
import com.ftd.monitortool.util.ConnectionManager;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.apache.log4j.Logger;

import org.snmp4j.PDU;
import org.snmp4j.smi.OID;
import org.snmp4j.tools.console.SnmpRequest;

public class LoadLookupThread extends Thread
{
    private static Logger logger  = Logger.getLogger(LoadLookupThread.class);

    private LoadAPIImpl      loadAPI;
    private int              refreshRate = 30000;
    private boolean          stop;
    private SimpleDateFormat sdf;

    public final static String OID_laLoadInt1 =  "1.3.6.1.4.1.2021.10.1.5.0";
    public final static String SNMP_PASSWORD = "ftdSNMPacct";
    public final static String SNMP_PASSWORD_TRITIUM = "public";
    public final static String TRITIUM= "tritium";
    
    public LoadLookupThread(LoadAPIImpl loadAPI)
    {
        this.loadAPI = loadAPI;
    }
    
    public void run()
    {
        sdf = new SimpleDateFormat("mmss");
        
        try
        {
            while (!stop)
            {
                try
                {
                    String timestamp = sdf.format(new Date());
                    // Get a list of queueNames to process
                    String[] servers = loadAPI.getServerNameListeners();
                    
                    // Process each server
                    for (int i=0; i < servers.length;i++)
                    {
                        String serverName = servers[i];                
                        int value = lookupLoad(serverName, OID_laLoadInt1);
                        logger.debug("Looking up load for " + serverName + " " + value);
                        LoadVO vo = new LoadVO();
                        // change the load to the right value
                        double doubleValue = ((double) value) / 100;
                        vo.setLoad(doubleValue);
                        loadAPI.fireLoadEvent(serverName,timestamp, vo);
                    }
                    sleep(refreshRate);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
        }
    }
    
    
    public void stopThread()
    {
        stop = true;
    }    
    
    public int lookupLoad(String system, String OID)
    {
        try
        {
            String password = SNMP_PASSWORD;
            if (system.equals(TRITIUM))
            {
                password = SNMP_PASSWORD_TRITIUM;
            }
            
            String[] snmpArgs = new String[] {"-c",password,"-v","1","-Ot",system,OID};
            
            SnmpRequest snmpRequest = new SnmpRequest(snmpArgs);
            PDU pdu = snmpRequest.send();
            if (pdu != null)
            {
                System.out.println(pdu.toString());
                return pdu.get(0).getVariable().toInt();           
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return 0;
        
    }
}
