package com.ftd.monitortool.server;

public class LoadVO
{
    private String hostname;
    private double load;
    private long freeMemory;
    
    public LoadVO()
    {
    }

    public void setLoad(double load)
    {
        this.load = load;
    }

    public double getLoad()
    {
        return load;
    }

    public void setFreeMemory(long freeMemory)
    {
        this.freeMemory = freeMemory;
    }

    public long getFreeMemory()
    {
        return freeMemory;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public String getHostname()
    {
        return hostname;
    }
}
