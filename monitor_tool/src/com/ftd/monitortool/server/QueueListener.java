package com.ftd.monitortool.server;

public interface QueueListener
{
    public void queueSizeUpdate(String queueName, String timestamp, int queueSize);
}
