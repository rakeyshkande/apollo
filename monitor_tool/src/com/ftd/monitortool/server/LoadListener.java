package com.ftd.monitortool.server;

public interface LoadListener
{
    public void loadUpdate(String hostname, String timestamp,  LoadVO load);
}
