package com.ftd.monitortool.server;

import com.ftd.monitortool.server.impl.AlertAPIImpl;
import com.ftd.monitortool.server.impl.LoadAPIImpl;
import com.ftd.monitortool.server.impl.QueueAPIImpl;

public class ServerFactory
{
    private static QueueAPI queueAPI;
    private static AlertAPI alertAPI;
    private static LoadAPI  loadAPI;
    
    public ServerFactory()
    {
    }
    
    public static QueueAPI createQueueAPI()
    {
        if (queueAPI == null)
        {
            queueAPI = new QueueAPIImpl();
        }
        return queueAPI;
    }

    public static AlertAPI createAlertAPI()
    {
        if (alertAPI == null)
        {
            alertAPI = new AlertAPIImpl();
        }
        return alertAPI;
    }

    public static LoadAPI createLoadAPI()
    {
        if (loadAPI == null)
        {
            loadAPI = new LoadAPIImpl();
        }
        return loadAPI;
    }
}
