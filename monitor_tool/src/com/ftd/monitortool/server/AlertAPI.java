package com.ftd.monitortool.server;

/**
 * API for getting alert information.
 */
public interface AlertAPI
{
    public static final String NORMAL_LEVEL = "NORM";
    public static final String WARN_LEVEL   = "WARN";
    public static final String CRIT_LEVEL   = "CRIT";
    
    public void subscribeAlert(AlertListener listener);
    public void unsubscribeAlert(AlertListener listener);
    public void fireAlertEvent(String message, String alertLevel);

}
