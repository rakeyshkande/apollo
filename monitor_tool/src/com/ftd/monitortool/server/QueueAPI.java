package com.ftd.monitortool.server;

/**
 * API for getting queue information.
 */
public interface QueueAPI
{
    public void subscribeQueueCount(String queueName, QueueListener listener);
    public void unsubscribeQueueCount(String queueName, QueueListener listener);
}
