package com.ftd.monitortool.server;

import java.util.Date;

public interface AlertListener
{
    public void alert(String message, Date timestamp, String alertLevel);
}
