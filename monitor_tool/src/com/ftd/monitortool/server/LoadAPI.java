package com.ftd.monitortool.server;

/**
 * API for getting queue information.
 */
public interface LoadAPI
{
    public void subscribeLoad(String hostname, LoadListener listener);

    public void unsubscribeLoad(String hostname, LoadListener listener);
}
