package com.ftd.monitortool.client.load;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class LoadConfigSetupJPanel extends JPanel
{
    private LoadModel model;

    private JList serverJList;
    private DefaultListModel serverListModel;
    private JButton addJButton;

    private JLabel categoryJLabel;
    private JLabel nameJLabel;
    private JLabel shortNameJLabel;
    private JLabel warnValueJLabel;
    private JLabel critValueJLabel;

    private JTextField categoryJTextField;
    private JTextField nameJTextField;
    private JTextField shortNameJTextField;
    private JTextField warnValueJTextField;
    private JTextField critValueJTextField;
    private JCheckBox monitorJCheckBox;

    private JButton saveJButton;
    private JButton closeJButton;


    public LoadConfigSetupJPanel(LoadModel model)
    {
        this.model = model;
        init();
    }

    private void init()
    {
        this.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.WEST;

        List serverNames = model.getSererNameList();
        serverListModel = new DefaultListModel();
        for (int i = 0; i < serverNames.size(); i++)
        {
            serverListModel.addElement(serverNames.get(i));
        }

        serverJList = new JList(serverListModel);

        // Add a listener for selection change
        serverJList.addListSelectionListener(new ListSelectionListener()
                {
                    public void valueChanged(ListSelectionEvent e)
                    {
                        listSelectionChanged();
                    }
                });
        JScrollPane listJScrollPane = new JScrollPane(serverJList);

        addJButton = new JButton("Add");
        addJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        addButtonPressed();
                    }
                });
        saveJButton = new JButton("Save");
        saveJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        saveButtonPressed();
                    }
                });
        closeJButton = new JButton("Close");
        closeJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        closeButtonPressed();
                    }
                });

        categoryJLabel = new JLabel("Category:");
        nameJLabel = new JLabel("Name:");
        shortNameJLabel = new JLabel("Short Name:");
        warnValueJLabel = new JLabel("Warning Value:");
        critValueJLabel = new JLabel("Critical Value:");

        categoryJTextField = new JTextField(20);
        nameJTextField = new JTextField(60);
        shortNameJTextField = new JTextField(10);
        warnValueJTextField = new JTextField(5);
        critValueJTextField = new JTextField(5);

        monitorJCheckBox = new JCheckBox("Monitor");


        gbc.gridheight = 4;
        this.add(listJScrollPane, gbc);
        gbc.gridx++;
        gbc.gridheight = 1;
        this.add(categoryJLabel, gbc);
        gbc.gridx++;
        this.add(categoryJTextField, gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(nameJLabel, gbc);
        gbc.gridx++;
        this.add(nameJTextField, gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(shortNameJLabel, gbc);
        gbc.gridx++;
        this.add(shortNameJTextField, gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(warnValueJLabel, gbc);
        gbc.gridx++;
        this.add(warnValueJTextField, gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(critValueJLabel, gbc);
        gbc.gridx++;
        this.add(critValueJTextField, gbc);
        gbc.gridx--;
        gbc.gridx--;
        gbc.gridy++;
        this.add(addJButton, gbc);
        gbc.gridx++;
        this.add(monitorJCheckBox, gbc);
        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        this.add(saveJButton, gbc);

    }

    private void listSelectionChanged()
    {
        String serverSelected = (String)serverJList.getSelectedValue();
        LoadConfigVO vo = model.getLoadDetails(serverSelected);
        if (vo == null)
        {
            categoryJTextField.setText("");
            nameJTextField.setText(serverSelected);
            shortNameJTextField.setText("");
            warnValueJTextField.setText("");
            critValueJTextField.setText("");
            monitorJCheckBox.setSelected(false);
        } else
        {
            categoryJTextField.setText(vo.getCategory());
            nameJTextField.setText(vo.getName());
            shortNameJTextField.setText(vo.getShortName());
            warnValueJTextField.setText(Integer.toString(vo.getWarnValue()));
            critValueJTextField.setText(Integer.toString(vo.getCritValue()));
            monitorJCheckBox.setSelected(vo.isMonitor());
        }
    }

    private void addButtonPressed()
    {
        serverListModel.addElement("New Server");
    }

    private void closeButtonPressed()
    {

    }

    private void saveButtonPressed()
    {
        LoadConfigVO vo = new LoadConfigVO();

        vo.setCategory(categoryJTextField.getText());
        vo.setName(nameJTextField.getText());
        vo.setShortName(shortNameJTextField.getText());
        int warnValueInt = 50;
        try
        {
            String warnValue = warnValueJTextField.getText();
            warnValueInt = Integer.parseInt(warnValue);
        } catch (NumberFormatException e)
        {
            // Just swallow
        }
        vo.setWarnValue(warnValueInt);

        int critValueInt = 50;
        try
        {
            String critValue = critValueJTextField.getText();
            critValueInt = Integer.parseInt(critValue);
        } catch (NumberFormatException e)
        {
            // Just swallow
        }
        vo.setCritValue(critValueInt);

        vo.setMonitor(monitorJCheckBox.isSelected());

        model.updateLoad(vo);
    }
}
