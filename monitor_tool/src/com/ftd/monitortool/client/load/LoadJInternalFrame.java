package com.ftd.monitortool.client.load;

import com.ftd.monitortool.DatabaseProperties;
import com.ftd.monitortool.client.queue.QueueConfigSetupJPanel;
import com.ftd.monitortool.client.queue.QueueModel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ThermometerPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultValueDataset;

public class LoadJInternalFrame extends JInternalFrame
{
    // Charting Stuff
    protected ChartPanel chartPanel;
    protected JPanel buttonJPanel;
    protected JPanel textJPanel;
    protected JButton configureJButton;

    protected JFreeChart chart;

    protected LoadModel model;
    
    protected String category;

    public LoadJInternalFrame(String category)
    {
        super("Load Monitor", true, true, true, true);
        this.category = category;
        init();
    }

    private void init()
    {
        this.setSize(300, 300);
        this.setLocation(300,350);
        JPanel contentJPanel = new JPanel();
        this.getContentPane().add(contentJPanel);
        contentJPanel.setLayout(new BorderLayout());
        
        model = new LoadModel(category);

        CategoryDataset dataset = model.getDataset();
        chart = createLineChart("Servers", "category", "axis", dataset, PlotOrientation.VERTICAL, true);
        chartPanel = new ChartPanel(chart);
        
        //XYPlot xyPlot = chart.getXYPlot();

        buttonJPanel = new JPanel();
        configureJButton = new JButton("Configure");
        buttonJPanel.add(configureJButton);
        configureJButton.addActionListener(new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        configureButtonPressed();
                    }
                });

        textJPanel = createThermometerJPanel();
        
        contentJPanel.add(buttonJPanel, BorderLayout.NORTH);
        contentJPanel.add(chartPanel, BorderLayout.CENTER);
        //contentJPanel.add(textJPanel, BorderLayout.SOUTH);
    }

    public void removeNotify()
    {
        model.unsubscribeUpdates();
        super.removeNotify();
    }

    protected void configureButtonPressed()
    {
        JDialog loadConfigDialog = new JDialog();
        LoadConfigSetupJPanel loadJPanel = new LoadConfigSetupJPanel(model);
        loadConfigDialog.getContentPane().add(loadJPanel);
        loadConfigDialog.setModal(true);
        loadConfigDialog.pack();
        loadConfigDialog.setVisible(true);
    }


    protected JFreeChart createLineChart(String title, String categoryAxisLabel, String valueAxisLabel, 
                                         CategoryDataset dataset, PlotOrientation orientation, boolean legend)
    {
        CategoryAxis categoryAxis = new CategoryAxis(categoryAxisLabel);
        ValueAxis valueAxis = new NumberAxis(valueAxisLabel);

        LineAndShapeRenderer renderer = new LineAndShapeRenderer(true, false);
        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
        plot.setOrientation(orientation);
        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);

        return chart;

    }
    
    protected JPanel createThermometerJPanel()
    {
        JPanel thermoJPanel = new JPanel();
        thermoJPanel.setLayout(new FlowLayout());
     
        addServerThermometer(thermoJPanel, "gold");
        addServerThermometer(thermoJPanel, "silver");

        return thermoJPanel;
        
    }
    
    protected void addServerThermometer(JPanel thermoJPanel, String server)
    {
        DefaultValueDataset dataset = new DefaultValueDataset(new Double(10.40));
        ThermometerPlot plot = new ThermometerPlot(dataset);
        
        plot.setSubrange(ThermometerPlot.NORMAL, 0.0, 5.0);
        plot.setSubrange(ThermometerPlot.WARNING, 5.0, 12.0);
        plot.setSubrange(ThermometerPlot.CRITICAL, 12.0, 40.0);
        
        JFreeChart chart = new JFreeChart("Load", JFreeChart.DEFAULT_TITLE_FONT,plot,false);
        
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setSize(100,100);

        thermoJPanel.add(chartPanel);
    }

}
