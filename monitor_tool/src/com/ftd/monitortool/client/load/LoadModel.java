package com.ftd.monitortool.client.load;

import com.ftd.monitortool.client.ClientProperties;
import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.LoadListener;
import com.ftd.monitortool.server.LoadVO;
import com.ftd.monitortool.server.ServerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class LoadModel implements LoadListener
{
    private static Logger logger = Logger.getLogger(LoadModel.class);

    private static final int MAX_COLUMN_COUNT = 20;

    protected AlertAPI alertAPI;

    protected DefaultCategoryDataset dataset;
    protected Map<String, LoadConfigVO> loadHash;
    protected String loadCategory;

    public LoadModel(String category)
    {
        this.loadCategory = category;
        dataset = new DefaultCategoryDataset();
        loadHash = new HashMap();
        alertAPI = ServerFactory.createAlertAPI();

        initLoadHash();

        subscribeUpdates();
    }

    public CategoryDataset getDataset()
    {
        return dataset;
    }

    public void unsubscribeUpdates()
    {
        Iterator keyIterator = loadHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String loadName = (String)keyIterator.next();
            LoadConfigVO vo = loadHash.get(loadName);
            if (vo.isMonitor())
            {
                ServerFactory.createLoadAPI().unsubscribeLoad(loadName, this);
            }
        }
    }

    public void subscribeUpdates()
    {
        Iterator keyIterator = loadHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String loadName = (String)keyIterator.next();
            LoadConfigVO vo = loadHash.get(loadName);
            if (vo.isMonitor())
            {
                ServerFactory.createLoadAPI().subscribeLoad(loadName, this);
            }
        }

    }

    public void loadUpdate(String serverName, String timestamp, LoadVO load)
    {
        LoadConfigVO vo = loadHash.get(serverName);
        String shortName = serverName;
        if (vo != null)
        {
            shortName = vo.getShortName();
        }
        dataset.addValue(load.getLoad(), shortName, timestamp);

        if (dataset.getColumnCount() > MAX_COLUMN_COUNT)
        {
            dataset.removeColumn(0);
        }

        if (load.getLoad() > 0)
        {
            if (load.getLoad()  > vo.getCritValue())
            {
                String alertLevel = AlertAPI.CRIT_LEVEL;
                String message = serverName + " is at " + load.getLoad() ;
                alertAPI.fireAlertEvent(message, alertLevel);

            } else if (load.getLoad()  > vo.getWarnValue())
            {
                String alertLevel = AlertAPI.WARN_LEVEL;
                String message = serverName + " is at " + load.getLoad() ;
                alertAPI.fireAlertEvent(message, alertLevel);
            }
        }
    }

    public List getSererNameList()
    {
        List serverNameList = new ArrayList();
        Iterator keyIterator = loadHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String serverName = (String)keyIterator.next();
            serverNameList.add(serverName);
        }
        return serverNameList;
    }


    public LoadConfigVO getLoadDetails(String serverName)
    {
        LoadConfigVO vo = loadHash.get(serverName);
        return vo;
    }

    public void updateLoad(LoadConfigVO vo)
    {
        boolean addSub = false;
        boolean removeSub = false;
        if (loadHash.containsKey(vo.getName()))
        {
            LoadConfigVO oldvo = loadHash.get(vo.getName());
            if (oldvo.isMonitor() && !vo.isMonitor())
            {
                removeSub = true;
            }
            if (!oldvo.isMonitor() && vo.isMonitor())
            {
                addSub = true;
            }
        } else
        {
            if (vo.isMonitor())
            {
                addSub = true;
            }
        }
        loadHash.put(vo.getName(), vo);

        if (addSub)
        {
            ServerFactory.createLoadAPI().subscribeLoad(vo.getName(), this);
        }
        if (removeSub)
        {
            ServerFactory.createLoadAPI().unsubscribeLoad(vo.getName(), this);
        }
        //Update the properties so it saves
        ClientProperties properties = ClientProperties.getInstance();
        properties.setLoadProperty(vo.getName(), vo.getEncodedValue());
    }

    private void initLoadHash()
    {
        logger.debug("loading");
        ClientProperties properties = ClientProperties.getInstance();
        String[] loadProperties = properties.getLoadProperties();
        logger.debug("load array is size " + loadProperties.length);
        for (int i = 0; i < loadProperties.length; i++)
        {
            logger.debug("property = " + loadProperties[i]);
            LoadConfigVO vo = new LoadConfigVO(loadProperties[i]);
            logger.debug(vo.getName() + " " + vo.isMonitor());
            if (vo.getCategory().equals(loadCategory))
            {
                loadHash.put(vo.getName(), vo);
            }

        }
    }

}
