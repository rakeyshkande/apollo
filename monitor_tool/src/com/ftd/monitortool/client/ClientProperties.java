package com.ftd.monitortool.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;


public class ClientProperties
{
    private static Logger logger  = Logger.getLogger(ClientProperties.class);

    private static ClientProperties instance;
    private Properties properties;
    private String homeLocation;
    private File   propertiesFile;
    
    private final static String QUEUE_PROPERTY          = "queue.monitor";
    private final static String LOAD_PROPERTY           = "load.monitor";
    
    private final static String FILENAME = "monitortool.properties";

    private ClientProperties()
    {
        init();
    }
    
    public synchronized static ClientProperties getInstance()
    {
        if (instance == null)
        {
            instance = new ClientProperties();
        }
        return instance;
    }
    
    public String[] getQueueProperties()
    {
        List queueList = new ArrayList();
        
        String key = QUEUE_PROPERTY;
        Enumeration names = properties.propertyNames();
        while (names.hasMoreElements())
        {
            String name = (String) names.nextElement();
            if (name.startsWith(key))
            {
                queueList.add(properties.getProperty(name));
            }
        }
        String[] nameArray = new String[queueList.size()];
        queueList.toArray(nameArray);
        
        return nameArray;
    }

    public void setQueueProperty(String queueCategory, String queueName, String queueProperty)
    {
        String key = QUEUE_PROPERTY + "." + queueCategory + "." + queueName;
        properties.setProperty(key,queueProperty);
        saveProperties();
    }

    public String[] getLoadProperties()
    {
        List loadList = new ArrayList();
        
        String key = LOAD_PROPERTY;
        Enumeration names = properties.propertyNames();
        while (names.hasMoreElements())
        {
            String name = (String) names.nextElement();
            if (name.startsWith(key))
            {
                loadList.add(properties.getProperty(name));
            }
        }
        String[] nameArray = new String[loadList.size()];
        loadList.toArray(nameArray);
        
        return nameArray;
    }

    public void setLoadProperty(String serverName, String loadProperty)
    {
        String key = LOAD_PROPERTY + "." + serverName;
        properties.setProperty(key,loadProperty);
        saveProperties();
    }
    
    /**
     * Read the properties from the default location.  If none exists, create one.
     */
    private void init()
    {
        String homeLocation = getHomeLocation();
        propertiesFile = new File(homeLocation);
        properties = new Properties();
        readProperties();
    }

    private void saveProperties()
    {   
        try
        {
            FileOutputStream fos = new FileOutputStream(propertiesFile);
            properties.store(fos, "Default Properties");    
        }
        catch (IOException e)
        {
            logger.error("Error saving properties file",e);
        }
    }

    private void readProperties()
    {   
        try
        {
            if (propertiesFile.exists())
            {
                FileInputStream fis = new FileInputStream(propertiesFile);
                properties.load(fis);    
            }
        }
        catch (IOException e)
        {
            logger.error("Error reading properties file",e);
        }
    }
    
    /**
     * Get the home location where we are going to store the properties.
     * This is retrieved from the environment variables.
     * @return
     */
    private String getHomeLocation()
    {
        if (homeLocation == null)
        {
                
            homeLocation = "./";
            String userProfile = System.getenv("USERPROFILE");
            if (userProfile != null && !userProfile.equals(""))
            {
                homeLocation = userProfile;
            }
            else
            {  
                String userHome = System.getenv("HOME");
                if (userHome != null && !userHome.equals(""))
                {
                    homeLocation = userHome;
                }
            }
            homeLocation += "/" + FILENAME;
        }        
        logger.debug("Home location is [" + homeLocation + "]");
        return homeLocation;
    }

    
}
