package com.ftd.monitortool.client.queue;

import com.ftd.monitortool.DatabaseProperties;

import java.awt.BorderLayout;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class QueueJInternalFrame extends JInternalFrame
{
    // Charting Stuff
    ChartPanel chartPanel;
    JPanel     buttonJPanel;
    JButton    configureJButton;
    
    JFreeChart chart;
    
    QueueModel model;

    protected DatabaseProperties databaseProperties;
    
    protected String category;
    
    public QueueJInternalFrame(DatabaseProperties databaseProperties, String category)
    {
        super("Queue Monitor",true,true,true,true);
        this.category = category;
        this.databaseProperties = databaseProperties;
        init();
    }
    
    private void init()
    {
        this.setSize(900,300);
        JPanel contentJPanel = new JPanel();
        this.getContentPane().add(contentJPanel);
        contentJPanel.setLayout(new BorderLayout());
     
        model = new QueueModel(this.category);
        
        CategoryDataset dataset = model.getDataset();
        chart = createLineChart("Queues " + category, "category", "axis", dataset, PlotOrientation.VERTICAL,true);
        chartPanel = new ChartPanel(chart);
        
        buttonJPanel = new JPanel();
        configureJButton = new JButton("Configure");
        buttonJPanel.add(configureJButton);
        configureJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                configureButtonPressed();
            }
        });
        
        contentJPanel.add(buttonJPanel, BorderLayout.SOUTH);
        contentJPanel.add(chartPanel, BorderLayout.CENTER);
        

    }
    
    public void removeNotify()
    {
        model.unsubscribeUpdates();
        super.removeNotify();
    }
    
    protected void configureButtonPressed()
    {
        JDialog queueConfigDialog = new JDialog();
        QueueConfigSetupJPanel queueJPanel = new QueueConfigSetupJPanel(model);
        queueConfigDialog.getContentPane().add(queueJPanel);
        queueConfigDialog.setModal(true);
        queueConfigDialog.pack();
        queueConfigDialog.setVisible(true);
    }
    
    
    protected JFreeChart createLineChart(String title,
                                             String categoryAxisLabel,
                                             String valueAxisLabel,
                                             CategoryDataset dataset,
                                             PlotOrientation orientation,
                                             boolean legend) 
    {
        CategoryAxis categoryAxis = new CategoryAxis(categoryAxisLabel);
        ValueAxis valueAxis = new NumberAxis(valueAxisLabel);

        LineAndShapeRenderer renderer = new LineAndShapeRenderer(true, true);
        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
        plot.setOrientation(orientation);
        JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);

        return chart;

    }
}

