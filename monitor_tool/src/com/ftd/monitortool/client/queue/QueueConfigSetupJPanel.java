package com.ftd.monitortool.client.queue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.List;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class QueueConfigSetupJPanel extends JPanel
{
    private QueueModel model;
    
    private JList      queueJList;
    private DefaultListModel queueListModel;
    private JButton    addJButton;
    
    private JLabel     categoryJLabel;
    private JLabel     nameJLabel;
    private JLabel     shortNameJLabel;
    private JLabel     warnValueJLabel;
    private JLabel     critValueJLabel;
    
    private JTextField categoryJTextField;
    private JTextField nameJTextField;
    private JTextField shortNameJTextField;
    private JTextField warnValueJTextField;
    private JTextField critValueJTextField;
    private JCheckBox  monitorJCheckBox;

    private JButton    saveJButton;
    private JButton    closeJButton;
    
    
    public QueueConfigSetupJPanel(QueueModel model)
    {
        this.model = model;
        init();
    }
    
    private void init()
    {
        this.setLayout(new GridBagLayout());
    
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.WEST;
        
        List queueNames = model.getQueueNameList();
        queueListModel = new DefaultListModel();
        for (int i=0; i < queueNames.size(); i++)
        {
            queueListModel.addElement(queueNames.get(i));
        }
        
        queueJList = new JList(queueListModel);
        
        // Add a listener for selection change
        queueJList.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                listSelectionChanged();
            }
        });
        JScrollPane listJScrollPane = new JScrollPane(queueJList);

        addJButton = new JButton("Add");
        addJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                addButtonPressed();
            }
        });
        saveJButton = new JButton("Save");
        saveJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                saveButtonPressed();
            }
        });
        closeJButton = new JButton("Close");
        closeJButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                closeButtonPressed();
            }
        });
        
        categoryJLabel = new JLabel("Category:");
        nameJLabel = new JLabel("Name:");
        shortNameJLabel = new JLabel("Short Name:");
        warnValueJLabel = new JLabel("Warning Value:");
        critValueJLabel = new JLabel("Critical Value:");
        
        categoryJTextField = new JTextField(60);
        nameJTextField = new JTextField(60);
        shortNameJTextField = new JTextField(10);
        warnValueJTextField = new JTextField(5);
        critValueJTextField = new JTextField(5);
        
        monitorJCheckBox = new JCheckBox("Monitor");

        
        gbc.gridheight = 4;
        this.add(listJScrollPane,gbc);
        gbc.gridx++;
        gbc.gridheight = 1;
        this.add(categoryJLabel, gbc);
        gbc.gridx++;
        this.add(categoryJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(nameJLabel, gbc);
        gbc.gridx++;
        this.add(nameJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(shortNameJLabel, gbc);
        gbc.gridx++;
        this.add(shortNameJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(warnValueJLabel, gbc);
        gbc.gridx++;
        this.add(warnValueJTextField,gbc);
        gbc.gridx--;
        gbc.gridy++;
        this.add(critValueJLabel, gbc);
        gbc.gridx++;
        this.add(critValueJTextField,gbc);
        gbc.gridx--;
        gbc.gridx--;
        gbc.gridy++;
        this.add(addJButton, gbc);
        gbc.gridx++;
        this.add(monitorJCheckBox, gbc);
        gbc.gridx=0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        this.add(saveJButton,gbc);
        
    }
    
    private void listSelectionChanged()
    {
        String queueSelected = (String) queueJList.getSelectedValue();
        QueueConfigVO vo = model.getQueueDetails(queueSelected);  
        if (vo == null)
        {
            categoryJTextField.setText("");
            nameJTextField.setText(queueSelected);
            shortNameJTextField.setText("");
            warnValueJTextField.setText("");
            critValueJTextField.setText("");
            monitorJCheckBox.setSelected(false);
        }
        else
        {
            categoryJTextField.setText(vo.getCategory());
            nameJTextField.setText(vo.getName());
            shortNameJTextField.setText(vo.getShortName());
            warnValueJTextField.setText(Integer.toString(vo.getWarnValue()));
            critValueJTextField.setText(Integer.toString(vo.getCritValue()));
            monitorJCheckBox.setSelected(vo.isMonitor());
        }
    }
    
    private void addButtonPressed()
    {
        queueListModel.addElement("New Queue");
    }
    
    private void closeButtonPressed()
    {
        
    }
    
    private void saveButtonPressed()
    {
        QueueConfigVO vo = new QueueConfigVO();  

        vo.setCategory(categoryJTextField.getText());
        vo.setName(nameJTextField.getText());
        vo.setShortName(shortNameJTextField.getText());
        int warnValueInt = 50;
        try
        {
            String warnValue = warnValueJTextField.getText();
            warnValueInt = Integer.parseInt(warnValue);
        }
        catch (NumberFormatException e)
        {
            // Just swallow
        }
        vo.setWarnValue(warnValueInt);
        
        int critValueInt = 50;
        try
        {
            String critValue = critValueJTextField.getText();
            critValueInt = Integer.parseInt(critValue);
        }
        catch (NumberFormatException e)
        {
            // Just swallow
        }
         vo.setCritValue(critValueInt);

        vo.setMonitor(monitorJCheckBox.isSelected());
        
        model.updateQueue(vo);
    }
}
