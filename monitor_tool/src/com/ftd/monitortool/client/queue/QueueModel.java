package com.ftd.monitortool.client.queue;

import com.ftd.monitortool.client.ClientProperties;
import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.QueueListener;
import com.ftd.monitortool.server.ServerFactory;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class QueueModel implements QueueListener
{
    private static Logger logger  = Logger.getLogger(QueueModel.class);
    
    private static final int MAX_COLUMN_COUNT = 20;

    protected AlertAPI alertAPI;
    
    protected DefaultCategoryDataset dataset;
    protected Map<String,QueueConfigVO> queueHash;
    
    protected Map<String,Integer> lastQueueValue;
    
    protected String queueCategory;
    
    public QueueModel(String queueCategory)
    {
        this.queueCategory = queueCategory;
        dataset = new DefaultCategoryDataset();
        queueHash = new HashMap<String,QueueConfigVO>();
        lastQueueValue = new HashMap<String,Integer>();
        alertAPI = ServerFactory.createAlertAPI();
        
        initQueueHash();
        
        subscribeUpdates();
    }
    
    public CategoryDataset getDataset()
    {
        return dataset;
    }
    
    public void unsubscribeUpdates()
    {
        Iterator keyIterator = queueHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String queueName = (String) keyIterator.next();
            QueueConfigVO vo = queueHash.get(queueName);
            if (vo.isMonitor())
            {
                ServerFactory.createQueueAPI().unsubscribeQueueCount(queueName,this);
            }
        }
    }
    
    public void subscribeUpdates()
    {
        Iterator keyIterator = queueHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String queueName = (String) keyIterator.next();
            QueueConfigVO vo = queueHash.get(queueName);
            if (vo.isMonitor())
            {
                ServerFactory.createQueueAPI().subscribeQueueCount(queueName,this);
            }
        }
        
    }

    public void queueSizeUpdate(String queueName, String timestamp, int queueSize)
    {
        QueueConfigVO vo = queueHash.get(queueName);
        String shortName= queueName;
        if (vo != null)
        {
            shortName = vo.getShortName();
        }
        
        Integer oldQueueSize = 0;
        if (lastQueueValue.containsKey(shortName))
        {
            oldQueueSize = (Integer) lastQueueValue.get(shortName);
        }
        lastQueueValue.put(shortName,queueSize);
        
        dataset.addValue(queueSize, shortName, timestamp);

        if (dataset.getColumnCount() > MAX_COLUMN_COUNT)
        {
            dataset.removeColumn(0);            
        }
        
        if (queueSize > 0)
        {
            if (queueSize > vo.getCritValue())
            {
                String alertLevel = AlertAPI.CRIT_LEVEL;
                String message = queueName + " is at " + queueSize + "  (" + (queueSize - oldQueueSize) + ")";
                alertAPI.fireAlertEvent(message,alertLevel);
                
            }
            else if (queueSize > vo.getWarnValue())
            {
                String alertLevel = AlertAPI.WARN_LEVEL;
                String message = queueName + " is at " + queueSize + "  (" + (queueSize - oldQueueSize) + ")";
                alertAPI.fireAlertEvent(message,alertLevel);
            }
        }
    }
    
    public List getQueueNameList()
    {
        List queueNameList = new ArrayList();
        Iterator keyIterator = queueHash.keySet().iterator();
        while (keyIterator.hasNext())
        {
            String queueName = (String) keyIterator.next();
            queueNameList.add(queueName);
        }
        return queueNameList;
    }
    

    public QueueConfigVO getQueueDetails(String queueName)
    {
        QueueConfigVO vo = queueHash.get(queueName);
        return vo;
    }
    
    public void updateQueue(QueueConfigVO vo)
    {
        boolean addSub = false;
        boolean removeSub = false;
        if (queueHash.containsKey(vo.getName()))
        {
            QueueConfigVO oldvo = queueHash.get(vo.getName());
            if (oldvo.isMonitor() && !vo.isMonitor())
            {
                removeSub = true;
            }
            if (!oldvo.isMonitor() && vo.isMonitor())
            {
                addSub = true;
            }
        }
        else
        {
            if (vo.isMonitor())
            {
                addSub = true;
            }
        }
        queueHash.put(vo.getName(),vo);
        
        if (addSub)
        {
            ServerFactory.createQueueAPI().subscribeQueueCount(vo.getName(),this);
        }
        if (removeSub)
        {
            ServerFactory.createQueueAPI().unsubscribeQueueCount(vo.getName(),this);
        }
        //Update the properties so it saves
        ClientProperties properties = ClientProperties.getInstance();
        properties.setQueueProperty(queueCategory,vo.getName(),vo.getEncodedValue());
    }

    private void initQueueHash()
    {
        logger.debug("loading");
        ClientProperties properties = ClientProperties.getInstance();
        String[] queueProperties = properties.getQueueProperties();
        logger.debug("queue array is size " + queueProperties.length);
        for (int i=0; i < queueProperties.length; i++)
        {
            logger.debug("property = " + queueProperties[i]);
            QueueConfigVO vo = new QueueConfigVO(queueProperties[i]);
            logger.debug(vo.getName() + " " + vo.isMonitor());
            if (vo.getCategory().equals(queueCategory))
            {
                queueHash.put(vo.getName(),vo);
            }
        }
    }

}
