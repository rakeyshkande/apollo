package com.ftd.monitortool.client.queue;

import java.util.StringTokenizer;

public class QueueConfigVO
{
    private String  category;
    private String  name;
    private String  shortName;
    private int     warnValue;
    private int     critValue;
    private boolean monitor;
    
    public QueueConfigVO()
    {
    }
    
    public QueueConfigVO(String encodedValue)
    {
        StringTokenizer tokenizer = new StringTokenizer(encodedValue,";");
        if (tokenizer.countTokens() != 6)
        {
            throw new IllegalArgumentException("Invalid number of tokens " + tokenizer.countTokens());
        }
        
        category = tokenizer.nextToken();
        name = tokenizer.nextToken();
        shortName = tokenizer.nextToken();
        warnValue = Integer.parseInt(tokenizer.nextToken());
        critValue = Integer.parseInt(tokenizer.nextToken());
        monitor = Boolean.valueOf(tokenizer.nextToken());
        
    }

    public String getEncodedValue()
    {
        return category + ";" + name + ";" + shortName + ";" + warnValue + ";" + critValue + ";" + monitor;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getShortName()
    {
        return shortName;
    }

    public void setWarnValue(int warnValue)
    {
        this.warnValue = warnValue;
    }

    public int getWarnValue()
    {
        return warnValue;
    }

    public void setCritValue(int critValue)
    {
        this.critValue = critValue;
    }

    public int getCritValue()
    {
        return critValue;
    }

    public void setMonitor(boolean monitor)
    {
        this.monitor = monitor;
    }

    public boolean isMonitor()
    {
        return monitor;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getCategory()
    {
        return category;
    }
}
