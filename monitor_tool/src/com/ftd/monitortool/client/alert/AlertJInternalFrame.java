package com.ftd.monitortool.client.alert;

import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.AlertListener;
import com.ftd.monitortool.server.ServerFactory;

import java.awt.BorderLayout;

import java.awt.FlowLayout;
import java.awt.Image;

import java.awt.Toolkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.SimpleDateFormat;

import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class AlertJInternalFrame extends JInternalFrame implements AlertListener
{
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    
    JTextArea logJTextArea;
    JFrame    parentFrame;
    JButton   clearJButton;

    Image redImage;
    Image yellowImage;
    Image greenImage;

    String currentAlertLevel;
    Date   critAlertTime;  
    Date   warnAlertTime;  
    
    public AlertJInternalFrame(JFrame parentFrame)
    {
        super("Alerts", true, true, true, true);
        this.parentFrame = parentFrame;
        init();
    }

    private void init()
    {
        currentAlertLevel = AlertAPI.NORMAL_LEVEL;
        
        this.setSize(600, 200);
        this.setLocation(0,350);
        JPanel contentJPanel = new JPanel();
        this.getContentPane().add(contentJPanel);
        contentJPanel.setLayout(new BorderLayout());

        logJTextArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(logJTextArea);
        contentJPanel.add(scrollPane, BorderLayout.CENTER);
        
        clearJButton = new JButton("Clear");
        clearJButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    clearButtonPressed();
                }

                });
        JPanel buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new FlowLayout());
        buttonJPanel.add(clearJButton);
        contentJPanel.add(buttonJPanel, BorderLayout.SOUTH);
        
        ServerFactory.createAlertAPI().subscribeAlert(this);
        
        redImage = Toolkit.getDefaultToolkit().getImage(java.net.URLClassLoader.getSystemResource("mushroom.jpg"));
        yellowImage = Toolkit.getDefaultToolkit().getImage(java.net.URLClassLoader.getSystemResource("yellow.JPG"));
        greenImage = Toolkit.getDefaultToolkit().getImage(java.net.URLClassLoader.getSystemResource("green.JPG"));

        parentFrame.setIconImage(greenImage);
        
        AlertStatusThread alertStatusThread = new AlertStatusThread(this);
        alertStatusThread.start();
        
        
    }

    public void alert(String message, Date timestamp, String alertLevel)
    {
        StringBuilder sb = new StringBuilder();
        String currentTimeGroup = sdf.format(timestamp);
        sb.append(currentTimeGroup);
        sb.append(" ");
        sb.append(alertLevel);
        sb.append(" " );
        sb.append(message);
        sb.append("\n");
        logJTextArea.insert(sb.toString(),0);

        setAlertLevel(timestamp, alertLevel);
    }
    
    private void setAlertLevel(Date currentAlertTime, String alertLevel)
    {
        if (alertLevel.equals(AlertAPI.CRIT_LEVEL))
        {
            critAlertTime = currentAlertTime;
        }
        else if (alertLevel.equals(AlertAPI.WARN_LEVEL) && currentAlertLevel.equals(AlertAPI.NORMAL_LEVEL))
        {
            warnAlertTime = currentAlertTime;
        }
    }
    
    protected Date getLastCritTime()
    {
        return  critAlertTime;
    }
    protected Date getLastWarnTime()
    {
        return  warnAlertTime;
    }
   
    protected void displayAlertLevel(String alertLevel)
    {
        if (!currentAlertLevel.equals(alertLevel))
        {
            if (alertLevel.equals(AlertAPI.CRIT_LEVEL))
            {
                SwingUtilities.invokeLater(new Runnable()
                {
                    public void run()
                    {
                        displayCritAlert();
                    }
                });
            }
            else if (alertLevel.equals(AlertAPI.WARN_LEVEL))
            {
                SwingUtilities.invokeLater(new Runnable()
                {
                    public void run()
                    {
                        displayWarnAlert();
                    }
                });
            }
            else
            {
                SwingUtilities.invokeLater(new Runnable()
                {
                    public void run()
                    {
                        displayNormalAlert();
                    }
                });
            }
        }
    }
    
    private void displayCritAlert()
    {
        currentAlertLevel = AlertAPI.CRIT_LEVEL;
        parentFrame.setIconImage(redImage);
    }
    
    private void displayWarnAlert()
    {
        currentAlertLevel = AlertAPI.WARN_LEVEL;
        parentFrame.setIconImage(yellowImage);
    }
    
    private void displayNormalAlert()
    {
        currentAlertLevel = AlertAPI.NORMAL_LEVEL;
        parentFrame.setIconImage(greenImage);
    }

    private void clearButtonPressed()
    {
        // Clear out the text area
        logJTextArea.setText("");
    }

}
