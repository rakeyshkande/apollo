package com.ftd.monitortool.client.alert;

import com.ftd.monitortool.server.AlertAPI;
import com.ftd.monitortool.server.ServerFactory;
import com.ftd.monitortool.server.impl.QueueDAO;
import com.ftd.monitortool.util.ConnectionManager;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

import javax.swing.JFrame;

import org.apache.log4j.Logger;


public class AlertStatusThread extends Thread
{
    private static Logger logger = Logger.getLogger(AlertStatusThread.class);

    private int refreshRate = 1000;
    private boolean stop;
    private AlertJInternalFrame alertFrame;    

    public AlertStatusThread(AlertJInternalFrame alertFrame)
    {
        this.alertFrame = alertFrame;
    }

    public void run()
    {

        try
        {
            while (!stop)
            {
                try
                {
                    // Get the current status and timestamp
                    Date lastCritTime = alertFrame.getLastCritTime();
                    Date lastWarnTime = alertFrame.getLastWarnTime();
                    // If crit happened within last 29 seconds...
                    if (within29Seconds(lastCritTime))
                    {
                        alertFrame.displayAlertLevel(AlertAPI.CRIT_LEVEL);                        
                    }
                    else if (within29Seconds(lastWarnTime))
                    {
                        alertFrame.displayAlertLevel(AlertAPI.WARN_LEVEL);                        
                    }
                    else
                    {
                        alertFrame.displayAlertLevel(AlertAPI.NORMAL_LEVEL);
                    }
                } catch (Exception e)
                {
                    logger.error("Exception" ,e);
                }
                sleep(refreshRate);
            }
        } catch (Exception e)
        {
            logger.error("Exception" ,e);
        } finally
        {
        }

    }

    public void stopThread()
    {
        stop = true;
    }
    
    private boolean within29Seconds(Date checkTime)
    {
        if (checkTime == null)
        {
            return false;
        }
        
        Date now = new Date();
        long nowMillis = now.getTime();
        long alertMillis = checkTime.getTime();
        long diffTime = nowMillis - alertMillis;
        
        if (diffTime < 29000)
        {
            return true;
        }
        
        return false;
    }
}
