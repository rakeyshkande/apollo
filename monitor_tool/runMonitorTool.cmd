echo off

set classpath=dist/monitor-tool.jar
set classpath=%classpath%;../lib/ftdutilities.jar
set classpath=%classpath%;../lib/utilities.jar
set classpath=%classpath%;../lib/log4j-1.2.5.jar
set classpath=%classpath%;../lib/commons-lang-2.2.jar
set classpath=%classpath%;../lib/xmlparserv2.jar
set classpath=%classpath%;lib/jcommon-1.0.6.jar
set classpath=%classpath%;lib/jfreechart-1.0.3.jar
set classpath=%classpath%;lib/ojdbc5dms.jar
set classpath=%classpath%;lib/dms.jar
set classpath=%classpath%;lib/SNMP4J.jar

java com.ftd.monitortool.MonitorTool
