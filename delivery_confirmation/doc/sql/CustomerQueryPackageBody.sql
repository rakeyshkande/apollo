CREATE OR REPLACE PACKAGE BODY CUSTOMER_QUERY_PKG AS
FUNCTION         IS_EMAIL_ADDRESS_ASSOCIATED
(
IN_ORDER_DETAIL_ID IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_EMAIL_ADDRESS IN CLEAN.EMAIL.EMAIL_ADDRESS%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns true if the email address is associated with the customer in the order.

Input:
        order detail id                      number
        email address			string

Output:
        Y/N

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
SELECT COUNT(DISTINCT email_address) FROM CLEAN.EMAIL em
INNER JOIN CLEAN.CUSTOMER_EMAIL_REF  cer ON cer.EMAIL_ID = em.EMAIL_ID
INNER JOIN CLEAN.ORDERS ord ON ord.CUSTOMER_ID = cer.CUSTOMER_ID  or ord.EMAIL_ID = em.EMAIL_ID
INNER JOIN CLEAN.ORDER_DETAILS orddtl ON orddtl.ORDER_GUID = ord.ORDER_GUID
WHERE orddtl.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
AND em.EMAIL_ADDRESS = IN_EMAIL_ADDRESS;

v_exists        char(1) := 'N';
v_count			int := 0;
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_count;
CLOSE exists_cur;

IF v_count > 0 THEN
   v_exists := 'Y';
END IF;

RETURN v_exists;

END IS_EMAIL_ADDRESS_ASSOCIATED;
END CUSTOMER_QUERY_PKG;