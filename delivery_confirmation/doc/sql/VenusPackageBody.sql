create or replace PACKAGE BODY VENUS_PKG AS
PROCEDURE FIND_FTD_MESS_BY_ORDER_DETAIL
(
 IN_ORDER_DETAIL_ID IN VENUS.VENUS.REFERENCE_NUMBER%TYPE,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table venus
        for a FTD message type that does not have an 'CAN' or 'REJ' message
        associated and is related to an order_detail
        for the order detail id passed in.
Input:
        order_detail_id  VARCHAR2
Output:
        cursor containing venus record
-----------------------------------------------------------------------------*/
BEGIN
  OPEN out_cursor FOR
 SELECT
v1.VENUS_ID,
v1.VENUS_ORDER_NUMBER,
v1.MSG_TYPE,
v1.ORDER_DATE,
v1.DELIVERY_DATE,
v1.SHIP_DATE,
v1.REFERENCE_NUMBER,
v1.SHIP_METHOD,
v1.TRACKING_NUMBER,
v1.FINAL_CARRIER,
v1.FINAL_SHIP_METHOD,
v1.LAST_SCAN,
v1.DELIVERY_SCAN
 from venus.venus v1 
WHERE v1.reference_number = in_order_detail_id  AND v1.msg_type = 'FTD' 
AND NOT EXISTS (SELECT * FROM venus.venus v2 WHERE  v1."VENUS_ORDER_NUMBER" = v2."VENUS_ORDER_NUMBER"
AND v2."MSG_TYPE" IN ('CAN','REJ') )
ORDER BY CREATED_ON DESC;
END FIND_FTD_MESS_BY_ORDER_DETAIL;

END VENUS_PKG;