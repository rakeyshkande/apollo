package com.ftd.dc.venus.dao;

import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


/**
 *
 *
 * @author Dave Ross
 */
public class VenusDAO 
{

  Connection conn;
  
    private Logger logger;  
    private static final int TN_MESSAGE_ERROR_LENGTH = 300;
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_ERROR_MESSAGE";  
    
  
  public VenusDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger(DCConstants.LOGGER_CATEGORY_VENUSDAO);
  }
  

    /**
     * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_FTD_MESS_BY_DETAIL_ID
     * stored procedure. 
     * 
     * @param String venusOrderNumber
     * @param String messsageType
     * @return List messages
     * @throws IOException, ParserConfigurationException, SQLException, SAXException
     */
    
    public List getVenusFTDMessageByDetailId(String orderDetailId) throws IOException, ParserConfigurationException, SQLException, SAXException
    {
      logger.debug("getVenusFTDMessageByDetailId(String orderDetailId ("+orderDetailId+")");
      CachedResultSet crs = null;
      VenusMessageVO msgVO = null;
      ArrayList messages = null;
      DataRequest dataRequest = new DataRequest();
      dataRequest.reset();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("FIND_FTD_MESS_BY_ORDER_DETAIL");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

      if( crs != null )
      {
        messages = new ArrayList();
        
        while ( crs.next() )
        {
          msgVO = new VenusMessageVO();
          msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
          msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
          msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
          msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
          msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
          msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
          msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
          msgVO.setShipMethod((String)crs.getObject("SHIP_METHOD"));
          msgVO.setTrackingNumber((String)crs.getObject("TRACKING_NUMBER"));
          msgVO.setFinalCarrier((String)crs.getObject("FINAL_CARRIER"));
          msgVO.setFinalShipMethod((String)crs.getObject("FINAL_SHIP_METHOD"));
          msgVO.setDeliveryScan(getUtilDate(crs.getObject("DELIVERY_SCAN")));
          msgVO.setLastScan(getUtilDate(crs.getObject("LAST_SCAN")));

          messages.add(msgVO);
        }
      }
      return messages;
    }
    
    private long getLong(Object obj)
    {
        return obj == null ? 0 : Integer.parseInt(obj.toString());
    }

    private double getDouble(Object obj)
    {
        return obj == null ? 0.0 : Double.parseDouble(obj.toString());
    }
    
       /*
       * Converts a database timestamp to a java.util.Date.
       */
      private java.util.Date getUtilDate(Object time)
      {
      
      
        java.util.Date theDate = null;
        boolean test  = false;
        if(time != null){
                if(test || time instanceof Timestamp)
                {
                  Timestamp timestamp = (Timestamp)time;
                  theDate = new java.util.Date(timestamp.getTime());
                }
                else
                {
                  theDate = (java.util.Date)time;
                  //theDate = new java.util.Date(timestamp.getTime());                
                 }
        }


        return theDate;
      }
    


}