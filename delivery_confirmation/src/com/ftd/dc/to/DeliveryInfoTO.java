package com.ftd.dc.to;

import com.ftd.dc.constants.DCConstants;

import java.util.Date;

import org.apache.axis.types.URI;


public class DeliveryInfoTO {
    

    private String trackingNumber;

    private URI carrierLookupURL;

    private String carrierSelected;

    private Date shipDate;

    private Date scheduledDeliveryDate;

    private String actualDeliveryDateTime;

    private String status;

    public DeliveryInfoTO() {
        setStatus(DCConstants.DC_UNKNOWN);
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setCarrierLookupURL(URI carrierLookupURL) {
        this.carrierLookupURL = carrierLookupURL;
    }

    public URI getCarrierLookupURL() {
        return carrierLookupURL;
    }

    public void setCarrierSelected(String carrierSelected) {
        this.carrierSelected = carrierSelected;
    }

    public String getCarrierSelected() {
        return carrierSelected;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setScheduledDeliveryDate(Date scheduledDeliveryDate) {
        this.scheduledDeliveryDate = scheduledDeliveryDate;
    }

    public Date getScheduledDeliveryDate() {
        return scheduledDeliveryDate;
    }

    public void setActualDeliveryDateTime(String actualDeliveryDateTime) {
        this.actualDeliveryDateTime = actualDeliveryDateTime;
    }

    public String getActualDeliveryDateTime() {
        return actualDeliveryDateTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    
    public String printInfo() {
        StringBuffer info = new StringBuffer();
        info.append("Status = " + getStatus() + ",");
        info.append("Ship Date = " + getShipDate() + ",");
        info.append("Scheduled Delivery Date = " + getScheduledDeliveryDate() + ",");
        info.append("Carrier Selected = " + getCarrierSelected() + ",");
        info.append("Carrier URL = " + getCarrierLookupURL() + ",");
        info.append("Tracking Number = " + getTrackingNumber() + ",");
        info.append("Actual Delivery Date = " + getActualDeliveryDateTime());
        return info.toString();
    }
}
