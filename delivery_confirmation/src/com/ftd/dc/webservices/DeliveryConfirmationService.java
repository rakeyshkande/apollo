/**
 * DeliveryConfirmationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.dc.webservices;

public interface DeliveryConfirmationService extends java.rmi.Remote {
    public com.ftd.dc.webservices.DeliveryConfirmationResponse getDeliveryConfirmation(com.ftd.dc.webservices.DeliveryConfirmationRequest request) throws java.rmi.RemoteException;
}
