/**
 * DeliveryConfirmationServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.dc.webservices;

public interface DeliveryConfirmationServiceService extends javax.xml.rpc.Service {
    public java.lang.String getDeliveryConfirmationServiceAddress();

    public com.ftd.dc.webservices.DeliveryConfirmationService getDeliveryConfirmationService() throws javax.xml.rpc.ServiceException;

    public com.ftd.dc.webservices.DeliveryConfirmationService getDeliveryConfirmationService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
