/**
 * DeliveryConfirmationServiceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.dc.webservices;

import com.ftd.dc.to.DeliveryInfoTO;


/**
 * DeliveryConfirmationServiceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
public class

DeliveryConfirmationServiceSoapBindingImpl implements com.ftd.dc.webservices.DeliveryConfirmationService{
    public com.ftd.dc.webservices.DeliveryConfirmationResponse getDeliveryConfirmation(com.ftd.dc.webservices.DeliveryConfirmationRequest request) throws java.rmi.RemoteException {
    return getConfirmation(request.getOrderNumber(),request.getEmailAddress());
    }
    
    private DeliveryConfirmationResponse getConfirmation(String orderNumber, String emailAddress) {
        
		DeliveryConfirmationResponse resp = new DeliveryConfirmationResponse();
        com.ftd.dc.service.DeliveryConfirmationService confService = new com.ftd.dc.service.DeliveryConfirmationService();
        DeliveryInfoTO deliveryInfoTO = confService.getDeliveryConfirmation(orderNumber,emailAddress);
        resp.setActualDeliveryDateTime(deliveryInfoTO.getActualDeliveryDateTime());
        resp.setCarrierLookupURL(deliveryInfoTO.getCarrierLookupURL());
        resp.setCarrierSelected(deliveryInfoTO.getCarrierSelected());
        resp.setScheduledDeliveryDate(deliveryInfoTO.getScheduledDeliveryDate());
        resp.setShipDate(deliveryInfoTO.getShipDate());
        resp.setStatus(deliveryInfoTO.getStatus());
        resp.setTrackingNumber(deliveryInfoTO.getTrackingNumber());
        return resp;
    }

}
