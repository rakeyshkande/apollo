/**
 * DeliveryConfirmationServiceSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.dc.webservices;

public class DeliveryConfirmationServiceSoapBindingSkeleton implements com.ftd.dc.webservices.DeliveryConfirmationService, org.apache.axis.wsdl.Skeleton {
    private com.ftd.dc.webservices.DeliveryConfirmationService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:deliveryConfirmation", "DeliveryConfirmationRequest"), com.ftd.dc.webservices.DeliveryConfirmationRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getDeliveryConfirmation", _params, new javax.xml.namespace.QName("", "response"));
        _oper.setReturnType(new javax.xml.namespace.QName("urn:deliveryConfirmation", "DeliveryConfirmationResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("urn:deliveryConfirmation", "getDeliveryConfirmation"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getDeliveryConfirmation") == null) {
            _myOperations.put("getDeliveryConfirmation", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getDeliveryConfirmation")).add(_oper);
    }

    public DeliveryConfirmationServiceSoapBindingSkeleton() {
        this.impl = new com.ftd.dc.webservices.DeliveryConfirmationServiceSoapBindingImpl();
    }

    public DeliveryConfirmationServiceSoapBindingSkeleton(com.ftd.dc.webservices.DeliveryConfirmationService impl) {
        this.impl = impl;
    }
    public com.ftd.dc.webservices.DeliveryConfirmationResponse getDeliveryConfirmation(com.ftd.dc.webservices.DeliveryConfirmationRequest request) throws java.rmi.RemoteException
    {
        com.ftd.dc.webservices.DeliveryConfirmationResponse ret = impl.getDeliveryConfirmation(request);
        return ret;
    }

}
