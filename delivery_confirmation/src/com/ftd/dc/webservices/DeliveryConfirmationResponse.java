/**
 * DeliveryConfirmationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.dc.webservices;

public class DeliveryConfirmationResponse  implements java.io.Serializable {
    private java.lang.String trackingNumber;

    private org.apache.axis.types.URI carrierLookupURL;

    private java.lang.String carrierSelected;

    private java.util.Date shipDate;

    private java.util.Date scheduledDeliveryDate;

    private java.lang.String actualDeliveryDateTime;

    private java.lang.String status;

    public DeliveryConfirmationResponse() {
    }

    public DeliveryConfirmationResponse(
           java.lang.String trackingNumber,
           org.apache.axis.types.URI carrierLookupURL,
           java.lang.String carrierSelected,
           java.util.Date shipDate,
           java.util.Date scheduledDeliveryDate,
           java.lang.String actualDeliveryDateTime,
           java.lang.String status) {
           this.trackingNumber = trackingNumber;
           this.carrierLookupURL = carrierLookupURL;
           this.carrierSelected = carrierSelected;
           this.shipDate = shipDate;
           this.scheduledDeliveryDate = scheduledDeliveryDate;
           this.actualDeliveryDateTime = actualDeliveryDateTime;
           this.status = status;
    }


    /**
     * Gets the trackingNumber value for this DeliveryConfirmationResponse.
     * 
     * @return trackingNumber
     */
    public java.lang.String getTrackingNumber() {
        return trackingNumber;
    }


    /**
     * Sets the trackingNumber value for this DeliveryConfirmationResponse.
     * 
     * @param trackingNumber
     */
    public void setTrackingNumber(java.lang.String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }


    /**
     * Gets the carrierLookupURL value for this DeliveryConfirmationResponse.
     * 
     * @return carrierLookupURL
     */
    public org.apache.axis.types.URI getCarrierLookupURL() {
        return carrierLookupURL;
    }


    /**
     * Sets the carrierLookupURL value for this DeliveryConfirmationResponse.
     * 
     * @param carrierLookupURL
     */
    public void setCarrierLookupURL(org.apache.axis.types.URI carrierLookupURL) {
        this.carrierLookupURL = carrierLookupURL;
    }


    /**
     * Gets the carrierSelected value for this DeliveryConfirmationResponse.
     * 
     * @return carrierSelected
     */
    public java.lang.String getCarrierSelected() {
        return carrierSelected;
    }


    /**
     * Sets the carrierSelected value for this DeliveryConfirmationResponse.
     * 
     * @param carrierSelected
     */
    public void setCarrierSelected(java.lang.String carrierSelected) {
        this.carrierSelected = carrierSelected;
    }


    /**
     * Gets the shipDate value for this DeliveryConfirmationResponse.
     * 
     * @return shipDate
     */
    public java.util.Date getShipDate() {
        return shipDate;
    }


    /**
     * Sets the shipDate value for this DeliveryConfirmationResponse.
     * 
     * @param shipDate
     */
    public void setShipDate(java.util.Date shipDate) {
        this.shipDate = shipDate;
    }


    /**
     * Gets the scheduledDeliveryDate value for this DeliveryConfirmationResponse.
     * 
     * @return scheduledDeliveryDate
     */
    public java.util.Date getScheduledDeliveryDate() {
        return scheduledDeliveryDate;
    }


    /**
     * Sets the scheduledDeliveryDate value for this DeliveryConfirmationResponse.
     * 
     * @param scheduledDeliveryDate
     */
    public void setScheduledDeliveryDate(java.util.Date scheduledDeliveryDate) {
        this.scheduledDeliveryDate = scheduledDeliveryDate;
    }


    /**
     * Gets the actualDeliveryDateTime value for this DeliveryConfirmationResponse.
     * 
     * @return actualDeliveryDateTime
     */
    public java.lang.String getActualDeliveryDateTime() {
        return actualDeliveryDateTime;
    }


    /**
     * Sets the actualDeliveryDateTime value for this DeliveryConfirmationResponse.
     * 
     * @param actualDeliveryDateTime
     */
    public void setActualDeliveryDateTime(java.lang.String actualDeliveryDateTime) {
        this.actualDeliveryDateTime = actualDeliveryDateTime;
    }


    /**
     * Gets the status value for this DeliveryConfirmationResponse.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this DeliveryConfirmationResponse.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeliveryConfirmationResponse)) return false;
        DeliveryConfirmationResponse other = (DeliveryConfirmationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trackingNumber==null && other.getTrackingNumber()==null) || 
             (this.trackingNumber!=null &&
              this.trackingNumber.equals(other.getTrackingNumber()))) &&
            ((this.carrierLookupURL==null && other.getCarrierLookupURL()==null) || 
             (this.carrierLookupURL!=null &&
              this.carrierLookupURL.equals(other.getCarrierLookupURL()))) &&
            ((this.carrierSelected==null && other.getCarrierSelected()==null) || 
             (this.carrierSelected!=null &&
              this.carrierSelected.equals(other.getCarrierSelected()))) &&
            ((this.shipDate==null && other.getShipDate()==null) || 
             (this.shipDate!=null &&
              this.shipDate.equals(other.getShipDate()))) &&
            ((this.scheduledDeliveryDate==null && other.getScheduledDeliveryDate()==null) || 
             (this.scheduledDeliveryDate!=null &&
              this.scheduledDeliveryDate.equals(other.getScheduledDeliveryDate()))) &&
            ((this.actualDeliveryDateTime==null && other.getActualDeliveryDateTime()==null) || 
             (this.actualDeliveryDateTime!=null &&
              this.actualDeliveryDateTime.equals(other.getActualDeliveryDateTime()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrackingNumber() != null) {
            _hashCode += getTrackingNumber().hashCode();
        }
        if (getCarrierLookupURL() != null) {
            _hashCode += getCarrierLookupURL().hashCode();
        }
        if (getCarrierSelected() != null) {
            _hashCode += getCarrierSelected().hashCode();
        }
        if (getShipDate() != null) {
            _hashCode += getShipDate().hashCode();
        }
        if (getScheduledDeliveryDate() != null) {
            _hashCode += getScheduledDeliveryDate().hashCode();
        }
        if (getActualDeliveryDateTime() != null) {
            _hashCode += getActualDeliveryDateTime().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeliveryConfirmationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:deliveryConfirmation", "DeliveryConfirmationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trackingNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierLookupURL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carrierLookupURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierSelected");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carrierSelected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduledDeliveryDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "scheduledDeliveryDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualDeliveryDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "actualDeliveryDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
