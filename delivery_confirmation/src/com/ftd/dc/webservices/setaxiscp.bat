@ECHO OFF
 REM Locations: Change these to match your environment
 set AXIS_HOME=C:\lib.dir\axis-1_2beta
 set ACTIVATION_HOME=C:\lib.dir\jaf-1.0.2
 set MAIL_HOME=C:\lib.dir\javamail-1.3.1
 set XML_HOME=C:\lib.dir\xalan-j_2_6_0\bin
 set ["LOG4J"]_PROPERTIES_HOME=C:\axisUtils
 set AXIS_LIB=%AXIS_HOME%\lib
 REM Create the class path 
 set AXISCP=.
 set AXISCP=%AXISCP%;%ACTIVATION_HOME%\activation.jar
 set AXISCP=%AXISCP%;%AXIS_LIB%\axis.jar
 set AXISCP=%AXISCP%;%AXIS_LIB%\commons-discovery.jar
 set AXISCP=%AXISCP%;%AXIS_LIB%\commons-logging.jar 
 set AXISCP=%AXISCP%;%AXIS_LIB%\jaxrpc.jar
 set AXISCP=%AXISCP%;%AXIS_LIB%\saaj.jar 
 set AXISCP=%AXISCP%;%AXIS_LIB%\log4j-1.2.8.jar
 set AXISCP=%AXISCP%;%AXIS_LIB%\wsdl4j.jar
 set AXISCP=%AXISCP%;%MAIL_HOME%\mail.jar
 set AXISCP=%AXISCP%;%XML_HOME%\xml-apis.jar
 set AXISCP=%AXISCP%;%XML_HOME%\xercesImpl.jar
 ECHO set up classpath
 REM set logging to the console using log4j
 set AXISCP=%AXISCP%;%LOG4J_PROPERTIES_HOME%
 ECHO initialised logging
 ECHO Should now be able to call
 ECHO adminclient, tcpmon, wsdl2java, java2wsdl ...