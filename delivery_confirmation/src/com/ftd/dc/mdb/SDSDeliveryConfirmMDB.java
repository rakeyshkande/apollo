package com.ftd.dc.mdb;

import com.ftd.dc.framework.util.CommonUtils;
import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.service.EmailDeliveryConfirmationService;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


public class SDSDeliveryConfirmMDB implements MessageDrivenBean, MessageListener {
    private MessageDrivenContext context;
    private Logger logger;
   

    public void ejbCreate() {

    }

    public void onMessage(Message msg) {
    	Connection conn = null;
        logger.debug("SDS Delivery Confirm Message Driven Bean [Start]");
        String msgData = null;
        try {
        	conn = CommonUtils.getConnection();
            //get message 
            TextMessage textMessage = (TextMessage)msg;
            msgData = textMessage.getText();
            if(msgData == null || msgData.equalsIgnoreCase("")) {
                logger.info("Received empty or null message.");
            }else {

                logger.debug("processing JMS MESSAGE: " + msgData);

                // process the order
                EmailDeliveryConfirmationService service = new EmailDeliveryConfirmationService(conn);
                service.sendDeliveryConfirmation(msgData);
            }
        } catch (Exception e) {
            logger.error(e);  
            try {
            	CommonUtils.sendSystemMessage("SDSDeliveryConfirmMDB", "SDSDeliveryConfirmMDB failed:" + msgData + "\n" + e.getMessage());  
            } catch (Throwable t) {
            	logger.error(t);            	
            }
        }
        finally{
        	if (conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        }
        logger.debug("SDS Delivery Confirm Message Driven Bean [End]");
    }

    public void ejbRemove() {
        
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx) {
        this.context = ctx;
        logger = new Logger(DCConstants.LOGGER_CATEGORY_SDSDeliveryConfirmMDB);
    }
}
