package com.ftd.dc.framework.util;

import com.ftd.dc.constants.DCConstants;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import java.util.Calendar;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;


/**
 *
 * @author Jason Weiss
 */
public class CommonUtils
{

  public CommonUtils(){

  }

  /*
   * Get database connection
   */
  static public Connection getConnection() throws Exception
  {
    return getConnection("DATABASE_CONNECTION");
  }

  static public Connection getConnection(String connectionName) throws Exception
  {

    ConfigurationUtil config = ConfigurationUtil.getInstance();
    String dbConnection = config.getProperty(DCConstants.PROPERTY_FILE,connectionName);

    //get DB connection
    DataSource dataSource = (DataSource)lookupResource(dbConnection);
    return dataSource.getConnection();
  }


  static public DataSource getDataSource() throws Exception
  {
    ConfigurationUtil config = ConfigurationUtil.getInstance();
    String dbConnection = config.getProperty(DCConstants.PROPERTY_FILE,"DATABASE_CONNECTION");

    //get DB connection
    DataSource dataSource = (DataSource)lookupResource(dbConnection);
    return dataSource;

  }

  /**
   * This method sends a message to the System Messenger.
   *
   * @param String message
   * @returns String message id
   */
  static public String sendSystemMessage(String message) throws Exception
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String messageSource = configUtil.getProperty(DCConstants.PROPERTY_FILE,"MESSAGE_SOURCE");

    return sendSystemMessage(messageSource, message);
  }

  /**
   * This method sends a message to the System Messenger.
   * Overloaded method that also takes a "source" as input. This is so
   * that source can be easily identified if the page from this source
   * needs to be turned off.
   *
   * @param String message
   * @returns String message id
   */
  static public String sendSystemMessage(String source, String message) throws Exception
  {
    Logger logger =  new Logger(DCConstants.LOGGER_CATEGORY_COMMONUTILS);

    logger.error("Sending System Message:" + message);

    String messageID = "";

    //build system vo
    SystemMessengerVO sysMessage = new SystemMessengerVO();
    sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
    sysMessage.setSource(source);
    sysMessage.setType("ERROR");
    sysMessage.setMessage(message);

    SystemMessenger sysMessenger = SystemMessenger.getInstance();
    Connection conn = getConnection();
    try
    {
      messageID = sysMessenger.send(sysMessage,conn);

      if(messageID == null) {
        String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
        logger.error(msg);
        System.out.println(msg);
      }
    }
    finally
    {
      conn.close();
    }

    return messageID;
  }  
  
  /**
   * Returns a transactional resource from the EJB container.
   *
   * @param jndiName
   * @return
   * @throws javax.naming.NamingException
   */
  public static Object lookupResource(String jndiName)
              throws NamingException
  {
    InitialContext initContext = null;
    try
    {
      initContext = new InitialContext();
//      Context myenv = (Context) initContext.lookup("java:comp/env");
//      System.out.println("Resource lookup env:" + myenv.getEnvironment());

      return initContext.lookup(jndiName);
    }finally  {
      try  {
        initContext.close();
      } catch (Exception ex)  {

      } finally  {
      }
    }
  }
  /**
 * Sends out a JMS message.
 *
 * @param context Initial Context
 * @param messageToken JMS Message
 * @throws java.lang.Exception
 */
  public static void sendJMSMessage(InitialContext context,MessageToken messageToken)
      throws Exception {

//
//     ConfigurationUtil config = ConfigurationUtil.getInstance();
//     String connectionFactoryLocation = config.getProperty(CONFIG_FILE,"CONNECTION_FACTORY_LOCATION");
//     String destinationLocation = config.getProperty(CONFIG_FILE,"MERCURY_DESTINATION_LOCATION");

     //make the correlation id the same as the message
     messageToken.setJMSCorrelationID((String)messageToken.getMessage());

     Dispatcher dispatcher = Dispatcher.getInstance();
     messageToken.setStatus("OPSUFFIX");
     dispatcher.dispatchTextMessage(context, messageToken);
  }

/**
   * Sends out a JMS message.
   *
   * @throws java.lang.Exception
   * @param status
   * @param destinationLocationPropertyName
   * @param messageToken
   * @param context
   */
  public static void sendJMSMessage(InitialContext context,MessageToken messageToken, String status)
      throws Exception {

//
//     ConfigurationUtil config = ConfigurationUtil.getInstance();
//     String connectionFactoryLocation = config.getProperty(CONFIG_FILE,"CONNECTION_FACTORY_LOCATION");
//     String destinationLocation = config.getProperty(CONFIG_FILE, destinationLocationPropertyName);

     //make the correlation id the same as the message
     messageToken.setJMSCorrelationID((String)messageToken.getMessage());

     Dispatcher dispatcher = Dispatcher.getInstance();
     messageToken.setStatus(status);
     dispatcher.dispatchTextMessage(context, messageToken);
  }

  /**
   * Clears the time from a date object.
   * @return Date
   * @param Date
   */
  public static Date clearTime (Date date)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.clear(Calendar.HOUR_OF_DAY);
    calendar.clear(Calendar.HOUR);
    calendar.clear(Calendar.MINUTE);
    calendar.clear(Calendar.SECOND);
    calendar.clear(Calendar.MILLISECOND);
    calendar.set(Calendar.AM_PM,Calendar.AM);
    return new Date(calendar.getTimeInMillis());
  }
}