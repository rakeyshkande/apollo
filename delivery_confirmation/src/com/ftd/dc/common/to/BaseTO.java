package com.ftd.dc.common.to;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.List;


public class BaseTO implements Serializable 
{
  public BaseTO()
  {
  }

 /** 
  * This method prints all the getters for a given object using System.out.println.
  * 
  * @param Object obj
  * **/
 public static void printGetters (Object obj) throws Exception 
  {
    if (obj != null)
    {
        try{
          Class thisClass = obj.getClass ();
          Method[] methods = thisClass.getMethods();
          String ret;
          for (int i = 0; i < methods.length; i++) {
               String name = methods [i].getName ();
           if (name.startsWith ("get")||name.startsWith ("is")) {
                  System.out.println(name + " : "+methods[i].invoke(obj, null));
                  //logger.debug(name + " : "+methods[i].invoke(obj, null));
               }
           }
        }
       catch ( Exception e) {
            e.printStackTrace ();
           //throw new RuntimeException (e.getMessage ());
        }
    }
    else
    {
      System.out.println("Object is null.");
    }
  }

  public static void printGettersForList (List list)throws Exception
  {
      for (int i = 0; i< list.size(); i ++)
      {
        BaseTO.printGetters(list.get(i));
      }
  }


 /** 
  * This method prints all the getters for a given object using System.out.println.
  * 
  * @param Object obj
  * **/
 public static void logGetters (Logger logger, Object obj) throws Exception 
  {
    logger.debug("---------------- ENTER logGetters (Logger logger, Object obj) -----------");
    if (obj != null)
    {
        try{
          Class thisClass = obj.getClass ();
          Method[] methods = thisClass.getMethods();
          String ret;
          for (int i = 0; i < methods.length; i++) {
               String name = methods [i].getName ();
           if (name.startsWith ("get")||name.startsWith ("is")) {
                  logger.debug(name + " : "+methods[i].invoke(obj, null));
               }
           }
        }
       catch ( Exception e) {
            e.printStackTrace ();
           //throw new RuntimeException (e.getMessage ());
        }
    }
    else
    {
      System.out.println("Object is null.");
    }
    logger.debug("----------------EXIT logGetters (Logger logger, Object obj) -----------");
  }

}