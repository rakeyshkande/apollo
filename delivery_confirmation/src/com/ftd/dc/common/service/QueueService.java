package com.ftd.dc.common.service;

import com.ftd.dc.common.dao.QueueDAO;
import com.ftd.dc.common.vo.QueueVO;
import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.order.dao.OrderDAO;
import com.ftd.dc.order.vo.OrderDetailVO;
import com.ftd.dc.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Calendar;


/**
 * QueueHandler
 *
 * This class handles the transmission of orders
 * to various queues for additional processing.
 *
 * @author Nicole Roberts
 */
 
public class QueueService 
{
  private Logger logger;
  
  // queue names
  private static final String DI = "DI";

  private static final String VENUS = "Venus";
  private static final String MERCURY = "Merc";
  
  private Connection conn;
  
  /**
     * Constructor
     * 
     * @param none
     * @return n/a
     * @throws none
     */
  public QueueService(Connection c)
  {
    conn = c;
    logger = new Logger(DCConstants.LOGGER_CATEGORY_QUEUESERVICE);
  }
  
  
  /**
   * This method places the order in the DI Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToDIQueue(OrderDetailVO orderDetail) throws Exception {
    sendOrderToCSQueue(orderDetail, DI);
  }
  

  /**
   * Place a given order in the passed in customer service queue
   * 
   * @param orderDetail order to enqueue
   * @param queue customer service queue
   */
  private void sendOrderToCSQueue(OrderDetailVO orderDetail, String queue) throws Exception {
    try{
      OrderDAO orderDao = new OrderDAO();
      QueueDAO queueDao = new QueueDAO(conn);

      QueueVO queueItem = new QueueVO();
      OrderVO order = orderDao.getOrder(conn,orderDetail.getOrderGuid());      
      /* throw exeption if no order record retrieved */
      if(order == null)
      {
        throw new Exception("No order record found");
      }
            
      queueItem.setQueueType(queue);
      queueItem.setMessageType(queue);
      
      // set message timestamp to now
      Calendar cal = Calendar.getInstance();
      queueItem.setMessageTimestamp(cal.getTime());
      if(orderDetail.getShipMethod() == null || orderDetail.getShipMethod().equals("SD")) {
        queueItem.setSystem(MERCURY);
      } else 
      {
        queueItem.setSystem(VENUS);
      }
      queueItem.setMercuryNumber(null);
      queueItem.setMasterOrderNumber(order.getMasterOrderNumber());
      queueItem.setOrderGuid(orderDetail.getOrderGuid());
      queueItem.setOrderDetailId(new Long(orderDetail.getOrderDetailId()).toString());
      queueItem.setExternalOrderNumber(orderDetail.getExternalOrderNumber());
      queueItem.setMercuryId(null);
      
      queueDao.insertQueueRecord(queueItem);
    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
  }

}