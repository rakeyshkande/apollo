package com.ftd.dc.common.vo;

import com.ftd.dc.common.to.BaseTO;

import java.io.Serializable;


public class BaseVO implements Serializable
{

  public BaseVO()
  {
  }

 /** 
  * This method prints all the getters for a given object using System.out.println.
  * 
  * @param Object obj
  * **/
 public static void printGetters (Object obj) throws Exception 
  {
    BaseTO.printGetters(obj);
  }

}
