package com.ftd.dc.common.dao;


import com.ftd.dc.common.vo.QueueVO;
import com.ftd.dc.constants.DCConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;

/**
 * 
 * @author Jason Weiss
 */
public class QueueDAO 
{
  private Connection conn;
  private Logger logger;
  
  // stored procedure strings
  private static final String QUEUE_TYPE = "IN_QUEUE_TYPE";
  private static final String MESSAGE_TYPE = "IN_MESSAGE_TYPE";
  private static final String MESSAGE_TIMESTAMP = "IN_MESSAGE_TIMESTAMP";
  private static final String SYSTEM = "IN_SYSTEM";
  private static final String MERCURY_NUMBER = "IN_MERCURY_NUMBER";
  private static final String MASTER_ORDER_NUMBER = "IN_MASTER_ORDER_NUMBER";
  private static final String ORDER_GUID = "IN_ORDER_GUID";
  private static final String ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
  private static final String EXTERNAL_ORDER_NUMBER = "IN_EXTERNAL_ORDER_NUMBER";
  private static final String POC_ID = "IN_POINT_OF_CONTACT_ID";
  private static final String EMAIL_ADDRESS = "IN_EMAIL_ADDRESS";
  private static final String MERCURY_ID = "IN_MERCURY_ID";
  
  private static final String INSERT_QUEUE_RECORD = "INSERT_QUEUE_RECORD";
  private static final String STATUS = "OUT_STATUS";
  
  // SP output checking strings
  private static final String NO = "N";
  private static final String ERROR_MESSAGE = "OUT_ERROR_MESSAGE";
  
  public QueueDAO(Connection c)
  {
    conn = c;
    logger = new Logger(DCConstants.LOGGER_CATEGORY_QUEUEDAO);
  }
  public void insertQueueRecord(QueueVO queueRecord) throws Exception{
    DataRequest dataRequest = new DataRequest();
    HashMap outputs = null;
    
   
      
      /* setup stored procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(QUEUE_TYPE, new String(queueRecord.getQueueType()));
      inputParams.put(MESSAGE_TYPE, new String(queueRecord.getMessageType()));      
      java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());      
      inputParams.put(MESSAGE_TIMESTAMP, sqlTimeStamp);
      inputParams.put(SYSTEM, queueRecord.getSystem() != null ? queueRecord.getSystem() : null);
      if(queueRecord.getMercuryNumber() != null) {
        inputParams.put(MERCURY_NUMBER, new String(queueRecord.getMercuryNumber()));
      } else 
      {
        inputParams.put(MERCURY_NUMBER, null);
      }
      inputParams.put(MASTER_ORDER_NUMBER, queueRecord.getMasterOrderNumber() != null ? queueRecord.getMasterOrderNumber() : null);
      inputParams.put(ORDER_GUID, queueRecord.getOrderGuid() != null ? queueRecord.getOrderGuid() : null);
      inputParams.put(ORDER_DETAIL_ID, queueRecord.getOrderDetailId() != null ? queueRecord.getOrderDetailId() : null);
      inputParams.put(EXTERNAL_ORDER_NUMBER, queueRecord.getExternalOrderNumber() != null ? queueRecord.getExternalOrderNumber() : null);
      inputParams.put(POC_ID, null);
      inputParams.put(EMAIL_ADDRESS, null);
      inputParams.put(MERCURY_ID, queueRecord.getMercuryId());

      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(INSERT_QUEUE_RECORD);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (HashMap) dataAccessUtil.execute(dataRequest);
      
      /* read stored procedure output parameters to determine 
       * if the procedure executed successfully */
      if (!outputs.get("OUT_STATUS").equals("Y"))
      {
          throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
      }

     
    }
}