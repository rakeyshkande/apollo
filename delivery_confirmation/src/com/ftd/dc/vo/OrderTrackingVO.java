package com.ftd.dc.vo;


public class OrderTrackingVO extends BaseVO
{

  private	String	trackingNumber;
  private String trackingURL;
  private	long	  orderDetailId;
  private	String	trackingDescription;
  private   String carrierName;
  private String carrierURL;
  private String carrierPhone;


  public OrderTrackingVO()
  {
  }


  public void setTrackingNumber(String trackingNumber)
  {
    if(valueChanged(this.trackingNumber, trackingNumber))
    {
      setChanged(true);
    }
    this.trackingNumber = trim(trackingNumber);
  }


  public String getTrackingNumber()
  {
    return trackingNumber;
  }


  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setTrackingDescription(String trackingDescription)
  {
    if(valueChanged(this.trackingDescription, trackingDescription))
    {
      setChanged(true);
    }
    this.trackingDescription = trim(trackingDescription);
  }


  public String getTrackingDescription()
  {
    return trackingDescription;
  }

  public String getCarrierName()
  {
     return carrierName;
  }

  public void setCarrierName(String carrierName)
  {
     if(valueChanged(this.carrierName, carrierName))
     {
        setChanged(true);
     }

     this.carrierName = carrierName;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public String getCarrierURL()
    {
        return carrierURL;
    }

    public void setCarrierURL(String carrierURL)
    {
        if(valueChanged(this.carrierURL, carrierURL))
        {
           setChanged(true);
        }

        this.carrierURL = carrierURL;
    }

    public String getCarrierPhone()
    {
        return carrierPhone;
    }

    public void setCarrierPhone(String carrierPhone)
    {
        if(valueChanged(this.carrierPhone, carrierPhone))
        {
           setChanged(true);
        }

        this.carrierPhone = carrierPhone;
    }

    public void setTrackingURL(String trackingURL) {
        this.trackingURL = trackingURL;
    }

    public String getTrackingURL() {
        return trackingURL;
    }
}
