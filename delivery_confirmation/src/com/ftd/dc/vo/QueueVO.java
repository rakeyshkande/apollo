package com.ftd.dc.vo;

import java.util.Date;


/**
 * Adding comments
 * @author Jason Weiss
 */
public class QueueVO {

  private String attached; 
  private Date   createdOn;
  private Date   deliveryDate;
  private String emailAddress;
  private String externalOrderNumber;
  private String masterOrderNumber;
  private String mercuryId;
  private String mercuryNumber;
  private String messageId; 
  private Date   messageTimestamp;
  private String messageType;
  private String orderDetailId;
  private String orderGuid;
  private String pointOfContactId; 
  private int    priority; 
  private String queueType;
  private String recipientLocation;
  private String sameDayGift;
  private String system;
  private String tagged;
  private String timezone;
  private String untaggedNnewItems;

  private OrderDetailVO ordDetVO;


  public QueueVO()
  {
  }


  public void setQueueType(String queueType)
  {
    this.queueType = queueType;
  }


  public String getQueueType()
  {
    return queueType;
  }


  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }


  public String getMessageType()
  {
    return messageType;
  }


  public void setMessageTimestamp(Date messageTimestamp)
  {
    this.messageTimestamp = (Date)messageTimestamp.clone();
  }


  public Date getMessageTimestamp()
  {
    return messageTimestamp;
  }


  public void setSystem(String system)
  {
    this.system = system;
  }


  public String getSystem()
  {
    return system;
  }


  public void setMercuryNumber(String mercuryNumber)
  {
    this.mercuryNumber = mercuryNumber;
  }


  public String getMercuryNumber()
  {
    return mercuryNumber;
  }


  public void setMasterOrderNumber(String masterOrderNumber)
  {
    this.masterOrderNumber = masterOrderNumber;
  }


  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }


  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }


  public void setOrderGuid(String orderGuid)
  {
    this.orderGuid = orderGuid;
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }


  public String getMercuryId()
  {
    return mercuryId;
  }

  public void setMessageId(String messageId)
  {
    this.messageId = messageId;
  }

  public String getMessageId()
  {
    return messageId;
  }

  public void setPointOfContactId(String pointOfContactId)
  {
    this.pointOfContactId = pointOfContactId;
  }

  public String getPointOfContactId()
  {
    return pointOfContactId;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }

  public int getPriority()
  {
    return priority;
  }

  public void setAttached(String attached)
  {
    this.attached = attached;
  }

  public String getAttached()
  {
    return attached;
  }

  public void setOrdDetVO(OrderDetailVO ordDetVO)
  {
    this.ordDetVO = ordDetVO;
  }

  public OrderDetailVO getOrdDetVO()
  {
    return ordDetVO;
  }

  public void setCreatedOn(Date createdOn)
  {
    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }

  public void setEmailAddress(String emailAddress)
  {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress()
  {
    return emailAddress;
  }

  public void setRecipientLocation(String recipientLocation)
  {
    this.recipientLocation = recipientLocation;
  }

  public String getRecipientLocation()
  {
    return recipientLocation;
  }

  public void setSameDayGift(String sameDayGift)
  {
    this.sameDayGift = sameDayGift;
  }

  public String getSameDayGift()
  {
    return sameDayGift;
  }

  public void setTimezone(String timezone)
  {
    this.timezone = timezone;
  }

  public String getTimezone()
  {
    return timezone;
  }

  public void setUntaggedNnewItems(String untaggedNnewItems)
  {
    this.untaggedNnewItems = untaggedNnewItems;
  }

  public String getUntaggedNnewItems()
  {
    return untaggedNnewItems;
  }

  public void setTagged(String tagged)
  {
    this.tagged = tagged;
  }

  public String getTagged()
  {
    return tagged;
  }
}
