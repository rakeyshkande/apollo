package com.ftd.dc.test;

import com.ftd.dc.service.EmailDeliveryConfirmationService;

import java.sql.Connection;

import java.sql.DriverManager;

import junit.framework.TestCase;

public class DCOMSvsTest extends TestCase {
    private Connection conn = null;
    
    public DCOMSvsTest(String sTestName) {
        super(sTestName);
    }

    public static void main(String[] args) {
        try {
            DCOMSvsTest test = new DCOMSvsTest("testService");
            test.runBare();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testService() throws Exception {
        EmailDeliveryConfirmationService svs = new EmailDeliveryConfirmationService(conn);
        svs.sendDeliveryConfirmation("1554981");
    }
    
    public void setUp() throws Exception
    {
        DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());

        conn = DriverManager.getConnection
             ("jdbc:oracle:thin:@adonis.ftdi.com:1522:dev6", "osp", "osp");
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
}
