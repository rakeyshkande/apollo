package com.ftd.dc.test;

import com.ftd.dc.webservices.DeliveryConfirmationRequest;
import com.ftd.dc.webservices.DeliveryConfirmationResponse;
import com.ftd.dc.webservices.DeliveryConfirmationService;
import com.ftd.dc.webservices.DeliveryConfirmationServiceService;
import com.ftd.dc.webservices.DeliveryConfirmationServiceServiceLocator;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.AxisFault;


public class DCONServiceTestServlet extends HttpServlet {
    private Logger logger = new Logger("com.ftd.dc.test.DCONServiceTestServlet");
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, 
                                                           IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>DCONServiceTestServlet</title></head>");
        out.println("<body>");        
        String emailAddress = (String) request.getParameter("emailAddress");
        String externalOrdNum = (String) request.getParameter("externalOrderNumber");
        StringBuffer respStr = new StringBuffer("<p>");
        respStr.append("Servlet Received emailAddress=" + emailAddress + " externalOrderNumber=" + externalOrdNum);
        respStr.append("&nbsp;<br><br><br><br>");
        out.println(respStr.toString());            
        this.getDeliveryConfirmation(emailAddress,externalOrdNum,out);
        out.println("</p>");
        out.println("</body></html>");
        out.close();
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, 
                                                            IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>DCONServiceTestServlet</title></head>");
        out.println("<body>");
        out.println("<h3><font color=\"#0000ff\"><strong>");
        String emailAddress = (String) request.getParameter("emailAddress");
        String externalOrdNum = (String) request.getParameter("externalOrderNumber");
        StringBuffer respStr = new StringBuffer("<p>");
        respStr.append("Received emailAddress=" + emailAddress + " externalOrderNumber=" + externalOrdNum);
        respStr.append("&nbsp;<br><br><br><br>");
        out.println(respStr.toString());            
        this.getDeliveryConfirmation(emailAddress,externalOrdNum,out);
        out.println("</p>");
        out.println("</strong></font></h3>");        
        out.println("</body></html>");
        out.close();
    }
    
    private void getDeliveryConfirmation(String emailAddress, String externalOrdNum, PrintWriter out) {
        try {
            DeliveryConfirmationServiceService service = 
                new DeliveryConfirmationServiceServiceLocator();
            DeliveryConfirmationService port = 
                service.getDeliveryConfirmationService();
            DeliveryConfirmationRequest req = 
                new DeliveryConfirmationRequest();
            req.setEmailAddress(emailAddress);
            
            req.setOrderNumber(externalOrdNum);
            DeliveryConfirmationResponse resp = 
                port.getDeliveryConfirmation(req);
            //print the results

            out.println("Response : <br><br>");
            out.println("Actual Delivery Date=" + 
                               resp.getActualDeliveryDateTime());
            out.println("<br><br>");
            out.println("Carrier Lookup URL=" + 
                               resp.getCarrierLookupURL());
            out.println("<br><br>");                               
            out.println("Carrier Selected=" + 
                               resp.getCarrierSelected());
            out.println("<br><br>");
            out.println("Scheduled Delivery Date=" + 
                               resp.getScheduledDeliveryDate());
            out.println("<br><br>");
            out.println("Ship Date=" + resp.getShipDate());
            out.println("<br><br>");
            out.println("Tracking Number=" + resp.getTrackingNumber());
            out.println("<br><br>");
            out.println("Status=" + resp.getStatus());
            out.println("<br><br>");
        } catch (AxisFault af) {
            af.printStackTrace(out);
        } catch (Exception e) {
            e.printStackTrace(out);
        }

    }    
}
