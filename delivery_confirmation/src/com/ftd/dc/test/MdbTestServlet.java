package com.ftd.dc.test;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jms.JMSException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MdbTestServlet extends HttpServlet {
    private Logger logger = new Logger("com.ftd.dc.test.MdbTestServlet");
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, 
                                                           IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>MdbTestServlet</title></head>");
        out.println("<body>");        
        try {
        
            String msgData = (String)request.getParameter("msgData");
            
            if(msgData != null && msgData.equalsIgnoreCase("test")) {
                msgData = "1482338";
            }
            dispatchEmailRequest(msgData);
            StringBuffer respStr = new StringBuffer("<p>");
            respStr.append("Servlet Received msgData=" + msgData);
            respStr.append("&nbsp;<br>");
            respStr.append("Email request dispatched to SDSDeliveryConfirm</p>");
            out.println(respStr.toString());
        } catch (Exception ex) {
            logger.error(ex);
            ex.printStackTrace(out);
        }
        out.println("</body></html>");
        
        out.close();
    }

 

    /**
     * send message through jms
     * @param xmlRequest - E-mail XML request
     * @return n/a
     * @throws Exception
     * @todo: set status for message token.  need from DBA
     */
    private void dispatchEmailRequest(String msgData) throws NamingException, JMSException, 
                                               Exception {
        logger.debug("Dispatch Email Request [BEGIN]");
        MessageToken token = new MessageToken();
        Context context = new InitialContext();
//        String orderDetailId = "C1000698004";
        logger.debug("Sending request to send email. msgData==" + msgData);
        token.setMessage(msgData); // is this sufficient?
        token.setStatus("EMAILDELIVERYCONFIRM");
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
        logger.debug("Dispatch Email Request [END]");
    }
}
