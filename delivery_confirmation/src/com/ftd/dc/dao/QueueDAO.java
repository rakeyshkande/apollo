package com.ftd.dc.dao;

import com.ftd.dc.vo.QueueVO;
import com.ftd.dc.constants.DCConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;


/**
 *
 * @author Jason Weiss
 */
public class QueueDAO
{
  private Connection conn;
  private Logger logger;

  // SP output checking strings
  private static final String NO = "N";
  private static final String ERROR_MESSAGE = "OUT_ERROR_MESSAGE";

  public QueueDAO(Connection c)
  {
    conn = c;
    logger = new Logger(DCConstants.LOGGER_CATEGORY_QUEUEDAO);
  }
  public void insertQueueRecord(QueueVO queueRecord) throws Exception{
    DataRequest dataRequest = new DataRequest();
    HashMap outputs = null;

    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_QUEUE_TYPE", queueRecord.getQueueType());
    inputParams.put("IN_MESSAGE_TYPE", queueRecord.getMessageType());
    java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());
    inputParams.put("IN_MESSAGE_TIMESTAMP", sqlTimeStamp);
    inputParams.put("IN_SYSTEM", queueRecord.getSystem());
    inputParams.put("IN_MERCURY_NUMBER", queueRecord.getMercuryNumber());
    inputParams.put("IN_MASTER_ORDER_NUMBER", queueRecord.getMasterOrderNumber());
    inputParams.put("IN_ORDER_GUID", queueRecord.getOrderGuid());
    inputParams.put("IN_ORDER_DETAIL_ID", queueRecord.getOrderDetailId());
    inputParams.put("IN_EXTERNAL_ORDER_NUMBER", queueRecord.getExternalOrderNumber());
    inputParams.put("IN_POINT_OF_CONTACT_ID", queueRecord.getPointOfContactId());
    inputParams.put("IN_EMAIL_ADDRESS", queueRecord.getEmailAddress());
    inputParams.put("IN_MERCURY_ID", queueRecord.getMercuryId());

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_QUEUE_RECORD");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (HashMap) dataAccessUtil.execute(dataRequest);

    /* read stored procedure output parameters to determine
     * if the procedure executed successfully */
    if (!outputs.get("OUT_STATUS").equals("Y"))
    {
        throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
    }


    }
}