package com.ftd.dc.dao;

import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.vo.ASKEventHandlerVO;
import com.ftd.dc.vo.CustomerPhoneVO;
import com.ftd.dc.vo.CustomerVO;
import com.ftd.dc.vo.OrderDetailVO;
import com.ftd.dc.vo.OrderVO;
import com.ftd.dc.vo.QueueVO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;



import org.xml.sax.SAXException;


/**
 * This class is the Data access class for inserting and
 * retrieving email queues in the Queue database schema.
 */
public class

EmailQueueDAO
{

  private Logger logger = 
    new Logger(DCConstants.LOGGER_CATEGORY_EMAILQUEUEDAO);
  private String status;
  private String message;


  //database
  private Connection dbConnection;

  public EmailQueueDAO(Connection conn)
  {
    super();
    dbConnection = conn;
  }

  /**
     * Archive the original XML to the Queue database
     * @param emailRequestXMLString - XML request
     * @return String - Point of Contact ID
     * @throws SQLException, Exception
     * @todo stored proc not ready change name of stored proc
     */
  public String storeDIRequest(OrderVO ordVO, 
                               OrderDetailVO ordDetVO,
                               CustomerVO custVO, 
                               CustomerPhoneVO custDayPhoneVO, 
                               CustomerPhoneVO custEveningPhoneVO, 
                               String custEmailAddress,
                               CustomerVO recipVO,
                               CustomerPhoneVO recipDayPhoneVO, 
                               CustomerPhoneVO recipEveningPhoneVO)
    throws

      IOException, SAXException, ParserConfigurationException, 
       SQLException, Exception
  {
    String pointOfContactID = "";

    DataRequest request = new DataRequest();

    try
    {
      //         MessageUtil messageUtil = MessageUtil.getInstance();
      //          InquiryDetailVO inqVO = emailRequest.getInquiryVO();

      // setup store procedure input parameters and parse out attributes
      HashMap inputParams = new HashMap();
      
      inputParams.put("IN_CUSTOMER_ID", ordVO!=null? new Long(ordVO.getCustomerId()).toString():null);
      inputParams.put("IN_ORDER_GUID", ordDetVO!=null? ordDetVO.getOrderGuid():null);
      inputParams.put("IN_ORDER_DETAIL_ID", ordDetVO!=null? new Long(ordDetVO.getOrderDetailId()).toString():null);
      inputParams.put("IN_MASTER_ORDER_NUMBER", ordVO!=null? ordVO.getMasterOrderNumber():null);
      inputParams.put("IN_EXTERNAL_ORDER_NUMBER", ordDetVO!=null? ordDetVO.getExternalOrderNumber():null);
      inputParams.put("IN_DELIVERY_DATE", (ordDetVO!=null && ordDetVO.getDeliveryDate()!=null)?new java.sql.Date(ordDetVO.getDeliveryDate().getTime()):null);
      inputParams.put("IN_COMPANY_ID", ordVO!=null? ordVO.getCompanyId():null);
      inputParams.put("IN_FIRST_NAME", custVO!=null? custVO.getFirstName():null);
      inputParams.put("IN_LAST_NAME", custVO!=null? custVO.getLastName():null);
      inputParams.put("IN_DAYTIME_PHONE_NUMBER", custDayPhoneVO!=null? custDayPhoneVO.getPhoneNumber():null);
      inputParams.put("IN_EVENING_PHONE_NUMBER", custEveningPhoneVO!=null? custEveningPhoneVO.getPhoneNumber():null);
      inputParams.put("IN_SENDER_EMAIL_ADDRESS", custEmailAddress);
      inputParams.put("IN_LETTER_TITLE", null);
      inputParams.put("IN_EMAIL_SUBJECT", "Novator Email Request: DI");
      inputParams.put("IN_BODY", null);
      inputParams.put("IN_COMMENT_TYPE", null);
      inputParams.put("IN_RECIPIENT_EMAIL_ADDRESS", null);
      inputParams.put("IN_NEW_RECIP_FIRST_NAME", recipVO!=null? recipVO.getFirstName():null);
      inputParams.put("IN_NEW_RECIP_LAST_NAME", recipVO!=null? recipVO.getLastName():null);
      String recipientAddress = null; 
      if (recipVO!= null && recipVO.getAddress1() != null)
        recipientAddress = recipVO.getAddress1(); 
      else if (recipVO!= null && recipVO.getAddress2() != null)
        recipientAddress += recipVO.getAddress2(); 
      inputParams.put("IN_NEW_ADDRESS", recipientAddress);
      inputParams.put("IN_NEW_CITY", recipVO!=null? recipVO.getCity():null);
      if (recipDayPhoneVO != null && recipDayPhoneVO.getPhoneNumber() != null)
        inputParams.put("IN_NEW_PHONE", recipDayPhoneVO.getPhoneNumber());
      else if (recipEveningPhoneVO != null && recipEveningPhoneVO.getPhoneNumber() != null)
        inputParams.put("IN_NEW_PHONE", recipEveningPhoneVO.getPhoneNumber());
      else
        inputParams.put("IN_NEW_PHONE", null);
      inputParams.put("IN_NEW_ZIP_CODE", recipVO!=null? recipVO.getZipCode():null);
      inputParams.put("IN_NEW_STATE", recipVO!=null? recipVO.getState():null);
      inputParams.put("IN_NEW_COUNTRY", recipVO!=null? recipVO.getCountry():null);
      inputParams.put("IN_NEW_INSTITUTION", recipVO!=null? recipVO.getBusinessName():null);
      inputParams.put("IN_NEW_DELIVERY_DATE", (ordDetVO!=null && ordDetVO.getDeliveryDate()!=null)?new java.sql.Date(ordDetVO.getDeliveryDate().getTime()):null);
      inputParams.put("IN_NEW_CARD_MESSAGE", ordDetVO!=null? ordDetVO.getCardMessage():null);
      inputParams.put("IN_NEW_CARD_SIGNATURE", ordDetVO!=null? ordDetVO.getCardSignature():null);
      inputParams.put("IN_EMAIL_HEADER", null);
      inputParams.put("IN_NEW_PRODUCT", ordDetVO!=null? ordDetVO.getProductId():null);
      inputParams.put("IN_CHANGE_CODES", null);
      inputParams.put("IN_SENT_RECEIVED_INDICATOR", "I");
      inputParams.put("IN_SEND_FLAG", null);
      inputParams.put("IN_PRINT_FLAG", null);
      inputParams.put("IN_POINT_OF_CONTACT_TYPE", "Email");
      inputParams.put("IN_COMMENT_TEXT", null);
      inputParams.put("IN_CREATED_BY", "SYS");

      // build DataRequest object
      request.setConnection(dbConnection);
      request.reset();
      request.setInputParams(inputParams);
      request.setStatementID("QUEUE_INSERT_POINT_OF_CONTACT");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();
      Map outputs = (Map) dau.execute(request);
      // read store prodcedure output parameters to determine
      // if the procedure executed successfully
      status = (String) outputs.get("OUT_STATUS");
      if (status.equals("N"))
      {
        message = (String) outputs.get("OUT_MESSAGE");
        throw new Exception(message);
      }
      else
      {
        pointOfContactID = outputs.get("OUT_SEQUENCE_NUMBER").toString();

      }

    }
    finally
    {
      // Since this DML involved CLOBs, cleanup any temporarily generated CLOBs.
      request.reset();
    }

    return pointOfContactID;
  }


  /**
     * Look up queues in database based on given order external number.
     * Iterate through results to find the highest priority queue for the order
     * Return QueueVO with highest priority if there is one, otherwise
     * return null.
     * @param orderDetailID
     * @return QueueVO
     * @throws SQLException, Exception
     * @todo need stored procedure
     */
  public QueueVO lookupHighestPriorityExistingQueue(String orderDetailID)
    throws IOException, SAXException, SQLException, 
           ParserConfigurationException , Exception
  {
    DataRequest request = new DataRequest();
    QueueVO queueVO = null;
    CachedResultSet queueRS = null;

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
    inputParams.put("IN_QUEUE_INDICATOR", "Email");

    /* build DataRequest object */
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("GET_ORDR_HIGHEST_PRIORITY_QUE");

    /* execute the store prodcedure */
    DataAccessUtil dau = DataAccessUtil.getInstance();
    queueRS = (CachedResultSet) dau.execute(request);
    while (queueRS.next()) // get queue config context parms
    {
      /* Populate QueueVO with stored procedure results  */
      queueVO = new QueueVO();
      queueVO.setMercuryNumber(queueRS.getString("mercury_number"));
      queueVO.setMessageId(queueRS.getString("message_id"));
      queueVO.setMessageType(queueRS.getString("message_type"));
      queueVO.setPointOfContactId(queueRS.getString("point_of_contact_id"));
      queueVO.setPriority(queueRS.getInt("priority"));
      queueVO.setQueueType(queueRS.getString("queue_type"));
      queueVO.setSystem(queueRS.getString("system"));
      String masterOrderNumber = queueRS.getString("master_order_number");
      if (masterOrderNumber != null)
      {
        queueVO.setAttached("Y");
      }
      else
      {
        queueVO.setAttached("N");
      }

    }

    return queueVO;

  }

  /**
     * remove queue
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
  public void removeQueue(String messageID)
    throws Exception
  {
    DataRequest request = new DataRequest();
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_MESSAGE_ID", messageID);
    inputParams.put("IN_CSR_ID", "SYS");

    // build DataRequest object
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("DELETE_QUEUE_RECORD");

    // get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);
    status = (String) outputs.get("OUT_STATUS");
    if (status.equalsIgnoreCase("N"))
    {
      message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
     * Lock order
     * @param OrderVO
     * @return n/a
     * @throws SQLException, Exception
     * @todo stored procedure not completed
     */
  public boolean isOrderLocked(OrderDetailVO ordDetVO, String sessionId, 
                               String csr_id)
    throws Exception
  {
    DataRequest request = new DataRequest();
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ENTITY_TYPE", "MODIFY_ORDER");
    inputParams.put("IN_ENTITY_ID", (new Long(ordDetVO.getOrderDetailId())).toString());
    inputParams.put("IN_SESSION_ID", sessionId);
    inputParams.put("IN_CSR_ID", csr_id);
    inputParams.put("IN_ORDER_LEVEL", null);

    // build DataRequest object
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("CHECK_CSR_LOCKED_ENTITIES");

    // get data
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);
    String locked = (String) outputs.get("OUT_LOCKED_IND");
    String lockedCsr = (String) outputs.get("OUT_LOCKED_CSR_ID");
    boolean isLocked = (locked != null && locked.trim().equalsIgnoreCase("Y"));
    if (isLocked)
    {
      logger.warn("Order " + ordDetVO + "is locked by " + lockedCsr);
    }
    return isLocked;

  }


  /**
     * Store email queue value object to the Queue database.
     * @param emailQueueVO - QueueVO instance
     * @return n/a
     * @throws SQLException, Exception
     * @todo find out what to pass into procedure
     */
  public void insertComments(String comments, OrderDetailVO ordDetail, 
                             CustomerVO custVO, String csr_id)
    throws Exception
  {
    DataRequest request = new DataRequest();
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();

    inputParams.put("IN_CUSTOMER_ID", new Long(custVO.getCustomerId()).toString());
    inputParams.put("IN_ORDER_GUID", ordDetail.getOrderGuid());
    inputParams.put("IN_ORDER_DETAIL_ID", new Long(ordDetail.getOrderDetailId()).toString());
    inputParams.put("IN_COMMENT_ORIGIN", "");
    inputParams.put("IN_REASON", "");
    inputParams.put("IN_DNIS_ID", null);
    inputParams.put("IN_COMMENT_TEXT", comments);
    inputParams.put("IN_COMMENT_TYPE", "Order");
    inputParams.put("IN_CSR_ID", csr_id);

    /* build DataRequest object */
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("INSERT_COMMENTS");

    /* execute the store prodcedure and retrieve output*/
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);

    /* read store prodcedure output parameters to determine if the procedure executed successfully */
    status = (String) outputs.get("OUT_STATUS");
    if (status.equalsIgnoreCase("N"))
    {
      message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }


  /**
     */
  public ASKMessageTO getMercuryASKMessage(String orderDetailID)
    throws IOException, SAXException, SQLException, 
           ParserConfigurationException , Exception
  {
    DataRequest request = new DataRequest();
    ASKMessageTO askTO = null;
    CachedResultSet messageRS = null;
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailID);
    inputParams.put("IN_MESSAGE_TYPE", "Mercury");
    inputParams.put("IN_COMP_ORDER", "N"); //exclude COMP order.
    /* build DataRequest object */
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("GET_MESG_DETAIL_FROM_LAST_FTD");

    /* execute the store prodcedure */
    DataAccessUtil dau = DataAccessUtil.getInstance();
    messageRS = (CachedResultSet) dau.execute(request);
    if (messageRS.next()) // get queue config context parms
    {
      askTO = new ASKMessageTO();
      askTO.setMercuryId(messageRS.getString("ftd_message_id"));

      askTO.setFillingFlorist(messageRS.getString("filling_florist"));
      askTO.setSendingFlorist(messageRS.getString("sending_florist"));
    }

    return askTO;
  }


  /**
     * log a message transaction for an ASK message sent
     * @param n/a
     * @return n/a
     * @throws Exception
     */
  public void storeMessageTran(ASKEventHandlerVO askVO)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, Exception
  {
    DataRequest request = new DataRequest();

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", askVO.getOrderDetailID());
    inputParams.put("IN_FILLING_FLORIST_ID", askVO.getFillingFloristId());
    inputParams.put("IN_MESSAGE_TIMESTAMP", new Timestamp(System.currentTimeMillis()));
    inputParams.put("IN_SYSTEM_TYPE", askVO.getMessageType());
    inputParams.put("IN_RESPONSE_FLAG", DCConstants.NO);
    inputParams.put("IN_SENDER_EMAIL_ADDRESS", askVO.getSendEmail());
    inputParams.put("IN_POINT_OF_CONTACT_ID", askVO.getPointOfContactId());
    inputParams.put("IN_QUEUE_TYPE", askVO.getQueueType());
    inputParams.put("IN_MERCURY_ID", askVO.getMercuryMessageId());
    /* build DataRequest object */
    request.setConnection(dbConnection);
    request.reset();
    request.setInputParams(inputParams);
    request.setStatementID("INSERT_ASK_MESSAGE_EVENT_LOG");

    /* execute the store prodcedure and retrieve output*/
    DataAccessUtil dau = DataAccessUtil.getInstance();
    Map outputs = (Map) dau.execute(request);
    /* read store prodcedure output parameters to determine
           * if the procedure executed successfully */
    String status = (String) outputs.get("OUT_STATUS");
    if (status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }


  }


  public void setDbConnection(Connection dbConnection)
  {
    this.dbConnection = dbConnection;
  }


  public Connection getDbConnection()
  {
    return dbConnection;
  }

    /**
     * gets the value of the delivery confirmation status
     *
     * @param orderDetailId
     * @return String with the delivery confimraiton status
     * @throws java.lang.Exception
     *
     */

    public String getDeliveryConfirmationStatus(long orderDetailId) throws Exception
    {
      logger.debug("getDeliveryConfirmationStatus for orderDetail " + orderDetailId);
      
      String dconStatus = null;
      DataRequest dataRequest = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

      /* build DataRequest object */
      dataRequest.setConnection(this.dbConnection);
      dataRequest.setStatementID("GET_DCON_STATUS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      dconStatus = (String)dataAccessUtil.execute(dataRequest);
      
      return dconStatus;
    }

    /**
     * Sets the value of the delivery confirmation status
     *
     * @param orderDetailId
     * @param message type
     * @param comp order
     * @return CachedResultSet
     * @throws java.lang.Exception
     *
     */

    public void updateDeliveryConfirmationStatus(long orderDetailId, String newStatus, String updatedBy) throws Exception 
    {
      logger.debug("updateDeliveryConfirmationStatus");
      
      Map output = null;
      DataRequest dataRequest = new DataRequest();

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
      inputParams.put("IN_STATUS", newStatus);
      inputParams.put("IN_CSR_ID", updatedBy);

      /* build DataRequest object */
      dataRequest.setConnection(this.dbConnection);
      dataRequest.setStatementID("UPDATE_DCON_STATUS");
      dataRequest.setInputParams(inputParams);

      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      output = (Map)dataAccessUtil.execute(dataRequest);

      /* read store prodcedure output parameters to determine
      * if the procedure executed successfully */
      String status = (String)output.get("OUT_STATUS");
      if (status != null && status.equalsIgnoreCase("N")) {
          String message = (String)output.get("OUT_MESSAGE");
          throw new Exception(message);
      }     
    }
    
}
