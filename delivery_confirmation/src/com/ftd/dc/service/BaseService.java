package com.ftd.dc.service;

import com.ftd.dc.cache.handler.EmailQueueConfigHandler;
import com.ftd.dc.cache.handler.EmailQueuePriorityCacheHandler;
import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.dao.EmailQueueDAO;
import com.ftd.dc.dao.OrderDAO;
import com.ftd.dc.dao.QueueDAO;
import com.ftd.dc.framework.util.CommonUtils;
import com.ftd.dc.util.MessagingServiceLocator;
import com.ftd.dc.vo.ASKEventHandlerVO;
import com.ftd.dc.vo.CustomerPhoneVO;
import com.ftd.dc.vo.CustomerVO;
import com.ftd.dc.vo.OrderDetailVO;
import com.ftd.dc.vo.OrderVO;
import com.ftd.dc.vo.QueueVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;


public class BaseService
{
  protected Connection connection;
  protected OrderDAO orderDAO = null;
  protected EmailQueueDAO emailQDAO = null;
  private Logger logger;

  public BaseService()
  {
    logger = new Logger(DCConstants.LOGGER_BASE);
  }

  public void process()
    throws Exception
  {
    this.connection = CommonUtils.getConnection();
    this.orderDAO = new OrderDAO(this.connection);
    this.emailQDAO = new EmailQueueDAO(this.connection);
  }


  /**
   *
   * This method will call the DAO to store the point of contact, and will return the point
   * of contact id back to the calling method.
   *
   */
  public String persistPointOfContact(String custEmailAddress, 
                                      OrderVO ordVO, 
                                      OrderDetailVO ordDetVO, 
                                      CustomerVO custVO, 
                                      CustomerPhoneVO custDayPhoneVO, 
                                      CustomerPhoneVO custEveningPhoneVO, 
                                      CustomerVO recipVO, 
                                      CustomerPhoneVO recipDayPhoneVO, 
                                      CustomerPhoneVO recipEveningPhoneVO)
    throws Exception
  {
    String pointOfContactID = null;
    pointOfContactID = 
        emailQDAO.storeDIRequest(ordVO, ordDetVO, custVO, custDayPhoneVO, 
                                 custEveningPhoneVO, custEmailAddress, 
                                 recipVO, recipDayPhoneVO, 
                                 recipEveningPhoneVO);
    return pointOfContactID;
  }


  /**
   *
   * This method will call the DAO to store the point of contact, and will return the point
   * of contact id back to the calling method.
   *
   */
  public boolean lookupUntaggedExistingQueue(OrderDetailVO ordDetVO)
    throws Exception
  {
    boolean continueProcess = true;

    QueueVO highestQueue = 
      (QueueVO) lookupQueueVO(new Long(ordDetVO.getOrderDetailId()).toString());

    if (highestQueue != null && highestQueue.getPriority() != 0)
    {
      int emailPriority = retrievePriority("DI");
      int highPriority = highestQueue.getPriority();
      //lower the priority number, higher the priority.
      if (emailPriority < highPriority)
      {
        if (!highestQueue.getTagged().equalsIgnoreCase("Y"))
        {
          removeQueue(highestQueue);
        }
      }
      else
      {
        //email request processing used to send an auto response here
        //however, in this process, since the communication is synchronous, I don't think that
        //we need to send an auto response.  
        continueProcess = false;
      }
    }
    else
    {
      logger.debug("no highest queue found");
    }

    return continueProcess;
  }

  /**
     * Look up Queue Database for queues with the same order external number.
     *
     * @param orderExternalNumber -
     *            String
     * @return QueueVO
     * @throws Exception
     */
  public QueueVO lookupQueueVO(String orderDetailId)
    throws Exception
  {
    QueueVO lookupQueue = new QueueVO();
    lookupQueue = 
        emailQDAO.lookupHighestPriorityExistingQueue(orderDetailId);
    return lookupQueue;
  }


  /**
     * Call EmailQueueDAO to store Email Queue Value Object in Queue Database.
     *
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws Exception
     */
  public int retrievePriority(String queueType)
    throws Exception
  {
    int requestPriority = 0;
    EmailQueuePriorityCacheHandler emailQueuePriorityHandler = 
      (EmailQueuePriorityCacheHandler) CacheManager.getInstance().getHandler(DCConstants.EMAIL_QUEUE_PRIORITY_CACHE_HANDLER);
    requestPriority = 
        emailQueuePriorityHandler.getQueuePriority(queueType);
    return requestPriority;
  }


  /**
     * Calls the email queue DAO to remove a queue record from the database
     *
     * @param emailQueueVO -
     *            QueueVO
     * @return n/a
     * @throws IOException,
     *             SAXException, SQLException, ParserConfigurationException,
     *              Exception
     */
  public void removeQueue(QueueVO emailQueueVO)
    throws Exception
  {
    /* remove the queue from the database */
    emailQDAO.removeQueue(emailQueueVO.getMessageId());

  }


  /**
     * Insert Comments for a queue order
     *
     * @param comments - String
     * @param emailQueueVO - QueueVO
     * @return n/a
     * @throws Exception
     */
  public void insertComments(String comments, String csrId, 
                             OrderDetailVO ordDetVO, CustomerVO custVO)
    throws Exception
  {
    emailQDAO.insertComments(comments, ordDetVO, custVO, csrId);
  }


  /**
     * @param emailVO
     * @param emailDAO
     * @param ordDetail
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws SQLException
     * @throws Exception
     * @throws IOException
     * @throws SAXException
     * @throws TransformerException
     * void
     */
  public void persistQueue(QueueVO qVO)
    throws Exception
  {
    //create a new QeueuVO
    QueueDAO qDAO = new QueueDAO(this.connection);
    qDAO.insertQueueRecord(qVO);
  }

  /**
   * Returns the difference in days between the startDate and endDate.
   *
   * @param startDate
   * @param endDate
   * @return The difference in days between the startDate and endDate
   * @throws IllegalArgumentException if either startDate or endDate are null
   */
  public int getDiffInDays(Date startDate, Date endDate)
  {

    // null check inputs
    if (startDate == null || endDate == null)
    {
      throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + 
                                         startDate + "', endDate='" + 
                                         endDate + "' were null.");
    }

    long diffInMillis = endDate.getTime() - startDate.getTime();

    int diffInDays = (int) (diffInMillis / DateUtils.MILLIS_PER_DAY);

    return diffInDays;
  }


  /**
     * Retrieve message related data, such as type, message detail type,
     * messageOrderNumber and orderDetailId from HTTP request and create an ASK
     * Message TO. In addition, insert any information retrieved from the
     * request into the parameters HashMap
     *
     * @param request -
     *            HttpServletRequest
     * @param paramters -
     *            HashMap
     * @return ASKMessageTO
     * @throws TransformerException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XSLException
     */
  public ASKMessageTO createASKMessageTO(String emailType, String csrId, String orderDetailId)
    throws Exception
  {
    if (logger.isDebugEnabled())
    {
      logger.debug("Entering createASKMessageTO");
    }

    ASKMessageTO askTO = null;
    askTO = emailQDAO.getMercuryASKMessage(orderDetailId);
    if (askTO != null)
    {
      if (emailType.equalsIgnoreCase("DI"))
      {
        askTO.setComments("Please confirm delivery.  Thank you.");
      }

      askTO.setOperator(csrId);
      askTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
      askTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
    }

    return askTO;
  }


  /**
     * Send ASK messages via Mercury Message API.
     *
     * @param request -
     *            HttpServletRequest
     * @return n/a
     * @throws Exception????
     */
  public ResultTO sendMercuryASKMessage(ASKMessageTO askMessage)
    throws Exception
  {
    logger.debug("Sending ASK Message");
    return MessagingServiceLocator.getInstance().getMercuryAPI().sendASKMessage(askMessage, connection);
  }


  /**
     * Determine whether or not the date falls during a holiday period
     *
     * @param n/a
     * @return boolean
     * @throws Exception
     */
  public boolean isHolidayTime(Date requestDate)
    throws Exception
  {

    boolean holidayTime = false;
    if (requestDate != null)
    {
      try
      {
        EmailQueueConfigHandler emailQueueConfig = 
          (EmailQueueConfigHandler) CacheManager.getInstance().getHandler(DCConstants.EMAIL_QUEUE_CONFIG_HANDLER);

        java.util.Date startHoliday = emailQueueConfig.getStartHoliday();
        Date endHoliday = emailQueueConfig.getEndHoliday();
        if (requestDate.compareTo(startHoliday) >= 0 && 
            requestDate.compareTo(endHoliday) <= 0)
        {
          holidayTime = true;

        }
      }
      finally
      {
        if (logger.isDebugEnabled())
        {
          logger.debug("Exiting isHolidayTime");
        }
      }
    }
    return holidayTime;
  }

  public void logMessageTransaction(ASKEventHandlerVO askEvent)
    throws Exception
  {
    emailQDAO.storeMessageTran(askEvent);
  }


  public void processEnd()
    throws Exception
  {
    this.connection.close();
  }


}
