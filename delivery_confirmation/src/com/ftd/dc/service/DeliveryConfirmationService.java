package com.ftd.dc.service;

import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.dao.VenusDAO;
import com.ftd.dc.dao.EmailQueueDAO;
import com.ftd.dc.to.DeliveryInfoTO;
import com.ftd.dc.vo.ASKEventHandlerVO;
import com.ftd.dc.vo.CustomerPhoneVO;
import com.ftd.dc.vo.CustomerVO;
import com.ftd.dc.vo.OrderDetailVO;
import com.ftd.dc.vo.OrderTrackingVO;
import com.ftd.dc.vo.OrderVO;
import com.ftd.dc.vo.QueueVO;
import com.ftd.dc.vo.VenusMessageVO;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.osp.utilities.DeliveryInquiryUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.GeneralConstants;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axis.types.URI;
import org.apache.commons.lang.StringUtils;


public class DeliveryConfirmationService extends BaseService
{
  private final String SYSTEM_CSR = "SYS";
  private Logger logger;
  private String comments = null;
  private OrderDetailVO ordDetVO = null;
  private OrderVO ordVO = null;     
  private CustomerVO custVO = null;     
  private CustomerPhoneVO custDayPhoneVO = null;     
  private CustomerPhoneVO custEveningPhoneVO = null;     
  private CustomerVO recipVO = null;     
  private CustomerPhoneVO recipDayPhoneVO = null;     
  private CustomerPhoneVO recipEveningPhoneVO = null;     
  private QueueVO qVO = null;    
  
  private StringBuffer debugLogBuffer;

  public DeliveryConfirmationService()
  {
    logger =
        new Logger(DCConstants.LOGGER_CATEGORY_DELIVERYCONFIRMATIONSERVICE);
  }

  public DeliveryInfoTO getDeliveryConfirmation(String orderNumber,
                                                String emailAddress)
  {

    logger.info(" Delivery Confirmation Service [BEGIN]");
    logger.info(" Requested delivery confirmation for order=" + orderNumber + " email address=" + emailAddress);
    clearDebugLogs();
    
    DeliveryInfoTO deliveryInfoTO = new DeliveryInfoTO();
    try
    {
        super.process();
        if (orderNumber == null || orderNumber.equalsIgnoreCase("") ||
            emailAddress == null || emailAddress.equalsIgnoreCase(""))
        {
          debugLog(" Order Number or Email Address empty or null. NoOrder status");
          deliveryInfoTO.setStatus(DCConstants.DC_NO_ORDER);
        }
        else
        {
          //obtain order detail id from external order number
          debugLog(" Locating order " + orderNumber);
        
          Map orderNumberMap = super.orderDAO.findOrderNumber(orderNumber);
          if (orderNumberMap == null)
          {
            debugLog(" Unable to locate order number=" + orderNumber);
            deliveryInfoTO.setStatus(DCConstants.DC_NO_ORDER);
          }
          else if (orderNumberMap.get("OUT_ORDER_DETAIL_ID") == null)
          {
            debugLog(" Unable to locate order number=" + orderNumber +
                         ". SP returned NULL Order Detail ID.");
            deliveryInfoTO.setStatus(DCConstants.DC_NO_ORDER);
          }
          else
          {
            String orderDetailId =
              orderNumberMap.get("OUT_ORDER_DETAIL_ID").toString();
            debugLog(" Found Order. Order Detail Id=" + orderDetailId);
            debugLog(" Locating matching emailaddress=" + emailAddress);
            if (!super.orderDAO.isEmailAddressAssociated(orderDetailId,
                                                   emailAddress))
            {
              debugLog("Matching email address not found.");
              // set status no match
              deliveryInfoTO.setStatus(DCConstants.DC_NO_MATCH);
            }
            else
            {
        
              debugLog("Matching email address found.");
              
              debugLog("Check order disposition code to see if order is in scrub");
              String orderDispCode = null;
              if( orderNumberMap.get("OUT_ORDER_DISP_CODE") != null )
              {
                orderDispCode = orderNumberMap.get("OUT_ORDER_DISP_CODE").toString();
              }
                
              // check to see if order is still in scrub. Order is in scrub if disp code is In-Scrub  
              if(orderDispCode != null &&  orderDispCode.equalsIgnoreCase(DCConstants.ORDER_IN_SCRUB_DISP_CODE)
                  || orderDispCode.equalsIgnoreCase(DCConstants.ORDER_PENDING_DISP_CODE)
                  || orderDispCode.equalsIgnoreCase(DCConstants.ORDER_REMOVED_DISP_CODE)) {
                  logger.info(" Order is in Scrub");
                  debugLog(" Order is in scrub. Send to Delivery Inquiry queue");
                  // set status to scrub
                   deliveryInfoTO.setStatus(DCConstants.DC_SCRUB);
                   
                   // send it to queue
                   // place the order in delivery inquiry queue
                  sendToDeliveryInquiryQueue(orderNumberMap, emailAddress);
              }
              else {
              
                  //get the associated venus order message
                  VenusMessageVO venusMessageVO = getAssociatedOrder(orderDetailId);
        
                  //check if the venus order was found
                  if (venusMessageVO == null)
                  {
                    logger.info("Florist Order. Sending order to Delivery Inquiry queue");
                    // set status to queued
                    deliveryInfoTO.setStatus(DCConstants.DC_QUEUED);
        
                    // place the order in delivery inquiry queue
                      sendToDeliveryInquiryQueue(orderNumberMap, emailAddress);
                  }
                  else
                  {   
                      String sdsStatus = venusMessageVO.getSdsStatus();
                      String finalCarrier = venusMessageVO.getFinalCarrier();
                      Date scheduledDeliveryDate = venusMessageVO.getDeliveryDate();
                      Date shipDate = venusMessageVO.getShipDate();
                      
                      if( sdsStatus !=null && !sdsStatus.equalsIgnoreCase(DCConstants.SDS_CANCELLED) && 
                            !sdsStatus.equalsIgnoreCase(DCConstants.SDS_REJECTED)
                          && ( finalCarrier == null || scheduledDeliveryDate == null || shipDate == null
                          || finalCarrier.length() ==0 ) ) {
                          
                              logger.info("Venus message found but missing final carrier or scheduled delivery date or ship date");
                              debugLog(" Final Carrier = " + finalCarrier);
                              debugLog(" Scheduled Delivery Date = " + scheduledDeliveryDate);
                              debugLog(" Ship Date = " + shipDate);
                              debugLog("Sending the order to Delivery Inquiry queue");
                              // set status to queued
                              deliveryInfoTO.setStatus(DCConstants.DC_VENDOR_QUEUED);
                      
                              // place the order in delivery inquiry queue
                              sendToDeliveryInquiryQueue(orderNumberMap, emailAddress);
                      } else {
                          
                          deliveryInfoTO.setCarrierSelected(finalCarrier);
                          deliveryInfoTO.setScheduledDeliveryDate(scheduledDeliveryDate);
                          deliveryInfoTO.setShipDate(shipDate); 
                          
                          if(sdsStatus == null || sdsStatus.equalsIgnoreCase(DCConstants.SDS_NEW) || sdsStatus.equalsIgnoreCase(DCConstants.SDS_AVAILABLE)) 
                          {
                              debugLog("SDS Status=" + sdsStatus + ". New Order");
                              // set status to try again. too early to get status.
                              deliveryInfoTO.setStatus(DCConstants.DC_NEW);                             
                          } else if(sdsStatus.equalsIgnoreCase(DCConstants.SDS_CANCELLED) || sdsStatus.equalsIgnoreCase(DCConstants.SDS_REJECTED) ) 
                          {
                              debugLog("SDS Status Cancelled/Rejected. Send Cancelled/rejected response.");
                              // set status to rejected.
                              deliveryInfoTO.setStatus(DCConstants.DC_CANCELREJECT);  
        
                              // place the order in delivery inquiry queue
                              sendToDeliveryInquiryQueue(orderNumberMap, emailAddress);                           
                          }
                          else if(sdsStatus.equalsIgnoreCase(DCConstants.SDS_SHIPPED) || sdsStatus.equalsIgnoreCase(DCConstants.SDS_PRINTED) ) 
                          {
        
                              // retrieve the tracking details
                              debugLog("Retrieving tracking information");
                              OrderTrackingVO trackingVO = super.orderDAO.getTrackingInfo(Long.parseLong(orderDetailId));
                              if(trackingVO != null) {
                                 // set status to shipped
                                 deliveryInfoTO.setStatus(DCConstants.DC_SHIPPED);
                                 debugLog("Tracking Number="+trackingVO.getTrackingNumber() + " Carrier URL="+trackingVO.getTrackingURL());
                                 try {
                                     deliveryInfoTO.setCarrierLookupURL(new URI(trackingVO.getTrackingURL()));
                                     deliveryInfoTO.setTrackingNumber(trackingVO.getTrackingNumber());
                                      // check the delivery scan to determine the status to be shipped or delivered
                                       if(sdsStatus.equalsIgnoreCase(DCConstants.SDS_SHIPPED) && venusMessageVO.getDeliveryScan() != null) {
                                          // set status delivered
                                          deliveryInfoTO.setStatus(DCConstants.DC_DELIVERED);
                                          
                                          // retrieve the actual delivery date time
                                          SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
                                          debugLog("Delivery Information available. Actual Delivery : " + dateFormat.format(venusMessageVO.getDeliveryScan()));
                                          deliveryInfoTO.setActualDeliveryDateTime(dateFormat.format(venusMessageVO.getDeliveryScan()));
                                      }
                                  } catch (URI.MalformedURIException excep) {
                                      logger.warn("Caught MalformedURIException. URI="+trackingVO.getTrackingURL());
                                      debugLog("Caught MalformedURIException. URI="+trackingVO.getTrackingURL());
                                      debugLog("Sending the order to Delivery Inquiry queue");
                                      // set status to queued
                                      deliveryInfoTO.setStatus(DCConstants.DC_VENDOR_QUEUED);
                                      
                                      // place the order in delivery inquiry queue
                                      sendToDeliveryInquiryQueue(orderNumberMap, emailAddress);
                                  }
                              } else {
                                  logger.error("Tracking information not available. Should never happen. New Order.");
                                  logger.error(getDebugLogs());
                                  // set status to try again. too early to get status.
                                  deliveryInfoTO.setStatus(DCConstants.DC_NEW);
                              }
                          } else {
                              logger.error("Invalid SDS Status.");
                              logger.error(getDebugLogs());
                              // set status to UNKNOWN. 
                              // This should never happen. 
                              // SDS Status must be New, Available, Printed or Shipped
                              deliveryInfoTO.setStatus(DCConstants.DC_UNKNOWN);
                          }
                      }
                  }
              }
            }
          }
        }
    }
    catch (Exception e)
    {
      logger.error(getDebugLogs());
      logger.error(e);
    }
    finally
    {
      try
      {
        super.processEnd();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
    logger.info(" Delivery confirmation response: " + deliveryInfoTO.printInfo());
    logger.info(" Delivery Confirmation Service [END]");
    return deliveryInfoTO;
  }
  
  
  /*
   * write all the debug logs to string buffer. Used to reduce logs.
   * this buffer gets displayed only in case of errors.
   */
  private void debugLog(String buffer) {
      if(debugLogBuffer == null) {
          debugLogBuffer = new StringBuffer("");
      }
      
      debugLogBuffer.append(buffer);
      debugLogBuffer.append("\n");
  }
  
    /*
     * write all the debug logs to string buffer. Used to reduce logs.
     * this buffer gets displayed only in case of errors.
     */
  private String getDebugLogs() {
      if(debugLogBuffer == null) {
          debugLogBuffer = new StringBuffer("");
      }  
      return debugLogBuffer.toString();
  }
  
  
  
  /*
   * clear log buffer
   */
   private void clearDebugLogs() {
    debugLogBuffer = new StringBuffer("");
   }



  /*
     * Get the order message assocated with the past in venus messageVO
     */

  private VenusMessageVO getAssociatedOrder(String orderDetailId)
    throws Exception
  {
    VenusDAO venusDAO = new VenusDAO(super.connection);

    VenusMessageVO foundOrder = null;

    debugLog(" Locating venus FTD message for " + orderDetailId);

    //Retrieve venus FTD order message related to this message
    // Check should be made for FTD messages without a CAN or REJ
    List venusList = venusDAO.getVenusFTDMessageByDetailId(orderDetailId);

    if (venusList == null || venusList.size() == 0)
    {
      //return null
    }
    else
    {
      if (venusList.size() > 1)
      {
        debugLog("Multiple FTDs were found for the for order detail id=" +
                    orderDetailId +
                    ". Latest venus FTD Message will be used.");

      }
      foundOrder = (VenusMessageVO) venusList.get(0);
      debugLog(" Found Venus FTD Message. Reference Number=" +
                   foundOrder.getReferenceNumber());
      debugLog(" Venus SDS Status=" + foundOrder.getSdsStatus());

    }
    return foundOrder;
  }

  private void sendToDeliveryInquiryQueue(Map orderNumberMap, 
                                          String emailAddress)
    throws Exception
  {
    String orderDetailId = orderNumberMap.get("OUT_ORDER_DETAIL_ID").toString();
    String orderGuid = orderNumberMap.get("OUT_ORDER_GUID").toString();
    this.ordVO = new OrderVO(); 
    this.ordDetVO = new OrderDetailVO(); 
    this.custVO = new CustomerVO(); 
    this.custDayPhoneVO = new CustomerPhoneVO(); 
    this.custEveningPhoneVO = new CustomerPhoneVO(); 
    this.recipVO = new CustomerVO(); 
    this.recipDayPhoneVO = new CustomerPhoneVO(); 
    this.recipEveningPhoneVO = new CustomerPhoneVO(); 
    this.qVO = new QueueVO(); 
    String attemptedFTD = null;
    String cancelSent = null;
    String ftdStatus = null;
    String hasLiveFTD = null;
    String rejectSent = null;

    createVO(orderDetailId, orderGuid); 

    String pointOfContactId = super.persistPointOfContact(emailAddress, this.ordVO, this.ordDetVO, 
                                   this.custVO, this.custDayPhoneVO, this.custEveningPhoneVO, 
                                   this.recipVO, this.recipDayPhoneVO, this.recipEveningPhoneVO);

    storeDIQueue(emailAddress, pointOfContactId);

    boolean continueProcess = super.lookupUntaggedExistingQueue(this.ordDetVO);

    if (continueProcess)
    {
      boolean orderLocked = isOrderLocked();
      if (orderLocked)
      {
        super.persistQueue(this.qVO);
        this.comments = "Incoming delivery inquiry email received.  Email has been queued.";
        super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, custVO); 
        continueProcess = false;
      }
    }

    if (continueProcess)
    {
      CachedResultSet mercuryOrderMessageStatusInfo = 
            super.orderDAO.getMercuryOrderMessageStatus(orderDetailId, "Mercury","N");
      //populate mercury order message status from the mercury status query results
      if(mercuryOrderMessageStatusInfo.next())
      {
        if(mercuryOrderMessageStatusInfo.getString("reject_sent") != null)
          rejectSent = mercuryOrderMessageStatusInfo.getString("reject_sent").trim();
        if(mercuryOrderMessageStatusInfo.getString("cancel_sent") != null)
          cancelSent = mercuryOrderMessageStatusInfo.getString("cancel_sent").trim();
        if(mercuryOrderMessageStatusInfo.getString("attempted_ftd") != null)
          attemptedFTD = mercuryOrderMessageStatusInfo.getString("attempted_ftd").trim();
        if(mercuryOrderMessageStatusInfo.getString("has_live_ftd") != null)
          hasLiveFTD = mercuryOrderMessageStatusInfo.getString("has_live_ftd").trim();
        if(mercuryOrderMessageStatusInfo.getString("ftd_status") != null)
          ftdStatus = mercuryOrderMessageStatusInfo.getString("ftd_status").trim();
      }

      String isOrderFTDM = super.orderDAO.isOrderFTDM(new Long(this.ordDetVO.getOrderDetailId()).toString());


      if (StringUtils.isNotEmpty(hasLiveFTD) && hasLiveFTD.equalsIgnoreCase("Y"))
      {
        if (StringUtils.isNotEmpty(isOrderFTDM) && isOrderFTDM.equalsIgnoreCase("Y"))
        {
          //* FTDM order 
          this.comments = "Incoming Delivery Inquiry email received for FTDM order.  Email has been queued.";
          super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, custVO);
          super.persistQueue(this.qVO);
        }
        else
        {
          if (isPastDeliveryDate())
          {
            processDeliveryInquiryPastDeliveryDate(pointOfContactId, emailAddress, orderDetailId);
          }
          else
          {
            // order not past delivery date 
            this.comments = "Incoming Delivery Inquiry response email request received.  Delivery Inquiry Email sent. Email not queued.";
            super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, custVO); 
          }
        }
      }
      else
      {
        //* FTD order 
        this.comments = "Incoming delivery inquiry email received.  Email has been queued.";
        super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, custVO);
        super.persistQueue(this.qVO);
      }


    }
/*****************************************************
 *****************************************************
 * my changes end
 *****************************************************
 ******************************************************/

  }


/*****************************************************
 * createVO
 ******************************************************/
  private void createVO(String orderDetailId, String orderGuid)
    throws Exception
  {
    retrieveOrderDetailInformation(orderDetailId);
    retrieveOrderInformation(orderGuid);
    retrieveCustomerInformation();
    retrieveCustomerPhoneInformation();
    retrieveRecipientInformation();
    retrieveRecipientPhoneInformation();
  }


/*****************************************************
 * retrieveOrderInformation
 ******************************************************/
  private void retrieveOrderInformation(String orderGuid)
    throws Exception
  {
    //create an order vo
    this.ordVO = super.orderDAO.getOrder(orderGuid);
  }

/*****************************************************
 * retrieveOrderDetailInformation
 ******************************************************/
  private void retrieveOrderDetailInformation(String orderDetailId)
    throws Exception
  {
    //create an order detail vo
    this.ordDetVO = super.orderDAO.getOrderDetail(orderDetailId);
  }

/*****************************************************
 * retrieveCustomerInformation
 ******************************************************/
  private void retrieveCustomerInformation()
    throws Exception
  {
    //create a customer vo
    this.custVO = super.orderDAO.getCustomer(this.ordVO.getCustomerId());
  }

/*****************************************************
 * retrieveCustomerPhoneInformation
 ******************************************************/
  private void retrieveCustomerPhoneInformation()
    throws Exception
  {
    //create a customer phone vo
    List custPhoneList = super.orderDAO.getCustomerPhones(this.ordVO.getCustomerId());
    CustomerPhoneVO phoneVO = null;

    //itereate thru the list and create the VOs
    Iterator custPhoneIter = custPhoneList.iterator();
    while (custPhoneIter.hasNext())
    {
      phoneVO = (CustomerPhoneVO) custPhoneIter.next();
      if (phoneVO.getPhoneType().equalsIgnoreCase("Evening"))
      {
        this.custEveningPhoneVO.setCreatedBy(phoneVO.getCreatedBy());
        this.custEveningPhoneVO.setCreatedOn(phoneVO.getCreatedOn());
        this.custEveningPhoneVO.setCustomerId(phoneVO.getCustomerId());
        this.custEveningPhoneVO.setExtension(phoneVO.getExtension());
        this.custEveningPhoneVO.setPhoneId(phoneVO.getPhoneId());
        this.custEveningPhoneVO.setPhoneNumber(phoneVO.getPhoneNumber());
        this.custEveningPhoneVO.setPhoneType(phoneVO.getPhoneType());
        this.custEveningPhoneVO.setUpdatedBy(phoneVO.getUpdatedBy());
        this.custEveningPhoneVO.setUpdatedOn(phoneVO.getUpdatedOn());
      }
      else
      {
        this.custDayPhoneVO.setCreatedBy(phoneVO.getCreatedBy());
        this.custDayPhoneVO.setCreatedOn(phoneVO.getCreatedOn());
        this.custDayPhoneVO.setCustomerId(phoneVO.getCustomerId());
        this.custDayPhoneVO.setExtension(phoneVO.getExtension());
        this.custDayPhoneVO.setPhoneId(phoneVO.getPhoneId());
        this.custDayPhoneVO.setPhoneNumber(phoneVO.getPhoneNumber());
        this.custDayPhoneVO.setPhoneType(phoneVO.getPhoneType());
        this.custDayPhoneVO.setUpdatedBy(phoneVO.getUpdatedBy());
        this.custDayPhoneVO.setUpdatedOn(phoneVO.getUpdatedOn());
      }
    }
  }

/*****************************************************
 * retrieveRecipientInformation
 ******************************************************/
  private void retrieveRecipientInformation()
    throws Exception
  {
    //create a recipient vo
    this.recipVO = super.orderDAO.getCustomer(this.ordDetVO.getRecipientId());
  }

/*****************************************************
 * retrieveRecipientPhoneInformation
 ******************************************************/
  private void retrieveRecipientPhoneInformation()
    throws Exception
  {
    //create a recipient phone vo
    List recipPhoneList = super.orderDAO.getCustomerPhones(this.ordDetVO.getRecipientId());
    CustomerPhoneVO phoneVO = null;

    //itereate thru the list and create the VOs
    Iterator recipPhoneIter = recipPhoneList.iterator();
    while (recipPhoneIter.hasNext())
    {
      phoneVO = (CustomerPhoneVO) recipPhoneIter.next();
      if (phoneVO.getPhoneType().equalsIgnoreCase("Evening"))
      {
        this.recipEveningPhoneVO.setCreatedBy(phoneVO.getCreatedBy());
        this.recipEveningPhoneVO.setCreatedOn(phoneVO.getCreatedOn());
        this.recipEveningPhoneVO.setCustomerId(phoneVO.getCustomerId());
        this.recipEveningPhoneVO.setExtension(phoneVO.getExtension());
        this.recipEveningPhoneVO.setPhoneId(phoneVO.getPhoneId());
        this.recipEveningPhoneVO.setPhoneNumber(phoneVO.getPhoneNumber());
        this.recipEveningPhoneVO.setPhoneType(phoneVO.getPhoneType());
        this.recipEveningPhoneVO.setUpdatedBy(phoneVO.getUpdatedBy());
        this.recipEveningPhoneVO.setUpdatedOn(phoneVO.getUpdatedOn());
      }
      else
      {
        this.recipDayPhoneVO.setCreatedBy(phoneVO.getCreatedBy());
        this.recipDayPhoneVO.setCreatedOn(phoneVO.getCreatedOn());
        this.recipDayPhoneVO.setCustomerId(phoneVO.getCustomerId());
        this.recipDayPhoneVO.setExtension(phoneVO.getExtension());
        this.recipDayPhoneVO.setPhoneId(phoneVO.getPhoneId());
        this.recipDayPhoneVO.setPhoneNumber(phoneVO.getPhoneNumber());
        this.recipDayPhoneVO.setPhoneType(phoneVO.getPhoneType());
        this.recipDayPhoneVO.setUpdatedBy(phoneVO.getUpdatedBy());
        this.recipDayPhoneVO.setUpdatedOn(phoneVO.getUpdatedOn());
      }
    }

  }


  private boolean isOrderLocked()
    throws Exception
  {
    boolean isOrderLocked = false;
    String sessionId = "" + this.hashCode() + new Date().getTime();
    isOrderLocked = super.emailQDAO.isOrderLocked(this.ordDetVO, sessionId, SYSTEM_CSR);

    return isOrderLocked;

  }


  /**
     * @param emailVO
     * @param emailDAO
     * @param ordDetail
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws SQLException
     * @throws Exception
     * @throws IOException
     * @throws SAXException
     * @throws TransformerException
     * void
     */
  private void storeDIQueue( String custEmailAddress,
                           String pointOfCotactId)
    throws Exception
  {
    this.qVO.setAttached(null);
    this.qVO.setCreatedOn(new Date());
    this.qVO.setDeliveryDate(this.ordDetVO.getDeliveryDate());
    this.qVO.setEmailAddress(custEmailAddress);
    this.qVO.setExternalOrderNumber(this.ordDetVO.getExternalOrderNumber());
    this.qVO.setMasterOrderNumber(this.ordVO.getMasterOrderNumber());
    this.qVO.setMercuryId(null);
    this.qVO.setMercuryNumber(null);
    this.qVO.setMessageId(null);
    this.qVO.setMessageTimestamp(new Date());
    this.qVO.setMessageType(DCConstants.DELIVERY_INQUIRY);
    this.qVO.setOrdDetVO(this.ordDetVO);
    this.qVO.setOrderDetailId((new Long(this.ordDetVO.getOrderDetailId())).toString());
    this.qVO.setOrderGuid(this.ordVO.getOrderGuid());
    this.qVO.setPointOfContactId(pointOfCotactId);
    this.qVO.setQueueType(DCConstants.DELIVERY_INQUIRY);
    this.qVO.setRecipientLocation(null);
    this.qVO.setSameDayGift(this.ordDetVO.getSameDayGift());
    this.qVO.setSystem(DCConstants.SYSTEM_TYPE_MERCURY);
    this.qVO.setTagged(DCConstants.NO);
    this.qVO.setTimezone(null);
    
  }


  /**
     */
  private boolean isPastDeliveryDate()
  {
    boolean pastDeliveryDate = false; 
    
    //Create Calendar Object
    Calendar cTodaysDate = Calendar.getInstance();

    Date deliveryDate = this.ordDetVO.getDeliveryDate();

    //Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    String test = sdf.format(deliveryDate); 

    //Since the delivery date's time is 00:00:00, set today's date time to 00:00:00 too
    cTodaysDate.set(Calendar.HOUR, 0); 
    cTodaysDate.set(Calendar.MINUTE, 0); 
    cTodaysDate.set(Calendar.SECOND, 0); 
    cTodaysDate.set(Calendar.MILLISECOND, 0);
    cTodaysDate.set( Calendar.AM_PM, Calendar.AM );

    if (super.getDiffInDays(cTodaysDate.getTime(), deliveryDate) <= 0)
    {
      pastDeliveryDate = true; 
    }

    return pastDeliveryDate; 
  }




  /**
     */
  private void processDeliveryInquiryPastDeliveryDate(String pointOfContactId,
                                                      String custEmailAddress,
                                                      String orderDetailId)
    throws Exception
  {

    super.persistQueue(this.qVO);
    DeliveryInquiryUtil diu = null;
    EmailQueueDAO emailDAO = new EmailQueueDAO(super.connection);
    String dconStatus = emailDAO.getDeliveryConfirmationStatus(Long.parseLong(orderDetailId));
    if (dconStatus == null) {
        dconStatus = "";
    }
    logger.info("dconStatus is " + dconStatus );
    if (dconStatus == "" || dconStatus.equals(GeneralConstants.DCON_PENDING)) {

        diu = new DeliveryInquiryUtil(emailDAO.getDbConnection());
        if (diu.sendASKMessage(new Long(this.ordDetVO.getOrderDetailId()).toString())) {
	        ASKMessageTO askMessage = super.createASKMessageTO(DCConstants.DELIVERY_INQUIRY, SYSTEM_CSR, 
	                                    (new Long(this.ordDetVO.getOrderDetailId()).toString()));
	        if (askMessage != null)
	        {
	          try
	          {
	            debugLog("Delivery Inquiry is past delivery date. Send ASK Message to Florist. [BEGIN]");
	            ResultTO result = super.sendMercuryASKMessage(askMessage);
	            debugLog("Delivery Inquiry is past delivery date. Send ASK Message to Florist. [END]");
	            if (result.isSuccess())
	            {
	              ASKEventHandlerVO askEvent = new ASKEventHandlerVO();
	              askEvent.setFillingFloristId(this.ordDetVO.getFloristId());
	              askEvent.setOrderDetailID(new Long(this.ordDetVO.getOrderDetailId()).toString());
	              askEvent.setMessageType(DCConstants.SYSTEM_TYPE_MERCURY);
	              askEvent.setPointOfContactId(pointOfContactId);
	              askEvent.setQueueType(DCConstants.DELIVERY_INQUIRY);
	              askEvent.setSendEmail(custEmailAddress);
	              askEvent.setMercuryMessageId(result.getKey());
	              super.logMessageTransaction(askEvent);
	              if (this.isHolidayTime(new Date()))
	              {
	                debugLog("is holiday");
	                this.comments = "Incoming Delivery Inquiry email request received.  Holiday Delivery Inquiry email sent. Email has been queued.";
	                super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, custVO);
	              }
	              else
	              {
	
	                this.comments = "Incoming Delivery Inquiry email request received.  Delivery Inquiry email sent. Email has been queued." ;
	                super.insertComments(this.comments, SYSTEM_CSR, this.ordDetVO, this.custVO);
	              }
	
	              if (dconStatus.equals(GeneralConstants.DCON_PENDING)) {
	                  emailDAO.updateDeliveryConfirmationStatus(Long.parseLong(orderDetailId),GeneralConstants.DCON_SENT, "DI_SYS");
	              }
	                 
	            }
	            else
	            {
	              logger.error("Can't send Mercury ASK message. Error: " + result.getErrorString());
	            }
	          }
	          catch (Exception e)
	          {
	            logger.error("Can't send Mercury ASK message. Error: ", e);
	          }
	        }
	        else
	        {
	          logger.error("Can't create Mercury ASK message. ");
	        }
    	}
    } else {
        logger.info("DCON status is not blank or Pending");
    }
  }


}
