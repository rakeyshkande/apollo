package com.ftd.dc.service;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.constants.OrderConstants;
import com.ftd.dc.dao.OrderDAO;
import com.ftd.dc.framework.util.CommonUtils;
import com.ftd.dc.util.OrderEmailUtilities;
import com.ftd.dc.vo.OrderDetailVO;
import com.ftd.dc.vo.OrderVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.XMLFormat;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;


public class EmailDeliveryConfirmationService {
    
	private Logger logger;
    private Connection conn = null;
    private static final String EMAIL_TYPE_DCON = "DCON";
    
    public EmailDeliveryConfirmationService(Connection conn) {
        this.conn = conn;
        logger = new Logger(DCConstants.LOGGER_CATEGORY_EMAILDELIVERYCONFIRMATIONSERVICE);
    }

    public void sendDeliveryConfirmation(String orderDetailId) throws Exception {
        logger.debug("EmailDeliveryConfirmationService [BEGIN]");
        sendDCONEmail(conn,orderDetailId);
        logger.debug("EmailDeliveryConfirmationService [END]");
    }



    private void sendDCONEmail(Connection conn, String orderDetailId) throws Exception {
        String includeComments  = "Y";
        String messageType = DCConstants.POC_EMAIL_TYPE;
        String originId = null;
        String companyId = null;
        String messageId = null;
        String extOrderNum = null;
        String masterOrderNum = null;
        String customerId = null;
        String customerFirstname = null;
        String customerLastname = null;
        String customerEmailAddress = null;
        String customerPhone = null;
        String orderGuid = null;
        String partnerName = null;
        String sourceCode = null;
        String orderOrigin = null;

        CachedResultSet rs = null;

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");

        String fedExEmailTemplate = null;
        OrderDAO orderAccess = new OrderDAO(conn);
        

        try {          

            // use OrderDAO to access some header info on the sendEMail.xsl page
        
            logger.debug(" Locating customer information. OrderDetailId=" + orderDetailId);
            rs = orderAccess.getOrderCustomerInfo(orderDetailId);
            if(rs.next())
            {
                customerId = rs.getString("customer_id");
                orderGuid = rs.getString("order_guid");
                extOrderNum = rs.getString("external_order_number");
                masterOrderNum = rs.getString("master_order_number");
                customerFirstname = rs.getString("first_name");
                customerLastname = rs.getString("last_name");
                customerEmailAddress = rs.getString("email_address");
                companyId = rs.getString("company_id");
                originId = rs.getString("origin_id");
                customerPhone = rs.getString("customer_phone_number");
                sourceCode = rs.getString("source_code");
            }
            else
            {
              logger.error("DCON email not sent. Customer Info missing from DB for orderDetailId="+orderDetailId);
              return;
            }
  
            if(customerEmailAddress == null || customerEmailAddress.equalsIgnoreCase("")) {
                logger.error("DCON email not sent. Customer email address not available for orderDetailId="+orderDetailId);
                return;
            }

            logger.debug(" Locating order detail information. OrderDetailId=" + orderDetailId);
    
            OrderDetailVO orderVO = orderAccess.getOrderDetail(orderDetailId);
            
            // get the order origin.
            OrderVO order = null;
            if(orderGuid != null) {
            	order = orderAccess.getOrder(orderGuid);
            	if(order != null) {
            		orderOrigin = order.getOriginId();
            		logger.debug("Origin on order : " + orderOrigin + ", this is used to determine the partner order and replace FTD external order number with partner order item number");
            	}
            }
	            
            if (orderVO == null)
            {
                logger.error("DCON email not sent. OrderDetail Info could not be gathered from DB for orderDetailId="+orderDetailId);
                return; 
            }
            
            logger.debug(" Locating partner information. OrderDetailId=" + orderDetailId +" and source code=" +orderVO.getSourceCode());
    
            PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderVO.getSourceCode());
            
            if (partnerVO != null && partnerVO.getPartnerName() != null)
            {
                partnerName = partnerVO.getPartnerName();
            }
            else
            {
                partnerName = "";
            }
                   
            if(globalParamHandler != null)
            {
                if(StringUtils.isNotBlank(partnerName))
                {
                    fedExEmailTemplate = globalParamHandler.getFrpGlobalParm(OrderConstants.PREFERRED_PARTNER_CONFIG, partnerName + "_" + OrderConstants.DCON_EMAIL_TITLE);
                }
                else
                {
                   fedExEmailTemplate = globalParamHandler.getFrpGlobalParm(OrderConstants.PREFERRED_PARTNER_CONFIG, OrderConstants.DCON_EMAIL_TITLE);
                }
            }

            logger.debug("Email Template: " + fedExEmailTemplate);
    
            // scrub the origin id against config file origins list
            if(originId!=null)
            {
              String originList = configUtil.getProperty(DCConstants.PROPERTY_FILE, DCConstants.EMAIL_ORIGINS);
              if(originList.indexOf(originId)>=0)
              {
                // keep the origin id
              }
              else
              {
                originId = null;
              }
            }

            // Retrieve order data using OrderDAO to be used for the token replacement
            Document dataDocXML = this.getOrderDataDocumentXML(orderDetailId, sourceCode, includeComments, conn);

           // get an instance of StockMessageGenerator
           StockMessageGenerator msgGen = new StockMessageGenerator();
           MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(conn);
  
           // use company_id to pull SenderEmailAddress from db
           logger.debug("Company For Email: " + companyId);
  
           String companyEmailAddress = msgGenDAO.loadSenderEmailAddress(fedExEmailTemplate, companyId, Long.valueOf(orderDetailId));

            // Create a PointOfCOntactVO setting the pocType, customerId, orderDetailId and companyId from
            // the request object and order dataDocument just created
            PointOfContactVO pocObj = new PointOfContactVO();
            pocObj.setPointOfContactType(messageType);

            long customerIdNum = 0;
            if(customerId!=null)
              customerIdNum = Long.parseLong(customerId);

            long orderDetailIdNum = 0;
            if(orderDetailId!=null && !orderDetailId.equals(""))
              orderDetailIdNum = Long.parseLong(orderDetailId);

            pocObj.setCustomerId(customerIdNum);
            pocObj.setOrderGuid(orderGuid);
            pocObj.setOrderDetailId(orderDetailIdNum);
            pocObj.setCompanyId(companyId);
            pocObj.setMasterOrderNumber(masterOrderNum);
            pocObj.setExternalOrderNumber(getOrderNumber(extOrderNum, orderOrigin)); 
            pocObj.setFirstName(customerFirstname);
            pocObj.setLastName(customerLastname);
            pocObj.setDaytimePhoneNumber(customerPhone);
            pocObj.setDataDocument(dataDocXML);
            pocObj.setLetterTitle(fedExEmailTemplate);
            pocObj.setCommentType(messageType);
            pocObj.setTemplateId(messageId);
            pocObj.setOriginId(originId);
            pocObj.setSenderEmailAddress(companyEmailAddress);
            pocObj.setRecipientEmailAddress(customerEmailAddress);
            
            // changes related to ET/FICO
            pocObj.setSourceCode(orderVO.getSourceCode());
            pocObj.setEmailType(EMAIL_TYPE_DCON);
	        if (orderVO.getOrderGuid() != null) {
	        	pocObj.setRecordAttributesXML(msgGen.generateRecordAttributeXMLFromOrderGuid(orderVO.getOrderGuid()));  
	        }
	        else {
	      	  logger.error("Unable to create XML attributes string for orderGuid="+orderVO.getOrderGuid());
	        }
//            pocObj.setMailserverCode(globalParamHandler.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_KEY)); 

          // Build the message using StockMessageGenerator passing the POCVO and returning another POCVO
          // containing the merged body and subject
//          pocObj = msgGen.buildMessage(pocObj);
//
//          // Create XML from the POC object and append to primary document
//          String pocXMLString = pocObj.toXML();
//          logger.debug("Company Email Id=" + companyEmailAddress + " Customer Email Id=" + customerEmailAddress + " Origin Id = " + originId );
//          logger.debug(" Generated Email :" + pocXMLString);
            
            
  
            logger.debug("Sending DCON email to Customer=" + customerFirstname + " " + customerLastname + " Email Address:" + customerEmailAddress);
            msgGen.processMessage(pocObj);
        } catch (Exception e){   
                logger.error(e);  
                CommonUtils.sendSystemMessage("Attempt to send DCON failed. order detail id:" + orderDetailId);    
        }
        

        try {
              //update DCON status to Confirmed if preferred partner.
              if(StringUtils.isNotBlank(partnerName))
              {  
                  orderAccess.updateDeliveryConfirmationStatus(Long.parseLong(orderDetailId), GeneralConstants.DCON_CONFIRMED, "DCON_SCAN"); 
              }

        } catch (Exception e){   
              logger.error(e);  
              CommonUtils.sendSystemMessage("Attempt to update DCON status to Confirmed failed. order detail id:" + orderDetailId);    
        }

    }



    private Document getOrderDataDocumentXML(String orderDetailId, String sourceCode, String includeComments, Connection conn) throws Exception
    {
      //Instantiate OrderDAO
      OrderDAO orderDAO = new OrderDAO(conn);

      // pull base document
      Document primaryOrderXML = DOMUtil.getDocument();

      if(orderDetailId != null && !orderDetailId.equals(""))
      {
          //hashmap that will contain the output from the stored proc
          HashMap printOrderInfo = new HashMap();

          //Call getCartInfo method in the DAO
          printOrderInfo = orderDAO.getOrderInfoForPrint(orderDetailId, includeComments, DCConstants.CONS_PROCESSING_ID_CUSTOMER);

          //Create an Document and call a method that will transform the CachedResultSet from the cursor:
          //OUT_ORDERS_CUR into an master Document.
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDERS_CUR", "ORDERS", "ORDER").getChildNodes());

          //OUT_ORDER_ADDON_CUR into an Document.  Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_ADDON_CUR", "ORDER_ADDONS", "ORDER_ADDON").getChildNodes());

          //OUT_ORDER_BILLS_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_BILLS_CUR", "ORDER_BILLS", "ORDER_BILL").getChildNodes());

          //OUT_ORDER_REFUNDS_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_REFUNDS_CUR", "ORDER_REFUNDS", "ORDER_REFUND").getChildNodes());

          //OUT_ORDER_TOTAL_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_TOTAL_CUR", "ORDER_TOTALS", "ORDER_TOTAL").getChildNodes());

          //OUT_ORDER_PAYMENT_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_ORDER_PAYMENT_CUR", "ORDER_PAYMENTS", "ORDER_PAYMENT").getChildNodes());

          //CUST_CART_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "CUST_CART_CUR", "CUSTOMERS", "CUSTOMER").getChildNodes());

          //OUT_CUSTOMER_PHONE_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_CUSTOMER_PHONE_CUR", "CUSTOMER_PHONES", "CUSTOMER_PHONE").getChildNodes());

          //OUT_MEMBERSHIP_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_MEMBERSHIP_CUR", "MEMBERSHIPS", "MEMBERSHIP").getChildNodes());

          //OUT_TAX_REFUND_CUR into an Document. Append this a master XML document
          DOMUtil.addSection(primaryOrderXML, buildXML(printOrderInfo, "OUT_TAX_REFUND_CUR", "TAX_REFUNDS", "TAX_REFUND").getChildNodes());

          // append the current date to the document
          Element element = (Element)primaryOrderXML.createElement("cur_date");
          element.appendChild(primaryOrderXML.createTextNode(DateFormat.getDateInstance(DateFormat.SHORT).format(new Date())));
          primaryOrderXML.getDocumentElement().appendChild(element);

          //Append the web loyalty url to the document
          String wlURL = this.getWebLoyaltyURL(conn, sourceCode);
          logger.debug("EmailDeliveryConfirmationService.getOrderDataDocumentXML() - wlUrl>>>>>>>>>" + wlURL + "<<<<<<<<<<");
          element = (Element)primaryOrderXML.createElement("web_loyalty_url");
          if( wlURL!=null && wlURL.length()>0 ) {
              element.appendChild(primaryOrderXML.createTextNode(wlURL));
          } else {
              element.appendChild(primaryOrderXML.createTextNode(" "));
          }
          primaryOrderXML.getDocumentElement().appendChild(element);
          
           Element csrElement = (Element)primaryOrderXML.createElement("CSR");
           Element csrName =  (Element)primaryOrderXML.createElement("first_name");
           csrElement.appendChild(csrName);
           csrName.appendChild(primaryOrderXML.createTextNode("FTD.com Customer Service"));
           csrName =  (Element) primaryOrderXML.createElement("last_name");
           csrElement.appendChild(csrName);
           csrName.appendChild(primaryOrderXML.createTextNode(""));
           primaryOrderXML.getDocumentElement().appendChild(csrElement);
      }
         

      return primaryOrderXML;
    }

    private String getWebLoyaltyURL(Connection conn, String sourceCode) throws Exception {

        OrderEmailUtilities oeUtil = new OrderEmailUtilities();
        String wlString="";

        //retrieve Source code information.
        SourceMasterVO sVO = oeUtil.retrieveSourceCodeInfo(conn, sourceCode);
        if (sVO == null)
            throw new Exception ("Source code was not found");

        //find out if the source code was for webloyalty
        boolean webloyaltyOrder = sVO.getWebloyaltyFlag().equalsIgnoreCase("Y")?true:false;

        //if webloyalty order, process for webloyalty.
        if (webloyaltyOrder)
        {
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            String slogan;
            try {
                slogan = config.getFrpGlobalParm(DCConstants.SERVICE_LOCATOR_CONFIG_CONTEXT,"WEB_LOYALTY_EMAIL_SLOGAN");
            } catch (Exception e) {
                slogan = config.getProperty(DCConstants.PROPERTY_FILE,"DEFAULT_WEB_LOYALTY_EMAIL_SLOGAL");
            }

            String wlUrl = oeUtil.processWebloyalty(conn);
            if(StringUtils.isNotBlank(wlUrl)) {
                StringBuilder sb = new StringBuilder();
                sb.append("\n");
                sb.append(slogan);
                sb.append("\n");
                sb.append(wlUrl);
                sb.append("\n");
                wlString = sb.toString();
                sb.append("\n\n");
            }
        }

        return wlString;
    }


    /*******************************************************************************************
    * buildXML()
    *******************************************************************************************/
    private Document buildXML(HashMap outMap, String cursorName, String topName, String bottomName)
          throws Exception
    {

      //Create a CachedResultSet object using the cursor name that was passed.
      CachedResultSet rs = (CachedResultSet) outMap.get(cursorName);

      //Create an XMLFormat, and initialize the top and bottom node.  Note that since this method
      //can be/is called by various other methods, the top and botton names MUST be passed to it.
      XMLFormat xmlFormat = new XMLFormat();
      xmlFormat.setAttribute("type", "element");
      xmlFormat.setAttribute("top", topName);
      xmlFormat.setAttribute("bottom", bottomName );

      //call the DOMUtil's converToXMLDOM method
      Document doc = (Document) DOMUtil.convertToXMLDOM(rs, xmlFormat);

      //return the xml document.
      return doc;

    }
    
    /** Get order number as per the origin. If not FTD order, replace order number with the XX partner order number.
   	 * @param externalOrderNumber
   	 * @param orderOrigin
   	 * @return
   	 */
   	private String getOrderNumber(String externalOrderNumber, String orderOrigin) {    		
		try {				
	   		// return FTD external order number, when order origin is invalid		
	   		if(StringUtils.isEmpty(orderOrigin)) {
	   			logger.error("Unable to determine if the order is partner order, invalid order origin");
	   			return externalOrderNumber;
	   		}
	
	   		// If partner order, return partner order item number.
	   		CachedResultSet result = new PartnerUtility().getPtnOrderDetailByConfNumber(externalOrderNumber, orderOrigin, conn);
	   		if (result != null && result.next()) {
	   			if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
	   				return result.getString("PARTNER_ORDER_ITEM_NUMBER");
	   			}
	   		}
		} catch (Exception e) {
			logger.error("Error caught getting order number, default it to external order number ", e);
		}
   		return externalOrderNumber; 
   	}

}
