package com.ftd.dc.constants;

/**
 * 03/01/2007 - Imported from order_email project by tpeterson
 */
public class OrderEmailConstants {
  public static final char DEFAULT_TOKENIZER = 255;
  public static final String CREF           = "cref";

  /*********************************************************************************************
  //custom parameters returned from database procedure
  *********************************************************************************************/
  public static final String STATUS_PARAM = "OUT_STATUS";
  public static final String MESSAGE_PARAM = "OUT_MESSAGE";

  /*********************************************************************************************
  //default user ids for the order email process
  *********************************************************************************************/
  public static final String CONFIRMATION_USER    = "DELIVERY_CONFIRMATION";

  /*********************************************************************************************
  //webloyalty URL constants that will be retrieve from Global parms
  *********************************************************************************************/
  public static final String CONTEXT_WEBLOYALTY                 = "WEBLOYALTY";
  public static final String NAME_OFFER_POOL_ID_DATA_SHIP_CONF  = "offer_pool_id_data_ship_conf";
  public static final String NAME_OFFER_POOL_ID_LITERAL         = "offer_pool_id_literal";
  public static final String NAME_OFFER_SRC_ID_DATA_SHIP_CONF   = "offer_src_id_data_ship_conf";
  public static final String NAME_OFFER_SRC_ID_LITERAL          = "offer_src_id_literal";
  public static final String NAME_SERVER                        = "server";
  public static final String NAME_URL_PARAMETER_1               = "url_parameter_1";


  /*********************************************************************************************
  //HashMap constants for webloyalty global parms
  *********************************************************************************************/
  public static final String HK_OFFER_POOL_ID_DATA_SHIP_CONF  = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_POOL_ID_DATA_SHIP_CONF;
  public static final String HK_OFFER_POOL_ID_LITERAL         = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_POOL_ID_LITERAL;
  public static final String HK_OFFER_SRC_ID_DATA_SHIP_CONF   = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_SRC_ID_DATA_SHIP_CONF;
  public static final String HK_OFFER_SRC_ID_LITERAL          = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_OFFER_SRC_ID_LITERAL;
  public static final String HK_SERVER                        = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_SERVER;
  public static final String HK_URL_PARAMETER_1               = OrderEmailConstants.CONTEXT_WEBLOYALTY + OrderEmailConstants.NAME_URL_PARAMETER_1;

  /**
   * default constructor
   */
  public OrderEmailConstants()
  {
  }
}
