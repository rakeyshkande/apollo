package com.ftd.dc.constants;

import com.ftd.dc.cache.handler.EmailQueueConfigHandler;

public class DCConstants {
    public DCConstants() {
    super();
    }

    // property file
    public static final String PROPERTY_FILE                  = "delivery-confirmation-config.xml";
    public static final String SERVICE_LOCATOR_CONFIG_FILE    = "service-locator-config.xml";
    public static final String SERVICE_LOCATOR_CONFIG_CONTEXT = "DELIVERY_CONFIRMATION_CONFIG";


    // delivery confirmation web service status constants
    public static final String DC_NO_ORDER = "No Order";
    public static final String DC_NO_MATCH = "No Match";
    public static final String DC_SHIPPED = "Shipped";
    public static final String DC_DELIVERED = "Delivered";
    public static final String DC_QUEUED = "Queued";
    public static final String DC_VENDOR_QUEUED = "Queued";
    public static final String DC_UNKNOWN = "Unknown";
    public static final String DC_NEW = "New";
    public static final String DC_SCRUB = "Queued";
    public static final String DC_CANCELREJECT = "Cancelled/Rejected";
   
    
    
    // SDS status
     public static final String SDS_CANCELLED = "CANCELLED";
     public static final String SDS_REJECTED = "REJECTED";
     public static final String SDS_SHIPPED = "SHIPPED";
     public static final String SDS_PRINTED = "PRINTED";
     public static final String SDS_AVAILABLE = "AVAILABLE";
     public static final String SDS_NEW = "NEW";
     
     
     // order scrub status
    public static final String ORDER_IN_SCRUB_DISP_CODE = "In-Scrub";
    public static final String ORDER_PENDING_DISP_CODE = "Pending";
    public static final String ORDER_REMOVED_DISP_CODE = "Removed";

    // constants
    public static final String EMAIL_ORIGINS                = "EMAIL_ORIGINS";
    public static final String CONS_PROCESSING_ID_CUSTOMER            = "CUSTOMER";
    public static final String POC_EMAIL_TYPE = "Email";
    public static final String SYSTEM_TYPE_VENUS = "Venus";
    public static final String SYSTEM_TYPE_MERCURY = "Merc";
    public static final String DELIVERY_INQUIRY = "DI";
    public static final String EMAIL_QUEUE_CONFIG_HANDLER = "EmailQueueConfigHandler";  
    public static final String EMAIL_QUEUE_PRIORITY_CACHE_HANDLER = "EmailQueuePriorityCacheHandler";  
    public static final String EMAIL_QUEUE_CONFIG_CONTEXT = "EMAIL_QUEUE_CONFIG";
    public static final String EMAIL_QUEUE_CONFIG_END_HOLIDAY = "END_HOLIDAY";
    public static final String EMAIL_QUEUE_CONFIG_START_HOLIDAY = "START_HOLIDAY";
    public static final String EMAIL_QUEUE_CONFIG_MAX_EMAIL_SIZE = "MAX_EMAIL_SIZE";
    public static final String HOLIDAY_DATE_FORMAT =  "MM/dd/yyyy";


    // boolean values
     public final static String YES = "Y";
    public final static String NO = "N";
    public final static String URL = "URL";


    /* logger:  category name */
    public static final String LOGGER_BASE = "com.ftd.dc.service.BaseService";
    public static final String LOGGER_CATEGORY_DELIVERYCONFIRMATIONSERVICE = "com.ftd.dc.service.DeliveryConfirmationService";
    public static final String LOGGER_CATEGORY_SDSDeliveryConfirmMDB = "com.ftd.dc.mdb.SDSDeliveryConfirmMDB";
    public static final String LOGGER_CATEGORY_EMAILDELIVERYCONFIRMATIONSERVICE = "com.ftd.dc.service.EmailDeliveryConfirmationService";
    public static final String LOGGER_CATEGORY_ORDERDAO = "com.ftd.dc.dao.OrderDAO";
    public static final String LOGGER_CATEGORY_VENUSDAO = "com.ftd.dc.dao.VenusDAO";
    public static final String LOGGER_CATEGORY_QUEUEDAO = "com.ftd.dc.dao.QueueDAO";
    public static final String LOGGER_CATEGORY_EMAILQUEUEDAO = "com.ftd.dc.dao.EmailQueueDAO";
    public static final String LOGGER_CATEGORY_QUEUESERVICE = "com.ftd.dc.service.QueueService";
    public static final String LOGGER_CATEGORY_COMMONUTILS = "com.ftd.dc.framework.util.CommonUtilites";

}
