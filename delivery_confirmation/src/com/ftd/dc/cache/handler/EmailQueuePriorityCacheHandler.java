package com.ftd.dc.cache.handler;

import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;
import com.ftd.osp.utilities.plugins.Logger;


public class EmailQueuePriorityCacheHandler
  extends CacheHandlerBase
{

  private Map queuePriorityMap = null;
    private Logger logger = new Logger(
        "com.ftd.dc.cache.handler.EmailQueuePriorityCacheHandler");


  public EmailQueuePriorityCacheHandler()
  {
  }


  /**
     * load queue configuration data
     *
     * @param Connection      - database connection
     * @return Object         - map of configuration information
     * @throws CacheException - cachecontroller error
     */
  public Object load(Connection conn)
    throws CacheException
  {

    Map cachedEmailQueueMap = new HashMap();
    try
    {
      // build DataRequest object
      DataRequest request = new DataRequest();
      request.reset();
      request.setConnection(conn);
      HashMap inputParams = new HashMap();
      inputParams.put("IN_QUEUE_INDICATOR", "Email");
      request.setInputParams(inputParams);
      request.setStatementID("GET_TYPES_BY_QUEUE_INDICATOR");

      // get data
      DataAccessUtil dau = DataAccessUtil.getInstance();

      CachedResultSet queueConfigContextRS =
        (CachedResultSet) dau.execute(request);
      while (queueConfigContextRS.next()) // get queue config context parms
      {
        cachedEmailQueueMap.put(queueConfigContextRS.getString("queue_type"),
                                new Integer(queueConfigContextRS.getInt("priority")));
      }
    }
    catch (Exception e)
    {
      logger.error(e);
      throw new CacheException(e.toString());
    }
    finally
    {
    }
    return cachedEmailQueueMap;
  }


  /**
     * set cached object
     *
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
  public void setCachedObject(Object cachedObject)
    throws CacheException
  {
    queuePriorityMap = (Map) cachedObject;
    return;
  }


  public int getQueuePriority(String queueType)
  {
    if (this.queuePriorityMap != null &&
        this.queuePriorityMap.get(queueType) != null)
    {
      return ((Integer) this.queuePriorityMap.get(queueType)).intValue();

    }
    else
    {
      return 0;
    }

  }

}
