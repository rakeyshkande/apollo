package com.ftd.dc.cache.handler;

import com.ftd.dc.constants.DCConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.vo.CacheHandlerBase;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * This class retrieves e-mail queue configuration information from database
 * configuration tables.
 *
 * @author Charles Fox
 * @todo check comments, error handling, and message handling
 */

public class EmailQueueConfigHandler extends CacheHandlerBase
{
    private Logger logger = new Logger(
        "com.ftd.dc.cache.handler.EmailQueueConfigHandler");


    private static final String EMAIL_QUEUE_CONFIG_END_HOLIDAY = "END_HOLIDAY";
    private static final String EMAIL_QUEUE_CONFIG_START_HOLIDAY = "START_HOLIDAY";
    private static final String EMAIL_QUEUE_CONFIG_CONTEXT = "EMAIL_QUEUE_CONFIG";
    private static final String EMAIL_QUEUE_CONFIG_MAX_EMAIL_SIZE = "MAX_EMAIL_SIZE";


    private Map queueConfigMap = null;

    /**
     * constructor
     *
     * @param none
     * @return n/a
     * @throws none
     */
    public EmailQueueConfigHandler()
    {
        super();
    }


    /**
     * load queue configuration data
     *
     * @param Connection      - database connection
     * @return Object         - map of configuration information
     * @throws CacheException - cachecontroller error
     */
    public Object load(Connection conn) throws CacheException
    {

        if(logger.isDebugEnabled()){
            logger.debug("Entering load");
            logger.debug("Connection : " + conn);
        }
        Map cachedEmailQueueMap = new HashMap();
        try
        {
            // build DataRequest object
            DataRequest request = new DataRequest();
            request.reset();
            request.setConnection(conn);
            HashMap inputParams = new HashMap();
            inputParams.put("CONTEXT_ID",
                DCConstants.EMAIL_QUEUE_CONFIG_CONTEXT);
            request.setInputParams(inputParams);
            request.setStatementID("GET_EMAIL_QUEUE_CONFIG_DATA");

            // get data
            DataAccessUtil dau = DataAccessUtil.getInstance();

            CachedResultSet queueConfigContextRS = (CachedResultSet) dau.execute(request);
            while(queueConfigContextRS.next())      // get queue config context parms
            {
                cachedEmailQueueMap.put((String) queueConfigContextRS.getObject(2),
                                   (String) queueConfigContextRS.getObject(3));
            }
        }
       catch(Exception e){
            logger.error(e);
            throw new CacheException(e.toString());
       }
       finally
        {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting load");
            }
        }
        return cachedEmailQueueMap;
    }


    /**
     * set cached object
     *
     * @param Object  - cached object
     * @return void
     * @throws CacheException
     */
    public void setCachedObject(Object cachedObject) throws CacheException
    {
        queueConfigMap = (Map) cachedObject;
        return;
    }


    /**
     * get start holiday date stored in global params
     *
     * @param none
     * @return startDate - the start date
     * @throws Exception
     */
    public Date getStartHoliday() throws ParseException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getStartHoliday");
        }

        java.util.Date startHoliday;

        try{
            String startDate = "";
            if((String) queueConfigMap.get(
                DCConstants.EMAIL_QUEUE_CONFIG_START_HOLIDAY) != null)
            {
                startDate = (String) queueConfigMap.get(
                    DCConstants.EMAIL_QUEUE_CONFIG_START_HOLIDAY);
            }

            SimpleDateFormat sdfOutput =
                   new SimpleDateFormat (
                   DCConstants.HOLIDAY_DATE_FORMAT);

            startHoliday = sdfOutput.parse( startDate );
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getStartHoliday");
            }
        }
        return startHoliday;
    }
    /**
     * get end holiday date stored in global params
     *
     * @param none
     * @return Date - the end date
     * @throws Exception
     */
    public Date getEndHoliday() throws ParseException, Exception
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getEndHoliday");
        }

        java.util.Date endHoliday;

        try{
            String endDate = "";
            if((String) queueConfigMap.get(DCConstants.EMAIL_QUEUE_CONFIG_END_HOLIDAY) != null)
            {
                endDate = (String) queueConfigMap.get(DCConstants.EMAIL_QUEUE_CONFIG_END_HOLIDAY);
            }

            SimpleDateFormat sdfOutput =
                   new SimpleDateFormat (
                   DCConstants.HOLIDAY_DATE_FORMAT);

            endHoliday = sdfOutput.parse( endDate );
        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getEndHoliday");
            }
        }
        return endHoliday;
    }

    /**
     * get max e-mail size stored in global params
     *
     * @param none
     * @return max e-mail size
     * @throws Exception
     */
    public String getMaxEmailSize()
    {
        if(logger.isDebugEnabled()){
            logger.debug("Entering getMaxEmailSize");
        }

        String maxEmailSize = "";

        try{

            if(queueConfigMap!=null&&queueConfigMap.get(DCConstants.EMAIL_QUEUE_CONFIG_MAX_EMAIL_SIZE) != null)
            {
                maxEmailSize = (String) queueConfigMap.get(DCConstants.EMAIL_QUEUE_CONFIG_MAX_EMAIL_SIZE);
            }

        }finally
        {
            if(logger.isDebugEnabled()){
               logger.debug("Exiting getMaxEmailSize");
            }
        }
        return maxEmailSize;
    }
}
