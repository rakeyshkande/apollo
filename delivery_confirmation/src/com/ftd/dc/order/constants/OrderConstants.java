package com.ftd.dc.order.constants;

public class OrderConstants 
{
  /**
     * constructor
     * 
     * @param none
     * @return n/a
     * @throws none
     */
  public OrderConstants()
  {
    super();
  }
 
  /* db stored procedure */
   public final static String SP_GET_COMPANY = "GET_COMPANY";
   public final static String SP_GET_CUSTOMER = "GET_CUSTOMER";
   public final static String SP_GET_ORDER_BY_GUID = "GET_ORDER_BY_GUID";
   public final static String SP_GET_ORDER_DETAIL = "GET_ORDER_DETAILS";
   public final static String SP_IS_EMAIL_ASSOCIATED = "IS_EMAIL_ASSOCIATED";
   
  
    /* db column names and input parameters for db stored procedure */
    
    //common
     public final static String ORDER_GUID = "ORDER_GUID";
     public final static String SOURCE_CODE = "SOURCE_CODE";
     public final static String COMPANY_ID = "COMPANY_ID";
     public final static String ENABLE_LP_PROCESSING = "ENABLE_LP_PROCESSING";
     public final static String PHONE_NUMBER = "PHONE_NUMBER";
     public final static String ORDER_DETAIL_ID = "ORDER_DETAIL_ID";  
     public final static String DELIVERY_DATE = "DELIVERY_DATE";
     public final static String PRODUCT_ID = "PRODUCT_ID";
     public final static String FLORIST_ID = "FLORIST_ID";
     public final static String CUSTOMER_ID = "CUSTOMER_ID";
    
    //SP_GET_ORDER_BY_GUID
    public final static String MASTER_ORDER_NUMBER = "MASTER_ORDER_NUMBER";
    public final static String MEMBERSHIP_ID = "MEMBERSHIP_ID";
    public final static String ORIGIN_ID = "ORIGIN_ID";
    public final static String ORDER_DATE = "ORDER_DATE";
    public final static String ORDER_TOTAL = "ORDER_TOTAL";
    public final static String PRODUCT_TOTAL = "PRODUCT_TOTAL";
    public final static String ADD_ON_TOTAL = "ADD_ON_TOTAL";
    public final static String SERVICE_FEE_TOTAL = "SERVICE_FEE_TOTAL";
    public final static String SHIPPING_FEE_TOTAL = "SHIPPING_FEE_TOTAL";
    public final static String DISCOUNT_TOTAL = "DISCOUNT_TOTAL";
    public final static String TAX_TOTAL = "TAX_TOTAL";
    public final static String LOSS_PREVENTION_INDICATOR = "LOSS_PREVENTION_INDICATOR";
  
    //SP_UPDATE_ORDER_DETAIL
    public final static String RECIPIENT_ID = "RECIPIENT_ID";  
    public final static String QUANTITY = "QUANTITY";
    public final static String COLOR_1 = "COLOR_1";
    public final static String COLOR_2 = "COLOR_2";
    public final static String SUBSTITUTION_INDICATOR = "SUBSTITUTION_INDICATOR";
    public final static String SAME_DAY_GIFT = "SAME_DAY_GIFT";
    public final static String OCCASION = "OCCASION";
    public final static String CARD_MESSAGE = "CARD_MESSAGE";
    public final static String CARD_SIGNATURE = "CARD_SIGNATURE";
    public final static String SPECIAL_INSTRUCTIONS = "SPECIAL_INSTRUCTIONS";
    public final static String RELEASE_INFO_INDICATOR = "RELEASE_INFO_INDICATOR";  
    public final static String SHIP_METHOD = "SHIP_METHOD";
    public final static String SHIP_DATE = "SHIP_DATE";
    public final static String ORDER_DISP_CODE = "ORDER_DISP_CODE";
    public final static String ORDER_HELD = "ORDER_HELD";
    public final static String SECOND_CHOICE_PRODUCT = "SECOND_CHOICE_PRODUCT";
    public final static String ZIP_QUEUE_COUNT = "ZIP_QUEUE_COUNT";
    public final static String RANDOM_WEIGHT_DIST_FAILURES = "RANDOM_WEIGHT_DIST_FAILURES";
    public final static String UPDATED_BY = "UPDATED_BY";
    public final static String DELIVERY_DATE_RANGE_END = "DELIVERY_DATE_RANGE_END";
    public final static String SCRUBBED_ON = "SCRUBBED_ON";
    public final static String SCRUBBED_ON_DATE = "SCRUBBED_ON_DATE";
    public final static String SCRUBBED_BY = "SCRUBBED_BY";
    public final static String USER_ID = "USER_ID";
    public final static String ARIBA_UNSPSC_CODE = "ARIBA_UNSPSC_CODE";
    public final static String ARIBA_PO_NUMBER = "ARIBA_PO_NUMBER";
    public final static String ARIBA_AMS_PROJECT_CODE = "ARIBA_AMS_PROJECT_CODE";
    public final static String ARIBA_COST_CENTER = "ARIBA_COST_CENTER";
    public final static String SIZE_INDICATOR = "SIZE_INDICATOR";
    public final static String MILES_POINTS = "MILES_POINTS";
    public final static String SUBCODE = "SUBCODE";
    
    //SP_GET_COMPANY  
    public final static String COMPANY_NAME = "COMPANY_NAME";
    public final static String INTERNET_ORIGIN = "INTERNET_ORIGIN";
    public final static String DEFAULT_PROGRAM_ID = "DEFAULT_PROGRAM_ID";
    public final static String CLEARING_MEMBER_NUMBER = "CLEARING_MEMBER_NUMBER";
    public final static String LOGO_FILE_NAME = "LOGO_FILENAME"; 
    
    //SP_GET_CUSTOMER  
    public final static String CONCAT_ID = "CONCAT_ID";
    public final static String CUSTOMER_FIRST_NAME = "CUSTOMER_FIRST_NAME";
    public final static String CUSTOMER_LAST_NAME = "CUSTOMER_LAST_NAME";
    public final static String BUSINESS_NAME = "BUSINESS_NAME";
    public final static String CUSTOMER_ADDRESS_1 = "CUSTOMER_ADDRESS_1";
    public final static String CUSTOMER_ADDRESS_2 = "CUSTOMER_ADDRESS_2";
    public final static String CUSTOMER_CITY = "CUSTOMER_CITY";
    public final static String CUSTOMER_STATE = "CUSTOMER_STATE";
    public final static String CUSTOMER_ZIP_CODE = "CUSTOMER_ZIP_CODE";
    public final static String CUSTOMER_COUNTRY = "CUSTOMER_COUNTRY";
}