package com.ftd.dc.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class OrderDetailVO extends BaseVO
{
  private	long	    orderDetailId;
  private	String	  orderGuid;
  private	String	  externalOrderNumber;
  private	String	  hpOrderNumber;
  private	String	  sourceCode;
  private	Date      deliveryDate;
  private	long	    recipientId;
  private	String	  productId;
  private	long	    quantity;
  private	String	  color1;
  private	String	  color2;
  private	String	  substitutionIndicator;
  private	String	  sameDayGift;
  private	String	  occasion;
  private	String	  cardMessage;
  private	String	  cardSignature;
  private	String	  specialInstructions;
  private	String	  releaseInfoIndicator;
  private	String	  floristId;
  private	String	  shipMethod;
  private	Date      shipDate;
  private	String	  orderDispCode;
  private String    secondChoiceProduct;
  private long      zipQueueCount;
  private long      rejectRetryCount;
  private String    lpOrderIndicator;
  private Date      deliveryDateRangeEnd;
  private Date      scrubbedOn;
  private String    scrubbedBy;
  private String    aribaUnspscCode;
  private String    aribaPoNumber;
  private String    aribaAmsProjectCode;
  private String    aribaCostCenter;
  private String    sizeIndicator;
  private long      milesPoints;
  private String    subcode;
  private String    opStatus;
  private String    carrierDelivery;
  private String    carrierId;
  private String    venusMethodOfPayment;
  private String    vendorId;
 
  public OrderDetailVO()
  {
    
  }

  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }

  public long getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }

  public String getOrderGuid()
  {
    return orderGuid;
  }
  
  public void setExternalOrderNumber(String externalOrderNumber)
  {
    if(valueChanged(this.externalOrderNumber, externalOrderNumber))
    {
      setChanged(true);
    }
    this.externalOrderNumber = trim(externalOrderNumber);
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setHpOrderNumber(String hpOrderNumber)
  {
    if(valueChanged(this.hpOrderNumber, hpOrderNumber))
    {
      setChanged(true);
    }
    this.hpOrderNumber = trim(hpOrderNumber);
  }

  public String getHpOrderNumber()
  {
    return hpOrderNumber;
  }

  public void setSourceCode(String sourceCode)
  {
    if(valueChanged(this.sourceCode, sourceCode))
    {
      setChanged(true);
    }
    this.sourceCode = trim(sourceCode);
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    if(valueChanged(this.deliveryDate, deliveryDate))
    {
      setChanged(true);
    }
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }
    
  public void setRecipientId(long recipientId)
  {
    if(valueChanged(this.recipientId, recipientId))
    {
      setChanged(true);
    }
    this.recipientId = recipientId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setProductId(String productId)
  {
    if(valueChanged(this.productId, productId))
    {
      setChanged(true);
    }
    this.productId = trim(productId);
  }

  public String getProductId()
  {
    return productId;
  }

  public void setQuantity(long quantity)
  {
     if(valueChanged(this.quantity, quantity))
    {
      setChanged(true);
    }
   this.quantity = quantity;
  }

  public long getQuantity()
  {
    return quantity;
  }

  public void setColor1(String color1)
  {
    if(valueChanged(this.color1, color1))
    {
      setChanged(true);
    }
    this.color1 = trim(color1);
  }

  public String getColor1()
  {
    return color1;
  }

  public void setColor2(String color2)
  {
    if(valueChanged(this.color2, color2))
    {
      setChanged(true);
    }
    this.color2 = trim(color2);
  }

  public String getColor2()
  {
    return color2;
  }

  public void setSubstitutionIndicator(String substitutionIndicator)
  {
    if(valueChanged(this.substitutionIndicator, substitutionIndicator))
    {
      setChanged(true);
    }
    this.substitutionIndicator = trim(substitutionIndicator);
  }

  public String getSubstitutionIndicator()
  {
    return substitutionIndicator;
  }

  public void setSameDayGift(String sameDayGift)
  {
    if(valueChanged(this.sameDayGift, sameDayGift))
    {
      setChanged(true);
    }
    this.sameDayGift = trim(sameDayGift);
  }

  public String getSameDayGift()
  {
    return sameDayGift;
  }

  public void setOccasion(String occasion)
  {
    if(valueChanged(this.occasion, occasion))
    {
      setChanged(true);
    }
    this.occasion = trim(occasion);
  }

  public String getOccasion()
  {
    return occasion;
  }

  public void setCardMessage(String cardMessage)
  {
    if(valueChanged(this.cardMessage, cardMessage))
    {
      setChanged(true);
    }
    this.cardMessage = trim(cardMessage);
  }

  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setCardSignature(String cardSignature)
  {
    if(valueChanged(this.cardSignature, cardSignature))
    {
      setChanged(true);
    }
    this.cardSignature = trim(cardSignature);
  }

  public String getCardSignature()
  {
    return cardSignature;
  }

  public void setSpecialInstructions(String specialInstructions)
  {
    if(valueChanged(this.specialInstructions, specialInstructions))
    {
      setChanged(true);
    }
    this.specialInstructions = trim(specialInstructions);
  }

  public String getSpecialInstructions()
  {
    return specialInstructions;
  }

  public void setReleaseInfoIndicator(String releaseInfoIndicator)
  {
    if(valueChanged(this.releaseInfoIndicator, releaseInfoIndicator))
    {
      setChanged(true);
    }
    this.releaseInfoIndicator = trim(releaseInfoIndicator);
  }

  public String getReleaseInfoIndicator()
  {
    return releaseInfoIndicator;
  }

  public void setFloristId(String floristId)
  {
    if(valueChanged(this.floristId, floristId))
    {
      setChanged(true);
    }
    this.floristId = trim(floristId);
  }

  public String getFloristId()
  {
    return floristId;
  }

  public void setShipMethod(String shipMethod)
  {
    if(valueChanged(this.shipMethod, shipMethod))
    {
      setChanged(true);
    }
    this.shipMethod = trim(shipMethod);
  }

  public String getShipMethod()
  {
    return shipMethod;
  }

  public void setShipDate(Date shipDate)
  {
    if(valueChanged(this.shipDate, shipDate))
    {
      setChanged(true);
    }
    this.shipDate = shipDate;
  }

  public Date getShipDate()
  {
    return shipDate;
  }

  public void setOrderDispCode(String orderDispCode)
  {
    if(valueChanged(this.orderDispCode, orderDispCode))
    {
      setChanged(true);
    }
    this.orderDispCode = trim(orderDispCode);
  }

  public String getOrderDispCode()
  {
    return orderDispCode;
  }
  
  public void setSecondChoiceProduct(String secondChoiceProduct)
  {
    if(valueChanged(this.secondChoiceProduct, secondChoiceProduct))
    {
      setChanged(true);
    }
    this.secondChoiceProduct = trim(secondChoiceProduct);
  }

  public String getSecondChoiceProduct()
  {
    return secondChoiceProduct;
  }
  
  public void setZipQueueCount(long zipQueueCount)
  {
    if(valueChanged(this.zipQueueCount, zipQueueCount))
    {
      setChanged(true);
    }
    this.zipQueueCount = zipQueueCount;
  }

  public long getZipQueueCount()
  {
    return zipQueueCount;
  }
   
  public void setLpOrderIndicator(String lpOrderIndicator)
  {
    if(valueChanged(this.lpOrderIndicator, lpOrderIndicator))
    {
      setChanged(true);
    }
    this.lpOrderIndicator = trim(lpOrderIndicator);
  }

  public String getLpOrderIndicator()
  {
    return lpOrderIndicator;
  }
  
  public void setDeliveryDateRangeEnd(Date deliveryDateRangeEnd)
  {
    if(valueChanged(this.deliveryDateRangeEnd, deliveryDateRangeEnd))
    {
      setChanged(true);
    }
    this.deliveryDateRangeEnd = deliveryDateRangeEnd;
  }


  public Date getDeliveryDateRangeEnd()
  {
    return deliveryDateRangeEnd;
  }


  public void setScrubbedOn(Date scrubbedOn)
  {
    if(valueChanged(this.scrubbedOn, scrubbedOn))
    {
      setChanged(true);
    }
    this.scrubbedOn = scrubbedOn;
  }


  public Date getScrubbedOn()
  {
    return scrubbedOn;
  }


  public void setScrubbedBy(String scrubbedBy)
  {
    if(valueChanged(this.scrubbedBy, scrubbedBy))
    {
      setChanged(true);
    }
    this.scrubbedBy = trim(scrubbedBy);
  }


  public String getScrubbedBy()
  {
    return scrubbedBy;
  }


  public void setAribaUnspscCode(String aribaUnspscCode)
  {
    if(valueChanged(this.aribaUnspscCode, aribaUnspscCode))
    {
      setChanged(true);
    }
    this.aribaUnspscCode = trim(aribaUnspscCode);
  }


  public String getAribaUnspscCode()
  {
    return aribaUnspscCode;
  }


  public void setAribaPoNumber(String aribaPoNumber)
  {
    if(valueChanged(this.aribaPoNumber, aribaPoNumber))
    {
      setChanged(true);
    }
    this.aribaPoNumber = trim(aribaPoNumber);
  }


  public String getAribaPoNumber()
  {
    return aribaPoNumber;
  }


  public void setAribaAmsProjectCode(String aribaAmsProjectCode)
  {
    if(valueChanged(this.aribaAmsProjectCode, aribaAmsProjectCode))
    {
      setChanged(true);
    }
    this.aribaAmsProjectCode = trim(aribaAmsProjectCode);
  }


  public String getAribaAmsProjectCode()
  {
    return aribaAmsProjectCode;
  }


  public void setAribaCostCenter(String aribaCostCenter)
  {
    if(valueChanged(this.aribaCostCenter, aribaCostCenter))
    {
      setChanged(true);
    }
    this.aribaCostCenter = trim(aribaCostCenter);
  }


  public String getAribaCostCenter()
  {
    return aribaCostCenter;
  }


  public void setSizeIndicator(String sizeIndicator)
  {
    if(valueChanged(this.sizeIndicator, sizeIndicator))
    {
      setChanged(true);
    }
    this.sizeIndicator = trim(sizeIndicator);
  }


  public String getSizeIndicator()
  {
    return sizeIndicator;
  }


  public void setMilesPoints(long milesPoints)
  {
    if(valueChanged(this.milesPoints, milesPoints))
    {
      setChanged(true);
    }
    this.milesPoints = milesPoints;
  }


  public long getMilesPoints()
  {
    return milesPoints;
  }


  public void setSubcode(String subcode)
  {
    if(valueChanged(this.subcode, subcode))
    {
      setChanged(true);
    }
    this.subcode = trim(subcode);
  }


  public String getSubcode()
  {
    return subcode;
  }
 
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setRejectRetryCount(long rejectRetryCount)
  {
    this.rejectRetryCount = rejectRetryCount;
  }


  public long getRejectRetryCount()
  {
    return rejectRetryCount;
  }
  
  public String getCarrierDelivery()
  {
    return carrierDelivery;
  }
 
  public void setCarrierDelivery(String carrierDelivery)
  {
    if(valueChanged(this.carrierDelivery, carrierDelivery))
    {
      setChanged(true);
    }
    this.carrierDelivery = trim(carrierDelivery);
  }
  
  public String getCarrierId()
  {
    return carrierId;
  }
 
  public void setCarrierId(String carrierId)
  {
    if(valueChanged(this.carrierId, carrierId))
    {
      setChanged(true);
    }
    this.carrierId = trim(carrierId);
  }
  
  public String getVenusMethodOfPayment()
  {
    return venusMethodOfPayment;
  }
 
  public void setVenusMethodOfPayment(String venusMethodOfPayment)
  {
    if(valueChanged(this.venusMethodOfPayment, venusMethodOfPayment))
    {
      setChanged(true);
    }
    this.venusMethodOfPayment = trim(venusMethodOfPayment);
  }

  public void setOpStatus(String opStatus)
  {
    this.opStatus = opStatus;
  }

  public String getOpStatus()
  {
    return opStatus;
  }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }
}
