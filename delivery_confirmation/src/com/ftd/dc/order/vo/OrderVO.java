package com.ftd.dc.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class OrderVO extends BaseVO
{
  private	String	  orderGuid;
  private	String	  masterOrderNumber;
  private	long 	    customerId;
  private	long	    membershipId;
  private	String	  companyId;
  private	String	  sourceCode;
  private	String	  originId;
  private	Date    	orderDate;
  private	double	  orderTotal;
  private	double	  productTotal;
  private	double	  addOnTotal;
  private	double	  serviceFeeTotal;
  private	double	  shippingFeeTotal;
  private	double	  discountTotal;
  private	double	  taxTotal;
  private	String	  lossPreventionIndicator;
  private String    fraudFlag;
  private List      orderDetailVOList;
  private List      orderHistoryVOList;
  private List      paymentVOList;
  private List      commentsVOList;

  public OrderVO()
  {
    orderDetailVOList   = new ArrayList();
    orderHistoryVOList  = new ArrayList();
    paymentVOList       = new ArrayList();
    commentsVOList      = new ArrayList();
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = trim(companyId);
  }


  public String getCompanyId()
  {
    return companyId;
  }

  public void setMasterOrderNumber(String masterOrderNumber)
  {
    if(valueChanged(this.masterOrderNumber, masterOrderNumber))
    {
      setChanged(true);
    }
    this.masterOrderNumber = trim(masterOrderNumber);
  }


  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }


  public void setMembershipId(long membershipId)
  {
    if(valueChanged(this.membershipId, membershipId))
    {
      setChanged(true);
    }
    this.membershipId = membershipId;
  }


  public long getMembershipId()
  {
    return membershipId;
  }


  public void setSourceCode(String sourceCode)
  {
    if(valueChanged(this.sourceCode, sourceCode))
    {
      setChanged(true);
    }
    this.sourceCode = trim(sourceCode);
  }


  public String getSourceCode()
  {
    return sourceCode;
  }


  public void setOriginId(String originId)
  {
    if(valueChanged(this.originId, originId))
    {
      setChanged(true);
    }
    this.originId = trim(originId);
  }


  public String getOriginId()
  {
    return originId;
  }


  public void setOrderDate(Date orderDate)
  {
    if(valueChanged(this.orderDate, orderDate))
    {
      setChanged(true);
    }
    this.orderDate = orderDate;
  }


  public Date getOrderDate()
  {
    return orderDate;
  }


  public void setOrderTotal(double orderTotal)
  {
    if(valueChanged(new Double(this.orderTotal), new Double(orderTotal)))
    {
      setChanged(true);
    }
    this.orderTotal = orderTotal;
  }


  public double getOrderTotal()
  {
    return orderTotal;
  }


  public void setProductTotal(double productTotal)
  {
    if(valueChanged(new Double(this.productTotal), new Double(productTotal)))
    {
      setChanged(true);
    }
    this.productTotal = productTotal;
  }


  public double getProductTotal()
  {
    return productTotal;
  }


  public void setAddOnTotal(double addOnTotal)
  {
    if(valueChanged(new Double(this.addOnTotal), new Double(addOnTotal)))
    {
      setChanged(true);
    }
    this.addOnTotal = addOnTotal;
  }


  public double getAddOnTotal()
  {
    return addOnTotal;
  }


  public void setServiceFeeTotal(double serviceFeeTotal)
  {
    if(valueChanged(new Double(this.serviceFeeTotal), new Double(serviceFeeTotal)))
    {
      setChanged(true);
    }
    this.serviceFeeTotal = serviceFeeTotal;
  }


  public double getServiceFeeTotal()
  {
    return serviceFeeTotal;
  }


  public void setShippingFeeTotal(double shippingFeeTotal)
  {
    if(valueChanged(new Double(this.shippingFeeTotal), new Double(shippingFeeTotal)))
    {
      setChanged(true);
    }
    this.shippingFeeTotal = shippingFeeTotal;
  }


  public double getShippingFeeTotal()
  {
    return shippingFeeTotal;
  }


  public void setDiscountTotal(double discountTotal)
  {
    if(valueChanged(new Double(this.discountTotal), new Double(discountTotal)))
    {
      setChanged(true);
    }
    this.discountTotal = discountTotal;
  }


  public double getDiscountTotal()
  {
    return discountTotal;
  }


  public void setTaxTotal(double taxTotal)
  {
    if(valueChanged(new Double(this.taxTotal), new Double(taxTotal)))
    {
      setChanged(true);
    }
    this.taxTotal = taxTotal;
  }


  public double getTaxTotal()
  {
    return taxTotal;
  }


  public void setLossPreventionIndicator(String lossPreventionIndicator)
  {
    if(valueChanged(this.lossPreventionIndicator, lossPreventionIndicator))
    {
      setChanged(true);
    }
    this.lossPreventionIndicator = trim(lossPreventionIndicator);
  }


  public String getLossPreventionIndicator()
  {
    return lossPreventionIndicator;
  }


  public void setOrderDetailVOList(List orderDetailVOList)
  {
		if(orderDetailVOList != null)
		{
      Iterator it = orderDetailVOList.iterator();
      while(it.hasNext())
      {
        OrderDetailVO orderDetailVO = (OrderDetailVO) it.next();
        orderDetailVO.setChanged(true);
      }
		}
    this.orderDetailVOList = orderDetailVOList;
  }


  public List getOrderDetailVOList()
  {
    return orderDetailVOList;
  }


  public void setOrderHistoryVOList(List orderHistoryVOList)
  {
		if(orderHistoryVOList != null)
		{
      Iterator it = orderHistoryVOList.iterator();
      while(it.hasNext())
      {
        OrderHistoryVO orderHistoryVO = (OrderHistoryVO) it.next();
        orderHistoryVO.setChanged(true);
      }
		}
    this.orderHistoryVOList = orderHistoryVOList;
  }


  public List getOrderHistoryVOList()
  {
    return orderHistoryVOList;
  }

  public List getPaymentVOList()
  {
    return paymentVOList;
  }


  public List getCommentsVOList()
  {
    return commentsVOList;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setFraudFlag(String fraudFlag)
  {
    this.fraudFlag = fraudFlag;
  }


  public String getFraudFlag()
  {
    return fraudFlag;
  }


}
