package com.ftd.dc.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class AltContactVO extends BaseVO
{
  private long 		  customerId;
  private String 		name;
  private String 		email;
  private String		phoneNumber;
  private String		extension;
  private Date 	createdOn;
  private String 		createdBy;
  private Date 	updatedOn;
  private String 		updatedBy;


  public AltContactVO()
  {
  }

  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }

  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setName(String name)
  {
    if(valueChanged(this.name, name))
    {
      setChanged(true);
    }
    this.name = trim(name);
  }


  public String getName()
  {
    return name;
  }


  public void setEmail(String email)
  {
    if(valueChanged(this.email, email))
    {
      setChanged(true);
    }
    this.email = trim(email);
  }


  public String getEmail()
  {
    return email;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    if(valueChanged(this.phoneNumber, phoneNumber))
    {
      setChanged(true);
    }
    this.phoneNumber = trim(phoneNumber);
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setExtension(String extension)
  {
    if(valueChanged(this.extension, extension))
    {
      setChanged(true);
    }
    this.extension = trim(extension);
  }


  public String getExtension()
  {
    return extension;
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}
