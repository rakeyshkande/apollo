package com.ftd.dc.order.dao;

import com.ftd.dc.constants.DCConstants;
import com.ftd.dc.order.constants.OrderConstants;
import com.ftd.dc.order.vo.CustomerPhoneVO;
import com.ftd.dc.order.vo.CustomerVO;
import com.ftd.dc.order.vo.OrderDetailVO;
import com.ftd.dc.order.vo.OrderTrackingVO;
import com.ftd.dc.order.vo.OrderVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * OrderDAO
 *
 * This is the DAO that handles inserting florist information, updating
 * order information and retrieving information needed for order processing.
 *
 * @author Nicole Roberts
 */
public class

OrderDAO {
    private Logger logger;

    /**
     * Constructor
     *
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public OrderDAO() {
        logger = new Logger(DCConstants.LOGGER_CATEGORY_ORDERDAO);
    }

    /*
   * Get the Long representation of a long.  If numeric value is zero
   * then return null.
   */

    private String getLong(long longValue) {
        return longValue == 0 ? null : Long.toString(longValue);
    }

    /*
   * Get the Long representation of a long.  If numeric value is zero
   * then return null.
   */

    private Double getDouble(Object obj) {
        return obj == null ? null : new Double(obj.toString());
    }



        /**
       * This method is a wrapper for the GET_ORDER_DETAILS SP.
       * It populates an Order Detail VO based on the returned record set.
       * (Taken from com.ftd.op.order.dao.OrderDAO)
       *
       * @param conn database connection
       * @param orderDetailID to retrieve
       * @return OrderDetailVO
       */
        public OrderDetailVO getOrderDetail(Connection conn, String orderDetailID) throws Exception {
            DataRequest dataRequest = new DataRequest();
            OrderDetailVO orderDetail = new OrderDetailVO();
            boolean isEmpty = true;
            CachedResultSet outputs = null;
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

            try {
                logger.debug("getOrderDetail (String orderDetailID(" + 
                             orderDetailID + ")) :: OrderDetailVO");
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(OrderConstants.ORDER_DETAIL_ID, 
                                new Long(orderDetailID));

                /* build DataRequest object */
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("GET_ORDER_DETAILS");
                dataRequest.setInputParams(inputParams);

                /* execute the store prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

                /* populate object */
                while (outputs.next()) {
                    isEmpty = false;
                    orderDetail.setOrderDetailId(outputs.getLong(OrderConstants.ORDER_DETAIL_ID));
                    orderDetail.setDeliveryDate(outputs.getString(OrderConstants.DELIVERY_DATE) == 
                                                null ? null : 
                                                df.parse(outputs.getString(OrderConstants.DELIVERY_DATE)));
                    orderDetail.setRecipientId(outputs.getLong(OrderConstants.RECIPIENT_ID));
                    orderDetail.setProductId(outputs.getString(OrderConstants.PRODUCT_ID));
                    orderDetail.setQuantity(outputs.getLong(OrderConstants.QUANTITY));
                    orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
                    orderDetail.setColor1(outputs.getString(OrderConstants.COLOR_1));
                    orderDetail.setColor2(outputs.getString(OrderConstants.COLOR_2));
                    orderDetail.setSubstitutionIndicator(outputs.getString(OrderConstants.SUBSTITUTION_INDICATOR));
                    orderDetail.setSameDayGift(outputs.getString(OrderConstants.SAME_DAY_GIFT));
                    orderDetail.setOccasion(outputs.getString(OrderConstants.OCCASION));
                    orderDetail.setCardMessage(outputs.getString(OrderConstants.CARD_MESSAGE));
                    orderDetail.setCardSignature(outputs.getString(OrderConstants.CARD_SIGNATURE));
                    orderDetail.setSpecialInstructions(outputs.getString(OrderConstants.SPECIAL_INSTRUCTIONS));
                    orderDetail.setReleaseInfoIndicator(outputs.getString(OrderConstants.RELEASE_INFO_INDICATOR));
                    orderDetail.setFloristId(outputs.getString(OrderConstants.FLORIST_ID));
                    orderDetail.setShipMethod(outputs.getString(OrderConstants.SHIP_METHOD));
                    orderDetail.setShipDate(outputs.getDate(OrderConstants.SHIP_DATE));
                    orderDetail.setOrderDispCode(outputs.getString(OrderConstants.ORDER_DISP_CODE));
                    orderDetail.setDeliveryDateRangeEnd(outputs.getString(OrderConstants.DELIVERY_DATE_RANGE_END) == 
                                                        null ? null : 
                                                        df.parse(outputs.getString(OrderConstants.DELIVERY_DATE_RANGE_END)));
                    orderDetail.setScrubbedOn(outputs.getDate(OrderConstants.SCRUBBED_ON_DATE));
                    orderDetail.setScrubbedBy(outputs.getString(OrderConstants.USER_ID));
                    orderDetail.setOrderGuid(outputs.getString(OrderConstants.ORDER_GUID));
                    orderDetail.setSourceCode(outputs.getString(OrderConstants.SOURCE_CODE));
                    orderDetail.setSecondChoiceProduct(outputs.getString("SECOND_CHOICE_PRODUCT"));
                    orderDetail.setRejectRetryCount(outputs.getLong("REJECT_RETRY_COUNT"));
                    orderDetail.setSizeIndicator(outputs.getString("SIZE_INDICATOR"));
                    orderDetail.setSubcode(outputs.getString("SUBCODE"));
                    orderDetail.setOpStatus(outputs.getString("OP_STATUS"));
                    orderDetail.setCarrierDelivery(outputs.getString("CARRIER_DELIVERY"));
                    orderDetail.setCarrierId(outputs.getString("CARRIER_ID"));
                    orderDetail.setVenusMethodOfPayment(outputs.getString("METHOD_OF_PAYMENT"));
                }
            } catch (Exception e) {
                logger.error(e);
                throw e;
            }
            if (isEmpty)
                return null;
            else
                return orderDetail;
        }


        /**
       * This method is a wrapper for the SP_GET_ORDER SP.
       * It populates a Order VO based on the returned record set.
       * (Taken from com.ftd.op.order.dao.OrderDAO)
       *
       * @param conn database connection
       * @param orderGuid filter
       * @return OrderVO
       */
        public OrderVO getOrder(Connection conn, String orderGuid) throws Exception {
            DataRequest dataRequest = new DataRequest();
            OrderVO order = new OrderVO();
            boolean isEmpty = true;
            CachedResultSet outputs = null;

            try {
                logger.debug("getOrder (String orderGuid) :: OrderVO");
                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(OrderConstants.ORDER_GUID, orderGuid);

                /* build DataRequest object */
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("GET_ORDER_BY_GUID");
                dataRequest.setInputParams(inputParams);

                /* execute the store prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

                /* populate object */
                while (outputs.next()) {
                    isEmpty = false;
                    order.setMasterOrderNumber(outputs.getString(OrderConstants.MASTER_ORDER_NUMBER));
                    order.setCustomerId(outputs.getLong(OrderConstants.CUSTOMER_ID));
                    order.setMembershipId(outputs.getLong(OrderConstants.MEMBERSHIP_ID));
                    order.setCompanyId(outputs.getString(OrderConstants.COMPANY_ID));
                    order.setSourceCode(outputs.getString(OrderConstants.SOURCE_CODE));
                    order.setOriginId(outputs.getString(OrderConstants.ORIGIN_ID));
                    order.setOrderDate(outputs.getDate(OrderConstants.ORDER_DATE));
                    order.setOrderTotal(outputs.getDouble(OrderConstants.ORDER_TOTAL));
                    order.setProductTotal(outputs.getDouble(OrderConstants.PRODUCT_TOTAL));
                    order.setAddOnTotal(outputs.getDouble(OrderConstants.ADD_ON_TOTAL));
                    order.setServiceFeeTotal(outputs.getDouble(OrderConstants.SERVICE_FEE_TOTAL));
                    order.setShippingFeeTotal(outputs.getDouble(OrderConstants.SHIPPING_FEE_TOTAL));
                    order.setDiscountTotal(outputs.getDouble(OrderConstants.DISCOUNT_TOTAL));
                    order.setTaxTotal(outputs.getDouble(OrderConstants.TAX_TOTAL));
                    order.setLossPreventionIndicator(outputs.getString(OrderConstants.LOSS_PREVENTION_INDICATOR));
                    order.setFraudFlag(outputs.getString("FRAUD_INDICATOR"));
                }
            } catch (Exception e) {
                logger.error(e);
                throw e;
            }
            if (isEmpty)
                return null;
            else
                return order;
        }



        /**
       * This method is a wrapper for the SP_GET_CUSTOMER SP.
       * It populates a customer VO based on the returned record set.
       * (Taken from com.ftd.op.order.dao.OrderDAO)
       *
       * @param conn database connection
       * @param customerId to locate
       * @return CustomerVO
       */
        public CustomerVO getCustomer(Connection conn, long customerId) throws Exception {
            DataRequest dataRequest = new DataRequest();
            CustomerVO customer = new CustomerVO();
            boolean isEmpty = true;
            CachedResultSet outputs = null;
            CachedResultSet outputs2 = null;

            try {
                logger.debug("getCustomer (long customerId (" + customerId + 
                             ")) :: CustomerVO");

                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(OrderConstants.CUSTOMER_ID, new Long(customerId));

                /* build DataRequest object */
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("GET_CUSTOMER");
                dataRequest.setInputParams(inputParams);

                /* execute the store prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

                /* populate object */
                customer.setCustomerId(new Long(customerId).longValue());
                while (outputs.next()) {
                    isEmpty = false;
                    customer.setConcatId(outputs.getString(OrderConstants.CONCAT_ID));
                    customer.setFirstName(outputs.getString(OrderConstants.CUSTOMER_FIRST_NAME));
                    customer.setLastName(outputs.getString(OrderConstants.CUSTOMER_LAST_NAME));
                    customer.setBusinessName(outputs.getString(OrderConstants.BUSINESS_NAME));
                    customer.setAddress1(outputs.getString(OrderConstants.CUSTOMER_ADDRESS_1));
                    customer.setAddress2(outputs.getString(OrderConstants.CUSTOMER_ADDRESS_2));
                    customer.setCity(outputs.getString(OrderConstants.CUSTOMER_CITY));
                    customer.setState(outputs.getString(OrderConstants.CUSTOMER_STATE));

                    // fixed to deal with international zip_code of N/A

                    String zip_code = 
                        outputs.getString(OrderConstants.CUSTOMER_ZIP_CODE);
                    if (zip_code != null) {
                        zip_code = 
                                zip_code.substring(0, zip_code.length() >= 5 ? 5 : 
                                                      zip_code.length());
                        customer.setZipCode(zip_code);
                    }

                    customer.setCountry(outputs.getString(OrderConstants.CUSTOMER_COUNTRY));
                    customer.setAddressType(outputs.getString("address_type"));
                }

                /* setup store procedure input parameters */
                HashMap inputParams2 = new HashMap();
                inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());

                /* build DataRequest object */
                DataRequest dataRequest2 = new DataRequest();
                dataRequest2.setConnection(conn);
                dataRequest2.setStatementID("GET_CUSTOMER_PHONES");
                dataRequest2.setInputParams(inputParams2);

                /* execute the store prodcedure */
                outputs2 = (CachedResultSet)dataAccessUtil.execute(dataRequest2);

                /* populate object */
                List phoneList = new ArrayList();
                while (outputs2.next()) {
                    CustomerPhoneVO phoneVO = new CustomerPhoneVO();
                    phoneVO.setCustomerId(customerId);
                    phoneVO.setExtension(outputs2.getString("CUSTOMER_EXTENSION"));
                    phoneVO.setPhoneId(outputs2.getLong("CUSTOMER_PHONE_ID"));
                    phoneVO.setPhoneNumber(outputs2.getString("CUSTOMER_PHONE_NUMBER"));
                    phoneVO.setPhoneType(outputs2.getString("CUSTOMER_PHONE_TYPE"));
                    phoneList.add(phoneVO);
                }
                customer.setCustomerPhoneVOList(phoneList);

            } catch (Exception e) {
                logger.error(e);
                throw e;
            }
            if (isEmpty)
                return null;
            else
                return customer;
        }


      /**
       * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_ORDER_TRACKING
       * (Taken from com.ftd.op.order.dao.OrderDAO)
       *
       * @throws java.lang.Exception
       * @return OrderTrackingVO
       * @param orderDetailId
       */
        public OrderTrackingVO getTrackingInfo(Connection conn, long orderDetailId) throws Exception {
            CachedResultSet results = null;
            OrderTrackingVO trackingVO = null;
            List resultList = new ArrayList();
            DataRequest dataRequest = new DataRequest();
            HashMap inParms = new HashMap();

            inParms.put("IN_ORDER_DETAIL_ID", Long.toString(orderDetailId));

            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_ORDER_TRACKING");
            dataRequest.setInputParams(inParms);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            if (results.next()) {
                trackingVO = new OrderTrackingVO();
                trackingVO.setOrderDetailId(Long.parseLong(results.getObject(1).toString()));
                trackingVO.setTrackingNumber(results.getObject(2).toString());
                trackingVO.setCarrierName(results.getObject(4).toString());
                trackingVO.setCarrierURL(results.getObject(5).toString());
                trackingVO.setCarrierPhone(results.getObject(6).toString());
                trackingVO.setTrackingURL(results.getObject(7).toString());

                resultList.add(trackingVO);
            }
            return trackingVO;
        }

  /**
   * findOrderNumber
   *
   * @param a_orderNum
   * @throws java.lang.Exception
   */
    public HashMap findOrderNumber(Connection conn,String a_orderNum) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("FIND_ORDER_NUMBER");
        dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Date startTime = new Date();
        HashMap searchResults = (HashMap)dataAccessUtil.execute(dataRequest);
        long timeInMs = (System.currentTimeMillis() - startTime.getTime());
        logger.debug("***STRESS TEST***  - findOrderNumber() - CLEAN.ORDER_QUERY_PKG.FIND_ORDER_NUMBER took " + 
                     timeInMs + " milliseconds.");

        return searchResults;
    }
    
    
    /**
     * This is a wrapper for CLEAN.CUSTOMER_QUERY_PKG.IS_EMAIL_ADDRESS_ASSOCIATED
     * @param orderDetailId
     * @param emailAddress
     * @throws java.lang.Exception
     */
    public boolean isEmailAddressAssociated(Connection conn,String orderDetailId, String emailAddress) throws Exception {
        boolean found = false;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("IS_EMAIL_ADDRESS_ASSOCIATED");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.addInputParam("IN_EMAIL_ADDRESS", emailAddress);

        /* execute the stored procedure */
        CachedResultSet results = null;
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String outputs = (String)dataAccessUtil.execute(dataRequest);
        logger.debug("Email Address Match=" + outputs);
        if(outputs.equalsIgnoreCase(DCConstants.YES)) {
        logger.debug("Email Address Match found");
            found = true;
        }
        return found;        
    }
    

      public CachedResultSet getOrderCustomerInfo(Connection conn,String orderDetailId) throws Exception
      {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_ORDER_CUSTOMER_INFO");
            dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            return rs;
      }
      
    /**
     * method to search for a shopping cart based on the input data
     *
     * @param session id
     * @param customer service representative id
     * @param master order number
     * @param order guid
     * @param start position
     * @param maxRecords
     *
     * @return HashMap containing cursors and output parameters
     *
     * @throws java.lang.Exception
     */

    public HashMap getOrderInfoForPrint(Connection conn,String orderDetailId, String includeComments, String processingId)
            throws Exception
    {
      HashMap searchResults = new HashMap();
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_ORDER_INFO_FOR_PRINT");

      dataRequest.addInputParam("IN_ORDER_DETAIL_ID",       orderDetailId);
      dataRequest.addInputParam("IN_INCLUDE_COMMENTS",        includeComments);
      dataRequest.addInputParam("IN_PROCESSING_ID",           processingId);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
      searchResults = (HashMap) dataAccessUtil.execute(dataRequest);

      return searchResults;

    }      
}

