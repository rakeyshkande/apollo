package com.ftd.dc.util;

import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.osp.utilities.plugins.Logger;


/**
* This locator class provides access to external or dependenct services.
* The class is a singleton.
*/
public class MessagingServiceLocator
{

  private static MessagingServiceLocator messagingServiceLocator;
  private Logger logger = new Logger("com.ftd.dc.util.MessagingServiceLocator"); 
 
  private MercuryAPIBO mercuryAPIBO;  

  /**
  * Private constructor to enforce singleton. 
  * 
  * @see #getInstance()
  */
  private MessagingServiceLocator() throws Exception
  {
     mercuryAPIBO = new MercuryAPIBO();  
  }

  /**
  * Initializes and returns a reference to the Locator singleton instance.
  */
  public static MessagingServiceLocator getInstance() throws Exception
  {
    if(messagingServiceLocator==null)
    {
      messagingServiceLocator=new MessagingServiceLocator();
    }
    return messagingServiceLocator;
  }

  /**
  * @return Reference to the Mercury API
  */
  public  MercuryAPIBO getMercuryAPI() throws Exception
  {
      return mercuryAPIBO;
  }
  
}