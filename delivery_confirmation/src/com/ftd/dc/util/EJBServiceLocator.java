package com.ftd.dc.util;

import com.ftd.dc.constants.DCConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.ejb.EJBHome;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;


/**
 * <p>
 * Encapsulates all JNDI usage and hides the complexities of initial context
 * creation, EJBHome object lookup, and EJBObject recreation. Multiple projects
 * can reuse this class to reduce code complexity, provide a single point of
 * control, and improve performance through use of a caching facility (reducing
 * the number of JNDI lookups required).
 * </p>
 *
 * @author  Jason Weiss
 * @version $Revision: 1.3 $, $Date: 2007/01/12 20:20:17 $
 */
public final class EJBServiceLocator {

    private Logger logger = new Logger(this.getClass().getName());
    /** EJB create method*/
    private static final String CREATE = "create";

    /** Constant for initial cache size. Set this to the number of Beans you want to cache*/
    private static final int INITIAL_CACHE_SIZE = 15;

    private String EJB_PROVIDER_URL;
    private String INITIAL_CONTEXT_FACTORY;

    /** Initial context */
    private static Context sContext;

    /** A reference to the single instance of this class. */
    private static EJBServiceLocator sInstance;

    /** The cache of EJBHome objects. */
    private Map mEjbHomeCache = new HashMap(INITIAL_CACHE_SIZE);
    private boolean useDomainFix;

    /**
     * Default constructor. Private as this class has been implemented as a
     * Singleton. Sets the JNDI InitialContext.
     *
     * @throws Exception when initializing the ejb context.
     */
    private EJBServiceLocator() throws Exception {
            useDomainFix = false;
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            EJB_PROVIDER_URL = config.getFrpGlobalParm(DCConstants.SERVICE_LOCATOR_CONFIG_CONTEXT,"EJB_PROVIDER_URL");
            INITIAL_CONTEXT_FACTORY = config.getProperty(DCConstants.SERVICE_LOCATOR_CONFIG_FILE, "INITIAL_CONTEXT_FACTORY");
            sContext = getInitialContext();
    }
    private EJBServiceLocator(boolean domainFix) throws Exception
    {
            useDomainFix = domainFix;
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            EJB_PROVIDER_URL = config.getFrpGlobalParm(DCConstants.SERVICE_LOCATOR_CONFIG_CONTEXT,"EJB_PROVIDER_URL");
            INITIAL_CONTEXT_FACTORY = config.getProperty(DCConstants.SERVICE_LOCATOR_CONFIG_FILE, "INITIAL_CONTEXT_FACTORY");
            sContext = getInitialContext();
    }
    /**
     * Implements the singleton pattern for this service locator.
     *
     * @return ServiceLocator returns the ServiceLocator instance, instantiating a new one as necessary
     *
     * @throws Exception
     */
    public static EJBServiceLocator getInstance()
        throws Exception {

        if (sInstance == null) {
            sInstance = new EJBServiceLocator();
        }

        return sInstance;
    }

  /**
   * Implements the singleton pattern for this service locator.
   *
   * @throws java.lang.Exception
   * @return
   * @param useFix true if we should use the dedicated.rmicontext fix
   */
    public static EJBServiceLocator getinstance(boolean useFix) throws Exception
    {
      if (sInstance == null)
      {
        sInstance = new EJBServiceLocator(useFix);
      }
      return sInstance;
    }
    /**
     * Gets the <code>EJBHome</code> object from an ejbHome cache if possible,
     * or via JNDI lookup otherwise for the specified jndiName.
     *
     * @param jndiHomeName the jndiName of the remote object.
     * @param homeName the classname.
     *
     * @return the <code>EJBHome</code> object.
     *
     * @exception Exception thrown if the EJBHome class cannot be found;
     *            thrown if a NamingException is encountered during lookup.
     */
    public EJBHome getRemoteHome(String jndiHomeName, String homeName)
        throws Exception {

        EJBHome home = (EJBHome) mEjbHomeCache.get(jndiHomeName);
        Object[] args = null; // define error message arguments

        if (home == null) {

            try {

                Object objref = sContext.lookup(jndiHomeName);
                Class homeClass = Class.forName(homeName);
                Object obj = PortableRemoteObject.narrow(objref, homeClass);

                home = (EJBHome) obj;
                mEjbHomeCache.put(jndiHomeName, home);
            } catch (CommunicationException ce) {
                throw ce;
            } catch (ClassCastException cce) {
              throw cce;
            } catch (NamingException ne) {
		throw ne;
            } catch (ClassNotFoundException cnfe) {
		throw cnfe;
            }
        } else
        {
          System.out.println("got home from cache");
        }

        return home;
    }

    /**
     * Gets the remote bean home
     *
     * @param jndiHomeName the jndi home name
     * @param className the class name
     *
     * @return remote bean
     *
     * @throws Exception if <code>Exception</code> occurs
     */
    public Object getRemoteBean(String jndiHomeName, String className)
        throws Exception {

        return createRemoteBean(jndiHomeName, className, true);
    }

    /**
     * This method instantiates a remote bean and returns an instance of the bean
     * for later method invocation.
     *
     * @param jndiHomeName The JNDI name of the bean to instantiate
     * @param className The class name of the bean to instantiate
     * @param reTry A flag which controls whether exceptions should be handled (by reattempting the call) or thrown to the caller.
     *
     * @return A remote bean instance
     *
     * @throws Exception This method throws exceptions if a remote instance of the required EJB cannot be instantiated.
     */
    private Object createRemoteBean(String jndiHomeName, String className, boolean reTry)
        throws Exception {

        logger.debug("InitialContext: "+sContext.getEnvironment());
        if(!reTry)
        {
          //re-initialize the initialContext.
          sContext = getInitialContext();

        }

        EJBHome home = (EJBHome) mEjbHomeCache.get(jndiHomeName);

        if (home == null) {
            home = getRemoteHome(jndiHomeName, className);
        }

        Object object = null;

        try {

            Method createMethod = home.getClass().getMethod(CREATE, null);

            object = createMethod.invoke(home, null);
        } catch (Exception e1) {

            //Clear the cache and try again to eliminate the invalid cache entries
            //due to things like server restart
            mEjbHomeCache.clear();

            if (reTry) {
                try {

                    return createRemoteBean(jndiHomeName, className, false);
                } catch (Exception e2) {
                    //App server seem to hold on to the socket, so need to try again
                    return createRemoteBean(jndiHomeName, className, false);
                }
            } else {
		throw e1;
            }
        }

        return object;
    }

    /**
     * Gets the initial context
     *
     * @return the initial context
     *
     * @throws NamingException if <code>NamingException</code> occurs.
     */
    public Context getInitialContext() throws NamingException {

        Hashtable p = new Hashtable();
        p.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        p.put(Context.SECURITY_CREDENTIALS, "welcome");
        p.put(Context.SECURITY_PRINCIPAL, "admin");
        p.put(Context.PROVIDER_URL, EJB_PROVIDER_URL);

        // put in to fix an domain issue when using from inside a servlet
        // see: http://www.oracle.com/technology/tech/java/oc4j/904/collateral/OC4J-FAQ-EJB-904.html
        //      #15 for details
        if (useDomainFix) {
          p.put("dedicated.rmicontext","true");
        }

        return new InitialContext(p);
    }
}