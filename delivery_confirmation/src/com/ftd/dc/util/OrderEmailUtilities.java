package com.ftd.dc.util;

import com.ftd.dc.constants.OrderEmailConstants;
import com.ftd.dc.dao.OrderEmailDAO;
import com.ftd.osp.utilities.WebloyaltyUtilities;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

/**
 * This class was initially created while implementing Webloyalty
 *
 * It will contain all the common methods that can be used and called upon from order_email
 *
 * @author Ali Lakhani
 *
 * 03/01/2007 Imported from the order_email project by tpeterson
 */
public class OrderEmailUtilities {
  private Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.osp.orderemail.utilities.OrderEmailUtilities";
  private static final String SOURCE_CODE_HANDLER_NAME = "CACHE_NAME_SOURCE_MASTER";
  private static final String GLOBAL_PARM_HANDLER = "CACHE_NAME_GLOBAL_PARM";

/*******************************************************************************************
 * Constructor - default constructor
 *******************************************************************************************/
  public OrderEmailUtilities()
  {
    this.logger = new Logger(LOGGER_CATEGORY);
  }


  /**
     * retrieve and return the source code info
     * @param conn
     * @param sourceCode
     * @return source code vo
     * @throws Exception
     */
  public SourceMasterVO retrieveSourceCodeInfo(Connection conn, String sourceCode) throws Exception
  {
    logger.debug("retrieveSourceCodeInfo: " + sourceCode)  ;
    //check source code
    SourceMasterVO sVO = null;

    // Get source code info from cache handler.  If nothing found return with error.
    try
    {
      SourceMasterHandler sch = (SourceMasterHandler) CacheManager.getInstance().getHandler(SOURCE_CODE_HANDLER_NAME);
      sVO = sch.getSourceCodeById(sourceCode);
    }
    catch (Exception e)
    {
      logger.info("WARNING - WARNING -------------------- Source code could not be retrieved from the cache  -------------------- ");
    }
    finally
    {
      if (sVO == null) {
        logger.debug("sVO is null, calling getSourceCodeRecord()");
        OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
        sVO = oeDAO.getSourceCodeRecord(sourceCode);
      }
    }

    if (sVO != null)
      return sVO;
    else
      throw new Exception("Source code not found");

  }

  /**
     * retrieve and return the global parm info for webloyalty
     * @param conn
     * @return Hashmap containing Webloyalty global parms
     * @throws Exception
     */
  public HashMap retrieveWebloyaltyGlobalParms(Connection conn)
    throws Exception
  {
    HashMap wlMap = new HashMap();
    Map globalMap = new HashMap();
    try
    {
      GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(GLOBAL_PARM_HANDLER);
      globalMap = gph.getFrpGlobalParmMap();
    }
    catch (Exception e)
    {
      logger.info("WARNING - WARNING -------------------- Global Parms could not be retrieved from the cache  -------------------- ");
    }
    finally
    {
      if (globalMap == null)
      {
        OrderEmailDAO oeDAO = new OrderEmailDAO(conn);
        globalMap = oeDAO.getGlobalParms();
      }
    }

    if (globalMap != null)
    {
      wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_SHIP_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_SHIP_CONF));
      wlMap.put(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
      wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_SHIP_CONF, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_SHIP_CONF));
      wlMap.put(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL, globalMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
      wlMap.put(OrderEmailConstants.HK_SERVER, globalMap.get(OrderEmailConstants.HK_SERVER));
      wlMap.put(OrderEmailConstants.HK_URL_PARAMETER_1, globalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1));
      return wlMap;
    }
    else
      throw new Exception("");

  }

  /**
     * create the unencrypted offer string
     *
     * @param wlOfferMap containing webloyalty offer data to be included as part of href
     * @return String that will contain webloyalty offer data to be included as part of href
     */
  public String createUnEncryptedOffer(HashMap wlOfferMap)
  {
    StringBuffer unEncryptedSB = new StringBuffer();

    char cTokenizer = OrderEmailConstants.DEFAULT_TOKENIZER;

    //append src literal
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_LITERAL));
    unEncryptedSB.append(cTokenizer);

    //append src data
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_SRC_ID_DATA_SHIP_CONF));
    unEncryptedSB.append(cTokenizer);

    //append pool literal
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_LITERAL));
    unEncryptedSB.append(cTokenizer);

    //append pool data
    unEncryptedSB.append(wlOfferMap.get(OrderEmailConstants.HK_OFFER_POOL_ID_DATA_SHIP_CONF));

    return unEncryptedSB.toString();

  }


  /**
     * process Web loyalty code, and return the URL with encrypted info.
     *
     * processing includes:
     *    retrieve and create an offer string (a Webloyalty URL parm) from the Global
     *    encrypt the offer string using Webloyalty encryptiong
     *    encrypt the master order number using MD5 encryption
     *    insert record in the CLEAN.webloyalty_master table
     *    create and return a url for webloyalty
     *
     * @param conn
     * @return source code vo
     */
  public String processWebloyalty(Connection conn) throws Exception
  {
    WebloyaltyUtilities wlUtil = new WebloyaltyUtilities();
    String wlUrl = "";
    HashMap wlGlobalMap;

    //variables uses to create offer
    String unEncryptedOffer = null;
    String wlEncryptedOffer = null;

    //retrieve the global parms
    wlGlobalMap = this.retrieveWebloyaltyGlobalParms(conn);

    //create unencrypted Offer string using the parms from wlMap
    unEncryptedOffer = this.createUnEncryptedOffer(wlGlobalMap);

    //encrypted the offer string
    wlEncryptedOffer = wlUtil.encrypt(unEncryptedOffer);

    //create the URL - Note that for the delivery confirmation email, we dont need the "b" parameter
    wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_SERVER));
    wlUrl += "?"; 
    wlUrl += ((String)wlGlobalMap.get(OrderEmailConstants.HK_URL_PARAMETER_1)); 
    wlUrl += "="; 
    wlUrl += wlEncryptedOffer;
    logger.debug("OrderEmailUtilities.processWebloyalty() - wlUrl>>>>>" + wlUrl + "<<<<<");

    return wlUrl;
  }
}
