<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:template match="/root">

<html>

<head>
    <title>FTD - OPS Administration</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"></script>

    <script type="text/javascript" language="javascript">
  	var app_context = '<xsl:value-of select="$applicationcontext"/>';
    var site_name = '<xsl:value-of select="$sitename"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
    
    var addonfeedGenerated = '<xsl:for-each select="/root/Addon/StatusMessage">
							  <xsl:value-of select="value" />
							  </xsl:for-each>';

    <![CDATA[

    /*
     *  Initializations
     */
      function init(){
        setNavigationHandlers();
        checkAddonStatus();
      }
     
    /*
     *  Actions
     */
      function getSecurityParams(){
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
                "&context=" + document.forms[0].context.value;
      }

      function doExitAction(){
        performAction("ExitServlet");
      }

      function doReinstateAction(selectedContext){
        document.forms[0].action = "ReinstateOrderServlet" + getSecurityParams();
        document.forms[0].selected_context.value = selectedContext;
        document.forms[0].submit();
      }

      function performAction(url){
        document.forms[0].action = url + getSecurityParams();
        document.forms[0].submit();
      }
      
  function performSslAction(servlet) {
    document.forms[0].action = "https://" + site_name_ssl + app_context + "/servlet/" + servlet;
		document.forms[0].submit();
	}
	
	function performSendAllAddonAction(url){
		
		var action;
		if(url === 'SendAllAddOnFeedServletToStore'){
			action = confirm("Do you want to Send All Add-Ons updates to store ?");
		}
		else{
			action = confirm("Do you want to Send All Add-Ons updates to novator ?");
		} 
		if(action== true)
		{		
			if(url === 'SendAllAddOnFeedServletToStore'){
				document.forms[0].send_to.value = 'store';
			}else{
				document.forms[0].send_to.value = 'novator';
			}
	        document.forms[0].action = url + getSecurityParams();
	        document.forms[0].submit();
        }
        else
        {
        	return false;
        }
      }
	
	function checkAddonStatus(){
	  if (addonfeedGenerated != null && addonfeedGenerated != '')
	  { 
		alert(addonfeedGenerated);	
	  }	  
      }
      ]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'OPS Administration'"/>
    <xsl:with-param name="showTime" select="'true'"/>
    <xsl:with-param name="showExitButton" select="'true'"/>
  </xsl:call-template>

  <form name="OpsAdminForm" method="get" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="selected_context" value=""/>
    <input type="hidden" name="send_to" value=""/>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('ManagerDashboardServlet');">Scrub Dashboard</a>
              </td>
              <td>Novator</td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('BoxXDashboardServlet');">Box X Dashboard</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('ProductUpdatesToNovator');">Send Product Updates</a>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('EAPIDashboardServlet');">EAPI Dashboard</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('NovatorVendorUpdateServlet');">Send Vendor Updates</a>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
              &nbsp;&nbsp;<a href="#" onclick="javascript:performSendAllAddonAction('SendAllAddOnFeedServletToStore');">Send All Add-On Updates To Store</a>
              </td>
            </tr>
             <tr>
              <td></td>
              <td>
              &nbsp;&nbsp;<a href="#" onclick="javascript:performSendAllAddonAction('SendAllAddOnFeedServletToNovator');">Send All Add-On Updates To Novator</a>
              </td>
            </tr>
            <tr>
              <td>State of the System</td>
              <td>Dropship</td>
            </tr>
            <tr>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('QueueCountsServlet');">Counts of JMS Queues</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('VenusOrderReprocessServlet');">Reprocess Orders</a>
              </td>
            </tr>
            <tr>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:doReinstateAction('');">Reinstate Order in JMS Workflow</a>
                &nbsp;&nbsp;<a href="#" onclick="javascript:doReinstateAction('none');">(Select context)</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('CarrierZipBlockingServlet');">Carrier Zip Blocking</a>
              </td>
            </tr>
            <tr>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('SystemMessagesServlet');">System Messages</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('InventoryTrackingUpdateServlet');">Update Inventory Tracking Counts</a>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('ConfigurationServlet');">Configuration</a>
              </td>
              <td>
                End Of Day
              </td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performSslAction('SecureConfigServlet');">Secure Configuration</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('EODUtilServlet');">EOD Utility</a>
              </td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('SchedulerServlet');">Scheduler</a>
              </td>
              <td>
                Content
              </td>
            </tr>
            <tr>
              <td>
                <a href="#" onclick="javascript:performAction('MercuryRetrievalServlet');">Mercury Retrieval</a>
              </td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('ContentSetup.do');">Content Context Setup</a>
              </td>
            </tr>
            
            
            <tr>
              <td>JBoss</td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('ContentDetail.do');">Content Context Detail Setup</a>
              </td>
            </tr>
           
           
            <tr>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('OrderServiceSimulatorServlet');">Order Service Simulator</a>
              </td>
              <td>
                Apollo Support
              </td>
            </tr>
            
            
            
            <tr>
              <td>Partner Integration</td>
              <td>
                &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('PageFilterList.do');">Page Filter</a>
              </td>
            </tr>
          
              <tr>
              <td> &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('ResendPartnerFeedServlet');">Resend Partner Feeds</a></td>
              </tr>
              
               <tr>
               <td>Groupon Refund</td>
              </tr>
              <tr>
              <td> &nbsp;&nbsp;<a href="#" onclick="javascript:performAction('GrouponRefundServlet');">Gift and Coupons Refund</a></td>
              </tr>
          
          </table>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>


</body>
</html>

</xsl:template>
</xsl:stylesheet>