<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<html>
  <head>  
    <title>Page Filter</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    
    <link rel="stylesheet" type="text/css" href="../css/ftd.css">
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/clock.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript" language="javascript">

var updateStatusIdArray = new Array();
var updateStatusValueArray = new Array();
var arrayCount = 0;  

function init() {
    if (document.getElementById('saveResult').value == 'success') {
        document.getElementById("instructions").innerHTML = "Changes were successfully saved";
        document.getElementById("instructions").className = "Instruction"
    } else {
        document.getElementById("instructions").innerHTML = document.getElementById('saveResult').value;
        document.getElementById("instructions").className = "ErrorMessage";
    }
}

function doEdit(filterId) {
    var form = document.forms[0];
    form.filterId.value = filterId;
    form.formAction.value = "display_edit";
    url="PageFilterEdit.do";
    performAction(url);
}

function doAdd() {
    var form = document.forms[0];
    form.filterId.value ="";
    form.formAction.value = "display_add";
    url="PageFilterEdit.do";
    performAction(url);
}

function doDelete(filterId) {
    var modalText = "Are you sure?";
    var modalDim = "dialogWidth:400px; dialogHeight:160px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
    var ret = window.showModalDialog("../confirm.html", modalText, modalDim);
    
    if (ret){
	    var form = document.forms[0];
	    form.filterId.value = filterId;
	    form.formAction.value = "delete";
	    url="PageFilterList.do";
	    performAction(url);
    }
}

function doFilter() {
    var form = document.forms[0];
    form.formAction.value = "search";
    url="PageFilterList.do";
    performAction(url);
}


function doStatusUpdate(filterId) {
	isChecked = document.getElementById("cb_" + filterId).checked;
	checkboxVal = "Y";
	if(!isChecked) {
		checkboxVal = "N";
	}
	updateStatusIdArray[arrayCount] = filterId;
	updateStatusValueArray[arrayCount] = checkboxVal;
	arrayCount++;  
}

function doUpdateStatusAction()
{
    if (arrayCount == 0) {
    	alert("No status change made.")
    }
    else {
	   	var updateStatusId;
	   	var updateStatusValue;
	    for (i=0; i< arrayCount; i++) {	      		
	      		// add hidden field for multi value - filter id
	      		updateStatusId = document.createElement("input");
	      		updateStatusId.type = "hidden"; 
	      		updateStatusId.name = "updateStatusIds"; 
	      		updateStatusId.id = "updateStatusIds"+i;
	      		updateStatusId.value = updateStatusIdArray[i];
	      		document.forms[0].appendChild(updateStatusId);
	      		
	      		// add hidden field for multi value - active flag
	      		updateStatusValue = document.createElement("input");
	      		updateStatusValue.type = "hidden"; 
	      		updateStatusValue.name = "updateStatusValues"; 
	      		updateStatusValue.id = "updateStatusValues"+i;
	      		updateStatusValue.value = updateStatusValueArray[i];
	      		document.forms[0].appendChild(updateStatusValue);
	    }
	    document.forms[0].formAction.value = "update_status";
	    url="PageFilterList.do";
	    performAction(url);	    
    }
}

    </script>
  </head>

  <body onload="init()">
    <c:set var="headerName" value="Page Filter List" />
    <%@ include file="header.jsp" %>

    <html:form action="/servlet/PageFilterList">
        <input type="hidden" name="filterId" value=""/>
        <input type="hidden" name="formAction" value=""/>
        <input type="hidden" id="saveResult" name="saveResult" value="${pageFilterListForm.saveResult}" />
        
        <%@ include file="security.jsp" %>

    <table width="98%" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
       	<td id="instructions"/>
      </tr>
      <tr>
        <td align="center">
            <label class="LabelRight">Action:&nbsp;</label>
            <html:select property="actionFilter" >
              <html:option value="" >All</html:option>
              <html:optionsCollection property="actionTypeList"
                            value="actionType"
                            label="actionType" />
            </html:select>
            &nbsp;&nbsp;
            <input type="button" name="filterButton" value="Filter" onclick="doFilter()"/>
        </td>
      </tr>
    </table>
    <div STYLE=" height: 350px; overflow: auto;">
    <table width="98%" border="3" align="left" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td align="center">
          <display:table id="pageFilterVO" name="pageFilterListForm.pageFilterList">
           <display:column title="Edit" headerClass="ColHeader">
              <center>
              <input type="button" name="editButton" value=" Edit " tabindex="2" onclick="doEdit(${pageFilterVO.filterId})"/>
              </center>
            </display:column>
            <display:column property="filterName" title="Filter Name" headerClass="ColHeader" />
            <display:column title="Active" headerClass="ColHeader">
              <input type="checkbox" id="cb_${pageFilterVO.filterId}" name="cb_${pageFilterVO.filterId}" value="${pageFilterVO.filterId}"
                <c:if test="${pageFilterVO.activeFlag == 'Y'}">checked="checked"</c:if>
                      onClick="doStatusUpdate(${pageFilterVO.filterId})"/>
            </display:column>
            <display:column style="width: 10" property="messageSource" title="Source" headerClass="ColHeader"/>
            <display:column property="messageSubject" title="Subject" headerClass="ColHeader"/>
            <display:column property="messageBody" title="Body" headerClass="ColHeader"/>
            <display:column property="actionType" title="Action" headerClass="ColHeader"/>
            <display:column property="nopageStartHour" title="Nopage Start" headerClass="ColHeader"/>
            <display:column property="nopageEndHour" title="Nopage End" headerClass="ColHeader"/>
            <display:column property="subjectPrefix" title="Subject Prefix" headerClass="ColHeader"/>
            <display:column property="overwriteProject" title="Overwrite Distro" headerClass="ColHeader"/>
            <display:column title="Delete" headerClass="ColHeader">
              <center>
              <input type="button" name="deleteButton" value="Delete" tabindex="2" onclick="doDelete(${pageFilterVO.filterId})"/>
              </center>
            </display:column>            

          </display:table>
        </td>
      </tr>
    </table>
    </div>
    <table align="center">
      <tr>
        <td align="center">
          <input type="button" name="save" value="Save" tabindex="10" onclick="doUpdateStatusAction()"/>
          <input type="button" name="addButton" value=" Add " tabindex="20" onclick="doAdd()"/>
        </td>      
      </tr>
    </table>
    <%@ include file="footer.jsp" %>
    </html:form>
  </body>
</html>
