<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<html>
  <head>  
    <title>Page Filte Add/Edit</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../css/ftd.css">
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/clock.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript" language="javascript">


    
function init() {
    if (document.getElementById('saveResult').value == 'success') {
        document.getElementById("instructions").innerHTML = "Changes were successfully saved";
        document.getElementById("instructions").className = "Instruction"
    } else {
        document.getElementById("instructions").innerHTML = document.getElementById('saveResult').value;
        document.getElementById("instructions").className = "ErrorMessage";
    }
}
function doSubmit() {
    var form = document.forms[0];
    var msg = "";

    var filterName = document.getElementById("filterName").value;
    if (filterName == "") {
        msg = msg + "Page Filter Name is required\n";
    }
    var msgSource = document.getElementById("msgSource").value;
    var msgSubject = document.getElementById("msgSubject").value;
    var msgBody = document.getElementById("msgBody").value;
    
    if (msgSource == "" && msgSubject == "" && msgBody == "") {
        msg = msg + "At least one filter field is required\n";
    }
    
    var selectedAct;
    var actSelected = false;
    for(var i=0; i < form.filterAction.length; i++) {
    	if(form.filterAction[i].checked) {
    		selectedAct = form.filterAction[i].value;
    		actSelected = true;
    		break;
    	}
    }

    if (!actSelected) {
    	msg = msg + "A filter action is required\n";
    }
    if((selectedAct == "SUPPRESS" || selectedAct == "EXCL_FROM_ON_OFF_SWITCH") &&
       (form.nopageStartHour.value != "" ||form.nopageEndHour.value != "" ||
			form.subjectPrefix.value != "" || form.overwriteDistro.value != "" )
      ) 
    {
    	msg = msg + "Please only enter parameters related to selected filter action\n";
    }
    else if ((selectedAct == "NOPAGE") && 
    		 (form.subjectPrefix.value != "" || form.overwriteDistro.value != "" )) {
    	msg = msg + "Please only enter parameters related to selected filter action\n";
    }
    else if (selectedAct == "OVERWRITE_DISTRO") {
    	if (form.nopageStartHour.value != "" ||form.nopageEndHour.value != "" ||
    					form.subjectPrefix.value != "") {
    		msg = msg + "Please only enter parameters related to selected filter action\n";
    	} else if(form.overwriteDistro.value == "") {
    		msg = msg + "Please enter overwrite distro.\n"
    	}
    } 
    else if (selectedAct == "MODIFY_SUBJECT") {
   		if (form.nopageStartHour.value != "" ||form.nopageEndHour.value != "" ||
   					form.overwriteDistro.value != "") {
   			msg = msg + "Please only enter parameters related to selected filter action\n";
   		} else if(form.subjectPrefix.value == "") {
    		msg = msg + "Please enter prepending subject text.\n"
    	}
   }
    

    if (msg != "" ) {
        alert(msg);
        return false;
    }
    
    form.formAction.value = "edit_save";
    url="PageFilterEdit.do";
    performAction(url);
}

function doCancel() {
    url="PageFilterList.do";
    performAction(url);
}

function doBackAction(){
    document.forms[0].method="get";
    performAction("PageFilterList.do");
}

function doRadioCheck(obj){
    var form = document.forms[0];
    var selectedAct = obj.value;
    if(selectedAct == "SUPPRESS" || selectedAct == "EXCL_FROM_ON_OFF_SWITCH") {
    	form.nopageStartHour.value = "";
    	form.nopageEndHour.value = "";
    	form.subjectPrefix.value = "";
    	form.overwriteDistro.value = "";
    }
    else if(selectedAct == "NOPAGE") {
    	form.subjectPrefix.value = "";
    	form.overwriteDistro.value = "";
    }
    else if(selectedAct == "OVERWRITE_DISTRO") {
    	form.nopageStartHour.value = "";
    	form.nopageEndHour.value = "";
    	form.subjectPrefix.value = "";
    }
    else if(selectedAct == "MODIFY_SUBJECT") {
    	form.nopageStartHour.value = "";
    	form.nopageEndHour.value = "";
    	form.overwriteDistro.value = "";
    }
}
    </script>
  </head>

  <body onload="init()">
    <c:set var="headerName" value="Page Filter Add/Edit" />
    <%@ include file="header.jsp" %>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td id="instructions"/>
      </tr>
      <tr>
        <td>
          <!-- Begin Content Area -->
          <html:form action="/servlet/PageFilterEdit">
            <input type="hidden" id="filterId" name="filterId" value="${pageFilterEditForm.filterId}" />
            <input type="hidden" id="saveResult" name="saveResult" value="${pageFilterEditForm.saveResult}" />
            <input type="hidden" id="formAction" name="formAction" value=""/>
            
            <%@ include file="security.jsp" %>

            <table align="center" cellpadding="3" cellspacing="3">
              <tr>
                <td class="LabelRight">Filter Name:</td>
                <td>&nbsp;</td>
                <td>
                  <input id="filterName" id="filterName" name="filterName" value="${pageFilterEditForm.filterName}" size="100" maxlength="100"/>
                </td> 
              </tr>
              <tr>
                <td class="LabelRight">Apply When:</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              	<td>Source Contains</td>
                <td>
                  <input type="text" id="msgSource" name="msgSource" value="${pageFilterEditForm.msgSource}" size="100" maxlength="100"/>
                </td>    
              </tr>
              <tr>
              	<td>&nbsp;</td>
              	<td>Subject Contains</td>
                <td>
                  <input type="text" id="msgSubject" name="msgSubject" value="${pageFilterEditForm.msgSubject}" size="100" maxlength="100"/>
                </td>    
              </tr>
              <tr>
              	<td>&nbsp;</td>
              	<td>Body Contains</td>
                <td>
                  <input type="text" id="msgBody" name="msgBody" value="${pageFilterEditForm.msgBody}" size="100" maxlength="500"/>
                </td>    
              </tr> 
              <tr>
                <td class="LabelRight">Action:</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>  
              <tr>
              	<td>&nbsp;</td>
                <td>
                <html:radio property="filterAction" value="SUPPRESS" onclick="doRadioCheck(this)"/>
                  Suppress
                </td>
                <td>&nbsp;</td>
              </tr>  
              <tr>
              	<td>&nbsp;</td>
                <td>
                <html:radio property="filterAction" value="NOPAGE" onclick="doRadioCheck(this)"/>
                  Nopage
                </td>
                <td>Nopage Start Hour
                
                    <html:select property="nopageStartHour">
                    	<html:option value="" ></html:option>
                    	<html:options property="hourList" ></html:options>
                    </html:select>
                   
                    &nbsp;&nbsp;
                    Nopage End Hour 

                    <html:select property="nopageEndHour">
                    	<html:option value="" ></html:option>
                    	<html:options property="hourList" ></html:options>
                    </html:select>

                </td>
              </tr>    
              <tr>
              	<td>&nbsp;</td>
                <td>
                	<html:radio property="filterAction" value="OVERWRITE_DISTRO" onclick="doRadioCheck(this)"/>Overwrite Distro
                </td>                
                <td>
                    <html:select property="overwriteDistro" >
              			<html:option value="" ></html:option>
              			<html:optionsCollection property="distroList"
                            value="sitescopeProject"
                            label="sitescopeProject" />
            		</html:select>
                </td>
              </tr>  
              <tr>
              	<td>&nbsp;</td>
                <td>
                	<html:radio property="filterAction" value="MODIFY_SUBJECT" onclick="doRadioCheck(this)"/>        
                Prepend to Subject
                </td>
                <td>
                <input type="text" id="subjectPrefix" name="subjectPrefix" value="${pageFilterEditForm.subjectPrefix}" size="50"/>
                </td>
              </tr>  
              <tr>
              	<td>&nbsp;</td>
                <td>
                	<html:radio property="filterAction" value="EXCL_FROM_ON_OFF_SWITCH" onclick="doRadioCheck(this)"/>                
                Exclude From On/Off Switch</td>
                <td>&nbsp;</td>
              </tr>                                                                                         
              <tr>
                <td colspan="3" align="center">
                  <input type="button" name="submitAction" value="Save" onclick="doSubmit()"/>
                  <input type="button" name="cancelButton" value="Cancel" onclick="doCancel()"/>
                </td>
              </tr>

                           
            </table>
        
          </html:form>
          <!-- End Content Area -->
        </td>
      </tr>
    </table>
    <%@ include file="footer.jsp" %>
  </body>
</html>
