<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<html>
  <head>  
    <title>Content Setup</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../css/ftd.css">
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/clock.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript" language="javascript">

function doEdit(detailId) {
    var form = document.forms[0];
    form.formAction.value = "edit";
    form.contentdetailid.value = detailId;
    //form.contentmasterid.value = masterId;
    url="ContentDetail.do";
    performAction(url);
}

function doAdd() {
    var form = document.forms[0];
    if (form.contentMasterId.value <= 0) {
      return false;
    }
    form.formAction.value = "add";
    url="ContentDetail.do";
    performAction(url);
}

function doCancel() {
    var form = document.forms[0];
    form.formAction.value = "cancel";
    url="ContentDetail.do";
    performAction(url);
}

function doFilter() {
    var form = document.forms[0];
    url="ContentDetail.do";
    performAction(url);
}

    </script>
  </head>

  <body>
    <c:set var="headerName" value="Content Detail Setup" />
    <%@ include file="header.jsp" %>

    <html:form action="/servlet/ContentDetail">
        <input type="hidden" name="formAction"/>
        <input type="hidden" name="contentdetailid" />
        <%@ include file="security.jsp" %>
    
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
            <label class="LabelRight">Context:&nbsp;</label>
            <html:select property="contextFilter" >
              <html:option value="" >All</html:option>
              <html:optionsCollection property="contentContextValues"
                            value="context"
                            label="context" />
            </html:select>
            &nbsp;&nbsp;
            <input type="button" name="filterButton" value="Filter" onclick="doFilter()"/>
        </td>
      </tr>
      <tr>
        <td>
          <display:table id="contentDetailVO" name="contentDetailForm.contentDetailList" class="altstripe">
            <display:column property="context" title="Context" headerClass="ColHeader" />
            <display:column property="name" title="Name" headerClass="ColHeader" />
            <display:column title="Filter 1 Name" headerClass="ColHeader">
              <c:set var="filter1Name" value="${contentDetailForm.contentFilterMap[contentDetailVO.filter1Id].contentFilterName}" />
              ${filter1Name}
            </display:column>
            <display:column property="filter1Value" title="Filter 1 Value" headerClass="ColHeader" />
            <display:column title="Filter 2 Name" headerClass="ColHeader">
              <c:set var="filter2Name" value="${contentDetailForm.contentFilterMap[contentDetailVO.filter2Id].contentFilterName}" />
              ${filter2Name}
            </display:column>
            <display:column property="filter2Value" title="Filter 2 Value" headerClass="ColHeader" />
            <display:column property="text" title="Value" headerClass="ColHeader" />
            <display:column title="Action" headerClass="ColHeader">
              <center>
              <input type="button" name="editButton" value=" Edit " tabindex="2" onclick="doEdit(${contentDetailVO.contentDetailID})"/>
              </center>
            </display:column>
            <display:footer>
                <tr>
                    <td><img src="../images/200.gif" border="0"/></td>
                    <td><img src="../images/200.gif" border="0"/></td>
                    <td><img src="../images/100.gif" border="0"/></td>
                    <td><img src="../images/100.gif" border="0"/></td>
                    <td><img src="../images/100.gif" border="0"/></td>
                    <td><img src="../images/100.gif" border="0"/></td>
                    <td><img src="../images/200.gif" border="0"/></td>
                    <td><img src="../images/40.gif" border="0"/></td>
                </tr>
            </display:footer>
          </display:table>
        </td>
      </tr>
      <tr>
        <td>
          <html:select property="contentMasterId" >
            <html:optionsCollection property="masterValues"
              value="contentMasterId"
              label="description" />
          </html:select>
          &nbsp;&nbsp;
          <input type="button" name="addButton" value=" Add " tabindex="2" onclick="doAdd()"/>
        </td>
      </tr>
    </table>
    <%@ include file="footer.jsp" %>

    </html:form>
  </body>
</html>
