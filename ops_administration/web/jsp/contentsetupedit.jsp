<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<html>
  <head>  
    <title>Content Setup Add/Edit</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../css/ftd.css">
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/clock.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript" language="javascript">

function init() {
    if (document.getElementById('saveResult').value == 'success') {
        document.getElementById("instructions").innerHTML = "Changes were successfully saved";
        document.getElementById("instructions").className = "Instruction"
    } else {
        document.getElementById("instructions").innerHTML = document.getElementById('saveResult').value;
        document.getElementById("instructions").className = "ErrorMessage";
    }

}
function doSubmit() {
    var form = document.forms[0];
    var msg = "";

    var contextValue = document.getElementById("contentContext").value;
    if (contextValue == "") {
        msg = msg + "Content Context is required\n";
    }
    var contentName = document.getElementById("contentName").value;
    if (contentName == "") {
        msg = msg + "Content Name is required\n";
    }
    var description = document.getElementById("contentDescription").value;
    if (description == "") {
        msg = msg + "Content Description is required\n";
    }
    var filter1Id = document.getElementById("contentFilter1Id").value;
    var filter2Id = document.getElementById("contentFilter2Id").value;
    if (filter1Id != "" && filter1Id == filter2Id) {
        msg = msg + "Cannot have the same filter for 1 and 2\n";
    } else if (filter1Id == "" && filter2Id != "") {
        msg = msg + "Cannot have a filter 2 and no filter 1\n";
    }
    var defaultValue = document.getElementById("defaultValue").value;
    if (defaultValue == "") {
        msg = msg + "Default Value is required\n";
    } else if (defaultValue.length > 4000) {
        msg = msg + "Default Value must be less than 4000 characters\n";
    }
    if (msg != "" ) {
        alert(msg);
        return false;
    }
    form.formAction.value = "Save";
    url="ContentSetupEdit.do";
    performAction(url);
}

function doCancel() {
    url="ContentSetup.do";
    performAction(url);
}

function doBackAction(){
    document.forms[0].method="get";
    performAction("ContentSetup.do");
}

    </script>
  </head>

  <body onload="init()">
    <c:set var="headerName" value="Content Setup Add/Edit" />
    <%@ include file="header.jsp" %>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td id="instructions"/>
      </tr>
      <tr>
        <td>
          <!-- Begin Content Area -->
          <html:form action="/servlet/ContentSetupEdit">
            <input type="hidden" id="contentMasterId" name="contentMasterId" value="${contentSetupEditForm.contentMasterVO.contentMasterId}" />
            <input type="hidden" id="saveResult" name="saveResult" value="${contentSetupEditForm.saveResult}" />
            <input type="hidden" id="formAction" name="formAction" value=""/>
            <input type="hidden" id="contextFilter" name="contextFilter" value="${contentSetupEditForm.contextFilter}" />
            <%@ include file="security.jsp" %>

            <table align="center" cellpadding="3" cellspacing="3">
              <tr>
                <td class="LabelRight">Content Context:</td>
                <td>
                  <c:if test="${contentSetupEditForm.contentMasterVO.contentMasterId != 0}" >
                  <input type="hidden" id="contentContext" name="contentContext" value="${contentSetupEditForm.contentMasterVO.context}"/>
                  ${contentSetupEditForm.contentMasterVO.context}
                  </c:if>
                  <c:if test="${contentSetupEditForm.contentMasterVO.contentMasterId == 0}" >
                  <input id="contentContext" name="contentContext" value="${contentSetupEditForm.contentMasterVO.context}" size="100"/>
                  </c:if>
                </td>
              </tr>
              <tr>
                <td class="LabelRight">Content Name:</td>
                <td>
                  <c:if test="${contentSetupEditForm.contentMasterVO.contentMasterId != 0}" >
                  <input type="hidden" id="contentName" name="contentName" value="${contentSetupEditForm.contentMasterVO.name}"/>
                  ${contentSetupEditForm.contentMasterVO.name}
                  </c:if>
                  <c:if test="${contentSetupEditForm.contentMasterVO.contentMasterId == 0}" >
                  <input id="contentName" name="contentName" value="${contentSetupEditForm.contentMasterVO.name}" size="100"/>
                  </c:if>
                </td>
              </tr>
              <tr>
                <td class="LabelRight">Content Description:</td>
                <td><input id="contentDescription" name="contentDescription" value="${contentSetupEditForm.contentMasterVO.description}" size="100"/></td>
              </tr>
              <tr>
                <td class="LabelRight">Filter 1:</td>
                <td>
                	<input id="contentFilter1Id" type="hidden">
                    <html:select property="contentFilter1Id">
                      <html:option value="" >None</html:option>
                      <html:optionsCollection property="filterValues"
                                    value="contentFilterId"
                                    label="contentFilterName" />
                    </html:select>
                    </input>
                </td>
              </tr>
              <tr>
                <td class="LabelRight">Filter 2:</td>
                <td>
                	<input id="contentFilter2Id" type="hidden">
                    <html:select property="contentFilter2Id">
                      <html:option value="" >None</html:option>
                      <html:optionsCollection property="filterValues"
                                    value="contentFilterId"
                                    label="contentFilterName" />
                    </html:select>
                    </input>
                </td>
              </tr>
              <c:choose>
              <c:when test="${contentSetupEditForm.contentMasterVO.contentMasterId == ''}" >
                <tr>
                  <td class="LabelRight">Default Value:</td>
                  <td>
                    <textarea name="defaultValue" cols="100" rows="5"></textarea>
                  </td>
                </tr>
              </c:when>
              <c:otherwise>
                <input type="hidden" id="defaultValue" name="defaultValue" value="N/A"/>
              </c:otherwise>
              </c:choose>

              <tr>
                <td colspan="2" align="center">
                  <input type="button" name="submitAction" value="Save" onclick="doSubmit()"/>
                  <input type="button" name="cancelButton" value="Cancel" onclick="doCancel()"/>
                </td>
              </tr>
            </table>
        
          </html:form>
          <!-- End Content Area -->
        </td>
      </tr>
    </table>
    <%@ include file="footer.jsp" %>
  </body>
</html>
