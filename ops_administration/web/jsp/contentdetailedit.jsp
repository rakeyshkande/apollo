<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri = "http://struts.apache.org/tags-html" prefix = "html"%>

<html>
  <head>  
    <title>Content Detail Add/Edit</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../css/ftd.css">
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/clock.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript" language="javascript">

function init() {
    if ("${ContentDetailForm.contentDetailVO.filter1Name}" == "") {
        document.getElementById('filter1Div').style.display = "none";
    } else {
        document.getElementById('filter1Label').innerHTML = "Filter 1 (" + "${ContentDetailForm.contentDetailVO.filter1Name}" + ") :";
    }
    if ("${ContentDetailForm.contentDetailVO.filter2Name}" == "") {
        document.getElementById('filter2Div').style.display = "none";
    } else {
        document.getElementById('filter2Label').innerHTML = "Filter 2 (" + "${ContentDetailForm.contentDetailVO.filter2Name}" + ") :";
    }
    if (document.getElementById('saveResult').value == 'success') {
        document.getElementById("instructions").innerHTML = "Changes were successfully saved";
        document.getElementById("instructions").className = "Instruction"
    } else {
        document.getElementById("instructions").innerHTML = document.getElementById('saveResult').value;
        document.getElementById("instructions").className = "ErrorMessage";
    }
}

function getSecurityParams(){
    return  "?securitytoken=" + document.forms[0].securitytoken.value +
        "&context=" + document.forms[0].context.value;
}

function doSubmit() {
    var form = document.forms[0];
    var textValue = document.getElementById("text").value;
    var msg = "";

    if (textValue == "") {
        msg = msg + "Value is required\n";
    } else if (textValue.length > 4000) {
        msg = msg + "Value must be less than 4000 characters\n";
    }
    if (msg != "" ) {
        alert(msg);
        return false;
    }
    form.formAction.value = "save";
    url="ContentDetail.do";
    performAction(url);
}

function doCancel() {
    var form = document.forms[0];
    form.formAction.value = "cancel";
    url="ContentDetail.do";
    performAction(url);
}

function doBackAction(){
    document.forms[0].method="get";
    performAction("ContentDetail.do");
}

    </script>
  </head>

  <body onload="init()">
    <c:set var="headerName" value="Content Detail Add/Edit" />
    <%@ include file="header.jsp" %>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td id="instructions"/>
      </tr>
      <tr>
        <td>
          <form id="myform" action="/servlet/ContentDetail.do" method="post">
            <%@ include file="security.jsp" %>
            <table width="100%" border="0" align="left" cellpadding="3" cellspacing="3">
              <tr>
                <td class="LabelRight" width="40%">Content Context:&nbsp;</td>
                <td class="Label" width="60%">
                  <c:set var="contentContext" value="${ContentDetailForm.contentDetailVO.context}" />
                  ${contentContext}
                </td>
              </tr>
              <tr>
                <td class="LabelRight">Content Name:&nbsp;</td>
                <td class="Label">
                  <c:set var="contentName" value="${ContentDetailForm.contentDetailVO.name}" />
                  ${contentName}
                </td>
              </tr>
              <tr id="filter1Div">
                <td class="LabelRight">
                  <label id="filter1Label">Filter 1:</label>
                </td>
                <td>
                  <input name="filter1Value" value="${contentDetailForm.contentDetailVO.filter1Value}" size="100"/>
                </td>
              </tr>
              <tr id="filter2Div">
                <td class="LabelRight">
                  <label id="filter2Label">Filter 2:</label>
                </td>
                <td>
                  <input name="filter2Value" value="${contentDetailForm.contentDetailVO.filter2Value}" size="100"/>
                </td>
              </tr>
              <tr>
                <td class="LabelRight">Value:&nbsp;</td>
                <td>
                  <textarea id="text" name="text" cols="100" rows="5">${contentDetailForm.contentDetailVO.text}</textarea>
                </td>
              </tr>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>&nbsp;</td>
                <td>
                  <!--html:submit value="Save"/-->
                  <input type="button" id="submitButton" name="submitButton" value="Save" tabindex="2" onclick="doSubmit()"/>
                  <input type="button" id="cancelButton" name="cancelButton" value="Cancel" tabindex="2" onclick="doCancel()"/>
                  <input type="hidden" id="formAction" name="formAction" value="" />
                  <input type="hidden" id="contentdetailid" name="contentdetailid" value="${contentDetailForm.contentDetailVO.contentDetailID}" />
                  <input type="hidden" id="contentMasterId" name="contentMasterId" value="${contentDetailForm.contentDetailVO.contentMasterID}" />
                  <input type="hidden" id="saveResult" name="saveResult" value="${contentDetailForm.saveResult}" />
                  <input type="hidden" id="contextFilter" name="contextFilter" value="${contentDetailForm.contextFilter}" />
                </td>
              </tr>
            </table>
          </form>
        </td>
      </tr>
    </table>
    <%@ include file="footer.jsp" %>
  </body>
</html>
