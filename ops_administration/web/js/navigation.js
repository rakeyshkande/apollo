function getSecurityParams()
{
    return  "?securitytoken=" + document.forms[0].securitytoken.value +
            "&context=" + document.forms[0].context.value;
}


function doBackAction()
{
    var url = "OpsAdminServlet";
    performAction(url);
}

function doExitAction()
{
    var url = "ExitServlet";
    performAction(url);
}
    
function performAction(url)
{
    document.forms[0].action = url + getSecurityParams();
    document.forms[0].submit();
}    