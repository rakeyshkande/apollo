<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>Mercury Retrieval Screen</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doBackAction(){
      var url = "OpsAdminServlet" +
                getSecurityParams();
      performAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" +
                getSecurityParams();
      performAction(url);
    }
    
    function doPostAction(){
      var url = "MercuryRetrievalServlet" +
                getSecurityParams();
      performAction(url);
    }

    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body>

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Mercury Retrieval Screen'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="SystemConfigurationForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

  <!-- Box X table -->
   <table width="600" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Fields -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">

            <tr>
              <td class="Label" width="30%">Suffix</td>
              <td width="30%"><input type="text" name="suffix" size="2" maxlength="2"/></td>
              <td width="40%">AA, AB, etc.</td>
            </tr>
            <tr>
              <td class="Label">From Message Number</td>
              <td><input type="text" name="fromMsgNumber" size="15" maxlength="13"/></td>
              <td>Format: ANNNNA-NNNN/A</td>
            </tr>
            <tr>
              <td class="Label">From Message Date</td>
              <td><input type="text" name="fromMsgDate" size="15" maxlength="6"/></td>
              <td>Format: MMM DD</td>
            </tr>
            <tr>
              <td class="Label">To Message Number</td>
              <td><input type="text" name="toMsgNumber" size="15" maxlength="11"/></td>
              <td>Format: ANNNNA-NNNN</td>
            </tr>
            <tr>
              <td class="Label">To Message Date</td>
              <td><input type="text" name="toMsgDate" size="15" maxlength="6"/></td>
              <td>Format: MMM DD</td>
            </tr>
            <tr>
              <td class="label">Operator</td>
              <td><input type="text" name="operator" size="15" maxlength="15"/></td>
              <td>Your Name</td>
            </tr>

          </table>
        </td>
      </tr>
    </table>
    
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>
          <input type="button" name="submitButton" value="Submit" tabindex="1" onclick="javascript:doPostAction();" style="padding-left:8px;padding-right:8px"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
    
  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>