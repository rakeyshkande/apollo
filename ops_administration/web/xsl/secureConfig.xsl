<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:template match="/root">

<xsl:variable name="selectedContext" select="key('pageData','selected_context')/value"/>
<xsl:variable name="viewPermission" select="key('security','view_permission')/value"/>

<html>
<head>
    <title>FTD - Secure System Configuration</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">

    var app_context = '<xsl:value-of select="$applicationcontext"/>';
    var site_name = '<xsl:value-of select="$sitename"/>';
    
    <![CDATA[

  /*
   *  Global variables
   */
    var EXIT = "_EXIT_";

    /*
    *  Initialization
    */
    function init(){
      setNavigationHandlers();
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
      var configDiv = document.getElementById("configListing");
      var newHeight = document.body.clientHeight - configDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        configDiv.style.height = newHeight;
    }

  /*
   *  Events
   */
    function doValueBlur(input){
      if (input.defaultValue == input.value){
        setButtonActionAccess(true);
        document.getElementById("id_" + input.key).checked = false;
      }
      else if(input.value == "") {
        alert("A value must be entered.  Resetting back to original value.");
        input.value = input.defaultValue;
      }
      else {
        setButtonActionAccess(false);
        document.getElementById("id_" + input.key).checked = true;
      }
      checkActionAccess();
    }

  /*
   *  Button control
   */
    function checkActionAccess(){
      var checkboxes = document.getElementsByTagName("input");
      for (var i = 0; i < checkboxes.length; i++){
        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked){
          setButtonActionAccess(false);
          return;
        }
      }
      setButtonActionAccess(true);
    }
    
    function setButtonActionAccess(access){
      var form = document.forms[0];
      form.deleteButton.disabled = access;
      form.changesButton.disabled = access;
    }

  /*
   *  Popups
   */
    function doAddConfigPopup(){
      var form = document.forms[0];
      var url_source =  "AddSecureConfigServlet" +
                        getSecurityParams();

      var modal_dim = "dialogWidth:550px; dialogHeight:400px; center:yes; status=0; help:no";
      var ret = window.showModalDialog(url_source, "", modal_dim);

      if (ret && ret != null && ret[0] != EXIT){
        doAddAction(ret);
      }
      else if (ret && ret != null && ret[0] == EXIT){
        doExitAction();
      }
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doAddAction(newConfig){
      var form = document.forms[0];
      form.new_context.value = newConfig[0];
      form.new_name.value = newConfig[1];
      form.new_value.value = newConfig[2];
      form.new_desc.value = newConfig[3];

      var url = "SecureConfigServlet" +
                getSecurityParams() +
                "&action=add";

      performAction(url);
    }

    function doAction(action){
      var modalText = "Are you sure?";
      var modalDim = "dialogWidth:200px; dialogHeight:150px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../confirm.html", modalText, modalDim);
      if (ret){
        var url = "SecureConfigServlet" +
                  getSecurityParams() +
                  "&action=" + action;

        performAction(url);
      }
    }

    function refreshContext(){
        // Set the context selected from the dropdown
        var form = document.forms[0];
        form.selected_context.value = form.context_select.value;
        
        var url = "SecureConfigServlet" +
          getSecurityParams();

        performAction(url);
    }

    function doBackAction(){
      var url = "OpsAdminServlet" +
                getSecurityParams();
      performNonSslAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" +
                getSecurityParams();
      performNonSslAction(url);
    }
    
    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }

    function performNonSslAction(servlet) {
      document.forms[0].action = "http://" + site_name + app_context + "/servlet/" + servlet;
      document.forms[0].submit();
    }    
    
    ]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Secure System Configuration'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="SystemConfigurationForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="view_permission" value="{key('security', 'view_permission')/value}"/>
    <input type="hidden" name="selected_context" value="{key('pageData','selected_context')/value}"/>
    <input type="hidden" name="new_context"/>
    <input type="hidden" name="new_name"/>
    <input type="hidden" name="new_value"/>
    <input type="hidden" name="new_desc"/>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

            <!-- Only display the actions if the user has the appropriate security -->
            <xsl:if test="$viewPermission = 'true'">
                <!-- Actions -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblheader" colspan="4">Actions</td>
                    </tr>
                    <tr>
                        <td width="33%" align="center">
                            <input type="button" name="addButton" value="Add" onclick="javascript:doAddConfigPopup();"/>
                        </td>
                        <td width="33%" align="center">
                            <input type="button" name="changesButton" value="Apply Changes" disabled="true" onclick="javascript:doAction('update');"/>
                        </td>
                        <td width="33%" align="center">
                            <input type="button" name="deleteButton" value="Delete" disabled="true" onclick="javascript:doAction('delete');"/>
                        </td>
                    </tr>
                </table>
            </xsl:if>
            
          <!-- Secure System Configuration -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Properties</td>
            </tr>
            <tr>
                <!-- Only display the checkbox column if the user has the appropriate security -->
                <xsl:if test="$viewPermission = 'true'">
                    <td width="4%" class="colHeaderCenter"></td>
                </xsl:if>
                <td width="24%" class="colHeaderCenter">Context:&nbsp;
                        <select name="context_select" onchange="refreshContext()">
                            <option value="">All</option>
                            <xsl:for-each select="contexts_list/contexts">
                                <xsl:choose>
                                    <xsl:when test="$selectedContext = context">
                                        <option selected="true" value="{context}"><xsl:value-of select="context"/></option>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <option value="{context}"><xsl:value-of select="context"/></option>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </select>
                </td>
                <td width="24%" class="colHeaderCenter">Name</td>
                <!-- Only display the value column if the user has the appropriate security -->
                <xsl:if test="$viewPermission = 'true'">
                    <td width="24%" class="colHeaderCenter">Value</td>
                </xsl:if>
                <td width="24%" class="colHeaderCenter">Description</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">

                <div id="configListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <xsl:for-each select="property_list/property">
                      <xsl:sort select="@num" data-type="number"/>
                      <tr>
                        <!-- Only display the checkbox column if the user has the appropriate security -->
                        <xsl:if test="$viewPermission = 'true'">
                            <td width="4%" align="center">
                                <input type="checkbox" name="id_{context}{name}" key="{context}{name}" onclick="javascript:checkActionAccess();"/>
                            </td>
                        </xsl:if>
                        <td width="24%" align="center"><input type="hidden" name="context_{context}{name}" value="{context}"/><xsl:value-of select="context"/></td>
                        <td width="24%" align="center"><input type="hidden" name="name_{context}{name}" value="{name}"/><xsl:value-of select="name"/></td>
                        <!-- Only display the value column if the user has the appropriate security -->
                        <xsl:if test="$viewPermission = 'true'">
                            <td width="24%" align="center">
                                <input type="text" name="value_{context}{name}" key="{context}{name}" value="{value}" size="30" maxlength="50" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                            </td>
                        </xsl:if>
                        <td width="24%" align="center">
                            <!-- Only display the description as an input field if the user has the appropriate security -->
                            <xsl:choose>
                                <xsl:when test="$viewPermission = 'true'">
                                    <a title="{description}">
                                        <input type="text" name="desc_{context}{name}" key="{context}{name}" value="{description}" size="50" maxlength="50" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a title="{description}">
                                        <xsl:value-of select="description"/>
                                    </a>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>