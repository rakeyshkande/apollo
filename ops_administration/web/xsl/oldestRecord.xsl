<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>

  <head>
    <title>FTD - Confirm</title>
    <link rel="stylesheet" href="../css/ftd.css"/>
    <script  language="javascript">
    /*
     *  Initialization
     */
      function init() {
        document.getElementById("cancel").attachEvent("onmouseover", doMouseOver);
      }

    /*
     *  Actions
     */
      function doCloseAction(){
        window.close();
      }

    /*
     *  Tool tip
     */
      var display = true;
      var timerId = null;
      function doMouseOver(event) {
        toolTipFloat = document.getElementById("toolTip").style;
        toolTipFloat.left = event.clientX + document.body.scrollLeft - 120;
        toolTipFloat.top = event.clientY  + document.body.scrollTop;
        toolTipFloat.display = "block";
        checkToolTip();
      }
      function checkToolTip(){
        if (display)
          timerId = setTimeout("checkToolTip()", 5000);
        else
          doMouseOut();

        display = !display;
      }
      function doMouseOut() {
        toolTipFloat.display = "none";
        display = true;
        clearTimeout(timerId);
      }
    </script>
  </head>

  <body onload="javascript:init();" style="margin-top:0px;margin-left:0px;margin-right:0px;margin-bottom:0px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
      <tr>
        <td bgcolor="#ADD8E6" height="27" style="padding-left:10px;border-bottom:1px solid #10659E;" width="100%">
          <img src="../images/attention.gif" border="0"/>
        </td>
        <td bgcolor="#ADD8E6" style="padding-right:10px; border-bottom:1px solid #10659E; cursor:hand;">
          <img id="cancel" src="../images/cancel.gif" border="0" align="absmiddle" onclick="javascript:doCloseAction();"  onmouseout="javascript:doMouseOut();"/>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="2" height="100%" class="attentionText">
          The oldest record in Scrub has a timestamp of:<br/><br/>
          <b><xsl:value-of select="oldestRecordList/oldestRecord/oldest_date"/></b>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center" style="padding-bottom:10px;">
          <button class="blueButton" id="okButton" onclick="javascript:doCloseAction();" style="padding-left:8px;padding-right:8px">Ok</button>
        </td>
      </tr>
    </table>

    <div id="toolTip" style="display:none; position:absolute; border:1px solid black; background-image: url(../images/blueButton.gif);">
      <table border="0">
        <tr>
          <td>
            Click to close window.
          </td>
        </tr>
      </table>
    </div>

</body>
</html>

</xsl:template>
</xsl:stylesheet>