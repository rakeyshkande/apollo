<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="footer1">
<script type="text/javascript" src="../js/copyright.js"/>
  <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr><td>&nbsp;</td></tr>
   	<tr style="position:fixed"  width="98%">
					<td align="center" width="33%">
						<INPUT id="submitPageButton" name="submitPageButton" tabindex="6" accesskey="S"  type="button" class="BlueButton" style="width:80;" value="(S)ubmit" onclick="javascript:formSubmission();"/>
					
				
						<INPUT id="clearAllfield" name="clearAllfield" tabindex="7" accesskey="C" type="button" class="BlueButton" style="width:80;" value="(C)lear all fields" onclick="javascript:clearAllFeildAction();" />

					</td>
		</tr>
    <tr><td>&nbsp;</td></tr>
    <tr>      
      	<td class="disclaimer"></td><script>showCopyright();</script>  
    </tr>
  </table>

</xsl:template>
</xsl:stylesheet>