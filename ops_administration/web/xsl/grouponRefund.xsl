<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header1.xsl" />
	<xsl:import href="footer1.xsl" />
	<xsl:output method="html" indent="yes" />

	<xsl:key name="security" match="/root/securityParams/params" use="name" />
	<xsl:param name="applicationcontext"> <xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
	<xsl:param name="sitename">	<xsl:value-of select="key('security','sitename')/value" /> </xsl:param>
	<xsl:param name="sitenamessl"> <xsl:value-of select="key('security','sitenamessl')/value" /> </xsl:param>
	<xsl:template match="/root">

<html>
	<head>
				<title>FTD - Groupon Refund</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css" />
				<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
				<script type="text/javascript" src="../js/util.js"></script>
				<script type="text/javascript" src="../js/calendar.js"></script>
				<script type="text/javascript" language="javascript">
				
					var app_context = '<xsl:value-of select="$applicationcontext" />';
					var site_name = '<xsl:value-of select="$sitename" />';
					var site_name_ssl = '<xsl:value-of select="$sitenamessl" />';

    <![CDATA[

    /*
     *  Initializations
     */
     
      function init(){
        setNavigationHandlers();
      }
     
    /*
     *  Actions
     */
     
function getSecurityParams()
{
 return  "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
}

function doExitAction()
{
 performAction("ExitServlet");
}

    
function performAction(url){
        document.forms[0].action = url + getSecurityParams();
        document.forms[0].submit();
      }
      
  function performSslAction(servlet) {
    document.forms[0].action = "https://" + site_name_ssl + app_context + "/servlet/" + servlet;
		document.forms[0].submit();
	}

// check whether textarea has more than 350 coupons.

 function setInputGrouponCoupons()
			{
			    var p = document.getElementById("inputTextArea").value;
			    var partnerOrderNumberInput = "";
			    partnerOrderNumberInput = p.split("\n").join();	
   		    	document.getElementById("p_gc_number").value = partnerOrderNumberInput;			
			}
			
// function for toggle display.It perform when the input xls radio button is selected then textarea will be disable and vise versa.
			
function toggleDisplay(searchBy) {
	
	if (searchBy == "FILE") {
  		document.getElementById("inputTextArea").disabled = true;
		document.getElementById("inputTextArea").value = "";
		document.getElementById("inputExcelFile").disabled = false;
		
	}
	else if(searchBy == "INPUTBOX"){
        document.getElementById("inputTextArea").disabled = false;
        document.getElementById("inputExcelFile").disabled = true;
	}
	

}

// fuction for Clear all feild.

 function clearAllFeildAction()
            {
			document.getElementById("GrouponForm").reset();
			document.getElementById("inputTextArea").disabled = true;
			document.getElementById("amount").value = "";
			document.getElementById("expiryDate").value = "";
			document.getElementById("errorMessage").innerHTML = "";
			document.getElementById("inputExcelFile").disabled = false;
			
          }

  
// fuction for form submission.

function formSubmission() 
{	
	setInputGrouponCoupons();
	var intialStatus = false;
	var amount = document.getElementById("amount").value;
	var expiryDate = document.getElementById("expiryDate").value;

if (	document.getElementById("cancelled").checked || 
		document.getElementById("issuedActive").checked || document.getElementById("issuedExpired").checked ||  
		document.getElementById("redeemed").checked || document.getElementById("reinstate").checked || 
		document.getElementById("refund").checked || document.getElementById("issuedInactive").checked  ) {
		var intialStatus = true;	
	}
	
	
if ( (intialStatus ) || (amount != null && amount.length > 0 )|| (expiryDate != null && expiryDate != ""))
{	     
	// QE-87 validating the amount field
	if(amount!= null && amount.length > 0)
	{
	  if((String(parseInt(amount)).match(/^\d{0,7}$/)))
	   {
		 var pattern=/^[0-9]+(\.[0-9]{1,2})?$/;
		 if(amount.match(pattern))
		   {
			  document.forms[0].action = "GrouponRefundServlet" + getSecurityParams();
			  document.getElementById("submitPageButton").disabled = true;
			  document.forms[0].submit();			
		   }
		 else
		   {
		     alert("please enter a valid issue amount");
		     return false;
		   }
		  
	    }
	  else
	    {
		  alert("please enter a valid issue amount");
		  return false;
	    }
	 }
	document.forms[0].action = "GrouponRefundServlet" + getSecurityParams();
	document.getElementById("submitPageButton").disabled = true;
	document.forms[0].submit();

}
 else 
 {
   alert("Please do any operartion (Either updating Status or Amount or Expiry date or combination of these. )");
   return false;
 }


}
    
    
        
  
      ]]>
		
	
</script>
	
	
	</head>



			<body onload="javascript:init();">

				<!-- Header Template -->
				<xsl:call-template name="header1">
					<xsl:with-param name="headerName" select="'Gift Certificate Mass Edit Tool'" />
					<xsl:with-param name="showTime" select="true()" />
					<xsl:with-param name="showExitButton" select="true()" />
				</xsl:call-template>


				<form name="GrouponForm"  method="post" action="GrouponRefundServlet"  enctype="multipart/form-data">
				
					<input type="hidden" name="securitytoken"  value="{key('security', 'securitytoken')/value}" />
					<input type="hidden" name="context"		  value="{key('security', 'context')/value}" />
					<input type="hidden" name="adminAction"   value="{key('security', 'adminAction')/value}" />
					<input id="p_gc_number" type="hidden" name="p_gc_number" />
					
					
				    
								

					<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">

						<tr>
							<td>

								<table width="100%" border="0" cellpadding="2" 	cellspacing="2">
								
									 <!--************* RADIO BUTTONS *************-->

									<tr>
											<td class="label" width="23%" align="left" valign="top">
												<input type="radio" name="inputType" id="inputExcel" value="FILE" checked="true" onclick="toggleDisplay('FILE')" />
												Input .xls (Max. Limit 50K Record)
												<font color="red">&nbsp;*&nbsp; </font>:&nbsp;									   
											</td>
										
											<td class="Instruction" style="font-size:12pt; font-weight: bold;" width="10%" align="left">
												<center>OR</center>
											</td>
																		
											<td class="label" width="23%" align="left" valign="top">
												<input type="radio" name="inputType" id="inputType" value="INPUTBOX" onclick="toggleDisplay('INPUTBOX')" />
												Input Batch Numbers/Coupon Numbers (Max. Limit 350):
												<font color="red">&nbsp;*&nbsp; </font>:&nbsp;
											</td>
									</tr>
									

									<tr>
											<td colspan="2" class="label" width="33%" align="left" valign="top">
												<input type="file" name="inputExcelFile" id="inputExcelFile"  />
											   <!--This is a note added regarding  QE-77 i.e the excel file input-->  
											   <br></br><br></br> <font color="red" face="sans-serif" style="font-size: 8pt; font-weight: normal;"> NOTE: Please format the cells to text before copying the coupon numbers into the excel sheet.</font>
											   <br></br><br></br>
											</td>
										
											<td colspan="2" class="label" width="33%" align="left" valign="top">
												<textarea name="inputTextArea"  id="inputTextArea" rows="3" cols="20" disabled="true" ></textarea>
											</td>
									</tr>
									
									<tr>
											<td>
											<div class="label" style="width:50%;float:left">Input xls or Text area Contains:</div>
											
											<div style="width:50%;float:left">
												<input type="radio" name="couponType"  value="CouponNumber" checked="checked" />
												Coupon Number
												
												<input type="radio" name="couponType"  value="BatchNumber"  />
												Batch Number
											</div>
												
											</td>						
											
									</tr>

							 	    <tr>
										<td  >
										<div class="label" style="width:50%;float:left">Current Status of the Gift Coupons:</div>
																		
										<div  style="width:50%;float:left">										
										<input id="cancelled" type="checkbox" name="intialGcStatus" value="Cancelled"/>Cancelled<br/>
										<input id="issuedActive" type="checkbox" name="intialGcStatus" value="Issued Active"/>Issued Active<br/>
										<input id="issuedExpired" type="checkbox" name="intialGcStatus" value="Issued Expired"/>Issued Expired<br/>
										<input id="issuedInactive" type="checkbox" name="intialGcStatus" value="Issued Inactive"/>Issued Inactive<br/>
										<input id="redeemed" type="checkbox" name="intialGcStatus" value="Redeemed"/>Redeemed<br/>
										<input id="refund" type="checkbox" name="intialGcStatus" value="Refund"/>Refund<br/>
										<input id="reinstate" type="checkbox" name="intialGcStatus" value="Reinstate"/>Reinstate	
										</div>
										</td>								
									</tr> 
									
									<tr>
												<td >
												<div class="label" style="width:50%;float:left">Set the Status of gift Coupons to : </div>
												
											    <div style="width:50%;float:left">
													<select name="finalGcStatus">														
														<option id="finalGcStatus" value="Cancelled">Cancelled</option>
														<option id="finalGcStatus" value="Issued Active">Issued Active</option>
														<option id="finalGcStatus" value="Issued Expired">Issued Expired</option>
														<option id="finalGcStatus" value="Issued Inactive">Issued Inactive</option>
														<option id="finalGcStatus" value="Redeemed">Redeemed</option>
														<option id="finalGcStatus" value="Refund">Refund</option>
														<option id="finalGcStatus" value="Reinstate">Reinstate</option>
													</select>
													</div>
												</td>
									    </tr>
									    <tr>
									    
									    </tr>
										<td>
										
										<div class="label" style="width:50%;float:left">Issue amount:</div>
										 <div style="width:50%;float:left">
											<input id="amount" type="text" name="amount" maxlength="10"/>
                						</div>
										</td>
										<tr>
										<td>
										
										<div class="label" style="width:50%;float:left">Expiry Date: </div>
										 <div style="width:50%;float:left">
											<input id="expiryDate" type="text" size="10" 
												name="expiryDate" value="{key('pageData', 'startDate')/value}" tabindex="30"   />  &nbsp;&nbsp;
											<input type="image" id="calendar1" tabindex="35"
												BORDER="0" SRC="../images/calendar.gif" width="16" height="16" />
												</div>
										</td>
									</tr>
										<tr>
										<td id="errorMessage" height="40px" colspan="3" style="color:red;">
											<xsl:for-each select="/root/DateMessage/UpdateStatusMessage">
												<xsl:value-of select="value" />
											</xsl:for-each>
										</td>
									</tr>
									
							</table>
							</td>
						</tr>
					</table>

					<!-- Footer Template -->
					<xsl:call-template name="footer1" />
				</form>



			</body>
		</html>
		
		<script type="text/javascript">
			Calendar.setup(
			{
			inputField : "expiryDate", // ID of the input field
			ifFormat : "mm/dd/y", // the date format
			button : "calendar1" // ID of the button
			}
			);
			</script>
	
	</xsl:template>
</xsl:stylesheet>