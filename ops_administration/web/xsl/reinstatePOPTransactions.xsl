<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">
  <html>
<head>
    <title>FTD - Re-instate POP Transactions</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">
  /*
   *  Gobal variables
   *
   */
    var THRESHOLD = 20;
    var tId;
    tId = <xsl:value-of select="pageData/params[name = 'tableid']/value"/>;
  
<![CDATA[
                          
  /*
   *  Initialization
   */
    function init(){
        setNavigationHandlers();
        setScrollingDivHeight();
        window.onresize = setScrollingDivHeight;
        setDropDownList();
    }
    
    function setDropDownList()
    {
      
      document.forms[0].ddlList.selectedIndex = (tId - 1);
    }

    function setScrollingDivHeight(){
        var orderListingDiv = document.getElementById("orderListing");
        var newHeight = document.body.clientHeight - orderListingDiv.getBoundingClientRect().top - 100;
        if (newHeight > 15)
          orderListingDiv.style.height = newHeight;
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      var parms =   "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + 
document.forms[0].contextttt.value;
      return parms;
    }
    
    /*
     *  Retieve the id value from the 
     *  drop down list
     *
     */
    function getTableId()
    {
      var ddlId = 
        document.forms[0].ddlList.options[document.forms[0].ddlList.selectedIndex].value
      return  ddlId;
    }
    
    /*  Reload the page based on the user
     *  selection from the drop down list.
     */
    function doLoadReinstateAction()
    {
      var url = "ReinstatePOPTransactionServlet" +
            getSecurityParams() + "&tableid=" + getTableId();
        performAction(url);
    }
    
    /*  Retrieve the xml and key 
     *  based on user selection
     *  forward user to the fix xml page
     */
    function doReinstateAction(key) {  
        
     var xml = document.getElementById('xml_'+ key).value; 
     document.forms[0].xmlText.value = xml;
     
    
     
     var  url = "ReinstatePOPTransactionServlet" + 
                      getSecurityParams()+ "&action=fix_transaction_xml" +
                            "&tableid=" + getTableId() + "&key=" + key;
  
      performAction( url );
    
    }
     
    function doExitAction(){
    
      performAction("OpsAdminServlet" + getSecurityParams());
      
    }
    
    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }
   
  
    ]]>
    </script>
</head>
<body onload="javascript:init();">
<xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Reinstate POP Transactions'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
</xsl:call-template>

 <form name="TransactionForm" method="post" action="ReinstatePOPTransactionServlet">
 <table border="0" width="98%"  >
  <tr>
   <td >&nbsp;&nbsp;&nbsp;
  <select  name="ddlList" onchange="javascript:doLoadReinstateAction()">
      <option value="1" >Order Gatherer</option>
      <option value="2">Inbound Transmissions</option>
      <option value="3">Outbound Partner Messages</option>
    </select>
    </td>
    </tr>
 </table>
  <br />
  <input type="hidden" name="xmlText" value=""/>
  <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
  <input type="hidden" name="contextttt" value="{key('security', 'context')/value}"/>
  <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

 <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Reinstate Transaction -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="20%" class="colHeaderCenter">Transmission Id</td>
              <td width="20%" class="colHeaderCenter">Partner Id</td>
              <td width="40%" class="colHeaderCenter">Error Description</td>
              <td width="20%" class="colHeaderCenter">Timestamp</td>
            </tr>
            <tr>
              <td colspan="6" width="100%" align="center">

                <!-- Scrolling div contiains orders -->
                <div id="orderListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2" id="errorTable">
                    <xsl:for-each select="walmartOrderList/reinstateOrder">
                         <tr>
                            <td width="20%" align="center" id="'{transmission_id}'"> 
                                <a href="#" onclick="javascript:doReinstateAction('{transmission_id}');">
                                    <xsl:value-of select="transmission_id" />
                                  
                                </a>
                            </td>
                              <td width="20%" align="center"><xsl:value-of select="partner_id"/></td>
                              <td width="40%" align="center"><xsl:value-of select="error_desc"/></td>
                              <td width="20%" align="center"><xsl:value-of select="time_stamp"/> </td> 
                              <input type="hidden" id="xml_{transmission_id}" value="{xml}"/>
                        </tr>
                    </xsl:for-each>
                  </table>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" 
onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
</form>
 <!-- Footer Template -->
  <xsl:call-template name="footer"/>
 
      </body>
    </html>
  </xsl:template>   
</xsl:stylesheet>
