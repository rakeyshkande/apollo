<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:variable name="errorMessage" select="boolean(key('pageData','XMLERROR')/value = 'TRUE')"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Fix Wal-Mart Transactions</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
    
  
  /*
   *  Global variables
   */
  var key = <xsl:value-of select="pageData/data[name = 'KEY']/value"/>;
  var tableid =  <xsl:value-of select="pageData/data[name = 'TABLEID']/value"/>;
  /*
   *  Initialization
   */
    

  /*
   *  Events
   */
   

  /*
   *  Validation
   */
  <![CDATA[

  /*
   *  Actions
   */
    function getSecurityParams(){
      var parms =   "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
      return parms;
    }
   
   function doSubmitAction(){
        if( window.confirm('Submit changes?') ){
             submit();       
        }     
   }
   
   function submit() {
        var url;
       /*Place the fixed XML in the hidden text field to be passed to the servlet */
        document.forms[0].xmlText.value = document.getElementById('textWindow').value;
        url  = "ReinstateWalMartTransactionServlet" + getSecurityParams() +
                "&key=" + key + "&tableid=" + tableid + "&action=reinstate";
     performAction(url);  
   }
   
   function doExitAction(){
      var url = "OpsAdminServlet" + getSecurityParams();
      performAction(url);
    }
    
     function doCloseAction(){
      var url = "ReinstateWalMartTransactionServlet" + getSecurityParams() + "&tableid=" + tableid;
      performAction(url);
    }

  function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }
    ]]>
    </script>
    </head>
<body>

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Fix Wal-Mart Transactions'"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="fixXML" method="post" >
      <input type="hidden" name="xmlText" value="" />
      <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
      <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
      <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <!-- Main table -->
      
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <!-- Settings -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Fix Wal-Mart Transactions</td>
            </tr>
            <tr>
              <td style="color:red">
                <xsl:if test="$errorMessage" ><xsl:value-of select="key('pageData','XMLMESSAGE')/value"/></xsl:if>
              </td>
            </tr>
            <tr>
              <td>
                  <xsl:for-each select="pageData/data">
                   <xsl:if test="name = 'XML'">
                      <textarea rows="30" cols="165" wrap="virtual" id="textWindow" name="xmlWindow">
                         <xsl:value-of select="value"/> 
                       </textarea>
                   </xsl:if>
               </xsl:for-each>
                  
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="20%" align="center">&nbsp;</td>
        <td width="60%" align="center"> 
            <input type="button" name="Submit" value="Submit" id="submit_button" onclick="javascript:doSubmitAction();"/>
            &nbsp; &nbsp;
            <input type="button" value="Close" id="close_button" onclick="javascript:doCloseAction();"/>
        </td>
        <td width="18%" align="right">
          <input type="button" name="exit" id="exit" value="Exit" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>
  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>
