<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:variable name="remove_chars">&quot;&apos;</xsl:variable>
<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - System Messages</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">
  /*
   *  Gobal variables
   */
    var THRESHOLD = 45;
    var messages = new Array( <xsl:for-each select="systemMessagesList/systemMessages">
                                "<xsl:value-of select="translate(normalize-space(message), $remove_chars, '')"/>"
                                <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                              </xsl:for-each>);<![CDATA[

  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
      setMessages();
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
    }

    function setMessages(){
      var element, message, newMessage;

      for (var i = 0; i < messages.length; i++){
        element = document.getElementById("message_" + (i+1));
        message = messages[i];
        if (message.length >= THRESHOLD){
          newMessage = "<a href='#' onclick='javascript:doMessageAction(\"" + message + "\");'>" + message.substring(0, THRESHOLD) + "&nbsp;&nbsp;&nbsp;(Click to see more)</a>";
        }
        else{
          newMessage = message;
        }
        element.innerHTML = newMessage;
      }
    }

    function setScrollingDivHeight(){
      var messageDiv = document.getElementById("messageListing");
      var newHeight = document.body.clientHeight - messageDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        messageDiv.style.height = newHeight;
    }


  /*
   *  Delete button control
   */
    function checkDeleteAccess(){
      var form = document.forms[0];
      var checkboxes = document.getElementsByTagName("input");
      for (var i = 0; i < checkboxes.length; i++){
        if (checkboxes[i].type == "checkbox" && checkboxes[i].checked){
          form.deleteButton.disabled = false;
          return;
        }
      }
      form.deleteButton.disabled = true;
    }


  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }
   
    function paramsHelper(){
      var form = document.forms[0];
      return  "&view_type=" + form.view_type[form.view_type.selectedIndex].value +
              "&source_type=" + form.source_type[form.source_type.selectedIndex].value;
    }

    function doRefreshAction(){
      var url = document.forms[0].action +
                getSecurityParams() +
                paramsHelper();
      performAction(url);
    }

    function doMessageAction(message){
      var modalDim = "dialogWidth:600px; dialogHeight:400px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../information.html", message, modalDim);
    }

    function doDeleteAction(){
      var form = document.forms[0];
      var modalText = "Are you sure?";
      var modalDim = "dialogWidth:200px; dialogHeight:150px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../confirm.html", modalText, modalDim);
      if (ret){
        form.method = "post";
        performAction(form.action);
      }
    }

    function doBackAction(){
      var url = "OpsAdminServlet" +
                getSecurityParams();
      performAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" +
                getSecurityParams();
      performAction(url);
    }
    
    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'System Messages'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="SystemMessagesForm" method="get" action="SystemMessagesServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Actions -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Actions</td>
            </tr>
            <tr>
              <td width="25%" align="center">
                <span class="label">View:</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <select name="view_type">
                  <option value="NEW">
                    <xsl:if test="key('pageData', 'view_type')/value='NEW'"><xsl:attribute name="SELECTED"/></xsl:if>
                    NEW
                  </option>
                  <option value="ALL">
                    <xsl:if test="key('pageData', 'view_type')/value='ALL'"><xsl:attribute name="SELECTED"/></xsl:if>
                    ALL
                  </option>
                </select>
              </td>
              <td width="25%" align="center">
                <span class="label">Source:</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <select name="source_type">
                  <option value="">Select One</option>
                  <xsl:for-each select="systemMessagesSourcesList/systemMessagesSources">
                    <option value="{source}">
                      <xsl:if test="key('pageData', 'source_type')/value=source"><xsl:attribute name="SELECTED"/></xsl:if>
                      <xsl:value-of select="source"/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
              <td width="25%" align="right">
                <input type="button" name="refreshButton" value="Refresh" onclick="javascript:doRefreshAction();"/>
              </td>
              <td width="25%" align="center">
                <input type="button" name="deleteButton" value="Delete" disabled="true" onclick="javascript:doDeleteAction();"/>
              </td>
            </tr>
          </table>

          <!-- System Messages -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Messages</td>
            </tr>
            <tr>
              <td width="5%" class="colHeaderCenter"></td>
              <td width="10%" class="colHeaderCenter">Source</td>
              <td width="15%" class="colHeaderCenter">Type</td>
              <td width="50%" class="colHeaderCenter">Message</td>
              <td width="20%" class="colHeaderCenter">Timestamp</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">

                <!-- Scrolling div contiains system messages -->
                <div id="messageListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <xsl:for-each select="systemMessagesList/systemMessages">
                      <xsl:sort select="@num" data-type="number"/>
                      <tr>
                        <td width="5%" align="center">
                          <input type="checkbox" name="system_message_id{system_message_id}" onclick="javascript:checkDeleteAccess();"/>
                        </td>
                        <td width="10%" align="center"><xsl:value-of select="source"/></td>
                        <td width="15%" align="center"><xsl:value-of select="type"/></td>
                        <td id="message_{@num}" width="50%" align="center"></td>
                        <td width="20%" align="center"><xsl:value-of select="timestamp"/></td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>