<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pop" match="/root/pageData/params" use="name"/>
<xsl:key name="errorMsg" match="/root/pageData/params" use="name"/>
<xsl:key name="message" match="/root/pageData/params" use="name"/>

<xsl:variable name="errorMessage" select="boolean(key('errorMsg','ERROR')/value = 'TRUE')"/>
<xsl:variable name="sentMessage" select="boolean(key('errorMsg','ERROR')/value = 'FALSE')"/>

<xsl:template match="/root">
<html>
<head>
    <title>FTD - Upload Transaction</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">
    
 <![CDATA[

      function init()
      {
        setNavigationHandlers();
      }
          
     function doSubmitAction()
     {
        var url = "UploadTransactionServlet" + 
              getSecurityParams() + "&send=upload" + "&file_name=" + document.forms[0].file_name.value;
        
        performAction(url);
     }
     
      function getSecurityParams()
      {
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
                "&context=" + document.forms[0].context.value;
      }

      function doExitAction()
      {
         var url = "OpsAdminServlet" + getSecurityParams();
         performAction(url);
      }

      function performAction(url)
      {
        document.forms[0].action = url;
        document.forms[0].submit();
      }
]]>
    </script>
</head>
<body onload="javascript:init();">
  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Upload Wal-Mart Transaction'"/>
    <xsl:with-param name="showTime" select="'true'"/>
    <xsl:with-param name="showExitButton" select="'true'"/>
  </xsl:call-template>

  <form name="uploadForm" method="post" enctype='multipart/form-data'>
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="pop_url" value="{key('pop','URL')/value}"/>

    <table border="0">
      <tr>
        <td>
          Upload file: <input type='file' name='file_name' id='file_name' />
       </td>
      </tr>
      <tr>
       <td>
           <input type='hidden' name='partner' />  
       </td>
      </tr>
      <tr>
       <td>
        <input type='button' value='Upload' onclick="javascript:doSubmitAction();" />   
       </td>
      </tr>
      <tr style="color:red">
       <td >
          <xsl:if test="$sentMessage"><xsl:value-of select="key('message','MESSAGE')/value"/></xsl:if>
          <xsl:if test="$errorMessage"><xsl:value-of select="key('message','MESSAGE')/value"/></xsl:if>
       </td>
      </tr>
 
    </table>

  </form>
  <!-- Footer Template -->
  <xsl:call-template name="footer"/>
</body>
</html>
</xsl:template>
</xsl:stylesheet>