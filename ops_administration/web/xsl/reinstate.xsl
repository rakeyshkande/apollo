<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:variable name="selectedContext" select="key('pageData','selected_context')/value"/>
<xsl:variable name="selectedStatus" select="key('pageData','selected_status')/value"/>


<xsl:template match="/root">
<html>
<head>
    <title>FTD - Re-Instate Order in JMS Workflow</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">
  /*
   *  Gobal variables
   */

		//alert(getSecurityParams());
   	
    var THRESHOLD = 20;
    var THRESHOLD_2 = 75;
    var guids = new Array(  <xsl:for-each select="reinstateOrderList/reinstateOrder">
                              <xsl:choose>
                                <xsl:when test="detail_seq_within_guid = 1">
                                  "<xsl:value-of select="normalize-space(message)"/>"
                                </xsl:when>
                                <xsl:otherwise>
                                  ""
                                </xsl:otherwise>
                                </xsl:choose>
                                  <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>                  
                             </xsl:for-each>);
    var errors = new Array( <xsl:for-each select="reinstateOrderList/reinstateOrder">
                              <xsl:choose>
                               <xsl:when test="detail_seq_within_guid = 1">
                                "<xsl:value-of select="normalize-space(error)"/>"
                               </xsl:when>
                               <xsl:otherwise>
                                  ""
                               </xsl:otherwise>                               
                              </xsl:choose>
                             <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                            </xsl:for-each>);<![CDATA[
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
      setGuids();
      setError();
    }

    function setGuids(){
      var element, guid, newGuid;

      for (var i = 0; i < guids.length; i++){
        element = document.getElementById("guid_" + (i+1));
        guid = guids[i];
        if (guid.length >= THRESHOLD){
          newGuid = "<a href='#' onclick='javascript:doGuidAction(\"" + guid + "\");'>" + guid.substring(0, THRESHOLD) + "&nbsp;&nbsp;(More)</a>";
        }
        else{
          newGuid = guid;
        }
        element.innerHTML = newGuid;
      }
    }

    function setError(){
      var element, error, newError;

      for (var i = 0; i < errors.length; i++){
        element = document.getElementById("error_" + (i+1));
        error = errors[i];
        if (error.length > 0) {
          newError = "<a href='#' onclick='javascript:doErrorAction(\"" + error + "\");'>" + "(View Error Message)</a>";
        }
        else {
          newError = "";
        }
        element.innerHTML = newError;
      }
    }

    function setScrollingDivHeight(){
      var orderListingDiv = document.getElementById("orderListing");
      var newHeight = document.body.clientHeight - orderListingDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        orderListingDiv.style.height = newHeight;
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
    var parms =   "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].contextttt.value;
    return parms;
    }

    function doGuidAction(guid){
      document.forms[0].popupOrderGuid.value = guid;
      document.forms[0].target="reinstatePopup";
      var url = "ReinstateOrderServlet" + getSecurityParams() + "&action=popupOrder";
      document.forms[0].action = url;
      window.open("","reinstatePopup","width=600,height=200,toolbar=0,resizable=yes,scrollbars=yes"); 
      var a = window.setTimeout("document.forms[0].submit();",500); 
    }


    function doReinstateAction()
    {    
      var url = "ReinstateOrderServlet" + getSecurityParams() + "&action=reinstate";
      var checkBoxes = document.getElementsByName("order_checkbox");
      for (i=0; i< checkBoxes.length; i++)
      {
        if (checkBoxes[i].type == 'checkbox' && checkBoxes[i].checked == true)
        {
          var temp = checkBoxes[i].value;
          if (checkBoxes.length == 1)
          {
            document.forms[0].guid.value = temp.substring(0, temp.indexOf('^'));
            document.forms[0].ordercontext.value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
          }
          else
          {
            document.forms[0].guid[i].value = temp.substring(0, temp.indexOf('^'));
            document.forms[0].ordercontext[i].value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
          }
        }
      }

      performAction(url);
    }


    function doRemoveAction()
    {    
      var modalText = "Are you sure you want to remove selected entries from order_exceptions table?";
      var modalDim = "dialogWidth:400px; dialogHeight:160px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../confirm.html", modalText, modalDim);
      if (ret){
          var url = "ReinstateOrderServlet" + getSecurityParams() + "&action=removeEntry";
          var checkBoxes = document.getElementsByName("order_checkbox");
          var isChecked = false;
          for (i=0; i< checkBoxes.length; i++)
          {
            if (checkBoxes[i].type == 'checkbox' && checkBoxes[i].checked == true)
            {
              isChecked = true;
              var temp = checkBoxes[i].value;
              if (checkBoxes.length == 1)
              {
                document.forms[0].guid.value = temp.substring(0, temp.indexOf('^'));
                document.forms[0].ordercontext.value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
              }
              else
              {
                document.forms[0].guid[i].value = temp.substring(0, temp.indexOf('^'));
                document.forms[0].ordercontext[i].value = temp.substring(temp.lastIndexOf('^')+1, temp.length);
              }
            }
          }
          if (isChecked) {
            performAction(url);
          } else {
            var infoText = "You trying to play me for a fool?  I know you didn't select anything. Stop playing games! ";
            window.showModalDialog("../information.html", infoText, modalDim);
          }
      }
    }


    function refreshFilter(){
        // Set the context selected from the dropdown
        var form = document.forms[0];
        form.selected_context.value = form.context_select.value;
        form.selected_status.value = form.status_select.value;

        var url = "ReinstateOrderServlet" + getSecurityParams();
        performAction(url);
    }

    function doRefreshAction(){
      var url = "ReinstateOrderServlet" + getSecurityParams() + "&action=refresh";
      uncheckAll();
      performAction(url);
    }


    function doBackAction(){
      uncheckAll();
      performAction("OpsAdminServlet" + getSecurityParams() + "&action=back");
    }

    function doExitAction(){
      performAction("ExitServlet" + getSecurityParams());
    }
    
    function performAction(url){
      document.forms[0].target="";
      document.forms[0].action = url;
      document.forms[0].submit();
    }
    
    function checkAll()
    {
      var checkBoxes = document.getElementsByName("order_checkbox");
      for (i=0; i< checkBoxes.length; i++)
      {
        if (checkBoxes[i].type == 'checkbox')
        {
          checkBoxes[i].checked = true;
        }
      }
    }
  
    function uncheckAll()
    {
      var checkBoxes = document.getElementsByName("order_checkbox");
      for (i=0; i< checkBoxes.length; i++)
      {
        if (checkBoxes[i].type == 'checkbox')
        {
          checkBoxes[i].checked = false;
        }
      }
    }
    
    function doErrorAction(error){
      var modalDim = "dialogWidth:600px; dialogHeight:250px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../information.html", error, modalDim);
    }
    
    ]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Re-Instate Order in JMS Workflow'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>
  <form name="ReinstateForm" method="post" action="ReinstateOrderServlet">
    <input type="hidden" name="sc_mode"/>
    
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="contextttt" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="selected_context" value="{key('pageData','selected_context')/value}"/>
    <input type="hidden" name="selected_status" value="{key('pageData','selected_status')/value}"/>

    <input type="hidden" name="popupOrderGuid" value=""/>


    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Reinstate Order -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <!-- Action Buttons -->
            <tr>
              <td width="80%" align="center" colspan="6">
                <input type="button" name="backButton" value="Back" tabindex="2" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
                <input type="button" name="refreshButton" value="Refresh" tabindex="1" onclick="javascript:doRefreshAction();"/>
                <input type="button" name="reinstateButton" value="Re-Instate" onclick="javascript:doReinstateAction();"/>
                <input type="button" name="removeButton" value="Remove Entry" onclick="javascript:doRemoveAction();"/>
              </td>
            </tr>
            <tr>
             <td width="100%" align="center" colspan="8">
              <hr/>
              </td>
            </tr>
            <tr>
              <td colspan="6" width="100%" align="center">
                <!-- Scrolling div contains orders -->
                <div id="orderListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="1" cellspacing="1">                
                    <tr>
                      <td width="1%" class="colHeaderCenter" align="left">
                        <a href="javascript:checkAll()"><img src="../images/all_check.gif" width="42" height="16" border="0" align="absmiddle"/></a>
                        <a href="javascript:uncheckAll()"><img src="../images/all_uncheck.gif" width="42" height="16" border="0" align="absmiddle"/></a>
                      </td>
                      <td width="20%" class="colHeaderCenter" align="left">Context</td>
                      <td width="20%" class="colHeaderCenter" align="left">Message/GUID</td>
                      <td width="10%" class="colHeaderCenter" align="left">Order Status</td>
                      <td width="7%" class="colHeaderCenter" align="left">Order Detail Id</td>
                      <td width="7%" class="colHeaderCenter" align="left">Order Number</td>
                      <td width="7%" class="colHeaderCenter" align="left">Detail Status</td>
                      <td width="14%" class="colHeaderCenter" align="left">Timestamp</td>
                      <td width="14%" class="colHeaderCenter" align="left">Error Message</td>
                   </tr>
                   <tr>
                      <td width="1%" class="colHeaderCenter" align="left">
                      </td>
                      <td width="20%" class="colHeaderCenter" align="left">               
                         <select name="context_select" onchange="refreshFilter()">
                              <xsl:choose>
                                  <xsl:when test="$selectedContext = 'none'">
                                      <option value="none">Please select context</option>
                                      <option value="">All</option>
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <option value="">All</option>
                                  </xsl:otherwise>
                              </xsl:choose>
                              <xsl:for-each select="reinstateOrderContext/reinstateContext">
                                  <xsl:choose>
                                      <xsl:when test="$selectedContext = context">
                                          <option selected="true" value="{context}"><xsl:value-of select="context"/> - (<xsl:value-of select="count"/>)</option>
                                      </xsl:when>
                                      <xsl:otherwise>
                                          <option value="{context}"><xsl:value-of select="context"/> - (<xsl:value-of select="count"/>)</option>
                                      </xsl:otherwise>
                                  </xsl:choose>
                              </xsl:for-each>
                          </select>      
                      
                      
                      </td>
                      <td width="20%" class="colHeaderCenter" align="left"></td>
                      <td width="10%" class="colHeaderCenter" align="left">                      
                          <select name="status_select" onchange="refreshFilter()">
                              <xsl:choose>
                                  <xsl:when test="$selectedStatus = 'none'">
                                      <option value="none">Please select status</option>
                                      <option value="">All</option>
                                  </xsl:when>
                                  <xsl:otherwise>
                                      <option value="">All</option>
                                  </xsl:otherwise>
                              </xsl:choose>
                              <xsl:for-each select="reinstateOrderStatus/reinstateStatus">
                                  <xsl:choose>
                                      <xsl:when test="$selectedStatus = status">
                                          <option selected="true" value="{status}"><xsl:value-of select="status"/> - (<xsl:value-of select="count"/>)</option>
                                      </xsl:when>
                                      <xsl:otherwise>
                                          <option value="{status}"><xsl:value-of select="status"/> - (<xsl:value-of select="count"/>)</option>
                                      </xsl:otherwise>
                                  </xsl:choose>
                              </xsl:for-each>
                          </select>   
                      
                      
                      </td>
                      <td width="7%" class="colHeaderCenter" align="left"></td>
                      <td width="7%" class="colHeaderCenter" align="left"></td>
                      <td width="7%" class="colHeaderCenter" align="left"></td>
                      <td width="14%" class="colHeaderCenter" align="left"></td>
                      <td width="14%" class="colHeaderCenter" align="left"></td>
                   </tr>

                    <xsl:for-each select="reinstateOrderList/reinstateOrder">
                       <xsl:choose>                       
                        <xsl:when test="1 = detail_seq_within_guid">
                            <xsl:choose>
                              <xsl:when test="position() != 1">
                               <tr>
                                <td width="100%" align="center" colspan="9">
                                  <hr/>
                                </td>
                              </tr>
                             </xsl:when>
                            </xsl:choose>
                            <input type="hidden" name="guid" value=""/>
                            <input type="hidden" name="ordercontext" value=""/>

                            <tr>
                              <td align="center">
                                  <input TYPE="checkbox" id="order_checkbox" name="order_checkbox" value="{message}^{context}" />
                              </td>
                              <td align="center"><xsl:value-of select="context"/></td>
                              <td id="guid_{@num}" align="center">&nbsp;
                              
                                      <script type="javascript">
                                              document.write("blah");
                                      </script>
                              </td>
                              <td align="center"><xsl:value-of select="order_status"/></td>
                              <td align="center"><xsl:value-of select="order_detail_id"/></td>
                              <td align="center"><xsl:value-of select="external_order_number"/></td>
                              <td align="center"><xsl:value-of select="order_detail_status"/></td>
                              <td align="center"><xsl:value-of select="timestamp"/>&nbsp;</td>                        
                              <td id="error_{@num}" align="center">&nbsp;
                              
                                      <script type="javascript">
                                              document.write("blah");
                                      </script>
                              
                              </td>
                            </tr>
                           </xsl:when>
                           <xsl:otherwise>

                            <tr>
                                <td/>
                                <td/>
                                <td id="guid_{@num}"/>
                                <td/>
                                <td align="center"><xsl:value-of select="order_detail_id"/></td>
                                <td align="center"><xsl:value-of select="external_order_number"/></td>
                                <td align="center"><xsl:value-of select="order_detail_status"/></td>
                                <td/>                        
                                <td id="error_{@num}"/>
                              </tr>
                           
                           
                           
                           </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    <tr>
                      <td width="100%" align="center" colspan="9">
                        <hr/>
                      </td>
                    </tr>
                  </table>
                </div>

              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="2" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="refreshButton" value="Refresh" tabindex="1" onclick="javascript:doRefreshAction();"/>
          <input type="button" name="reinstateButton" value="Re-Instate" onclick="javascript:doReinstateAction();"/>
          <input type="button" name="removeButton" value="Remove Entry" onclick="javascript:doRemoveAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>