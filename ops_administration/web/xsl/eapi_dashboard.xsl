<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
	
	var EXIT = "_EXIT_";
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
    }
  /*
   *  Popups
   */
    function doAddConfigPopup(){
      document.forms[0].operation.value="add";
      var url_source =  "MercuryEapiOpsEditServlet" +
                        getSecurityParams() + "&operation=add";

      performAction(url_source);      
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doRefreshAction(){
      var url = document.forms[0].action + getSecurityParams();
      performAction(url);
    }

    function doBackAction(){
      var url = "OpsAdminServlet" + getSecurityParams();
      performAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" + getSecurityParams();
      performAction(url);
    }
	
    function doAddAction(newConfig){
      document.forms[0].method="post";
      var form = document.forms[0];
      form.newMainMemberCode.value = newConfig[0];
      form.newInUse.value = newConfig[1];
      form.newAvailableFlag.value = newConfig[2];

      var url = "MercuryEapiOpsEditServlet" +
                getSecurityParams() +
                "&operation=add";

      performAction(url);
    }
    
    function toggle(source) {
	  checkBoxes = document.getElementsByName("selectRow");
	  for(var i = 0; i < checkBoxes.length; i++) {
		checkBoxes[i].checked = source.checked;
	  }
	}
	    
    function doSwitchAvailable(){
	  document.forms[0].method="post";
	  var result = "false";
      var url =  "MercuryEapiOpsEditServlet" +
                        getSecurityParams() + "&operation=available";
      checkBoxes = document.getElementsByName("selectRow");
      for(var i = 0; i < checkBoxes.length; i++){
        if(checkBoxes[i].checked) {
          result = "true";
        }
      }
      
      if(result != "true"){
        alert("Please check atleast one checkbox.");
        return;
      } 
      performAction(url);
    }
    
    function doClearInUse(){
	  document.forms[0].method="post";
      var result = "false";
      var url =  "MercuryEapiOpsEditServlet" +
                        getSecurityParams() + "&operation=inUse";
      checkBoxes = document.getElementsByName("selectRow");
      for(var i = 0; i < checkBoxes.length; i++){
        if(checkBoxes[i].checked) {
          result = "true";
        }
      }
      
      if(result != "true"){
        alert("Please check atleast one checkbox.");
        return;
      } 
      performAction(url);
    }  

    function performAction(url){
      document.forms[0].action = url;
	  document.forms[0].submit();
    }

    function mercuryDetail(suffix, status) {
      var url_source =  "MercuryDetailServlet" + getSecurityParams() +
        "&suffix=" + suffix +
        "&status=" + status;

      var modal_dim = "dialogWidth:550px; dialogHeight:400px; center:yes; status=0; help:no";
      var ret = window.showModalDialog(url_source, "", modal_dim);
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'EAPI Dashboard'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="DashboardForm" method="get" action="EAPIDashboardServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="mainMemberCode" value=""/><input type="hidden" name="inUse" value=""/>
	<input type="hidden" name="newMainMemberCode" value=""/>
	<input type="hidden" name="newInUse" value=""/>
	<input type="hidden" name="newAvailableFlag" value=""/>
	<input type="hidden" name="operation" value=""/>

    <!-- Box X table -->
   <table width="700" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
		
          <!-- Actions -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Actions</td>
            </tr>
            <tr>
              <td width="33%" align="center">
                <input type="button" name="addButton" value="Add New" onclick="javascript:doAddConfigPopup();"/>
              </td>
              <td width="33%" align="center">
                <input type="button" name="availableButton" value="Switch Available" onclick="javascript:doSwitchAvailable();"/>
              </td>
              <td width="33%" align="center">
                <input type="button" name="inUseButton" value="Clear In Use" onclick="javascript:doClearInUse();"/>
              </td>
            </tr>
          </table>

          <!-- Statistics -->
          <table width="100%" border="0" cellpadding="1" cellspacing="2">

            <!-- Column headers -->
            <tr>
              <td class="tblheader" colspan="5">EAPI Main Member Code Status</td>
            </tr>
            
            <tr>
            
            </tr>

            <tr>
              <td width="20%" class="colHeaderCenter">Check/Uncheck<input type="checkbox" onclick="toggle(this)"/></td>
              <td width="20%" class="colHeaderCenter">Main Member Code</td>
              <td width="20%" class="colHeaderCenter">In Use</td>
              <td width="20%" class="colHeaderCenter">Available</td>
              <td width="20%" class="colHeaderCenter">Last Updated On</td>
            </tr>

            <!-- Data -->
            <xsl:for-each select="eapiDashboardCountsList/eapiDashboardCounts">
            <tr>
              <td align="center"><input type="checkbox" name="selectRow" value="{main_member_code}"/></td>
              <td align="center"><xsl:value-of select="main_member_code"/></td>
              <td align="center"><xsl:value-of select="in_use"/></td>
              <td align="center"><xsl:value-of select="is_available"/><input type="hidden" name="{main_member_code}Available" value="{is_available}"/></td>
              <td align="center"><xsl:value-of select="updated_on"/></td>
            </tr>

            </xsl:for-each>

          </table>
        </td>
      </tr>
    </table>
    
    <table>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>
    
    <table width="400" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td class="TotalLine" colspan="3" align="center">Pending Mercury Summary</td>
      </tr>

      <tr>
        <td class="colHeaderCenter">Suffix</td>
        <td class="colHeaderCenter">Open</td>
        <td class="colHeaderCenter">Oldest Record</td>
      </tr>

    <!-- Data -->
    <xsl:for-each select="mercuryEapiList/mercuryEapi">
      <tr>
        <td class="label" align="center" style="text-transform:uppercase;"><xsl:value-of select="outbound_id"/></td>
        <td align="center">
        <xsl:choose>
          <xsl:when test="open_total &gt; 0">
            <a href="javascript:mercuryDetail('{outbound_id}','MO')"><xsl:value-of select="open_total"/></a>
          </xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
        </td>
        <td align="center"><xsl:value-of select="oldest_time"/></td>
      </tr>
    </xsl:for-each>

    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="refreshButton" value="Refresh" tabindex="2" onclick="javascript:doRefreshAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>
    
  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>