<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doRefreshAction(){
      performAction(document.forms[0].action);
    }

    function doBackAction(){
      performAction("OpsAdminServlet");
    }

    function doExitAction(){
      performAction("ExitServlet");
    }
    
    function mercuryDetail(suffix, status) {
      var url_source =  "MercuryDetailServlet" + getSecurityParams() +
        "&suffix=" + suffix +
        "&status=" + status;

      var modal_dim = "dialogWidth:550px; dialogHeight:400px; center:yes; status=0; help:no";
      var ret = window.showModalDialog(url_source, "", modal_dim);

    }
    
    function doSendJMS() {
      var checkboxes = document.getElementsByTagName("input");
      var suffix = "";
      for (var i=0; i< checkboxes.length; i++)
      {
        if (checkboxes[i].type == "radio" && checkboxes[i].checked)
        {
          suffix = checkboxes[i].value;
          checkboxes[i].checked = false;
        }
      }
      var url_source =  "SendJMSServlet" + getSecurityParams() +
        "&sendJMSvalue=" + suffix;

      var modal_dim = "dialogWidth:550px; dialogHeight:400px; center:yes; status=0; help:no";
      var ret = window.showModalDialog(url_source, "", modal_dim);
    }
    
    function mercuryOpsEdit(suffix) {
      var form = document.forms[0];
      form.formSuffix.value = suffix;
      url="MercuryOpsEditServlet";
      performAction(url);
    }

    function performAction(url){
      document.forms[0].action = url + getSecurityParams();
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Box X Dashboard'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="DashboardForm" method="get" action="BoxXDashboardServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="formSuffix" value=""/>

    <!-- Box X table -->
   <table width="700" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Statistics -->
          <table width="100%" border="0" cellpadding="1" cellspacing="2">

            <!-- Column headers -->
            <tr>
              <td class="TotalLine" colspan="11" align="center">Box X Status</td>
            </tr>

            <tr>
              <td width="40" class="colHeaderCenter">&nbsp;</td>
              <td width="40" class="colHeaderCenter">Suffix</td>
              <td width="40" class="colHeaderCenter">In Use</td>
              <td width="50" class="colHeaderCenter">Available</td>
              <td width="60" class="colHeaderCenter">Send New</td>
              <td width="45" class="colHeaderCenter">Sleep</td>
              <td width="60" class="colHeaderCenter">Batch Size</td>
              <td width="140" class="colHeader">Last Transmission</td>
              <td width="125" class="colHeader">Last Activity</td>
              <td width="40" class="colHeaderRight">Queued</td>
              <td width="60" class="colHeaderCenter">Send JMS</td>
            </tr>

            <!-- Data -->
            <xsl:for-each select="boxXDashboardCountsList/boxXDashboardCounts">
            <tr>
              <td align="center">
                <a href="javascript:mercuryOpsEdit('{suffix}')">
                <img src="../images/edit.gif" border="0"/>
                </a>
              </td>
              <td align="center"><xsl:value-of select="suffix"/></td>
              <td align="center"><xsl:value-of select="in_use_flag"/></td>
              <td align="center"><xsl:value-of select="available_flag"/></td>
              <td align="center"><xsl:value-of select="send_new_orders"/></td>
              <td align="center"><xsl:value-of select="sleep_time"/></td>
              <td align="center"><xsl:value-of select="batch_size"/></td>
              <td align="left"><xsl:value-of select="last_transmission"/></td>
              <td align="left"><xsl:value-of select="last_message_activity"/></td>
              <td align="right">
                <xsl:choose>
                  <xsl:when test="queued &gt; 0">
                    <a href="javascript:mercuryDetail('{suffix}','MQ')"><xsl:value-of select="queued"/></a>
                  </xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </td>
              <td align="center"><input type="radio" name="sendJMSvalue" value="{suffix}"/></td>
            </tr>

            </xsl:for-each>

          </table>
        </td>
      </tr>
    </table>
    
    <table>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>
    
    <table width="400" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td class="TotalLine" colspan="3" align="center">Pending Mercury Summary</td>
      </tr>

      <tr>
        <td class="colHeaderCenter">Suffix</td>
        <td class="colHeaderCenter">Open</td>
        <td class="colHeaderCenter">Oldest Record</td>
      </tr>

    <!-- Data -->
    <xsl:for-each select="mercuryOpenList/mercuryOpen">
      <tr>
        <td class="label" align="center" style="text-transform:uppercase;"><xsl:value-of select="outbound_id"/></td>
        <td align="center">
        <xsl:choose>
          <xsl:when test="open_total &gt; 0">
            <a href="javascript:mercuryDetail('{outbound_id}','MO')"><xsl:value-of select="open_total"/></a>
          </xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
        </td>
        <td align="center"><xsl:value-of select="oldest_time"/></td>
      </tr>
    </xsl:for-each>

    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="refreshButton" value="Refresh" tabindex="2" onclick="javascript:doRefreshAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="sendJMSButton" value="Send JMS" tabindex="2" onclick="javascript:doSendJMS();" style="padding-left:8px;padding-right:8px"/>&nbsp;
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>
    
  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>