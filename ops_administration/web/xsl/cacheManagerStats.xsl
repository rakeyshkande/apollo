<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>

  <head>
    <title>FTD - CacheManagerStats</title>
    <link rel="stylesheet" href="../css/ftd.css"/>
    <script  language="javascript"><![CDATA[
    /*
     *  Initialization
     */
      function init() {
        document.getElementById("cancel").attachEvent("onmouseover", doMouseOver);
      }

    /*
     *  Actions
     */
    function doCloseAction(){
        window.close();
    }

    function getSecurityParams(){
        return  "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].context.value;
    }

    function performAction(command, cname){
        document.forms[0].command.value = command;
        document.forms[0].cachename.value = cname;
        document.forms[0].submit();
    }


    /*
     *  Tool tip
     */
      var display = true;
      var timerId = null;
      function doMouseOver(event) {
        toolTipFloat = document.getElementById("toolTip").style;
        toolTipFloat.left = event.clientX + document.body.scrollLeft - 120;
        toolTipFloat.top = event.clientY  + document.body.scrollTop;
        toolTipFloat.display = "block";
        checkToolTip();
      }
      function checkToolTip(){
        if (display)
          timerId = setTimeout("checkToolTip()", 5000);
        else
          doMouseOut();

        display = !display;
      }
      function doMouseOut() {
        toolTipFloat.display = "none";
        display = true;
        clearTimeout(timerId);
      }]]>
    </script>
  </head>

  <body onload="javascript:init();" style="margin-top:0px;margin-left:0px;margin-right:0px;margin-bottom:0px;">
  <form name="CacheManagerStatsForm" method="post" action="CacheManagerStatsServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="command" value=""/>
    <input type="hidden" name="cachename" value=""/>
    <table border="0" cellpadding="8" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="#ADD8E6" height="27" style="padding-left:10px;border-bottom:1px solid #10659E;" width="100%">
          <img src="../images/attention.gif" border="0"/>
        </td>
        <td bgcolor="#ADD8E6" style="padding-right:10px; border-bottom:1px solid #10659E; cursor:hand;">
          <img id="cancel" src="../images/cancel.gif" border="0" align="absmiddle" onclick="javascript:doCloseAction();"  onmouseout="javascript:doMouseOut();"/>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="2" class="attentionText">
        <h2>CacheManager Statistics</h2>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="2" class="attentionText">
          <xsl:value-of select="/root/time/curtime"/>
        </td>
      </tr>

      <tr>
        <td align="center" colspan="2">
            <table border="1" cellpadding="2" cellspacing="0">
              <tr>
              <td>cacheName</td>
              <td>cacheType</td>
              <td>accessCount</td>
              <td>refreshCount</td>
              <td>refreshRate</td>
              <td>lastRefreshTime</td>
              <td>expireRate</td>
              <td>expireCheckRate</td>
              <td>firstLoadTime</td>
              <td>cacheTimer</td>
              </tr>
              <xsl:for-each select="/root/caches/CacheVO">
              <xsl:variable name="curCacheName" select="cacheName"/>
                <tr>
                <td>
                <xsl:choose>
                  <xsl:when test="cacheType = 'Transient'">
                    <a href="javascript:performAction('getTransient','{$curCacheName}');"><xsl:value-of select="cacheName"/></a>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="cacheName"/>
                  </xsl:otherwise>
                </xsl:choose>
                </td>
                <td><xsl:value-of select="cacheType"/></td>
                <td><xsl:value-of select="accessCount"/></td>
                <td><xsl:value-of select="refreshCount"/></td>
                <td><xsl:value-of select="refreshRate"/></td>
                <td><xsl:value-of select="lastRefreshTime"/></td>
                <td><xsl:value-of select="expireRate"/></td>
                <td><xsl:value-of select="expireCheckRate"/></td>
                <td><xsl:value-of select="firstLoadTime"/></td>
                <td><xsl:value-of select="cacheTimer"/></td>
                </tr>
              </xsl:for-each>
           </table>
           <br/><br/><br/>
        </td>
      </tr>


      <tr>
        <td align="center" colspan="2">
            <xsl:for-each select="/root/TransientCacheVO">
            <xsl:variable name="curCacheName" select="cacheName"/>
            <table border="1" cellpadding="2" cellspacing="0">
              <tr><td colspan="5"><xsl:value-of select="cacheName"/></td>
              </tr>
              <tr>
              <td>elementKey</td>
              <td>elementAccessCount</td>
              <td>creationTime</td>
              <td>updateTime</td>
              <td>Calculated expire time</td>
              </tr>
              <xsl:for-each select="TransientCacheElementVO">
                <tr>
                <td>
                <xsl:choose>
                  <xsl:when test="string-length(elementKey) > 30">
                    <xsl:value-of select="substring(elementKey,1,30)"/>...
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="elementKey"/>
                  </xsl:otherwise>
                </xsl:choose>
                </td>
                <td><xsl:value-of select="elementAccessCount"/></td>
                <td><xsl:value-of select="creationTime"/></td>
                <td><xsl:value-of select="updateTime"/></td>
                <td><xsl:value-of select="calculatedExpireTime"/></td>
                </tr>
             </xsl:for-each>
           </table>
           <br/><br/><br/>
           </xsl:for-each>
        </td>
      </tr>
   </table>
    <div id="toolTip" style="display:none; position:absolute; border:1px solid black; background-image: url(../images/blueButton.gif);">
      <table border="0">
        <tr>
          <td>
            Click to close window.
          </td>
        </tr>
      </table>
    </div>

  </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>