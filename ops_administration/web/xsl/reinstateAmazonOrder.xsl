<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">
  <html>
<head>
    <title>FTD - Reinstate Amazon Order</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript">
  /*
   *  Gobal variables
   */
    var THRESHOLD = 20;
    
<![CDATA[
                          
  /*
   *  Initialization
   */
    function init(){
        setNavigationHandlers();
        setScrollingDivHeight();
        window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
        var orderListingDiv = document.getElementById("orderListing");
        var newHeight = document.body.clientHeight - orderListingDiv.getBoundingClientRect().top - 100;
        if (newHeight > 15)
          orderListingDiv.style.height = newHeight;
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      var parms =   "?securitytoken=" + document.forms[0].securitytoken.value + "&context=" + document.forms[0].contextttt.value;
      return parms;
    }
    
 function displayXML(strXML)
{
	var display = window.open("","DISPLAY","height=400,width=600,resizable,scrollbars=yes");
	display.document.open();
	display.document.write("<HTML><HEAD><STYLE type='text/css'>body {font-family:Courier New;font-size:8pt;}</STYLE></HEAD><BODY><PRE>");
	display.document.write(strXML.replace(/</g,"&lt;"));
	display.document.write("</PRE></BODY></HTML>");
	display.document.close();
} 
    
    function doReinstateAction(id, context, event_name) {  
        
     var xml = document.getElementById('xml_'+id).value; 
     document.forms[0].xmlText.value = xml;
    
     
     var  url = "ReinstateAmazonOrderServlet" + 
              getSecurityParams()  + "&order_xml_id=" + id
              + "&perspective="  + context + "&event_name=" + 
              event_name +  "&action=fix_xml"; 
              
      performAction( url );
    
    }
     
    function doExitAction(){
    
      performAction("OpsAdminServlet" + getSecurityParams());
      
    }
    
    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }
   
  
    ]]>
    </script>
</head>
<body onload="javascript:init();">
<xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Reinstate Amazon Order'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
</xsl:call-template>

 <form name="ReinstateForm" method="post" action="ReinstateAmazonOrderServlet">
 
  <input type="hidden" name="xmlText" value=""/>
  
  <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
  <input type="hidden" name="contextttt" value="{key('security', 'context')/value}"/>
  <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

  
 <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Reinstate Order -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="17%" class="colHeaderCenter">Amazon Order #</td>
              <td width="18%" class="colHeaderCenter">Context</td>
              <td width="12%" class="colHeaderCenter">Event Handler</td>
              <td width="33%" class="colHeaderCenter">Error Message</td>
              <td width="20%" class="colHeaderCenter">Timestamp</td>
            </tr>
            <tr>
              <td colspan="6" width="100%" align="center">

                <!-- Scrolling div contiains orders -->
                <div id="orderListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2" id="errorTable">
                    <xsl:for-each select="reinstateOrderList/reinstateOrder ">
                      <tr>
                          <td width="17%" align="center" id='{az_error_id}'> 
                              <a href="#" onclick="javascript:doReinstateAction('{az_error_id}','{context}','{event_name}' );">
                                  <xsl:value-of select="az_error_id" />
                              </a>
                          </td>
                            <td width="18%" align="center"><xsl:value-of select="context"/></td>
                            <td width="13%" align="center"><xsl:value-of select="event_name"/></td>
                            <td width="34%" align="center"><xsl:value-of select="error"/> </td> 
                            <td width="18%" align="center"><xsl:value-of select="timestamp"/></td>
                            <input type="hidden" id="xml_{az_error_id}" value="{xml}"/>
                         
                      </tr>
                  
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>


    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
</form>
 <!-- Footer Template -->
  <xsl:call-template name="footer"/>
 
      </body>
    </html>
  </xsl:template> 
</xsl:stylesheet>
