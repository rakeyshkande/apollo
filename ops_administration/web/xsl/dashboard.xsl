<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doRefreshAction(){
      performAction(document.forms[0].action);
    }

    function doBackAction(){
      performAction("ExitServlet");
    }

    function doExitAction(){
    
    //  performAction("ExitServlet");
    window.history.back();
    }

    function performAction(url){
      document.forms[0].action = url + getSecurityParams();
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Manager Dashboard'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="DashboardForm" method="get" action="ManagerDashboardServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Statistics -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">

            <!-- Column headers -->
            <tr>
              <td class="colHeaderCenter">Category</td>
              <td class="colHeaderCenter">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="2" class="colHeaderCenter">Unscrubbed Carts</td>
                  </tr>
                  <tr>
                    <td width="50%" class="colHeaderCenter">Total</td>
                    <td width="50%" class="colHeaderCenter">Delivery/Ship Today</td>
                  </tr>
                </table>
              </td>
              <td class="colHeaderCenter">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="2" class="colHeaderCenter">Pending Carts</td>
                  </tr>
                  <tr>
                    <td width="50%" class="colHeaderCenter">Total</td>
                    <td width="50%" class="colHeaderCenter">Delivery/Ship Today</td>
                  </tr>
                </table>
              </td>
              <td class="colHeaderCenter">Removed Carts</td>
            </tr>

            <!-- Data -->
            <xsl:for-each select="dashboardCountsList/dashboardCounts">
            <tr>
              <td class="label" align="center" style="text-transform:uppercase;"><xsl:value-of select="order_origin"/></td>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="50%" align="center"><xsl:value-of select="unscrubbed_count"/></td>
                    <td width="50%" align="center"><xsl:value-of select="unscrub_ship_today_count"/></td>
                  </tr>
                </table>
              </td>
              <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="50%" align="center"><xsl:value-of select="pending_count"/></td>
                    <td width="50%" align="center"><xsl:value-of select="pending_ship_today_count"/></td>
                  </tr>
                </table>
              </td>
              <td align="center"><xsl:value-of select="removed_count"/></td>
            </tr>
            </xsl:for-each>

          </table>
        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="refreshButton" value="Refresh" tabindex="2" onclick="javascript:doRefreshAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>