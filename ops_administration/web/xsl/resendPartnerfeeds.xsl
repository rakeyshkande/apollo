<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="header1.xsl" />
	<xsl:import href="footer1.xsl" />
	<xsl:output method="html" indent="yes" />

	<xsl:key name="security" match="/root/securityParams/params"
		use="name" />
	<xsl:param name="applicationcontext">
		<xsl:value-of select="key('security','applicationcontext')/value" />
	</xsl:param>
	<xsl:param name="sitename">
		<xsl:value-of select="key('security','sitename')/value" />
	</xsl:param>
	<xsl:param name="sitenamessl">
		<xsl:value-of select="key('security','sitenamessl')/value" />
	</xsl:param>
	<xsl:template match="/root">


		<html>

			<head>
				<title>FTD - Resend Partner Feeds</title>
				<link rel="stylesheet" type="text/css" href="../css/ftd.css" />
				<link rel="stylesheet" type="text/css" href="../css/calendar.css" />
				<script type="text/javascript" src="../js/util.js"></script>
				<script type="text/javascript" src="../js/calendar.js"></script>

				<script type="text/javascript" language="javascript">
					var app_context = '<xsl:value-of select="$applicationcontext" />';
					var site_name = '<xsl:value-of select="$sitename" />';
					var site_name_ssl = '<xsl:value-of select="$sitenamessl" />';

    <![CDATA[

    /*
     *  Initializations
     */
     
      function init(){
        setNavigationHandlers();
      }
     
    /*
     *  Actions
     */
     
      function getSecurityParams(){
        return  "?securitytoken=" + document.forms[0].securitytoken.value +
                "&context=" + document.forms[0].context.value;
      }

      function doExitAction(){
        performAction("ExitServlet");
      }

    
      function performAction(url){
        document.forms[0].action = url + getSecurityParams();
        document.forms[0].submit();
      }
      
  function performSslAction(servlet) {
    document.forms[0].action = "https://" + site_name_ssl + app_context + "/servlet/" + servlet;
		document.forms[0].submit();
	}
	
 // Defect- 286
// Issues- 1 : Partner Order Numbers text field is enabled when Date radio button is defaulted after Clear button is clicked.
// added code inside clearAllFeildAction() and fixed the above Issues.

 function clearAllFeildAction()
            {
			document.getElementById("ResendPartnerFeedForm").reset();
			document.getElementById("partnerOrderNumbers").disabled = true;
			document.getElementById("partnerOrderNumbers").value = "";
			document.getElementById("startDate").disabled = false;
			document.getElementById("endDate").disabled = false;
			document.getElementById("calendar1").disabled = false;
			document.getElementById("calendar2").disabled = false;
			document.getElementById("errorMessage").innerHTML = "";
            console.log(document.getElementById("errorMessage"));
            document.sqlform.submitPageButton.disabled = false;
				        
            }
   
   // Defect- 278
   // Included the global param into alert box
           
 function setInputPartnerOrderNumbers()
			{
			    var p = document.getElementById("partnerOrderNumbers").value;
			    var count= document.getElementById("partner_order_number_count").value;
			    var partnerOrderNumberInput = "";
			    partnerOrderNumberInput = p.split("\n").join();	
			    var words = partnerOrderNumberInput.split(",");
			    if (words.length > count) 
			    {
			    	alert("Partner Order Numbers should be less than " +count);
			    	return false;
			    }else
			    {		    
			    	document.getElementById("p_partner_order_number").value = partnerOrderNumberInput;
			    	return true;
			    }
			}

// Defect 286
//Issue 4: Messages for entering invalid date in the dates fields are not user friendly
// Put javascript date validation in UI.

function isDate (year, month, day) {
    // month argument must be in the range 1 - 12
    month = month - 1;  // javascript month range : 0- 11
    var tempDate = new Date(year,month,day);
    var testYear = tempDate.getYear();
    if( testYear < 2000 ) { //Mozilla
      testYear+=1900;
    }

    if ( testYear == year &&
      (month == tempDate.getMonth()) &&
      (day == tempDate.getDate()) )
        return true;
    else
        return false
  }
  
function validateDateFormat(srcdate) {
    if( srcdate.substring(2,3)=="/" && srcdate.substring(5,6)=="/" ) {
      return isDate(srcdate.substring(6,10),srcdate.substring(0,2),srcdate.substring(3,5));
    }
    else {
      return false;
    }
  }

// Defect- 287 
// Issues- 1 : All the orders numbers are removed and user is forced to re-enter them after clicking OK on alert pop up.
// Fixes : remove the code where the order number are removed after click OK in alert pop up

function validateDates() 
{

	var startDate = document.getElementById("startDate").value;
	var endDate = document.getElementById("endDate").value;
	
		
	var start_date = Date.parse(startDate);
	var end_date = Date.parse(endDate);
	
	if ((startDate != null && startDate != "" && endDate != null && endDate != ""))
	{
		if(!validateDateFormat(startDate)) {
	         alert("Please enter a properly formatted Start Date (MM/DD/YYYY) ");
	         return false;
	        }
	        
	     if(!validateDateFormat(endDate)) {
	         alert("Please enter a properly formatted End Date (MM/DD/YYYY) ");
	          return false;
	        }
	        
		if  (start_date > end_date) 
		{
		
		alert("Start Date should be less than End Date ");
		return false;
		}
		
		if ((end_date - start_date) > 864000000)
		{
		
			alert("The range of Start Date and End Date should be less than 10 days");
			return false;
		}
		
		return true;
		
	}
        
	 
}

function formSubmission() 
{	
	var startDate = document.getElementById("startDate").value;
	var endDate = document.getElementById("endDate").value;
	
	
	if (document.getElementById("orderNumber").checked)
	{
		if(document.getElementById("partnerOrderNumbers").value == null || document.getElementById("partnerOrderNumbers").value == "")
		{
			alert("Please Enter Partner Order Numbers");
			return false;
		}
		else  
		{
			if(!setInputPartnerOrderNumbers())
			{
				return false;
			}
			 // Defect # 287
			 //Issues 4 -  Observing Blank screen if an order with single quotes entered in Partner Order Numbers text field.
			 // Doing a UI validation for the partner order number whether it contain special character (except , and - )  
			 		     			
     			var iChars = "!@#$%^&*()+=[]\\\';./{}|\":<>?";
			for (var i = 0; i < document.getElementById("partnerOrderNumbers").value.length; i++) {
				if (iChars.indexOf(document.getElementById("partnerOrderNumbers").value.charAt(i)) != -1) {
					alert ("Partner Order Numbers has special characters. Please provide valid Partner Order Numbers (Line delimited) ");
					return false;
				}
   			}	
   			
		}
	}	
	
	if (document.getElementById("date").checked)
	{
		if(!((startDate != null && startDate != "") && (endDate != null && endDate != "")))
		{
				alert("Please select Start Date and End Date");
				return false;
		}
		else
		{
			if(!validateDates())
			{
				return false;
			}	
		}
	}	
	
	document.getElementById("submitPageButton").disabled = true;
	document.ResendPartnerForm.submit();
	
	 
}
// Defect- 287 
// Issues- 2 : fixed the toggling between radio buttons, removes orders & disables Partner order numbers field when user switches from Order Numbers to Date. 
		
function toggleDisplay() {
	var searchBy;
	
	if (document.getElementById("orderNumber").checked) {
  		searchBy = document.getElementById("orderNumber").value;
	}
	else if(document.getElementById("date").checked){
		searchBy = document.getElementById("date").value;
	}
	
	if (searchBy == "orderNumber") {
		document.getElementById("partnerOrderNumbers").disabled = false;
		document.getElementById("startDate").disabled = true;
		document.getElementById("endDate").disabled = true;
		document.getElementById("calendar1").disabled = true;
		document.getElementById("calendar2").disabled = true;
		document.getElementById("calendar1").value="";
		document.getElementById("calendar2").value="";
		document.getElementById("startDate").value="";
		document.getElementById("endDate").value="";
	}
	else if(searchBy == "Date") {
		document.getElementById("partnerOrderNumbers").disabled = true;
		document.getElementById("partnerOrderNumbers").value = "";
		document.getElementById("startDate").disabled = false;
		document.getElementById("endDate").disabled = false;
		document.getElementById("calendar1").disabled = false;
		document.getElementById("calendar2").disabled = false;
	}
	 
}
            
  
      ]]>
				</script>
			</head>



			<body onload="javascript:init();">

				<!-- Header Template -->
				<xsl:call-template name="header1">
					<xsl:with-param name="headerName" select="'Resend Partner Feeds'" />
					<xsl:with-param name="showTime" select="true()" />
					<xsl:with-param name="showExitButton" select="true()" />
				</xsl:call-template>


				<form name="ResendPartnerForm"  id="ResendPartnerFeedForm" method="get"
					action="ResendPartnerFeedServlet">
					<input type="hidden" name="securitytoken"
						value="{key('security', 'securitytoken')/value}" />
					<input type="hidden" name="context"
						value="{key('security', 'context')/value}" />
					<input type="hidden" name="adminAction"
						value="{key('security', 'adminAction')/value}" />
					<input type="hidden" name="selected_context" value="" />
					<input id="p_partner_order_number" type="hidden" name="p_partner_order_number" />
					<input id="partner_order_number_count" type="hidden" name="partner_order_number_count" value="{/root/data/PageData/value}" />
					
					
					


					<table width="98%" border="3" align="center" cellpadding="1"
						cellspacing="1" bordercolor="#006699">

						<tr>
							<td>

								<table width="100%" border="0" cellpadding="2"
									cellspacing="2">

									<tr>
										<td>Search by :</td>
										<td>
											<input id="orderNumber" type="radio" name="select"
												value="orderNumber" style="padding-left:8px;padding-right:8px"
												onclick="toggleDisplay()" />&nbsp;&nbsp;&nbsp;&nbsp;Order
											Numbers &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</td>
										<td align="left">
											<input id="date" type="radio" name="select" value="Date"
												checked="true" style="padding-left:8px;padding-right:8px"
												onclick="toggleDisplay()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date
										</td>
									</tr>

									<tr>
										<td>Partner Id :</td>
										<td>
											<select name="partnerId">
												<xsl:for-each select="/root/PartnerList/Partner">
													<option>
														<xsl:value-of select="name" />
													</option>
												</xsl:for-each>
											</select>
										</td>
									</tr>

									<tr>
										<td>Feed Type :</td>
										<td>
											<select name="feedType">
												<option id="feedType" value="ADJUSTMENT">Adjustment
													Feeds</option>
												<option id="feedType" value="FULFILLMENT">Fulfillment
													Feeds</option>
												<option id="feedType" value="DCON">DCON</option>
											</select>
										</td>
									</tr>

									<tr>
										<td width="16%">Partner Order Numbers :</td>
										<td>
											<textarea name="partnerOrderNumbers" id="partnerOrderNumbers"
												rows="3" cols="20" disabled="true"
												onBlur="javascript:setInputPartnerOrderNumbers();">
											</textarea>
										</td>
									</tr>
									<tr>
										<td>Start Date :</td>
										<td width="13%">
											<input id="startDate" type="text" size="10" maxlength="10"
												name="startDate" value="{key('pageData', 'startDate')/value}"
												tabindex="30"   />
                &nbsp;&nbsp;
											<input type="image" id="calendar1" tabindex="35"
												BORDER="0" SRC="../images/calendar.gif" width="16" height="16" />
										</td>
									</tr>

									<tr>
										<td>End Date :</td>
										<td width="13%">
											<input id="endDate" type="text" size="10" maxlength="10"
												name="endDate" value="{key('pageData', 'endDate')/value}"
												tabindex="31"  />
                &nbsp;&nbsp;
											<input type="image" id="calendar2" tabindex="36"
												BORDER="0" SRC="../images/calendar.gif" width="16" height="16" />
										</td>
									</tr>

									<tr>
										<td id="errorMessage" height="40px" colspan="3" style="color:red;">
											<xsl:for-each select="/root/DateMessage/UpdateStatusMessage">
												<xsl:value-of select="value" />
											</xsl:for-each>

											<xsl:for-each select="/root/OrderNumberMessage/UpdateStatusMessage">
												<xsl:value-of select="value" />
												<br />

											</xsl:for-each>

										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					<!-- Footer Template -->
					<xsl:call-template name="footer1" />
				</form>



			</body>
		</html>

		<script type="text/javascript">
			Calendar.setup(
			{
			inputField : "startDate", // ID of the input field
			ifFormat : "mm/dd/y", // the date format
			button : "calendar1" // ID of the button
			}
			);

			Calendar.setup(
			{
			inputField : "endDate", // ID of the input field
			ifFormat : "mm/dd/y", // the date format
			button : "calendar2" // ID of the button
			}
			);

		</script>


	</xsl:template>
</xsl:stylesheet>