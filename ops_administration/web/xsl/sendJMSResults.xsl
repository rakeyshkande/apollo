<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>Send JMS Messages</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Actions
   */
    function doCloseAction(){
      window.close();
    }]]>
    </script>
</head>

<body>

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Send JMS Messages'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="false()"/>
  </xsl:call-template>

  <form name="DashboardForm" method="get" action="BoxXDashboardServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
  </form>
  
  <!-- Box X table -->
   <table width="500" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Fields -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">

            <tr>
              <td class="Label">Suffix</td>
              <td class="Label">Result</td>
            </tr>

            <xsl:for-each select="resultParams/params">
              <tr>
                <td><xsl:value-of select="name"/></td>
                <td><xsl:value-of select="value"/></td>
             </tr>
            </xsl:for-each>
          
          </table>
        </td>
      </tr>
    </table>
    
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="80%" align="center">
          <input type="button" name="closeButton" value="Close" tabindex="1" onclick="javascript:doCloseAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
        </td>
      </tr>
    </table>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>