<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>Inventory Tracking Maintenance Screen</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <link rel="stylesheet" type="text/css" href="../css/calendar.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" src="../js/calendar.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Actions
   */
    function init() {
        //alert('invAction: ' + document.inventoryForm.invAction.value);
        if (document.inventoryForm.invAction.value == 'load') {
            document.getElementById("ftdQty").disabled = true;
            document.getElementById("canQty").disabled = true;
            document.getElementById("rejQty").disabled = true;
            document.getElementById("tableName").focus();
        } else if (document.inventoryForm.invAction.value == 'retrieve') {
            document.getElementById("productId").disabled = true;
            document.getElementById("shipDate").disabled = true;
            document.getElementById("calendar1").disabled = true;
            document.getElementById("vendorId").disabled = true;
            document.getElementById("ftdQty").focus();
        } else if (document.inventoryForm.invAction.value == 'update') {
            document.getElementById("productId").disabled = true;
            document.getElementById("shipDate").disabled = true;
            document.getElementById("calendar1").disabled = true;
            document.getElementById("vendorId").disabled = true;
            document.getElementById("ftdQty").disabled = true;
            document.getElementById("canQty").disabled = true;
            document.getElementById("rejQty").disabled = true;
        }
    }

    function tableChange(sel) {
        var value = sel.options[sel.selectedIndex].value;
        //alert('tableName: ' + value);
        if (value == 'INV_TRK_ASSIGNED_COUNT') {
            document.getElementById("vendorDiv").style.display="block";
        } else {
            document.getElementById("vendorDiv").style.display="none";
        }
    }

    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doBackAction(){
      var url = "OpsAdminServlet" +
                getSecurityParams();
      performAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" +
                getSecurityParams();
      performAction(url);
    }
    
    function doPostAction(){
      msg = "";
      if (document.inventoryForm.invAction.value == 'retrieve') {
          if (isNaN(document.getElementById("ftdQty").value) || document.getElementById("ftdQty").value < 0) {
              msg = msg + "FTD Count must be a non-negative number\n";
          }
          if (isNaN(document.getElementById("canQty").value) || document.getElementById("canQty").value < 0) {
              msg = msg + "CAN Count must be a non-negative number\n";
          }
          if (isNaN(document.getElementById("rejQty").value) || document.getElementById("rejQty").value < 0) {
              msg = msg + "REJ Count must be a non-negative number\n";
          }
      }
      if (msg != "") {
        alert(msg);
        return;
      }
      var url = "InventoryTrackingUpdateServlet" +
              getSecurityParams();
      if (document.inventoryForm.invAction.value == 'load') {
        document.inventoryForm.invAction.value = 'retrieve';
      } else {
        document.inventoryForm.invAction.value = 'update';
      }
      document.getElementById("productId").disabled = false;
      document.getElementById("shipDate").disabled = false;
      document.getElementById("calendar1").disabled = false;
      document.getElementById("vendorId").disabled = false;
      document.getElementById("ftdQty").disabled = false;
      document.getElementById("canQty").disabled = false;
      document.getElementById("rejQty").disabled = false;
      performAction(url);
    }

    function doClearAction(){
      var url = "InventoryTrackingUpdateServlet" +
                getSecurityParams();
      document.inventoryForm.invAction.value = '';
      document.getElementById("productId").value = '';
      document.getElementById("shipDate").value = '';
      document.getElementById("calendar1").value = '';
      document.getElementById("vendorId").value = '';
      document.getElementById("ftdQty").value = '';
      document.getElementById("canQty").value = '';
      document.getElementById("rejQty").value = '';
      performAction(url);
    }

    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="init()">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Inventory Tracking Maintenance Screen'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="inventoryForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="invAction" value="{key('pageData', 'invAction')/value}"/>

    <table width="600" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Fields -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">

            <tr>
              <xsl:choose>
                <xsl:when test="key('pageData','result')/value = 'success'">
                  <td class="OperationSuccess">
                    Update Successful
                  </td>
                </xsl:when>
                <xsl:when test="key('pageData','result')/value != ''">
                  <td class="ErrorMessage">
                    <xsl:value-of select="key('pageData','result')/value"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td>&nbsp;</td>
                </xsl:otherwise>
              </xsl:choose>
            </tr>
            <tr>
              <td class="Label" width="50%">Table</td>
              <td width="50%">
                <xsl:choose>
                  <xsl:when test="key('pageData', 'invAction')/value = 'load'">
                    <select name="tableName" tabindex="10" onchange="tableChange(this)">
                      <option value="INV_TRK_ASSIGNED_COUNT">INV_TRK_ASSIGNED_COUNT</option>
                      <option value="INV_TRK_COUNT">INV_TRK_COUNT</option>
                    </select>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="key('pageData','tableName')/value"/>
                    <input type="hidden" name="tableName" value="{key('pageData', 'tableName')/value}"/>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="Label" width="50%">Product Id</td>
              <td width="50%"><input type="text" name="productId" value="{key('pageData', 'productId')/value}" tabindex="20" size="15" maxlength="15"/></td>
            </tr>
            <tr>
              <td class="Label" width="50%">Ship Date</td>
              <td>
                <input type="text" size="10" maxlength="10" name="shipDate" value="{key('pageData', 'shipDate')/value}" tabindex="30"/>
                &nbsp;&nbsp;<input type="image" id="calendar1" tabindex="35" BORDER="0" SRC="../images/calendar.gif" width="16" height="16"/>
              </td>
            </tr>
            <tr id="vendorDiv">
              <td class="Label" width="50%">Vendor Id</td>
              <td width="50%"><input type="text" name="vendorId" value="{key('pageData', 'vendorId')/value}" tabindex="40" size="15" maxlength="15"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
              <td class="Label" width="50%">FTD Count</td>
              <td width="50%"><input type="text" name="ftdQty" value="{key('pageData', 'ftdQty')/value}" tabindex="50" size="10" maxlength="10"/></td>
            </tr>
            <tr>
              <td class="Label" width="50%">CAN Count</td>
              <td width="50%"><input type="text" name="canQty" value="{key('pageData', 'canQty')/value}" tabindex="60" size="10" maxlength="10"/></td>
            </tr>
            <tr>
              <td class="Label" width="50%">REJ Count</td>
              <td width="50%"><input type="text" name="rejQty" value="{key('pageData', 'rejQty')/value}" tabindex="70" size="10" maxlength="10"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>

          </table>
        </td>
      </tr>
    </table>
    
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="100" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>
          <xsl:choose>
            <xsl:when test="key('pageData', 'invAction')/value = 'load'">
              <input type="button" name="submitButton" value="Submit" tabindex="110" onclick="javascript:doPostAction();" style="padding-left:8px;padding-right:8px"/>
            </xsl:when>
            <xsl:when test="key('pageData', 'invAction')/value = 'retrieve'">
              <input type="button" name="submitButton" value="Submit" tabindex="120" onclick="javascript:doPostAction();" style="padding-left:8px;padding-right:8px"/>
              <input type="button" name="clearButton" value="Clear" tabindex="130" onclick="javascript:doClearAction();" style="padding-left:8px;padding-right:8px"/>
            </xsl:when>
            <xsl:when test="key('pageData', 'invAction')/value = 'update'">
              <input type="button" name="clearButton" value="Clear" tabindex="140" onclick="javascript:doClearAction();" style="padding-left:8px;padding-right:8px"/>
            </xsl:when>
          </xsl:choose>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="150" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
    
  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "shipDate",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar1"  // ID of the button
    }
  );
</script>

</xsl:template>
</xsl:stylesheet>