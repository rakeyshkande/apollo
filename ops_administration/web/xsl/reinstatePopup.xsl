<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:template match="/root">
<html>
<head>
    <title>FTD - Re-Instate Order in JMS Workflow</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</head>

<body>

    <table width="98%" height="100%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Stats -->
          <table border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td>
                 <xsl:value-of select="key('pageData','popupOrderGuid')/value"/>
              </td>
            </tr>
          </table>
          <br/>
          <table align="center" width="60%" border="0" cellpadding="2" cellspacing="2">
            <xsl:for-each select="orderStatsList/orderStats">
            <tr>
              <td align="left">
                 <xsl:value-of select="external_order_number"/>
              </td>
              <td align="left">
                 <xsl:value-of select="column_name"/>
              </td>
              <td align="left">
                 <xsl:value-of select="info"/>
              </td>
            </tr>
            </xsl:for-each>
          </table>
          
        </td>
      </tr>
    </table>
        
</body>
</html>

</xsl:template>
</xsl:stylesheet>