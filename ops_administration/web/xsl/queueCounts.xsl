<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Counts of JMS Queues</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
      var countListingDiv = document.getElementById("countListing");
      var newHeight = document.body.clientHeight - countListingDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        countListingDiv.style.height = newHeight;
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doBackAction(){
      performAction("OpsAdminServlet");
    }

    function doRefreshAction(){
      performAction(document.forms[0].action);
    }

    function doExitAction(){
      performAction("ExitServlet");
    }
    
    function performAction(url){
      document.forms[0].action = url + getSecurityParams();
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Counts of JMS Queues'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="QueueCountForm" method="get" action="QueueCountsServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Queue Counts -->
          <table width="100%" align="center" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Counts</td>
            </tr>
            <tr>
              <td width="50%" class="colHeaderCenter">Queue Name</td>
              <td width="25%" class="colHeaderCenter">Count</td>
              <td width="25%" class="colHeaderCenter">Oldest Record</td>
            </tr>
            <tr>
              <td colspan="3" width="100%" align="center">

                <!-- Scrolling div contiains orders -->
                <div id="countListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <xsl:for-each select="queueCountsList/queueCounts">
                      <tr>
                        <td width="50%" align="center">
                          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="bgcolor">#FFF8DC</xsl:attribute></xsl:if>
                          <xsl:value-of select="queue_name"/>
                        </td>
                        <td width="25%" align="center">
                          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="bgcolor">#FFF8DC</xsl:attribute></xsl:if>
                          <xsl:value-of select="queue_count"/>
                        </td>
                        <td width="25%" align="center">
                          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="bgcolor">#FFF8DC</xsl:attribute></xsl:if>
                          <xsl:value-of select="min_enq_time"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="refreshButton" value="Refresh" tabindex="2" onclick="javascript:doRefreshAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>