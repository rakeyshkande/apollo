<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>Mercury Ops Edit</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doExitAction(){
      performAction("ExitServlet");
    }

    function doBackAction(){
      document.forms[0].method="get";
      performAction("BoxXDashboardServlet");
    }

    function doPostAction(){
      var url = "MercuryOpsEditServlet";
      performAction(url);
    }

    function performAction(url){
      document.forms[0].action = url + getSecurityParams();
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Mercury Ops Edit'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="MercuryOpsForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>

    <!-- Box X table -->
   <table width="400" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Fields -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
          
          <xsl:for-each select="SUFFIXS/SUFFIX">
          
            <tr>
              <td class="Label" width="60%">Suffix</td>
              <td class="Label" width="40%"><xsl:value-of select="@suffix"/></td>
              <input type="hidden" name="formSuffix" value="{@suffix}"/>
            </tr>
            <tr>
              <td class="Label">In Use</td>
              <td>
                <xsl:choose>
                  <xsl:when test="@in_use_flag='Y'">
                    <input type="radio" name="inUse" value="Y" checked="Y">Yes</input>
                    <input type="radio" name="inUse" value="N">No</input>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="radio" name="inUse" value="Y" disabled="true">Yes</input>
                    <input type="radio" name="inUse" value="N" checked="Y">No</input>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="Label">Available Flag</td>
              <td>
                <xsl:choose>
                  <xsl:when test="@available_flag='Y'">
                    <input type="radio" name="availableFlag" value="Y" checked="Y">Yes</input>
                    <input type="radio" name="availableFlag" value="N">No</input>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="radio" name="availableFlag" value="Y">Yes</input>
                    <input type="radio" name="availableFlag" value="N" checked="Y">No</input>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="Label">Send New Orders</td>
              <td>
                <xsl:choose>
                  <xsl:when test="@send_new_orders='Y'">
                    <input type="radio" name="sendNew" value="Y" checked="Y">Yes</input>
                    <input type="radio" name="sendNew" value="N">No</input>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="radio" name="sendNew" value="Y">Yes</input>
                    <input type="radio" name="sendNew" value="N" checked="Y">No</input>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="Label">Sleep Time</td>
              <td><input type="text" name="sleepTime" size="5" value="{@sleep_time}"/></td>
            </tr>
            <tr>
              <td class="Label">Batch Size</td>
              <td><input type="text" name="batchSize" size="5" value="{@batch_size}"/></td>
            </tr>
	        <tr>
	          <td class="Label">ACK Attempts</td>
	          <td>
	          	<xsl:variable name='currentAckCount' select="@ack_count"/>
				<select name="ackCount">
				  <xsl:for-each select="/root/ackCombo/param">
                    <option value="{name}">
                        <xsl:if test="$currentAckCount = name"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="value"/>
                    </option>
  				  </xsl:for-each>
				</select>
			  </td>
			</tr>
			<tr>
              <td class="Label">Get Last ACK Response</td>
			  <td>
                <xsl:choose>
                  <xsl:when test="@ack_ask='Y'">
                    <input type="radio" name="ackAsk" value="Y" checked="Y">Yes</input>
                    <input type="radio" name="ackAsk" value="N">No</input>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="radio" name="ackAsk" value="Y">Yes</input>
                    <input type="radio" name="ackAsk" value="N" checked="Y">No</input>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
			</tr>
			<tr>
              <td class="Label">Resend Last Batch</td>
              <td>
                <xsl:choose>
                  <xsl:when test="@resend_batch='Y'">
                    <input type="radio" name="resendBatch" value="Y" checked="Y">Yes</input>
                    <input type="radio" name="resendBatch" value="N">No</input>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="radio" name="resendBatch" value="Y">Yes</input>
                    <input type="radio" name="resendBatch" value="N" checked="Y">No</input>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
			</tr> 
            
          </xsl:for-each>
          
          </table>
        </td>
      </tr>
    </table>
    
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
          <input type="button" name="postButton" value="Submit" tabindex="2" onclick="javascript:doPostAction();" style="padding-left:8px;padding-right:8px"/>&nbsp;
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="3" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>
    
  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>