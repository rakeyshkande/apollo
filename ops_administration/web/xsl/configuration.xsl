<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<xsl:variable name="selectedContext" select="key('pageData','selected_context')/value"/>

<html>
<head>
    <title>FTD - System Configuration</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <link rel="stylesheet" type="text/css" href="../css/tooltip.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Global variables
   */
    var EXIT = "_EXIT_";
    var checkcntr = 0;

  /*
   *  Initialization
   */
    function init(){
      //setNavigationHandlers();  Disabled since too slow for page with this many form fields
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
      var configDiv = document.getElementById("configListing");
      var newHeight = document.body.clientHeight - configDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        configDiv.style.height = newHeight;
    }

  /*
   *  Events
   */
    function doValueBlur(input){
      if (input.defaultValue == input.value){
        setButtonActionAccess(true);
        document.getElementById("id_" + input.key).checked = false;
      }
      else {
        setButtonActionAccess(false);
        document.getElementById("id_" + input.key).checked = true;
      }
    }

  /*
   *  Description input field control
   */
    function toggleDescriptionInput(inputfield){
      if (document.getElementById(inputfield).style.display == "inline") {
        document.getElementById(inputfield).style.display = "none";
      } else {
        document.getElementById(inputfield).style.display = "inline";
      }
    }

  /*
   *  Button control
   */
    function checkActionAccess(input){
      if (input.checked == false) {
        setButtonActionAccess(true);
      } else {
        setButtonActionAccess(false);
      }
    }
    
    function setButtonActionAccess(access){
      var form = document.forms[0];
      if (access == false) {
        checkcntr++;
      } else {
        checkcntr--;
      }
      if (checkcntr > 0) {
        form.deleteButton.disabled = false;
        form.changesButton.disabled = false;
      } else {
        form.deleteButton.disabled = true;
        form.changesButton.disabled = true;
        checkcntr = 0;
      }
    }

  /*
   *  Popups
   */
    function doAddConfigPopup(){
      var form = document.forms[0];
      var url_source =  "ConfigurationServlet" +
                        getSecurityParams() +
                        "&action=add";

      var modal_dim = "dialogWidth:600px; dialogHeight:400px; center:yes; status=0; help:no";
      var ret = window.showModalDialog(url_source, "", modal_dim);

      if (ret && ret != null && ret[0] != EXIT){
        doAddAction(ret);
      }
      else if (ret && ret != null && ret[0] == EXIT){
        doExitAction();
      }
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }

    function doAddAction(newConfig){
      var form = document.forms[0];
      form.new_context.value = newConfig[0];
      form.new_name.value = newConfig[1];
      form.new_value.value = newConfig[2];
      form.new_description.value = newConfig[3];

      var url = "ConfigurationServlet" +
                getSecurityParams() +
                "&action=add";

      performAction(url);
    }

    function doAction(action){
      var modalText = "Are you sure?";
      var modalDim = "dialogWidth:200px; dialogHeight:150px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = window.showModalDialog("../confirm.html", modalText, modalDim);
      if (ret){
        var url = "ConfigurationServlet" +
                  getSecurityParams() +
                  "&action=" + action;

        performAction(url);
      }
    }

    function refreshContext(){
        // Set the context selected from the dropdown
        var form = document.forms[0];
        form.selected_context.value = form.context_select.value;
        
        var url = "ConfigurationServlet" +
          getSecurityParams();

        performAction(url);
    }

    function doBackAction(){
      var url = "OpsAdminServlet" +
                getSecurityParams();
      performAction(url);
    }

    function doExitAction(){
      var url = "ExitServlet" +
                getSecurityParams();
      performAction(url);
    }
    
    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'System Configuration'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="SystemConfigurationForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="new_context"/>
    <input type="hidden" name="new_name"/>
    <input type="hidden" name="new_value"/>
    <input type="hidden" name="new_description"/>
    <input type="hidden" name="selected_context" value="{key('pageData','selected_context')/value}"/>


    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Actions -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Actions</td>
            </tr>
            <tr>
              <td width="33%" align="center">
                <input type="button" name="addButton" value="Add" onclick="javascript:doAddConfigPopup();"/>
              </td>
              <td width="33%" align="center">
                <input type="button" name="changesButton" value="Apply Changes" disabled="true" onclick="javascript:doAction('update');"/>
              </td>
              <td width="33%" align="center">
                <input type="button" name="deleteButton" value="Delete" disabled="true" onclick="javascript:doAction('delete');"/>
              </td>
            </tr>
          </table>

          <!-- System Configuration -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Messages</td>
            </tr>
            <tr>
              <td width="5%" class="colHeaderCenter"></td>
              <td width="30%" class="colHeaderCenter">Context:&nbsp;
                <select name="context_select" onchange="refreshContext()">
                            <option value="">All</option>
                            <xsl:for-each select="contextsList/contexts">
                                <xsl:choose>
                                    <xsl:when test="$selectedContext = context">
                                        <option selected="true" value="{context}"><xsl:value-of select="context"/></option>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <option value="{context}"><xsl:value-of select="context"/></option>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </select>
              </td>
              <td width="30%" class="colHeaderCenter">Name</td>
              <td width="30%" class="colHeaderCenter">Value</td>
            </tr>
            <tr>
              <td colspan="5" width="100%" align="center">

                <!-- Scrolling div contiains system messages -->
                <div id="configListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <xsl:for-each select="globalParmsList/globalParms">
                      <xsl:sort select="@num" data-type="number"/>
                      <tr>
                        <td width="5%" align="center">
                          <input type="checkbox" name="id_{position()}" key="{position()}" onclick="javascript:checkActionAccess(this);"/>
                          <input type="hidden" name="context_{position()}" value="{context}"/>
                          <input type="hidden" name="name_{position()}" value="{name}"/>
                        </td>
                        <td width="30%" align="center"><xsl:value-of select="context"/></td>
                        <td width="30%" align="center">
                          <a href="#" class="tooltip" onclick="javascript:toggleDescriptionInput('descIn_{position()}');return false;">
                          <xsl:value-of select="name"/>
                          <span><xsl:choose>
                            <xsl:when test="string-length(description) > 0"><xsl:value-of select="description"/></xsl:when>
                            <xsl:otherwise>No description defined (click to add)</xsl:otherwise>
                            </xsl:choose>
                          </span></a>
                        </td>
                        <td width="30%">
                          <input type="text" name="value_{position()}" key="{position()}" value="{value}" size="60" maxlength="150" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="3" align="left"><div id="descIn_{position()}" style="display: none">
                        <input type="text" name="desc_{position()}" key="{position()}" value="{description}" size="180" maxlength="255" onblur="javascript:doValueBlur(this);fieldBlur();" onclick="javascript:fieldFocus();"/>
                        </div></td>
                      </tr>
                    </xsl:for-each>
                    <tr><td>&nbsp;</td></tr>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="1" onclick="javascript:doBackAction();" style="padding-left:8px;padding-right:8px"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>