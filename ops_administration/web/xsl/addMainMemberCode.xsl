<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="security" match="/root/securityParams/params" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - New Main Member Code</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Global variables
   */
    var EXIT = "_EXIT_";
    var fieldNames = new Array("mainMemberCode", "inUse", "availableFlag");

  /*
   *  Initialization
   */
    function init(){
      addDefaultListenersArray(fieldNames);
      document.forms[0].mainMemberCode.focus();
    }

  /*
   *  Events
   */
    function doContextSwitch(checkbox){
      var select = document.getElementById("contextSelect").style;
      var text = document.getElementById("contextText").style;
      
      if (checkbox.value == "E"){
        select.display = "block";
        text.display = "none";
      }
      else if (checkbox.value == "N"){
        select.display = "none";
        text.display = "block";
      }
    }

  /*
   *  Validation
   */
    function validateMainMemberCode(){
      document.forms[0].mainMemberCode.value = document.forms[0].mainMemberCode.value.toUpperCase();
	  var mainMemberCode = document.forms[0].mainMemberCode.value;
	  if(mainMemberCode == "" || mainMemberCode == null){
      	alert("Main member code should not be empty or null");
        return false;
      }
      
      var pattern = /^\d{2}-\d{4}\w{2}$/;
     
      if(!mainMemberCode.match(pattern)) {
      	alert("Invalid main member code");
        return false;
      } 
      return true;
    }

  /*
   *  Actions
   */
    function getSecurityParams(){
      return  "?securitytoken=" + document.forms[0].securitytoken.value +
              "&context=" + document.forms[0].context.value;
    }
    
    function doBackAction(){
      var url = "EAPIDashboardServlet" + getSecurityParams();
      performAction(url);
    }
    
    function doExitAction(){
      window.returnValue = new Array(EXIT);
      doCloseAction();
    }

    function doCloseAction(){
      window.close();
    }

    function doAddAction(){
      if (validateMainMemberCode()){

        var form = document.forms[0];
        var ret = new Array();
        form.newMainMemberCode.value = form.mainMemberCode.value;
		if (form.inUse[0].checked) {
		  form.newInUse.value = "Y";
		} else {
		    form.newInUse.value = "N";
		}
        if (form.availableFlag[0].checked) {
		  form.newAvailableFlag.value = "Y";
		} else {
		    form.newAvailableFlag.value = "N";
		}
        var url = "MercuryEapiOpsEditServlet" +
                getSecurityParams() +
                "&operation=add";

        performAction(url);
        //doCloseAction();
      }
    }

    function performAction(url){
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'New Main Member Code'"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="AddMercuryEapiOpsForm" method="post" action="MercuryEapiOpsEditServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="adminAction" value="{key('security', 'adminAction')/value}"/>
    <input type="hidden" name="newMainMemberCode" value=""/>
	<input type="hidden" name="newInUse" value=""/>
	<input type="hidden" name="newAvailableFlag" value=""/>
	
    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Settings -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Settings</td>
            </tr>
            <tr>
              <td width="15%" class="label">Main Member Code:</td>
              <td width="20%"><input type="text" name="mainMemberCode"/></td>
              <td width="33%">&nbsp;</td>
            </tr>
            <tr>
              <td class="Label">In Use</td>
              <td>
                <input type="radio" name="inUse" value="Y" disabled="true">Yes</input>
                <input type="radio" name="inUse" value="N" checked="Y">No</input>
              </td>
              <td width="33%">&nbsp;</td>
            </tr>
            <tr>
              <td width="15%" class="label">Available</td>
              <td width="20%">
                <input type="radio" name="availableFlag" value="Y">Yes</input>
                <input type="radio" name="availableFlag" value="N" checked="Y">No</input>
              </td>
              <td width="33%">&nbsp;</td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="80%" align="center">
          <input type="button" name="backButton" value="Back" tabindex="30" onClick="javascript:doBackAction();"/>&nbsp;
          <input type="button" name="submitButton" value="Submit" tabindex="30" onClick="javascript:doAddAction();"/>&nbsp;
          <input type="button" name="closeButton" value="Close" tabindex="30" onClick="javascript:doCloseAction();"/>
        </td>
        <td width="20%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>