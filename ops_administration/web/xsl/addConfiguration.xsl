<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - New Configuration</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Global variables
   */
    var EXIT = "_EXIT_";
    var fieldNames = new Array("config_context_text", "config_name", "config_value", "config_description");

  /*
   *  Initialization
   */
    function init(){
      addDefaultListenersArray(fieldNames);
      document.forms[0].config_context_select.focus();
    }

  /*
   *  Events
   */
    function doContextSwitch(checkbox){
      var select = document.getElementById("contextSelect").style;
      var text = document.getElementById("contextText").style;
      
      if (checkbox.value == "E"){
        select.display = "block";
        text.display = "none";
      }
      else if (checkbox.value == "N"){
        select.display = "none";
        text.display = "block";
      }
    }

  /*
   *  Validation
   */
    function validateContextSelect(){
      var form = document.forms[0];

      if (form.context_type[0].checked){
        if (form.config_context_select.selectedIndex == 0){
          setErrorField("config_context_select");
          alert("Please select a valid context.");
          return false;
        }
      }
      return true;
    }

    function validateDescription(){
      var form = document.forms[0];
      if (form.config_description.value.length > 255) {
        setErrorField("config_description");
        alert("Hey that's a nice description, but it's too long - sorry it must be less than 255 chars.");
        return false;
      }
      return true;
    }

  /*
   *  Actions
   */
    function doExitAction(){
      window.returnValue = new Array(EXIT);
      doCloseAction();
    }

    function doCloseAction(){
      window.close();
    }

    function doAddAction(){
      if (validateContextSelect() && validateDescription()){

        var form = document.forms[0];
        var ret = new Array();

        if (form.context_type[0].checked)
          ret[0] = form.config_context_select[form.config_context_select.selectedIndex].value;
        else if (form.context_type[1].checked)
          ret[0] = form.config_context_text.value;

        ret[1] = form.config_name.value;
        ret[2] = form.config_value.value;
        ret[3] = form.config_description.value;

        window.returnValue = ret;
        doCloseAction();
      }
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'New Configuration'"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="AddConfigurationForm" method="get" action="ConfigurationServlet">

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Settings -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Settings</td>
            </tr>
            <tr>
              <td width="15%" class="label">Context:</td>
              <td width="20%">
                <div id="contextSelect" style="display:block">
                  <select name="config_context_select">
                    <option value="">Select One</option>
                    <xsl:for-each select="contextsList/contexts">
                      <option value="{context}"><xsl:value-of select="context"/></option>
                    </xsl:for-each>
                  </select>
                </div>
                <div id="contextText" style="display:none">
                  <input type="text" name="config_context_text"/>
                </div>
              </td>
              <td width="33%" class="label">
                <input type="radio" name="context_type" value="E" onclick="javascript:doContextSwitch(this);" checked="true"/>Existing&nbsp;&nbsp;
                <input type="radio" name="context_type" value="N" onclick="javascript:doContextSwitch(this);"/>New
              </td>
            </tr>
            <tr>
              <td width="15%" class="label">Name:</td>
              <td width="20%"><input type="text" name="config_name"/></td>
              <td width="33%">&nbsp;</td>
            </tr>
            <tr>
              <td width="15%" class="label">Value:</td>
              <td width="20%"><input type="text" name="config_value"/></td>
              <td width="33%">&nbsp;</td>
            </tr>
            <tr>
              <td width="15%" class="label">Description:</td>
              <td colspan="2"><textarea name="config_description" rows="3" cols="80"/></td>
            </tr>

          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="60%" align="center">
          <input type="button" name="submitButton" value="Submit" tabindex="30" onClick="javascript:doAddAction();"/>&nbsp;
          <input type="button" name="closeButton" value="Close" tabindex="30" onClick="javascript:doCloseAction();"/>
        </td>
        <td width="20%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>