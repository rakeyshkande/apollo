package com.ftd.osp.opsadministration.bo;

import com.ftd.osp.opsadministration.dao.ContentDAO;
import com.ftd.osp.opsadministration.presentation.form.BaseContentForm;
import com.ftd.osp.opsadministration.presentation.form.ContentDetailForm;
import com.ftd.osp.opsadministration.presentation.form.ContentSetupEditForm;

import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.ContentFilterVO;
import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.sql.Connection;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ContentBO
{
    private static Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.bo.ContentBO");
    
    public ContentBO()
    {
    }

    public void getMasterList(Connection con, BaseContentForm form) throws Exception
    {
        
        ContentDAO dao = new ContentDAO();
        
        String contextFilter = form.getContextFilter();
        List<ContentMasterVO> contentMasterList = dao.getContentMasterList(con, contextFilter);
        
        form.setContentMasterList(contentMasterList);
        
        Map<String,ContentMasterVO> contentContextMap = dao.getContentContextMap(con);
        form.setContentContextMap(contentContextMap);
    }
    
    public void getFilterMap(Connection con, BaseContentForm form) throws Exception
    {
        ContentDAO dao = new ContentDAO();
        
        Map<Long,ContentFilterVO> contentFilterMap = dao.getContentFilterMap(con);

        form.setContentFilterMap(contentFilterMap);     
    }

    public void getDetailList(Connection con, BaseContentForm form) throws Exception
    {
        ContentDAO dao = new ContentDAO();
        String contextFilter = form.getContextFilter();
        List<ContentDetailVO> contentDetailList = dao.getContentDetailList(con, contextFilter);
        form.setContentDetailList(contentDetailList);
    }
    
    public void getDetailById(Connection con, String detailId, ContentDetailForm form) throws Exception
    {
        ContentDAO dao = new ContentDAO();
        ContentDetailVO contentDetailVO = dao.getContentDetailById(con, detailId);
        form.setContentDetailVO(contentDetailVO);
    }
    
    public void getNewDetail(Connection con, String masterId, ContentDetailForm form) throws Exception
    {
        ContentDAO dao = new ContentDAO();
        ContentDetailVO contentDetailVO = dao.getContentMasterById(con, masterId);
        form.setContentDetailVO(contentDetailVO);
    }
    
    public void getMasterVO(ContentSetupEditForm contentSetupForm)
    {
        // find the master from the list
        List<ContentMasterVO> contentMasterList = contentSetupForm.getContentMasterList();
        for (int i=0; i < contentMasterList.size(); i++)
        {
            ContentMasterVO vo = contentMasterList.get(i);
            if (vo.getContentMasterId() == contentSetupForm.getContentMasterId())
            {
                contentSetupForm.setContentMasterVO(vo);
                logger.debug("Found master vo for " + contentSetupForm.getContentMasterId());
            }
        }
    }

    public void saveMaster(Connection con, ContentSetupEditForm contentSetupEditForm) throws Exception
    {
        if (validMaster(contentSetupEditForm))
        {
            ContentDAO dao = new ContentDAO();
            ContentMasterVO vo = new ContentMasterVO();
            vo.setContentMasterId(contentSetupEditForm.getContentMasterId());
            vo.setContext(contentSetupEditForm.getContentContext().toUpperCase());
            vo.setName(contentSetupEditForm.getContentName().toUpperCase());
            vo.setDescription(contentSetupEditForm.getContentDescription());
            vo.setFilter1Id(contentSetupEditForm.getContentFilter1Id());
            vo.setFilter2Id(contentSetupEditForm.getContentFilter2Id());
            vo.setUpdatedBy(contentSetupEditForm.getUpdatedBy());
            
            String result = dao.saveContentMaster(con,vo);
            if (result == null || result.equals("")) result = "success";
            contentSetupEditForm.setSaveResult(result);
            logger.debug("result: " + result);
            if (contentSetupEditForm.getContentMasterId() <= 0 && result.equalsIgnoreCase("success")) {
                contentSetupEditForm.setContentMasterId(vo.getContentMasterId());
                logger.debug("Creating default detail record");
                ContentDetailForm cdf = new ContentDetailForm();
                cdf.setContentMasterId(vo.getContentMasterId());
                cdf.setText(contentSetupEditForm.getDefaultValue());
                cdf.setUpdatedBy(vo.getUpdatedBy());
                String detailResult = saveDetail(con, cdf);
                logger.debug("detail save result: " + detailResult);
            }
        }
    }
    
    public String saveDetail(Connection con, ContentDetailForm contentDetailForm) throws Exception {
        String result = "";
        
        if (validDetail(contentDetailForm)) {
            ContentDetailVO vo = new ContentDetailVO();
            vo.setContentDetailID(contentDetailForm.getContentDetailId());
            vo.setContentMasterID(contentDetailForm.getContentMasterId());
            vo.setFilter1Value(contentDetailForm.getFilter1Value());
            vo.setFilter2Value(contentDetailForm.getFilter2Value());
            vo.setText(contentDetailForm.getText());
            vo.setUpdatedBy(contentDetailForm.getUpdatedBy());
            
            ContentDAO dao = new ContentDAO();
            result = dao.saveContentDetail(con, vo);

        }

        return result;
    }
    
    public boolean validMaster(ContentSetupEditForm contentSetupEditForm)
    {
        logger.debug("validMaster()");
        
        logger.debug("id = " + contentSetupEditForm.getContentMasterId());            
        logger.debug("context = " + contentSetupEditForm.getContext());            
        logger.debug("name = " + contentSetupEditForm.getContentName());            
        logger.debug("F1 = " + contentSetupEditForm.getContentFilter1Id());            
        logger.debug("F2 = " + contentSetupEditForm.getContentFilter2Id());            
        logger.debug("upd = " + contentSetupEditForm.getUpdatedBy());
        logger.debug("default value: " + contentSetupEditForm.getDefaultValue());
        
        boolean result = true;
        return result;
    }

    public boolean validDetail(ContentDetailForm contentDetailForm) {
        logger.debug("validDetail()");
        logger.debug("detailId: " + contentDetailForm.getContentDetailId());
        logger.debug("masterId: " + contentDetailForm.getContentMasterId());
        logger.debug("filter1 value: " + contentDetailForm.getFilter1Value());
        logger.debug("filter2 value: " + contentDetailForm.getFilter2Value());
        logger.debug("text: " + contentDetailForm.getText());
        boolean result = true;
        
        return result;
    }

    /**
     * Gets the userId based on the sessionId found in the request object.
     * @param request
     */
    public String getUserId(HttpServletRequest request) {

        String userId = null;
        String defaultUserId = "nouserfound";
        String sessionId = request.getParameter("securitytoken");
        if( sessionId!=null ) {
            try {
                SecurityManager sm = SecurityManager.getInstance();
                UserInfo userInfo = sm.getUserInfo(sessionId);
                userId = userInfo.getUserID();
            } catch (Exception e) {
                logger.debug(e);
                logger.warn("Unable to determine user id");
            }
        }
        // if still no user, because of error or problem
        if( userId==null || userId.equalsIgnoreCase("")) {
            logger.debug("Setting userId to default....");
            userId = defaultUserId;
        }
        logger.debug("userid: " + userId);
        return userId;
    }

}
