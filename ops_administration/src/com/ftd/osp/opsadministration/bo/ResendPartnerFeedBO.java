package com.ftd.osp.opsadministration.bo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.opsadministration.dao.ResendPartnerFeedDAO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author rksingh
 * 
 */
public class ResendPartnerFeedBO {

	private static final String DATA_SOURCE = "ORDER SCRUB";
	private static final String OUT_MESSAGE = "RegisterOutMessage";
	private static final String OUT_INVALID_ORD_NUM_CURSOR = "RegisterOutCursor";
	private static final String STATUS_PARAM = "RegisterOutParameterStatus";
	private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
	

	private static Logger logger = new Logger("com.ftd.osp.opsadministration.bo.ResendPartnerFeedBO");

	/**
	 * @param partnerOrderNumberList
	 * @param partnerFeedType
	 * @param partnerId
	 * @param updatedBy
	 * @return
	 */
	public Map<String, String> updatePartnerFeedByPartnerOrdNum(List<String> partnerOrderNumberList, String partnerFeedType,
			String partnerId, String updatedBy) {
		
		if(partnerOrderNumberList == null || partnerOrderNumberList.size() == 0) {
			logger.info("No partner order number found to update");
			return null;
		}
				
		Connection conn = null;
		String commaSeperatedOrderNumbers = null;
		Map<String, Object> outputs = null;
		Map<String, String> results = null;
		try {			
		
			commaSeperatedOrderNumbers = getListAsString(partnerOrderNumberList, ",", "'");	
						
			logger.info("Partner order number ---->  " + commaSeperatedOrderNumbers);	
			logger.info("Updating the records for Feed Type " + partnerFeedType + " and for the partner " + partnerId + " for given partner oredr number" );

			conn = DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

			// Call the DAO
			ResendPartnerFeedDAO rsnPFDAO = new ResendPartnerFeedDAO(conn);
			outputs = rsnPFDAO.updatePartnerFeedStausByNumber(commaSeperatedOrderNumbers, partnerFeedType, partnerId, updatedBy);
			
			if(outputs != null) {
				
				results = new LinkedHashMap<String, String>();				
				results.put("UpdateStatus", (String)outputs.get(OUT_MESSAGE));
				
				logger.info((String)outputs.get(OUT_MESSAGE));
				
				
				
				if(outputs.get(OUT_INVALID_ORD_NUM_CURSOR) != null) {	

					// get the valid error numbers and construct invalid error numbers list.
					CachedResultSet rs = (CachedResultSet) outputs.get(OUT_INVALID_ORD_NUM_CURSOR);
					List<String> validOrderList = new ArrayList<String>();
					while (rs.next()) {
						validOrderList.add(rs.getString("valid_partner_number"));
					}				
					partnerOrderNumberList.removeAll(validOrderList);
				
					if(partnerOrderNumberList == null || partnerOrderNumberList.size() == 0) {
						results.put("OrderValidation", "All the orders are  valid and have " + partnerFeedType + " records");
						logger.info("All the orders are  valid and have " + partnerFeedType + " records");
					} else {					
						results.put("OrderValidation",( "List of Invalid Partner Order Numbers:  "+getListAsString(partnerOrderNumberList, ",", " ") ) );
						logger.info("List of Invalid Partner Order Numbers:  "+getListAsString(partnerOrderNumberList, ",", " ") );
					}
				}
				
				return results;
			}
			
		} catch(Exception e) {
			logger.error("Error caught updatePartnerFeedByPartnerOrdNum, ", e);
			results = new HashMap<String, String>();
			results.put("UpdateStatus", "Cannot update the partner order feed status. Internal Error, " + e.getMessage());
			
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error("Error caught closing connection, ", e);
			}
		}
		
		return null;		
	}
	
	/**
	 * @param list
	 * @param seperator
	 * @param quote
	 * @return
	 */
	private String getListAsString(List<String> list, String seperator, String quote) {
		StringBuffer string = new StringBuffer();
		if (list != null && list.size() > 0) {
			int count = 1;
			for (String element : list) {
				string.append(quote);
				string.append(element);
				string.append(quote);

				if (count != list.size()) {
					string.append(seperator);
				}
				count++;
			}
		}
    return string.toString();
 }
	
	public Map<String, String> updatePartnerFeedBydate(String partnerFeedType, String startDate, String endDate,String partnerId , String updatedBy) {
		
			
		Connection conn = null;
		Map<String, Object> outputs = null;
		Map<String, String> results = null;
		try {			
			
				conn = DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

				logger.info("Updating the records for Feed Type " + partnerFeedType + " and for the partner " + partnerId + " between " + startDate + " and " + endDate );
			// Call the DAO
			ResendPartnerFeedDAO rsnPFDAO = new ResendPartnerFeedDAO(conn);
			outputs = rsnPFDAO.updatePartnerFeedStausByDate(partnerFeedType, startDate, endDate, partnerId ,updatedBy);
									
			if(outputs != null) {
				
				results = new HashMap<String, String>();				
				results.put((String)outputs.get(STATUS_PARAM), (String)outputs.get(MESSAGE_PARAM));	
				logger.info((String)outputs.get(MESSAGE_PARAM));
						
				return results;
			}
			
		} catch(Exception e) {
			logger.error("Error caught updatePartnerFeedBydate, ", e);
			results = new HashMap<String, String>();
			results.put("UpdateStatus", "Cannot update the partner order feed status. Internal Error, " + e.getMessage());
			
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error("Error caught closing connection, ", e);
			}
		}
		
		return null;		
	}


}
