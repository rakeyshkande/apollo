package com.ftd.osp.opsadministration.bo;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.GiftCodeUpdateBulkReq;

/**
 * @author rksingh
 * 
 */
public class GrouponRefundBO {

	private static Logger logger = new Logger(
			"com.ftd.osp.opsadministration.bo.GrouponRefundBO");
	private static String COMMA = ",";

	/**
	 * This Method is for Updating the Groupon coupon by coupon number provided
	 * in the Gift and Coupons Refund Screen.
	 * 
	 * @param gCcouponList
	 *            - list of grouponCoupons
	 * @param intialStatus
	 *            -
	 * @param finalStatus
	 * @param amount
	 * @param expiryDate
	 * @param updatedBy
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public Map<String, String> updateGrouponByCouponNumber(
			List<String> gCcouponList, String intialStatus, String finalStatus,
			String amount, String expiryDate, String updatedBy) {

		GiftCodeUpdateBulkReq request = new GiftCodeUpdateBulkReq();
		request.setGiftCodeId(gCcouponList);
		request.setIssueAmount(amount);
		Date expirationDate = null;
		try {
			if (null != expiryDate) {
				expirationDate = new SimpleDateFormat("MM/dd/yyyy")
						.parse(expiryDate);
			}
		} catch (ParseException e1) {
			logger.error("Unparseable Date : " + expiryDate);
			e1.printStackTrace();
		}
		request.setExpirationDate(expirationDate);
		request.setNewStatus(finalStatus);
		List<String> intialStatusLst = new ArrayList<String>(
				Arrays.asList(intialStatus.split(COMMA)));
		request.setCurrentGCStatus(intialStatusLst);
		request.setUpdatedBy(updatedBy);
		request.setIsBatch(Boolean.FALSE);
		Map<String, String> results = new LinkedHashMap<String, String>();
		Integer count = 0;
		String displayMessage = null;
		try {
			count = com.ftd.osp.utilities.GiftCodeUtil.updateBulk(request);
			displayMessage = ((null != count ? (Integer.toString(count)) : "0") + " Groupon coupons got successfully updated.");
		} catch (Exception ex) {
			displayMessage = "Gift Code Service did not return OK. Details : "
					+ ex.getMessage();
		}
		logger.debug("Count of Gift Certificates Updated:  " + count);
		results.put("UpdateStatus : ", displayMessage);
		return results;
	}

	/**
	 * This Method is for Updating the Groupon coupon by batch/request number
	 * provided in the Gift and Coupons Refund Screen.
	 * 
	 * @param gCcouponList
	 *            - list of batch number
	 * @param intialStatus
	 *            -
	 * @param finalStatus
	 * @param amount
	 * @param expiryDate
	 * @param updatedBy
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws JAXBException
	 */

	public Map<String, String> updateGrouponByBatchNumber(
			List<String> batchNumberList, String intialStatus,
			String finalStatus, String amount, String expiryDate,
			String updatedBy) {

		GiftCodeUpdateBulkReq request = new GiftCodeUpdateBulkReq();
		request.setBatchNumber(batchNumberList);
		request.setIssueAmount(amount);
		Date expirationDate = null;
		try {
			if (null != expiryDate) {
				expirationDate = new SimpleDateFormat("MM/dd/yyyy")
						.parse(expiryDate);
			}
		} catch (ParseException e1) {
			logger.error("Unparseable Date : " + expiryDate);
			e1.printStackTrace();
		}
		request.setExpirationDate(expirationDate);
		request.setNewStatus(finalStatus);
		List<String> intialStatusLst = new ArrayList<String>(
				Arrays.asList(intialStatus.split(COMMA)));
		request.setCurrentGCStatus(intialStatusLst);
		request.setUpdatedBy(updatedBy);
		request.setIsBatch(Boolean.TRUE);

		Map<String, String> results = new LinkedHashMap<String, String>();
		Integer count = 0;
		String displayMessage = null;
		try {
			count = com.ftd.osp.utilities.GiftCodeUtil.updateBulk(request);
			displayMessage = ((null != count ? (Integer.toString(count)) : "0") + " Groupon coupons got successfully updated.");
		} catch (Exception ex) {
			displayMessage = "Gift Code Service did not return OK. Details : "
					+ ex.getMessage();
		}
		logger.debug("Count of Gift Certificates Updated:  " + count);
		results.put("UpdateStatus : ", displayMessage);
		return results;
	}
}
