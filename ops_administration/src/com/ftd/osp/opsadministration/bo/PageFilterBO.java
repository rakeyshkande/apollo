package com.ftd.osp.opsadministration.bo;

import com.ftd.osp.opsadministration.dao.ContentDAO;
import com.ftd.osp.opsadministration.dao.PageFilterDAO;
import com.ftd.osp.opsadministration.presentation.form.ContentDetailForm;
import com.ftd.osp.opsadministration.presentation.form.PageFilterEditForm;
import com.ftd.osp.opsadministration.presentation.form.PageFilterListForm;
import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.PageFilterActionVO;
import com.ftd.osp.opsadministration.vo.PageFilterDistroVO;
import com.ftd.osp.opsadministration.vo.PageFilterVO;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;


import javax.servlet.http.HttpServletRequest;

public class PageFilterBO
{
    private static Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.bo.PageFilterBO");
    
    public PageFilterBO()
    {
    }
    /*
    public PageFilterBO(PageFilterEditForm form) 
    {
    	form.setNopageStartHour("");
    	form.setNopageEndHour("");
    	form.setOverwriteDistro("");
    
    }
*/
    public void getPageFilterList(Connection con, PageFilterListForm form) throws Exception
    {
        
        PageFilterDAO dao = new PageFilterDAO();
        
        String actionFilter = form.getActionFilter();
        logger.debug("action filter: " + actionFilter);
        List<PageFilterVO> pageFilterList = dao.getPageFilterList(con, actionFilter);
        
        form.setPageFilterList(pageFilterList);
        
        //Map<String,ContentMasterVO> contentContextMap = dao.getContentContextMap(con);
        //form.setContentContextMap(contentContextMap);
    }
    
    public void getActionTypeList(Connection con, PageFilterListForm form) throws Exception
    {
        
        PageFilterDAO dao = new PageFilterDAO();
        
        
        List<PageFilterActionVO> actionTypeList = dao.getActionTypeList(con);
        
        form.setActionTypeList(actionTypeList);
        
    }   
    
    public void getDistroList(Connection con, PageFilterEditForm form) throws Exception
    {
        
        PageFilterDAO dao = new PageFilterDAO();
        
        
        List<PageFilterDistroVO> distroList = dao.getPageDistroList(con);
        
        form.setDistroList(distroList);
        
    }  
    
    public static void getNopageHourList(Connection con, PageFilterEditForm form) throws Exception
    {
        
        List<String> hourList = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
        	String s = String.valueOf(i);
        	if(s.length() == 1) {
        		s = "0" + s;
        	}
        	hourList.add(s);
        }
        form.setHourList(hourList);
        
    }   
    
    public void getPageFilterById(Connection con, PageFilterEditForm form) throws Exception
    {
        long filterId = form.getFilterId();
        PageFilterDAO dao = new PageFilterDAO();

        PageFilterVO vo = dao.getPageFilterById(con, filterId);
        
        form.setFilterId(vo.getFilterId());
        form.setFilterName(vo.getFilterName());
        form.setFilterAction(vo.getActionType());
        form.setMsgSource(vo.getMessageSource());
        form.setMsgSubject(vo.getMessageSubject());
        form.setMsgBody(vo.getMessageBody());
        form.setNopageStartHour(vo.getNopageStartHour());
        form.setNopageEndHour(vo.getNopageEndHour());
        form.setOverwriteDistro(vo.getOverwriteProject());
        form.setSubjectPrefix(vo.getSubjectPrefix());
    }  
    
    public String deletePageFilter(Connection con, PageFilterListForm form) throws Exception
    {	
        long filterId = form.getFilterId();
        PageFilterDAO dao = new PageFilterDAO();

        String result = dao.deletePageFilter(con, filterId);
        
        if (result == null || result.equals("")) {
        	result = "success";
        }
        form.setSaveResult(result);
        return result;
    }     
    
    public String savePageFilter(Connection con, PageFilterEditForm pageFilterEditForm) throws Exception {
        String result = "";
        
        if (validDetail(pageFilterEditForm)) {
            PageFilterVO vo = new PageFilterVO();
            vo.setFilterId(pageFilterEditForm.getFilterId());
            vo.setFilterName(pageFilterEditForm.getFilterName());
            vo.setActionType(pageFilterEditForm.getFilterAction());
            vo.setMessageSource(pageFilterEditForm.getMsgSource());
            vo.setMessageSubject(pageFilterEditForm.getMsgSubject());
            vo.setMessageBody(pageFilterEditForm.getMsgBody());
            vo.setNopageStartHour(pageFilterEditForm.getNopageStartHour());
            vo.setNopageEndHour(pageFilterEditForm.getNopageEndHour());
            vo.setOverwriteProject(pageFilterEditForm.getOverwriteDistro());
            vo.setSubjectPrefix(pageFilterEditForm.getSubjectPrefix());
            vo.setUpdatedBy(pageFilterEditForm.getUpdatedBy());
            
            if(vo.getFilterName() != null) {
            	vo.setFilterName(vo.getFilterName().trim());	
            }
            if(vo.getActionType() != null) {
            	vo.setActionType(vo.getActionType().trim());
            }
            if(vo.getMessageSource() != null) {
            	vo.setMessageSource(vo.getMessageSource().trim());	
            }
            if(vo.getMessageSubject() != null) {
            	vo.setMessageSubject(vo.getMessageSubject().trim());	
            }
            if(vo.getMessageBody() != null) {
            	vo.setMessageBody(vo.getMessageBody().trim());	
            }
            if(vo.getNopageStartHour() != null) {
            	vo.setNopageStartHour(vo.getNopageStartHour().trim());	
            }    
            if(vo.getNopageEndHour() != null) {
            	vo.setNopageEndHour(vo.getNopageEndHour().trim());	
            }            
            if(vo.getSubjectPrefix() != null) {
            	vo.setSubjectPrefix(vo.getSubjectPrefix().trim());	
            }  
            if(vo.getOverwriteProject() != null) {
            	vo.setOverwriteProject(vo.getOverwriteProject().trim());	
            }  
            
            
            
            PageFilterDAO dao = new PageFilterDAO();
            result = dao.savePageFilter(con, vo);

            if (result == null || result.equals("")) {
            	result = "success";
            }
            pageFilterEditForm.setFilterId(vo.getFilterId());
            pageFilterEditForm.setSaveResult(result);
        }

        return result;
    }    
    
    public String updatePageFilterStatus(Connection con, PageFilterListForm form, HttpServletRequest request)  {
    	String result = "";
    	String[] idArray = request.getParameterValues("updateStatusIds");
    	String[] valueArray = request.getParameterValues("updateStatusValues");
    	PageFilterVO vo = null;
    	String errorMsg = "Updates completed but these filter ids failed to update: ";
    	String returnedMsg = "";
    	boolean hasError = false;
    	PageFilterDAO dao = new PageFilterDAO();
    	
        for (int i = 0; i < idArray.length; i++) {
            String filterId = idArray[i];
            String filterStatus = valueArray[i];
            
            logger.debug("updatePageFilterStatus id:" + filterId);
            logger.debug("updatePageFilterStatus status:" + filterStatus);
            vo = new PageFilterVO();
            vo.setFilterId(Long.valueOf(filterId));
            vo.setActiveFlag(filterStatus);
            vo.setUpdatedBy(this.getUserId(request));
            
            try {
            	returnedMsg = dao.updatePageFilterStatus(con, vo);
            	logger.error("Filter id " + filterId + " failed due to:" + returnedMsg);
            	if(returnedMsg != null && returnedMsg != "") {
            		logger.error("Filter id " + filterId + " failed to update status due to:" + returnedMsg);
            		hasError = true;
            		errorMsg = errorMsg + filterId + " ";
            		
            	}
            	
            } catch (Exception e) {
            	logger.error("Filter id " + filterId + " failed to update status due to:" + e.getMessage());
            	logger.error(e);
            	hasError = true;
            	errorMsg = errorMsg + filterId + " ";	
            }
        }
        if(hasError) {
        	result = errorMsg;
        } else {
        	result = "success";
        }
        form.setSaveResult(result);
    	return result;
    	
    }  
    
    public boolean validDetail(PageFilterEditForm pageFilterEditForm) {
        logger.debug("validDetail()");
        logger.debug("filterId: " + pageFilterEditForm.getFilterId());
        logger.debug("filterName: " + pageFilterEditForm.getFilterName());
        logger.debug("actionType: " + pageFilterEditForm.getFilterAction());
        logger.debug("msgSource: " + pageFilterEditForm.getMsgSource());
        logger.debug("msgSubject:" + pageFilterEditForm.getMsgSubject());
        logger.debug("msgBody: " + pageFilterEditForm.getMsgBody());
        logger.debug("nopageStartHour: " + pageFilterEditForm.getNopageStartHour());
        logger.debug("nopageEndHour: " + pageFilterEditForm.getNopageEndHour());
        logger.debug("subjectPrefix: " + pageFilterEditForm.getSubjectPrefix());
        logger.debug("overwriteDistro: " + pageFilterEditForm.getOverwriteDistro());
        boolean result = true;
        
        return result;
    }    


    /**
     * Gets the userId based on the sessionId found in the request object.
     * @param request
     */
    public String getUserId(HttpServletRequest request) {

        String userId = null;
        String defaultUserId = "nouserfound";
        String sessionId = request.getParameter("securitytoken");
        if( sessionId!=null ) {
            try {
                SecurityManager sm = SecurityManager.getInstance();
                UserInfo userInfo = sm.getUserInfo(sessionId);
                userId = userInfo.getUserID();
            } catch (Exception e) {
                logger.debug(e);
                logger.warn("Unable to determine user id");
            }
        }
        // if still no user, because of error or problem
        if( userId==null || userId.equalsIgnoreCase("")) {
            logger.debug("Setting userId to default....");
            userId = defaultUserId;
        }
        logger.debug("userid: " + userId);
        return userId;
    }

}
