package com.ftd.osp.opsadministration.dao;

import com.ftd.osp.opsadministration.vo.ContentMasterVO;
import com.ftd.osp.opsadministration.vo.PageFilterActionVO;
import com.ftd.osp.opsadministration.vo.PageFilterDistroVO;
import com.ftd.osp.opsadministration.vo.PageFilterVO;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageFilterDAO
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    public PageFilterDAO()
    {
    }
 
    public List<PageFilterVO> getPageFilterList(Connection conn, String filterAction) throws Exception 
    {
    	List<PageFilterVO> pageFilterList = new ArrayList<PageFilterVO>();
        PageFilterVO vo = null;
        
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAGE_FILTER_LIST");
        inputParms.put("IN_ACTION_TYPE", filterAction);
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new PageFilterVO();
            vo.setFilterId(results.getBigDecimal("SYSTEM_MSG_FILTER_ID").longValue());
            vo.setFilterName(results.getString("FILTER_NAME"));
            vo.setActiveFlag(results.getString("ACTIVE_FLAG"));
            vo.setMessageSource(results.getString("MSG_SOURCE"));
            vo.setMessageSubject(results.getString("MSG_SUBJECT"));
            vo.setMessageBody(results.getString("MSG_BODY"));
            vo.setActionType(results.getString("ACTION_TYPE"));
            vo.setNopageStartHour(results.getString("NOPAGE_START_HOUR"));
            vo.setNopageEndHour(results.getString("NOPAGE_END_HOUR"));
            vo.setSubjectPrefix(results.getString("SUBJECT_PREFIX_TXT"));
            vo.setOverwriteProject(results.getString("OVERWRITE_DISTRO_TXT"));
            
            //logger.debug("filter name: " + vo.getFilterName());
            //logger.debug("message source: " + vo.getMessageSource());
            pageFilterList.add(vo);
        }
        logger.debug("Got " + pageFilterList.size() + " values for GET_PAGE_FILTER");
        return pageFilterList;
    }        

    public PageFilterVO getPageFilterById(Connection conn, long filterId) throws Exception 
    {
        PageFilterVO vo = null;
        
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_FILTER_BY_ID");
        inputParms.put("IN_SYSTEM_MSG_FILTER_ID", new BigDecimal(filterId));
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() ) 
        {
            vo = new PageFilterVO();
            vo.setFilterId(results.getBigDecimal("SYSTEM_MSG_FILTER_ID").longValue());
            vo.setFilterName(results.getString("FILTER_NAME"));
            vo.setActiveFlag(results.getString("ACTIVE_FLAG"));
            vo.setMessageSource(results.getString("MSG_SOURCE"));
            vo.setMessageSubject(results.getString("MSG_SUBJECT"));
            vo.setMessageBody(results.getString("MSG_BODY"));
            vo.setActionType(results.getString("ACTION_TYPE"));
            vo.setNopageStartHour(results.getString("NOPAGE_START_HOUR"));
            vo.setNopageEndHour(results.getString("NOPAGE_END_HOUR"));
            vo.setSubjectPrefix(results.getString("SUBJECT_PREFIX_TXT"));
            vo.setOverwriteProject(results.getString("OVERWRITE_DISTRO_TXT"));
        }
        
        return vo;
    }       
    
    public List<PageFilterActionVO> getActionTypeList(Connection conn) throws Exception 
    {
    	List<PageFilterActionVO> actionTypeList = new ArrayList<PageFilterActionVO>();
        PageFilterActionVO vo;
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ACTION_TYPE_LIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
        	vo = new PageFilterActionVO();
        	vo.setActionType(results.getString("ACTION_TYPE"));
        	vo.setActionDesc(results.getString("ACTION_DESC"));
        	vo.setPriority(results.getString("PRIORITY"));
            actionTypeList.add(vo);
        }
        logger.debug("Got " + actionTypeList.size() + " values for GET_ACTION_TYPE_LIST");
        return actionTypeList;
    }    

    public List<PageFilterDistroVO> getPageDistroList(Connection conn) throws Exception 
    {
    	List<PageFilterDistroVO> distroList = new ArrayList<PageFilterDistroVO>();
        PageFilterDistroVO vo;
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAGE_DISTRO_LIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
        	vo = new PageFilterDistroVO();
        	vo.setSitescopeProject(results.getString("PROJECT"));
            distroList.add(vo);
        }
        logger.debug("Got " + distroList.size() + " values for GET_PAGE_DISTRO_LIST");
        return distroList;
    }    
    
    public String savePageFilter(Connection con, PageFilterVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INS_UPD_SYSTEM_MSG_FILTER");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_FILTER_NAME", vo.getFilterName());
        inputParams.put("IN_ACTION_TYPE", vo.getActionType());
        inputParams.put("IN_MSG_SOURCE", vo.getMessageSource());
        inputParams.put("IN_MSG_SUBJECT", vo.getMessageSubject());
        inputParams.put("IN_MSG_BODY", vo.getMessageBody());
        inputParams.put("IN_NOPAGE_START_HOUR", vo.getNopageStartHour());
        inputParams.put("IN_NOPAGE_END_HOUR", vo.getNopageEndHour());
        inputParams.put("IN_SUBJECT_PREFIX_TXT", vo.getSubjectPrefix());
        inputParams.put("IN_OVERWRITE_DISTRO_TXT", vo.getOverwriteProject());
        inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());
        

        if (vo.getFilterId() == 0)
        {
            inputParams.put("IN_SYSTEM_MSG_FILTER_ID", null);
        }
        else
        {
            inputParams.put("IN_SYSTEM_MSG_FILTER_ID", new BigDecimal(vo.getFilterId()));
        }

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
            logger.error(message);
        } else {
            // If the key was zero, then we need the new key from the sequence
            String idString = (String) outputs.get("OUT_SYSTEM_MSG_FILTER_ID");
            logger.debug("out_cmi = " + idString);
            vo.setFilterId(Long.parseLong(idString));
            
        }
        
        return message;
    }    
    
    public String deletePageFilter(Connection con, long filterId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        HashMap<String, Object> inputParms = new HashMap();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("DELETE_PAGE_FILTER");
        inputParms.put("IN_SYSTEM_MSG_FILTER_ID", new BigDecimal(filterId));
        dataRequest.setInputParams(inputParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
            logger.error(message);
        }         
        return message;
    }    
    
    public String updatePageFilterStatus(Connection con, PageFilterVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("UPD_PAGE_FILTER_STATUS");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_SYSTEM_MSG_FILTER_ID", new BigDecimal(vo.getFilterId()));
        inputParams.put("IN_ACTIVE_FLAG", vo.getActiveFlag());
        inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());

        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
            logger.error(message);
        }         
        return message;
    }        

}
