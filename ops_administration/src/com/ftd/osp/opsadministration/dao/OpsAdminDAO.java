package com.ftd.osp.opsadministration.dao;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


public class OpsAdminDAO {
  
  private Connection conn;
  protected Logger logger = new Logger(this.getClass().getName());
  
  // custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  private static final String Y_STRING = "Y";
  private static final String N_STRING = "N";
  
  public OpsAdminDAO(Connection conn) {
    this.conn = conn;
  }
  
  /**
   * Updates the MercuryEapiOps row for the given main member code.
   * 
   * @param mainMemberCode
   * @param inUse
   * @param available
   * @param updatedBy
   * @throws Exception
   */
  public void updateMercuryEapiOps(String mainMemberCode, String inUse, String available, String updatedBy) throws Exception
  {   
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      logger.error("DB connection is null");
      return;
    }
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("UPDATE_MERCURY_EAPI_OPS_ADMIN");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE",mainMemberCode);
    paramMap.put("IN_IN_USE",inUse);
    paramMap.put("IN_IS_AVAILABLE",available);
    paramMap.put("IN_UPDATED_BY", updatedBy);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

  }
  
  /**
   * Inserts new main member code into the MercuryEapiOps table.
   * 
   * @param mainMemberCode
   * @param inUse
   * @param available
   * @param userName
   * @throws Exception
   */
  public void insertMercuryEapiOps(String mainMemberCode, String inUse, String available, String userName) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      logger.error("DB connection is null");
      return;
    }
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_MERCURY_EAPI_OPS_ADMIN");
    Map paramMap = new HashMap();
    paramMap.put("IN_MAIN_MEMBER_CODE",mainMemberCode);
    paramMap.put("IN_IN_USE",inUse);
    paramMap.put("IN_IS_AVAILABLE",available);
    paramMap.put("IN_CREATED_BY",userName);
    paramMap.put("IN_UPDATED_BY",userName);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }

  }

}
