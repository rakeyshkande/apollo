package com.ftd.osp.opsadministration.dao;

import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.ContentFilterVO;
import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentDAO
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    public ContentDAO()
    {
    }
    
    public Map<Long,ContentFilterVO> getContentFilterMap(Connection conn) throws Exception 
    {
        ContentFilterVO vo = new ContentFilterVO();
        Map<Long,ContentFilterVO> contentMasterMap = new HashMap<Long,ContentFilterVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTENT_FILTER");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentFilterVO();

            vo.setContentFilterId(results.getBigDecimal("CONTENT_FILTER_ID").longValue());
            vo.setContentFilterName(results.getString("CONTENT_FILTER_NAME"));

            contentMasterMap.put(vo.getContentFilterId(),vo);
        }
        logger.debug("Got " + contentMasterMap.size() + " values for GET_CONTENT_FILTER");

        return contentMasterMap;
    }

    public Map<String,ContentMasterVO> getContentContextMap(Connection conn) throws Exception {
        ContentMasterVO vo = new ContentMasterVO();
        Map<String, ContentMasterVO> contextMap = new HashMap<String, ContentMasterVO>();
        
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTEXT_LIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentMasterVO();
            vo.setContext(results.getString("CONTENT_CONTEXT"));
            contextMap.put(vo.getContext(),vo);
        }
        logger.debug("Got " + contextMap.size() + " values for GET_CONTEXT_LIST");
        
        return contextMap;
    }

    public List<ContentMasterVO> getContentMasterList(Connection conn, String contextFilter) throws Exception 
    {
        ContentMasterVO vo = new ContentMasterVO();
        List<ContentMasterVO> contentMasterList = new ArrayList<ContentMasterVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_CONTEXT_FILTER", contextFilter);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTENT_MASTER");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentMasterVO();

            vo.setContentMasterId(results.getBigDecimal("CONTENT_MASTER_ID").longValue());
            vo.setContext(results.getString("CONTENT_CONTEXT"));
            vo.setName(results.getString("CONTENT_NAME"));
            vo.setDescription(results.getString("CONTENT_DESCRIPTION"));
            BigDecimal tempBigD = results.getBigDecimal("CONTENT_FILTER_ID1");
            if (tempBigD != null)
            {
                vo.setFilter1Id(tempBigD.longValue());
            }
            tempBigD = results.getBigDecimal("CONTENT_FILTER_ID2");
            if (tempBigD != null)
            {
                vo.setFilter2Id(tempBigD.longValue());
            }

            contentMasterList.add(vo);
        }
        logger.debug("Got " + contentMasterList.size() + " values for GET_CONTENT_MASTER");

        return contentMasterList;
    }

    public List<ContentDetailVO> getContentDetailList(Connection conn, String contextFilter) throws Exception 
    {
        ContentDetailVO vo = new ContentDetailVO();
        List<ContentDetailVO> contentDetailList = new ArrayList<ContentDetailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_CONTEXT_FILTER", contextFilter);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTENT_DETAIL");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentDetailVO();

            vo.setContentDetailID(results.getBigDecimal("CONTENT_DETAIL_ID").longValue());
            vo.setContentMasterID(results.getBigDecimal("CONTENT_MASTER_ID").longValue());
            vo.setFilter1Value(results.getString("FILTER_1_VALUE"));
            vo.setFilter2Value(results.getString("FILTER_2_VALUE"));
            vo.setText(results.getString("CONTENT_TXT"));
            vo.setContext(results.getString("CONTENT_CONTEXT"));
            vo.setName(results.getString("CONTENT_NAME"));
            BigDecimal tempBigD = results.getBigDecimal("CONTENT_FILTER_ID1");
            if (tempBigD != null)
            {
                vo.setFilter1Id(tempBigD.longValue());
            }
            tempBigD = results.getBigDecimal("CONTENT_FILTER_ID2");
            if (tempBigD != null)
            {
                vo.setFilter2Id(tempBigD.longValue());
            }

            contentDetailList.add(vo);
        }
        logger.debug("Got " + contentDetailList.size() + " values for GET_CONTENT_DETAIL");

        return contentDetailList;
    }

    public ContentDetailVO getContentDetailById(Connection conn, String contentDetailId) throws Exception 
    {
        logger.debug("getContentDetailByID: " + contentDetailId);
        ContentDetailVO vo = new ContentDetailVO();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_CONTENT_DETAIL_ID",  new BigDecimal(contentDetailId));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTENT_DETAIL_BY_ID");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentDetailVO();

            vo.setContentDetailID(results.getBigDecimal("CONTENT_DETAIL_ID").longValue());
            vo.setContentMasterID(results.getBigDecimal("CONTENT_MASTER_ID").longValue());
            vo.setFilter1Value(results.getString("FILTER_1_VALUE"));
            vo.setFilter2Value(results.getString("FILTER_2_VALUE"));
            vo.setText(results.getString("CONTENT_TXT"));
            vo.setContext(results.getString("CONTENT_CONTEXT"));
            vo.setName(results.getString("CONTENT_NAME"));
            BigDecimal tempBigD = results.getBigDecimal("CONTENT_FILTER_ID1");
            if (tempBigD != null)
            {
                vo.setFilter1Id(tempBigD.longValue());
            }
            tempBigD = results.getBigDecimal("CONTENT_FILTER_ID2");
            if (tempBigD != null)
            {
                vo.setFilter2Id(tempBigD.longValue());
            }
            vo.setFilter1Name(results.getString("FILTER1_NAME"));
            vo.setFilter2Name(results.getString("FILTER2_NAME"));
            logger.debug("filter1: " + vo.getFilter1Name() + " " + vo.getFilter1Value());
            logger.debug("filter2: " + vo.getFilter2Name() + " " + vo.getFilter2Value());
            logger.debug("value: " + vo.getText());

        }

        return vo;
    }

    public ContentDetailVO getContentMasterById(Connection conn, String contentMasterId) throws Exception 
    {
        logger.debug("getContentMasterByID: " + contentMasterId);
        ContentDetailVO vo = new ContentDetailVO();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_CONTENT_MASTER_ID",  new BigDecimal(contentMasterId));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CONTENT_MASTER_BY_ID");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() ) 
        {
            vo = new ContentDetailVO();

            vo.setContentDetailID(0);
            vo.setContentMasterID(results.getBigDecimal("CONTENT_MASTER_ID").longValue());
            //vo.setFilter1Value(results.getString("FILTER_1_VALUE"));
            //vo.setFilter2Value(results.getString("FILTER_2_VALUE"));
            //vo.setText(results.getString("CONTENT_TXT"));
            vo.setContext(results.getString("CONTENT_CONTEXT"));
            vo.setName(results.getString("CONTENT_NAME"));
            BigDecimal tempBigD = results.getBigDecimal("CONTENT_FILTER_ID1");
            if (tempBigD != null)
            {
                vo.setFilter1Id(tempBigD.longValue());
            }
            tempBigD = results.getBigDecimal("CONTENT_FILTER_ID2");
            if (tempBigD != null)
            {
                vo.setFilter2Id(tempBigD.longValue());
            }
            vo.setFilter1Name(results.getString("FILTER1_NAME"));
            vo.setFilter2Name(results.getString("FILTER2_NAME"));
            logger.debug("filter1: " + vo.getFilter1Name() + " " + vo.getFilter1Value());
            logger.debug("filter2: " + vo.getFilter2Name() + " " + vo.getFilter2Value());

        }

        return vo;
    }

    public String saveContentMaster(Connection con, ContentMasterVO vo) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INSERT_CONTENT_MASTER");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTENT_MASTER_ID", new BigDecimal(vo.getContentMasterId()));
        inputParams.put("IN_CONTENT_CONTEXT", vo.getContext());
        inputParams.put("IN_CONTENT_NAME", vo.getName());
        inputParams.put("IN_CONTENT_DESCRIPTION", vo.getDescription());
        if (vo.getFilter1Id() == 0)
        {
            inputParams.put("IN_CONTENT_FILTER_ID1", null);
        }
        else
        {
            inputParams.put("IN_CONTENT_FILTER_ID1", new BigDecimal(vo.getFilter1Id()));
        }
        if (vo.getFilter2Id() == 0)
        {
            inputParams.put("IN_CONTENT_FILTER_ID2", null);
        }
        else
        {
            inputParams.put("IN_CONTENT_FILTER_ID2", new BigDecimal(vo.getFilter2Id()));
        }
        inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
            logger.error(message);
        } else {
            // If the key was zero, then we need the new key from the sequence
            if (vo.getContentMasterId() == 0)
            {
                String idString = (String) outputs.get("OUT_CONTENT_MASTER_ID");
                logger.debug("out_cmi = " + idString);
                vo.setContentMasterId(Long.parseLong(idString));
            }
        }
        
        return message;
    }
    
    public String saveContentDetail(Connection con, ContentDetailVO vo) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        dataRequest.setStatementID("INSERT_CONTENT_DETAIL");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_CONTENT_DETAIL_ID", new BigDecimal(vo.getContentDetailID()));
        inputParams.put("IN_CONTENT_MASTER_ID", new BigDecimal(vo.getContentMasterID()));
        inputParams.put("IN_FILTER_1_VALUE", vo.getFilter1Value());
        inputParams.put("IN_FILTER_2_VALUE", vo.getFilter2Value());
        inputParams.put("IN_CONTENT_TXT", vo.getText());
        inputParams.put("IN_UPDATED_BY", vo.getUpdatedBy());
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map)dataAccessUtil.execute(dataRequest);

        String status = (String)outputs.get("OUT_STATUS");
        String message = "";
        if (status != null && status.equalsIgnoreCase("N"))
        {
            message = (String)outputs.get("OUT_MESSAGE");
        } else {
            String detailId = (String)outputs.get("OUT_CONTENT_DETAIL_ID");
            if (detailId != null) {
                logger.debug("new detail id: " + detailId);
                message = "newid=" + detailId;
            }
        }
        logger.debug("status: " + status + " " + message);
        
        return message;        
    }
}
