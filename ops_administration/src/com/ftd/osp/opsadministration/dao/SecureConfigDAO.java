package com.ftd.osp.opsadministration.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

public class SecureConfigDAO 
{
  private static Logger logger = new Logger("com.ftd.osp.opsadministration.dao.SecureConfigDAO");
  private Connection conn = null;

  // Stored procedure names
  private static final String GET_SECURE_CONTEXTS = "FRP.GET_SECURE_CONTEXTS";
  private static final String GET_SECURE_CONTEXT = "FRP.GET_SECURE_CONTEXT";
  private static final String UPDATE_SECURE_PROPERTY = "FRP.UPDATE_SECURE_PROPERTY";
  private static final String DELETE_SECURE_PROPERTY = "FRP.DELETE_SECURE_PROPERTY";
  
  // Custom parameters returned from database procedure
  private static final String STATUS_PARAM = "OUT_STATUS";
  private static final String MESSAGE_PARAM = "OUT_MESSAGE";

  /**
  * Constructor which accepts a connection
  * @param conn - Database connection
  */
  public SecureConfigDAO(Connection conn)
  {
    this.conn = conn;
  }

  /**
  * Loads all contexts
  * @return Document - Context data
  */
	public Document loadContexts()
    throws SAXException, ParserConfigurationException, IOException, SQLException
	{
    DataRequest dataRequest = null;
    Document contexts = null;
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                                            
    dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(GET_SECURE_CONTEXTS);
    
    contexts = (Document) dataAccessUtil.execute(dataRequest);
    
    return contexts;
  }

  /**
  * Loads all properties for a given context.  If context is null all
  * properties will be loaded.
  * @param context - Context to retrieve.  If null obtain all context
  * properties.
  * @return Document - Context property data
  */
  public Document loadProperties(String context)
    throws IOException, ParserConfigurationException, SAXException, SQLException
  {
    DataRequest dataRequest = null;
    Document properties = null;
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                        
    dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(GET_SECURE_CONTEXT);
    dataRequest.addInputParam("IN_CONTEXT", context);
    
    properties = (Document) dataAccessUtil.execute(dataRequest);
    
    return properties;
  }
	
  /**
  * Add/Update secure property
  * @param context - context of configuration property
  * @param name - name of configuration property
  * @param value - value of configuration property
  * @param description - description of configuration property
  */
  public void updateProperty(String context, String name, String value, 
    String description, String userId)
    throws IOException, ParserConfigurationException, SAXException, SQLException,
      Exception
  {
    DataRequest dataRequest = null;
    Document properties = null;
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                                                    
    dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(UPDATE_SECURE_PROPERTY);
    dataRequest.addInputParam("IN_CONTEXT", context);
    dataRequest.addInputParam("IN_NAME", name);
    dataRequest.addInputParam("IN_VALUE", value);
    dataRequest.addInputParam("IN_DESCRIPTION", description);
    dataRequest.addInputParam("IN_UPDATED_BY", userId);
    
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }
  }

  /**
  * Delete secure property
  * @param context - context of configuration property
  * @param name - name of configuration property
  */
  public void deleteProperty(String context, String name, String userId)
    throws IOException, ParserConfigurationException, SAXException, SQLException,
    Exception
  {
    DataRequest dataRequest = null;
    Document properties = null;
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                                                    
    dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(DELETE_SECURE_PROPERTY);
    dataRequest.addInputParam("IN_CONTEXT", context);
    dataRequest.addInputParam("IN_NAME", name);
    dataRequest.addInputParam("IN_UPDATED_BY", userId);
    
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
    String status = (String) outputs.get(STATUS_PARAM);
    if(status.equals("N"))
    {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }
  }
}
