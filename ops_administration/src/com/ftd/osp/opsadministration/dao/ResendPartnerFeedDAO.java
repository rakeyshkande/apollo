package com.ftd.osp.opsadministration.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * @author rksingh
 * 
 */

public class ResendPartnerFeedDAO {
	private static final String STATUS_PARAM = "RegisterOutParameterStatus";
	private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
	private static final String STATUS_PARAMTER = "RegisterOutStatus";
	private static final String OUT_MESSAGE = "RegisterOutMessage";
	private static final String OUT_INVALID_ORD_NUM_CURSOR = "RegisterOutCursor";

	private Connection conn;
	private Logger logger = new Logger(this.getClass().getName());

	public ResendPartnerFeedDAO(Connection conn) {
		this.conn = conn;
	}

	
	// Method for updating the status when date is passed from UI
	@SuppressWarnings("unchecked")
	public Map<String, Object> updatePartnerFeedStausByDate(String partnerFeedType, String startDate, String endDate ,String partnerId , String updatedBy) throws Exception {
		
		logger.debug("updatePartnerFeedStausByDate()");
		
		if (this.conn == null) {
			logger.error("DB connection is null");
			return null;
		}
		
		DataRequest dataRequest = null;
		Map<String, Object> outputs = null;
		
		try {	
			dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("UPDATE_FEED_STATUS_BY_DATE");
		    Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("IN_FEED_TYPE", partnerFeedType);
			paramMap.put("IN_START_DATE", startDate);
			paramMap.put("IN_END_DATE", endDate);
			paramMap.put("IN_PARTNER_ID", partnerId);
			paramMap.put("IN_UPDATED_BY", updatedBy);

			dataRequest.setInputParams(paramMap);
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		    outputs = (Map<String, Object>)dataAccessUtil.execute(dataRequest);
		    
		    
			if(outputs != null) {				
				if("N".equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					logger.error("An error caught update the partner order numbers, " + outputs.get(MESSAGE_PARAM));
				}				
			}

		    return outputs;
		}catch(Exception e) {
		logger.error("Error caught in updatePartnerFeedStausByDate, ", e);
			return null;
		
	}
		
		

	}
	
	
	// Method for updating the status when  partner order numbers is passed from UI

	@SuppressWarnings("unchecked")
	public Map<String, Object> updatePartnerFeedStausByNumber(String tempPartnerOrderNumber, String partnerFeedType, String partnerId, String updatedBy) throws Exception {
		
		logger.debug("updatePartnerFeedStausByNumber()");
		if (this.conn == null) {
			logger.error("DB connection is null");
			return null;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID("UPDATE_PRTNR_ORD_FEED_STATUS");
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("IN_PARTNER_ORDER_NUMBERS", tempPartnerOrderNumber);
			paramMap.put("IN_FEED_TYPE", partnerFeedType);
			paramMap.put("IN_PARTNER_ID", partnerId);
			paramMap.put("IN_UPDATED_BY", updatedBy);
			dataRequestObject.setInputParams(paramMap);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if("N".equalsIgnoreCase((String) outputs.get(STATUS_PARAMTER))) {
					logger.error("An error caught update the partner order numbers, " + outputs.get(OUT_MESSAGE));
					// When error only return error message in map, the invalid order number cursor should be null
					outputs.put(OUT_INVALID_ORD_NUM_CURSOR, null);
				}				
			}

		

		return outputs;

	} catch(Exception e) {
		logger.error("Error caught in updatePartnerFeedStausByNumber, ", e);
			return null;
		
	}
	}
}