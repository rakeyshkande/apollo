package com.ftd.osp.opsadministration.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * @author rksingh
 * 
 */

public class GrouponRefundDAO {
	private static final String STATUS_PARAM = "RegisterOutParameterStatus";
	private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
	private static final String MESSAGE_PARAM_1 = "RegisterOutParameterMessageBn";

	private Connection conn;
	private Logger logger = new Logger(this.getClass().getName());

	public GrouponRefundDAO(Connection conn) {
		this.conn = conn;
	}

	/**
	 * This Method is for Updating the Groupon coupon by coupon number provided in the Gift and Coupons Refund Screen.
	 * @param gCcouponList - list of grouponCoupons
	 * @param intialStatus - 
	 * @param finalStatus
	 * @param amount
	 * @param expiryDate
	 * @param updatedBy
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> updateGcStausByGcNumber(String gcNumber, String intialStatus, String finalStatus ,String amount, String expiryDate , String updatedBy) 
			throws Exception {
		
		logger.debug("updateGcStausByGcNumber()");
		
		if (this.conn == null) {
			logger.error("DB connection is null");
			return null;
		}
		
		DataRequest dataRequest = null;
		Map<String, Object> outputs = null;
		
		try {	
			dataRequest = new DataRequest();
			dataRequest.setConnection(conn);
			dataRequest.setStatementID("UPDATE_GC_STATUS_BY_GCNUMBER");
			
		    Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("IN_GC_NUM", gcNumber);
			paramMap.put("IN_INTIAL_STATUS", intialStatus);
			paramMap.put("IN_FINAL_STATUS", finalStatus);
			paramMap.put("IN_AMOUNT", amount);
			paramMap.put("IN_EXPIRY_DATE", expiryDate);
			paramMap.put("IN_UPDATED_BY", updatedBy);

			dataRequest.setInputParams(paramMap);
			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		    outputs = (Map<String, Object>)dataAccessUtil.execute(dataRequest);
		    
		    
			if(outputs != null) {				
				if("N".equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					logger.error("An error caught update the Groupon Coupons number, " + outputs.get(MESSAGE_PARAM));
				}				
			}

		    return outputs;
		}catch(Exception e) {
		logger.error("Error caught in updateGcStausByGcNumber, ", e);
			return null;
		
	}
}
	
	
	/**
	 * This Method is for Updating the Groupon coupon by batch/request number provided in the Gift and Coupons Refund Screen.
	 * @param gCcouponList - list of batch number
	 * @param intialStatus - 
	 * @param finalStatus
	 * @param amount
	 * @param expiryDate
	 * @param updatedBy
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> updateGcStausByBatchNumber(String batchNumber, String intialStatus, String finalStatus ,String amount, String expiryDate , String updatedBy) 
			throws Exception {
		
		logger.debug("updateGcStausByBatchNumber()");
		if (this.conn == null) {
			logger.error("DB connection is null");
			return null;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID("UPDATE_GC_STATUS_BY_BATCHNUMBER");
			Map<String, String> paramMap = new HashMap<String, String> ();
			paramMap.put("IN_BATCH_NUM", batchNumber);
			paramMap.put("IN_INTIAL_STATUS", intialStatus);
			paramMap.put("IN_FINAL_STATUS", finalStatus);
			paramMap.put("IN_AMOUNT", amount);
			paramMap.put("IN_EXPIRY_DATE", expiryDate);
			paramMap.put("IN_UPDATED_BY", updatedBy);
			
			dataRequestObject.setInputParams(paramMap);

			
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if("N".equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					
					logger.error("An error caught update the Groupon number, " + outputs.get(MESSAGE_PARAM));
					logger.error("An error caught update the Groupon Batch number, " + outputs.get(MESSAGE_PARAM_1));

				}				
			}
	

		return outputs;

	} catch(Exception e) {
		logger.error("Error caught in updateGcStausByBatchNumber, ", e);
			return null;
		
	}
	}
}