package com.ftd.osp.opsadministration.vo;

public class ContentDetailVO
{
    private long contentDetailID;
    private long contentMasterID;
    private String filter1Value;
    private String filter2Value;
    private String text;
    private String updatedBy;
    private String context;
    private String name;
    private long filter1Id;
    private long filter2Id;
    private String filter1Name;
    private String filter2Name;
    
    public ContentDetailVO()
    {
    }

    public void setContentDetailID(long contentDetailID)
    {
        this.contentDetailID = contentDetailID;
    }

    public long getContentDetailID()
    {
        return contentDetailID;
    }

    public void setContentMasterID(long contentMasterID)
    {
        this.contentMasterID = contentMasterID;
    }

    public long getContentMasterID()
    {
        return contentMasterID;
    }

    public void setFilter1Value(String filter1Value)
    {
        this.filter1Value = filter1Value;
    }

    public String getFilter1Value()
    {
        return filter1Value;
    }

    public void setFilter2Value(String filter2Value)
    {
        this.filter2Value = filter2Value;
    }

    public String getFilter2Value()
    {
        return filter2Value;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return text;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFilter1Id(long filter1Id) {
        this.filter1Id = filter1Id;
    }

    public long getFilter1Id() {
        return filter1Id;
    }

    public void setFilter2Id(long filter2Id) {
        this.filter2Id = filter2Id;
    }

    public long getFilter2Id() {
        return filter2Id;
    }

    public void setFilter1Name(String filter1Name) {
        this.filter1Name = filter1Name;
    }

    public String getFilter1Name() {
        return filter1Name;
    }

    public void setFilter2Name(String filter2Name) {
        this.filter2Name = filter2Name;
    }

    public String getFilter2Name() {
        return filter2Name;
    }
}
