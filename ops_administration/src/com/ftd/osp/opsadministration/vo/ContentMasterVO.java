package com.ftd.osp.opsadministration.vo;

public class ContentMasterVO
{
    private long contentMasterId;
    private String context;
    private String name;
    private String description;
    private long filter1Id;
    private long filter2Id;
    private String updatedBy;
    
    public ContentMasterVO()
    {
    }

    public void setContentMasterId(long contentMasterId)
    {
        this.contentMasterId = contentMasterId;
    }

    public long getContentMasterId()
    {
        return contentMasterId;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setFilter1Id(long filter1Id)
    {
        this.filter1Id = filter1Id;
    }

    public long getFilter1Id()
    {
        return filter1Id;
    }

    public void setFilter2Id(long filter2Id)
    {
        this.filter2Id = filter2Id;
    }

    public long getFilter2Id()
    {
        return filter2Id;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
}
