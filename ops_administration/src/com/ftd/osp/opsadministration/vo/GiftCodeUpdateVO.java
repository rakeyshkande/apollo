package com.ftd.osp.opsadministration.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class GiftCodeUpdateVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8205068699173436166L;
	private List<String> batchNumber;
	private List<String> giftCodeId;
	private List<String> currentGCStatus;
	private String newStatus;
	private String issueAmount;
	private Date expirationDate;
	private String updatedBy;
	private Boolean isBatch;
	
	public GiftCodeUpdateVO(){
		super();
	}

	public GiftCodeUpdateVO(List<String> batchNumber, List<String> giftCodeId,
			List<String> currentGCStatus, String newStatus, String issueAmount,
			Date expirationDate, String updatedBy, Boolean isBatch) {
		super();
		this.batchNumber = batchNumber;
		this.giftCodeId = giftCodeId;
		this.currentGCStatus = currentGCStatus;
		this.newStatus = newStatus;
		this.issueAmount = issueAmount;
		this.expirationDate = expirationDate;
		this.updatedBy = updatedBy;
		this.isBatch = isBatch;
	}

	public Boolean getIsBatch() {
		return isBatch;
	}

	public void setIsBatch(Boolean isBatch) {
		this.isBatch = isBatch;
	}

	public List<String> getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(List<String> batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	public List<String> getGiftCodeId() {
		return giftCodeId;
	}

	public void setGiftCodeId(List<String> giftCodeId) {
		this.giftCodeId = giftCodeId;
	}

	public List<String> getCurrentGCStatus() {
		return currentGCStatus;
	}

	public void setCurrentGCStatus(List<String> currentGCStatus) {
		this.currentGCStatus = currentGCStatus;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getIssueAmount() {
		return issueAmount;
	}

	public void setIssueAmount(String issueAmount) {
		this.issueAmount = issueAmount;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "GiftCodeUpdateVO [batchNumber=" + batchNumber + ", giftCodeId="
				+ giftCodeId + ", currentGCStatus=" + currentGCStatus
				+ ", newStatus=" + newStatus + ", issueAmount=" + issueAmount
				+ ", expirationDate=" + expirationDate + ", updatedBy="
				+ updatedBy + ", isBatch=" + isBatch + "]";
	}

}
