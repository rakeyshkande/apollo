package com.ftd.osp.opsadministration.vo;

public class ContentFilterVO
{
    private long   contentFilterId;
    private String contentFilterName;
    
    public ContentFilterVO()
    {
    }

    public void setContentFilterId(long contentFilterId)
    {
        this.contentFilterId = contentFilterId;
    }

    public long getContentFilterId()
    {
        return contentFilterId;
    }

    public void setContentFilterName(String contentFilterName)
    {
        this.contentFilterName = contentFilterName;
    }

    public String getContentFilterName()
    {
        return contentFilterName;
    }
}
