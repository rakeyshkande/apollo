package com.ftd.osp.opsadministration.vo;

public class PageFilterVO
{
    private long filterId;
    private String filterName;
    private String activeFlag;
    private String messageSource;
    private String messageSubject;
    private String messageBody;
    private String actionType;
    private String nopageStartHour;
    private String nopageEndHour;
    private String subjectPrefix;
    private String overwriteProject;
    private String updatedBy;
    
    public PageFilterVO()
    {
    }

	public long getFilterId() {
		return filterId;
	}

	public void setFilterId(long filterId) {
		this.filterId = filterId;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessageSubject() {
		return messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getNopageStartHour() {
		return nopageStartHour;
	}

	public void setNopageStartHour(String nopageStartHour) {
		this.nopageStartHour = nopageStartHour;
	}

	public String getNopageEndHour() {
		return nopageEndHour;
	}

	public void setNopageEndHour(String nopageEndHour) {
		this.nopageEndHour = nopageEndHour;
	}

	public String getSubjectPrefix() {
		return subjectPrefix;
	}

	public void setSubjectPrefix(String subjectPrefix) {
		this.subjectPrefix = subjectPrefix;
	}

	public String getOverwriteProject() {
		return overwriteProject;
	}

	public void setOverwriteProject(String overwriteProject) {
		this.overwriteProject = overwriteProject;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


}
