package com.ftd.osp.opsadministration.vo;

public class PageFilterActionVO
{
    private String actionType;
    private String actionDesc;
    private String priority;

    
    public PageFilterActionVO()
    {
    }


	public String getActionType() {
		return actionType;
	}


	public void setActionType(String actionType) {
		this.actionType = actionType;
	}


	public String getActiveDesc() {
		return actionDesc;
	}


	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}


	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		this.priority = priority;
	}


}
