package com.ftd.osp.opsadministration.utilities;

import com.ftd.osp.utilities.xml.DOMUtil;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This is a test class that will generate the reinstateOrderList node with test data.
 * Since there are currently issues with Oracle and CLOBS, test data will be 
 * generated using this utility class.
 * @author Rose Lazuk
 */
 
public class GenerateTestXML 
{
  public GenerateTestXML()
  {
  }
  
    /**
     * Build the test XML.  
     * 
     * @return Document
     * @exception ParserConfigurationException
     * @exception TransformerConfigurationException
     * @exception TransformerException
     * 
     */
    public Document generateXML() throws ParserConfigurationException,
    TransformerConfigurationException, TransformerException
    {
        
        String xml =  " <xml><order><header></header><items><item></item></items></order></xml> ";
        Element tagElement = null;            
        Document orderDocument = DOMUtil.getDefaultDocument();
              
        // Create reinstate order list root
        Element reinstateOrderListElement = orderDocument.createElement("reinstateOrderList");
        orderDocument.appendChild(reinstateOrderListElement);

        Document reinstateOrderDocument = DOMUtil.getDefaultDocument();
        Element reinstateOrderElement = reinstateOrderDocument.createElement("reinstateOrder");
        
        reinstateOrderElement.appendChild(this.createTextNode("az_error_id", "050-1234567-1234567", reinstateOrderDocument, tagElement));
        reinstateOrderElement.appendChild(this.createTextNode("context", "AMAZON", reinstateOrderDocument, tagElement));
        reinstateOrderElement.appendChild(this.createTextNode("error", "Order count does not equal total number of items received in XML file.", reinstateOrderDocument, tagElement));
        reinstateOrderElement.appendChild(this.createTextNode("timestamp", "2004-08-16 13:46:05.0", reinstateOrderDocument, tagElement));
        reinstateOrderElement.appendChild(this.createCDATANode("xml", xml, reinstateOrderDocument, tagElement)); 
        reinstateOrderElement.appendChild(this.createTextNode("event_name", "ORDER INFUSOR", reinstateOrderDocument, tagElement));
        
        reinstateOrderListElement.appendChild(orderDocument.importNode(reinstateOrderElement, true));                  
        
        return orderDocument;
    }
    
    
    
    
    
    /**
     * Creates a Text node given the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }
    
    
    
     /**
     * Creates a CDATASection node whose value is the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createCDATANode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createCDATASection(elementValue));
        }

        return tagElement;
    }
   
   public static void main(String[] args)
    {
        GenerateTestXML gtXML = new GenerateTestXML();
        
               
        try
        {
              
            Document doc = (Document) gtXML.generateXML();
            DOMUtil.print(doc, System.out);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
