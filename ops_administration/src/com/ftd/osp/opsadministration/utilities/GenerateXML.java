package com.ftd.osp.opsadministration.utilities;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;

import java.sql.Clob;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

/**
 * This utility class will generate the reinstateOrderList node with data
 * returned from the VIEW_AZ_ERROR stored procedure.
 *
 * @author Rose Lazuk
 */
 
public class GenerateXML 
{
  public GenerateXML()
  {
  }
  
    /**
     * Build the XML Document containing the information returned from the AZ_ERROR table.  
     * 
     * @param rs CachedResultSet
     * @return Document
     * @exception ParserConfigurationException
     * @exception TransformerConfigurationException
     * @exception TransformerException
     * 
     */
    public Document generateReinstateAmazonOrderXML(CachedResultSet rs) throws ParserConfigurationException,
    TransformerConfigurationException, TransformerException, Exception
    {
        Element tagElement = null;            
        Document orderDocument = DOMUtil.getDefaultDocument();
    
        // Create reinstate order list root
        Element reinstateOrderListElement = orderDocument.createElement("reinstateOrderList");
        orderDocument.appendChild(reinstateOrderListElement);
        
        Document reinstateOrderDocument = null;
        Element reinstateOrderElement = null;
        Clob clob = null;
        String xmlString = null;
        DataAccessUtil util = DataAccessUtil.getInstance();
        int counter = 0;
        while(rs != null && rs.next())
        {
            reinstateOrderDocument = DOMUtil.getDefaultDocument();
            reinstateOrderElement = reinstateOrderDocument.createElement("reinstateOrder");
            
            reinstateOrderElement.appendChild(this.createTextNode("az_error_id",  String.valueOf(rs.getObject(1)), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("context",  (String)rs.getObject(2), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("error",  (String)rs.getObject(3), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("timestamp",  rs.getObject(4).toString(), reinstateOrderDocument, tagElement));
            clob = (Clob) rs.getObject(5);
            xmlString =  util.toXMLString(clob);
                            
            //in order to format the xml to make it look pretty when being
            //displayed on the fixOrder.xsl page, need to create a new
            //document by passing over the xml string.  Then we need to 
            //convert the document back to a string before adding it to the
            //reinstateOrderElement.
            
            InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
            Reader reader = new InputStreamReader(is,"UTF-8");
             
            InputSource isource = new InputSource(reader);
            
            Document document = null;
            document = JAXPUtil.parseDocument(is); 
            
            StringWriter s = new StringWriter();
            DOMUtil.print(document, new PrintWriter(s));
            xmlString = s.toString();
            
            reinstateOrderElement.appendChild(this.createCDATANode("xml", xmlString, reinstateOrderDocument, tagElement)); 
            reinstateOrderElement.appendChild(this.createTextNode("event_name",  (String)rs.getObject(6), reinstateOrderDocument, tagElement));
            
            reinstateOrderListElement.appendChild(orderDocument.importNode(reinstateOrderElement, true));                  
      //orderDocument.print(System.out);
     
        }
        
        return orderDocument;
    }    
    
    /**
     * Creates a Text node given the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }
    
    
    
     /**
     * Creates a CDATASection node whose value is the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createCDATANode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createCDATASection(elementValue));
        }

        return tagElement;
    }
}
