package com.ftd.osp.opsadministration.utilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import org.xml.sax.SAXException;

import java.io.IOException;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;


/**
 * Collection of methods used by the Servlets.
 */
public class ServletHelper {
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  private static Logger logger = new Logger("com.ftd.osp.opsadministration.utilities.ServletHelper");

  /**
   * Indicates whether or not the security token and security context
  are valid.
   *
   * @param request the http request object
   * @exception ExpiredIdentityException
   * @exception ExpiredSessionException
   * @exception InvalidSessionException
   * @exception SAXException
   * @exception ParserConfigurationException
   * @exception IOException
   * @exception SQLException
   * @exception Exception
   * @return whether or not the security token and the security
  context are valid
   */
  public static boolean isValidToken(HttpServletRequest request)
    throws ExpiredIdentityException, ExpiredSessionException, InvalidSessionException, SAXException, ParserConfigurationException, 
      IOException, SQLException, Exception {
    //get data from configuration file
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String unit = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "UNIT");

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);

    // change to use a constant here
    if ((securityContext == null) || (securityContext.equals(""))) {
      logger.debug("No Security Context found");

      return false;
    }

    logger.debug("securityContext: " + securityContext);

    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

    if ((securityToken == null) || (securityToken.equals(""))) {
      logger.debug("No Security Token found");

      return false;
    }

    logger.debug("securityToken: " + securityToken);

    logger.debug("authenticating security token");

    SecurityManager securityManager = SecurityManager.getInstance();

    try
    {
      return securityManager.authenticateSecurityToken(securityContext, unit, securityToken);
    }
    catch (ExpiredIdentityException e)
    {
      return false;
    }
    catch (ExpiredSessionException e)
    {
      return false;
    }
    catch (InvalidSessionException e)
    {
      return false;
    }
  }

  /**
   * Returns CSR id
   *
   * @param request the http request object
   * @exception ExpiredIdentityException
   * @exception ExpiredSessionException
   * @exception InvalidSessionException
   * @exception SAXException
   * @exception ParserConfigurationException
   * @exception IOException
   * @exception SQLException
   * @exception Exception
   * @return csrId
   */
  public static String getUserId(HttpServletRequest request)
    throws ExpiredIdentityException, ExpiredSessionException, InvalidSessionException, SAXException, ParserConfigurationException, 
      IOException, SQLException, Exception {
    //get data from configuration file
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String unit = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "UNIT");

    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

    if ((securityToken == null) || (securityToken.equals(""))) {
      logger.debug("No Security Token found");
    }

    SecurityManager securityManager = SecurityManager.getInstance();

    return securityManager.getUserInfo(securityToken).getUserID();
  }
  
  public static String getSiteNameSsl() throws Exception
  {
    return com.ftd.security.util.ServletHelper.switchServerPort(
     getSiteName(),
     ConfigurationUtil.getInstance().getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, "sitesslport"));
  }
  
  public static String getSiteName() throws Exception {          
        return ConfigurationUtil.getInstance().getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, "sitename");           
   }
   
   public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response) 
   {
     com.ftd.security.util.ServletHelper.redirectToLogin(request, response, request.getParameter(HTML_PARAM_SECURITY_TOKEN));
   }
}
