package com.ftd.osp.opsadministration.utilities;


import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Clob;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class GenerateWalMartXML 
{
  //table id's
  private static final String ORDER_GATHERER_MESSAGES = "1";
  private static final String INBOUND_TRANSMISSIONS = "2";
  private static final String OUTBOUND_PARTNER_MESSAGES = "3";
  //used for Order Gatherer Messages only
  private static final String ORDER_TYPE_ORDER = "ORDER";
  private static final String ORDER_TYPE_CANCEL = "CANCEL";
  //column values
  private int P_KEY = 1;
  private int TRANS_ID = 0;
  private int PARTNER_ID  = 0;
  private int ERROR_DESC  = 0;
  private int TIME = 0;
  private int XML_CLOB = 0;
  
  private Logger logger;
  /**
   * Create this object with a tableId
   * allowing the appropriate fields to be 
   * captured from the result set.
   * @param tableid
   */
  public GenerateWalMartXML()
  {
    logger = new Logger("com.ftd.osp.opsadministration.utilities.GenerateWalMartXML");
  }

  
  /**
   * Build XML document from the result set passed in.
   * @param rs The records pulled from the table
   * @param tableid An id specific to the table from which we are pulling errors
   * @return Document
   * @throws javax.xml.parsers.ParserConfigurationException
   */
  public Document generateWalMartXML(CachedResultSet rs, String tableid)throws ParserConfigurationException,
         TransformerConfigurationException, TransformerException, Exception
  {
         //Initialize the appropriate variables 
         //used in retrieving columns from the result set
      init(tableid);

      Element tagElement = null;            
      Document orderDocument = DOMUtil.getDefaultDocument();
    
      Element walmartOrderListElement = orderDocument.createElement("walmartOrderList");
      orderDocument.appendChild(walmartOrderListElement);
      
    
        Document reinstateOrderDocument = null;
        Element reinstateOrderElement = null;
        Clob clob = null;
        String xmlString = null;
        DataAccessUtil util = DataAccessUtil.getInstance();
        int counter = 0;
        while(rs != null && rs.next() )
        {
            reinstateOrderDocument = DOMUtil.getDefaultDocument();
            reinstateOrderElement = reinstateOrderDocument.createElement("reinstateOrder");
            
            reinstateOrderElement.appendChild(this.createTextNode("transmission_id",  String.valueOf(rs.getObject(TRANS_ID)), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("partner_id",  (String)rs.getObject(PARTNER_ID), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("error_desc",  (String)rs.getObject(ERROR_DESC), reinstateOrderDocument, tagElement));
            reinstateOrderElement.appendChild(this.createTextNode("time_stamp",  rs.getObject(TIME).toString(), reinstateOrderDocument, tagElement));
           //reinstateOrderElement.appendChild(this.createTextNode("tableid",  tableid.toString(), reinstateOrderDocument, tagElement));
            clob = (Clob) rs.getObject(XML_CLOB);
            xmlString =  util.toXMLString(clob);
                             
            //in order to format the xml to make it look like XML when being
            //displayed on the text area we need to create a new
            //document by passing over the xml string.  Then we need to 
            //convert the document back to a string before adding it to the
            //reinstateOrderElement.
            
            Document document = null;
          try
          {
               //this try block was put here because invalid 
               //XML does not parse to a document object
               //but will display.
        	  
            document = JAXPUtil.parseDocument(xmlString); 
            StringWriter s = new StringWriter();
            DOMUtil.print(document, new PrintWriter(s));
            xmlString = s.toString();
          }
          catch(Exception e)
          {
            logger.error("Un-parseable transaction Id: " +  String.valueOf(rs.getObject(TRANS_ID)) + " this is not an exception.");
            logger.error("GenerateWalMartXML: Unable to parse, CLOB contains malformed XML " + 
                  "\n" + "Continuing to process....CLOB will be displayed as a string.");
          
          }
            reinstateOrderElement.appendChild(this.createCDATANode("xml", xmlString, reinstateOrderDocument, tagElement)); 
            walmartOrderListElement.appendChild(orderDocument.importNode(reinstateOrderElement, true));                       
        }//end while
        return orderDocument;    
  }//end generateWalMartXML()
  
  
  
  
   /**
     * Creates a Text node given the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
    private Element createTextNode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createTextNode(elementValue));
        }

        return tagElement;
    }//end createTextNode
    
    
    
     /**
     * Creates a CDATASection node whose value is the specified string.
     * 
     * @param elementName String
     * @param elementValue String
     * @param document Document
     * @param tagElement Element 
     * @return Element
     * 
     */
     private Element createCDATANode(String elementName, String elementValue, Document document, Element tagElement)
    {
        tagElement = document.createElement(elementName);
        if(elementValue != null)
        {
            tagElement.appendChild(document.createCDATASection(elementValue));
        }

        return tagElement;
    }//end createCDATANode
    
    
  /**
   * Sets up the variables to 
   * a specific table in POP
   * @param id
   */
  private void init(String id)
  { 
    if( id.equals(ORDER_GATHERER_MESSAGES))
    {
      this.TRANS_ID = 1;
      this.PARTNER_ID = 2;
      this.ERROR_DESC = 9;
      this.TIME = 6;
      this.XML_CLOB = 4;
    }
    else if( id.equals(INBOUND_TRANSMISSIONS))
    {
      this.TRANS_ID = 1;
      this.PARTNER_ID = 2;
      this.ERROR_DESC = 8;
      this.TIME = 5;
      this.XML_CLOB = 6;
    }
    else if ( id.equals(OUTBOUND_PARTNER_MESSAGES))
    {
      this.TRANS_ID = 1;
      this.PARTNER_ID = 2;
      this.ERROR_DESC = 10;
      this.TIME = 7;
      this.XML_CLOB = 6;
    }
  }
    
    
    
}//end GenerateWalMartXML class
  
  
  
  
  
