package com.ftd.osp.opsadministration.utilities;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.codehaus.jackson.map.ObjectMapper;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.GiftCodeUpdateBulkResp;

public class GiftCodeUtil {
	private static final Logger logger = new Logger(
			GiftCodeUtil.class.getName());

	public static String convertToJSonString(Object req) {
		logger.debug("Req : " + req);
		String jsonString = null;
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			jsonString = mapperObj.writeValueAsString(req);
		} catch (IOException ex) {

		}
		logger.debug("Req as JSON: " + jsonString);
		return jsonString;
	}

	public static String getPath(String baseURL) {
		String path = null;
		URI uri = null;
		try {
			uri = new URI(baseURL);
			path = uri.getPath();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			logger.error("This is an invalid URL" + baseURL);
		}
		return path;
	}

	public static int getPort(String baseURL) {
		int port;
		URI uri = null;
		try {
			uri = new URI(baseURL);
			port = uri.getPort();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			port = 443;
		}
		if (port <= 0)
			port = 443;
		return port;

	}

	public static String getHost(String baseURL) {
		String host = null;
		URI uri = null;
		try {
			uri = new URI(baseURL);
			host = uri.getHost();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			logger.error("This is an invalid URL" + baseURL);
		}
		return host;
	}

	/**
	 * Converts Response String into Response Object 
	 * @param s
	 * @return GiftCodeUpdateBatchResponseVO
	 */
	public static GiftCodeUpdateBulkResp convertToObject(String s) {
		GiftCodeUpdateBulkResp obj = new GiftCodeUpdateBulkResp();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			obj = mapperObj.readValue(s, GiftCodeUpdateBulkResp.class);
		} catch (IOException ex) {
		}
		return obj;
	}
}
