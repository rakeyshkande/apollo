package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.utilities.dataaccess.*;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.j2ee.*;
import com.ftd.osp.utilities.xml.*;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.*;
import java.util.HashMap;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;

import org.w3c.dom.Document;

public class TestServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private static final String VIEW_SYSTEM_MESSAGES_SOURCES = "FRP.VIEW_SYSTEM_MESSAGES_SOURCES";


    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        
        super.init(config);
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadOpsAdmin(request, response);
    }

      /**
     * Retrieve the manager dashboard counts from the 
     * database.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadOpsAdmin(HttpServletRequest request, HttpServletResponse response)
    {
              
       Connection connection = null;
       DataRequest dataRequest = null;
                
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
                   
            
            
            

            File xslFile = new File(getServletContext().getRealPath("/xsl/opsAdmin.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                se.printStackTrace();
            }
        }
        
    }
}
