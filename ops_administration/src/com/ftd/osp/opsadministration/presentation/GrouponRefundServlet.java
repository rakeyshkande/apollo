package com.ftd.osp.opsadministration.presentation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.w3c.dom.Document;

import com.ftd.osp.opsadministration.bo.GrouponRefundBO;
import com.ftd.osp.opsadministration.dao.SecureConfigDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.exceptions.ExcelProcessingException;

/**
 * @author rksingh
 * 
 */

@SuppressWarnings("serial")
public class GrouponRefundServlet extends HttpServlet {

	private static Logger logger;
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";
	private Connection conn = null;

	// Data source name
	private static final String DATA_SOURCE = "ORDER SCRUB";
	// Security parameters
	private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
	private static final String HTML_PARAM_SECURITY_CONTEXT = "context";
	private static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
	private static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
	private static final String SECURITY_OFF = "SECURITY_OFF";
	private static final String TRUE = "true";
	private static final String BAD_FILE = "Invalid user file. Please submit valid user file";
	public static final String FILE_ITEM = "file_item";
	public static final String AMOUNT = "amount";
	public static final String INITIAL_STATUS = "intialGcStatus";
	public static final String COUPON_TYPE = "couponType";
	public static final String EXPIRY_DATE = "expiryDate";
	public static final String FINAL_STATUS = "finalGcStatus";
	public static final String INPUT_TEXT_AREA = "p_gc_number";
	public static final int MAX_GC_COUPON_IN_XLS = 50000;
	public static final int MAX_GC_COUPON_IN_TEXT_AREA = 350;

	/**
	 * init.
	 * 
	 * @param config
	 * @exception ServletException
	 * 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger = new Logger(
				"com.ftd.osp.opsadministration.presentation.GrouponRefundServlet");
	}

	/**
	 * Retrieves security parameters.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Hashmap of security parameters
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap getSecurity(HttpServletRequest request) {
		// Retrieve security variables
		String securityContext = request
				.getParameter(HTML_PARAM_SECURITY_CONTEXT);
		String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
		String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

		if (securityContext == null) {
			securityContext = "";
		}

		if (securityToken == null) {
			securityToken = "";
		}

		if (adminAction == null) {
			adminAction = "";
		}

		// Add security variables
		HashMap securityParams = new HashMap();
		securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
		securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
		securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);

		return securityParams;
	}

	/**
	 * Obtains required for the initial load of the grouponRefund.xsl page
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @exception ServletException
	 * @exception IOException
	 * 
	 */

	/**
	 * doGet.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @exception ServletException
	 * @exception IOException
	 * 
	 */

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		this.grouponRefund(request, response);
	}

	/**
	 * doPost.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @exception ServletException
	 * @exception IOException
	 * 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		this.grouponRefund(request, response);
	}

	public void grouponRefund(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HashMap<String, Serializable> fieldMap = new HashMap<String, Serializable>();
		List<?> items = null;
		FileItem fileItem = null;
		String initialStatus = null;
		String finalStatus = null;
		String amount = null;
		String couponsType = null;
		String textArea = null;
		String expiryDate = null;
		ArrayList<String> gcCouponsNumber = null;
		Document dataDocument = null;
		ArrayList<String> intialCouponStatus = new ArrayList<String>();
		// Validation Of the Security params
		try {
			conn = DataSourceUtil.getInstance().getConnection(DATA_SOURCE);
			dataDocument = DOMUtil.getDocument();

			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF)
					.equals(TRUE) && !ServletHelper.isValidToken(request)) {
				// Failed log on
				logger.error("Logon failed for security token: "
						+ request.getParameter(HTML_PARAM_SECURITY_TOKEN));
				ServletHelper.redirectToLogin(request, response);
				return;
			}
			// Retrieve all Secure System Configuration properties
			SecureConfigDAO scDAO = new SecureConfigDAO(conn);
			Document secureData = scDAO.loadContexts();
			DOMUtil.addSection(dataDocument, secureData.getChildNodes());
			// Add security variables to the root document
			DOMUtil.addSection(dataDocument, "securityParams", "params",
					getSecurity(request), true);

			// check whether it is multi part form.
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (isMultipart) {
				try {
					// Create a new file upload handler
					DiskFileUpload upload = new DiskFileUpload();
					items = upload.parseRequest(request);

					// Get file items. Put in a map.
					for (int i = 0; i < items.size(); i++) {
						FileItem item = (FileItem) items.get(i);

						if (item.isFormField()) {
							logger.debug("Found Form field Name="
									+ item.getFieldName() + "...value="
									+ item.getString());
							if (item.getFieldName().equalsIgnoreCase(
									INITIAL_STATUS)) {
								intialCouponStatus.add(item.getString());
							} else

								fieldMap.put(item.getFieldName(),
										item.getString());
						} else {
							logger.debug("Found a file...");
							fieldMap.put(FILE_ITEM, item);
						}
					}
				} catch (Exception ex) {
					logger.error(ex);
					throw ex;
				}

			}
			if (!fieldMap.isEmpty()) {

				fileItem = (FileItem) fieldMap.get(FILE_ITEM);
				initialStatus = getListAsString(intialCouponStatus, ",");
				finalStatus = (String) fieldMap.get(FINAL_STATUS);
				amount = (String) fieldMap.get(AMOUNT);
				expiryDate = (String) fieldMap.get(EXPIRY_DATE);
				couponsType = (String) fieldMap.get(COUPON_TYPE);
				textArea = (String) fieldMap.get(INPUT_TEXT_AREA);
			}
			if (fileItem != null) {
				gcCouponsNumber = processFile(fileItem.getInputStream());

			} else {
				gcCouponsNumber = getCouponNumberAsList(textArea);
			}
			if (couponsType != null && gcCouponsNumber.size() <= 0) {
				Map<String, String> updateStatusMap = new HashMap<String, String>();
				updateStatusMap.put("UpdateStatus",
						"Input Field Batch Numbers/Coupon Numbers is empty");
				DOMUtil.addSection(dataDocument, "DateMessage",
						"UpdateStatusMessage",
						(HashMap<String, String>) updateStatusMap, true);
			}
			else {
				if (fileItem != null
						&& gcCouponsNumber.size() > MAX_GC_COUPON_IN_XLS) {
					Map<String, String> updateStatusMap = new HashMap<String, String>();
					updateStatusMap.put("UpdateStatus",
							"Groupon Coupons should be less than 50K in XLS");
					DOMUtil.addSection(dataDocument, "DateMessage",
							"UpdateStatusMessage",
							(HashMap<String, String>) updateStatusMap, true);
				} else if (fileItem == null
						&& gcCouponsNumber.size() > MAX_GC_COUPON_IN_TEXT_AREA) {
					Map<String, String> updateStatusMap = new HashMap<String, String>();
					updateStatusMap
							.put("UpdateStatus",
									"Groupon Coupons should be less than 350 in TextArea");
					DOMUtil.addSection(dataDocument, "DateMessage",
							"UpdateStatusMessage",
							(HashMap<String, String>) updateStatusMap, true);
				}
				else {
					// QE-76 removing duplicates from the arayList

					Set<String> hs = new LinkedHashSet<String>(gcCouponsNumber);
					gcCouponsNumber.clear();
					gcCouponsNumber.addAll(hs);
					if (couponsType != null && !couponsType.isEmpty()
							&& couponsType.equalsIgnoreCase("CouponNumber")) {
						
						  Map<String, String> updateStatusMap = new
						  GrouponRefundBO()
						  .updateGrouponByCouponNumber(gcCouponsNumber,
						  initialStatus, finalStatus, amount, expiryDate,
						  ServletHelper.getUserId(request));
						  DOMUtil.addSection(dataDocument, "DateMessage",
						  "UpdateStatusMessage", (HashMap<String, String>)
						  updateStatusMap, true);
					}
					if (couponsType != null && !couponsType.isEmpty()
							&& couponsType.equalsIgnoreCase("BatchNumber")) {
						Map<String, String> updateStatusMap = new GrouponRefundBO()
								.updateGrouponByBatchNumber(gcCouponsNumber,
										initialStatus, finalStatus, amount,
										expiryDate,
										ServletHelper.getUserId(request));
						DOMUtil.addSection(dataDocument, "DateMessage",
								"UpdateStatusMessage",
								(HashMap<String, String>) updateStatusMap, true);
					}
				}
			}
			File xslFile = new File(getServletContext().getRealPath(
					"/xsl/grouponRefund.xsl"));
			TraxUtil.getInstance().transform(request, response, dataDocument,
					xslFile, null);

		} catch (Exception e) {
			Map<String, String> updateStatusMap = new HashMap<String, String>();
			updateStatusMap
					.put("UpdateStatus",
							"Cannot update the Groupon Coupon status."
									+ e.getMessage());
			DOMUtil.addSection(dataDocument, "DateMessage",
					"UpdateStatusMessage",
					(HashMap<String, String>) updateStatusMap, true);
			logger.error(e);
			File xslFile = new File(getServletContext().getRealPath(
					"/xsl/grouponRefund.xsl"));
			try {
				TraxUtil.getInstance().transform(request, response,
						dataDocument, xslFile, null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				logger.error(se);
			}
		}
	}

	/**
	 * This Method is for getting the Gc Coupon as list from the String.
	 * 
	 * @param textArea
	 * 
	 */

	private ArrayList<String> getCouponNumberAsList(String textArea) {
		ArrayList<String> gcNumbers = new ArrayList<String>();

		if (textArea != null) {
			StringTokenizer st;
			if (textArea.contains(",")) {
				textArea = textArea.replace(",", "\r\n");
			}
			st = new StringTokenizer(textArea.toString(), "\r\n");

			while (st.hasMoreElements()) {
				gcNumbers.add(st.nextElement().toString());
			}

		}

		return gcNumbers;
	}

	/**
	 * This Method is for getting the Gc Coupon as list from the xls sheet.
	 * QE-77 - Formatting issues with excel file in Gift Certificate Mass Edit
	 * Tool
	 * 
	 * @param InputStream
	 *            uploadedStream
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public static ArrayList<String> processFile(InputStream uploadedStream)
			throws ExcelProcessingException, Exception {
		try {
			ArrayList<String> gcNumbers = new ArrayList<String>();
			HSSFWorkbook workbook = new HSSFWorkbook(uploadedStream);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator rows = sheet.rowIterator();

			while (rows.hasNext()) {
				HSSFRow row = (HSSFRow) rows.next();
				Iterator cells = row.cellIterator();
				List<String> data = new ArrayList<String>();

				while (cells.hasNext()) {
					HSSFCell cell = (HSSFCell) cells.next();

					if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
						if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
							String cellValue = Double.toString(
									cell.getNumericCellValue()).trim();
							data.add(cellValue);
						} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
							data.add(cell.getRichStringCellValue().getString()
									.trim());
						}
					}
				}
				gcNumbers.addAll(data);
			}

			return gcNumbers;

		} catch (IOException ex) {
			throw new ExcelProcessingException(BAD_FILE);
		}
	}

	/**
	 * This Method is for string with separator and quotes from the list
	 * 
	 * @param InputStream
	 *            uploadedStream
	 * 
	 */
	private String getListAsString(List<String> list, String seperator) {
		StringBuffer string = new StringBuffer();
		if (list != null && list.size() > 0) {
			int count = 1;
			for (String element : list) {
				string.append(element);
				if (count != list.size()) {
					string.append(seperator);
				}
				count++;
			}
		}
		return string.toString();
	}

}
