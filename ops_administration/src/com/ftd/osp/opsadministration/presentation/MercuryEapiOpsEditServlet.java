package com.ftd.osp.opsadministration.presentation;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.opsadministration.dao.OpsAdminDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This class retrieves results from two database queries,
 * one that contains every row in the MERCURY_EAPI_OPS table. 
 * These results are passed to boxx_dashboard.xsl
 * for display and editing, if applicable.
 *
 */
public class MercuryEapiOpsEditServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private static final String VIEW_MERCURY_EAPI_OPS = "VIEW_MERCURY_EAPI_OPS";
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";
    public static final String BOXX_PAGE_PROPERTY = "boxx_url";
    private static final String Y_STRING = "Y";
    private static final String N_STRING = "N";

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.MercuryEapiOpsEditServlet");
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      String logonPage = null;
      String exitPage = null;

      try{
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
        exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
        if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
        {

          String operation = request.getParameter("operation");
          if (operation.equalsIgnoreCase("add")) {
            this.addMercuryEapiOps(request, response);
          } else if (operation.equalsIgnoreCase("available")) {
            this.updateMercuryEapiOps(request, response, operation); 
          } else if (operation.equalsIgnoreCase("inUse")) {
            this.updateMercuryEapiOps(request, response, operation); 
          }

          ServletContext context = this.getServletContext();
          RequestDispatcher dispatcher = context.getRequestDispatcher("/servlet/EAPIDashboardServlet");

          // change your request and response accordingly

          dispatcher.forward(request, response);
            
        }
        else
        {
          //failed logon
          logger.error("Logon Failed");
          ServletHelper.redirectToLogin(request, response);
          return;         
        }
      } 
      catch(Exception e)
      {
          logger.debug(e);
      }
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      response.setContentType(CONTENT_TYPE);
      this.loadMercuryEapiOps(request, response);
    }

     /**
     * Retrieve the MERCURY_OPS records and open MERCURY records from the database.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadMercuryEapiOps(HttpServletRequest request, HttpServletResponse response)
    {
        
        String mainMemberCode = request.getParameter("mainMemberCode");
        String operation = request.getParameter("operation");
        Connection connection = null;
        DataRequest dataRequest = null;
        
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }

        if(securityToken == null)
        {
          securityToken = "";
        }

        if (adminAction == null) 
        {
            adminAction = "";
        }

        try{          
            Document responseDocument = DOMUtil.getDocument();
            
            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
                        
            if ( operation != null && operation.equalsIgnoreCase("add") ) {
              File xslFile = new File(getServletContext().getRealPath("/xsl/addMainMemberCode.xsl"));
              TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
              return;
            } 
              
        } 
        
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    /**
     * Updates the MERCURY__EAPI_OPS records.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void updateMercuryEapiOps(HttpServletRequest request, HttpServletResponse response, String operation)
    {
        Connection conn = null;
        OpsAdminDAO dao = null;
     
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }

        if(securityToken == null)
        {
          securityToken = "";
        }

        if (adminAction == null) 
        {
            adminAction = "";
        }
        try{   
            conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");        
            dao = new OpsAdminDAO(conn);
            if (operation.equalsIgnoreCase("available")) {
              String[] mainMemberCodes = request.getParameterValues("selectRow");
              this.updateMercuryEapiOps(request, mainMemberCodes, null, ServletHelper.getUserId(request),dao);
              
            } else if (operation.equalsIgnoreCase("inUse")) {
              String[] mainMemberCodes = request.getParameterValues("selectRow");
              this.updateMercuryEapiOps(mainMemberCodes, N_STRING, null, ServletHelper.getUserId(request),dao);
            }            
        } 
        
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(conn != null)
                  conn.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    /**
     * Updates the availability flags for the select main member code
     * 
     * @param request
     * @param mainMemberCodes
     * @param inUse
     * @param updatedBy
     * @param dao
     */
    private void updateMercuryEapiOps(HttpServletRequest request, String[] mainMemberCodes, String inUse, String updatedBy, OpsAdminDAO dao) {
      final String AVAILABLE = "Available";
      try{
          if (mainMemberCodes == null) {
            return;
          }
          
          for (int i =0; i < mainMemberCodes.length; i++) {
            if (Y_STRING.equalsIgnoreCase(request.getParameter(mainMemberCodes[i]+AVAILABLE))) {
              dao.updateMercuryEapiOps(mainMemberCodes[i], inUse, N_STRING, updatedBy);
            }
            else {
              dao.updateMercuryEapiOps(mainMemberCodes[i], inUse, Y_STRING, updatedBy);
            }
          }
          
      } 
      
      catch(Exception e)
      {
          logger.error(e);
      } 
    }

    
    /**
     * Updates all the main member code config in MERCURY_EAPI_OPS table
     * 
     * @param mainMemberCodes
     * @param inUse
     * @param availableFlag
     * @param updatedBy
     * @param dao
     */
    private void updateMercuryEapiOps(String[] mainMemberCodes, String inUse, String availableFlag, String updatedBy, OpsAdminDAO dao) {
      
      try{
          if (mainMemberCodes == null) {
            return;
          }
          
          for (String mainMemberCode : mainMemberCodes) {         
            dao.updateMercuryEapiOps(mainMemberCode, inUse, availableFlag, updatedBy);
          }
          
      } 
      
      catch(Exception e)
      {
          logger.error(e);
      } 
    }
    
    /**
     * Add new main member code into the MERCURY__EAPI_OPS table.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void addMercuryEapiOps(HttpServletRequest request, HttpServletResponse response)
    {
        String inUse = request.getParameter("newInUse");
        String availableFlag = request.getParameter("newAvailableFlag");
        String mainMemberCode = request.getParameter("newMainMemberCode");
        Connection conn = null;
        OpsAdminDAO dao = null;
        
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
                        
        try
        {
          // get database connection
          conn =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
          dao = new OpsAdminDAO(conn);

          dao.insertMercuryEapiOps(mainMemberCode, inUse, availableFlag, ServletHelper.getUserId(request));
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(conn != null)
                  conn.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
  }
