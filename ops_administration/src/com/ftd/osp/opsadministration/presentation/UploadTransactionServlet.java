package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.*;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.*;

import java.util.HashMap;

import javax.servlet.*;
import javax.servlet.http.*;

import org.w3c.dom.Document;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.MultipartPostMethod;


/**
 * This class works witht the uploadTransactions.xsl page
 * to allow the admin to upload a Wal-Mart Transaction back 
 * into processing.
 * 
 * @author Carl Jenkins
 */

public class UploadTransactionServlet extends HttpServlet
{
  private static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  private static final String HTML_PARAM_EXIT_PAGE = "exitpage";
  private static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  private static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  private static final String LOGON_PAGE_PROPERTY = "logonpage";
  private static final String EXIT_PAGE_PROPERTY = "exitpage";

  private static final String DISPLAY_ERROR_MSG = "Unable to send transaction, please review error logs.";
  private static final String DISPLAY_ERROR_MSG_EMTPY = "Unable to send transaction, please select a file.";
  private static final String DISPLAY_SENT_MSG  = "Transaction sent successfully.";
  private static String ERROR_MESSAGE = "";
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private static final String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
  private static final String POP_URL = "pop_url";
  private static final String SEND = "send";
  private static final String UPLOAD = "upload";
  private static final String FILE = "file_name";
  private static final String ERROR = "ERROR";
  private static final String FALSE = "FALSE";
  private static final String TRUE = "TRUE";
  private static boolean httpCommunicationError = false;
 
  
  //logging
  private Logger logger;
    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.UploadTransactionServlet");
    }

   /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      doPost(request, response);
    }

    /**
   * This method calls the approperiate methods
   * to execute based on the actions taken.
   * @param request
   * @param response
   * @throws javax.servlet.ServletException
   * @throws java.io.IOException
   */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        String action = request.getParameter(SEND);
        String file = request.getParameter(FILE);
        //if action has a value then we upload the XML
        if((action != null && !action.equals("")) && (file != null && !file.equals("")))
        {
          try {
              //send the transaction file to the Wal-Mart servlet
              uploadWalMartTransaction(request, response);
          }
          catch(Exception e)
          {
            String message = "Unable to upload Wal-Mart transaction" + e.toString();
            logger.error(message);
          }
        }
        else if( ( file == null || file.equals("") ) && ( action != null && !action.equals("")))
        {
          this.setDisplayError(request);
          ERROR_MESSAGE = DISPLAY_ERROR_MSG_EMTPY;
          //dispaly the upload page
          this.dispalyUploadPage(request, response);
        }
        else
        {
          //dispaly the upload page
          this.dispalyUploadPage(request, response);
        }
        
    }
    
    
    /**
   * Makes a connection to the Wal-Mart servlet 
   * and send the Wal-Mart Transaction.
   * 
   * @param request
   * @param response
   * @throws java.lang.Exception
   */
    private void uploadWalMartTransaction(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
      String parameter = "FILE";
      String fileString = request.getParameter("file_name");
      MultipartPostMethod post = null;
      String url = null;
      int result = 0;
      
      try{
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            url = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,POP_URL);
            
            logger.error("Preparing to send Wal-Mart transaction");
            File xmlFile = new File(fileString);
            
            //parser.parse(new InputSource(new FileReader(xmlFile)));
           // document = parser.getDocument();
           // StringWriter s = new StringWriter();
            //document.print(new PrintWriter(s));
            
            // Prepare MultipartHTTP post
            post = new MultipartPostMethod(url);
            post.addParameter(parameter, xmlFile);
            
            // Get HTTP client
            HttpClient httpclient = new HttpClient();
            
            // Execute request and log results
            result = httpclient.executeMethod(post);
            logger.debug("Returned response from WalmartServlet: " + result);
        
        }
        catch(java.io.IOException e)
        {
           httpCommunicationError = true;
           String message = "Error trying to communicate with WalmartServlet." + e.toString();
           logger.error(e);
        
        }
        catch(IllegalStateException ise) 
       {
          httpCommunicationError = true;
          String message = "Error trying to communicate with WalMartServlet. URL:" + url + " Error:" + ise.toString();
          logger.error(ise);
          
       }  
       catch(IllegalArgumentException iae)
       {
         httpCommunicationError = true;
         String message = "Error trying to communicate with WalMartServelt. URL: " + url +" ERROR:" + iae.toString();
         logger.error(iae);
       }
       finally
       {
         // Release current connection to the connection pool once you are done
         post.releaseConnection();
         if(httpCommunicationError)
         {
           ERROR_MESSAGE = DISPLAY_ERROR_MSG;
           setDisplayError(request);
         }
         else
         {
           setDisplaySuccess(request);
         }
          this.dispalyUploadPage(request, response);
       }
      
    }//end uploadWalMartTransaction
    
    
    /**
   * Set an ERROR attribute to TRUE 
   * @param request
   */
    private void setDisplayError(HttpServletRequest request)
    {
      request.removeAttribute(ERROR);
      request.setAttribute(ERROR,TRUE);
    }
    /**
   * Set an ERROR attribute to FALSE
   * @param request
   */
    private void setDisplaySuccess(HttpServletRequest request)
    {
      request.removeAttribute(ERROR);
      request.setAttribute(ERROR,FALSE);
    }
  
      /**
     * Dispaly the page used to upload 
     * Wal-Mart Transactions to the WalmartServlet
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void dispalyUploadPage(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
             ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
             String url = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, POP_URL);
              
             Object errorObj = request.getAttribute("ERROR");
             String error = (String)errorObj;
             
             String securityContext = request.getParameter( HTML_PARAM_SECURITY_CONTEXT );
             String securityToken = request.getParameter( HTML_PARAM_SECURITY_TOKEN );
             String adminAction = request.getParameter( HTML_PARAM_ADMIN_ACTION );
            
        
            if( securityContext == null )
            {
               securityContext = "";
            }
               
            if( securityToken == null )
             {
              securityToken = "";
             }
               
            if( adminAction == null )
              {
                adminAction = "";
              }
            
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
    
            if(error != null && error.equalsIgnoreCase("true"))
            {
              HashMap params = new HashMap();
              params.put("ERROR", "TRUE");
              params.put("MESSAGE", ERROR_MESSAGE);
              DOMUtil.addSection(responseDocument, "pageData", "params", params, true); 
            }
            else if (error != null && error.equalsIgnoreCase("false"))
            {
              HashMap params = new HashMap();
              params.put("ERROR", "FALSE");
              params.put("MESSAGE", DISPLAY_SENT_MSG );
              DOMUtil.addSection(responseDocument, "pageData", "params", params, true); 
            }
            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
      
            File xslFile = new File(getServletContext().getRealPath("/xsl/uploadTransactions.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
          logger.error("UploadTransactionSerlvet: Unable to transform document");
          logger.error(e);
        }
        finally{}
        
    }
}//end UploadTransactionServlet
