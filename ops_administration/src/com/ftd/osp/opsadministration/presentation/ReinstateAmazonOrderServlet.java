package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.GenerateXML;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.EventHandlerUtility;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;

import javax.servlet.*;
import javax.servlet.http.*;

import javax.transaction.UserTransaction;

import org.w3c.dom.Document;

/**
 * This class is used to reinstate an Amazon.com order.  It retrieves the 
 * information stored in the AZ_ERROR table.    
 *
 * @author Rose Lazuk
 */
 
public class ReinstateAmazonOrderServlet extends HttpServlet 
{
      private final static String  XSL_REINSTATE_AMAZON_ORDER = "/xsl/reinstateAmazonOrder.xsl";
      private final static String  XSL_FIX_ORDER = "/xsl/fixOrder.xsl";
      private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
      private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
      private static final String CONTENT_TYPE = "text/html; charset=utf-8";
      private static final String ORDER_SCRUB = "ORDER SCRUB";     
      private static final boolean SEND_JMS_MSG = true;   
      public static final String ACTION = "action";
      public static final String REINSTATE = "reinstate";
      public static final String FIX_XML = "fix_xml";

      
     
      private static final String RETRIEVE_ALL_ORDERS = "SCRUB.VIEW_AZ_ERROR";
      private static final String DELETE_RECORD = "SCRUB.DELETE_AZ_ERROR";
      private static final String INSERT_ERROR = "SCRUB.INSERT_AZ_ERROR";
      
      private Logger logger;
    
      //custom parameters returned from database procedure
      private static final String STATUS_PARAM = "RegisterOutParameterStatus";
      private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  
      //security parameters
      public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
      public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
      public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
      public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
      public static final String LOGON_PAGE_PROPERTY = "logonpage";    
      public static final String EXIT_PAGE_PROPERTY = "exitpage";
      
      public static final String xmlData = "<?xml version = '1.0' encoding = 'utf-8'?><order guid=''>"
                             + "<header>"
                             + "<master-order-number>A003</master-order-number>"
                             + "<source-code>522</source-code>"
                             + "<origin>AMZNI</origin>"
                             + "<order-count>2</order-count>"
                             + "<order-amount>61.11</order-amount>"
                             + "<transaction-date>Mon Jan 21 12:30:08 CST 2004</transaction-date>"
                             + "<socket-timestamp/>"
                             + "<buyer-first-name>Rose</buyer-first-name>"
                             + "<buyer-last-name>Lazuk</buyer-last-name>"
                             + "<buyer-address1>3113 woodcreek drive</buyer-address1>"
                             + "<buyer-address2/>"
                             + "<buyer-city>Downers Grove</buyer-city>"
                             + "<buyer-state>IL</buyer-state>"
                             + "<buyer-postal-code>60515</buyer-postal-code>"
                             + "<buyer-country>US</buyer-country>"
                             + "<buyer-daytime-phone>5555555555</buyer-daytime-phone>"
                             + "<buyer-evening-phone/>"
                             + "<buyer-work-ext/>"
                             + "<buyer-fax/>"
                             + "<buyer-email-address/>"
                             + "<ariba-buyer-cookie/>"
                             + "<ariba-asn-buyer-number/>"
                             + "<ariba-payload/>"
                             + "<news-letter-flag>Y</news-letter-flag>"
                             + "<cc-type>IN</cc-type>"
                             + "<cc-number/>"
                             + "<cc-exp-date/>"
                             + "<cc-approval-code/>"
                             + "<cc-approval-amt/>"
                             + "<cc-approval-verbage/>"
                             + "<cc-approval-action-code/>"
                             + "<cc-avs-result/>"
                             + "<cc-acq-data/>"
                             + "<co-brand-credit-card-code/>"
                             + "<aafes-ticket-number/>"
                             + "<gift-certificates/>"
                             + "<co-brands/>"
                             + "</header>"                             
                             + "<items>"
                             + "<item>"
                             + "<order-number>Z003</order-number>"
                             + "<order-total>61.11</order-total>"
                             + "<tax-amount>4.13</tax-amount>"
                             + "<service-fee>9.99</service-fee>"
                             + "<extra-shipping-fee/>"
                             + "<drop-ship-charges/>"
                             + "<retail-variable-price>46.99</retail-variable-price>"
                             + "<productid>C13-3068</productid>"
                             + "<product-price>46.99</product-price>"
                             + "<color/>"
                             + "<size/>"
                             + "<add-ons/>"
                             + "<recip-first-name>robin</recip-first-name>"
                             + "<recip-last-name>bird</recip-last-name>"
                             + "<recip-address1>7205 Seminole Drive</recip-address1>"
                             + "<recip-address2/>"
                             + "<recip-city>Darien</recip-city>"
                             + "<recip-state>IL</recip-state>"
                             + "<recip-postal-code>60561</recip-postal-code>"
                             + "<recip-country>US</recip-country>"
                             + "<recip-international/>"
                             + "<recip-phone>5555555555</recip-phone>"
                             + "<recip-phone-ext/>"
                             + "<ship-to-type>R</ship-to-type>"
                             + "<ship-to-type-name/>"
                             + "<ship-to-type-info/>"
                             + "<occassion>2</occassion>"
                             + "<delivery-date>02/21/2004</delivery-date>"
                             + "<second-delivery-date/>"
                             + "<card-message>test</card-message>"
                             + "<card-signature>test</card-signature>"
                             + "<special-instructions/>"
                             + "<shipping-method/>"
                             + "<lmg-flag/>"
                             + "<lmg-email-address/>"
                             + "<lmg-email-signature/>"
                             + "<fol-indicator>FTD</fol-indicator>"
                             + "<sunday-delivery-flag/>"
                             + "<sender-release-flag/>"
                             + "<qms-result-code/>"
                             + "<qms-address1>7205 Seminole Drive</qms-address1>"
                             + "<qms-address2/>"
                             + "<qms-city>Darien</qms-city>"
                             + "<qms-state>IL</qms-state>"
                             + "<qms-postal-code>60561</qms-postal-code>"
                             + "<qms-firm-name/>"
                             + "<qms-latitude/>"
                             + "<qms-longtitude/>"
                             + "<qms-override-flag/>"
                             + "<qms-usps-range-record-type/>"
                             + "<ariba-po-number/>"
                             + "<ariba-cost-center/>"
                             + "<ariba-ams-project-code/>"
                             + "<ariba-unspsc-code>empty</ariba-unspsc-code>"
                             + "<product-substitution-acknowlegement/>"
                             + "</item>"
                             + "</items></order>";
                             
        
            
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    logger = new Logger("com.ftd.osp.opsadministration.presentation.ReinstateAmazonOrderServlet");
  }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        String logonPage = null;
        String exitPage = null;

        
        try{
              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
              logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
              exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            response.setContentType(CONTENT_TYPE);
            //if there is an action in the request, then call
            //fixOrder.xsl page
            //     if(request.getParameter( ACTION ) != null)
            // {
            //   this.loadFixOrder(request, response);
            // }             
            //if not action parameter is passed over, then get all Amazon Orders
            //in the AZ_ERROR table
            //  else
            // {
              //this.insertError();
              this.loadReinstateAmazonOrder(request, response);
           // }
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
    }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
        String logonPage = null;
        String exitPage = null;

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    

          if ( request.getParameter(ACTION) == null )
          {
            this.loadReinstateAmazonOrder(request,response);
          }
          else 
          {
              if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
              {
    
                String action = request.getParameter( ACTION );
                if(action.equals( REINSTATE ))
                {
                  this.reinstateAmazonOrder(request, response);
                }
                else if(action.equalsIgnoreCase( FIX_XML  ) )
                {
                    this.loadFixOrder(request, response);
                }
                                        
              }
              else
              {
                //failed logon
                logger.error("Logon Failed");
                ServletHelper.redirectToLogin(request, response);
                return;         
              }
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
        
  }
  
  /**
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadReinstateAmazonOrder(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
            Connection connection = null;
            DataRequest dataRequest = null;
        try
        {
            String securityContext = request.getParameter( HTML_PARAM_SECURITY_CONTEXT );
            String securityToken = request.getParameter( HTML_PARAM_SECURITY_TOKEN );
            String adminAction = request.getParameter( HTML_PARAM_ADMIN_ACTION );
                
            
            if( securityContext == null )
            {
               securityContext = "";
            }
               
            if( securityToken == null )
             {
              securityToken = "";
             }
               
            if( adminAction == null )
              {
                adminAction = "";
              }
                
            Document respDoc = ( Document )DOMUtil.getDocument();
            
            connection = DataSourceUtil.getInstance().getConnection( ORDER_SCRUB  );
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            dataRequest = new DataRequest();
            dataRequest.setConnection( connection );
            dataRequest.setStatementID( RETRIEVE_ALL_ORDERS  );
            
            CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
                        
                 
            //use GenerateXML utility class to generate the reinstate amazon
            //order data.  This is being done because the current data access utility
            //class can not handle processing the clob when output type is set to
            //Document.  
            GenerateXML generateXML = new GenerateXML();
            Document reinstateOrderDocument = (Document)generateXML.generateReinstateAmazonOrderXML(rs);
            
            DOMUtil.addSection(respDoc, reinstateOrderDocument.getChildNodes() ); 
            
            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);
            DOMUtil.addSection(respDoc, "securityParams", "params", securityParams, true);
            
            File xslFile = new File(getServletContext().getRealPath( XSL_REINSTATE_AMAZON_ORDER ));
            TraxUtil.getInstance().transform(request, response, respDoc, xslFile, null);
        }
        catch(Exception e)
        {
          logger.error(e);
          throw new Exception(e);
          
        }
        finally
        {
          connection.close();
        }
    }
    
    /**
     * Delete record from the AZ_ERROR table.
     * @param confirmation order number String
     * @param connection Connection
     * @exception Exception
     */
    private void deleteRecord(String id, Connection connection) throws Exception
    {
        DataRequest dataRequest = null;
                
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                  
        dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID( DELETE_RECORD );

        HashMap systemMessage = new HashMap();
        systemMessage.put("IN_ID", id);
        systemMessage.put(STATUS_PARAM,"");
        systemMessage.put(MESSAGE_PARAM,"");
        
        dataRequest.setInputParams( systemMessage );

        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get( STATUS_PARAM );

        if(status.equals("N"))
        {
            String message = (String) outputs.get( MESSAGE_PARAM );
            throw new Exception( message );
        }
    }
    
    /**
     * Loads the fix Order page.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadFixOrder(HttpServletRequest request, HttpServletResponse response)
    {
      StringBuffer buf = new StringBuffer();
      
      try
        {
            // Create the initial document
            String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
            String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);        
            String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
            
            if(securityContext == null)
            {
              securityContext = "";
            }
            
            if(securityToken == null)
            {
              securityToken = "";
            }
            
            if (adminAction == null) 
            {
                adminAction = "";
            }
            
            Document responseDocument = DOMUtil.getDocument();
            
            HashMap pageData = new HashMap();
            pageData.put("ID", request.getParameter("order_xml_id"));   
            pageData.put("XML", request.getParameter("xmlText") );   
            pageData.put("CONTEXT", request.getParameter("perspective")); 
            pageData.put("EVENT_NAME", request.getParameter("event_name")); 
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
      
            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
                           
            File xslFile = new File(getServletContext().getRealPath( XSL_FIX_ORDER ));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        
    }
    
      /**
     * Reinstate the order.  
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void reinstateAmazonOrder(HttpServletRequest request, HttpServletResponse response)
    {
          ConfigurationUtil configUtil = null;
          Connection connection = null;
          String context = null;
          String id = null;
          String eventName = null;

          InitialContext initialContext = null;
          UserTransaction userTransaction = null;

          try
          {
            String userId = ServletHelper.getUserId(request);
            ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
            int transactionTimeout = Integer.parseInt((String)configurationUtil.getProperty("ops_admin_config.xml", "transaction_timeout_in_seconds"));      
            // Retrieve the UserTransaction object
            String jndi_usertransaction_entry = configurationUtil.getProperty("ops_admin_config.xml", "jndi_usertransaction_entry");
            // get the initial context
            initialContext = new InitialContext();
            userTransaction = (UserTransaction)  initialContext.lookup(jndi_usertransaction_entry);
            logger.debug("User transaction obtained");
      
            userTransaction.setTransactionTimeout(transactionTimeout);
            logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");
            
            // Start the transaction with the begin method
            userTransaction.begin();
            logger.debug("********************BEGIN TRANSACTION***********************");
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection( ORDER_SCRUB );
  
            id = request.getParameter("order_xml_id");
            context = request.getParameter("perspective");    
            eventName = request.getParameter( "event_name" );
            //retrive the fixed XML 
            String xml = request.getParameter( "xmlText" );
             
            // record trying to enter data //
            Document document = (Document) DOMUtil.getDocument(xml);
            String amazonOrderNumber = DOMUtil.getNodeValue(document,"AmazonOrderID");
            String masterOrderNumber = "A"+amazonOrderNumber.replaceAll("-","");
            this.recordAmazonReinstateOrder(masterOrderNumber, context, xml, userId, connection);
            
            EventHandlerUtility.enqueueJmsMessage(eventName, xml, context);      
            
            this.deleteRecord(id, connection);
  
            
            //Assuming everything went well, commit the transaction.
            userTransaction.commit();
            logger.debug("User transaction committed");
            logger.debug("********************END TRANSACTION***********************");
          }
          catch(Throwable ex)
          {
            logger.error(ex);
            if (userTransaction != null)  
            {
              // rollback the user transaction
              try 
              {
                userTransaction.rollback();
                logger.error("User transaction rolled back");                
              } catch (Exception ex2) 
              {
                logger.error(ex2);
              }
            }
            
          } 
          finally
          {
            try  
            {
             // close initial context
             if(initialContext != null)
              {
                 initialContext.close();
              }
             if (connection != null && (!connection.isClosed())) 
             {
                connection.close();
            }
            } catch (Exception ex) 
            {
              logger.error(ex);
            }
         }
         
        try
        {
          this.loadReinstateAmazonOrder(request, response);
        }
        catch( Exception e )
        {
            logger.error(e);        
        }
    
    }
    
    private void insertError()
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                        
        try
        {
          
            // get database connection
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            
            /*
            String orderXMLString = null;
            String record = "";
            String fileName = "C://FTD//Order Scrub Project//services//ops administration//wwwroot//WEB-INF//src//com//ftd//osp//opsadministration//presentation//OneGoodOrder.xml";
            FileReader fr     = new FileReader(fileName);
            
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            while ((record = br.readLine()) != null) {
               sb.append(record);
            }
            orderXMLString = sb.toString();
            */
            
            String context = "AMAZON";
            String error = "Order count does not equal total number of items received in XML file.";
            String eventName = "ORDER-INFUSOR";
            
            // insert global parm data into the 
            // GLOBAL_PARMS table
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            HashMap systemMessage = new HashMap();
            systemMessage.put("IN_CONTEXT", context);
            systemMessage.put("IN_ERROR", error);
            systemMessage.put("IN_XML", this.xmlData);
            systemMessage.put("IN_EVENT_NAME", eventName);
            
            dataRequest.setStatementID(this.INSERT_ERROR);
            dataRequest.setInputParams(systemMessage);
  
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            String status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                String message = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(message);
            }

        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

  /**
     * Record the reinstate the order in a shadow table.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     *
     */
  private void recordAmazonReinstateOrder(String masterOrderNumber, String context, String xml, String createdBy, Connection connection)
    throws Exception {
    DataRequest dataRequest = null;

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    dataRequest = new DataRequest();
    dataRequest.setConnection(connection);
    HashMap systemMessage = new HashMap();
    systemMessage.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
    systemMessage.put("IN_CONTEXT", context);
    systemMessage.put("IN_XML", xml);
    systemMessage.put("IN_CREATED_BY", createdBy);
    dataRequest.setStatementID("AZ_INSERT_REINSTATE_ORDER");
    dataRequest.setInputParams(systemMessage);

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);    
    String status = (String) outputs.get("OUT_STATUS");
    
    if (status.equals("N")) {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

    
}
