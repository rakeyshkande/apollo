package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class SchedulerServlet extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
  
  //security parameters
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String EXIT_PAGE_PROPERTY = "exitpage";
    
  private Logger logger;

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    logger = new Logger("com.ftd.osp.opsadministration.presentation.SchedulerServlet");
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request,response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    String logonPage = null;
    String exitPage = null;

    try{
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
      exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_FILE,EXIT_PAGE_PROPERTY);
      
      if (   configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") 
          || ServletHelper.isValidToken(request)) 
      {
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

        if(securityContext == null)
        {
          securityContext = "";
        }
        
        if(securityToken == null)
        {
          securityToken = "";
        }
        
        if (adminAction == null) 
        {
            adminAction = "";
        }  
        
        StringBuffer sb = new StringBuffer();
        sb.append(configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, "scheduler_url"));
        sb.append("?context=");
        sb.append(securityContext);
        sb.append("&adminAction=");
        sb.append(adminAction);
        sb.append("&securitytoken=");
        sb.append(securityToken);
        logger.debug("Redirecting to: " + sb.toString());
        response.sendRedirect(sb.toString());
      }
      else
      {
        //failed logon
        logger.error("Logon Failed");
        ServletHelper.redirectToLogin(request, response);
        return;         
      }
    } 
    catch(Exception e)
    {
        logger.error(e);
    }
  }
}
