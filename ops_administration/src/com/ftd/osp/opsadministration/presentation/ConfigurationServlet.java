package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This class retrieves, updates, inserts and deletes data from
 * the GLOBAL_PARMS table in FRP.
 *
 * @author Rose Lazuk
 */
public class ConfigurationServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private static final String VIEW_GLOBAL_PARMS = "FRP.VIEW_GLOBAL_PARMS";
  private static final String UPDATE_GLOBAL_PARMS = "FRP.UPDATE_GLOBAL_PARMS";
  private static final String DELETE_GLOBAL_PARMS = "FRP.DELETE_GLOBAL_PARMS";
  private static final String VIEW_UNIQUE_CONTEXT = "FRP.VIEW_UNIQUE_CONTEXT";
  private static final String VIEW_PARMS_BY_CONTEXT = "FRP.VIEW_PARMS_BY_CONTEXT";

  private Logger logger;
  
  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

  //security parameters
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONTEXT";
  public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String EXIT_PAGE_PROPERTY = "exitpage";

  public static final String SELECTED_CONTEXT = "selected_context";
  
    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.ConfigurationServlet");
    }

     /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String logonPage = null;
        String exitPage = null;

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {

            String action = request.getParameter("action");
            if (action == null)
            {
                action = "";
                this.loadGlobalParms(request, response);
            }
            if(action.equals("add"))
            {
              this.addGlobalParms(request, response);
            }
              
            if(action.equals("update"))
            {
              this.updateGlobalParms(request, response);
            }
              
            if(action.equals("delete"))
            {
              this.deleteGlobalParms(request, response);
            }
              
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String logonPage = null;
        String exitPage = null;

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            response.setContentType(CONTENT_TYPE);
            if(request.getParameter("action") != null)
            {
              this.loadContexts(request, response);
            }             
            else
            {
              this.loadGlobalParms(request, response);
            }
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
    }

     /**
     * Retrieve the data from the GLOBAL_PARMS table 
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadGlobalParms(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                        
        try
        {
            // Create the initial document
            String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
            String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);        
            String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
            String selectedContext = request.getParameter(SELECTED_CONTEXT);
            logger.debug("selectedContext = " + selectedContext);
            
            if(securityContext == null)
            {
              securityContext = "";
            }
            
            if(securityToken == null)
            {
              securityToken = "";
            }
            
            if (adminAction == null) 
            {
                adminAction = "";
            }
            
            Document responseDocument = DOMUtil.getDocument();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            //retrieve global parm data from the GLOBAL_PARMS table
            //table 
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            HashMap systemMessage = new HashMap();
            
            if (selectedContext == null || selectedContext.equals(""))
            {
                dataRequest.setStatementID(VIEW_GLOBAL_PARMS);
                DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 
            }
            else
            {
                logger.debug("doing context specific select");
                HashMap pageData = new HashMap();
                pageData.put("selected_context", selectedContext);
                // Convert the page data hashmap to XML and append it to the final XML
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 

                dataRequest.setStatementID(VIEW_PARMS_BY_CONTEXT);
                dataRequest.addInputParam("IN_CONTEXT",selectedContext);
                DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 
            }

            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(VIEW_UNIQUE_CONTEXT);
            DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 

            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);

            StringWriter sw = new StringWriter();
            DOMUtil.print(responseDocument, sw);
            logger.debug(sw.toString());                
            
            File xslFile = new File(getServletContext().getRealPath("/xsl/configuration.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

     /**
     * Delete the context/name value pair from the GLOBAL_PARMS table
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void deleteGlobalParms(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                       
        try
        {
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                        
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            Enumeration enu = request.getParameterNames();
            String param = "";
            String id = "";

            while(enu.hasMoreElements())
            {     
                param = (String) enu.nextElement();
                if(param.startsWith("id_"))
                {
                  id = param.substring(3);  
                  HashMap systemMessage = new HashMap();
                  systemMessage.put("IN_CONTEXT", request.getParameter("context_" + id));
                  systemMessage.put("IN_NAME",    request.getParameter("name_" + id));
                  systemMessage.put("IN_UPDATED_BY", ServletHelper.getUserId(request));
                  dataRequest.setStatementID(DELETE_GLOBAL_PARMS);
                  dataRequest.setInputParams(systemMessage);
  
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }
                  
                }
            } 

            this.loadGlobalParms(request, response);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Update the value for the corresponding context/name pair in
     * the GLOBAL_PARMS table in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void updateGlobalParms(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                       
        try
        {
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                        
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            Enumeration enum1 = request.getParameterNames();
            String param = "";
            String id = "";

            while(enum1.hasMoreElements())
            {     
                param = (String) enum1.nextElement();
                if(param.startsWith("id_"))
                {
                  id = param.substring(3);  
                  String gpContext = request.getParameter("context_" + id);
                  String gpName    = request.getParameter("name_" + id);
                  String gpUser    = ServletHelper.getUserId(request);
                  logger.debug("Change to GlobalParms entry: " + gpContext + " " + gpName + 
                               " was made by: " + gpUser); 
                  HashMap systemMessage = new HashMap();
                  systemMessage.put("IN_CONTEXT",     gpContext);
                  systemMessage.put("IN_NAME",        gpName);
                  systemMessage.put("IN_VALUE",       request.getParameter("value_" + id));
                  systemMessage.put("IN_DESCRIPTION", request.getParameter("desc_" + id));
                  systemMessage.put("IN_UPDATED_BY",  gpUser);
                  dataRequest.setStatementID(UPDATE_GLOBAL_PARMS);
                  dataRequest.setInputParams(systemMessage);
  
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  String status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      String message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }
                  
                }
            } 

            this.loadGlobalParms(request, response);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Insert the data from the GLOBAL_PARMS table 
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void addGlobalParms(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                        
        try
        {
          
            // get database connection
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            // insert global parm data into the 
            // GLOBAL_PARMS table
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            HashMap systemMessage = new HashMap();
            systemMessage.put("IN_CONTEXT", request.getParameter("new_context"));
            systemMessage.put("IN_NAME", request.getParameter("new_name"));
            systemMessage.put("IN_VALUE", request.getParameter("new_value"));
            systemMessage.put("IN_DESCRIPTION", request.getParameter("new_description"));
            systemMessage.put("IN_UPDATED_BY", ServletHelper.getUserId(request));

            
            dataRequest.setStatementID(UPDATE_GLOBAL_PARMS);
            dataRequest.setInputParams(systemMessage);
  
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            String status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                String message = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(message);
            }

            this.loadGlobalParms(request, response);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

     /**
     * Retrieve the list of unique contexts from
     * from the GLOBAL_PARMS table 
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadContexts(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                        
        try
        {
            // Create the initial document
            String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
            String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);        
            String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
            
            if(securityContext == null)
            {
              securityContext = "";
            }
            
            if(securityToken == null)
            {
              securityToken = "";
            }
            
            if (adminAction == null) 
            {
                adminAction = "";
            }
            
            Document responseDocument = DOMUtil.getDocument();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            //retrieve global parm data from the GLOBAL_PARMS table
            //table 
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            HashMap systemMessage = new HashMap();
            dataRequest.setStatementID(VIEW_UNIQUE_CONTEXT);
            DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 

            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
            
                                                          
            File xslFile = new File(getServletContext().getRealPath("/xsl/addConfiguration.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
}
