package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.dao.SecureConfigDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * This class retrieves, updates, inserts and deletes data from
 * the secure_config table in FRP.
 *
 * @author Mike Kruger
 */
public class SecureConfigServlet extends HttpServlet 
{
  private static Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.SecureConfigServlet");
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private Connection conn = null;
  
  // Data Source name
  private static final String DATA_SOURCE = "ORDER SCRUB";
  
  // Security parameters
  private static final String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  private static final String SECURITY_CONTEXT = "Order Proc";
  private static final String VIEW_CONFIG_RESOURCE = "Secure Configuration";
  private static final String VIEW_PERMISSION = "View";
  public static final String HTML_VIEW_PERMISSION = "view_permission";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARM_SITENAME = "sitename";
  public static final String HTML_PARM_APP_CONTEXT = "applicationcontext";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String SECURITY_OFF = "SECURITY_OFF";
  public static final String TRUE = "true";
    
	

  /**
  * Initialize servlet
  * @param config
  * @exception Servlet Exception
  */
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  /**
  * Obtain security parameters.
  * @param request HttpServletRequest
  * @return Hashmap of security parameters 
  */
  private HashMap getSecurity(HttpServletRequest request)
    throws Exception
  {
    // Retrieve security variables
    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);        
    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    
    if(securityContext == null)
    {
      securityContext = "";
    }
    
    if(securityToken == null)
    {
      securityToken = "";
    }
    
    if (adminAction == null) 
    {
      adminAction = "";
    }
    
    // Obtain variables required for Non SSL Requests
    String sitename = ServletHelper.getSiteName();

    // Add security variables
    HashMap securityParams = new HashMap();
    securityParams.put(HTML_PARM_SITENAME, sitename == null? "" : sitename);
    securityParams.put(HTML_PARM_APP_CONTEXT, ((HttpServletRequest) request).getContextPath());
    securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
    securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
    securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
    
    // Retrieve user security for viewing secure configuration variables 
    String permission = request.getParameter(HTML_VIEW_PERMISSION);
    // If view permission does not exist retrieve it from the database
    if(permission == null || "".equals(permission))
    {
      permission = String.valueOf(SecurityManager.getInstance().
          assertPermission(SECURITY_CONTEXT, securityToken, VIEW_CONFIG_RESOURCE
              , VIEW_PERMISSION));
    }
    
    // Add view permission security
    securityParams.put(HTML_VIEW_PERMISSION, permission);
    
    return securityParams;
	}
		
  /**
  * Handles view/add/update/delete of SecureConfig.xsl page.
  * @param request HttpServletRequest
  * @param response HttpServletResponse
  * @exception ServletException
  * @exception IOException
  */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    try
    {
      // Get connection
      conn =  DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

      // Test for valid securitytoken
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF).equals(TRUE) 
        && !ServletHelper.isValidToken(request)) 
      {
        // Failed log on
        logger.error("Logon failed for security token: " + request.getParameter(HTML_PARAM_SECURITY_TOKEN));
        ServletHelper.redirectToLogin(request, response);
        return;         
      }

      // Obtain page action
      String action = request.getParameter("action");

      // If action is add - add a secure config property
      if(action != null && action.equals("add"))
      {
        updateProperty(request.getParameter("new_context"), 
            request.getParameter("new_name"),
            request.getParameter("new_value"),
            request.getParameter("new_desc"),
            SecurityManager.getInstance().getUserInfo(request.getParameter("securitytoken")).getUserID());
      }
      // If action is update - update secure config properties
      else if(action != null && action.equals("update"))
      {
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String id = "";
        String userId = SecurityManager.getInstance().getUserInfo(request.getParameter("securitytoken")).getUserID();
      
        // Iterate through parameters to find updated properties
        while(enum1.hasMoreElements())
        {     
          param = (String) enum1.nextElement();
          if(param.startsWith("id_"))
          {
            updateProperty(request.getParameter("context_" + param.substring(3)), 
                request.getParameter("name_" + param.substring(3)),
                request.getParameter("value_" + param.substring(3)),
                request.getParameter("desc_" + param.substring(3)),
                userId);
          }
        } 
      }
      // If action is delete - delete selected config properties
      else if(action != null && action.equals("delete"))
      {
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String id = "";
        String userId = SecurityManager.getInstance().getUserInfo(request.getParameter("securitytoken")).getUserID();
      
        // Iterate through parameters to find selected properties
        while(enum1.hasMoreElements())
        {     
          param = (String) enum1.nextElement();
          if(param.startsWith("id_"))
          {
            deleteProperty(request.getParameter("context_" + param.substring(3)), 
                request.getParameter("name_" + param.substring(3)),
                userId);
          }
        } 
      }
                    
      // Retrieve context currently selected within the dropdown
      String context = request.getParameter("selected_context");
      Document secureData = null;
                    
      // If selected context does not exist load all contexts and properties
      if(context == null || "".equals(context))
      {
        secureData = load(null);							
      }
      // If selected context exists load all contexts and only 
      // properties for the selected context
      else
      {
        secureData = load(context);
  
        // Pass along selected context as page data
        HashMap pageData = new HashMap();
        pageData.put("selected_context", context);
  
        // Convert the page data hashmap to XML and append it to the final XML
        DOMUtil.addSection(secureData, "pageData", "data", pageData, true); 
      }
                        
      // Add security variables
      DOMUtil.addSection(secureData, "securityParams", "params", getSecurity(request), true);
                  
      File xslFile = new File(getServletContext().getRealPath("/xsl/secureConfig.xsl"));
      TraxUtil.getInstance().transform(request, response, secureData, xslFile, null);
    } 
    catch(Exception e)
    {
      logger.error(e);
    }
    finally
    {
      try 
      {
        if(conn != null)
          conn.close();
      }
      catch(SQLException se) 
      {
        logger.error(se);
      }
    }
  }

  /**
  * Loads initial SecureConfig.xsl page.
  * @param request HttpServletRequest
  * @param response HttpServletResponse
  * @exception ServletException
  * @exception IOException
  * 
  */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    try
    {
      // Get connection
      conn =  DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

      // Test for valid securitytoken
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF).equals(TRUE) 
        && !ServletHelper.isValidToken(request)) 
      {
        // Failed log on
        logger.error("Logon failed for security token: " + request.getParameter(HTML_PARAM_SECURITY_TOKEN));
        ServletHelper.redirectToLogin(request, response);
        return;         
      }
        
      // Retrieve all Secure System Configuration contexts and properties
      Document secureData = load(null);
      
      // Add security variables
      DOMUtil.addSection(secureData, "securityParams", "params", getSecurity(request), true);
      
      File xslFile = new File(getServletContext().getRealPath("/xsl/secureConfig.xsl"));
      TraxUtil.getInstance().transform(request, response, secureData, xslFile, null);
    } 
    catch(Exception e)
    {
      logger.debug(e);
    }
    finally
    {
      try 
      {
        if(conn != null)
          conn.close();
      }
      catch(SQLException se) 
      {
        logger.error(se);
      }
    }
  }

  /**
  * Retrieve Secure Configuration contexts and properties 
  * @param context - context of configuration parameters to obtain
  * @return Document - Secure configuration data
  */
  private Document load(String context)
    throws Exception
  {
    // Create root document
    Document dataDocument = DOMUtil.getDocument();
    
    // Obtain all contexts
    Document contexts = loadContexts();
    
    // Obtain properties
    Document properties = loadProperties(context);

    // Add contexts and properties to the root document
    DOMUtil.addSection(dataDocument, contexts.getChildNodes());
    DOMUtil.addSection(dataDocument, properties.getChildNodes());

    return dataDocument;
  }

  /**
  * Loads all contexts
  * @return Document - Context data
  */
  private Document loadContexts()
    throws SAXException, ParserConfigurationException, IOException, SQLException
  {
    SecureConfigDAO scDAO = new SecureConfigDAO(conn);
    return scDAO.loadContexts();
  }

  /**
  * Loads all properties for a given context.  If context is null all
  * properties will be loaded.
  * @param context - Context to retrieve.  If null obtain all context
  * properties.
  * @return Document - Context property data
  */
  private Document loadProperties(String context)
    throws SAXException, ParserConfigurationException, IOException, SQLException
  {
    SecureConfigDAO scDAO = new SecureConfigDAO(conn);
    return scDAO.loadProperties(context);
  }

  /**
  * Add/Update secure property
  * @param context - context of configuration property
  * @param name - name of configuration property
  * @param value - value of configuration property
  * @param description - description of configuration property
  */
  private void updateProperty(String context, String name, String value,
    String description, String userId)
    throws SAXException, ParserConfigurationException, IOException, SQLException,
      Exception
  {
    SecureConfigDAO scDAO = new SecureConfigDAO(conn);
    scDAO.updateProperty(context, name, value, description, userId);
  }

  /**
  * Delete secure property
  * @param context - context of configuration property
  * @param name - name of configuration property
  */
  private void deleteProperty(String context, String name, String userId)
    throws SAXException, ParserConfigurationException, IOException, SQLException,
      Exception
  {
    SecureConfigDAO scDAO = new SecureConfigDAO(conn);
    scDAO.deleteProperty(context, name, userId);
  }
}
