package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This class retrieves data from the SYSTEM_MESSAGES table in FRP.
 *
 * @author Rose Lazuk
 */
public class SystemMessagesServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private static final String VIEW_SYSTEM_MESSAGES = "FRP.VIEW_SYSTEM_MESSAGES";
  private static final String VIEW_SYSTEM_MESSAGES_SOURCES = "FRP.VIEW_SYSTEM_MESSAGES_SOURCES";
  private static final String UPDATE_SYSTEM_MESSAGES_READ = "FRP.UPDATE_SYSTEM_MESSAGES_READ";
  private static final String DELETE_SYSTEM_MESSAGES = "FRP.DELETE_SYSTEM_MESSAGES";
  private static final String DEFAULT_VIEW_TYPE = "NEW";

  private Logger logger;
  private String status;
  private String message;
  
  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

  //security parameters
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String EXIT_PAGE_PROPERTY = "exitpage";

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.SystemMessagesServlet");
    }

     /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String logonPage = null;
        String exitPage = null;

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            this.deleteSystemMessages(request, response);
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }
        
        if(securityToken == null)
        {
          securityToken = "";
        }
        
        if (adminAction == null) 
        {
            adminAction = "";
        }

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            this.loadSystemMessages(request, response);
            }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
    }


     
     /**
     * Retrieve the system messages from the SYSTEM_MESSAGES table
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadSystemMessages(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
        String view_type = "";
                
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            // Add page data to xml
            HashMap pageData = new HashMap();
            if(request.getParameter("view_type") == null)
                view_type = DEFAULT_VIEW_TYPE;
            else
              view_type = request.getParameter("view_type");
                      
            pageData.put("view_type", view_type);
            pageData.put("source_type", request.getParameter("source_type"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            
            //retrieve the list of sources from the SYSTEM_MESSAGES
            //table 
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            HashMap systemMessage = new HashMap();
            dataRequest.setStatementID(VIEW_SYSTEM_MESSAGES_SOURCES);
            DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 
                                   
            //retrieve system message data from SYSTEM_MESSAGES table            
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection); 
            systemMessage = new HashMap();
            dataRequest.setStatementID(VIEW_SYSTEM_MESSAGES);
            systemMessage.put("IN_READ_FLAG", view_type);
            systemMessage.put("IN_SOURCE", request.getParameter("source_type"));
            dataRequest.setInputParams(systemMessage);
            DOMUtil.addSection(responseDocument, ((Document) dataAccessUtil.execute(dataRequest)).getChildNodes()); 
            
            //set READ flag to 'Y' for all NEW messages that were just
            //retrieved in the stored proc call above
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection); 
            systemMessage = new HashMap();
            systemMessage.put("IN_SOURCE", request.getParameter("source_type"));
            systemMessage.put(STATUS_PARAM,"");
            systemMessage.put(MESSAGE_PARAM,"");
            dataRequest.setStatementID(UPDATE_SYSTEM_MESSAGES_READ);
            dataRequest.setInputParams(systemMessage);
  
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
            status = (String) outputs.get(STATUS_PARAM);
            if(status.equals("N"))
            {
                message = (String) outputs.get(MESSAGE_PARAM);
                throw new Exception(message);
            }

            //add security variables
            String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
            String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
            String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
            
            if(securityContext == null)
            {
              securityContext = "";
            }
            
            if(securityToken == null)
            {
              securityToken = "";
            }
            
            if (adminAction == null) 
            {
                adminAction = "";
            }
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
                                    
            File xslFile = new File(getServletContext().getRealPath("/xsl/systemMessages.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

     /**
     * Delete the system messages from the SYSTEM_MESSAGES table
     * in the FRP database schema.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void deleteSystemMessages(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
                       
        try
        {
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                                        
            dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            Enumeration enu = request.getParameterNames();
            String param = "";
            String id = "";

            while(enu.hasMoreElements())
            {     
                param = (String) enu.nextElement();
                if(param.startsWith("system_message_id"))
                {
                  id = param.substring(17);  
                  HashMap systemMessage = new HashMap();
                  systemMessage.put("IN_SYSTEM_MESSAGE_ID", new Integer(id));
                  systemMessage.put(STATUS_PARAM,"");
                  systemMessage.put(MESSAGE_PARAM,"");
                  dataRequest.setStatementID(DELETE_SYSTEM_MESSAGES);
                  dataRequest.setInputParams(systemMessage);
  
                  Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
                  status = (String) outputs.get(STATUS_PARAM);
                  if(status.equals("N"))
                  {
                      message = (String) outputs.get(MESSAGE_PARAM);
                      throw new Exception(message);
                  }
                }
            } 

            this.loadSystemMessages(request, response);
        } 
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
}
