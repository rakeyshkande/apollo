package com.ftd.osp.opsadministration.presentation;


import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.UserTransaction;
import org.w3c.dom.Document;

import java.net.URL;
import java.net.URLDecoder;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.Node;

public class ProductUpdatesToNovator extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
  
  //security parameters
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String EXIT_PAGE_PROPERTY = "exitpage";
    
  private Logger logger;

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
    logger = new Logger("com.ftd.osp.opsadministration.presentation.ProductUpdatesToNovator");
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request,response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    String logonPage = null;
    String exitPage = null;
    String action = null;

    try{
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
      exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);
      //action = request.getParameter("action");
      
      if (   configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") 
          || ServletHelper.isValidToken(request)) 
      {
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

        if(securityContext == null)
        {
          securityContext = "";
        }
        
        if(securityToken == null)
        {
          securityToken = "";
        }
        
        if (adminAction == null) 
        {
            adminAction = "";
        }  
        
        //http://{$sitename}/pdb/showProductHolidayList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}
        StringBuffer sb = new StringBuffer();
        sb.append(configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, "novator_update_url"));
        sb.append("?context=");
        sb.append(securityContext);
        sb.append("&adminAction=");
        sb.append(adminAction);
        sb.append("&securitytoken=");
        sb.append(securityToken);
        logger.debug("Redirecting to: " + sb.toString());
        response.sendRedirect(sb.toString());
      }
      else
      {
        //failed logon
        logger.error("Logon Failed");
        ServletHelper.redirectToLogin(request, response);
        return;         
      }
    } 
    catch(Exception e)
    {
        logger.error(e);
    }
  }
}
