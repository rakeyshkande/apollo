package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.TransientCacheHandler;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class helps test and check the heartbeat of the caching mechanism and the CacheMgr threads.
 */
public class CacheManagerStatsServlet extends HttpServlet 
{

    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private Logger logger;

    //security parameters
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String HTML_PARAM_CACHE_COMMAND = "command";
    public static final String HTML_PARAM_CACHE_NAME = "cachename";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.CacheManagerStatsServlet");
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        //this.testCacheMgr(request, response);
        this.dumpCacheMgrStats(request, response);
    }


     /**
     * Dump current statistics of Cache Manager
     */
    private void dumpCacheMgrStats(HttpServletRequest request, HttpServletResponse response)
    {
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        String command = request.getParameter(HTML_PARAM_CACHE_COMMAND);
        String cachename = request.getParameter(HTML_PARAM_CACHE_NAME);
        
        if(securityContext == null)
        {
          securityContext = "";
        }
        
        if(securityToken == null)
        {
          securityToken = "";
        }

        if (adminAction == null) 
        {
            adminAction = "";
        }

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {        
            try
            {

                // Create the initial document
                Document responseDocument = (Document) DOMUtil.getDocument();

                // Dump all CacheVO data            
                CacheManager cm = CacheManager.getInstance();
                Document dumpDoc = cm.dumpAllCaches();
                Document dumpTransientDoc = null;

                // Dump specific TransientCache data (if specified)
                if ("getTransient".equals(command) && cachename != null) {
                    dumpTransientDoc = cm.dumpTransientCache(cachename);
                }
                
                //add security variables
                HashMap securityParams = new HashMap();
                securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
                securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
                securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
                DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss,SSS");
                String curtime = (new StringBuilder(df.format(new Date())).toString());
                HashMap timemap = new HashMap();
                timemap.put("Current time: ", curtime);
                DOMUtil.addSection(responseDocument, "time", "curtime", timemap, true);
                
                if (dumpDoc != null) {
                    DOMUtil.addSection(responseDocument, dumpDoc.getChildNodes());
                }
                if (dumpTransientDoc != null) {
                    DOMUtil.addSection(responseDocument, dumpTransientDoc.getChildNodes());
                }

                File xslFile = new File(getServletContext().getRealPath("/xsl/cacheManagerStats.xsl"));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
            } 
            catch(Exception e)
            {
                logger.error(e);
            }
            finally
            {
            }//end finally
          }//end if
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
    }
}