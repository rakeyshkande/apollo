package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This class retrieves results from MERCURY_EAPI_OPS table.
 * These results are passed to eapi_dashboard.xsl
 * for display and editing, if applicable.
 *
 */
public class EAPIDashboardServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private static final String VIEW_EAPI_DASHBOARD_COUNTS = "VIEW_EAPI_DASHBOARD_COUNTS";
    private static final String VIEW_MERCURY_EAPI_COUNTS = "VIEW_MERCURY_EAPI_COUNTS";
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage"; 

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.EAPIDashboardServlet");
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      this.loadEAPIDashboard(request, response);
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadEAPIDashboard(request, response);
    }

     /**
     * Retrieve the MERCURY_EAPI_OPS records and open MERCURY records from the database.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadEAPIDashboard(HttpServletRequest request, HttpServletResponse response)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
        
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }

        if(securityToken == null)
        {
          securityToken = "";
        }

        if (adminAction == null) 
        {
            adminAction = "";
        }

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
                
            dataRequest.setConnection(connection);
            // get MERCURY_EAPI_OPS statistics
            dataRequest.setStatementID(VIEW_EAPI_DASHBOARD_COUNTS);
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
            // get MERCURY recrods where MERCURY_STATUS = 'MO'
            dataRequest.setStatementID(VIEW_MERCURY_EAPI_COUNTS);
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
            
            File xslFile = new File(getServletContext().getRealPath("/xsl/eapi_dashboard.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

  }
