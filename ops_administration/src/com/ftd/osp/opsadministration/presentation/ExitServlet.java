package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to bring a user back to the Systems
 * Administration page when they click on an exit button
 * on any of the pages in Operations Administration.
 */
public class ExitServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";
    public static final String ADMIN_ACTION = "ADMIN_ACTION";



    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.ExitServlet");
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.exitOpsAdmin(request, response);
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.exitOpsAdmin(request, response);
    }

     /**
     * Send user back to System Administrations Page.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void exitOpsAdmin(HttpServletRequest request, HttpServletResponse response)
    {
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }
        
        if(securityToken == null)
        {
          securityToken = "";
        }
        
        if (adminAction == null) 
        {
            adminAction = "";
        }
        
        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
           try
            {
               logger.debug("page going to: " + exitPage + "?adminAction=" + adminAction + "&context=" + securityContext + "&securitytoken=" + securityToken );
               response.sendRedirect(exitPage + "?adminAction=" + adminAction + "&context=" + securityContext + "&securitytoken=" + securityToken);
            } 
            catch(Exception e)
            {
                logger.debug(e);
            }
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            logger.debug("page going to: " + logonPage);
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        catch(Exception e)
        {
            logger.debug(e);
        }
           
    }
}