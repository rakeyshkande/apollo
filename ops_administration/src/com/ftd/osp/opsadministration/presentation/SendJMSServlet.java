package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.mercuryinterface.vo.*;
import com.ftd.osp.mercuryinterface.dao.MercuryDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import java.util.Timer;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import javax.naming.InitialContext;

/**
 * This class retrieves the counts of the unscrubbed carts,
 * pending carts and removed carts and sends these counts
 * to the dashboard.xsl page to be displayed.  This will
 * provide a view of the current activity that is
 * occurring in Order Scrub.
 *
 * @author Rose Lazuk
 */
public class SendJMSServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private static final String VIEW_DASHBOARD_COUNTS = "VIEW_BOXX_DASHBOARD_COUNTS";
    private static final String DELETE_QUEUE_MESSAGES = "DELETE_MERCURY_QUEUE_CORRID";
    
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage"; 

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.SendJMSServlet");
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      String inValue = request.getParameter("sendJMSvalue");
      logger.info("Sending JMS message for suffix " + inValue);
      
      Connection conn = null;
      Connection eventConn = null;
      
      try {
        Document responseDocument = DOMUtil.getDocument();
        HashMap resultParams = new HashMap();
        
        //make a dao & datarequest object
        conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();            
        dataRequest.setConnection(conn);
        MercuryDAO dao = new MercuryDAO(conn);                  
      
        //get initial context.  Need for JMS messaging
        InitialContext context = null;
        context = new InitialContext();
        
        //retrieve mercury ops for this suffix to see if it is available
        MercuryOpsVO vo = dao.getMercuryOpsBySuffix(inValue);            
            
        //mark suffix unavailable if it is not already unavailable
        boolean wasAvailable = vo.isAvailable();
        if(vo.isAvailable())
        {
            vo.setAvailable(false);
            dao.updateMercuryOPS(vo);
        }
        
        //pause to allow any current processes to stop running
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        int waitSeconds = Integer.parseInt(configUtil.getProperty(OPS_ADMIN_CONFIG_FILE,"JMS_WAIT_SECONDS"));
        Calendar endDelayTime = Calendar.getInstance();               
        Calendar rightNow = Calendar.getInstance();
        while(rightNow.before(endDelayTime))
        {
            rightNow = Calendar.getInstance();
            System.out.println("Now:" + rightNow.getTime().toString() + " || " + "Delay:" + endDelayTime.getTime().toString());
        }

        //delete all JMS messages for this suffix
        eventConn = DataSourceUtil.getInstance().getConnection("EVENTS_Q");
        DataRequest eventRequest = new DataRequest();
        eventRequest.setConnection(eventConn);
        eventRequest.setStatementID(DELETE_QUEUE_MESSAGES);
        HashMap params = new HashMap();
        params.put("IN_CORRID", inValue);
        eventRequest.setInputParams(params);
        DataAccessUtil.getInstance().execute(eventRequest); 
        
        //if suffix was available before this process started, then make in available again    
        if(wasAvailable)
        {
            vo.setAvailable(true);
            dao.updateMercuryOPS(vo);
        }
          
        //send JMS message
        MessageToken token = new MessageToken();
        token.setMessage(inValue);
        //token.setProperty(MercuryConstants.JMS_DELAY_PROPERTY,jmsEFOSdelay ,"int");
        CommonUtilites.sendJMSMessage(context,token);   
        resultParams.put(inValue,"JMS Message Sent");   
        DOMUtil.addSection(responseDocument, "resultParams", "params", resultParams, true);
        StringWriter s = new StringWriter();
        DOMUtil.print(responseDocument, new PrintWriter(s));
        String xmlString = s.toString();
        System.out.println(s);

        File xslFile = new File(getServletContext().getRealPath("/xsl/sendJMSResults.xsl"));
        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
      }
      catch(Exception e)
      {
        logger.error(e);
      }
      finally
      {
          if(conn != null)
          {
            try{
              conn.close();
            }
            catch(Exception e)
            {              
            }
          }
          if(eventConn != null)
          {
            try{
              eventConn.close();
            }
            catch(Exception e)
            {              
            }
          }
      }
    }

  }
