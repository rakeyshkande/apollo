package com.ftd.osp.opsadministration.presentation;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map; 
import java.util.StringTokenizer;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.opsadministration.bo.ResendPartnerFeedBO;
import com.ftd.osp.opsadministration.dao.SecureConfigDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * @author rksingh
 * 
 */

@SuppressWarnings("serial")
public class ResendPartnerFeedServlet extends HttpServlet {

	private Logger logger = new Logger(
			"com.ftd.osp.opsadministration.presentation.ResendPartnerFeedServlet");
	@SuppressWarnings("unused")
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";
	private Connection conn = null;

	// Data source name
	private static final String DATA_SOURCE = "ORDER SCRUB";

	// Security parameters
	private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
	private static final String HTML_PARAM_SECURITY_CONTEXT = "context";
	private static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
	private static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
	private static final String SECURITY_OFF = "SECURITY_OFF";
	private static final String TRUE = "true";

	/**
	 * init.
	 * 
	 * @param config
	 * @exception Servlet
	 *                Exception
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * Retrieves security parameters.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return Hashmap of security parameters
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap getSecurity(HttpServletRequest request) {
		// Retrieve security variables
		String securityContext = request
				.getParameter(HTML_PARAM_SECURITY_CONTEXT);
		String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
		String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

		if (securityContext == null) {
			securityContext = "";
		}

		if (securityToken == null) {
			securityToken = "";
		}

		if (adminAction == null) {
			adminAction = "";
		}

		// Add security variables
		HashMap securityParams = new HashMap();
		securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
		securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
		securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);

		return securityParams;
	}

	/**
	 * Obtains required for the initial load of the resendPartnerfeeds.xsl page
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @exception ServletException
	 * @exception IOException
	 */
public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			// Get the connection
			conn = DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

			// Create root document
			Document dataDocument = DOMUtil.getDocument();

			// Test for valid securitytoken
			
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF)
					.equals(TRUE) && !ServletHelper.isValidToken(request)) {

				// Failed log on
				logger.error("Logon failed for security token: "
						+ request.getParameter(HTML_PARAM_SECURITY_TOKEN));
				ServletHelper.redirectToLogin(request, response);
				return;
			}

			// Load the partners to the document 
			List<PartnerMappingVO> partnersInfo = new PartnerUtility()
					.getAllPartnersMappingInfo();
			Map<String, String> partners = null;
			if (partnersInfo != null && partnersInfo.size() != 0) {
				partners = new HashMap<String, String>();
				for (PartnerMappingVO partnerMappingVO : partnersInfo) {
					if(partnerMappingVO.isDefaultBehaviour()){
					partners.put(partnerMappingVO.getPartnerId(),
							partnerMappingVO.getPartnerName());
					}
				}

				DOMUtil.addSection(dataDocument, "PartnerList", "Partner",
						(HashMap<String, String>) partners, true);
			}
			
//			//Load the Global Params and add to the document		
			
			Map<String, Integer> globalCount = new HashMap<String, Integer>();
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String MaxPartnerOrderNumber = cu.getFrpGlobalParm("PI_CONFIG", "PARTNER_ORDER_MAX_COUNT");
			globalCount.put("Maximum Count for Partner Order Numbers",Integer.parseInt(MaxPartnerOrderNumber));
			DOMUtil.addSection(dataDocument, "data", "PageData",(HashMap<String, Integer>) globalCount, true);

			// Retrieve all Secure System Configuration properties
			SecureConfigDAO scDAO = new SecureConfigDAO(conn);
			Document secureData = scDAO.loadContexts();

			// Add contexts data to the root document
			DOMUtil.addSection(dataDocument, secureData.getChildNodes());

			// Add security variables to the root document
			DOMUtil.addSection(dataDocument, "securityParams", "params",
					getSecurity(request), true);

			// Get the Parameter from xls
			
			logger.info("Username --->" + ServletHelper.getUserId(request));
			
			String partnerFeedType = request.getParameter("feedType");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String partnerId = request.getParameter("partnerId");

			List<String> orderList = getOrderListFromString(request
					.getParameter("p_partner_order_number"));

			// calling  the BO 
			
				if (partnerFeedType != null && startDate != null
						&& !startDate.isEmpty() && endDate != null
						&& !endDate.isEmpty() && partnerId != null
								&& !partnerId.isEmpty()) {
					
					Map<String, String> updateStatusMap = new ResendPartnerFeedBO()
					.updatePartnerFeedBydate(partnerFeedType, startDate, endDate, partnerId ,ServletHelper.getUserId(request));

					DOMUtil.addSection(dataDocument, "DateMessage",
							"UpdateStatusMessage",
							(HashMap<String, String>) updateStatusMap, true);
				}else if (partnerFeedType != null && partnerId != null
						&& !partnerId.isEmpty() && orderList != null
						&& orderList.size() > 0) {
					Map<String, String> updateStatusMap = new ResendPartnerFeedBO()
							.updatePartnerFeedByPartnerOrdNum(orderList,
									partnerFeedType, partnerId,
									ServletHelper.getUserId(request));

					DOMUtil.addSection(dataDocument, "OrderNumberMessage",
							"UpdateStatusMessage",
							(LinkedHashMap<String, String>) updateStatusMap, true);
				}
	

		// 	logger.info("Data response document: "
		//			+ JAXPUtil.toString(dataDocument));
				
			File xslFile = new File(getServletContext().getRealPath(
					"/xsl/resendPartnerfeeds.xsl"));
			TraxUtil.getInstance().transform(request, response, dataDocument,
					xslFile, null);

		} catch (Exception e) {
			logger.error(e);
			
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				logger.error(se);
			}
		}
	}

	private List<String> getOrderListFromString(String partnerOrderString) {
		List<String> orderList = new ArrayList<String>();

		if (partnerOrderString != null) {
			StringTokenizer st;
			if (partnerOrderString.contains(",")) {
				partnerOrderString = partnerOrderString.replace(",", "\r\n");
			}
			st = new StringTokenizer(partnerOrderString.toString(), "\r\n");

			while (st.hasMoreElements()) {
				orderList.add(st.nextElement().toString().trim());
			}

		
			
		}
		
		return orderList;
	}

}
