package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.net.URL;
import java.net.URLDecoder;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.transaction.UserTransaction;

import javax.xml.xpath.XPath;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * This class retrieves the master order number, guid and
 * timestamp of all orders in frp with status of 1013.
 *
 * @author Rose Lazuk
 */
public class ReinstateOrderServlet extends HttpServlet {
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private static final String VIEW_REINSTATE_ORDERS = "SCRUB.VIEW_REINSTATE_ORDERS";
  private static final String VIEW_REINSTATE_ORDERS_CONTEXT = "SCRUB.VIEW_REINSTATE_ORDERS_CONTEXT";
  private static final String VIEW_REINSTATE_ORDERS_STATUS = "SCRUB.VIEW_REINSTATE_ORDERS_STATUS";
  private static final String VIEW_MISC_ORDER_STATS = "VIEW_MISC_ORDER_STATS";
  private static final String DELETE_ORDER = "SCRUB.DELETE_ORDER_EXCEPTIONS";
  private static final boolean SEND_JMS_MSG = true;
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
  private final static String WEBOE_ORDER_TEXT_MESSAGE = "weboe-order-text-message.xml";

  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

  //security parameters
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";
  public static final String EXIT_PAGE_PROPERTY = "exitpage";
  private Logger logger;
  private Document weboeOrderTextMessage;

  /**
   * init.
   * @param config
   * @exception Servlet Exception
   */
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    logger = new Logger("com.ftd.osp.opsadministration.presentation.ReinstateOrderServlet");
    loadWebOEConfig();
  }

  /**
   * doPost.
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @exception ServletException
   * @exception IOException
   *
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    response.setContentType(CONTENT_TYPE);

    String logonPage = null;
    String exitPage = null;
    String action = null;

    try {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, LOGON_PAGE_PROPERTY);
      exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, EXIT_PAGE_PROPERTY);
      action = request.getParameter("action");

      if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) {
        if (action != null) {
          if (action.equals("refresh")) {
            this.loadReinstateOrder(request, response);
          } else if (action.equals("reinstate")) {
            this.reinstateOrder(request, response);
          } else if (action.equals("removeEntry")) {
            this.removeEntry(request, response);
          } else if (action.equals("popupOrder")) {
            this.loadPopupOrder(request, response);
          }
        } else {
          this.loadReinstateOrder(request, response);
        }
      } else {
        //failed logon
        logger.error("Logon Failed");
        ServletHelper.redirectToLogin(request, response);

        return;
      }
    } catch (Exception e) {
      logger.error(e);
    }
  }

  /**
   * doGet.
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @exception ServletException
   * @exception IOException
   *
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    this.doPost(request, response);
  }

  /**
  * Retrieve the master order number, guid and
  * timestamp of all orders that have status code
  * of 1013 in scrub.
  * @param request HttpServletRequest
  * @param response HttpServletResponse
  *
  */
  private void loadReinstateOrder(HttpServletRequest request, HttpServletResponse response) {
    Connection connection = null;
    DataRequest dataRequest = null;

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

    if (securityContext == null) {
      securityContext = "";
    }

    if (securityToken == null) {
      securityToken = "";
    }

    if (adminAction == null) {
      adminAction = "";
    }

    try {
      HashMap pageData = new HashMap();
      
      // Create the initial document
      Document responseDocument = DOMUtil.getDocument();

      // get database connection
      connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      dataRequest = new DataRequest();

      // Retrieve context currently selected within the dropdown
      String selContext = request.getParameter("selected_context");
      String selStatus = request.getParameter("selected_status");

      if (selContext == null || "".equals(selContext)) {
          selContext = null;
      } else {
          pageData.put("selected_context", selContext);
      }
      
      if (selStatus == null || "".equals(selStatus)) {
          selStatus = null;
      } else {
          pageData.put("selected_status", selStatus);
      }    
      
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(VIEW_REINSTATE_ORDERS_CONTEXT);
      DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());
      
      dataRequest.setStatementID(VIEW_REINSTATE_ORDERS_STATUS);
      DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

      dataRequest.addInputParam("IN_CONTEXT", selContext);
      dataRequest.addInputParam("IN_ORDER_STATUS", selStatus);

      dataRequest.setStatementID(VIEW_REINSTATE_ORDERS);
      DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

      //add security variables
      HashMap securityParams = new HashMap();
      securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
      securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
      securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);
      DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
 
      //add scrub url
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      pageData.put("scrub_url", configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT, "scrub_url"));
      DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
      
      if (logger.isDebugEnabled()) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DOMUtil.print(responseDocument, baos);
        logger.debug(baos.toString());
      }
      
      
      File xslFile = new File(getServletContext().getRealPath("/xsl/reinstate.xsl"));
      TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
    } catch (Exception e) {
      logger.error(e);
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException se) {
        logger.error(se);
      }
    }
  }


  /**
  * Retrieve order status information for popup window.
  * @param request HttpServletRequest
  * @param response HttpServletResponse
  *
  */
  private void loadPopupOrder(HttpServletRequest request, HttpServletResponse response) {
    Connection connection = null;
    DataRequest dataRequest = null;
        
    try {
        HashMap pageData = new HashMap();
        Document responseDocument = DOMUtil.getDocument();

        String orderGuid = request.getParameter("popupOrderGuid");
        
        // If passed guid looks like an order guid (as opposed to email guid),
        // then strip off any trailing item number and get associated order stats.
        // Otherwise just put guid alone on page.
        //
        if (orderGuid != null && (orderGuid.startsWith("FTD_GUID") || orderGuid.startsWith("ORD"))) {
           int endOfGuid = orderGuid.lastIndexOf("/");
           if (endOfGuid > 0) {
               orderGuid = orderGuid.substring(0,endOfGuid);
           }
           connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           dataRequest = new DataRequest();
           dataRequest.setConnection(connection);
           dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
           dataRequest.setStatementID(VIEW_MISC_ORDER_STATS);
           DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());
        }
        pageData.put("popupOrderGuid", orderGuid);
        DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
    
        File xslFile = new File(getServletContext().getRealPath("/xsl/reinstatePopup.xsl"));
        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
    } catch (Exception e) {
        logger.error(e);
    } finally {
        try {
          if (connection != null) {
            connection.close();
          }
        } catch (SQLException se) {
          logger.error(se);
        }
    }
  }


  /**
   * Reinstate the order.
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   *
   */
  private void reinstateOrder(HttpServletRequest request, HttpServletResponse response)
    throws Exception {
    ConfigurationUtil configUtil = null;
    String context = null;
    String guid = null;
    String jms_status = null;
    String userId = ServletHelper.getUserId(request);
    InitialContext initialContext = null;

    try {
      String[] guidArray = request.getParameterValues("guid");
      String[] contextArray = request.getParameterValues("ordercontext");

      if ((guidArray != null) && (contextArray != null)) {
        ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
        int transactionTimeout = Integer.parseInt((String) configurationUtil.getProperty("ops_admin_config.xml",
              "transaction_timeout_in_seconds"));

        // Retrieve the UserTransaction object
        String jndi_usertransaction_entry = configurationUtil.getProperty("ops_admin_config.xml", "jndi_usertransaction_entry");

        // get the initial context
        initialContext = new InitialContext();

        Dispatcher dispatcher = Dispatcher.getInstance();
        MessageToken token = null;

        for (int i = 0; i < guidArray.length; i++) {
          guid = guidArray[i];
          context = contextArray[i]; // both arrays are assumed to be equal in length
          jms_status = context.substring(context.indexOf('/') + 1, context.length());

          if (((guid != null) && (!guid.equals(""))) && ((context != null) && (!context.equals(""))) &&
              ((jms_status != null) && (!jms_status.equals("")))) {
            UserTransaction userTransaction = null;
            Connection connection = null;

            try {
              if (SEND_JMS_MSG) {

                logger.debug("guid: " + guid);
                logger.debug("context: " + context);
                logger.debug("jms_status: " + jms_status);

                userTransaction = (UserTransaction) initialContext.lookup(jndi_usertransaction_entry);
                logger.debug("User transaction obtained");

                userTransaction.setTransactionTimeout(transactionTimeout);
                logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");

                // Start the transaction with the begin method
                userTransaction.begin();
                logger.debug("********************BEGIN TRANSACTION***********************");

                // get database connection
                connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

                // record in shadow table that we attempted to reinstate the order //
                this.recordReinstateOrder(guid, userId, connection, true);

                String webOeJmsStatus = configurationUtil.getProperty("ops_admin_config.xml", "weboe_order_jms_status");
                String joeJmsStatus = configurationUtil.getProperty("ops_admin_config.xml", "joe_order_jms_status");

                if (jms_status.equals(webOeJmsStatus)) {
                  token = setWebOEOrderToken(guid, jms_status);
                  dispatcher.dispatchTextMessage(initialContext, token);
                } else if (jms_status.equalsIgnoreCase(joeJmsStatus)) {
                  token = new MessageToken();
                  token.setStatus("JOE Order Dispatcher");
                  token.setMessage(guid);
                  dispatcher.dispatchTextMessage(initialContext, token);
                } else if (jms_status.equalsIgnoreCase("MERCURYINBOUND")) {
                    token = new MessageToken();
                    token.setStatus(jms_status);
                    token.setMessage(guid);
                    dispatcher.dispatchTextMessage(initialContext, token);
                } 
                else {
                  token = new MessageToken();
                  token.setMessage(guid);
                  token.setStatus(jms_status);
                  dispatcher.dispatch(initialContext, token);
                }

                logger.debug("Order with a guid:" + guid + " re-instated. Status used:" + jms_status);
              }

              this.deleteRecord(context, guid, connection);

              //Assuming everything went well, commit the transaction.
              userTransaction.commit();
              logger.debug("User transaction committed");
              logger.debug("********************END TRANSACTION***********************");
            } catch (Throwable ex) {
              logger.error(ex);

              if (userTransaction != null) {
                // rollback the user transaction
                try {
                  userTransaction.rollback();
                  logger.error("User transaction rolled back");
                } catch (Exception ex2) {
                  logger.error(ex2);
                }
              }
            } finally {
              try {
                if ((connection != null) && (!connection.isClosed())) {
                  connection.close();
                }
              } catch (Exception ex) {
                logger.error(ex);
              }

              connection = null;
              userTransaction = null;
            }
          }
        } //End for loop
      } // end if
    } catch (Throwable ex) {
      //TODO this needs to be throw up and presented to the end user
      logger.error(ex);
    } finally {
      try {
        // close initial context
        if (initialContext != null) {
          initialContext.close();
        }
      } catch (Exception ex) {
        logger.error(ex);
      }
    }

    this.loadReinstateOrder(request, response);
  }



    /**
     * Remove order from reinstate order list (i.e., order is untouched, but
     * removed from order_exceptions table).
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     *
     */
    private void removeEntry(HttpServletRequest request, HttpServletResponse response)
      throws Exception {
      ConfigurationUtil configUtil = null;
      String context = null;
      String guid = null;
      String jms_status = null;
      String userId = ServletHelper.getUserId(request);
      InitialContext initialContext = null;
    
      try {
        String[] guidArray = request.getParameterValues("guid");
        String[] contextArray = request.getParameterValues("ordercontext");
    
        if ((guidArray != null) && (contextArray != null)) {
          ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
          int transactionTimeout = Integer.parseInt((String) configurationUtil.getProperty("ops_admin_config.xml",
                "transaction_timeout_in_seconds"));
    
          // Retrieve the UserTransaction object
          String jndi_usertransaction_entry = configurationUtil.getProperty("ops_admin_config.xml", "jndi_usertransaction_entry");
    
          // get the initial context
          initialContext = new InitialContext();
    
          Dispatcher dispatcher = Dispatcher.getInstance();
          MessageToken token = null;
    
          for (int i = 0; i < guidArray.length; i++) {
            guid = guidArray[i];
            context = contextArray[i]; // both arrays are assumed to be equal in length
            jms_status = context.substring(context.indexOf('/') + 1, context.length());
    
            if (((guid != null) && (!guid.equals(""))) && ((context != null) && (!context.equals(""))) &&
                ((jms_status != null) && (!jms_status.equals("")))) {
              UserTransaction userTransaction = null;
              Connection connection = null;
    
              try {
                if (SEND_JMS_MSG) {
                  userTransaction = (UserTransaction) initialContext.lookup(jndi_usertransaction_entry);
                  logger.debug("User transaction obtained");
    
                  userTransaction.setTransactionTimeout(transactionTimeout);
                  logger.debug("Transaction timeout set at " + transactionTimeout + " seconds");
    
                  // Start the transaction with the begin method
                  userTransaction.begin();
                  logger.debug("********************BEGIN TRANSACTION***********************");
    
                  // get database connection
                  connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
    
                  // record in shadow table that we simple removed the entry //
                  this.recordReinstateOrder(guid, userId, connection, false);
    
                  logger.debug("Order with a guid:" + guid + " was removed from order_exceptions. Status was:" + jms_status);
                }
    
                this.deleteRecord(context, guid, connection);
    
                //Assuming everything went well, commit the transaction.
                userTransaction.commit();
                logger.debug("User transaction committed");
                logger.debug("********************END TRANSACTION***********************");
              } catch (Throwable ex) {
                logger.error(ex);
    
                if (userTransaction != null) {
                  // rollback the user transaction
                  try {
                    userTransaction.rollback();
                    logger.error("User transaction rolled back");
                  } catch (Exception ex2) {
                    logger.error(ex2);
                  }
                }
              } finally {
                try {
                  if ((connection != null) && (!connection.isClosed())) {
                    connection.close();
                  }
                } catch (Exception ex) {
                  logger.error(ex);
                }
    
                connection = null;
                userTransaction = null;
              }
            }
          } //End for loop
        } // end if
      } catch (Throwable ex) {
        //TODO this needs to be throw up and presented to the end user
        logger.error(ex);
      } finally {
        try {
          // close initial context
          if (initialContext != null) {
            initialContext.close();
          }
        } catch (Exception ex) {
          logger.error(ex);
        }
      }
    
      this.loadReinstateOrder(request, response);
    }



  /**
     * Record the reinstate the order in a shadow table.
     * If reinstatedFlag is true then order was reinstated, otherwise entry
     * was simply removed from order_exceptions table.
     */
  private void recordReinstateOrder(String orderGuid, String createdBy, Connection connection, boolean reinstatedFlag)
    throws Exception {
    DataRequest dataRequest = null;

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    dataRequest = new DataRequest();
    dataRequest.setConnection(connection);

    HashMap systemMessage = new HashMap();
    systemMessage.put("IN_ORDER_GUID", orderGuid);
    systemMessage.put("IN_CREATED_BY", createdBy);
    systemMessage.put("IN_REPROCESSED_FLAG", (reinstatedFlag?"Y":"N"));
    dataRequest.setStatementID("INSERT_REPROCESS_ORDERS");
    dataRequest.setInputParams(systemMessage);

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");

    if (status.equals("N")) {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
    * Delete record from the FRP system if the previous order status
    * was 1001.
    * @param guid String
    * @param connection Connection
    * @exception Exception
    */
  private void deleteRecord(String context, String guid, Connection connection)
    throws Exception {
    DataRequest dataRequest = null;
    OrderVO orders = null;

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    dataRequest = new DataRequest();
    dataRequest.setConnection(connection);

    HashMap systemMessage = new HashMap();
    systemMessage.put("IN_CONTEXT", context);
    systemMessage.put("IN_ORDER_GUID", guid);
    systemMessage.put(STATUS_PARAM, "");
    systemMessage.put(MESSAGE_PARAM, "");
    dataRequest.setStatementID(DELETE_ORDER);
    dataRequest.setInputParams(systemMessage);

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get(STATUS_PARAM);

    if (status.equals("N")) {
      String message = (String) outputs.get(MESSAGE_PARAM);
      throw new Exception(message);
    }
  }

  private void loadWebOEConfig() {
    InputStream inputStream = ResourceUtil.getInstance().getResourceFileAsStream(WEBOE_ORDER_TEXT_MESSAGE);

    if (inputStream == null) {
      logger.error("The configuration file:" + WEBOE_ORDER_TEXT_MESSAGE + " was not found.");
    }

    try {
      weboeOrderTextMessage = (Document) DOMUtil.getDocument(inputStream);
    } catch (Exception e) {
      logger.error("Unable to parse file:" + WEBOE_ORDER_TEXT_MESSAGE, e);
    }
  }

  private MessageToken setWebOEOrderToken(String guid, String jms_status)
    throws Exception {
    MessageToken token = new MessageToken();

    Element eventContextElement = ((Element) DOMUtil.selectSingleNode(weboeOrderTextMessage, "//event/context"));
    Node eventContextNode = eventContextElement.getFirstChild();

    Element eventNameElement = ((Element) DOMUtil.selectSingleNode(weboeOrderTextMessage, "//event/event-name"));
    Node eventNameNode = eventNameElement.getFirstChild();

    Element payloadElement = ((Element) DOMUtil.selectSingleNode(weboeOrderTextMessage, "//event/payload"));
    Node payloadNode = payloadElement.getFirstChild();

    String correlationID = eventContextNode.getNodeValue() + " " + eventNameNode.getNodeValue();
    token.setJMSCorrelationID(correlationID);

    token.setJMSExpiration(0);
    token.setStatus(jms_status);

    payloadNode.setNodeValue(guid);

    StringWriter s = new StringWriter();
    DOMUtil.print(weboeOrderTextMessage, new PrintWriter(s));

    String xmlMessage = s.toString();

    token.setMessage(xmlMessage);

    return token;
  }
  

}
