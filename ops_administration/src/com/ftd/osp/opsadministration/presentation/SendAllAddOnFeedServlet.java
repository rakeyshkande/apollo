/**
 * 
 */
package com.ftd.osp.opsadministration.presentation;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.opsadministration.dao.SecureConfigDAO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SendUpdateFeed;
import com.ftd.osp.utilities.StoreFeedUtil;
import com.ftd.osp.utilities.feed.INovatorFeedTransformer;
import com.ftd.osp.utilities.feed.NovatorFeedTransformerAddOnImpl;
import com.ftd.osp.utilities.feed.vo.INovatorFeedDataVO;
import com.ftd.osp.utilities.feed.vo.NovatorFeedDataAddOnVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.util.ServletHelper;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;

/**
 * @author bsurimen This servlet is being used to send all the add-ons updates
 *         from Apollo to Store at one shot as feed. It will insert all the
 *         add-ons as feeds in store_feeds table.
 *
 */
public class SendAllAddOnFeedServlet extends HttpServlet {

	/**
	 * 
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = -1641019897671943157L;
	/**
	 * 
	 */

	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
	private static final String HTML_PARAM_SECURITY_CONTEXT = "context";
	private static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
	private static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
	private static final String SECURITY_OFF = "SECURITY_OFF";
	private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
	private static final String DATA_SOURCE = "ORDER SCRUB";
	private static final String TRUE = "true";

	private static final String ADD_ON = "add_on";
	private static final String ADD_ONS = "add_ons";
	private static final String ADD_ON_ID = "add_on_id";
	private static final String ADD_ON_DESCRIPTION = "add_on_description";
	private static final String ADD_ON_TYPE_ID = "add_on_type_id";
	private static final String ADD_ON_TYPE_DESCRIPTION = "add_on_type_description";
	private static final String PRICE = "price";
	private static final String ADD_ON_TEXT = "add_on_text";
	private static final String UNSPSC = "unspsc";
	private static final String AVAILABLE_FLAG = "available_flag";
	private static final String DEFAULT_PER_TYPE_FLAG = "default_per_type_flag";
	private static final String DISPLAY_PRICE_FLAG = "display_price_flag";
	private static final String DELIVERY_TYPE = "delivery_type";

	private static final String ADDON_FEEDS_SUCCESS_MSG = "All the Add-Ons Feeds are Successfully generated.";
	private static final String ADDON_FEEDS_FAILURE_MSG = "Error occured during Add-Ons feed generation.Please contact Production Support immediately";

	private static final String FLORAL = "Floral";
	private static final String VENDOR = "Vendor";
	private static final String FLORALORVENDOR = "FloralorVendor";
	
	// Global parameters for Novator environment IP and ports
    private static final String NOVATOR_PRODUCTION_FEED_IP = "production_feed_ip";
    private static final String NOVATOR_PRODUCTION_FEED_PORT = "production_feed_port";
    private static final String NOVATOR_CONTENT_FEED_IP = "content_feed_ip";
    private static final String NOVATOR_CONTENT_FEED_PORT = "content_feed_port";
    private static final String NOVATOR_TEST_FEED_IP = "test_feed_ip";
    private static final String NOVATOR_TEST_FEED_PORT = "test_feed_port";
    private static final String NOVATOR_UAT_FEED_IP = "uat_feed_ip";
    private static final String NOVATOR_UAT_FEED_PORT = "uat_feed_port";
    private static final String NOVATOR_FEED_TIMEOUT = "feed_timeout";
    
    // Global parameters for which Novator environments are enabled 
    public static final String NOVATOR_CONTENT_FEED_ALLOWED  = "content_feed_allowed";
    public static final String NOVATOR_PRODUCTION_FEED_ALLOWED  = "production_feed_allowed";
    public static final String NOVATOR_TEST_FEED_ALLOWED  = "test_feed_allowed";
    public static final String NOVATOR_UAT_FEED_ALLOWED  = "uat_feed_allowed";
    public static final String NOVATOR_CONTENT_FEED_CHECKED  = "content_feed_checked";
    public static final String NOVATOR_PRODUCTION_FEED_CHECKED  = "production_feed_checked";
    public static final String NOVATOR_TEST_FEED_CHECKED  = "test_feed_checked";
    public static final String NOVATOR_UAT_FEED_CHECKED  = "uat_feed_checked";
    
    // Novator environments
    public static final String CONTENT = "content";
    public static final String PRODUCTION = "production";
    public static final String TEST = "test";
    public static final String UAT = "uat";
    
    // Context for Novator global parameters
    private static final String NOVATOR_CONFIG = "NOVATOR_CONFIG";
    private NovatorFeedUtil feedUtil = new NovatorFeedUtil();
    

	private Connection conn = null;

	private Logger logger;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger = new Logger(
				"com.ftd.osp.opsadministration.presentation.SendAllAddOnFeedServlet");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private HashMap getSecurity(HttpServletRequest request) throws Exception {
		// Retrieve security variables
		String securityContext = request
				.getParameter(HTML_PARAM_SECURITY_CONTEXT);
		String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
		String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

		if (securityContext == null) {
			securityContext = "";
		}

		if (securityToken == null) {
			securityToken = "";
		}

		if (adminAction == null) {
			adminAction = "";
		}

		// Add security variables
		HashMap securityParams = new HashMap();
		securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
		securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
		securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);

		return securityParams;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Document dataDocument = null;
		response.setContentType(CONTENT_TYPE);
		SecureConfigDAO scDAO = null;
		Document secureData = null;
		String sendTo = request.getParameter("send_to");
		
		try {

			conn = DataSourceUtil.getInstance().getConnection(DATA_SOURCE);
			dataDocument = DOMUtil.getDocument();

			// Retrieve all Secure System Configuration properties
			scDAO = new SecureConfigDAO(conn);
			secureData = scDAO.loadContexts();
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF)
					.equals(TRUE)
					&& !com.ftd.osp.opsadministration.utilities.ServletHelper
							.isValidToken(request)) {

				// Failed log on
				logger.error("Logon failed for security token: "
						+ request.getParameter(HTML_PARAM_SECURITY_TOKEN));
				ServletHelper.redirectToLogin(request, response);
				return;
			}

			DOMUtil.addSection(dataDocument, secureData.getChildNodes());
			// Add security variables to the root document
			DOMUtil.addSection(dataDocument, "securityParams", "params",
					getSecurity(request), true);

			insertAllAddOnFeeds(sendTo);
			
			HashMap<String, String> updateStatusMap = new HashMap<String, String>();
			updateStatusMap.put("addonStatus", ADDON_FEEDS_SUCCESS_MSG );

			DOMUtil.addSection(dataDocument, "Addon",	"StatusMessage", updateStatusMap, true);
			
			File xslFile = new File(getServletContext().getRealPath("/xsl/opsAdmin.xsl"));
			TraxUtil.getInstance().transform(request, response, dataDocument,xslFile, null);		
			

		} catch (Exception e) {
			logger.error("Exception in SendAllAddOnFeedServletToStore " + e.getMessage());
			logger.error(e);
			
			HashMap<String, String> updateStatusMap = new HashMap<String, String>();
			updateStatusMap.put("addonStatus", ADDON_FEEDS_FAILURE_MSG );

			DOMUtil.addSection(dataDocument, "Addon","StatusMessage", updateStatusMap, true);
			
			File xslFile = new File(getServletContext().getRealPath("/xsl/opsAdmin.xsl"));
			try {
				TraxUtil.getInstance().transform(request, response, dataDocument,xslFile, null);
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}

	}

	private void insertAllAddOnFeeds(String sendTo) throws Exception {
		AddOnUtility addOnUtil = new AddOnUtility();
		HashMap<String, ArrayList<AddOnVO>> addOnMap = addOnUtil
				.getAllAddonList(false, false, conn);
		ArrayList<AddOnVO> totalAddOnList = new ArrayList<AddOnVO>();

		totalAddOnList = addOnMap.get(AddOnVO.ADD_ON_VO_ADD_ON_KEY);
		totalAddOnList.addAll(addOnMap.get(AddOnVO.ADD_ON_VO_VASE_KEY));
		totalAddOnList.addAll(addOnMap.get(AddOnVO.ADD_ON_VO_BANNER_KEY));
		totalAddOnList.addAll(addOnMap.get(AddOnVO.ADD_ON_VO_CARD_KEY));

		StoreFeedUtil storeFeedUtil = new StoreFeedUtil();

		logger.info("Number of Add-on Records " + totalAddOnList.size()
				+ " Identified to send as Feed.");

		try {
			
				
				Element node = null;
				Document doc = JAXPUtil.createDocument();
				Element root = doc.createElement(ADD_ONS);
				doc.appendChild(root);
				
				
			for (AddOnVO addOnVO : totalAddOnList) {
				
				Element subRoot = doc.createElement(ADD_ON);
				root.appendChild(subRoot);

				node = JAXPUtil.buildSimpleXmlNode(doc, ADD_ON_ID,
						addOnVO.getAddOnId());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNodeCDATA(doc,
						ADD_ON_DESCRIPTION, addOnVO.getAddOnDescription());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNode(doc, ADD_ON_TYPE_ID,
						addOnVO.getAddOnTypeId());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNode(doc,
						ADD_ON_TYPE_DESCRIPTION,
						addOnVO.getAddOnTypeDescription());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNode(doc, PRICE,
						addOnVO.getAddOnPrice());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNodeCDATA(doc, ADD_ON_TEXT,
						addOnVO.getAddOnText());
				subRoot.appendChild(node);
				node = JAXPUtil.buildSimpleXmlNode(doc, UNSPSC,
						addOnVO.getAddOnUNSPSC());
				subRoot.appendChild(node);
				if (addOnVO.getAddOnAvailableFlag() == true) {
					node = JAXPUtil
							.buildSimpleXmlNode(doc, AVAILABLE_FLAG, "Y");
				} else {
					node = JAXPUtil
							.buildSimpleXmlNode(doc, AVAILABLE_FLAG, "N");
				}
				subRoot.appendChild(node);

				if (addOnVO.getDefaultPerTypeFlag() == true) {
					node = JAXPUtil.buildSimpleXmlNode(doc,
							DEFAULT_PER_TYPE_FLAG, "Y");
				} else {
					node = JAXPUtil.buildSimpleXmlNode(doc,
							DEFAULT_PER_TYPE_FLAG, "N");
				}
				subRoot.appendChild(node);

				if (addOnVO.getDisplayPriceFlag() == true) {
					node = JAXPUtil.buildSimpleXmlNode(doc, DISPLAY_PRICE_FLAG,
							"Y");
				} else {
					node = JAXPUtil.buildSimpleXmlNode(doc, DISPLAY_PRICE_FLAG,
							"N");
				}

				subRoot.appendChild(node);

				if ("florist".equals(addOnVO.getDeliveryType())) {
					node = JAXPUtil.buildSimpleXmlNode(doc, DELIVERY_TYPE,
							FLORAL);
				} else if ("dropship".equals(addOnVO.getDeliveryType())) {
					node = JAXPUtil.buildSimpleXmlNode(doc, DELIVERY_TYPE,
							VENDOR);
				} else {
					node = JAXPUtil.buildSimpleXmlNode(doc, DELIVERY_TYPE,
							FLORALORVENDOR);
				}

				subRoot.appendChild(node);
				
				
			}
			
			if(sendTo.equals("store")){
				logger.info("Sending Addon feed to store");
				storeFeedUtil.saveStoreFeed(JAXPUtil.toStringNoFormat(doc));
			}else if(sendTo.equals("novator")){
				
				List<String> envKeys = feedUtil.getNovatorEnvironmentsAllowedAndChecked();
				String env = envKeys.get(0);
				logger.info("Sending Addon feed to novator : "+env);
				String xml = JAXPUtil.toStringNoFormat(doc);
				
				if (env.equals(CONTENT)) {
	                sendNovatorXML(xml, NOVATOR_CONTENT_FEED_IP, NOVATOR_CONTENT_FEED_PORT);
	            }
	            if (env.equals(PRODUCTION)) {
	                sendNovatorXML(xml, NOVATOR_PRODUCTION_FEED_IP, NOVATOR_PRODUCTION_FEED_PORT);
	            }
	            if (env.equals(TEST)) {
	                sendNovatorXML(xml, NOVATOR_TEST_FEED_IP, NOVATOR_TEST_FEED_PORT);
	            }
	            if (env.equals(UAT)) {
	                sendNovatorXML(xml, NOVATOR_UAT_FEED_IP, NOVATOR_UAT_FEED_PORT);
	            }
			}



			logger.info(ADDON_FEEDS_SUCCESS_MSG);

		} catch (Exception e) {

			logger.error("Exception in SendAllAddOnFeedServlet  in insertAllAddOnFeeds() "
					+ e.getMessage());
			logger.error(e);
			throw new Exception(e);
		}

	}
	
	  private String sendNovatorXML(String xmlString, String ipParamName, String portParamName) throws Exception {
		  
	        ConfigurationUtil cu = ConfigurationUtil.getInstance();
	        String server = cu.getFrpGlobalParm(NOVATOR_CONFIG,ipParamName);
	        String port = cu.getFrpGlobalParm(NOVATOR_CONFIG,portParamName);
	        Integer portInt = Integer.parseInt(port);
	        String timeout = cu.getFrpGlobalParm(NOVATOR_CONFIG,NOVATOR_FEED_TIMEOUT);
	        Integer timeoutInt = Integer.parseInt(timeout);
	        String response = "";
	        Socket socket = null;
	        PrintWriter out = null;
	        BufferedReader in = null;

	        try 
	        {
	          logger.debug("SendUpdateFeed: Connecting to " + server + " on port " + portInt + " for feed upload");
	          socket = new Socket(server, portInt);
	          socket.setSoTimeout(timeoutInt);

	          out = new PrintWriter(socket.getOutputStream(), true);
	          in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	        }
	        catch (UnknownHostException e)
	        {            
	          logger.error(e);
	          throw new Exception(e);
	        }
	        catch (IOException e)
	        {
	          logger.error(e);
	          throw new Exception(e);
	        }

	        logger.debug("SendUpdateFeed: Sending " + xmlString + " to " + server + " on port " + portInt);
	        out.println(xmlString);
	        out.flush();
	          
	        logger.debug("SendUpdateFeed: Data sent");
	        
	        try
	        {
	          logger.debug("SendUpdateFeed: Waiting for response");
	          StringBuffer sb = new StringBuffer();
	          response = in.readLine();
	          while( response!=null && response.length()>0 ) 
	          {
	              sb.append(response);
	              response = in.readLine();
	          }
	          response = sb.toString();
	          logger.debug("Novator Response:" + response);
	          
	        } 
	        catch(SocketTimeoutException stoe)
	        {
	          logger.error(stoe);
	          throw new Exception(stoe);
	        }
	        catch(IOException ioe)
	        {
	          logger.error(ioe);
	          throw new Exception(ioe);   
	        } 
	        finally 
	        {
	          try
	          {
	            out.close();
	            in.close();
	            socket.close();
	          }
	          catch(IOException ioe)
	          {
	            logger.error(ioe);
	            throw new Exception(ioe);   
	          }
	        }
	          
	        logger.debug("Ending send method");

	        return response;
	    }

}
