package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This class is used to render the opsAdmin.xsl page.
 */
public class InventoryTrackingUpdateServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.InventoryTrackingServlet");
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	logger.info("doGet()");
        response.setContentType(CONTENT_TYPE);
        this.processInventoryTracking(request, response);
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        try {
            this.processInventoryTracking(request, response);
        } catch(Exception e) {
            logger.error(e);
        }
    }

     /**
     * Load the opsAdmin.xsl page.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    @SuppressWarnings("unchecked")
	private void processInventoryTracking(HttpServletRequest request, HttpServletResponse response)
    {
              
    	String invAction = request.getParameter("invAction");
    	String tableName = null;
    	String vendorId = null;
    	String productId = null;
    	String shipDate = null;
    	String ftdQty = "0";
    	String canQty = "0";
    	String rejQty = "0";
    	String result = "";
    	
    	if (invAction == null || invAction.equals("")) {
    		invAction = "load";
    	} else {
    		logger.info("invAction: " + invAction);
    		tableName = request.getParameter("tableName");
            logger.info("tableName: " + tableName);
            vendorId = request.getParameter("vendorId");
            logger.info("vendorId: " + vendorId);
            productId = request.getParameter("productId");
            logger.info("productId: " + productId);
            shipDate = request.getParameter("shipDate");
            logger.info("shipDate: " + shipDate);

            Connection connection = null;
            DataRequest dataRequest = null;
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            try {
                // get database connection
                connection = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                dataRequest = new DataRequest();
                dataRequest.setConnection(connection);
                HashMap inputParams = new HashMap();

                if (invAction.equalsIgnoreCase("retrieve")) {
                    inputParams.put("IN_PRODUCT_ID", productId);
                    java.sql.Date sqlDate = new java.sql.Date(sdf.parse(shipDate).getTime());
                    inputParams.put("IN_SHIP_DATE", sqlDate);
                    if (tableName == null || tableName.equalsIgnoreCase("INV_TRK_COUNT")) {
                        dataRequest.setStatementID("GET_VENUS_INV_TRK");
                    } else {
                        inputParams.put("IN_VENDOR_ID", vendorId);
                    	dataRequest.setStatementID("GET_VENUS_INV_TRK_ASSIGNED");
                    }
                    dataRequest.setInputParams( inputParams );
                    CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
                    if ( results != null && results.next() ) {
                        ftdQty = results.getString("ftd_msg_count");
                        canQty = results.getString("can_msg_count");
                        rejQty = results.getString("rej_msg_count");
                    } else {
                    	result = "No record found";
                    	invAction = "load";
                    }
                } else if (invAction.equalsIgnoreCase("update")) {
                	ftdQty = request.getParameter("ftdQty");
                	canQty = request.getParameter("canQty");
                	rejQty = request.getParameter("rejQty");
                    inputParams.put("IN_PRODUCT_ID", productId);
                    java.sql.Date sqlDate = new java.sql.Date(sdf.parse(shipDate).getTime());
                    inputParams.put("IN_SHIP_DATE", sqlDate);
                    inputParams.put("IN_FTD_QTY", new BigDecimal(ftdQty));
                    inputParams.put("IN_REJ_QTY", new BigDecimal(rejQty));
                    inputParams.put("IN_CAN_QTY", new BigDecimal(canQty));
                    if (tableName == null || tableName.equalsIgnoreCase("INV_TRK_COUNT")) {
                        dataRequest.setStatementID("UPDATE_VENUS_INV_TRK");
                    } else {
                        inputParams.put("IN_VENDOR_ID", vendorId);
                    	dataRequest.setStatementID("UPDATE_VENUS_INV_TRK_ASSIGNED");
                    }
                    dataRequest.setInputParams( inputParams );
                    Map outputs = (Map)DataAccessUtil.getInstance().execute(dataRequest);
            	    result = "success";
                }
            } catch (Exception e) {
            	logger.error(e);
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException se) {
                    logger.error(se);
                }
            }
    	}
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
                
        if(securityContext == null) {
          securityContext = "";
        }
        
        if(securityToken == null) {
          securityToken = "";
        }
        
        if (adminAction == null) {
            adminAction = "";
        }
        
        try {
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();

            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);

            HashMap pageData = new HashMap();
            pageData.put("invAction", invAction);
//            if (invAction != null && !invAction.equalsIgnoreCase("load")) {
            	pageData.put("tableName", tableName);
            	pageData.put("vendorId", vendorId);
            	pageData.put("productId", productId);
            	pageData.put("shipDate", shipDate);
            	pageData.put("ftdQty", ftdQty);
            	pageData.put("canQty", canQty);
            	pageData.put("rejQty", rejQty);
            	pageData.put("result", result);
//            }
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true); 

            logger.info(DOMUtil.convertToString(responseDocument));
            
            File xslFile = new File(getServletContext().getRealPath("/xsl/inventoryTracking.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
          } else {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } catch(Exception e) {
            logger.debug(e);
        }
           
    }
}
