package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.utilities.GenerateWalMartXML;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.*;

import org.w3c.dom.Document;


public class ReinstatePOPTransactionServlet extends HttpServlet {
  //XSL pages
  private static final String XSL_REINSTATE_POP_TRANSACTION = "/xsl/reinstatePOPTransactions.xsl";
  private static final String XSL_FIX_POP_TRANSACTION = "/xsl/fixPOPTransactions.xsl";

  //table display and request variables
  private static final String ORDER_GATHERER_MESSAGES = "1";
  private static final String INBOUND_TRANSMISSIONS = "2";
  private static final String OUTBOUND_PARTNER_MESSAGES = "3";
  private static final String REINSTATE = "reinstate";
  private static final String FIX_TRANSACTION_XML = "fix_transaction_xml";
  private static final String ACTION = "action";
  private static final String TABLEID = "tableid";
  private static final String ORDER_SCRUB = "ORDER SCRUB";
  private static final String ERROR = "ERROR";
  private static final String NOT_SENT = "NOT SENT";
  private static final String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  private static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  private static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  private static final String LOGON_PAGE_PROPERTY = "logonpage";
  private static final String EXIT_PAGE_PROPERTY = "exitpage";
  private static final String XML_MESSAGE = "Please enter a value before submitting.";

  //stored procs
  private static final String POP_GET_ORDER_GATHERER_MESSAGES = "POP.GET_OGM_BY_PARTNER_STATUS";
  private static final String POP_GET_INBOUND_TRANSMISSION = "POP.GET_IN_TRAN_BY_PARTNER_STATUS";
  private static final String POP_GET_OUTBOUND_PARTNER_MESSAGES = "POP.GET_OPM_BY_PARTNER_STATUS";
  private static final String POP_UPDATE_GATHERER_MSG_ORDER_LOB = "POP.UPDATE_GATHERER_MSG_ORDER_LOB";
  private static final String POP_UPDATE_IN_TRANS_TRANSMISSION = "POP.UPDATE_IN_TRANS_TRANSMISSION";
  private static final String POP_UPDATE_OUTBOUND_MSG_MESSAGE = "POP.UPDATE_OUTBOUND_MSG_MESSAGE";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private String KEY;
  private String IN_STATUS;
  private String IN_CLOB = null;

  //logging
  private Logger logger;

  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    logger = new Logger("com.ftd.osp.opsadministration.presentation.ReinstatePOPTransactionServlet");
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    doPost(request, response);
  } //end doGet()

  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    String action = request.getParameter(ACTION);
    String tableid = request.getParameter(TABLEID);

    String logonPage = "";
    String exitPage = "";

    try {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      logonPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, LOGON_PAGE_PROPERTY);
      exitPage = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, EXIT_PAGE_PROPERTY);

      if ((action == null) || ((action != null) && ((tableid == null) && tableid.equals("")))) {
        loadPOPTransactions(request, response);
      } else {
        if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) {
          if (action.equals(FIX_TRANSACTION_XML)) {
            loadFixTransactions(request, response);
          } else if (action.equals(REINSTATE)) {
            reinstatePOPTransaction(request, response);
          }
        } else {
          //failed logon
          logger.error("Logon Failed");
          ServletHelper.redirectToLogin(request, response);

          return;
        }
      }
    } catch (Exception e) {
      logger.error(e);
    }
  }

  /**
   * Used to build the page that wil display
   * all the ERROR transacations from a particular table
   * @param request
   * @param response
   * @throws java.lang.Exception
   */
  private void loadPOPTransactions(HttpServletRequest request, HttpServletResponse response)
    throws Exception {
    Connection connection = null;
    DataRequest dataRequest = null;

    try {

      String statementId = null;
      String status = ERROR;
      String tableId = (request.getParameter(TABLEID) == null) ? ORDER_GATHERER_MESSAGES : request.getParameter(TABLEID);
      statementId = getStatement(tableId);

      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

      if (securityContext == null) {
        securityContext = "";
      }

      if (securityToken == null) {
        securityToken = "";
      }

      if (adminAction == null) {
        adminAction = "";
      }

      Document respDoc = DOMUtil.getDocument();

      connection = DataSourceUtil.getInstance().getConnection(ORDER_SCRUB);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      HashMap inParms = new HashMap();

      inParms.put("IN_STATUS", status);

      dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(statementId);
      dataRequest.setInputParams(inParms);

      CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

      //use GenerateXML utility class to generate
      //the XML for each table
      GenerateWalMartXML walMartGenerator = new GenerateWalMartXML();
      Document walMartOrderDocument = (Document) walMartGenerator.generateWalMartXML(rs, tableId);
      DOMUtil.addSection(respDoc, walMartOrderDocument.getChildNodes());

      HashMap tableParm = new HashMap();
      tableParm.put("tableid", tableId);
      DOMUtil.addSection(respDoc, "pageData", "params", tableParm, true);

      //add security variables
      HashMap securityParams = new HashMap();
      securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
      securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
      securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);
      DOMUtil.addSection(respDoc, "securityParams", "params", securityParams, true);

      File xslFile = new File(getServletContext().getRealPath(XSL_REINSTATE_POP_TRANSACTION));
      TraxUtil.getInstance().transform(request, response, respDoc, xslFile, null);
    } catch (Exception e) {
      String message = "ReinstatePOPTransactionServlet: problems exist loading records from the table";
      logger.error(message + " " + e.toString());
    } finally {
      if (connection != null) {
        connection.close();
      }
    }
  } //end loadPOPTransactions

  /**
   * Loads the main page reinstatePOPTransactions.xsl
   * where a user can select a transaction to fix.
   * @param request
   * @param response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  private void loadFixTransactions(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {

    try {
      // Create the initial document
      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
      String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
      String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);

      if (securityContext == null) {
        securityContext = "";
      }

      if (securityToken == null) {
        securityToken = "";
      }

      if (adminAction == null) {
        adminAction = "";
      }

      Object obj = request.getAttribute("XMLError");
      String xmlError = (String) obj;

      Document responseDocument = DOMUtil.getDocument();

      HashMap pageData = new HashMap();
      pageData.put("KEY", request.getParameter("key"));
      pageData.put("XML", request.getParameter("xmlText"));
      pageData.put("TABLEID", request.getParameter("tableid"));
      DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

      if ((xmlError != null) && xmlError.equals("TRUE")) {
        HashMap mapData = new HashMap();
        mapData.put("XMLERROR", xmlError);
        mapData.put("XMLMESSAGE", XML_MESSAGE);
        DOMUtil.addSection(responseDocument, "pageData", "data", mapData, true);
      }

      //add security variables
      HashMap securityParams = new HashMap();
      securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
      securityParams.put(HTML_PARAM_SECURITY_TOKEN, securityToken);
      securityParams.put(HTML_PARAM_ADMIN_ACTION, adminAction);
      DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);

      File xslFile = new File(getServletContext().getRealPath(XSL_FIX_POP_TRANSACTION));
      TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
    } catch (Exception e) {
      logger.error(e);
    }
  } //end loadFixTransactions()

  /**
   * Reinstate a POP Transaction and builds
   * the XSL page the user is returned to.
   * @param request
   * @param response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  private void reinstatePOPTransaction(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException, Exception {
    String statementId;
    String tableId;
    String xmlText = null;
    String userId = ServletHelper.getUserId(request);
    Connection connection = null;
    boolean isXML = true;

    try {
      tableId = request.getParameter(TABLEID);
      statementId = getUpdateStatement(tableId);
      xmlText = request.getParameter("xmlText");

      //check to see if the user is actually submitting a value
      if ((xmlText.trim() == null) || xmlText.trim().equals("")) {
        isXML = false;

        String message = "ReinstatePOPTransactionServelt: No XML/Text was submitted";
        logger.error(message);
        throw new Exception(message);
      }

      DataRequest dataRequest = null;

      connection = DataSourceUtil.getInstance().getConnection(ORDER_SCRUB);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      HashMap inParms = new HashMap();

      //this uses the table id to set the proper column names
      //for the inParms map based on the tableId
      setMapKeys(tableId);
      this.recordPOPReinstateOrder(KEY, request.getParameter("key"), request.getParameter("xmlText"), userId, getLogTableStatement(tableId), connection );      

      inParms.put(KEY, request.getParameter("key"));
      inParms.put(IN_STATUS, NOT_SENT);
      inParms.put(IN_CLOB, request.getParameter("xmlText"));
      dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(statementId);
      dataRequest.setInputParams(inParms);

      Map outputs = (Map) dataAccessUtil.execute(dataRequest);

      String status = (String) outputs.get(STATUS_PARAM);

      if (status.equalsIgnoreCase("N")) {
        logger.error("Unable to update POP Transaction: " + (String) outputs.get(MESSAGE_PARAM));
        throw new Exception("ReinstatePOPTransactionServlet: Unable to update POP transaction: " + 
                            (String) outputs.get(MESSAGE_PARAM));
      } else {
        logger.debug("Process successful");
      }
    } catch (Exception e) {
      logger.error(e);
    } finally {
      if (connection != null) {
        connection.close();
      }

      if (isXML) {
        loadPOPTransactions(request, response);
      } else {
        setResponse(request);
        this.loadFixTransactions(request, response);
      }
    }
  } //end reinstatePOPTransaction()

  private void recordPOPReinstateOrder(String idColumnName, String id, String xml, String createdBy,
    String statement, Connection connection) throws Exception {
    DataRequest dataRequest = null;
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    dataRequest = new DataRequest();
    dataRequest.setConnection(connection);

    HashMap systemMessage = new HashMap();
    systemMessage.put(idColumnName, id);
    systemMessage.put("IN_XML", xml);
    systemMessage.put("IN_CREATED_BY", createdBy);
    dataRequest.setStatementID(statement);
    dataRequest.setInputParams(systemMessage);

    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");

    if (status.equals("N")) {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }

  /**
    * Returns the appropriate SQL statement
    * based on the tableId passed in.
    * @param id
    * @return
    */
  private String getStatement(String id) {
    String statementId = "";

    if ((id == null) || (id == "")) {
      id = ORDER_GATHERER_MESSAGES;
    }

    if (id.equals(INBOUND_TRANSMISSIONS)) {
      statementId = POP_GET_INBOUND_TRANSMISSION;
    } else if (id.equals(OUTBOUND_PARTNER_MESSAGES)) {
      statementId = POP_GET_OUTBOUND_PARTNER_MESSAGES;
    } else if (id.equals(ORDER_GATHERER_MESSAGES)) {
      statementId = POP_GET_ORDER_GATHERER_MESSAGES;
    }

    return statementId;
  }

  /**
    * Return the database statement ID
    * based on the tableId being used.
    * @param id
    * @return
    */
  private String getUpdateStatement(String id) {
    String updateStatement = "";

    if (id.equals(INBOUND_TRANSMISSIONS)) {
      updateStatement = POP_UPDATE_IN_TRANS_TRANSMISSION;
    } else if (id.equals(OUTBOUND_PARTNER_MESSAGES)) {
      updateStatement = POP_UPDATE_OUTBOUND_MSG_MESSAGE;
    } else if (id.equals(ORDER_GATHERER_MESSAGES)) {
      updateStatement = POP_UPDATE_GATHERER_MSG_ORDER_LOB;
    }

    return updateStatement;
  }

  /**
     * Return the database statement ID
     * based on the tableId being used.
     * @param id
     * @return
     */
  private String getLogTableStatement(String id) {
    String logStatement = "";

    if ((id == null) || (id == "")) {
      id = ORDER_GATHERER_MESSAGES;
    }
    if (id.equals(INBOUND_TRANSMISSIONS)) {
      logStatement = "INSERT_INBND_TRANSMISSIONS";
    } else if (id.equals(OUTBOUND_PARTNER_MESSAGES)) {
      logStatement = "INSERT_OUTBND_PARTNER_MSGS";
    } else if (id.equals(ORDER_GATHERER_MESSAGES)) {
      logStatement = "INSERT_ORDER_GATHERER_MSGS";
    }

    return logStatement;
  }

  /**
    * Set the column values
    * based on the table being used.
    * @param id
    */
  private void setMapKeys(String id) {
    if (id.equals(INBOUND_TRANSMISSIONS)) {
      this.KEY = "IN_TRANSMISSION_ID";
      this.IN_STATUS = "IN_STATUS";
      this.IN_CLOB = "IN_TRANSMISSION";
    } else if (id.equals(OUTBOUND_PARTNER_MESSAGES)) {
      this.KEY = "IN_MESSAGE_ID";
      this.IN_STATUS = "IN_STATUS";
      this.IN_CLOB = "IN_MESSAGE";
    } else if (id.endsWith(ORDER_GATHERER_MESSAGES)) {
      this.KEY = "IN_ID";
      this.IN_STATUS = "IN_STATUS";
      this.IN_CLOB = "IN_ORDER_LOB";
    }
  }

  private void setResponse(HttpServletRequest request) {
    request.removeAttribute("XMLError");
    request.setAttribute("XMLError", "TRUE");
  }



} //End ReinstatePOPTransactionServlet
