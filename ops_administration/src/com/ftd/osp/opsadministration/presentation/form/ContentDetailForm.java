package com.ftd.osp.opsadministration.presentation.form;

import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ContentDetailForm extends BaseContentForm
{
    private long contentDetailId;
    private long contentMasterId;
    private String filter1Value;
    private String filter2Value;
    private String text;
    private ContentDetailVO contentDetailVO;
    private String formAction;
    private String updatedBy;
    private String saveResult;
    private Map<Long,ContentMasterVO> contentMasterMap;

    public ContentDetailForm()
    {
    }

    public void setContentDetailVO(ContentDetailVO contentDetailVO) {
        this.contentDetailVO = contentDetailVO;
    }

    public ContentDetailVO getContentDetailVO() {
        return contentDetailVO;
    }

    public void setContentDetailId(long contentDetailId) {
        this.contentDetailId = contentDetailId;
    }

    public long getContentDetailId() {
        return contentDetailId;
    }

    public void setFilter1Value(String filter1Value) {
        this.filter1Value = filter1Value;
    }

    public String getFilter1Value() {
        return filter1Value;
    }

    public void setFilter2Value(String filter2Value) {
        this.filter2Value = filter2Value;
    }

    public String getFilter2Value() {
        return filter2Value;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setFormAction(String formAction) {
        this.formAction = formAction;
    }

    public String getFormAction() {
        return formAction;
    }

    public void setSaveResult(String saveResult) {
        this.saveResult = saveResult;
    }

    public String getSaveResult() {
        return saveResult;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public List getContentMasterList()
    {
        return getContentMasterList();
    }

    public void setContentMasterId(long contentMasterId) {
        this.contentMasterId = contentMasterId;
    }

    public long getContentMasterId() {
        return contentMasterId;
    }

    public void setContentMasterMap(Map<Long, ContentMasterVO> contentMasterMap) {
        this.contentMasterMap = contentMasterMap;
    }

    public Map<Long, ContentMasterVO> getContentMasterMap() {
        return contentMasterMap;
    }

    public Collection getMasterValues()
    {
        return contentMasterMap.values();
    }

    public Collection getContentContextValues()
    {
        return getContentContextMap().values();
    }

}
