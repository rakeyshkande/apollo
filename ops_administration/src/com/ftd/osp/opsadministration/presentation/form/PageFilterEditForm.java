package com.ftd.osp.opsadministration.presentation.form;


import com.ftd.osp.opsadministration.vo.PageFilterActionVO;
import com.ftd.osp.opsadministration.vo.PageFilterDistroVO;
import com.ftd.osp.opsadministration.vo.PageFilterVO;

import java.util.Collection;
import java.util.List;

import java.util.Map;

import org.apache.struts.action.ActionForm;

public class PageFilterEditForm extends ActionForm
{
    private long filterId;
    private String filterName;
    private String msgSubject;
    private String msgSource;
    private String msgBody;
    private String filterAction;
    private String nopageStartHour;
    private String nopageEndHour;
    private String subjectPrefix;
    private String overwriteDistro;
    private String updatedBy;
    private String saveResult;
    private List<PageFilterDistroVO> distroList;
    private List<String> hourList;


	public PageFilterEditForm()
    {
    }


	public long getFilterId() {
		return filterId;
	}


	public void setFilterId(long filterId) {
		this.filterId = filterId;
	}


	public String getFilterName() {
		return filterName;
	}


	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}


	public String getMsgSubject() {
		return msgSubject;
	}


	public void setMsgSubject(String msgSubject) {
		this.msgSubject = msgSubject;
	}


	public String getMsgSource() {
		return msgSource;
	}


	public void setMsgSource(String msgSource) {
		this.msgSource = msgSource;
	}


	public String getMsgBody() {
		return msgBody;
	}


	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}


	public String getFilterAction() {
		return filterAction;
	}


	public void setFilterAction(String filterAction) {
		this.filterAction = filterAction;
	}


	public String getNopageStartHour() {
		return nopageStartHour;
	}


	public void setNopageStartHour(String nopageStartHour) {
		this.nopageStartHour = nopageStartHour;
	}


	public String getNopageEndHour() {
		return nopageEndHour;
	}


	public void setNopageEndHour(String nopageEndHour) {
		this.nopageEndHour = nopageEndHour;
	}


	public String getSubjectPrefix() {
		return subjectPrefix;
	}


	public void setSubjectPrefix(String subjectPrefix) {
		this.subjectPrefix = subjectPrefix;
	}


	public String getOverwriteDistro() {
		return overwriteDistro;
	}


	public void setOverwriteDistro(String overwriteDistro) {
		this.overwriteDistro = overwriteDistro;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getSaveResult() {
		return saveResult;
	}


	public void setSaveResult(String saveResult) {
		this.saveResult = saveResult;
	}


	public List<PageFilterDistroVO> getDistroList() {
		return distroList;
	}


	public void setDistroList(List<PageFilterDistroVO> distroList) {
		this.distroList = distroList;
	}


	public List<String> getHourList() {
		return hourList;
	}


	public void setHourList(List<String> hourList) {
		this.hourList = hourList;
	}


}
