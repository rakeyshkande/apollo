package com.ftd.osp.opsadministration.presentation.form;

import java.util.Collection;

public class ContentSetupForm extends BaseContentForm
{

     public ContentSetupForm()
    {
    }

    public Collection getContentContextValues()
    {
        return getContentContextMap().values();
    }

}
