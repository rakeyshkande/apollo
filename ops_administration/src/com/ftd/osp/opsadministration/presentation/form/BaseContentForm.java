package com.ftd.osp.opsadministration.presentation.form;

import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.ContentFilterVO;
import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import org.apache.struts.action.ActionForm;

public class BaseContentForm extends ActionForm
{
    private List<ContentMasterVO> contentMasterList;
    private Map<Long,ContentFilterVO> contentFilterMap;
    private List<String> messages;
    private String securitytoken;
    private String context;
    private List<ContentDetailVO> contentDetailList;
    private Map<String,ContentMasterVO> contentContextMap;
    private String contextFilter;
    
    public BaseContentForm()
    {
    }

    public void setContentMasterList(List<ContentMasterVO> contentMasterList)
    {
        this.contentMasterList = contentMasterList;
    }

    public List<ContentMasterVO> getContentMasterList()
    {
        return contentMasterList;
    }

    public void setContentFilterMap(Map<Long, ContentFilterVO> contentFilterMap)
    {
        this.contentFilterMap = contentFilterMap;
    }

    public Map<Long, ContentFilterVO> getContentFilterMap()
    {
        return contentFilterMap;
    }
    
    public ContentFilterVO getFilterVOById(long filterId)
    {
        ContentFilterVO vo = null;
        
        vo = contentFilterMap.get(filterId);
        
        return vo;
    }

    public void setMessages(List<String> messages)
    {
        this.messages = messages;
    }

    public List<String> getMessages()
    {
        return messages;
    }
    
    public void addMessage(String message)
    {
        if (messages == null)
        {
            messages = new ArrayList<String>();
        }
        messages.add(message);
    }

    public void setSecuritytoken(String securitytoken)
    {
        this.securitytoken = securitytoken;
    }

    public String getSecuritytoken()
    {
        return securitytoken;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }

    public void setContentDetailList(List<ContentDetailVO> contentDetailList) {
        this.contentDetailList = contentDetailList;
    }

    public List<ContentDetailVO> getContentDetailList() {
        return contentDetailList;
    }

    public void setContentContextMap(Map<String, ContentMasterVO> contentContextMap) {
        this.contentContextMap = contentContextMap;
    }

    public Map<String, ContentMasterVO> getContentContextMap() {
        return contentContextMap;
    }

    public void setContextFilter(String contextFilter) {
        this.contextFilter = contextFilter;
    }

    public String getContextFilter() {
        return contextFilter;
    }
}
