package com.ftd.osp.opsadministration.presentation.form;


import com.ftd.osp.opsadministration.vo.PageFilterActionVO;
import com.ftd.osp.opsadministration.vo.PageFilterVO;

import java.util.Collection;
import java.util.List;

import java.util.Map;

import org.apache.struts.action.ActionForm;

public class PageFilterListForm extends ActionForm
{
    private List<PageFilterVO> pageFilterList;
    private Map<Long,PageFilterVO> pageFilterMap;
    private String securitytoken;
    private String context;
    private String actionFilter;
    private String saveResult;
    private List<PageFilterActionVO> actionTypeList;
    private long filterId;


	public PageFilterListForm()
    {
    }

    public void setPageFilterList(List<PageFilterVO> pageFilterList)
    {
        this.pageFilterList = pageFilterList;
    }

    public List<PageFilterVO> getPageFilterList()
    {
        return pageFilterList;
    }

    public void setPageFilterMap(Map<Long, PageFilterVO> pageFilterMap)
    {
        this.pageFilterMap = pageFilterMap;
    }

    public Map<Long, PageFilterVO> getPageFilterMap()
    {
        return pageFilterMap;
    }
    
    public PageFilterVO getFilterVOById(long filterId)
    {
        PageFilterVO vo = null;
        
        vo = pageFilterMap.get(filterId);
        
        return vo;
    }


    public void setSecuritytoken(String securitytoken)
    {
        this.securitytoken = securitytoken;
    }

    public String getSecuritytoken()
    {
        return securitytoken;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getContext()
    {
        return context;
    }
    
    public String getActionFilter() {
		return actionFilter;
	}

	public void setActionFilter(String actionFilter) {
		this.actionFilter = actionFilter;
	}
	

	public List<PageFilterActionVO> getActionTypeList() {
		return actionTypeList;
	}

	public void setActionTypeList(List<PageFilterActionVO> actionTypeList) {
		this.actionTypeList = actionTypeList;
	}

	public String getSaveResult() {
		return saveResult;
	}

	public void setSaveResult(String saveResult) {
		this.saveResult = saveResult;
	}

	public long getFilterId() {
		return filterId;
	}

	public void setFilterId(long filterId) {
		this.filterId = filterId;
	}
}
