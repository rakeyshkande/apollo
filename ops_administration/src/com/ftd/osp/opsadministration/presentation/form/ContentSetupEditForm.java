package com.ftd.osp.opsadministration.presentation.form;

import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.Collection;

public class ContentSetupEditForm extends BaseContentForm
{
 
    private static Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.form.ContentSetupEditForm");

    private long contentMasterId;
    private String contentContext;
    private String contentName;
    private String contentDescription;
    private long contentFilter1Id;
    private long contentFilter2Id;
    private String updatedBy;
    private String saveResult;
    private String defaultValue;
    
    private String submitAction;
    
    private ContentMasterVO contentMasterVO;
    
    public ContentSetupEditForm()
    {
        contentMasterVO = new ContentMasterVO();
    }

    public void setContentMasterId(long contentMasterId)
    {
        this.contentMasterId = contentMasterId;
    }

    public long getContentMasterId()
    {
        return contentMasterId;
    }

    public void setContentMasterVO(ContentMasterVO contentMasterVO)
    {
        this.contentMasterVO = contentMasterVO;
    }

    public ContentMasterVO getContentMasterVO()
    {
        return contentMasterVO;
    }

    public void setContentContext(String contentContext)
    {
        this.contentContext = contentContext;
    }

    public String getContentContext()
    {
        return contentContext;
    }

    public void setContentName(String contentName)
    {
        this.contentName = contentName;
    }

    public String getContentName()
    {
        return contentName;
    }

    public void setContentFilter2Id(long contentFilter2Id)
    {
        this.contentFilter2Id = contentFilter2Id;
    }

    public long getContentFilter2Id()
    {
        return contentFilter2Id;
    }

    public void setContentFilter1Id(long contentFilter1Id)
    {
        this.contentFilter1Id = contentFilter1Id;
    }

    public long getContentFilter1Id()
    {
        return contentFilter1Id;
    }

    public void setSubmitAction(String submitAction)
    {
        this.submitAction = submitAction;
    }

    public String getSubmitAction()
    {
        return submitAction;
    }
    
    public Collection getFilterValues()
    {
        return getContentFilterMap().values();
    }

    public void setContentDescription(String contentDescription)
    {
        this.contentDescription = contentDescription;
    }

    public String getContentDescription()
    {
        return contentDescription;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setSaveResult(String saveResult) {
        this.saveResult = saveResult;
    }

    public String getSaveResult() {
        return saveResult;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public Collection getContentContextValues()
    {
        logger.info("csef: " + getContentContextMap().size());
        return getContentContextMap().values();
    }
}
