package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.mercuryinterface.dao.MercuryDAO;
import com.ftd.osp.mercuryinterface.vo.MercuryOpsVO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.w3c.dom.NamedNodeMap;

/**
 * This class retrieves results from two database queries,
 * one that contains every row in the MERCURY_OPS table and
 * another that contains the count of records in the MERCURY
 * table that have a MERCURY_STATUS of 'MO', grouped by
 * OUTBOUND_ID. These results are passed to boxx_dashboard.xsl
 * for display and editing, if applicable.
 *
 * @author Tim Schmig
 */
public class MercuryOpsEditServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    private static final String VIEW_MERCURY_OPS = "VIEW_MERCURY_OPS";
    private Logger logger;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    private final static String OPS_ADMIN_CONFIG_CONTEXT = "OPS_ADMIN_CONFIG";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";
    public static final String BOXX_PAGE_PROPERTY = "boxx_url";

    /**
     * init.
     * @param config
     * @exception Servlet Exception
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        logger = new Logger("com.ftd.osp.opsadministration.presentation.MercuryOpsEditServlet");
    }

    /**
     * doPost.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Connection conn = null;
        String suffix = request.getParameter("formSuffix");
        String inUse = request.getParameter("inUse");
        String availableFlag = request.getParameter("availableFlag");
        String sendNew = request.getParameter("sendNew");
        String akkAsk = request.getParameter("ackAsk");
        String resendBatch = request.getParameter("resendBatch");
        String ackCount = request.getParameter("ackCount");

        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }
        if(securityToken == null)
        {
          securityToken = "";
        }
        if (adminAction == null) 
        {
            adminAction = "";
        }
        
        logger.debug("Posting suffix " + suffix);

        try
        {
          conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
          MercuryDAO dao = new MercuryDAO(conn);
          
          MercuryOpsVO vo = dao.getMercuryOpsBySuffix(suffix);

          if ( inUse.equals("N") ) 
          {
            vo.setInUse(false);
          }
          if ( availableFlag.equals("Y") ) 
          {
            vo.setAvailable(true);
          } else 
          {
            vo.setAvailable(false);
          }
          if ( sendNew.equals("Y") )
          {
            vo.setSendNewOrders(true);
          } else 
          {
            vo.setSendNewOrders(false);
          }
          vo.setSleepTime(Integer.parseInt(request.getParameter("sleepTime")));
          vo.setMaxBatchSize(Integer.parseInt(request.getParameter("batchSize")));
          if ( availableFlag.equals("Y") ) 
          {
            vo.setAvailable(true);
          } else 
          {
            vo.setAvailable(false);
          }
          if ( akkAsk.equals("Y") ) 
          {
            vo.setAckAsk(true);
          } else 
          {
            vo.setAckAsk(false);
          }
          if ( resendBatch.equals("Y") ) 
          {
            vo.setResendBatch(true);
          } else 
          {
            vo.setResendBatch(false);
          }
          
          vo.setAckCount(Integer.parseInt(ackCount));
        
          dao.updateMercuryOPS(vo);
          
          this.loadMercuryOps(request, response, suffix);
                    
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(conn != null)
                  conn.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * doGet.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @exception ServletException
     * @exception IOException
     * 
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        String suffix = request.getParameter("formSuffix");
        this.loadMercuryOps(request, response, suffix);
    }

     /**
     * Retrieve the MERCURY_OPS records and open MERCURY records from the database.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     */
    private void loadMercuryOps(HttpServletRequest request, HttpServletResponse response, String suffix)
    {
        Connection connection = null;
        DataRequest dataRequest = null;
        
        String logonPage = null;
        String exitPage = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        
        if(securityContext == null)
        {
          securityContext = "";
        }

        if(securityToken == null)
        {
          securityToken = "";
        }

        if (adminAction == null) 
        {
            adminAction = "";
        }

        try{
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          logonPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(OPS_ADMIN_CONFIG_CONTEXT,EXIT_PAGE_PROPERTY);    
          if (configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "SECURITY_OFF").equals("true") || ServletHelper.isValidToken(request)) 
          {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // get database connection
            connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
            dataRequest = new DataRequest();
                
            dataRequest.setConnection(connection);
            // get MERCURY_OPS statistics
            dataRequest.setStatementID(VIEW_MERCURY_OPS);
            HashMap systemMessage = new HashMap();
            systemMessage = new HashMap();
            systemMessage.put("IN_SUFFIX", suffix);
            systemMessage.put("IN_AVAILABLE", null );
            dataRequest.setInputParams(systemMessage);
            Document systemMessageDoc = (Document) DataAccessUtil.getInstance().execute(dataRequest);
            DOMUtil.addSection(responseDocument, systemMessageDoc.getChildNodes()); 

            //add security variables
            HashMap securityParams = new HashMap();
            securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
            securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
            securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
            DOMUtil.addSection(responseDocument, "securityParams", "params", securityParams, true);
            
            dataRequest.reset();
            
            dataRequest.setConnection(connection);
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            Map paramMap = new HashMap();
            paramMap.put("IN_CONTEXT", "MERCURY_INTERFACE");
            paramMap.put("IN_PARAM", "MAX_ACK_COUNT");
            dataRequest.setInputParams(paramMap);
            String rs = (String)DataAccessUtil.getInstance().execute(dataRequest);
            String value = rs;
            
            int maxAttempts;
            try 
            {
              maxAttempts = Integer.parseInt(value);
            } catch (Exception e) 
            {
              logger.warn("Parameter MERCURY_INTERFACE/MAX_ACK_COUNT is not set up in FRP.GLOBAL_PARMS correctly",e);
              maxAttempts=3;
            }
      
            //Populate the ack count combo with the greater of maxAttempts or ackCount
            int ackCount;
            Node attNode = (Node) DOMUtil.selectSingleNode(systemMessageDoc, "/SUFFIXS/SUFFIX/@ack_count");
            if( attNode!=null ) 
            {
              value = attNode.getNodeValue();
              ackCount = Integer.parseInt(value);
              
              if( ackCount>maxAttempts ) 
              {
                maxAttempts=ackCount;
              }
            }
            
            //Generate the entries for the ACK Count combo
            LinkedHashMap ackCombo = new LinkedHashMap(maxAttempts+1);
            for( int cnt=0; cnt<=maxAttempts; cnt++ ) 
            {
              String item = String.valueOf(cnt);
              ackCombo.put(item,item);  
            }
            DOMUtil.addSection(responseDocument,"ackCombo","param",ackCombo,true);
            
            File xslFile = new File(getServletContext().getRealPath("/xsl/mercuryOpsEdit.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            ServletHelper.redirectToLogin(request, response);
            return;         
          }
        } 
        
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(connection != null)
                  connection.close();
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
  
  public static void main(String[] args)
  {
    try{
    MercuryDAO me = new MercuryDAO(null);
    System.out.println(me.truncateField("111","Some Field",null,5));
    }
    catch(Exception e)
    {
      System.out.println(e);
    }
  }

  }
