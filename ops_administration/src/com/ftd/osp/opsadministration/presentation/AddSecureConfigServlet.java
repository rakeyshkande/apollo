package com.ftd.osp.opsadministration.presentation;

import com.ftd.osp.opsadministration.dao.SecureConfigDAO;
import com.ftd.osp.opsadministration.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This class retrieves data required for the AddSecureConfig.xsl page
 *
 * @author Mike Kruger
 */
public class AddSecureConfigServlet extends HttpServlet 
{
  private Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.AddSecureConfigServlet");
  private static final String CONTENT_TYPE = "text/html; charset=utf-8";
  private Connection conn = null;

  // Data source name
  private static final String DATA_SOURCE = "ORDER SCRUB";

  // Security parameters
  private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
  public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
  public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
  public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";
  public static final String LOGON_PAGE_PROPERTY = "logonpage";    
  public static final String SECURITY_OFF = "SECURITY_OFF";
  public static final String TRUE = "true";

  /**
  * init.
  * @param config
  * @exception Servlet Exception
  */
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  /**
  * Retrieves security parameters.
  * @param request HttpServletRequest
  * @return Hashmap of security parameters 
  */
  public HashMap getSecurity(HttpServletRequest request)
  {
    // Retrieve security variables
    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);        
    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    
    if(securityContext == null)
    {
        securityContext = "";
    }
    
    if(securityToken == null)
    {
        securityToken = "";
    }
    
    if (adminAction == null) 
    {
        adminAction = "";
    }
    
    // Add security variables
    HashMap securityParams = new HashMap();
    securityParams.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
    securityParams.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
    securityParams.put(HTML_PARAM_ADMIN_ACTION,adminAction);
    
    return securityParams;
  }
		
  /**
  * Obtains required for the initial load of the addSecureConfig.xsl page
  * @param request HttpServletRequest
  * @param response HttpServletResponse
  * @exception ServletException
  * @exception IOException
  */
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    try
    {
      // Get connection
      conn =  DataSourceUtil.getInstance().getConnection(DATA_SOURCE);

      // Test for valid securitytoken
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      if (!configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, SECURITY_OFF).equals(TRUE) 
        && !ServletHelper.isValidToken(request)) 
      {
        // Failed log on
        logger.error("Logon failed for security token: " + request.getParameter(HTML_PARAM_SECURITY_TOKEN));
        ServletHelper.redirectToLogin(request, response);
        return;         
      }

      // Retrieve all Secure System Configuration properties
      SecureConfigDAO scDAO = new SecureConfigDAO(conn);
      Document secureData = scDAO.loadContexts();

      // Create root document
      Document dataDocument = DOMUtil.getDocument();

      // Add contexts data to the root document
      DOMUtil.addSection(dataDocument, secureData.getChildNodes());

      // Add security variables
      DOMUtil.addSection(dataDocument, "securityParams", "params", getSecurity(request), true);

      File xslFile = new File(getServletContext().getRealPath("/xsl/addSecureConfig.xsl"));
      TraxUtil.getInstance().transform(request, response, dataDocument, xslFile, null);
    } 
    catch(Exception e)
    {
      logger.debug(e);
    }
    finally
    {
      try 
      {
        if(conn != null)
          conn.close();
      }
      catch(SQLException se) 
      {
        logger.error(se);
      }
    }
  }
}
