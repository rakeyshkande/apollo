package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;

import org.apache.struts.action.Action;

public class AbstractContentAction extends Action
{
    public AbstractContentAction()
    {
    }
    
    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection      
        Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");

        return conn;
    }
    
}
