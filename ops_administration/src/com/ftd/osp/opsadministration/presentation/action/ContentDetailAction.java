package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.opsadministration.bo.ContentBO;
import com.ftd.osp.opsadministration.dao.ContentDAO;
import com.ftd.osp.opsadministration.presentation.form.ContentDetailForm;

import com.ftd.osp.opsadministration.vo.ContentDetailVO;
import com.ftd.osp.opsadministration.vo.ContentFilterVO;
import com.ftd.osp.opsadministration.vo.ContentMasterVO;

import java.io.IOException;

import java.sql.Connection;

import com.ftd.osp.utilities.plugins.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContentDetailAction extends AbstractContentAction
{
    private static Logger logger  = new Logger("com.ftd.osp.opsadministration.presentation.action.ContentDetailAction");

    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
          throws IOException, ServletException, Exception
    {
        ContentDetailForm contentDetailForm = (ContentDetailForm) form;
        logger.debug("Start " + contentDetailForm);
        String forwardMapping = "";
        
        String action = request.getParameter("formAction");
        logger.debug("action: " + action);

        String detailId = request.getParameter("contentdetailid");
        logger.debug("detail id: " + detailId);
        if (detailId != null && !detailId.equals("")) {
            contentDetailForm.setContentDetailId(Long.parseLong(detailId));
        }
        String masterId = request.getParameter("contentMasterId");
        logger.debug("master id: " + masterId);
        if (masterId != null && !masterId.equals("")) {
            contentDetailForm.setContentMasterId(Long.parseLong(masterId));
        }
        
        String contextFilter = request.getParameter("contextFilter");
        logger.debug("context filter: " + contextFilter);
        contentDetailForm.setContextFilter(contextFilter);

        Connection con = getNewConnection();
        ContentBO contentBO = new ContentBO();

        String userId = contentBO.getUserId(request);
        contentDetailForm.setUpdatedBy(userId);

        if (action == null || action.equals("") || action.equalsIgnoreCase("cancel")) {
            contentBO.getMasterList(con, contentDetailForm);
            contentBO.getFilterMap(con, contentDetailForm);
            contentBO.getDetailList(con, contentDetailForm);
            forwardMapping = "success";

            Map<Long,ContentMasterVO> contentMasterMap = new HashMap<Long,ContentMasterVO>();
            ContentDAO dao = new ContentDAO();
            List<ContentMasterVO> contentMasterList = dao.getContentMasterList(con, contextFilter);
            //List<ContentMasterVO> contentMasterList = contentDetailForm.getContentMasterList();
            Map<Long,ContentFilterVO> contentFilterMap = dao.getContentFilterMap(con);
            for (int i=0; i<contentMasterList.size(); i++) {
                ContentMasterVO vo = contentMasterList.get(i);
                String master = vo.getContext() + " / " + vo.getName();
                long filter1 = vo.getFilter1Id();
                long filter2 = vo.getFilter2Id();
                boolean detailExists = false;
                if (filter1 > 0) {
                    master = master + " / " + contentFilterMap.get(filter1).getContentFilterName();
                } else {
                    // look for detail existing detail record
                    List<ContentDetailVO> contentDetailList = contentDetailForm.getContentDetailList();
                    for (int j=0; j<contentDetailList.size(); j++) {
                        ContentDetailVO cdVO = contentDetailList.get(j);
                        if (cdVO.getContentMasterID() == vo.getContentMasterId()) {
                            detailExists = true;
                            continue;
                        }
                    }
                }
                if (filter2 > 0) {
                    master = master + " / " + contentFilterMap.get(filter2).getContentFilterName();
                }
                if (!detailExists) {
                    vo.setDescription(master);
                    contentMasterMap.put(vo.getContentMasterId(), vo);
                }
            }
            if (contentMasterMap.size() < 1) {
                ContentMasterVO vo = new ContentMasterVO();
                vo.setContentMasterId(0);
                vo.setDescription("None");
                contentMasterMap.put(vo.getContentMasterId(), vo);
            }
            contentDetailForm.setContentMasterMap(contentMasterMap);

        } else if (action.equalsIgnoreCase("edit")) {
            contentBO.getDetailById(con, detailId, contentDetailForm);
            forwardMapping = "edit";
        } else if (action.equalsIgnoreCase("add")) {
            contentBO.getNewDetail(con, masterId, contentDetailForm);
            forwardMapping = "edit";
        } else if (action.equalsIgnoreCase("save")) {
            String result = contentBO.saveDetail(con, contentDetailForm);
            if (result == null || result.equals("")) {
                result = "success";
            } else if (result.substring(0,6).equalsIgnoreCase("newid=")) {
                String newId = result.substring(6);
                logger.debug("newId: " + newId);
                detailId = newId;
                result = "success";
            }
            contentDetailForm.setSaveResult(result);
            contentBO.getDetailById(con, detailId, contentDetailForm);
            forwardMapping = "edit";
        }

        request.setAttribute("contentDetailForm", contentDetailForm);

        if (con != null && !con.isClosed())
        	try {
        		con.close();
        	} catch (Exception ex) {
        		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
        	}

        return mapping.findForward(forwardMapping);
    }

}
