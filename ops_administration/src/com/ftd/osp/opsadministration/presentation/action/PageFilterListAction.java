package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.opsadministration.bo.PageFilterBO;
import com.ftd.osp.opsadministration.presentation.form.PageFilterListForm;
import com.ftd.osp.opsadministration.vo.PageFilterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PageFilterListAction extends AbstractContentAction
{
    private static Logger logger  = new Logger("com.ftd.osp.opsadministration.presentation.action.PageFilterListAction");

    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
    {
    	Connection con = null;
    	String forwardMapping = "list";
        try
        {
            PageFilterListForm pageFilterListForm = (PageFilterListForm) form;
            logger.debug("Start " + pageFilterListForm);
            String action = request.getParameter("formAction");
            logger.debug("action: " + action);

            con = getNewConnection();
            
            PageFilterBO pageBO = new PageFilterBO();
            if (action == null) {
            	action = "";
            }
            if (action.equalsIgnoreCase("update_status")){
            	pageBO.updatePageFilterStatus(con, pageFilterListForm, request);
            }
            else if (action.equalsIgnoreCase("delete")) {
                pageBO.deletePageFilter(con, pageFilterListForm);
            } else {
            	//this is a search.
            	pageFilterListForm.setSaveResult("");
            }
            
        	pageBO.getPageFilterList(con, pageFilterListForm);
        	pageBO.getActionTypeList(con, pageFilterListForm);
            request.setAttribute("pageFilterListForm", pageFilterListForm);
            return mapping.findForward(forwardMapping);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(),e);
        }
        finally {
        	if (con != null && !con.isClosed())
            	try {
            		con.close();
            	} catch (Exception ex) {
            		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
            	}
        }

        return mapping.findForward("list");
    }
       
    }
