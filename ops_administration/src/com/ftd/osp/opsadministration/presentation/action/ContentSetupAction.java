package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.opsadministration.bo.ContentBO;
import com.ftd.osp.opsadministration.presentation.form.ContentSetupForm;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContentSetupAction extends AbstractContentAction
{
    private static Logger logger  = new Logger("com.ftd.osp.opsadministration.presentation.action.ContentSetupAction");

    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
    {
    	Connection con = null;
        try
        {
            ContentSetupForm contentSetupForm = (ContentSetupForm) form;
            logger.debug("Start " + contentSetupForm);
            
            String contextFilter = request.getParameter("contextFilter");
            logger.debug("context filter: " + contextFilter);
            contentSetupForm.setContextFilter(contextFilter);

            con = getNewConnection();
            
            ContentBO contentBO = new ContentBO();
    
            contentBO.getMasterList(con, contentSetupForm);
            contentBO.getFilterMap(con, contentSetupForm);
            
            request.setAttribute("contentSetupForm", contentSetupForm);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(),e);
        }
        finally {
        	if (con != null && !con.isClosed())
            	try {
            		con.close();
            	} catch (Exception ex) {
            		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
            	}
        }

        return mapping.findForward("success");
    }
       
    }
