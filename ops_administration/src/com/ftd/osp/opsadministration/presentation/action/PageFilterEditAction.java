package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.opsadministration.bo.PageFilterBO;
import com.ftd.osp.opsadministration.presentation.form.PageFilterEditForm;
import com.ftd.osp.opsadministration.vo.PageFilterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PageFilterEditAction extends AbstractContentAction
{
        private static Logger logger  = new Logger("com.ftd.osp.opsadministration.presentation.action.PageFilterEditAction");

        public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
              throws IOException, ServletException, Exception
        {
            PageFilterEditForm pageFilterEditForm = (PageFilterEditForm) form;
            logger.debug("Start " + pageFilterEditForm);
            String forwardMapping = "";
            
            String action = request.getParameter("formAction");
            logger.debug("action: " + action);

            String filterId = request.getParameter("filterId");
            logger.debug("filter id: " + filterId);
            if (filterId != null && !filterId.equals("")) {
            	pageFilterEditForm.setFilterId(Long.parseLong(filterId));
            }

            Connection con = getNewConnection();
            PageFilterBO pageBO = new PageFilterBO();

            String userId = pageBO.getUserId(request);
            pageFilterEditForm.setUpdatedBy(userId);

            if (action == null || action.equals("display_add")) {
            	pageFilterEditForm.setSaveResult("");
            	forwardMapping = "edit";
            } 
            else if (action.equalsIgnoreCase("display_edit")) {
            	pageFilterEditForm.setSaveResult("");
            	pageBO.getPageFilterById(con, pageFilterEditForm);
                forwardMapping = "edit";
            }            
            else if (action.equalsIgnoreCase("edit_save")) {
                pageBO.savePageFilter(con, pageFilterEditForm);
                forwardMapping = "edit";
            }

            else {
            	forwardMapping = "edit";
            	
            }
            pageBO.getDistroList(con, pageFilterEditForm);
            PageFilterBO.getNopageHourList(con, pageFilterEditForm);

            request.setAttribute("pageFilterEditForm", pageFilterEditForm);

            if (con != null && !con.isClosed())
            	try {
            		con.close();
            	} catch (Exception ex) {
            		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
            	}

            return mapping.findForward(forwardMapping);
        }


       
    }
