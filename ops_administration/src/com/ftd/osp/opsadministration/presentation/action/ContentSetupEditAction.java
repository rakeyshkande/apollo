package com.ftd.osp.opsadministration.presentation.action;

import com.ftd.osp.opsadministration.bo.ContentBO;
import com.ftd.osp.opsadministration.presentation.form.ContentSetupEditForm;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContentSetupEditAction extends AbstractContentAction
{
    private static String SAVE = "Save";
    
    private static Logger logger = new Logger("com.ftd.osp.opsadministration.presentation.action.ContentSetupEditAction");

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                 HttpServletResponse response) throws IOException, ServletException, Exception
    {
        ContentSetupEditForm contentSetupEditForm = (ContentSetupEditForm)form;
        logger.debug("Start " + contentSetupEditForm);
        Connection con = null;
        try
        {
            con = getNewConnection();
    
            ContentBO contentBO = new ContentBO();
    
            String contextFilter = request.getParameter("contextFilter");
            logger.debug("context filter: " + contextFilter);
            contentSetupEditForm.setContextFilter(contextFilter);

            String action = request.getParameter("formAction");
            logger.debug("action = " + action);
            if (action != null && action.equalsIgnoreCase(SAVE))
            {
                String userId = contentBO.getUserId(request);
                contentSetupEditForm.setUpdatedBy(userId);
                contentBO.saveMaster(con, contentSetupEditForm);                                                    
            }
            
            contentBO.getMasterList(con, contentSetupEditForm);
            contentBO.getFilterMap(con, contentSetupEditForm);
            contentBO.getMasterVO(contentSetupEditForm);
    
            // Set the defaults for the select boxes
            if (contentSetupEditForm.getContentMasterVO() != null)
            {
                contentSetupEditForm.setContentFilter1Id(contentSetupEditForm.getContentMasterVO().getFilter1Id());
                contentSetupEditForm.setContentFilter2Id(contentSetupEditForm.getContentMasterVO().getFilter2Id());
            }
            
            request.setAttribute("contentSetupEditForm", contentSetupEditForm);
    
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(),e);
            contentSetupEditForm.addMessage(e.getMessage());
        }
        finally {
        	if (con != null && !con.isClosed())
            	try {
            		con.close();
            	} catch (Exception ex) {
            		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
            	}
        }
        return mapping.findForward("success");
    }

}
