package com.ftd.osp.opsadministration.presentation.test.presentation;


import com.ftd.osp.opsadministration.presentation.ReinstateAmazonOrderServlet;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * This class tests the ReinstateAmazonOrderServlet.
 *
 * @author Carl Jenkins
 */
public class ReinstateAmazonOrderServletTest  extends TestCase
{
    private static String URL = "";
    private static ReinstateAmazonOrderServlet reinstateOrder;
    private ConfigurationUtil configUtil = null;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";
    
    private final static String ORDER_XML_ID = "order_xml_id";
    private final static String PERSPECTIVE = "context";
    private final static String EVENT_NAME = "event_name";
    private final static String ORDER_XML = "order_xml";
    private final static String ACTION = "action";
    private final static String REINSTATE = "reinstate";
    private final static String FIX_XML = "fix_xml";
    
    private Map map = null;
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public ReinstateAmazonOrderServletTest(String name) { 
        super(name); 
        try
        {
          configUtil = ConfigurationUtil.getInstance();
          URL = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "url");
         
          System.out.println( URL );
        }     
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }
//add values to the put method 
 protected void setUp()
    { 
       map = new HashMap();
       map.put(ORDER_XML_ID,"1" );
       map.put(PERSPECTIVE,  "AMAZON");
       map.put(EVENT_NAME,  "ORDER INFUSER" );
       map.put(ORDER_XML,   "<?xml version = '1.0' encoding = 'utf-8'?><order guid=''><header><master-order-number>A001</master-order-number><source-code>522</source-code><origin>AMZNI</origin><order-count>2</order-count><order-amount>61.11</order-amount><transaction-date>Mon Jan 21 12:30:08 CST 2004</transaction-date><socket-timestamp/><buyer-first-name>Rose</buyer-first-name><buyer-last-name>Lazuk</buyer-last-name><buyer-address1>3113 woodcreek drive</buyer-address1><buyer-address2/><buyer-city>Downers Grove</buyer-city><buyer-state>IL</buyer-state><buyer-postal-code>60515</buyer-postal-code><buyer-country>US</buyer-country><buyer-daytime-phone>5555555555</buyer-daytime-phone><buyer-evening-phone/><buyer-work-ext/><buyer-fax/><buyer-email-address/><ariba-buyer-cookie/><ariba-asn-buyer-number/><ariba-payload/><news-letter-flag>Y</news-letter-flag><cc-type>IN</cc-type><cc-number/><cc-exp-date/><cc-approval-code/><cc-approval-amt/><cc-approval-verbage/><cc-approval-action-code/><cc-avs-result/><cc-acq-data/><co-brand-credit-card-code/><aafes-ticket-number/><gift-certificates/><co-brands/></header><items><item><order-number>Z001</order-number><order-total>61.11</order-total><tax-amount>4.13</tax-amount><service-fee>9.99</service-fee><extra-shipping-fee/><drop-ship-charges/><retail-variable-price>46.99</retail-variable-price><productid>C13-3068</productid><product-price>46.99</product-price><color/><size/><add-ons/><recip-first-name>robin</recip-first-name><recip-last-name>bird</recip-last-name><recip-address1>7205 Seminole Drive</recip-address1><recip-address2/><recip-city>Darien</recip-city><recip-state>IL</recip-state><recip-postal-code>60561</recip-postal-code><recip-country>US</recip-country><recip-international/><recip-phone>5555555555</recip-phone><recip-phone-ext/><ship-to-type>R</ship-to-type><ship-to-type-name/><ship-to-type-info/><occassion>2</occassion><delivery-date>02/21/2004</delivery-date><second-delivery-date/><card-message>test</card-message><card-signature>test</card-signature><special-instructions/><shipping-method/><lmg-flag/><lmg-email-address/><lmg-email-signature/><fol-indicator>FTD</fol-indicator><sunday-delivery-flag/><sender-release-flag/><qms-result-code/><qms-address1>7205 Seminole Drive</qms-address1><qms-address2/><qms-city>Darien</qms-city><qms-state>IL</qms-state><qms-postal-code>60561</qms-postal-code><qms-firm-name/><qms-latitude/><qms-longtitude/><qms-override-flag/><qms-usps-range-record-type/><ariba-po-number/><ariba-cost-center/><ariba-ams-project-code/><ariba-unspsc-code>empty</ariba-unspsc-code><product-substitution-acknowlegement/></item></items></order>" );
    }   
   
   /** 
    * 
    * This particular method tests the ReinstateAmazonOrderServlet's 
    * ability to pull back all entries from the AZ_ERROR table.
    * Executes the method in ReainstateAmazonOrderServlet 
    * that displays all records from AZ_ERROR
    **/ 
    public void testLoadReinstateAmazonOrder() throws Exception{ 
          System.out.println("testing reinstate Amazon order servlet");
          System.out.println("\n" + "testLoadReinstateAmazonOrder" );
          //no parameters needed to retrieve all orders.
          map = new HashMap();
          String response = send(URL,map);          
       //   System.out.println("response: " + response);
          //Validate response
          //Check if the result is not null
          assertNotNull("testReinstateAmazonOrder result", response);
    } 
 /** 
    * A method to test the ReinstateAmazonOrderServlet's
    * ability to delete a record from the AZ_ERROR table.
    * The deleteRecord in ReinstateAmazonOrderServlet is 
    * executed as a side effect of the reinstateAmazonOrder methods
    * execution.
    **/ 
  public void testDeleteRecord() throws Exception {
          System.out.println("testing reinstate Amazon order servlet");
          System.out.println("\n" + "testDeleteRecord" );
          //to delete a record another parameter must be added to tell the 
          //servlet to reinstate the order first 
          map.put( ACTION,  REINSTATE );
          String response = send(URL,map);          
         // System.out.println("response: " + response);
          //Validate response
          //Check if the result is not null
          assertNotNull("testDeleteRecord result", response);
          
    
  }
 
 /** 
    * A method to test the ReinstateAmazonOrderServlet's
    * ability to load a particular record from the AZ_ERROR table
    * which (on the UI) will be fixed by the user then submitted 
    * for reinstatement into the order processing system.
    **/ 
  public void testLoadFixOrder() throws Exception {
          System.out.println("testing reinstate Amazon order servlet");
          System.out.println("\n" + "testLoadFixOrder()");
          //Be sure to change the values for map in the setup method above
          //to reflect the order from AZ_ERROR.
          //Must add an extra parameter to inform the servlet that
          //we want to call the loadFixOrderMethod.
          map.put(ACTION, FIX_XML );
          String response = send(URL,map);          
         // System.out.println("response: " + response);
          //Validate response
          //Check if the result is not null
          assertNotNull("testLoadFixOrder result", response);
         
          
    
  } 
  
  /** 
   * This method will test the ReinstateAmazonOrderServlet's 
   * ability to reinstate an order back into the system
   * once the xml has been fixed by the user.
    **/ 
  public void testReinstateAmazonOrder() throws Exception {
          System.out.println("testing reinstate Amazon order servlet");  
          System.out.println("\n" + "testReinstateAmazonOrder");
          //add an extra parameter for reinstate of order.
          //same functionality as the deleteRecord.
          //deleteRecord in ReinstateAmazonOrderServlet is executed
          //as a side effect of an order being reinstated.
          map.put( ACTION,  REINSTATE );
          String response = send(URL,map);          
          System.out.println("response: " + response);
          //Validate response
          //Check if the result is not null
          assertNotNull("testReinstateAmazonOrder result", response);
    
  }
  
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{
      
      
        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);
            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);
          if ( con == null ) 
            System.out.println( "NULL****");
            //Send data
            PrintStream out = new PrintStream(con.getOutputStream() );
            //OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }
  
                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

           // InputStream is = con.getInputStream();
            DataInputStream is = new DataInputStream( con.getInputStream() );
            
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
           //suite.addTest(new ReinstateAmazonOrderServletTest("testLoadReinstateAmazonOrder"));
           //suite.addTest( new ReinstateAmazonOrderServletTest("testDeleteRecord"));
           suite.addTest( new ReinstateAmazonOrderServletTest("testLoadFixOrder"));
           //suite.addTest( newgoo ReinstateAmazonOrderServletTest("testReinstateAmazonOrder"));
            
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}