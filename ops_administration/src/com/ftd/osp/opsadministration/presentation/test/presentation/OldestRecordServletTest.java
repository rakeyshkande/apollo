package com.ftd.osp.opsadministration.presentation.test.presentation;

import com.ftd.osp.opsadministration.presentation.OldestRecordServlet;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * This class tests the OldestRecordServlet.
 *
 * @author Rose Lazuk
 */
public class OldestRecordServletTest  extends TestCase
{
    private static String URL = "";
    private static OldestRecordServlet oldestRecordServlet;
    private ConfigurationUtil configUtil = null;
    private final static String OPS_ADMIN_CONFIG_FILE = "ops_admin_config.xml";

    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public OldestRecordServletTest(String name) { 
        super(name); 
        try
        {
          configUtil = ConfigurationUtil.getInstance();
          URL = configUtil.getProperty(OPS_ADMIN_CONFIG_FILE, "url");
        }
        catch(Exception e)
        {
          e.printStackTrace();
        }

    }

 protected void setUp()
    { 
       
    }   
   
   /** 
    * Test Oldest Record Servlet.  
    **/ 
    public void testOldestRecord() throws Exception{ 
          System.out.println("testing oldest record servlet");
          Map map = new HashMap();
          String response = send(URL,map);          
          System.out.println("response: " + response);
          //Validate response
          //Check if the result is not null
          assertNotNull("testOldestRecord result", response);
    } 

  
  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
      suite.addTest(new OldestRecordServletTest("testOldestRecord"));
      
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    } 

    public static void main(String args[])
{
  junit.textui.TestRunner.run( suite() );
}

}