package com.ftd.weboedispatcher;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.Types;


public class ExceptionHandler extends EventHandler 
{

  private Logger logger = new Logger("com.ftd.weboedispatcher.ExceptionHandler");

  public ExceptionHandler()
  {
  }
  
   public void invoke(Object payload) throws Throwable
   {
    CallableStatement cstmt = null;
    Connection connection = null;
    
    try
    {
      MessageToken token = (MessageToken)payload;
      String orderID = (String)token.getMessage();
      logger.info("WebOE Dispatcher ExceptionHandler received a message for order:" + orderID);
        
      connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
      cstmt = connection.prepareCall("{call SCRUB.MISC_PKG.INSERT_ORDER_EXCEPTIONS(?, ?, ?, ?, ?)}");
      cstmt.setString(1, "WEBOE ORDER EVENTS QUEUE" + "/" + "DISPATCH WEBOE ORDER EVENT");
      cstmt.setString(2, orderID);

      String errorMessage = "";
      // convert the error message to an xml safe format
      errorMessage = DOMUtil.encodeChars(errorMessage);

      cstmt.setString(3, errorMessage);

      cstmt.registerOutParameter(4, Types.VARCHAR);
      cstmt.registerOutParameter(5, Types.VARCHAR);

      cstmt.execute();

      String statusFromDB = (String) cstmt.getObject(4);
      String messageFromDB = (String) cstmt.getObject(5);
      logger.debug("Log Error Status:: " + statusFromDB);
      logger.debug("Log Error Message:: " + messageFromDB);

    } catch (Exception e) {
      logger.error(e.toString(), e);
      throw e;
    } finally {
      if (cstmt != null) {
        try {
          cstmt.close();
          connection.close();
        } catch (Exception e){
          logger.error(e.toString(), e);
        }
      }
    }
  }
   
}


  