package com.ftd.weboedispatcher;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.DriverManager;

public class WebOEDispatcher extends EventHandler 
{

  private Logger logger = new Logger("com.ftd.weboedispatcher.WebOEDispatcher");

  public WebOEDispatcher()
  {
  }
  
   public void invoke(Object payload) throws Throwable
   {
      try {
        MessageToken token = (MessageToken)payload;
        String orderID = (String)token.getMessage();
        logger.info("WebOE Dispatcher received a message for order:" + orderID);
        boolean dispatchOrder = true;
  
        ResourceManager rm = ResourceManager.getInstance();
        String duplicateOrderCheck = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "weboe.dispatcher.duplicate.order.check");
  
        if (duplicateOrderCheck != null && duplicateOrderCheck.equals("Y")) 
        {
          if (orderAlreadyInScrub(orderID)) 
          {
            dispatchOrder = false;
          }
        }
  
        if (dispatchOrder) {
          WebOEDispatcherSVC webOEDispatcherSVC = new WebOEDispatcherSVC();
          webOEDispatcherSVC.dispatchOrder(orderID);
        }
        
      } catch (Exception e) 
      {
        logger.error(e.toString(), e);
        throw e;
      }
      
   }
   
   private boolean orderAlreadyInScrub(String order_id)
   throws Exception
   {
      logger.info("Looking up order in SCRUB schema to make sure it was not already processed. Order_GUID:" + order_id);
      boolean orderFoundInScrub = false;
      Connection connection = null;
      String result = "N";

      try {
        connection =  DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("SCRUB_ORDER_EXISTS");
        dataRequest.addInputParam("IN_ORDER_GUID", order_id);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        result = (String) dataAccessUtil.execute(dataRequest); 

        if (result.equalsIgnoreCase("Y")) {
            logger.info("Order already in SCRUB schema!!! Do not process.");
            orderFoundInScrub = true;
        }
        
      } catch (Exception e) {
        logger.error(e.toString(), e);
        throw e;
      } finally {
        try {
          connection.close();
        } catch(Exception e){
          throw e;  
        }
      }

      return orderFoundInScrub;
   }
}