package com.ftd.weboedispatcher;

import com.ftd.applications.oe.common.persistent.OrderPO;
import com.ftd.applications.oe.services.utilities.BillingUTIL;
import com.ftd.framework.businessservices.servicebusinessobjects.BusinessService;
import com.ftd.framework.common.valueobjects.FTDArguments;
import com.ftd.framework.common.valueobjects.FTDCommand;
import com.ftd.framework.common.valueobjects.FTDSystemVO;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBLocal;
import com.ftd.newsletterevents.bo.ejb.NewsletterPubEJBLocalHome;
import com.ftd.newsletterevents.vo.StatusType;

import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;


public class WebOEDispatcherSVC extends BusinessService {
    public FTDSystemVO main(FTDCommand command, FTDArguments arguments) {
        return null;
    }

    public void initialize() {
    }

    public String toString() {
        return null;
    }

    public void dispatchOrder(String orderID) throws Exception {
        OrderPO order = null;

        try {
            this.getLogManager().info("Retrieving order from the FTD_APPS schema. Order_ID:" +
                orderID);
            order = (OrderPO) this.getPersistentServiceObject(orderID,
                    OrderPO.class);
            
            // JP Puzon 09-28-05, 11-06-2005
            // First, publish any newsletter subscription as an event.
            // Doing this shorter and faster step first will reduce
            // the chance of a race condition when persisting email-pertinent data
            // for this event and the proceeding billing dispatch event.
            if ((order.getSpecialPromotionFlag() != null) &&
                    order.getSpecialPromotionFlag().equalsIgnoreCase("Y") &&
                    (order.getBillToCustomer().getEmailAddress() != null)) {
                    
                this.getLogManager().info("Customer opted-in for special promotions. Publishing Newsletter event for Order_ID:" +
                orderID);

                Context jndiContext = new InitialContext();

               try {
                    NewsletterPubEJBLocalHome home = (NewsletterPubEJBLocalHome) jndiContext.lookup("java:comp/env/ejb/local/NewsletterPubEJB");

                    // create an EJB instance 
                    NewsletterPubEJBLocal npEjb = home.create();

                    npEjb.process(order.getBillToCustomer().getEmailAddress(),
                        order.getCompanyId(), StatusType.SUBSCRIBED, order.getCsrUserName(),
                        new Date());
                } finally {
                    jndiContext.close();
                }
            }
            
            BillingUTIL billingUtil = new BillingUTIL(this.getLogManager());
            this.getLogManager().debug("Calling BillingUTIL.dispatchOrder()");
            billingUtil.dispatchOrder(order, false);
            
        } catch (Exception e) {
            this.getLogManager().error(e.toString(), e);
            throw e;
        }
    }
}
