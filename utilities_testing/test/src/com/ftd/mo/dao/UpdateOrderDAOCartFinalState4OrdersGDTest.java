package com.ftd.mo.dao;

import org.junit.Test;


/**
 * Check if carts with products in various states are updateable. Since these carts 
 * have gift card payments they will not always be updateable.
 * This class repeats all the tests of its super class, just with 4 orders instead of 2. (5 give a fraud alert)
 *
 */
public class UpdateOrderDAOCartFinalState4OrdersGDTest extends UpdateOrderDAOCartFinalState2OrdersGDTest
{    
    public void setUp() throws Exception
    {
        if (!setup)
        {
            setup=true;
            numProducts = 4;

            initClass();

            // setup an order in clean
            createGiftCardOrder();

            assertTrue(processor.isOrderClean(connection, products));
        }
    }


    @Test
    public void testIsCartInFinalStateProcessedProcessed() throws Exception
    {
        super.testIsCartInFinalStateProcessedProcessed();
    }

    @Test
    public void testIsCartInFinalStateHoldProcessed() throws Exception
    {
        super.testIsCartInFinalStateHoldProcessed();
    }

    @Test
    public void testIsCartInFinalStateProcessedHold() throws Exception
    {
        super.testIsCartInFinalStateProcessedHold();
    }

    @Test
    public void testIsCartInFinalStateValidatedProcessed() throws Exception
    {
        super.testIsCartInFinalStateValidatedProcessed();
    }

    @Test
    public void testIsCartInFinalStateProcessedValidated() throws Exception
    {
        super.testIsCartInFinalStateProcessedValidated();
    }

}
