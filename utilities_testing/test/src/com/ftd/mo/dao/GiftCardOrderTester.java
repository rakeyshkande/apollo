package com.ftd.mo.dao;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import com.ftd.order.CleanOrderCreator;
import com.ftd.order.OrderBuilder;
import com.ftd.order.OrderProcessor;
import com.ftd.order.Product;
import com.ftd.order.payment.CreditCardPayment;
import com.ftd.order.payment.GiftCardPayment;
import com.ftd.order.profile.OrderProfile;
import com.ftd.utils.OutOfContainerTester;
import com.ftd.utils.TestUtilities;

public abstract class GiftCardOrderTester extends OutOfContainerTester
{

    protected static CleanOrderCreator cleanOrderCreator;
    protected static OrderProcessor processor;
    protected static UpdateOrderDAO uodao;
    protected static Integer numProducts = null;;
    protected static List<Product> products = new ArrayList<Product>();
    private OrderProfile profile = null;
    
    protected void initClass() throws Exception
    {
        setupConnection();
        setupInitialContext();          
        setupDataSources();
               
        cleanOrderCreator = new CleanOrderCreator();
        processor = new OrderProcessor();
        uodao = new UpdateOrderDAO(connection); 
        
        // if a giftCardTester was used to create a gift card order and authorize the gift card, refund the gift card when done.
        // we have to use a shutdown hook since junit 3 does not support @AfterClass which would be way better.
        // might be even better to addd refund attribute to giftCardPayment since some tests are creating data to be persisted.
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                if (profile != null && profile.getGiftCardPayment() != null && profile.getGiftCardPayment().isAuth())
                {
                    try
                    {
                        profile.getGiftCardPayment().refund(connection);
                        System.out.println("Gift card refunded");
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        fail("Unable to refund gift card");
                    }
                }
            }
        });

    }       

    protected void createGiftCardOrder() throws Exception
    {
        assertNotNull(numProducts);
        OrderBuilder builder = new OrderBuilder();   
    
        // set configurations
        profile = new OrderProfile();
        profile.setCreateCleanOrder(true);
        profile.setFreeShipping(false);
    
        for (int i=0; i < numProducts; i++)
        {
            profile.addProduct(Product.getDefaultBearProduct());
        }        
    
        int gdamt = 5;
        String gdnum = "6006492147013900431";
        GiftCardPayment gdp = new GiftCardPayment(gdnum, gdamt); // don't really know if this card has a balance - should ensure that
        profile.addPayment(gdp);
    
        double ccAmt = builder.getOrderTotal() - gdamt;
        CreditCardPayment ccp = new CreditCardPayment(null, null, ccAmt, true);
        profile.addPayment(ccp);
    
        String masterOrderNumber = null;
    
        masterOrderNumber = builder.submitOrder(profile);  
        products = builder.getOrderProducts();
    
        System.out.println("Master order number = " + masterOrderNumber);
    }
    

    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        List<Product> products = cleanOrderCreator.getProducts();
        // ensure cart is in state we expect
        resetCartToClean(products);
    }

    @After
    public void tearDown() throws Exception
    {
        super.tearDown();
    }

    protected String getOrderDetailID(String orderNumber) throws SQLException
    {
        return (TestUtilities.getOrderDetailID(connection, orderNumber)).toString();
    }

    private void resetCartToClean(List<Product> products) throws SQLException
    {
        for (Product product : products)
        {
            processor.setOrderToProcessed(connection, product.getOrderNumber());
        }
    }

}