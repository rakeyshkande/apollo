package com.ftd.mo.dao;

import java.util.List;

import org.junit.Test;

import com.ftd.order.Product;

/**
 * Check if carts with products in various states are updateable. Since these carts 
 * have no gift card payments they will always be updateable.
 *
 */
public class UpdateOrderDAOCartFinalStateNoGDTest extends GiftCardOrderTester
{    
    public void setUp() throws Exception
    {
        if (!setup)
        {
            setup=true;
            initClass();

            // setup an order in clean
            assertTrue(cleanOrderCreator.createOrderToClean(connection, 2));   
        }
        super.setUp();
    }

    @Test
    public void testIsCartInFinalStateProcessedProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        List<Product> products = cleanOrderCreator.getProducts();

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateHoldProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        List<Product> products = cleanOrderCreator.getProducts();

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToHold(connection, orderNumber);

        // verify cart is in final state if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateProcessedHold() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        List<Product> products = cleanOrderCreator.getProducts();

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToHold(connection, orderNumber);

        // verify cart is in final state if we check the hold item
        orderNumber = products.get(1).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }


    @Test
    public void testIsCartInFinalStateValidatedProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        List<Product> products = cleanOrderCreator.getProducts();

        // get the first order item and set it to hold
        orderNumber =  products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToValidated(connection, orderNumber);

        // verify cart is in final state if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateProcessedValidated() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        List<Product> products = cleanOrderCreator.getProducts();

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToValidated(connection, orderNumber);

        // verify cart is in final state if we check the processed item
        orderNumber = products.get(1).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertNotSame("Y", status);
    }

}
