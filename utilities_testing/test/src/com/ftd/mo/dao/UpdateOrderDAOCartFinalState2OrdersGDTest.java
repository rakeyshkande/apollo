package com.ftd.mo.dao;


import org.junit.Test;


/**
 * Check if carts with products in various states are updateable. Since these carts 
 * have gift card payments they will not always be updateable.
 * This class tests with 2 orders in the cart.
 *
 */
public class UpdateOrderDAOCartFinalState2OrdersGDTest extends GiftCardOrderTester
{    
    public void setUp() throws Exception
    {
        if (!setup)
        {
            setup=true;
            numProducts = 2;

            initClass();

            // setup an order in clean
            createGiftCardOrder();

            assertTrue(processor.isOrderClean(connection, products));
        }
        super.setUp();
    }


    @Test
    public void testIsCartInFinalStateProcessedProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateHoldProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToHold(connection, orderNumber);

        // verify is updateable if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateProcessedHold() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToHold(connection, orderNumber);

        // verify is not updateable if we check the hold item
        orderNumber = products.get(1).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertFalse("Y".equals(status));
    }


    @Test
    public void testIsCartInFinalStateValidatedProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber =  products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToValidated(connection, orderNumber);

        // verify is updateable if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateProcessedValidated() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToValidated(connection, orderNumber);

        // verify cart is not updateable if we check the processed item
        orderNumber = products.get(1).getOrderNumber();
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertFalse("Y".equals(status));
    }

}
