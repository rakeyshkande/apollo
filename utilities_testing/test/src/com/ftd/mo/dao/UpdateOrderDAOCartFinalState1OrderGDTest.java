package com.ftd.mo.dao;


import org.junit.Test;


/**
 * Check if carts with products in various states are updateable. Since these carts 
 * have gift card payments they will not always be updateable.
 * This class tests with only 1 order in the cart and therefore should always be updateable.
 *
 */
public class UpdateOrderDAOCartFinalState1OrderGDTest extends GiftCardOrderTester
{        
    public void setUp() throws Exception
    {
        if (!setup)
        {
            setup=true;
            numProducts = 1;

            initClass();

            // setup an order in clean
            createGiftCardOrder();

            assertTrue(processor.isOrderClean(connection, products));
        }
        super.setUp();
    }


    @Test
    public void testIsCartInFinalStateProcessed() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateHold() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber = products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToHold(connection, orderNumber);

        // verify is updateable if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }

    @Test
    public void testIsCartInFinalStateValidated() throws Exception
    {
        String status;
        String orderNumber;
        String orderDetailId;

        // get the first order item and set it to hold
        orderNumber =  products.get(0).getOrderNumber();

        orderDetailId = getOrderDetailID(orderNumber);
        processor.setOrderToValidated(connection, orderNumber);

        // verify is updateable if we check the hold item
        orderDetailId = getOrderDetailID(orderNumber);
        status = uodao.isCartInFinalState(orderDetailId);
        assertEquals("Y", status);
    }
}
