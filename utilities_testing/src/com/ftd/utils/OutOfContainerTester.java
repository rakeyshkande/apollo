package com.ftd.utils;

import java.util.Properties;
import static org.junit.Assert.fail;

import javax.naming.Context;
import javax.naming.InitialContext;

import oracle.jdbc.pool.OracleDataSource;

import org.junit.Before;

public abstract class OutOfContainerTester extends ApolloTester
{
    protected static void setupInitialContext()
    {
        Properties env = System.getProperties();
        env.put("java.naming.factory.url", "org.jboss.naming:org.jnp.interfaces");
        env.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        env.put("line.separator", "\n");
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");

        System.setProperties(env);			
    }

    protected static void setupDataSources()
    {
        // Create initial context
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");            
        System.setProperty("line.separator", "\n");
        try
        {
            InitialContext ic = new InitialContext();
            // clean up any previous bindings in case the initial context has already been set
            try {
                ic.destroySubcontext("jdbc");
                ic.destroySubcontext("java:comp");
            }
            catch(Exception e) // ignore errors of the jdbc not being bound yet
            {            
            }

            ic.createSubcontext("jdbc");
            ic.createSubcontext("java:comp");

            // Construct DataSource
            OracleDataSource ds = new OracleDataSource();
            ds.setURL(TestUtilities.getDatabaseString());
            ds.setUser("osp");
            ds.setPassword("osp");

            // bind scrubds datasource - add other datasources as needed        
            ic.bind("jdbc/SCRUBDS", ds);
            ic.bind("jdbc/ORDER_SCRUBDS", ds);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail("Unble to initialize data sources");
        }
    }

    public OutOfContainerTester()
    {
        super();
        setupInitialContext();
        setupDataSources();
    }
}