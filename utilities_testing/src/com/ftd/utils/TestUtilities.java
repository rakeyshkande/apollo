package com.ftd.utils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Document;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.osp.utilities.xml.DOMUtil;

public class TestUtilities
{
    private static String database_ = "jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";

    /**
     * setup a database connection based on the input database string. 
     * @param db- If not specified use default defined in this class - this parm allows multiple connections to be established
     * @return
     */
    public static Connection setupConnection(String db)
    {
        Connection connection = null;
        String dbString = getDatabaseString();
        if (db != null)
        {
            dbString = db;
        }
        
        try
        {
            String driver_ = "oracle.jdbc.driver.OracleDriver";
            // TODO: user dedicated login/password with full access 
            String user_ = "chill";
            String password_ = "chill0425";

            Class.forName(driver_);                                                           
            connection = DriverManager.getConnection(dbString, user_, password_);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        return connection;
    }
    
    /**
     * TODO: don't use hard-coded string - put config in a file somewhere so it can be easily changed as environments change 
     * @return
     */
    public static String getDatabaseString()
    {
        return database_;
    }


    /**
     * Return midnight of tonight, i.e. start of today
     * @return
     */
    public static Calendar midnight()
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal;
    }

    /**
     * Execute a SQL query and return the result.
     * 
     * @param connection - database connection
     * @param query, e.g. "select florist_id AS floristID from fit.florists"
     * @param parm - value to be retrieved from query string, e.g. floristID
     * @return
     */
    public static Object getSQLResult(Connection connection, String sqlQuery, String parm) throws SQLException
    {
        PreparedStatement psQuery = null;
        ResultSet rs = null;

        Object returnValue = null;
        try {
            psQuery = connection.prepareStatement(sqlQuery);
            rs = psQuery.executeQuery();

            if (rs.next()) {
                returnValue = rs.getObject(parm);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (rs != null) {rs.close();}
                if (psQuery != null) {psQuery.close();}
            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
        return returnValue;
    }

    /**
     * Execute any SQL string passed in, e.g. "delete from fit.florists where florist_id = 'X';" - use with caution!
     * 
     * @param connection - database connection
     * @param sqlQuery
     * @return
     * @throws SQLException 
     */
    public static void executeSQL(Connection connection, String sqlQuery) throws SQLException {
        PreparedStatement psQuery = null;

        try {
            psQuery = connection.prepareStatement(sqlQuery);
            int rc = psQuery.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (psQuery != null) {psQuery.close();}
            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
                throw ex;
            }
        }       
    }

    /**
     * Get the current balance of a gift card using the giftCardBO
     * 
     * @param connection
     * @param giftCardNumber
     * @param giftCardPin
     * @return
     * @throws Exception
     */
    public static double getCardBalance(Connection connection, String  giftCardNumber, String giftCardPin) throws Exception
    {
        GiftCardBO gbo = new GiftCardBO(connection);

        Document errorDocument = DOMUtil.getDefaultDocument();
        double balance = gbo.getCurrentBalance(errorDocument, giftCardNumber, giftCardPin, "0");
        return balance;
    }

    /**
     * return an order guid given a master order number
     * @param connection
     * @param masterOrderNumber
     * @return
     * @throws SQLException
     */
    public static String getOrderGuid(Connection connection, String masterOrderNumber) throws SQLException
    {
        String query = "select order_guid AS guid from scrub.orders where master_order_number = '"+ masterOrderNumber + "'";
        return (String) getSQLResult(connection, query, "guid");
    }

    /**
     * return an order detail id given an order number
     * @param connection
     * @param externalOrderNumber
     * @return
     * @throws SQLException
     */
    public static BigDecimal getOrderDetailID(Connection connection, String externalOrderNumber) throws SQLException
    {
        String query = "select order_detail_id AS id from scrub.order_details where external_order_number = '"+ externalOrderNumber + "'";
        return (BigDecimal) getSQLResult(connection, query, "id");
    }

}
