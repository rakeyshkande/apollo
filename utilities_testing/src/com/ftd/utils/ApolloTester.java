package com.ftd.utils;

import java.sql.Connection;

import junit.framework.TestCase;

// extends
public abstract class ApolloTester extends TestCase
{
    protected static boolean setup = false;

    protected static Connection connection;

    protected static Connection setupConnection()
    {
        if (connection == null)
        {
            connection = TestUtilities.setupConnection(null);
        }        
        return connection;
    }
    
    public ApolloTester(String name)
    {
        super(name);
        setupConnection();
    }
    
    public ApolloTester()
    {
        super("");
        setupConnection();
    }
}