/**
 * 
 */
package com.ftd.order;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.mo.dao.UpdateOrderDAO;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.order.payment.CreditCardPayment;
import com.ftd.order.payment.GiftCardPayment;
import com.ftd.order.payment.Payment;
import com.ftd.order.profile.OrderProfile;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.utils.ApolloTester;
import com.ftd.utils.OutOfContainerTester;
import com.ftd.utils.TestUtilities;

/**
 * This class will build a simple order XML
 *
 */
public class OrderBuilder extends OutOfContainerTester {

    public OrderBuilder() {
        super();
        
        // every order has a cc payment section, even if only empty nodes
        CreditCardPayment ccp = new CreditCardPayment(null, null, 0, false);
        addPayment(ccp);
    }

    Map <String,Payment>payments = new HashMap<String,Payment>();

    String defaultSourceCode = "2333";
    String masterOrderNumber;
    String orderNumberRoot;    

    private List<Product> products = new ArrayList<Product>();
    private String emailAddress = "chris@orderbuildertest.com";
    private boolean createCleanOrder = true; // would be nice to make this product specific some day

    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) {        
        OrderBuilder builder = new OrderBuilder();

        try
        {
            builder.submitDefaultOrder(builder);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        } 
        System.exit(0);
    }


    public void submitDefaultOrder(OrderBuilder builder) throws Exception
    {
        setDefaultConfigs();

        // set products in order
        setDefaultOrderProducts();

        addDefaultPayments();

        // create and submit the order
        String orderXML = buildXML();
        submitOrder(orderXML);
    }


    private void addDefaultPayments()
    {
        // add gd payment
        GiftCardPayment gdp = new GiftCardPayment("6006492147013900423", 20);
        //gdp.setAmount(getOrderTotal());
        addPayment(gdp);

        // update cc payment
        CreditCardPayment ccp = new CreditCardPayment(null, null, getOrderTotal() - gdp.getAmount(), true);
        addPayment(ccp);
    }


    private void setDefaultConfigs() throws Exception
    {
        setCreateCleanOrder(true);
        setFreeShippingOn(false);
    }

    private void setDefaultOrderProducts() throws Exception
    {
        addOrderItem(Product.getDefaultBearProduct());
        addOrderItem(Product.getDefaultSpecialtyGiftProduct());
    }
    
    public String submitOrder(OrderProfile profile) throws Exception
    {
        resetOrderProducts();
        resetPayments();
        
        // setup order based on profile
        setCreateCleanOrder(profile.isCreateCleanOrder());
        setFreeShippingOn(profile.isFreeShipping());
        for (Product product : profile.getProducts())
        {
            addOrderItem(product);
        }
        for (Payment payment : profile.getPayments())
        {
            addPayment(payment);
        }
        
        return submitOrder(buildXML());
    }


    public String submitOrder(String orderXML) throws Exception
    {
        // allow clients to default or pass in their own order xml
        if (orderXML == null)
        {
            orderXML = buildXML();
        }

        System.out.print(orderXML);
        System.out.println();

        // connect to order gatherer to submit the order
        URL url = new URL(getOGURL());
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter os = new OutputStreamWriter(conn.getOutputStream());
        os.write("Order="+URLEncoder.encode(orderXML, "UTF-8"));
        os.flush();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        os.close();
        br.close();
        
        System.out.println("submitted to OG: " + masterOrderNumber);
        
        return masterOrderNumber;
    }

    private String getOGURL()
    {
        return "http://brass2.ftdi.com:8712/OG/servlet/OrderGatherer";
    }


    public String buildXML() throws Exception {		
        masterOrderNumber = generateMasterOrderNumber();        

        String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" +
        "<order key-name=\"\">" +
        "	<header>" +
        "		<buyer-signed-in>Y</buyer-signed-in>" +
        "		<buyer-first-name><![CDATA[OrderBuilder]]></buyer-first-name>" +
        "		<socket-timestamp />" +
        "		<buyer-email-address><![CDATA["+emailAddress+"]]></buyer-email-address>" + 
        "		<buyer-city><![CDATA[Downers Grove]]></buyer-city>" +
        "		<buyer-postal-code><![CDATA[60515]]></buyer-postal-code>" +
        "		<order-extensions />" +
        "		<language-id>ENUS</language-id>" +
        "		<ariba-asn-buyer-number />" +
        "		<buyer-daytime-phone><![CDATA[5882300232]]></buyer-daytime-phone>";

        for (Payment payment : payments.values())
        {
            payment.charge(connection);
            xml += payment.generateOrderXML();
        }

        xml+="		<buyer-fax />" +
        "		<buyer-state><![CDATA[IL]]></buyer-state>" +
        "		<origin>FOX</origin>" +
        "		<ariba-buyer-cookie />" +
        "		<aafes-ticket-number />" +
        "		<buyer-evening-phone><![CDATA[5882300232]]></buyer-evening-phone>" +
        "		<loss-prevention-indicator>1319291648310185900069911815</loss-prevention-indicator>" +
        "		<fraud-codes />" +
        "		<buyer-last-name><![CDATA[Tester]]></buyer-last-name>" +
        "		<ariba-payload />" +
        "		<transaction-date>"+new Date() +"</transaction-date>" +
        "		<order-count>"+products.size()+"</order-count>";

        xml+="	<buyer-work-ext />" +
        "		<source-code>"+defaultSourceCode+"</source-code>" +
        "		<news-letter-flag>Y</news-letter-flag>" +
        "		<order-amount>"+getOrderTotal()+"</order-amount>" +
        "		<co-brand-credit-card-code />" +
        "		<co-brands />" +
        "		<buyer-country><![CDATA[US]]></buyer-country>" +
        "		<buyer-address1><![CDATA[3113 Woodcreek Dr]]></buyer-address1>" +
        "		<buyer-address2 />" +
        "                <master-order-number>" + masterOrderNumber + "</master-order-number>" +
        "	</header>" +
        "	<items>";

        // Generate order products xml
        int i=0;
        for (Product item : (List<Product>)products) {
            xml+="		<item>" +
            "			<order-number>"+ item.getOrderNumber() +"</order-number>" +
            "			<productid>"+item.productId+"</productid>" +
            "           <product-price>"+item.price+"</product-price>" +
            "			<retail-variable-price>"+item.price+"</retail-variable-price>" +
            "			<discounted-product-price>"+item.price+"</discounted-product-price>" +
            "			<discount-amount>0</discount-amount>" +
            "			<service-fee>"+item.serviceFee+"</service-fee>" +
            "			<drop-ship-charges>"+item.shippingFee+"</drop-ship-charges>	" +
            "           <order-total>"+(item.getTotalCost())+"</order-total>" +
            "			<taxes>" +
            "				<tax>" +
            "					<amount>7.42</amount>" +
            "					<name>il-tax</name>" +
            "				</tax>" +
            "			</taxes>		" +
            "" +
            "			<second-color-choice />" +
            "			<fol-indicator>FTD</fol-indicator>" +
            "			<lmg-email-signature />" +
            "			<qms-usps-range-record-type />" +
            "			<lmg-email-address />" +
            "			<size-indicator>S</size-indicator>" +
            "			<item-of-the-week-flag>Y</item-of-the-week-flag>" +
            "			<qms-state><![CDATA[AL]]></qms-state>" +
            "			<ariba-ams-project-code />" +
            "			<qms-result-code />" +
            "			<auto-renew />" +
            "			<add-ons />" +
            "			<recip-phone><![CDATA[3140000000]]></recip-phone>" +
            "			<item-extensions />" +
            "			<sunday-delivery-flag />" +
            "			<recip-first-name><![CDATA[Buddy]]></recip-first-name>" +
            "			<recip-last-name><![CDATA[Clark]]></recip-last-name>" +
            "			<recip-phone-ext />" +
            "			<recip-address1><![CDATA[100 Regal]]></recip-address1>" +
            "			<recip-address2 />" +
            "			<recip-city><![CDATA[Enterprise]]></recip-city>" +
            "			<recip-state><![CDATA[AL]]></recip-state>" +
            "			<recip-postal-code><![CDATA[36330]]></recip-postal-code>" +
            "			<recip-country><![CDATA[US]]></recip-country>" +
            "			<recip-international />" +
            "			<taxes />" +
            "			<shipping-method>"+item.shippingMethod+"</shipping-method>" +
            "			<product-substitution-acknowledgement />" +
            "			<sender-release-flag />" +
            "			<extra-shipping-fee />" +
            "			<lmg-flag />" +
            "			<ship-to-type-name />" +
            "			<personal-greeting-id />" +
            "" +
            "			<item-source-code>"+defaultSourceCode+"</item-source-code>" +
            "			<ariba-po-number />" +
            "			<ariba-cost-center />" +
            "			<occassion>13</occassion>" +
            "			<first-color-choice />" +
            "			<ariba-unspsc-code>empty</ariba-unspsc-code>" +
            "			<card-message><![CDATA[Happy Halloween!  We love you and thought you might like a sweet treat without any kind of trick :)  Love Jeff, Cheri, Jack, Ella and Evan]]></card-message>" +
            "			<second-delivery-date />" +
            "			<ship-to-type-info />" +
            "			" +
            "			<delivery-date>" + getDeliveryDateString() + "</delivery-date>" +
            "			<ship-to-type><![CDATA[R]]></ship-to-type>" +
            "			<special-instructions />" +
            "			<tax-amount>0.00</tax-amount>" +
            "			<card-signature />" +
            "           <address-verification>" +
            "           <recip-avs-performed>Y</recip-avs-performed>" +
            "           <recip-address-verification-result>PASS</recip-address-verification-result>" +
            "           <recip-address-verification-override-flag>N</recip-address-verification-override-flag>" +
            "           <avs-street-address><![CDATA[100 Regal]]></avs-street-address>" +
            "           <avs-city><![CDATA[Enterprise]]></avs-city>" +
            "           <avs-state><![CDATA[AL]]></avs-state>" +
            "           <avs-postal-code><![CDATA[36330]]></avs-postal-code>" +
            "           <avs-country>US</avs-country>" +
            "           <avs-entity-type>address</avs-entity-type>" +
            "           <avs-longitude>LONGITUDE_HERE</avs-longitude>" +
            "           <avs-latitude>LATITUDE_HERE</avs-latitude>" +
            "           <avs-address-scores>" +
            "               <avs_score_low_confidence>0</avs_score_low_confidence>" +
            "               <avs_score_high_confidence>1</avs_score_high_confidence>" +
            "               <avs_score_address_type_address>0</avs_score_address_type_address>" +
            "               <avs_score_zip_different>0</avs_score_zip_different>" +
            "               <avs_score_address_line_different>0</avs_score_address_line_different>" +
            "               <avs_score_medium_confidence>0</avs_score_medium_confidence>" +
            "               <avs_score_address_type_other>0</avs_score_address_type_other>" +
            "               <avs_score_zipplusfour_different>0</avs_score_zipplusfour_different>" +
            "               <avs_score_city_different>0</avs_score_city_different>" +
            "               <avs_score_state_different>0</avs_score_state_different>" +
            "           </avs-address-scores>" +
            "           </address-verification>" +
            "		</item>";
        }

        xml+=   "	</items>" +
        "	<guid />" +
        "</order>";


        return xml;
    }

    
    /**
     * Generate the master order number using the order number generator. 
     * This will generate a web order# (I think) - at least it isn't an OE order.
     * @return
     * @throws Exception 
     */   
    private String generateMasterOrderNumber() throws Exception
    {
        UpdateOrderDAO uoDAO = null;
        uoDAO = new UpdateOrderDAO(connection);
            
        //Next available sequence number for the master order 
        String masterOrderNumber = uoDAO.getMasterOrderSeqNum();

        if (StringUtils.isNotEmpty(masterOrderNumber))
            masterOrderNumber = "MJ" + masterOrderNumber; // if we don't use J the xml is not correct
        else
            throw new Exception("An empty sequence number was returned while creating the Master Order Number.");
        
        return masterOrderNumber;
    }

    /**
     * Generate the order number for a single order item using the order number generator. This will generate a web order#.
     * @return
     * @throws Exception 
     */
    private String generateOrderNumberRoot() throws Exception
    {
        UpdateOrderDAO uoDAO = null;
        uoDAO = new UpdateOrderDAO(connection);
        
        //Next available sequence number for the external order 
        String singleOrderNumber = uoDAO.getExternalOrderSeqNum();

        if (StringUtils.isNotEmpty(singleOrderNumber))
            singleOrderNumber = "CJ" + singleOrderNumber; // if we don't add J the xml is not correct
        else
            throw new Exception("An empty sequence number was returned while creating the External Order Number.");
        
        return singleOrderNumber;
    }
    

    private String getDeliveryDateString()
    {
        Calendar cal = TestUtilities.midnight();

        if (isCreateCleanOrder())
        {
            cal.add(Calendar.DAY_OF_YEAR, 7);
        }
        else
        {
            cal.add(Calendar.DAY_OF_YEAR, -1);            
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String delDate = sdf.format(cal.getTime());

        return delDate;
    }


    public double getOrderTotal() {		
        double total = 0;
        for (Product product : (List<Product>)products) {
            total = total + product.getTotalCost();
        }
        return total;
    }

    private void addPayment(Payment payment)
    {
        // only allows one payment per type - if that changes, just make this a list instead of a hashmap
        payments.put(payment.getPaymentType(), payment);
    }

    private void resetPayments()
    {
        payments.clear();
    }

    private void resetOrderProducts() throws Exception
    {
        products.clear();
        orderNumberRoot = generateOrderNumberRoot();
    }

    public List<Product> getOrderProducts()
    {
        return products;
    }


    private Product addOrderItem(Product item) throws Exception
    {
        if (products.size() == 0)
        {
            orderNumberRoot = generateOrderNumberRoot();
        }
        // we set the order number here as sometimes the client will want to know the order number
        item.setOrderNumber(orderNumberRoot+ (products.size()+1));
        products.add(item);
        return item;
    }

    public boolean isCreateCleanOrder()
    {
        return createCleanOrder;
    }

    private void setCreateCleanOrder(boolean createCleanOrder)
    {
        this.createCleanOrder = createCleanOrder;
    }

    /* 
     * TODO: hardcoded to a specific address right now. Should be updated to use the commented out query. osp user cannot see these 
     * tables though so the query doesn't work. changed to use chill user - beware.
     * 
     * Also, free shipping can be affected by source - this is again hardcoded, but shouldn't be.
     * 
     * Lastly, not all products support FS but that is hard to get around since clients are specifying their own products.
     */
    private void setFreeShippingOn(boolean freeShip) throws SQLException
    {
        if (freeShip)
        {
            String sqlQuery = "select ae.EMAIL_ADDRESS AS address from ACCOUNT.ACCOUNT_EMAIL ae, ACCOUNT.ACCOUNT_PROGRAM ap " +
            "where ae.ACCOUNT_MASTER_ID = ap.ACCOUNT_MASTER_ID and ap.ACCOUNT_PROGRAM_STATUS = 'A' and ae.ACTIVE_FLAG = 'Y'  and ap.expiration_date > sysdate and rownum = 1";

            emailAddress = (String)TestUtilities.getSQLResult(connection, sqlQuery, "address");
            //emailAddress = "spatsa@sodium.ftdi.com";
            defaultSourceCode = "514";
        }
        else
        {
            emailAddress = "chris@orderbuildertest.com";
        }
    }
}
