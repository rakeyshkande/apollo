package com.ftd.order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ftd.order.payment.CreditCardPayment;
import com.ftd.order.profile.OrderProfile;
import com.ftd.utils.ApolloTester;
import com.ftd.utils.TestUtilities;

/**
 * Tool to setup a generic cart as follows:
 * - a specified number of products, all the same kind
 * - with cc payment only
 * - into clean
 * - no free shipping
 * It will wait until the order is in clean (or times out) before exiting
 *
 */
public class CleanOrderCreator extends ApolloTester
{
    List<Product> products = new ArrayList<Product>();
    String masterOrderNumber;            

    public static void main(String args[])
    {
        CleanOrderCreator creator = new CleanOrderCreator();
        try
        {
            creator.createOrderToClean(setupConnection(), 2);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.exit(0);
    }

    /**
     * A convenience for junits and other testing
     * @param connection
     * @param numProducts
     * @return
     * @throws Exception
     */
    public boolean createOrderToClean(Connection connection, int numProducts) throws Exception
    {
        // build order   
        createOrder(numProducts);

        // wait for it to get to clean
        OrderProcessor proc = new OrderProcessor();
        return proc.isOrderClean(connection, products);
    }
        
    private void createOrder(int numProducts) throws Exception
    {
        OrderProfile profile = new OrderProfile();
        profile.setCreateCleanOrder(true);
        profile.setFreeShipping(false);
        
        for (int i=0; i < numProducts; i++)
        {
            profile.addProduct(Product.getDefaultBearProduct());
        }
        
        double amt = profile.getOrderTotal();
        CreditCardPayment ccp = new CreditCardPayment(null, null, amt, true);
        profile.addPayment(ccp);

        OrderBuilder builder = new OrderBuilder();
        masterOrderNumber = builder.submitOrder(profile);  
        products = builder.getOrderProducts();
    }

    public List<Product> getProducts()
    {
        return products;
    }

    public String getMasterOrderNumber()
    {
        return masterOrderNumber;
    }
 
}
