package com.ftd.order.payment;

import java.sql.Connection;

/**
 * @author chill
 *
 */
public abstract class Payment
{
    protected String paymentType = null;
    public static final String GiftCertificatePaymentType = "GC";
    public static final String GiftCardPaymentType = "GD";
    public static final String CreditCardPaymentType = "CC";
    public static final String BillMeLaterPaymentType = "BM";
    
    protected double amount;
    protected String accountNumber;
    
    public void charge(Connection conn) throws Exception
    {
        return;
    }
    public abstract String generateOrderXML();

    
    public String getPaymentType()
    {
        return paymentType;
    }
    public void setPaymentType(String paymentType)
    {
        this.paymentType = paymentType;
    }
    public double getAmount()
    {
        return amount;
    }
    public void setAmount(double amount)
    {
        this.amount = amount;
    }
    public String getAccountNumber()
    {
        return accountNumber;
    }
    protected void setAccountNumber(String acctnum)
    {
        this.accountNumber = acctnum;
    }

}
