package com.ftd.order.payment;

public class CreditCardPayment extends Payment
{
    protected boolean use = false;
    private String cardType;
    
    public CreditCardPayment(String cardnum, String cardtype, double amt, boolean use)
    {
        paymentType = CreditCardPaymentType;
        if (cardnum == null && use)
        {
            // default card # and type
            setAccountNumber("4444333322221111");
            setCardType("Visa");
        }
        setAmount(amt);
        setUse(use);
    }
    
    public boolean isUse()
    {
        return use;
    }
    public void setUse(boolean use)
    {
        this.use = use;
    }

    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public String getCardType()
    {
        return cardType;
    }

    @Override
    public String generateOrderXML()
    {
        String xml = "";

        if (isUse()) {
            xml+="      <cc-approval-amt>"+getAmount()+"</cc-approval-amt> " +
            "       <cc-number>"+getAccountNumber()+"</cc-number>" +
            "       <cc-type>"+getCardType()+"</cc-type>" +
            "       <cc-avs-result />" +
            "       <cc-exp-date>11/15</cc-exp-date>" +
            "       <cc-approval-action-code>000</cc-approval-action-code>" +
            "       <cc-approval-code />" +
            "       <cc-acq-data>aWb161332488045973cQR5Pd5e00fC</cc-acq-data>" +
            "       <csc-response-code >M</csc-response-code>" +
            "       <csc-validated-flag>Y</csc-validated-flag>" +
            "       <cc-approval-verbage>AP</cc-approval-verbage> ";
        } else {
            xml+= "<cc-type /><cc-number /><cc-approval-amt /><cc-approval-verbage /><csc-validated-flag>N</csc-validated-flag><cc-approval-code />";
        }
        return xml;
    }

}
