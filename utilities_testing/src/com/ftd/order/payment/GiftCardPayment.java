package com.ftd.order.payment;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.ftd.customerordermanagement.bo.GiftCardBO;
import com.ftd.customerordermanagement.vo.PaymentVO;
import com.ftd.mo.exceptions.GiftCardException;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.id.vo.IdRequestVO;

public class GiftCardPayment extends Payment
{
    private String pin;
    private String transactionID;
    
    /**
     * should this payment be authorized/charged when an order is created?
     */
    private boolean auth = true;

    /**
     * map of supported gift cards and their respective pins
     */
    private static Map <String, String>giftCardToPin = new HashMap<String, String>() {{
        put("6006492147013900373", "6611");
        put("6006492147013900381", "1167");
        put("6006492147013900399", "4305");
        put("6006492147013900407", "2379");
        put("6006492147013900415", "8085");
        put("6006492147013900423", "1396");
        put("6006492147013900431", "6107");
        put("6006492147013900449", "2018");
        put("6006492147013900456", "6807");
        put("6006492147013900464", "5530");
        put("6006492147013900472", "0453");
        put("6006492147013900480", "5482");
        put("6006492147013900498", "3898");
        put("6006492147013900506", "1371");
        put("6006492147013900514", "0763");
    }};
    
    public GiftCardPayment(String cardnum, double amt)
    {
        paymentType = GiftCardPaymentType;
        if (cardnum != null)
        {
            setAccountNumber(cardnum);
        }
        else
        {
            // provide a default option - use the first card
            setAccountNumber(giftCardToPin.keySet().iterator().next());
        }
        setAmount(amt);
    }
    
    /**
     * implementation of the charge method to authorize a gift card ("preauth"/not settled)
     */
    @Override
    public void charge(Connection conn) throws Exception
    {
        if (isAuth())
        {
            GiftCardBO giftCardBO = initGiftCard(conn);

            PaymentVO pmt = new PaymentVO();
            pmt.setCardNumber(getAccountNumber());
            pmt.setGiftCardPin(getPin());
            pmt.setCreditAmount(getAmount());
            giftCardBO.authorizePayment(pmt);

            setTransactionID(pmt.getTransactionId());
        }
    }
    
    /**
     * Refund a payment - perhaps this should settle and refund? 
     * @param conn
     * @throws Exception
     */
    public void refund(Connection conn) throws Exception
    {
        GiftCardBO giftCardBO = initGiftCard(conn);

        PaymentVO pmt = new PaymentVO();
        pmt.setCardNumber(getAccountNumber());
        pmt.setGiftCardPin(getPin());
        pmt.setCreditAmount(getAmount());
        giftCardBO.refundPayment(pmt);
    }

    /**
     * Generate the orderXML segment for a gift card payment
     */
    @Override
    public String generateOrderXML()
    {
        return
        "  <gift-certificates>" +
        "           <gift-certificate>" +
        "               <payment-type>GD</payment-type>" +
        "               <transaction-id><![CDATA["+getTransactionID()+"]]></transaction-id>" +
        "               <amount><![CDATA["+getAmount()+"]]></amount>" +
        "               <pin><![CDATA["+getPin()+"]]></pin>" +
        "               <number><![CDATA["+getAccountNumber()+"]]></number>" +
        "           </gift-certificate>" +
        "       </gift-certificates>";        
    }
    
    
    private GiftCardBO initGiftCard(Connection conn) throws GiftCardException, Exception
    {
        GiftCardBO gbo = new GiftCardBO(conn);
        return gbo;
    }


    /**
     * Set the gift card number and pin that goes with it
     */
    @Override
    protected void setAccountNumber(String cardNumber)
    {
        this.accountNumber = cardNumber;
        pin = giftCardToPin.get(cardNumber);
    }
 
    public String getPin()
    {
        return pin;
    }

    public void setPin(String pin)
    {
        this.pin = pin;
    }

    public String getTransactionID()
    {
        return transactionID;
    }

    public void setTransactionID(String transactionID)
    {
        this.transactionID = transactionID;
    }
    
    public static Set <String>getGiftCards()
    {
        return giftCardToPin.keySet(); 
    }

    public boolean isAuth()
    {
        return auth;
    }

    public void setAuth(boolean auth)
    {
        this.auth = auth;
    }

}
