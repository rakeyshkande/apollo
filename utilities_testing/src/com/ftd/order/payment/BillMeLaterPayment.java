package com.ftd.order.payment;


public class BillMeLaterPayment extends Payment
{
    private String authorizationID;
    private String orderNumber;
   
    public BillMeLaterPayment(String acctnum, double amt, String authid, String ordernum)
    {
        paymentType = BillMeLaterPaymentType;
        setAccountNumber(acctnum);
        setAmount(amt);
        setAuthorizationID(authid);
        setOrderNumber(ordernum);
    }
    
    @Override
    public String generateOrderXML()
    {
        return
        "       <alt-pay-method>" +
        "           <approval-amount><![CDATA["+getAmount()+"]]></approval-amount>" +
        "           <account-number><![CDATA["+getAccountNumber()+"]]></account-number>" +
        "           <miles-points />" +
        "           <redemption-rate />" +
        "           <auth-id><![CDATA["+getAuthorizationID()+"]]></auth-id>" +
        "           <payment-type>BM</payment-type>" +
        "           <order-number><![CDATA["+getOrderNumber()+"]]></order-number>" +
        "       </alt-pay-method>";
    }
    
    private void setAuthorizationID(String authorizationID)
    {
        this.authorizationID = authorizationID;
    }

    public String getAuthorizationID()
    {
        return authorizationID;
    }

    private void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

}
