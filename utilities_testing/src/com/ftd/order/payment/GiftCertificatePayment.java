package com.ftd.order.payment;

import java.sql.Connection;

public class GiftCertificatePayment extends Payment
{
    Connection conn; // only needed if we are going to redeem the GC
   
    public GiftCertificatePayment(Connection conn, String certificateNum, double amt)
    {
        paymentType = GiftCertificatePaymentType;
        this.conn = conn;
        setAccountNumber(certificateNum);
        setAmount(amt);
    }
    
    @Override
    public String generateOrderXML()
    {
        return
        "  <gift-certificates>" +
        "           <gift-certificate>" +
        "               <amount><![CDATA["+getAmount()+"]]></amount>" +
        "               <number><![CDATA["+getAccountNumber()+"]]></number>" +
        "           </gift-certificate>" +
        "       </gift-certificates>";
    }

}
