package com.ftd.order;

import javax.swing.JOptionPane;

import com.ftd.order.payment.CreditCardPayment;
import com.ftd.order.payment.GiftCardPayment;
import com.ftd.order.profile.OrderProfile;
import com.ftd.order.profile.OrderProfileController;
import com.ftd.utils.TestUtilities;


public class OrderClient
{
    boolean cleanOrder, freeShipping, bearItem, specialtyItem, floralItem, ccPayment, gdPayment;
    double gdPaymentAmount = 0.0;
    String gdCard = null;

    static OrderBuilder builder;

    /**
     * 
     * @param args - use "-b" to run in batch mode, i.e. not to prompt the user but use hard-coded configs.
     * @throws Exception
     */
    public static void main(String args[])
    {
        builder = new OrderBuilder();
        OrderProfile profile;
        
        if (args.length > 0 && args[0].equals("-b"))
        {
            // run in batch mode with default configurations
            // these defaults can be programmatically changed for test usage - they are presented
            // here for ease of testing
            profile = new OrderProfile(
                    true, 
                    false, 
                    new CreditCardPayment(null, null, Product.getDefaultBearProduct().getTotalCost() - 5, true), 
                    Product.getDefaultBearProduct()); 
        }
        else 
        {
            // prompt user for order details
            OrderClient client = new OrderClient();
            profile = client.getOrderProfileInteractive();
        }
        
        if (profile != null)
        {
            OrderBuilder builder = new OrderBuilder();
            try
            {
                builder.submitOrder(profile);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } 
        }

        System.exit(0);
    }    
    
    private OrderProfile getOrderProfileInteractive()
    { 
        OrderProfileController profileController = new OrderProfileController();

        // display dialog and get data
        // we can use a simple dialog for now since there aren't that many fields on the form. If that grows something more sophisticated may be needed.
        int ret = JOptionPane.showConfirmDialog(null, profileController.getView(), "Order Options", JOptionPane.OK_CANCEL_OPTION);
        if ( ret != JOptionPane.OK_OPTION )
        {
            return null;
        }

        return profileController.setProfile();
    }


}
