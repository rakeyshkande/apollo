package com.ftd.order;

import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TestProduct
{
    @Test
    public void testGetTotalCost()
    {
        Product bear = Product.getDefaultBearProduct();
        double total = bear.getTotalCost();
        assertTrue(32.98 == total);
    }
}
