package com.ftd.order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ftd.utils.TestUtilities;

public class OrderProcessor
{
    
    public boolean isOrderClean(Connection connection, List<Product> products) throws SQLException
    {
        String query = "select order_disp_code AS status from clean.order_details od where od.external_order_number = ";
        Set<Integer> cleanProducts = new HashSet<Integer>();

        int numTries = 100; 
        int sleepms = 2000;

        while (cleanProducts.size() < products.size() && numTries-- > 0)
        {
            System.out.println("Checking order status. Will try for another " + (sleepms * numTries-1)/1000 + " seconds.");
            for (int i = 0; i < products.size(); i++)
            {
                if (cleanProducts.contains(i))
                {
                    continue;
                }
                // see if item is clean
                String q = query + "'" + products.get(i).getOrderNumber() + "'";
                String status = (String)TestUtilities.getSQLResult(connection, q, "status");
                System.out.println("Item " + products.get(i).getOrderNumber() + " is " + status + ".");
                if (status != null && status.equals("Processed"))
                {
                    // clean
                    cleanProducts.add(i);
                }
            }
            try {Thread.sleep(sleepms);} catch (InterruptedException e){}
        }

        return cleanProducts.size() == products.size();
    }


    
    /**
     * Set order status to processed
     * @throws SQLException 
     */
    public void setOrderToProcessed(Connection connection, String extOrderNum) throws SQLException
    {
        String query = "update clean.order_details od set order_disp_code = 'Processed' where od.external_order_number = '" + extOrderNum + "'";
        TestUtilities.executeSQL(connection, query);        
    }

    /**
     * Set order status to hold
     * @throws SQLException 
     */
    public void setOrderToHold(Connection connection, String extOrderNum) throws SQLException
    {
            String query = "update clean.order_details od set order_disp_code = 'Held' where od.external_order_number = '" + extOrderNum + "'";
            TestUtilities.executeSQL(connection, query);
    }

    /**
     * Set order status to validated
     */
    public void setOrderToValidated(Connection connection, String extOrderNum) throws SQLException
    {
        String query = "update clean.order_details od set order_disp_code = 'Validated' where od.external_order_number = '" + extOrderNum + "'";
        TestUtilities.executeSQL(connection, query);
    }

    /**
     * Clean an order in scrub and move it to clean/dispatch it
     */
    public void scrubOrderToClean()
    {
        // no idea!
    }
}
