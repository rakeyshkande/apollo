package com.ftd.order;

import javax.swing.JCheckBox;
import javax.swing.JPanel;


public class Product {
    protected String productId;
    protected double price;
    protected double serviceFee = 0.0;
    protected double shippingFee = 0.0;
    protected String shippingMethod = "";
    protected String orderNumber;

    public String getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }
    public String getProductId()
    {
        return productId;
    }
    public void setProductId(String productId)
    {
        this.productId = productId;
    }
    public double getPrice()
    {
        return price;
    }
    public void setPrice(double price)
    {
        this.price = price;
    }
    public double getServiceFee()
    {
        return serviceFee;
    }
    public void setServiceFee(double serviceFee)
    {
        this.serviceFee = serviceFee;
    }
    public double getShippingFee()
    {
        return shippingFee;
    }
    public void setShippingFee(double shippingFee)
    {
        this.shippingFee = shippingFee;
    }
    public String getShippingMethod()
    {
        return shippingMethod;
    }
    public void setShippingMethod(String shippingMethod)
    {
        this.shippingMethod = shippingMethod;
    }

    public double getTotalCost() {     
        double total = 0;
        total = total + price + serviceFee + shippingFee;
        return total;
    }


    public static Product getDefaultBearProduct()
    {
        return new Product() {{
            productId = "BB53";
            price = 19.99;
            serviceFee = 12.99;
            shippingMethod = "GR";
        }
        };
    }

    public static Product getDefaultSpecialtyGiftProduct()
    {
        return new Product() {{
            productId = "GC01";
            price = 75.00;
            shippingFee = 10.99;
            shippingMethod = "GR";
        }};
    }

    public static Product getDefaultExpensiveFloralProduct()
    {
        return new Product() {{
            productId = "8212";
            price = 199.00;
            shippingFee = 15.99;
            shippingMethod = "GR";
        }};
    }

}