package com.ftd.order.profile;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class OrderProfilePanel extends JPanel
{
    private ProductView productView;
    private PaymentView paymentView;
    private MiscOptionsView miscOptionsView;
    
    private List<ProfileView> views;

    public void setup()
    {        
        views = new ArrayList<ProfileView>();
        
        // create the GUI
        this.setLayout(new BorderLayout());

        JPanel mainPanel = new JPanel(); 
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        miscOptionsView = new MiscOptionsView();
        views.add(miscOptionsView);
        
        mainPanel.add(miscOptionsView);
        mainPanel.add(Box.createVerticalStrut(15)); // a spacer

        // add products
        productView = new ProductView();
        mainPanel.add(productView);        
        mainPanel.add(Box.createVerticalStrut(15)); // a spacer
        views.add(productView);

        paymentView = new PaymentView();
        views.add(paymentView);

        // add payment section
        this.add(mainPanel, BorderLayout.NORTH);
        this.add(paymentView, BorderLayout.CENTER);

        return;
    }
    
    public List<ProfileView> getViews()
    {
        return views;
    }


}
