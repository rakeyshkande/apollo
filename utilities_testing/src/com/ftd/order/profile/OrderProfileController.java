package com.ftd.order.profile;

import java.util.List;

import javax.swing.JPanel;

public class OrderProfileController
{
    private OrderProfile profile;
    private OrderProfilePanel mainPanel;

    public OrderProfileController()
    {
        // reset profile
        profile = new OrderProfile();
        mainPanel = new OrderProfilePanel();
        mainPanel.setup();
    }
        
    public OrderProfile setProfile()
    {
        List <ProfileView>views = mainPanel.getViews();
        
        for (ProfileView view : views)
        {
            view.populateProfile(profile);
        }
               
        return profile;
    }
    
    public JPanel getView()
    {
        return mainPanel;
    }
    
}
