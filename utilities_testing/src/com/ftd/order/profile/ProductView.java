package com.ftd.order.profile;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import com.ftd.order.Product;

public class ProductView extends ProfileView
{
    private JCheckBox defaultBearBox;
    private JCheckBox defaultSpecialtyBox;
    private JCheckBox defaultLargeFloralBox;


    public ProductView()
    {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Select Products"));

        defaultBearBox = new JCheckBox("Order Bear");
        defaultSpecialtyBox = new JCheckBox("Order Specialty Item");
        defaultLargeFloralBox = new JCheckBox("Order Expensive Floral Item");

        this.add(defaultBearBox);
        this.add(defaultSpecialtyBox);
        this.add(defaultLargeFloralBox);
    }
    
    public void populateProfile(OrderProfile profile)
    {
        if (isDefaultBear())
        {
            profile.addProduct(Product.getDefaultBearProduct());
        }
        if (isDefaultSpecialty())
        {
            profile.addProduct(Product.getDefaultSpecialtyGiftProduct());
        }
        if (isDefaultLargeFloral())
        {
            profile.addProduct(Product.getDefaultExpensiveFloralProduct());
        }
    }

    private boolean isDefaultBear()
    {
        return defaultBearBox.isSelected();
    }

    private boolean isDefaultSpecialty()
    {
        return defaultSpecialtyBox.isSelected();
    }

    private boolean isDefaultLargeFloral()
    {
        return defaultLargeFloralBox.isSelected();
    }

}