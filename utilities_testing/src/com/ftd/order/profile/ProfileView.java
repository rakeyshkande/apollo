package com.ftd.order.profile;

import javax.swing.JPanel;

public abstract class ProfileView extends JPanel
{
    public abstract void populateProfile(OrderProfile profile);

}
