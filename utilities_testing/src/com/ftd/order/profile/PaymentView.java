package com.ftd.order.profile;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import com.ftd.order.payment.CreditCardPayment;
import com.ftd.order.payment.GiftCardPayment;

public class PaymentView extends ProfileView
{
    private JCheckBox ccPaymentBox;
    private JCheckBox gdPaymentBox;
    private JTextField gdAmountTF;
    private JComboBox gdCardCB;
    
    private JCheckBox gdAuthBox;

    public PaymentView()
    {
        super();
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Select Payment Methods"));

        ccPaymentBox = new JCheckBox("Pay With Default Credit Card");        
        gdPaymentBox = new JCheckBox("Pay With Selected Gift Card");
        gdAuthBox = new JCheckBox("Authorize Gift Card");
        gdAmountTF = new JTextField(5);
        gdCardCB = new JComboBox(GiftCardPayment.getGiftCards().toArray());
        
        JPanel giftCardPanel1 = new JPanel();
        giftCardPanel1.setLayout(new BoxLayout(giftCardPanel1, BoxLayout.Y_AXIS));
        // cc checkbox
        giftCardPanel1.add(ccPaymentBox);
        giftCardPanel1.add(Box.createVerticalStrut(15)); // a spacer
        giftCardPanel1.add(new JSeparator()); // a spacer
        giftCardPanel1.add(Box.createVerticalStrut(15)); // a spacer
        // gd checkbox
        giftCardPanel1.add(gdAuthBox);
        giftCardPanel1.add(gdPaymentBox);

        // gd card combo box and amount
        JPanel giftCardPanel2 = new JPanel();
        giftCardPanel2.setLayout(new BoxLayout(giftCardPanel2, BoxLayout.X_AXIS));
        giftCardPanel2.add(gdCardCB);
        // gd amount
        giftCardPanel2.add(new JLabel("Amount To Charge Gift Card "));
        giftCardPanel2.add(gdAmountTF);

        this.add(giftCardPanel1, BorderLayout.NORTH);
        this.add(giftCardPanel2, BorderLayout.SOUTH);
    }
    
    public void populateProfile(OrderProfile profile)
    {
        boolean gdPayment = isGiftCardPayment();
        double gdPaymentAmount = 0.0;
        String gdCard = null;
        if (gdPayment)
        {
            // default amount to full order if not specified
            String gdamt = getGiftCardAmount();
            if (gdamt == null || gdamt.equals(""))
            {
                // defaults to order total
                gdPaymentAmount = profile.getOrderTotal();
            }
            else
            {
                gdPaymentAmount = new Double(gdamt);
            }
            gdCard = (String) getGiftCardNumber();

            GiftCardPayment gdp = new GiftCardPayment(gdCard, gdPaymentAmount);
            gdp.setAuth(isGiftCardAuth());
            profile.addPayment(gdp);
        }
        
        if (isCreditCardPayment())
        {
            double amt = profile.getOrderTotal();
            if (gdPayment)
            {
                amt -= gdPaymentAmount; 
            }
            CreditCardPayment ccp = new CreditCardPayment(null, null, amt, true);
            profile.addPayment(ccp);
        }
    }
    
    private boolean isCreditCardPayment()
    {
        return ccPaymentBox.isSelected();
    }

    private boolean isGiftCardPayment()
    {
        return gdPaymentBox.isSelected();
    }

    private String getGiftCardAmount()
    {
        return gdAmountTF.getText();
    }

    private String getGiftCardNumber()
    {
        return (String)gdCardCB.getSelectedItem();
    }
    
    private boolean isGiftCardAuth()
    {
        return gdAuthBox.isSelected();
    }
}
