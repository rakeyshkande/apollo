package com.ftd.order.profile;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class MiscOptionsView extends ProfileView
{
    private JCheckBox cleanBox;
    private JCheckBox freeBox;

    public MiscOptionsView()
    {
        this.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Select Order Options"));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // create the input fields
        cleanBox = new JCheckBox("Send Order To Clean");
        freeBox = new JCheckBox("Apply Free Shipping");
        
        this.add(cleanBox);
        this.add(freeBox);
    }


    public void populateProfile(OrderProfile profile)
    {
            profile.setCreateCleanOrder(isCreateCleanOrder());
            profile.setFreeShipping(isFreeShipping());
    }

    private boolean isCreateCleanOrder()
    {
        return cleanBox.isSelected();
    }

    private boolean isFreeShipping()
    {
        return freeBox.isSelected();
    }
   
}
