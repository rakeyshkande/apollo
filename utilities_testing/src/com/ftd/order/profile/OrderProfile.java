package com.ftd.order.profile;

import java.util.ArrayList;
import java.util.List;

import com.ftd.order.Product;
import com.ftd.order.payment.GiftCardPayment;
import com.ftd.order.payment.Payment;

public class OrderProfile
{    
    private boolean clean;
    private boolean freeShipping;
    private List<Payment>payments = new ArrayList<Payment>();
    private List<Product>products = new ArrayList<Product>();
    
    // there are times when it would be convenient to quickly get the gift card payment from a profile
    private GiftCardPayment giftCardPayment = null;

    /**
     * Default constructor
     */
    public OrderProfile()
    {
    }

    /**
     * A quick and dirty constructor for ease of testing/use
     * @param clean
     * @param freeShipping
     * @param payment
     * @param product
     */
    public OrderProfile(boolean clean, boolean freeShipping, Payment payment, Product product)
    {
        setCreateCleanOrder(clean);
        setFreeShipping(freeShipping);
        addPayment(payment);
        addProduct(product);
    }

    public boolean isCreateCleanOrder()
    {
        return clean;
    }
    public void setCreateCleanOrder(boolean clean)
    {
        this.clean = clean;
    }
    public boolean isFreeShipping()
    {
        return freeShipping;
    }
    public void setFreeShipping(boolean freeShipping)
    {
        this.freeShipping = freeShipping;
    }
    public List<Payment> getPayments()
    {
        return payments;
    }
    public List<Product> getProducts()
    {
        return products;
    }
    public void addPayment(Payment payment)
    {
        payments.add(payment);
        if (payment instanceof GiftCardPayment)
        {
            giftCardPayment = (GiftCardPayment) payment;
        }
    }
    
    /**
     * convenience method so clients don't have to search payment list for gift card payment
     * @return
     */
    public GiftCardPayment getGiftCardPayment()
    {
        return giftCardPayment;
    }

    public void addProduct(Product product)
    {
        products.add(product);
    }

    public void resetPayments()
    {
        payments.clear();
    }

    public void resetProducts()
    {
        products.clear();
    }

    /**
     * Return total cost of the order based on the products added to it.
     * @return
     */
    public double getOrderTotal() {     
        double total = 0;
        for (Product product : (List<Product>)products) {
            total = total + product.getTotalCost();
        }
        return Math.round((total) * 100) / 100;
    }

}
