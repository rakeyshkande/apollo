classes12.jar (1,621,582 bytes) - for use with JDK 1.2 and JDK 1.3
ojdbc14.jar (1,569,316 bytes) - classes for use with JDK 1.4 and 1.5

Note: JDeveloper and the OC4J Containers use ojdbc14.jar. 

Note:
If you see the following error with CachedResultSet, it is because of classes12.jar. Switch to ojdbc14.jar.
java.lang.ClassCastException: oracle.sql.TIMESTAMP
	at com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet.getTimestamp(CachedResultSet.java:315)
