package com.ftd.marssimulator.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.marssimulator.exception.ExcelException;
import com.ftd.marssimulator.util.ExcelUtils;

public class ExcelUtilsTest {

	@Test
	public void testGetMarsVOs() {
		String filePath = "/u02/xlsMARSSimulatorv1000.xlsx";
		File file = new File(filePath);
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);		
			List<MarsVO> marsVOs = ExcelUtils.getMarsVOs(IOUtils.toByteArray(fis));
			System.out.println(marsVOs);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
			Assert.fail();
		} catch (ExcelException e) {
			System.err.println("Unable to parse the Excel file");
			e.printStackTrace();
			Assert.fail();
		} catch (IOException e) {
			System.err.println("Unable to process the file");
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testGetMarsVOsWithNewValidColumns() {
		String filePath = "/u02/xlsMARSSimulatorv1000_with_new_column.xlsx";
		File file = new File(filePath);
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);		
			List<MarsVO> marsVOs = ExcelUtils.getMarsVOs(IOUtils.toByteArray(fis));
			System.out.println(marsVOs);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
			Assert.fail();
		} catch (ExcelException e) {
			System.err.println("Unable to parse the Excel file");
			e.printStackTrace();
			Assert.fail();
		} catch (IOException e) {
			System.err.println("Unable to process the file");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testGetMarsVOsWithRej() {
		String filePath = "/u02/xlsMARSSimulatorv1000_with_REJ.xlsx";
		File file = new File(filePath);
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);		
			List<MarsVO> marsVOs = ExcelUtils.getMarsVOs(IOUtils.toByteArray(fis));
			System.out.println(marsVOs);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
			Assert.fail();
		} catch (ExcelException e) {
			System.err.println("Unable to parse the Excel file");
			e.printStackTrace();
			Assert.fail();
		} catch (IOException e) {
			System.err.println("Unable to process the file");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testGetMarsVOsWithInvalidColumn() {
		String filePath = "/u02/xlsMARSSimulatorv1000_with_invalid_column.xlsx";
		File file = new File(filePath);
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);		
			List<MarsVO> marsVOs = ExcelUtils.getMarsVOs(IOUtils.toByteArray(fis));
			System.out.println(marsVOs);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
			Assert.fail();
		} catch (ExcelException e) {
			System.err.println("Unable to parse the Excel file");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Unable to process the file");
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testGetMarsVOsWithInvalidData() {
		String filePath = "/u02/xlsMARSSimulatorv1000_with_invalid_data.xlsx";
		File file = new File(filePath);
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);		
			List<MarsVO> marsVOs = ExcelUtils.getMarsVOs(IOUtils.toByteArray(fis));
			System.out.println(marsVOs);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
			Assert.fail();
		} catch (ExcelException e) {
			System.err.println("Unable to parse the Excel file");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Unable to process the file");
			e.printStackTrace();
			Assert.fail();
		}
	}

}
