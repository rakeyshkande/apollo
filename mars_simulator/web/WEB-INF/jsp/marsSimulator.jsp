<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
	<head>
		<title>MARS Simulator</title>
		<link rel="stylesheet" type="text/css" href="resources/css/ftdAdmin.css"/>
		<link rel="stylesheet" type="text/css" href="resources/css/ftd.css"/>
		<style>
			#resultsTHId {
			  width : 12.5%;
			  background-color: #006699;
			  color: white;
			  font-weight: bold;
			}
			.resultsClass {
				border: 1px solid black;
				border-collapse: collapse;
			}
			#resultsTableId {
			border: 1px solid black;
				border-collapse: collapse;
				width: 100%;
			}
			.resultsCellEven{
				background-color: #AAC9D4
			}

			.resultsCellClass {
				text-align: center;
				padding: 8px;
			}
			#textMessage {
			  color: red;
			}
		</style>
		<script type="text/javascript" src="resources/js/clock.js"></script>
		<script type="text/javascript" src="resources/js/util.js"></script>
		<script language="javascript">
			
		</script>
	</head>
	<body onload="javascript: init()">
		<form id="marsSimulatorFormId" name="marsSimulatorForm" method="post" action="marsSimulator" enctype="multipart/form-data">
			<input id="securitytoken" type="hidden" name="securitytoken" value=${securitytoken} />
			<input id="context" type="hidden" name="context" value=${context} />
			<input id="adminAction" type="hidden" name="adminAction" value=${adminAction} />
			<input id="sitename" type="hidden" name="sitename" value=${sitename} />
								
			<!-- Header-->
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%" align="left">
						<img border="0" src="resources/images/wwwftdcom_131x32.gif" width="131" height="32"/>
					</td>
					<td width="60%" align="center" colspan="1" class="header">MARS Simulator</td>
					<td width="20%" align="right" class="label">
						<table width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td id="time" align="right" class="label"></td>
								<script type="text/javascript">startClock();</script>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<hr/>
					</td>
				</tr>
			</table>
			<!-- Display table-->
			<table width="99%" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td align="right">
						<input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="300" onClick="javascript:location.href ='http://${sitename}/secadmin/security/Main.do?context=Order%20Proc&securitytoken=${securitytoken}&adminAction=${adminAction}';" />
					</td>
				</tr>
			</table>
  <table width="98%" border="3" align="center" cellpadding="1"
		 cellspacing="1" bordercolor="#006699">
	<tr>
	  <td>
		<table width="98%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td>
					<b>Instructions:</b>
					<ol>
						<li>Enter Rules Version number. If you need the current active version then refer to global parm MARS_CONFIG.VERSION.</li>
						<li>Download the base input file if you need a template to start with.</li>
						<li>Modify the base input file to add your own data. Do not add, remove, or reorder columns.</li>
						<li>Browse and select spreadsheet.</li>
						<li>Review the Rules Version number. Click "Run Simulator" to submit the request.</li>
						<li>The History section will show past simulations.</li>
						<li>The "Replace Input File" button replaces the base input file.</li>
						<li>If done then press Exit to return to the menu..</li>
					</ol>
				</td>
			</tr>
			<tr>
			  <td valign="top">
				<table>
				  <tr>
					<td><label>Rules Version:</label></td>
					<td><input id="rulesVersion" type="text" name="rulesVersion"/></td>
					<td><a href="javascript: downloadBaseFile()">Download Base Input File</a></td>
				  </tr>
				  <tr>
					<td><label>Upload Input File:</label></td>
					<td><input id="fileChooser" style="position: relative" type="file" name="inputFile" /></td>
					<td><input type="button" class="BlueButton" name="RunSimulator" Value="Run Simulator" tabindex="250" onClick="javascript: runSimulator();"/></td>
				  </tr>
				  <tr>
					<td colspan="3">
						<div id="textMessage">
							${message}
						</div>
					</td>
				  </tr>
				</table>
			  </td>
			</tr>
		</table>
	  </td>
	</tr>
	<tr>
		<td>
		<table width="98%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td width="100%" align="center">
						<input type="button" class="BlueButton" name="Refresh" Value="Refresh" tabindex="260" onClick="javascript: refreshPage();"/>
					</td>
			</tr>
			<tr>
			  <td valign="top">
				<table class="resultsClass" id="resultsTableId"  width="100%">
				  <tr>
					<td id="resultsTHId" class="resultsCellClass" style="{width=7%}"><label>User</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=16%}"><label>Simulation Time</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=10%}"><label>Rules Version</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=7%}"><label>Status</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=23%}"><label>Error Message</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=15%}"><label>Input Spreadsheet</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=15%}"><label>Output Spreadsheet</label></td>
					<td id="resultsTHId" class="resultsCellClass" style="{width=7%}"><label>Action</label></td>
				  </tr>
					<c:set var="count" value="0" scope="page" />
					<c:forEach var='resultsVO' items='${resultsVOs}'>
						<tr id="resultsTRId">
							<c:choose>
								<c:when test="${count % 2 == 0}">
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=7%}">${resultsVO.createdBy}</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=16%}"><fmt:formatDate value="${resultsVO.createdOn}" pattern="yyyy-MM-dd hh:mma" /></td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=10%}">${resultsVO.versionNo}</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=7%}">${resultsVO.status}</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=23%}">${resultsVO.errorMessage}</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=15%}">
										<a href="javascript: downloadInputExcelFile(${resultsVO.resultId})">${resultsVO.inputFileName}</a>
									</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=15%}">
										<c:if test="${fn:containsIgnoreCase(resultsVO.status, 'COMPLETED')}">
											<a href="javascript: downloadOutputExcelFile(${resultsVO.resultId})">${resultsVO.outputFileName}</a>
										</c:if>
									</td>
									<td class="resultsCellClass resultsClass resultsCellEven" style="{width=7%}">
										<c:if test="${fn:containsIgnoreCase(resultsVO.status, 'COMPLETED')}">
											<input type="button" class="BlueButton" name="Overwrite" Value="Overwrite Base Input File" tabindex="260" onClick="javascript: overwriteBaseFile(${resultsVO.resultId})" />
										</c:if>
									</td>
								</c:when>
								<c:otherwise>
									<td class="resultsCellClass resultsClass" style="{width=7%}">${resultsVO.createdBy}</td>
									<td class="resultsCellClass resultsClass" style="{width=16%}"><fmt:formatDate value="${resultsVO.createdOn}" pattern="yyyy-MM-dd hh:mma" /></td>
									<td class="resultsCellClass resultsClass" style="{width=10%}">${resultsVO.versionNo}</td>
									<td class="resultsCellClass resultsClass" style="{width=7%}">${resultsVO.status}</td>
									<td class="resultsCellClass resultsClass" style="{width=23%}">${resultsVO.errorMessage}</td>
									<td class="resultsCellClass resultsClass" style="{width=15%}"><a href="javascript: downloadInputExcelFile(${resultsVO.resultId})">${resultsVO.inputFileName}</a></td>
									<td class="resultsCellClass resultsClass" style="{width=15%}">
								
									<c:if test="${fn:containsIgnoreCase(resultsVO.status, 'COMPLETED')}">
										<a href="javascript: downloadOutputExcelFile(${resultsVO.resultId})">${resultsVO.outputFileName}</a>
									</c:if>
									</td>
									<td class="resultsCellClass resultsClass" style="{width=7%}">
										<c:if test="${fn:containsIgnoreCase(resultsVO.status, 'COMPLETED')}">
											<input type="button" class="BlueButton" name="Overwrite" Value="Overwrite Base Input File" tabindex="260" onClick="javascript: overwriteBaseFile(${resultsVO.resultId})" />
										</c:if>
									</td>
								</c:otherwise>
							</c:choose>
							<c:set var="count" value="${count + 1}" scope="page"/>
						</tr>
					</c:forEach>
				</table>
			  </td>
			</tr>
		</table>
		</td>
	</tr>
  </table>
			<table width="99%" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="100%" align="right">
						<input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="300" onClick="javascript:location.href ='http://${sitename}/secadmin/security/Main.do?context=Order%20Proc&securitytoken=${securitytoken}&adminAction=${adminAction}';" />
					</td>
				</tr>
				
			</table>
		</form>
		<script>
			function init()
			{
				document.getElementById("rulesVersion").focus();
				setNavigationHandlers();
			}
			function getSecurityParams() {
				var sitename = document.getElementById("sitename").value;
				var securitytoken = document.getElementById("securitytoken").value;
				var context = document.getElementById("context").value;
				var adminAction = document.getElementById("adminAction").value;
				
				return "securitytoken="+securitytoken+"&context="+context+"&adminAction="+adminAction+"&sitename="+sitename;
			}
								
			function runSimulator() {
				var marsSimulatorForm = document.getElementById("marsSimulatorFormId");
				var rulesVersion = document.getElementById("rulesVersion").value;

				if (rulesVersion.length == 0) {
					document.getElementById("rulesVersion").focus();
					document.getElementById("textMessage").innerHTML = 'Rules Version is required';
					return;
				}
				if (document.getElementById("fileChooser").value.length == 0) {
					document.getElementById("fileChooser").focus();
					document.getElementById("textMessage").innerHTML = 'Input file is required';
					return;
				}
				var length = document.getElementById("fileChooser").value.length;
				if (length < 5) {
					document.getElementById("fileChooser").focus();
					document.getElementById("textMessage").innerHTML = 'Input file should be xls or xlsx';
					return;
				}
				var fileName  = document.getElementById("fileChooser").value;
				var format = fileName.substring(length-4,length);
				if (format != ".xls" && format != "xlsx")  {
					document.getElementById("fileChooser").focus();
					document.getElementById("textMessage").innerHTML = 'Input file should be xls or xlsx';
					return;
				}
				document.forms[0].action = "marsSimulator?versionNo="+rulesVersion+"&actionType=uploadFile&"+getSecurityParams();
				document.forms[0].submit();
			}
			function refreshPage() {
				document.forms[0].action = "marsSimulator?"+getSecurityParams();
				document.forms[0].submit();
			}
			function downloadBaseFile() {
				var marsSimulatorForm = document.getElementById("marsSimulatorFormId");
				var rulesVersion = document.getElementById("rulesVersion").value;
				if (rulesVersion.length == 0) {
					document.getElementById("rulesVersion").focus();
					document.getElementById("textMessage").innerHTML = 'Rules Version is required';
					return;
				}
				document.forms[0].action = "marsSimulator?versionNo="+rulesVersion+"&actionType=downloadInputFile&"+getSecurityParams();
				document.forms[0].submit();
			}
			function downloadInputExcelFile(resultId) {
				document.forms[0].action = "marsSimulator?resultId="+resultId+"&actionType=downloadInputExcelFile&"+getSecurityParams();
				document.forms[0].submit();
			}
			function downloadOutputExcelFile(resultId) {
				document.forms[0].action = "marsSimulator?resultId="+resultId+"&actionType=downloadOutputExcelFile&"+getSecurityParams();
				document.forms[0].submit();
			}
			function overwriteBaseFile(resultId) {
				document.forms[0].action = "marsSimulator?resultId="+resultId+"&actionType=overwriteBaseFile&"+getSecurityParams();
				document.forms[0].submit();
			}
		</script>
	</body>
</html>
