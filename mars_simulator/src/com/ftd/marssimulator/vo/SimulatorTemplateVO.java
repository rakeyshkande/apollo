package com.ftd.marssimulator.vo;

import java.util.Date;

public class SimulatorTemplateVO {

	private String templateHistId;
	private String versionNo;
	private byte[] template;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SimulatorTemplateVO [templateHistId=");
		buffer.append(templateHistId);
		buffer.append(", versionNo=");
		buffer.append(versionNo);
		buffer.append(", template=");
		buffer.append(template);
		buffer.append(", createdOn=");
		buffer.append(createdOn);
		buffer.append(", createdBy=");
		buffer.append(createdBy);
		buffer.append(", updatedOn=");
		buffer.append(updatedOn);
		buffer.append(", updatedBy=");
		buffer.append(updatedBy);
		buffer.append("]");
		return buffer.toString();
	}

	public String getTemplateHistId() {
		return templateHistId;
	}

	public void setTemplateHistId(String templateHistId) {
		this.templateHistId = templateHistId;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public byte[] getTemplate() {
		return template;
	}

	public void setTemplate(byte[] template) {
		this.template = template;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
