package com.ftd.marssimulator.vo;

import java.util.Date;

public class SimulationResultsVO {
	
	private String resultId;
	private String versionNo;
	private byte[] input;
	private byte[] output;
	private String status;
	private String errorMessage;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String inputFileName;
	private String outputFileName;

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SimulationResultsVO [resultId=");
		buffer.append(resultId);
		buffer.append(", versionNo=");
		buffer.append(versionNo);
		buffer.append(", input=");
		buffer.append(input);
		buffer.append(", output=");
		buffer.append(output);
		buffer.append(", status=");
		buffer.append(status);
		buffer.append(", errorMessage=");
		buffer.append(errorMessage);
		buffer.append(", createdOn=");
		buffer.append(createdOn);
		buffer.append(", createdBy=");
		buffer.append(createdBy);
		buffer.append(", updatedOn=");
		buffer.append(updatedOn);
		buffer.append(", updatedBy=");
		buffer.append(updatedBy);
		buffer.append(", inputFileName=");
		buffer.append(inputFileName);
		buffer.append(", outputFileName=");
		buffer.append(outputFileName);
		buffer.append("]");
		return buffer.toString();
	}
	
	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public byte[] getInput() {
		return input;
	}

	public void setInput(byte[] input) {
		this.input = input;
	}

	public byte[] getOutput() {
		return output;
	}

	public void setOutput(byte[] output) {
		this.output = output;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

}
