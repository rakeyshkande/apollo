package com.ftd.marssimulator.mdb;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.marssimulator.bo.MARSSimulatorBO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * 
 * @author kdatchan
 *
 */
@SuppressWarnings("serial")
public class MARSSimulatorMDB implements MessageDrivenBean, MessageListener 
{

  private MessageDrivenContext messageDrivenContext;
  private static final Logger LOGGER = new Logger(MARSSimulatorMDB.class.getName()); 
  
  public void onMessage(Message msg)
  {
      MARSSimulatorBO simulatorBO = null;

      try {
          TextMessage textMessage = (TextMessage) msg;
          if (textMessage != null && textMessage.getText() != null) {
          	simulatorBO = new MARSSimulatorBO();
          	simulatorBO.processRequest(textMessage.getText());
          }
      } 
      catch (Throwable t) {
      	LOGGER.error(t.getMessage(), t);
          throw new RuntimeException(t);
      } 
    
  }

  public void ejbRemove() throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("remove()");
    }
  }

  public void ejbCreate() {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("create()");
    }
  }

  @Override
  public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("setMessageDrivenContext()");
    }
    this.messageDrivenContext = ctx;
  }
  
}