package com.ftd.marssimulator.bo;

import java.util.ArrayList;
import java.util.List;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.marssimulator.exception.RulesException;
import com.ftd.marssimulator.ruleengine.cache.RuleEngineCache;
import com.ftd.marssimulator.util.CommonUtils;
import com.ftd.rulesexecutor.core.domain.RuleExecutionContext;
import com.ftd.rulesexecutor.core.domain.RuleFact;
import com.ftd.rulesexecutor.engine.RuleEngine;
import com.ftd.rulesexecutor.engine.RuleEngineFactory;
import com.ftd.rulesexecutor.engine.RuleExecutor;

/**
 * Business object to invoke the Rule Engine framework 
 * with the given inputs.
 * 
 * <br>
 * 1) Get the Rule Engine Object
 * <br>
 * 2) Execute the Rules for the input
 * <br>
 * 3) Return the Rules response to the Caller.
 * 
 * @author kdatchan
 *
 */
public class RulesProcessorBO {

	public List<RulesResponse> getRulesResponses(List<MarsVO> marsVOs, String versionNo) throws RulesException {
		
		RuleFact ruleFact = null;
		List<RulesResponse> rulesResponses = null;
		RulesResponse rulesResponse = null;
		RuleExecutionContext executionContext = null;
		
		try {
			
			if (marsVOs != null && !marsVOs.isEmpty()) {
				rulesResponses = new ArrayList<RulesResponse>();
				for (MarsVO marsVO : marsVOs) {
					rulesResponse = new RulesResponse();
					if (marsVO != null && marsVO.getNewMercuryPrice() != null && marsVO.getOriginalMercuryPrice() != null) {
						marsVO.setAskpThreshold(marsVO.getNewMercuryPrice().subtract(marsVO.getOriginalMercuryPrice()));
						marsVO.setViewQueueHolidayDate(CommonUtils.getViewQueueHolidayDate());
						marsVO.setViewQueueControl(CommonUtils.getViewQueueControl());
					}
					
					executionContext = new RuleExecutionContext(marsVO, rulesResponse);
					RuleEngine ruleEngine = getRuleEngine(versionNo);
					final RuleExecutor ruleExecutor = new RuleExecutor(ruleEngine);
				  ruleExecutor.process(executionContext);
				  ruleFact = executionContext.getRuleFact();
				  if (ruleFact != null) {
				  	List<Object> objects = ruleFact.getFacts();
				  	
				  	for(Object obj : objects) {
							if (obj instanceof RulesResponse) {
								rulesResponses.add((RulesResponse) obj);
							}
						}
				  }	
				}
			}
		} 
		catch (Exception e) {
			throw new RulesException("Rule Engine failed to process the request", e);
		}
		
		return rulesResponses;
	}
	
	private RuleEngine getRuleEngine(String versionNo) throws Exception {
		
		String groupId = CommonUtils.getGroupId();
		String artifactId = CommonUtils.getArtifactId();
		Long breTimeout = CommonUtils.getBreTimeout();
		
		StringBuffer sb = new StringBuffer(artifactId);
		sb.append(versionNo);
		
		RuleEngine ruleEngine = RuleEngineCache.getInstance().getRuleEngine(sb.toString());
		
		if (ruleEngine != null) {
			return ruleEngine; 
		}
		else {
			ruleEngine = RuleEngineFactory.getRuleEngine(groupId, artifactId, versionNo, breTimeout);
			RuleEngineCache.getInstance().setRuleEngine(sb.toString(), ruleEngine);
		}
		
		return ruleEngine;
	}
}
