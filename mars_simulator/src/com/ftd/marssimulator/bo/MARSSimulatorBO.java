package com.ftd.marssimulator.bo;

import java.sql.Connection;
import java.util.List;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.marssimulator.constant.MARSSimulatorConstant;
import com.ftd.marssimulator.dao.MARSSimulatorDAO;
import com.ftd.marssimulator.exception.ExcelException;
import com.ftd.marssimulator.exception.RulesException;
import com.ftd.marssimulator.util.ExcelUtils;
import com.ftd.marssimulator.util.CommonUtils;
import com.ftd.marssimulator.vo.SimulationResultsVO;
import com.ftd.osp.utilities.plugins.Logger;

import static com.ftd.marssimulator.util.CommonUtils.logDebugMessage;

/**
 * Business object to process the Input Excel file, 
 * Extract the data and Run it against the Rules. 
 * 
 * 
 * @author kdatchan
 *
 */
public class MARSSimulatorBO {
	
	private Connection conn = null;
	private MARSSimulatorDAO simulatorDAO = null;
	private RulesProcessorBO rulesProcessorBO = null;
	private static final Logger LOGGER = new Logger(MARSSimulatorBO.class.getName()); 
	
	/**
	 * Process the request for the given Result Id.
	 * <br>
	 * 1) Get the complete request information from the DB 
	 * for the given Result Id.
	 * <br>
	 * 2) Get the Excel file object from the request information,
	 * parse the Excel and get the MarsVOs.
	 * <br>
	 * 3) Call RulesProcessor to execute the rules for the MarsVO.
	 * <br>
	 * 4) Construct the output Excel and persist into the DB.   
	 * 
	 * @param resultId
	 * @throws Exception
	 */
	public void processRequest(String resultId) throws Exception {
		
		SimulationResultsVO resultsVO = null;
		List<MarsVO> marsVOs = null;
		List<RulesResponse> rulesResponses = null;
		byte[] outputBytes = null;
		StringBuffer sb = new StringBuffer();
		
		sb.append("Proceesing request for Result Id: ").append(resultId);
		logDebugMessage(LOGGER, sb.toString());
		
		try {
			
			// Get the request information from the DB.
			resultsVO = this.getRequestData(resultId);
			
			if (resultsVO != null) {
				marsVOs = ExcelUtils.getMarsVOs(resultsVO.getInput());
			}
			
			if (marsVOs != null) {
				rulesProcessorBO = new RulesProcessorBO();
				rulesResponses = rulesProcessorBO.getRulesResponses(marsVOs, resultsVO.getVersionNo());
			}
			
			if (rulesResponses != null) {
				outputBytes = ExcelUtils.getExcelDataBytes(resultsVO.getInput(), rulesResponses);
			}
			
			if (outputBytes != null) {
				resultsVO.setOutput(outputBytes);
				resultsVO.setStatus(MARSSimulatorConstant.STATUS_Completed);
				this.updateRequestStatus(resultsVO);
			}
			
		}
		catch (ExcelException e) {
			sb = new StringBuffer("Exception caught while processing the excel file");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			this.updateErrorStatus(resultsVO, resultId, e.getMessage());
			
		}
		catch (RulesException e) {
			sb = new StringBuffer("Exception caught while processing the excel file");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			this.updateErrorStatus(resultsVO, resultId, e.getMessage());
		}
		catch (Exception e) {
			sb = new StringBuffer("Exception caught while processing the excel file");
			sb.append(e.getMessage());
			LOGGER.error(sb.toString(), e);
			this.updateErrorStatus(resultsVO, resultId, e.getMessage());
		}
		finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	private SimulationResultsVO getRequestData(String resultId) throws Exception {
	
		SimulationResultsVO resultsVO = null;
		StringBuffer sb = null;
		if (resultId != null) {
			conn = CommonUtils.getConnection();
			simulatorDAO = new MARSSimulatorDAO(conn);
			sb = new StringBuffer("Calling DAO to get the data for Result Id: ");
			sb.append(resultId);
			LOGGER.info(sb.toString());
			resultsVO = simulatorDAO.getNUpdateSimulationResultById(resultId);
			sb = new StringBuffer("Output from DAO. ");
			sb.append(resultsVO);
			logDebugMessage(LOGGER, sb.toString());
		}
		
		return resultsVO;
	}
	
	private void updateRequestStatus(SimulationResultsVO resultsVO) throws Exception {
		
		if (resultsVO != null) {
			simulatorDAO.updateSimulationResults(resultsVO);
		}
	}
	
	private void updateErrorStatus(SimulationResultsVO resultsVO, String resultId, String errorMessage) throws Exception {
		
		if (resultsVO == null) {
			resultsVO = new SimulationResultsVO();
		}
		resultsVO.setResultId(resultId);
		resultsVO.setStatus(MARSSimulatorConstant.STATUS_ERROR);
		resultsVO.setErrorMessage(errorMessage);
		this.updateRequestStatus(resultsVO);
	}
	
}
