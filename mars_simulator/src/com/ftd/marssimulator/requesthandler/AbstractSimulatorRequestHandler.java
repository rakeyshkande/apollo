package com.ftd.marssimulator.requesthandler;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.ftd.marssimulator.constant.MARSSimulatorConstant;
import com.ftd.marssimulator.dao.MARSSimulatorDAO;
import com.ftd.marssimulator.util.CommonUtils;
import com.ftd.marssimulator.util.FileHandler;
import com.ftd.marssimulator.vo.SimulationResultsVO;
import com.ftd.marssimulator.vo.SimulatorTemplateVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Abstract class the handle the requests for various action types.
 * 
 * @author kdatchan
 *
 */
public abstract class AbstractSimulatorRequestHandler {

	private static final Logger LOGGER = new Logger("com.ftd.marssimulator.requesthandler.MARSSimulatorRequestHandler");
	
	protected static final String SIMULATOR_PAGE = "WEB-INF/jsp/marsSimulator.jsp";
	protected static final String REFRESH_SIMULATION_HISTORY = "refresh";
	protected static final String DOWNLOAD_INPUT_FILE = "downloadInputFile";
	protected static final String DOWNLOAD_INPUT_EXCEL_FILE = "downloadInputExcelFile";
	protected static final String DOWNLOAD_OUTPUT_EXCEL_FILE = "downloadOutputExcelFile";
	protected static final String OVERWRITE_BASE_FILE = "overwriteBaseFile";
	protected static final String UPLOAD_INPUT_REQUEST = "uploadFile";
	protected static final String SECURITY_TOKEN = "securitytoken";
	protected static final String ACTION = "action";
	protected static final String CONTEXT = "context";
	protected static final String ADMIN_ACTION = "adminAction";
	protected static final String SITE_NAME = "sitename";
	protected static final String ACTION_TYPE = "actionType";
	protected static final String VERSION_NO = "versionNo";
	protected static final String RESULT_ID = "resultId";
	protected static final String MESSAGE = "message";
	protected static final String RESULTS_VOS = "resultsVOs";

	protected static final String TYPE_OUTPUT = "output";
	protected static final String TYPE_INPUT = "input";
	
	protected void defaultRequest(HttpServletRequest request,
			HttpServletResponse response, List<SimulationResultsVO> resultsVOs,
			String message) throws IOException, ServletException {
		try {
			resultsVOs = refreshSimulationHistory(response);
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to get the Simulation results", e);
		}
		dispatchRequest(request, response, SIMULATOR_PAGE, message, resultsVOs);
		return;
	}

	protected String downloadOutputExcelRequest(HttpServletRequest request,
			HttpServletResponse response) {

		String result = null;
		try {
			result = this.downloadExcelRequest(request.getParameter(RESULT_ID), response, TYPE_OUTPUT);
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while downloading the output file", e);
			result = "Failed to download the output file";
		}
		return result;
	}

	protected String downloadInputExcelRequest(HttpServletRequest request,
			HttpServletResponse response) {

		String result = null;
		try {
			result = this.downloadExcelRequest(request.getParameter(RESULT_ID), response, TYPE_INPUT);
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while downloading the input file", e);
			result = "Failed to download the input file";
		}
		return result;
	}

	protected String downloadExcelRequest(String resultId,
			HttpServletResponse response, String type) throws IOException {
		FileHandler fileHandler = null;
		MARSSimulatorDAO dao = null;
		SimulationResultsVO resultsVO = null;

		StringBuffer sb = new StringBuffer("Failed to download the ");
		sb.append(resultId).append(" file. Please try again!");
		String result = sb.toString();
		Connection conn = null;
		try {
			conn = CommonUtils.getConnection();
			dao = new MARSSimulatorDAO(conn);
			resultsVO = dao.getSimulationResultById(resultId);
			if (resultsVO != null) {
				fileHandler = new FileHandler();
				String tempDownloadPath = CommonUtils.getTempDownloadDir();
				sb = new StringBuffer(((tempDownloadPath != null && !tempDownloadPath.isEmpty()) ? tempDownloadPath : MARSSimulatorConstant.DEFAULT_TEMP_DOWNLOAD_DIRECTORY));
				String fileName = null;
				fileHandler = new FileHandler();
				if (TYPE_INPUT.equalsIgnoreCase(type)) {
					fileName = CommonUtils.getInputFileName(resultsVO.getVersionNo());
					sb.append(File.separator).append(fileName);
					fileHandler.writeFileToTempPath(sb.toString(), (byte[]) resultsVO.getInput());
					fileHandler.writeFileToResponse(fileName, sb.toString(), response, (byte[]) resultsVO.getInput());
				} else {
					fileName = CommonUtils.getOutputFileName(resultsVO.getVersionNo(), resultsVO.getCreatedOn(), resultsVO.getCreatedBy());
					sb.append(File.separator).append(fileName);
					fileHandler.writeFileToTempPath(sb.toString(), (byte[]) resultsVO.getOutput());
					fileHandler.writeFileToResponse(fileName, sb.toString(), response, (byte[]) resultsVO.getOutput());
				}
				fileHandler.deleteFile(sb.toString());

				sb = new StringBuffer("Successfully downloaded ");
				sb.append(resultId).append(" file");
				result = sb.toString();
			}
		} 
		catch (Exception e) {
			sb = new StringBuffer("Exception caught while downloading the ");
			sb.append(resultId).append(" file");
			LOGGER.error(sb.toString(), e);
			sb = new StringBuffer("Failed to download the ");
			sb.append(resultId).append(" file");
			result = sb.toString();
		} 
		finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error("Exception caught when trying to close the connection", e);
				}
			}
		}
		return result;
	}

	protected List<SimulationResultsVO> refreshSimulationHistory(
			HttpServletResponse response) throws Exception {
		Connection conn = null;
		MARSSimulatorDAO simulatorDAO = null;
		List<SimulationResultsVO> resultsVOs = null;
		try {
			conn = CommonUtils.getConnection();
			simulatorDAO = new MARSSimulatorDAO(conn);
			resultsVOs = simulatorDAO.getSimulationResults();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error("Exception caught when trying to close the connection", e);
				}
			}
		}
		return resultsVOs;
	}

	protected void dispatchRequest(HttpServletRequest request,
			HttpServletResponse response, String view, String message, Object data)
			throws IOException, ServletException {
		RequestDispatcher rd = null;
		try {
			if (message != null && !message.isEmpty()) {
				request.setAttribute(MESSAGE, message);
			}
			if (data != null) {
				request.setAttribute(RESULTS_VOS, data);
			}
			rd = request.getRequestDispatcher(view);
			rd.include(request, response);
		} catch (ServletException e) {
			LOGGER.error("Error caught while dispatching request:", e);
			rd = request.getRequestDispatcher(SIMULATOR_PAGE);
			rd.forward(request, response);
		}
	}

	protected String overwriteBaseFileRequest(HttpServletRequest request,
			HttpServletResponse response) {

		SimulatorTemplateVO templateVO = null;
		MARSSimulatorDAO dao = null;
		Connection conn = null;
		String result = "Overwrite Base Input file was successful";

		try {
			templateVO = new SimulatorTemplateVO();
			templateVO.setCreatedBy(CommonUtils.getUserId(request, request.getParameter(SECURITY_TOKEN)));
			templateVO.setUpdatedBy(CommonUtils.getUserId(request, request.getParameter(SECURITY_TOKEN)));
			conn = CommonUtils.getConnection();
			dao = new MARSSimulatorDAO(conn);
			if (!dao.overwriteBaseInputFile(templateVO, request.getParameter("resultId"))) {
				result = "Failed to overwrite the base file. Please try again";
			}
		} 
		catch (Exception e) {
			result = "Failed to overwrite the base file. Please try again";
			LOGGER.error(" - Exception caught while overwriting the base input file", e);
		} 
		finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error("Exception caught when trying to close the connection", e);
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	protected String uploadInputRequest(HttpServletRequest request,
			HttpServletResponse response) {

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		SimulationResultsVO resultsVO = null;
		FileHandler fileHandler = null;
		StringBuffer filePath = null;
		MARSSimulatorDAO dao = null;
		Connection conn = null;
		String result = "Request was submitted successfully";
		String responseId = null;

		if (!isMultipart) {
			String errorMessage = "Not a Valid Upload Request. Please select a valid xls file to upload.";
			LOGGER.error(errorMessage);
			return errorMessage;
		}

		try {
			List<FileItem> multiparts = new ServletFileUpload(
					new DiskFileItemFactory()).parseRequest(request);

			for (FileItem item : multiparts) {
				if (!item.isFormField()) {
					String tempUploadPath = CommonUtils.getTempUploadDir();
					filePath = new StringBuffer(((tempUploadPath != null && !tempUploadPath.isEmpty()) ? tempUploadPath : MARSSimulatorConstant.DEFAULT_TEMP_UPLOAD_DIRECTORY));
					filePath.append(File.separator)
									.append("SimulatorRequest")
									.append(System.currentTimeMillis()).append(".xlsx");
					item.write(new File(filePath.toString()));
				}
			}
			if (filePath != null) {
				fileHandler = new FileHandler();
				resultsVO = new SimulationResultsVO();
				resultsVO.setCreatedBy(CommonUtils.getUserId(request, request.getParameter(SECURITY_TOKEN)));
				resultsVO.setUpdatedBy(CommonUtils.getUserId(request, request.getParameter(SECURITY_TOKEN)));
				resultsVO.setVersionNo(request.getParameter(VERSION_NO));
				resultsVO.setInput(fileHandler.getBytesFromStream(filePath.toString()));

				conn = CommonUtils.getConnection();
				dao = new MARSSimulatorDAO(conn);
				responseId = dao.insertSimulationResults(resultsVO);
				if (responseId != null) {
					CommonUtils.sendJMSMessage(null, responseId, MARSSimulatorConstant.MARSSIMULATOR_JMS_QUEUE, MARSSimulatorConstant.MARSSIMULATOR_JMS_QUEUE_DELAY_TIME);
				} 
				else {
					result = "Failed to upload the input file. Please try again";
				}
				fileHandler.deleteFile(filePath.toString());
			}
		} 
		catch (Exception e) {
			result = "Failed to upload the input file. Please try again";
			LOGGER.error(" - Exception caught while uploading the input file", e);
		} 
		finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error("Exception caught when trying to close the connection", e);
				}
			}
		}
		return result;
	}

	protected String downloadInputFile(HttpServletResponse response,
			String versionNo) {
		MARSSimulatorDAO dao = null;
		SimulatorTemplateVO templateVO = null;
		FileHandler fileHandler = null;
		StringBuffer sb = null;
		String result = "Invalid rules version";
		Connection conn = null;
		try {
			conn = CommonUtils.getConnection();
			dao = new MARSSimulatorDAO(conn);
			templateVO = dao.getSimulatorTemplate(versionNo);
			if (templateVO != null) {
				String fileName = CommonUtils.getInputFileName(templateVO.getVersionNo());
				String tempDownloadPath = CommonUtils.getTempDownloadDir();
				sb = new StringBuffer(((tempDownloadPath != null && !tempDownloadPath.isEmpty()) ? tempDownloadPath : MARSSimulatorConstant.DEFAULT_TEMP_DOWNLOAD_DIRECTORY));
				sb.append(File.separator)
					.append(fileName)
					.append(System.currentTimeMillis());

				fileHandler = new FileHandler();
				fileHandler.writeFileToTempPath(sb.toString(), (byte[]) templateVO.getTemplate());
				fileHandler.writeFileToResponse(fileName, sb.toString(), response, (byte[]) templateVO.getTemplate());
				fileHandler.deleteFile(sb.toString());
				result = "Successfully downloaded the base input file";
			}

		} catch (Exception e) {
			LOGGER.error("Exception caught while downloading the base input file", e);
			result = "Failed to download the base input file";
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					LOGGER.error("Exception caught when trying to close the connection", e);
				}
			}
		}
		return result;
	}
}
