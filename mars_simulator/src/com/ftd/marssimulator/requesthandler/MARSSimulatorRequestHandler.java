/**
 * 
 */
package com.ftd.marssimulator.requesthandler;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.ftd.marssimulator.vo.SimulationResultsVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Spring Request Handler class to handle the request coming from the users.
 * 
 * @author kdatchan
 *
 */
public class MARSSimulatorRequestHandler extends AbstractSimulatorRequestHandler implements HttpRequestHandler  { 
	
	private static Logger LOGGER = new Logger(MARSSimulatorRequestHandler.class.getName());
				
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String actionType = request.getParameter(ACTION_TYPE);
		request.setAttribute(SECURITY_TOKEN, request.getParameter(SECURITY_TOKEN));
		request.setAttribute(ACTION, request.getParameter(ACTION));
		request.setAttribute(CONTEXT, request.getParameter(CONTEXT));
		request.setAttribute(ADMIN_ACTION, request.getParameter(ADMIN_ACTION));
		request.setAttribute(SITE_NAME, request.getParameter(SITE_NAME));
		List<SimulationResultsVO> resultsVOs = null;
		
		String result = null;
		StringBuffer sb = new StringBuffer();
		
		sb.append("Request for action: ").append(actionType);
		LOGGER.info(sb.toString());
		
		// Request for getting the previously submitted requests.
		if(actionType != null && !actionType.isEmpty() && REFRESH_SIMULATION_HISTORY.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for fetching the Simulation results");
			this.defaultRequest(request, response, resultsVOs, result);
			return;
		}
		// Request for downloading the Input file.
		else if(actionType != null && !actionType.isEmpty() && DOWNLOAD_INPUT_EXCEL_FILE.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for downloading the input file");
			result = downloadInputExcelRequest(request, response);
			this.defaultRequest(request, response, resultsVOs, result);
			return;
		}
		// Request for downloading the Output file.
		else if(actionType != null && !actionType.isEmpty() && DOWNLOAD_OUTPUT_EXCEL_FILE.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for downloading the output file");
			result = downloadOutputExcelRequest(request, response);
			this.defaultRequest(request, response, resultsVOs, result);
			return;
		}
		// Request for downloading the Base Input file.
		else if(actionType != null && !actionType.isEmpty() && DOWNLOAD_INPUT_FILE.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for downloading the base input file");
			result = downloadInputFile(response, request.getParameter(VERSION_NO));
			this.defaultRequest(request, response, resultsVOs, result);
			return;
		}
		// Request for uploading the Input file.
		else if(actionType != null && !actionType.isEmpty() && UPLOAD_INPUT_REQUEST.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for Uploading requested file into the DB");
			result = uploadInputRequest(request, response);
			this.defaultRequest(request, response, resultsVOs, result);
			return;
		}		
		// Request for overwriting the Base Input file.
		else if(actionType != null && !actionType.isEmpty() && OVERWRITE_BASE_FILE.equalsIgnoreCase(actionType)) {
			LOGGER.info("Request came for overwriting the base input file");
			result = overwriteBaseFileRequest(request, response);
			defaultRequest(request, response, resultsVOs, result);
			return;
		}
		else {
			LOGGER.info("Request came for loading the Simulator page");
			this.defaultRequest(request, response, resultsVOs, null);
			return;
		}

	}

	

}
