package com.ftd.marssimulator.util;

import java.util.HashMap;
import java.util.Map;

import com.ftd.marssimulator.constant.MARSSimulatorConstant;

public class ColumnVariableMapper {
	
	private static final ColumnVariableMapper COLUMN_VARIABLE_MAPPER = new ColumnVariableMapper();
	private Map<String, String> map = null;
	private boolean status = false;
	
	private ColumnVariableMapper() {
	}
	
	public static ColumnVariableMapper getInstance() {
		return COLUMN_VARIABLE_MAPPER;
	}
	
	public synchronized Map<String, String> getVariableColumnMap() {
		if (map == null && !status) {
			init();
			status = true;
		}
		return map;
	}
	
	private void init() {
		map = new HashMap<String, String>();
		map.put(MARSSimulatorConstant.COLUMN_ORDER_DATE, MARSSimulatorConstant.VARIABLE_ORDER_DATE);
		map.put(MARSSimulatorConstant.COLUMN_ORDER_ORIGIN, MARSSimulatorConstant.VARIABLE_ORDER_ORIGIN);
		map.put(MARSSimulatorConstant.COLUMN_SOURCE_CODE, MARSSimulatorConstant.VARIABLE_SOURCE_CODE);
		map.put(MARSSimulatorConstant.COLUMN_MESSAGE_TYPE, MARSSimulatorConstant.VARIABLE_MESSAGE_TYPE);
		map.put(MARSSimulatorConstant.COLUMN_DELIVERY_DATE, MARSSimulatorConstant.VARIABLE_DELIVERY_DATE);
		map.put(MARSSimulatorConstant.COLUMN_PRODUCT_ID, MARSSimulatorConstant.VARIABLE_PRODUCT_ID);
		map.put(MARSSimulatorConstant.COLUMN_DIRECTION, MARSSimulatorConstant.VARIABLE_DIRECTION);
		map.put(MARSSimulatorConstant.COLUMN_MESSAGE_TEXT, MARSSimulatorConstant.VARIABLE_MESSAGE_TEXT);
		map.put(MARSSimulatorConstant.COLUMN_SAK_TEXT, MARSSimulatorConstant.VARIABLE_SAK_TEXT);
		map.put(MARSSimulatorConstant.COLUMN_CUT_OFF_TIME, MARSSimulatorConstant.VARIABLE_CUT_OFF_TIME);
		map.put(MARSSimulatorConstant.COLUMN_OCCASION, MARSSimulatorConstant.VARIABLE_OCCASION);
		map.put(MARSSimulatorConstant.COLUMN_DELIVERY_ZIP_CODE, MARSSimulatorConstant.VARIABLE_DELIVERY_ZIP_CODE);
		map.put(MARSSimulatorConstant.COLUMN_DELIVERY_STATE, MARSSimulatorConstant.VARIABLE_DELIVERY_STATE);
		map.put(MARSSimulatorConstant.COLUMN_DELIVERY_CITY, MARSSimulatorConstant.VARIABLE_DELIVERY_CITY);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_FIRST_NAME, MARSSimulatorConstant.VARIABLE_RECIPIENT_FIRST_NAME);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_LAST_NAME, MARSSimulatorConstant.VARIABLE_RECIPIENT_LAST_NAME);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_PHONE_NUMBER, MARSSimulatorConstant.VARIABLE_RECIPIENT_PHONE_NUMBER);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_ADDRESS, MARSSimulatorConstant.VARIABLE_RECIPIENT_ADDRESS);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_CITY, MARSSimulatorConstant.VARIABLE_DELIVERY_CITY);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_STATE, MARSSimulatorConstant.VARIABLE_DELIVERY_STATE);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_ZIP_CODE, MARSSimulatorConstant.VARIABLE_DELIVERY_ZIP_CODE);
		map.put(MARSSimulatorConstant.COLUMN_RECIPIENT_COUNTRY, MARSSimulatorConstant.VARIABLE_RECIPIENT_COUNTRY);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_FIRST_NAME, MARSSimulatorConstant.VARIABLE_CUSTOMER_FIRST_NAME);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_LAST_NAME, MARSSimulatorConstant.VARIABLE_CUSTOMER_LAST_NAME);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_PHONE_NUMBER, MARSSimulatorConstant.VARIABLE_CUSTOMER_PHONE_NUMBER);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_ADDRESS, MARSSimulatorConstant.VARIABLE_CUSTOMER_ADDRESS);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_CITY, MARSSimulatorConstant.VARIABLE_CUSTOMER_CITY);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_STATE, MARSSimulatorConstant.VARIABLE_CUSTOMER_STATE);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_ZIP_CODE, MARSSimulatorConstant.VARIABLE_CUSTOMER_ZIP_CODE);
		map.put(MARSSimulatorConstant.COLUMN_CUSTOMER_COUNTRY, MARSSimulatorConstant.VARIABLE_CUSTOMER_COUNTRY);
		map.put(MARSSimulatorConstant.COLUMN_MERCURY_ID, MARSSimulatorConstant.VARIABLE_MERCURY_ID);
		map.put(MARSSimulatorConstant.COLUMN_MERCURY_NUMBER, MARSSimulatorConstant.VARIABLE_MERCURY_NUMBER);
		map.put(MARSSimulatorConstant.COLUMN_ORIGINAL_MERCURY_PRICE, MARSSimulatorConstant.VARIABLE_ORIGINAL_MERCURY_PRICE);
		map.put(MARSSimulatorConstant.COLUMN_NEW_MERCURY_PRICE, MARSSimulatorConstant.VARIABLE_NEW_MERCURY_PRICE);
		map.put(MARSSimulatorConstant.COLUMN_ASKP_THRESHOLD, MARSSimulatorConstant.VARIABLE_ASKP_THRESHOLD);
		map.put(MARSSimulatorConstant.COLUMN_TIME_ZONE, MARSSimulatorConstant.VARIABLE_TIME_ZONE);
		map.put(MARSSimulatorConstant.COLUMN_PREFERRED_PARTNER_ID, MARSSimulatorConstant.VARIABLE_PREFERRED_PARTNER_ID);
		map.put(MARSSimulatorConstant.COLUMN_ORDER_BOUNCE_COUNT, MARSSimulatorConstant.VARIABLE_ORDER_BOUNCE_COUNT);
		map.put(MARSSimulatorConstant.COLUMN_MARS_ORDER_BOUNCE_COUNT, MARSSimulatorConstant.VARIABLE_MARS_ORDER_BOUNCE_COUNT);
		map.put(MARSSimulatorConstant.COLUMN_FLORIST_SOFT_BLOCK_DAYS, MARSSimulatorConstant.VARIABLE_FLORIST_SOFT_BLOCK_DAYS);
		map.put(MARSSimulatorConstant.COLUMN_PHOENIX_ELIGIBLE, MARSSimulatorConstant.VARIABLE_PHOENIX_ELIGIBLE);
		map.put(MARSSimulatorConstant.COLUMN_ASK_ANSWER_CODE, MARSSimulatorConstant.VARIABLE_ASK_ANSWER_CODE);
		map.put(MARSSimulatorConstant.COLUMN_ORDER_STATUS, MARSSimulatorConstant.VARIABLE_ORDER_STATUS);
		map.put(MARSSimulatorConstant.COLUMN_ENABLED, MARSSimulatorConstant.VARIABLE_ENABLED);
		map.put(MARSSimulatorConstant.COLUMN_ORDER_COMMENTS, MARSSimulatorConstant.VARIABLE_ORDER_COMMENTS);
		map.put(MARSSimulatorConstant.COLUMN_VIEW_QUEUE, MARSSimulatorConstant.VARIABLE_VIEW_QUEUE);
		map.put(MARSSimulatorConstant.COLUMN_ORDER_DETAIL_ID, MARSSimulatorConstant.VARIABLE_ORDER_DETAIL_ID);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_CUTOFF_TIME, MARSSimulatorConstant.VARIABLE_GLOBAL_CUTOFF_TIME);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_MONDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_MONDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_TUESDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_TUESDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_WEDNESDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_WEDNESDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_THURSDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_THURSDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_FRIDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_FRIDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_SATURDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_SATURDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_SUNDAY_CUTOFF, MARSSimulatorConstant.VARIABLE_GLOBAL_SUNDAY_CUTOFF);
		map.put(MARSSimulatorConstant.COLUMN_AUTO_RESPONSE_CUTOFF_STOP, MARSSimulatorConstant.VARIABLE_AUTO_RESPONSE_CUTOFF_STOP);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_REJECT_RETRY_LIMIT, MARSSimulatorConstant.VARIABLE_GLOBAL_REJECT_RETRY_LIMIT);
		map.put(MARSSimulatorConstant.COLUMN_GLOBAL_MARS_ORDER_COUNT, MARSSimulatorConstant.VARIABLE_GLOBAL_MARS_ORDER_COUNT);
		map.put(MARSSimulatorConstant.COLUMN_CODIFICATION_ID, MARSSimulatorConstant.VARIABLE_CODIFICATION_ID);
		map.put(MARSSimulatorConstant.COLUMN_CITY_SEARCH_ORDER, MARSSimulatorConstant.VARIABLE_CITY_SEARCH_ORDER);
		map.put(MARSSimulatorConstant.COLUMN_VIEW_QUEUE_CONTROL, MARSSimulatorConstant.VARIABLE_VIEW_QUEUE_CONTROL);
	}
	 
		
}
