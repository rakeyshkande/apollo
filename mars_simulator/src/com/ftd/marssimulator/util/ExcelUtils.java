package com.ftd.marssimulator.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.core.domain.response.RulesResponse;
import com.ftd.marssimulator.constant.MARSSimulatorConstant;
import com.ftd.marssimulator.exception.ExcelException;

/**
 * 
 * An Utility class to Read/Write the Excel data object
 * 
 * @author kdatchan
 *
 */
public class ExcelUtils {
			
	public static List<MarsVO> getMarsVOs(byte[] input) throws ExcelException {
		ByteArrayInputStream baisInput = null;
		List<MarsVO> marsVOs = null;
		
		if(input != null && input.length > 0) {
			try { 
  			baisInput = new ByteArrayInputStream(input);
  			marsVOs = readExcel(baisInput);
			}
			finally {
				if (baisInput != null) {
					try {
						baisInput.close();
					} 
					catch (IOException e) {
						throw new ExcelException(e.getMessage(), e);
					}
				}
			}
		}
		return marsVOs;
	}

	/**
	 * An utility method to read the Excel object and prepare the MarsVOs
	 * 
	 * <br>
	 * 1. Read the Excel object and create an Excel worksheet using Apache POI.
	 * <br>
	 * 2. The input data in the excel file will start only from a Header row num. 
	 * Get the Header row num from the Global Parms.
	 * <br>
	 * 3. Validate all the columns present in the Excel file. The columns are pre-defined in MARSSimulatorConstant
	 * and only the columns that are present in the constants file will be used. Error will thrown if incorrec/invalid
	 * columns are present in the Excel file.
	 * <br>
	 * 4. Map the data into the MarsVO pojo using java reflection. This is done using ColumnVariableMapper utility class.
	 * <br>
	 * 
	 * @param baisInput
	 * @return
	 * @throws ExcelException
	 */
	private static List<MarsVO> readExcel(ByteArrayInputStream baisInput) throws ExcelException {
		MarsVO marsVO = null; 
		List<MarsVO> marsVOs = new ArrayList<MarsVO>();
		int rowNum = 0;
		int cellNum = 0;
		int startRowNum = 0;
		XSSFWorkbook workbook = null;
		try {
			// Step 1: The input data in the excel file will start only from a certain row num. 
  		// Get the starting row num from the Global Parms.
			startRowNum = ((CommonUtils.getStartExcelRowNum() != null && !CommonUtils.getStartExcelRowNum().isEmpty()) ? Integer.valueOf(CommonUtils.getStartExcelRowNum()) : MARSSimulatorConstant.DEFAULT_START_EXCEL_ROW_NUM);
			workbook = new XSSFWorkbook(baisInput);
			XSSFSheet sheet = workbook.getSheetAt(0);
			TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
			Iterator<Row> rowIterator = sheet.iterator();
			// Step 2: Iterate the excel rows
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (rowNum >= startRowNum) {
					marsVO = new MarsVO();
					Iterator<Cell> cellIterator = row.cellIterator();
					// Step 3: Iterate the excel cells
					while(cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						if (cellNum > 0) {
							// Step 4: Start Row Num will have Header section(i.e. column names).
							// Columns names validated if the Current Row Num = Header Row Num
							if (rowNum == startRowNum) {
								// Step 4.1: Column names are validated
								if (!validateColumnName(cell.getStringCellValue(), treeMap, cellNum)) {
									throw new ExcelException(cell.getStringCellValue() + " is not the right column name. Remove this column and retry.");
								}
							}
							// Step 4.2: Data is mapped to the POJO.
							else {
								mapCellToMarsVO(marsVO, treeMap, cellNum, cell);
							}
						}
						cellNum++;
					}
					// Step 5: POJOs are created only after the Header section. So add the POJO into 
					// the list after the Header Row Num
					if (rowNum > startRowNum) {
						marsVOs.add(marsVO);	
					}
					cellNum = 0;
				}
				rowNum++;
			}
		} 
		catch (IOException e) {
			throw new ExcelException(e.getMessage());
		} 
		catch (ExcelException e) {
			StringBuffer sb = new StringBuffer();
			if (rowNum != 0 && cellNum != 0) {
				sb.append("Bad data at row ").append(rowNum + 2).append("; cell ").append(cellNum + 1).append("; ");
			}
			sb.append(e.getMessage());
			throw new ExcelException(sb.toString(), e);
		} catch (NumberFormatException e) {
			throw new ExcelException("Unable to parse the Global parm for getting the Start Excel Row Num", e);
		} catch (Exception e) {
			throw new ExcelException("Unable to parse the Excel", e);
		} 
		return marsVOs;
	}

	/**
	 * An utility class build the output Excel file from 1. Input Excel and 2. Rules response
	 * 
	 * <br>
	 * 1. Read the Input Excel object and create an output Excel worksheet using Apache POI.
	 * <br>
	 * 2. The input data in the excel file will start only from a Header row num. 
	 * Get the Header row num from the Global Parms.
	 * <br>
	 * 3. Map the data into the Rules Response pojo into the output excel.
	 * <br>
	 * 
	 * @param inputBytes
	 * @param rulesResponses
	 * @return
	 * @throws IOException
	 * @throws ExcelException
	 */
	public static byte[] getExcelDataBytes(byte[] inputBytes, List<RulesResponse> rulesResponses) throws IOException, ExcelException {

		byte[] outputBytes = null;
		ByteArrayInputStream bais = null;
		ByteArrayOutputStream baos = null;
		int expectedActionCellNum = 0;
		int startRowNum = 0;
		if (inputBytes != null && inputBytes.length > 0 && rulesResponses != null && !rulesResponses.isEmpty()) {
			
  		try {
  			// Step 1: Get the Header section row from the global parm
  			startRowNum = ((CommonUtils.getStartExcelRowNum() != null && !CommonUtils.getStartExcelRowNum().isEmpty()) ? Integer.valueOf(CommonUtils.getStartExcelRowNum()) : MARSSimulatorConstant.DEFAULT_START_EXCEL_ROW_NUM);
  			bais = new ByteArrayInputStream(inputBytes);

  			XSSFWorkbook workbook = new XSSFWorkbook(bais);
  			Cell headerCell = null;
  			Cell bodyCell = null;
  			// Step 2: Create the output excel worksheet  			
  			XSSFSheet sheet = workbook.getSheetAt(0);
  			Iterator<Row> rowIterator = sheet.iterator();
  			int rowNum = 0;
  			int cellNum = 0;
  			int rulesResponseCount = 0;
  			while (rowIterator.hasNext()) {
  				Row row = rowIterator.next();
  				if (rowNum >= startRowNum) {
  					Iterator<Cell> cellIterator = row.cellIterator();
  					while(cellIterator.hasNext()) {
  						Cell cell = cellIterator.next();
  						if (cellNum > 0) {
  							// Step 3: Start the processing from the Header Row num. If the current row num = header row num then get the header cell.
  							// Header Cell is required for creating new Headers(i.e. Actual Action, Execution Status and Rules Executed)
  							if (rowNum == startRowNum) {
  								headerCell = cell;
  								// Step 3.1: Get the value of the Expected Action column value for the current row. This is required for comparing 
  								// the Expected Action vs Actual Action and generate the result in Execution Status column.
  								if (isExpectedActionColumn(cell.getStringCellValue())) {
  									expectedActionCellNum = cellNum;
  								}
  							}
  							// Step 4: Get the cell properties for the body cell 
  							else {
  								bodyCell = cell;
  							}
  						}
  						cellNum++;
  					}
  					// Step 5: Add new columns i.e.Actual Action, Execution Status and Rules Executed on the output Excel worksheet.
  					// Style for the new columns are copied from the Header cell which is initialized at Step 3:
  					if (rowNum == startRowNum) {
  						CellStyle style = workbook.createCellStyle();
  						style.cloneStyleFrom(headerCell.getCellStyle());
  						style.setFillBackgroundColor(IndexedColors.ORANGE.getIndex());
  						style.setFillPattern(CellStyle.LESS_DOTS);
  						style.setWrapText(true);
  						Cell newCell1 = row.createCell(cellNum);
  						sheet.setColumnWidth(cellNum, 5000);
  						newCell1.setCellValue(MARSSimulatorConstant.COLUMN_ACTUAL_ACTION);
  						newCell1.setCellStyle(style);
  						Cell newCell2 = row.createCell(cellNum + 1);
  						sheet.setColumnWidth(cellNum + 1, 5000);
  						newCell2.setCellStyle(style);
  						newCell2.setCellValue(MARSSimulatorConstant.COLUMN_EXECUTION_STATUS);
  						Cell newCell3 = row.createCell(cellNum + 2);
  						sheet.setColumnWidth(cellNum + 2, 10000);
  						newCell3.setCellStyle(style);
  						newCell3.setCellValue(MARSSimulatorConstant.COLUMN_RULE_EXECUTED);
  					}
  					// Step 6: Value for the columns are populated from the Rules response object.
  					// Style for the new columns are copied from the Body cell which is initialized at Step 3.2
  					else {
  						String expectedActionText = row.getCell(expectedActionCellNum).getStringCellValue();
  						Cell newCell1 = row.createCell(cellNum);
  						newCell1.setCellStyle(bodyCell.getCellStyle());
  						Cell newCell2 = row.createCell(cellNum + 1);
  						newCell2.setCellStyle(bodyCell.getCellStyle());
  						Cell newCell3 = row.createCell(cellNum + 2);
  						newCell3.setCellStyle(bodyCell.getCellStyle());
  						if (rulesResponseCount <= rulesResponses.size()) {
    						mapRulesResponseToExcel(newCell1, newCell2, newCell3, rulesResponses.get(rulesResponseCount), expectedActionText);
  						}
  						rulesResponseCount++;
  					}
  					cellNum = 0;
  				}
  				rowNum++;
  			}
  			baos = new ByteArrayOutputStream();
  			workbook.write(baos);
  			
  			if (baos != null) {
  				outputBytes = baos.toByteArray();
  			}
  			
  		}	catch (NumberFormatException e) {
  			throw new ExcelException("Unable to parse the Global parm for getting the Start Excel Row Num", e);
  		} catch (Exception e) {
  			throw new ExcelException("Unable to parse the Excel", e);
  		} 
  		finally {
  			if (bais != null) {
  				bais.close();
  			}
  			
  			if (baos != null) {
  				baos.close();
  			}
  		}
		}
		return outputBytes;
	}
	
	private static boolean validateColumnName(String columnName, TreeMap<Integer, String> treeMap, Integer index) {
		Map<String, String> map = ColumnVariableMapper.getInstance().getVariableColumnMap();
		
		if (map != null) {
			if (map.get(columnName) != null && !map.get(columnName).isEmpty()) {
				treeMap.put(index, columnName);
				return true;
			}
			
			if(MARSSimulatorConstant.COLUMN_EXPECTED_ACTION.equalsIgnoreCase(columnName)) {
				return true;
			}
			
		}
		return false;
	}
	
	private static void mapCellToMarsVO(MarsVO marsVO, TreeMap<Integer, String> treeMap, Integer index, Cell cell) throws ExcelException {
		if (treeMap != null && !treeMap.isEmpty()) {
			try {
  			String value = treeMap.get(index);
  			if (cell != null) {
  				Map<String, String> map = ColumnVariableMapper.getInstance().getVariableColumnMap();
  				
  				if (map != null) {
  					if (map.get(value) != null && !map.get(value).isEmpty()) {
  						Field field = marsVO.getClass().getDeclaredField(map.get(value));
  						if (field != null) {
  							field.setAccessible(true);
  							if (MARSSimulatorConstant.TYPE_STRING.equalsIgnoreCase(field.getType().getName())) {
  								field.set(marsVO, cell.getStringCellValue());
  							}
  							else if(MARSSimulatorConstant.TYPE_LONG.equalsIgnoreCase(field.getType().getName())) {
  								field.set(marsVO, Double.valueOf(cell.getNumericCellValue()).longValue());
  							}
  							else if(MARSSimulatorConstant.TYPE_BIG_DECIMAL.equalsIgnoreCase(field.getType().getName())) {
  								field.set(marsVO, new BigDecimal(String.valueOf(cell.getNumericCellValue())));
  							}
  							else if(MARSSimulatorConstant.TYPE_DATE.equalsIgnoreCase(field.getType().getName())) {
  								String deliveryDateString = cell.getStringCellValue();
  								if (deliveryDateString != null && !deliveryDateString.isEmpty()) {
  									DateFormat df = new SimpleDateFormat(MARSSimulatorConstant.TIME_FORMAT_1);
  									field.set(marsVO, df.parse(deliveryDateString));
  								}
  							}
  						}
  					}
  				}
  			}
  			if (marsVO != null) {
  				if (marsVO.getMessageText() != null) {
  					if (marsVO.getMessageText().isEmpty()) {
  						marsVO.setMessageText(null);	
  					}
  					else{
  						marsVO.setMessageText(marsVO.getMessageText().toUpperCase());
  					}
  				}
  				if (marsVO.getProductId() != null) {
  					if (marsVO.getProductId().isEmpty()) {
  						marsVO.setProductId(null);	
  					}
  					else{
  						marsVO.setProductId(marsVO.getProductId().toUpperCase());
  					}
  				}
  				if (marsVO.getCodifiedId() != null) {
  					if (marsVO.getCodifiedId().isEmpty()) {
  						marsVO.setCodifiedId(null);	
  					}
  					else{
  						marsVO.setCodifiedId(marsVO.getCodifiedId().toUpperCase());
  					}
  				}
  				if (marsVO.getDeliveryCity() != null) {
  					if (marsVO.getDeliveryCity().isEmpty()) {
  						marsVO.setDeliveryCity(null);	
  					}
  					else{
  						marsVO.setDeliveryCity(marsVO.getDeliveryCity().toUpperCase());
  					}
  				}
  				if (marsVO.getDeliveryState() != null) {
  					if (marsVO.getDeliveryState().isEmpty()) {
  						marsVO.setDeliveryState(null);	
  					}
  					else{
  						marsVO.setDeliveryState(marsVO.getDeliveryState().toUpperCase());
  					}
  				}
  				if (marsVO.getDeliveryZipCode() != null) {
  					if (marsVO.getDeliveryZipCode().isEmpty()) {
  						marsVO.setDeliveryZipCode(null);	
  					}
  					else{
  						marsVO.setDeliveryZipCode(marsVO.getDeliveryZipCode().toUpperCase());
  					}
  				}
  				if (marsVO.getRecipientCity() != null) {
  					if (marsVO.getRecipientCity().isEmpty()) {
  						marsVO.setRecipientCity(null);	
  					}
  					else{
  						marsVO.setRecipientCity(marsVO.getRecipientCity().toUpperCase());
  					}
  				}
  				if (marsVO.getRecipientState() != null) {
  					if (marsVO.getRecipientState().isEmpty()) {
  						marsVO.setRecipientState(null);	
  					}
  					else{
  						marsVO.setRecipientState(marsVO.getRecipientState().toUpperCase());
  					}
  				}
  				if (marsVO.getRecipientZipCode() != null) {
  					if (marsVO.getRecipientZipCode().isEmpty()) {
  						marsVO.setRecipientZipCode(null);	
  					}
  					else{
  						marsVO.setRecipientZipCode(marsVO.getRecipientZipCode().toUpperCase());
  					}
  				}
  				if (marsVO.getRecipientCountry() != null) {
  					if (marsVO.getRecipientCountry().isEmpty()) {
  						marsVO.setRecipientCountry(null);	
  					}
  					else{
  						marsVO.setRecipientCountry(marsVO.getRecipientCountry().toUpperCase());
  					}
  				}
  				
  				marsVO.setAutoResponseCutoffStop(CommonUtils.getAutoResponseCutoffStop());
  				marsVO.setGlobalMondayCutoff(CommonUtils.getGlobalMondayCutoff());
  				marsVO.setGlobalTuesdayCutoff(CommonUtils.getGlobalTuesdayCutoff());
  				marsVO.setGlobalWednesdayCutoff(CommonUtils.getGlobalWednesdayCutoff());
  				marsVO.setGlobalThursdayCutoff(CommonUtils.getGlobalThursdayCutoff());
  				marsVO.setGlobalFridayCutoff(CommonUtils.getGlobalFridayCutoff());
  				marsVO.setGlobalSaturdayCutoff(CommonUtils.getGlobalSaturdayCutoff());
  				marsVO.setGlobalSundayCutoff(CommonUtils.getGlobalSaturdayCutoff());
  			}
			}
			catch (ParseException e) {
				throw new ExcelException("Unable to parse the delivery date");
			}
			catch (IllegalStateException e) {
				throw new ExcelException(e.getMessage());
			}
			catch (Exception e) {
				throw new ExcelException(e.getMessage());
			}
		}
	}
	
	private static void mapRulesResponseToExcel(Cell cell1, Cell cell2, Cell cell3, RulesResponse rulesResponse, String expectedActionText) {

		Map<String, List<String>> ruleFiredMap = null;
		
		if (rulesResponse != null) {
			ruleFiredMap = rulesResponse.getRulesFiredMap();
		}
		if (ruleFiredMap != null) {
			Set<String> rulesFired = ruleFiredMap.keySet();
			String tempCell1 = null;
			String tempCell3 = null;
			for (String ruleFired : rulesFired) {
				if (ruleFired != null && ruleFired.contains("ACTION_DESCRIPTION")) {
					continue;
				}
				List<String> actionCodes = (List<String>) ruleFiredMap.get(ruleFired);
				if (tempCell1 != null) {
					tempCell1 = tempCell1 +  "," + (actionCodes != null && !actionCodes.isEmpty() ? actionCodes.toString().replaceAll("[\\s\\[\\]]", "") : null);
				}
				else {
					tempCell1 = actionCodes != null && !actionCodes.isEmpty() ? actionCodes.toString().replaceAll("[\\s\\[\\]]", "") : null;	
				}
				
				if (expectedActionText != null && tempCell1 != null && expectedActionText.trim().equalsIgnoreCase(tempCell1.trim())) {
					cell2.setCellValue(MARSSimulatorConstant.PASSED);
				}
				else {
					cell2.setCellValue(MARSSimulatorConstant.FAILED);
				}
				if (tempCell3 != null) {
					tempCell3 = tempCell3 + ", " + ruleFired;
				}
				else {
					tempCell3 = ruleFired;	
				}
				
			}	
			cell1.setCellValue(tempCell1);
			cell3.setCellValue(tempCell3);
		}
		else {
			cell3.setCellValue("Input condition didn't match with any Rules. So the default action is set");
			if (expectedActionText != null && MARSSimulatorConstant.EXECUTE_EXISTING_LOGIC.equalsIgnoreCase(expectedActionText.trim())) {
				cell2.setCellValue(MARSSimulatorConstant.PASSED);
			}
			else {
				cell2.setCellValue(MARSSimulatorConstant.FAILED);
			}
			cell1.setCellValue(MARSSimulatorConstant.EXECUTE_EXISTING_LOGIC);
		}
		
	}

	private static boolean isExpectedActionColumn(String stringCellValue) {

		if (stringCellValue != null && MARSSimulatorConstant.COLUMN_EXPECTED_ACTION.equalsIgnoreCase(stringCellValue)) {
			return true;
		}
		return false;
	}
}
