package com.ftd.marssimulator.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.core.domain.request.MarsVO;
import com.ftd.marssimulator.constant.MARSSimulatorConstant;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

public class CommonUtils {

	private static final String CONFIG_FILE = "mars_simulator_config.xml";
	private static final String DATASOURCE = "MARS_SIMULATOR_DS";
	private static final Logger LOGGER = new Logger(CommonUtils.class.getName());

	public static Connection getConnection() throws Exception {

		ConfigurationUtil config = ConfigurationUtil.getInstance();
		String dbConnection = config.getProperty(CONFIG_FILE, DATASOURCE);

		return DataSourceUtil.getInstance().getConnection(dbConnection);
	}

	public static String getGroupId() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.GROUP_ID);
	}

	public static String getArtifactId() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.ARTIFACT_ID);
	}

	public static long getBreTimeout() throws Exception {
		return Long.valueOf(getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.BRE_TIMEOUT));
	}

	public static Integer getDisplayCount() {
		try {
			return Integer.valueOf(getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
					MARSSimulatorConstant.SHOW_SIMULATION_RESULTS_COUNT));
		} 
		catch (Exception e) {
			return 0;
		}
	}

	public static String getTempUploadDir() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.TEMP_UPLOAD_DIRECTORY);
	}

	public static String getTempDownloadDir() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.TEMP_DOWNLOAD_DIRECTORY);
	}

	private static String getGlobalParam(String context, String name)
			throws Exception {
		String paramValue = null;

		ConfigurationUtil configUtil;
		try {
			configUtil = ConfigurationUtil.getInstance();
			paramValue = configUtil.getFrpGlobalParm(context, name);
		} catch (Exception e) {
			throw e;
		}

		return paramValue;
	}

	public static String getInputFileName(String versionNo) {
		StringBuffer fileName = new StringBuffer("MARSimulatorv");
		fileName.append(versionNo != null ? versionNo.replace(".", "") : "");
		fileName.append(".xlsx");
		return fileName.toString();
	}

	public static String getOutputFileName(String versionNo, Date createdOn,
			String createdBy) {

		StringBuffer fileName = new StringBuffer();
		fileName.append(createdOn != null ? getStringFromDate(createdOn) : "");
		fileName.append(new Random().nextInt(99));
		fileName.append("v");
		fileName.append(versionNo != null ? versionNo.replace(".", "") : "");

		fileName.append(createdBy != null ? createdBy.replace(".", "") : "");
		fileName.append(".xlsx");
		return fileName.toString();
	}

	private static String getStringFromDate(Date createdOn) {
		DateFormat df = new SimpleDateFormat(MARSSimulatorConstant.TIME_FORMAT);
		return df.format(createdOn);
	}

	public static String getUserId(HttpServletRequest request, String token)
			throws ExpiredIdentityException, ExpiredSessionException,
			InvalidSessionException, SAXException, ParserConfigurationException,
			IOException, SQLException, Exception {
		SecurityManager securityManager = SecurityManager.getInstance();
		return securityManager.getUserInfo(token).getUserID();
	}
	
	public static void sendJMSMessage(InitialContext context, String message, String queue, String delayTime)
      throws Exception {

	  MessageToken token = new MessageToken();
	  token.setMessage(message);
	  if (delayTime != null) {
	  	token.setProperty(MARSSimulatorConstant.JMS_DELAY_PROPERTY, delayTime, MARSSimulatorConstant.TYPE_INT);	
	  }
	  
	  sendJMSMessage(context, token, queue);
  }
	
	private static void sendJMSMessage(InitialContext context, MessageToken messageToken, String queue) throws Exception {
    messageToken.setJMSCorrelationID((String) messageToken.getMessage());

    Dispatcher dispatcher;
		try {
			if (context == null) {
				context = new InitialContext();
			}
			dispatcher = Dispatcher.getInstance();
			messageToken.setStatus(queue);
	    dispatcher.dispatchTextMessage(context, messageToken);
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while trying to send the message to queue: " + queue);
			throw e;
		}
  }
	
	public static void logDebugMessage(Logger LOGGER, String message) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(message);
		}
	}

	public static String getStartExcelRowNum() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
				MARSSimulatorConstant.EXCEL_ROW_NUM);
	}
	
	public static Date getViewQueueHolidayDate() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String dateString;
		try {
			dateString = getGlobalParam(MARSSimulatorConstant.MARS_CONTEXT,
					MARSSimulatorConstant.VIEW_QUEUE_HOLIDAY_DATE);
			return df.parse(dateString);
		} 
		catch (Exception e) {
		}
		return null;
		
	}
	
	public static String getViewQueueControl() throws Exception {
		return getGlobalParam(MARSSimulatorConstant.LMD_CONTEXT,
				MARSSimulatorConstant.VIEW_QUEUE_SWITCH);
	}
	
  /**
   * Gets the MondayCutoff from the global params
   * 
   * @return MondayCutoff
   * @throws Exception
   */
	public static long getGlobalMondayCutoff() throws Exception {
		return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_MONDAY_CUTOFF));
	}
	
  /**
   * Gets the TuesdayCutoff from the global params
   * 
   * @return TuesdayCutoff
   * @throws Exception
   */
	public static long getGlobalTuesdayCutoff() throws Exception {
		return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_TUESDAY_CUTOFF));
	}
	
  /**
   * Gets the WednesdayCutoff from the global params
   * 
   * @return WednesdayCutoff
   * @throws Exception
   */
	public static long getGlobalWednesdayCutoff() throws Exception {
    return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_WEDNESDAY_CUTOFF));
	}
	
  /**
   * Gets the ThursdayCutoff from the global params
   * 
   * @return ThursdayCutoff
   * @throws Exception
   */
	public static long getGlobalThursdayCutoff() throws Exception {
    return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_THURSDAY_CUTOFF));
	}
	
  /**
   * Gets the FridayCutoff from the global params
   * 
   * @return FridayCutoff
   * @throws Exception
   */
	public static long getGlobalFridayCutoff() throws Exception {
	  return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_FRIDAY_CUTOFF));
	}
	
  /**
   * Gets the SaturdayCutoff from the global params
   * 
   * @return SaturdayCutoff
   * @throws Exception
   */
	public static long getGlobalSaturdayCutoff() throws Exception {
		return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_SATURDAY_CUTOFF));
	}
	
  /**
   * Gets the SundayCutoff from the global params
   * 
   * @return SundayCutoff
   * @throws Exception
   */
	public static long getGlobalSundayCutoff() throws Exception {
	  return Long.valueOf(getGlobalParam(MARSSimulatorConstant.FTDAPPS_PARMS_CONTEXT, MARSSimulatorConstant.GLOBAL_SUNDAY_CUTOFF));
	}
	
  /**
   * Gets the AutoResponseCutoffStop from the global params
   * 
   * @return AutoResponseCutoffStop
   * @throws Exception
   */
	public static long getAutoResponseCutoffStop() throws Exception {
    return Long.valueOf(getGlobalParam(MARSSimulatorConstant.ORDER_PROCESSING_CONTEXT, MARSSimulatorConstant.AUTO_RESP_BEFORE_CUTOFF));
	}
	
	public static long getCutoff(MarsVO mars) {
		long cutoffTime = 1400;
		Calendar date = Calendar.getInstance();
		switch (date.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			cutoffTime = mars.getGlobalMondayCutoff();
			break;

		case Calendar.TUESDAY:
			cutoffTime = mars.getGlobalTuesdayCutoff();
			break;

		case Calendar.WEDNESDAY:
			cutoffTime = mars.getGlobalWednesdayCutoff();
			break;

		case Calendar.THURSDAY:
			cutoffTime = mars.getGlobalThursdayCutoff();
			break;

		case Calendar.FRIDAY:
			cutoffTime = mars.getGlobalFridayCutoff();
			break;

		case Calendar.SATURDAY:
			cutoffTime = mars.getGlobalSaturdayCutoff();
			break;

		case Calendar.SUNDAY:
			cutoffTime = mars.getGlobalSundayCutoff();
			break;
		}
		return cutoffTime;
	}
	

}
