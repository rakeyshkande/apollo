package com.ftd.marssimulator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.ftd.osp.utilities.plugins.Logger;

public class FileHandler {
	public static final Logger logger = new Logger(FileHandler.class.getName());
	
	public void writeFileToResponse(String fileName, String filePath,
			HttpServletResponse resp, byte[] buffer) throws IOException {
		
		ServletOutputStream out = null;
		try 
		{
  		if (resp != null) {
  			String mimeType = new MimetypesFileTypeMap().getContentType(filePath);
  			String contentDispositionValue = "attachment;filename=" + fileName;
  			long fileSize = new File(filePath).length();
  			out = resp.getOutputStream();
  			resp.setContentType(mimeType);
  			resp.setHeader("Content-Disposition", contentDispositionValue);
  			resp.setHeader("Content-Length", String.valueOf(fileSize));
  			
  			out.write(buffer);
  			out.flush();
  			
  		}
		}
		catch (IOException e) {
			logger.error("Exception occured while trying to write the response to a xlsx file");
			throw e;
		}
		finally {
			if (out !=null) {
				out.close();
			}
		}
		
	}
	
	public void writeFileToTempPath(String filePath, byte[] data) throws IOException  {
		
		File file = new File(filePath);
		FileOutputStream fos = null;
		try {
			if (!file.exists()) {
  			file.createNewFile();
  		}
  		
  		fos = new FileOutputStream(file);
  
  		fos.write(data);
  		fos.flush();
		}
		catch (IOException e) {
			logger.error("Exception occured while trying to get the file output stream");
			throw e;
		}
		finally {
			if (fos !=null) {
				fos.close();
			}
		}
	
	}

	public static boolean fileExists(String filePath) {
		if (filePath != null) {
			File f = new File(filePath);
			if (f.exists()) {
				return true;
			}
		}
		return false;
	}
	
	public byte[] getBytesFromStream(String filePath) throws IOException {
		InputStream is = null;
		try {
			is = new FileInputStream(new File(filePath));
			byte[] buffer = IOUtils.toByteArray(is);
			return buffer;
		} catch (IOException e) {
			logger.error("Exception occured while trying to get the input stream");
			throw e;
		}
		finally {
			if (is !=null) {
				is.close();
			}
		}
		
		
	}
	
	public void deleteFile(String filePath) throws IOException {
		File file = new File(filePath);
		if(file.delete()){
			logger.info(file.getName() + " is deleted!");
		}
		else{
			logger.error("Delete operation is failed.");
		}
	}

}
