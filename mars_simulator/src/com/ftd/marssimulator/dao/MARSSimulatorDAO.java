/**
 * 
 */
package com.ftd.marssimulator.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.marssimulator.util.CommonUtils;
import com.ftd.marssimulator.vo.SimulationResultsVO;
import com.ftd.marssimulator.vo.SimulatorTemplateVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Data Access Object to send/receive the informtation
 * from/to the DB
 * <br>
 * Following operations are performed by this class
 * <br>
 * 1. Insert the Base Input file.
 * 2. Update the Base Input file.
 * 3. Overwrite the Base Input file.
 * 4. Get the Input/Output file.
 * 5. Insert the Simulation results.
 * 6. Update the Simulation results.
 * 7. Get the Simulation results.
 * 
 * @author kdatchan
 *
 */
public class MARSSimulatorDAO {

	private static final String N_STRING = "N";
	private static final String STMT_INSERT_SIMULATOR_TEMPLATE = "INSERT_SIMULATOR_TEMPLATE";
	private static final String STMT_INSERT_SIMULATION_RESULTS = "INSERT_SIMULATION_RESULTS";
	private static final String STMT_UPDATE_SIMULATOR_TEMPLATE = "UPDATE_SIMULATOR_TEMPLATE";
	private static final String STMT_UPDATE_SIMULATION_RESULTS = "UPDATE_SIMULATION_RESULTS";
	private static final String STMT_GET_SIMULATOR_TEMPLATE = "GET_SIMULATOR_TEMPLATE";
	private static final String STMT_GET_SIMULATION_RESULTS = "GET_SIMULATION_RESULTS";
	private static final String STMT_OVERWRITE_SIMULATOR_TEMPLATE = "OVERWRITE_SIMULATOR_TEMPLATE";
	private static final String STMT_GET_SIMULATION_RESULT_BY_ID = "GET_SIMULATION_RESULT_BY_ID";
	private static final String STMT_GET_N_UPD_SIMULATION_BY_ID = "GET_N_UPD_SIMULATION_BY_ID";
	private static final String STATUS_PARAM = "RegisterOutParameterStatus";
	private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
	
	private static final String CREATED_ON = "CREATED_ON";
	private static final String CREATED_BY = "CREATED_BY";
	private static final String UPDATED_ON = "UPDATED_ON";
	private static final String UPDATED_BY = "UPDATED_BY";
	private static final String VERSION_NO = "VERSION_NO";
	private static final String TEMPLATE = "TEMPLATE";
	private static final String INPUT = "INPUT";
	private static final String OUTPUT = "OUTPUT";
	private static final String RESULT_ID = "RESULT_ID";
	private static final String IN_CREATED_BY = "IN_CREATED_BY";
	private static final String IN_UPDATED_BY = "IN_UPDATED_BY";
	private static final String IN_VERSION_NO = "IN_VERSION_NO";
	private static final String IN_TEMPLATE = "IN_TEMPLATE";
	private static final String IN_INPUT = "IN_INPUT";
	private static final String IN_OUTPUT = "IN_OUTPUT";
	private static final String IN_RESULT_ID = "IN_RESULT_ID";
	private static final String IN_STATUS = "IN_STATUS";
	private static final String IN_ERROR_MESSAGE = "IN_ERROR_MESSAGE";
	private static final String STATUS = "STATUS";
	private static final String ERROR_MESSAGE = "ERROR_MESSAGE";
		
	private Connection conn;
	private Logger LOGGER = new Logger(this.getClass().getName());

	public MARSSimulatorDAO(Connection conn) {
		this.conn = conn;
	}
	
	/**
	 * Insert the Base Input file with the given data.
	 * 
	 * @param templateVO
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public boolean insertBaseInputFile(SimulatorTemplateVO templateVO) 
			throws Exception {
		
		boolean result = false;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("insertBaseInputFile Start");	
		}
		StringBuffer sb = new StringBuffer("Inserting base input file into DB. ");
		sb.append(templateVO);
		LOGGER.info(sb.toString());
		
		if (this.conn == null) {
			LOGGER.error("DB connection is null");
			return false;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID(STMT_INSERT_SIMULATOR_TEMPLATE);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(IN_VERSION_NO, templateVO.getVersionNo());
			map.put(IN_TEMPLATE, new javax.sql.rowset.serial.SerialBlob(templateVO.getTemplate()));
			map.put(IN_CREATED_BY, templateVO.getCreatedBy());
			map.put(IN_UPDATED_BY, templateVO.getUpdatedBy());
			
			dataRequestObject.setInputParams(map);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if(N_STRING.equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					sb = new StringBuffer("Exception occurred while trying to insert the base input file into the DB. Exception message: ");
					sb.append(outputs.get(MESSAGE_PARAM));
					LOGGER.error(sb.toString());
				}
				else {
					result = true;
				}
			}
			return result;
		} 
		catch(Exception e) {
			LOGGER.error("Error caught in insertBaseInputFile", e);
			return false;
		}
		finally {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("insertBaseInputFile End");	
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateBaseInputFile(SimulatorTemplateVO templateVO) 
			throws Exception {
		
		boolean result = false;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("updateBaseInputFile Start");	
		}
		StringBuffer sb = new StringBuffer("Updating base input file into DB. ");
		sb.append(templateVO);
		LOGGER.info(sb.toString());
		if (this.conn == null) {
			LOGGER.error("DB connection is null");
			return false;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID(STMT_UPDATE_SIMULATOR_TEMPLATE);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(IN_VERSION_NO, templateVO.getVersionNo());
			if (templateVO.getTemplate() != null) {
				map.put(IN_TEMPLATE, conn.createBlob().setBytes(1, templateVO.getTemplate()));	
			}
			map.put(IN_UPDATED_BY, templateVO.getUpdatedBy());
			
			dataRequestObject.setInputParams(map);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if(N_STRING.equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					sb = new StringBuffer("Exception occurred while trying to update the base input file into the DB. Exception message: ");
					sb.append(outputs.get(MESSAGE_PARAM));
					LOGGER.error(sb.toString());
				}
				else {
					result = true;
				}
			}

			return result;

		} 
		catch(Exception e) {
			LOGGER.error("Error caught in updateBaseInputFile", e);
			return false;
		}
		finally {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("updateBaseInputFile End");	
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean overwriteBaseInputFile(SimulatorTemplateVO templateVO, String resultId) 
			throws Exception {
		
		boolean result = false;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("overwriteBaseInputFile Start");	
		}
		StringBuffer sb = new StringBuffer("Overwrite base input file into DB. ");
		sb.append(templateVO).append("; Result Id: ").append(resultId);
		LOGGER.info(sb.toString());
		if (this.conn == null) {
			LOGGER.error("DB connection is null");
			return false;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID(STMT_OVERWRITE_SIMULATOR_TEMPLATE);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(IN_RESULT_ID, Long.valueOf(resultId));
			map.put(IN_VERSION_NO, templateVO.getVersionNo());
			map.put(IN_CREATED_BY, templateVO.getCreatedBy());
			map.put(IN_UPDATED_BY, templateVO.getUpdatedBy());
			
			dataRequestObject.setInputParams(map);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if(N_STRING.equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					sb = new StringBuffer("Exception occurred while trying to overwrite the base input file into the DB. Exception message: ");
					sb.append(outputs.get(MESSAGE_PARAM));
					LOGGER.error(sb.toString());
				}
				else {
					result = true;
				}
			}
			return result;
		} 
		catch(Exception e) {
			LOGGER.error("Error caught in overwriteBaseInputFile", e);
			return false;
		}
		finally {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("overwriteBaseInputFile End");	
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String insertSimulationResults(SimulationResultsVO resultsVO) 
			throws Exception {
		
		String result = null;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("insertSimulationResults Start");	
		}
		StringBuffer sb = new StringBuffer("Insert Simulation Results into DB. ");
		sb.append(resultsVO);
		LOGGER.info(sb.toString());
		if (this.conn == null) {
			LOGGER.error("DB connection is null");
			return null;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID(STMT_INSERT_SIMULATION_RESULTS);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(IN_VERSION_NO, resultsVO.getVersionNo());
			if (resultsVO.getInput() != null) {
				Blob blob = conn.createBlob();
				blob.setBytes(1, resultsVO.getInput());
				map.put(IN_INPUT, blob);	
			}
			map.put(IN_STATUS, resultsVO.getStatus());
			map.put(IN_ERROR_MESSAGE, resultsVO.getErrorMessage());
			map.put(IN_CREATED_BY, resultsVO.getCreatedBy());
			map.put(IN_UPDATED_BY, resultsVO.getUpdatedBy());
			
			dataRequestObject.setInputParams(map);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if(N_STRING.equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					sb = new StringBuffer("Exception occurred while trying to insert the simulation results into the DB. Exception message: ");
					sb.append(outputs.get(MESSAGE_PARAM));
					LOGGER.error(sb.toString());
				}
				else {
					result = (String) outputs.get(MESSAGE_PARAM);
				}
			}

			return result;

		} 
		catch(Exception e) {
			LOGGER.error("Error caught in insertSimulationResults", e);
			return null;
		}
		finally {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("insertSimulationResults End");	
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateSimulationResults(SimulationResultsVO resultsVO) 
			throws Exception {
		
		boolean result = false;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("updateInputRequest Start");	
		}
		StringBuffer sb = new StringBuffer("Update Simulation Results into DB. ");
		sb.append(resultsVO);
		LOGGER.info(sb.toString());
		if (this.conn == null) {
			LOGGER.error("DB connection is null");
			return false;
		}
		
		DataRequest dataRequestObject = null;
		Map<String, Object> outputs = null;
		try {		

			dataRequestObject = new DataRequest();	
			dataRequestObject.setConnection(conn);
			dataRequestObject.setStatementID(STMT_UPDATE_SIMULATION_RESULTS);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(IN_RESULT_ID, Long.valueOf(resultsVO.getResultId()));
			map.put(IN_VERSION_NO, resultsVO.getVersionNo());
			map.put(IN_STATUS, resultsVO.getStatus());
			map.put(IN_ERROR_MESSAGE, resultsVO.getErrorMessage());
			if (resultsVO.getOutput() != null) {
				Blob blob = conn.createBlob();
				blob.setBytes(1, resultsVO.getOutput());
				map.put(IN_OUTPUT, blob);	
			}
			map.put(IN_UPDATED_BY, resultsVO.getUpdatedBy());
			
			dataRequestObject.setInputParams(map);

			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			outputs = (HashMap<String, Object>) dataAccessUtil.execute(dataRequestObject);
			
			if(outputs != null) {				
				if(N_STRING.equalsIgnoreCase((String) outputs.get(STATUS_PARAM))) {
					sb = new StringBuffer("Exception occurred while trying to update the simulation results into the DB. Exception message: ");
					sb.append(outputs.get(MESSAGE_PARAM));
					LOGGER.error(sb.toString());
				}
				else {
					result = true;
				}
			}

			return result;

		} 
		catch(Exception e) {
			LOGGER.error("Error caught in updateSimulationResults", e);
			return false;
		}
		finally {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("updateInputRequest End");	
			}
		}
	}
	
	public SimulatorTemplateVO getSimulatorTemplate(String versionNo) throws Exception {
    
    SimulatorTemplateVO templateVO = null;
    DataRequest dataRequest = new DataRequest();
    
    if (conn == null) {
      LOGGER.error("DB connection is null");
      return null;
    }

		StringBuffer sb = new StringBuffer("Get template for Version No: ");
		sb.append(versionNo);
		LOGGER.info(sb.toString());
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(STMT_GET_SIMULATOR_TEMPLATE);
    Map<String, Object> paramMap = new HashMap<String, Object>();
    paramMap.put(IN_VERSION_NO, versionNo);
    dataRequest.setInputParams(paramMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    if (rs.next()) {
    	templateVO = new SimulatorTemplateVO();
    	templateVO.setCreatedBy(rs.getString(CREATED_BY));
    	templateVO.setCreatedOn(rs.getDate(CREATED_ON));
    	templateVO.setUpdatedBy(rs.getString(UPDATED_BY));
    	templateVO.setUpdatedOn(rs.getDate(UPDATED_ON));
    	Blob blob = rs.getBlob(TEMPLATE);
    	if (blob != null) {
    		templateVO.setTemplate((byte[] )blob.getBytes(1,(int)blob.length()));	
    	}
    	templateVO.setVersionNo(rs.getString(VERSION_NO));
    }
    return templateVO;
  }
	
	public List<SimulationResultsVO> getSimulationResults() throws Exception {
    
		SimulationResultsVO resultsVO = null;
		List<SimulationResultsVO> resultsVOs = null;
    DataRequest dataRequest = new DataRequest();
    int count = 0;
    
    if (conn == null) {
      LOGGER.error("DB connection is null");
      return null;
    }
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(STMT_GET_SIMULATION_RESULTS);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    resultsVOs = new ArrayList<SimulationResultsVO>();
    int displayCount = CommonUtils.getDisplayCount();
    
    while (rs.next()) {
    	if (++count > displayCount && displayCount != 0) {
    		break;
    	}
    	resultsVO = new SimulationResultsVO();
    	resultsVO.setCreatedBy(rs.getString(CREATED_BY));
    	resultsVO.setCreatedOn(rs.getDate(CREATED_ON));
    	resultsVO.setUpdatedBy(rs.getString(UPDATED_BY));
    	resultsVO.setUpdatedOn(rs.getDate(UPDATED_ON));
    	resultsVO.setStatus(rs.getString(STATUS));
    	resultsVO.setErrorMessage(rs.getString(ERROR_MESSAGE));
    	Blob blob1 = rs.getBlob(INPUT);
    	if (blob1 != null) {
    		resultsVO.setInput((byte[] )blob1.getBytes(1,(int)blob1.length()));	
    	}
    	Blob blob2 = rs.getBlob(OUTPUT);
    	if (blob2 != null) {
    		resultsVO.setOutput((byte[] )blob2.getBytes(1,(int)blob2.length()));	
    	}
    	resultsVO.setVersionNo(rs.getString(VERSION_NO));
    	resultsVO.setResultId(rs.getString(RESULT_ID));
    	resultsVO.setInputFileName(CommonUtils.getInputFileName(resultsVO.getVersionNo()));
    	resultsVO.setOutputFileName(CommonUtils.getOutputFileName(resultsVO.getVersionNo(), resultsVO.getCreatedOn(), resultsVO.getCreatedBy()));
    	resultsVOs.add(resultsVO);
    }
    return resultsVOs;
  }

	public SimulationResultsVO getSimulationResultById(String resultId) throws Exception {
    return getSimulationResultById(resultId, null);
	}
	
	public SimulationResultsVO getNUpdateSimulationResultById(String resultId) throws Exception {
	  return getSimulationResultById(resultId, STMT_GET_N_UPD_SIMULATION_BY_ID);
	}
	
	private SimulationResultsVO getSimulationResultById(String resultId, String statementId) throws Exception {
	  
		SimulationResultsVO resultsVO = null;
		
		if (conn == null) {
      LOGGER.error("DB connection is null");
      return null;
    }
		
    DataRequest dataRequest = new DataRequest();
    Map<String, Object> inputMap = new HashMap<String, Object>();
    
    inputMap.put(IN_RESULT_ID, Long.valueOf(resultId));
    
    dataRequest.setConnection(conn);
    if (statementId != null) {
    	dataRequest.setStatementID(STMT_GET_N_UPD_SIMULATION_BY_ID);
    }
    else {
    	dataRequest.setStatementID(STMT_GET_SIMULATION_RESULT_BY_ID);
    }
    dataRequest.setInputParams(inputMap);
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
  
    CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    
    if (rs.next()) {
    	resultsVO = new SimulationResultsVO();
    	resultsVO.setCreatedBy(rs.getString(CREATED_BY));
    	resultsVO.setCreatedOn(rs.getDate(CREATED_ON));
    	resultsVO.setUpdatedBy(rs.getString(UPDATED_BY));
    	resultsVO.setUpdatedOn(rs.getDate(UPDATED_ON));
    	Blob blob1 = rs.getBlob(INPUT);
    	if (blob1 != null) {
    		resultsVO.setInput((byte[] )blob1.getBytes(1,(int)blob1.length()));	
    	}
    	Blob blob2 = rs.getBlob(OUTPUT);
    	if (blob2 != null) {
    		resultsVO.setOutput((byte[] )blob2.getBytes(1,(int)blob2.length()));	
    	}
    	resultsVO.setVersionNo(rs.getString(VERSION_NO));
    	resultsVO.setResultId(rs.getString(RESULT_ID));
    }
    return resultsVO;
	}
}
